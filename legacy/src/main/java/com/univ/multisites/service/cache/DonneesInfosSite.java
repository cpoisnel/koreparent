package com.univ.multisites.service.cache;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSiteFactory;
import com.univ.utils.URLResolver;

/**
 * Bean contenant la liste des sites de l'application en fonction des critères les plus souvent utilisés : les hosts, les code des rubriques, les alias, ...
 * @deprecated Ces données sont désormais calculées par le service {@link com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus}
 */
@Deprecated
public class DonneesInfosSite implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5768970279225460911L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DonneesInfosSite.class);

    private static final String HTTPS = "https";

    private static final String HTTP = "http";

    private final Map<String, InfosSite> infosSitesParCode;

    private final Map<String, InfosSite> infosSitesParHost;

    private final Map<String, InfosSite> infosSitesParCodeRubrique;

    private final Set<String> urlsDefinis;

    private InfosSite sitePrincipal;

    private InfosSite siteGlobal;

    public DonneesInfosSite() {
        final Collection<InfosSite> listeInfosSite = getInfosSitesDefinis();
        infosSitesParCode = new HashMap<>(listeInfosSite.size());
        infosSitesParCodeRubrique = new HashMap<>(listeInfosSite.size());
        infosSitesParHost = new HashMap<>();
        urlsDefinis = new HashSet<>();
        for (final InfosSite infosSite : listeInfosSite) {
            if (infosSite.isActif()) {
                if (infosSite.isSitePrincipal()) {
                    sitePrincipal = infosSite;
                }
                if (StringUtils.isEmpty(infosSite.getCodeRubrique())) {
                    siteGlobal = infosSite;
                }
                infosSitesParCode.put(infosSite.getAlias(), infosSite);
                infosSitesParCodeRubrique.put(infosSite.getCodeRubrique(), infosSite);
                if (StringUtils.isNotEmpty(infosSite.getHttpHostname())) {
                    infosSitesParHost.put(infosSite.getHttpHostname(), infosSite);
                    urlsDefinis.add(URLResolver.getBasePath(infosSite, Boolean.FALSE));
                    urlsDefinis.add(URLResolver.getBasePath(infosSite, Boolean.TRUE));
                }
                if (StringUtils.isNotEmpty(infosSite.getHttpsHostname())) {
                    urlsDefinis.add(URLResolver.getBasePath(infosSite, Boolean.TRUE));
                    infosSitesParHost.put(infosSite.getHttpsHostname(), infosSite);
                }
                for (final String hostAlias : infosSite.getListeHostAlias()) {
                    if (StringUtils.isNotEmpty(hostAlias)) {
                        infosSitesParHost.put(hostAlias, infosSite);
                        urlsDefinis.add(URLResolver.getBasePath(HTTP, hostAlias, infosSite.getHttpPort()));
                        urlsDefinis.add(URLResolver.getBasePath(HTTPS, hostAlias, infosSite.getHttpsPort()));
                    }
                }
            }
        }
        if (sitePrincipal == null && infosSitesParHost.size() >= 1) {
            sitePrincipal = (InfosSite) CollectionUtils.get(infosSitesParHost.values(), 0);
        }
        if (siteGlobal == null) {
            siteGlobal = sitePrincipal;
        }
        LOGGER.info("Chargement de " + infosSitesParCode.size() + " site(s) OK");
    }

    private Collection<InfosSite> getInfosSitesDefinis() {
        Collection<InfosSite> listeInfosSite = null;
        try {
            listeInfosSite = ServiceInfosSiteFactory.getServiceInfosSite().getListeTousInfosSites();
        } catch (final Exception e) {
            LOGGER.error("impossible de récupérer la liste des sites déclarés dans l'application");
        }
        if (listeInfosSite == null) {
            listeInfosSite = Collections.emptyList();
        }
        return listeInfosSite;
    }

    /**
     *
     * @return la liste des sites actifs de l'application par leur code ou une map vide si il n'y en a aucun de déclaré.
     */
    public Map<String, InfosSite> getInfosSitesParCode() {
        return infosSitesParCode;
    }

    /**
     *
     * @return la liste des sites actifs de l'application par leur host.
     */
    public Map<String, InfosSite> getInfosSitesParHost() {
        return infosSitesParHost;
    }

    /**
     *
     * @return la liste des sites actifs de l'application par leur code de rubrique.
     */
    public Map<String, InfosSite> getInfosSitesParCodeRubrique() {
        return infosSitesParCodeRubrique;
    }

    /**
     *
     * @return l'ensemble des urls des sites actifs.
     */
    public Set<String> getUrlsDefinis() {
        return urlsDefinis;
    }

    /**
     *
     * @return le site principal de l'application
     */
    public InfosSite getSitePrincipal() {
        return sitePrincipal;
    }

    /**
     *
     * @return le site global. (pas de code de rubrique ?!?)
     * @deprecated utiliser getSitePrincipal
     */
    @Deprecated
    public InfosSite getSiteGlobal() {
        return siteGlobal;
    }
}
