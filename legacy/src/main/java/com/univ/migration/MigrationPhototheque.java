package com.univ.migration;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.webutils.ContextePage;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.collaboratif.om.Fichiergw;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Photo;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.ContexteDao;
import com.univ.utils.FileUtil;
import com.univ.utils.ImageInfo;
import com.univ.utils.PhotoUtil;

import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

/**
 * The Class MigrationPhototheque.
 *
 * @author Romain
 *         <p>
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 * @deprecated cette classe était utilisée pour la migration de photo 5.0 / 5.1, elle n'a plus de sens en v6 et ne doit être utiliser qu'en cours de migration.
 */
@Deprecated
public class MigrationPhototheque implements Runnable {

    private static final String LOG_DEBUG = "DEBUG";

    private static final String LOG_ERROR = "ERROR";

    private static final String LOG_INFO = "INFO";

    private static ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(MigrationPhototheque.class);

    private static JspWriter outWriter = null;

    private static EspaceCollaboratifDAO espaceCollaboratifDAO = ApplicationContextManager.getCoreContextBean(EspaceCollaboratifDAO.ID_BEAN, EspaceCollaboratifDAO.class);

    public static void main(String[] args) {
        MigrationPhototheque capture = new MigrationPhototheque();
        Thread thread = (new Thread(capture));
        thread.start();
        try {
            thread.join();
        } catch (Exception e) {
            logger.error("Echec thread.join()", e);
        }
    }

    /**
     * Migrer.
     *
     * @param out the out
     * @throws Exception the exception
     */
    public static void migrer(JspWriter out) throws Exception {
        outWriter = out;
        FileAppender<ILoggingEvent> appender;
        MigrationPhototheque mig = new MigrationPhototheque();
        appender = mig.initialisationLog();
        mig.migrerPhototheque();
        logger.detachAppender(appender);
        //appender.close();
    }

    /**
     * Migrer documents.
     *
     * @throws Exception the exception
     */
    public static void migrerPhotos() throws Exception {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        ecrireLog("*** Migration des photos en medias de type photo ***", LOG_INFO);
        //out.flush();
        int nbModifs = 0;
        int nbErreur = 0;
        int nbMigres = 0;
        // creation du répertoire /medias/photo/ si nécessaire
        String repertoire = MediathequeHelper.getAbsolutePath();
        File fRepertoire = new File(repertoire);
        if (!fRepertoire.exists()) {
            fRepertoire.mkdir();
        }
        repertoire = repertoire + File.separator + "photo";
        fRepertoire = new File(repertoire);
        if (!fRepertoire.exists()) {
            fRepertoire.mkdir();
        }
        Photo photo = new Photo();
        photo.init();
        try (ContexteDao contexteDao = new ContexteDao()) {
            photo.setCtx(contexteDao);
            String sRepertoirePhotoOld = "";
            // tous les photos presentes dans la base sont migrees
            photo.select("");
            while (photo.nextItem()) {
                try {
                    boolean isAlreadySync = false;
                    MediaBean media = new MediaBean();
                    media.setId(photo.getIdPhoto());
                    media.setTitre(photo.getTitre());
                    media.setLegende(photo.getLegende());
                    media.setDescription(photo.getDescription());
                    String url = photo.getUrl();
                    sRepertoirePhotoOld = url.substring(0, url.indexOf("/") + 1);
                    if (url.endsWith("gif") || url.endsWith("GIF")) {
                        media.setFormat("image/gif");
                    } else {
                        media.setFormat("image/jpeg");
                    }
                    url = url.substring(url.indexOf("/") + 1);
                    media.setUrl(url);
                    media.setSource(url);
                    media.setIsMutualise("0");
                    media.setTypeRessource("photo");
                    media.setTypeMedia(photo.getTypePhoto());
                    media.setCodeRubrique(photo.getCodeRubrique());
                    media.setCodeRattachement(photo.getCodeStructure());
                    media.setUrlVignette("");
                    media.setCodeRedacteur(photo.getCodeRedacteur());
                    media.setDateCreation(photo.getDateCreation());
                    media.setMetaKeywords(photo.getMetaKeywords());
                    // gestion des donnees specifiques
                    String sDatas = "";
                    Properties properties = new Properties();
                    properties.load(new ByteArrayInputStream(sDatas.getBytes()));
                    properties.setProperty("LARGEUR", photo.getLargeur().toString());
                    properties.setProperty("HAUTEUR", photo.getHauteur().toString());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    properties.store(baos, "");
                    sDatas = baos.toString();
                    media.setSpecificData(sDatas);
                    // gestion des traductions
                    Hashtable<String, String> hLangue = LangueUtil.getListeLangues();
                    sDatas = "";
                    properties = new Properties();
                    properties.load(new ByteArrayInputStream(sDatas.getBytes()));
                    // français par defaut, supprimé ici
                    if (hLangue.containsKey("0")) {
                        hLangue.remove("0");
                    }
                    String sMetaKeywords = photo.getMetaKeywords();
                    if (hLangue.size() > 0) {
                        int idxDebut = 0;
                        Enumeration<String> en = hLangue.keys();
                        while (en.hasMoreElements()) {
                            String sLg = en.nextElement();
                            if ((idxDebut = sMetaKeywords.indexOf("[" + sLg)) != -1) {
                                int idxFin = sMetaKeywords.indexOf("]", idxDebut);
                                if (idxFin != -1) {
                                    String sLangue = sMetaKeywords.substring(idxDebut + 1, idxFin);
                                    sMetaKeywords = sMetaKeywords.substring(0, idxDebut) + sMetaKeywords.substring(idxFin + 1, sMetaKeywords.length());
                                    String[] itemLg = sLangue.split("/", -2);
                                    if (itemLg.length == 3) {
                                        properties.setProperty("LEGENDE_" + sLg, itemLg[1]);
                                        properties.setProperty("DESCRIPTION_" + sLg, itemLg[2]);
                                    } else {
                                        properties.setProperty("LEGENDE_" + sLg, "");
                                        properties.setProperty("DESCRIPTION_" + sLg, "");
                                    }
                                }
                            } else {
                                properties.setProperty("LEGENDE_" + sLg, "");
                                properties.setProperty("DESCRIPTION_" + sLg, "");
                            }
                        }
                    }
                    baos = new ByteArrayOutputStream();
                    properties.store(baos, "");
                    sDatas = baos.toString();
                    media.setTraductionData(sDatas);
                    // deplacement du fichier
                    File oldPhoto = new File(photo.getPathAbsolu());
                    File newMedia;
                    if (oldPhoto.exists()) {
                        newMedia = new File(MediaUtils.getPathAbsolu(media));
                        if (!newMedia.exists()) {
                            int nResCopierFichier = FileUtil.copierFichier(oldPhoto, newMedia);
                            media.setPoids((long) nResCopierFichier);
                        } else {
                            isAlreadySync = true;
                            nbMigres++;
                        }
                    } else {
                        throw new Exception("fichier " + photo.getPathAbsolu() + " inexistant");
                    }
                    if (!isAlreadySync) {
                        // generation de la vignette
                        try {
                            ImageInfo ii;
                            InputStream inpStream;
                            int largeur;
                            int hauteur;
                            String critere = PropertyHelper.getCoreProperty(MediathequeHelper.CRITERES_VIGNETTE_PROPERTIES_KEY);
                            String[] lstLimite;
                            boolean vignetteOk = false;
                            String PathVignette = PropertyHelper.getCoreProperty("phototheque.path") + File.separator + sRepertoirePhotoOld + "v_" + media.getUrl();
                            File file = new File(PathVignette);
                            if (file.exists()) {
                                ii = new ImageInfo();
                                inpStream = new FileInputStream(PathVignette);
                                ii.setInput(inpStream);
                                ii.check();
                                ii.close();
                                lstLimite = critere.split("/", -2);
                                largeur = Integer.parseInt(lstLimite[1]);
                                hauteur = Integer.parseInt(lstLimite[2]);
                                if (ii.getWidth() >= largeur || ii.getHeight() >= hauteur) {
                                    FileUtil.copierFichier(file, new File(repertoire + File.separator + "v_" + media.getUrl()));
                                    vignetteOk = true;
                                }
                            }
                            if (!vignetteOk) {
                                File fVignette = newMedia;
                                lstLimite = critere.split("/", -2);
                                ii = new ImageInfo();
                                inpStream = new FileInputStream(photo.getPathAbsolu());
                                ii.setInput(inpStream);
                                ii.check();
                                ii.close();
                                largeur = Integer.parseInt(lstLimite[1]);
                                hauteur = Integer.parseInt(lstLimite[2]);
                                //On ne redimensionne que si la taille était plus grande ...
                                if (largeur < ii.getWidth() && hauteur < ii.getHeight()) {
                                    String extension = FileUtil.getExtension(media.getSource());
                                    //resize cree une copie de l'image lorsque le dernier param est a false
                                    fVignette = resize(fVignette.getAbsolutePath(), extension, largeur, hauteur, false);
                                    FileUtil.copierFichier(fVignette, new File(repertoire + File.separator + "v_" + media.getUrl()));
                                } else {
                                    FileUtil.copierFichier(fVignette, new File(repertoire + File.separator + "v_" + media.getUrl()));
                                }
                            }
                            media.setUrlVignette("v_" + media.getUrl());
                        } catch (Exception e) {
                            ecrireLog("ERREUR lors de la creation de vignette de la photo " + media.getUrl() + " Exception :" + e, LOG_ERROR);
                        }
                        serviceMedia.add(media);
                        nbModifs++;
                    }
                } catch (Exception e) {
                    ecrireLog("Erreur sur la photo " + photo.getPathAbsolu() + " : " + e.getMessage(), LOG_ERROR);
                    nbErreur++;
                }
            }
        }
        ecrireLog(nbModifs + " photo(s) migrée(s) ", LOG_INFO);
        ecrireLog(nbMigres + " photo(s) déja migrée(s).", LOG_INFO);
        ecrireLog(nbErreur + " erreur(s).", LOG_INFO);
    }

    private static void migrerReferenceAuMedia() throws Exception {
        final List<MetatagBean> metas = MetatagUtils.getMetatagService().getByPhotoReferences();
        for (MetatagBean currentMeta : metas) {
            String listeReference = currentMeta.getMetaListeReferences();
            String[] listeIdMedia = StringUtils.substringsBetween(listeReference, "[photo;", "]");
            for (String idMedia : listeIdMedia) {
                migrerRessource(currentMeta, idMedia);
            }
        }
    }

    private static void migrerRessource(MetatagBean metaDonnees, String idMedia) throws Exception {
        if (isRessourceAMigrer(idMedia)) {
            String codeParent = formaterCodeParent(metaDonnees);
            final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
            final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
            final List<RessourceBean> ressources = serviceRessource.getByMediaId(new Long(idMedia));
            final Iterator<RessourceBean> itRessources = ressources.iterator();
            boolean dejaImportee = Boolean.FALSE;
            while (itRessources.hasNext() && !dejaImportee) {
                final RessourceBean ressource = itRessources.next();
                dejaImportee = codeParent.equals(ressource.getCodeParent());
            }
            final MediaBean media = serviceMedia.getById(new Long(idMedia));
            if (!dejaImportee && media != null) {
                final RessourceBean ressourceBean = new RessourceBean();
                ressourceBean.setIdMedia(Long.valueOf(idMedia));
                ressourceBean.setCodeParent(codeParent);
                ressourceBean.setOrdre(ressources.size());
                ressourceBean.setEtat("1");
                serviceRessource.add(ressourceBean);
                ecrireLog("reference du media " + idMedia + " à la fiche " + codeParent + " importée", LOG_INFO);
            } else {
                ecrireLog("reference du media " + idMedia + " à la fiche " + codeParent + " déjà importée", LOG_INFO);
            }
        }
    }

    private static String formaterCodeParent(MetatagBean metaDonnees) {
        return metaDonnees.getMetaIdFiche() + ",TYPE=IMG_" + metaDonnees.getMetaCodeObjet() + ",CODE=" + metaDonnees.getMetaCode();
    }

    /**
     * L"idMedia commence par F si c'est un fichiergw. Les ressources sont déjà gérés.
     */
    private static boolean isRessourceAMigrer(String idMedia) {
        return !idMedia.startsWith("F");
    }

    public static File resize(String path, String format, int newLargeur, int newHauteur, boolean replace) throws Exception {
        File fOld = new File(path);
        File fNew = new File(fOld.getParent(), "tmp_" + System.currentTimeMillis());
        Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName(format);
        ImageWriter writer = writers.next();
        BufferedImage image = ImageIO.read(fOld);
        BufferedImage bImage = PhotoUtil.scale(image, newLargeur, newHauteur, format);
        if (!image.equals(bImage)) {
            ImageOutputStream ios = ImageIO.createImageOutputStream(fNew);
            writer.setOutput(ios);
            writer.write(bImage);
            ios.close();
            writer.dispose();
        } else {
            fNew = fOld;
        }
        if (replace) {
            FileUtil.copierFichier(fNew, fOld, true);
            return fOld;
        }
        return fNew;
    }

    /**
     * Migrer photos albums.
     *
     * @throws Exception the exception
     */
    public static void migrerFichiergw() throws Exception {
        ecrireLog("*** Migration des fichiergw ***", LOG_INFO);
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
        //out.flush();
        int nbModifs = 0;
        int nbMigres = 0;
        int nbErreur = 0;
        Fichiergw f = new Fichiergw();
        f.init();
        try (ContexteDao ctx = new ContexteDao()) {
            f.setCtx(ctx);
            // on recupere ts les fichiergw sauf les vignettes
            if (f.select("WHERE ETAT = '1' AND TYPE_FICHIER != 'vignette'") > 0) {
                while (f.nextItem()) {
                    try {
                        Long idMedia;
                        MediaBean media = new MediaBean();
                        ecrireLog("Migration fichier " + f.getPathAbsolu() + ";IdFichiergw=" + f.getIdFichiergw() + "code=" + f.getCode(), LOG_INFO);
                        // pas de media associe
                        if (f.getIdPhoto().equals("")) {
                            try {
                                idMedia = new Long(f.getCode());
                                media.setId(idMedia);
                            } catch (Exception e) {
                                // le fichier ne possede pas de code
                            }
                            media.setTitre(f.getLibelleFichierJoint());
                            media.setLegende("");
                            media.setDescription(f.getCommentaireVersion());
                            media.setFormat(f.getFormatFichierJoint());
                            media.setUrl(f.getPathFichierJoint());
                            media.setSource(f.getNomFichierJoint());
                            media.setIsMutualise("2");
                            media.setCodeRedacteur(f.getAuteurVersion());
                            media.setDateCreation(f.getDateVersion());
                            try {
                                media.setPoids(Long.parseLong(f.getPoidsFichierJoint()));
                            } catch (Exception e) {
                                logger.error(String.format("Une erreur est survenue lors du calcul du poids pour le média %s",f.getLibelleFichierJoint()), e);
                            }
                            if (f.getTypeFichier().equals("fichier") || f.getTypeFichier().equals("document") || f.getTypeFichier().equals("lien")) {
                                media.setTypeRessource("fichier");
                            } else if (f.getTypeFichier().equals("photo") || f.getTypeFichier().equals("image") || f.getTypeFichier().equals("img") || f.getTypeFichier().equals("logo") || f.getTypeFichier().equals("galerie")) {
                                media.setTypeRessource("photo");
                            }
                            // gestion de la vignette
                            if (f.getIdVignette().length() > 0) {
                                Fichiergw vignette = new Fichiergw();
                                vignette.init();
                                try (ContexteDao contexteDao = new ContexteDao()) {
                                    vignette.setCtx(contexteDao);
                                    vignette.setIdFichiergw(new Long(f.getIdVignette()));
                                    vignette.retrieve();
                                    media.setUrlVignette(vignette.getPathFichierJoint());
                                    // deplacement du fichier
                                    File file = new File(vignette.getPathAbsolu());
                                    //La vignette doit se trouver sous fichiergw
                                    if (file.exists()) {
                                        File file2 = new File(MediaUtils.getPathVignetteAbsolu(media));
                                        if (!file2.exists()) {
                                            FileUtil.copierFichier(file, file2);
                                        }
                                    } else {
                                        //Si ce n'est pas le cas on regarde dans photo
                                        File filePhoto = new File(PropertyHelper.getCoreProperty("phototheque.path") + File.separator + vignette.getPathFichierJoint());
                                        if (filePhoto.exists()) {
                                            File file2 = new File(MediaUtils.getPathVignetteAbsolu(media));
                                            if (!file2.exists()) {
                                                FileUtil.copierFichier(filePhoto, file2);
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    logger.error(String.format("Une erreur est survenue lors de la génération de la vignette pour le média %s", f.getLibelleFichierJoint()), e);
                                }
                            }
                            final MediaBean existingMedia = serviceMedia.getByUrl(media.getUrl());
                            if (existingMedia == null) {
                                serviceMedia.add(media);
                                idMedia = media.getId();
                            } else {
                                idMedia = existingMedia.getId();
                            }
                        } else {
                            media = serviceMedia.getById(new Long(f.getIdPhoto()));
                            idMedia = media.getId();
                        }
                        if (!idMedia.equals(new Long(0))) {
                            boolean objetParentExiste = Boolean.FALSE;
                            // creation de la ressource
                            RessourceBean ressource = new RessourceBean();
                            if (f.getCodeParent().matches("^([0-9]+)(,TYPE=)([0-9]+)$")) {
                                ressource.setCodeParent(f.getCodeParent() + ",NO=1");
                            } else {
                                ressource.setCodeParent(f.getCodeParent());
                            }
                            ressource.setEtat(f.getEtat());
                            ressource.setIdMedia(idMedia);
                            ressource.setOrdre(new Integer(f.getNumeroVersion()));
                            //On test l'existence du parent avant de creer la ressource
                            String codeParent = ressource.getCodeParent();
                            FicheUniv ficheUniv = null;
                            // en apercu on ne controle pas la fiche car elle a déja été supprimée
                            if (isAppartientAFicheUniv(ressource) && isFichierAImporter(ressource.getEtat())) {
                                String idFiche = codeParent.substring(0, codeParent.indexOf(","));
                                String typeObjet = recupererTypeFicheParent(codeParent);
                                ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(typeObjet));
                                try (ContexteDao contexteDao = new ContexteDao()) {
                                    if (ficheUniv != null) {
                                        ficheUniv.setCtx(contexteDao);
                                        ficheUniv.init();
                                        ficheUniv.setIdFiche(new Long(idFiche));
                                        ficheUniv.retrieve();
                                        //retreive genere une exception si on ne trouve pas la fiche, on ne met donc pas le booleen a true
                                        objetParentExiste = Boolean.TRUE;
                                    }
                                } catch (Exception e) {
                                    ecrireLog("Erreur lors du test de l'existence de la FicheUniv : " + idFiche + " Exception: " + e + "Class=" + ficheUniv.getClass(), LOG_ERROR);
                                }
                            } else if (isFichierEspaceCollaboratif(codeParent) && isFichierAImporter(ressource.getEtat())) {
                                EspaceCollaboratifBean espaceParent = espaceCollaboratifDAO.getByCode(getCodeEspaceCollaboratif(codeParent));
                                objetParentExiste = espaceParent != null;
                            }
                            //Si la ressource n'existe pas et que la fiche parente existe bien
                            final RessourceBean existingRessource = serviceRessource.getByMediaIdAndCodeParent(idMedia, ressource.getCodeParent());
                            if (existingRessource == null && objetParentExiste) {
                                serviceRessource.add(ressource);
                                ecrireLog("=> OK", LOG_INFO);
                                nbModifs++;
                            } else {
                                if (objetParentExiste) {
                                    ecrireLog("=> déja migré", LOG_INFO);
                                    nbMigres++;
                                }
                            }
                        } else {
                            ecrireLog("=> fichiergw code 0, pas de creation de media ", LOG_INFO);
                        }
                    } catch (Exception e) {
                        if (e instanceof SQLException) {
                            nbMigres++;
                            ecrireLog("=> déja migré", LOG_INFO);
                        } else {
                            ecrireLog("Erreur sur le fichier " + f.getPathAbsolu() + " : " + e.getMessage(), LOG_ERROR);
                            nbErreur++;
                        }
                    }
                }
            }
        }
        ecrireLog(nbModifs + " fichiergw(s) migré(s).", LOG_INFO);
        ecrireLog(nbMigres + " fichiergw(s) déja migre(s).", LOG_INFO);
        ecrireLog(nbErreur + " erreur(s).", LOG_INFO);
    }

    private static String getCodeEspaceCollaboratif(String codeParent) {
        String codeEspace = null;
        String[] codeParentSpliter = codeParent.split(",");
        if (codeParentSpliter.length > 0) {
            codeEspace = codeParentSpliter[0];
        }
        return codeEspace;
    }

    private static Boolean isAppartientAFicheUniv(RessourceBean ressource) {
        String codeParent = ressource.getCodeParent();
        return StringUtils.isNotEmpty(codeParent) && !codeParent.startsWith("TYPE") && !isFichierEspaceCollaboratif(codeParent);
    }

    private static boolean isFichierAImporter(String etat) {
        return !"0".equals(etat) && !"2".equals(etat);
    }

    private static boolean isFichierEspaceCollaboratif(String codeParent) {
        return codeParent.contains("LOGO_ESPACE");
    }

    private static String recupererTypeFicheParent(String codeParent) {
        String typeObjet = codeParent.substring(codeParent.indexOf("TYPE=") + 5);
        if (typeObjet.length() > 4) {
            if (typeObjet.contains("_")) {
                typeObjet = typeObjet.substring(typeObjet.indexOf("_") + 1, typeObjet.indexOf("_") + 5);
            }
            if (typeObjet.contains(",")) {
                typeObjet = typeObjet.substring(0, typeObjet.indexOf(","));
            }
        }
        return typeObjet;
    }

    /**
     * Migrer photos albums.
     *
     * @param out the out
     * @param ctx the ctx
     * @throws Exception the exception
     */
    public static void migrerVignette(JspWriter out, ContextePage ctx) throws Exception {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        out.println("<br />*** Migration des vignettes ***<br />");
        out.flush();
        int nbModifs = 0;
        int nbMigres = 0;
        int nbErreur = 0;
        String newPath;
        String oldPath;
        String mediathequePath = MediathequeHelper.getAbsolutePath();
        String fichiergwPath = RessourceUtils.getAbsolutePath();
        final List<MediaBean> medias = serviceMedia.getByFilledUrlVignette();
        // on recupere les medias avec vignette
            for (MediaBean currentMedia : medias) {
                try {
                    // new path
                    newPath = mediathequePath;
                    if (StringUtils.isNotBlank(currentMedia.getTypeRessource())) {
                        newPath += "/" + currentMedia.getTypeRessource().toLowerCase();
                    }
                    newPath += "/" + currentMedia.getUrlVignette();
                    File fileCible = new File(newPath);
                    if (!fileCible.exists()) {
                        // ancien path
                        oldPath = fichiergwPath + "/" + currentMedia.getUrlVignette();
                        File fileOrig = new File(oldPath);
                        if (fileOrig.exists()) {
                            FileUtil.copierFichier(fileOrig, fileCible);
                            nbModifs++;
                        } else {
                            nbErreur++;
                            out.println("Erreur vignette inexistante : " + fileOrig + "<br />");
                            out.flush();
                        }
                    }
                } catch (Exception e) {
                    nbErreur++;
                    out.println("Erreur " + e.getMessage() + "<br />");
                    out.flush();
                }
                nbMigres++;
            }
        out.println(nbModifs + " vignette(s) déplacée(s) sur un total de " + nbMigres + "<br />");
        out.println(nbErreur + " erreur(s).<br />");
        out.flush();
    }

    private static void ecrireLog(String sTexteLog, String sMode) throws Exception {
        if (outWriter != null) {
            outWriter.println(sTexteLog + "<br />");
            outWriter.flush();
        }
        if (sMode.equals(LOG_INFO)) {
            logger.info(sTexteLog);
        } else if (sMode.equals(LOG_ERROR)) {
            logger.error(sTexteLog);
        } else if (sMode.equals(LOG_DEBUG)) {
            logger.debug(sTexteLog);
        }
    }

    public static void setOutWriter(JspWriter outWriter) {
        MigrationPhototheque.outWriter = outWriter;
    }

    public void migrerPhototheque() throws Exception {
        logger.info("Date Début migration phototheque: " + new java.util.Date(System.currentTimeMillis()));
        ecrireLog("DEBUT du traitement ...", LOG_INFO);
        // migration des photos en media
        migrerPhotos();
        // migration des fichiergw en ressource + creation d'un media
        migrerFichiergw();
        migrerReferenceAuMedia();
        ecrireLog("FIN du traitement.", LOG_INFO);
        logger.info("Date fin migration phototheque: " + new java.util.Date(System.currentTimeMillis()));
    }

    @Override
    public void run() {
        FileAppender<ILoggingEvent> appender;
        try {
            // On crée le nouveau 'logger'.
            appender = initialisationLog();
            migrerPhototheque();
            logger.detachAppender(appender);
            //appender.close();
        } catch (Exception e) {
            logger.error("Erreur lors du traitement de migration > Message :" + e.getMessage() + "StackTrace :");
        }
    }

    private FileAppender<ILoggingEvent> initialisationLog() throws Exception {
        FileAppender<ILoggingEvent> appender = null;
        String sFileLog = "";
        try {
            // On crée le nouveau 'logger'.
            String logPath = WebAppUtil.getLogsPath();
            sFileLog = logPath + File.separator + "MigrationPhototheque.log";
            appender = new FileAppender<ILoggingEvent>();
            //( new SimpleLayout(),sFileLog);
            appender.setFile(sFileLog);
            appender.setEncoder(new PatternLayoutEncoder());
            // On 'démarre' l'appender.
            appender.setAppend(false);
            //appender.activateOptions();
            logger.detachAndStopAllAppenders();
            // On ajoute l'appender file au logger.
            logger.addAppender(appender);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("Erreur lors du traitement de migration > Message :" + e.getMessage() + "StackTrace :");
        }
        return appender;
    }
}
