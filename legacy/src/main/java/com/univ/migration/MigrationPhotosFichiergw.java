/*
 * Created on 23 mars 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.migration;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.webutils.ContextePage;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ImageInfo;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * The Class MigrationPhotosFichiergw.
 *
 * @author Romain
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 */
public class MigrationPhotosFichiergw {

    /**
     * Migrer.
     *
     * @param out
     *            the out
     *
     * @throws Exception
     *             the exception
     */
    public static void migrer(final JspWriter out) throws Exception {
        out.println("DEBUT du traitement ...<br />");
        out.flush();
        final ContextePage ctx = new ContextePage("");
        // migration des fichiergw en ressource + creation d'un media
        setSpecificDataImages(out, ctx);
        out.flush();
        out.println("<br /><br />FIN du traitement.");
        ctx.release();
    }

    /**
     * Renseigne le champ SPECIFIC DATA des éléments de MEDIA dont ce champ n'a pas ete renseigne lors de la migration Il s'agit de photos dont les infos etaient stockees dans la
     * table FICHIERGW
     *
     * @param out
     *            the out
     * @param ctx
     *            the ctx
     *
     * @throws Exception
     *             the exception
     */
    public static void setSpecificDataImages(final JspWriter out, final ContextePage ctx) throws Exception {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        out.println("<br />*** Alimentation du champ SPECIFIC_DATA des images de la table MEDIA*** <br />");
        String sPath;
        int nCount = 0;
        final ClauseWhere clauseWhere = new ClauseWhere(ConditionHelper.egalVarchar("SPECIFIC_DATA", StringUtils.EMPTY));
        clauseWhere.and(ConditionHelper.in("FORMAT", new ArrayList<>(Arrays.asList(new String[] {"image/gif", "image/jpeg"}))));
        final List<MediaBean> medias = serviceMedia.getMediasFromWhere(clauseWhere);
        for (MediaBean currentMedia : medias) {
            //Recuperation du chemin de l'image
            sPath = MediaUtils.getPathAbsolu(currentMedia);
            //Recuperation des informations de l'image
            final ImageInfo ii = new ImageInfo();
            try {
                final InputStream inpStream = new FileInputStream(sPath);
                //Création du champ SPECIFIC_DATA
                ii.setInput(inpStream);
                ii.check();
                ii.close();
                String sDatas = "";
                final Properties properties = new Properties();
                properties.load(new ByteArrayInputStream(sDatas.getBytes()));
                properties.setProperty("LARGEUR", Integer.toString(ii.getWidth()));
                properties.setProperty("HAUTEUR", Integer.toString(ii.getHeight()));
                final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                properties.store(baos, "");
                sDatas = baos.toString();
                currentMedia.setSpecificData(sDatas);
                //Mise à jour de l'enregistrement en base
                serviceMedia.update(currentMedia);
                out.println("Image " + sPath + " => Mise à jour de MEDIA.SPECIFIC_DATA : sDatas <br />");
                nCount++;
            } catch (final IOException e) {
                out.println("Erreur recupération informations Fichier :" + sPath + " Exception :" + e + "<br />");
            }
        }
        out.println("Nombre d'images migrees :" + nCount + "<br />");
    }
}