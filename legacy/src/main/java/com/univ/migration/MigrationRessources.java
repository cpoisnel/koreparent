package com.univ.migration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.jsp.JspWriter;

import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.mediatheque.Mediatheque;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.FileUtil;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
// TODO: Auto-generated Javadoc

/**
 * The Class MigrationPhototheque.
 *
 * @author Romain
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 */
public class MigrationRessources implements Runnable {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(MigrationRessources.class);

    private static JspWriter OutWriter = null;

    public static void main(final String[] args) {
        final MigrationRessources capture = new MigrationRessources();
        final Thread thread = new Thread(capture);
        thread.start();
        try {
            thread.join();
        } catch (final Exception e) {
            LOGGER.error("Echec thread.join()", e);
        }
    }

    /**
     * Migrer.
     *
     * @param out
     *            the out
     *
     * @throws Exception
     *             the exception
     */
    public static void migrer(final JspWriter out) throws Exception {
        OutWriter = out;
        FileAppender<ILoggingEvent> appender;
        final MigrationRessources mig = new MigrationRessources();
        appender = mig.initialisationLog();
        mig.securiserAll();
        LOGGER.detachAppender(appender);
        //appender.close();
    }

    private static void ecrireLog(final String sTexteLog, final String sMode) throws IOException {
        if (OutWriter != null) {
            OutWriter.println(sTexteLog + "<br />");
            OutWriter.flush();
        }
        if (sMode.equals("INFO")) {
            LOGGER.info(sTexteLog);
        } else if (sMode.equals("ERROR")) {
            LOGGER.error(sTexteLog);
        } else if (sMode.equals("DEBUG")) {
            LOGGER.debug(sTexteLog);
        }
    }

    public void securiserAll() throws Exception {
        ecrireLog("Début traitement securisation ressources : " + new java.util.Date(System.currentTimeMillis()), "INFO");
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        final String pathFichierSecurise = RessourceUtils.getAbsolutePath();
        final String pathMediaPublic = MediathequeHelper.getAbsolutePath();
        int compteurPrive = 0;
        int compteurPublic = 0;
        int erreur = 0;
        final Mediatheque mediatheque = Mediatheque.getInstance();
        for (final String type : mediatheque.getTypesMedia().keySet()) {
            // on recupère le paramètre par defaut
            final String secure = PropertyHelper.getCoreProperty("mediatheque.secure." + type.toLowerCase());
            // fichier non mutualise et prive
            if ("2".equals(secure)) {
                final ClauseWhere clauseWhere = new ClauseWhere(ConditionHelper.egalVarchar("TYPE_RESSOURCE", type.toLowerCase()));
                clauseWhere.and(ConditionHelper.notIn("IS_MUTUALISE", new ArrayList<>(Arrays.asList(new String[] {"0", "2"}))));
                final List<MediaBean> medias = serviceMedia.getMediasFromWhere(clauseWhere);
                for (MediaBean currentMedia : medias) {
                    try {
                        // media local et non présent sous /WEB-INF/fichiergw/
                        if (MediaUtils.isLocal(currentMedia)) {
                            final File source = new File(MediaUtils.getPathAbsolu(currentMedia));
                            if (source.exists()) {
                                final File destination = new File(pathFichierSecurise + "/" + currentMedia.getUrl());
                                FileUtil.copierFichier(source, destination);
                                if (destination.exists()) {
                                    source.delete();
                                    currentMedia.setIsMutualise("2");
                                    serviceMedia.update(currentMedia);
                                    compteurPrive++;
                                }
                            }
                        }
                    } catch (final Exception e) {
                        erreur++;
                        ecrireLog("Erreur copie fichier " + MediaUtils.getPathAbsolu(currentMedia), "ERROR");
                    }
                }
            } else if ("1".equals(secure)) {
                final ClauseWhere clauseWhere = new ClauseWhere(ConditionHelper.egalVarchar("TYPE_RESSOURCE", type.toLowerCase()));
                clauseWhere.and(ConditionHelper.notIn("IS_MUTUALISE", new ArrayList<>(Arrays.asList(new String[] {"0", "1"}))));
                final List<MediaBean> medias = serviceMedia.getMediasFromWhere(clauseWhere);
                for (MediaBean currentMedia : medias) {
                    try {
                        // media local et non présent sous /medias/type/
                        if (MediaUtils.isLocal(currentMedia)) {
                            final File source = new File(MediaUtils.getPathAbsolu(currentMedia));
                            if (source.exists()) {
                                final File destination = new File(pathMediaPublic + "/" + currentMedia.getTypeRessource().toLowerCase() + "/" + currentMedia.getUrl());
                                FileUtil.copierFichier(source, destination);
                                if (destination.exists()) {
                                    source.delete();
                                    currentMedia.setIsMutualise("1");
                                    serviceMedia.update(currentMedia);
                                    compteurPublic++;
                                }
                            }
                        }
                    } catch (final Exception e) {
                        erreur++;
                        ecrireLog("Erreur copie fichier " + MediaUtils.getPathAbsolu(currentMedia), "ERROR");
                    }
                }
            }
        }
        // fin for
        ecrireLog("Bilan", "INFO");
        ecrireLog(compteurPrive + " ressource(s) sécurisée(s)", "INFO");
        ecrireLog(compteurPublic + " ressource(s) rendue(s) publique(s)", "INFO");
        ecrireLog(erreur + " erreur(s)", "INFO");
        ecrireLog("Fin traitement securisation ressources : " + new java.util.Date(System.currentTimeMillis()), "INFO");
    }

    protected void moveFile(final String destinationPath) {}

    @Override
    public void run() {
        FileAppender<ILoggingEvent> appender;
        // TODO Auto-generated method stub
        try {
            // On crée le nouveau 'LOGGER'.
            appender = initialisationLog();
            securiserAll();
            LOGGER.detachAppender(appender);
            //appender.close();
        } catch (final Exception e) {
            LOGGER.error("Erreur lors du traitement de migration > Message :" + e.getMessage() + "StackTrace :");
        }
    }

    private FileAppender<ILoggingEvent> initialisationLog() throws Exception {
        FileAppender<ILoggingEvent> appender = null;
        String sFileLog = "";
        try {
            // On crée le nouveau 'LOGGER'.
            final String logPath = WebAppUtil.getLogsPath();
            sFileLog = logPath + File.separator + "migrationRessource.log";
            appender = new FileAppender<>();
            // On 'démarre' l'appender.
            appender.setEncoder(new PatternLayoutEncoder());
            appender.setAppend(false);
            appender.setFile(sFileLog);
            LOGGER.detachAndStopAllAppenders();
            LOGGER.setAdditive(false);
            // On ajoute l'appender file au LOGGER.
            LOGGER.addAppender(appender);
        } catch (final Exception e) {
            // TODO Auto-generated catch block
            LOGGER.error("Erreur initialisation fichier de log migrationRessource.log");
        }
        return appender;
    }
}
