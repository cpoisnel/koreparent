/*
 * Created on 24 nov. 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.migration;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.jsp.JspWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.webutils.ContextePage;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.utils.FileUtil;

/**
 * Procédure de migration des jsp pour les méthodes obsolètes.
 *
 * @author fred
 */
public class MigrationFront51 {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationFront51.class);
    /** The write dir. */
    //private File writeDir = null;

    /** The liste methode. */
    private final ArrayList<String> listeMethode = new ArrayList<>();

    /** The read dir. */
    private File readDir = null;

    /** The props migration. */
    private Properties propsMigration = null;

    /** The out. */
    private JspWriter out = null;

    /** The bilan. */
    private StringBuffer bilan = null;

    /**
     * Migrer.
     *
     * @param _out
     *            the _out
     *
     * @throws Exception
     *             the exception
     */
    public static void migrer(final JspWriter _out) throws Exception {
        _out.println("DEBUT du traitement ...<br />");
        _out.flush();
        final ContextePage ctx = new ContextePage("");
        final String startDir = WebAppUtil.getAbsolutePath() + File.separator + "jsp";
        final MigrationFront51 migration = new MigrationFront51();
        migration.setOut(_out);
        migration.setReadDir(new File(startDir));
        Properties prop = new Properties();
        String propPath = "migration.methodes";
        try {
            final InputStream isProp = MigrationFront51.class.getResourceAsStream("/" + propPath);
            prop.load(isProp);
        } catch (final Exception e) {
            System.err.println("ERROR: fichier de migration de méthodes non trouvé");
            return;
        }
        migration.setPropsMigration(prop);
        _out.println("<br />*** Migration des jsps du front office ***<br />");
        migration.migrerMethodes();
        propPath = "migration.libelles";
        prop = new Properties();
        try {
            final InputStream isProp = MigrationFront51.class.getResourceAsStream("/" + propPath);
            prop.load(isProp);
        } catch (final Exception e) {
            System.err.println("ERROR: fichier de migration de libellés non trouvé");
            return;
        }
        migration.setPropsMigration(prop);
        _out.println("<br />*** Migration des libellés du front office ***<br />");
        final String newProperties = migration.migrerLibelles(ctx);
        if (newProperties.length() > 0) {
            _out.println("<br />Ecriture des libellés ...");
            final FileWriter fw = new FileWriter(WebAppUtil.getAbsolutePath() + File.separator + "WEB-INF" + File.separator + "classes" + File.separator + "Message_fr_FR.properties", true);
            final BufferedWriter output = new BufferedWriter(fw);
            output.write("\n");
            output.write(newProperties);
            output.flush();
            output.close();
        }
        _out.println("<br /><br />FIN du traitement.");
        ctx.release();
    }

    /**
     * Sets the read dir.
     *
     * @param readDir
     *            The readDir to set.
     */
    public void setReadDir(final File readDir) {
        this.readDir = readDir;
    }

    /**
     * Migrer methodes.
     */
    public void migrerMethodes() {
        try {
            final Enumeration<Object> en = propsMigration.keys();
            while (en.hasMoreElements()) {
                final String nomMethode = (String) en.nextElement();
                if (nomMethode.indexOf("methode.") == 0) {
                    final String elementsMethode[] = nomMethode.split("\\.", -2);
                    if (elementsMethode.length > 2 && elementsMethode[2].equals("old")) {
                        listeMethode.add(elementsMethode[1]);
                    }
                }
            }
            bilan = new StringBuffer();
            processFile(readDir);
            printlnOut("<br /><br />*** BILAN ***", true);
            printlnOut(bilan.toString(), true);
            out.flush();
        } catch (final IOException e) {
            LOG.error("impossible de flusher le flux de sortie", e);
        }
    }

    /**
     * Migrer methodes.
     *
     * @param ctx
     *            the ctx
     *
     * @return the string
     */
    public String migrerLibelles(final ContextePage ctx) {
        String res = "";
        boolean changed = false;
        final Properties prop = new Properties();
        try {
            final Enumeration<Object> en = propsMigration.keys();
            while (en.hasMoreElements()) {
                final String key = (String) en.nextElement();
                if (MessageHelper.getCoreMessage(key).length() == 0) {
                    prop.put(key, propsMigration.get(key));
                    printlnOut("<br />LIBELLE ajouté : " + key, true);
                    changed = true;
                }
            }
            if (changed) {
                final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                prop.store(baos, "Migration des libellés");
                res = baos.toString();
                baos.close();
            }
        } catch (final Exception e) {
            LOG.error("erreur d'ecriture dans l'outputStream", e);
        }
        return res;
    }

    /**
     * Println out.
     *
     * @param s
     *            the s
     * @param system
     *            the system
     */
    private void printlnOut(final String s, final boolean system) {
        try {
            if (system) {
                System.out.println(s);
            }
            out.write(s);
            out.newLine();
        } catch (final IOException e) {
            LOG.error("erreur d'ecriture", e);
        }
    }

    /**
     * Process file.
     *
     * @param fRead
     *            the f read
     */
    private void processFile(final File fRead) {
        if (fRead.isDirectory()) {
            final File[] listFiles = fRead.listFiles();
            for (File listFile : listFiles) {
                if (!listFile.isHidden()) {
                    processFile(listFile);
                }
            }
        } else if (fRead.getName().endsWith(".jsp")) {
            processJsp(fRead);
        }
    }

    /**
     * Process jsp.
     *
     * @param fRead
     *            the f read
     */
    private void processJsp(final File fRead) {
        final File fWrite = new File(fRead.getAbsolutePath() + ".tmp");
        printlnOut("<br />Traitement du fichier : " + fRead, true);
        final StringBuffer sb = new StringBuffer();
        boolean changed = false;
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(fRead));
            final BufferedWriter writer = new BufferedWriter(new FileWriter(fWrite));
            String line = null;
            String methode = "";
            int indiceLine = 1;
            while ((line = reader.readLine()) != null) // chaque ligne
            {
                for (int i = 0; i < listeMethode.size(); i++) // chaque attribut impacté
                {
                    methode = propsMigration.getProperty("methode." + listeMethode.get(i) + ".old");
                    if (line.contains(methode)) // si la ligne contient l'attribut
                    {
                        changed = true;
                        line = processAttribute(line, indiceLine, i, sb);
                    }
                }
                writer.write(line + "\r\n");
                indiceLine++;
            }
            reader.close();
            writer.flush();
            writer.close();
            if (changed) {
                FileUtil.copierFichier(fWrite, fRead);
            } else {
                fWrite.delete();
            }
            if (sb.length() > 0) {
                bilan.append("<br /><br />Fichier : ").append(fRead);
                bilan.append(sb);
            }
        } catch (final IOException e) {
            LOG.error("erreur lors du traitement des JSP", e);
        }
    }

    /**
     * Process attribute.
     *
     * @param line
     *            the line
     * @param indiceLine
     *            the indice line
     * @param indAttrib
     *            the ind attrib
     * @param stringBuffer
     *            the string buffer
     *
     * @return the string
     */
    private String processAttribute(final String line, final int indiceLine, final int indAttrib, final StringBuffer stringBuffer) {
        final StringBuilder bufferLine = new StringBuilder(line);
        int pos = 0;
        final String oldMethode = propsMigration.getProperty("methode." + listeMethode.get(indAttrib) + ".old");
        final String newMethode = propsMigration.getProperty("methode." + listeMethode.get(indAttrib) + ".new");
        final String commMethode = propsMigration.getProperty("methode." + listeMethode.get(indAttrib) + ".commentaire");
        while ((pos = bufferLine.indexOf(oldMethode, pos)) != -1) {
            // on remplace bourrinement la méthode si pas de commentaire
            if (commMethode == null) {
                bufferLine.delete(pos, pos + oldMethode.length());
                bufferLine.insert(pos, newMethode);
                pos = pos + newMethode.length();
            } else {
                pos = pos + oldMethode.length();
            }
        }
        if (commMethode != null) {
            printlnOut("--> TODO methode a remplacer : " + oldMethode + " , ligne " + indiceLine, true);
            bufferLine.insert(bufferLine.length(), "<br /><br />/* TODO MIGRATION remplacer " + oldMethode + "<br />" + "<br />" + propsMigration.getProperty("methode." + listeMethode.get(indAttrib) + ".commentaire") + "<br /><br />*/");
            stringBuffer.append("<br />--> TODO , ligne ").append(indiceLine).append(" , méthode ").append(oldMethode);
        } else {
            printlnOut("<br />&nbsp;&nbsp;&nbsp;--> INFO methode migree : " + newMethode + " , ligne " + indiceLine, true);
            stringBuffer.append("<br />--> INFO methode migree : ").append(newMethode).append(" , ligne ").append(indiceLine);
        }
        return bufferLine.toString();
    }

    /**
     * Gets the props migration.
     *
     * @return the props migration
     */
    public Properties getPropsMigration() {
        return propsMigration;
    }

    /**
     * Sets the props migration.
     *
     * @param propsMigration
     *            the new props migration
     */
    public void setPropsMigration(final Properties propsMigration) {
        this.propsMigration = propsMigration;
    }

    /**
     * Sets the out.
     *
     * @param out
     *            the new out
     */
    public void setOut(final JspWriter out) {
        this.out = out;
    }
}
