package com.univ.datagrid.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.url.UrlManager;
import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;

public class VoirFicheEnLigneServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -6232012835398251982L;

    private static final String PARAM_OBJET = "OBJET";

    private static final String PARAM_ID_FICHE = "ID_FICHE";

    private static final Logger LOG = LoggerFactory.getLogger(VoirFicheEnLigneServlet.class);

    private final transient ServiceMetatag serviceMetatag;

    public VoirFicheEnLigneServlet() {
        serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        final String idFiche = req.getParameter(PARAM_ID_FICHE);
        final String codeObjet = req.getParameter(PARAM_OBJET);
        if (StringUtils.isNotBlank(idFiche) && StringUtils.isNumeric(idFiche) && StringUtils.isNotBlank(codeObjet)) {
            try {
                final MetatagBean metaCourant = serviceMetatag.getByCodeAndIdFiche(codeObjet, Long.valueOf(idFiche));
                if (metaCourant != null && EtatFiche.EN_LIGNE.getEtat().equals(metaCourant.getMetaEtatObjet())) {
                    final String url = URLResolver.getAbsoluteUrl(UrlManager.calculerUrlFiche(metaCourant), ContexteUtil.getContexteUniv());
                    resp.sendRedirect(url);
                }
            } catch (final IOException | NumberFormatException e) {
                LOG.error("impossible de récupérer l'url de la fiche d'id " + idFiche + " et de type " + codeObjet);
                throw new ServletException(e);
            }
        }
    }
}
