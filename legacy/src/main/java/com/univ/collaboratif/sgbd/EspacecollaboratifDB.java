package com.univ.collaboratif.sgbd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.datasource.sql.utils.SqlDateConverter;

/**
 * The Class EspacecollaboratifDB.
 * @deprecated utilisez {@link com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO}
 */
@Deprecated
public class EspacecollaboratifDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The stmt. */
    private PreparedStatement stmt = null;

    /** The rs. */
    private ResultSet rs = null;

    /** The id espacecollaboratif. */
    private Long idEspacecollaboratif = null;

    /** The code. */
    private String code = null;

    /** The actif. */
    private String actif = null;

    /** The theme. */
    private String theme = null;

    /** The inscription front. */
    private String inscriptionFront = null;

    /** The intitule. */
    private String intitule = null;

    /** The code structure. */
    private String codeStructure = null;

    /** The groupes membres. */
    private String groupesMembres = null;

    /** The roles membre. */
    private String rolesMembre = null;

    /** The description. */
    private String description = null;

    /** The inscriptions en cours. */
    private String inscriptionsEnCours = null;

    /** The services. */
    private String services = null;

    /** The date creation. */
    private Date dateCreation = null;

    /** The periodicite newsletter. */
    private String periodiciteNewsletter = null;

    /** The contenu newsletter. */
    private String contenuNewsletter = null;

    /** The date dernier envoi newsletter. */
    private Date dateDernierEnvoiNewsletter = null;

    /** The date envoi newsletter. */
    private Date dateEnvoiNewsletter = null;

    /** The groupes inscription. */
    private String groupesInscription = null;

    /** The groupes consultation. */
    private String groupesConsultation = null;

    /** The code rubrique. */
    private String codeRubrique = null;

    /** The langue. */
    private String langue = null;

    /** header */
    private String enteteEspace = null;

    /** footer */
    private String piedEspace = null;

    /**
     * Instantiates a new espacecollaboratif db.
     */
    public EspacecollaboratifDB() {
    }

    /**
     * Adds the espace.
     *
     * @throws Exception the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + " ESPACECOLLABORATIF (ID_ESPACECOLLABORATIF, CODE, ACTIF, THEME, INSCRIPTION_FRONT, INTITULE, CODE_STRUCTURE, GROUPES_MEMBRES, ROLES_MEMBRE, DESCRIPTION, INSCRIPTIONS_EN_COURS, SERVICES, DATE_CREATION, PERIODICITE_NEWSLETTER, CONTENU_NEWSLETTER, DATE_DERNIER_ENVOI_NEWSLETTER, DATE_ENVOI_NEWSLETTER, GROUPES_INSCRIPTION, GROUPES_CONSULTATION, CODE_RUBRIQUE, LANGUE, ENTETE_ESPACE, PIED_ESPACE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setObject(1, getIdEspacecollaboratif(), Types.BIGINT);
            stmt.setObject(2, getCode(), Types.VARCHAR);
            stmt.setObject(3, getActif(), Types.VARCHAR);
            stmt.setObject(4, getTheme(), Types.VARCHAR);
            stmt.setObject(5, getInscriptionFront(), Types.VARCHAR);
            stmt.setObject(6, getIntitule(), Types.VARCHAR);
            stmt.setObject(7, getCodeStructure(), Types.VARCHAR);
            stmt.setObject(8, getGroupesMembres(), Types.LONGVARCHAR);
            stmt.setObject(9, getRolesMembre(), Types.VARCHAR);
            stmt.setObject(10, getDescription(), Types.LONGVARCHAR);
            stmt.setObject(11, getInscriptionsEnCours(), Types.LONGVARCHAR);
            stmt.setObject(12, getServices(), Types.LONGVARCHAR);
            stmt.setObject(13, SqlDateConverter.toDate(getDateCreation()), Types.DATE);
            stmt.setObject(14, getPeriodiciteNewsletter(), Types.VARCHAR);
            stmt.setObject(15, getContenuNewsletter(), Types.LONGVARCHAR);
            stmt.setObject(16, SqlDateConverter.toDate(getDateDernierEnvoiNewsletter()), Types.DATE);
            stmt.setObject(17, SqlDateConverter.toDate(getDateEnvoiNewsletter()), Types.DATE);
            stmt.setObject(18, getGroupesInscription(), Types.LONGVARCHAR);
            stmt.setObject(19, getGroupesConsultation(), Types.LONGVARCHAR);
            stmt.setObject(20, getCodeRubrique(), Types.VARCHAR);
            stmt.setObject(21, getLangue(), Types.VARCHAR);
            stmt.setObject(22, getEnteteEspace(), Types.LONGVARCHAR);
            stmt.setObject(23, getPiedEspace(), Types.LONGVARCHAR);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = stmt.getGeneratedKeys();
            rs.next();
            setIdEspacecollaboratif(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception the exception
     */
    public void delete() throws Exception {
        try {
            stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + " ESPACECOLLABORATIF WHERE ID_ESPACECOLLABORATIF = ? ");
            // put parameters into statement
            stmt.setObject(1, getIdEspacecollaboratif(), Types.BIGINT);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id espacecollaboratif.
     *
     * @return the id espacecollaboratif
     */
    public Long getIdEspacecollaboratif() {
        return idEspacecollaboratif;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the actif.
     *
     * @return the actif
     */
    public String getActif() {
        return actif;
    }

    /**
     * Gets the theme.
     *
     * @return the theme
     */
    public String getTheme() {
        return theme;
    }

    /**
     * Gets the inscription front.
     *
     * @return the inscription front
     */
    public String getInscriptionFront() {
        return inscriptionFront;
    }

    /**
     * Gets the intitule.
     *
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Gets the code structure.
     *
     * @return the code structure
     */
    public String getCodeStructure() {
        return codeStructure;
    }

    /**
     * Gets the groupes membres.
     *
     * @return the groupes membres
     */
    public String getGroupesMembres() {
        return groupesMembres;
    }

    /**
     * Gets the roles membre.
     *
     * @return the roles membre
     */
    public String getRolesMembre() {
        return rolesMembre;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the inscriptions en cours.
     *
     * @return the inscriptions en cours
     */
    public String getInscriptionsEnCours() {
        return inscriptionsEnCours;
    }

    /**
     * Gets the services.
     *
     * @return the services
     */
    public String getServices() {
        return services;
    }

    /**
     * Gets the date creation.
     *
     * @return the date creation
     */
    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * Gets the periodicite newsletter.
     *
     * @return the periodicite newsletter
     */
    public String getPeriodiciteNewsletter() {
        return periodiciteNewsletter;
    }

    /**
     * Gets the contenu newsletter.
     *
     * @return the contenu newsletter
     */
    public String getContenuNewsletter() {
        return contenuNewsletter;
    }

    /**
     * Gets the date dernier envoi newsletter.
     *
     * @return the date dernier envoi newsletter
     */
    public Date getDateDernierEnvoiNewsletter() {
        return dateDernierEnvoiNewsletter;
    }

    /**
     * Gets the date envoi newsletter.
     *
     * @return the date envoi newsletter
     */
    public Date getDateEnvoiNewsletter() {
        return dateEnvoiNewsletter;
    }

    /**
     * Gets the groupes inscription.
     *
     * @return the groupes inscription
     */
    public String getGroupesInscription() {
        return groupesInscription;
    }

    /**
     * Gets the groupes consultation.
     *
     * @return the groupes consultation
     */
    public String getGroupesConsultation() {
        return groupesConsultation;
    }

    /**
     * Gets the code rubrique.
     *
     * @return the code rubrique
     */
    public String getCodeRubrique() {
        return codeRubrique;
    }

    /**
     * Gets the langue.
     *
     * @return the langue
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Gets the header.
     * @return the header
     */
    public String getEnteteEspace() {
        return enteteEspace;
    }

    /**
     * Gets the footer.
     * @return the footer
     */
    public String getPiedEspace() {
        return piedEspace;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT T1.ID_ESPACECOLLABORATIF, T1.CODE, T1.ACTIF, T1.THEME, T1.INSCRIPTION_FRONT, T1.INTITULE, T1.CODE_STRUCTURE, T1.GROUPES_MEMBRES, T1.ROLES_MEMBRE, T1.DESCRIPTION, T1.INSCRIPTIONS_EN_COURS, T1.SERVICES, T1.DATE_CREATION, T1.PERIODICITE_NEWSLETTER, T1.CONTENU_NEWSLETTER, T1.DATE_DERNIER_ENVOI_NEWSLETTER, T1.DATE_ENVOI_NEWSLETTER, T1.GROUPES_INSCRIPTION, T1.GROUPES_CONSULTATION, T1.CODE_RUBRIQUE, T1.LANGUE, T1.ENTETE_ESPACE, T1.PIED_ESPACE FROM " + qualifier + "ESPACECOLLABORATIF T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                retrieveFromRS(rs);
                res = true;
            } else {
                try {
                    rs.close();
                } finally {
                    rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            stmt = getConnection().prepareStatement("SELECT ID_ESPACECOLLABORATIF, CODE, ACTIF, THEME, INSCRIPTION_FRONT, INTITULE, CODE_STRUCTURE, GROUPES_MEMBRES, ROLES_MEMBRE, DESCRIPTION, INSCRIPTIONS_EN_COURS, SERVICES, DATE_CREATION, PERIODICITE_NEWSLETTER, CONTENU_NEWSLETTER, DATE_DERNIER_ENVOI_NEWSLETTER, DATE_ENVOI_NEWSLETTER, GROUPES_INSCRIPTION, GROUPES_CONSULTATION, CODE_RUBRIQUE, LANGUE, ENTETE_ESPACE, PIED_ESPACE FROM " + qualifier + "ESPACECOLLABORATIF WHERE ID_ESPACECOLLABORATIF = ? ");
            // put parameters into statement
            stmt.setObject(1, getIdEspacecollaboratif(), Types.BIGINT);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param rs the rs
     * @throws Exception the exception
     */
    private void retrieveFromRS(final ResultSet rs) throws Exception {
        try {
            // get output from result set
            setIdEspacecollaboratif(rs.getLong(1));
            setCode(rs.getString(2));
            setActif(rs.getString(3));
            setTheme(rs.getString(4));
            setInscriptionFront(rs.getString(5));
            setIntitule(rs.getString(6));
            setCodeStructure(rs.getString(7));
            setGroupesMembres(rs.getString(8));
            setRolesMembre(rs.getString(9));
            setDescription(rs.getString(10));
            setInscriptionsEnCours(rs.getString(11));
            setServices(rs.getString(12));
            setDateCreation(SqlDateConverter.fromDate(rs.getDate(13)));
            setPeriodiciteNewsletter(rs.getString(14));
            setContenuNewsletter(rs.getString(15));
            setDateDernierEnvoiNewsletter(SqlDateConverter.fromDate(rs.getDate(16)));
            setDateEnvoiNewsletter(SqlDateConverter.fromDate(rs.getDate(17)));
            setGroupesInscription(rs.getString(18));
            setGroupesConsultation(rs.getString(19));
            setCodeRubrique(rs.getString(20));
            setLangue(rs.getString(21));
            setEnteteEspace(rs.getString(22));
            setPiedEspace(rs.getString(23));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix the sql suffix
     * @throws Exception the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            /* éxécution requete */
            stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            rs = stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix the sql suffix
     * @return the int
     * @throws Exception the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*) FROM " + qualifier + "ESPACECOLLABORATIF T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery(query);
            rs.next();
            count = rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            rs = stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            stmt = null;
        }
        return count;
    }

    /**
     * Sets the id espacecollaboratif.
     *
     * @param idEspacecollaboratif the new id espacecollaboratif
     */
    public void setIdEspacecollaboratif(final Long idEspacecollaboratif) {
        this.idEspacecollaboratif = idEspacecollaboratif;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets the actif.
     *
     * @param actif the new actif
     */
    public void setActif(final String actif) {
        this.actif = actif;
    }

    /**
     * Sets the theme.
     *
     * @param theme the new theme
     */
    public void setTheme(final String theme) {
        this.theme = theme;
    }

    /**
     * Sets the inscription front.
     *
     * @param inscriptionFront the new inscription front
     */
    public void setInscriptionFront(final String inscriptionFront) {
        this.inscriptionFront = inscriptionFront;
    }

    /**
     * Sets the intitule.
     *
     * @param intitule the new intitule
     */
    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    /**
     * Sets the code structure.
     *
     * @param codeStructure the new code structure
     */
    public void setCodeStructure(final String codeStructure) {
        this.codeStructure = codeStructure;
    }

    /**
     * Sets the groupes membres.
     *
     * @param groupesMembres the new groupes membres
     */
    public void setGroupesMembres(final String groupesMembres) {
        this.groupesMembres = groupesMembres;
    }

    /**
     * Sets the roles membre.
     *
     * @param rolesMembre the new roles membre
     */
    public void setRolesMembre(final String rolesMembre) {
        this.rolesMembre = rolesMembre;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets the inscriptions en cours.
     *
     * @param inscriptionsEnCours the new inscriptions en cours
     */
    public void setInscriptionsEnCours(final String inscriptionsEnCours) {
        this.inscriptionsEnCours = inscriptionsEnCours;
    }

    /**
     * Sets the services.
     *
     * @param services the new services
     */
    public void setServices(final String services) {
        this.services = services;
    }

    /**
     * Sets the date creation.
     *
     * @param dateCreation the new date creation
     */
    public void setDateCreation(final Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * Sets the periodicite newsletter.
     *
     * @param periodiciteNewsletter the new periodicite newsletter
     */
    public void setPeriodiciteNewsletter(final String periodiciteNewsletter) {
        this.periodiciteNewsletter = periodiciteNewsletter;
    }

    /**
     * Sets the contenu newsletter.
     *
     * @param contenuNewsletter the new contenu newsletter
     */
    public void setContenuNewsletter(final String contenuNewsletter) {
        this.contenuNewsletter = contenuNewsletter;
    }

    /**
     * Sets the date dernier envoi newsletter.
     *
     * @param dateDernierEnvoiNewsletter the new date dernier envoi newsletter
     */
    public void setDateDernierEnvoiNewsletter(final Date dateDernierEnvoiNewsletter) {
        this.dateDernierEnvoiNewsletter = dateDernierEnvoiNewsletter;
    }

    /**
     * Sets the date envoi newsletter.
     *
     * @param dateEnvoiNewsletter the new date envoi newsletter
     */
    public void setDateEnvoiNewsletter(final Date dateEnvoiNewsletter) {
        this.dateEnvoiNewsletter = dateEnvoiNewsletter;
    }

    /**
     * Sets the groupes inscription.
     *
     * @param groupesInscription the new groupes inscription
     */
    public void setGroupesInscription(final String groupesInscription) {
        this.groupesInscription = groupesInscription;
    }

    /**
     * Sets the groupes consultation.
     *
     * @param groupesConsultation the new groupes consultation
     */
    public void setGroupesConsultation(final String groupesConsultation) {
        this.groupesConsultation = groupesConsultation;
    }

    /**
     * Sets the code rubrique.
     *
     * @param codeRubrique the new code rubrique
     */
    public void setCodeRubrique(final String codeRubrique) {
        this.codeRubrique = codeRubrique;
    }

    /**
     * Sets the langue.
     *
     * @param langue the new langue
     */
    public void setLangue(final String langue) {
        this.langue = langue;
    }

    /**
     * Sets the header.
     *
     * @param enteteEspace the new header
     */
    public void setEnteteEspace(final String enteteEspace) {
        this.enteteEspace = enteteEspace;
    }

    /**
     * Sets the footer.
     *
     * @param piedEspace the new footer
     */
    public void setPiedEspace(final String piedEspace) {
        this.piedEspace = piedEspace;
    }

    /**
     * Sets the param.
     *
     * @param ctx the ctx
     * @param qualifier the qualifier
     */
    public void setParam(final OMContext ctx, final String qualifier) {
        this.ctx = ctx;
        this.qualifier = "";
        if (!"".equals(qualifier)) {
            if (!".".equals(qualifier.substring(qualifier.length() - 1))) {
                this.qualifier = qualifier + ".";
            } else {
                this.qualifier = qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }

    /**
     * To string.
     *
     * @param aSeparator the a separator
     * @return the string
     */
    public String toString(final String aSeparator) {
        return String.valueOf("" + aSeparator + getIdEspacecollaboratif() + aSeparator + getCode() + aSeparator + getActif() + aSeparator + getTheme() + aSeparator + getInscriptionFront() + aSeparator + getIntitule() + aSeparator + getCodeStructure() + aSeparator + getGroupesMembres() + aSeparator + getRolesMembre() + aSeparator + getDescription() + aSeparator + getInscriptionsEnCours() + aSeparator + getServices() + aSeparator + getDateCreation() + aSeparator + getPeriodiciteNewsletter() + aSeparator + getContenuNewsletter() + aSeparator + getDateDernierEnvoiNewsletter() + aSeparator + getDateEnvoiNewsletter() + aSeparator + getGroupesInscription() + aSeparator + getGroupesConsultation() + aSeparator + getCodeRubrique() + aSeparator + getLangue() + aSeparator + getEnteteEspace() + aSeparator + getPiedEspace());
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception the exception
     */
    public void update() throws Exception {
        try {
            stmt = getConnection().prepareStatement("UPDATE " + qualifier + "ESPACECOLLABORATIF SET ID_ESPACECOLLABORATIF = ?, CODE = ?, ACTIF = ?, THEME = ?, INSCRIPTION_FRONT = ?, INTITULE = ?, CODE_STRUCTURE = ?, GROUPES_MEMBRES = ?, ROLES_MEMBRE = ?, DESCRIPTION = ?, INSCRIPTIONS_EN_COURS = ?, SERVICES = ?, DATE_CREATION = ?, PERIODICITE_NEWSLETTER = ?, CONTENU_NEWSLETTER = ?, DATE_DERNIER_ENVOI_NEWSLETTER = ?, DATE_ENVOI_NEWSLETTER = ?, GROUPES_INSCRIPTION = ?, GROUPES_CONSULTATION = ?, CODE_RUBRIQUE = ?, LANGUE = ?, ENTETE_ESPACE = ?, PIED_ESPACE = ? WHERE ID_ESPACECOLLABORATIF = ? ");
            // put parameters into statement
            stmt.setObject(1, getIdEspacecollaboratif(), Types.BIGINT);
            stmt.setObject(2, getCode(), Types.VARCHAR);
            stmt.setObject(3, getActif(), Types.VARCHAR);
            stmt.setObject(4, getTheme(), Types.VARCHAR);
            stmt.setObject(5, getInscriptionFront(), Types.VARCHAR);
            stmt.setObject(6, getIntitule(), Types.VARCHAR);
            stmt.setObject(7, getCodeStructure(), Types.VARCHAR);
            stmt.setObject(8, getGroupesMembres(), Types.LONGVARCHAR);
            stmt.setObject(9, getRolesMembre(), Types.VARCHAR);
            stmt.setObject(10, getDescription(), Types.LONGVARCHAR);
            stmt.setObject(11, getInscriptionsEnCours(), Types.LONGVARCHAR);
            stmt.setObject(12, getServices(), Types.LONGVARCHAR);
            stmt.setObject(13, SqlDateConverter.toDate(getDateCreation()), Types.DATE);
            stmt.setObject(14, getPeriodiciteNewsletter(), Types.VARCHAR);
            stmt.setObject(15, getContenuNewsletter(), Types.LONGVARCHAR);
            stmt.setObject(16, SqlDateConverter.toDate(getDateDernierEnvoiNewsletter()), Types.DATE);
            stmt.setObject(17, SqlDateConverter.toDate(getDateEnvoiNewsletter()), Types.DATE);
            stmt.setObject(18, getGroupesInscription(), Types.LONGVARCHAR);
            stmt.setObject(19, getGroupesConsultation(), Types.LONGVARCHAR);
            stmt.setObject(20, getCodeRubrique(), Types.VARCHAR);
            stmt.setObject(21, getLangue(), Types.VARCHAR);
            stmt.setObject(22, getEnteteEspace(), Types.LONGVARCHAR);
            stmt.setObject(23, getPiedEspace(), Types.LONGVARCHAR);
            stmt.setObject(24, getIdEspacecollaboratif(), Types.BIGINT);
            stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param ctx the new ctx
     */
    public void setCtx(final OMContext ctx) {
        setParam(ctx, "");
    }
}
