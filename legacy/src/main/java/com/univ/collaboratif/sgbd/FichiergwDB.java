package com.univ.collaboratif.sgbd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.datasource.sql.utils.SqlDateConverter;

/**
 * The Class FichiergwDB.
 * @deprecated utilisez {@link com.univ.objetspartages.dao.impl.RessourceDAO} ou mieux, le service {@link com.univ.objetspartages.services.ServiceRessource}
 */
@Deprecated
public class FichiergwDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet rs = null;

    /** The id fichiergw. */
    private Long idFichiergw = null;

    /** The code. */
    private String code = null;

    /** The code parent. */
    private String codeParent = null;

    /** The type fichier. */
    private String typeFichier = null;

    /** The id photo. */
    private String idPhoto = null;

    /** The id vignette. */
    private String idVignette = null;

    /** The path fichier joint. */
    private String pathFichierJoint = null;

    /** The format fichier joint. */
    private String formatFichierJoint = null;

    /** The poids fichier joint. */
    private String poidsFichierJoint = null;

    /** The nom fichier joint. */
    private String nomFichierJoint = null;

    /** The libelle fichier joint. */
    private String libelleFichierJoint = null;

    /** The numero version. */
    private String numeroVersion = null;

    /** The date version. */
    private Date dateVersion = null;

    /** The commentaire version. */
    private String commentaireVersion = null;

    /** The auteur version. */
    private String auteurVersion = null;

    /** The etat. */
    private String etat = null;

    /**
     * Instantiates a new fichiergw db.
     */
    public FichiergwDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "FICHIERGW (ID_FICHIERGW  ,   CODE  ,   CODE_PARENT  ,   TYPE_FICHIER  ,   ID_PHOTO  ,   ID_VIGNETTE  ,   PATH_FICHIER_JOINT  ,   FORMAT_FICHIER_JOINT  ,   POIDS_FICHIER_JOINT  ,   NOM_FICHIER_JOINT  ,   LIBELLE_FICHIER_JOINT  ,   NUMERO_VERSION  ,   DATE_VERSION  ,   COMMENTAIRE_VERSION  ,   AUTEUR_VERSION  ,   ETAT  )              VALUES (?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdFichiergw(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getCodeParent(), Types.VARCHAR);
            _stmt.setObject(4, getTypeFichier(), Types.VARCHAR);
            _stmt.setObject(5, getIdPhoto(), Types.VARCHAR);
            _stmt.setObject(6, getIdVignette(), Types.VARCHAR);
            _stmt.setObject(7, getPathFichierJoint(), Types.VARCHAR);
            _stmt.setObject(8, getFormatFichierJoint(), Types.VARCHAR);
            _stmt.setObject(9, getPoidsFichierJoint(), Types.VARCHAR);
            _stmt.setObject(10, getNomFichierJoint(), Types.VARCHAR);
            _stmt.setObject(11, getLibelleFichierJoint(), Types.VARCHAR);
            _stmt.setObject(12, getNumeroVersion(), Types.VARCHAR);
            _stmt.setObject(13, SqlDateConverter.toDate(getDateVersion()), Types.DATE);
            _stmt.setObject(14, getCommentaireVersion(), Types.LONGVARCHAR);
            _stmt.setObject(15, getAuteurVersion(), Types.VARCHAR);
            _stmt.setObject(16, getEtat(), Types.VARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdFichiergw(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "FICHIERGW WHERE " + "             ID_FICHIERGW = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdFichiergw(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id fichiergw.
     *
     * @return the id fichiergw
     */
    public Long getIdFichiergw() {
        return idFichiergw;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the code parent.
     *
     * @return the code parent
     */
    public String getCodeParent() {
        return codeParent;
    }

    /**
     * Gets the type fichier.
     *
     * @return the type fichier
     */
    public String getTypeFichier() {
        return typeFichier;
    }

    /**
     * Gets the id photo.
     *
     * @return the id photo
     */
    public String getIdPhoto() {
        return idPhoto;
    }

    /**
     * Gets the id vignette.
     *
     * @return the id vignette
     */
    public String getIdVignette() {
        return idVignette;
    }

    /**
     * Gets the path fichier joint.
     *
     * @return the path fichier joint
     */
    public String getPathFichierJoint() {
        return pathFichierJoint;
    }

    /**
     * Gets the format fichier joint.
     *
     * @return the format fichier joint
     */
    public String getFormatFichierJoint() {
        return formatFichierJoint;
    }

    /**
     * Gets the poids fichier joint.
     *
     * @return the poids fichier joint
     */
    public String getPoidsFichierJoint() {
        return poidsFichierJoint;
    }

    /**
     * Gets the nom fichier joint.
     *
     * @return the nom fichier joint
     */
    public String getNomFichierJoint() {
        return nomFichierJoint;
    }

    /**
     * Gets the libelle fichier joint.
     *
     * @return the libelle fichier joint
     */
    public String getLibelleFichierJoint() {
        return libelleFichierJoint;
    }

    /**
     * Gets the numero version.
     *
     * @return the numero version
     */
    public String getNumeroVersion() {
        return numeroVersion;
    }

    /**
     * Gets the date version.
     *
     * @return the date version
     */
    public Date getDateVersion() {
        return dateVersion;
    }

    /**
     * Gets the commentaire version.
     *
     * @return the commentaire version
     */
    public String getCommentaireVersion() {
        return commentaireVersion;
    }

    /**
     * Gets the auteur version.
     *
     * @return the auteur version
     */
    public String getAuteurVersion() {
        return auteurVersion;
    }

    /**
     * Gets the etat.
     *
     * @return the etat
     */
    public String getEtat() {
        return etat;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT " + "T1.ID_FICHIERGW ,  " + "T1.CODE ,  " + "T1.CODE_PARENT ,  " + "T1.TYPE_FICHIER ,  " + "T1.ID_PHOTO ,  " + "T1.ID_VIGNETTE ,  " + "T1.PATH_FICHIER_JOINT ,  " + "T1.FORMAT_FICHIER_JOINT ,  " + "T1.POIDS_FICHIER_JOINT ,  " + "T1.NOM_FICHIER_JOINT ,  " + "T1.LIBELLE_FICHIER_JOINT ,  " + "T1.NUMERO_VERSION ,  " + "T1.DATE_VERSION ,  " + "T1.COMMENTAIRE_VERSION ,  " + "T1.AUTEUR_VERSION ,  " + "T1.ETAT " + "         FROM " + qualifier + "FICHIERGW T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                retrieveFromRS(rs);
                res = true;
            } else {
                try {
                    rs.close();
                } finally {
                    rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_FICHIERGW," + "             CODE," + "             CODE_PARENT," + "             TYPE_FICHIER," + "             ID_PHOTO," + "             ID_VIGNETTE," + "             PATH_FICHIER_JOINT," + "             FORMAT_FICHIER_JOINT," + "             POIDS_FICHIER_JOINT," + "             NOM_FICHIER_JOINT," + "             LIBELLE_FICHIER_JOINT," + "             NUMERO_VERSION," + "             DATE_VERSION," + "             COMMENTAIRE_VERSION," + "             AUTEUR_VERSION," + "             ETAT" + "         FROM " + qualifier + "FICHIERGW WHERE " + "             ID_FICHIERGW = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdFichiergw(), Types.BIGINT);
            final ResultSet rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdFichiergw(new Long(_rs.getLong(1)));
            setCode(_rs.getString(2));
            setCodeParent(_rs.getString(3));
            setTypeFichier(_rs.getString(4));
            setIdPhoto(_rs.getString(5));
            setIdVignette(_rs.getString(6));
            setPathFichierJoint(_rs.getString(7));
            setFormatFichierJoint(_rs.getString(8));
            setPoidsFichierJoint(_rs.getString(9));
            setNomFichierJoint(_rs.getString(10));
            setLibelleFichierJoint(_rs.getString(11));
            setNumeroVersion(_rs.getString(12));
            setDateVersion(SqlDateConverter.fromDate(_rs.getDate(13)));
            setCommentaireVersion(_rs.getString(14));
            setAuteurVersion(_rs.getString(15));
            setEtat(_rs.getString(16));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "FICHIERGW T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            rs = _stmt.executeQuery(query);
            rs.next();
            count = rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id fichiergw.
     *
     * @param _idFichiergw
     *            the new id fichiergw
     */
    public void setIdFichiergw(final Long _idFichiergw) {
        idFichiergw = _idFichiergw;
    }

    /**
     * Sets the code.
     *
     * @param _code
     *            the new code
     */
    public void setCode(final String _code) {
        code = _code;
    }

    /**
     * Sets the code parent.
     *
     * @param _codeParent
     *            the new code parent
     */
    public void setCodeParent(final String _codeParent) {
        codeParent = _codeParent;
    }

    /**
     * Sets the type fichier.
     *
     * @param _typeFichier
     *            the new type fichier
     */
    public void setTypeFichier(final String _typeFichier) {
        typeFichier = _typeFichier;
    }

    /**
     * Sets the id photo.
     *
     * @param _idPhoto
     *            the new id photo
     */
    public void setIdPhoto(final String _idPhoto) {
        idPhoto = _idPhoto;
    }

    /**
     * Sets the id vignette.
     *
     * @param _idVignette
     *            the new id vignette
     */
    public void setIdVignette(final String _idVignette) {
        idVignette = _idVignette;
    }

    /**
     * Sets the path fichier joint.
     *
     * @param _pathFichierJoint
     *            the new path fichier joint
     */
    public void setPathFichierJoint(final String _pathFichierJoint) {
        pathFichierJoint = _pathFichierJoint;
    }

    /**
     * Sets the format fichier joint.
     *
     * @param _formatFichierJoint
     *            the new format fichier joint
     */
    public void setFormatFichierJoint(final String _formatFichierJoint) {
        formatFichierJoint = _formatFichierJoint;
    }

    /**
     * Sets the poids fichier joint.
     *
     * @param _poidsFichierJoint
     *            the new poids fichier joint
     */
    public void setPoidsFichierJoint(final String _poidsFichierJoint) {
        poidsFichierJoint = _poidsFichierJoint;
    }

    /**
     * Sets the nom fichier joint.
     *
     * @param _nomFichierJoint
     *            the new nom fichier joint
     */
    public void setNomFichierJoint(final String _nomFichierJoint) {
        nomFichierJoint = _nomFichierJoint;
    }

    /**
     * Sets the libelle fichier joint.
     *
     * @param _libelleFichierJoint
     *            the new libelle fichier joint
     */
    public void setLibelleFichierJoint(final String _libelleFichierJoint) {
        libelleFichierJoint = _libelleFichierJoint;
    }

    /**
     * Sets the numero version.
     *
     * @param _numeroVersion
     *            the new numero version
     */
    public void setNumeroVersion(final String _numeroVersion) {
        numeroVersion = _numeroVersion;
    }

    /**
     * Sets the date version.
     *
     * @param _dateVersion
     *            the new date version
     */
    public void setDateVersion(final Date _dateVersion) {
        dateVersion = _dateVersion;
    }

    /**
     * Sets the commentaire version.
     *
     * @param _commentaireVersion
     *            the new commentaire version
     */
    public void setCommentaireVersion(final String _commentaireVersion) {
        commentaireVersion = _commentaireVersion;
    }

    /**
     * Sets the auteur version.
     *
     * @param _auteurVersion
     *            the new auteur version
     */
    public void setAuteurVersion(final String _auteurVersion) {
        auteurVersion = _auteurVersion;
    }

    /**
     * Sets the etat.
     *
     * @param _etat
     *            the new etat
     */
    public void setEtat(final String _etat) {
        etat = _etat;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdFichiergw() + aSeparator + getCode() + aSeparator + getCodeParent() + aSeparator + getTypeFichier() + aSeparator + getIdPhoto() + aSeparator + getIdVignette() + aSeparator + getPathFichierJoint() + aSeparator + getFormatFichierJoint() + aSeparator + getPoidsFichierJoint() + aSeparator + getNomFichierJoint() + aSeparator + getLibelleFichierJoint() + aSeparator + getNumeroVersion() + aSeparator + getDateVersion() + aSeparator + getCommentaireVersion() + aSeparator + getAuteurVersion() + aSeparator + getEtat()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "FICHIERGW SET " + "ID_FICHIERGW = ?, " + "CODE = ?, " + "CODE_PARENT = ?, " + "TYPE_FICHIER = ?, " + "ID_PHOTO = ?, " + "ID_VIGNETTE = ?, " + "PATH_FICHIER_JOINT = ?, " + "FORMAT_FICHIER_JOINT = ?, " + "POIDS_FICHIER_JOINT = ?, " + "NOM_FICHIER_JOINT = ?, " + "LIBELLE_FICHIER_JOINT = ?, " + "NUMERO_VERSION = ?, " + "DATE_VERSION = ?, " + "COMMENTAIRE_VERSION = ?, " + "AUTEUR_VERSION = ?, " + "ETAT = ? " + "         WHERE " + "             ID_FICHIERGW = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdFichiergw(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getCodeParent(), Types.VARCHAR);
            _stmt.setObject(4, getTypeFichier(), Types.VARCHAR);
            _stmt.setObject(5, getIdPhoto(), Types.VARCHAR);
            _stmt.setObject(6, getIdVignette(), Types.VARCHAR);
            _stmt.setObject(7, getPathFichierJoint(), Types.VARCHAR);
            _stmt.setObject(8, getFormatFichierJoint(), Types.VARCHAR);
            _stmt.setObject(9, getPoidsFichierJoint(), Types.VARCHAR);
            _stmt.setObject(10, getNomFichierJoint(), Types.VARCHAR);
            _stmt.setObject(11, getLibelleFichierJoint(), Types.VARCHAR);
            _stmt.setObject(12, getNumeroVersion(), Types.VARCHAR);
            _stmt.setObject(13, SqlDateConverter.toDate(getDateVersion()), Types.DATE);
            _stmt.setObject(14, getCommentaireVersion(), Types.LONGVARCHAR);
            _stmt.setObject(15, getAuteurVersion(), Types.VARCHAR);
            _stmt.setObject(16, getEtat(), Types.VARCHAR);
            _stmt.setObject(17, getIdFichiergw(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
