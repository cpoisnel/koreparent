package com.univ.collaboratif.om;

import java.io.File;

import com.univ.collaboratif.sgbd.FichiergwDB;
import com.univ.objetspartages.util.RessourceUtils;

/**
 * The Class Fichiergw.
 *
 * @author romain
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 * @deprecated cette classe est remplacé par {@link com.univ.objetspartages.bean.RessourceBean} pour les données et {@link com.univ.objetspartages.services.ServiceRessource}
 * pour l'accees  en bdd etc.
 */
@Deprecated
public class Fichiergw extends FichiergwDB implements Cloneable {

    /** The PAT h_ servle t_ lectur e_ fichier. */
    public static String PATH_SERVLET_LECTURE_FICHIER = "/servlet/com.univ.collaboratif.utils.LectureFichiergw";

    /** The ETA t_ sauvegard e_ structur e_ courante. */
    public static String ETAT_SAUVEGARDE_STRUCTURE_COURANTE = "destruction_sauvegarde_courante";

    /**
     * Instantiates a new fichiergw.
     */
    public Fichiergw() {
        super();
    }

    /**
     * Initialisation de l'objet.
     */
    public void init() {
        setIdFichiergw((long) 0);
        setCodeParent("");
        setCode("");
        setTypeFichier("");
        setIdPhoto("");
        setIdVignette("");
        setPathFichierJoint("");
        setFormatFichierJoint("");
        setPoidsFichierJoint("");
        setNomFichierJoint("");
        setLibelleFichierJoint("");
        setNumeroVersion("");
        setCommentaireVersion("");
        setAuteurVersion("");
        setEtat("");
    }

    /**
     * Gets the path absolu.
     *
     * @return le path physique absolu du fichier
     */
    public String getPathAbsolu() {
        return RessourceUtils.getAbsolutePath() + File.separatorChar + getPathFichierJoint();
    }
}
