package com.univ.mediatheque;

import org.springframework.stereotype.Component;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.cache.AbstractCacheManager;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.services.ServiceMedia;

/**
 * @deprecated Le service {@link ServiceMedia} est désormais en charge du cache.
 */
@Deprecated
@Component
public class CacheMediaManager extends AbstractCacheManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "cacheMediaManager";

    public static final String KEY_CACHE = "CacheMediaManager.cacheRubriqueBySource";

    private ServiceMedia serviceMedia;

    public void setServiceMedia(final ServiceMedia serviceMedia) {
        this.serviceMedia = serviceMedia;
    }

    /**
     * @deprecated Utilisez le service {@link ServiceMedia} en le récupérant à travers le {@link com.kosmos.service.impl.ServiceManager}
     */
    @Deprecated
    public static CacheMediaManager getInstance() {
        return (CacheMediaManager) ApplicationContextManager.getCoreContextBean(ID_BEAN);
    }

    /**
     * @deprecated Utilisez {@link ServiceMedia#getCodeRubrique(String)}
     */
    @Deprecated
    public String getCodeRubrique(String sUrl) {
        return serviceMedia.getCodeRubrique(sUrl);
    }

    /**
     * @deprecated Ne plus utiliser cette méthode. Le refresh est géré par le {@link ServiceMedia}.
     */
    @Deprecated
    public void refresh(MediaBean media, boolean async) {
        // Does nothing
    }

    @Override
    public Object getObjectToCache() throws Exception {
        return null;
    }

    @Override
    public String getCacheName() {
        return KEY_CACHE;
    }

    @Override
    public Object getObjectKey() {
        return KEY_CACHE;
    }
}
