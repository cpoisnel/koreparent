package com.univ.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.Rubrique;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.LimitHelper;
import com.univ.utils.sql.criterespecifique.RequeteSQLHelper;

/**
 * Classe de formatage de requete SQL.
 * @deprecated  A ne surtout pas utiliser. Cette classe introduit des failles de sécu SQLinjection, elle échape pas correctement les caractères.
 */
@Deprecated
public class SQLUtil {

    private static Logger LOG = LoggerFactory.getLogger(SQLUtil.class);


    /**
     * Méthode technique d'ajout d'un critère à la requête SQL.
     *
     * @deprecated on ne doit plus passer par cette méthode pour construire ces requêtes
     * @param _requete
     *            the _requete
     * @param _nouveauCritere
     *            the _nouveau critere
     *
     * @return the string
     */
    @Deprecated
    public static String ajouterCritere(final String _requete, final String _nouveauCritere) {
        if (_nouveauCritere.length() == 0) {
            return _requete;
        }
        String requete;
        final String nouveauCritere = "(" + _nouveauCritere + ")";
        if (_requete.length() == 0) {
            requete = "WHERE " + nouveauCritere;
        } else {
            requete = _requete + " AND " + nouveauCritere;
        }
        return requete;
    }

    /**
     * Formatage d'une date au format de la base (pour l'instant Mysql).
     *
     * @deprecated
     * @param _date
     *            the _date
     *
     * @return the string
     */
    @Deprecated
    public static String formaterDate(final Object _date) {
        String value = "";
        try {
            final Date date = (Date) _date;
            final java.util.GregorianCalendar cal = new java.util.GregorianCalendar();
            cal.setTime(date);
            if (cal.get(Calendar.YEAR) > 1970) {
                final Object[] arguments = { new Integer(cal.get(Calendar.DAY_OF_MONTH)), new Integer(cal.get(Calendar.MONTH) + 1), new Integer(cal.get(Calendar.YEAR)) };
                value = java.text.MessageFormat.format("{2,number,0000}-{1,number,00}-{0,number,00}", arguments);
            } else {
                value = "";
            }
        } catch (final Exception e) {
            LOG.error("erreur dans les arguments fourni pour le format", e);
        }
        return value;
    }

    /**
     * Despecialise chaine.
     *
     * @param chaine
     *            the chaine
     *
     * @return the string
     */
    public static String despecialiseChaine(String chaine) {
        if (chaine == null) {
            return chaine;
        }
        final char[] listeCars = { '\\', '\'', '"', '#', '(', ')', ';' };
        for (final char listeCar : listeCars) {
            int idxCar = chaine.indexOf(listeCar);
            while (idxCar != -1) {
                chaine = chaine.substring(0, idxCar) + "\\" + listeCar + chaine.substring(idxCar + 1);
                idxCar += 2;
                idxCar = chaine.indexOf(listeCar, idxCar);
            }
        }
        return chaine;
    }

    /**
     * Recherche de plusieurs mots dans une zone.
     *
     * @param _nomDonnee
     *            the _nom donnee
     * @param _motsCles
     *            the _mots cles
     *
     * @return the string
     * @deprecated utiliser {@link ConditionHelper#rechercheMots(String, String)}
     */
    @Deprecated
    public static String formaterRechercheMots(final String _nomDonnee, final String _motsCles) {
        String resultatRequete = "";
        if (StringUtils.isNotEmpty(_motsCles)) {
            final StringTokenizer motsCles = new StringTokenizer(_motsCles.toUpperCase(), " +'\"");
            boolean debutRequete = true;
            while (motsCles.hasMoreTokens()) {
                if (!debutRequete) {
                    resultatRequete += " AND ";
                }
                debutRequete = false;
                resultatRequete += _nomDonnee + " LIKE '%" + despecialiseChaine(motsCles.nextToken()) + "%'";
            }
        }
        return resultatRequete;
    }

    /**
     * Recherche de plusieurs mots dans plusieurs zones.
     *
     * @deprecated jamais utilisé de toute façon...
     * @param _listeZones
     *            the _liste zones
     * @param _motsCles
     *            the _mots cles
     *
     * @return the string
     */
    @Deprecated
    public static String formaterRechercheMotsZones(final String[] _listeZones, final String _motsCles) {
        String resultatRequete = "";
        if (_motsCles.length() > 0) {
            final StringTokenizer motsCles = new StringTokenizer(_motsCles.toUpperCase(), " +'\"");
            boolean debutRequete = true;
            while (motsCles.hasMoreTokens()) {
                if (!debutRequete) {
                    resultatRequete += " AND ";
                }
                debutRequete = false;
                final String mot = motsCles.nextToken();
                for (int i = 0; i < _listeZones.length; i++) {
                    if (i > 0) {
                        resultatRequete += " OR ";
                    } else {
                        resultatRequete += "(";
                    }
                    resultatRequete += _listeZones[i] + " LIKE '%" + despecialiseChaine(mot) + "%'";
                }
                resultatRequete += ")";
            }
        }
        return resultatRequete;
    }

    /**
     * Recherche d'une fiche par le rédacteur : - soit par code rédacteur uniquement -> '418' - soit par code rédacteur ou par code à blanc et par structure de rattachement
     * (relai-composante) -> '418&CODE_RATTACHEMENT=906'.
     *
     * @deprecated ca n'a aucun sens de splitter le code rédacteur pour rechercher sur le code de rattachement après...
     * @param _nomDonnee
     *            the _nom donnee
     * @param _redacteurs
     *            the _redacteurs
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String formaterRechercheParRedacteur(final String _nomDonnee, final String _redacteurs) throws Exception {
        String resultatRequete = "";
        String redacteur = "";
        if (_redacteurs.length() > 0) {
            final StringTokenizer st = new StringTokenizer(_redacteurs, ";");
            boolean debutRequete = true;
            while (st.hasMoreTokens()) {
                redacteur = st.nextToken();
                if (!debutRequete) {
                    /* Extraction du code rattachement */
                    final StringTokenizer st2 = new StringTokenizer(redacteur, "=");
                    String nom2 = "", codeStructure = "";
                    int indice2 = 0;
                    while (st2.hasMoreTokens()) {
                        if (indice2 == 0) {
                            nom2 = st2.nextToken();
                        }
                        if (indice2 == 1) {
                            codeStructure = st2.nextToken();
                        }
                        indice2++;
                    }
                    // JSS 20030610-001
                    if (codeStructure.length() > 0) {
                        resultatRequete += " OR (CODE_REDACTEUR='' AND ";
                        resultatRequete += formaterRechercheParStructure(nom2, codeStructure);
                        resultatRequete += ")";
                    }
                } else {
                    debutRequete = false;
                    resultatRequete += _nomDonnee + " = '" + EscapeString.escapeSql(redacteur) + "'";
                }
            }
        }
        return resultatRequete;
    }

    /**
     * Formater recherche par critere mutliple.
     *
     * @deprecated ne plus utiliser cette méthode pour construire des requetes remplacer par {@link ConditionHelper#likePourValeursMultiple(String, String)}
     * @param _nomDonnee
     *            the _nom donnee
     * @param _codes
     *            the _codes
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String formaterRechercheParCritereMutliple(final String _nomDonnee, String _codes) throws Exception {
        String res = "";
        _codes = StringUtils.replace(_codes, "+", ";");
        final StringTokenizer st = new StringTokenizer(_codes, ";");
        while (st.hasMoreTokens()) {
            final String item = st.nextToken();
            if (res.length() > 0) {
                res += " or ";
            }
            res += _nomDonnee + " like '" + item + "'";
            res += " or " + _nomDonnee + " like '" + item + ";%'";
            res += " or " + _nomDonnee + " like '%;" + item + "'";
            res += " or " + _nomDonnee + " like '%;" + item + ";%'";
        }
        return res;
    }

    /**
     * Formater recherche par code.
     *
     * @deprecated ne plus utiliser cette méthode pour construire des requetes
     * @param _nomDonnee
     *            the _nom donnee
     * @param _codes
     *            the _codes
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String formaterRechercheParCode(final String _nomDonnee, String _codes) throws Exception {
        String res = "";
        _codes = StringUtils.replace(_codes, "+", ";");
        final StringTokenizer st = new StringTokenizer(_codes, ";");
        while (st.hasMoreTokens()) {
            final String item = st.nextToken();
            if (res.length() > 0) {
                res += ",";
            }
            res += "'" + item + "'";
        }
        return _nomDonnee + " IN (" + res + ")";
    }

    /**
     * TODO secu BDD : a refondre _NOARBO Recherche d'une fiche par rapport à sa structure de rattachement.
     *
     * @param _nomDonnee
     *            the _nom donnee
     * @param _codeStructures
     *            the _code structures
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     * @deprecated utiliser {@link ConditionHelper#getConditionStructure(String, String)}
     */
    @Deprecated
    public static String formaterRechercheParStructure(final String _nomDonnee, final String _codeStructures) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        //    Les '+' sont à convertir en ';' (; interdits en saisie WYSIWYG)
        String newStructures = StringUtils.replace(_codeStructures, "+", ";");
        // la rubrique est spécifiée
        if (newStructures.length() > 0 && !newStructures.contains(";") && newStructures.endsWith("_NOARBO")) {
            newStructures = newStructures.substring(0, newStructures.indexOf("_NOARBO"));
            return _nomDonnee.toUpperCase() + "= '" + newStructures + "'";
        }
        String resultatRequete = "";
        //JSS 20021104-001
        final StringTokenizer stStructures = new StringTokenizer(newStructures, ";");
        //AM 200501 : déclarations de variables sorties de la boucle
        String codeStructure;
        while (stStructures.hasMoreTokens()) {
            codeStructure = stStructures.nextToken();
            if (codeStructure.length() > 0) {
                final List<StructureModele> subStructures = serviceStructure.getAllSubStructures(codeStructure, LangueUtil.getIndiceLocaleDefaut(), true);
                if (resultatRequete.length() > 0) {
                    resultatRequete += ",";
                }
                resultatRequete += "'" + codeStructure + "'";
                for (StructureModele currentStructure : subStructures) {
                    resultatRequete += ",";
                    resultatRequete += "'" + currentStructure.getCode() + "'";
                }
            }
        }
        if (resultatRequete.length() > 0) {
            return _nomDonnee.toUpperCase() + " IN (" + resultatRequete + ")";
        } else {
            return "";
        }
    }

    /**
     * Méthode technique de controle de restriction et de personnalisation.
     *
     * @deprecated utiliser {@link ConditionHelper#getConditionDSI(OMContext, FicheUniv)} Cependant, l'appel à cette fonction et à la fonction
     *             {@link SQLUtil#ajouterCriteresBO(String, OMContext, FicheUniv)} ont été regroupés
     *             {@link RequeteSQLHelper#getRequeteGenerique(com.univ.utils.sql.clause.ClauseWhere, OMContext, FicheUniv, String)}
     *
     * @param _requete
     *            the _requete
     * @param _ctx
     *            the _ctx
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String ajouterCriteresDsi(final String _requete, final OMContext _ctx) throws Exception {
        /* Controle si DSI activée */
        String activationDsi = PropertyHelper.getCoreProperty("dsi.activation");
        if (activationDsi == null) {
            activationDsi = "0";
        }
        if (activationDsi.equals("0")) {
            return _requete;
        }
        /* La DSI est activée */
        HttpServletRequest requeteHTTP = null;
        ContexteUniv ctx = null;
        // On controle que la requête est issue du Web
        boolean traiterPersonnalisation = false;
        if (_ctx instanceof ContexteUniv) {
            ctx = ((ContexteUniv) _ctx);
            if (ctx.isCalculListeResultatsFront()) {
                requeteHTTP = ctx.getRequeteHTTP();
                traiterPersonnalisation = true;
                if (requeteHTTP != null) {
                    requeteHTTP.getRemoteAddr();
                    String rechercheDomaine = PropertyHelper.getCoreProperty("dsi.recherche_domaine");
                    if (rechercheDomaine == null) {
                        rechercheDomaine = "0";
                    }
                    if (rechercheDomaine.equals("1")) {
                        requeteHTTP.getRemoteHost();
                    }
                }
            }
        }
        if (traiterPersonnalisation) {
            /***************/
            /* Restriction */
            /***************/
            String requete = "";
            String nouveauCritere = "( ";
            nouveauCritere += "T1.DIFFUSION_MODE_RESTRICTION = '0'";
            // Restriction par rapport aux profil et groupes
            String restrictionGroupe = calculerRestrictionGroupeDsi(ctx, "T1.DIFFUSION_PUBLIC_VISE");
            // JSS 20050510 : profil dynamique
            if (restrictionGroupe.length() > 0) {
                nouveauCritere += " OR ( T1.DIFFUSION_MODE_RESTRICTION = '2' AND (" + restrictionGroupe + ")    )";
            }
            // JSS 20050510 : profil dynamique
            restrictionGroupe = calculerRestrictionGroupeDsi(ctx, "T1.DIFFUSION_PUBLIC_VISE_RESTRICTION");
            if (restrictionGroupe.length() > 0) {
                nouveauCritere += " OR ( T1.DIFFUSION_MODE_RESTRICTION = '3' AND (" + restrictionGroupe + ")    )";
            }
            nouveauCritere += ")";
            if (_requete.length() == 0) {
                requete = "WHERE " + nouveauCritere;
            } else {
                requete = _requete + " AND " + nouveauCritere;
            }
            /********************/
            /* PERSONNALISATION */
            /********************/
            final String groupe = ctx.getGroupePersonnalisationCourant();
            if (groupe.length() > 0) {
                nouveauCritere = calculerPersonnalisationGroupeDsi(ctx, "T1.DIFFUSION_PUBLIC_VISE");
                if (nouveauCritere.length() > 0) {
                    requete = requete + " AND (" + nouveauCritere + ") ";
                }
            }
            String codeEspace = ctx.getEspacePersonnalisationCourant();
            if (codeEspace.length() > 0) {
                requete = requete + " AND ( T1.DIFFUSION_PUBLIC_VISE_RESTRICTION='" + codeEspace + "') ";
            }
            return requete;
        }
        return _requete;
    }

    /**
     * Chaine SQL de controle de restriction.
     *
     * @param ctx
     *            the ctx
     * @param nomColonne
     *            the nom colonne
     * @deprecated
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    private static String calculerPersonnalisationGroupeDsi(final ContexteUniv ctx, final String nomColonne) throws Exception {
        GroupeDsiBean group;
        /* *********************************************** */
        /*  Restriction par rapport aux groupes            */
        /* *********************************************** */
        if (ctx.getAutorisation() == null) {
            return nomColonne + " = 'ZZZZZZZZZZZZZZZZZZZZZZZZ'";
        }
        final List<GroupeDsiBean> listeGroupes = new ArrayList<>();
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        // groupe(s) de l'utilisateur
        if (ctx.getGroupePersonnalisationCourant().equals(IRequeteurConstantes.CODE_DYNAMIQUE)) {
            int niveau;
            for(String currentCode : ctx.getGroupesDsi()) {
                group = serviceGroupeDsi.getByCode(currentCode);
                listeGroupes.add(group);
                niveau = serviceGroupeDsi.getLevel(group);
                while (niveau > 0) {
                    group = serviceGroupeDsi.getByCode(group.getCodeGroupePere());
                    listeGroupes.add(group);
                    niveau--;
                }
            }
        }
        // FBO / PCO 2010-02-17
        // Restriction sur les groupes d'un type particulier de type de l'utilisateur
        //
        else if (ctx.getGroupePersonnalisationCourant().endsWith("_TYPEGROUPE")) {
            String sTypeCourant = "";
            try {
                /* On récupère le code du type de groupe recherché*/
                sTypeCourant = ctx.getGroupePersonnalisationCourant().substring(0, ctx.getGroupePersonnalisationCourant().indexOf("_TYPEGROUPE"));
            } catch (final Exception e) {
                sTypeCourant = "";
            }
            if (!"".equals(sTypeCourant)) {
                int niveau;
                for(String currentCode : ctx.getGroupesDsi()) {
                    group = serviceGroupeDsi.getByCode(currentCode);
                    if (sTypeCourant.equalsIgnoreCase(group.getType())) {
                        listeGroupes.add(group);
                    }
                    niveau = serviceGroupeDsi.getLevel(group);
                    while (niveau > 0) {
                        group = serviceGroupeDsi.getByCode(group.getCodeGroupePere());
                        if (sTypeCourant.equalsIgnoreCase(group.getType())) {
                            listeGroupes.add(group);
                        }
                        niveau--;
                    }
                }
            }
            /* Gestion des types de groupes avec aucune occurence*/
            if (listeGroupes.size() < 1) {
                return nomColonne + " = 'ZZZZZZZZZZZZZZZZZZZZZZZZ'";
            }
        }
        //groupe personnalisé au préalable
        else {
            final String[] groupes = ctx.getGroupePersonnalisationCourant().split(";");
            for (final String groupe : groupes) {
                group = serviceGroupeDsi.getByCode(groupe);
                listeGroupes.add(group);
            }
        }
        String restrictionGroupe = "";
        for(GroupeDsiBean currentGroup : listeGroupes) {
            if (restrictionGroupe.length() > 0) {
                restrictionGroupe += " OR ";
            }
            restrictionGroupe += nomColonne + " LIKE '%[/" + EscapeString.escapeSql(currentGroup.getCode()) + "]%'";
        }
        return restrictionGroupe;
    }

    /**
     * Chaine SQL de controle de restriction !!!! ATTENTION : CES CONTROLES SONT REDONDANTS AVEC CEUX EFFECTUES LORS D'UNE CONSULTATION DE FICHE
     * (FicheUnivMgr.controlerRestrictionGroupeDsi) : TOUTE MODIF DOIT DONC ETRE REPORTEE
     *
     * @deprecated
     * @param ctx
     *            the ctx
     * @param nomColonne
     *            the nom colonne
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    private static String calculerRestrictionGroupeDsi(final ContexteUniv ctx, final String nomColonne) throws Exception {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        /***************************************/
        /* On détermine l'ensemble des groupes */
        /***************************************/
        final TreeSet<String> listeGroupes = new TreeSet<>();
        for (final String groupeCourant : ctx.getGroupesDsi()) {
            listeGroupes.add(groupeCourant);
            // JSS 20040419 : le parcours se fait pour tous les groupes
            GroupeDsiBean group = serviceGroupeDsi.getByCode(groupeCourant);
            /* On recherche les items de niveau supérieur */
            final int niveauItemCourant = serviceGroupeDsi.getLevel(group);
            int niveau = niveauItemCourant - 1;
            //AM 200501 : déclaration de variable sortie de la boucle
            while (niveau > 0) {
                listeGroupes.add(group.getCodeGroupePere());
                group = serviceGroupeDsi.getByCode(group.getCodeGroupePere());
                niveau--;
            }
        }
        /*************************************************/
        /* Restriction par rapport aux profil et groupes */
        /*************************************************/
        String restrictionGroupe = "";
        //JSS 20050510 : profil dynamique
        // nomColonne + " LIKE '%[" + ctx.getProfilDsi() + "/]%' ";
        for (final String codeGroupeDsi : listeGroupes) {
            final String sqlGroupe = nomColonne + " LIKE '%[/" + EscapeString.escapeSql(codeGroupeDsi) + "]%'";
            //JSS 20050510 : profil dynamique
            //sqlGroupe += " OR " + nomColonne + " LIKE '%[" + ctx.getProfilDsi() + "/" + codeGroupeDsi + "]%' ";
            if (restrictionGroupe.length() > 0) {
                restrictionGroupe += " OR ";
            }
            restrictionGroupe += sqlGroupe;
        }
        return restrictionGroupe;
    }

    /**
     * Formatage SQL de la Recherche d'une fiche par rapport à sa rubrique principale ou une de ses rubriques de publication.
     *
     * @deprecated
     * @param _ctx
     *            the _ctx
     * @param _ficheUniv
     *            the _fiche univ
     * @param _codeRubrique
     *            the _code rubrique
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String formaterRechercheParRubriquePublication(final OMContext _ctx, final FicheUniv _ficheUniv, String _codeRubrique) throws Exception {
        StringBuffer sousReq = null;
        String rubriqueSite = null;
        boolean rechercheArborescente = true;
        InfosSite infosSite = null;
        // on controle que la requête est issue du web
        ContexteUniv ctx = null;
        // en front on restreint la rubrique au site courant si il est cloisonné
        if (_ctx instanceof ContexteUniv) {
            if (((ContexteUniv) _ctx).isCalculListeResultatsFront()) {
                ctx = ((ContexteUniv) _ctx);
                infosSite = ctx.getInfosSite();
                if (infosSite != null && infosSite.getRestriction() == 1) {
                    rubriqueSite = infosSite.getCodeRubrique();
                }
            }
        }
        //  la rubrique est spécifiée
        if (_codeRubrique.length() > 0) {
            if (_codeRubrique.endsWith("_NOARBO")) {
                _codeRubrique = _codeRubrique.substring(0, _codeRubrique.indexOf("_NOARBO"));
                rechercheArborescente = false;
            }
        }
        // si la rubrique a été forcée au site courant
        if (rubriqueSite != null && rubriqueSite.length() > 0) {
            final InfosRubriques infosRub = Rubrique.renvoyerItemRubrique(_codeRubrique);
            if (_codeRubrique.length() > 0 && infosRub.getCode().equals("")) {
                _codeRubrique = ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE;
            } else {
                // restriction au site uniquement si la rubrique n'appartient pas au site
                if (_codeRubrique.length() == 0) {
                    _codeRubrique = rubriqueSite;
                } else if (!infosSite.isRubriqueVisibleInSite(infosRub)) {
                    _codeRubrique = ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE;
                }
            }
        }
        String dsiActif = PropertyHelper.getCoreProperty("dsi.activation");
        if (dsiActif == null) {
            dsiActif = "0";
        }
        InfosRubriques rubrique = Rubrique.renvoyerItemRubrique(_codeRubrique);
        if (_codeRubrique.length() > 0 && rubrique.getCode().equals("")) {
            _codeRubrique = ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE;
        }
        // en front on applique la dsi sur les rubriques
        Collection<InfosRubriques> listeRubriques = new HashSet<InfosRubriques>();
        if (!_codeRubrique.equals(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE)) {
            if (dsiActif.equals("1") && ctx != null) {
                if (Rubrique.controlerRestrictionRubrique(ctx, _codeRubrique)) {
                    if (rechercheArborescente) {
                        listeRubriques = Rubrique.determinerListeSousRubriquesAutorisees(ctx, rubrique);
                    }
                } else {
                    // si aucun droit on bloque la recherche par un code bidon
                    rubrique = new InfosRubriques(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE);
                }
                listeRubriques.add(rubrique);
            } else {
                // JSS 20051205 : dans le back, si rubrique vide, on ne renvoie
                // aucune rubrique
                if (_codeRubrique.length() > 0) {
                    if (rechercheArborescente) {
                        listeRubriques = rubrique.getListeSousRubriquesTousNiveaux();
                    }
                    listeRubriques.add(rubrique);
                }
            }
        } else {
            rubrique = new InfosRubriques(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE);
            listeRubriques.add(rubrique);
        }
        // si on au moins une de recherche
        if (!listeRubriques.isEmpty()) {
            //   210 est la taille moyenne estimée de la requête pour chaque code rubrique de 30 caractères
            sousReq = new StringBuffer(100 * listeRubriques.size());
            final Iterator<InfosRubriques> iter = listeRubriques.iterator();
            String codeRubriqueCourant = null;
            while (iter.hasNext()) {
                codeRubriqueCourant = iter.next().getCode();
                if (sousReq.length() > 0) {
                    sousReq.append(",");
                }
                sousReq.append("'" + codeRubriqueCourant + "'");
            }
        }
        if (sousReq == null) {
            return "";
        } else {
            return " T1.CODE_RUBRIQUE IN (" + sousReq.toString() + ") OR RUB_PUB.RUBRIQUE_DEST IN (" + sousReq.toString() + ") ";
        }
    }

    /**
     * Recherche d'une fiche par rapport à sa rubrique (récursif).
     *
     * @deprecated utiliser {@link ConditionHelper#getConditionRubrique(String, String)}
     * @param _nomDonnee
     *            the _nom donnee
     * @param _codeRubrique
     *            the _code rubrique
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String formaterRechercheParRubrique(final String _nomDonnee, String _codeRubrique) throws Exception {
        // la rubrique est spécifiée
        if (_codeRubrique.length() > 0 && _codeRubrique.indexOf(";") == -1 && _codeRubrique.endsWith("_NOARBO")) {
            _codeRubrique = _codeRubrique.substring(0, _codeRubrique.indexOf("_NOARBO"));
            return _nomDonnee.toUpperCase() + "= '" + _codeRubrique + "'";
        }
        final InfosRubriques infosRub = Rubrique.renvoyerItemRubrique(_codeRubrique);
        final Collection<InfosRubriques> sousRubriques = infosRub.getListeSousRubriquesTousNiveaux();
        String sousReq = "";
        if (!sousRubriques.isEmpty()) {
            final Iterator<InfosRubriques> it = sousRubriques.iterator();
            while (it.hasNext()) {
                if (sousReq.length() > 0) {
                    sousReq += " OR ";
                }
                sousReq += _nomDonnee.toUpperCase() + " = '" + it.next().getCode() + "'";
            }
        }
        if (sousReq.length() > 0) {
            sousReq += " OR ";
        }
        sousReq += _nomDonnee.toUpperCase() + " = '" + _codeRubrique + "'";
        return sousReq;
    }

    /**
     * Récupération de la semaine à partir d'une date.
     *
     * @param _date
     *            the _date
     *
     * @return the mois
     */
    public static String getMois(final Object _date) {
        String value = "";
        final Date date = (Date) _date;
        final java.util.GregorianCalendar cal = new java.util.GregorianCalendar();
        cal.setTime(date);
        value = "" + (cal.get(Calendar.MONTH) + 1);
        return value;
    }

    /**
     * Récupération de la semaine à partir d'une date.
     *
     * @param _date
     *            the _date
     *
     * @return the semaine
     */
    public static String getSemaine(final Object _date) {
        String value = "";
        try {
            final Date date = (Date) _date;
            final java.util.GregorianCalendar cal = new java.util.GregorianCalendar(new Locale("FRENCH"));
            cal.setTime(date);
            cal.setMinimalDaysInFirstWeek(7);
            value = "" + cal.get(Calendar.WEEK_OF_YEAR);
        } catch (final Exception e) {}
        return value;
    }

    /**
     * Récupération de la semaine à partir d'une date.
     *
     * @param _date
     *            the _date
     *
     * @return the annee
     */
    public static String getAnnee(final Object _date) {
        String value = "";
        try {
            final Date date = (Date) _date;
            final java.util.GregorianCalendar cal = new java.util.GregorianCalendar();
            cal.setTime(date);
            value = "" + cal.get(Calendar.YEAR);
        } catch (final Exception e) {}
        return value;
    }

    /**
     * Méthode technique de controle des perimètres de back-office en fonction des autorisations (complete la methode select de l'objet metier).
     *
     * @deprecated utiliser {@link ConditionHelper#getConditionBO(OMContext, FicheUniv)} Cependant, l'appel à cette fonction et à la fonction
     *             {@link SQLUtil#ajouterCriteresDsi(String, OMContext)} ont été regroupés
     *             {@link RequeteSQLHelper#getRequeteGenerique(com.univ.utils.sql.clause.ClauseWhere, OMContext, FicheUniv, String)}
     *
     * @param _requete
     *            the _requete
     * @param _ctx
     *            the _ctx
     * @param ficheUniv
     *            the fiche univ
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String ajouterCriteresBO(final String _requete, final OMContext _ctx, final FicheUniv ficheUniv) throws Exception {
        final Map<String, Object> datas = _ctx.getDatas();
        if (datas == null) {
            return _requete;
        }
        final String controle = (String) datas.get("CONTROLE_PERIMETRE_BO");
        if ((controle == null) || (controle.equals("1") == false)) {
            return _requete;
        }
        String requete = "";
        final String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
        String nouveauCritere = "";
        boolean aucuneRestriction = false;
        /* Boucle sur les permissions */
        final AutorisationBean autorisations = (AutorisationBean) datas.get("AUTORISATIONS");
        final Hashtable<String, Vector<Perimetre>> listePermissions = autorisations.getListePermissions();
        final Enumeration<String> e = listePermissions.keys();
        while (e.hasMoreElements()) {
            final String key = e.nextElement();
            final PermissionBean permission = new PermissionBean(key);
            if ((permission.getType().equals("FICHE")) && (permission.getObjet().equals(codeObjet)) && (permission.getAction().equals("M"))) {
                final Vector<Perimetre> listePerimetres = listePermissions.get(key);
                for (int iPerimetre = 0; iPerimetre < listePerimetres.size(); iPerimetre++) {
                    String critereSelectionPerimetre = "";
                    final Perimetre perimetre = listePerimetres.get(iPerimetre);
                    if (perimetre.getCodeRubrique().length() == 0 && perimetre.getCodeStructure().length() == 0 && perimetre.getCodeProfil().length() == 0 && perimetre.getCodeGroupe().length() == 0) {
                        aucuneRestriction = true;
                    } else {
                        // Permet de gerér les objets pour lesquels les périmètres
                        // groupes et structures ne sont pas pertinents
                        boolean criteresPertinents = false;
                        /* Restriction rubrique */
                        final String codeRubrique = perimetre.getCodeRubrique();
                        // JSS 20051104 : gestion périmètre
                        if (codeRubrique.length() > 0) {
                            criteresPertinents = true;
                            if (codeRubrique.equals("-")) {
                                final String nomCodeRubrique = "CODE_RUBRIQUE";
                                critereSelectionPerimetre = nomCodeRubrique + " = '' ";
                            } else {
                                final String nomCodeRubrique = "CODE_RUBRIQUE";
                                critereSelectionPerimetre = "(" + SQLUtil.formaterRechercheParRubrique(nomCodeRubrique, codeRubrique) + ")";
                            }
                        }
                        /* Restriction structure */
                        final String codeStructure = perimetre.getCodeStructure();
                        if (codeStructure.length() > 0) {
                            String criteresStructure = "";
                            criteresPertinents = true;
                            // JSS 20051104 : gestion périmètre
                            if (codeStructure.equals("-")) {
                                // ??????????
                                String nomCodeStructure = "CODE_RATTACHEMENT";
                                criteresStructure = nomCodeStructure + " = '' ";
                                // JSS 20051031 : rattachement secondaires
                                if (ficheUniv instanceof FicheRattachementsSecondaires) {
                                    nomCodeStructure = "CODE_RATTACHEMENT_AUTRES";
                                    criteresStructure = criteresStructure + " AND " + nomCodeStructure + " = '' ";
                                }
                            } else {
                                String nomCodeStructure = "CODE_RATTACHEMENT";
                                criteresStructure = SQLUtil.formaterRechercheParStructure(nomCodeStructure, codeStructure);
                                // DEBUG JSS 20050405 : il manque les structures directement égales au périmètre
                                // On rajoute une clause CODE = code_du_périmètre
                                if (ficheUniv instanceof StructureModele) {
                                    criteresStructure = " CODE = '" + codeStructure + "' OR " + criteresStructure;
                                }
                                // JSS 20051031 : rattachement secondaires
                                if (ficheUniv instanceof FicheRattachementsSecondaires) {
                                    nomCodeStructure = "CODE_RATTACHEMENT_AUTRES";
                                    criteresStructure = criteresStructure + " OR (" + SQLUtil.formaterRechercheParStructureMultiple(nomCodeStructure, codeStructure) + ")";
                                }
                            }
                            if (critereSelectionPerimetre.length() > 0) {
                                critereSelectionPerimetre += " AND ";
                            }
                            critereSelectionPerimetre += "(" + criteresStructure + ")";
                        }
                        // JSS 20040409 : espace collaboratif
                        /* Restriction espace */
                        final String codeEspaceCollaboratif = perimetre.getCodeEspaceCollaboratif();
                        if (codeEspaceCollaboratif.length() > 0) {
                            if (critereSelectionPerimetre.length() > 0) {
                                critereSelectionPerimetre += " AND ";
                            }
                            critereSelectionPerimetre += "(T1.DIFFUSION_MODE_RESTRICTION='4' AND T1.DIFFUSION_PUBLIC_VISE_RESTRICTION='" + codeEspaceCollaboratif + "')";
                            criteresPertinents = true;
                        }
                        if (critereSelectionPerimetre.length() > 0 && criteresPertinents) {
                            if (nouveauCritere.length() > 0) {
                                nouveauCritere += " OR ";
                            }
                            nouveauCritere += "(" + critereSelectionPerimetre + ")";
                        }
                    } // fin du else aucune restriction
                }// fin for
            }
        }// fin while
            // Aucun périmètre ne correspond
        if ((nouveauCritere.length() == 0) && (aucuneRestriction == false)) {
            nouveauCritere = "CODE='ZZZZZZZZZZZZZZZZZZZZ'";
        }
        // JSS 20051202 :  On rajoute systématiquement les fiches dont on est rédacteur
        if (nouveauCritere.length() > 0) {
            nouveauCritere = "( " + nouveauCritere + " OR CODE_REDACTEUR = '" + autorisations.getCode() + "' )";
        }
        requete = ajouterCritere(_requete, nouveauCritere);
        return requete;
    }

    /**
     * Recherche d'une fiche par rapport à sa structure de rattachement (zone multivaluée).
     *
     * @param _nomDonnee
     *            the _nom donnee
     * @param _codeStructures
     *            the _code structures
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     * @deprecated utiliser {@link ConditionHelper#getConditionStructureMultiple(String, String)}
     */
    @Deprecated
    public static String formaterRechercheParStructureMultiple(final String _nomDonnee, final String _codeStructures) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        String resultatRequete = "";
        //JSS 20021104-001
        //Les '+' sont à convertir en ';' (; interdits en saisie WYSIWYG)
        final String newStructures = StringUtils.replace(_codeStructures, "+", ";");
        final StringTokenizer stStructures = new StringTokenizer(newStructures, ";");
        String codeStructure;
        while (stStructures.hasMoreTokens()) {
            codeStructure = stStructures.nextToken();
            if (!codeStructure.equals("")) {
                final List<StructureModele> subStructures = serviceStructure.getAllSubStructures(codeStructure, LangueUtil.getIndiceLocaleDefaut(), true);
                if (resultatRequete.length() > 0) {
                    resultatRequete += "|";
                }
                resultatRequete += codeStructure;
                for (StructureModele currentStructure : subStructures) {
                    resultatRequete += "|";
                    resultatRequete += currentStructure.getCode();
                }
            }
        }
        if (resultatRequete.length() > 0) {
            return _nomDonnee.toUpperCase() + " REGEXP '(^|.*;|.*\\\\[)(" + resultatRequete + ")($|;.*|\\\\].*)'";
        } else {
            return "";
        }
    }

    /**
     * Ajout des critères de jointure (spécifique à l'objet + générique pour les rubriques de publication).
     *
     * @deprecated ne pas faire les jointures à partir de cette méthode pour éviter les injections sql
     * @param _requete
     *            the _requete
     * @param _ctx
     *            the _ctx
     * @param ficheUniv
     *            the fiche univ
     * @param _leftJoinTable
     *            the _left join table
     * @param _leftJoinOnClause
     *            the _left join on clause
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String ajouterCriteresJointure(final String _requete, final OMContext _ctx, final FicheUniv ficheUniv, final String _leftJoinTable, final String _leftJoinOnClause)
        throws Exception {
        String debutRequete = "";
        String tempRequete = null;
        if ((_requete.indexOf("RUB_PUB.") != -1) || (_requete.indexOf("META.") != -1) || (_leftJoinTable.length() > 0)) {
            debutRequete = "";
            if (_requete.indexOf("RUB_PUB.") != -1) {
                debutRequete += " LEFT JOIN RUBRIQUEPUBLICATION RUB_PUB    ";
                debutRequete += " ON ( T1.CODE = RUB_PUB.CODE_FICHE_ORIG AND T1.LANGUE = RUB_PUB.LANGUE_FICHE_ORIG AND RUB_PUB.TYPE_FICHE_ORIG='" + ReferentielObjets.getCodeObjet(ficheUniv) + "')";
            }
            if (_requete.indexOf("META.") != -1) {
                final String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
                final String nomClasse = ReferentielObjets.getNomObjet(codeObjet).toUpperCase();
                debutRequete += " LEFT JOIN METATAG META ";
                debutRequete += " ON (T1.ID_" + nomClasse + " = META.META_ID_FICHE )";
                tempRequete = "( META.META_CODE_OBJET  = '" + codeObjet + "' )";
            } else if (_requete.indexOf("RUB_PUB.") != -1) {
                tempRequete = "(RUB_PUB.ID_RUBRIQUEPUBLICATION IS NULL OR RUB_PUB.TYPE_FICHE_ORIG = '" + ReferentielObjets.getCodeObjet(ficheUniv) + "' )";
            }
            if (_leftJoinTable.length() > 0) {
                debutRequete += " LEFT JOIN " + _leftJoinTable + " ";
                debutRequete += " ON (" + _leftJoinOnClause + ") ";
            }
        }
        String requete;
        if (tempRequete != null) {
            if (_requete.startsWith("WHERE")) {
                requete = debutRequete + " WHERE " + tempRequete + " AND "
                // "WHERE".length( ) == 5
                + _requete.substring(5);
            } else {
                requete = debutRequete + " WHERE " + tempRequete;
            }
        } else {
            requete = debutRequete + _requete;
        }
        return requete;
    }

    /**
     * Ajout des critères de jointure (spécifique à l'objet + générique pour les rubriques de publication). On passe les left join et les clauses dans des tableaux.
     *
     * @deprecated ne pas faire les jointures à partir de cette méthode pour éviter les injections sql
     *
     *             20051117 : Optimisée pour les left join sur plusieurs tables.
     *
     * @param _requete
     *            requete a completer
     * @param _ctx
     *            contexte
     * @param ficheUniv
     *            fiche pour recuperation du code de l'objet
     * @param _leftsJoinTable
     *            tableau des left join
     * @param _leftsJoinOnClause
     *            tableau des clauses de jointures
     *
     * @return requete
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String ajouterCriteresJointure(final String _requete, final OMContext _ctx, final FicheUniv ficheUniv, final String[] _leftsJoinTable,
        final String[] _leftsJoinOnClause) throws Exception {
        String debutRequete = "";
        String tempRequete = null;
        int nbJointures = 0;
        if (_leftsJoinTable != null) {
            nbJointures = _leftsJoinOnClause.length;
        }
        if ((_requete.indexOf("RUB_PUB.") != -1) || (_requete.indexOf("META.") != -1) || (nbJointures > 0)) {
            debutRequete = "";
            if (_requete.indexOf("RUB_PUB.") != -1) {
                debutRequete += " LEFT JOIN RUBRIQUEPUBLICATION RUB_PUB    ";
                debutRequete += " ON (T1.CODE = RUB_PUB.CODE_FICHE_ORIG AND T1.LANGUE = RUB_PUB.LANGUE_FICHE_ORIG AND RUB_PUB.TYPE_FICHE_ORIG='" + ReferentielObjets.getCodeObjet(ficheUniv) + "')";
            }
            if (_requete.indexOf("META.") != -1) {
                final String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
                final String nomClasse = ReferentielObjets.getNomObjet(codeObjet).toUpperCase();
                debutRequete += " LEFT JOIN METATAG META ";
                debutRequete += " ON ( T1.ID_" + nomClasse + " = META.META_ID_FICHE ) ";
                tempRequete = "( META.META_CODE_OBJET  = '" + codeObjet + "' )";
            } else if (_requete.indexOf("RUB_PUB.") != -1) {
                tempRequete = "( RUB_PUB.ID_RUBRIQUEPUBLICATION IS NULL OR RUB_PUB.TYPE_FICHE_ORIG = '" + ReferentielObjets.getCodeObjet(ficheUniv) + "' )";
            }
            if (nbJointures > 0) {
                if (nbJointures > _leftsJoinOnClause.length) {
                    nbJointures = _leftsJoinOnClause.length;
                }
                for (int i = 0; i < nbJointures; ++i) {
                    debutRequete += " LEFT JOIN " + _leftsJoinTable[i] + " ON (" + _leftsJoinOnClause[i] + ") ";
                }
            }
        }
        String requete;
        if (tempRequete != null) {
            if (_requete.startsWith("WHERE")) {
                requete = debutRequete + " WHERE " + tempRequete + " AND "
                // "WHERE".length( ) == 5
                + _requete.substring(5);
            } else {
                requete = debutRequete + " WHERE " + tempRequete;
            }
        } else {
            requete = debutRequete + _requete;
        }
        return requete;
    }

    /**
     * Ajout des critères génériques (dsi, back-office, rubrique publication).
     *
     * @deprecated utiliser {@link RequeteSQLHelper#getRequeteGenerique(com.univ.utils.sql.clause.ClauseWhere, OMContext, FicheUniv, String)}
     * @param _requete
     *            the _requete
     * @param _ctx
     *            the _ctx
     * @param _ficheUniv
     *            the _fiche univ
     * @param _codeRubrique
     *            code rubrique de recherche
     * @param _leftJoinTable
     *            table métier à intégrer dans le left join
     * @param _leftJoinOnClause
     *            clauses métiers à intégrer dans le ON du LEFT JOINT
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String ajouterCriteresGeneriques(final String _requete, final OMContext _ctx, final FicheUniv _ficheUniv, final String _codeRubrique,
        final String _leftJoinTable, final String _leftJoinOnClause) throws Exception {
        String requete = _requete;
        // RP20051118 jointure sur la date de déclenchement
        if (_ctx instanceof ContexteUniv) {
            if (((ContexteUniv) _ctx).isCalculListeResultatsFront()) {
                final Date _aujourdhui = new Date(System.currentTimeMillis());
                final SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd");
                requete = SQLUtil.ajouterCritere(requete, " DATE_FORMAT(META.META_DATE_MISE_EN_LIGNE, '%Y-%m-%d') <= '" + formatDateTime.format(_aujourdhui) + "'");
                // si le paramètre jtf structure.inTree est activé, on ne filtre pas les structures dans les résultats
                if (_ficheUniv instanceof StructureModele && !"1".equals(PropertyHelper.getCoreProperty("structure.inTree"))) {
                    requete = SQLUtil.ajouterCritere(requete, "META.META_IN_TREE ='1'");
                }
            }
        }
        if (_ficheUniv instanceof DiffusionSelective) {
            requete = SQLUtil.ajouterCriteresDsi(requete, _ctx);
        }
        // delegation
        requete = SQLUtil.ajouterCriteresBO(requete, _ctx, _ficheUniv);
        // multi-sites
        requete = SQLUtil.ajouterCritere(requete, SQLUtil.formaterRechercheParRubriquePublication(_ctx, _ficheUniv, _codeRubrique));
        // Jointure
        requete = SQLUtil.ajouterCriteresJointure(requete, _ctx, _ficheUniv, _leftJoinTable, _leftJoinOnClause);
        return requete;
    }

    /**
     * Ajout des critères génériques (dsi, back-office, rubrique publication)
     *
     * 20051117 : Optimisée pour les left join sur plusieurs tables.
     *
     * @deprecated ne pas utiliser cette méthode pour éviter les injections sql
     * @param _requete
     *            the _requete
     * @param _ctx
     *            the _ctx
     * @param _ficheUniv
     *            the _fiche univ
     * @param _codeRubrique
     *            code rubrique de recherche
     * @param _leftJoinTable
     *            table métier à intégrer dans le left join
     * @param _leftJoinOnClause
     *            clauses métiers à intégrer dans le ON du LEFT JOINT
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String ajouterCriteresGeneriques(final String _requete, final OMContext _ctx, final FicheUniv _ficheUniv, final String _codeRubrique,
        final String[] _leftJoinTable, final String[] _leftJoinOnClause) throws Exception {
        String requete = _requete;
        // RP20051118 jointure sur la date de déclenchement
        if (_ctx instanceof ContexteUniv) {
            final Date _aujourdhui = new Date(System.currentTimeMillis());
            final SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyy-MM-dd");
            requete = SQLUtil.ajouterCritere(requete, " DATE_FORMAT(META.META_DATE_MISE_EN_LIGNE, '%Y-%m-%d') <= '" + formatDateTime.format(_aujourdhui) + "'");
        }
        if (_ficheUniv instanceof DiffusionSelective) {
            requete = SQLUtil.ajouterCriteresDsi(requete, _ctx);
        }
        // delegation
        requete = SQLUtil.ajouterCriteresBO(requete, _ctx, _ficheUniv);
        // multi-sites
        requete = SQLUtil.ajouterCritere(requete, SQLUtil.formaterRechercheParRubriquePublication(_ctx, _ficheUniv, _codeRubrique));
        // Jointure
        requete = SQLUtil.ajouterCriteresJointure(requete, _ctx, _ficheUniv, _leftJoinTable, _leftJoinOnClause);
        return requete;
    }

    /**
     * Ajout des critères de limite (dsi, back-office, rubrique publication)
     *
     * 20051117 : Optimisée pour les left join sur plusieurs tables.
     *
     * @deprecated utiliser {@link LimitHelper#ajouterCriteresLimitesEtOptimisation(OMContext, String)}
     * @param _requete
     *            the _requete
     * @param _ctx
     *            the _ctx
     * @param _limit
     *            the _limit
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public static String ajouterCriteresLimitesEtOptimisation(final String _requete, final OMContext _ctx, final String _limit) throws Exception {
        String requete = _requete;
        try {
            if (_limit.length() > 0) {
                Integer.parseInt(_limit);
                if (_ctx.getDatas().get("optimizedSelect") != null) {
                    _ctx.getDatas().put("optimizedLimit", _limit);
                } else {
                    requete = requete + " LIMIT  0," + _limit;
                }
            }
        } catch (final Exception e) {} finally {
            _ctx.getDatas().put("optimizedObject", "true");
        }
        return requete;
    }
}
