package com.univ.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Apporte des fonctions utilitaires pour gérer les accés à la base de données.
 *
 * @author pierre.cosson
 * @deprecated ancienne classe permettant de faire des pseudos DAO, cette classe n'est plus d'utilité depuis la nouvelle architecture
 */
@Deprecated
public abstract class AbstractBaseDonneeDao {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractBaseDonneeDao.class);

    protected ContexteDao instancierContexteDao() {
        return new ContexteDao();
    }

    /**
     * Fermer les accés ouvert pour accéder à la base de données.
     *
     * @param ctx
     */
    protected void fermerAccesBD(final ContexteDao ctx) {
        fermerAccesBD(ctx, null, null);
    }

    /**
     * Fermer les accés ouvert pour accéder à la base de données.
     *
     * @param ctx
     * @param stmt
     */
    protected void fermerAccesBD(final ContexteDao ctx, final PreparedStatement stmt) {
        fermerAccesBD(ctx, stmt, null);
    }

    /**
     * Fermer les accés ouvert pour accéder à la base de données.
     *
     * @param ctx
     * @param stmt
     * @param res
     */
    protected void fermerAccesBD(final ContexteDao ctx, final PreparedStatement stmt, final ResultSet res) {
        try {
            if (res != null) {
                res.close();
            }
        } catch (final Exception e) {
            LOG.debug("Erreur lors de la fermeture du resultSet", e);
        }
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (final Exception e) {
            LOG.debug("Erreur lors de la fermeture du statement", e);
        }
        if (ctx != null) {
            ctx.close();
        }
    }
}
