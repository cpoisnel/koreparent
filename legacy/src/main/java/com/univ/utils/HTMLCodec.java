/*
 * HTMLCodec.java
 *
 * Created on August 20, 2002, 5:37 PM
 */
package com.univ.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
// TODO: Auto-generated Javadoc

/**
 * Converts ASCII characters to their HTML entities and back This may not contain all the HTML 4.0 entities. I haven't checked to make sure.
 * @deprecated cette classe ne fait pas du tout ce qu'elle dit. Elle se base sur StringTokenizer(machaine, "") ce qui renvoie comme token, la chaine en question...
 */
@Deprecated
public class HTMLCodec {

    /** Singleton instance. */
    private static HTMLCodec codec = null;

    /** The entity lookup. */
    private final Map<String, String> entityLookup;

    /** The char lookup. */
    private final Map<String, Object> charLookup;

    /**
     * Creates a new instance of HTMLCodec.
     */
    public HTMLCodec() {
        /** set up the HashMaps */
        entityLookup = new HashMap<>();
        charLookup = new HashMap<>();
        initialize();
    }

    /**
     * Returns an instance of the specific codec to be used. This is so they can be singletons and reused.
     *
     * @return The Codec object to use.
     */
    public static HTMLCodec getInstance() {
        if (codec == null) {
            codec = new HTMLCodec();
        }
        return codec;
    }

    /**
     * This method decodes the string passed in and returns a decoded version of the string.
     *
     * @param chars
     *            The encoded text string that should be decoded
     *
     * @return The encoded version of the string passed in.
     */
    public final String decode(String chars) {
        if (!charLookup.containsKey("&#32;")) {
            // this is an expensive one to call so lets make sure it hasn't already been set up.
            addBasicChars();
        }
        StringBuilder tempBuffer = new StringBuilder();
        StringBuilder results = new StringBuilder();
        StringTokenizer tokenizer = new StringTokenizer(chars, "");
        int lastAmpersandIndex = -1;
        while (tokenizer.hasMoreElements()) {
            String currentChar = tokenizer.nextToken();
            tempBuffer.append(currentChar);
            if ("&".equals(currentChar)) {
                lastAmpersandIndex = tempBuffer.length();
            }
            if (";".equals(currentChar) && lastAmpersandIndex > -1 && lastAmpersandIndex > tempBuffer.length() - 7) {
                // 7 because of the &#xxxx; entities
                try {
                    results.append((String) charLookup.get(tempBuffer.toString()));
                } catch (NullPointerException e) {
                    results.append(tempBuffer.toString());
                }
                lastAmpersandIndex = -1;
                tempBuffer.delete(1, tempBuffer.length());
            }
        }
        return results.toString();
    }

    /**
     * This method decodes the character array passed in and returns a decoded version of the character array.
     *
     * @param chars
     *            The plain text character array that should be encode
     *
     * @return The encoded version of the character array passed in.
     */
    public char[] decode(char[] chars) {
        return decode(new String(chars)).toCharArray();
    }

    /**
     * This method encodes the character array passed in and returns an encoded character array.
     *
     * @param chars
     *            The plain text character array that should be encode
     *
     * @return The encoded version of the character array passed in.
     */
    public char[] encode(char[] chars) {
        return encode(new String(chars)).toCharArray();
    }

    /**
     * This method encodes the string passed in and returns the encoded string.
     *
     * @param chars
     *            The plain text string that should be encode
     *
     * @return The encoded version of the string passed in.
     */
    public final String encode(String chars) {
        return encode(chars, false);
    }

    /**
     * This method encodes the string passed in and returns the encoded string.
     *
     * @param chars
     *            The plain text string that should be encode
     * @param includeBasicChars
     *            should be set to true if you wish to also encode \32 through \127
     *
     * @return The encoded version of the string passed in.
     */
    public final String encode(String chars, boolean includeBasicChars) {
        if (includeBasicChars) {
            addBasicChars();
        }
        //StringTokenizer tokenizer = new StringTokenizer(chars, "");
        char[] charArray = chars.toCharArray();
        StringBuilder buffer = new StringBuilder();
        for (char element : charArray) {
            //while (tokenizer.hasMoreElements()){
            String currentChar = "" + element;//tokenizer.nextToken();
            try {
                buffer.append(entityLookup.get(currentChar));
            } catch (NullPointerException e) {
                buffer.append(currentChar);
            }
        }
        return buffer.toString();
    }

    /**
     * Set up the HashMap that contains the converstion Data.
     */
    private void initialize() {
        entityLookup.put("\t", "&#09;");
        entityLookup.put("\n", "&#10;");
        entityLookup.put(System.getProperty("line.separator"), "&#10;");
        entityLookup.put("\r", "&#13;");
        entityLookup.put("<", "&#60;");
        entityLookup.put(">", "&#62;");
        for (int index = 11; index < 32; index++) {
            String aChar = String.valueOf((char) index);
            entityLookup.put(aChar, "&#0" + index + ";");
        }
        for (int index = 128; index < 256; index++) {
            String aChar = String.valueOf((char) index);
            entityLookup.put(aChar, "&#" + index + ";");
        }
        entityLookup.put("\u00A1", "&iexcl;");
        entityLookup.put("\u00A2", "&cent;");
        entityLookup.put("\u00A3", "&pound;");
        entityLookup.put("\u00A4", "&curren;");
        entityLookup.put("\u00A5", "&yen;");
        entityLookup.put("\u00A6", "&brvbar;");
        entityLookup.put("\u00A7", "&sect;");
        entityLookup.put("\u00A8", "&uml;");
        entityLookup.put("\u00A9", "&copy;");
        entityLookup.put("\u00AA", "&ordf;");
        entityLookup.put("\u00AB", "&laquo;");
        entityLookup.put("\u00AC", "&not;");
        entityLookup.put("\u00AD", "&shy;");
        entityLookup.put("\u00AE", "&reg;");
        entityLookup.put("\u00AF", "&macr;");
        entityLookup.put("\u00B0", "&deg;");
        entityLookup.put("\u00B1", "&plusmn;");
        entityLookup.put("\u00B2", "&sup2;");
        entityLookup.put("\u00B3", "&sup3;");
        entityLookup.put("\u00B4", "&acute;");
        entityLookup.put("\u00B5", "&micro;");
        entityLookup.put("\u00B6", "&para;");
        entityLookup.put("\u00B7", "&middot;");
        entityLookup.put("\u00B8", "&cedil;");
        entityLookup.put("\u00B9", "&sup1;");
        entityLookup.put("\u00BA", "&ordm;");
        entityLookup.put("\u00BB", "&raquo;");
        entityLookup.put("\u00BC", "&frac14;");
        entityLookup.put("\u00BD", "&frac12;");
        entityLookup.put("\u00BE", "&frac34;");
        entityLookup.put("\u00BF", "&iquest;");
        entityLookup.put("\u00C0", "&Agrave;");
        entityLookup.put("\u00C1", "&Aacute;");
        entityLookup.put("\u00C2", "&Acirc;");
        entityLookup.put("\u00C3", "&Atilde;");
        entityLookup.put("\u00C4", "&Auml;");
        entityLookup.put("\u00C5", "&Aring;");
        entityLookup.put("\u00C6", "&AElig;");
        entityLookup.put("\u00C7", "&Ccedil;");
        entityLookup.put("\u00C8", "&Egrave;");
        entityLookup.put("\u00C9", "&Eacute;");
        entityLookup.put("\u00CA", "&Ecirc;");
        entityLookup.put("\u00CB", "&Euml;");
        entityLookup.put("\u00CC", "&Igrave;");
        entityLookup.put("\u00CD", "&Iacute;");
        entityLookup.put("\u00CE", "&Icirc;");
        entityLookup.put("\u00CF", "&Iuml;");
        entityLookup.put("\u00D0", "&ETH;");
        entityLookup.put("\u00D1", "&Ntilde;");
        entityLookup.put("\u00D2", "&Ograve;");
        entityLookup.put("\u00D3", "&Oacute;");
        entityLookup.put("\u00D4", "&Ocirc;");
        entityLookup.put("\u00D5", "&Otilde;");
        entityLookup.put("\u00D6", "&Ouml;");
        entityLookup.put("\u00D7", "&times;");
        entityLookup.put("\u00D8", "&Oslash;");
        entityLookup.put("\u00D9", "&Ugrave;");
        entityLookup.put("\u00DA", "&Uacute;");
        entityLookup.put("\u00DB", "&Ucirc;");
        entityLookup.put("\u00DC", "&Uuml;");
        entityLookup.put("\u00DD", "&Yacute;");
        entityLookup.put("\u00DE", "&THORN;");
        entityLookup.put("\u00DF", "&szlig;");
        entityLookup.put("\u00E0", "&agrave;");
        entityLookup.put("\u00E1", "&aacute;");
        entityLookup.put("\u00E2", "&acirc;");
        entityLookup.put("\u00E3", "&atilde;");
        entityLookup.put("\u00E4", "&auml;");
        entityLookup.put("\u00E5", "&aring;");
        entityLookup.put("\u00E6", "&aelig;");
        entityLookup.put("\u00E7", "&ccedil;");
        entityLookup.put("\u00E8", "&egrave;");
        entityLookup.put("\u00E9", "&eacute;");
        entityLookup.put("\u00EA", "&ecirc;");
        entityLookup.put("\u00EB", "&euml;");
        entityLookup.put("\u00EC", "&igrave;");
        entityLookup.put("\u00ED", "&iacute;");
        entityLookup.put("\u00EE", "&icirc;");
        entityLookup.put("\u00EF", "&iuml;");
        entityLookup.put("\u00F0", "&eth;");
        entityLookup.put("\u00F1", "&ntilde;");
        entityLookup.put("\u00F2", "&ograve;");
        entityLookup.put("\u00F3", "&oacute;");
        entityLookup.put("\u00F4", "&ocirc;");
        entityLookup.put("\u00F5", "&otilde;");
        entityLookup.put("\u00F6", "&ouml;");
        entityLookup.put("\u00F7", "&divid;");
        entityLookup.put("\u00F8", "&oslash;");
        entityLookup.put("\u00F9", "&ugrave;");
        entityLookup.put("\u00FA", "&uacute;");
        entityLookup.put("\u00FB", "&ucirc;");
        entityLookup.put("\u00FC", "&uuml;");
        entityLookup.put("\u00FD", "&yacute;");
        entityLookup.put("\u00FE", "&thorn;");
        entityLookup.put("\u00FF", "&yuml;");
        entityLookup.put("\u0080", "&euro;");
    }

    /**
     * adds through \u007E to the entityLookup HashMap.
     */
    public final void addBasicChars() {
        /**
         * &#032; space through \u007E &#126; ~ Tilde int entityCounter=32; for (char c = '\u0020'; c <= '\u007E'; c++){ entityLookup.put(c + "", new String("&#" +
         * entityCounter+";")); entityCounter++; }
         */
        for (int index = 32; index < 128; index++) {
            String aChar = String.valueOf((char) index);
            entityLookup.put(aChar, "&#" + index + ";");
        }
        createCharLookup();
    }

    /**
     * builds the CharLookup HashMap from the entityLookup HashMap.
     */
    private void createCharLookup() {
        Set<String> keySet = entityLookup.keySet();
        for (String aKeySet : keySet) {
            Object currentKey = entityLookup.get(aKeySet);
            charLookup.put(entityLookup.get(currentKey), currentKey);
        }
    }
}