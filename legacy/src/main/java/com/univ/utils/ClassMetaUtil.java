package com.univ.utils;

import java.util.Hashtable;

import com.univ.objetspartages.om.ClassMeta;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.processus.SaisieFiche;

/**
 * Classe d'accès aux méta-données des classes.
 * @deprecated cette classe servait à récupérer les noms des champs de la BDD pour code rattachement / code structure qui était différent en V4.
 * ce code n'a plus de sens depuis 2005
 */
@Deprecated
public class ClassMetaUtil {

    /** The h class meta. */
    private static Hashtable<String, String> hClassMeta = null;

    /**
     * Gets the objet class meta default value.
     *
     * @param metaName
     *            the meta name
     *
     * @return the objet class meta default value
     */
    private static String getObjetClassMetaDefaultValue(String metaName) {
        if (hClassMeta == null) {
            hClassMeta = new Hashtable<>();
            hClassMeta.put("NOM_CODE_RATTACHEMENT", "CODE_RATTACHEMENT");
            // JSS 20051031 : rattachement secondaires
            hClassMeta.put("NOM_CODE_RATTACHEMENT_AUTRES", "CODE_RATTACHEMENT_AUTRES");
            hClassMeta.put("NOM_CODE_RUBRIQUE", "CODE_RUBRIQUE");
        }
        return hClassMeta.get(metaName);
    }

    /**
     * Gets the processus class meta default value.
     *
     * @param metaName
     *            the meta name
     *
     * @return the processus class meta default value
     */
    private static String getProcessusClassMetaDefaultValue(String metaName) {
        if (hClassMeta == null) {
            hClassMeta = new Hashtable<>();
            hClassMeta.put("NOM_CODE_RATTACHEMENT", "CODE_RATTACHEMENT");
            // JSS 20051031 : rattachement secondaires
            hClassMeta.put("NOM_CODE_RATTACHEMENT_AUTRES", "CODE_RATTACHEMENT_AUTRES");
            hClassMeta.put("NOM_CODE_RUBRIQUE", "CODE_RUBRIQUE");
        }
        return hClassMeta.get(metaName);
    }

    /**
     * Gets the class meta value.
     *
     * @param ficheUniv
     *            the fiche univ
     * @param metaName
     *            the meta name
     *
     * @return the class meta value
     */
    public static String getClassMetaValue(FicheUniv ficheUniv, String metaName) {
        String value = null;
        if (ficheUniv instanceof ClassMeta) {
            value = ((ClassMeta) ficheUniv).getClassMetaValue(metaName);
        }
        if (value == null) {
            value = getObjetClassMetaDefaultValue(metaName);
        }
        return value;
    }

    /**
     * Gets the class meta value.
     *
     * @param processus
     *            the processus
     * @param metaName
     *            the meta name
     *
     * @return the class meta value
     */
    public static String getClassMetaValue(SaisieFiche processus, String metaName) {
        String value = null;
        if (processus instanceof ClassMeta) {
            value = ((ClassMeta) processus).getClassMetaValue(metaName);
        }
        if (value == null) {
            value = getProcessusClassMetaDefaultValue(metaName);
        }
        return value;
    }
}
