package com.univ.objetspartages.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.univ.objetspartages.bean.AbstractRestrictionFicheBean;

/**
 * Abstraction permettant de mapper en plus des champs de {@link CommonFicheUnivRowMapper} les champs de diffusion selective.
 * À savoir : DIFFUSION_PUBLIC_VISE, DIFFUSION_MODE_RESTRICTION et DIFFUSION_PUBLIC_VISE_RESTRICTION
 */
public abstract class RestrictionFicheUnivRowMapper<T extends AbstractRestrictionFicheBean> extends CommonFicheUnivRowMapper<T> {

    @Override
    public T mapRow(ResultSet rs, int rowNum) throws SQLException {
        T ficheBean = super.mapRow(rs, rowNum);
        ficheBean.setDiffusionPublicVise(rs.getString("DIFFUSION_PUBLIC_VISE"));
        ficheBean.setDiffusionModeRestriction(rs.getString("DIFFUSION_MODE_RESTRICTION"));
        ficheBean.setDiffusionPublicViseRestriction(rs.getString("DIFFUSION_PUBLIC_VISE_RESTRICTION"));
        return ficheBean;
    }
}
