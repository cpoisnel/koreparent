package com.univ.objetspartages.dao.mapper.utils;

import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.bean.AbstractRestrictionFicheBean;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.sql.Types;

/**
 * Created by olivier.camon on 13/04/15.
 */
public class FicheUnivParameterSourceUtils {

    public static MapSqlParameterSource getParamsFromFicheBean(AbstractFicheBean ficheBean) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", ficheBean.getId(), Types.BIGINT);
        parameterSource.addValue("code", ficheBean.getCode(), Types.VARCHAR);
        parameterSource.addValue("codeRubrique", ficheBean.getCodeRubrique(), Types.VARCHAR);
        parameterSource.addValue("codeRattachement", ficheBean.getCodeRattachement(), Types.VARCHAR);
        parameterSource.addValue("metaKeywords", ficheBean.getMetaKeywords(), Types.LONGVARCHAR);
        parameterSource.addValue("metaDescription", ficheBean.getMetaDescription(), Types.LONGVARCHAR);
        parameterSource.addValue("titreEncadre", ficheBean.getTitreEncadre(), Types.VARCHAR);
        parameterSource.addValue("contenuEncadre", ficheBean.getContenuEncadre(), Types.LONGVARCHAR);
        parameterSource.addValue("encadreRecherche", ficheBean.getEncadreRecherche(), Types.VARCHAR);
        parameterSource.addValue("encadreRechercheBis", ficheBean.getEncadreRechercheBis(), Types.VARCHAR);
        parameterSource.addValue("dateAlerte", ficheBean.getDateAlerte(), Types.DATE);
        parameterSource.addValue("messageAlerte", ficheBean.getMessageAlerte(), Types.LONGVARCHAR);
        parameterSource.addValue("dateCreation", SqlDateConverter.toTimestamp(ficheBean.getDateCreation()), Types.TIMESTAMP);
        parameterSource.addValue("dateProposition", SqlDateConverter.toTimestamp(ficheBean.getDateProposition()), Types.TIMESTAMP);
        parameterSource.addValue("dateValidation", SqlDateConverter.toTimestamp(ficheBean.getDateValidation()), Types.TIMESTAMP);
        parameterSource.addValue("dateModification", SqlDateConverter.toTimestamp(ficheBean.getDateModification()), Types.TIMESTAMP);
        parameterSource.addValue("codeRedacteur", ficheBean.getCodeRedacteur(), Types.VARCHAR);
        parameterSource.addValue("codeValidation", ficheBean.getCodeValidation(), Types.VARCHAR);
        parameterSource.addValue("langue", ficheBean.getLangue(), Types.VARCHAR);
        parameterSource.addValue("etatObjet", ficheBean.getEtatObjet(), Types.VARCHAR);
        parameterSource.addValue("nbHits", ficheBean.getNbHits(), Types.BIGINT);
        return parameterSource;
    }

    public static MapSqlParameterSource getParamFromRestrictionFicheBean(AbstractRestrictionFicheBean ficheBean) {
        MapSqlParameterSource parameterSource = getParamsFromFicheBean(ficheBean);
        parameterSource.addValue("diffusionPublicVise", ficheBean.getDiffusionPublicVise(), Types.VARCHAR);
        parameterSource.addValue("diffusionModeRestriction", ficheBean.getDiffusionModeRestriction(), Types.LONGVARCHAR);
        parameterSource.addValue("diffusionPublicViseRestriction", ficheBean.getDiffusionPublicViseRestriction(), Types.LONGVARCHAR);
        return parameterSource;
    }
}
