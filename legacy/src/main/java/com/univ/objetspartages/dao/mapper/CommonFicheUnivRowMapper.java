package com.univ.objetspartages.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.univ.objetspartages.bean.AbstractFicheBean;

/**
 * Abstraction permettant de mapper les champs communs des fiches.
 * L'ensemble des champs mapper sont :
 * CODE, CODE_RATTACHEMENT, META_KEYWORDS, META_DESCRIPTION, TITRE_ENCADRE, CONTENU_ENCADRE, ENCADRE_RECHERCHE, ENCADRE_RECHERCHE_BIS, DATE_ALERTE, MESSAGE_ALERTE
 * DATE_CREATION, DATE_PROPOSITION, DATE_VALIDATION, DATE_MODIFICATION, CODE_REDACTEUR, CODE_VALIDATION, LANGUE, ETAT_OBJET, NB_HITS
 */
public abstract class CommonFicheUnivRowMapper<T extends AbstractFicheBean> implements RowMapper<T> {

    @Override
    public T mapRow(ResultSet rs, int rowNum) throws SQLException {
        T ficheBean = mapRowFiche(rs, rowNum);
        ficheBean.setCode(rs.getString("CODE"));
        ficheBean.setCodeRubrique(rs.getString("CODE_RUBRIQUE"));
        ficheBean.setCodeRattachement(rs.getString("CODE_RATTACHEMENT"));
        ficheBean.setMetaKeywords(rs.getString("META_KEYWORDS"));
        ficheBean.setMetaDescription(rs.getString("META_DESCRIPTION"));
        ficheBean.setTitreEncadre(rs.getString("TITRE_ENCADRE"));
        ficheBean.setContenuEncadre(rs.getString("CONTENU_ENCADRE"));
        ficheBean.setEncadreRecherche(rs.getString("ENCADRE_RECHERCHE"));
        ficheBean.setEncadreRechercheBis(rs.getString("ENCADRE_RECHERCHE_BIS"));
        ficheBean.setDateAlerte(SqlDateConverter.fromDate(rs.getDate("DATE_ALERTE")));
        ficheBean.setMessageAlerte(rs.getString("MESSAGE_ALERTE"));
        ficheBean.setDateCreation(SqlDateConverter.fromTimestamp(rs.getTimestamp("DATE_CREATION")));
        ficheBean.setDateProposition(SqlDateConverter.fromTimestamp(rs.getTimestamp("DATE_PROPOSITION")));
        ficheBean.setDateValidation(SqlDateConverter.fromTimestamp(rs.getTimestamp("DATE_VALIDATION")));
        ficheBean.setDateModification(SqlDateConverter.fromTimestamp(rs.getTimestamp("DATE_MODIFICATION")));
        ficheBean.setCodeRedacteur(rs.getString("CODE_REDACTEUR"));
        ficheBean.setCodeValidation(rs.getString("CODE_VALIDATION"));
        ficheBean.setLangue(rs.getString("LANGUE"));
        ficheBean.setEtatObjet(rs.getString("ETAT_OBJET"));
        ficheBean.setNbHits(rs.getLong("NB_HITS"));
        return ficheBean;
    }

    public abstract T mapRowFiche(ResultSet rs, int rowNum) throws SQLException;
}
