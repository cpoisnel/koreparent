package com.univ.objetspartages.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.ContexteUniv;

/**
 * Classe gérant les ancines caches des rubriques lorsqu'ils étaient chargé au lancement de l'application.
 * @deprecated cette classe n'est plus utilisé dans le produit, le cache ne passe plus par les objets {@link InfosRubriques}, il est
 * maintenant géré directement sur le {@link ServiceRubrique}. Les Objets en cache sont des {@link RubriqueBean} et plus des {@link InfosRubriques}
 */
@Deprecated
public class InfosRubriquesUtils {

    /**
     * Récupération d'une rubrique stockée en mémoire.
     *
     * @param code
     *            the code
     *
     * @return the infos rubriques
     * @deprecated Les {@link InfosRubriques} sont dépréciés, il faut utiliser {@link ServiceRubrique#getRubriqueByCode(String)}
     */
    @Deprecated
    public static InfosRubriques renvoyerItemRubrique(final String code) {
        InfosRubriques res = new InfosRubriques("");
        if (StringUtils.isNotBlank(code)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(code);
            if (rubriqueBean != null) {
                res = new InfosRubriques(rubriqueBean);
            }
        }
        return res;
    }

    /**
     *
     * @param listeGroupes
     * @param codeRubriqueFiche
     * @return
     * @deprecated Les {@link InfosRubriques} sont dépréciés, il faut utiliser {@link ServiceRubrique#controlerRestrictionRubrique(Set, String)}
     */
    @Deprecated
    public static boolean controlerRestrictionRubrique(Set<String> listeGroupes, final String codeRubriqueFiche) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        return serviceRubrique.controlerRestrictionRubrique(listeGroupes, codeRubriqueFiche);
    }

    /**
     * Retourne une "fausse" rubrique contenant le code {@link ServiceRubrique#CODE_RUBRIQUE_ROOT}.
     * @deprecated cette méthode ne doit plus être utiliser. Il faut passer par {@link ServiceRubrique}. La rubrique root n'a  plus de sens.
     * @return une fausse rubrique représentant la rubrique de niveau 0
     */
    @Deprecated
    public static InfosRubriques getTopLevelRubrique() {
        return new InfosRubriques(ServiceRubrique.CODE_RUBRIQUE_ROOT);
    }

    /**
     * Renvoie la liste des rubriques principales (premier niveau) triée en fonction de l'ordre.
     *
     * @param allGroups
     *            l'ensemble des groupes de l'utilisateur
     *
     * @return Une liste d'InfosRubrique
     * @deprecated cette méthode est utilisé que dans des méthodes dépréciées. Utilisez {@link ServiceRubrique#getRubriqueByCodeParentFront(String, Set)}
     * avec une chaine vide en paramètre et les groupes de l'utilisateur en seconds paramètre
     */
    @Deprecated
    public static Collection<InfosRubriques> getListeRubriquesPrincipaleFront(Set<String> allGroups) {
        final InfosRubriques topLevelRubrique = InfosRubriquesUtils.getTopLevelRubrique();
        return topLevelRubrique.getListeSousRubriquesFront(allGroups);
    }

    /**
     * Renvoie la liste des rubriques principales (premier niveau) triée en fonction de l'ordre (méthode back, pas de dsi).
     *
     * @return Une liste d'InfosRubrique
     * @deprecated cette méthode est utilisé que dans des méthodes dépréciées. Utilisez {@link ServiceRubrique#getRubriqueByCodeParent(String)} avec une chaine vide en paramètre
     */
    @Deprecated
    public static Collection<InfosRubriques> getListeRubriquesPrincipaleBack() {
        final InfosRubriques topLevelRubrique = InfosRubriquesUtils.getTopLevelRubrique();
        return topLevelRubrique.getListeSousRubriques();
    }

    /**
     * Nouvelle méthode avec juste les paramètres dont on a besoin...
     *
     * @param codeRubrique
     *            le code de la rubrique dont on veut l'intitulé
     * @return l'intitulé de la rubrique ou vide si elle n'existe pas
     * @deprecated ne plus utiliser cette méthode, il faut passer par {@link ServiceRubrique#getRubriqueByCode(String)} pour récupérer
     * le label d'une rubrique.
     */
    @Deprecated
    public static String getIntitule(final String codeRubrique) {
        String intitule = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
        if (StringUtils.isNotBlank(codeRubrique)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (rubriqueBean != null) {
                intitule = rubriqueBean.getIntitule();
            }
        }
        return intitule;
    }

    /**
     * Gets the fil ariane.
     *
     * @param codeRubrique
     *            the code rubrique
     * @param separateur
     *            the separateur
     *
     * @return the fil ariane
     * @deprecated utiliser {@link ServiceRubrique#getLabelWithAscendantsLabels(String)}
     */
    @Deprecated
    public static String getFilAriane(final String codeRubrique, String separateur) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        return serviceRubrique.getLabelWithAscendantsLabels(codeRubrique);
    }

    /**
     * Récupération de l'intitulé complet (Concaténation mere + fille 1 niveau + ...)
     *
     * @param code
     *            le code de la rubrique à partir du quel on doit partir
     * @param inclureRubriqueCourante
     *            Doit on inclure la rubrique courante dans la chaine générée?
     *
     * @return L'intitulé de toute les rubriques jusqu'a la racine
     * @deprecated utiliser {@link ServiceRubrique#getLabelWithAscendantsLabels(String)} si  inclureRubriqueCourante est true sinon {@link ServiceRubrique#getAscendantsLabelOnly(String)}
      */
    @Deprecated
    public static String getIntituleComplet(final String code, final boolean inclureRubriqueCourante) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        String res = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(code)) {
            if (inclureRubriqueCourante) {
                res = serviceRubrique.getLabelWithAscendantsLabels(code);
            } else {
                res = serviceRubrique.getAscendantsLabelOnly(code);
            }
        }
        return res;
    }

    /**
     * Forcage du rechargement des rubriques en mémoire.
     * @deprecated cette méthode ne rafraichie plus les caches
     */
    @Deprecated
    public static void refresh() {
        //ne fait plus rien
    }

    /**
     * Récupération des rubriques.
     *
     * @return la liste rubriques par intitule complet
     * @ðeprecated ne pas utiliser, cette méthode retourne une map contenant l'ensemble des rubriques de l'applications... c'est un peu trop...
     */
    @Deprecated
    public static Map<String, String> getListeRubriquesParIntituleComplet() {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final List<RubriqueBean> allRubrique = serviceRubrique.getAllRubriques();
        final Map<String, String> res = new HashMap<>(allRubrique.size());
        for (final RubriqueBean rubrique : allRubrique) {
            res.put(rubrique.getCode(), serviceRubrique.getLabelWithAscendantsLabels(rubrique));
        }
        return res;
    }

    /**
     * Cette méthode renvoit une rubrique de la fiche dans le site courant
     * par defaut la rubrique principale ou la première de ces rubriques de publication
     * si précisé, on cherchera en priorité la rubrique incluse dans la rubrique courante sinon on renverra celle par défaut
     * @param ctx
     *            the ctx
     * @param infosRubriqueCourante
     *            the infos rubrique courante
     * @param lstCodeRubriquePubliable
     *            the lst code rubrique publiable
     * @param inRubriqueCourante
     *            the _in rubrique courante
     *
     * @return the rubrique publication
     * @deprecated {@link InfosRubriques} est dépréciés, utilisez {@link ServiceRubrique#getRubriquePublication(OMContext, String, List, boolean)} si vous avez uniquement un {@link InfosRubriques}
     * ou son code. Si vous avez un {@link RubriqueBean} utilisez {@link ServiceRubrique#getRubriquePublication(OMContext, RubriqueBean, List, boolean)}
     */
    @Deprecated
    public static String getRubriquePublication(final OMContext ctx, final InfosRubriques infosRubriqueCourante, final List<String> lstCodeRubriquePubliable, final boolean inRubriqueCourante) {
        final InfosSite siteCourant = ctx.getInfosSite();
        boolean controlDsi = false;
        boolean rubriqueAutorisee = false;
        boolean possedeRubrique = false;
        if (ctx instanceof ContexteUniv) {
            controlDsi = true;
        }
        String codeRubrique = null;
        for (final String aLstCodeRubriquePubliable : lstCodeRubriquePubliable) {
            possedeRubrique = true;
            final InfosRubriques rubrique = InfosRubriquesUtils.renvoyerItemRubrique(aLstCodeRubriquePubliable);
            // si la rubrique n'existe pas/plus on passe
            if (StringUtils.isEmpty(rubrique.getCode())) {
                continue;
            }
            if (!controlDsi || InfosRubriquesUtils.controlerRestrictionRubrique(((ContexteUniv) ctx).getGroupesDsiAvecAscendants(), rubrique.getCode())) {
                rubriqueAutorisee = true;
                if (siteCourant.isRubriqueVisibleInSite(rubrique)) {
                    if (!inRubriqueCourante) {
                        return rubrique.getCode();
                    }
                    if (codeRubrique == null) {
                        codeRubrique = rubrique.getCode();
                    }
                    if (rubrique.getCode().equals(infosRubriqueCourante.getCode()) || infosRubriqueCourante.getListeSousRubriquesTousNiveaux().contains(rubrique)) {
                        return rubrique.getCode();
                    }
                }
            }
        }
        if (controlDsi && possedeRubrique && !rubriqueAutorisee) {
            ((ContexteUniv) ctx).setCodeRubriquePageCourante(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE);
        }
        return codeRubrique;
    }

    /**
     * Détermine de facon récursive la liste des rubriques autorisées pour un utilisateur donné.
     *
     * @param allGroups
     *            contexte pour la base et les autorisations du user
     * @param rubrique
     *            the rubrique
     *
     * @return the collection
     *
     */
    @Deprecated
    public static Collection<InfosRubriques> determinerListeSousRubriquesAutorisees(final Set<String> allGroups, InfosRubriques rubrique) {
        final Collection<InfosRubriques> listeRubriquesAutorisees = new HashSet<>();
        if (rubrique == null || StringUtils.isEmpty(rubrique.getCode())) {
            rubrique = InfosRubriquesUtils.getTopLevelRubrique();
        }
        determinerListeSousRubriquesAutorisees(allGroups, rubrique, listeRubriquesAutorisees);
        return listeRubriquesAutorisees;
    }

    /**
     * Détermine la liste des rubriques autorisées pour un utilisateur donné.
     *
     * @param listeGroupes
     *            the liste groupes
     * @param rubrique
     *            the rubrique
     * @param listeRecursive
     *            the liste recursive
     *
     */
    public static void determinerListeSousRubriquesAutorisees(final Set<String> listeGroupes, final InfosRubriques rubrique, final Collection<InfosRubriques> listeRecursive) {
        final Collection<InfosRubriques> listeSousRubriques = rubrique.getListeSousRubriques();
        // on boucle sur les rubriques
        for (final InfosRubriques rubriqueCourante : listeSousRubriques) {
            // JSS 20051013 : bug sur récursivité si pas de DSI
            boolean autorisationRubrique = true;
            // on boucle sur les groupes de restriction de la rubrique
            final Iterator<String> iter2 = rubriqueCourante.getGroupesDsi().iterator();
            if (iter2.hasNext()) {
                autorisationRubrique = false;
                while (iter2.hasNext()) {
                    // si l'utilisateur appartient a au moins un groupe de restriction on boucle recursivement
                    if (listeGroupes.contains(iter2.next())) {
                        autorisationRubrique = true;
                    }
                }
            }
            if (autorisationRubrique) {
                listeRecursive.add(rubriqueCourante);
                if (!rubriqueCourante.getListeSousRubriques().isEmpty()) {
                    determinerListeSousRubriquesAutorisees(listeGroupes, rubriqueCourante, listeRecursive);
                }
            }
        }
    }
}
