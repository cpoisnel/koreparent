package com.univ.objetspartages.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.InfosGroupeDsi;
import com.univ.objetspartages.om.InfosRequeteGroupe;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.services.ServiceGroupeDsi;

/**
 * Classe contenant l'ensemble des méthodes traitant des {@link InfosGroupeDsi} cette classe est amené à être déprécié
 * lors de la maj de la gestion des caches de kportal
 * @deprecated Utiliser {@link com.univ.objetspartages.bean.GroupeDsiBean} et {@link com.univ.objetspartages.services.ServiceGroupeDsi}
 */
@Deprecated
public final class InfosGroupeDsiUtils {

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByCode} pour récupérer un groupe particulier ou {@link com.univ.objetspartages.services.ServiceGroupeDsi#getAll}
     */
    @Deprecated
    private static Map<String, InfosGroupeDsi> getListeGroupesDsi() {
        final Map<String, InfosGroupeDsi> map = new HashMap<>();
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        for (GroupeDsiBean groupeDsiBean : serviceGroupeDsi.getAll()) {
            map.put(groupeDsiBean.getCode(), new InfosGroupeDsi(groupeDsiBean));
        }
        return map;
    }

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getGroupRequestList}
     */
    @Deprecated
    public static Map<String, InfosRequeteGroupe> getListeRequetesGroupes() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getGroupRequestList();
    }

    /**
     * Récupération d'une requête stockée en mémoire par son code
     *
     * @param code
     *            le code de l'{@link InfosRequeteGroupe} recherché
     *
     * @return l'{@link InfosRequeteGroupe} correspond à un groupe ou un objet vide si non trouvé.
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getGroupRequestList()}
     */
    @Deprecated
    public static InfosRequeteGroupe renvoyerItemRequeteGroupe(final String code) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        InfosRequeteGroupe res = serviceGroupeDsi.getGroupRequestList().get(code);
        if (res == null) {
            res = new InfosRequeteGroupe();
        }
        return res;
    }

    /**
     * Récupération d'un groupe stockée en mémoire.
     *
     * @param code le code du groupe
     *
     * @return l'infos groupe dsi récupéré du cache ou un {@link InfosGroupeDsi} vide si non trouvé...
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByCode}
     */
    @Deprecated
    public static InfosGroupeDsi renvoyerItemGroupeDsi(final String code) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final GroupeDsiBean groupeDsiBean = serviceGroupeDsi.getByCode(code);
        /* groupe vide pour éviter les plantages */
        if (groupeDsiBean == null) {
            return new InfosGroupeDsi();
        }
        return new InfosGroupeDsi(groupeDsiBean);
    }

    /**
     * Récupération des groupes dynamiques.
     *
     * @return the liste groupes dynamiques
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getAllDynamics}
     */
    @Deprecated
    public static List<InfosGroupeDsi> getListeGroupesDynamiques() {
        final List<InfosGroupeDsi> res = new ArrayList<>();
        for (final InfosGroupeDsi info : getListeGroupesDsi().values()) {
            if (StringUtils.isNotBlank(info.getRequete())) {
                res.add(info);
            }
        }
        return res;
    }

    /**
     * Récupération de l'intitulé : gestion de plusieurs codes.(pour retro compat...)
     * @param codes une liste de code séparé par des ";" (malin n'est ce pas?)
     * @return l'intitule si il y en a plusieurs ils sont séparés pa rdes ";" (toujours malin n'est ce pas?)
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getIntitules(String)}
     */
    @Deprecated
    public static String getIntitules(final String codes) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getIntitules(codes);
    }

    /**
     * Récupération de l'intitulé : gestion d'un seule code...
     * @param code le code du groupe dont on cherche l'intitule
     * @return l'intitule du groupe si il est trouvé, sinon BO_GROUPE_INEXISTANT. Enfin si le code est vide ou null, on retourne une chaine vide.
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getIntitule(String)}
     */
    @Deprecated
    public static String getIntitule(final String code) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getIntitule(code);
    }

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getBreadCrumbsAsList}
     */
    @Deprecated
    public static List<String> getFilAriane(final String codeGroupe) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getBreadCrumbsAsList(codeGroupe);
    }

    /**
     * Gets the fil ariane.
     *
     * @param codeGroupe
     *            the code groupe
     * @param separateur
     *            the separateur
     *
     * @return the fil ariane
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getBreadCrumbs}
     */
    @Deprecated
    public static String getFilAriane(final String codeGroupe, String separateur) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getBreadCrumbs(codeGroupe, separateur);
    }

    /**
     * Récupération pour affichage liste des requêtes.
     *
     * @return une Map contenant l'alias et l'intitulé du groupe contenu dans le cache
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getListeRequetesGroupesPourAffichage()}
     */
    @Deprecated
    public static Map<String, String> getListeRequetesGroupesPourAffichage() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getListeRequetesGroupesPourAffichage();
    }

    /**
     * Renvoie le groupe dsi de plus haut niveau (père des groupes de 1er niveau).
     *
     * @return Un InfosGroupeDsi
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByCode(String)}
     */
    @Deprecated
    private static InfosGroupeDsi getTopLevelGroupedsi() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return new InfosGroupeDsi(serviceGroupeDsi.getByCode(ServiceGroupeDsi.CODE_GROUPE_ROOT));
    }

    /**
     * Renvoie la liste des infos des groupes pour un type.
     *
     * @param type
     *            the _type
     *
     * @return the vector
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByType}
     */
    @Deprecated
    public static List<InfosGroupeDsi> renvoyerCodesGroupeDsiParType(final String type) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final List<InfosGroupeDsi> res = new ArrayList<>();
        for (final GroupeDsiBean info : serviceGroupeDsi.getByType(type)) {
            res.add(new InfosGroupeDsi(info));
        }
        return res;
    }

    /**
     * Renvoie la liste des {groupes,périmètres} associé à un role et pour un perimetre.
     *
     * @param role
     *            the role
     * @param codesStructures
     *            the codes structures
     * @param codeRubrique
     *            the _code rubrique
     * @param publicsVises
     *            the _publics vises
     * @param codeEspaceCollaboratif
     *            the _code espace collaboratif
     *
     * @return the hashtable
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#renvoyerGroupesEtPerimetres(String, java.util.List, String, String, String)}
     */
    @Deprecated
    public static Map<String, List<Perimetre>> renvoyerGroupesEtPerimetres(final String role, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.renvoyerGroupesEtPerimetres(role, codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif);
    }

    /**
     * méthode qui permet de tester le périmètre de modification d'un objet sur un groupe dsi.
     *
     * @param autorisations
     *            the autorisations
     * @param sPermission
     *            the s permission
     * @param codeGroupe
     *            the code groupe
     *
     * @return true, if controler permission
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#controlerPermission(AutorisationBean, String, String)}
     */
    @Deprecated
    public static boolean controlerPermission(final AutorisationBean autorisations, final String sPermission, final String codeGroupe) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.controlerPermission(autorisations, sPermission, codeGroupe);
    }
}
