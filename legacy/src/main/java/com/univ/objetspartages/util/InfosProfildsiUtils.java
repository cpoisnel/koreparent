package com.univ.objetspartages.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.om.InfosProfilDsi;
import com.univ.objetspartages.services.ServiceProfildsi;

/**
 * Classe amener à disparaitre lors de la refonte du cache de l'application.
 * Elle permet de gérer le cache sur les profils avec les objest {@link InfosProfilDsi}
 */
public class InfosProfildsiUtils {

    /**
     * @deprecated Utiliser les méthodes du service {@link com.univ.objetspartages.services.ServiceProfildsi}. /!\ Cette méthode ne retourne désormais qu'une map vide /!\
     */
    @Deprecated
    private static Map<String, InfosProfilDsi> getListeInfosProfilDsi() {
        return new HashMap<>();
    }


    /**
     * Récupération d'un profil stocké en mémoire.
     *
     * @param code
     *            the code
     *
     * @return the infos profil dsi
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#getByCode(String)}
     */
    @Deprecated
    public static InfosProfilDsi renvoyerItemProfilDsi(String code) {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        final ProfildsiBean profildsiBean = serviceProfildsi.getByCode(code);
        if(profildsiBean != null) {
            return new InfosProfilDsi(profildsiBean);
        } else {
            return new InfosProfilDsi();
        }
    }

    /**
     * Renvoyer profils groupes.
     *
     * @param groupesUtilisateur
     *            the _groupes utilisateur
     *
     * @return the vector
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#renvoyerProfilsGroupes(java.util.Collection)}
     */
    @Deprecated
    public static List<String> renvoyerProfilsGroupes(Collection<String> groupesUtilisateur) {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        return serviceProfildsi.renvoyerProfilsGroupes(groupesUtilisateur);
    }


    /**
     * Renvoie la liste de tous les profils DSI (code+libellé) Peut être affiché directement dans une Combo.
     *
     * @return the liste profils dsi
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#getDisplayableProfiles()}
     */
    @Deprecated
    public static Map<String, String> getListeProfilsDSI() {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        return serviceProfildsi.getDisplayableProfiles();
    }

    /**
     * Renvoie la liste de tous les profils pour une liste de groupes.
     *
     * @param groupes
     *            the _groupes
     *
     * @return the liste profils dsi par groupes
     * @deprecated Utiliser le service {@link com.univ.objetspartages.services.ServiceProfildsi#getListeProfilsDSIParGroupes(java.util.Collection)}
     */
    @Deprecated
    public static Map<String, String> getListeProfilsDSIParGroupes(Collection<String> groupes) {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        return serviceProfildsi.getListeProfilsDSIParGroupes(groupes);
    }

    /**
     * Récupération de l'intitulé.
     *
     * @param code
     *            the code
     *
     * @return the intitule
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#getDisplayableLabel(String)}
     */
    @Deprecated
    public static String getIntitule(final String code) {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        return serviceProfildsi.getDisplayableLabel(code);
    }
}
