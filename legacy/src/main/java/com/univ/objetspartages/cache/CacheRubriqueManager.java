package com.univ.objetspartages.cache;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cache.AbstractCacheManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * Le cache sur les rubriques est maintenant géré via {@link ServiceRubrique} directement.
 */
@Deprecated
@Component
public class CacheRubriqueManager extends AbstractCacheManager {

    public static final String ID_BEAN = "cacheRubriqueManager";

    public static final String KEY_CACHE = "CacheRubriqueManager.cacheRubriques";

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheRubriqueManager.class);

    public static CacheRubriqueManager getInstance() {
        return (CacheRubriqueManager) ApplicationContextManager.getCoreContextBean(ID_BEAN);
    }

    private HashMap<String, InfosRubriques> getCacheRubriques() throws Exception {
        final HashMap<String, InfosRubriques> listeRubriques = new HashMap<>();
        // Initialise la rubrique de niveau supérieur
        final InfosRubriques infosRubriqueTop = new InfosRubriques(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        listeRubriques.put(infosRubriqueTop.getCode(), infosRubriqueTop);
        int count = 0;
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        for (RubriqueBean rubrique : serviceRubrique.getAllRubriques()) {
            InfosRubriques infosRubrique = new InfosRubriques(rubrique);
            // Bandeau de navigation
            if (rubrique.getIdBandeau() != null && rubrique.getIdBandeau() != 0L) {
                infosRubrique.setIdMediaBandeau(rubrique.getIdBandeau());
            }
            // Picto associé
            if (rubrique.getIdPicto() != null && rubrique.getIdPicto() != 0L) {
                infosRubrique.setIdMediaPicto(rubrique.getIdPicto());
            }
            // Ajout dans la bibliothèque de rubriques
            listeRubriques.put(infosRubrique.getCode(), infosRubrique);
            count++;
        }
        // Effectue les rattachements de rubrique et calcule les niveaux
        for (final InfosRubriques rubrique : listeRubriques.values()) {
            if (!rubrique.getCode().equals(ServiceRubrique.CODE_RUBRIQUE_ROOT)) {
                InfosRubriques rubriqueMere = listeRubriques.get(rubrique.getCodeRubriqueMere());
                if (rubriqueMere == null || rubriqueMere.getCode().length() == 0) {
                    // La rubrique mère est le sommet de l'arbre
                    rubriqueMere = infosRubriqueTop;
                }
                // Rattache la rubrique à sa rubrique mère
                rubrique.rattacheA(rubriqueMere);
            }
        }
        LOGGER.info("Chargement de " + count + " rubriques OK");
        return listeRubriques;
    }

    @Override
    public Object getObjectToCache() throws Exception {
        return getCacheRubriques();
    }

    @Override
    public String getCacheName() {
        return KEY_CACHE;
    }

    @Override
    public Object getObjectKey() {
        return KEY_CACHE;
    }

    @Override
    public Object call() {
        final Object res = super.call();
        if (res != null) {
            return res;
        } else {
            return new HashMap<String, InfosRubriques>();
        }
    }
}
