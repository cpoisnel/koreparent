package com.univ.objetspartages.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.om.InfosProfilDsi;
import com.univ.objetspartages.services.ServiceProfildsi;

/**
 * @deprecated Le cache est désormais géré à travers le service {@link com.univ.objetspartages.services.ServiceProfildsi}
 */
@Deprecated
public class CacheProfilDsiManager {

    public static final String ID_BEAN = "cacheProfilDsiManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheProfilDsiManager.class);

    /**
     * @deprecated Utiliser le service {@link com.univ.objetspartages.services.ServiceProfildsi}
     */
    @Deprecated
    public Map<String, InfosProfilDsi> getListeProfilsDsi() {
        final Map<String, InfosProfilDsi> listeProfilsDsi = new HashMap<>();
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        List<ProfildsiBean> allProfiles = serviceProfildsi.getAll();
        for (ProfildsiBean currentProfile : allProfiles) {
            final InfosProfilDsi infosProfil = new InfosProfilDsi(currentProfile.getCode(), currentProfile.getLibelle(), currentProfile.getCodeRubriqueAccueil(), currentProfile.getRoles(), currentProfile.getCodeRattachement(), currentProfile.getGroupes());
            listeProfilsDsi.put(currentProfile.getCode(), infosProfil);
        }
        LOGGER.info("Chargement de " + allProfiles.size() + " profilDsi OK");
        return listeProfilsDsi;
    }

    /**
     * @deprecated Les evictions de cache sont désormais gérés à travers le service {@link com.univ.objetspartages.services.ServiceProfildsi}
     */
    @Deprecated
    public void flush() {}
}
