package com.univ.objetspartages.cache;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.ElementArboStructure;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.InfosStructure;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteDao;

@Deprecated
public class CacheStructureManager implements ICacheFicheManager {

    /** The Constant CODE_STRUCTURE_ROOT. */
    public static final String CODE_STRUCTURE_ROOT = "00";

    public static final String ID_BEAN = "cacheStructureManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheStructureManager.class);

    private ServiceMetatag serviceMetatag;

    private ServiceMedia serviceMedia;

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    public void setServiceMedia(final ServiceMedia serviceMedia) {
        this.serviceMedia = serviceMedia;
    }

    @Deprecated
    public HashMap<String, ElementArboStructure> getListeStructures() {
        final HashMap<String, ElementArboStructure> listeStructure = new HashMap<>();
        try (ContexteDao ctx = new ContexteDao()) {
            // Charge en mémoire les listes des différents types de structures
            for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
                FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
                if (ficheUniv != null && ficheUniv instanceof StructureModele) {
                    final StructureModele structure = (StructureModele) ficheUniv;
                    structure.setCtx(ctx);
                    final int count = structure.select("where ETAT_OBJET = '0003'");
                    if (count > 0) {
                        // chargement des métas en une seule requete
                        HashMap<Long, String> hashMetaStructure = new HashMap<>();
                        final List<MetatagBean> metas = serviceMetatag.getByObjectCode(codeObjet);
                        if (CollectionUtils.isNotEmpty(metas)) {
                            for(MetatagBean currentMeta : metas) {
                                hashMetaStructure.put(currentMeta.getMetaIdFiche(), currentMeta.getMetaInTree());
                            }
                        }
                        while (structure.nextItem()) {
                            // Instanciation de l'InfosStructure
                            InfosStructure infosStructure = new InfosStructure(structure);
                            if (structure.getIdBandeau() != 0) {
                                infosStructure.setUrlBandeau(MediaUtils.getUrlAbsolue(serviceMedia.getById(structure.getIdBandeau())));
                            }
                            if (hashMetaStructure.containsKey(structure.getIdFiche())) {
                                infosStructure.setVisibleInFront("1".equals(hashMetaStructure.get(structure.getIdFiche())));
                            }
                            // Instanciation de l'ElementArboStructure si
                            // nécessaire (gestion de la langue)
                            ElementArboStructure eltArbo = listeStructure.get(structure.getCode());
                            if (eltArbo == null) {
                                // Ajout dans la bibliothèque de structures
                                eltArbo = new ElementArboStructure(structure.getCode());
                                listeStructure.put(structure.getCode(), eltArbo);
                            }
                            eltArbo.addInfosStructure(infosStructure);
                        }
                        LOGGER.info("Chargement de " + count + " structures de type " + ReferentielObjets.getLibelleObjet(codeObjet) + " OK");
                    }
                }
            }
            // Initialise la structure de niveau supérieur
            ElementArboStructure eltArboTop = listeStructure.get(CODE_STRUCTURE_ROOT);
            if (eltArboTop == null) {
                eltArboTop = new ElementArboStructure(CODE_STRUCTURE_ROOT);
                listeStructure.put(eltArboTop.getCode(), eltArboTop);
            }
            for (int i = 0; i < LangueUtil.getNbLangues(); i++) {
                InfosStructure infosStructTop = new InfosStructure();
                infosStructTop.setCode(CODE_STRUCTURE_ROOT);
                infosStructTop.setLangue(String.valueOf(i));
                eltArboTop.addInfosStructure(infosStructTop);
            }
        } catch (final Exception e) {
            LOGGER.error("Impossible de charger le cache des structures", e);
        }
        return listeStructure;
    }

    @Override
    @Deprecated
    public void flush() {}
}
