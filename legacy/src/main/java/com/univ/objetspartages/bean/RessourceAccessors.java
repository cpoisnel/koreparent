package com.univ.objetspartages.bean;

import java.util.Date;

/**
 * @deprecated Interface inutile
 */
@Deprecated
public interface RessourceAccessors {

    /**
     * Gets the titre.
     *
     * @return the titre
     */
    public String getTitre();

    /**
     * Gets the legende.
     *
     * @return the legende
     */
    public String getLegende();

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription();

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl();

    /**
     * Gets the vignette url.
     *
     * @return the vignette url
     */
    public String getUrlVignette();

    /**
     * Gets the vignette url.
     *
     * @return the vignette url
     */
    public String getPathVignetteAbsolu();

    /**
     * Gets the path absolu.
     *
     * @return the path absolu
     */
    public String getPathAbsolu();

    /**
     * Gets the url absolue.
     *
     * @return the url absolue
     */
    public String getUrlAbsolue();

    /**
     * Gets the type ressource.
     *
     * @return the type ressource
     */
    public String getTypeRessource();

    /**
     * Gets the type media.
     *
     * @return the type media
     */
    public String getTypeMedia();

    /**
     * Gets the poids.
     *
     * @return the poids
     */
    public Integer getPoids();

    /**
     * Gets the source.
     *
     * @return the source
     */
    public String getSource();

    /**
     * Gets the format.
     *
     * @return the format
     */
    public String getFormat();

    /**
     * Sets the source.
     *
     * @param source
     *            the new source
     */
    public void setSource(String source);

    /**
     * Sets the format.
     *
     * @param format
     *            the new format
     */
    public void setFormat(String format);

    /**
     * Gets the auteur.
     *
     * @return the auteur
     */
    public String getAuteur();

    /**
     * Gets the copyright.
     *
     * @return the copyright
     */
    public String getCopyright();

    /**
     * Gets the specific data.
     *
     * @return the specific data
     */
    public String getSpecificData();

    /**
     * Gets the date creation.
     *
     * @return the date creation
     */
    public Date getDateCreation();

    /**
     * Gets the meta keywords.
     *
     * @return the meta keywords
     */
    public String getMetaKeywords();

    /**
     * Gets the code redacteur.
     *
     * @return the code redacteur
     */
    public String getCodeRedacteur();

    /**
     * Sets the titre.
     *
     * @param sTitre
     *            the new titre
     */
    public void setTitre(String sTitre);

    /**
     * Sets the legende.
     *
     * @param sLegende
     *            the new legende
     */
    public void setLegende(String sLegende);

    /**
     * Sets the description.
     *
     * @param sDescription
     *            the new description
     */
    public void setDescription(String sDescription);

    /**
     * Sets the url.
     *
     * @param sUrl
     *            the new url
     */
    public void setUrl(String sUrl);

    /**
     * Sets the vignette url.
     *
     * @param sUrl
     *            the new url
     */
    public void setUrlVignette(String sUrl);

    /**
     * Sets the type ressource.
     *
     * @param sTypeRessource
     *            the new type ressource
     */
    public void setTypeRessource(String sTypeRessource);

    /**
     * Sets the type media.
     *
     * @param sTypeMedia
     *            the new type media
     */
    public void setTypeMedia(String sTypeMedia);

    /**
     * Sets the poids.
     *
     * @param iPoids
     *            the new poids
     */
    public void setPoids(Integer iPoids);

    /**
     * Sets the auteur.
     *
     * @param sAuteur
     *            the new auteur
     */
    public void setAuteur(String sAuteur);

    /**
     * Sets the copyright.
     *
     * @param sCopyright
     *            the new copyright
     */
    public void setCopyright(String sCopyright);

    /**
     * Sets the specific data.
     *
     * @param sSpecificData
     *            the new specific data
     */
    public void setSpecificData(String sSpecificData);

    /**
     * Sets the date creation.
     *
     * @param sDateCreation
     *            the new date creation
     */
    public void setDateCreation(Date sDateCreation);

    /**
     * Sets the code redacteur.
     *
     * @param sCodeRedacteur
     *            the new code redacteur
     */
    public void setCodeRedacteur(String sCodeRedacteur);

    /**
     * Sets the meta keywords.
     *
     * @param sKeywords
     *            the new meta keywords
     */
    public void setMetaKeywords(String sKeywords);

    /**
     * Sets the meta keywords.
     *
     * @return the checks if is mutualise
     */
    public String getIsMutualise();
}
