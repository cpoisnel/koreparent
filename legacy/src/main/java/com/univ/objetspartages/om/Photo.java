package com.univ.objetspartages.om;

import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.sgbd.PhotoDB;
import com.univ.objetspartages.bean.RessourceBean;

import java.io.File;
import java.util.Date;
// TODO: Auto-generated Javadoc

/**
 * The Class Photo.
 * @deprecated classe plus utilisée, elle est remplacée par {@link RessourceBean}
 */
@Deprecated
public class Photo extends PhotoDB {

    /**
     * Instantiates a new photo.
     */
    public Photo() {
        super();
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdPhoto((long) 0);
        setDescription("");
        setTitre("");
        setLegende("");
        setTypePhoto("");
        setLargeur(0);
        setHauteur(0);
        setCodeStructure("");
        setUrl("");
        setPath("");
        setDateCreation(new Date(System.currentTimeMillis()));
        setCodeRedacteur("");
        setMetaKeywords("");
        setCodeRubrique("");
    }

    /**
     * Gets the path absolu.
     *
     * @return the path absolu
     */
    public String getPathAbsolu() {
        return PropertyHelper.getCoreProperty("phototheque.path") + File.separator + getUrl().replace('/', File.separatorChar);
    }

    /**
     * Gets the url absolue.
     *
     * @return the url absolue
     */
    public String getUrlAbsolue() {
        return PropertyHelper.getCoreProperty("phototheque.url") + getUrl();
    }
}
