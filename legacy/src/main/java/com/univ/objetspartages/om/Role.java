package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.database.OMContext;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.objetspartages.cache.CacheRoleManager;
import com.univ.objetspartages.sgbd.RoleDB;
import com.univ.objetspartages.util.InfosRolesUtils;
import com.univ.utils.ContexteDao;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * The Class Role.
 * @deprecated la classe role ne doit plus être utilisé. Pour les données {@link com.univ.objetspartages.bean.RoleBean} pour les méthodes de services {@link com.univ.objetspartages.services.ServiceRole}
 * pour les méthodes appelant le cache {@link InfosRolesUtils}
 */
@Deprecated
public class Role extends RoleDB implements Cloneable {

    private static final Logger LOG = LoggerFactory.getLogger(Role.class);

    /**
     * renvoie le périmètre d'affectation d'un role dans une liste d'affectations stockée sous la forme sous la forme [role1;per1][role2;per2]
     *
     * Cette fonction applique en plus du péimètre d'affectation le masque de périmètre du role.
     *
     * @param roles
     *            the roles
     * @param ctx
     *            the _ctx
     * @param codeRole
     *            the _code role
     * @param codesStructures
     *            the _codes structures
     * @param codeRubrique
     *            the _code rubrique
     * @param publicsVises
     *            the _publics vises
     * @param codeEspaceCollaboratif
     *            the _code espace collaboratif
     *
     * @return the vector
     * @deprecated on passe le contexte pour rien, utiliser {@link InfosRolesUtils#renvoyerPerimetresAffectation(String, String, List, String, String, String)} à la place
     *
     */
    @Deprecated
    public static Vector<Perimetre> renvoyerPerimetresAffectation(final OMContext ctx, final String roles, final String codeRole, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif) {
        return InfosRolesUtils.renvoyerPerimetresAffectation(roles, codeRole, codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif);
    }

    /**
     * renvoie le périmètre d'affectation d'un role dans une liste d'affectations stockée sous la forme sous la forme [role1;per1][role2;per2]
     *
     * Cette fonction applique en plus du péimètre d'affectation le masque de périmètre du role.
     *
     * @param roles
     *            the roles
     * @param _codeRole
     *            the _code role
     * @param codesStructures
     *            the _codes structures
     * @param codeRubrique
     *            the _code rubrique
     * @param publicsVises
     *            the _publics vises
     * @param codeEspaceCollaboratif
     *            the _code espace collaboratif
     *
     * @return the vector
     */
    public static Vector<Perimetre> renvoyerPerimetresAffectation(final String roles, final String _codeRole, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif) {
        return InfosRolesUtils.renvoyerPerimetresAffectation(roles, _codeRole, codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif);
    }

    /**
     * Renvoie la liste de tous les roles Peut être affiché directement dans une Combo
     *
     * @param ctx
     *            the _ctx
     *
     * @return the liste id roles
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert à rien @see getAllRolesWithoutCollab()
     */
    @Deprecated
    public static Collection<Role> getListeIDRoles(final OMContext ctx) throws Exception {
        final Role role = new Role();
        role.setCtx(ctx);
        String requete = StringUtils.EMPTY;
        // filtrage des roles collaboratifs si collaboratif non activé
        if (!Espacecollaboratif.isExtensionActivated()) {
            requete = "WHERE PERIMETRE NOT LIKE '%/*'";
        }
        final int count = role.select(requete);
        final Collection<Role> res = new ArrayList<>(count);
        while (role.nextItem()) {
            res.add((Role) role.clone());
        }
        return res;
    }

    /**
     * Renvoie la liste de tous les roles Peut être affiché directement dans une Combo
     *
     * @return the liste id roles
     *
     * @throws Exception
     *             the exception
     */
    public static Collection<Role> getAllRolesWithoutCollab() throws Exception {
        final Role role = new Role();
        final Collection<Role> res = new ArrayList<>();
        try (ContexteDao ctx = new ContexteDao()) {
            role.setCtx(ctx);
            String requete = StringUtils.EMPTY;
            // filtrage des roles collaboratifs si collaboratif non activé
            if (!Espacecollaboratif.isExtensionActivated()) {
                requete = "WHERE PERIMETRE NOT LIKE '%/*'";
            }
            role.select(requete);
            while (role.nextItem()) {
                res.add((Role) role.clone());
            }
        }
        return res;
    }

    /**
     * Renvoie la liste de tous les roles Peut être affiché directement dans une Combo
     *
     * @param ctx
     *            the ctx : on en a pas besoin...
     *
     * @return the liste roles
     *
     */
    public static Hashtable<String, String> getListeRoles(final OMContext ctx) {
        final Map<String, InfosRole> listeRoles = InfosRolesUtils.getListeRoles();
        final Collection<InfosRole> toutLesInfosRoles = listeRoles.values();
        final Hashtable<String, String> res = new Hashtable<>(listeRoles.size());
        for (final InfosRole info : toutLesInfosRoles) {
            res.put(info.getCode(), info.getIntitule());
        }
        return res;
    }

    /**
     * Renvoie la liste de tous les roles (hors espace collaboratif) Peut être affiché directement dans une Combo
     *
     * @param ctx
     *            the _ctx
     *
     * @return the liste roles hors espace
     *
     * @deprecated on n'a pas besoin du contexte utiliser la même méthode sans contexte
     */
    @Deprecated
    public static Hashtable<String, String> getListeRolesHorsEspace(final OMContext ctx) {
        return getListeRolesHorsEspace();
    }

    /**
     * Renvoie la liste de tous les roles (hors espace collaboratif) Peut être affiché directement dans une Combo
     *
     * @return the liste roles hors espace
     *
     */
    public static Hashtable<String, String> getListeRolesHorsEspace() {
        final Map<String, InfosRole> listeRoles = InfosRolesUtils.getListeRoles();
        final Collection<InfosRole> toutLesInfosRoles = listeRoles.values();
        final Hashtable<String, String> res = new Hashtable<>(listeRoles.size());
        for (final InfosRole info : toutLesInfosRoles) {
            if (new Perimetre(info.getPerimetre()).getCodeEspaceCollaboratif().length() == 0) {
                res.put(info.getCode(), info.getIntitule());
            }
        }
        return res;
    }

    /**
     * Renvoie la liste de tous les roles dédiés aux espaces collaboratif Peut être affiché directement dans une Combo
     *
     * @param ctx
     *            the _ctx
     *
     * @return the liste roles espace collaboratif
     *
     * @deprecated on a pas besoin du contexte ici, utilisez la même sans contexte
     */
    @Deprecated
    public static Hashtable<String, String> getListeRolesEspaceCollaboratif(final OMContext ctx) {
        return getListeRolesEspaceCollaboratif();
    }

    /**
     * Renvoie la liste de tous les roles dédiés aux espaces collaboratif Peut être affiché directement dans une Combo
     *
     *
     * @return the liste roles espace collaboratif
     *
     */
    // TODO : à externaliser dans le module collab
    public static Hashtable<String, String> getListeRolesEspaceCollaboratif() {
        final Map<String, InfosRole> listeRoles = InfosRolesUtils.getListeRoles();
        final Hashtable<String, String> res = new Hashtable<>(listeRoles.size());
        for (final InfosRole info : listeRoles.values()) {
            if ("*".equals(new Perimetre(info.getPerimetre()).getCodeEspaceCollaboratif())) {
                res.put(info.getCode(), info.getIntitule());
            }
        }
        return res;
    }

    /**
     * Renvoie la liste des rôles
     * @return la liste de tout les rôles du cache
     */
    public static HashMap<String, InfosRole> getListeRoles() {
        final CacheRoleManager cache = (CacheRoleManager) ApplicationContextManager.getCoreContextBean(CacheRoleManager.ID_BEAN);
        return cache.getListeRoles();
    }

    /**
     * Récupération d'un profil stocké en mémoire.
     *
     * @param code
     *            the code
     *
     * @return the infos role
     *
     */
    public static InfosRole renvoyerItemRole(final String code) {
        return InfosRolesUtils.renvoyerItemRole(code);
    }

    /**
     * Récupération de l'intitulé.
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the _code
     *
     * @return the intitule
     * @deprecated on passe le contexte pour rien, utiliser {@link InfosRolesUtils#getIntitule(String)}
     */
    @Deprecated
    public static String getIntitule(final OMContext ctx, final String code) {
        return InfosRolesUtils.getIntitules(code);
    }

    /**
     * Récupération de l'intitulé.
     *
     * @param codes
     *            the _code
     *
     * @return the intitule
     *
     */
    public static String getIntitule(final String codes) {
        return InfosRolesUtils.getIntitules(codes);
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdRole((long) 0);
        setCode(String.valueOf(System.currentTimeMillis()));
        setLibelle("");
        setPerimetre("");
        setPermissions("");
    }

    /**
     * Select.
     *
     * @param code
     *            the _code
     * @param libelle
     *            the _libelle
     * @param permission
     *            the _permission
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String code, final String libelle, final String permission) throws Exception {
        final RequeteSQL requeteCodeLibellePermission = new RequeteSQL();
        final ClauseOrderBy orderBy = new ClauseOrderBy("LIBELLE", SensDeTri.ASC);
        final ClauseWhere where = new ClauseWhere();
        if (StringUtils.isNotEmpty(code)) {
            where.setPremiereCondition(ConditionHelper.egalVarchar("CODE", code));
        }
        if (StringUtils.isNotEmpty(libelle)) {
            where.and(ConditionHelper.like("LIBELLE", libelle, "%[", "]%"));
        }
        if (StringUtils.isNotEmpty(permission)) {
            where.and(ConditionHelper.like("PERMISSIONS", permission, "%[", "]%"));
        }
        requeteCodeLibellePermission.where(where).orderBy(orderBy);
        return select(requeteCodeLibellePermission.formaterRequete());
    }

    /**
     * Renvoie la liste des permissions sous forme de vecteur
     *
     * @return the vecteur permissions
     *
     */
    public Vector<PermissionBean> getVecteurPermissions() {
        final Vector<PermissionBean> v = new Vector<>();
        final StringTokenizer st = new StringTokenizer(getPermissions(), "[]");
        while (st.hasMoreTokens()) {
            final String val = st.nextToken();
            v.add(new PermissionBean(val));
        }
        return v;
    }

    /**
     * Valorise la liste des permissions sous forme de vecteur.
     *
     * @param v
     *            the new vecteur permissions
     *
     */
    public void setVecteurPermissions(final Vector<PermissionBean> v) {
        String liste = "";
        final Enumeration<PermissionBean> en = v.elements();
        while (en.hasMoreElements()) {
            liste = liste + "[" + (en.nextElement().getChaineSerialisee()) + "]";
        }
        setPermissions(liste);
    }

    @Override
    protected Role clone() throws CloneNotSupportedException {
        return (Role) super.clone();
    }
}
