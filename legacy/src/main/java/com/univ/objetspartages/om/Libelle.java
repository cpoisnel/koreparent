package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.cache.CacheLibelleManager;
import com.univ.objetspartages.dao.impl.LabelDAO;
import com.univ.objetspartages.services.ServiceLabel;
import com.univ.utils.ContexteUtil;

/**
 * The Class Libelle.
 * @deprecated La classe Libelle est remplacée par {@link ServiceLabel}. Le service permet de requêter en BDD via {@link LabelDAO}.
 * Les données sont stockés dans l'objet {@link LabelBean}
 * Les méthodes statiques utilitaires sont aussi présente sur {@link ServiceLabel} mais sont maintenant des méthodes d'instance du service.
 */
@Deprecated
public class Libelle extends AbstractOm<LabelBean, LabelDAO> implements Cloneable {

    private Collection<String> languesPossiblesPourTraduction = new ArrayList<>();

    private boolean isLibelleSupprimable = Boolean.TRUE;

    private static ServiceLabel getServiceLabel() {
        return ServiceManager.getServiceForBean(LabelBean.class);
    }

    /**
     * @deprecated Plus besoin de renseigner le contexte. Cette méthode est maintenant vide.
     */
    @Deprecated
    public void setCtx(OMContext ctx) {
        // Nothing to do
    }

    /**
     * Ajoute (ou modifie) un libellé
     *
     * @param _ctx
     *            the _ctx
     * @param _typeLibelle
     *            the _type libelle
     * @param _code
     *            the _code
     * @param _libelle
     *            the _libelle
     * @param _langue
     *            the _langue
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceLabel#createNewLabel(String, String, String, String)}
     */
    @Deprecated
    public static String addLibelle(final OMContext _ctx, final String type, final String code, final String libelle, final String langue) throws Exception {
        getServiceLabel().createNewLabel(type, code, libelle, langue);
        return StringUtils.EMPTY;
    }

    /**
     * @deprecated Utilisez {@link ServiceLabel#getLabelsLibelles(String, List, String)}. La seule différence étant que le paramètre _code est maintenant une List plutôt qu'une
     * chaine de caractère à splitter
     * @param _ctx
     * @param _typeLibelle
     * @param _codes
     * @param _locale
     * @return
     */
    @Deprecated
    public static List<String> getLibelleSousFormeDeListe(final OMContext _ctx, final String _typeLibelle, final String _codes, final Locale _locale) {
        return getLibelleSousFormeDeListe(_typeLibelle, _codes, _locale);
    }

    /**
     * Renvoie les libellés sous forme de liste pour pouvoir les gérer de manière plus souple.
     *
     * @param _codes
     * @param _typeLibelle
     * @param _locale
     * @deprecated Utilisez {@link ServiceLabel#getLabelsLibelles(String, List, String)}. La seule différence étant que le paramètre _code est maintenant une List plutôt qu'une
     * chaine de caractère à splitter
     */
    @Deprecated
    public static List<String> getLibelleSousFormeDeListe(final String type, final String codes, final Locale locale) {
        List<String> codeList = new ArrayList<>();
        if(StringUtils.isNotBlank(codes) && !"0000".equals(codes)) {
            codeList = Arrays.asList(codes.split(";"));
        }
        return getServiceLabel().getLabelsLibelles(type, codeList, Integer.toString(LangueUtil.getIndiceLocale(locale)));
    }

    /**
     * @deprecated Utilisez {@link ServiceLabel#getLabelsLibelles(String, List, String)}
     * @param ctx
     * @param typeLibelle
     * @param codes
     * @param locale
     * @return
     */
    @Deprecated
    public static String getLibelle(final OMContext ctx, final String typeLibelle, final String codes, final Locale locale) {
        return getLibelle(typeLibelle, codes, locale);
    }

    /**
     * @deprecated Utilisez {@link ServiceLabel#getLabelsLibelles(String, List, String)} Le séparateur n'était pas utiliser
     * @param ctx
     * @param typeLibelle
     * @param codes
     * @param locale
     * @param separateur
     * @return
     */
    @Deprecated
    public static String getLibelle(final OMContext ctx, final String typeLibelle, final String codes, final Locale locale, final String separateur) {
        return getLibelle(typeLibelle, codes, locale);
    }

    /**
     * Gets the libelle. Renvoie un libelle (ou une liste de libellés) Si plusieurs libellés, et ctx = ContexteUniv (front-office), le séparateur est ", " Si plusieurs libellés et
     * ctx != ContexteUniv, le séparateur est ";" Peut être affiché directement dans une Combo
     *
     * @param _typeLibelle
     *            the _type libelle
     * @param _codes
     *            the _codes
     * @param _locale
     *            the _locale
     *
     * @return the libelle
     * @deprecated Utilisez {@link ServiceLabel#getLabelsLibelles(String, List, String)}
     */
    @Deprecated
    public static String getLibelle(final String type, final String codes, final Locale locale) {
        final List<String> libelles = getLibelleSousFormeDeListe(type, codes, locale);
        if(ContexteUtil.getContexteUniv() != null) {
            return StringUtils.join(libelles, ", ");
        } else {
            return StringUtils.join(libelles, ";");
        }
    }

    /**
     * @deprecated Utilisez {@link ServiceLabel#getLabelForCombo(String, String)}
     * @param _ctx
     * @param _typeLibelle
     * @param _locale
     * @return
     * @throws Exception
     */
    @Deprecated
    public static Hashtable<String, String> getListe(final OMContext _ctx, final String _typeLibelle, Locale _locale) throws Exception {
        return getListe(_typeLibelle, _locale);
    }

    /**
     * Renvoie la liste de tous les libelles Peut être affiché directement dans une Combo
     *
     * @param _typeLibelle
     *            the _type libelle
     * @param _locale
     *            the _locale
     *
     * @return the liste
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceLabel#getLabelForCombo(String, Locale)}
     */
    @Deprecated
    public static Hashtable<String, String> getListe(final String typeLibelle, Locale locale) throws Exception {
        final Hashtable<String, String> h = new Hashtable<>();
        if (locale == null) {
            locale = LangueUtil.getDefaultLocale();
        }
        h.putAll(getServiceLabel().getLabelForCombo(typeLibelle, String.valueOf(LangueUtil.getIndiceLocale(locale))));
        return h;
    }

    /**
     * Renvoie une liste triée de libellés
     *
     * @param _ctx
     *            the _ctx
     * @param _typeLibelle
     *            the _type libelle
     * @param _locale
     *            the _locale
     *
     * @return the liste triee par libelle
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceLabel#getByTypeLanguage(String, String)}
     */
    @Deprecated
    public static Vector<Libelle> getListeTrieeParLibelle(final OMContext ctx, final String type, final Locale locale) throws Exception {
        final Vector<Libelle> v = new Vector<>();
        final List<LabelBean> labelList = getServiceLabel().getByTypeLanguage(type, String.valueOf(LangueUtil.getIndiceLocale(locale)));
        for(LabelBean currentLabel : labelList) {
            final Libelle libelle = new Libelle();
            libelle.setPersistenceBean(currentLabel);
            v.add(libelle);
        }
        return v;
    }

    /**
     * Renvoie la liste de tous les libelles matchant un token SQL (ex :%math%) Peut être affiché directement dans une Combo
     *
     * @param _ctx
     *            the _ctx
     * @param _typeLibelle
     *            the _type libelle
     * @param token
     *            the token
     *
     * @return the liste par token
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceLabel#getMatchingLabelForCombo(String, String)}
     */
    @Deprecated
    public static Hashtable<String, String> getListeParToken(final OMContext _ctx, final String type, final String token) throws Exception {
        final Hashtable<String, String> h = new Hashtable<>();
        h.putAll(getServiceLabel().getMatchingLabelForCombo(type, token));
        return h;
    }

    /**
     * @deprecated Utilisez {@link CacheLibelleManager#getListeTypesLibelles()} pour récupérer l'ensemble des types de label dispo sur l'appli.
     * @return
     */
    @Deprecated
    public static Hashtable<String, String> getListeTypesLibelles() {
        final Hashtable<String, String> table = new Hashtable<>();
        final CacheLibelleManager cache = (CacheLibelleManager) ApplicationContextManager.getCoreContextBean(CacheLibelleManager.ID_BEAN);
        table.putAll(cache.getListeTypesLibelles());
        return table;
    }

    /**
     * Récupération d'un libelle stocké en mémoire.
     *
     * @param type
     *            the type
     * @param code
     *            the code
     * @param langue
     *            the langue
     *
     * @return the infos libelle
     * @deprecated Utilisez {@link ServiceLabel#getByTypeCodeLanguage(String, String, String)}
     */
    @Deprecated
    public static InfosLibelle renvoyerItemLibelle(final String type, final String code, final String langue) {
        return null;
    }

    /**
     * @deprecated cette méthode est vide, elle est présente uniquement pour que les anciennes applis compilent.
     */
    @Deprecated
    public void init() {
    }

    /**
     * Gets the libelle affichable.
     *
     * @return the libelle affichable
     * @deprecated Utilisez {@link ServiceLabel#getDisplayableLibelle(LabelBean)}
     */
    @Deprecated
    public String getLibelleAffichable() {
        if (getLibelle().startsWith("[")) {
            final int i = getLibelle().indexOf("]") + 1;
            if (i != 0 && i != getLibelle().length()) {
                return getLibelle().substring(i);
            }
        }
        return getLibelle();
    }

    /**
     * Gets the code site.
     *
     * @return the code site
     * @deprecated Utilisez {@link ServiceLabel#getCodeSite(LabelBean)}
     */
    @Deprecated
    public String getCodeSiteLibelle() {
        if (getLibelle().startsWith("[")) {
            final int i = getLibelle().indexOf("]") + 1;
            if (i != 0 && i != getLibelle().length()) {
                return getLibelle().substring(1, i - 1);
            }
        }
        return "";
    }

    @Override
    public Libelle clone() throws CloneNotSupportedException {
        return (Libelle) super.clone();
    }

    /**
     * @deprecated ce paramètre n'était jamais initialiser.
     */
    @Deprecated
    public Collection<String> getLanguesPossiblesPourTraduction() {
        return languesPossiblesPourTraduction;
    }

    /**
     * @deprecated ce paramètre n'était jamais initialiser.
     */
    @Deprecated
    public void setLanguesPossiblesPourTraduction(final Collection<String> languesPossiblesPourTraduction) {
        this.languesPossiblesPourTraduction = languesPossiblesPourTraduction;
    }

    /**
     * @deprecated ce paramètre n'était jamais initialiser.
     */
    @Deprecated
    public boolean isLibelleSupprimable() {
        return isLibelleSupprimable;
    }

    /**
     * @deprecated ce paramètre n'était jamais initialiser.
     */
    @Deprecated
    public void setLibelleSupprimable(final boolean isLibelleSupprimable) {
        this.isLibelleSupprimable = isLibelleSupprimable;
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public Long getIdLibelle() {
        return persistenceBean.getId();
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public void setIdLibelle(Long id) {
        persistenceBean.setId(id);
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public String getType() {
        return persistenceBean.getType();
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public void setType(String type) {
        persistenceBean.setType(type);
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public String getCode() {
        return persistenceBean.getCode();
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public void setCode(String code) {
        persistenceBean.setCode(code);
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public String getLibelle() {
        return persistenceBean.getLibelle();
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public void setLibelle(String libelle) {
        persistenceBean.setLibelle(libelle);
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public String getLangue() {
        return persistenceBean.getLangue();
    }

    /**
     * @deprecated Utilisez {@link LabelBean}
     */
    @Deprecated
    public void setLangue(String langue) {
        persistenceBean.setLangue(langue);
    }
}
