/*
 * Created on 6 nov. 07
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.objetspartages.om;

import com.univ.objetspartages.bean.RessourceAccessors;
// TODO: Auto-generated Javadoc

/**
 * The Interface RessourceUniv.
 */
public interface RessourceUniv extends RessourceAccessors {

    /**
     * @return
     */
    public String getGenerateName();

    public void setGenerateName(String generateName);
}
