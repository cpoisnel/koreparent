package com.univ.objetspartages.om;

import java.util.Collection;
import java.util.List;
import java.util.Vector;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.dao.impl.GroupeUtilisateurDAO;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;

/**
 * The Class Groupeutilisateur.
 * @deprecated Cet objet est obsolete, il est maintenant dispatché entre un bean {@link com.univ.objetspartages.bean.GroupeUtilisateurBean} pour les données et un service {@link ServiceGroupeUtilisateur} pour
 * les méthodes gérant l'accès à la bdd / les méthodes utilitaires etc.
 */
@Deprecated
public class Groupeutilisateur extends AbstractOm<GroupeUtilisateurBean, GroupeUtilisateurDAO> {

    /**
     * Renvoie la liste des groupes d'un utilisateur.
     *
     * @param ctx le contexte ne sert à rien
     * @param codeUtilisateur
     *            the _code utilisateur
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#getAllUserCodeByGroup(String)}
     */
    @Deprecated
    public static Vector<String> getVecteurGroupes(final OMContext ctx, final String codeUtilisateur) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        return new Vector<>(serviceGroupeUtilisateur.getAllGroupsCodesByUserCode(codeUtilisateur));
    }

    /**
     * Renvoie la liste des groupes d'un utilisateur.
     *
     * @param codeUtilisateur
     *            the _code utilisateur
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#getAllUserCodeByGroup(String)}
     */
    @Deprecated
    public static Vector<String> getVecteurGroupes(final String codeUtilisateur) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        return new Vector<>(serviceGroupeUtilisateur.getAllGroupsCodesByUserCode(codeUtilisateur));
    }

    /**
     * Renvoie la liste des groupes d'un utilisateur pour une source d'import.
     *
     * @param ctx
     *            the _ctx
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param sourceImport
     *            the source import
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#getGroupCodeByUserAndImportSource(String, String)}
     */
    @Deprecated
    public static Vector<String> getVecteurGroupesSourceImport(final OMContext ctx, final String codeUtilisateur, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        return new Vector<>(serviceGroupeUtilisateur.getGroupCodeByUserAndImportSource(codeUtilisateur, sourceImport));
    }

    /**
     * Renvoie la liste des groupes d'un utilisateur pour une source d'import.
     *
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param sourceImport
     *            the source import
     *
     * @return @throws Exception
     *
     * @throws Exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#getGroupCodeByUserAndImportSource(String, String)}
     */
    @Deprecated
    public static Vector<String> getVecteurGroupesSourceImport(final String codeUtilisateur, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        return new Vector<>(serviceGroupeUtilisateur.getGroupCodeByUserAndImportSource(codeUtilisateur, sourceImport));
    }

    /**
     *
     * @param ctx
     * @param codeGroupe
     * @param sourceImport
     * @return
     * @throws Exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#getUserCodeByGroupAndImportSource(String, String)}
     */
    @Deprecated
    public static Vector<String> getVecteurUtilisateursSourceImport(final OMContext ctx, final String codeGroupe, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        return new Vector<>(serviceGroupeUtilisateur.getUserCodeByGroupAndImportSource(codeGroupe, sourceImport));
    }

    /**
     * Renvoie la liste des groupes d'un utilisateur pour une source d'import.
     *
     * @param codeGroupe
     *            the _code groupe
     * @param sourceImport
     *            the source import
     *
     * @return @throws Exception
     *
     * @throws Exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#getUserCodeByGroupAndImportSource(String, String)}
     */
    @Deprecated
    public static Vector<String> getVecteurUtilisateursSourceImport(final String codeGroupe, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        return new Vector<>(serviceGroupeUtilisateur.getUserCodeByGroupAndImportSource(codeGroupe, sourceImport));
    }

    /**
     * Valorise la liste des groupes d'un utilisateur.
     *
     * Cette méthode conserve les sources d'import pour les relations déjà présentes.
     *
     * @param ctx
     *            the _ctx
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param listeGroupes
     *            the liste groupes
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#setGroupsForUser(String, java.util.Collection)}
     */
    @Deprecated
    public static void setVecteurGroupes(final OMContext ctx, final String codeUtilisateur, final Vector<String> listeGroupes) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.setGroupsForUser(codeUtilisateur, listeGroupes);
    }

    /**
     * Valorise la liste des groupes d'un utilisateur.
     *
     * Cette méthode conserve les sources d'import pour les relations déjà présentes.
     *
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param listeGroupes
     *            the liste groupes
     *
     * @throws Exception principalement sur les requêtes en BDD
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#setGroupsForUser(String, java.util.Collection)}
     */
    @Deprecated
    public static void setVecteurGroupes(final String codeUtilisateur, final Vector<String> listeGroupes) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.setGroupsForUser(codeUtilisateur, listeGroupes);
    }

    /**
     * Synchronise la liste des groupes d'un utilisateur pour une source d'import.
     *
     * @param ctx
     *            the _ctx
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param listeGroupes
     *            the liste groupes
     * @param sourceImport
     *            the source import
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#synchronizeGroupsForUser(Collection, String, String)}
     */
    @Deprecated
    public static void synchroniserGroupesUtilisateur(final OMContext ctx, final String codeUtilisateur, final Vector<String> listeGroupes, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.synchronizeGroupsForUser(listeGroupes, codeUtilisateur, sourceImport);
    }

    /**
     * Synchronise la liste des groupes d'un utilisateur pour une source d'import.
     *
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param listeGroupes
     *            the liste groupes
     * @param sourceImport
     *            the source import
     *
     * @throws Exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#synchronizeGroupsForUser(Collection, String, String)}
     */
    @Deprecated
    public static void synchroniserGroupesUtilisateur(final String codeUtilisateur, final Vector<String> listeGroupes, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.synchronizeGroupsForUser(listeGroupes, codeUtilisateur, sourceImport);
    }

    /**
     * Synchronise la liste des utilisateurs d'un groupe pour une source d'import.
     *
     * @param ctx
     *            the _ctx
     * @param codeGroupe
     *            the _code groupe
     * @param listeUtilisateurs
     *            the liste utilisateurs
     * @param sourceImport
     *            the source import
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#synchronizeUsersForGroup(Collection, String, String)}
     */
    @Deprecated
    public static void synchroniserUtilisateursGroupe(final OMContext ctx, final String codeGroupe, final Vector<String> listeUtilisateurs, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.synchronizeUsersForGroup(listeUtilisateurs, codeGroupe, sourceImport);
    }

    /**
     * Synchronise la liste des utilisateurs d'un groupe pour une source d'import.
     *
     * @param codeGroupe
     *            the _code groupe
     * @param listeUtilisateurs
     *            the liste utilisateurs
     * @param sourceImport
     *            the source import
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#synchronizeUsersForGroup(Collection, String, String)}
     */
    @Deprecated
    public static void synchroniserUtilisateursGroupe(final String codeGroupe, final List<String> listeUtilisateurs, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.synchronizeUsersForGroup(listeUtilisateurs, codeGroupe, sourceImport);
    }

    /**
     * Ajoute une liste de groupes à un utilisateur.
     *
     * @param ctx
     *            the _ctx
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param listeGroupes
     *            the liste groupes
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#addGroupsToUser(Collection, String)}
     */
    @Deprecated
    public static void ajouterGroupesUtilisateur(final OMContext ctx, final String codeUtilisateur, final Vector<String> listeGroupes) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.addGroupsToUser(listeGroupes, codeUtilisateur);
    }

    /**
     * Ajoute une liste de groupes à un utilisateur.
     *
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param listeGroupes
     *            the liste groupes
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#addGroupsToUser(Collection, String)}
     */
    @Deprecated
    public static void ajouterGroupesUtilisateur(final String codeUtilisateur, final Vector<String> listeGroupes) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.addGroupsToUser(listeGroupes, codeUtilisateur);
    }

    /**
     * Ajoute une liste d'utilisateurs à un groupe.
     *
     * @param ctx
     *            the _ctx
     * @param codeGroupe
     *            the _code groupe
     * @param listeUtilisateurs
     *            the liste utilisateurs
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#addUsersToGroup(Collection, String)}
     */
    @Deprecated
    public static void ajouterUtilisateursGroupe(final OMContext ctx, final String codeGroupe, final Vector<String> listeUtilisateurs) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.addUsersToGroup(listeUtilisateurs, codeGroupe);
    }

    /**
     * Ajoute une liste d'utilisateurs à un groupe.
     *
     * @param codeGroupe
     *            the _code groupe
     * @param listeUtilisateurs
     *            the liste utilisateurs
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#addUsersToGroup(Collection, String)}
     */
    @Deprecated
    public static void ajouterUtilisateursGroupe(final String codeGroupe, final List<String> listeUtilisateurs) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.addUsersToGroup(listeUtilisateurs, codeGroupe);
    }

    /**
     * Valorise la liste des utilisateurs d'un groupe.
     *
     * @param ctx
     *            the _ctx
     * @param listeUtilisateurs
     *            the _liste utilisateurs
     * @param codeGroupe
     *            the _code groupe
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#setUsersForGroup(Collection, String)}
     */
    @Deprecated
    public static void setVecteurUtilisateurs(final OMContext ctx, final Vector<String> listeUtilisateurs, final String codeGroupe) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.setUsersForGroup(listeUtilisateurs, codeGroupe);
    }

    /**
     * Valorise la liste des utilisateurs d'un groupe.
     *
     * @param listeUtilisateurs
     *            the _liste utilisateurs
     * @param codeGroupe
     *            the _code groupe
     *
     * @throws Exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#setUsersForGroup(Collection, String)}
     */
    @Deprecated
    public static void setVecteurUtilisateurs(final Vector<String> listeUtilisateurs, final String codeGroupe) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.setUsersForGroup(listeUtilisateurs, codeGroupe);
    }

    /**
     *
     * @param ctx
     * @param codesGroupe
     * @return
     * @throws Exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#getAllUserCodeByGroup(String)}
     */
    @Deprecated
    public static Vector<String> getVecteurUtilisateurs(final OMContext ctx, final String codesGroupe) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        return new Vector<>(serviceGroupeUtilisateur.getAllUserCodeByGroup(codesGroupe));
    }

    /**
     * Renvoie la liste des utilisateurs d'un groupe (parcours hiérarchique).
     *
     * @param codesGroupe
     *            the _codes groupe
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#getAllUserCodeByGroup(String)}
     */
    @Deprecated
    public static Vector<String> getVecteurUtilisateurs(final String codesGroupe) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        return new Vector<>(serviceGroupeUtilisateur.getAllUserCodeByGroup(codesGroupe));
    }

    /**
     * Destruction des relations associées à un utilisateur.
     *
     * @param ctx
     *            the _ctx
     * @param codeUtilisateur
     *            the _code utilisateur
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#deleteByUser(String)}
     */
    @Deprecated
    public static void deleteParUtilisateur(final OMContext ctx, final String codeUtilisateur) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByUser(codeUtilisateur);
    }

    /**
     * Destruction des relations associées à un utilisateur.
     *
     * @param codeUtilisateur
     *            the _code utilisateur
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#deleteByUser(String)}
     */
    @Deprecated
    public static void deleteParUtilisateur(final String codeUtilisateur) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByUser(codeUtilisateur);
    }

    /**
     * Destruction des relations associées à un utilisateur et a une source d'import.
     *
     * @param ctx
     *            the _ctx
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param sourceImport
     *            the _source import
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#deleteByUserAndImportSource(String, String)}
     */
    @Deprecated
    public static void deleteParUtilisateurSourceImport(final OMContext ctx, final String codeUtilisateur, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByUserAndImportSource(codeUtilisateur, sourceImport);
    }

    /**
     * Destruction des relations associées à un utilisateur et a une source d'import.
     *
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param sourceImport
     *            the _source import
     *
     * @throws Exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#deleteByUserAndImportSource(String, String)}
     */
    @Deprecated
    public static void deleteParUtilisateurSourceImport(final String codeUtilisateur, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByUserAndImportSource(codeUtilisateur, sourceImport);
    }

    /**
     * Destruction des relations associées à un groupe.
     *
     * @param ctx
     *            the _ctx
     * @param codeGroupe
     *            the _code groupe
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#deleteByGroup(String)}
     */
    @Deprecated
    public static void deleteParGroupe(final OMContext ctx, final String codeGroupe) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByGroup(codeGroupe);
    }

    /**
     * Destruction des relations associées à un groupe.
     *
     * @param codeGroupe
     *            the _code groupe
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#deleteByGroup(String)}
     */
    @Deprecated
    public static void deleteParGroupe(final String codeGroupe) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByGroup(codeGroupe);
    }

    /**
     * Destruction des relations associées à un groupe.
     *
     * @param ctx
     *            the _ctx
     * @param codeGroupe
     *            the _code groupe
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#deleteByUserAndGroup(String, String)}
     */
    @Deprecated
    public static void deleteUtilisateurDuGroupe(final OMContext ctx, final String codeUtilisateur, final String codeGroupe) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByUserAndGroup(codeUtilisateur, codeGroupe);
    }

    /**
     * Destruction des relations associées à un groupe.
     *
     * @param codeGroupe
     *            the _code groupe
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le contexte uniquement pour une requête en BDD, utilisez {@link ServiceGroupeUtilisateur#deleteByUserAndGroup(String, String)}
     */
    @Deprecated
    public static void deleteUtilisateurDuGroupe(final String codeUtilisateur, final String codeGroupe) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByUserAndGroup(codeUtilisateur, codeGroupe);
    }

    /**
     *
     * @param ctx le contexte ne sert à rien
     * @param codeGroupe
     * @param sourceImport
     * @throws Exception
     * @deprecated Migrer dans un service, utilisez {@link ServiceGroupeUtilisateur#deleteByGroupAndImportSource(String, String)}
     */
    @Deprecated
    public static void deleteParGroupeSourceImport(final OMContext ctx, final String codeGroupe, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByGroupAndImportSource(codeGroupe, sourceImport);
    }

    /**
     * Destruction des relations associées à un groupe et a une source d'import.
     *
     * @param sourceImport
     *            the _source import
     * @param codeGroupe
     *            the _code groupe
     *
     * @throws Exception
     * @deprecated Migrer dans un service, utilisez {@link ServiceGroupeUtilisateur#deleteByGroupAndImportSource(String, String)}
     */
    @Deprecated
    public static void deleteParGroupeSourceImport(final String codeGroupe, final String sourceImport) throws Exception {
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByGroupAndImportSource(codeGroupe, sourceImport);
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdGroupeutilisateur(0L);
        setCodeGroupe("");
        setCodeUtilisateur("");
        setSourceImport("");
    }


    public Long getIdGroupeutilisateur() {
        return persistenceBean.getIdGroupeutilisateur();
    }

    public void setIdGroupeutilisateur(Long idGroupeutilisateur) {
        persistenceBean.setIdGroupeutilisateur(idGroupeutilisateur);
    }

    public String getCodeGroupe() {
        return persistenceBean.getCodeGroupe();
    }

    public void setCodeGroupe(String codeGroupe) {
        persistenceBean.setCodeGroupe(codeGroupe);
    }

    public String getCodeUtilisateur() {
        return persistenceBean.getCodeUtilisateur();
    }

    public void setCodeUtilisateur(String codeUtilisateur) {
        persistenceBean.setCodeUtilisateur(codeUtilisateur);
    }

    public String getSourceImport() {
        return persistenceBean.getSourceImport();
    }

    public void setSourceImport(String sourceImport) {
        persistenceBean.setSourceImport(sourceImport);
    }
}
