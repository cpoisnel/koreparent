package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.dao.impl.GroupeDsiDAO;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.util.InfosGroupeDsiUtils;
import com.univ.objetspartages.util.InfosRolesUtils;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.xhtml.JavascriptTreeGroupedsi;

/**
 * Ancienne classe des Groupedsi.
 * @deprecated Les données sont maintenant dans {@link GroupeDsiBean}, les méthodes utilisant le cache {@link InfosGroupeDsi} sont
 * dans {@link InfosGroupeDsiUtils} et les méthodes de services sont dans {@link ServiceGroupeDsi}.
 *
 */
@Deprecated
public class Groupedsi extends AbstractOm<GroupeDsiBean, GroupeDsiDAO> implements Cloneable {

    /**
     * @deprecated méthode présente uniquement pour ne pas empecher la compilation sur des vieux bout de codes
     * @param ctx
     */
    @Deprecated
    public void setCtx(OMContext ctx) {
        //pour retro compat
    }
    /**
     * @deprecated utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getIntitules(String)}
     * @param ctx
     * @param code
     * @return
     */
    @Deprecated
    public static String getIntitule(final OMContext ctx, final String code) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getIntitules(code);
    }

    /**
     * Récupération de l'intitulé : gestion de plusieurs codes.
     *
     * @param codes
     *            the _code
     *
     * @return the intitule
     * @deprecated utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getIntitule(String)}
     */
    @Deprecated
    public static String getIntitule(final String codes) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getIntitules(codes);
    }

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByCode} pour récupérer un groupe particulier ou {@link com.univ.objetspartages.services.ServiceGroupeDsi#getAll}
     */
    @Deprecated
    private static Map<String, InfosGroupeDsi> getListeGroupesDsi() {
        final Map<String, InfosGroupeDsi> map = new HashMap<>();
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        for(GroupeDsiBean groupeDsiBean : serviceGroupeDsi.getAll()) {
            map.put(groupeDsiBean.getCode(), new InfosGroupeDsi(groupeDsiBean));
        }
        return map;
    }

    /**
     *
     * @return Les {@link InfosRequeteGroupe} qui sont en cache.
     * @deprecated utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getGroupRequestList()}
     */
    @Deprecated
    public static Map<String, InfosRequeteGroupe> getListeRequetesGroupes() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getGroupRequestList();
    }

    /**
     * Renvoie la liste des infos des groupes pour un type.
     *
     * @param type
     *            the _type
     *
     * @return the vector
     * @deprecated utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByType(String)}
     */
    @Deprecated
    public static Vector<InfosGroupeDsi> renvoyerCodesGroupeDsiParType(final String type) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final Vector<InfosGroupeDsi> groups = new Vector<>();
        for(GroupeDsiBean currentGroup : serviceGroupeDsi.getByType(type)) {
            groups.add(new InfosGroupeDsi(currentGroup));
        }
        return groups;
    }

    /**
     * Récupération d'une rubrique stockée en mémoire.
     *
     * @param code
     *            the code
     *
     * @return the infos groupe dsi
     * @deprecated utiliser {@link InfosGroupeDsiUtils#renvoyerItemGroupeDsi(String)}
     */
    @Deprecated
    public static InfosGroupeDsi renvoyerItemGroupeDsi(final String code) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        GroupeDsiBean res = serviceGroupeDsi.getByCode(code);
        /* groupe vide pour éviter les plantages */
        if (res == null) {
            return new InfosGroupeDsi();
        }
        return new InfosGroupeDsi(res);
    }

    /**
     * Renvoie la liste des {groupes,périmètres} associé à un role et pour un perimetre.
     *
     * @param ctx
     *            the _ctx
     * @param role
     *            the _role
     * @param codesStructures
     *            the codes structures
     * @param codeRubrique
     *            the _code rubrique
     * @param publicsVises
     *            the _publics vises
     * @param codeEspaceCollaboratif
     *            the _code espace collaboratif
     *
     * @return the hashtable
     *
     * @throws Exception
     *             the exception
     * @deprecated Le contexte ne sert à rien la méthode est remplacée par {@link com.univ.objetspartages.services.ServiceGroupeDsi#renvoyerGroupesEtPerimetres(String, List, String, String, String)}
     */
    @Deprecated
    public static Hashtable<String, Vector<Perimetre>> renvoyerGroupesEtPerimetres(final OMContext ctx, final String role, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif) throws Exception {
        final Hashtable<String, Vector<Perimetre>> h = new Hashtable<>();
        for (final InfosGroupeDsi info : getListeGroupesDsi().values()) {
            final Vector<Perimetre> v = InfosRolesUtils.renvoyerPerimetresAffectation(info.getRoles(), role, codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif);
            if (v.size() > 0) {
                h.put(info.getCode(), v);
            }
        }
        return h;
    }

    /**
     * Renvoie la liste des {groupes,périmètres} associé à un role et pour un perimetre.
     *
     * @param role
     *            the _role
     * @param codesStructures
     *            the codes structures
     * @param codeRubrique
     *            the _code rubrique
     * @param publicsVises
     *            the _publics vises
     * @param codeEspaceCollaboratif
     *            the _code espace collaboratif
     *
     * @return the hashtable
     * @throws Exception
     *             the exception
     * @deprecated Le contexte ne sert à rien la méthode est remplacée par {@link com.univ.objetspartages.services.ServiceGroupeDsi#renvoyerGroupesEtPerimetres(String, List, String, String, String)}
     */
    @Deprecated
    public static Hashtable<String, Vector<Perimetre>> renvoyerGroupesEtPerimetres(final String role, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif) throws Exception {
        final Hashtable<String, Vector<Perimetre>> h = new Hashtable<>();
        for (final InfosGroupeDsi info : getListeGroupesDsi().values()) {
            final Vector<Perimetre> v = InfosRolesUtils.renvoyerPerimetresAffectation(info.getRoles(), role, codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif);
            if (v.size() > 0) {
                h.put(info.getCode(), v);
            }
        }
        return h;
    }

    /**
     *
     * @param ctx le contexte ne sert à rien
     * @return une Hashtable contenant l'alias et l'intitulé du groupe contenu dans le cache
     * @deprecated utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getListeRequetesGroupesPourAffichage()}
     */
    @Deprecated
    public static Hashtable<String, String> getListeRequetesGroupesPourAffichage(final OMContext ctx) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return new Hashtable<>(serviceGroupeDsi.getListeRequetesGroupesPourAffichage());
    }

    /**
     * Récupération pour affichage liste des requêtes.
     *
     * @return une Hashtable contenant l'alias et l'intitulé du groupe contenu dans le cache
     * @deprecated utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getListeRequetesGroupesPourAffichage()}
     */
    @Deprecated
    public static Hashtable<String, String> getListeRequetesGroupesPourAffichage() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return new Hashtable<>(serviceGroupeDsi.getListeRequetesGroupesPourAffichage());
    }

    /**
     * Renvoie la liste des requetes.
     *
     * @param ctx
     *            the _ctx
     *
     * @return the infos requete
     * @deprecated utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getListeRequetesGroupesPourAffichage()}
     */
    @Deprecated
    public static Hashtable<String, String> getInfosRequete(final OMContext ctx) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return new Hashtable<>(serviceGroupeDsi.getListeRequetesGroupesPourAffichage());
    }

    /**
     * Récupération d'une requête stockée en mémoire.
     *
     * @param code
     *            the code
     *
     * @return the infos requete groupe
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getGroupRequestList()}
     */
    @Deprecated
    public static InfosRequeteGroupe renvoyerItemRequeteGroupe(final String code) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getGroupRequestList().get(code);
    }

    /**
     * @param ctx le contexte ne sert à rien
     * @return
     * @deprecated le code à été migrés dans {@link com.univ.objetspartages.services.ServiceGroupeDsi#getAllDynamics}
     */
    @Deprecated
    public static Vector<InfosGroupeDsi> getListeGroupesDynamiques(final OMContext ctx) {
        return new Vector<>(InfosGroupeDsiUtils.getListeGroupesDynamiques());
    }

    /**
     * Récupération des groupes dynamiques.
     * @return the liste groupes dynamiques
     * @deprecated le code à été migrés dans {@link com.univ.objetspartages.services.ServiceGroupeDsi#getAllDynamics}
     */
    @Deprecated
    public static Vector<InfosGroupeDsi> getListeGroupesDynamiques() {
        return new Vector<>(InfosGroupeDsiUtils.getListeGroupesDynamiques());
    }

    /**
     * Renvoie la liste des groupes principaux (premier niveau).
     *
     * @return Une liste d'InfosGroupeDsi
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByParentGroup(String)}
     */
    @Deprecated
    public static Collection<InfosGroupeDsi> getListeGroupesPrincipaux() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final List<GroupeDsiBean> groups = serviceGroupeDsi.getByParentGroup(StringUtils.EMPTY);
        final Collection<InfosGroupeDsi> infos = new ArrayList<>();
        for(GroupeDsiBean currentGroup : groups) {
            infos.add(new InfosGroupeDsi(currentGroup));
        }
        return infos;
    }

    /**
     * méthode qui permet de tester le périmètre de modification d'un objet sur un groupe dsi.
     *
     * @param autorisations
     *            the autorisations
     * @param sPermission
     *            the s permission
     * @param codeGroupe
     *            the code groupe
     *
     * @return true, if controler permission
     * @deprecated utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#controlerPermission(AutorisationBean, String, String)} même si, cette méthode n'a pas de sens...
     */
    @Deprecated
    public static boolean controlerPermission(final AutorisationBean autorisations, final String sPermission, final String codeGroupe) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.controlerPermission(autorisations,sPermission,codeGroupe);
    }

    /**
     * Affichage dynamique de l'arbre javascript des rubriques.
     *
     * @param code
     *            the code
     * @param autorisations
     *            the autorisations
     * @param sPermission
     *            the s permission
     *
     * @return the arbre java script
     *
     * @throws Exception
     *             the exception
     * @deprecated cette méthode sert uniquement à l'affichage des arbres 5.1. Elle ne devrait plus être appeler mêmes sur les projets 5.1 si la version était supérieure à la 5.1.4.
     */
    @Deprecated
    public static String getArbreJavaScript(String code, final AutorisationBean autorisations, final String sPermission) throws Exception {
        if (code == null || code.length() == 0) {
            code = ServiceGroupeDsi.CODE_GROUPE_ROOT;
        }
        final JavascriptTreeGroupedsi tree = new JavascriptTreeGroupedsi(autorisations, sPermission);
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final GroupeDsiBean rootGroup = serviceGroupeDsi.getByCode(ServiceGroupeDsi.CODE_GROUPE_ROOT);
        tree.load(rootGroup, serviceGroupeDsi.getByCode(code));
        return tree.print();
    }

    /**
     * Gets the fil ariane.
     *
     * @param codeGroupe
     *            the code groupe
     * @param separateur
     *            the separateur
     *
     * @return the fil ariane
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getBreadCrumbs(String, String)}
     */
    @Deprecated
    public static String getFilAriane(final String codeGroupe, String separateur) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.getBreadCrumbs(codeGroupe, separateur);
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdGroupedsi((long) 0);
        setCode(String.valueOf(System.currentTimeMillis()));
        setLibelle("");
        setType("");
        setCodePageTete("");
        setCodeStructure("");
        setRoles("");
        setCodeGroupePere("");
        setSourceImport("");
        setRequeteGroupe("");
        setRequeteLdap("");
        setGestionCache("0");
        final Long tsInit = (long) 0;
        setDelaiExpirationCache(tsInit);
        setDerniereMajCache(tsInit);
        setSelectionnable("1");
    }

    /**
     * Sélection d'un groupe DSI à partir de l'ensemble des critères combinés.
     *
     * @param code
     *            the _code
     * @param type
     *            the _type
     * @param libelle
     *            the _libelle
     * @param codeStructure
     *            the _code structure
     * @param gestionCache
     *            the _gestion cache
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     * @deprecated utiliser {@link ServiceGroupeDsi#getByCodeTypeLabelStructureAndCache(String, String, String, String, String)} mais privilégiez les autres méthodes plus ciblés en fonction du contexte.
     */
    @Deprecated
    public int select(final String code, final String type, final String libelle, final String codeStructure, final String gestionCache) throws Exception {
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseWhere where = new ClauseWhere();
        final ClauseOrderBy orderBy = new ClauseOrderBy("LIBELLE", SensDeTri.ASC);
        if (StringUtils.isNotEmpty(code)) {
            where.setPremiereCondition(ConditionHelper.egalVarchar("CODE", code));
        }
        if (StringUtils.isNotEmpty(type) && !"0000".equals(type)) {
            where.and(ConditionHelper.egalVarchar("TYPE", type));
        }
        if (StringUtils.isNotEmpty(libelle)) {
            where.and(ConditionHelper.rechercheMots("LIBELLE", libelle));
        }
        if (StringUtils.isNotEmpty(codeStructure)) {
            where.and(ConditionHelper.getConditionStructure("CODE_STRUCTURE", codeStructure));
        }
        if (StringUtils.isNotEmpty(gestionCache)) {
            where.and(ConditionHelper.egalVarchar("GESTION_CACHE", gestionCache));
        }
        requeteSelect.where(where).orderBy(orderBy);
        return select(requeteSelect.formaterRequete());
    }

    @Override
    public Groupedsi clone() throws CloneNotSupportedException {
        return (Groupedsi) super.clone();
    }


    public Long getIdGroupedsi() {
        return persistenceBean.getIdGroupedsi();
    }

    public void setIdGroupedsi(Long idGroupedsi) {
        persistenceBean.setIdGroupedsi(idGroupedsi);
    }

    public String getCode() {
        return persistenceBean.getCode();
    }

    public void setCode(String code) {
        persistenceBean.setCode(code);
    }

    public String getLibelle() {
        return persistenceBean.getLibelle();
    }

    public void setLibelle(String libelle) {
        persistenceBean.setLibelle(libelle);
    }

    public String getType() {
        return persistenceBean.getType();
    }

    public void setType(String type) {
        persistenceBean.setType(type);
    }

    public String getCodeStructure() {
        return persistenceBean.getCodeStructure();
    }

    public void setCodeStructure(String codeStructure) {
        persistenceBean.setCodeStructure(codeStructure);
    }

    public String getCodePageTete() {
        return persistenceBean.getCodePageTete();
    }

    public void setCodePageTete(String codePageTete) {
        persistenceBean.setCodePageTete(codePageTete);
    }

    public String getRoles() {
        return persistenceBean.getRoles();
    }

    public void setRoles(String roles) {
        persistenceBean.setRoles(roles);
    }

    public String getCodeGroupePere() {
        return persistenceBean.getCodeGroupePere();
    }

    public void setCodeGroupePere(String codeGroupePere) {
        persistenceBean.setCodeGroupePere(codeGroupePere);
    }

    public String getRequeteGroupe() {
        return persistenceBean.getRequeteGroupe();
    }

    public void setRequeteGroupe(String requeteGroupe) {
        persistenceBean.setRequeteGroupe(requeteGroupe);
    }

    public String getRequeteLdap() {
        return persistenceBean.getRequeteLdap();
    }

    public void setRequeteLdap(String requeteLdap) {
        persistenceBean.setRequeteLdap(requeteLdap);
    }

    public String getSourceImport() {
        return persistenceBean.getSourceImport();
    }

    public void setSourceImport(String sourceImport) {
        persistenceBean.setSourceImport(sourceImport);
    }

    public String getGestionCache() {
        return persistenceBean.getGestionCache();
    }

    public void setGestionCache(String gestionCache) {
        persistenceBean.setGestionCache(gestionCache);
    }

    public Long getDelaiExpirationCache() {
        return persistenceBean.getDelaiExpirationCache();
    }

    public void setDelaiExpirationCache(Long delaiExpirationCache) {
        persistenceBean.setDelaiExpirationCache(delaiExpirationCache);
    }

    public Long getDerniereMajCache() {
        return persistenceBean.getDerniereMajCache();
    }

    public void setDerniereMajCache(Long derniereMajCache) {
        persistenceBean.setDerniereMajCache(derniereMajCache);
    }

    public String getSelectionnable() {
        return persistenceBean.getSelectionnable();
    }

    public void setSelectionnable(String selectionnable) {
        persistenceBean.setSelectionnable(selectionnable);
    }
}
