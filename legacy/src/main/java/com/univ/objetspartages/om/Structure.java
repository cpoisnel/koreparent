package com.univ.objetspartages.om;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.services.ServiceStructure;

/**
 * The Class Structure.
 * @deprecated Utiliser la classe {@link StructureModele} avec le service {@link com.univ.objetspartages.services.ServiceStructure}
 */
@Deprecated
public class Structure {

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#checkCode(String, String)}
     */
    @Deprecated
    public static String checkCode(final String _code, final String _langue, final OMContext _ctx) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.checkCode(_code, _langue);
    }

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#checkCycle(String, String, String)}
     */
    @Deprecated
    public static void checkCycle(final String _code, final String _langue, final String _codeRattachement) throws ErreurApplicative {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        serviceStructure.checkCycle(_code, _langue, _codeRattachement);
    }

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#checkPermission(com.univ.objetspartages.om.AutorisationBean, String, String)}
     */
    @Deprecated
    public static boolean controlerPermission(final AutorisationBean autorisations, final String sPermission, final String codeStructure) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.checkPermission(autorisations, sPermission, codeStructure);
    }

    /**
     * @deprecated Cette méthode ne doit plus être utilisée. Elle renvoit désormais une chaine vide.
     */
    @Deprecated
    public static String getArbreJavaScript(String _code, final String _langue, final AutorisationBean autorisations, final String _sPermission, final String _sFiltre, final boolean front, String _codeRoot) {
        return StringUtils.EMPTY;
    }

    /**
     * Récupération du code de la composante d'une structure.
     *
     * @param _code
     *            the code
     * @param _langue
     *            the langue
     *
     * @return the structure premier niveau
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getStructurePremierNiveau(String, String)}
     */
    @Deprecated
    public static InfosStructure getStructurePremierNiveau(final String _code, final String _langue) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        try {
            final StructureModele structure = serviceStructure.getStructurePremierNiveau(_code, _langue);
            return new InfosStructure(structure);
        } catch (Exception e) {
            return new InfosStructure();
        }
    }

    @Deprecated
    public static StructureModele getFicheStructure(final OMContext _ctx, final String _code, final String _langue) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getByCodeLanguage(_code, _langue);
    }

    /**
     * Récupération d'une fiche structure en base à partir de son code et de sa langue si le contexte est de type ContexteUniv, seules les fiches en état "en ligne" seront
     * renvoyées sinon, si aucune fiche en ligne n'existe, une fiche dans un autre état sera renvoyée.
     *
     * @param _code
     *            code de la fiche
     * @param _langue
     *            langue de la fiche
     *
     * @return the fiche structure
     *
     * @throws Exception
     *             the exception
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getByCodeLanguage(String, String)}
     */
    @Deprecated
    public static StructureModele getFicheStructure(final String _code, final String _langue) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getByCodeLanguage(_code, _langue);
    }

    /**
     * @param _ctx
     * @param _code
     * @param _langue
     * @param _etatEnLigne
     * @return
     * @throws Exception
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getByCodeLanguage(String, String, boolean)}
     */
    @Deprecated
    public static StructureModele getFicheStructure(final OMContext _ctx, final String _code, final String _langue, final boolean _etatEnLigne) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getByCodeLanguage(_code, _langue, _etatEnLigne);
    }

    /**
     * Récupération d'une fiche structure en base à partir de son code et de sa langue.
     *
     * @param _code
     *            code de la fiche
     * @param _langue
     *            langue de la fiche
     * @param _etatEnLigne
     *            : permet de spécifier si on souhaite uniquement les fiches en ligne (true), ou non (false)
     *
     * @return the fiche structure
     *
     * @throws Exception
     *             the exception
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getByCodeLanguage(String, String, boolean)}
     */
    @Deprecated
    public static StructureModele getFicheStructure(final String _code, final String _langue, final boolean _etatEnLigne) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getByCodeLanguage(_code, _langue, _etatEnLigne);
    }

    @Deprecated
    public static String getLibelleAffichable(final OMContext _ctx, final String _codes, final String _langue) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getDisplayableLabel(_codes, _langue);
    }

    /**
     * Récupération du libellé de la structure.
     *
     * @param _codes
     *            the _codes
     * @param _langue
     *            the langue
     *
     * @return the libelle affichable
     *
     * @throws Exception
     *             the exception
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getDisplayableLabel(String, String)}
     */
    @Deprecated
    public static String getLibelleAffichable(final String _codes, final String _langue) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getDisplayableLabel(_codes, _langue);
    }

    /**
     * Récupération du libellé de la composante d'une structure.
     *
     * @param code
     *            the code
     * @param langue
     *            the langue
     *
     * @return the libelle structure premier niveau
     *
     * @throws Exception
     *             the exception
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getFirstLevelLabel(String, String)}
     */
    @Deprecated
    public static String getLibelleStructurePremierNiveau(final String code, final String langue) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getFirstLevelLabel(code, langue);
    }

    /**
     * Récupération du libellé de la composante d'une structure.
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the code
     * @param langue
     *            the langue
     *
     * @return the libelle structure premier niveau
     *
     * @throws Exception
     *             the exception
     * @deprecated utilisez {@link com.univ.objetspartages.services.ServiceStructure#getFirstLevelLabel(String, String)} le contexte ne sert à rien
     */
    @Deprecated
    public static String getLibelleStructurePremierNiveau(final OMContext ctx, final String code, final String langue) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getFirstLevelLabel(code, langue);
    }

    /**
     * Récupération du libellé de rattachement (Concaténation composante Département ...)
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the code
     * @param langue
     *            the langue
     * @param inclureStructureDepart
     *            the inclure structure depart
     *
     * @return the libelle rattachement
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert plus, utiliser {@link com.univ.objetspartages.services.ServiceStructure#getAttachementLabel(String, String, boolean)}
     */
    @Deprecated
    public static String getLibelleRattachement(final OMContext ctx, final String code, final String langue, final boolean inclureStructureDepart) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getAttachementLabel(code, langue, inclureStructureDepart);
    }

    /**
     * Récupération du libellé de rattachement (Concaténation composante Département ...)
     *
     * @param code
     *            the code
     * @param langue
     *            the langue
     * @param inclureStructureDepart
     *            the inclure structure depart
     *
     * @return the libelle rattachement
     *
     * @throws Exception
     *            the exception
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getAttachementLabel(String, String, boolean)}
     */
    @Deprecated
    public static String getLibelleRattachement(final String code, final String langue, final boolean inclureStructureDepart) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getAttachementLabel(code, langue, inclureStructureDepart);
    }

    /**
     * @deprecated Ne plus utiliser cette méthode. Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getAllStructures()}
     */
    @Deprecated
    public static HashMap<String, ElementArboStructure> getListeStructures() {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final HashMap<String, ElementArboStructure> arbos = new HashMap<>();
        for (StructureModele currentStructure : serviceStructure.getAllStructures().values()) {
            final InfosStructure currentInfos = new InfosStructure(currentStructure);
            final ElementArboStructure currentArbo = new ElementArboStructure(currentInfos.getCode());
            currentArbo.addInfosStructure(currentInfos);
            arbos.put(currentArbo.getCode(), currentArbo);
        }
        return arbos;
    }

    /**
     * Renvoie la structure de plus haut niveau (mère des structures de 1er niveau).
     *
     * @param codeRoot
     *            the code root
     * @param _langue
     *            the langue
     * @return Un InfosStructure
     * @deprecated Ne plus utiliser cette méthode
     */
    @Deprecated
    public static InfosStructure getTopLevelStructure(final String codeRoot, final String _langue) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return new InfosStructure(serviceStructure.getByCodeLanguage(StringUtils.EMPTY, _langue));
    }

    /**
     * Renvoie la structure de plus haut niveau (mère des structures de 1er niveau).
     *
     * @param _langue
     *            the langue
     *
     * @return Un InfosStructure
     * @deprecated Ne plus utiliser cette méthode
     */
    @Deprecated
    public static InfosStructure getTopLevelStructure(final String _langue) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return new InfosStructure(serviceStructure.getByCodeLanguage(StringUtils.EMPTY, _langue));
    }

    /**
     * Récupération des composantes en front (si front, filtre les structures non visibles).
     *
     * @param _langue
     *            the langue
     * @param front
     *            the front
     *
     * @return the liste structures premier niveau
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getSubStructures(String, String, boolean)}
     */
    @Deprecated
    public static Hashtable<String, String> getListeStructuresPremierNiveau(final String _langue, final boolean front) {
        final Hashtable<String, String> res = new Hashtable<>();
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        for (StructureModele currentStructure : serviceStructure.getSubStructures(StringUtils.EMPTY, _langue, front)) {
            res.put(currentStructure.getCode(), currentStructure.getLibelleAffichable());
        }
        return res;
    }

    /**
     * Récupération d'une structure stockée en mémoire.
     *
     * @param code
     *            the code
     *
     * @return the infos structure
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getByCodeLanguage(String, String)} (en plaçant le deuxième paramètre à "0")
     */
    @Deprecated
    public static InfosStructure renvoyerItemStructure(final String code) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return new InfosStructure(serviceStructure.getByCodeLanguage(code, LangueUtil.getIndiceLocaleDefaut()));
    }

    /**
     * Récupération de toutes les structures avec leur chemin complet.
     *
     * @param _ctx
     *            the _ctx
     *
     * @return the liste structure par intitule complet
     *
     * @throws Exception
     *             the exception
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getStructureListWithFullLabels()}
     */
    @Deprecated
    public static Hashtable<String, String> getListeStructureParIntituleComplet(final OMContext _ctx) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getStructureListWithFullLabels();
    }

    /**
     * Récupération du libellé de la structure.
     *
     * @param _ctx
     *            the _ctx
     * @param _codes
     *            the _codes
     * @param _langue
     *            the langue
     *
     * @return the libelle affichable long
     *
     * @throws Exception
     *             the exception
     * @deprecated Méthode plus utilisée
     */
    @Deprecated
    public static String getLibelleAffichableLong(final OMContext _ctx, final String _codes, final String _langue) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        String res = "";
        if ((_codes == null) || (_codes.length() == 0)) {
            return res;
        }
        final StringTokenizer st = new StringTokenizer(_codes, ";");
        while (st.hasMoreTokens()) {
            final String code = st.nextToken();
            // VIN 23092004 Refonte des structures
            final StructureModele structure = serviceStructure.getByCodeLanguage(code, _langue);
            if (res.length() > 0) {
                res += ";";
            }
            if (structure != null) {
                res += structure.getLibelleLong();
            } else {
                res += '-';
            }
        }
        if (res.length() == 0) {
            res = "-";
        }
        return res;
    }

    /**
     * Récupération d'une structure stockée en mémoire.
     *
     * @param code
     *            the code
     * @param langue
     *            the langue
     * @return the infos structure
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getByCodeLanguage(String, String)}
     */
    @Deprecated
    public static InfosStructure renvoyerItemStructure(final String code, final String langue) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final StructureModele structure = serviceStructure.getByCodeLanguage(code, langue);
        return new InfosStructure(structure);
    }

    /**
     * Gets the fil ariane.
     *
     * @param codeStructure
     *            the code structure
     * @param langue
     *            the langue
     *
     * @return the fil ariane
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getBreadCrumbs(String, String)}
     */
    @Deprecated
    public static String getFilAriane(final String codeStructure, final String langue) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getBreadCrumbs(codeStructure, langue);
    }

    /**
     * @deprecated Ne plus utiliser cette méthode.
     */
    @Deprecated
    public static void forcerRechargement() {
        // Nothing to do, retro-compat only
    }
}
