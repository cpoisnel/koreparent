package com.univ.objetspartages.om;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.sgbd.SiteDB;

/**
 * The Class Site.
 * @deprecated le bean a été remplacé par {@link com.univ.objetspartages.bean.SiteExterneBean} et les méthodes de service par {@link com.univ.objetspartages.services.ServiceSiteExterne}
 */
@Deprecated
public class Site extends SiteDB {

    private static final Logger LOGGER = LoggerFactory.getLogger(Site.class);

    private String etatIndexation;

    /**
     * Renvoie la liste des sites.
     *
     * @param ctx
     *            the ctx
     *
     * @return the liste sites
     *
     * @throws Exception
     *             the exception
     */
    public static List<Site> getListeSites(final OMContext ctx) throws Exception {
        final Site site = new Site();
        site.setCtx(ctx);
        final int count = site.select(" order by CODE");
        final List<Site> listeSites = new ArrayList<Site>(count);
        while (site.nextItem()) {
            listeSites.add((Site) site.clone());
        }
        return listeSites;
    }

    public String getEtatIndexation() {
        return etatIndexation;
    }

    public void setEtatIndexation(final String etatIndexation) {
        this.etatIndexation = etatIndexation;
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdSite(new Long(System.currentTimeMillis()));
        setCode(String.valueOf(System.currentTimeMillis()));
        setLibelle("");
        setUrl("");
        setLangue("0");
        setRegExpAccepte("");
        setRegExpRefuse("");
        setNiveauProfondeur(new Integer(-1));
    }

    /**
     * Fait une copie des proprietes d'un site. Utilise pour l'empilement de site a indexer.
     *
     * @return copie du site
     */
    @Override
    public Object clone() {
        final Site siteDest = new Site();
        siteDest.init();
        siteDest.setCode(getCode());
        siteDest.setIdSite(new Long(getIdSite().longValue()));
        siteDest.setLangue(getLangue());
        siteDest.setLibelle(getLibelle());
        siteDest.setRegExpAccepte(getRegExpAccepte());
        siteDest.setRegExpRefuse(getRegExpRefuse());
        siteDest.setNiveauProfondeur(new Integer(getNiveauProfondeur().intValue()));
        siteDest.setUrl(getUrl());
        return siteDest;
    }

    /**
     * Recopie.
     *
     * @param siteSrc
     *            the site src
     *
     * @return the site
     *
     * @deprecated : utiliser la methode standard clone
     */
    @Deprecated
    public Site recopie(final Site siteSrc) {
        return (Site) siteSrc.clone();
    }

}
