package com.univ.objetspartages.om;

import com.univ.objetspartages.sgbd.DemandeabonnementDB;
// TODO: Auto-generated Javadoc

/**
 * The Class Demandeabonnement.
 * @deprecated les demandes d'abonnements sont maintenant gérer via l'extension newsletter
 */
@Deprecated
public class Demandeabonnement extends DemandeabonnementDB {

    /**
     * Instantiates a new demandeabonnement.
     */
    public Demandeabonnement() {
        super();
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdDemandeAbonnement((long) 0);
        setCodeUtilisateur("");
        setAdresseMailUtilisateur("");
        setListeModelesMails("");
    }
}
