package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Contient les informations sur les structures dans chaque langue pour un élément de l'arborescence.
 * @deprecated Ne plus utiliser cette méthode
 */
@Deprecated
public class ElementArboStructure implements Serializable {

    private static final long serialVersionUID = 16558328191942227L;

    /** The code. */
    private String code = "";

    /** hashtable contenant les infosStructures de chaque langue : { 0, infosStructure} la clé est la langue, et la valeur est un infosStructure. */
    private Map<String, InfosStructure> listeInfosStructure = null;

    /**
     * Commentaire relatif au constructeur ElementArboStructure.
     *
     * @param code
     *            the code
     */
    public ElementArboStructure(final String code) {
        this.code = code;
        listeInfosStructure = new HashMap<>();
    }

    /**
     * Renvoit le infosStructure de la langue demandée Date de création : (01/12/2003 11:07:29).
     *
     * @param infosStructure
     *            the infos structure
     *
     */
    public void addInfosStructure(final InfosStructure infosStructure) {
        listeInfosStructure.put(infosStructure.getLangue(), infosStructure);
    }

    /**
     * renvoit le code de la structure Date de création : (01/12/2003 11:03:31).
     *
     * @return String
     */
    public String getCode() {
        return code;
    }

    /**
     * Renvoit le infosStructure de la langue demandée Date de création : (01/12/2003 11:07:29).
     *
     * @param langue
     *            the langue
     *
     * @return com.univ.objetspartages.om.InfosStructure
     */
    public InfosStructure getInfosStructure(final String langue) {
        InfosStructure structure = null;
        if (listeInfosStructure.get(langue) == null) {
            structure = listeInfosStructure.get("0");
        } else {
            structure = listeInfosStructure.get(langue);
        }
        return structure;
    }

    /**
     * Renvoie la liste des InfosStructure pour toutes les langues.
     *
     * @return the liste infos structure
     */
    public Map<String, InfosStructure> getListeInfosStructure() {
        return listeInfosStructure;
    }
}
