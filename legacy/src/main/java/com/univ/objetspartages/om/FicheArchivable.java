package com.univ.objetspartages.om;

import java.util.Date;

/**
 * @deprecated Toutes les fiches sont désormais archivable et les informations de date sont stockées directement sur le meta.
 * Cette interface est donc inutile.
 */
@Deprecated
public interface FicheArchivable {

    Date getDateArchivage();
}
