package com.univ.objetspartages.om;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.dao.impl.RessourceDAO;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.FileUtil;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * The Class Ressource.
 * @deprecated Utilisez {@link RessourceBean} et {@link ServiceRessource}
 */
@Deprecated
public class Ressource extends AbstractOm<RessourceBean, RessourceDAO> implements Cloneable {

    private static final String COLONNE_CODE_PARENT = "CODE_PARENT";

    /** The media. */
    private MediaBean media;

    private static ServiceRessource getServiceRessource(){
        return ServiceManager.getServiceForBean(RessourceBean.class);
    }

    /**
     * Suppression des fichiers d'une fiche.
     *
     * @param codeparent
     *            the _codeparent
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#deleteFiles(String)}
     */
    @Deprecated
    public static void supprimerFichier(final String codeparent) throws Exception {
        getServiceRessource().deleteFiles(codeparent);
    }

    private static ClauseWhere whereIdMediaEgal(final Long idMedia) {
        return new ClauseWhere(ConditionHelper.egalVarchar("ID_MEDIA", String.valueOf(idMedia)));
    }

    private static ClauseWhere whereCodeParentEgal(final String codeParent) {
        return new ClauseWhere(ConditionHelper.egalVarchar(COLONNE_CODE_PARENT, codeParent));
    }

    /**
     * suppression des fichiers d'une fiche.
     *
     * @param ctx
     *            the _ctx
     * @param fiche
     *            the _fiche
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#deleteFiles(FicheUniv)}
     */
    @Deprecated
    public static void supprimerFichiers(final OMContext ctx, final FicheUniv fiche) throws Exception {
        getServiceRessource().deleteFiles(fiche);
    }

    /**
     * suppression des fichiers d'une fiche.
     *
     * @param fiche
     *            the fiche
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#deleteFiles(FicheUniv)}
     */
    @Deprecated
    public static void supprimerFichiers(final FicheUniv fiche) throws Exception {
        getServiceRessource().deleteFiles(fiche);
    }

    /**
     * Synchroniser liste fichier.
     *
     * @param ctx
     *            the _ctx
     * @param fiche
     *            the _fiche
     * @param chaineFichier
     *            the _chaine fichier
     * @param modeFichier
     *            the mode fichier
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#syncFiles(String, String)}
     */
    @Deprecated
    public static void synchroniserListeFichier(final OMContext ctx, final FicheUniv fiche, final String chaineFichier, final String modeFichier, final int indice) throws Exception {
        final String codeParent = fiche.getIdFiche() + ",TYPE=" + ReferentielObjets.getCodeObjet(fiche) + ",NO=" + indice;
        getServiceRessource().syncFiles(codeParent, chaineFichier);
    }

    /**
     * duplication des fichiers sur une newsletter pour faire l'apercu methode utilisee dans SaisieModeleMail pour laquelle il n'y a pas de ficheUniv.
     *
     * @param ctx
     *            the _ctx
     * @param codeParent
     *            the _code parent
     * @param chaineFichier
     *            the _chaine fichier
     * @param modeFichier
     *            the mode fichier
     * @param apercu
     *            the _apercu
     *
     * @throws Exception
     *             the exception
     */
    public static void synchroniserListeFichier(final OMContext ctx, final String codeParent, final String chaineFichier, final String modeFichier, final boolean apercu) throws Exception {
        getServiceRessource().syncFiles(codeParent, chaineFichier);
    }

    /**
     * duplication du fichier unique sur la nouvelle fiche.
     *
     * @param ctx
     *            the _ctx
     * @param fiche
     *            the _fiche
     * @param chaineFichier
     *            the _chaine fichier
     * @param modeFichier
     *            the mode fichier
     * @param indice
     *            the indice
     *
     * @throws Exception
     *             the exception
     * @deprecated Utiliser {@link ServiceRessource#syncFile(FicheUniv, String, int)}
     */
    @Deprecated
    public static void synchroniserFichier(final OMContext ctx, final FicheUniv fiche, final String chaineFichier, final String modeFichier, final int indice) throws Exception {
        getServiceRessource().syncFile(fiche, chaineFichier, indice);
    }

    /**
     * Save content ressource.
     *
     * @param _ctx
     *            the _ctx
     * @param infoBean
     *            the _info bean
     * @param ficheUniv
     *            the _fiche univ
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#saveContentResource(java.util.Map, FicheUniv)}
     */
    @Deprecated
    public static void saveContentRessource(final OMContext _ctx, final Map<String, Object> infoBean, final FicheUniv ficheUniv) throws Exception {
        getServiceRessource().saveContentResource(infoBean, ficheUniv);
    }

    /**
     * Synchroniser liste fichier.
     *
     * @param _ctx
     *            the _ctx
     * @param codeParent
     *            the _code parent
     * @param chaineFichier
     *            the _chaine fichier
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#syncFiles(String, String)}
     */
    @Deprecated
    public static void synchroniserListeFichier(final OMContext _ctx, final String codeParent, final String chaineFichier) throws Exception {
        getServiceRessource().syncFiles(codeParent, chaineFichier);
    }

    /**
     * Synchroniser fichier : duplication du fichier unique sur un objet non fiche.
     *
     * @param ctx
     *            the _ctx
     * @param chaineFichier
     *            the _chaine fichier
     * @param codeParent
     *            the code parent
     * @param modeFichier
     *            the mode fichier
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#syncFile(String, String)}
     */
    @Deprecated
    public static void synchroniserFichier(final OMContext ctx, final String chaineFichier, final String codeParent, final String modeFichier) throws Exception {
        getServiceRessource().syncFile(codeParent, chaineFichier);
    }

    /**
     * Duplication de tous les fichiers d'une fiche
     *
     * @param ctx
     *            the _ctx
     * @param ficheOrigine
     *            the _fiche origine
     * @param ficheNouvelle
     *            the _fiche nouvelle
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#duplicateFiles(FicheUniv, FicheUniv)}
     */
    @Deprecated
    public static void dupliquerListeFichier(final OMContext ctx, final FicheUniv ficheOrigine, final FicheUniv ficheNouvelle) throws Exception {
        getServiceRessource().duplicateFiles(ficheOrigine, ficheNouvelle);
    }

    /**
     * Duplication de tous les fichiers d'une fiche
     *
     * @param ficheOrigine
     *            La fiche à laquelle appartiennent les fichiers d'origines
     * @param ficheNouvelle
     *            la nouvelle fiche
     *
     * @throws Exception
     *             Lors des requêtes en bdd
     * @deprecated Utilisez {@link ServiceRessource#duplicateFiles(FicheUniv, FicheUniv)}
     */
    @Deprecated
    public static void dupliquerListeFichier(final FicheUniv ficheOrigine, final FicheUniv ficheNouvelle) throws Exception {
        getServiceRessource().duplicateFiles(ficheOrigine, ficheNouvelle);
    }

    /**
     * Duplication d'un fichier sur un autre
     *
     * @param _ctx
     *            the _ctx
     * @param _fichiergw
     *            the _fichiergw
     * @param _codeParent
     *            the _code parent
     * @param _etat
     *            the _etat
     *
     * @return the ressource
     *
     * @throws Exception
     *             the exceptio
     * @deprecated le contexte n'ext pas nécessaire. il faut utiliser {@link Ressource#dupliquerFichier(Ressource, String, String)}
     */
    @Deprecated
    public static Ressource dupliquerFichier(final OMContext _ctx, final Ressource _fichiergw, final String _codeParent, final String _etat) throws Exception {
        return dupliquerFichier(_fichiergw, _codeParent, _etat);
    }

    /**
     * Duplication d'un fichier sur un autre
     *
     * @param fichiergw
     *            le fichier à dupliquer
     * @param codeParent
     *            le code parent de la ressource dupliquée
     * @param etat
     *            l'état de la ressource dupliquée
     *
     * @return la ressource dupliquée
     *
     * @throws Exception
     *             Lors de la requête en base
     * @deprecated Utilisez {@link ServiceRessource#duplicateFile(RessourceBean, String, String)}
     */
    @Deprecated
    public static Ressource dupliquerFichier(final Ressource fichiergw, final String codeParent, String etat) throws Exception {
        final Ressource ressource = new Ressource();
        ressource.setPersistenceBean(getServiceRessource().duplicateFile(fichiergw.getPersistenceBean(), codeParent, etat));
        return ressource;
    }

    /**
     * Renvoie la liste des fichiers globale d'une fiche tout type confondu sauf vignette.
     *
     * @param ctx
     *            the _ctx
     * @param fiche
     *            the _fiche
     *
     * @return the liste totale
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#getFullFileList(FicheUniv)}
     */
    @Deprecated
    public static Vector<Ressource> getListeTotale(final OMContext ctx, final FicheUniv fiche) throws Exception {
        return getListeTotale(fiche);
    }

    /**
     * Renvoie la liste des fichiers globale d'une fiche tout type confondu sauf vignette.
     *
     * @param fiche
     *            la fiche dont on veut récupérer les fichiers
     *
     * @return la liste totale des ressources liés à cette fiches
     *
     * @throws Exception
     *             the exception lors de la requête en base
     * @deprecated Utilisez {@link ServiceRessource#getFullFileList(FicheUniv)}
     */
    @Deprecated
    public static Vector<Ressource> getListeTotale(final FicheUniv fiche) throws Exception {
        final List<RessourceBean> ressources = getServiceRessource().getFullFileList(fiche);
        final Vector<Ressource> results = new Vector<>();
        for(RessourceBean currentRessource : ressources) {
            final Ressource ressource = new Ressource();
            ressource.setPersistenceBean(currentRessource);
            results.add(ressource);
        }
        return results;
    }

    /**
     * Renvoie la liste des fichiers reliees directement a une fiche (liste de n fichier).
     *
     * @param ctx
     *            the _ctx
     * @param fiche
     *            the _fiche
     *
     * @return the liste fichier
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceRessource#getFiles(FicheUniv)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichier(final OMContext ctx, final FicheUniv fiche) throws Exception {
        return getListeFichier(fiche);
    }

    /**
     * Renvoie la liste des fichiers reliees directement a une fiche (liste de n fichier).
     *
     * @param fiche
     *            la fiche dont on veut avoir les ressources
     *
     * @return La liste des fichiers liés à la fiche
     *
     * @throws Exception
     *             En cas d'exception sur la requête en base
     * @deprecated Utilisez {@link ServiceRessource#getFiles(FicheUniv)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichier(final FicheUniv fiche) throws Exception {
        final List<RessourceBean> ressources = getServiceRessource().getFiles(fiche);
        final Vector<Ressource> results = new Vector<>();
        for(RessourceBean currentRessource : ressources) {
            final Ressource ressource = new Ressource();
            ressource.setPersistenceBean(currentRessource);
            results.add(ressource);
        }
        return results;
    }

    /**
     *
     * @param _ctx
     * @param codeParent
     * @return
     * @throws Exception
     * @deprecated le contexte n'est pas necessaire dans ce cas présent il ne sert qu'a avoir la connexion à la bdd. Il faut utiliser
     *             {@link Ressource#getListeFichierByCodeParent(String)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichierByCodeParent(final OMContext _ctx, final String codeParent) throws Exception {
        return getListeFichierByCodeParent(codeParent);
    }

    /**
     * Renvoie la liste des fichiers reliees directement au code parent fourni en paramètre
     *
     * @param codeParent
     *            le code parent dont on veut avoir les fichiers liés
     * @return La liste des fichiers liés à la fiche
     * @throws Exception
     *             En cas d'exception sur la requête en base
     * @deprecated Utilisez {@link ServiceRessource#getFilesOrderBy(String, String)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichierByCodeParent(final String codeParent) throws Exception {
        final List<RessourceBean> ressources = getServiceRessource().getFilesOrderBy(codeParent, COLONNE_CODE_PARENT);
        final Vector<Ressource> results = new Vector<>();
        for(RessourceBean currentRessource : ressources) {
            final Ressource ressource = new Ressource();
            ressource.setPersistenceBean(currentRessource);
            results.add(ressource);
        }
        return results;
    }

    /**
     *
     * @param _ctx
     * @param _fiche
     * @param indice
     * @return
     * @throws Exception
     * @deprecated le contexte n'est pas necessaire dans ce cas présent il ne sert qu'a avoir la connexion à la bdd. Il faut utiliser
     *             {@link Ressource#getListeFichier(FicheUniv, String)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichier(final OMContext _ctx, final FicheUniv _fiche, final String indice) throws Exception {
        return getListeFichier(_fiche, indice);
    }

    /**
     * Renvoie la liste des fichiers reliees directement au champs d'indice fourni pour la fiche passé en paramètre
     *
     * @param fiche
     *            la fiche dont on veut avoir les ressources
     * @param indice
     *            l'indice du champ dont on veut avoir les ressources
     * @return La liste des fichiers liés à la fiche
     * @throws Exception
     *             En cas d'exception sur la requête en base
     */
    public static Vector<Ressource> getListeFichier(final FicheUniv fiche, final String indice) throws Exception {
        final String codeParent = fiche.getIdFiche() + ",TYPE=" + ReferentielObjets.getCodeObjet(fiche) + ",NO=" + indice;
        return getListeFichier(codeParent, COLONNE_CODE_PARENT);
    }

    /**
     * Renvoie la liste des fichiers indicès relièes directement à une fiche
     *
     * @param _ctx
     *            the _ctx
     * @param _fiche
     *            the _fiche
     *
     * @return the liste fichier indice
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte n'est pas necessaire dans ce cas présent il ne sert qu'a avoir la connexion à la bdd. Il faut utiliser
     *             {@link Ressource#getListeFichierIndice(FicheUniv)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichierIndice(final OMContext _ctx, final FicheUniv _fiche) throws Exception {
        return getListeFichierIndice(_fiche);
    }

    /**
     * Renvoie la liste des fichiers indicès relièes directement à une fiche
     *
     * @param fiche
     *            la fiche dont on veut avoir les ressources
     *
     * @return La liste des fichiers indicès liés à la fiche
     *
     * @throws Exception
     *             En cas d'exception sur la requête en base
     */
    public static Vector<Ressource> getListeFichierIndice(final FicheUniv fiche) throws Exception {
        final String codeParent = fiche.getIdFiche() + ",TYPE=FICHIER_" + ReferentielObjets.getCodeObjet(fiche) + "%";
        return getListeFichier(codeParent, COLONNE_CODE_PARENT);
    }

    /**
     *
     * @param _ctx
     * @param _fiche
     * @param indice
     * @return
     * @throws Exception
     * @deprecated le contexte n'est pas necessaire dans ce cas présent il ne sert qu'a avoir la connexion à la bdd. Il faut utiliser
     *             {@link Ressource#getListeFichierIndice(FicheUniv, String)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichierIndice(final OMContext _ctx, final FicheUniv _fiche, final String indice) throws Exception {
        return getListeFichierIndice(_fiche, indice);
    }

    public static Vector<Ressource> getListeFichierIndice(final FicheUniv fiche, final String indice) throws Exception {
        final String codeParent = fiche.getIdFiche() + ",TYPE=FICHIER_" + ReferentielObjets.getCodeObjet(fiche) + ",NO=" + indice;
        return getListeFichier(codeParent, COLONNE_CODE_PARENT);
    }

    /**
     * Gets the liste fichier.
     *
     * @param _ctx
     *            the _ctx
     * @param _codeParent
     *            the _code parent
     *
     * @return the liste fichier
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte n'est pas nécessaire. Il faut utiliser {@link Ressource#getListeFichier(String)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichier(final OMContext _ctx, final String _codeParent) throws Exception {
        return getListeFichier(_codeParent);
    }

    /**
     * Récupère la liste des fichiers liés au code parent fourni en paramètre
     *
     * @param codeParent
     *            Le code parent de la fiche dont on souhaite aboir les fichiers joints
     *
     * @return la liste fichier lié au code parent
     *
     * @throws Exception
     *             Lors de la requête en bdd
     */
    public static Vector<Ressource> getListeFichier(final String codeParent) throws Exception {
        return getListeFichier(codeParent, StringUtils.EMPTY);
    }

    /**
     * Renvoie la liste des fichiers correspondant aux criteres voulus.
     *
     * @param ctx
     *            the _ctx
     * @param codeParent
     *            the _code parent
     * @param orderBy
     *            the _order by
     *
     * @return the liste fichier
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte n'est pas nécessaire. Il faut utiliser {@link Ressource#getListeFichier(String, String)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichier(final OMContext ctx, final String codeParent, final String orderBy) throws Exception {
        return getListeFichier(codeParent, orderBy);
    }

    /**
     * Renvoie la liste des fichiers correspondant aux criteres voulus.
     *
     * @param codeParent
     *            Le code parent dont on souhaite récupérer les fichiers
     * @param orderBy
     *            Le tri à faire
     *
     * @return la liste des fichiers liés au code parent
     *
     * @throws Exception
     *             Lors de la requête en bdd
     * @deprecated Utilisez {@link ServiceRessource#getFilesOrderBy(String, String)}
     */
    @Deprecated
    public static Vector<Ressource> getListeFichier(final String codeParent, final String orderBy) throws Exception {
        final List<RessourceBean> ressources = getServiceRessource().getFilesOrderBy(codeParent, orderBy);
        final Vector<Ressource> liste = new Vector<>();
        for (RessourceBean currentRessource : ressources) {
            final Ressource fic = new Ressource();
            fic.setPersistenceBean(currentRessource);
            liste.add(fic);
        }
        return liste;
    }

    /**
     * Renvoie le fichier lié a un objet parent.
     *
     * @param _ctx
     *            the _ctx
     * @param _fiche
     *            the _fiche
     *
     * @return the fichier
     *
     * @deprecated le contexte n'est pas nécessaire. Il faut utiliser {@link Ressource#getFichier(FicheUniv)}
     */
    @Deprecated
    public static Ressource getFichier(final OMContext _ctx, final FicheUniv _fiche) {
        return getFichier(_fiche);
    }

    /**
     * Renvoie le fichier lié a un objet parent.
     *
     * @param fiche
     *            la fiche dont on souhaite récupérer le fichier
     *
     * @return le fichier lié
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceRessource#getFile(com.univ.objetspartages.om.FicheUniv)}
     */
    @Deprecated
    public static Ressource getFichier(final FicheUniv fiche) {
        return getFichier(fiche, "1");
    }

    /**
     * Renvoie le fichier lié a un objet parent.
     *
     * @param ctx
     *            the _ctx
     * @param fiche
     *            the _fiche
     * @param indice
     *            the indice
     *
     * @return the fichier
     *
     * @deprecated le contexte n'est pas nécessaire. Il faut utiliser {@link Ressource#getFichier(FicheUniv, String)}
     */
    @Deprecated
    public static Ressource getFichier(final OMContext ctx, final FicheUniv fiche, final String indice) {
        final Ressource fichiergw = new Ressource();
        fichiergw.setPersistenceBean(getServiceRessource().getFile(fiche, indice));
        return fichiergw;
    }

    /**
     * Renvoie le fichier lié a un objet parent.
     *
     * @param fiche
     *            La fiche surlaquelle on souhaite requeter
     * @param indice
     *            l'indice du champ du fichier
     *
     * @return le fichier lié ou null si rien n'a été trouvé
     * @deprecated Utilisez {@link ServiceRessource#getByCodeParent(String)}
     */
    @Deprecated
    public static Ressource getFichier(final FicheUniv fiche, final String indice) {
        final Ressource fichiergw = new Ressource();
        fichiergw.setPersistenceBean(getServiceRessource().getFile(fiche, indice));
        return fichiergw;
    }

    /**
     * Renvoie le fichier lie a un objet parent.
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the _code
     *
     * @return the fichier
     *
     * @deprecated le contexte n'est pas nécessaire. Il faut utiliser {@link Ressource#getFichier(String)}
     */
    @Deprecated
    public static Ressource getFichier(final OMContext ctx, final String code) {
        final Ressource fichiergw = new Ressource();
        fichiergw.setPersistenceBean(getServiceRessource().getFile(code));
        return fichiergw;
    }

    /**
     * Renvoie le fichier lié au code fourni
     *
     * @param codeParentOuId
     *            Le code parent ou l'id de la ressources...
     *
     * @return le fichier lié ou null si rien n'a été trouvé
     * @deprecated Utiliser {@link ServiceRessource#getByCodeParent(String)} ou {@link ServiceRessource#getById(Long)}
     */
    @Deprecated
    public static Ressource getFichier(final String codeParentOuId) {
        final Ressource fichiergw = new Ressource();
        if (codeParentOuId.contains("TYPE=")) {
            final List<RessourceBean> ressourceBean = getServiceRessource().getByCodeParent(codeParentOuId);
            fichiergw.setPersistenceBean(ressourceBean.get(0));
            return fichiergw;
        }
        // soit un id
        else {
            final RessourceBean ressourceBean = getServiceRessource().getById(new Long(codeParentOuId));
            fichiergw.setPersistenceBean(ressourceBean);
            return fichiergw;
        }
    }

    /**
     * Initialisation de l'objet.
     * @deprecated Plus nécessaire
     */
    @Deprecated
    public void init() {
    }

    /**
     * Gets the path absolu.
     *
     * @return le path physique absolu du fichier
     * @deprecated Utilisez {@link RessourceUtils#getPathAbsolu(RessourceBean)}
     */
    @Deprecated
    public String getPathAbsolu() {
        return RessourceUtils.getPathAbsolu(this.persistenceBean);
    }

    /**
     * Checks if is photo.
     *
     * @return vrai si le fichier est du type photo
     * @deprecated Utilisez {@link MediaUtils#isPhoto(MediaBean)}
     */
    @Deprecated
    @JsonIgnore
    public boolean isPhoto() {
        return "photo".equalsIgnoreCase(getMedia().getTypeRessource());
    }

    /**
     * Checks if is photo.
     *
     * @return vrai si le fichier est du type photo
     * @deprecated Inutile
     */
    @Deprecated
    public Ressource getPhoto() {
        if (isPhoto()) {
            return this;
        } else {
            return new Ressource();
        }
    }

    /**
     * Gets the url photo.
     *
     * @return l'url de la photo
     * @deprecated Utilisez {@link com.univ.objetspartages.util.RessourceUtils#getUrlPhoto}
     */
    @Deprecated
    public String getUrlPhoto() {
        return RessourceUtils.getUrlPhoto(this.persistenceBean);
    }

    /**
     * Gets the largeur photo.
     *
     * @return la largeur de la photo
     * @deprecated Inutile
     */
    @Deprecated
    public int getLargeurPhoto() {
        int largeur = 0;
        if (isPhoto()) {
            try {
                final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
                largeur = Integer.parseInt(serviceMedia.getSpecificData(getMedia(), MediaPhoto.ATTRIBUT_LARGEUR));
            } catch (final Exception e) {
                largeur = MediaPhoto.getCritereLimite().getLargeur();
            }
        }
        return largeur;
    }

    /**
     * Gets the largeur vignette.
     *
     * @return la largeur de la vignette
     * @deprecated Inutile
     */
    @Deprecated
    public int getLargeurVignette() {
        return MediaPhoto.getCritereVignette().getLargeur();
    }

    /**
     * Gets the hauteur photo.
     *
     * @return la hauteur de la photo
     * @deprecated Inutile
     */
    @Deprecated
    public int getHauteurPhoto() {
        int hauteur = 0;
        if (isPhoto()) {
            try {
                final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
                hauteur = Integer.parseInt(serviceMedia.getSpecificData(getMedia(), MediaPhoto.ATTRIBUT_HAUTEUR));
            } catch (final Exception e) {
                hauteur = MediaPhoto.getCritereLimite().getHauteur();
            }
        }
        return hauteur;
    }

    /**
     * Gets the hauteur vignette.
     *
     * @return la hauteur de la vignette
     * @deprecated Utilisez {@link MediaPhoto#getCritereVignette()}
     */
    @Deprecated
    public int getHauteurVignette() {
        return MediaPhoto.getCritereVignette().getHauteur();
    }

    /**
     * Checks if is local.
     *
     * @return true, if is local
     * @deprecated Utilisez {@link MediaUtils#isLocal(MediaBean)}
     */
    @Deprecated
    public boolean isLocal() {
        boolean bLocal = false;
        try {
            new URL(getMedia().getUrl());
        } catch (final MalformedURLException e) {
            if (!getMedia().getUrl().contains("/")) {
                bLocal = true;
            }
        }
        return bLocal;
    }

    /**
     * Gets the media.
     *
     * @return the media
     * @deprecated Utilisez {@link ServiceMedia#getById(Long)}
     */
    @Deprecated
    public MediaBean getMedia() {
        if (media == null && this.persistenceBean.getIdMedia() != null && this.persistenceBean.getIdMedia() != 0) {
            final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
            media = serviceMedia.getById(this.persistenceBean.getIdMedia());
        }
        return media;
    }

    /**
     * Gets the auteur.
     *
     * @return the auteur
     * @deprecated Utilisez {@link RessourceUtils#getAuteur(RessourceBean)}
     */
    @Deprecated
    public String getAuteur() {
        return getMedia().getAuteur();
    }

    /**
     * Gets the code redacteur.
     *
     * @return the code redacteur
     * @deprecated Utilisez {@link MediaBean#getCodeRedacteur()}
     */
    @Deprecated
    public String getCodeRedacteur() {
        return getMedia().getCodeRedacteur();
    }

    /**
     * Gets the copyright.
     *
     * @return the copyright
     * @deprecated Utilisez {@link MediaBean#getCopyright()}
     */
    @Deprecated
    public String getCopyright() {
        return getMedia().getCopyright();
    }

    /**
     * Gets the date creation.
     *
     * @return the date creation
     * @deprecated Utilisez {@link MediaBean#getDateCreation()}
     */
    @Deprecated
    public Date getDateCreation() {
        return getMedia().getDateCreation();
    }

    /**
     * Gets the description.
     *
     * @return the description
     * @deprecated Utilisez {@link MediaBean#getDescription()}
     */
    @Deprecated
    public String getDescription() {
        return getMedia().getDescription();
    }

    /**
     * Gets the format.
     *
     * @return the format
     * @deprecated Utilisez {@link MediaBean#getFormat()}
     */
    @Deprecated
    public String getFormat() {
        return getMedia().getFormat();
    }

    /**
     * Gets the legende.
     *
     * @return the legende
     * @deprecated Utilisez {@link MediaBean#getLegende()}
     */
    @Deprecated
    public String getLegende() {
        return getMedia().getLegende();
    }

    /**
     * Gets the meta keywords.
     *
     * @return the meta keywords
     * @deprecated Utilisez {@link MediaBean#getMetaKeywords()}
     */
    @Deprecated
    public String getMetaKeywords() {
        return getMedia().getMetaKeywords();
    }

    /**
     * Gets the poids.
     *
     * @return the poids
     * @deprecated Utilisez {@link MediaBean#getPoids()}
     */
    @Deprecated
    public Long getPoids() {
        return getMedia().getPoids();
    }

    /**
     * Gets the specific data.
     *
     * @return the specific data
     * @deprecated Utilisez {@link MediaBean#getSpecificData()}
     */
    @Deprecated
    public String getSpecificData() {
        return getMedia().getSpecificData();
    }

    /**
     * Gets the source.
     *
     * @return the source
     * @deprecated Utilisez {@link MediaBean#getSource()}
     */
    @Deprecated
    public String getSource() {
        return getMedia().getSource();
    }

    /**
     * Gets the titre.
     *
     * @return the titre
     * @deprecated Utilisez {@link MediaBean#getTitre()}
     */
    @Deprecated
    public String getTitre() {
        return getMedia().getTitre();
    }

    /**
     * Checks if is securise.
     *
     * @return true, if is securise
     * @deprecated Utilisez {@link !MediaUtils#isPublic(MediaBean)}
     */
    @Deprecated
    public boolean isSecurise() {
        return !MediaUtils.isPublic(getMedia());
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     * @deprecated Utilisez {@link MediaUtils#getLibelleAffichable(MediaBean)}
     */
    @Deprecated
    public String getLibelle() {
        return MediaUtils.getLibelleAffichable(getMedia());
    }

    /**
     * Gets the type media.
     *
     * @return the type media
     * @deprecated Utilisez {@link MediaBean#getTypeMedia()}
     */
    @Deprecated
    public String getTypeMedia() {
        return getMedia().getTypeMedia();
    }

    /**
     * Gets the type ressource.
     *
     * @return the type ressource
     * @deprecated Utilisez {@link MediaBean#getTypeRessource()}
     */
    @Deprecated
    public String getTypeRessource() {
        return getMedia().getTypeRessource();
    }

    /**
     * Gets the url.
     *
     * @return the url
     * @deprecated Utilisez {@link MediaUtils#getUrlAbsolue(MediaBean)}
     */
    @Deprecated
    public String getUrl() {
        return MediaUtils.getUrlAbsolue(getMedia());
    }

    /**
     * Gets the url vignette.
     *
     * @return the url vignette
     * @deprecated Utilisez {@link MediaUtils#getUrlVignetteAbsolue(MediaBean)}
     */
    @Deprecated
    public String getUrlVignette() {
        return MediaUtils.getUrlVignetteAbsolue(getMedia());
    }

    /**
     * Retourne si possible l'extension de la ressource
     *
     * @return l'extension de la ressource, si le nom de la ressource se termine par un . ou un -
     * @deprecated Utilisez {@link FileUtil#getExtension(String)}
     */
    @Deprecated
    public String getExtension() {
        return FileUtil.getExtension(getMedia().getSource());
    }
}
