package com.univ.objetspartages.om;

/**
 * The Class InfosLibelle.
 */
@Deprecated
public class InfosLibelle {

    /** The id libelle. */
    private Long idLibelle = (long) 0;

    /** The type. */
    private String type = "";

    /** The code. */
    private String code = "";

    /** The libelle. */
    private String libelle = "";

    /** The langue. */
    private String langue = "";

    /**
     * Constructeur.
     *
     * @param libelle
     *            le libelle à partir duquel on construit l'objet InfosLibelle
     */
    public InfosLibelle(Libelle libelle) {
        this.idLibelle = libelle.getIdLibelle();
        this.type = libelle.getType();
        this.code = libelle.getCode();
        this.libelle = libelle.getLibelle();
        this.langue = libelle.getLangue();
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the id libelle.
     *
     * @return the id libelle
     */
    public Long getIdLibelle() {
        return idLibelle;
    }

    /**
     * Gets the langue.
     *
     * @return the langue
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    public String getLibelleAffichable() {
        if (getLibelle().startsWith("[")) {
            int i = getLibelle().indexOf("]") + 1;
            if (i != 0 && i != getLibelle().length()) {
                return getLibelle().substring(i);
            }
        }
        return getLibelle();
    }
}
