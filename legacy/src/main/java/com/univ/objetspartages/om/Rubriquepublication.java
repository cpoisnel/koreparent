package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.univ.multisites.InfosFicheReferencee;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.dao.impl.RubriquepublicationDAO;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.utils.ContexteDao;
import com.univ.utils.ContexteUniv;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * The Class Rubriquepublication.
 * @deprecated Cette classe ne sert plus, les données sont dans {@link RubriquepublicationBean} et les méthodes utilitaires dans {@link ServiceRubriquePublication}
 */
@Deprecated
public class Rubriquepublication extends AbstractOm<RubriquepublicationBean, RubriquepublicationDAO>  implements Cloneable {

    private static final Logger LOG = LoggerFactory.getLogger(Rubriquepublication.class);

    /**
     * Instantiates a new rubriquepublication.
     */
    public Rubriquepublication() {
        super();
    }

    /**
     * @deprecated cette méthode ne fait plus rien elle est présente pour la retro compat.
     * @param ctx
     */
    @Deprecated
    public void setCtx(OMContext ctx) {
        //pour retro compatibilite
    }
    /**
     * Gets the liste rubriques publication.
     *
     * @param _ctx
     *            the _ctx
     * @param fiche
     *            the fiche
     *
     * @return the liste rubriques publication
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert plus, utiliser {@link ServiceRubriquePublication#getRubriqueDestByFicheUniv(FicheUniv)}
     */
    @Deprecated
    public static Vector<String> getListeRubriquesPublication(final OMContext _ctx, final FicheUniv fiche) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        return new Vector<>(serviceRubriquePublication.getRubriqueDestByFicheUniv(fiche));
    }

    /**
     * Gets the liste rubriques publication.
     *
     * @param fiche
     *            the fiche
     *
     * @return the liste rubriques publication
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert plus, utiliser {@link ServiceRubriquePublication#getRubriqueDestByFicheUniv(FicheUniv)}
     */
    @Deprecated
    public static Vector<String> getListeRubriquesPublication(final FicheUniv fiche) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        return new Vector<>(serviceRubriquePublication.getRubriqueDestByFicheUniv(fiche));
    }

    /**
     * Gets the liste rubriques publication.
     *
     * @param _ctx le contexte ne sert plus
     * @param fiche
     *            the fiche
     * @param rubriqueRecherche
     *            the _rubrique recherche
     *
     * @return the liste rubriques publication
     *
     * @throws Exception
     *             the exception
     * @deprecated utiliser {@link ServiceRubriquePublication#getRubriqueDestByFicheUnivAndRubrique(FicheUniv, String)}
     */
    @Deprecated
    public static Vector<String> getListeRubriquesPublication(final OMContext _ctx, final FicheUniv fiche, final String rubriqueRecherche) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        return new Vector<>(serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubrique(fiche, rubriqueRecherche));
    }

    /**
     * Renvoie pour une fiche la liste des rubriques de publication. Il est également possible de préciser un périmètre de recherche sur les rubriques (ex : ne chercher que les
     * rubriques d'un site) le booleen permet d'identifier les rubriques automatiques #AUTO#+code (utilisé dans controleurUniv pour éviter la suppression de ces rubriques)
     *
     * @param _ctx
     *            the _ctx
     * @param fiche
     *            the fiche
     * @param rubriqueRecherche
     *            the _rubrique recherche
     * @param controleurUniv
     *            the _controleur univ
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert plus, utilisez {@link ServiceRubriquePublication#getRubriqueDestByFicheUnivAndRubriqueWithSourceControl(FicheUniv, String)}
     */
    @Deprecated
    public static Vector<String> getListeRubriquesPublication(final OMContext _ctx, final FicheUniv fiche, final String rubriqueRecherche, final boolean controleurUniv) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        Vector<String> results;
        if (controleurUniv) {
            results = new Vector<>(serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubriqueWithSourceControl(fiche,rubriqueRecherche));
        } else {
            results = new Vector<>(serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubrique(fiche, rubriqueRecherche));
        }
        return results;
    }

    /**
     * Renvoie pour une fiche la liste des rubriques de publication. Il est également possible de préciser un périmètre de recherche sur les rubriques (ex : ne chercher que les
     * rubriques d'un site) le booleen permet d'identifier les rubriques automatiques #AUTO#+code (utilisé dans controleurUniv pour éviter la suppression de ces rubriques)
     *
     * @param fiche
     *            the fiche
     * @param rubriqueRecherche
     *            the _rubrique recherche
     * @param controleurUniv
     *            the _controleur univ
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     */
    public static Vector<String> getListeRubriquesPublication(final FicheUniv fiche, final String rubriqueRecherche, final boolean controleurUniv) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        Vector<String> results;
        if (controleurUniv) {
            results = new Vector<>(serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubriqueWithSourceControl(fiche,rubriqueRecherche));
        } else {
            results = new Vector<>(serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubrique(fiche, rubriqueRecherche));
        }
        return results;
    }

    /**
     * Suppression des fiches référencées pour unr rubrique.
     *
     * @param ctx
     *            the _ctx
     * @param codeRubriquePublication
     *            the code rubrique publication
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte n'est pas utile, utiliser {@link ServiceRubriquePublication#deleteByRubriqueDestAndNoSource(String)}
     */
    @Deprecated
    public static void supprimerRubriquePublication(final OMContext ctx, final String codeRubriquePublication) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        serviceRubriquePublication.deleteByRubriqueDestAndNoSource(codeRubriquePublication);
    }

    /**
     * Suppression des fiches référencées pour unr rubrique.
     *
     * @param codeRubriquePublication
     *            the code rubrique publication
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     */
    public static void supprimerRubriquePublication(final String codeRubriquePublication) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        serviceRubriquePublication.deleteByRubriqueDestAndNoSource(codeRubriquePublication);
    }

    /**
     * Suppression des rubriques de publication d'une fiche.
     *
     * @param _ctx
     *            the _ctx
     * @param fiche
     *            the fiche
     * @param ajouterCritereRequete
     *            the _ajouter critere requete
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert plus, utiliser {@link ServiceRubriquePublication#deleteByFicheReference(InfosFicheReferencee)} si ajouterCritereRequete est à true, sinon {@link ServiceRubriquePublication#deleteByFicheReferenceWithoutSource(InfosFicheReferencee)}
     */
    @Deprecated
    public static void supprimerRubriquePublicationFiche(final OMContext _ctx, final InfosFicheReferencee fiche, final boolean ajouterCritereRequete) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        if (ajouterCritereRequete) {
            serviceRubriquePublication.deleteByFicheReference(fiche);
        } else {
            serviceRubriquePublication.deleteByFicheReferenceWithoutSource(fiche);
        }
    }

    /**
     * Suppression des rubriques de publication d'une fiche.
     *
     * @param fiche
     *            the fiche
     * @param ajouterCritereRequete
     *            the _ajouter critere requete
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert plus, utiliser {@link ServiceRubriquePublication#deleteByFicheReference(InfosFicheReferencee)} si ajouterCritereRequete est à true, sinon {@link ServiceRubriquePublication#deleteByFicheReferenceWithoutSource(InfosFicheReferencee)}
     */
    @Deprecated
    public static void supprimerRubriquePublicationFiche(final InfosFicheReferencee fiche, final boolean ajouterCritereRequete) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        if (ajouterCritereRequete) {
            serviceRubriquePublication.deleteByFicheReference(fiche);
        } else {
            serviceRubriquePublication.deleteByFicheReferenceWithoutSource(fiche);
        }
    }

    /**
     * Delete global.
     *
     * @param ctx
     *            the _ctx
     * @param ficheUniv
     *            the fiche univ
     *
     * @throws Exception
     *             the exception
     * @deprecated utiliser {@link ServiceRubriquePublication#deleteByFiche(FicheUniv)}
     */
    @Deprecated
    public static void deleteGlobal(final OMContext ctx, final FicheUniv ficheUniv) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        serviceRubriquePublication.deleteByFiche(ficheUniv);
    }

    /**
     * SUpprime l'ensemble des rubriques de publications de la fiche fourni en paramètre.
     *
     * @param ficheUniv
     *            la fiche dont on souhaite supprimer les rubriques de publications
     *
     */
    public static void deleteGlobal(final FicheUniv ficheUniv) {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        serviceRubriquePublication.deleteByFiche(ficheUniv);
    }

    /**
     * Enregistre pour une rubrique la liste des fiches référencées.
     *
     * @param ctx le contexte ne sert à rien
     * @param codeRubriquePublication
     *            the code rubrique publication
     * @param listeFiches
     *            the liste fiches
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated utiliser {@link ServiceRubriquePublication#saveAllFicheForRubPub(String, Collection)}
     */
    @Deprecated
    public static void enregistrerRubriquePublication(final OMContext ctx, final String codeRubriquePublication, final List<InfosFicheReferencee> listeFiches) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        serviceRubriquePublication.saveAllFicheForRubPub(codeRubriquePublication, listeFiches);
    }

    /*
     *  Methode de suppression globale des rubriques de publication d'une fiche
     *  appelé uniquement dans la méthode FicheUnivMgr.supprimerFiche
     */

    /**
     * Enregistre pour une rubrique la liste des fiches référencées.
     *
     * @param codeRubriquePublication
     *            the code rubrique publication
     * @param listeFiches
     *            the liste fiches
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated utiliser {@link ServiceRubriquePublication#saveAllFicheForRubPub(String, Collection)}
     */
    @Deprecated
    public static void enregistrerRubriquePublication(final String codeRubriquePublication, final List<InfosFicheReferencee> listeFiches) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        serviceRubriquePublication.saveAllFicheForRubPub(codeRubriquePublication, listeFiches);
    }

    /**
     * Enregistre pour une fiche la liste des rubriques référencées.
     *
     * @param ctx
     *            the _ctx
     * @param infosFiche
     *            the infos fiche
     * @param listeRubriques
     *            the liste rubriques
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert plus, utiliser {@link ServiceRubriquePublication#saveAllRubPubForFiche(InfosFicheReferencee, Collection)}
     */
    @Deprecated
    public static void enregistrerRubriquePublicationFiche(final OMContext ctx, final InfosFicheReferencee infosFiche, final Vector<String> listeRubriques) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        serviceRubriquePublication.saveAllRubPubForFiche(infosFiche, listeRubriques);
    }

    /**
     * Enregistre pour une fiche la liste des rubriques référencées.
     *
     * @param infosFiche
     *            the infos fiche
     * @param listeRubriques
     *            the liste rubriques
     *
     * @return @throws Exception
     *
     * @deprecated le contexte ne sert plus, utiliser {@link ServiceRubriquePublication#saveAllRubPubForFiche(InfosFicheReferencee, Collection)}
     */
    @Deprecated
    public static void enregistrerRubriquePublicationFiche(final InfosFicheReferencee infosFiche, final Vector<String> listeRubriques) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        serviceRubriquePublication.saveAllRubPubForFiche(infosFiche, listeRubriques);
    }

    /**
     * Renvoie pour rubrique la liste des fiches référéencés.
     *
     * @param ctx
     *            the _ctx
     * @param codeRubriquePublication
     *            the _code rubrique publication
     * @param avecInfosFiche
     *            the _avec infos fiche
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode n'utilise pas le contexte,, utliser {@link Rubriquepublication#getListeFichesReferences(String, boolean)}
     */
    @Deprecated
    public static ArrayList<InfosFicheReferencee> getListeFichesReferences(final OMContext ctx, final String codeRubriquePublication, final boolean avecInfosFiche) throws Exception {
        return new ArrayList<>(getListeFichesReferences(codeRubriquePublication, avecInfosFiche));
    }

    /**
     * Renvoie pour rubrique la liste des fiches référéencés.
     *
     * @param codeRubriquePublication
     *            the _code rubrique publication
     * @param avecInfosFiche
     *            the _avec infos fiche
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     */
    public static List<InfosFicheReferencee> getListeFichesReferences(final String codeRubriquePublication, final boolean avecInfosFiche) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        return serviceRubriquePublication.getListeFichesReferences(codeRubriquePublication, avecInfosFiche);
    }

    /**
     * Méthode qui teste si la fiche est rubriquée dans le site courant.
     *
     * @param _ctx
     *            the _ctx
     * @param _fiche
     *            the fiche
     *
     * @return the rubrique dans site courant
     *
     * @throws Exception
     *             the exception
     */
    public static String getRubriqueDansSiteCourant(final ContexteUniv _ctx, final FicheUniv _fiche) throws Exception {
        String codeRubriqueTemp = null;
        String codeRubrique = null;
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean infosRubCourante = serviceRubrique.getRubriqueByCode(_ctx.getCodeRubriqueHistorique());
        boolean rubriquePrincipaleAutorisee = false;
        boolean rubriquePublicationAutorisee = false;
        boolean possedeRubrique = false;
        //RP20070727 : on cherche en priorité la rubrique du site courant SI POSSIBLE dans la rubrique courante
        // Test sur rubrique principale
        String codeRubriquePrincipale = _fiche.getCodeRubrique();
        if (codeRubriquePrincipale.length() > 0) {
            possedeRubrique = true;
            final RubriqueBean rubriquePrincipale = serviceRubrique.getRubriqueByCode(codeRubriquePrincipale);
            if (rubriquePrincipale != null && serviceRubrique.controlerRestrictionRubrique(_ctx.getGroupesDsiAvecAscendants(), rubriquePrincipale.getCode())) {
                if (_ctx.getInfosSite().isRubriqueVisibleInSite(rubriquePrincipale)) {
                    codeRubriqueTemp = rubriquePrincipale.getCode();
                }
                rubriquePrincipaleAutorisee = true;
            }
        }
        codeRubriquePrincipale = null;
        // on sauvegarde la rubrique principale dans le site courant
        if (codeRubriqueTemp != null) {
            // si la rubrique principale est dans le site ET dans la rubrique courante on la renvoit
            codeRubriquePrincipale = codeRubriqueTemp;
            Collection<RubriqueBean> allChilds = serviceRubrique.getAllChilds(infosRubCourante.getCode());
            if (infosRubCourante.getCode().equals(codeRubriqueTemp) || allChilds.contains(serviceRubrique.getRubriqueByCode(codeRubriqueTemp))) {
                codeRubrique = codeRubriqueTemp;
            }
        }
        // test sur rubriques de publication
        if (codeRubrique == null) {
            final String codeRubriqueSite = _ctx.getInfosSite().getCodeRubrique();
            final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
            final Collection<String> listeRubriquesPublication = serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubrique(_fiche, codeRubriqueSite);
            for (String aListeRubriquesPublication : listeRubriquesPublication) {
                possedeRubrique = true;
                RubriqueBean rubriquePub = serviceRubrique.getRubriqueByCode(aListeRubriquesPublication);
                if (rubriquePub != null && serviceRubrique.controlerRestrictionRubrique(_ctx.getGroupesDsiAvecAscendants(), rubriquePub)) {
                    rubriquePublicationAutorisee = true;
                    if (_ctx.getInfosSite().isRubriqueVisibleInSite(rubriquePub)) {
                        codeRubriqueTemp = rubriquePub.getCode();
                        Collection<RubriqueBean> allChilds = serviceRubrique.getAllChilds(infosRubCourante.getCode());
                        if (infosRubCourante.getCode().equals(codeRubriqueTemp) || allChilds.contains(rubriquePub)) {
                            codeRubrique = codeRubriqueTemp;
                            break;
                        }
                    }
                }
            }
        }
        // si pas de rubrique de publication dans la rubrique courante
        if (codeRubrique == null) {
            // on renvoit en priorité la rubrique principale du site
            if (codeRubriquePrincipale != null) {
                codeRubrique = codeRubriquePrincipale;
                // sinon une des rubriques de publication
            } else {
                codeRubrique = codeRubriqueTemp;
            }
        }
        if (possedeRubrique && !rubriquePrincipaleAutorisee && !rubriquePublicationAutorisee) {
            _ctx.setCodeRubriquePageCourante(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE);
        }
        return codeRubrique;
    }

    /**
     * Renvoie le site d'affichage de la fiche si elle ne s'affiche pas dans le site courant.
     *
     * @param ctx
     *            the _ctx
     * @param fiche
     *            the fiche
     *
     * @return the site affichage
     *
     * @throws Exception
     *
     * @deprecated le contexte ne sert pas, utiliser {@link com.univ.multisites.service.ServiceInfosSite#displaySite(FicheUniv)}
     */
    @Deprecated
    public static InfosSite getSiteAffichage(final ContexteUniv ctx, final FicheUniv fiche) throws Exception {
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        return serviceInfosSite.displaySite(fiche);
    }

    /**
     * Renvoie le site d'affichage de la fiche si elle ne s'affiche pas dans le site courant.
     *
     * @param fiche
     *            the fiche
     *
     * @return the site affichage
     *
     * @throws Exception
     *             the exception
     * @deprecated utiliser {@link com.univ.multisites.service.ServiceInfosSite#displaySite(FicheUniv)}
     */
    @Deprecated
    public static InfosSite getSiteAffichage(final FicheUniv fiche) throws Exception {
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        return serviceInfosSite.displaySite(fiche);
    }

    /**
     * Supprime le rattachement des fiches à une rubrique provenant d'une requete de publication automatique car cette requete a ete supprimee AA 200805 : ajout d'un test car les
     * requetes comportent desormais un critère de plus : la langue leur syntaxe diffère donc entre les anciennes requetes et les nouvelles.
     *
     * @param ctx
     *            the _ctx
     * @param codeRubrique
     *            the _code rubrique
     * @param requete
     *            the _requete
     * @param suppressionEffective
     *            the _suppression effective
     *
     * @return type treeset( new infosFichecomparator() )
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert plus, utiliser {@link ServiceRubriquePublication#getInfosFichesReferencees(String, String)} si suppressionEffective est à false ou {@link ServiceRubriquePublication#deleteRubPubAuto(String, String)}
     * si suppressionEffective est à true
     */
    @Deprecated
    public static TreeSet<InfosFicheReferencee> supprimerRubriquePublicationAutomatique(final OMContext ctx, final String codeRubrique, final String requete, final boolean suppressionEffective) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        TreeSet<InfosFicheReferencee> results;
        if (suppressionEffective) {
            results = new TreeSet<>(serviceRubriquePublication.deleteRubPubAuto(codeRubrique, requete));
        } else {
            results = new TreeSet<>(serviceRubriquePublication.getInfosFichesReferencees(codeRubrique, requete));
        }
        return results;
    }

    /**
     * Supprime le rattachement des fiches à une rubrique provenant d'une requete de publication automatique car cette requete a ete supprimee AA 200805 : ajout d'un test car les
     * requetes comportent desormais un critère de plus : la langue leur syntaxe diffère donc entre les anciennes requetes et les nouvelles.
     *
     * @param codeRubrique
     *            the _code rubrique
     * @param requete
     *            the _requete
     * @param suppressionEffective
     *            the _suppression effective
     *
     * @return type treeset( new infosFichecomparator() )
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert plus, utiliser {@link ServiceRubriquePublication#getInfosFichesReferencees(String, String)} si suppressionEffective est à false ou {@link ServiceRubriquePublication#deleteRubPubAuto(String, String)}
     * si suppressionEffective est à true
     */
    @Deprecated
    public static TreeSet<InfosFicheReferencee> supprimerRubriquePublicationAutomatique(final String codeRubrique, final String requete, final boolean suppressionEffective) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        TreeSet<InfosFicheReferencee> results;
        if (suppressionEffective) {
            results = new TreeSet<>(serviceRubriquePublication.deleteRubPubAuto(codeRubrique, requete));
        } else {
            results = new TreeSet<>(serviceRubriquePublication.getInfosFichesReferencees(codeRubrique, requete));
        }
        return results;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    @Override
    public Rubriquepublication clone() throws CloneNotSupportedException {
        return (Rubriquepublication) super.clone();
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdRubriquepublication((long) 0);
        setTypeFicheOrig("");
        setCodeFicheOrig("");
        setLangueFicheOrig("");
        setRubriqueDest("");
        setSourceRequete("");
    }

    /**
     * Renvoie pour une fiche la liste des rubriques de publication. Il est également possible de préciser un périmètre de recherche sur les rubriques (ex : ne chercher que les
     * rubriques d'un site) le booleen permet d'identifier les rubriques automatiques #AUTO#+code (utilisé dans controleurUniv pour éviter la suppression de ces rubriques) FIXME :
     * Pas de controle des valeurs fournis ?
     *
     * @param typeFiche
     *            the type fiche
     * @param codeFiche
     *            the code fiche
     * @param langueFiche
     *            the langue fiche
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String typeFiche, final String codeFiche, final String langueFiche) throws Exception {
        /* 1ère partie de la requête : recherche de la fiche */
        final ClauseWhere whereTypeCodeLangue = new ClauseWhere(ConditionHelper.egalVarchar("TYPE_FICHE_ORIG", typeFiche));
        whereTypeCodeLangue.and(ConditionHelper.egalVarchar("CODE_FICHE_ORIG", codeFiche));
        whereTypeCodeLangue.and(ConditionHelper.egalVarchar("LANGUE_FICHE_ORIG", langueFiche));
        return select(whereTypeCodeLangue.formaterSQL());
    }


    public Long getIdRubriquepublication() {
        return persistenceBean.getIdRubriquepublication();
    }

    public void setIdRubriquepublication(Long idRubriquepublication) {
        persistenceBean.setIdRubriquepublication(idRubriquepublication);
    }

    public String getTypeFicheOrig() {
        return persistenceBean.getTypeFicheOrig();
    }

    public void setTypeFicheOrig(String typeFicheOrig) {
        persistenceBean.setTypeFicheOrig(typeFicheOrig);
    }

    public String getCodeFicheOrig() {
        return persistenceBean.getCodeFicheOrig();
    }

    public void setCodeFicheOrig(String codeFicheOrig) {
        persistenceBean.setCodeFicheOrig(codeFicheOrig);
    }

    public String getLangueFicheOrig() {
        return persistenceBean.getLangueFicheOrig();
    }

    public void setLangueFicheOrig(String langueFicheOrig) {
        persistenceBean.setLangueFicheOrig(langueFicheOrig);
    }

    public String getRubriqueDest() {
        return persistenceBean.getRubriqueDest();
    }

    public void setRubriqueDest(String rubriqueDest) {
        persistenceBean.setRubriqueDest(rubriqueDest);
    }

    public String getSourceRequete() {
        return persistenceBean.getSourceRequete();
    }

    public void setSourceRequete(String sourceRequete) {
        persistenceBean.setSourceRequete(sourceRequete);
    }
}
