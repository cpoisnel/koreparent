package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;

/**
 * Informations stockées en mémoire pour un groupe DSI.
 * @deprecated Utiliser directement {@link com.univ.objetspartages.bean.GroupeDsiBean} et le service {@link com.univ.objetspartages.services.ServiceGroupeDsi}
 */
@Deprecated
public class InfosGroupeDsi implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7437361262139171837L;

    /** The id **/
    private Long id = 0L;

    /** The code. */
    private String code = "";

    /** The structure code */
    private String codeStructure = "";

    /** The intitule. */
    private String intitule = "";

    /** The code page tete. */
    private String codePageTete = "";

    /** The roles. */
    private String roles = "";

    /** The code groupe pere. */
    private String codeGroupePere = "";

    /** The requete. */
    private String requete = "";

    /** The selectionnable. */
    private String selectionnable = "";

    /** The type. */
    private String type = "";

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByCode(String)}
     */
    @Deprecated
    public InfosGroupeDsi getGroupePere() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return new InfosGroupeDsi(serviceGroupeDsi.getByCode(codeGroupePere));
    }

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByParentGroup(String)}
     */
    @Deprecated
    public Collection<InfosGroupeDsi> getListeSousGroupes() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final List<GroupeDsiBean> groups = serviceGroupeDsi.getByParentGroup(code);
        final Collection<InfosGroupeDsi> children = new ArrayList<>();
        for(GroupeDsiBean currentGroup : groups) {
            children.add(new InfosGroupeDsi(currentGroup));
        }
        return children;
    }

    /**
     * Constructeur de InfosGroupeDsi.
     *
     * @deprecated Ne plus utiliser cette classe. Utiliser {@link com.univ.objetspartages.bean.GroupeDsiBean} avec le service {@link com.univ.objetspartages.services.ServiceGroupeDsi}
     */
    @Deprecated
    public InfosGroupeDsi() {}

    /**
     * Constructeur de InfosGroupeDsi.
     *
     * @param groupe
     *            the groupe
     * @deprecated Ne plus utiliser cette classe. Utiliser {@link com.univ.objetspartages.bean.GroupeDsiBean} avec le service {@link com.univ.objetspartages.services.ServiceGroupeDsi}
     */
    @Deprecated
    public InfosGroupeDsi(final Groupedsi groupe) {
        if(groupe != null) {
            this.id = groupe.getIdGroupedsi();
            this.code = groupe.getCode();
            this.codeStructure = groupe.getCodeStructure();
            this.intitule = groupe.getLibelle();
            this.type = groupe.getType();
            this.codeGroupePere = groupe.getCodeGroupePere();
            this.codePageTete = groupe.getCodePageTete();
            this.roles = groupe.getRoles();
            this.requete = groupe.getRequeteGroupe();
            this.selectionnable = groupe.getSelectionnable();
        }
    }

    /**
     * Constructeur de InfosGroupeDsi.
     *
     * @param groupe
     *            the groupe
     * @deprecated Ne plus utiliser cette classe. Utiliser {@link com.univ.objetspartages.bean.GroupeDsiBean} avec le service {@link com.univ.objetspartages.services.ServiceGroupeDsi}
     */
    @Deprecated
    public InfosGroupeDsi(final GroupeDsiBean groupe) {
        if(groupe != null) {
            this.id = groupe.getIdGroupedsi();
            this.code = groupe.getCode();
            this.codeStructure = groupe.getCodeStructure();
            this.intitule = groupe.getLibelle();
            this.type = groupe.getType();
            this.codeGroupePere = groupe.getCodeGroupePere();
            this.codePageTete = groupe.getCodePageTete();
            this.roles = groupe.getRoles();
            this.requete = groupe.getRequeteGroupe();
            this.selectionnable = groupe.getSelectionnable();
        }
    }

    /**
     * Get the id
     *
     * @return id as a <code>Long</code> value
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            The code to set.
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Get the structure's code
     *
     * @return the code as a <code>String</code> value
     */
    public String getCodeStructure() {
        return codeStructure;
    }

    /**
     * Gets the requete.
     *
     * @return Returns the requete.
     */
    public String getRequete() {
        return requete;
    }

    /**
     * Sets the requete.
     *
     * @param requete
     *            The requete to set.
     */
    public void setRequete(final String requete) {
        this.requete = requete;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the code page tete.
     *
     * @return the code page tete
     */
    public String getCodePageTete() {
        return codePageTete;
    }

    /**
     * Gets the intitule.
     *
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (10/01/2003 13:50:46)
     *
     * @return String
     */
    public String getType() {
        return type;
    }

    /**
     * Gets the roles.
     *
     * @return Returns the roles.
     */
    public String getRoles() {
        return roles;
    }

    /**
     * Trie les sous-structures sur leur libellé long.
     *
     * @return the liste sous groupes sorted by libelle
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getByParentGroup(String)}
     */
    @Deprecated
    public Collection<InfosGroupeDsi> getListeSousGroupesSortedByLibelle() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final Collection<InfosGroupeDsi> infos = new ArrayList<>();
        for(GroupeDsiBean currentGroup : serviceGroupeDsi.getByParentGroup(getCode())) {
            infos.add(new InfosGroupeDsi(currentGroup));
        }
        return infos;
    }

    /**
     * Renvoie la liste des sous-groupes de ce groupe tous niveaux confondus.
     *
     * @return La liste des sous-groupes de ce groupe tous niveaux confondus
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getAllSubGroups(String)}
     */
    @Deprecated
    public Collection<InfosGroupeDsi> getListeSousGroupesTousNiveaux() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final Collection<InfosGroupeDsi> infos = new ArrayList<>();
        for(GroupeDsiBean currentGroup : serviceGroupeDsi.getAllSubGroups(getCode())) {
            infos.add(new InfosGroupeDsi(currentGroup));
        }
        return infos;
    }

    /**
     * Gets the niveau.
     *
     * @return Returns the niveau.
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#getLevel(com.univ.objetspartages.bean.GroupeDsiBean)}. Cette méthode ne renvoit que "0"
     */
    @Deprecated
    public int getNiveau() {
        return 0;
    }

    /**
     * Gets the code groupe pere.
     *
     * @return Returns the codeGroupePere.
     */
    public String getCodeGroupePere() {
        return codeGroupePere;
    }

    /**
     * Rattache le groupe à son groupe père et met à jour son niveau et la liste des sous-groupes de son groupe père.
     *
     * @param groupe
     *            Le groupe père
     */
    public void rattacheA(final InfosGroupeDsi groupe) {
        updateNiveau();
    }

    /**
     * Met a jour le niveau du groupe et de ses sous-groupes en fonction du niveau du groupe père.
     * @deprecated Ne fait plus rien
     */
    @Deprecated
    private void updateNiveau() {
        // Nothing to do here. Retro-compat only
    }

    /**
     * Vérifie si ce groupe contient le groupe passé en paramètre (on teste si le groupe passé en paramètre ou un de ses groupes père ne serait pas par hasard égal à ce groupe.
     *
     * @param groupe
     *            the groupe
     *
     * @return true, if contains
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceGroupeDsi#contains}
     */
    @Deprecated
    public boolean contains(InfosGroupeDsi groupe) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return serviceGroupeDsi.contains(serviceGroupeDsi.getByCode(getCode()), serviceGroupeDsi.getByCode(groupe.getCode()));
    }

    /**
     * Gets the selectionnable.
     *
     * @return the selectionnable
     */
    public String getSelectionnable() {
        return selectionnable;
    }

    /**
     * Checks if is selectionnable.
     *
     * @return true, if is selectionnable
     */
    public boolean isSelectionnable() {
        boolean res = false;
        if ("1".equals(selectionnable)) {
            res = true;
        }
        return res;
    }
}
