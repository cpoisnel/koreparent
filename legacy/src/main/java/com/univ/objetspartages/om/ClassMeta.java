package com.univ.objetspartages.om;
// TODO: Auto-generated Javadoc

/**
 * Obligatoire si la classe a des méta-donnes différents des méta-données par défaut ( ex : le code rattachement qui s'appelle CODE_STRUCTURE.
 * @deprecated cette classe servait à récupérer les noms des champs de la BDD pour code rattachement / code structure qui était différent en V4.
 * ce code n'a plus de sens depuis 2005
 */
@Deprecated
public interface ClassMeta {

    /**
     * Gets the class meta value.
     *
     * @param metaName
     *            the meta name
     *
     * @return the class meta value
     */
    public String getClassMetaValue(String metaName);
}