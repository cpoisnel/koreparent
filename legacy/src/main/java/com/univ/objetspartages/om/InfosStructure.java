package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.services.ServiceStructure;

/**
 * Classe permettant de stocker en mémoire les informations relatives à une structure.
 * @deprecated Ne plus utiliser cette classe. Passer par {@link StructureModele} en utilisant le service {@link com.univ.objetspartages.services.ServiceStructure}.
 */
@Deprecated
public class InfosStructure implements Serializable {

    private static final long serialVersionUID = 7296998673760536148L;

    /** The id structure. */
    private Long idStructure = (long) 0;

    /** The code. */
    private String code = "";

    /** The code objet. */
    private String codeObjet = "";

    /** The code rattachement. */
    private String codeRattachement = "";

    /** The langue. */
    private String langue = "";

    /** The libelle court. */
    private String libelleCourt = "";

    /** The libelle long. */
    private String libelleLong = "";

    /** The onglets. */
    private String onglets = "";

    /** The couleur titre. */
    private String couleurTitre = "";

    /** The couleur fond. */
    private String couleurFond = "";

    /** The type structure. */
    private String typeStructure = "";

    /** The url bandeau. */
    private String urlBandeau = "";

    /** The id bandeau. */
    private Long idBandeau = (long) 0;

    /** The attribut specifique1. */
    private String attributSpecifique1 = "";

    /** The attribut specifique2. */
    private String attributSpecifique2 = "";

    /** The attribut specifique3. */
    private String attributSpecifique3 = "";

    /** The attribut specifique4. */
    private String attributSpecifique4 = "";

    /** The attribut specifique5. */
    private String attributSpecifique5 = "";

    /** The visible in front. */
    private boolean visibleInFront = true; // Affichage en front ou non

    public InfosStructure() {}

    /**
     * Constructeur InfosStructure.
     *
     * @param structure
     *            the structure
     */
    public InfosStructure(final StructureModele structure) {
        if (structure != null) {
            this.idStructure = structure.getIdFiche();
            this.code = structure.getCode();
            this.codeObjet = ReferentielObjets.getCodeObjet(structure);
            this.codeRattachement = structure.getCodeRattachement();
            this.langue = structure.getLangue();
            this.libelleCourt = structure.getLibelleCourt();
            this.libelleLong = structure.getLibelleLong();
            this.onglets = structure.getOnglets();
            this.couleurTitre = structure.getCouleurTitre();
            this.couleurFond = structure.getCouleurFond();
            this.typeStructure = structure.getTypeStructure();
            this.idBandeau = structure.getIdBandeau();
            this.attributSpecifique1 = structure.getAttributSpecifique1();
            this.attributSpecifique2 = structure.getAttributSpecifique2();
            this.attributSpecifique3 = structure.getAttributSpecifique3();
            this.attributSpecifique4 = structure.getAttributSpecifique4();
            this.attributSpecifique5 = structure.getAttributSpecifique5();
        }
    }

    /**
     * Constructeur InfosStructure.
     *
     * @param structure
     *            the structure
     */
    public InfosStructure(final InfosStructure structure) {
        this.idStructure = structure.getIdStructure();
        this.code = structure.getCode();
        this.codeRattachement = structure.getCodeRattachement();
        this.langue = structure.getLangue();
        this.libelleCourt = structure.getLibelleCourt();
        this.libelleLong = structure.getLibelleLong();
        this.onglets = structure.getOnglets();
        this.couleurTitre = structure.getCouleurTitre();
        this.couleurFond = structure.getCouleurFond();
        this.typeStructure = structure.getTypeStructure();
        this.idBandeau = structure.getIdBandeau();
        this.urlBandeau = structure.getUrlBandeau();
        this.visibleInFront = structure.isVisibleInFront();
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getCode() {
        return code;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getCodeRattachement() {
        return codeRattachement;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getLibelleLong() {
        return libelleLong;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getOnglets() {
        return onglets;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getCouleurTitre() {
        return couleurTitre;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getCouleurFond() {
        return couleurFond;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getTypeStructure() {
        return typeStructure;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public String getUrlBandeau() {
        return urlBandeau;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return String
     */
    public boolean isVisibleInFront() {
        return visibleInFront;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @return int
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getLevel(com.univ.objetspartages.om.StructureModele)}
     */
    @Deprecated
    public int getNiveau() {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final StructureModele structure = serviceStructure.getByCodeLanguage(getCode(), getLangue());
        return serviceStructure.getLevel(structure);
    }

    /**
     * Renvoie la structure de rattachement de cette structure.
     *
     * @return a structure de rattachement de cette structure
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getByCodeLanguage(String, String)} à partir du code rattachement.
     */
    @Deprecated
    public InfosStructure getStructureRattachement() {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return new InfosStructure(serviceStructure.getByCodeLanguage(getCodeRattachement(), getLangue()));
    }

    /**
     * Renvoie la liste des sous-structures de cette structure.
     *
     * @return La liste des sous-structures de cette structure
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getSubStructures(String, String, boolean)} à partir du code.
     */
    @Deprecated
    public Collection<InfosStructure> getListeSousStructures() {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final Collection<InfosStructure> infos = new ArrayList<>();
        for (StructureModele currentStructure : serviceStructure.getSubStructures(getCode(), getLangue(), true)) {
            infos.add(new InfosStructure(currentStructure));
        }
        return infos;
    }

    /**
     * Trie les sous-structures sur leur libellé court.
     *
     * @return the liste sous structures sorted by libelle court
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getSubStructures(String, String, boolean)} à partir du code. Faire le tri si nécessaire à partir de la liste retournée.
     */
    @Deprecated
    public Collection<InfosStructure> getListeSousStructuresSortedByLibelleCourt() {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final TreeMap<String, InfosStructure> sortedListeSousStructures = new TreeMap<>();
        for (StructureModele currentStructure : serviceStructure.getSubStructures(getCode(), getLangue(), true)) {
            sortedListeSousStructures.put(currentStructure.getLibelleCourt() + "___" + currentStructure.getCode(), new InfosStructure(currentStructure));
        }
        return sortedListeSousStructures.values();
    }

    /**
     * Trie les sous-structures sur leur libellé long.
     *
     * @return the liste sous structures sorted by libelle long
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getSubStructures(String, String, boolean)} à partir du code. Faire le tri si nécessaire à partir de la liste retournée.
     */
    @Deprecated
    public Collection<InfosStructure> getListeSousStructuresSortedByLibelleLong() {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final TreeMap<String, InfosStructure> sortedListeSousStructures = new TreeMap<>();
        for (StructureModele currentStructure : serviceStructure.getSubStructures(getCode(), getLangue(), true)) {
            sortedListeSousStructures.put(currentStructure.getLibelleLong() + "___" + currentStructure.getCode(), new InfosStructure(currentStructure));
        }
        return sortedListeSousStructures.values();
    }

    /**
     * Renvoie la liste des sous-structures de cette structure tous niveaux confondus.
     *
     * @return La liste des sous-structures de cette structure tous niveaux confondus
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getAllSubStructures(String, String, boolean)} à partir du code.
     */
    @Deprecated
    public Collection<InfosStructure> getListeSousStructuresTousNiveaux() {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final Collection<InfosStructure> infos = new ArrayList<>();
        for (StructureModele currentStructure : serviceStructure.getAllSubStructures(getCode(), getLangue(), true)) {
            infos.add(new InfosStructure(currentStructure));
        }
        return infos;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @param newCode
     *            String
     */
    public void setCode(final String newCode) {
        code = newCode;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @param newLangue
     *            String
     */
    public void setLangue(final String newLangue) {
        langue = newLangue;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @param newUrlBandeau
     *            String
     */
    public void setUrlBandeau(final String newUrlBandeau) {
        urlBandeau = newUrlBandeau;
    }

    /**
     * Insérez la description de la méthode à cet endroit.
     *
     * @param visibleInFront
     *            the visible in front
     */
    public void setVisibleInFront(final boolean visibleInFront) {
        this.visibleInFront = visibleInFront;
    }

    /**
     * Ajoute une sous-structure à la liste des sous-structures.
     *
     * @param sousStructure
     *            Une sous-structure
     */
    public void addSousStructure(final InfosStructure sousStructure) {
        // Does nothing
    }

    /**
     * Rattache la structure à sa structure mère et met à jour son niveau et la liste des sous-structures de la structure mère.
     *
     * @param structureMere
     *           La structure mère
     * @deprecated Ne plus utiliser cette méthode. Les notions de rattachement sont calculées automatiquement par le service {@link com.univ.objetspartages.services.ServiceStructure}.
     */
    @Deprecated
    public void rattacheA(final InfosStructure structureMere) {
        // Nothing to do
    }

    /**
     * Vérifie si cette structure contient la structure passée en paramètre (on teste si la structure passée en paramètre ou une de ses structures mère ne serait pas par hasard
     * égale à cette structure).
     *
     * @param structure
     *            the structure
     *
     * @return true, if contains
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#contains(StructureModele, StructureModele)}.
     */
    @Deprecated
    public boolean contains(InfosStructure structure) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final StructureModele thisStructure = serviceStructure.getByCodeLanguage(getCode(), getLangue());
        final StructureModele otherStructure = serviceStructure.getByCodeLanguage(structure.getCode(), getLangue());
        return serviceStructure.contains(thisStructure, otherStructure);
    }

    /**
     * Gets the id bandeau.
     *
     * @return Returns the idBandeau.
     */
    public Long getIdBandeau() {
        return idBandeau;
    }

    /**
     * Gets the id structure.
     *
     * @return Returns the idStructure.
     */
    public Long getIdStructure() {
        return idStructure;
    }

    /**
     * Gets the libelle affichable.
     *
     * @return the libelle affichable
     */
    public String getLibelleAffichable() {
        return libelleLong;
    }

    /**
     * Clone un objet InfosStructure.
     *
     * @return the object
     */
    @Override
    public Object clone() {
        return new InfosStructure(this);
    }

    /**
     * Gets the attribut specifique1.
     *
     * @return the attribut specifique1
     */
    public String getAttributSpecifique1() {
        return attributSpecifique1;
    }

    /**
     * Gets the attribut specifique2.
     *
     * @return the attribut specifique2
     */
    public String getAttributSpecifique2() {
        return attributSpecifique2;
    }

    /**
     * Gets the attribut specifique3.
     *
     * @return the attribut specifique3
     */
    public String getAttributSpecifique3() {
        return attributSpecifique3;
    }

    /**
     * Gets the attribut specifique4.
     *
     * @return the attribut specifique4
     */
    public String getAttributSpecifique4() {
        return attributSpecifique4;
    }

    /**
     * Gets the attribut specifique5.
     *
     * @return the attribut specifique5
     */
    public String getAttributSpecifique5() {
        return attributSpecifique5;
    }

    /**
     * Gets the code objet.
     *
     * @return the code objet
     */
    public String getCodeObjet() {
        return codeObjet;
    }
}
