package com.univ.objetspartages.om;

import org.apache.commons.lang3.StringUtils;

import com.univ.objetspartages.sgbd.ServiceDB;

/**
 * The Class Service.
 * @deprecated cette classe n'est plus utiliser, voir {@link com.univ.objetspartages.dao.impl.ServiceDAO} et {@link ServiceBean} pour les nouvelles implémentations
 */
@Deprecated
public class Service extends ServiceDB {

    /**
     * Instantiates a new service.
     */
    public Service() {
        super();
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdService((long) 0);
        setCode(StringUtils.EMPTY);
        setIntitule(StringUtils.EMPTY);
        setExpirationCache(0);
        setUrl(StringUtils.EMPTY);
        setUrlPopup("0");
        setJetonKportal("0");
        setProxyCas("0");
        setVueReduiteUrl(StringUtils.EMPTY);
        setDiffusionMode("0");
        setDiffusionPublicVise(StringUtils.EMPTY);
        setDiffusionModeRestriction("0");
        setDiffusionPublicViseRestriction(StringUtils.EMPTY);
    }
}
