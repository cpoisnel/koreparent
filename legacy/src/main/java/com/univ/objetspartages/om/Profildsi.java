package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.objetspartages.sgbd.ProfildsiDB;
import com.univ.objetspartages.util.InfosProfildsiUtils;
import com.univ.objetspartages.util.InfosRolesUtils;
import com.univ.utils.Chaine;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * The Class Profildsi.
 * @deprecated les méthodes appelant le cache (pour {@link InfosProfilDsi} sont dans {@link InfosProfildsiUtils}, pour les acces en bdd {@link com.univ.objetspartages.services.ServiceProfildsi}
 * pour les données du bean {@link com.univ.objetspartages.bean.ProfildsiBean}
 */
@Deprecated
public class Profildsi extends ProfildsiDB implements Cloneable {

    /**
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#getAll()}
     */
    @Deprecated
    public static Map<String, InfosProfilDsi> getListeInfosProfilDsi() {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        final Map<String, InfosProfilDsi> profilesMap = new HashMap<>();
        for(ProfildsiBean currentProfile : serviceProfildsi.getAll()) {
            profilesMap.put(currentProfile.getLibelle(), new InfosProfilDsi(currentProfile));
        }
        return profilesMap;
    }

    @Deprecated
    public static String getIntitule(OMContext _ctx, String _code) {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        return serviceProfildsi.getDisplayableLabel(_code);
    }

    /**
     * Récupération de l'intitulé.
     *
     * @param code
     *            the code
     *
     * @return the intitule
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#getDisplayableLabel(String)}
     */
    @Deprecated
    public static String getIntitule(final String code) {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        return  serviceProfildsi.getDisplayableLabel(code);
    }

    /**
     * Renvoie la liste de tous les profils DSI (id+libellé) Peut être affiché directement dans une Combo
     *
     * @param _ctx
     *            the _ctx
     *
     * @return the liste id profils dsi
     *
     * @throws Exception
     *             the exception
     * @deprecated Méthode plus utilisée
     */
    @Deprecated
    public static Hashtable<String, String> getListeIDProfilsDSI(OMContext _ctx) throws Exception {
        Profildsi profildsi = new Profildsi();
        profildsi.setCtx(_ctx);
        profildsi.init();
        Hashtable<String, String> h = new Hashtable<>();
        if (profildsi.select("") > 0) {
            while (profildsi.nextItem()) {
                h.put(profildsi.getIdProfildsi().toString(), profildsi.getLibelle());
            }
        }
        return h;
    }

    /**
     * Renvoie la liste de tous les profils DSI (id+libellé) Peut être affiché directement dans une Combo
     *
     * @param ctx
     *            the _ctx
     *
     * @return the liste id profils dsi
     *
     * @throws Exception
     *             the exception
     */
    public static Collection<Profildsi> getListeProfilsDSI(OMContext ctx) throws Exception {
        Profildsi profildsi = new Profildsi();
        profildsi.setCtx(ctx);
        profildsi.init();
        Collection<Profildsi> toutLesProfils = new ArrayList<>();
        if (profildsi.select("") > 0) {
            while (profildsi.nextItem()) {
                toutLesProfils.add(profildsi.clone());
            }
        }
        return toutLesProfils;
    }

    /**
     * Renvoie la liste de tous les profils DSI (code+libellé) Peut être affiché directement dans une Combo.
     *
     * @return the liste profils dsi
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#getDisplayableProfiles()}
     */
    @Deprecated
    public static Hashtable<String, String> getListeProfilsDSI() {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        return new Hashtable<>(serviceProfildsi.getDisplayableProfiles());
    }

    /**
     * Renvoyer profils groupes.
     *
     * @param groupesUtilisateur
     *            the _groupes utilisateur
     *
     * @return the vector
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#renvoyerProfilsGroupes(java.util.Collection)}
     */
    @Deprecated
    public static Vector<String> renvoyerProfilsGroupes(Vector<String> groupesUtilisateur) {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        return new Vector<>(serviceProfildsi.renvoyerProfilsGroupes(groupesUtilisateur));
    }

    /**
     * Renvoie la liste de tous les profils pour une liste de groupes.
     *
     * @param groupes
     *            the _groupes
     *
     * @return the liste profils dsi par groupes
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#getListeProfilsDSIParGroupes(java.util.Collection)}
     *
     */
    @Deprecated
    public static Hashtable<String, String> getListeProfilsDSIParGroupes(Vector<String> groupes) {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        return new Hashtable<>(serviceProfildsi.getListeProfilsDSIParGroupes(groupes));
    }

    /**
     * Récupération d'un profil stocké en mémoire.
     *
     * @param code
     *            the code
     *
     * @return the infos profil dsi
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceProfildsi#getByCode(String)}
     */
    @Deprecated
    public static InfosProfilDsi renvoyerItemProfilDsi(String code) {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
       return new InfosProfilDsi(serviceProfildsi.getByCode(code));
    }

    /**
     * Renvoie la liste des {profils/périmètres} associé à un role sous forme de Hashtable.
     *
     * @param _ctx
     *            the _ctx
     * @param _role
     *            the _role
     * @param _codesStructures
     *            the _codes structures
     * @param _codeRubrique
     *            the _code rubrique
     * @param _publicsVises
     *            the _publics vises
     * @param _codeEspaceCollaboratif
     *            the _code espace collaboratif
     *
     * @return the hashtable
     *
     * @throws Exception
     *             the exception
     * @deprecated Méthode plus utilisée
     */
    @Deprecated
    public static Hashtable<String, Vector<Perimetre>> renvoyerProfilsEtPerimetres(OMContext _ctx, String _role, List<String> _codesStructures, String _codeRubrique, String _publicsVises, String _codeEspaceCollaboratif) throws Exception {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        Hashtable<String, Vector<Perimetre>> h = new Hashtable<>();
        for (ProfildsiBean info : serviceProfildsi.getAll()) {
            Vector<Perimetre> v = InfosRolesUtils.renvoyerPerimetresAffectation(info.getRoles(), _role, _codesStructures, _codeRubrique, _publicsVises, _codeEspaceCollaboratif);
            if (v.size() > 0) {
                h.put(info.getCode(), v);
            }
        }
        return h;
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdProfildsi((long) 0);
        setCode(String.valueOf(System.currentTimeMillis()));
        setLibelle("");
        setCodeRubriqueAccueil("");
        setRoles("");
        setGroupes("");
        setCodeRattachement("");
    }

    /**
     * Select.
     *
     * @param code
     *            the _code
     * @param libelle
     *            the _libelle
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String code, String libelle) throws Exception {
        RequeteSQL requeteCodeLibelle = new RequeteSQL();
        ClauseOrderBy orderBy = new ClauseOrderBy("LIBELLE", SensDeTri.ASC);
        ClauseWhere where = new ClauseWhere();
        if (StringUtils.isNotEmpty(code)) {
            where.setPremiereCondition(ConditionHelper.egalVarchar("CODE", code));
        }
        if (StringUtils.isNotEmpty(libelle)) {
            where.and(ConditionHelper.like("LIBELLE", libelle, "%", "%"));
        }
        requeteCodeLibelle.where(where).orderBy(orderBy);
        return select(requeteCodeLibelle.formaterRequete());
    }

    /**
     * Sets the vecteur groupes.
     *
     * @param v
     *            the new vecteur groupes
     *
     */
    public void setVecteurGroupes(Vector<String> v) {
        setGroupes(Chaine.convertirAccolades(v));
    }

    /**
     * Gets the vecteur groupes.
     *
     * @return the vecteur groupes
     *
     */
    public Vector<String> getVecteurGroupes() {
        return Chaine.getVecteurAccolades(getGroupes());
    }

    @Override
    public Profildsi clone() throws CloneNotSupportedException {
        return (Profildsi) super.clone();
    }
}
