/*
 *
 */
package com.univ.objetspartages.om;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.mediatheque.Mediatheque;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.dao.impl.MediaDAO;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FileUtil;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * Classe representant un objet media.
 * @deprecated Cette classe ne doit plus être utilisé. Passez par {@link com.univ.objetspartages.services.ServiceMedia}, {@link com.univ.objetspartages.util.MediaUtils} et {@link com.univ.objetspartages.bean.MediaBean}
 */
@Deprecated
public class Media extends AbstractOm<MediaBean, MediaDAO> implements Cloneable {

    private static final Logger LOG = LoggerFactory.getLogger(Media.class);

    /** The langue. */
    private int langue = 0;

    private String generateName = "";

    /**
     * Instantiates a new media.
     */
    private static ServiceMedia getServiceMedia() {
        return ServiceManager.getServiceForBean(MediaBean.class);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#conditionPerimetreMedia(String)}
     */
    @Deprecated
    public static ConditionList conditionPerimetreMedia(final OMContext ctx, final String mode) throws Exception {
        return MediaUtils.conditionPerimetreMedia(mode);
    }

    /**
     * Delete by id.
     *
     * @param ctx
     *            the ctx
     * @param id
     *            the id
     * @deprecated see {@link com.univ.objetspartages.services.ServiceMedia#delete(Long)}
     */
    @Deprecated
    public static void deleteById(final OMContext ctx, final Long id) {
        deleteById(ctx, id, true);
    }

    /**
     * Delete by id.
     *
     * @param ctx
     *            the ctx
     * @param id
     *            the id
     * @param onlyNotMutualise
     *            the only not mutualise
     * @deprecated see {@link com.univ.objetspartages.services.ServiceMedia#delete(Long)}
     */
    @Deprecated
    public static void deleteById(final OMContext ctx, final Long id, final boolean onlyNotMutualise) {
        deleteById(id, onlyNotMutualise);
    }

    /**
     * Delete by id.
     *
     * @param id
     *            the id
     * @deprecated see {@link com.univ.objetspartages.services.ServiceMedia#delete(Long)}
     */
    @Deprecated
    public static void deleteById(final Long id) {
        deleteById(id, true);
    }

    /**
     * Delete a media by id.
     *
     * @param id
     *            the id
     * @param onlyNotMutualise
     *            should we delete the file if it's shared between different content?
     * @deprecated see {@link com.univ.objetspartages.services.ServiceMedia#delete(Long)}
     */
    @Deprecated
    public static void deleteById(final Long id, final boolean onlyNotMutualise) {
        final MediaBean media = getServiceMedia().getById(id);
        if (!onlyNotMutualise || (!media.getIsMutualise().equals(Mediatheque.ETAT_MUTUALISE))) {
            getServiceMedia().delete(id);
            new File(MediaUtils.getPathAbsolu(media)).delete();
            if (media.getUrlVignette().length() > 0) {
                new File(MediaUtils.getUrlVignetteAbsolue(media)).delete();
            }
        }
    }

    /**
     * Gets the media.
     *
     * @param ctx
     *            the ctx
     * @param idMedia
     *            the id media
     *
     * @return the media
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getById(Long)}
     */
    @Deprecated
    public static Media getMedia(final OMContext ctx, final Long idMedia) {
        return getMedia(idMedia);
    }

    /**
     * Récupère un Media depuis l'id fourni en paramètre
     *
     * @param idMedia
     *            l'id du média à récupéerer
     * @return le média ayant cet id ou un Media vide sinon...
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getById(Long)}
     */
    @Deprecated
    public static Media getMedia(final Long idMedia) {
        final Media media = new Media();
        media.setPersistenceBean(getServiceMedia().getById(idMedia));
        return media;
    }

    /**
     * Renvoie la liste de tous les medias photos d'un type donné Peut être affiché directement dans une Combo.
     *
     * @param _ctx
     *            the _ctx
     * @param type
     *            the type
     *
     * @return the liste par type
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getByTypeAndTypeMedia(String, String)}
     */
    @Deprecated
    public static Hashtable<String, String> getListePhotoParType(final OMContext _ctx, final String type) throws Exception {
        final List<MediaBean> medias = getServiceMedia().getByTypeAndTypeMedia("photo", type);
        final Hashtable<String, String> h = new Hashtable<>();
        h.putAll(MediaUtils.getMediasAsMap(medias));
        return h;
    }

    /**
     * @return the generateName
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#generateName(com.univ.objetspartages.bean.MediaBean, String)}
     */
    @Deprecated
    public String getGenerateName() {
        return generateName;
    }

    /**
     * @param generateName
     *            the generateName to set
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#generateName(com.univ.objetspartages.bean.MediaBean, String)}
     */
    @Deprecated
    public void setGenerateName(final String generateName) {
        this.generateName = generateName;
    }

    /**
     * Initialise l'objet metier.
     * @deprecated Plus nécessaire
     */
    @Deprecated
    public void init() {
        //Does nothing
    }

    /**
     * Renvoie le libelle a afficher.
     *
     * @return the libelle affichable
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getLibelleAffichable(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getLibelleAffichable() {
        String res = getTitre();
        if (res.length() == 0) {
            res = getSource();
        }
        return res;
    }

    /**
     * Renvoie le libelle pour l'attribut typeRessource.
     *
     * @return the libelle type ressource
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getLibelleTypeRessource(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getLibelleTypeRessource() throws Exception {
        return Mediatheque.getInstance().getRessourceByExtension(FileUtil.getExtension(this.persistenceBean.getSource())).getLibelle();
    }

    /**
     * Renvoie le libelle pour l'attribut typeMedia.
     *
     * @return the libelle type media
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getLibelleTypeMedia(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getLibelleTypeMedia() throws Exception {
        return LabelUtils.getLibelle("201", this.persistenceBean.getTypeMedia(), LangueUtil.getDefaultLocale());
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.RessourceUniv#getUrlAbsolue()
     */

    /**
     * Renvoie le libelle pour l'attribut thematique.
     *
     * @return the libelle thematique
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getLibelleThematique(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getLibelleThematique() throws Exception {
        return MediaUtils.getLibelleThematique(this.persistenceBean);
    }

    /**
     * Selection d'une instance de l'objet Media a partir de l'ensemble des criteres.
     *
     * @param idMedia
     *            the id media
     * @param titre
     *            the titre
     * @param legende
     *            the legende
     * @param description
     *            the description
     * @param auteur
     *            the auteur
     * @param typeRessource
     *            the type ressource
     * @param typeMedia
     *            the type media
     * @param codeRattachement
     *            the code rattachement
     * @param codeRubrique
     *            the code rubrique
     * @param codeRedacteur
     *            the code redacteur
     * @param metaKeywords
     *            the meta keywords
     * @param dateCreation
     *            the date creation
     * @param copyright
     *            the copyright
     * @param thematique
     *            the thematique
     *
     * @return String la requête
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#preparerRequete(Long, String, String, String, String, String, String, String, String, String, String, String, String, Date, String)}
     */
    @Deprecated
    private ClauseWhere preparerRequete(final Long idMedia, final String titre, final String legende, final String description, final String copyright, final String auteur, final String typeRessource, final String typeMedia, final String thematique, final String codeRattachement, final String codeRubrique, final String codeRedacteur, final String metaKeywords, final Date dateCreation, final String mode) throws Exception {
        return MediaUtils.preparerRequete(idMedia, titre, legende, description, copyright, auteur, typeRessource, typeMedia, thematique, codeRattachement, codeRubrique, codeRedacteur, metaKeywords, dateCreation, mode);
    }

    /**
     * Preparer requete.
     *
     * @param idMedia
     *            the id media
     * @param titre
     *            the titre
     * @param legende
     *            the legende
     * @param description
     *            the description
     * @param copyright
     *            the copyright
     * @param auteur
     *            the auteur
     * @param typeRessource
     *            the type ressource
     * @param typeMedia
     *            the type media
     * @param thematique
     *            the thematique
     * @param codeRattachement
     *            the code rattachement
     * @param codeRubrique
     *            the code rubrique
     * @param codeRedacteur
     *            the code redacteur
     * @param metaKeywords
     *            the meta keywords
     * @param dateCreation
     *            the date creation
     * @param _mode
     *            the _mode
     * @param poidsMinimum
     *            the poids minimum
     * @param poidsMaximum
     *            the poids maximum
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#preparerRequete(Long, String, String, String, String, String, String, String, String, String, String, String, String, Date, String)}
     */
    @Deprecated
    public ClauseWhere preparerRequete(final Long idMedia, final String titre, final String legende, final String description, final String copyright, final String auteur, final String typeRessource, final String typeMedia, final String thematique, final String codeRattachement, final String codeRubrique, final String codeRedacteur, final String metaKeywords, final Date dateCreation, final String _mode, final String poidsMinimum, final String poidsMaximum) throws Exception {
        final ClauseWhere where = preparerRequete(idMedia, titre, legende, description, copyright, auteur, typeRessource, typeMedia, thematique, codeRattachement, codeRubrique, codeRedacteur, metaKeywords, dateCreation, _mode);
        if (StringUtils.isNotBlank(poidsMinimum)) {
            where.and(ConditionHelper.greaterEquals("T1.POIDS", poidsMinimum, TypeOperande.VARCHAR));
        }
        if (StringUtils.isNotBlank(poidsMaximum)) {
            where.and(ConditionHelper.lessEquals("T1.POIDS", poidsMaximum, TypeOperande.VARCHAR));
        }
        return where;
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.RessourceUniv#getUrlAbsolue()
     */

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getUrlAbsolue() {
        return MediaUtils.getUrlAbsolue(this.persistenceBean);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getTypeRessource() {
        return this.persistenceBean.getTypeRessource();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getTypeMedia() {
        return this.persistenceBean.getTypeMedia();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public Long getPoids() {
        return this.persistenceBean.getPoids();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getSource() {
        return this.persistenceBean.getSource();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getFormat() {
        return this.persistenceBean.getFormat();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setSource(final String source) {
        this.persistenceBean.setSource(source);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setFormat(final String format) {
        this.persistenceBean.setFormat(format);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#isPublic(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public boolean isPublic() {
        return !getIsMutualise().equals(Mediatheque.ETAT_NON_MUTUALISE_NON_PUBLIC);
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.RessourceUniv#getUrlAbsolue()
     */

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getPathAbsolu(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getPathAbsolu() {
        return MediaUtils.getPathAbsolu(this.persistenceBean);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getPathVignetteAbsolu(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getPathVignetteAbsolu() {
        String sUrl = getUrlVignette();
        if (sUrl.length() > 0) {
            sUrl = MediathequeHelper.getAbsolutePath();
            if (getTypeRessource().length() > 0) {
                sUrl += "/" + getTypeRessource().toLowerCase();
            }
            sUrl += "/" + getUrlVignette();
        }
        return sUrl;
    }

    /**
     * Gets the url vignette absolue.
     *
     * @return the url vignette absolue
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlVignetteAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getUrlVignetteAbsolue() {
        return MediaUtils.getUrlVignetteAbsolue(this.persistenceBean);
    }

    /**
     * Checks if is local.
     *
     * @return true, if is local
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#isLocal(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public boolean isLocal() {
        return MediaUtils.isLocal(this.persistenceBean);
    }

    /**
     * Gets the specific data.
     *
     * @param propertie
     *            the propertie
     *
     * @return the specific data
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getSpecificData(com.univ.objetspartages.bean.MediaBean, String)}
     */
    @Deprecated
    public String getSpecificData(final String propertie) {
        String sRes = "";
        final Properties properties = new Properties();
        try {
            properties.load(new ByteArrayInputStream(getSpecificData().getBytes()));
        } catch (final IOException e) {
            LOG.error("erreur de la lecture des properties du media", e);
        }
        if (properties.get(propertie) != null) {
            sRes = (String) properties.get(propertie);
        }
        return sRes;
    }

    /**
     * Gets the traduction data.
     *
     * @param property
     *            the propertie
     *
     * @return the specific data
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getTraductionData(MediaBean, String)}
     */
    @Deprecated
    public String getTraductionData(final String property) {
        return getServiceMedia().getTraductionData(this.persistenceBean, property);
    }

    /**
     * Gets the specific data.
     *
     * @return the specific data
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getSpecificDataAsString(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getSpecificDataAsString() {
        return getServiceMedia().getSpecificDataAsString(this.persistenceBean);
    }

    /**
     * Save.
     *
     * @param _ctx
     *            the _ctx
     * @param sFileName
     *            the s file name
     * @param sAbsolutePathFile
     *            the s absolute path file
     * @param sAbsolutePathVignette
     *            the s absolute path vignette
     * @param sVignetteName
     *            the s vignette name
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#save(com.univ.objetspartages.bean.MediaBean, String, String, String, String)}
     */
    @Deprecated
    public void save(final OMContext _ctx, final String sAbsolutePathFile, final String sAbsolutePathVignette, final String sFileName, final String sVignetteName) throws Exception {
        getServiceMedia().save(this.persistenceBean, sAbsolutePathFile, sAbsolutePathVignette, sFileName, sVignetteName);
    }

    /**
     * Generate name.
     *
     * @param extension
     *            the extension
     *
     * @return the string
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#generateName(com.univ.objetspartages.bean.MediaBean, String)}
     */
    @Deprecated
    public synchronized String generateName(String extension) {
        return MediaUtils.generateName(this.persistenceBean, extension);
    }

    /**
     * Retourne si possible l'extension du média
     *
     * @return l'extension du media, si le nom du mÃ©dia se termine par un . ou un -
     * @deprecated Utilisez {@link com.univ.utils.FileUtil#getExtension(String)}
     */
    @Deprecated
    public String getExtension() {
        return FileUtil.getExtension(this.persistenceBean.getSource());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    @Override
    public Media clone() throws CloneNotSupportedException {
        return (Media) super.clone();
    }

    /**
     * Gets the langue.
     *
     * @return the langue
     * @deprecated Utilisez les méthodes du {@link com.univ.objetspartages.services.ServiceMedia} en passant la locale.
     */
    @Deprecated
    public int getLangue() {
        return langue;
    }

    /**
     * Sets the langue.
     *
     * @param langue
     *            the new langue
     * @deprecated Ne doit plus être utilisé
     */
    @Deprecated
    public void setLangue(final int langue) {
        this.langue = langue;
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getDescription(com.univ.objetspartages.bean.MediaBean, java.util.Locale)}
     */
    @Deprecated
    public String getDescription() {
        return getServiceMedia().getDescription(this.persistenceBean, ContexteUtil.getContexteUniv().getLocale());
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getUrl() {
        return this.persistenceBean.getUrl();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getUrlVignette() {
        return this.persistenceBean.getUrlVignette();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getTitre(com.univ.objetspartages.bean.MediaBean, java.util.Locale)}
     */
    @Deprecated
    public String getTitre() {
        return getServiceMedia().getTitre(this.persistenceBean, ContexteUtil.getContexteUniv().getLocale());
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getAuteur(com.univ.objetspartages.bean.MediaBean, java.util.Locale)}
     */
    @Deprecated
    public String getAuteur() {
        return getServiceMedia().getAuteur(this.persistenceBean, ContexteUtil.getContexteUniv().getLocale());
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getCopyright(com.univ.objetspartages.bean.MediaBean, java.util.Locale)}
     */
    @Deprecated
    public String getCopyright() {
        return getServiceMedia().getCopyright(this.persistenceBean, ContexteUtil.getContexteUniv().getLocale());
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getSpecificData() {
        return this.persistenceBean.getSpecificData();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public Date getDateCreation() {
        return this.persistenceBean.getDateCreation();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getLegende(com.univ.objetspartages.bean.MediaBean, java.util.Locale)}
     */
    @Deprecated
    public String getLegende() {
        return getServiceMedia().getLegende(this.persistenceBean, ContexteUtil.getContexteUniv().getLocale());
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.services.ServiceMedia#getMetaKeywords(com.univ.objetspartages.bean.MediaBean, java.util.Locale)}
     */
    @Deprecated
    public String getMetaKeywords() {
        return getServiceMedia().getMetaKeywords(this.persistenceBean, ContexteUtil.getContexteUniv().getLocale());
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getCodeRedacteur() {
        return this.persistenceBean.getCodeRedacteur();
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setTitre(final String sTitre) {
        this.persistenceBean.setTitre(sTitre);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setLegende(final String sLegende) {
        this.persistenceBean.setLegende(sLegende);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setDescription(final String sDescription) {
        this.persistenceBean.setDescription(sDescription);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setUrl(final String sUrl) {
        this.persistenceBean.setUrl(sUrl);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setUrlVignette(final String sUrl) {
        this.persistenceBean.setUrlVignette(sUrl);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setTypeRessource(final String sTypeRessource) {
        this.persistenceBean.setTypeRessource(sTypeRessource);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setTypeMedia(final String sTypeMedia) {
        this.persistenceBean.setTypeMedia(sTypeMedia);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setPoids(final Long iPoids) {
        this.persistenceBean.setPoids(iPoids);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setAuteur(final String sAuteur) {
        this.persistenceBean.setAuteur(sAuteur);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setCopyright(final String sCopyright) {
        this.persistenceBean.setCopyright(sCopyright);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setSpecificData(final String sSpecificData) {
        this.persistenceBean.setSpecificData(sSpecificData);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setDateCreation(final Date sDateCreation) {
        this.persistenceBean.setDateCreation(sDateCreation);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setCodeRedacteur(final String sCodeRedacteur) {
        this.persistenceBean.setCodeRedacteur(sCodeRedacteur);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public void setMetaKeywords(final String sKeywords) {
        this.persistenceBean.setMetaKeywords(sKeywords);
    }

    /**
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MediaUtils#getUrlAbsolue(com.univ.objetspartages.bean.MediaBean)}
     */
    @Deprecated
    public String getIsMutualise() {
        return this.persistenceBean.getIsMutualise();
    }
}
