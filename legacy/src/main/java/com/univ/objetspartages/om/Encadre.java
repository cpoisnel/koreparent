package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.dao.impl.EncadreDAO;
import com.univ.objetspartages.services.ServiceEncadre;
import com.univ.utils.ContexteUniv;

/**
 * The Class Encadre.
 * @deprecated Utilisez directement {@link EncadreBean}
 * @see com.univ.objetspartages.services.ServiceEncadre
 */
@Deprecated
public class Encadre extends AbstractOm<EncadreBean, EncadreDAO> implements Cloneable {

    /**
     * Instantiates a new encadre.
     */
    public Encadre() {
        super();
    }

    /**
     * @deprecated Plus besoin de contexte
     */
    @Deprecated
    public void setCtx(OMContext ctx) {
        // Nothing, just retro-compatibility
    }

    /**
     * Gets the liste encadres recherche.
     *
     * @return the liste encadres recherche
     *
     * @throws Exception
     *             the exception
     * @deprecated
     */
    @Deprecated
    public static Hashtable<String, String> getListeEncadresRecherche() throws Exception {
        final ServiceEncadre serviceEncadre = ServiceManager.getServiceForBean(EncadreBean.class);
        final Hashtable<String, String> listeEncadresRecherche = new Hashtable<>();
        listeEncadresRecherche.putAll(serviceEncadre.getEncadreSearchList());
        return listeEncadresRecherche;
    }

    /**
     * Récupération d'une liste d'encadrés triés par poids pour un type d'objet.
     *
     * @param ctx
     *            the _ctx
     * @param ficheUniv
     *            the fiche univ
     * @param langue
     *            the _langue
     *
     * @return the liste objets encadres
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceEncadre#getEncadreObjectList(String, FicheUniv, String)}
     */
    @Deprecated
    public static ArrayList<Encadre> getListeObjetsEncadres(final ContexteUniv ctx, final FicheUniv ficheUniv, final String langue) throws Exception {
        final String codeRubrique = ctx.getCodeRubriquePageCourante();
        return getListeObjetsEncadres(codeRubrique, ficheUniv, langue);
    }

    /* Renvoie la liste des objets sous forme de vecteur */

    /**
     * Récupération d'une liste d'encadrés triés par poids pour un type d'objet.
     *
     * @param codeRubrique
     *            le code rubrique dont on souhaite récupérer les encadrés
     * @param ficheUniv
     *            la fiche dont on souhaite récupérer les encadrés
     * @param langue
     *            la langue des encadrés
     *
     * @return the liste objets encadres
     *
     * @throws Exception
     *             lors de la récupération des encadrés en base
     * @deprecated Utilisez {@link ServiceEncadre#getEncadreObjectList(String, FicheUniv, String)}
     */
    @Deprecated
    public static ArrayList<Encadre> getListeObjetsEncadres(final String codeRubrique, final FicheUniv ficheUniv, final String langue) {
        final ServiceEncadre serviceEncadre = ServiceManager.getServiceForBean(EncadreBean.class);
        final ArrayList<Encadre> result = new ArrayList<>();
        final List<EncadreBean> encadres = serviceEncadre.getEncadreObjectList(codeRubrique, ficheUniv, langue);
        for (EncadreBean currentEncadre : encadres) {
            final Encadre encadre = new Encadre();
            encadre.setPersistenceBean(currentEncadre);
            result.add(encadre);
        }
        return result;
    }

    /**
     * Récupération d'une liste d'encadrés triés par poids pour un type d'objet.
     *
     * @param ctx
     *            Le contexte juste pour le code de rubrique de la page courante...
     * @param ficheUniv
     *            la fiche dont on souhaite récupérer les encadrés
     * @param langue
     *            la langue po
     *
     * @return the liste encadres
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceEncadre#getEncadresList(String, FicheUniv, String)}}
     */
    @Deprecated
    public static ArrayList<String> getListeEncadres(final ContexteUniv ctx, final FicheUniv ficheUniv, final String langue) throws Exception {
        return getListeEncadres(ctx.getCodeRubriquePageCourante(), ficheUniv, langue);
    }

    /**
     * Récupération d'une liste d'encadrés triés par poids pour un type d'objet.
     *
     * @param codeRubrique
     *            Le code de la rubrique dont on souhaite récupérer les encadrés
     * @param ficheUniv
     *            la fiche dont on souhaite récupérer les encadrés
     * @param langue
     *            la langue po
     *
     * @return the liste encadres
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceEncadre#getEncadresList(String, FicheUniv, String)}
     */
    @Deprecated
    public static ArrayList<String> getListeEncadres(final String codeRubrique, final FicheUniv ficheUniv, final String langue) throws Exception {
        final ServiceEncadre serviceEncadre = ServiceManager.getServiceForBean(EncadreBean.class);
        return serviceEncadre.getEncadresList(codeRubrique, ficheUniv, langue);
    }

    /**
     * implementation de la methode clone() (utilisée dans le module catalogue)
     */
    @Override
    public Encadre clone() throws CloneNotSupportedException {
        return (Encadre) super.clone();
    }

    /**
     * Inits the.
     * @deprecated Appel inutile
     */
    @Deprecated
    public void init() {
        setIdEncadre(System.currentTimeMillis());
        setIntitule("");
        setActif("1");
        setLangue("0");
        setPoids(0);
        setContenu("");
        setObjets("");
        // JSS 20050713 : ajout code pour e-sup
        setCode("");
        setCodeRattachement("");
        setCodeRubrique("");
    }

    /**
     * Gets the vecteur objets.
     *
     * @return the vecteur objets
     *
     * @throws Exception
     *             the exception
     * @deprecated
     */
    @Deprecated
    public List<String> getVecteurObjets() {
        return Arrays.asList(StringUtils.split(getObjets(), ";"));
    }

    /**
     * Sets the vecteur objets.
     *
     * @param v
     *            the new vecteur objets
     *
     * @throws Exception
     *             the exception
     * @deprecated
     */
    @Deprecated
    public void setVecteurObjets(final Vector<String> v) {
        setObjets(StringUtils.defaultString(StringUtils.join(v, ";")));
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public Long getIdEncadre() {
        return persistenceBean.getId();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setIdEncadre(Long idEncadre) {
        persistenceBean.setId(idEncadre);
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public String getIntitule() {
        return persistenceBean.getIntitule();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setIntitule(String intitule) {
        persistenceBean.setIntitule(intitule);
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public String getActif() {
        return persistenceBean.getActif();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setActif(String actif) {
        persistenceBean.setActif(actif);
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public String getLangue() {
        return persistenceBean.getLangue();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setLangue(String langue) {
        persistenceBean.setLangue(langue);
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public Integer getPoids() {
        return persistenceBean.getPoids();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setPoids(Integer poids) {
        persistenceBean.setPoids(poids);
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public String getContenu() {
        return persistenceBean.getContenu();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setContenu(String contenu) {
        persistenceBean.setContenu(contenu);
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public String getObjets() {
        return persistenceBean.getObjets();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setObjets(String objets) {
        persistenceBean.setObjets(objets);
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public String getCode() {
        return persistenceBean.getCode();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setCode(String code) {
        persistenceBean.setCode(code);
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public String getCodeRattachement() {
        return persistenceBean.getCodeRattachement();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setCodeRattachement(String codeRattachement) {
        persistenceBean.setCodeRattachement(codeRattachement);
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public String getCodeRubrique() {
        return persistenceBean.getCodeRubrique();
    }

    /**
     * @deprecated Seulement présent pour la rétro-compatibilité. Passez par {@link EncadreBean} directement.
     * @see EncadreBean
     * @see com.univ.objetspartages.services.ServiceEncadre
     */
    @Deprecated
    public void setCodeRubrique(String codeRubrique) {
        persistenceBean.setCodeRubrique(codeRubrique);
    }
}
