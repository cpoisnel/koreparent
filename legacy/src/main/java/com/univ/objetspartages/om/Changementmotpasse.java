package com.univ.objetspartages.om;

import com.univ.objetspartages.sgbd.ChangementmotpasseDB;
// TODO: Auto-generated Javadoc

/**
 * The Class Changementmotpasse.
 * @deprecated les changements de mot de passe sont maintenant géré par {@link com.univ.objetspartages.services.ServiceUserPass}
 */
@Deprecated
public class Changementmotpasse extends ChangementmotpasseDB {

    /**
     * Instantiates a new changementmotpasse.
     */
    public Changementmotpasse() {
        super();
    }
}
