package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.Collection;
import java.util.Vector;

import com.univ.objetspartages.bean.ProfildsiBean;

/**
 * Informations stockées en mémoire pour un profil DSI.
 * @deprecated Utiliser {@link com.univ.objetspartages.bean.ProfildsiBean} avec le service {@link com.univ.objetspartages.services.ServiceProfildsi}
 */
@Deprecated
public class InfosProfilDsi implements Serializable {

    /** The code. */
    private String code = "";

    /** The intitule. */
    private String intitule = "";

    /** The roles. */
    private String roles = "";

    /** The code rattachement. */
    private String codeRattachement = "";

    /** The groupes. */
    private Vector<String> groupes = new Vector<>();

    /** The code page accueil. */
    private String codeRubriqueAccueil = "";

    /**
     * Constructeur InfosGroupeDSI.
     */
    public InfosProfilDsi() {}

    /**
     * Commentaire relatif au constructeur InfosGroupeDSI.
     *
     * @param profildsiBean : un {@link com.univ.objetspartages.bean.ProfildsiBean} à partir du quel les valeurs seront mappées.
     */
    public InfosProfilDsi(ProfildsiBean profildsiBean) {
        if(profildsiBean != null) {
            this.code = profildsiBean.getCode();
            this.intitule = profildsiBean.getLibelle();
            this.codeRubriqueAccueil = profildsiBean.getCodeRubriqueAccueil();
            this.roles = profildsiBean.getRoles();
            this.groupes = new Vector<>(profildsiBean.getGroupes());
            this.codeRattachement = profildsiBean.getCodeRattachement();
        }
    }

    /**
     * Commentaire relatif au constructeur InfosGroupeDSI.
     *
     * @param code
     *            the code
     * @param intitule
     *            the intitule
     * @param codePageAccueil
     *            the code page accueil
     * @param roles
     *            the roles
     * @param codeRattachement
     *            the code rattachement
     * @param groupes
     *            the groupes
     */
    public InfosProfilDsi(String code, String intitule, String codePageAccueil, String roles, String codeRattachement, Collection<String> groupes) {
        this.code = code;
        this.intitule = intitule;
        this.codeRubriqueAccueil = codePageAccueil;
        this.roles = roles;
        this.groupes = new Vector<>(groupes);
        this.codeRattachement = codeRattachement;
    }

    /**
     * Gets the groupes.
     *
     * @return Returns the groupes.
     */
    public Vector<String> getGroupes() {
        return groupes;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the intitule.
     *
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Gets the code page accueil.
     *
     * @return the code page accueil
     */
    public String getCodeRubriqueAccueil() {
        return codeRubriqueAccueil;
    }

    /**
     * Gets the code rattachement.
     *
     * @return the code rattachement
     */
    public String getCodeRattachement() {
        return codeRattachement;
    }

    /**
     * Gets the roles.
     *
     * @return Returns the roles.
     */
    public String getRoles() {
        return roles;
    }
}
