package com.univ.objetspartages.om;
// TODO: Auto-generated Javadoc

/**
 * Insérez la description du type à cet endroit. Date de création : (28/02/2002 09:38:10)
 *
 * @deprecated cette classe n'a plus d'intêret depuis la 6.0. elle servait à récupérer les données des
 * objets type ficheUniv qui sont maintenant déclarés dans Spring.
 * @author :
 */
@Deprecated
public interface DonneesReferentiels {

    /**
     * Gets the caracteristiques objets.
     *
     * @return the caracteristiques objets
     */
    public String[][] getCaracteristiquesObjets();

    /**
     * Gets the ordre onglets.
     *
     * @return the ordre onglets
     */
    public String[] getOrdreOnglets();
}