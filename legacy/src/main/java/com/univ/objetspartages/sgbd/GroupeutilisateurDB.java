package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
// TODO: Auto-generated Javadoc

/**
 * The Class GroupeutilisateurDB.
 * @deprecated utiliser {@link com.univ.objetspartages.bean.GroupeUtilisateurBean} pour les données & {@link com.univ.objetspartages.dao.impl.GroupeUtilisateurDAO} pour l'acces au données
 */
@Deprecated
public class GroupeutilisateurDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet _rs = null;

    /** The id groupeutilisateur. */
    private Long idGroupeutilisateur = null;

    /** The code groupe. */
    private String codeGroupe = null;

    /** The code utilisateur. */
    private String codeUtilisateur = null;

    /** The source import. */
    private String sourceImport = null;

    /**
     * Instantiates a new groupeutilisateur db.
     */
    public GroupeutilisateurDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "GROUPEUTILISATEUR (ID_GROUPEUTILISATEUR  ,   CODE_GROUPE  ,   CODE_UTILISATEUR  ,   SOURCE_IMPORT  ) VALUES (?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdGroupeutilisateur(), Types.BIGINT);
            _stmt.setObject(2, getCodeGroupe(), Types.VARCHAR);
            _stmt.setObject(3, getCodeUtilisateur(), Types.VARCHAR);
            _stmt.setObject(4, getSourceImport(), Types.VARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdGroupeutilisateur(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement(String.format("DELETE FROM %sGROUPEUTILISATEUR WHERE ID_GROUPEUTILISATEUR = ? ", qualifier));
            // put parameters into statement
            _stmt.setObject(1, getIdGroupeutilisateur(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id groupeutilisateur.
     *
     * @return the id groupeutilisateur
     */
    public Long getIdGroupeutilisateur() {
        return idGroupeutilisateur;
    }

    /**
     * Gets the code groupe.
     *
     * @return the code groupe
     */
    public String getCodeGroupe() {
        return codeGroupe;
    }

    /**
     * Gets the code utilisateur.
     *
     * @return the code utilisateur
     */
    public String getCodeUtilisateur() {
        return codeUtilisateur;
    }

    /**
     * Gets the source import.
     *
     * @return the source import
     */
    public String getSourceImport() {
        return sourceImport;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return String.format("SELECT DISTINCT T1.ID_GROUPEUTILISATEUR , T1.CODE_GROUPE, T1.CODE_UTILISATEUR, T1.SOURCE_IMPORT FROM %sGROUPEUTILISATEUR T1 ", qualifier);
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(String.format("SELECT ID_GROUPEUTILISATEUR, CODE_GROUPE, CODE_UTILISATEUR, SOURCE_IMPORT FROM %sGROUPEUTILISATEUR WHERE ID_GROUPEUTILISATEUR = ? ", qualifier));
            // put parameters into statement
            _stmt.setObject(1, getIdGroupeutilisateur(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdGroupeutilisateur(new Long(_rs.getLong(1)));
            setCodeGroupe(_rs.getString(2));
            setCodeUtilisateur(_rs.getString(3));
            setSourceImport(_rs.getString(4));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            /* éxécution requete */
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "GROUPEUTILISATEUR T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id groupeutilisateur.
     *
     * @param _idGroupeutilisateur
     *            the new id groupeutilisateur
     */
    public void setIdGroupeutilisateur(final Long _idGroupeutilisateur) {
        idGroupeutilisateur = _idGroupeutilisateur;
    }

    /**
     * Sets the code groupe.
     *
     * @param _codeGroupe
     *            the new code groupe
     */
    public void setCodeGroupe(final String _codeGroupe) {
        codeGroupe = _codeGroupe;
    }

    /**
     * Sets the code utilisateur.
     *
     * @param _codeUtilisateur
     *            the new code utilisateur
     */
    public void setCodeUtilisateur(final String _codeUtilisateur) {
        codeUtilisateur = _codeUtilisateur;
    }

    /**
     * Sets the source import.
     *
     * @param _sourceImport
     *            the new source import
     */
    public void setSourceImport(final String _sourceImport) {
        sourceImport = _sourceImport;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (StringUtils.isNotEmpty(_qualifier)) {
            if (!_qualifier.contains(".")) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdGroupeutilisateur() + aSeparator + getCodeGroupe() + aSeparator + getCodeUtilisateur() + aSeparator + getSourceImport()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement(String.format("UPDATE %sGROUPEUTILISATEUR SET ID_GROUPEUTILISATEUR = ?, CODE_GROUPE = ?, CODE_UTILISATEUR = ?, SOURCE_IMPORT = ? WHERE ID_GROUPEUTILISATEUR = ? ", qualifier));
            // put parameters into statement
            _stmt.setObject(1, getIdGroupeutilisateur(), Types.BIGINT);
            _stmt.setObject(2, getCodeGroupe(), Types.VARCHAR);
            _stmt.setObject(3, getCodeUtilisateur(), Types.VARCHAR);
            _stmt.setObject(4, getSourceImport(), Types.VARCHAR);
            _stmt.setObject(5, getIdGroupeutilisateur(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
