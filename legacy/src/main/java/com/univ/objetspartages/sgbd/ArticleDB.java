package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.univ.objetspartages.bean.ArticleBean;
// TODO: Auto-generated Javadoc

/**
 * Classe d'acces aux donnees pour article.
 *
 * @deprecated utilisez {@link com.univ.objetspartages.dao.impl.ArticleDAO} et {@link ArticleBean}
 */
@Deprecated
public class ArticleDB extends ArticleBean {

    /**
     *
     */
    private static final long serialVersionUID = 4222997761779351527L;

    /** The ctx. */
    protected transient OMContext ctx = null;

    /** The rs. */
    private transient ResultSet rs = null;

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        final String s = "" + getIdArticle() + aSeparator + getTitre() + aSeparator + getSousTitre() + aSeparator + getDateArticle() + aSeparator + getIdVignette() + aSeparator + getChapeau() + aSeparator + getCorps() + aSeparator + getThematique() + aSeparator + getOrdre() + aSeparator + getCode() + aSeparator + getCodeRubrique() + aSeparator + getCodeRattachement() + aSeparator + getMetaKeywords() + aSeparator + getMetaDescription() + aSeparator + getTitreEncadre() + aSeparator + getContenuEncadre() + aSeparator + getEncadreRecherche() + aSeparator + getEncadreRechercheBis() + aSeparator + getDateAlerte() + aSeparator + getMessageAlerte() + aSeparator + getDateCreation() + aSeparator + getDateProposition() + aSeparator + getDateValidation() + aSeparator + getDateModification() + aSeparator + getCodeRedacteur() + aSeparator + getCodeValidation() + aSeparator + getLangue() + aSeparator + getEtatObjet() + aSeparator + getNbHits() + aSeparator + getDiffusionPublicVise() + aSeparator + getDiffusionModeRestriction() + aSeparator + getDiffusionPublicViseRestriction();
        return s;
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        ctx = _ctx;
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("insert into ARTICLE (ID_ARTICLE , TITRE , SOUS_TITRE , DATE_ARTICLE , ID_VIGNETTE , CHAPEAU , CORPS , THEMATIQUE , ORDRE , CODE , CODE_RUBRIQUE , CODE_RATTACHEMENT , META_KEYWORDS , META_DESCRIPTION , TITRE_ENCADRE , CONTENU_ENCADRE , ENCADRE_RECHERCHE , ENCADRE_RECHERCHE_BIS , DATE_ALERTE , MESSAGE_ALERTE , DATE_CREATION , DATE_PROPOSITION , DATE_VALIDATION , DATE_MODIFICATION , CODE_REDACTEUR , CODE_VALIDATION , LANGUE , ETAT_OBJET , NB_HITS , DIFFUSION_PUBLIC_VISE , DIFFUSION_MODE_RESTRICTION , DIFFUSION_PUBLIC_VISE_RESTRICTION) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setObject(1, getIdArticle(), Types.BIGINT);
            stmt.setObject(2, getTitre(), Types.VARCHAR);
            stmt.setObject(3, getSousTitre(), Types.LONGVARCHAR);
            stmt.setObject(4, SqlDateConverter.toDate(getDateArticle()), Types.DATE);
            stmt.setObject(5, getIdVignette(), Types.BIGINT);
            stmt.setObject(6, getChapeau(), Types.LONGVARCHAR);
            stmt.setObject(7, getCorps(), Types.LONGVARCHAR);
            stmt.setObject(8, getThematique(), Types.VARCHAR);
            stmt.setObject(9, getOrdre(), Types.INTEGER);
            stmt.setObject(10, getCode(), Types.VARCHAR);
            stmt.setObject(11, getCodeRubrique(), Types.VARCHAR);
            stmt.setObject(12, getCodeRattachement(), Types.VARCHAR);
            stmt.setObject(13, getMetaKeywords(), Types.LONGVARCHAR);
            stmt.setObject(14, getMetaDescription(), Types.LONGVARCHAR);
            stmt.setObject(15, getTitreEncadre(), Types.VARCHAR);
            stmt.setObject(16, getContenuEncadre(), Types.LONGVARCHAR);
            stmt.setObject(17, getEncadreRecherche(), Types.VARCHAR);
            stmt.setObject(18, getEncadreRechercheBis(), Types.VARCHAR);
            stmt.setObject(19, SqlDateConverter.toDate(getDateAlerte()), Types.DATE);
            stmt.setObject(20, getMessageAlerte(), Types.LONGVARCHAR);
            stmt.setObject(21, SqlDateConverter.toTimestamp(getDateCreation()), Types.TIMESTAMP);
            stmt.setObject(22, SqlDateConverter.toTimestamp(getDateProposition()), Types.TIMESTAMP);
            stmt.setObject(23, SqlDateConverter.toTimestamp(getDateValidation()), Types.TIMESTAMP);
            stmt.setObject(24, SqlDateConverter.toTimestamp(getDateModification()), Types.TIMESTAMP);
            stmt.setObject(25, getCodeRedacteur(), Types.VARCHAR);
            stmt.setObject(26, getCodeValidation(), Types.VARCHAR);
            stmt.setObject(27, getLangue(), Types.VARCHAR);
            stmt.setObject(28, getEtatObjet(), Types.VARCHAR);
            stmt.setObject(29, getNbHits(), Types.BIGINT);
            stmt.setObject(30, getDiffusionPublicVise(), Types.VARCHAR);
            stmt.setObject(31, getDiffusionModeRestriction(), Types.VARCHAR);
            stmt.setObject(32, getDiffusionPublicViseRestriction(), Types.VARCHAR);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            final ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            setIdArticle(rs.getLong(1));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD add()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("delete from ARTICLE " + "where ID_ARTICLE = ?");
            // put parameters into statement
            stmt.setObject(1, getIdArticle(), Types.BIGINT);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD delete()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        final String query = "select distinct " + "T1.ID_ARTICLE, " + "T1.TITRE, " + "T1.SOUS_TITRE, " + "T1.DATE_ARTICLE, " + "T1.ID_VIGNETTE, " + "T1.CHAPEAU, " + "T1.CORPS, " + "T1.THEMATIQUE, " + "T1.ORDRE, " + "T1.CODE, " + "T1.CODE_RUBRIQUE, " + "T1.CODE_RATTACHEMENT, " + "T1.META_KEYWORDS, " + "T1.META_DESCRIPTION, " + "T1.TITRE_ENCADRE, " + "T1.CONTENU_ENCADRE, " + "T1.ENCADRE_RECHERCHE, " + "T1.ENCADRE_RECHERCHE_BIS, " + "T1.DATE_ALERTE, " + "T1.MESSAGE_ALERTE, " + "T1.DATE_CREATION, " + "T1.DATE_PROPOSITION, " + "T1.DATE_VALIDATION, " + "T1.DATE_MODIFICATION, " + "T1.CODE_REDACTEUR, " + "T1.CODE_VALIDATION, " + "T1.LANGUE, " + "T1.ETAT_OBJET, " + "T1.NB_HITS, " + "T1.DIFFUSION_PUBLIC_VISE, " + "T1.DIFFUSION_MODE_RESTRICTION, " + "T1.DIFFUSION_PUBLIC_VISE_RESTRICTION " + "from ARTICLE T1 ";
        return query;
    }

    /**
     * Gets the sQL base optimized query.
     *
     * @return the sQL base optimized query
     */
    public String getSQLBaseOptimizedQuery() {
        final String query = "select " + "T1.ID_ARTICLE, " + "T1.CODE, " + "T1.LANGUE " + "from ARTICLE T1 ";
        return query;
    }

    /**
     * Renvoie l'element suivant du ResultSet.
     *
     * @return true, if next item
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                if (ctx.getDatas().get("optimizedSelect") != null) {
                    retrieveFromOptimizedRS();
                } else {
                    retrieveFromRS();
                }
                res = true;
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem()", e);
        }
        return res;
    }

    /**
     * Recuperation d'une ligne de la base de donnees.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("select " + "T1.ID_ARTICLE, " + "T1.TITRE, " + "T1.SOUS_TITRE, " + "T1.DATE_ARTICLE, " + "T1.ID_VIGNETTE, " + "T1.CHAPEAU, " + "T1.CORPS, " + "T1.THEMATIQUE, " + "T1.ORDRE, " + "T1.CODE, " + "T1.CODE_RUBRIQUE, " + "T1.CODE_RATTACHEMENT, " + "T1.META_KEYWORDS, " + "T1.META_DESCRIPTION, " + "T1.TITRE_ENCADRE, " + "T1.CONTENU_ENCADRE, " + "T1.ENCADRE_RECHERCHE, " + "T1.ENCADRE_RECHERCHE_BIS, " + "T1.DATE_ALERTE, " + "T1.MESSAGE_ALERTE, " + "T1.DATE_CREATION, " + "T1.DATE_PROPOSITION, " + "T1.DATE_VALIDATION, " + "T1.DATE_MODIFICATION, " + "T1.CODE_REDACTEUR, " + "T1.CODE_VALIDATION, " + "T1.LANGUE, " + "T1.ETAT_OBJET, " + "T1.NB_HITS, " + "T1.DIFFUSION_PUBLIC_VISE, " + "T1.DIFFUSION_MODE_RESTRICTION, " + "T1.DIFFUSION_PUBLIC_VISE_RESTRICTION " + "from ARTICLE T1 " + "where T1.ID_ARTICLE = ?");
            stmt.setObject(1, getIdArticle(), Types.BIGINT);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve : METHOD_NO_RESULTS");
            }
            retrieveFromRS();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieve()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS() throws Exception {
        try {
            // get output from result set
            setIdArticle(rs.getLong(1));
            setTitre(rs.getString(2));
            setSousTitre(rs.getString(3));
            setDateArticle(new Date(rs.getDate(4).getTime()));
            setIdVignette(rs.getLong(5));
            setChapeau(rs.getString(6));
            setCorps(rs.getString(7));
            setThematique(rs.getString(8));
            setOrdre(rs.getInt(9));
            setCode(rs.getString(10));
            setCodeRubrique(rs.getString(11));
            setCodeRattachement(rs.getString(12));
            setMetaKeywords(rs.getString(13));
            setMetaDescription(rs.getString(14));
            setTitreEncadre(rs.getString(15));
            setContenuEncadre(rs.getString(16));
            setEncadreRecherche(rs.getString(17));
            setEncadreRechercheBis(rs.getString(18));
            setDateAlerte(SqlDateConverter.fromDate(rs.getDate(19)));
            setMessageAlerte(rs.getString(20));
            setDateCreation(SqlDateConverter.fromTimestamp(rs.getTimestamp(21)));
            setDateProposition(SqlDateConverter.fromTimestamp(rs.getTimestamp(22)));
            setDateValidation(SqlDateConverter.fromTimestamp(rs.getTimestamp(23)));
            setDateModification(SqlDateConverter.fromTimestamp(rs.getTimestamp(24)));
            setCodeRedacteur(rs.getString(25));
            setCodeValidation(rs.getString(26));
            setLangue(rs.getString(27));
            setEtatObjet(rs.getString(28));
            setNbHits(rs.getLong(29));
            setDiffusionPublicVise(rs.getString(30));
            setDiffusionModeRestriction(rs.getString(31));
            setDiffusionPublicViseRestriction(rs.getString(32));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS()", e);
        }
    }

    /**
     * Retrieve from optimized rs.
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromOptimizedRS() throws Exception {
        try {
            // get output from result set
            setIdArticle(rs.getLong(1));
            setCode(rs.getString(2));
            setLangue(rs.getString(3));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieveFromOptimizedRS()", e);
        }
    }

    /**
     * Cette methode optimise l'execution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(final String sqlSuffix) throws Exception {
        PreparedStatement stmt = null;
        try {
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String sqlSuffix) throws Exception {
        int count = 0;
        if (ctx.getDatas().get("optimizedObject") != null && ctx.getDatas().get("optimizedSelect") != null) {
            count = selectNoCountOptimized(sqlSuffix);
        } else {
            PreparedStatement stmt = null;
            try {
                // recuperation nombre de lignes
                String query = "select count(distinct T1.ID_ARTICLE) from ARTICLE T1 ";
                if (sqlSuffix != null) {
                    query += sqlSuffix;
                }
                stmt = getConnection().prepareStatement(query);
                rs = stmt.executeQuery();
                rs.next();
                count = rs.getInt(1);
                // execution requete
                query = getSQLBaseQuery();
                if (sqlSuffix != null) {
                    query += sqlSuffix;
                }
                stmt = getConnection().prepareStatement(query);
                rs = stmt.executeQuery();
            } catch (final SQLException e) {
                throw new Exception("SELECT_FAILED", e);
            }
        }
        return count;
    }

    /**
     * Select no count optimized.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    private int selectNoCountOptimized(final String sqlSuffix) throws Exception {
        PreparedStatement stmt = null;
        final int count = -1;
        try {
            String query = getSQLBaseOptimizedQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            if (ctx.getDatas().get("optimizedLimit") != null) {
                query += " limit 0," + ctx.getDatas().get("optimizedLimit") + "0";
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
        return count;
    }

    /**
     * Mise a jour d'une ligne de la base de donnees.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("update ARTICLE set " + "ID_ARTICLE = ?, " + "TITRE = ?, " + "SOUS_TITRE = ?, " + "DATE_ARTICLE = ?, " + "ID_VIGNETTE = ?, " + "CHAPEAU = ?, " + "CORPS = ?, " + "THEMATIQUE = ?, " + "ORDRE = ?, " + "CODE = ?, " + "CODE_RUBRIQUE = ?, " + "CODE_RATTACHEMENT = ?, " + "META_KEYWORDS = ?, " + "META_DESCRIPTION = ?, " + "TITRE_ENCADRE = ?, " + "CONTENU_ENCADRE = ?, " + "ENCADRE_RECHERCHE = ?, " + "ENCADRE_RECHERCHE_BIS = ?, " + "DATE_ALERTE = ?, " + "MESSAGE_ALERTE = ?, " + "DATE_CREATION = ?, " + "DATE_PROPOSITION = ?, " + "DATE_VALIDATION = ?, " + "DATE_MODIFICATION = ?, " + "CODE_REDACTEUR = ?, " + "CODE_VALIDATION = ?, " + "LANGUE = ?, " + "ETAT_OBJET = ?, " + "NB_HITS = ?, " + "DIFFUSION_PUBLIC_VISE = ?, " + "DIFFUSION_MODE_RESTRICTION = ?, " + "DIFFUSION_PUBLIC_VISE_RESTRICTION = ? " + "where ID_ARTICLE = ?");
            // put parameters into statement
            stmt.setObject(1, getIdArticle(), Types.BIGINT);
            stmt.setObject(2, getTitre(), Types.VARCHAR);
            stmt.setObject(3, getSousTitre(), Types.LONGVARCHAR);
            stmt.setObject(4, SqlDateConverter.toDate(getDateArticle()), Types.DATE);
            stmt.setObject(5, getIdVignette(), Types.BIGINT);
            stmt.setObject(6, getChapeau(), Types.LONGVARCHAR);
            stmt.setObject(7, getCorps(), Types.LONGVARCHAR);
            stmt.setObject(8, getThematique(), Types.VARCHAR);
            stmt.setObject(9, getOrdre(), Types.INTEGER);
            stmt.setObject(10, getCode(), Types.VARCHAR);
            stmt.setObject(11, getCodeRubrique(), Types.VARCHAR);
            stmt.setObject(12, getCodeRattachement(), Types.VARCHAR);
            stmt.setObject(13, getMetaKeywords(), Types.LONGVARCHAR);
            stmt.setObject(14, getMetaDescription(), Types.LONGVARCHAR);
            stmt.setObject(15, getTitreEncadre(), Types.VARCHAR);
            stmt.setObject(16, getContenuEncadre(), Types.LONGVARCHAR);
            stmt.setObject(17, getEncadreRecherche(), Types.VARCHAR);
            stmt.setObject(18, getEncadreRechercheBis(), Types.VARCHAR);
            stmt.setObject(19, SqlDateConverter.toDate(getDateAlerte()), Types.DATE);
            stmt.setObject(20, getMessageAlerte(), Types.LONGVARCHAR);
            stmt.setObject(21, SqlDateConverter.toTimestamp(getDateCreation()), Types.TIMESTAMP);
            stmt.setObject(22, SqlDateConverter.toTimestamp(getDateProposition()), Types.TIMESTAMP);
            stmt.setObject(23, SqlDateConverter.toTimestamp(getDateValidation()), Types.TIMESTAMP);
            stmt.setObject(24, SqlDateConverter.toTimestamp(getDateModification()), Types.TIMESTAMP);
            stmt.setObject(25, getCodeRedacteur(), Types.VARCHAR);
            stmt.setObject(26, getCodeValidation(), Types.VARCHAR);
            stmt.setObject(27, getLangue(), Types.VARCHAR);
            stmt.setObject(28, getEtatObjet(), Types.VARCHAR);
            stmt.setObject(29, getNbHits(), Types.BIGINT);
            stmt.setObject(30, getDiffusionPublicVise(), Types.VARCHAR);
            stmt.setObject(31, getDiffusionModeRestriction(), Types.VARCHAR);
            stmt.setObject(32, getDiffusionPublicViseRestriction(), Types.VARCHAR);
            stmt.setObject(33, getIdArticle(), Types.BIGINT);
            stmt.executeUpdate();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD update()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
}
