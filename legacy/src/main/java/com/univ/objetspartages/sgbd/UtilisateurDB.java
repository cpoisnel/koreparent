package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.univ.objetspartages.services.ServiceUser;
// TODO: Auto-generated Javadoc

/**
 * The Class UtilisateurDB.
 * @deprecated Utilisez {@link ServiceUser} à la place
 */
@Deprecated
public class UtilisateurDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet _rs = null;

    /** The id utilisateur. */
    private Long idUtilisateur = null;

    /** The code. */
    private String code = null;

    /** The mot de passe. */
    private String motDePasse = null;

    /** The date naissance. */
    private Date dateNaissance = null;

    /** The civilite. */
    private String civilite = null;

    /** The nom. */
    private String nom = null;

    /** The prenom. */
    private String prenom = null;

    /** The code rattachement. */
    private String codeRattachement = null;

    /** The groupes. */
    private String groupes = null;

    /** The adresse mail. */
    private String adresseMail = null;

    /** The restriction validation. */
    private String restrictionValidation = null;

    /** The extension modification. */
    private String extensionModification = null;

    /** The centres interet. */
    private String centresInteret = null;

    /** The profil dsi. */
    private String profilDsi = null;

    /** The groupes dsi. */
    private String groupesDsi = null;

    /** The code ldap. */
    private String codeLdap = null;

    /** The groupes dsi import. */
    private String groupesDsiImport = null;

    /** The roles. */
    private String roles = null;

    /** The date derniere session. */
    private Date dateDerniereSession = null;

    /** The profil defaut. */
    private String profilDefaut = null;

    /** The source import. */
    private String sourceImport = null;

    /** The format envoi. */
    private String formatEnvoi = null;

    /** The mode saisie expert. */
    private String modeSaisieExpert = null;

    /** The ts cache groupes. */
    private Long tsCacheGroupes = null;

    /**
     * Instantiates a new utilisateur db.
     */
    public UtilisateurDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try (PreparedStatement stmt = getConnection().prepareStatement(String.format(" INSERT INTO %sUTILISATEUR (ID_UTILISATEUR  ,   CODE  ,   MOT_DE_PASSE  ,   DATE_NAISSANCE  ,   CIVILITE  ,   NOM  ,   PRENOM  ,   CODE_RATTACHEMENT  ,   GROUPES  ,   ADRESSE_MAIL  ,   RESTRICTION_VALIDATION  ,   EXTENSION_MODIFICATION  ,   CENTRES_INTERET  ,   PROFIL_DSI  ,   GROUPES_DSI  ,   CODE_LDAP  ,   GROUPES_DSI_IMPORT  ,   ROLES  ,   DATE_DERNIERE_SESSION  ,   PROFIL_DEFAUT  ,   SOURCE_IMPORT  ,   FORMAT_ENVOI  ,   MODE_SAISIE_EXPERT  ,   TS_CACHE_GROUPES  )              VALUES (?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ) ", qualifier), Statement.RETURN_GENERATED_KEYS);) {
            stmt.setObject(1, getIdUtilisateur(), Types.BIGINT);
            stmt.setObject(2, getCode(), Types.VARCHAR);
            stmt.setObject(3, getMotDePasse(), Types.VARCHAR);
            stmt.setObject(4, SqlDateConverter.toDate(getDateNaissance()), Types.DATE);
            stmt.setObject(5, getCivilite(), Types.VARCHAR);
            stmt.setObject(6, getNom(), Types.VARCHAR);
            stmt.setObject(7, getPrenom(), Types.VARCHAR);
            stmt.setObject(8, getCodeRattachement(), Types.VARCHAR);
            stmt.setObject(9, getGroupes(), Types.LONGVARCHAR);
            stmt.setObject(10, getAdresseMail(), Types.VARCHAR);
            stmt.setObject(11, getRestrictionValidation(), Types.VARCHAR);
            stmt.setObject(12, getExtensionModification(), Types.VARCHAR);
            stmt.setObject(13, getCentresInteret(), Types.LONGVARCHAR);
            stmt.setObject(14, getProfilDsi(), Types.VARCHAR);
            stmt.setObject(15, getGroupesDsi(), Types.LONGVARCHAR);
            stmt.setObject(16, getCodeLdap(), Types.VARCHAR);
            stmt.setObject(17, getGroupesDsiImport(), Types.LONGVARCHAR);
            stmt.setObject(18, getRoles(), Types.LONGVARCHAR);
            stmt.setObject(19, SqlDateConverter.toDate(getDateDerniereSession()), Types.DATE);
            stmt.setObject(20, getProfilDefaut(), Types.VARCHAR);
            stmt.setObject(21, getSourceImport(), Types.VARCHAR);
            stmt.setObject(22, getFormatEnvoi(), Types.VARCHAR);
            stmt.setObject(23, getModeSaisieExpert(), Types.VARCHAR);
            stmt.setObject(24, getTsCacheGroupes(), Types.BIGINT);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = stmt.getGeneratedKeys();
            rs.next();
            setIdUtilisateur(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "UTILISATEUR WHERE " + "             ID_UTILISATEUR = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdUtilisateur(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id utilisateur.
     *
     * @return the id utilisateur
     */
    public Long getIdUtilisateur() {
        return idUtilisateur;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the mot de passe.
     *
     * @return the mot de passe
     */
    public String getMotDePasse() {
        return motDePasse;
    }

    /**
     * Gets the date naissance.
     *
     * @return the date naissance
     */
    public Date getDateNaissance() {
        return dateNaissance;
    }

    /**
     * Gets the civilite.
     *
     * @return the civilite
     */
    public String getCivilite() {
        return civilite;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Gets the prenom.
     *
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Gets the code rattachement.
     *
     * @return the code rattachement
     */
    public String getCodeRattachement() {
        return codeRattachement;
    }

    /**
     * Gets the groupes.
     *
     * @return the groupes
     */
    public String getGroupes() {
        return groupes;
    }

    /**
     * Gets the adresse mail.
     *
     * @return the adresse mail
     */
    public String getAdresseMail() {
        return adresseMail;
    }

    /**
     * Gets the restriction validation.
     *
     * @return the restriction validation
     */
    public String getRestrictionValidation() {
        return restrictionValidation;
    }

    /**
     * Gets the extension modification.
     *
     * @return the extension modification
     */
    public String getExtensionModification() {
        return extensionModification;
    }

    /**
     * Gets the centres interet.
     *
     * @return the centres interet
     */
    public String getCentresInteret() {
        return centresInteret;
    }

    /**
     * Gets the profil dsi.
     *
     * @return the profil dsi
     */
    public String getProfilDsi() {
        return profilDsi;
    }

    /**
     * Gets the groupes dsi.
     *
     * @return the groupes dsi
     */
    public String getGroupesDsi() {
        return groupesDsi;
    }

    /**
     * Gets the code ldap.
     *
     * @return the code ldap
     */
    public String getCodeLdap() {
        return codeLdap;
    }

    /**
     * Gets the groupes dsi import.
     *
     * @return the groupes dsi import
     */
    public String getGroupesDsiImport() {
        return groupesDsiImport;
    }

    /**
     * Gets the roles.
     *
     * @return the roles
     */
    public String getRoles() {
        return roles;
    }

    /**
     * Gets the date derniere session.
     *
     * @return the date derniere session
     */
    public Date getDateDerniereSession() {
        return dateDerniereSession;
    }

    /**
     * Gets the profil defaut.
     *
     * @return the profil defaut
     */
    public String getProfilDefaut() {
        return profilDefaut;
    }

    /**
     * Gets the source import.
     *
     * @return the source import
     */
    public String getSourceImport() {
        return sourceImport;
    }

    /**
     * Gets the format envoi.
     *
     * @return the format envoi
     */
    public String getFormatEnvoi() {
        return formatEnvoi;
    }

    /**
     * Gets the mode saisie expert.
     *
     * @return the mode saisie expert
     */
    public String getModeSaisieExpert() {
        return modeSaisieExpert;
    }

    /**
     * Gets the ts cache groupes.
     *
     * @return the ts cache groupes
     */
    public Long getTsCacheGroupes() {
        return tsCacheGroupes;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT " + "T1.ID_UTILISATEUR ,  " + "T1.CODE ,  " + "T1.MOT_DE_PASSE ,  " + "T1.DATE_NAISSANCE ,  " + "T1.CIVILITE ,  " + "T1.NOM ,  " + "T1.PRENOM ,  " + "T1.CODE_RATTACHEMENT ,  " + "T1.GROUPES ,  " + "T1.ADRESSE_MAIL ,  " + "T1.RESTRICTION_VALIDATION ,  " + "T1.EXTENSION_MODIFICATION ,  " + "T1.CENTRES_INTERET ,  " + "T1.PROFIL_DSI ,  " + "T1.GROUPES_DSI ,  " + "T1.CODE_LDAP ,  " + "T1.GROUPES_DSI_IMPORT ,  " + "T1.ROLES ,  " + "T1.DATE_DERNIERE_SESSION ,  " + "T1.PROFIL_DEFAUT ,  " + "T1.SOURCE_IMPORT ,  " + "T1.FORMAT_ENVOI ,  " + "T1.MODE_SAISIE_EXPERT ,  " + "T1.TS_CACHE_GROUPES " + "         FROM " + qualifier + "UTILISATEUR T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_UTILISATEUR," + "             CODE," + "             MOT_DE_PASSE," + "             DATE_NAISSANCE," + "             CIVILITE," + "             NOM," + "             PRENOM," + "             CODE_RATTACHEMENT," + "             GROUPES," + "             ADRESSE_MAIL," + "             RESTRICTION_VALIDATION," + "             EXTENSION_MODIFICATION," + "             CENTRES_INTERET," + "             PROFIL_DSI," + "             GROUPES_DSI," + "             CODE_LDAP," + "             GROUPES_DSI_IMPORT," + "             ROLES," + "             DATE_DERNIERE_SESSION," + "             PROFIL_DEFAUT," + "             SOURCE_IMPORT," + "             FORMAT_ENVOI," + "             MODE_SAISIE_EXPERT," + "             TS_CACHE_GROUPES" + "         FROM " + qualifier + "UTILISATEUR WHERE " + "             ID_UTILISATEUR = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdUtilisateur(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdUtilisateur(_rs.getLong(1));
            setCode(_rs.getString(2));
            setMotDePasse(_rs.getString(3));
            setDateNaissance(SqlDateConverter.fromDate(_rs.getDate(4)));
            setCivilite(_rs.getString(5));
            setNom(_rs.getString(6));
            setPrenom(_rs.getString(7));
            setCodeRattachement(_rs.getString(8));
            setGroupes(_rs.getString(9));
            setAdresseMail(_rs.getString(10));
            setRestrictionValidation(_rs.getString(11));
            setExtensionModification(_rs.getString(12));
            setCentresInteret(_rs.getString(13));
            setProfilDsi(_rs.getString(14));
            setGroupesDsi(_rs.getString(15));
            setCodeLdap(_rs.getString(16));
            setGroupesDsiImport(_rs.getString(17));
            setRoles(_rs.getString(18));
            setDateDerniereSession(SqlDateConverter.fromDate(_rs.getDate(19)));
            setProfilDefaut(_rs.getString(20));
            setSourceImport(_rs.getString(21));
            setFormatEnvoi(_rs.getString(22));
            setModeSaisieExpert(_rs.getString(23));
            setTsCacheGroupes(_rs.getLong(24));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            /* éxécution requete */
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(DISTINCT(ID_UTILISATEUR)) FROM " + qualifier + "UTILISATEUR T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id utilisateur.
     *
     * @param _idUtilisateur
     *            the new id utilisateur
     */
    public void setIdUtilisateur(final Long _idUtilisateur) {
        idUtilisateur = _idUtilisateur;
    }

    /**
     * Sets the code.
     *
     * @param _code
     *            the new code
     */
    public void setCode(final String _code) {
        code = _code;
    }

    /**
     * Sets the mot de passe.
     *
     * @param _motDePasse
     *            the new mot de passe
     */
    public void setMotDePasse(final String _motDePasse) {
        motDePasse = _motDePasse;
    }

    /**
     * Sets the date naissance.
     *
     * @param _dateNaissance
     *            the new date naissance
     */
    public void setDateNaissance(final Date _dateNaissance) {
        dateNaissance = _dateNaissance;
    }

    /**
     * Sets the civilite.
     *
     * @param _civilite
     *            the new civilite
     */
    public void setCivilite(final String _civilite) {
        civilite = _civilite;
    }

    /**
     * Sets the nom.
     *
     * @param _nom
     *            the new nom
     */
    public void setNom(final String _nom) {
        nom = _nom;
    }

    /**
     * Sets the prenom.
     *
     * @param _prenom
     *            the new prenom
     */
    public void setPrenom(final String _prenom) {
        prenom = _prenom;
    }

    /**
     * Sets the code rattachement.
     *
     * @param _codeRattachement
     *            the new code rattachement
     */
    public void setCodeRattachement(final String _codeRattachement) {
        codeRattachement = _codeRattachement;
    }

    /**
     * Sets the groupes.
     *
     * @param _groupes
     *            the new groupes
     */
    public void setGroupes(final String _groupes) {
        groupes = _groupes;
    }

    /**
     * Sets the adresse mail.
     *
     * @param _adresseMail
     *            the new adresse mail
     */
    public void setAdresseMail(final String _adresseMail) {
        adresseMail = _adresseMail;
    }

    /**
     * Sets the restriction validation.
     *
     * @param _restrictionValidation
     *            the new restriction validation
     */
    public void setRestrictionValidation(final String _restrictionValidation) {
        restrictionValidation = _restrictionValidation;
    }

    /**
     * Sets the extension modification.
     *
     * @param _extensionModification
     *            the new extension modification
     */
    public void setExtensionModification(final String _extensionModification) {
        extensionModification = _extensionModification;
    }

    /**
     * Sets the centres interet.
     *
     * @param _centresInteret
     *            the new centres interet
     */
    public void setCentresInteret(final String _centresInteret) {
        centresInteret = _centresInteret;
    }

    /**
     * Sets the profil dsi.
     *
     * @param _profilDsi
     *            the new profil dsi
     */
    public void setProfilDsi(final String _profilDsi) {
        profilDsi = _profilDsi;
    }

    /**
     * Sets the groupes dsi.
     *
     * @param _groupesDsi
     *            the new groupes dsi
     */
    public void setGroupesDsi(final String _groupesDsi) {
        groupesDsi = _groupesDsi;
    }

    /**
     * Sets the code ldap.
     *
     * @param _codeLdap
     *            the new code ldap
     */
    public void setCodeLdap(final String _codeLdap) {
        codeLdap = _codeLdap;
    }

    /**
     * Sets the groupes dsi import.
     *
     * @param _groupesDsiImport
     *            the new groupes dsi import
     */
    public void setGroupesDsiImport(final String _groupesDsiImport) {
        groupesDsiImport = _groupesDsiImport;
    }

    /**
     * Sets the roles.
     *
     * @param _roles
     *            the new roles
     */
    public void setRoles(final String _roles) {
        roles = _roles;
    }

    /**
     * Sets the date derniere session.
     *
     * @param _dateDerniereSession
     *            the new date derniere session
     */
    public void setDateDerniereSession(final Date _dateDerniereSession) {
        dateDerniereSession = _dateDerniereSession;
    }

    /**
     * Sets the profil defaut.
     *
     * @param _profilDefaut
     *            the new profil defaut
     */
    public void setProfilDefaut(final String _profilDefaut) {
        profilDefaut = _profilDefaut;
    }

    /**
     * Sets the source import.
     *
     * @param _sourceImport
     *            the new source import
     */
    public void setSourceImport(final String _sourceImport) {
        sourceImport = _sourceImport;
    }

    /**
     * Sets the format envoi.
     *
     * @param _formatEnvoi
     *            the new format envoi
     */
    public void setFormatEnvoi(final String _formatEnvoi) {
        formatEnvoi = _formatEnvoi;
    }

    /**
     * Sets the mode saisie expert.
     *
     * @param _modeSaisieExpert
     *            the new mode saisie expert
     */
    public void setModeSaisieExpert(final String _modeSaisieExpert) {
        modeSaisieExpert = _modeSaisieExpert;
    }

    /**
     * Sets the ts cache groupes.
     *
     * @param _tsCacheGroupes
     *            the new ts cache groupes
     */
    public void setTsCacheGroupes(final Long _tsCacheGroupes) {
        tsCacheGroupes = _tsCacheGroupes;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (StringUtils.isNotBlank(_qualifier)) {
            if (StringUtils.isNotBlank(_qualifier.substring(_qualifier.length() - 1))) {
                qualifier = _qualifier;
            } else {
                qualifier = _qualifier + ".";
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdUtilisateur() + aSeparator + getCode() + aSeparator + getMotDePasse() + aSeparator + getDateNaissance() + aSeparator + getCivilite() + aSeparator + getNom() + aSeparator + getPrenom() + aSeparator + getCodeRattachement() + aSeparator + getGroupes() + aSeparator + getAdresseMail() + aSeparator + getRestrictionValidation() + aSeparator + getExtensionModification() + aSeparator + getCentresInteret() + aSeparator + getProfilDsi() + aSeparator + getGroupesDsi() + aSeparator + getCodeLdap() + aSeparator + getGroupesDsiImport() + aSeparator + getRoles() + aSeparator + getDateDerniereSession() + aSeparator + getProfilDefaut() + aSeparator + getSourceImport() + aSeparator + getFormatEnvoi() + aSeparator + getModeSaisieExpert() + aSeparator + getTsCacheGroupes()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "UTILISATEUR SET " + "ID_UTILISATEUR = ?, " + "CODE = ?, " + "MOT_DE_PASSE = ?, " + "DATE_NAISSANCE = ?, " + "CIVILITE = ?, " + "NOM = ?, " + "PRENOM = ?, " + "CODE_RATTACHEMENT = ?, " + "GROUPES = ?, " + "ADRESSE_MAIL = ?, " + "RESTRICTION_VALIDATION = ?, " + "EXTENSION_MODIFICATION = ?, " + "CENTRES_INTERET = ?, " + "PROFIL_DSI = ?, " + "GROUPES_DSI = ?, " + "CODE_LDAP = ?, " + "GROUPES_DSI_IMPORT = ?, " + "ROLES = ?, " + "DATE_DERNIERE_SESSION = ?, " + "PROFIL_DEFAUT = ?, " + "SOURCE_IMPORT = ?, " + "FORMAT_ENVOI = ?, " + "MODE_SAISIE_EXPERT = ?, " + "TS_CACHE_GROUPES = ? " + "         WHERE " + "             ID_UTILISATEUR = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdUtilisateur(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getMotDePasse(), Types.VARCHAR);
            _stmt.setObject(4, SqlDateConverter.toDate(getDateNaissance()), Types.DATE);
            _stmt.setObject(5, getCivilite(), Types.VARCHAR);
            _stmt.setObject(6, getNom(), Types.VARCHAR);
            _stmt.setObject(7, getPrenom(), Types.VARCHAR);
            _stmt.setObject(8, getCodeRattachement(), Types.VARCHAR);
            _stmt.setObject(9, getGroupes(), Types.LONGVARCHAR);
            _stmt.setObject(10, getAdresseMail(), Types.VARCHAR);
            _stmt.setObject(11, getRestrictionValidation(), Types.VARCHAR);
            _stmt.setObject(12, getExtensionModification(), Types.VARCHAR);
            _stmt.setObject(13, getCentresInteret(), Types.LONGVARCHAR);
            _stmt.setObject(14, getProfilDsi(), Types.VARCHAR);
            _stmt.setObject(15, getGroupesDsi(), Types.LONGVARCHAR);
            _stmt.setObject(16, getCodeLdap(), Types.VARCHAR);
            _stmt.setObject(17, getGroupesDsiImport(), Types.LONGVARCHAR);
            _stmt.setObject(18, getRoles(), Types.LONGVARCHAR);
            _stmt.setObject(19, SqlDateConverter.toDate(getDateDerniereSession()), Types.DATE);
            _stmt.setObject(20, getProfilDefaut(), Types.VARCHAR);
            _stmt.setObject(21, getSourceImport(), Types.VARCHAR);
            _stmt.setObject(22, getFormatEnvoi(), Types.VARCHAR);
            _stmt.setObject(23, getModeSaisieExpert(), Types.VARCHAR);
            _stmt.setObject(24, getTsCacheGroupes(), Types.BIGINT);
            _stmt.setObject(25, getIdUtilisateur(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
