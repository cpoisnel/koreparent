package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
// TODO: Auto-generated Javadoc

/**
 * The Class SiteDB.
 * @deprecated le bean a été remplacé par {@link com.univ.objetspartages.bean.SiteExterneBean} et les méthodes de service par {@link com.univ.objetspartages.services.ServiceSiteExterne}
 */
@Deprecated
public class SiteDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet _rs = null;

    /** The id site. */
    private Long idSite = null;

    /** The code. */
    private String code = null;

    /** The libelle. */
    private String libelle = null;

    /** The url. */
    private String url = null;

    /** The reg exp accepte. */
    private String regExpAccepte = null;

    /** The niveau profondeur. */
    private Integer niveauProfondeur = null;

    /** The reg exp refuse. */
    private String regExpRefuse = null;

    /** The langue. */
    private String langue = null;

    /**
     * Instantiates a new site db.
     */
    public SiteDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "SITE (ID_SITE  ,   CODE  ,   LIBELLE  ,   URL  ,   REG_EXP_ACCEPTE  ,   NIVEAU_PROFONDEUR  ,   REG_EXP_REFUSE  ,   LANGUE  )              VALUES (?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdSite(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getLibelle(), Types.VARCHAR);
            _stmt.setObject(4, getUrl(), Types.VARCHAR);
            _stmt.setObject(5, getRegExpAccepte(), Types.LONGVARCHAR);
            _stmt.setObject(6, getNiveauProfondeur(), Types.INTEGER);
            _stmt.setObject(7, getRegExpRefuse(), Types.LONGVARCHAR);
            _stmt.setObject(8, getLangue(), Types.VARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdSite(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "SITE WHERE " + "             ID_SITE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdSite(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id site.
     *
     * @return the id site
     */
    public Long getIdSite() {
        return idSite;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets the reg exp accepte.
     *
     * @return the reg exp accepte
     */
    public String getRegExpAccepte() {
        return regExpAccepte;
    }

    /**
     * Gets the niveau profondeur.
     *
     * @return the niveau profondeur
     */
    public Integer getNiveauProfondeur() {
        return niveauProfondeur;
    }

    /**
     * Gets the reg exp refuse.
     *
     * @return the reg exp refuse
     */
    public String getRegExpRefuse() {
        return regExpRefuse;
    }

    /**
     * Gets the langue.
     *
     * @return the langue
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT " + "T1.ID_SITE ,  " + "T1.CODE ,  " + "T1.LIBELLE ,  " + "T1.URL ,  " + "T1.REG_EXP_ACCEPTE ,  " + "T1.NIVEAU_PROFONDEUR ,  " + "T1.REG_EXP_REFUSE ,  " + "T1.LANGUE " + "         FROM " + qualifier + "SITE T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_SITE," + "             CODE," + "             LIBELLE," + "             URL," + "             REG_EXP_ACCEPTE," + "             NIVEAU_PROFONDEUR," + "             REG_EXP_REFUSE," + "             LANGUE" + "         FROM " + qualifier + "SITE WHERE " + "             ID_SITE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdSite(), Types.BIGINT);
            final ResultSet rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdSite(new Long(_rs.getLong(1)));
            setCode(_rs.getString(2));
            setLibelle(_rs.getString(3));
            setUrl(_rs.getString(4));
            setRegExpAccepte(_rs.getString(5));
            setNiveauProfondeur(new Integer(_rs.getInt(6)));
            setRegExpRefuse(_rs.getString(7));
            setLangue(_rs.getString(8));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "SITE T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id site.
     *
     * @param _idSite
     *            the new id site
     */
    public void setIdSite(final Long _idSite) {
        idSite = _idSite;
    }

    /**
     * Sets the code.
     *
     * @param _code
     *            the new code
     */
    public void setCode(final String _code) {
        code = _code;
    }

    /**
     * Sets the libelle.
     *
     * @param _libelle
     *            the new libelle
     */
    public void setLibelle(final String _libelle) {
        libelle = _libelle;
    }

    /**
     * Sets the url.
     *
     * @param _url
     *            the new url
     */
    public void setUrl(final String _url) {
        url = _url;
    }

    /**
     * Sets the reg exp accepte.
     *
     * @param _regExpAccepte
     *            the new reg exp accepte
     */
    public void setRegExpAccepte(final String _regExpAccepte) {
        regExpAccepte = _regExpAccepte;
    }

    /**
     * Sets the niveau profondeur.
     *
     * @param _niveauProfondeur
     *            the new niveau profondeur
     */
    public void setNiveauProfondeur(final Integer _niveauProfondeur) {
        niveauProfondeur = _niveauProfondeur;
    }

    /**
     * Sets the reg exp refuse.
     *
     * @param _regExpRefuse
     *            the new reg exp refuse
     */
    public void setRegExpRefuse(final String _regExpRefuse) {
        regExpRefuse = _regExpRefuse;
    }

    /**
     * Sets the langue.
     *
     * @param _langue
     *            the new langue
     */
    public void setLangue(final String _langue) {
        langue = _langue;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdSite() + aSeparator + getCode() + aSeparator + getLibelle() + aSeparator + getUrl() + aSeparator + getRegExpAccepte() + aSeparator + getNiveauProfondeur() + aSeparator + getRegExpRefuse() + aSeparator + getLangue()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "SITE SET " + "ID_SITE = ?, " + "CODE = ?, " + "LIBELLE = ?, " + "URL = ?, " + "REG_EXP_ACCEPTE = ?, " + "NIVEAU_PROFONDEUR = ?, " + "REG_EXP_REFUSE = ?, " + "LANGUE = ? " + "         WHERE " + "             ID_SITE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdSite(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getLibelle(), Types.VARCHAR);
            _stmt.setObject(4, getUrl(), Types.VARCHAR);
            _stmt.setObject(5, getRegExpAccepte(), Types.LONGVARCHAR);
            _stmt.setObject(6, getNiveauProfondeur(), Types.INTEGER);
            _stmt.setObject(7, getRegExpRefuse(), Types.LONGVARCHAR);
            _stmt.setObject(8, getLangue(), Types.VARCHAR);
            _stmt.setObject(9, getIdSite(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
