package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
// TODO: Auto-generated Javadoc

/**
 * The Class GroupedsiDB.
 * @deprecated classe plus appelé dans le produit. Utiliser {@link com.univ.objetspartages.dao.impl.GroupeDsiDAO}
 */
@Deprecated
public class GroupedsiDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet _rs = null;

    /** The id groupedsi. */
    private Long idGroupedsi = null;

    /** The code. */
    private String code = null;

    /** The libelle. */
    private String libelle = null;

    /** The type. */
    private String type = null;

    /** The code structure. */
    private String codeStructure = null;

    /** The code page tete. */
    private String codePageTete = null;

    /** The roles. */
    private String roles = null;

    /** The code groupe pere. */
    private String codeGroupePere = null;

    /** The requete groupe. */
    private String requeteGroupe = null;

    /** The requete ldap. */
    private String requeteLdap = null;

    /** The source import. */
    private String sourceImport = null;

    /** The gestion cache. */
    private String gestionCache = null;

    /** The delai expiration cache. */
    private Long delaiExpirationCache = null;

    /** The derniere maj cache. */
    private Long derniereMajCache = null;

    /** The selectionnable. */
    private String selectionnable = null;

    /**
     * Instantiates a new groupedsi db.
     */
    public GroupedsiDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "GROUPEDSI (ID_GROUPEDSI  ,   CODE  ,   LIBELLE  ,   TYPE  ,   CODE_STRUCTURE  ,   CODE_PAGE_TETE  ,   ROLES  ,   CODE_GROUPE_PERE  ,   REQUETE_GROUPE  ,   REQUETE_LDAP  ,   SOURCE_IMPORT  ,   GESTION_CACHE  ,   DELAI_EXPIRATION_CACHE  ,   DERNIERE_MAJ_CACHE  ,   SELECTIONNABLE  )              VALUES (?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdGroupedsi(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getLibelle(), Types.VARCHAR);
            _stmt.setObject(4, getType(), Types.VARCHAR);
            _stmt.setObject(5, getCodeStructure(), Types.VARCHAR);
            _stmt.setObject(6, getCodePageTete(), Types.VARCHAR);
            _stmt.setObject(7, getRoles(), Types.LONGVARCHAR);
            _stmt.setObject(8, getCodeGroupePere(), Types.VARCHAR);
            _stmt.setObject(9, getRequeteGroupe(), Types.VARCHAR);
            _stmt.setObject(10, getRequeteLdap(), Types.VARCHAR);
            _stmt.setObject(11, getSourceImport(), Types.VARCHAR);
            _stmt.setObject(12, getGestionCache(), Types.VARCHAR);
            _stmt.setObject(13, getDelaiExpirationCache(), Types.BIGINT);
            _stmt.setObject(14, getDerniereMajCache(), Types.BIGINT);
            _stmt.setObject(15, getSelectionnable(), Types.VARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdGroupedsi(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "GROUPEDSI WHERE " + "             ID_GROUPEDSI = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdGroupedsi(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id groupedsi.
     *
     * @return the id groupedsi
     */
    public Long getIdGroupedsi() {
        return idGroupedsi;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Gets the code structure.
     *
     * @return the code structure
     */
    public String getCodeStructure() {
        return codeStructure;
    }

    /**
     * Gets the code page tete.
     *
     * @return the code page tete
     */
    public String getCodePageTete() {
        return codePageTete;
    }

    /**
     * Gets the roles.
     *
     * @return the roles
     */
    public String getRoles() {
        return roles;
    }

    /**
     * Gets the code groupe pere.
     *
     * @return the code groupe pere
     */
    public String getCodeGroupePere() {
        return codeGroupePere;
    }

    /**
     * Gets the requete groupe.
     *
     * @return the requete groupe
     */
    public String getRequeteGroupe() {
        return requeteGroupe;
    }

    /**
     * Gets the requete ldap.
     *
     * @return the requete ldap
     */
    public String getRequeteLdap() {
        return requeteLdap;
    }

    /**
     * Gets the source import.
     *
     * @return the source import
     */
    public String getSourceImport() {
        return sourceImport;
    }

    /**
     * Gets the gestion cache.
     *
     * @return the gestion cache
     */
    public String getGestionCache() {
        return gestionCache;
    }

    /**
     * Gets the delai expiration cache.
     *
     * @return the delai expiration cache
     */
    public Long getDelaiExpirationCache() {
        return delaiExpirationCache;
    }

    /**
     * Gets the derniere maj cache.
     *
     * @return the derniere maj cache
     */
    public Long getDerniereMajCache() {
        return derniereMajCache;
    }

    /**
     * Gets the selectionnable.
     *
     * @return the selectionnable
     */
    public String getSelectionnable() {
        return selectionnable;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT " + "T1.ID_GROUPEDSI ,  " + "T1.CODE ,  " + "T1.LIBELLE ,  " + "T1.TYPE ,  " + "T1.CODE_STRUCTURE ,  " + "T1.CODE_PAGE_TETE ,  " + "T1.ROLES ,  " + "T1.CODE_GROUPE_PERE ,  " + "T1.REQUETE_GROUPE ,  " + "T1.REQUETE_LDAP ,  " + "T1.SOURCE_IMPORT ,  " + "T1.GESTION_CACHE ,  " + "T1.DELAI_EXPIRATION_CACHE ,  " + "T1.DERNIERE_MAJ_CACHE ,  " + "T1.SELECTIONNABLE " + "         FROM " + qualifier + "GROUPEDSI T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_GROUPEDSI," + "             CODE," + "             LIBELLE," + "             TYPE," + "             CODE_STRUCTURE," + "             CODE_PAGE_TETE," + "             ROLES," + "             CODE_GROUPE_PERE," + "             REQUETE_GROUPE," + "             REQUETE_LDAP," + "             SOURCE_IMPORT," + "             GESTION_CACHE," + "             DELAI_EXPIRATION_CACHE," + "             DERNIERE_MAJ_CACHE," + "             SELECTIONNABLE" + "         FROM " + qualifier + "GROUPEDSI WHERE " + "             ID_GROUPEDSI = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdGroupedsi(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdGroupedsi(new Long(_rs.getLong(1)));
            setCode(_rs.getString(2));
            setLibelle(_rs.getString(3));
            setType(_rs.getString(4));
            setCodeStructure(_rs.getString(5));
            setCodePageTete(_rs.getString(6));
            setRoles(_rs.getString(7));
            setCodeGroupePere(_rs.getString(8));
            setRequeteGroupe(_rs.getString(9));
            setRequeteLdap(_rs.getString(10));
            setSourceImport(_rs.getString(11));
            setGestionCache(_rs.getString(12));
            setDelaiExpirationCache(new Long(_rs.getLong(13)));
            setDerniereMajCache(new Long(_rs.getLong(14)));
            setSelectionnable(_rs.getString(15));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "GROUPEDSI T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id groupedsi.
     *
     * @param _idGroupedsi
     *            the new id groupedsi
     */
    public void setIdGroupedsi(final Long _idGroupedsi) {
        idGroupedsi = _idGroupedsi;
    }

    /**
     * Sets the code.
     *
     * @param _code
     *            the new code
     */
    public void setCode(final String _code) {
        code = _code;
    }

    /**
     * Sets the libelle.
     *
     * @param _libelle
     *            the new libelle
     */
    public void setLibelle(final String _libelle) {
        libelle = _libelle;
    }

    /**
     * Sets the type.
     *
     * @param _type
     *            the new type
     */
    public void setType(final String _type) {
        type = _type;
    }

    /**
     * Sets the code structure.
     *
     * @param _codeStructure
     *            the new code structure
     */
    public void setCodeStructure(final String _codeStructure) {
        codeStructure = _codeStructure;
    }

    /**
     * Sets the code page tete.
     *
     * @param _codePageTete
     *            the new code page tete
     */
    public void setCodePageTete(final String _codePageTete) {
        codePageTete = _codePageTete;
    }

    /**
     * Sets the roles.
     *
     * @param _roles
     *            the new roles
     */
    public void setRoles(final String _roles) {
        roles = _roles;
    }

    /**
     * Sets the code groupe pere.
     *
     * @param _codeGroupePere
     *            the new code groupe pere
     */
    public void setCodeGroupePere(final String _codeGroupePere) {
        codeGroupePere = _codeGroupePere;
    }

    /**
     * Sets the requete groupe.
     *
     * @param _requeteGroupe
     *            the new requete groupe
     */
    public void setRequeteGroupe(final String _requeteGroupe) {
        requeteGroupe = _requeteGroupe;
    }

    /**
     * Sets the requete ldap.
     *
     * @param _requeteLdap
     *            the new requete ldap
     */
    public void setRequeteLdap(final String _requeteLdap) {
        requeteLdap = _requeteLdap;
    }

    /**
     * Sets the source import.
     *
     * @param _sourceImport
     *            the new source import
     */
    public void setSourceImport(final String _sourceImport) {
        sourceImport = _sourceImport;
    }

    /**
     * Sets the gestion cache.
     *
     * @param _gestionCache
     *            the new gestion cache
     */
    public void setGestionCache(final String _gestionCache) {
        gestionCache = _gestionCache;
    }

    /**
     * Sets the delai expiration cache.
     *
     * @param _delaiExpirationCache
     *            the new delai expiration cache
     */
    public void setDelaiExpirationCache(final Long _delaiExpirationCache) {
        delaiExpirationCache = _delaiExpirationCache;
    }

    /**
     * Sets the derniere maj cache.
     *
     * @param _derniereMajCache
     *            the new derniere maj cache
     */
    public void setDerniereMajCache(final Long _derniereMajCache) {
        derniereMajCache = _derniereMajCache;
    }

    /**
     * Sets the selectionnable.
     *
     * @param _selectionnable
     *            the new selectionnable
     */
    public void setSelectionnable(final String _selectionnable) {
        selectionnable = _selectionnable;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdGroupedsi() + aSeparator + getCode() + aSeparator + getLibelle() + aSeparator + getType() + aSeparator + getCodeStructure() + aSeparator + getCodePageTete() + aSeparator + getRoles() + aSeparator + getCodeGroupePere() + aSeparator + getRequeteGroupe() + aSeparator + getRequeteLdap() + aSeparator + getSourceImport() + aSeparator + getGestionCache() + aSeparator + getDelaiExpirationCache() + aSeparator + getDerniereMajCache() + aSeparator + getSelectionnable()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "GROUPEDSI SET " + "ID_GROUPEDSI = ?, " + "CODE = ?, " + "LIBELLE = ?, " + "TYPE = ?, " + "CODE_STRUCTURE = ?, " + "CODE_PAGE_TETE = ?, " + "ROLES = ?, " + "CODE_GROUPE_PERE = ?, " + "REQUETE_GROUPE = ?, " + "REQUETE_LDAP = ?, " + "SOURCE_IMPORT = ?, " + "GESTION_CACHE = ?, " + "DELAI_EXPIRATION_CACHE = ?, " + "DERNIERE_MAJ_CACHE = ?, " + "SELECTIONNABLE = ? " + "         WHERE " + "             ID_GROUPEDSI = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdGroupedsi(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getLibelle(), Types.VARCHAR);
            _stmt.setObject(4, getType(), Types.VARCHAR);
            _stmt.setObject(5, getCodeStructure(), Types.VARCHAR);
            _stmt.setObject(6, getCodePageTete(), Types.VARCHAR);
            _stmt.setObject(7, getRoles(), Types.LONGVARCHAR);
            _stmt.setObject(8, getCodeGroupePere(), Types.VARCHAR);
            _stmt.setObject(9, getRequeteGroupe(), Types.VARCHAR);
            _stmt.setObject(10, getRequeteLdap(), Types.VARCHAR);
            _stmt.setObject(11, getSourceImport(), Types.VARCHAR);
            _stmt.setObject(12, getGestionCache(), Types.VARCHAR);
            _stmt.setObject(13, getDelaiExpirationCache(), Types.BIGINT);
            _stmt.setObject(14, getDerniereMajCache(), Types.BIGINT);
            _stmt.setObject(15, getSelectionnable(), Types.VARCHAR);
            _stmt.setObject(16, getIdGroupedsi(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
