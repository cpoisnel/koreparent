package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
// TODO: Auto-generated Javadoc

/**
 * The Class LibelleDB.
 * @deprecated utilisez {@link com.univ.objetspartages.dao.impl.LabelDAO} et {@link com.univ.objetspartages.bean.LabelBean}
 */
@Deprecated
public class LibelleDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet _rs = null;

    /** The id libelle. */
    private Long idLibelle = null;

    /** The type. */
    private String type = null;

    /** The code. */
    private String code = null;

    /** The libelle. */
    private String libelle = null;

    /** The langue. */
    private String langue = null;

    /**
     * Instantiates a new libelle db.
     */
    public LibelleDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "LIBELLE (ID_LIBELLE  ,   TYPE  ,   CODE  ,   LIBELLE  ,   LANGUE  )              VALUES (?  ,?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdLibelle(), Types.BIGINT);
            _stmt.setObject(2, getType(), Types.VARCHAR);
            _stmt.setObject(3, getCode(), Types.VARCHAR);
            _stmt.setObject(4, getLibelle(), Types.VARCHAR);
            _stmt.setObject(5, getLangue(), Types.VARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdLibelle(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "LIBELLE WHERE " + "             ID_LIBELLE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdLibelle(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id libelle.
     *
     * @return the id libelle
     */
    public Long getIdLibelle() {
        return idLibelle;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Gets the langue.
     *
     * @return the langue
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT " + "T1.ID_LIBELLE ,  " + "T1.TYPE ,  " + "T1.CODE ,  " + "T1.LIBELLE ,  " + "T1.LANGUE " + "         FROM " + qualifier + "LIBELLE T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_LIBELLE," + "             TYPE," + "             CODE," + "             LIBELLE," + "             LANGUE" + "         FROM " + qualifier + "LIBELLE WHERE " + "             ID_LIBELLE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdLibelle(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdLibelle(new Long(_rs.getLong(1)));
            setType(_rs.getString(2));
            setCode(_rs.getString(3));
            setLibelle(_rs.getString(4));
            setLangue(_rs.getString(5));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "LIBELLE T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Cette methode optimise l'execution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(final String sqlSuffix) throws Exception {
        PreparedStatement stmt = null;
        try {
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            _rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
    }

    /**
     * Sets the id libelle.
     *
     * @param _idLibelle
     *            the new id libelle
     */
    public void setIdLibelle(final Long _idLibelle) {
        idLibelle = _idLibelle;
    }

    /**
     * Sets the type.
     *
     * @param _type
     *            the new type
     */
    public void setType(final String _type) {
        type = _type;
    }

    /**
     * Sets the code.
     *
     * @param _code
     *            the new code
     */
    public void setCode(final String _code) {
        code = _code;
    }

    /**
     * Sets the libelle.
     *
     * @param _libelle
     *            the new libelle
     */
    public void setLibelle(final String _libelle) {
        libelle = _libelle;
    }

    /**
     * Sets the langue.
     *
     * @param _langue
     *            the new langue
     */
    public void setLangue(final String _langue) {
        langue = _langue;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdLibelle() + aSeparator + getType() + aSeparator + getCode() + aSeparator + getLibelle() + aSeparator + getLangue()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "LIBELLE SET " + "ID_LIBELLE = ?, " + "TYPE = ?, " + "CODE = ?, " + "LIBELLE = ?, " + "LANGUE = ? " + "         WHERE " + "             ID_LIBELLE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdLibelle(), Types.BIGINT);
            _stmt.setObject(2, getType(), Types.VARCHAR);
            _stmt.setObject(3, getCode(), Types.VARCHAR);
            _stmt.setObject(4, getLibelle(), Types.VARCHAR);
            _stmt.setObject(5, getLangue(), Types.VARCHAR);
            _stmt.setObject(6, getIdLibelle(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
