package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
// TODO: Auto-generated Javadoc

/**
 * The Class RoleDB.
 * @deprecated classe à ne plus utiliser, les données sont dans le bean {@link com.univ.objetspartages.bean.RoleBean} les méthodes du dao sont sur {@link com.univ.objetspartages.dao.impl.RoleDAO}
 */
@Deprecated
public class RoleDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet _rs = null;

    /** The id role. */
    private Long idRole = null;

    /** The code. */
    private String code = null;

    /** The libelle. */
    private String libelle = null;

    /** The perimetre. */
    private String perimetre = null;

    /** The permissions. */
    private String permissions = null;

    /**
     * Instantiates a new role db.
     */
    public RoleDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "ROLE (ID_ROLE  ,   CODE  ,   LIBELLE  ,   PERIMETRE  ,   PERMISSIONS  )              VALUES (?  ,?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdRole(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getLibelle(), Types.VARCHAR);
            _stmt.setObject(4, getPerimetre(), Types.VARCHAR);
            _stmt.setObject(5, getPermissions(), Types.LONGVARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdRole(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "ROLE WHERE " + "             ID_ROLE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdRole(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id role.
     *
     * @return the id role
     */
    public Long getIdRole() {
        return idRole;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Gets the perimetre.
     *
     * @return the perimetre
     */
    public String getPerimetre() {
        return perimetre;
    }

    /**
     * Gets the permissions.
     *
     * @return the permissions
     */
    public String getPermissions() {
        return permissions;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT " + "T1.ID_ROLE ,  " + "T1.CODE ,  " + "T1.LIBELLE ,  " + "T1.PERIMETRE ,  " + "T1.PERMISSIONS " + "         FROM " + qualifier + "ROLE T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_ROLE," + "             CODE," + "             LIBELLE," + "             PERIMETRE," + "             PERMISSIONS" + "         FROM " + qualifier + "ROLE WHERE " + "             ID_ROLE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdRole(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdRole(new Long(_rs.getLong(1)));
            setCode(_rs.getString(2));
            setLibelle(_rs.getString(3));
            setPerimetre(_rs.getString(4));
            setPermissions(_rs.getString(5));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "ROLE T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id role.
     *
     * @param _idRole
     *            the new id role
     */
    public void setIdRole(final Long _idRole) {
        idRole = _idRole;
    }

    /**
     * Sets the code.
     *
     * @param _code
     *            the new code
     */
    public void setCode(final String _code) {
        code = _code;
    }

    /**
     * Sets the libelle.
     *
     * @param _libelle
     *            the new libelle
     */
    public void setLibelle(final String _libelle) {
        libelle = _libelle;
    }

    /**
     * Sets the perimetre.
     *
     * @param _perimetre
     *            the new perimetre
     */
    public void setPerimetre(final String _perimetre) {
        perimetre = _perimetre;
    }

    /**
     * Sets the permissions.
     *
     * @param _permissions
     *            the new permissions
     */
    public void setPermissions(final String _permissions) {
        permissions = _permissions;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdRole() + aSeparator + getCode() + aSeparator + getLibelle() + aSeparator + getPerimetre() + aSeparator + getPermissions()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "ROLE SET " + "ID_ROLE = ?, " + "CODE = ?, " + "LIBELLE = ?, " + "PERIMETRE = ?, " + "PERMISSIONS = ? " + "         WHERE " + "             ID_ROLE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdRole(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getLibelle(), Types.VARCHAR);
            _stmt.setObject(4, getPerimetre(), Types.VARCHAR);
            _stmt.setObject(5, getPermissions(), Types.LONGVARCHAR);
            _stmt.setObject(6, getIdRole(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
