package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.bean.RubriqueBean;

/**
 * Classe d'acces aux donnees pour rubrique.
 * @deprecated Utiliser Le dao de rubrique plutôt que cette classe.
 */
@Deprecated
public class RubriqueDB extends RubriqueBean {

    /**
     *
     */
    private static final long serialVersionUID = -8476789393175049315L;

    /** The ctx. */
    protected transient OMContext ctx = null;

    /** The rs. */
    private transient ResultSet rs = null;

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return new StringBuilder().append("").append(getIdRubrique()).append(aSeparator).append(getCode()).append(aSeparator).append(getLangue()).append(aSeparator).append(getIntitule()).append(aSeparator).append(getNomOnglet()).append(aSeparator).append(getAccroche()).append(aSeparator).append(getIdBandeau()).append(aSeparator).append(getCouleurFond()).append(aSeparator).append(getCouleurTitre()).append(aSeparator).append(getCodeRubriqueMere()).append(aSeparator).append(getTypeRubrique()).append(aSeparator).append(getPageAccueil()).append(aSeparator).append(getGestionEncadre()).append(aSeparator).append(getEncadre()).append(aSeparator).append(getEncadreSousRubrique()).append(aSeparator).append(getOrdre()).append(aSeparator).append(getContact()).append(aSeparator).append(getGroupesDsi()).append(aSeparator).append(getRequetesRubriquePublication()).append(aSeparator).append(getCategorie()).toString();
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        ctx = _ctx;
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = getConnection().prepareStatement("insert into RUBRIQUE (ID_RUBRIQUE , CODE , LANGUE , INTITULE , NOM_ONGLET , ACCROCHE , ID_BANDEAU , COULEUR_FOND , COULEUR_TITRE , CODE_RUBRIQUE_MERE , TYPE_RUBRIQUE , PAGE_ACCUEIL , GESTION_ENCADRE , ENCADRE , ENCADRE_SOUS_RUBRIQUE , ORDRE , CONTACT , GROUPES_DSI , REQUETES_RUBRIQUE_PUBLICATION , CATEGORIE, ID_PICTO) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setObject(1, getIdRubrique(), Types.BIGINT);
            stmt.setObject(2, getCode(), Types.VARCHAR);
            stmt.setObject(3, getLangue(), Types.VARCHAR);
            stmt.setObject(4, getIntitule(), Types.VARCHAR);
            stmt.setObject(5, getNomOnglet(), Types.VARCHAR);
            stmt.setObject(6, getAccroche(), Types.LONGVARCHAR);
            stmt.setObject(7, getIdBandeau(), Types.BIGINT);
            stmt.setObject(8, getCouleurFond(), Types.VARCHAR);
            stmt.setObject(9, getCouleurTitre(), Types.VARCHAR);
            stmt.setObject(10, getCodeRubriqueMere(), Types.VARCHAR);
            stmt.setObject(11, getTypeRubrique(), Types.VARCHAR);
            stmt.setObject(12, getPageAccueil(), Types.LONGVARCHAR);
            stmt.setObject(13, getGestionEncadre(), Types.VARCHAR);
            stmt.setObject(14, getEncadre(), Types.LONGVARCHAR);
            stmt.setObject(15, getEncadreSousRubrique(), Types.VARCHAR);
            stmt.setObject(16, getOrdre(), Types.VARCHAR);
            stmt.setObject(17, getContact(), Types.VARCHAR);
            stmt.setObject(18, getGroupesDsi(), Types.LONGVARCHAR);
            stmt.setObject(19, getRequetesRubriquePublication(), Types.LONGVARCHAR);
            stmt.setObject(20, getCategorie(), Types.VARCHAR);
            stmt.setObject(21, getIdPicto(), Types.BIGINT);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = stmt.getGeneratedKeys();
            rs.next();
            setIdRubrique(rs.getLong(1));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD add()", e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("delete from RUBRIQUE where ID_RUBRIQUE = ?");
            // put parameters into statement
            stmt.setObject(1, getIdRubrique(), Types.BIGINT);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD delete()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "select distinct T1.ID_RUBRIQUE, T1.CODE, T1.LANGUE, T1.INTITULE, T1.NOM_ONGLET, T1.ACCROCHE, T1.ID_BANDEAU, T1.COULEUR_FOND, T1.COULEUR_TITRE, T1.CODE_RUBRIQUE_MERE, T1.TYPE_RUBRIQUE, T1.PAGE_ACCUEIL, T1.GESTION_ENCADRE, T1.ENCADRE, T1.ENCADRE_SOUS_RUBRIQUE, T1.ORDRE, T1.CONTACT, T1.GROUPES_DSI, T1.REQUETES_RUBRIQUE_PUBLICATION, T1.CATEGORIE, T1.ID_PICTO from RUBRIQUE T1 ";
    }

    /**
     * Renvoie l'element suivant du ResultSet.
     *
     * @return true, if next item
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                retrieveFromRS();
                res = true;
            } else {
                try {
                    rs.close();
                } finally {
                    rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem()", e);
        }
        return res;
    }

    /**
     * Recuperation d'une ligne de la base de donnees.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("select T1.ID_RUBRIQUE, T1.CODE, T1.LANGUE, T1.INTITULE, T1.NOM_ONGLET, T1.ACCROCHE, T1.ID_BANDEAU, T1.COULEUR_FOND, T1.COULEUR_TITRE, T1.CODE_RUBRIQUE_MERE, T1.TYPE_RUBRIQUE, T1.PAGE_ACCUEIL, T1.GESTION_ENCADRE, T1.ENCADRE, T1.ENCADRE_SOUS_RUBRIQUE, T1.ORDRE, T1.CONTACT, T1.GROUPES_DSI, T1.REQUETES_RUBRIQUE_PUBLICATION, T1.CATEGORIE, T1.ID_PICTO from RUBRIQUE T1 where T1.ID_RUBRIQUE = ?");
            stmt.setObject(1, getIdRubrique(), Types.BIGINT);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve : METHOD_NO_RESULTS");
            }
            retrieveFromRS();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieve()", e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS() throws Exception {
        try {
            // get output from result set
            setIdRubrique(rs.getLong(1));
            setCode(rs.getString(2));
            setLangue(rs.getString(3));
            setIntitule(rs.getString(4));
            setNomOnglet(rs.getString(5));
            setAccroche(rs.getString(6));
            setIdBandeau(rs.getLong(7));
            setCouleurFond(rs.getString(8));
            setCouleurTitre(rs.getString(9));
            setCodeRubriqueMere(rs.getString(10));
            setTypeRubrique(rs.getString(11));
            setPageAccueil(rs.getString(12));
            setGestionEncadre(rs.getString(13));
            setEncadre(rs.getString(14));
            setEncadreSousRubrique(rs.getString(15));
            setOrdre(rs.getString(16));
            setContact(rs.getString(17));
            setGroupesDsi(rs.getString(18));
            setRequetesRubriquePublication(rs.getString(19));
            setCategorie(rs.getString(20));
            setIdPicto(rs.getLong(21));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS()", e);
        }
    }

    /**
     * Cette methode optimise l'execution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(final String sqlSuffix) throws Exception {
        PreparedStatement stmt;
        try {
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String sqlSuffix) throws Exception {
        int count = 0;
        PreparedStatement stmt;
        try {
            // recuperation nombre de lignes
            String query = "select count(distinct T1.ID_RUBRIQUE) from RUBRIQUE T1 ";
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
            rs.next();
            count = rs.getInt(1);
            // execution requete
            query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
        return count;
    }

    /**
     * Select max order according to the provided parent's code.
     *
     * @param codeRubriqueMere
     *            : Rubrique's parent
     *
     * @return the maximum order found in DB
     *
     * @throws Exception
     *             the exception
     */
    public int selectMaxOrder(final String codeRubriqueMere) throws Exception {
        int ordre = 0;
        PreparedStatement stmt = null;
        try {
            // recuperation nombre de lignes
            final String query = "select max(T1.ORDRE) from RUBRIQUE T1 WHERE T1.CODE_RUBRIQUE_MERE = ?";
            stmt = getConnection().prepareStatement(query);
            stmt.setString(1, codeRubriqueMere);
            rs = stmt.executeQuery();
            rs.next();
            ordre = rs.getInt(1);
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
        return ordre;
    }

    /**
     * Mise a jour d'une ligne de la base de donnees.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("update RUBRIQUE set ID_RUBRIQUE = ?, CODE = ?, LANGUE = ?, INTITULE = ?, NOM_ONGLET = ?, ACCROCHE = ?, ID_BANDEAU = ?, COULEUR_FOND = ?, COULEUR_TITRE = ?, CODE_RUBRIQUE_MERE = ?, TYPE_RUBRIQUE = ?, PAGE_ACCUEIL = ?, GESTION_ENCADRE = ?, ENCADRE = ?, ENCADRE_SOUS_RUBRIQUE = ?, ORDRE = ?, CONTACT = ?, GROUPES_DSI = ?, REQUETES_RUBRIQUE_PUBLICATION = ?, CATEGORIE = ?, ID_PICTO = ? where ID_RUBRIQUE = ?");
            // put parameters into statement
            stmt.setObject(1, getIdRubrique(), Types.BIGINT);
            stmt.setObject(2, getCode(), Types.VARCHAR);
            stmt.setObject(3, getLangue(), Types.VARCHAR);
            stmt.setObject(4, getIntitule(), Types.VARCHAR);
            stmt.setObject(5, getNomOnglet(), Types.VARCHAR);
            stmt.setObject(6, getAccroche(), Types.LONGVARCHAR);
            stmt.setObject(7, getIdBandeau(), Types.BIGINT);
            stmt.setObject(8, getCouleurFond(), Types.VARCHAR);
            stmt.setObject(9, getCouleurTitre(), Types.VARCHAR);
            stmt.setObject(10, getCodeRubriqueMere(), Types.VARCHAR);
            stmt.setObject(11, getTypeRubrique(), Types.VARCHAR);
            stmt.setObject(12, getPageAccueil(), Types.LONGVARCHAR);
            stmt.setObject(13, getGestionEncadre(), Types.VARCHAR);
            stmt.setObject(14, getEncadre(), Types.LONGVARCHAR);
            stmt.setObject(15, getEncadreSousRubrique(), Types.VARCHAR);
            stmt.setObject(16, getOrdre(), Types.VARCHAR);
            stmt.setObject(17, getContact(), Types.VARCHAR);
            stmt.setObject(18, getGroupesDsi(), Types.LONGVARCHAR);
            stmt.setObject(19, getRequetesRubriquePublication(), Types.LONGVARCHAR);
            stmt.setObject(20, getCategorie(), Types.VARCHAR);
            stmt.setObject(21, getIdPicto(), Types.BIGINT);
            stmt.setObject(22, getIdRubrique(), Types.BIGINT);
            stmt.executeUpdate();
            stmt.close();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD update()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
}
