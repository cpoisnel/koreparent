package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.bean.EncadreBean;
// TODO: Auto-generated Javadoc

/**
 * The Class EncadreDB.
 */
@Deprecated
public class EncadreDB extends EncadreBean {

    private static final long serialVersionUID = 8066337613549682309L;

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected transient OMContext ctx = null;

    /** The _stmt. */
    private transient java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private transient ResultSet _rs = null;

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "ENCADRE (ID_ENCADRE  ,   INTITULE  ,   ACTIF  ,   LANGUE  ,   POIDS  ,   CONTENU  ,   OBJETS  ,   CODE  ,   CODE_RATTACHEMENT  ,   CODE_RUBRIQUE  )              VALUES (?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getId(), Types.BIGINT);
            _stmt.setObject(2, getIntitule(), Types.VARCHAR);
            _stmt.setObject(3, getActif(), Types.VARCHAR);
            _stmt.setObject(4, getLangue(), Types.VARCHAR);
            _stmt.setObject(5, getPoids(), Types.INTEGER);
            _stmt.setObject(6, getContenu(), Types.LONGVARCHAR);
            _stmt.setObject(7, getObjets(), Types.LONGVARCHAR);
            _stmt.setObject(8, getCode(), Types.VARCHAR);
            _stmt.setObject(9, getCodeRattachement(), Types.VARCHAR);
            _stmt.setObject(10, getCodeRubrique(), Types.VARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setId(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "ENCADRE WHERE              ID_ENCADRE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getId(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT T1.ID_ENCADRE ,  T1.INTITULE ,  T1.ACTIF ,  T1.LANGUE ,  T1.POIDS ,  T1.CONTENU ,  T1.OBJETS ,  T1.CODE ,  T1.CODE_RATTACHEMENT ,  T1.CODE_RUBRIQUE          FROM " + qualifier + "ENCADRE T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT              ID_ENCADRE,             INTITULE,             ACTIF,             LANGUE,             POIDS,             CONTENU,             OBJETS,             CODE,             CODE_RATTACHEMENT,             CODE_RUBRIQUE         FROM " + qualifier + "ENCADRE WHERE              ID_ENCADRE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getId(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setId(new Long(_rs.getLong(1)));
            setIntitule(_rs.getString(2));
            setActif(_rs.getString(3));
            setLangue(_rs.getString(4));
            setPoids(new Integer(_rs.getInt(5)));
            setContenu(_rs.getString(6));
            setObjets(_rs.getString(7));
            setCode(_rs.getString(8));
            setCodeRattachement(_rs.getString(9));
            setCodeRubrique(_rs.getString(10));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            /* éxécution requete */
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "ENCADRE T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getId() + aSeparator + getIntitule() + aSeparator + getActif() + aSeparator + getLangue() + aSeparator + getPoids() + aSeparator + getContenu() + aSeparator + getObjets() + aSeparator + getCode() + aSeparator + getCodeRattachement() + aSeparator + getCodeRubrique()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "ENCADRE SET ID_ENCADRE = ?, INTITULE = ?, ACTIF = ?, LANGUE = ?, POIDS = ?, CONTENU = ?, OBJETS = ?, CODE = ?, CODE_RATTACHEMENT = ?, CODE_RUBRIQUE = ?          WHERE              ID_ENCADRE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getId(), Types.BIGINT);
            _stmt.setObject(2, getIntitule(), Types.VARCHAR);
            _stmt.setObject(3, getActif(), Types.VARCHAR);
            _stmt.setObject(4, getLangue(), Types.VARCHAR);
            _stmt.setObject(5, getPoids(), Types.INTEGER);
            _stmt.setObject(6, getContenu(), Types.LONGVARCHAR);
            _stmt.setObject(7, getObjets(), Types.LONGVARCHAR);
            _stmt.setObject(8, getCode(), Types.VARCHAR);
            _stmt.setObject(9, getCodeRattachement(), Types.VARCHAR);
            _stmt.setObject(10, getCodeRubrique(), Types.VARCHAR);
            _stmt.setObject(11, getId(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
