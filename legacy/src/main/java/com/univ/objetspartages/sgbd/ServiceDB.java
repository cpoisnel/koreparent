package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;

/**
 * The Class ServiceDB.
 * @deprecated cette classe n'est plus utiliser, voir {@link com.univ.objetspartages.dao.impl.ServiceDAO} et {@link com.univ.objetspartages.om.ServiceBean} pour les nouvelles implémentations
 */
@Deprecated
public class ServiceDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet _rs = null;

    /** The id service. */
    private Long idService = null;

    /** The code. */
    private String code = null;

    /** The intitule. */
    private String intitule = null;

    /** The proxy cas. */
    private String proxyCas = null;

    /** The jeton kportal. */
    private String jetonKportal = null;

    /** The url. */
    private String url = null;

    /** The url popup. */
    private String urlPopup = null;

    /** The expiration cache. */
    private Integer expirationCache = null;

    /** The vue reduite url. */
    private String vueReduiteUrl = null;

    /** The diffusion mode. */
    private String diffusionMode = null;

    /** The diffusion public vise. */
    private String diffusionPublicVise = null;

    /** The diffusion mode restriction. */
    private String diffusionModeRestriction = null;

    /** The diffusion public vise restriction. */
    private String diffusionPublicViseRestriction = null;

    /**
     * Instantiates a new service db.
     */
    public ServiceDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "SERVICE (ID_SERVICE, CODE, INTITULE, PROXY_CAS, JETON_KPORTAL, URL, URL_POPUP, EXPIRATION_CACHE, VUE_REDUITE_URL, DIFFUSION_MODE, DIFFUSION_PUBLIC_VISE, DIFFUSION_MODE_RESTRICTION, DIFFUSION_PUBLIC_VISE_RESTRICTION) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdService(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getIntitule(), Types.VARCHAR);
            _stmt.setObject(4, getProxyCas(), Types.VARCHAR);
            _stmt.setObject(5, getJetonKportal(), Types.VARCHAR);
            _stmt.setObject(6, getUrl(), Types.VARCHAR);
            _stmt.setObject(7, getUrlPopup(), Types.VARCHAR);
            _stmt.setObject(8, getExpirationCache(), Types.INTEGER);
            _stmt.setObject(9, getVueReduiteUrl(), Types.VARCHAR);
            _stmt.setObject(10, getDiffusionMode(), Types.VARCHAR);
            _stmt.setObject(11, getDiffusionPublicVise(), Types.LONGVARCHAR);
            _stmt.setObject(12, getDiffusionModeRestriction(), Types.VARCHAR);
            _stmt.setObject(13, getDiffusionPublicViseRestriction(), Types.LONGVARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdService(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "SERVICE WHERE ID_SERVICE = ?");
            // put parameters into statement
            _stmt.setObject(1, getIdService(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id service.
     *
     * @return the id service
     */
    public Long getIdService() {
        return idService;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the intitule.
     *
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Gets the proxy cas.
     *
     * @return the proxy cas
     */
    public String getProxyCas() {
        return proxyCas;
    }

    /**
     * Gets the jeton kportal.
     *
     * @return the jeton kportal
     */
    public String getJetonKportal() {
        return jetonKportal;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets the url popup.
     *
     * @return the url popup
     */
    public String getUrlPopup() {
        return urlPopup;
    }

    /**
     * Gets the expiration cache.
     *
     * @return the expiration cache
     */
    public Integer getExpirationCache() {
        return expirationCache;
    }

    /**
     * Gets the vue reduite url.
     *
     * @return the vue reduite url
     */
    public String getVueReduiteUrl() {
        return vueReduiteUrl;
    }

    /**
     * Gets the diffusion mode.
     *
     * @return the diffusion mode
     */
    public String getDiffusionMode() {
        return diffusionMode;
    }

    /**
     * Gets the diffusion public vise.
     *
     * @return the diffusion public vise
     */
    public String getDiffusionPublicVise() {
        return diffusionPublicVise;
    }

    /**
     * Gets the diffusion mode restriction.
     *
     * @return the diffusion mode restriction
     */
    public String getDiffusionModeRestriction() {
        return diffusionModeRestriction;
    }

    /**
     * Gets the diffusion public vise restriction.
     *
     * @return the diffusion public vise restriction
     */
    public String getDiffusionPublicViseRestriction() {
        return diffusionPublicViseRestriction;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT T1.ID_SERVICE, T1.CODE, T1.INTITULE, T1.PROXY_CAS, T1.JETON_KPORTAL, T1.URL, T1.URL_POPUP, T1.EXPIRATION_CACHE, T1.VUE_REDUITE_URL, T1.DIFFUSION_MODE, T1.DIFFUSION_PUBLIC_VISE, T1.DIFFUSION_MODE_RESTRICTION, T1.DIFFUSION_PUBLIC_VISE_RESTRICTION FROM " + qualifier + "SERVICE T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT ID_SERVICE, CODE, INTITULE, PROXY_CAS, JETON_KPORTAL, URL, URL_POPUP, EXPIRATION_CACHE, VUE_REDUITE_URL, DIFFUSION_MODE, DIFFUSION_PUBLIC_VISE, DIFFUSION_MODE_RESTRICTION, DIFFUSION_PUBLIC_VISE_RESTRICTION FROM " + qualifier + "SERVICE WHERE ID_SERVICE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdService(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdService(_rs.getLong(1));
            setCode(_rs.getString(2));
            setIntitule(_rs.getString(3));
            setProxyCas(_rs.getString(4));
            setJetonKportal(_rs.getString(5));
            setUrl(_rs.getString(6));
            setUrlPopup(_rs.getString(7));
            setExpirationCache(_rs.getInt(8));
            setVueReduiteUrl(_rs.getString(9));
            setDiffusionMode(_rs.getString(10));
            setDiffusionPublicVise(_rs.getString(11));
            setDiffusionModeRestriction(_rs.getString(12));
            setDiffusionPublicViseRestriction(_rs.getString(13));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            /* éxécution requete */
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*) FROM " + qualifier + "SERVICE T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id service.
     *
     * @param _idService
     *            the new id service
     */
    public void setIdService(final Long _idService) {
        idService = _idService;
    }

    /**
     * Sets the code.
     *
     * @param _code
     *            the new code
     */
    public void setCode(final String _code) {
        code = _code;
    }

    /**
     * Sets the intitule.
     *
     * @param _intitule
     *            the new intitule
     */
    public void setIntitule(final String _intitule) {
        intitule = _intitule;
    }

    /**
     * Sets the proxy cas.
     *
     * @param _proxyCas
     *            the new proxy cas
     */
    public void setProxyCas(final String _proxyCas) {
        proxyCas = _proxyCas;
    }

    /**
     * Sets the jeton kportal.
     *
     * @param _jetonKportal
     *            the new jeton kportal
     */
    public void setJetonKportal(final String _jetonKportal) {
        jetonKportal = _jetonKportal;
    }

    /**
     * Sets the url.
     *
     * @param _url
     *            the new url
     */
    public void setUrl(final String _url) {
        url = _url;
    }

    /**
     * Sets the url popup.
     *
     * @param _urlPopup
     *            the new url popup
     */
    public void setUrlPopup(final String _urlPopup) {
        urlPopup = _urlPopup;
    }

    /**
     * Sets the expiration cache.
     *
     * @param _expirationCache
     *            the new expiration cache
     */
    public void setExpirationCache(final Integer _expirationCache) {
        expirationCache = _expirationCache;
    }

    /**
     * Sets the vue reduite url.
     *
     * @param _vueReduiteUrl
     *            the new vue reduite url
     */
    public void setVueReduiteUrl(final String _vueReduiteUrl) {
        vueReduiteUrl = _vueReduiteUrl;
    }

    /**
     * Sets the diffusion mode.
     *
     * @param _diffusionMode
     *            the new diffusion mode
     */
    public void setDiffusionMode(final String _diffusionMode) {
        diffusionMode = _diffusionMode;
    }

    /**
     * Sets the diffusion public vise.
     *
     * @param _diffusionPublicVise
     *            the new diffusion public vise
     */
    public void setDiffusionPublicVise(final String _diffusionPublicVise) {
        diffusionPublicVise = _diffusionPublicVise;
    }

    /**
     * Sets the diffusion mode restriction.
     *
     * @param _diffusionModeRestriction
     *            the new diffusion mode restriction
     */
    public void setDiffusionModeRestriction(final String _diffusionModeRestriction) {
        diffusionModeRestriction = _diffusionModeRestriction;
    }

    /**
     * Sets the diffusion public vise restriction.
     *
     * @param _diffusionPublicViseRestriction
     *            the new diffusion public vise restriction
     */
    public void setDiffusionPublicViseRestriction(final String _diffusionPublicViseRestriction) {
        diffusionPublicViseRestriction = _diffusionPublicViseRestriction;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (!"".equals(_qualifier)) {
            if (!".".equals(_qualifier.substring(_qualifier.length() - 1))) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdService() + aSeparator + getCode() + aSeparator + getIntitule() + aSeparator + getProxyCas() + aSeparator + getJetonKportal() + aSeparator + getUrl() + aSeparator + getUrlPopup() + aSeparator + getExpirationCache() + aSeparator + getVueReduiteUrl() + aSeparator + aSeparator + getDiffusionMode() + aSeparator + getDiffusionPublicVise() + aSeparator + getDiffusionModeRestriction() + aSeparator + getDiffusionPublicViseRestriction()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "SERVICE SET ID_SERVICE = ?, CODE = ?, INTITULE = ?, PROXY_CAS = ?, JETON_KPORTAL = ?, URL = ?, URL_POPUP = ?, EXPIRATION_CACHE = ?, VUE_REDUITE_URL = ?, DIFFUSION_MODE = ?, DIFFUSION_PUBLIC_VISE = ?, DIFFUSION_MODE_RESTRICTION = ?, DIFFUSION_PUBLIC_VISE_RESTRICTION = ? WHERE ID_SERVICE = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdService(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getIntitule(), Types.VARCHAR);
            _stmt.setObject(4, getProxyCas(), Types.VARCHAR);
            _stmt.setObject(5, getJetonKportal(), Types.VARCHAR);
            _stmt.setObject(6, getUrl(), Types.VARCHAR);
            _stmt.setObject(7, getUrlPopup(), Types.VARCHAR);
            _stmt.setObject(8, getExpirationCache(), Types.INTEGER);
            _stmt.setObject(9, getVueReduiteUrl(), Types.VARCHAR);
            _stmt.setObject(10, getDiffusionMode(), Types.VARCHAR);
            _stmt.setObject(11, getDiffusionPublicVise(), Types.LONGVARCHAR);
            _stmt.setObject(12, getDiffusionModeRestriction(), Types.VARCHAR);
            _stmt.setObject(13, getDiffusionPublicViseRestriction(), Types.LONGVARCHAR);
            _stmt.setObject(14, getIdService(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
