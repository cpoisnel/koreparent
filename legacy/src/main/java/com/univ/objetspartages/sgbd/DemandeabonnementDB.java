package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
// TODO: Auto-generated Javadoc

/**
 * The Class DemandeabonnementDB.
 */
@Deprecated
public class DemandeabonnementDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet rs = null;

    /** The id demande abonnement. */
    private Long idDemandeAbonnement = null;

    /** The code utilisateur. */
    private String codeUtilisateur = null;

    /** The format souhaite. */
    private String formatSouhaite = null;

    /** The adresse mail utilisateur. */
    private String adresseMailUtilisateur = null;

    /** The liste modeles mails. */
    private String listeModelesMails = null;

    /**
     * Instantiates a new demandeabonnement db.
     */
    public DemandeabonnementDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "DEMANDEABONNEMENT (ID_DEMANDE_ABONNEMENT  ,   CODE_UTILISATEUR  ,   FORMAT_SOUHAITE  ,   ADRESSE_MAIL_UTILISATEUR  ,   LISTE_MODELES_MAILS  )              VALUES (?  ,?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdDemandeAbonnement(), Types.BIGINT);
            _stmt.setObject(2, getCodeUtilisateur(), Types.VARCHAR);
            _stmt.setObject(3, getFormatSouhaite(), Types.VARCHAR);
            _stmt.setObject(4, getAdresseMailUtilisateur(), Types.VARCHAR);
            _stmt.setObject(5, getListeModelesMails(), Types.VARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdDemandeAbonnement(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "DEMANDEABONNEMENT WHERE " + "             ID_DEMANDE_ABONNEMENT = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdDemandeAbonnement(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id demande abonnement.
     *
     * @return the id demande abonnement
     */
    public Long getIdDemandeAbonnement() {
        return idDemandeAbonnement;
    }

    /**
     * Gets the code utilisateur.
     *
     * @return the code utilisateur
     */
    public String getCodeUtilisateur() {
        return codeUtilisateur;
    }

    /**
     * Gets the format souhaite.
     *
     * @return the format souhaite
     */
    public String getFormatSouhaite() {
        return formatSouhaite;
    }

    /**
     * Gets the adresse mail utilisateur.
     *
     * @return the adresse mail utilisateur
     */
    public String getAdresseMailUtilisateur() {
        return adresseMailUtilisateur;
    }

    /**
     * Gets the liste modeles mails.
     *
     * @return the liste modeles mails
     */
    public String getListeModelesMails() {
        return listeModelesMails;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT " + "T1.ID_DEMANDE_ABONNEMENT ,  " + "T1.CODE_UTILISATEUR ,  " + "T1.FORMAT_SOUHAITE ,  " + "T1.ADRESSE_MAIL_UTILISATEUR ,  " + "T1.LISTE_MODELES_MAILS " + "         FROM " + qualifier + "DEMANDEABONNEMENT T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                retrieveFromRS(rs);
                res = true;
            } else {
                try {
                    rs.close();
                } finally {
                    rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_DEMANDE_ABONNEMENT," + "             CODE_UTILISATEUR," + "             FORMAT_SOUHAITE," + "             ADRESSE_MAIL_UTILISATEUR," + "             LISTE_MODELES_MAILS" + "         FROM " + qualifier + "DEMANDEABONNEMENT WHERE " + "             ID_DEMANDE_ABONNEMENT = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdDemandeAbonnement(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdDemandeAbonnement(new Long(_rs.getLong(1)));
            setCodeUtilisateur(_rs.getString(2));
            setFormatSouhaite(_rs.getString(3));
            setAdresseMailUtilisateur(_rs.getString(4));
            setListeModelesMails(_rs.getString(5));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            /* éxécution requete */
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "DEMANDEABONNEMENT T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            rs = _stmt.executeQuery(query);
            rs.next();
            count = rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id demande abonnement.
     *
     * @param _idDemandeAbonnement
     *            the new id demande abonnement
     */
    public void setIdDemandeAbonnement(final Long _idDemandeAbonnement) {
        idDemandeAbonnement = _idDemandeAbonnement;
    }

    /**
     * Sets the code utilisateur.
     *
     * @param _codeUtilisateur
     *            the new code utilisateur
     */
    public void setCodeUtilisateur(final String _codeUtilisateur) {
        codeUtilisateur = _codeUtilisateur;
    }

    /**
     * Sets the format souhaite.
     *
     * @param _formatSouhaite
     *            the new format souhaite
     */
    public void setFormatSouhaite(final String _formatSouhaite) {
        formatSouhaite = _formatSouhaite;
    }

    /**
     * Sets the adresse mail utilisateur.
     *
     * @param _adresseMailUtilisateur
     *            the new adresse mail utilisateur
     */
    public void setAdresseMailUtilisateur(final String _adresseMailUtilisateur) {
        adresseMailUtilisateur = _adresseMailUtilisateur;
    }

    /**
     * Sets the liste modeles mails.
     *
     * @param _listeModelesMails
     *            the new liste modeles mails
     */
    public void setListeModelesMails(final String _listeModelesMails) {
        listeModelesMails = _listeModelesMails;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdDemandeAbonnement() + aSeparator + getCodeUtilisateur() + aSeparator + getFormatSouhaite() + aSeparator + getAdresseMailUtilisateur() + aSeparator + getListeModelesMails()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "DEMANDEABONNEMENT SET " + "ID_DEMANDE_ABONNEMENT = ?, " + "CODE_UTILISATEUR = ?, " + "FORMAT_SOUHAITE = ?, " + "ADRESSE_MAIL_UTILISATEUR = ?, " + "LISTE_MODELES_MAILS = ? " + "         WHERE " + "             ID_DEMANDE_ABONNEMENT = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdDemandeAbonnement(), Types.BIGINT);
            _stmt.setObject(2, getCodeUtilisateur(), Types.VARCHAR);
            _stmt.setObject(3, getFormatSouhaite(), Types.VARCHAR);
            _stmt.setObject(4, getAdresseMailUtilisateur(), Types.VARCHAR);
            _stmt.setObject(5, getListeModelesMails(), Types.VARCHAR);
            _stmt.setObject(6, getIdDemandeAbonnement(), Types.BIGINT);
            _stmt.executeUpdate();
            _stmt.close();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
