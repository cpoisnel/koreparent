package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Date;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
// TODO: Auto-generated Javadoc

/**
 * The Class PhotoDB.
 * @deprecated classe plus utilisée, elle est remplacée par {@link com.univ.objetspartages.bean.RessourceBean}
 */
@Deprecated
public class PhotoDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet _rs = null;

    /** The id photo. */
    private Long idPhoto = null;

    /** The titre. */
    private String titre = null;

    /** The legende. */
    private String legende = null;

    /** The description. */
    private String description = null;

    /** The type photo. */
    private String typePhoto = null;

    /** The largeur. */
    private Integer largeur = null;

    /** The hauteur. */
    private Integer hauteur = null;

    /** The code structure. */
    private String codeStructure = null;

    /** The url. */
    private String url = null;

    /** The path. */
    private String path = null;

    /** The date creation. */
    private Date dateCreation = null;

    /** The code redacteur. */
    private String codeRedacteur = null;

    /** The meta keywords. */
    private String metaKeywords = null;

    /** The code rubrique. */
    private String codeRubrique = null;

    /**
     * Instantiates a new photo db.
     */
    public PhotoDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "PHOTO (ID_PHOTO  ,   TITRE  ,   LEGENDE  ,   DESCRIPTION  ,   TYPE_PHOTO  ,   LARGEUR  ,   HAUTEUR  ,   CODE_STRUCTURE  ,   URL  ,   PATH  ,   DATE_CREATION  ,   CODE_REDACTEUR  ,   META_KEYWORDS  ,   CODE_RUBRIQUE  )              VALUES (?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdPhoto(), Types.BIGINT);
            _stmt.setObject(2, getTitre(), Types.VARCHAR);
            _stmt.setObject(3, getLegende(), Types.VARCHAR);
            _stmt.setObject(4, getDescription(), Types.LONGVARCHAR);
            _stmt.setObject(5, getTypePhoto(), Types.VARCHAR);
            _stmt.setObject(6, getLargeur(), Types.INTEGER);
            _stmt.setObject(7, getHauteur(), Types.INTEGER);
            _stmt.setObject(8, getCodeStructure(), Types.VARCHAR);
            _stmt.setObject(9, getUrl(), Types.VARCHAR);
            _stmt.setObject(10, getPath(), Types.VARCHAR);
            _stmt.setObject(11, getDateCreation(), Types.TIMESTAMP);
            _stmt.setObject(12, getCodeRedacteur(), Types.VARCHAR);
            _stmt.setObject(13, getMetaKeywords(), Types.LONGVARCHAR);
            _stmt.setObject(14, getCodeRubrique(), Types.VARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdPhoto(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "PHOTO WHERE " + "             ID_PHOTO = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdPhoto(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id photo.
     *
     * @return the id photo
     */
    public Long getIdPhoto() {
        return idPhoto;
    }

    /**
     * Gets the titre.
     *
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Gets the legende.
     *
     * @return the legende
     */
    public String getLegende() {
        return legende;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the type photo.
     *
     * @return the type photo
     */
    public String getTypePhoto() {
        return typePhoto;
    }

    /**
     * Gets the largeur.
     *
     * @return the largeur
     */
    public Integer getLargeur() {
        return largeur;
    }

    /**
     * Gets the hauteur.
     *
     * @return the hauteur
     */
    public Integer getHauteur() {
        return hauteur;
    }

    /**
     * Gets the code structure.
     *
     * @return the code structure
     */
    public String getCodeStructure() {
        return codeStructure;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * Gets the date creation.
     *
     * @return the date creation
     */
    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * Gets the code redacteur.
     *
     * @return the code redacteur
     */
    public String getCodeRedacteur() {
        return codeRedacteur;
    }

    /**
     * Gets the meta keywords.
     *
     * @return the meta keywords
     */
    public String getMetaKeywords() {
        return metaKeywords;
    }

    /**
     * Gets the code rubrique.
     *
     * @return the code rubrique
     */
    public String getCodeRubrique() {
        return codeRubrique;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT " + "T1.ID_PHOTO ,  " + "T1.TITRE ,  " + "T1.LEGENDE ,  " + "T1.DESCRIPTION ,  " + "T1.TYPE_PHOTO ,  " + "T1.LARGEUR ,  " + "T1.HAUTEUR ,  " + "T1.CODE_STRUCTURE ,  " + "T1.URL ,  " + "T1.PATH ,  " + "T1.DATE_CREATION ,  " + "T1.CODE_REDACTEUR ,  " + "T1.META_KEYWORDS ,  " + "T1.CODE_RUBRIQUE " + "         FROM " + qualifier + "PHOTO T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * RÃ©cupÃ©ration d'une ligne de la base de donnÃ©es.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_PHOTO," + "             TITRE," + "             LEGENDE," + "             DESCRIPTION," + "             TYPE_PHOTO," + "             LARGEUR," + "             HAUTEUR," + "             CODE_STRUCTURE," + "             URL," + "             PATH," + "             DATE_CREATION," + "             CODE_REDACTEUR," + "             META_KEYWORDS," + "             CODE_RUBRIQUE" + "         FROM " + qualifier + "PHOTO WHERE " + "             ID_PHOTO = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdPhoto(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdPhoto(_rs.getLong(1));
            setTitre(_rs.getString(2));
            setLegende(_rs.getString(3));
            setDescription(_rs.getString(4));
            setTypePhoto(_rs.getString(5));
            setLargeur(_rs.getInt(6));
            setHauteur(_rs.getInt(7));
            setCodeStructure(_rs.getString(8));
            setUrl(_rs.getString(9));
            setPath(_rs.getString(10));
            setDateCreation(SqlDateConverter.fromTimestamp(_rs.getTimestamp(11)));
            setCodeRedacteur(_rs.getString(12));
            setMetaKeywords(_rs.getString(13));
            setCodeRubrique(_rs.getString(14));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette mÃ©thode optimise l'exÃ©cution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            /* Ã©xÃ©cution requete */
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "PHOTO T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* Ã©xÃ©cution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id photo.
     *
     * @param _idPhoto
     *            the new id photo
     */
    public void setIdPhoto(final Long _idPhoto) {
        idPhoto = _idPhoto;
    }

    /**
     * Sets the titre.
     *
     * @param _titre
     *            the new titre
     */
    public void setTitre(final String _titre) {
        titre = _titre;
    }

    /**
     * Sets the legende.
     *
     * @param _legende
     *            the new legende
     */
    public void setLegende(final String _legende) {
        legende = _legende;
    }

    /**
     * Sets the description.
     *
     * @param _description
     *            the new description
     */
    public void setDescription(final String _description) {
        description = _description;
    }

    /**
     * Sets the type photo.
     *
     * @param _typePhoto
     *            the new type photo
     */
    public void setTypePhoto(final String _typePhoto) {
        typePhoto = _typePhoto;
    }

    /**
     * Sets the largeur.
     *
     * @param _largeur
     *            the new largeur
     */
    public void setLargeur(final Integer _largeur) {
        largeur = _largeur;
    }

    /**
     * Sets the hauteur.
     *
     * @param _hauteur
     *            the new hauteur
     */
    public void setHauteur(final Integer _hauteur) {
        hauteur = _hauteur;
    }

    /**
     * Sets the code structure.
     *
     * @param _codeStructure
     *            the new code structure
     */
    public void setCodeStructure(final String _codeStructure) {
        codeStructure = _codeStructure;
    }

    /**
     * Sets the url.
     *
     * @param _url
     *            the new url
     */
    public void setUrl(final String _url) {
        url = _url;
    }

    /**
     * Sets the path.
     *
     * @param _path
     *            the new path
     */
    public void setPath(final String _path) {
        path = _path;
    }

    /**
     * Sets the date creation.
     *
     * @param _dateCreation
     *            the new date creation
     */
    public void setDateCreation(final Date _dateCreation) {
        dateCreation = _dateCreation;
    }

    /**
     * Sets the code redacteur.
     *
     * @param _codeRedacteur
     *            the new code redacteur
     */
    public void setCodeRedacteur(final String _codeRedacteur) {
        codeRedacteur = _codeRedacteur;
    }

    /**
     * Sets the meta keywords.
     *
     * @param _metaKeywords
     *            the new meta keywords
     */
    public void setMetaKeywords(final String _metaKeywords) {
        metaKeywords = _metaKeywords;
    }

    /**
     * Sets the code rubrique.
     *
     * @param _codeRubrique
     *            the new code rubrique
     */
    public void setCodeRubrique(final String _codeRubrique) {
        codeRubrique = _codeRubrique;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdPhoto() + aSeparator + getTitre() + aSeparator + getLegende() + aSeparator + getDescription() + aSeparator + getTypePhoto() + aSeparator + getLargeur() + aSeparator + getHauteur() + aSeparator + getCodeStructure() + aSeparator + getUrl() + aSeparator + getPath() + aSeparator + getDateCreation() + aSeparator + getCodeRedacteur() + aSeparator + getMetaKeywords() + aSeparator + getCodeRubrique()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "PHOTO SET " + "ID_PHOTO = ?, " + "TITRE = ?, " + "LEGENDE = ?, " + "DESCRIPTION = ?, " + "TYPE_PHOTO = ?, " + "LARGEUR = ?, " + "HAUTEUR = ?, " + "CODE_STRUCTURE = ?, " + "URL = ?, " + "PATH = ?, " + "DATE_CREATION = ?, " + "CODE_REDACTEUR = ?, " + "META_KEYWORDS = ?, " + "CODE_RUBRIQUE = ? " + "         WHERE " + "             ID_PHOTO = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdPhoto(), Types.BIGINT);
            _stmt.setObject(2, getTitre(), Types.VARCHAR);
            _stmt.setObject(3, getLegende(), Types.VARCHAR);
            _stmt.setObject(4, getDescription(), Types.LONGVARCHAR);
            _stmt.setObject(5, getTypePhoto(), Types.VARCHAR);
            _stmt.setObject(6, getLargeur(), Types.INTEGER);
            _stmt.setObject(7, getHauteur(), Types.INTEGER);
            _stmt.setObject(8, getCodeStructure(), Types.VARCHAR);
            _stmt.setObject(9, getUrl(), Types.VARCHAR);
            _stmt.setObject(10, getPath(), Types.VARCHAR);
            _stmt.setObject(11, getDateCreation(), Types.TIMESTAMP);
            _stmt.setObject(12, getCodeRedacteur(), Types.VARCHAR);
            _stmt.setObject(13, getMetaKeywords(), Types.LONGVARCHAR);
            _stmt.setObject(14, getCodeRubrique(), Types.VARCHAR);
            _stmt.setObject(15, getIdPhoto(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }

    /**
     * Count.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int count(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            final String query = "SELECT COUNT(*)   FROM " + qualifier + "PHOTO T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }
}
