package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.services.ServiceMedia;
// TODO: Auto-generated Javadoc

/**
 * Classe d'acces aux donnees pour media.
 * @deprecated utilisez {@link ServiceMedia} pour les accès en bdd et {@link MediaBean} pour les données
 */
@Deprecated
public class MediaDB extends MediaBean {

    /**
     *
     */
    private static final long serialVersionUID = -2312624118553022318L;

    /** The ctx. */
    protected transient OMContext ctx = null;

    /** The qualifier. */
    protected String qualifier = null;

    /** The rs. */
    private transient ResultSet rs = null;

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        final String s = "" + getId() + aSeparator + getTitre() + aSeparator + getLegende() + aSeparator + getDescription() + aSeparator + getAuteur() + aSeparator + getCopyright() + aSeparator + getTypeRessource() + aSeparator + getTypeMedia() + aSeparator + getSource() + aSeparator + getFormat() + aSeparator + getCodeRubrique() + aSeparator + getCodeRattachement() + aSeparator + getUrl() + aSeparator + getUrlVignette() + aSeparator + getPoids() + aSeparator + getCodeRedacteur() + aSeparator + getDateCreation() + aSeparator + getThematique() + aSeparator + getMetaKeywords() + aSeparator + getSpecificData() + aSeparator + getTraductionData() + aSeparator + getAccessibilityData() + aSeparator + getIsMutualise();
        return s;
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        ctx = _ctx;
    }

    /**
     * Sets the qualifier.
     *
     * @param qualifier
     *            the new qualifier
     */
    public void setQualifier(final String qualifier) {
        this.qualifier = qualifier;
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = getConnection().prepareStatement("insert into MEDIA (ID_MEDIA , TITRE , LEGENDE , DESCRIPTION , AUTEUR , COPYRIGHT , TYPE_RESSOURCE , TYPE_MEDIA , SOURCE , FORMAT , CODE_RUBRIQUE , CODE_RATTACHEMENT , URL , URL_VIGNETTE , POIDS , CODE_REDACTEUR , DATE_CREATION , THEMATIQUE , META_KEYWORDS , SPECIFIC_DATA , TRADUCTION_DATA , ACCESSIBILITY_DATA , IS_MUTUALISE) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setObject(1, getId(), Types.BIGINT);
            stmt.setObject(2, getTitre(), Types.VARCHAR);
            stmt.setObject(3, getLegende(), Types.LONGVARCHAR);
            stmt.setObject(4, getDescription(), Types.LONGVARCHAR);
            stmt.setObject(5, getAuteur(), Types.LONGVARCHAR);
            stmt.setObject(6, getCopyright(), Types.LONGVARCHAR);
            stmt.setObject(7, getTypeRessource(), Types.VARCHAR);
            stmt.setObject(8, getTypeMedia(), Types.VARCHAR);
            stmt.setObject(9, getSource(), Types.VARCHAR);
            stmt.setObject(10, getFormat(), Types.VARCHAR);
            stmt.setObject(11, getCodeRubrique(), Types.VARCHAR);
            stmt.setObject(12, getCodeRattachement(), Types.VARCHAR);
            stmt.setObject(13, getUrl(), Types.LONGVARCHAR);
            stmt.setObject(14, getUrlVignette(), Types.LONGVARCHAR);
            stmt.setObject(15, getPoids(), Types.INTEGER);
            stmt.setObject(16, getCodeRedacteur(), Types.VARCHAR);
            stmt.setObject(17, getDateCreation(), Types.TIMESTAMP);
            stmt.setObject(18, getThematique(), Types.VARCHAR);
            stmt.setObject(19, getMetaKeywords(), Types.LONGVARCHAR);
            stmt.setObject(20, getSpecificData(), Types.LONGVARCHAR);
            stmt.setObject(21, getTraductionData(), Types.LONGVARCHAR);
            stmt.setObject(22, getAccessibilityData(), Types.LONGVARCHAR);
            stmt.setObject(23, getIsMutualise(), Types.VARCHAR);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = stmt.getGeneratedKeys();
            rs.next();
            setId(rs.getLong(1));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD add()", e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("delete from MEDIA " + "where ID_MEDIA = ?");
            // put parameters into statement
            stmt.setObject(1, getId(), Types.BIGINT);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD delete()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        final String query = "select distinct " + "T1.ID_MEDIA, " + "T1.TITRE, " + "T1.LEGENDE, " + "T1.DESCRIPTION, " + "T1.AUTEUR, " + "T1.COPYRIGHT, " + "T1.TYPE_RESSOURCE, " + "T1.TYPE_MEDIA, " + "T1.SOURCE, " + "T1.FORMAT, " + "T1.CODE_RUBRIQUE, " + "T1.CODE_RATTACHEMENT, " + "T1.URL, " + "T1.URL_VIGNETTE, " + "T1.POIDS, " + "T1.CODE_REDACTEUR, " + "T1.DATE_CREATION, " + "T1.THEMATIQUE, " + "T1.META_KEYWORDS, " + "T1.SPECIFIC_DATA, " + "T1.TRADUCTION_DATA, " + "T1.ACCESSIBILITY_DATA, " + "T1.IS_MUTUALISE " + "from " + (qualifier == null ? "" : qualifier) + "MEDIA T1 ";
        return query;
    }

    /**
     * Renvoie l'element suivant du ResultSet.
     *
     * @return true, if next item
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                retrieveFromRS();
                res = true;
            } else {
                try {
                    rs.close();
                } finally {
                    rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem()", e);
        }
        return res;
    }

    /**
     * Recuperation d'une ligne de la base de donnees.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("select " + "T1.ID_MEDIA, " + "T1.TITRE, " + "T1.LEGENDE, " + "T1.DESCRIPTION, " + "T1.AUTEUR, " + "T1.COPYRIGHT, " + "T1.TYPE_RESSOURCE, " + "T1.TYPE_MEDIA, " + "T1.SOURCE, " + "T1.FORMAT, " + "T1.CODE_RUBRIQUE, " + "T1.CODE_RATTACHEMENT, " + "T1.URL, " + "T1.URL_VIGNETTE, " + "T1.POIDS, " + "T1.CODE_REDACTEUR, " + "T1.DATE_CREATION, " + "T1.THEMATIQUE, " + "T1.META_KEYWORDS, " + "T1.SPECIFIC_DATA, " + "T1.TRADUCTION_DATA, " + "T1.ACCESSIBILITY_DATA, " + "T1.IS_MUTUALISE " + "from MEDIA T1 " + "where T1.ID_MEDIA = ?");
            stmt.setObject(1, getId(), Types.BIGINT);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve : METHOD_NO_RESULTS");
            }
            retrieveFromRS();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieve()", e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS() throws Exception {
        try {
            // get output from result set
            setId(rs.getLong(1));
            setTitre(rs.getString(2));
            setLegende(rs.getString(3));
            setDescription(rs.getString(4));
            setAuteur(rs.getString(5));
            setCopyright(rs.getString(6));
            setTypeRessource(rs.getString(7));
            setTypeMedia(rs.getString(8));
            setSource(rs.getString(9));
            setFormat(rs.getString(10));
            setCodeRubrique(rs.getString(11));
            setCodeRattachement(rs.getString(12));
            setUrl(rs.getString(13));
            setUrlVignette(rs.getString(14));
            setPoids(rs.getLong(15));
            setCodeRedacteur(rs.getString(16));
            setDateCreation(SqlDateConverter.fromTimestamp(rs.getTimestamp(17)));
            setThematique(rs.getString(18));
            setMetaKeywords(rs.getString(19));
            setSpecificData(rs.getString(20));
            setTraductionData(rs.getString(21));
            setAccessibilityData(rs.getString(22));
            setIsMutualise(rs.getString(23));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS()", e);
        }
    }

    /**
     * Cette methode optimise l'execution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(final String sqlSuffix) throws Exception {
        PreparedStatement stmt;
        try {
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String sqlSuffix) throws Exception {
        int count;
        PreparedStatement stmt;
        try {
            // recuperation nombre de lignes
            String query = "select count(distinct T1.ID_MEDIA) from " + (qualifier == null ? "" : qualifier) + "MEDIA T1 ";
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
            rs.next();
            count = rs.getInt(1);
            // execution requete
            query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
        return count;
    }

    /**
     * Mise a jour d'une ligne de la base de donnees.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("update MEDIA set " + "ID_MEDIA = ?, " + "TITRE = ?, " + "LEGENDE = ?, " + "DESCRIPTION = ?, " + "AUTEUR = ?, " + "COPYRIGHT = ?, " + "TYPE_RESSOURCE = ?, " + "TYPE_MEDIA = ?, " + "SOURCE = ?, " + "FORMAT = ?, " + "CODE_RUBRIQUE = ?, " + "CODE_RATTACHEMENT = ?, " + "URL = ?, " + "URL_VIGNETTE = ?, " + "POIDS = ?, " + "CODE_REDACTEUR = ?, " + "DATE_CREATION = ?, " + "THEMATIQUE = ?, " + "META_KEYWORDS = ?, " + "SPECIFIC_DATA = ?, " + "TRADUCTION_DATA = ?, " + "ACCESSIBILITY_DATA = ?, " + "IS_MUTUALISE = ? " + "where ID_MEDIA = ?");
            // put parameters into statement
            stmt.setObject(1, getId(), Types.BIGINT);
            stmt.setObject(2, getTitre(), Types.VARCHAR);
            stmt.setObject(3, getLegende(), Types.LONGVARCHAR);
            stmt.setObject(4, getDescription(), Types.LONGVARCHAR);
            stmt.setObject(5, getAuteur(), Types.LONGVARCHAR);
            stmt.setObject(6, getCopyright(), Types.LONGVARCHAR);
            stmt.setObject(7, getTypeRessource(), Types.VARCHAR);
            stmt.setObject(8, getTypeMedia(), Types.VARCHAR);
            stmt.setObject(9, getSource(), Types.VARCHAR);
            stmt.setObject(10, getFormat(), Types.VARCHAR);
            stmt.setObject(11, getCodeRubrique(), Types.VARCHAR);
            stmt.setObject(12, getCodeRattachement(), Types.VARCHAR);
            stmt.setObject(13, getUrl(), Types.LONGVARCHAR);
            stmt.setObject(14, getUrlVignette(), Types.LONGVARCHAR);
            stmt.setObject(15, getPoids(), Types.INTEGER);
            stmt.setObject(16, getCodeRedacteur(), Types.VARCHAR);
            stmt.setObject(17, getDateCreation(), Types.TIMESTAMP);
            stmt.setObject(18, getThematique(), Types.VARCHAR);
            stmt.setObject(19, getMetaKeywords(), Types.LONGVARCHAR);
            stmt.setObject(20, getSpecificData(), Types.LONGVARCHAR);
            stmt.setObject(21, getTraductionData(), Types.LONGVARCHAR);
            stmt.setObject(22, getAccessibilityData(), Types.LONGVARCHAR);
            stmt.setObject(23, getIsMutualise(), Types.VARCHAR);
            stmt.setObject(24, getId(), Types.BIGINT);
            stmt.executeUpdate();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD update()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Count.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int count(String sqlSuffix) throws Exception {
        int count = 0;
        PreparedStatement stmt = null;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            final String query = "SELECT COUNT(*)   FROM " + (qualifier == null ? "" : qualifier) + "MEDIA T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery(query);
            rs.next();
            count = rs.getInt(1);
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return count;
    }

    /**
     * Gets the thematique.
     *
     * @return the thematique
     */
    @Override
    public String getThematique() {
        return thematique;
    }

    /**
     * Sets the thematique.
     *
     * @param thematique
     *            the new thematique
     */
    @Override
    public void setThematique(final String thematique) {
        this.thematique = thematique;
    }
}
