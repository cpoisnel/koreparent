package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.services.ServiceRessource;
// TODO: Auto-generated Javadoc

/**
 * Classe d'acces aux donnees pour ressource.
 * @deprecated Utilisez {@link ServiceRessource} pour les accès en bdd et les méthodes associées et {@link RessourceBean} pour les données
 */
@Deprecated
public class RessourceDB extends RessourceBean {

    /** The ctx. */
    protected transient OMContext ctx = null;

    /** The qualifier. */
    protected String qualifier = null;

    /** The rs. */
    private transient ResultSet rs = null;

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return "" + getId() + aSeparator + getIdMedia() + aSeparator + getCodeParent() + aSeparator + getEtat() + aSeparator + getOrdre();
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        ctx = _ctx;
    }

    /**
     * Sets the qualifier.
     *
     * @param qualifier
     *            the new qualifier
     */
    public void setQualifier(final String qualifier) {
        this.qualifier = qualifier;
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = getConnection().prepareStatement("insert into RESSOURCE (ID_RESSOURCE , ID_MEDIA , CODE_PARENT , ETAT , ORDRE) values (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setObject(1, getId(), Types.BIGINT);
            stmt.setObject(2, getIdMedia(), Types.BIGINT);
            stmt.setObject(3, getCodeParent(), Types.VARCHAR);
            stmt.setObject(4, getEtat(), Types.VARCHAR);
            stmt.setObject(5, getOrdre(), Types.INTEGER);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = stmt.getGeneratedKeys();
            rs.next();
            setId(rs.getLong(1));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD add()", e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("delete from RESSOURCE " + "where ID_RESSOURCE = ?");
            // put parameters into statement
            stmt.setObject(1, getId(), Types.BIGINT);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD delete()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        final String query = "select distinct " + "T1.ID_RESSOURCE, " + "T1.ID_MEDIA, " + "T1.CODE_PARENT, " + "T1.ETAT, " + "T1.ORDRE " + "from " + (qualifier == null ? "" : qualifier) + "RESSOURCE T1 ";
        return query;
    }

    /**
     * Renvoie l'element suivant du ResultSet.
     *
     * @return true, if next item
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                retrieveFromRS();
                res = true;
            } else {
                try {
                    rs.close();
                } finally {
                    rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem()", e);
        }
        return res;
    }

    /**
     * Recuperation d'une ligne de la base de donnees.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("select " + "T1.ID_RESSOURCE, " + "T1.ID_MEDIA, " + "T1.CODE_PARENT, " + "T1.ETAT, " + "T1.ORDRE " + "from RESSOURCE T1 " + "where T1.ID_RESSOURCE = ?");
            stmt.setObject(1, getId(), Types.BIGINT);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve : METHOD_NO_RESULTS");
            }
            retrieveFromRS();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieve()", e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS() throws Exception {
        try {
            // get output from result set
            setId(rs.getLong(1));
            setIdMedia(rs.getLong(2));
            setCodeParent(rs.getString(3));
            setEtat(rs.getString(4));
            setOrdre(rs.getInt(5));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS()", e);
        }
    }

    /**
     * Cette methode optimise l'execution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(final String sqlSuffix) throws Exception {
        PreparedStatement stmt = null;
        try {
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String sqlSuffix) throws Exception {
        int count = 0;
        PreparedStatement stmt = null;
        try {
            // recuperation nombre de lignes
            String query = "select count(distinct T1.ID_RESSOURCE) from " + (qualifier == null ? "" : qualifier) + "RESSOURCE T1 ";
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
            rs.next();
            count = rs.getInt(1);
            // execution requete
            query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
        return count;
    }

    /**
     * Mise a jour d'une ligne de la base de donnees.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("update RESSOURCE set " + "ID_RESSOURCE = ?, " + "ID_MEDIA = ?, " + "CODE_PARENT = ?, " + "ETAT = ?, " + "ORDRE = ? " + "where ID_RESSOURCE = ?");
            // put parameters into statement
            stmt.setObject(1, getId(), Types.BIGINT);
            stmt.setObject(2, getIdMedia(), Types.BIGINT);
            stmt.setObject(3, getCodeParent(), Types.VARCHAR);
            stmt.setObject(4, getEtat(), Types.VARCHAR);
            stmt.setObject(5, getOrdre(), Types.INTEGER);
            stmt.setObject(6, getId(), Types.BIGINT);
            stmt.executeUpdate();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD update()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
}
