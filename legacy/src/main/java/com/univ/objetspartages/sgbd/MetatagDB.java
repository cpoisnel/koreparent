package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.univ.objetspartages.bean.MetatagBean;

/**
 * The Class MetatagDB.
 * @deprecated Utilisez MetatagBean
 */
@Deprecated
public class MetatagDB extends MetatagBean {

    /** */
    private static final long serialVersionUID = -3927458003077689419L;

    /** The qualifier. */
    protected transient String qualifier = null;

    /** The ctx. */
    protected transient OMContext ctx = null;

    /** The _stmt. */
    private transient java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private transient ResultSet _rs = null;

    /**
     * Instantiates a new metatag db.
     */
    public MetatagDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "METATAG ( ID_METATAG, META_ID_FICHE, META_CODE_OBJET, META_LIBELLE_OBJET, META_HISTORIQUE, META_DATE_ARCHIVAGE, META_LISTE_REFERENCES, META_FORUM, META_FORUM_ANO, META_SAISIE_FRONT, META_MAIL_ANONYME, META_NOTIFICATION_MAIL, META_IN_TREE, META_DOCUMENT_FICHIERGW, META_RUBRIQUES_PUBLICATION, META_NIVEAU_APPROBATION, META_LIBELLE_FICHE, META_CODE, META_CODE_RATTACHEMENT, META_CODE_RUBRIQUE, META_META_KEYWORDS, META_META_DESCRIPTION, META_DATE_CREATION, META_DATE_PROPOSITION, META_DATE_VALIDATION, META_DATE_MODIFICATION, META_DATE_OPERATION, META_CODE_REDACTEUR, META_CODE_VALIDATION, META_LANGUE, META_ETAT_OBJET, META_NB_HITS, META_SOURCE_IMPORT, META_CODE_RATTACHEMENT_AUTRES, META_DIFFUSION_PUBLIC_VISE, META_DIFFUSION_MODE_RESTRICTION, META_DIFFUSION_PUBLIC_VISE_RESTRICTION, META_DATE_MISE_EN_LIGNE, META_DATE_SUPPRESSION, META_DATE_RUBRIQUAGE, META_CODE_RUBRIQUAGE) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getId(), Types.BIGINT);
            _stmt.setObject(2, getMetaIdFiche(), Types.BIGINT);
            _stmt.setObject(3, getMetaCodeObjet(), Types.VARCHAR);
            _stmt.setObject(4, getMetaLibelleObjet(), Types.VARCHAR);
            _stmt.setObject(5, getMetaHistorique(), Types.LONGVARCHAR);
            _stmt.setObject(6, SqlDateConverter.toDate(getMetaDateArchivage()), Types.DATE);
            _stmt.setObject(7, getMetaListeReferences(), Types.LONGVARCHAR);
            _stmt.setObject(8, getMetaForum(), Types.VARCHAR);
            _stmt.setObject(9, getMetaForumAno(), Types.VARCHAR);
            _stmt.setObject(10, getMetaSaisieFront(), Types.VARCHAR);
            _stmt.setObject(11, getMetaMailAnonyme(), Types.VARCHAR);
            _stmt.setObject(12, getMetaNotificationMail(), Types.VARCHAR);
            _stmt.setObject(13, getMetaInTree(), Types.VARCHAR);
            _stmt.setObject(14, getMetaDocumentFichiergw(), Types.VARCHAR);
            _stmt.setObject(15, getMetaRubriquesPublication(), Types.LONGVARCHAR);
            _stmt.setObject(16, getMetaNiveauApprobation(), Types.VARCHAR);
            _stmt.setObject(17, getMetaLibelleFiche(), Types.VARCHAR);
            _stmt.setObject(18, getMetaCode(), Types.VARCHAR);
            _stmt.setObject(19, getMetaCodeRattachement(), Types.VARCHAR);
            _stmt.setObject(20, getMetaCodeRubrique(), Types.VARCHAR);
            _stmt.setObject(21, getMetaKeywords(), Types.LONGVARCHAR);
            _stmt.setObject(22, getMetaDescription(), Types.LONGVARCHAR);
            _stmt.setObject(23, SqlDateConverter.toTimestamp(getMetaDateCreation()), Types.TIMESTAMP);
            _stmt.setObject(24, SqlDateConverter.toTimestamp(getMetaDateProposition()), Types.TIMESTAMP);
            _stmt.setObject(25, SqlDateConverter.toTimestamp(getMetaDateValidation()), Types.TIMESTAMP);
            _stmt.setObject(26, SqlDateConverter.toTimestamp(getMetaDateModification()), Types.TIMESTAMP);
            _stmt.setObject(27, SqlDateConverter.toTimestamp(getMetaDateOperation()), Types.TIMESTAMP);
            _stmt.setObject(28, getMetaCodeRedacteur(), Types.VARCHAR);
            _stmt.setObject(29, getMetaCodeValidation(), Types.VARCHAR);
            _stmt.setObject(30, getMetaLangue(), Types.VARCHAR);
            _stmt.setObject(31, getMetaEtatObjet(), Types.VARCHAR);
            _stmt.setObject(32, getMetaNbHits(), Types.BIGINT);
            _stmt.setObject(33, getMetaSourceImport(), Types.VARCHAR);
            _stmt.setObject(34, getMetaCodeRattachementAutres(), Types.LONGVARCHAR);
            _stmt.setObject(35, getMetaDiffusionPublicVise(), Types.VARCHAR);
            _stmt.setObject(36, getMetaDiffusionModeRestriction(), Types.VARCHAR);
            _stmt.setObject(37, getMetaDiffusionPublicViseRestriction(), Types.VARCHAR);
            _stmt.setObject(38, SqlDateConverter.toTimestamp(getMetaDateMiseEnLigne()), Types.TIMESTAMP);
            _stmt.setObject(39, SqlDateConverter.toDate(getMetaDateSuppression()), Types.DATE);
            _stmt.setObject(40, SqlDateConverter.toDate(getMetaDateRubriquage()), Types.DATE);
            _stmt.setObject(41, getMetaCodeRubriquage(), Types.VARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setId(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "METATAG WHERE " + " ID_METATAG = ? ");
            // put parameters into statement
            _stmt.setObject(1, getId(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            _stmt.close();
            _stmt = null;
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the sQL base optimized query.
     *
     * @return the sQL base optimized query
     */
    public String getSQLBaseOptimizedQuery() {
        String distinct = "";
        if (ctx.getDatas().get("optimizedLimit") == null) {
            distinct = "DISTINCT ";
        }
        final String query = "SELECT " + distinct + "T1.META_ID_FICHE, T1.META_CODE_OBJET , T1.META_CODE, T1.META_LANGUE, T1.META_ETAT_OBJET, T1.META_LIBELLE_FICHE from " + qualifier + " METATAG T1 ";
        return query;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT " + "       T1.ID_METATAG ,  " + "       T1.META_ID_FICHE ,  " + "       T1.META_CODE_OBJET ,  " + "       T1.META_LIBELLE_OBJET ,  " + "       T1.META_HISTORIQUE ,  " + "       T1.META_DATE_ARCHIVAGE ,  " + "       T1.META_LISTE_REFERENCES ,  " + "       T1.META_FORUM ,  " + "       T1.META_FORUM_ANO ,  " + "       T1.META_SAISIE_FRONT ,  " + "       T1.META_MAIL_ANONYME ,  " + "       T1.META_NOTIFICATION_MAIL ,  " + "       T1.META_IN_TREE ,  " + "       T1.META_DOCUMENT_FICHIERGW ,  " + "       T1.META_RUBRIQUES_PUBLICATION ,  " + "       T1.META_NIVEAU_APPROBATION ,  " + "       T1.META_LIBELLE_FICHE ,  " + "       T1.META_CODE ,  " + "       T1.META_CODE_RATTACHEMENT ,  " + "       T1.META_CODE_RUBRIQUE ,  " + "       T1.META_META_KEYWORDS ,  " + "       T1.META_META_DESCRIPTION ,  " + "       T1.META_DATE_CREATION ,  " + "       T1.META_DATE_PROPOSITION ,  " + "       T1.META_DATE_VALIDATION ,  " + "       T1.META_DATE_MODIFICATION ,  " + "       T1.META_DATE_OPERATION ,  " + "       T1.META_CODE_REDACTEUR ,  " + "       T1.META_CODE_VALIDATION ,  " + "       T1.META_LANGUE ,  " + "       T1.META_ETAT_OBJET ,  " + "       T1.META_NB_HITS ,  " + "       T1.META_SOURCE_IMPORT ,  " + "       T1.META_CODE_RATTACHEMENT_AUTRES ,  " + "       T1.META_DIFFUSION_PUBLIC_VISE ,  " + "       T1.META_DIFFUSION_MODE_RESTRICTION ,  " + "       T1.META_DIFFUSION_PUBLIC_VISE_RESTRICTION ,  " + "       T1.META_DATE_MISE_EN_LIGNE ,  " + "       T1.META_DATE_SUPPRESSION ,  " + "       T1.META_DATE_RUBRIQUAGE ,  " + "       T1.META_CODE_RUBRIQUAGE " + "         FROM " + qualifier + "METATAG T1 ";
    }

    /**
     * Gets the sQL base query no distinct.
     *
     * @return the sQL base query no distinct
     */
    public String getSQLBaseQueryNoDistinct() {
        return "SELECT " + "       T1.ID_METATAG ,  " + "       T1.META_ID_FICHE ,  " + "       T1.META_CODE_OBJET ,  " + "       T1.META_LIBELLE_OBJET ,  " + "       T1.META_HISTORIQUE ,  " + "       T1.META_DATE_ARCHIVAGE ,  " + "       T1.META_LISTE_REFERENCES ,  " + "       T1.META_FORUM ,  " + "       T1.META_FORUM_ANO ,  " + "       T1.META_SAISIE_FRONT ,  " + "       T1.META_MAIL_ANONYME ,  " + "       T1.META_NOTIFICATION_MAIL ,  " + "       T1.META_IN_TREE ,  " + "       T1.META_DOCUMENT_FICHIERGW ,  " + "       T1.META_RUBRIQUES_PUBLICATION ,  " + "       T1.META_NIVEAU_APPROBATION ,  " + "       T1.META_LIBELLE_FICHE ,  " + "       T1.META_CODE ,  " + "       T1.META_CODE_RATTACHEMENT ,  " + "       T1.META_CODE_RUBRIQUE ,  " + "       T1.META_META_KEYWORDS ,  " + "       T1.META_META_DESCRIPTION ,  " + "       T1.META_DATE_CREATION ,  " + "       T1.META_DATE_PROPOSITION ,  " + "       T1.META_DATE_VALIDATION ,  " + "       T1.META_DATE_MODIFICATION ,  " + "       T1.META_DATE_OPERATION ,  " + "       T1.META_CODE_REDACTEUR ,  " + "       T1.META_CODE_VALIDATION ,  " + "       T1.META_LANGUE ,  " + "       T1.META_ETAT_OBJET ,  " + "       T1.META_NB_HITS ,  " + "       T1.META_SOURCE_IMPORT ,  " + "       T1.META_CODE_RATTACHEMENT_AUTRES ,  " + "       T1.META_DIFFUSION_PUBLIC_VISE ,  " + "       T1.META_DIFFUSION_MODE_RESTRICTION ,  " + "       T1.META_DIFFUSION_PUBLIC_VISE_RESTRICTION ,  " + "       T1.META_DATE_MISE_EN_LIGNE ,  " + "       T1.META_DATE_SUPPRESSION ,  " + "       T1.META_DATE_RUBRIQUAGE ,  " + "       T1.META_CODE_RUBRIQUAGE " + "         FROM " + qualifier + "METATAG T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     * the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                if (ctx.getDatas().get("optimizedSelect") != null) {
                    retrieveFromOptimizedRS();
                } else {
                    retrieveFromRS();
                }
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() ", e);
        }
        return res;
    }

    /**
     * Retrieve from optimized rs.
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromOptimizedRS() throws Exception {
        try {
            // get output from result set
            setMetaIdFiche(_rs.getLong(1));
            setMetaCodeObjet(_rs.getString(2));
            setMetaCode(_rs.getString(3));
            setMetaLangue(_rs.getString(4));
            setMetaEtatObjet(_rs.getString(5));
            setMetaLibelleFiche(_rs.getString(6));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromOptimizedRS() " + exc);
        }
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("SELECT  ID_METATAG, META_ID_FICHE, META_CODE_OBJET, META_LIBELLE_OBJET, META_HISTORIQUE, META_DATE_ARCHIVAGE, META_LISTE_REFERENCES, META_FORUM, META_FORUM_ANO, META_SAISIE_FRONT, META_MAIL_ANONYME, META_NOTIFICATION_MAIL, META_IN_TREE, META_DOCUMENT_FICHIERGW, META_RUBRIQUES_PUBLICATION, META_NIVEAU_APPROBATION, META_LIBELLE_FICHE, META_CODE, META_CODE_RATTACHEMENT, META_CODE_RUBRIQUE, META_META_KEYWORDS, META_META_DESCRIPTION, META_DATE_CREATION, META_DATE_PROPOSITION, META_DATE_VALIDATION, META_DATE_MODIFICATION, META_DATE_OPERATION, META_CODE_REDACTEUR, META_CODE_VALIDATION, META_LANGUE, META_ETAT_OBJET, META_NB_HITS, META_SOURCE_IMPORT, META_CODE_RATTACHEMENT_AUTRES, META_DIFFUSION_PUBLIC_VISE, META_DIFFUSION_MODE_RESTRICTION, META_DIFFUSION_PUBLIC_VISE_RESTRICTION, META_DATE_MISE_EN_LIGNE, META_DATE_SUPPRESSION, META_DATE_RUBRIQUAGE, META_CODE_RUBRIQUAGE         FROM " + qualifier + "METATAG WHERE  ID_METATAG = ? ");
            // put parameters into statement
            _stmt.setObject(1, getId(), Types.BIGINT);
            _rs = _stmt.executeQuery();
            if (!_rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (_rs != null) {
                _rs.close();
            }
            _stmt.close();
            _stmt = null;
        }
    }

    /**
     * Retrieve from rs.
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS() throws Exception {
        try {
            // get output from result set
            setId(_rs.getLong(1));
            setMetaIdFiche(_rs.getLong(2));
            setMetaCodeObjet(_rs.getString(3));
            setMetaLibelleObjet(_rs.getString(4));
            setMetaHistorique(_rs.getString(5));
            setMetaDateArchivage(SqlDateConverter.fromDate(_rs.getDate(6)));
            setMetaListeReferences(_rs.getString(7));
            setMetaForum(_rs.getString(8));
            setMetaForumAno(_rs.getString(9));
            setMetaSaisieFront(_rs.getString(10));
            setMetaMailAnonyme(_rs.getString(11));
            setMetaNotificationMail(_rs.getString(12));
            setMetaInTree(_rs.getString(13));
            setMetaDocumentFichiergw(_rs.getString(14));
            setMetaRubriquesPublication(_rs.getString(15));
            setMetaNiveauApprobation(_rs.getString(16));
            setMetaLibelleFiche(_rs.getString(17));
            setMetaCode(_rs.getString(18));
            setMetaCodeRattachement(_rs.getString(19));
            setMetaCodeRubrique(_rs.getString(20));
            setMetaKeywords(_rs.getString(21));
            setMetaDescription(_rs.getString(22));
            setMetaDateCreation(SqlDateConverter.fromTimestamp(_rs.getTimestamp(23)));
            setMetaDateProposition(SqlDateConverter.fromTimestamp(_rs.getTimestamp(24)));
            setMetaDateValidation(SqlDateConverter.fromTimestamp(_rs.getTimestamp(25)));
            setMetaDateModification(SqlDateConverter.fromTimestamp(_rs.getTimestamp(26)));
            setMetaDateOperation(SqlDateConverter.fromTimestamp(_rs.getTimestamp(27)));
            setMetaCodeRedacteur(_rs.getString(28));
            setMetaCodeValidation(_rs.getString(29));
            setMetaLangue(_rs.getString(30));
            setMetaEtatObjet(_rs.getString(31));
            setMetaNbHits(_rs.getLong(32));
            setMetaSourceImport(_rs.getString(33));
            setMetaCodeRattachementAutres(_rs.getString(34));
            setMetaDiffusionPublicVise(_rs.getString(35));
            setMetaDiffusionModeRestriction(_rs.getString(36));
            setMetaDiffusionPublicViseRestriction(_rs.getString(37));
            setMetaDateMiseEnLigne(SqlDateConverter.fromTimestamp(_rs.getTimestamp(38)));
            setMetaDateSuppression(SqlDateConverter.fromDate(_rs.getDate(39)));
            setMetaDateRubriquage(SqlDateConverter.fromDate(_rs.getDate(40)));
            setMetaCodeRubriquage(_rs.getString(41));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        if (ctx.getDatas().get("optimizedSelect") != null) {
            selectNoCountOptimized(sqlSuffix);
        } else {
            try {
                if (sqlSuffix == null) {
                    sqlSuffix = "";
                }
                /* éxécution requete */
                _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
                _rs = _stmt.executeQuery();
            } catch (final SQLException exc) {
                throw new Exception("SELECT_FAILED " + exc);
            } finally {
                _stmt = null;
            }
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    private void selectNoCountOptimized(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String limit = "";
            if (ctx.getDatas().get("optimizedLimit") != null) {
                limit = " LIMIT 0," + (String) ctx.getDatas().get("optimizedLimit") + "0";
            }
            /* éxécution requete */
            _stmt = getConnection().prepareStatement(getSQLBaseOptimizedQuery() + sqlSuffix + limit);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select no count no distinct.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCountNoDistinct(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            /* éxécution requete */
            _stmt = getConnection().prepareStatement(getSQLBaseQueryNoDistinct() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT( DISTINCT ID_METATAG )   FROM " + qualifier + "METATAG T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getId() + aSeparator + getMetaIdFiche() + aSeparator + getMetaCodeObjet() + aSeparator + getMetaLibelleObjet() + aSeparator + getMetaHistorique() + aSeparator + getMetaDateArchivage() + aSeparator + getMetaListeReferences() + aSeparator + getMetaForum() + aSeparator + getMetaForumAno() + aSeparator + getMetaSaisieFront() + aSeparator + getMetaMailAnonyme() + aSeparator + getMetaNotificationMail() + aSeparator + getMetaInTree() + aSeparator + getMetaDocumentFichiergw() + aSeparator + getMetaRubriquesPublication() + aSeparator + getMetaNiveauApprobation() + aSeparator + getMetaLibelleFiche() + aSeparator + getMetaCode() + aSeparator + getMetaCodeRattachement() + aSeparator + getMetaCodeRubrique() + aSeparator + getMetaKeywords() + aSeparator + getMetaDescription() + aSeparator + getMetaDateCreation() + aSeparator + getMetaDateProposition() + aSeparator + getMetaDateValidation() + aSeparator + getMetaDateModification() + aSeparator + getMetaDateOperation() + aSeparator + getMetaCodeRedacteur() + aSeparator + getMetaCodeValidation() + aSeparator + getMetaLangue() + aSeparator + getMetaEtatObjet() + aSeparator + getMetaNbHits() + aSeparator + getMetaSourceImport() + aSeparator + getMetaCodeRattachementAutres() + aSeparator + getMetaDiffusionPublicVise() + aSeparator + getMetaDiffusionModeRestriction() + aSeparator + getMetaDiffusionPublicViseRestriction() + aSeparator + getMetaDateMiseEnLigne() + aSeparator + getMetaDateSuppression() + aSeparator + getMetaDateRubriquage() + aSeparator + getMetaCodeRubriquage()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "METATAG SET " + "       ID_METATAG = ?, " + "       META_ID_FICHE = ?, " + "       META_CODE_OBJET = ?, " + "       META_LIBELLE_OBJET = ?, " + "       META_HISTORIQUE = ?, " + "       META_DATE_ARCHIVAGE = ?, " + "       META_LISTE_REFERENCES = ?, " + "       META_FORUM = ?, " + "       META_FORUM_ANO = ?, " + "       META_SAISIE_FRONT = ?, " + "       META_MAIL_ANONYME = ?, " + "       META_NOTIFICATION_MAIL = ?, " + "       META_IN_TREE = ?, " + "       META_DOCUMENT_FICHIERGW = ?, " + "       META_RUBRIQUES_PUBLICATION = ?, " + "       META_NIVEAU_APPROBATION = ?, " + "       META_LIBELLE_FICHE = ?, " + "       META_CODE = ?, " + "       META_CODE_RATTACHEMENT = ?, " + "       META_CODE_RUBRIQUE = ?, " + "       META_META_KEYWORDS = ?, " + "       META_META_DESCRIPTION = ?, " + "       META_DATE_CREATION = ?, " + "       META_DATE_PROPOSITION = ?, " + "       META_DATE_VALIDATION = ?, " + "       META_DATE_MODIFICATION = ?, " + "       META_DATE_OPERATION = ?, " + "       META_CODE_REDACTEUR = ?, " + "       META_CODE_VALIDATION = ?, " + "       META_LANGUE = ?, " + "       META_ETAT_OBJET = ?, " + "       META_NB_HITS = ?, " + "       META_SOURCE_IMPORT = ?, " + "       META_CODE_RATTACHEMENT_AUTRES = ?, " + "       META_DIFFUSION_PUBLIC_VISE = ?, " + "       META_DIFFUSION_MODE_RESTRICTION = ?, " + "       META_DIFFUSION_PUBLIC_VISE_RESTRICTION = ?, " + "       META_DATE_MISE_EN_LIGNE = ?, " + "       META_DATE_SUPPRESSION = ?, " + "       META_DATE_RUBRIQUAGE = ?, " + "       META_CODE_RUBRIQUAGE = ? " + "         WHERE " + " ID_METATAG = ? ");
            // put parameters into statement
            _stmt.setObject(1, getId(), Types.BIGINT);
            _stmt.setObject(2, getMetaIdFiche(), Types.BIGINT);
            _stmt.setObject(3, getMetaCodeObjet(), Types.VARCHAR);
            _stmt.setObject(4, getMetaLibelleObjet(), Types.VARCHAR);
            _stmt.setObject(5, getMetaHistorique(), Types.LONGVARCHAR);
            _stmt.setObject(6, SqlDateConverter.toDate(getMetaDateArchivage()), Types.DATE);
            _stmt.setObject(7, getMetaListeReferences(), Types.LONGVARCHAR);
            _stmt.setObject(8, getMetaForum(), Types.VARCHAR);
            _stmt.setObject(9, getMetaForumAno(), Types.VARCHAR);
            _stmt.setObject(10, getMetaSaisieFront(), Types.VARCHAR);
            _stmt.setObject(11, getMetaMailAnonyme(), Types.VARCHAR);
            _stmt.setObject(12, getMetaNotificationMail(), Types.VARCHAR);
            _stmt.setObject(13, getMetaInTree(), Types.VARCHAR);
            _stmt.setObject(14, getMetaDocumentFichiergw(), Types.VARCHAR);
            _stmt.setObject(15, getMetaRubriquesPublication(), Types.LONGVARCHAR);
            _stmt.setObject(16, getMetaNiveauApprobation(), Types.VARCHAR);
            _stmt.setObject(17, getMetaLibelleFiche(), Types.VARCHAR);
            _stmt.setObject(18, getMetaCode(), Types.VARCHAR);
            _stmt.setObject(19, getMetaCodeRattachement(), Types.VARCHAR);
            _stmt.setObject(20, getMetaCodeRubrique(), Types.VARCHAR);
            _stmt.setObject(21, getMetaKeywords(), Types.LONGVARCHAR);
            _stmt.setObject(22, getMetaDescription(), Types.LONGVARCHAR);
            _stmt.setObject(23, SqlDateConverter.toTimestamp(getMetaDateCreation()), Types.TIMESTAMP);
            _stmt.setObject(24, SqlDateConverter.toTimestamp(getMetaDateProposition()), Types.TIMESTAMP);
            _stmt.setObject(25, SqlDateConverter.toTimestamp(getMetaDateValidation()), Types.TIMESTAMP);
            _stmt.setObject(26, SqlDateConverter.toTimestamp(getMetaDateModification()), Types.TIMESTAMP);
            _stmt.setObject(27, SqlDateConverter.toTimestamp(getMetaDateOperation()), Types.TIMESTAMP);
            _stmt.setObject(28, getMetaCodeRedacteur(), Types.VARCHAR);
            _stmt.setObject(29, getMetaCodeValidation(), Types.VARCHAR);
            _stmt.setObject(30, getMetaLangue(), Types.VARCHAR);
            _stmt.setObject(31, getMetaEtatObjet(), Types.VARCHAR);
            _stmt.setObject(32, getMetaNbHits(), Types.BIGINT);
            _stmt.setObject(33, getMetaSourceImport(), Types.VARCHAR);
            _stmt.setObject(34, getMetaCodeRattachementAutres(), Types.LONGVARCHAR);
            _stmt.setObject(35, getMetaDiffusionPublicVise(), Types.VARCHAR);
            _stmt.setObject(36, getMetaDiffusionModeRestriction(), Types.VARCHAR);
            _stmt.setObject(37, getMetaDiffusionPublicViseRestriction(), Types.VARCHAR);
            _stmt.setObject(38, SqlDateConverter.toTimestamp(getMetaDateMiseEnLigne()), Types.TIMESTAMP);
            _stmt.setObject(39, SqlDateConverter.toDate(getMetaDateSuppression()), Types.DATE);
            _stmt.setObject(40, SqlDateConverter.toDate(getMetaDateRubriquage()), Types.DATE);
            _stmt.setObject(41, getMetaCodeRubriquage(), Types.VARCHAR);
            _stmt.setObject(42, getId(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            _stmt.close();
            _stmt = null;
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
