package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.bean.RubriquepublicationBean;
// TODO: Auto-generated Javadoc

/**
 * The Class RubriquepublicationDB.
 * @deprecated les données sont maintenant dans {@link RubriquepublicationBean} et le DAO est les méthodes d'accès au données dans {@link com.univ.objetspartages.services.ServiceRubriquePublication}
 */
@Deprecated
public class RubriquepublicationDB extends RubriquepublicationBean {

    /**
     *
     */
    private static final long serialVersionUID = -3902618148975148091L;

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected transient OMContext ctx = null;

    /** The _stmt. */
    private transient java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private transient ResultSet _rs = null;

    /**
     * Instantiates a new rubriquepublication db.
     */
    public RubriquepublicationDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement(" INSERT INTO " + qualifier + "RUBRIQUEPUBLICATION (ID_RUBRIQUEPUBLICATION  ,   TYPE_FICHE_ORIG  ,   CODE_FICHE_ORIG  ,   LANGUE_FICHE_ORIG  ,   RUBRIQUE_DEST  ,   SOURCE_REQUETE  )              VALUES (?  ,?  ,?  ,?  ,?  ,?  ) ", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdRubriquepublication(), Types.BIGINT);
            _stmt.setObject(2, getTypeFicheOrig(), Types.VARCHAR);
            _stmt.setObject(3, getCodeFicheOrig(), Types.VARCHAR);
            _stmt.setObject(4, getLangueFicheOrig(), Types.VARCHAR);
            _stmt.setObject(5, getRubriqueDest(), Types.VARCHAR);
            _stmt.setObject(6, getSourceRequete(), Types.LONGVARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdRubriquepublication(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "RUBRIQUEPUBLICATION WHERE " + "             ID_RUBRIQUEPUBLICATION = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdRubriquepublication(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT " + "T1.ID_RUBRIQUEPUBLICATION ,  " + "T1.TYPE_FICHE_ORIG ,  " + "T1.CODE_FICHE_ORIG ,  " + "T1.LANGUE_FICHE_ORIG ,  " + "T1.RUBRIQUE_DEST ,  " + "T1.SOURCE_REQUETE " + "         FROM " + qualifier + "RUBRIQUEPUBLICATION T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_RUBRIQUEPUBLICATION," + "             TYPE_FICHE_ORIG," + "             CODE_FICHE_ORIG," + "             LANGUE_FICHE_ORIG," + "             RUBRIQUE_DEST," + "             SOURCE_REQUETE" + "         FROM " + qualifier + "RUBRIQUEPUBLICATION WHERE " + "             ID_RUBRIQUEPUBLICATION = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdRubriquepublication(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdRubriquepublication(new Long(_rs.getLong(1)));
            setTypeFicheOrig(_rs.getString(2));
            setCodeFicheOrig(_rs.getString(3));
            setLangueFicheOrig(_rs.getString(4));
            setRubriqueDest(_rs.getString(5));
            setSourceRequete(_rs.getString(6));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "RUBRIQUEPUBLICATION T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdRubriquepublication() + aSeparator + getTypeFicheOrig() + aSeparator + getCodeFicheOrig() + aSeparator + getLangueFicheOrig() + aSeparator + getRubriqueDest() + aSeparator + getSourceRequete()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "RUBRIQUEPUBLICATION SET " + "ID_RUBRIQUEPUBLICATION = ?, " + "TYPE_FICHE_ORIG = ?, " + "CODE_FICHE_ORIG = ?, " + "LANGUE_FICHE_ORIG = ?, " + "RUBRIQUE_DEST = ?, " + "SOURCE_REQUETE = ? " + "         WHERE " + "             ID_RUBRIQUEPUBLICATION = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdRubriquepublication(), Types.BIGINT);
            _stmt.setObject(2, getTypeFicheOrig(), Types.VARCHAR);
            _stmt.setObject(3, getCodeFicheOrig(), Types.VARCHAR);
            _stmt.setObject(4, getLangueFicheOrig(), Types.VARCHAR);
            _stmt.setObject(5, getRubriqueDest(), Types.VARCHAR);
            _stmt.setObject(6, getSourceRequete(), Types.LONGVARCHAR);
            _stmt.setObject(7, getIdRubriquepublication(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
