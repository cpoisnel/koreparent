package com.univ.objetspartages.sgbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
// TODO: Auto-generated Javadoc

/**
 * The Class ProfildsiDB.
 * @deprecated utilisez {@link com.univ.objetspartages.bean.ProfildsiBean} pour les données &
 * {@link com.univ.objetspartages.dao.impl.ProfildsiDAO} pour l'accès au données
 */
@Deprecated
public class ProfildsiDB {

    /** The qualifier. */
    protected String qualifier = null;

    /** The ctx. */
    protected OMContext ctx = null;

    /** The _stmt. */
    private java.sql.PreparedStatement _stmt = null;

    /** The _rs. */
    private ResultSet _rs = null;

    /** The id profildsi. */
    private Long idProfildsi = null;

    /** The code. */
    private String code = null;

    /** The libelle. */
    private String libelle = null;

    /** The code page accueil. */
    private String codeRubriqueAccueil = null;

    /** The code rattachement. */
    private String codeRattachement = null;

    /** The roles. */
    private String roles = null;

    /** The groupes. */
    private String groupes = null;

    /**
     * Instantiates a new profildsi db.
     */
    public ProfildsiDB() {}

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    public void add() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("INSERT INTO " + qualifier + "PROFILDSI (ID_PROFILDSI  ,   CODE  ,   LIBELLE  ,   CODE_RUBRIQUE_ACCUEIL  ,   CODE_RATTACHEMENT  ,   ROLES  ,   GROUPES  )              VALUES (?  ,?  ,?  ,?  ,?  ,?  ,?  )", Statement.RETURN_GENERATED_KEYS);
            _stmt.setObject(1, getIdProfildsi(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getLibelle(), Types.VARCHAR);
            _stmt.setObject(4, getCodeRubriqueAccueil(), Types.VARCHAR);
            _stmt.setObject(5, getCodeRattachement(), Types.VARCHAR);
            _stmt.setObject(6, getRoles(), Types.LONGVARCHAR);
            _stmt.setObject(7, getGroupes(), Types.LONGVARCHAR);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = _stmt.getGeneratedKeys();
            rs.next();
            setIdProfildsi(rs.getLong(1));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD add() " + exc);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    public void delete() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("DELETE FROM " + qualifier + "PROFILDSI WHERE " + "             ID_PROFILDSI = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdProfildsi(), Types.BIGINT);
            final int rowsAffected = _stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            }
            if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD delete() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    private Connection getConnection() {
        return ctx.getConnection();
    }

    /**
     * Gets the id profildsi.
     *
     * @return the id profildsi
     */
    public Long getIdProfildsi() {
        return idProfildsi;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Gets the code page accueil.
     *
     * @return the code page accueil
     */
    public String getCodeRubriqueAccueil() {
        return codeRubriqueAccueil;
    }

    /**
     * Gets the code rattachement.
     *
     * @return the code rattachement
     */
    public String getCodeRattachement() {
        return codeRattachement;
    }

    /**
     * Gets the roles.
     *
     * @return the roles
     */
    public String getRoles() {
        return roles;
    }

    /**
     * Gets the groupes.
     *
     * @return the groupes
     */
    public String getGroupes() {
        return groupes;
    }

    /**
     * Gets the sQL base query.
     *
     * @return the sQL base query
     */
    public String getSQLBaseQuery() {
        return "SELECT DISTINCT " + "T1.ID_PROFILDSI ,  " + "T1.CODE ,  " + "T1.LIBELLE ,  " + "T1.CODE_RUBRIQUE_ACCUEIL ,  " + "T1.CODE_RATTACHEMENT ,  " + "T1.ROLES ,  " + "T1.GROUPES " + "         FROM " + qualifier + "PROFILDSI T1 ";
    }

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (_rs.next()) {
                retrieveFromRS(_rs);
                res = true;
            } else {
                try {
                    _rs.close();
                } finally {
                    _rs = null;
                }
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem() " + e);
        }
        return res;
    }

    /**
     * Récupération d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void retrieve() throws Exception {
        ResultSet rs = null;
        try {
            _stmt = getConnection().prepareStatement("SELECT " + "             ID_PROFILDSI," + "             CODE," + "             LIBELLE," + "             CODE_RUBRIQUE_ACCUEIL," + "             CODE_RATTACHEMENT," + "             ROLES," + "             GROUPES" + "         FROM " + qualifier + "PROFILDSI WHERE " + "             ID_PROFILDSI = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdProfildsi(), Types.BIGINT);
            rs = _stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve  : METHOD_NO_RESULTS");
            }
            // get output from result set
            retrieveFromRS(rs);
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieve() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Retrieve from rs.
     *
     * @param _rs
     *            the _rs
     *
     * @throws Exception
     *             the exception
     */
    private void retrieveFromRS(final ResultSet _rs) throws Exception {
        try {
            // get output from result set
            setIdProfildsi(new Long(_rs.getLong(1)));
            setCode(_rs.getString(2));
            setLibelle(_rs.getString(3));
            setCodeRubriqueAccueil(_rs.getString(4));
            setCodeRattachement(_rs.getString(5));
            setRoles(_rs.getString(6));
            setGroupes(_rs.getString(7));
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS() " + exc);
        }
    }

    /**
     * cette méthode optimise l'exécution de la requete (pas de count).
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @throws Exception
     *             the exception
     */
    public void selectNoCount(String sqlSuffix) throws Exception {
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            /* éxécution requete */
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
    }

    /**
     * Select.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(String sqlSuffix) throws Exception {
        int count = 0;
        try {
            if (sqlSuffix == null) {
                sqlSuffix = "";
            }
            String query = "SELECT COUNT(*)   FROM " + qualifier + "PROFILDSI T1 " + sqlSuffix;
            /* récupération nombre de lignes */
            _stmt = getConnection().prepareStatement(query);
            _rs = _stmt.executeQuery(query);
            _rs.next();
            count = _rs.getInt(1);
            /* éxécution requete */
            query = getSQLBaseQuery() + sqlSuffix;
            _stmt = getConnection().prepareStatement(getSQLBaseQuery() + sqlSuffix);
            _rs = _stmt.executeQuery();
        } catch (final SQLException exc) {
            throw new Exception("SELECT_FAILED " + exc);
        } finally {
            _stmt = null;
        }
        return count;
    }

    /**
     * Sets the id profildsi.
     *
     * @param _idProfildsi
     *            the new id profildsi
     */
    public void setIdProfildsi(final Long _idProfildsi) {
        idProfildsi = _idProfildsi;
    }

    /**
     * Sets the code.
     *
     * @param _code
     *            the new code
     */
    public void setCode(final String _code) {
        code = _code;
    }

    /**
     * Sets the libelle.
     *
     * @param _libelle
     *            the new libelle
     */
    public void setLibelle(final String _libelle) {
        libelle = _libelle;
    }

    /**
     * Sets the code page accueil.
     *
     * @param _codePageAccueil
     *            the new code page accueil
     */
    public void setCodeRubriqueAccueil(final String _codePageAccueil) {
        codeRubriqueAccueil = _codePageAccueil;
    }

    /**
     * Sets the code rattachement.
     *
     * @param _codeRattachement
     *            the new code rattachement
     */
    public void setCodeRattachement(final String _codeRattachement) {
        codeRattachement = _codeRattachement;
    }

    /**
     * Sets the roles.
     *
     * @param _roles
     *            the new roles
     */
    public void setRoles(final String _roles) {
        roles = _roles;
    }

    /**
     * Sets the groupes.
     *
     * @param _groupes
     *            the new groupes
     */
    public void setGroupes(final String _groupes) {
        groupes = _groupes;
    }

    /**
     * Sets the param.
     *
     * @param _ctx
     *            the _ctx
     * @param _qualifier
     *            the _qualifier
     */
    public void setParam(final OMContext _ctx, final String _qualifier) {
        ctx = _ctx;
        qualifier = "";
        if (_qualifier.equals("") == false) {
            if (_qualifier.substring(_qualifier.length() - 1).equals(".") == false) {
                qualifier = _qualifier + ".";
            } else {
                qualifier = _qualifier;
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return toString(".");
    }
    //----------------------------------------------------------------
    // Display methods
    //----------------------------------------------------------------

    /**
     * To string.
     *
     * @param aSeparator
     *            the a separator
     *
     * @return the string
     */
    public String toString(final String aSeparator) {
        return (String.valueOf("" + aSeparator + getIdProfildsi() + aSeparator + getCode() + aSeparator + getLibelle() + aSeparator + getCodeRubriqueAccueil() + aSeparator + getCodeRattachement() + aSeparator + getRoles() + aSeparator + getGroupes()));
    }

    /**
     * Mise à jour d'une ligne de la base de données.
     *
     * @throws Exception
     *             the exception
     */
    public void update() throws Exception {
        try {
            _stmt = getConnection().prepareStatement("UPDATE " + qualifier + "PROFILDSI SET " + "ID_PROFILDSI = ?, " + "CODE = ?, " + "LIBELLE = ?, " + "CODE_RUBRIQUE_ACCUEIL = ?, " + "CODE_RATTACHEMENT = ?, " + "ROLES = ?, " + "GROUPES = ? " + "         WHERE " + "             ID_PROFILDSI = ? ");
            // put parameters into statement
            _stmt.setObject(1, getIdProfildsi(), Types.BIGINT);
            _stmt.setObject(2, getCode(), Types.VARCHAR);
            _stmt.setObject(3, getLibelle(), Types.VARCHAR);
            _stmt.setObject(4, getCodeRubriqueAccueil(), Types.VARCHAR);
            _stmt.setObject(5, getCodeRattachement(), Types.VARCHAR);
            _stmt.setObject(6, getRoles(), Types.LONGVARCHAR);
            _stmt.setObject(7, getGroupes(), Types.LONGVARCHAR);
            _stmt.setObject(8, getIdProfildsi(), Types.BIGINT);
            _stmt.executeUpdate();
        } catch (final SQLException exc) {
            throw new Exception("ERROR_IN_METHOD update() " + exc);
        } finally {
            if (_stmt != null) {
                _stmt.close();
                _stmt = null;
            }
        }
    }

    /**
     * Sets the ctx.
     *
     * @param _ctx
     *            the new ctx
     */
    public void setCtx(final OMContext _ctx) {
        setParam(_ctx, "");
    }
}
