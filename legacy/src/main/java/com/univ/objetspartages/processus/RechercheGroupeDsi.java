package com.univ.objetspartages.processus;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.LabelUtils;

/**
 * processus saisie Utilisateur.
 * @deprecated ce processus sert uniquement pour de la recherche dans l'arbre des groupes 5.1. Si vous avez des fronts 5.1 il faut donc
 * rajouter le package legacy et webapp-legacy pour qu'il puisse fonctionner
 */
@Deprecated
public class RechercheGroupeDsi extends ProcessusBean {

    /** The Constant ECRAN_RECHERCHE. */
    private static final String ECRAN_RECHERCHE = "RECHERCHE";

    /** The Constant ECRAN_LISTE. */
    private static final String ECRAN_LISTE = "LISTE";

    private static final Logger LOG = LoggerFactory.getLogger(RechercheGroupeDsi.class);
    //    JSS 20050510 : groupes dynamiques

    /** The autorisations. */
    AutorisationBean autorisations = null;

    /**
     * processus recherche utilisateur.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public RechercheGroupeDsi(final InfoBean ciu) {
        super(ciu);
    }

    /**
     * Affichage de la liste des utilisateurs.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerLISTE() throws Exception {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        int i = 0;
        ecranLogique = ECRAN_LISTE;
        List<GroupeDsiBean> results = serviceGroupeDsi.getByCodeTypeLabelStructureAndCache(StringUtils.EMPTY, infoBean.getString("TYPE"), infoBean.getString("LIBELLE"), infoBean.getString("CODE_STRUCTURE"), StringUtils.EMPTY);
        String sPermission = StringUtils.defaultString(infoBean.getString("PERMISSION"));
        for (GroupeDsiBean groupedsi : results) {
            boolean insererGroupe = true;
            // Test permission
            if (sPermission.length() > 0) {
                if (!autorisations.possedePermissionPartielleSurPerimetre(new PermissionBean(infoBean.getString("PERMISSION")), new Perimetre("", "", "", groupedsi.getCode(), ""))) {
                    insererGroupe = false;
                }
            }
            if (insererGroupe) {
                infoBean.set("CODE#" + i, groupedsi.getCode());
                infoBean.set("LIBELLE#" + i, groupedsi.getLibelle());
                infoBean.set("LIBELLE_STRUCTURE#" + i, serviceStructure.getDisplayableLabel(groupedsi.getCodeStructure(), "0"));
                i++;
            }
        }
        if (i > 0) {
            infoBean.set("LISTE_NB_ITEMS", i);
        } else {
            infoBean.addMessageErreur("Aucun groupe ne répond aux critères");
        }
    }

    /**
     * Affichage de l'écran des critères de recherche d'un utilisateur.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerRECHERCHE() throws Exception {
        ecranLogique = ECRAN_RECHERCHE;
        infoBean.set("LISTE_TYPES", LabelUtils.getLabelCombo("11", LangueUtil.getDefaultLocale()));
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            try {
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                etat = EN_COURS;
                /* Entrée par lien hyper-texte */
                if (ecranLogique == null) {
                    if ("RECHERCHER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
                        preparerRECHERCHE();
                    }
                } else {
                    /* Entrée par formulaire */
                    if (ECRAN_RECHERCHE.equals(ecranLogique)) {
                        traiterRECHERCHE();
                    }
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOG.error("erreur lors du traitement du processus", e);
                infoBean.addMessageErreur(e.toString());
            }
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterRECHERCHE() throws Exception {
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            preparerLISTE();
        }
        if (action.equals(InfoBean.ACTION_ANNULER)) {
            ecranLogique = "FIN_TOOLBOX";
        }
    }
}
