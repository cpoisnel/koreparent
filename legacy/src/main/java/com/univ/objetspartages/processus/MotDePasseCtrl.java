package com.univ.objetspartages.processus;

import java.util.HashMap;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceUser;

/**
 * The Class MotDePasseCtrl.
 *
 * @author pguiomar
 *
 *         Gestion du changement de mot de passe pour un utilisateur identifie.
 */
@Deprecated
public class MotDePasseCtrl {

    /**
     * Valeur par défaut si rien n'est spécifié dans la conf
     */
    public static final int LONGUEUR_MOT_DE_PASSE_DEFAUT = 64;

    private final ServiceUser serviceUser;

    public MotDePasseCtrl() {
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Retourne la longeur du champ mot de passe a utiliser dans les formulaires
     *
     * @return la valeur de la propriété ou {@link MotDePasseCtrl#LONGUEUR_MOT_DE_PASSE_DEFAUT} si non défini
     * @deprecated Utilisez {@link ServiceUser#getLongueurChampMotDePasse()}
     */
    @Deprecated
    public static int getLongueurChampMotDePasse() {
        return ServiceUser.getLongueurChampMotDePasse();
    }

    /**
     * 1er phase de la gestion du mot de passe (regeneration) recuperation des donnees de l'utilisateur, enregistrement des donnees de l'utilsateur dans un objet provisoire
     * (ChangementMotDePasse), envoi d'email vers l'utilisateur avec un lien HTTPS de retour vers ce ctrl pour traiter la suite de la gestion du mot de passe.
     *
     * @param infoBean
     *            l'infobean pour avoir le champ email et login
     * @param ctx
     *            the ctx
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceUser#requestNewPass(InfoBean, OMContext)}
     */
    @Deprecated
    public void demande(final InfoBean infoBean, final OMContext ctx) throws Exception {
        serviceUser.requestNewPass(infoBean);
    }

    /**
     * 2e phase de la generation du mot de passe. recuperation des donnees a partir de l'objet temporaire (Changementmotpasse) grace a ID (timestamp), generation du mot de passe,
     * enregistrement dans la base du nouveau mot de passe avec eventuellement le nouvel email
     *
     * @param id
     *            the id
     * @param ctx
     *            the ctx
     *
     * @return le mot de passe
     *
     * @throws Exception
     *             the exception
     * @deprecated {@link ServiceUser#handlePassRequest(String)}
     */
    @Deprecated
    public HashMap<String, String> fabrication(final String id, final OMContext ctx) throws Exception {
        final HashMap<String, String> results = new HashMap<>();
        results.putAll(serviceUser.handlePassRequest(id));
        return results;
    }//fabrication

}
