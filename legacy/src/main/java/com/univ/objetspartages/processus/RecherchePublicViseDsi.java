package com.univ.objetspartages.processus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.services.ServiceProfildsi;
// TODO: Auto-generated Javadoc

/**
 * processus saisie Utilisateur.
 * @deprecated Ce processus a été créé en 2005, mais il n'y a pas de référence à ce dernier depuis avant la 5.0.
 */
@Deprecated
public class RecherchePublicViseDsi extends ProcessusBean {

    /** The Constant ECRAN_RECHERCHE. */
    private static final String ECRAN_RECHERCHE = "RECHERCHE";

    private static Logger LOG = LoggerFactory.getLogger(RecherchePublicViseDsi.class);



    /**
     * processus recherche utilisateur.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public RecherchePublicViseDsi(final InfoBean ciu) {
        super(ciu);
    }

    /**
     * Affichage de l'écran des critères de recherche d'un utilisateur.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerRECHERCHE() throws Exception {
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        ecranLogique = ECRAN_RECHERCHE;
        infoBean.set("LISTE_PROFILS_DSI", serviceProfildsi.getDisplayableProfiles());
        // JSS 20040419 : Filtrage de l'arbre des groupes
        if (infoBean.get("PERMISSION") != null) {
            final PermissionBean permission = new PermissionBean(infoBean.getString("PERMISSION"));
            // Positionnement variables pour filtre
            infoBean.set("GRS_FILTRE_ARBRE_GROUPE", "1");
            infoBean.set("GRS_PERMISSION_TYPE", permission.getType());
            infoBean.set("GRS_PERMISSION_OBJET", permission.getObjet());
            infoBean.set("GRS_PERMISSION_ACTION", permission.getAction());
        }
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        final Object o = getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (o == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            try {
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                etat = EN_COURS;
                /* Entrée par lien hyper-texte */
                if (ecranLogique == null) {
                    if ("RECHERCHER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
                        preparerRECHERCHE();
                    }
                } else {
                    /* Entrée par formulaire */
                    if (ECRAN_RECHERCHE.equals(ecranLogique)) {
                        traiterRECHERCHE();
                    }
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOG.error("erreur de traitement sur le processus", e);
                infoBean.addMessageErreur(e.toString());
            }
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterRECHERCHE() throws Exception {
        if (action.equals(InfoBean.ACTION_ANNULER)) {
            ecranLogique = "FIN_TOOLBOX";
        }
    }
}
