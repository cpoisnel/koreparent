package com.kportal.extension.om;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.Formateur;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.context.OverridedContextBean;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.module.IModule;
import com.kportal.extension.sgbd.ModuleDB;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseLimit;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.LimitHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * Classe representant un objet module.
 * @deprecated cette classe est remplacé par {@link com.kportal.extension.service.ServiceModule} pour les méthodes utilitaires et {@link com.kportal.extension.bean.ModuleBean} pour les données.
 */
@Deprecated
public class Module extends ModuleDB implements Cloneable {

    private static final String NB_CE_MODULE_SURCHARGE_LE_MODULE = "BO_EXTENSION_MODULE_SURCHARGE_LE_MODULE";

    private static final String DE_L_EXTENSION = "BO_EXTENSION_DE_L_EXTENSION";

    private static final String NB_CE_MODULE_EST_SURCHARGE_PAR_LE_MODULE = "BO_EXTENSION_MODULE_SURCHARGE_PAR_LE_MODULE";

    /**
     *
     */
    private static final long serialVersionUID = -5992754650636410517L;

    /**
     * Initialise l'objet metier.
     */
    public void init() {
        setId(0l);
        setIdBean(StringUtils.EMPTY);
        setIdExtension(0l);
        setLibelle(StringUtils.EMPTY);
        setEtat(IModule.ETAT_ACTIF);
        setType(IModule.TYPE_NON_PARAMETRABLE_NON_AFFICHABLE);
    }

    public String getLibelleAffichable(final String key) {
        return ExtensionHelper.getMessage(getIdBeanExtension(), key);
    }

    /**
     * Selection d'une instance de l'objet Module a partir de l'ensemble des criteres.
     */
    public int select(final String idBean, final Long idExtension, final String libelle, final Date dateCreation, final Date dateModification, final Integer etat, final Integer[] type, final String nombre) throws Exception {
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseWhere where = new ClauseWhere();
        // Traitement du parametre idBean
        if (StringUtils.isNotEmpty(idBean)) {
            where.and(ConditionHelper.egalVarchar("T1.ID_BEAN", idBean));
        }
        // Traitement du parametre idExtension
        if (idExtension != null) // TODO : type de donnees non pris en compte, controler le parametre
        {
            where.and(ConditionHelper.egal("T1.ID_EXTENSION", idExtension, TypeOperande.LONG));
        }
        // Traitement du parametre libelle
        if (StringUtils.isNotEmpty(libelle)) {
            where.and(ConditionHelper.likePourValeursMultiple("T1.LIBELLE", libelle));
        }
        // Traitement du parametre dateCreation
        if (Formateur.estSaisie(dateCreation)) {
            where.and(ConditionHelper.critereDateDebut("T1.DATE_CREATION", dateCreation));
        }
        // Traitement du parametre dateModification
        if (Formateur.estSaisie(dateModification)) {
            where.and(ConditionHelper.critereDateDebut("T1.DATE_MODIFICATION", dateModification));
        }
        // Traitement du parametre etat
        if (etat != null) {
            where.and(ConditionHelper.egal("T1.ETAT", etat, TypeOperande.INTEGER));
        }
        if (type != null) {
            final ConditionList conditionsSurTypes = new ConditionList();
            for (final Integer integer : type) {
                conditionsSurTypes.or(ConditionHelper.egal("T1.TYPE", integer, TypeOperande.INTEGER));
            }
            where.and(conditionsSurTypes);
        }
        requeteSelect.where(where);
        final ClauseOrderBy orderBy = new ClauseOrderBy();
        orderBy.orderBy("T1.LIBELLE", SensDeTri.ASC);
        requeteSelect.orderBy(orderBy);
        final ClauseLimit limite = LimitHelper.ajouterCriteresLimitesEtOptimisation(ctx, nombre);
        requeteSelect.limit(limite);
        return select(requeteSelect.formaterRequete());
    }

    public int selectByExtension(final Long idExtension, final Integer etat, final Integer[] type) throws Exception {
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseWhere where = new ClauseWhere();
        // Traitement du parametre idExtension
        if (idExtension != null) // TODO : type de donnees non pris en compte, controler le parametre
        {
            where.and(ConditionHelper.egal("T1.ID_EXTENSION", idExtension, TypeOperande.LONG));
        }
        if (etat != null) {
            where.and(ConditionHelper.egal("T1.ETAT", etat, TypeOperande.INTEGER));
        }
        if (type != null) {
            final ConditionList conditionsSurTypes = new ConditionList();
            for (final Integer integer : type) {
                conditionsSurTypes.or(ConditionHelper.egal("T1.TYPE", integer, TypeOperande.INTEGER));
            }
            where.and(conditionsSurTypes);
        }
        requeteSelect.where(where);
        final ClauseOrderBy orderBy = new ClauseOrderBy();
        orderBy.orderBy("T1.LIBELLE", SensDeTri.ASC);
        requeteSelect.orderBy(orderBy);
        return select(requeteSelect.formaterRequete());
    }

    @Override
    public Module clone() throws CloneNotSupportedException {
        return (Module) super.clone();
    }

    public Module setConfig(Extension extension) throws Exception {
        if (extension == null) {
            extension = getExtension();
        }
        final IModule module = (IModule) ApplicationContextManager.getBean(extension.getIdBean(), getIdBean());
        if (module != null) {
            String description = "";
            // si c'est un bean qui surcharge
            if (module instanceof OverridedContextBean) {
                // on récupére le bean surchargé
                IModule module2 = (IModule) ApplicationContextManager.getBean(extension.getIdBean(), getIdBean(), false);
                if (module2 != null) {
                    // si ce n'est pas le même
                    if (!module2.equals(module)) {
                        description = module2.getDescription();
                        description += MessageHelper.getCoreMessage(NB_CE_MODULE_EST_SURCHARGE_PAR_LE_MODULE) + MessageHelper.getMessage(module.getIdExtension(), module.getLibelle()) + MessageHelper.getCoreMessage(DE_L_EXTENSION) + MessageHelper.getMessage(module.getIdExtension(), module.getLibelleExtension()) + "\"";
                    }
                    // sinon on cherche le bean surchargé
                    else {
                        description = module.getDescription();
                        module2 = (IModule) ApplicationContextManager.getBean(((OverridedContextBean) module).getIdExtensionToOverride(), ((OverridedContextBean) module).getIdBeanToOverride(), false);
                        if (module2 != null) {
                            description += MessageHelper.getCoreMessage(NB_CE_MODULE_SURCHARGE_LE_MODULE) + MessageHelper.getMessage(((OverridedContextBean) module).getIdExtensionToOverride(), module2.getLibelle()) + MessageHelper.getCoreMessage(DE_L_EXTENSION) + MessageHelper.getMessage(((OverridedContextBean) module).getIdExtensionToOverride(), module2.getLibelleExtension()) + "\"";
                        }
                    }
                }
            }
            setIdBeanExtension(extension.getIdBean());
            setDescription(description);
            setLibelleExtension(extension.getLibelle());
        }
        return this;
    }

    public Extension getExtension() throws Exception {
        final Extension extension = new Extension();
        extension.init();
        extension.setCtx(ctx);
        extension.setIdExtension(getIdExtension());
        extension.retrieve();
        return extension;
    }
}
