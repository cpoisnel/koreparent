package com.kportal.extension.om;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.Formateur;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;
import com.kportal.extension.exception.NoSuchExtensionException;
import com.kportal.extension.sgbd.ExtensionDB;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseLimit;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.LimitHelper;
import com.univ.utils.sql.criterespecifique.OrderByHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * Classe representant un objet extension.
 * @deprecated les données des services sont maintenant dans {@link com.kportal.extension.bean.ExtensionBean} pour accéder à la base, utiliser {@link com.kportal.extension.service.ServiceExtension}
 */
@Deprecated
public class Extension extends ExtensionDB implements Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -996480676318427303L;

    /**
     * Initialise l'objet metier.
     */
    public void init() {
        setIdExtension((long) 0);
        setIdBean("");
        setLibelle("");
        setEtat(IExtension.ETAT_ACTIF);
        setType(IExtension.TYPE_PARAMETRABLE);
        setTables("");
    }

    public String getLibelleAffichable(final String key) {
        return ExtensionHelper.getMessage(getIdBean(), key);
    }

    /**
     * Selection d'une instance de l'objet Extension a partir de l'ensemble des criteres.
     */
    public int select(final String idBean, final String libelle, final Date dateCreation, final Date dateModification, final Integer etat, final Integer[] type, final String nombre, final String order) throws Exception {
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseWhere where = new ClauseWhere();
        // Traitement du parametre idBean
        if (StringUtils.isNotEmpty(idBean)) {
            where.and(ConditionHelper.egalVarchar("T1.ID_BEAN", idBean));
        }
        // Traitement du parametre libelle
        if (StringUtils.isNotEmpty(libelle)) {
            where.and(ConditionHelper.likePourValeursMultiple("T1.LIBELLE", libelle));
        }
        // Traitement du parametre dateCreation
        if (Formateur.estSaisie(dateCreation)) {
            where.and(ConditionHelper.critereDateDebut("T1.DATE_CREATION", dateCreation));
        }
        // Traitement du parametre dateModification
        if (Formateur.estSaisie(dateModification)) {
            where.and(ConditionHelper.critereDateDebut("T1.DATE_MODIFICATION", dateModification));
        }
        // Traitement du parametre etat
        if (etat != null) {
            where.and(ConditionHelper.egal("T1.ETAT", etat, TypeOperande.INTEGER));
        }
        // Traitement du parametre type
        if (type != null) {
            final ConditionList conditionsSurTypes = new ConditionList();
            for (final Integer integer : type) {
                conditionsSurTypes.or(ConditionHelper.egal("T1.TYPE", integer, TypeOperande.INTEGER));
            }
            where.and(conditionsSurTypes);
        }
        requeteSelect.where(where);
        ClauseOrderBy orderBy = new ClauseOrderBy();
        if (StringUtils.isNotEmpty(order)) {
            orderBy = OrderByHelper.reconstruireClauseOrderBy(order);
        } else {
            orderBy.orderBy("T1.LIBELLE", SensDeTri.ASC);
        }
        requeteSelect.orderBy(orderBy);
        final ClauseLimit limite = LimitHelper.ajouterCriteresLimitesEtOptimisation(ctx, nombre);
        requeteSelect.limit(limite);
        return select(requeteSelect.formaterRequete());
    }

    @Override
    public Extension clone() throws CloneNotSupportedException {
        return (Extension) super.clone();
    }

    public Extension setConfig() throws NoSuchExtensionException {
        final IExtension extension = getExtension();
        if (extension == null) {
            throw new NoSuchExtensionException(getIdBean());
        }
        setAuteur(extension.getAuteur());
        setLibelle(extension.getLibelle());
        setDescription(extension.getDescription());
        if (StringUtils.isNotBlank(extension.getLogo())) {
            setLogo(extension.getRelativePath() + extension.getLogo());
        }
        setUrl(extension.getUrl());
        setCoreVersion(extension.getCoreVersion());
        return this;
    }

    public IExtension getExtension() {
        return (IExtension) ApplicationContextManager.getBean(getIdBean(), getIdBean());
    }
}
