package com.kportal.extension.module.plugin;

import com.kportal.extension.module.IModule;

/**
 * @deprecated cette interface n'apporte aucun interet et ne fait que rajouter de la confusion.
 * Il faut utiliser {@link IModule} ou une de ses sous classes.
 */
@Deprecated
public interface IPlugin extends IModule {}
