package com.kportal.extension.service;

import java.sql.PreparedStatement;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.utils.ContexteDao;

/**
 * @deprecated cette classe est remplacé par le service {@link ServiceModule}
 */
@Deprecated
public class EtatModuleHelper {

    private static final Logger LOG = LoggerFactory.getLogger(EtatModuleHelper.class);

    /**
     * @deprecated cette méthode est remplacé par {@link ServiceModule#updateStateByExtensionId(int, Long)}
     * @param etatARestaurer
     * @param idExtension
     */
    @Deprecated
    public static void setEtatDB(final int etatARestaurer, final Long idExtension) {
        PreparedStatement statement;
        try (ContexteDao ctx = new ContexteDao()) {
            String requete = "UPDATE MODULE SET ETAT = ?, DATE_MODIFICATION = ? ";
            if (idExtension != null) {
                requete += "WHERE ID_EXTENSION = ? ";
            }
            statement = ctx.getConnection().prepareStatement(requete);
            statement.setInt(1, etatARestaurer);
            statement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            if (idExtension != null) {
                statement.setLong(3, idExtension);
            }
            statement.execute();
            statement.close();
        } catch (final Exception e) {
            LOG.error("erreur lors de la requête sql", e);
        }
    }
}
