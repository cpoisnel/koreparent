package com.kportal.extension;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * @deprecated cette classe abstraite n'a jamais été utilisée et ne rajoute que de la confusion dans l'application.
 */
@Deprecated
public abstract class LibExtensionImpl extends DefaultExtensionImpl implements Observer{

    protected List<String> extensionIds;

    public void init() {
        ExtensionHelper.getExtensionManager().addObserver(this);
    }

    @Override
    public void setType(int type) {
        if (type!=IExtension.TYPE_PARAMETRABLE){
            super.setType(type);
        }else{
            super.setType(IExtension.TYPE_NON_PARAMETRABLE_AFFICHABLE);
        }
    }

    @Override
    public int getType() {
        if (type!=IExtension.TYPE_PARAMETRABLE){
            return super.getType();
        }else{
            return IExtension.TYPE_NON_PARAMETRABLE_AFFICHABLE;
        }
    }

    public List<String> getExtensionIds() {
        return extensionIds;
    }

    public void setExtensionIds(List<String> extensionIds) {
        this.extensionIds = extensionIds;
    }

    @Override
    public abstract void update(Observable o, Object arg);
}

