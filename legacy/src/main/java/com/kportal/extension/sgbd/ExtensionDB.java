package com.kportal.extension.sgbd;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.kportal.extension.bean.ExtensionBean;

/**
 * Classe d'acces aux donnees pour extension.
 * @deprecated les données des services sont maintenant dans {@link com.kportal.extension.bean.ExtensionBean} pour accéder à la base, utiliser {@link com.kportal.extension.service.ServiceExtension}
 */
@Deprecated
public class ExtensionDB extends ExtensionBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8754735340624208482L;

    protected transient OMContext ctx = null;

    protected transient String qualifier = null;

    private transient ResultSet rs = null;

    @Override
    public String toString() {
        return toString(".");
    }

    public String toString(final String aSeparator) {
        return "" + getIdExtension() + aSeparator + getIdBean() + aSeparator + getLibelle() + aSeparator + getDateCreation() + aSeparator + getDateModification() + aSeparator + getEtat() + aSeparator + getType() + aSeparator + getTables() + aSeparator + getVersion();
    }

    public void setCtx(final OMContext _ctx) {
        ctx = _ctx;
    }

    public void setQualifier(final String qualifier) {
        this.qualifier = qualifier;
    }

    private Connection getConnection() {
        return ctx.getConnection();
    }

    public void add() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("insert into EXTENSION (ID_EXTENSION , ID_BEAN , LIBELLE , DATE_CREATION , DATE_MODIFICATION , ETAT , TYPE , TABLES ,VERSION) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setObject(1, getIdExtension(), Types.BIGINT);
            stmt.setObject(2, getIdBean(), Types.VARCHAR);
            stmt.setObject(3, getLibelle(), Types.VARCHAR);
            stmt.setObject(4, getDateCreation(), Types.TIMESTAMP);
            stmt.setObject(5, getDateModification(), Types.TIMESTAMP);
            stmt.setObject(6, getEtat(), Types.INTEGER);
            stmt.setObject(7, getType(), Types.INTEGER);
            stmt.setObject(8, getTables(), Types.LONGVARCHAR);
            stmt.setObject(9, getVersion(), Types.VARCHAR);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD add()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void delete() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("delete from EXTENSION " + "where ID_EXTENSION = ?");
            // put parameters into statement
            stmt.setObject(1, getIdExtension(), Types.BIGINT);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD delete()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public String getSQLBaseQuery() {
        return "select distinct " + "T1.ID_EXTENSION, " + "T1.ID_BEAN, " + "T1.LIBELLE, " + "T1.DATE_CREATION, " + "T1.DATE_MODIFICATION, " + "T1.ETAT, " + "T1.TYPE, " + "T1.TABLES, " + "T1.VERSION " + "from " + (qualifier == null ? "" : qualifier) + "EXTENSION T1 ";
    }

    /**
     * Renvoie l'element suivant du ResultSet.
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                retrieveFromRS();
                res = true;
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem()", e);
        }
        return res;
    }

    /**
     * Recuperation d'une ligne de la base de donnees.
     */
    public void retrieve() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("select " + "T1.ID_EXTENSION, " + "T1.ID_BEAN, " + "T1.LIBELLE, " + "T1.DATE_CREATION, " + "T1.DATE_MODIFICATION, " + "T1.ETAT, " + "T1.TYPE, " + "T1.TABLES, " + "T1.VERSION " + "from EXTENSION T1 " + "where T1.ID_EXTENSION = ?");
            stmt.setObject(1, getIdExtension(), Types.BIGINT);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve : METHOD_NO_RESULTS");
            }
            retrieveFromRS();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieve()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    private void retrieveFromRS() throws Exception {
        try {
            // get output from result set
            setIdExtension(rs.getLong(1));
            setIdBean(rs.getString(2));
            setLibelle(rs.getString(3));
            setDateCreation(SqlDateConverter.fromTimestamp(rs.getTimestamp(4)));
            setDateModification(SqlDateConverter.fromTimestamp(rs.getTimestamp(5)));
            setEtat(rs.getInt(6));
            setType(rs.getInt(7));
            setTables(rs.getString(8));
            setVersion(rs.getString(9));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS()", e);
        }
    }

    /**
     * Cette methode optimise l'execution de la requete (pas de count)
     *
     * @param sqlSuffix
     * @throws Exception
     */
    public void selectNoCount(final String sqlSuffix) throws Exception {
        PreparedStatement stmt = null;
        try {
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
    }

    public int select(final String sqlSuffix) throws Exception {
        int count = -1;
        PreparedStatement stmt = null;
        try {
            // execution requete
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
            rs.last();
            count = rs.getRow();
            rs.beforeFirst();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
        return count;
    }

    /**
     * Mise a jour d'une ligne de la base de donnees.
     */
    public void update() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("update EXTENSION set " + "ID_EXTENSION = ?, " + "ID_BEAN = ?, " + "LIBELLE = ?, " + "DATE_CREATION = ?, " + "DATE_MODIFICATION = ?, " + "ETAT = ?, " + "TYPE = ?, " + "TABLES = ?, " + "VERSION = ? " + "where ID_EXTENSION = ?");
            // put parameters into statement
            stmt.setObject(1, getIdExtension(), Types.BIGINT);
            stmt.setObject(2, getIdBean(), Types.VARCHAR);
            stmt.setObject(3, getLibelle(), Types.VARCHAR);
            stmt.setObject(4, getDateCreation(), Types.TIMESTAMP);
            stmt.setObject(5, getDateModification(), Types.TIMESTAMP);
            stmt.setObject(6, getEtat(), Types.INTEGER);
            stmt.setObject(7, getType(), Types.INTEGER);
            stmt.setObject(8, getTables(), Types.LONGVARCHAR);
            stmt.setObject(9, getVersion(), Types.VARCHAR);
            stmt.setObject(10, getIdExtension(), Types.BIGINT);
            stmt.executeUpdate();
            stmt.close();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD update()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
}
