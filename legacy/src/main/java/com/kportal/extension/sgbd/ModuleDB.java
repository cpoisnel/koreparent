package com.kportal.extension.sgbd;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.kportal.extension.bean.ModuleBean;

/**
 * Classe d'acces aux donnees pour module.
 * @deprecated cette classe est remplacé par {@link com.kportal.extension.dao.ModuleDAO} pour l'accès au données et {@link ModuleBean} pour le bean.
 */
@Deprecated
public class ModuleDB extends ModuleBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5907081374052876657L;

    protected transient OMContext ctx = null;

    protected transient String qualifier = null;

    private transient ResultSet rs = null;

    @Override
    public String toString() {
        return toString(".");
    }

    public String toString(final String aSeparator) {
        final String s = "" + getId() + aSeparator + getIdBean() + aSeparator + getIdExtension() + aSeparator + getLibelle() + aSeparator + getDateCreation() + aSeparator + getDateModification() + aSeparator + getEtat() + aSeparator + getType();
        return s;
    }

    public void setCtx(final OMContext _ctx) {
        ctx = _ctx;
    }

    public void setQualifier(final String qualifier) {
        this.qualifier = qualifier;
    }

    private Connection getConnection() {
        return ctx.getConnection();
    }

    public void add() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("insert into MODULE (ID_MODULE , ID_BEAN , ID_EXTENSION , LIBELLE , DATE_CREATION , DATE_MODIFICATION , ETAT , TYPE ) values (?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setObject(1, getId(), Types.BIGINT);
            stmt.setObject(2, getIdBean(), Types.VARCHAR);
            stmt.setObject(3, getIdExtension(), Types.BIGINT);
            stmt.setObject(4, getLibelle(), Types.VARCHAR);
            stmt.setObject(5, getDateCreation(), Types.TIMESTAMP);
            stmt.setObject(6, getDateModification(), Types.TIMESTAMP);
            stmt.setObject(7, getEtat(), Types.INTEGER);
            stmt.setObject(8, getType(), Types.INTEGER);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD add()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void delete() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("delete from MODULE " + "where ID_MODULE = ?");
            // put parameters into statement
            stmt.setObject(1, getId(), Types.BIGINT);
            final int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD delete()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public String getSQLBaseQuery() {
        final String query = "select distinct " + "T1.ID_MODULE, " + "T1.ID_BEAN, " + "T1.ID_EXTENSION, " + "T1.LIBELLE, " + "T1.DATE_CREATION, " + "T1.DATE_MODIFICATION, " + "T1.ETAT, " + "T1.TYPE " + "from " + (qualifier == null ? "" : qualifier) + "MODULE T1 ";
        return query;
    }

    /**
     * Renvoie l'element suivant du ResultSet.
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                retrieveFromRS();
                res = true;
            }
        } catch (final Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem()", e);
        }
        return res;
    }

    /**
     * Recuperation d'une ligne de la base de donnees.
     */
    public void retrieve() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("select " + "T1.ID_MODULE, " + "T1.ID_BEAN, " + "T1.ID_EXTENSION, " + "T1.LIBELLE, " + "T1.DATE_CREATION, " + "T1.DATE_MODIFICATION, " + "T1.ETAT, " + "T1.TYPE " + "from MODULE T1 " + "where T1.ID_MODULE = ?");
            stmt.setObject(1, getId(), Types.BIGINT);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve : METHOD_NO_RESULTS");
            }
            retrieveFromRS();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieve()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    private void retrieveFromRS() throws Exception {
        try {
            // get output from result set
            setId(rs.getLong(1));
            setIdBean(rs.getString(2));
            setIdExtension(rs.getLong(3));
            setLibelle(rs.getString(4));
            setDateCreation(SqlDateConverter.fromTimestamp(rs.getTimestamp(5)));
            setDateModification(SqlDateConverter.fromTimestamp(rs.getTimestamp(6)));
            setEtat(rs.getInt(7));
            setType(rs.getInt(8));
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS()", e);
        }
    }

    /**
     * Cette methode optimise l'execution de la requete (pas de count)
     *
     * @param sqlSuffix
     * @throws Exception
     */
    public void selectNoCount(final String sqlSuffix) throws Exception {
        PreparedStatement stmt = null;
        try {
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
    }

    public int select(final String sqlSuffix) throws Exception {
        int count = -1;
        PreparedStatement stmt = null;
        try {
            // execution requete
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
            rs.last();
            count = rs.getRow();
            rs.beforeFirst();
        } catch (final SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
        return count;
    }

    /**
     * Mise a jour d'une ligne de la base de donnees.
     */
    public void update() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("update MODULE set " + "ID_MODULE = ?, " + "ID_BEAN = ?, " + "ID_EXTENSION = ?, " + "LIBELLE = ?, " + "DATE_CREATION = ?, " + "DATE_MODIFICATION = ?, " + "ETAT = ?, " + "TYPE = ? " + "where ID_MODULE = ?");
            // put parameters into statement
            stmt.setObject(1, getId(), Types.BIGINT);
            stmt.setObject(2, getIdBean(), Types.VARCHAR);
            stmt.setObject(3, getIdExtension(), Types.BIGINT);
            stmt.setObject(4, getLibelle(), Types.VARCHAR);
            stmt.setObject(5, getDateCreation(), Types.TIMESTAMP);
            stmt.setObject(6, getDateModification(), Types.TIMESTAMP);
            stmt.setObject(7, getEtat(), Types.INTEGER);
            stmt.setObject(8, getType(), Types.INTEGER);
            stmt.setObject(9, getId(), Types.BIGINT);
            stmt.executeUpdate();
            stmt.close();
        } catch (final SQLException e) {
            throw new Exception("ERROR_IN_METHOD update()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
}
