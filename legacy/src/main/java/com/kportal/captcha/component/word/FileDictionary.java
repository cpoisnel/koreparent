/*
 * JCaptcha, the open source java framework for captcha definition and integration
 * Copyright (c)  2007 jcaptcha.net. All Rights Reserved.
 * See the LICENSE.txt file distributed with this package.
 */
package com.kportal.captcha.component.word;

import java.util.Locale;
import java.util.StringTokenizer;

import com.kportal.core.config.PropertyHelper;
import com.octo.captcha.component.word.DefaultSizeSortedWordList;
import com.octo.captcha.component.word.DictionaryReader;
import com.octo.captcha.component.word.SizeSortedWordList;

/**
 * <p>
 * Implementation of the DictionaryReader interface, uses a .properties file to retrieve words and return a WordList.Constructed with the name of the properties file. It uses
 * standard java mecanism for I18N
 * </p>
 *
 * @author <a href="mailto:mga@octo.com">Mathieu Gandin</a>
 * @version 1.1
 */
public class FileDictionary implements DictionaryReader {

    private final String property;

    public FileDictionary(final String property) {
        this.property = property;
    }

    @Override
    public SizeSortedWordList getWordList() {
        final SizeSortedWordList list = generateWordList(Locale.getDefault(), property);
        return list;
    }

    @Override
    public SizeSortedWordList getWordList(final Locale locale) {
        final SizeSortedWordList list = generateWordList(locale, property);
        return list;
    }

    protected SizeSortedWordList generateWordList(final Locale locale, final String property) {
        final DefaultSizeSortedWordList list = new DefaultSizeSortedWordList(locale);
        final StringTokenizer tokenizer = new StringTokenizer(PropertyHelper.getCoreProperty(property), ";");
        final int count = tokenizer.countTokens();
        for (int i = 0; i < count; i++) {
            list.addWord(tokenizer.nextToken());
        }
        return list;
    }
}
