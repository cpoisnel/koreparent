package com.kportal.captcha;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.captcha.image.Gimpy;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore;
import com.octo.captcha.service.multitype.MultiTypeCaptchaService;

public class CaptchaServlet extends HttpServlet {

    private static final long serialVersionUID = -3833735545073866482L;

    private static final Logger LOG = LoggerFactory.getLogger(CaptchaServlet.class);

    @Override
    protected void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse) throws ServletException, IOException {
        // get the session id that will identify the generated captcha.
        //the same id must be used to validate the response, the session id is a good candidate!
        final String captchaId = httpServletRequest.getSession(Boolean.FALSE).getId();
        try {
            if (httpServletRequest.getParameter("image") != null) {
                generateImageResponse(httpServletRequest, httpServletResponse, captchaId);
            } else if (httpServletRequest.getParameter("audio") != null) {
                generateAudioResponse(httpServletResponse, captchaId);
            }
        } catch (final IOException io) {
            LOG.error("unable to write the captcha response", io);
            try {
                httpServletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } catch (final IOException e) {
                LOG.error("unable to send the 500 error", e);
            }
        }
    }

    private static void generateAudioResponse(final HttpServletResponse httpServletResponse, final String captchaId) throws IOException {
        final FastHashMapCaptchaStore captchaStore = (FastHashMapCaptchaStore) ApplicationContextManager.getCoreContextBean("captchaStore");
        final Gimpy captcha = (Gimpy) captchaStore.getCaptcha(captchaId);
        final String response = captcha.getResponse();
        final File f = new File(WebAppUtil.getAbsolutePath() + PropertyHelper.getCoreProperty("captcha.audio_path") + response + ".mp3");
        if (f.exists()) {
            final ServletOutputStream responseOutputStream = httpServletResponse.getOutputStream();
            httpServletResponse.setHeader("Cache-Control", "no-store");
            httpServletResponse.setHeader("Pragma", "no-cache");
            httpServletResponse.setHeader("Content-Disposition", "inline;filename=\"mot.mp3\"");
            httpServletResponse.setDateHeader("Expires", 0);
            httpServletResponse.setContentType("audio/mpeg3");
            try (final FileInputStream fis = new FileInputStream(f);
                 final BufferedInputStream bis = new BufferedInputStream(fis);){
                final byte[] buf = new byte[1024 * 4];
                int nbBytes;
                while ((nbBytes = bis.read(buf)) > 0) {
                    responseOutputStream.write(buf, 0, nbBytes);
                }
            } finally {
                responseOutputStream.flush();
                responseOutputStream.close();
            }
        }
    }

    private static void generateImageResponse(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final String captchaId) throws IOException {// the output stream to render the captcha image as jpeg into
        final ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
        try {
            // call the ImageCaptchaService getChallenge method
            final MultiTypeCaptchaService captchaService = (MultiTypeCaptchaService) ApplicationContextManager.getCoreContextBean("captchaService");
            final BufferedImage image = captchaService.getImageChallengeForID(captchaId, httpServletRequest.getLocale());
            ImageIO.write(image, "jpeg", jpegOutputStream);
            final byte[] captchaChallengeAsJpeg = jpegOutputStream.toByteArray();
            // flush it in the response
            httpServletResponse.setHeader("Cache-Control", "no-store");
            httpServletResponse.setHeader("Pragma", "no-cache");
            httpServletResponse.setDateHeader("Expires", 0);
            httpServletResponse.setContentType("image/jpeg");
            final ServletOutputStream responseOutputStream = httpServletResponse.getOutputStream();
            responseOutputStream.write(captchaChallengeAsJpeg);
            responseOutputStream.flush();
            responseOutputStream.close();
        } catch (final IllegalArgumentException e) {
            LOG.debug("no valid argument", e);
            httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND);
        } catch (final CaptchaServiceException e) {
            LOG.debug("unable to generate the captcha", e);
            httpServletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
