package com.kportal.site.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.PropertyHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;

/**
 *
 * * @author yacouba.kone
 *
 * @deprecated cette classe n'est jamais utilisé. Utiliser le service {@link com.univ.multisites.service.ServiceInfosSite}.
 */
@Deprecated
public class SiteUtil {

    /**
     * Methode renvoyant une Hashtable contenant les couples(Code rubrique, Intitulé) des sites
     *
     * @return Hashtable<code rubrique, nom du site>
     * @throws Exception
     */
    public static Hashtable<String, String> getListeSiteParRubrique() {
        final Hashtable<String, String> mapsListeSite = new Hashtable<>();
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        for (final InfosSite infoSite : serviceInfosSite.getSitesList().values()) {
            mapsListeSite.put(infoSite.getCodeRubrique(), infoSite.getIntitule());
        }
        return mapsListeSite;
    }

    /**
     * Methode renvoyant une Hashtable contenant les couples(Code rubrique, Intitulé) des sites en ligne
     *
     * @return Hashtable<code rubrique, nom du site>
     * @throws Exception
     */
    public static Hashtable<String, String> getListeSiteEnLigneParRubrique() {
        final Hashtable<String, String> mapsListeSiteEnligne = new Hashtable<>();
        List<String> sitesNonVisibles = new ArrayList<>();
        final String paramSitesNonVisibles = PropertyHelper.getCoreProperty("recherche.sites_non_visibles");
        if (paramSitesNonVisibles != null) {
            sitesNonVisibles = Arrays.asList(paramSitesNonVisibles.split(";"));
        }
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        for (final InfosSite infoSite : serviceInfosSite.getSitesList().values()) {
            if (!sitesNonVisibles.contains(infoSite.getAlias())) {
                mapsListeSiteEnligne.put(infoSite.getCodeRubrique(), infoSite.getIntitule());
            }
        }
        return mapsListeSiteEnligne;
    }
}