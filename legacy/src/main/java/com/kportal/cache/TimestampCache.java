package com.kportal.cache;

import java.io.Serializable;

public class TimestampCache implements Serializable {

    private static final long serialVersionUID = -4148417746290524529L;

    private Long id;

    private String key;

    private Long timestamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
