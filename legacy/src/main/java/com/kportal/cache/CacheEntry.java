package com.kportal.cache;

import java.io.Serializable;
// TODO: Auto-generated Javadoc

/**
 * Accessible cache entry holding the creation date.
 */
public class CacheEntry implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The value. */
    private Object value;

    /** The creation date. */
    private Long creationDate;

    /**
     * Instantiates a new cache entry.
     *
     * @param value
     *            the value
     * @param creationDate
     *            the creation date
     */
    public CacheEntry(Object value, Long creationDate) {
        this.value = value;
        this.creationDate = creationDate;
    }

    /**
     * Instantiates a new cache entry.
     *
     * @param value
     *            the value
     */
    public CacheEntry(Object value) {
        this.value = value;
        this.creationDate = System.currentTimeMillis();
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Gets the creation date.
     *
     * @return the creation date
     */
    public Long getCreationDate() {
        return creationDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CacheEntry other = (CacheEntry) obj;
        if (creationDate == null) {
            if (other.creationDate != null) {
                return false;
            }
        } else if (!creationDate.equals(other.creationDate)) {
            return false;
        }
        if (value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!value.equals(other.value)) {
            return false;
        }
        return true;
    }
}
