package com.kportal.cache;


/**
 * @deprecated cette classe n'est jamais utilisé, ni sa table d'ailleurs, cela doit être un reste
 * d'un pseudo cache maison réaliser en v5
 */
@Deprecated
public class Cache extends CacheDB {

    /**
     * Initialise l'objet metier.
     */
    public void init() {
        setIdCache(new Long(0));
        setType("");
        setDate(new Long(0));
    }

    /**
     * Renvoie le libelle a afficher.
     */
    public String getLibelleAffichable() {
        return "Cache " + getIdCache();
    }
}
