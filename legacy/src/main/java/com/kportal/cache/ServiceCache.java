package com.kportal.cache;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.JDBCUtils;
import com.univ.utils.ContexteDao;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * @deprecated cette classe n'est jamais utilisé, ni sa table d'ailleurs, cela doit être un reste
 * d'un pseudo cache maison réaliser en v5
 */
@Deprecated
public class ServiceCache {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceCache.class);

    public static Cache getCache(final String type) throws SQLException {
        Cache cache = null;
        try (ContexteDao ctx = new ContexteDao()) {
            if (JDBCUtils.existe(ctx.getConnection(), "CACHE")) {
                final RequeteSQL req = new RequeteSQL();
                final ClauseWhere clauseWhere = new ClauseWhere();
                if (StringUtils.isNotEmpty(type)) {
                    clauseWhere.setPremiereCondition(ConditionHelper.egalVarchar("T1.TYPE", type));
                }
                final Cache c = new Cache();
                c.init();
                c.setCtx(ctx);
                c.select(req.where(clauseWhere).formaterRequete());
                if (c.nextItem()) {
                    cache = c;
                }
            }
        } catch (final Exception e) {
            LOG.error("Cache de type " + type + " introuvale", e);
        }
        return cache;
    }

    public static void addCache(final Cache cache) {
        try (ContexteDao ctx = new ContexteDao()) {
            if (JDBCUtils.existe(ctx.getConnection(), "CACHE")) {
                cache.setCtx(ctx);
                cache.add();
            } else {
                LOG.error("la table CACHE n'existe pas!");
            }
        } catch (final Exception e) {
            LOG.error("Ajout du cache de type " + cache.getType() + " imposssible", e);
        }
    }

    public static boolean needToRefresh(final HttpServletRequest request, final String cle) throws Exception {
        final String type = request.getServerName() + (request.getRequestURL().toString().contains(":" + request.getServerPort() + "/") ? ":" + request.getServerPort() : "") + "_" + cle;
        return needToRefresh(type);
    }

    public static boolean needToRefresh(final String type) throws Exception {
        final Cache cache = getCache(type);
        if (cache != null) {
            cache.delete();
            return true;
        }
        return false;
    }
}
