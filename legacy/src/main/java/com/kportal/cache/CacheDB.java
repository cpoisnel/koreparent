package com.kportal.cache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;


/**
 * @deprecated cette classe n'est jamais utilisé, ni sa table d'ailleurs, cela doit être un reste
 * d'un pseudo cache maison réaliser en v5
 */
@Deprecated
public class CacheDB {

    protected transient OMContext ctx = null;

    protected String qualifier = null;

    private ResultSet rs = null;

    private Long idCache = null;

    private String type = null;

    private Long date = null;

    public void setIdCache(Long idCache) {
        this.idCache = idCache;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getIdCache() {
        return idCache;
    }

    public String getType() {
        return type;
    }

    public Long getDate() {
        return date;
    }

    @Override
    public String toString() {
        return toString(".");
    }

    public String toString(String aSeparator) {
        String s = "" + getIdCache() + aSeparator + getType() + aSeparator + getDate();
        return s;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }

    private Connection getConnection() throws Exception {
        return ctx.getConnection();
    }

    public void add() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("insert into CACHE (ID_CACHE , TYPE , DATE) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setObject(1, getIdCache(), Types.BIGINT);
            stmt.setObject(2, getType(), Types.VARCHAR);
            stmt.setObject(3, getDate(), Types.BIGINT);
            int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
            rs = stmt.getGeneratedKeys();
            rs.next();
            setIdCache(rs.getLong(1));
        } catch (SQLException e) {
            throw new Exception("ERROR_IN_METHOD add()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void delete() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("DELETE FROM CACHE WHERE ID_CACHE = ?");
            // put parameters into statement
            stmt.setObject(1, getIdCache(), Types.BIGINT);
            int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 0) {
                throw new Exception("METHOD_AFFCTD_NOROWS");
            } else if (rowsAffected > 1) {
                throw new Exception("METHOD_AFFCTD_MULTROWS");
            }
        } catch (SQLException e) {
            throw new Exception("ERROR_IN_METHOD delete()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public String getSQLBaseQuery() {
        return "select distinct T1.ID_CACHE, T1.TYPE, T1.DATE from " + StringUtils.defaultString(qualifier) + "CACHE T1 ";
    }

    /**
     * Renvoie l'element suivant du ResultSet.
     */
    public boolean nextItem() throws Exception {
        boolean res = false;
        try {
            if (rs.next()) {
                retrieveFromRS();
                res = true;
            }
        } catch (Exception e) {
            throw new Exception("ERROR_IN_METHOD nextItem()", e);
        }
        return res;
    }

    /**
     * Recuperation d'une ligne de la base de donnees.
     */
    public void retrieve() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("SELECT T1.ID_CACHE, T1.TYPE, T1.DATE FROM CACHE T1 WHERE T1.ID_CACHE = ?");
            stmt.setObject(1, getIdCache(), Types.BIGINT);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                throw new Exception("retrieve : METHOD_NO_RESULTS");
            }
            retrieveFromRS();
        } catch (SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieve()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    private void retrieveFromRS() throws Exception {
        try {
            // get output from result set
            setIdCache(rs.getLong(1));
            setType(rs.getString(2));
            setDate(rs.getLong(3));
        } catch (SQLException e) {
            throw new Exception("ERROR_IN_METHOD retrieveFromRS()", e);
        }
    }

    public void select(String sqlSuffix) throws Exception {
        PreparedStatement stmt = null;
        try {
            // recuperation nombre de lignes
            String query = getSQLBaseQuery();
            if (sqlSuffix != null) {
                query += sqlSuffix;
            }
            stmt = getConnection().prepareStatement(query);
            rs = stmt.executeQuery();
        } catch (SQLException e) {
            throw new Exception("SELECT_FAILED", e);
        }
    }

    /**
     * Mise a jour d'une ligne de la base de donnees.
     */
    public void update() throws Exception {
        PreparedStatement stmt = null;
        try {
            stmt = getConnection().prepareStatement("update CACHE set ID_CACHE = ?, TYPE = ?, DATE = ? where ID_CACHE = ?");
            // put parameters into statement
            stmt.setObject(1, getIdCache(), Types.BIGINT);
            stmt.setObject(2, getType(), Types.VARCHAR);
            stmt.setObject(3, getDate(), Types.BIGINT);
            stmt.setObject(4, getIdCache(), Types.BIGINT);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            throw new Exception("ERROR_IN_METHOD update()", e);
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }

    public void setCtx(OMContext ctx) {
        this.ctx = ctx;
    }
}
