package com.kportal.ihm.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kportal.extension.module.composant.Menu;
import com.kportal.ihm.service.ServiceMenuFront;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.om.Rubrique;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.UnivWebFmt;

public class ServiceMenuFrontStandard implements ServiceMenuFront {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceMenuFrontStandard.class);

    private ServiceMedia serviceMedia;

    public void setServiceMedia(final ServiceMedia serviceMedia) {
        this.serviceMedia = serviceMedia;
    }

    @Override
    public Menu getMenuSiteCourant(final String nomProprieteSite) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        return getMenuSite(ctx.getInfosSite(), ctx.getLocale(), nomProprieteSite);
    }

    @Override
    public Menu getMenuSite(final InfosSite site, final String nomProprieteSite) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        return getMenuSite(site, ctx.getLocale(), nomProprieteSite);
    }

    @Override
    public Menu getMenuSite(final InfosSite site, final Locale langue, final String nomProprieteSite) {
        final List<String> codesRubriques = site.getProprieteComplementaireListString(nomProprieteSite);
        Menu menuCourant = null;
        try {
            if (CollectionUtils.isNotEmpty(codesRubriques)) {
                for (final String codeRubrique : codesRubriques) {
                    final InfosRubriques rubriqueMereMenu = Rubrique.renvoyerItemRubrique(codeRubrique);
                    if (StringUtils.isNotBlank(rubriqueMereMenu.getCode())) {
                        final Locale localeRubrique = LangueUtil.getLocale(rubriqueMereMenu.getLangue());
                        if (langue.equals(localeRubrique)) {
                            menuCourant = getMenuDeuxNiveauParRubrique(rubriqueMereMenu);
                        }
                    }
                }
            }
        } catch (final Exception e) {
            LOG.error("impossible de calculer le menu du site courant", e);
        }
        return menuCourant;
    }

    @Override
    public Menu getMenuLangue(final InfosSite site, final String nomProprieteSite) {
        final List<String> codesRubriques = site.getProprieteComplementaireListString(nomProprieteSite);
        Menu menuLangue = null;
        if (CollectionUtils.isNotEmpty(codesRubriques)) {
            final Collection<String> languesTraitee = new ArrayList<>();
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            languesTraitee.add(ctx.getLangue());
            try {
                menuLangue = new Menu();
                for (final String codeRubrique : codesRubriques) {
                    final InfosRubriques rubriqueMereMenu = Rubrique.renvoyerItemRubrique(codeRubrique);
                    if (StringUtils.isNotBlank(rubriqueMereMenu.getCode()) && !languesTraitee.contains(rubriqueMereMenu.getLangue())) {
                        menuLangue.addSousMenu(creerMenuItemDepuisRubrique(ctx, rubriqueMereMenu));
                        languesTraitee.add(rubriqueMereMenu.getLangue());
                    }
                }
            } catch (final Exception e) {
                LOG.error("impossible de récupérer le menu des langues", e);
            }
        }
        return menuLangue;
    }

    protected Menu getMenuDeuxNiveauParRubrique(final InfosRubriques rubriqueMereMenu) throws Exception {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final Menu menuPrincipal = creerMenuItemDepuisRubrique(ctx, rubriqueMereMenu);
        for (final InfosRubriques rubriqueDescendante : rubriqueMereMenu.getListeSousRubriquesFront(ctx)) {
            final Menu menuCourant = creerMenuItemDepuisRubrique(ctx, rubriqueDescendante);
            for (final InfosRubriques sousRubriqueDescendante : rubriqueDescendante.getListeSousRubriquesFront(ctx)) {
                menuCourant.addSousMenu(creerMenuItemDepuisRubrique(ctx, sousRubriqueDescendante));
            }
            menuPrincipal.addSousMenu(menuCourant);
        }
        return menuPrincipal;
    }

    protected Menu creerMenuItemDepuisRubrique(final ContexteUniv ctx, final InfosRubriques rubriqueDescendante) throws Exception {
        final Menu lienDuMenu = new Menu();
        lienDuMenu.setLangue(rubriqueDescendante.getLangue());
        lienDuMenu.setLibelle(rubriqueDescendante.getLibelleAffichable());
        lienDuMenu.setType(rubriqueDescendante.getTypeRubrique());
        lienDuMenu.setUrl(UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, rubriqueDescendante.getCode()));
        lienDuMenu.setVisuel(MediaUtils.getUrlAbsolue(serviceMedia.getById(rubriqueDescendante.getIdMediaBandeau())));
        lienDuMenu.setCode(rubriqueDescendante.getCode());
        lienDuMenu.setCodeRubriqueOrigine(rubriqueDescendante.getCode());
        return lienDuMenu;
    }

    @Override
    public Menu getMenuParCodeRubrique(final String codeRubrique) {
        Menu menuCourant = null;
        if (StringUtils.isNotBlank(codeRubrique)) {
            try {
                final InfosRubriques rubriqueMereMenu = Rubrique.renvoyerItemRubrique(codeRubrique);
                menuCourant = getMenuDeuxNiveauParRubrique(rubriqueMereMenu);
            } catch (final Exception e) {
                LOG.error("impossible de récupérer le menu depuis le code de rubrique", e);
            }
        }
        return menuCourant;
    }
}
