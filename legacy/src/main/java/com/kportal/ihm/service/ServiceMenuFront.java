package com.kportal.ihm.service;

import java.util.Locale;

import com.kportal.extension.module.composant.Menu;
import com.univ.multisites.InfosSite;

/**
 * Service se chargeant de calculer les différents menu nécessaire en front office.
 *
 * @author olivier.camon
 *
 */
public interface ServiceMenuFront {

    /**
     * Construit un {@link Menu } à partir d'un code de rubrique.
     *
     * @param codeRubrique
     *            la rubrique racine du menu
     * @return un bean Menu ou null si la rubrique n'est pas retrouvé
     */
    Menu getMenuParCodeRubrique(String codeRubrique);

    /**
     *
     * Construit un {@link Menu } à partir d'une propriété du site courant.
     *
     * @param nomProprieteSite
     *            la propriété du site contenant le code de rubrique.
     * @return un bean Menu ou null si la propriété est invalide ou si la rubrique n'est pas retrouvé
     */
    Menu getMenuSiteCourant(String nomProprieteSite);

    /**
     * Construit un {@link Menu } à partir d'une propriété du site fourni en paramètre.
     *
     * @param site
     * @param nomProprieteSite
     * @return un bean Menu ou null si la propriété est invalide ou si la rubrique n'est pas retrouvé
     */
    Menu getMenuSite(InfosSite site, String nomProprieteSite);

    /**
     * Construit un {@link Menu } à partir d'une propriété du site fourni en paramètre. La langue permet de récupérer la rubrique correspondant de la bonne langue.
     *
     * @param site
     *            le site sur lequel on veut récupérer le menu
     * @param langue
     *            la locale du menu que l'on souhaite retrouver
     * @param nomProprieteSite
     *            la propriété du site contenant le code de rubrique.
     * @return un bean Menu ou null si la propriété est invalide ou si la rubrique n'est pas retrouvé
     */
    Menu getMenuSite(InfosSite site, Locale langue, String nomProprieteSite);

    /**
     * Permet de récupérer le menu de gestion des langues. A L'inverse des autres méthodes, il récupére une liste de valeur de la propriété fourni en paramètre et retourne
     * l'ensemble des rubriques mappé en menu sauf celle de la langue courante.
     *
     * @param site
     *            le site sur lequel on veut récupérer le menu
     * @param nomProprieteSite
     *            la propriété du site contenant le code de rubrique.
     * @return un bean Menu ou null si la propriété est invalide ou si la rubrique n'est pas retrouvé
     */
    Menu getMenuLangue(InfosSite site, String nomProprieteSite);
}