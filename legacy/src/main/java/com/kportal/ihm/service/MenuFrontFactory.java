package com.kportal.ihm.service;

import com.jsbsoft.jtf.core.ApplicationContextManager;

/**
 * Se charge juste de récupérer le service gérant les menu en FO
 *
 * @author olivier.camon
 *
 */
public abstract class MenuFrontFactory {

    public static final String ID_BEAN_MENU_FRONT = "serviceMenuFront";

    public static ServiceMenuFront getServiceMenuFront() {
        return (ServiceMenuFront) ApplicationContextManager.getBean(ApplicationContextManager.DEFAULT_CORE_CONTEXT, ID_BEAN_MENU_FRONT);
    }
}