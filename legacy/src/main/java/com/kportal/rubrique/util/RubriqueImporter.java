package com.kportal.rubrique.util;

/**
 * Import de rubriques
 * 
 * @author emmanuel clisson
 * @deprecated ancien développement v5 pour exporter des rubriques, il n'a jamais été fonctionnel...
 */
@Deprecated
public class RubriqueImporter {

    private boolean synchrone = false;

    public boolean isSynchrone() {
        return synchrone;
    }

    public void setSynchrone(boolean synchrone) {
        this.synchrone = synchrone;
    }
}
