package com.kportal.rubrique.util;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.FicheUnivMgr;

/**
 *
 * @deprecated ancien développement v5 pour exporter des rubriques, il n'a jamais été fonctionnel...
 */
@Deprecated
public class RubriqueManager {

    private static final Logger LOG = LoggerFactory.getLogger(RubriqueManager.class);

    public static void deleteRubrique(final OMContext ctx, final InfosRubriques infosRubriques, final boolean deleteContent, final boolean recursif) throws Exception {
        if (infosRubriques == null) {
            return;
        }
        LOG.info("Requête rubrique : code=" + infosRubriques.getCode() + " libelle=" + infosRubriques.getIntitule());
        // suppression du contenu
        if (deleteContent && infosRubriques.getCode() != null) {
            final List<MetatagBean> metas = MetatagUtils.getMetatagService().getMetasForRubrique(infosRubriques.getCode());
            if (CollectionUtils.isNotEmpty(metas)) {
                for (MetatagBean currentMeta : metas) {
                    final FicheUniv ficheUniv = FicheUnivMgr.init(currentMeta);
                    ficheUniv.setCtx(ctx);
                    ficheUniv.setIdFiche(currentMeta.getMetaIdFiche());
                    try {
                        ficheUniv.retrieve();
                        FicheUnivMgr.supprimerFiche(ficheUniv, true);
                        LOG.info("Suppression fiche : type=" + ReferentielObjets.getNomObjet(currentMeta.getMetaCodeObjet()) + " code=" + ficheUniv.getCode() + " libelle=" + ficheUniv.getLibelleAffichable());
                    } catch (final Exception e) {
                        LOG.info("Erreur suppression fiche : type=" + ReferentielObjets.getNomObjet(currentMeta.getMetaCodeObjet()) + " code=" + ficheUniv.getCode() + " libelle=" + ficheUniv.getLibelleAffichable());
                    }
                }
            }
        }
        // suppression des sous rubriques
        if (recursif) {
            final Collection<InfosRubriques> lstSousRubriques = infosRubriques.getListeSousRubriques();
            if (lstSousRubriques.size() > 0) {
                for (final InfosRubriques infosSousRubriques : lstSousRubriques) {
                    deleteRubrique(ctx, infosSousRubriques, deleteContent, recursif);
                }
            }
        }
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        serviceRubrique.deletebyId(infosRubriques.getIdRubrique());
        LOG.info("Suppression rubrique : code=" + infosRubriques.getCode() + " libelle=" + infosRubriques.getIntitule());
    }

    public static void changeRedacteur(final OMContext ctx, final InfosRubriques infosRubriques, final String oldCode, final String newCode, final boolean recursif)
        throws Exception {
        if (infosRubriques == null || infosRubriques.getCode() == null || oldCode == null || newCode == null) {
            LOG.info("Changement de redacteur impossible avec ces parametres : oldcode = " + oldCode + " newCode = " + newCode + " infosRubriques " + infosRubriques);
            return;
        }
        LOG.info("Requête rubrique : code=" + infosRubriques.getCode() + " libelle=" + infosRubriques.getIntitule());
        // suppression du contenu
        final List<MetatagBean> metas = MetatagUtils.getMetatagService().getByCodeRedacteurAndCodeRubrique(oldCode, infosRubriques.getCode());
        if (CollectionUtils.isNotEmpty(metas)) {
            for (MetatagBean currentMeta : metas) {
                final FicheUniv ficheUniv = FicheUnivMgr.init(currentMeta);
                ficheUniv.setCtx(ctx);
                ficheUniv.setIdFiche(currentMeta.getMetaIdFiche());
                try {
                    ficheUniv.retrieve();
                    ficheUniv.setCodeRedacteur(newCode);
                    ficheUniv.setCodeValidation(newCode);
                    ficheUniv.update();
                    LOG.info("Mise à jour fiche : type=" + ReferentielObjets.getNomObjet(currentMeta.getMetaCodeObjet()) + " code=" + ficheUniv.getCode() + " libelle=" + ficheUniv.getLibelleAffichable());
                } catch (final Exception e) {
                    LOG.info("Erreur mise à jour fiche : type=" + ReferentielObjets.getNomObjet(currentMeta.getMetaCodeObjet()) + " code=" + ficheUniv.getCode() + " libelle=" + ficheUniv.getLibelleAffichable());
                }
            }
        }
        // mise à jour des sous rubriques
        if (recursif) {
            final Collection<InfosRubriques> lstSousRubriques = infosRubriques.getListeSousRubriques();
            if (lstSousRubriques.size() > 0) {
                for (final InfosRubriques infosSousRubriques : lstSousRubriques) {
                    changeRedacteur(ctx, infosSousRubriques, oldCode, newCode, recursif);
                }
            }
        }
    }
}
