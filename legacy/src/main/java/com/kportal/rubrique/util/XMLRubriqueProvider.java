package com.kportal.rubrique.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.univ.objetspartages.om.Rubrique;

/**
 * Instanciation de rubrique depuis un flux XML
 * 
 * @author emmanuel clisson
 * @deprecated ancien développement v5 pour exporter des rubriques, il n'a jamais été fonctionnel...
 */
@Deprecated
public class XMLRubriqueProvider extends RubriqueProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLRubriqueProvider.class);

    public final static String XML_MIME_TYPE_PATTERN = "^(.*,)?application/xml(,.*)?$";

    @Override
    public Map<String, RubriqueWrapper> setRubrique(File file) throws FileNotFoundException {
        Map<String, RubriqueWrapper> wrappers = null;
        InputStream stream = new BufferedInputStream(new FileInputStream(file));
        try {
            LOGGER.info("parsing xml document");
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(stream);
            if (doc != null) {
                NodeList items = doc.getElementsByTagName("ITEM");
                if (items != null && items.getLength() > 0) {
                    wrappers = new HashMap<String, RubriqueWrapper>();
                    for (int i = 0; i < items.getLength(); ++i) {
                        Node item = items.item(i);
                        if (item != null) {
                            NodeList itemHeadAndData = item.getChildNodes();
                            if (itemHeadAndData != null) {
                                Element itemHead = null;
                                Element itemData = null;
                                for (int j = 0; j < itemHeadAndData.getLength(); ++j) {
                                    if (itemHeadAndData.item(j).getNodeType() == Node.ELEMENT_NODE && ("ENTETE".equals(((Element) itemHeadAndData.item(j)).getTagName()))) {
                                        itemHead = (Element) itemHeadAndData.item(j);
                                    } else if (itemHeadAndData.item(j).getNodeType() == Node.ELEMENT_NODE && ("DONNEES".equals(((Element) itemHeadAndData.item(j)).getTagName()))) {
                                        itemData = (Element) itemHeadAndData.item(j);
                                    }
                                }
                                String objectProperty = null;
                                String actionProperty = null;
                                String codeProperty = null;
                                String langueProperty = null;
                                if (itemHead != null) {
                                    NodeList itemHeadProperties = itemHead.getChildNodes();
                                    if (itemHeadProperties != null) {
                                        for (int j = 0; j < itemHeadProperties.getLength(); ++j) {
                                            if (itemHeadProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("OBJET".equals(((Element) itemHeadProperties.item(j)).getTagName()))) {
                                                objectProperty = ((Element) itemHeadProperties.item(j)).getFirstChild().getNodeValue();
                                            } else if (itemHeadProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("ACTION".equals(((Element) itemHeadProperties.item(j)).getTagName()))) {
                                                actionProperty = ((Element) itemHeadProperties.item(j)).getFirstChild().getNodeValue();
                                            } else if (itemHeadProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("CODE".equals(((Element) itemHeadProperties.item(j)).getTagName()))) {
                                                codeProperty = ((Element) itemHeadProperties.item(j)).getFirstChild().getNodeValue();
                                            } else if (itemHeadProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("LANGUE".equals(((Element) itemHeadProperties.item(j)).getTagName()))) {
                                                langueProperty = ((Element) itemHeadProperties.item(j)).getFirstChild().getNodeValue();
                                            }
                                        }
                                    }
                                }
                                if (objectProperty != null && "RUBRIQUE".equals(objectProperty.toUpperCase())) {
                                    LOGGER.info("rubrique found : processing");
                                    Rubrique rubrique = new Rubrique();
                                    rubrique.init();
                                    RubriqueWrapper wrapper = new RubriqueWrapper(rubrique);
                                    if (codeProperty != null) {
                                        wrapper.setCode(codeProperty);
                                    }
                                    if (langueProperty != null) {
                                        wrapper.setLangue(langueProperty);
                                    }
                                    if (actionProperty != null) {
                                        wrapper.setAction(actionProperty);
                                    }
                                    NodeList itemDatasProperties = null;
                                    if (itemData != null) {
                                        itemDatasProperties = itemData.getChildNodes();
                                    }
                                    if (itemDatasProperties != null && itemDatasProperties.getLength() > 0) {
                                        for (int j = 0; j < itemDatasProperties.getLength(); ++j) {
                                            if (itemDatasProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("CODE_RUBRIQUE_MERE".equals(((Element) itemDatasProperties.item(j)).getTagName())) && itemDatasProperties.item(
                                                    j).getFirstChild() != null) {
                                                wrapper.setCodeRubriqueMere((((Element) itemDatasProperties.item(j)).getFirstChild().getNodeValue()));
                                            } else if (itemDatasProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("INTITULE".equals(((Element) itemDatasProperties.item(j)).getTagName())) && itemDatasProperties.item(
                                                    j).getFirstChild() != null) {
                                                wrapper.setIntitule((((Element) itemDatasProperties.item(j)).getFirstChild().getNodeValue()));
                                            } else if (itemDatasProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("NOM_ONGLET".equals(((Element) itemDatasProperties.item(j)).getTagName())) && itemDatasProperties.item(
                                                    j).getFirstChild() != null) {
                                                wrapper.setNomOnglet((((Element) itemDatasProperties.item(j)).getFirstChild().getNodeValue()));
                                            } else if (itemDatasProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("ORDRE".equals(((Element) itemDatasProperties.item(j)).getTagName())) && itemDatasProperties.item(
                                                    j).getFirstChild() != null) {
                                                wrapper.setOrdre((((Element) itemDatasProperties.item(j)).getFirstChild().getNodeValue()));
                                            } else if (itemDatasProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("TYPE_RUBRIQUE".equals(((Element) itemDatasProperties.item(j)).getTagName())) && itemDatasProperties.item(
                                                    j).getFirstChild() != null) {
                                                wrapper.setTypeRubrique((((Element) itemDatasProperties.item(j)).getFirstChild().getNodeValue()));
                                            } else if (itemDatasProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("PAGE_ACCUEIL".equals(((Element) itemDatasProperties.item(j)).getTagName())) && itemDatasProperties.item(
                                                    j).getFirstChild() != null) {
                                                wrapper.setPageAccueil((((Element) itemDatasProperties.item(j)).getFirstChild().getNodeValue()));
                                            } else if (itemDatasProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("GENERER_PAGE_TETE".equals(((Element) itemDatasProperties.item(j)).getTagName())) && itemDatasProperties.item(
                                                    j).getFirstChild() != null) {
                                                wrapper.setGenererPageTete((((Element) itemDatasProperties.item(j)).getFirstChild().getNodeValue()));
                                            } else if (itemDatasProperties.item(j).getNodeType() == Node.ELEMENT_NODE && ("CATEGORIE".equals(((Element) itemDatasProperties.item(j)).getTagName())) && itemDatasProperties.item(
                                                    j).getFirstChild() != null) {
                                                wrapper.setCategorie((((Element) itemDatasProperties.item(j)).getFirstChild().getNodeValue()));
                                            }
                                        }
                                        // build map
                                        if (wrapper.getCode() != null && !wrapper.getCode().trim().equals("")) {
                                            if (!wrappers.containsKey(wrapper.getCode())) {
                                                wrappers.put(wrapper.getCode(), wrapper);
                                                LOGGER.info("rubrique inserted : " + wrapper.getCode());
                                                if (wrapper.getCodeRubriqueMere() != null && !wrapper.getCodeRubriqueMere().trim().equals("")) {
                                                    if (wrappers.containsKey(wrapper.getCodeRubriqueMere())) {
                                                        wrappers.get(wrapper.getCodeRubriqueMere().trim()).getSousRubriques().add(wrapper);
                                                        LOGGER.info("rubrique defined as child of mother rubrique " + wrapper.getCodeRubriqueMere().trim());
                                                    } else {
                                                        wrapper.setCodeRubriqueMere("");
                                                        LOGGER.info("rubrique defined as child of mother rubrique [" + wrapper.getCodeRubriqueMere().trim() + "] but no match : set this rubrique as root");
                                                    }
                                                }
                                            } else {
                                                LOGGER.info("rubrique already found : ignore this occurence");
                                            }
                                        } else {
                                            LOGGER.info("rubrique without CODE (if it is a new one set an arbitrary value) : ignored");
                                        }
                                    } else {
                                        LOGGER.info("rubrique without any data : skip");
                                    }
                                }
                            }
                        }
                    }
                } else {
                    LOGGER.info("no rubriques found");
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("exception pendant l'import",e);
            }
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {}
            }
        }
        return wrappers;
    }
}
