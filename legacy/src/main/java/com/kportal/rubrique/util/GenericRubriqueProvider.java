package com.kportal.rubrique.util;

import java.io.File;
import java.util.Map;

import com.univ.mediatheque.Mediatheque;

/**
 * @author emmanuel clisson
 * @deprecated ancien développement v5 pour exporter des rubriques, il n'a jamais été fonctionnel...
 */
@Deprecated
public class GenericRubriqueProvider extends RubriqueProvider {

    @Override
    public Map<String, RubriqueWrapper> setRubrique(File file) throws Exception {
        RubriqueProvider provider = null;
        Map<String, RubriqueWrapper> wrappers = null;
        // recherche des mime types
        String mimeType = Mediatheque.getInstance().getContentType(file.getName());
        // choix du provider
        if (mimeType.matches(XMLRubriqueProvider.XML_MIME_TYPE_PATTERN)) {
            provider = new XMLRubriqueProvider();
        } else {
            provider = null;
        }
        // si possible instanciation d'objet K-Portal
        if (provider != null) {
            wrappers = provider.setRubrique(file);
        }
        return wrappers;
    }
}
