package com.kportal.rubrique.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.univ.objetspartages.om.Rubrique;
import com.univ.utils.ContexteDao;

/**
 * Importation de rubrique depuis le format XML
 * 
 * @author emmanuel clisson Les requetes JDBC sont réécrites à la main pour 2 raisons : 1) si nous voulons utiliser les méthodes déjà existantes c'est le drame (exception java) :
 *         a) un processus framework est nécéssaire : or nous souhaitons proposer l'opération indépendamment de cet aspect pour des raisons de flexibilité, entre autres... il est
 *         ainsi facle de recetter la fonctionaité puis de l'intégrer dans un processus framework ou autre. b) le contexte du framework n'est pas accessible visiblement (back
 *         office) : java null pointer exception 2) les méthodes du framework empêchent l'utilisation de transaction SQL... l'intégrité des données en souffrirait méchamment
 * @deprecated ancien développement v5 pour exporter des rubriques, il n'a jamais été fonctionnel...
 */
@Deprecated
public class XMLRubriqueImporter extends RubriqueImporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLRubriqueImporter.class);

    private RubriqueWrapper getOriginalWrapper(String code, String langue) throws Exception {
        Rubrique rubrique = new Rubrique();
        rubrique.init();
        RubriqueWrapper wrapper = new RubriqueWrapper(rubrique);
        final String query = "select * from RUBRIQUE where CODE = ? and LANGUE = ?";
        try (ContexteDao contexteDao = new ContexteDao();
            PreparedStatement stmt = contexteDao.getConnection().prepareStatement(query)){
            stmt.setObject(1, code, Types.VARCHAR);
            stmt.setObject(2, langue, Types.VARCHAR);
            LOGGER.info("SQL Query = " + query);
            try(ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    wrapper.setIdRubrique(Long.valueOf(rs.getString("ID_RUBRIQUE")));
                    wrapper.setCode(rs.getString("CODE"));
                    wrapper.setLangue(rs.getString("LANGUE"));
                    wrapper.setCodeRubriqueMere(rs.getString("CODE_RUBRIQUE_MERE"));
                    wrapper.setIntitule(rs.getString("INTITULE"));
                    wrapper.setNomOnglet(rs.getString("NOM_ONGLET"));
                    wrapper.setIntitule(rs.getString("INTITULE"));
                    wrapper.setOrdre(rs.getString("ORDRE"));
                    wrapper.setTypeRubrique(rs.getString("TYPE_RUBRIQUE"));
                    wrapper.setPageAccueil(rs.getString("PAGE_ACCUEIL"));
                    wrapper.setCategorie(rs.getString("CATEGORIE"));
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Une erreur est survenue lors de la tentative de récupération du wrapper", e);
        }
        return wrapper;
    }

    private boolean alreadyExists(String code, String langue) throws Exception {
        boolean alreadyExists = false;
        String query = "select ID_RUBRIQUE from RUBRIQUE where CODE = ? and LANGUE = ?";
        try (ContexteDao contexteDao = new ContexteDao();
             PreparedStatement stmt = contexteDao.getConnection().prepareStatement(query)){
            stmt.setObject(1, code, Types.VARCHAR);
            stmt.setObject(2, langue, Types.VARCHAR);
            LOGGER.info("SQL Query = " + query);
            try(ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    alreadyExists = true;
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Une erreur est survenue lors de la vérification d'existance de la rubrique", e);
        }
        return alreadyExists;
    }

    private boolean prepareWrapper(Map<String, RubriqueWrapper> rubriques, RubriqueWrapper wrapperSRC, RubriqueWrapper wrapperDST) throws Exception {
        boolean ok = true;
        // code
        if (wrapperSRC.getCode() == null || "".equals(wrapperSRC.getCode().trim())) {
            if (wrapperSRC == wrapperDST) {
                LOGGER.info("- nouvelle rubrique sans code généré : skip");
                ok = false;
            } else {
                LOGGER.info("- rubrique sans code généré : deprecated value");
            }
        }
        // langue
        if (wrapperSRC.getLangue() == null || LangueUtil.getLocale(wrapperSRC.getLangue()) == null) {
            if (wrapperSRC == wrapperDST) {
                LOGGER.info("- langue introuvable : set default one");
                wrapperDST.setLangue(String.valueOf(LangueUtil.getIndiceLocale(LangueUtil.getDefaultLocale())));
            } else {
                LOGGER.info("- langue introuvable : ignored value change");
            }
        } else {
            wrapperDST.setLangue(wrapperSRC.getLangue());
        }
        // ordre
        if (wrapperSRC.getOrdre() == null || "".equals(wrapperSRC.getOrdre().trim()) || !wrapperSRC.getOrdre().matches("^[0-9]+$")) {
            if (wrapperSRC == wrapperDST) {
                LOGGER.info("- ordre invalide : set 0");
                wrapperDST.setOrdre("0");
            } else {
                LOGGER.info("- ordre invalide : ignored value change");
            }
        } else {
            wrapperDST.setOrdre(wrapperSRC.getLangue());
        }
        // intitulé
        if (wrapperSRC.getIntitule() == null || "".equals(wrapperSRC.getIntitule().trim())) {
            if (wrapperSRC == wrapperDST) {
                LOGGER.info("- intitulé absent : deprecated value");
            } else {
                LOGGER.info("- intitulé absent : ignored value change");
            }
        } else {
            wrapperDST.setIntitule(wrapperSRC.getIntitule());
        }
        // nom d'onglet
        if (wrapperSRC.getNomOnglet() == null || "".equals(wrapperSRC.getNomOnglet().trim())) {
            if (wrapperSRC == wrapperDST) {
                LOGGER.info("- nom d'onglet absent : set INTITULE");
                wrapperDST.setNomOnglet(wrapperDST.getIntitule());
            } else {
                LOGGER.info("- nom d'onglet absent : ignored value change");
            }
        } else {
            wrapperDST.setNomOnglet(wrapperSRC.getNomOnglet());
        }
        if (wrapperDST.getNomOnglet() != null) {
            wrapperDST.setNomOnglet(wrapperDST.getNomOnglet().substring(0, Math.min(wrapperDST.getNomOnglet().length(), 32)));
        }
        // code rubrique mère
        if (!wrapperSRC.isRoot()) {
            boolean parentAlreadyExists = alreadyExists(wrapperSRC.getCodeRubriqueMere(), wrapperSRC.getLangue());
            if (!parentAlreadyExists && !rubriques.containsKey(wrapperSRC.getCodeRubriqueMere())) {
                LOGGER.info("- rubrique mère introuvable : set root");
                wrapperDST.setCodeRubriqueMere("");
            } else {
                wrapperDST.setCodeRubriqueMere(wrapperSRC.getCodeRubriqueMere());
            }
        } else {
            wrapperDST.setCodeRubriqueMere("");
        }
        // catégorie
        if (wrapperSRC.getCategorie() == null || "".equals(wrapperSRC.getCategorie().trim())) {
            if (wrapperSRC == wrapperDST) {
                LOGGER.info("- catégorie absente : no problem");
            } else {
                LOGGER.info("- catégorie absente : ignored value change");
            }
        } else {
            wrapperDST.setCategorie(wrapperSRC.getCategorie());
        }
        // génération de page d'entête ?
        if (wrapperSRC == wrapperDST) {
            if (wrapperSRC.getGenererPageTete() != null && "1".equals(wrapperSRC.getGenererPageTete().trim())) {
                wrapperDST.setTypeRubrique("0001");
                wrapperDST.setPageAccueil("{\"code\":\"?\",\"langue\":\""+wrapperSRC.getLangue()+"\",\"objet\":\"pagelibre\"}\"");
            } else {
                wrapperDST.setGenererPageTete("0");
                if (wrapperSRC.getPageAccueil() != null && !"".equals(wrapperSRC.getPageAccueil().trim())) {
                    wrapperDST.setTypeRubrique(wrapperSRC.getTypeRubrique());
                    wrapperDST.setPageAccueil(wrapperSRC.getPageAccueil());
                } else {
                    LOGGER.info("- aucune définition de CODE_PAGE_TETE : no checking");
                    if (wrapperSRC == wrapperDST) {
                        wrapperDST.setTypeRubrique(wrapperSRC.getTypeRubrique());
                        wrapperDST.setPageAccueil("");
                    } else {
                        LOGGER.info("- type et code : ignored value change");
                    }
                }
            }
        } else {
            LOGGER.info("- rubrique existante : génération de la page d'entête ignorée");
        }
        return ok;
    }

    /**
     * Import de rubriques
     *
     * @param rubriques
     *            : map où les rubriques sont indexées par code
     */
    public boolean importWrappers(Map<String, RubriqueWrapper> rubriques) {
//        LOGGER.info("---> import rubriques start");
//        boolean importOK = true;
//        long pageCodeGeneration = 0;
//        if (rubriques != null && !rubriques.isEmpty()) {
//            Iterator<RubriqueWrapper> iRubrique = rubriques.values().iterator();
//            // l'intégrité peut être brisées si une rubrique fille est importée
//            // mais que sa rubrique mère ne l'est pas... l'opération est donc transactionelle
//            try (ContexteDao ctx = new ContexteDao();
//                Connection connection = ctx.getConnection()){
//                connection.setAutoCommit(false);
//                while (iRubrique.hasNext()) {
//                    RubriqueWrapper wrapper = iRubrique.next();
//                    if (wrapper != null) {
//                        LOGGER.info("-> start import rubrique : " + wrapper.getCode());
//                        // interprétation des valeurs et unicité
//                        if (!prepareWrapper(rubriques, wrapper, wrapper)) {
//                            continue;
//                        }
//                        boolean alreadyExists = alreadyExists(wrapper.getCode(), wrapper.getLangue());
//                        if (alreadyExists) {
//                            LOGGER.info("- la rubrique existe déjà");
//                        } else {
//                            LOGGER.info("- nouvelle rubrique");
//                        }
//                        if (alreadyExists) {
//                            if (!isSynchrone()) {
//                                LOGGER.info("- importation sans synchronisation : rubrique skip");
//                                continue;
//                            } else {
//                                LOGGER.info("- importation avec synchronisation : rubrique will be scratched");
//                                RubriqueWrapper wrapperDST = getOriginalWrapper(wrapper.getCode(), wrapper.getLangue());
//                                if (!prepareWrapper(rubriques, wrapper, wrapperDST)) {
//                                    continue;
//                                } else {
//                                    wrapper = wrapperDST;
//                                }
//                            }
//                        }
//                        // import en base
//                        PreparedStatement stmt = null;
//                        try {
//                            String query = "";
//                            // création de page libre on the fly
//                            if (wrapper.getGenererPageTete() != null && "1".equals(wrapper.getGenererPageTete().trim())) {
//                                LOGGER.info("- génération à la volée d'une page libre pour la rubrique");
//                                PageLibre page = new PageLibre();
//                                // génération auto du code basé sur le temps unix actuel (init) : bidouillage needed
//                                page.init();
//                                // Thread.sleep(10); -> ça dépend trop de la précision du système : donc ça foire
//                                if (pageCodeGeneration == 0) {
//                                    pageCodeGeneration = Long.parseLong(page.getCode());
//                                } else {
//                                    ++pageCodeGeneration;
//                                    page.setCode(String.valueOf(pageCodeGeneration));
//                                }
//                                page.setLangue(wrapper.getLangue());
//                                page.setTitre(wrapper.getIntitule());
//                                page.setCodeRubrique(wrapper.getCode());
//                                query = "insert into PAGELIBRE(CODE, LANGUE, ETAT_OBJET, CODE_RUBRIQUE, TITRE, CONTENU, COMPLEMENTS, META_KEYWORDS, META_DESCRIPTION, CONTENU_ENCADRE, MESSAGE_ALERTE, NB_HITS) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
//                                stmt = connection.prepareStatement(query);
//                                stmt.setObject(1, page.getCode(), Types.VARCHAR);
//                                stmt.setObject(2, page.getLangue(), Types.VARCHAR);
//                                stmt.setObject(3, "0003", Types.VARCHAR);
//                                stmt.setObject(4, page.getCodeRubrique(), Types.VARCHAR);
//                                stmt.setObject(5, page.getTitre(), Types.VARCHAR);
//                                stmt.setObject(6, page.getContenu(), Types.VARCHAR);
//                                stmt.setObject(7, page.getComplements(), Types.VARCHAR);
//                                stmt.setObject(8, page.getMetaKeywords(), Types.VARCHAR);
//                                stmt.setObject(9, page.getMetaDescription(), Types.VARCHAR);
//                                stmt.setObject(10, page.getContenuEncadre(), Types.VARCHAR);
//                                stmt.setObject(11, page.getMessageAlerte(), Types.VARCHAR);
//                                stmt.setObject(12, page.getNbHits(), Types.BIGINT);
//                                LOGGER.info("- SQL Query = " + query);
//                                stmt.executeUpdate();
//                                wrapper.setPageAccueil(wrapper.getPageAccueil());
//                            }
//                            LOGGER.info("- insertion / maj de la rubrique");
//                            if (alreadyExists) {
//                                query = "update RUBRIQUE set INTITULE = ?, NOM_ONGLET = ?, ORDRE = ?, CODE_RUBRIQUE_MERE = ?, CATEGORIE = ? where ID_RUBRIQUE = ?";
//                                stmt = connection.prepareStatement(query);
//                                stmt.setObject(1, wrapper.getIntitule(), Types.VARCHAR);
//                                stmt.setObject(2, wrapper.getNomOnglet(), Types.VARCHAR);
//                                stmt.setObject(3, wrapper.getOrdre(), Types.VARCHAR);
//                                stmt.setObject(4, wrapper.getCodeRubriqueMere(), Types.VARCHAR);
//                                stmt.setObject(5, wrapper.getCategorie(), Types.VARCHAR);
//                                stmt.setObject(6, wrapper.getIdRubrique(), Types.BIGINT);
//                            } else {
//                                query = "insert into RUBRIQUE(CODE, LANGUE, INTITULE, NOM_ONGLET, ORDRE, TYPE_RUBRIQUE, PAGE_ACCUEIL, CODE_RUBRIQUE_MERE, CATEGORIE, ACCROCHE, ID_BANDEAU, ENCADRE, GROUPES_DSI, REQUETES_RUBRIQUE_PUBLICATION) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
//                                stmt = connection.prepareStatement(query);
//                                stmt.setObject(1, wrapper.getCode(), Types.VARCHAR);
//                                stmt.setObject(2, wrapper.getLangue(), Types.VARCHAR);
//                                stmt.setObject(3, wrapper.getIntitule(), Types.VARCHAR);
//                                stmt.setObject(4, wrapper.getNomOnglet(), Types.VARCHAR);
//                                stmt.setObject(5, wrapper.getOrdre(), Types.VARCHAR);
//                                stmt.setObject(6, wrapper.getTypeRubrique(), Types.VARCHAR);
//                                stmt.setObject(7, wrapper.getPageAccueil(), Types.VARCHAR);
//                                stmt.setObject(8, wrapper.getCodeRubriqueMere(), Types.VARCHAR);
//                                stmt.setObject(9, wrapper.getCategorie(), Types.VARCHAR);
//                                stmt.setObject(10, wrapper.getAccroche(), Types.VARCHAR);
//                                stmt.setObject(11, wrapper.getIdBandeau(), Types.BIGINT);
//                                stmt.setObject(12, wrapper.getEncadre(), Types.VARCHAR);
//                                stmt.setObject(13, wrapper.getGroupesDsi(), Types.VARCHAR);
//                                stmt.setObject(14, wrapper.getRequetesRubriquePublication(), Types.VARCHAR);
//                            }
//                            LOGGER.info("- SQL Query = " + query);
//                            stmt.executeUpdate();
//                            stmt.close();
//                        } finally {
//                            if (stmt != null) {
//                                try {
//                                    stmt.close();
//                                } catch (SQLException e) {}
//                            }
//                        }
//                        LOGGER.info("<- end import rubrique : " + wrapper.getCode());
//                    }
//                }
//                connection.commit();
//            } catch (Exception e) {
//                importOK = false;
//                LOGGER.info("- exception caught : all imports canceled");
//                if (LOGGER.isDebugEnabled()) {
//                    LOGGER.debug("exception pendant l'import",e);
//                }
//                try {
//                    if (connection != null && !connection.getAutoCommit()) {
//                        connection.rollback();
//                    }
//                } catch (Exception ee) {}
//            } finally {
//                if (connection != null) {
//                    BasicPoolMgr.releaseConnection(connection);
//                }
//            }
//        } else {
//            LOGGER.info("- no rubriques to import");
//        }
//        LOGGER.info("<--- import rubriques end");
//        return importOK;
        return false;
    }
}
