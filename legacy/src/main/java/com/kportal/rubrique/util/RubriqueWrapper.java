package com.kportal.rubrique.util;

import java.util.ArrayList;
import java.util.List;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.Rubrique;

/**
 * Représente une K-Portal Rubrique et ses sous rubriques. Ce wrapper permet de construire les instances et de les associer sans modifier le framework et surtout de découpler
 * l'instanciation et la persistance des objets Rubrique Pour rendre le tout persistant il faut utiliser les mécanismes d'import
 * 
 * @author emmanuel clisson
 * @deprecated ancien développement v5 pour exporter des rubriques, il n'a jamais été fonctionnel...
 */
@Deprecated
public class RubriqueWrapper implements Cloneable {

    private Rubrique rubrique = null;

    private List<RubriqueWrapper> sousRubriques = null;

    private String genererPageTete = null;

    private String action;

    public RubriqueWrapper(Rubrique rubrique) {
        if (rubrique == null) {
            rubrique = new Rubrique();
            rubrique.init();
        }
        this.rubrique = rubrique;
        sousRubriques = new ArrayList<RubriqueWrapper>();
    }

    public boolean isRoot() {
        return getCodeRubriqueMere() == null || getCodeRubriqueMere().trim().equals("");
    }

    // getters / setters
    public Rubrique getRubrique() {
        return rubrique;
    }

    public void setRubrique(Rubrique rubrique) {
        this.rubrique = rubrique;
    }

    public List<RubriqueWrapper> getSousRubriques() {
        return sousRubriques;
    }

    public void setSousRubriques(List<RubriqueWrapper> sousRubriques) {
        this.sousRubriques = sousRubriques;
    }

    public String getGenererPageTete() {
        return genererPageTete;
    }

    public void setGenererPageTete(String genererPageTete) {
        this.genererPageTete = genererPageTete;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    // delegate methods
    public void add() throws Exception {
        rubrique.add();
    }

    public void delete() throws Exception {
        rubrique.delete();
    }

    public String getAccroche() {
        return rubrique.getAccroche();
    }

    public String getCategorie() {
        return rubrique.getCategorie();
    }

    public String getCode() {
        return rubrique.getCode();
    }

    public String getCodeRubriqueMere() {
        return rubrique.getCodeRubriqueMere();
    }

    public String getContact() {
        return rubrique.getContact();
    }

    public String getCouleurFond() {
        return rubrique.getCouleurFond();
    }

    public String getCouleurTitre() {
        return rubrique.getCouleurTitre();
    }

    public String getEncadre() {
        return rubrique.getEncadre();
    }

    public String getEncadreSousRubrique() {
        return rubrique.getEncadreSousRubrique();
    }

    public String getFormatedAccroche() throws Exception {
        return rubrique.getFormatedAccroche();
    }

    public String getFormatedEncadre() throws Exception {
        return rubrique.getFormatedEncadre();
    }

    public String getGestionEncadre() {
        return rubrique.getGestionEncadre();
    }

    public String getGroupesDsi() {
        return rubrique.getGroupesDsi();
    }

    public Long getIdBandeau() {
        return rubrique.getIdBandeau();
    }

    public Long getIdRubrique() {
        return rubrique.getIdRubrique();
    }

    public String getIntitule() {
        return rubrique.getIntitule();
    }

    public String getLangue() {
        return rubrique.getLangue();
    }

    public String getNomOnglet() {
        return rubrique.getNomOnglet();
    }

    public String getOrdre() {
        return rubrique.getOrdre();
    }

    public String getRequetesRubriquePublication() {
        return rubrique.getRequetesRubriquePublication();
    }

    public String getTypeRubrique() {
        return rubrique.getTypeRubrique();
    }

    public String getPageAccueil() {
        return rubrique.getPageAccueil();
    }

    public void init() {
        rubrique.init();
    }


    public void init(RubriqueBean bean) {
        rubrique.init();
    }

    public boolean nextItem() throws Exception {
        return rubrique.nextItem();
    }

    public void retrieve() throws Exception {
        rubrique.retrieve();
    }

    public int select(String _code, String _langue, String _intitule, String saisi) throws Exception {
        return rubrique.select(_code, _langue, _intitule, saisi);
    }

    public int select(String arg0) throws Exception {
        return rubrique.select(arg0);
    }

    public int selectCodeLangueEtat(String _code, String _langue, String etat) throws Exception {
        return rubrique.selectCodeLangue(_code, _langue);
    }

    public void selectNoCount(String arg0) throws Exception {
        rubrique.select(arg0);
    }

    public void setAccroche(String accroche) {
        rubrique.setAccroche(accroche);
    }

    public void setCategorie(String categorie) {
        rubrique.setCategorie(categorie);
    }

    public void setCode(String code) {
        rubrique.setCode(code);
    }

    public void setCodeRubriqueMere(String codeRubriqueMere) {
        rubrique.setCodeRubriqueMere(codeRubriqueMere);
    }

    public void setContact(String contact) {
        rubrique.setContact(contact);
    }

    public void setCouleurFond(String couleurFond) {
        rubrique.setCouleurFond(couleurFond);
    }

    public void setCouleurTitre(String couleurTitre) {
        rubrique.setCouleurTitre(couleurTitre);
    }

    public void setCtx(OMContext _ctx) {
        rubrique.setCtx(_ctx);
    }

    public void setEncadre(String encadre) {
        rubrique.setEncadre(encadre);
    }

    public void setEncadreSousRubrique(String encadreSousRubrique) {
        rubrique.setEncadreSousRubrique(encadreSousRubrique);
    }

    public void setGestionEncadre(String gestionEncadre) {
        rubrique.setGestionEncadre(gestionEncadre);
    }

    public void setGroupesDsi(String groupesDsi) {
        rubrique.setGroupesDsi(groupesDsi);
    }

    public void setIdBandeau(Long idBandeau) {
        rubrique.setIdBandeau(idBandeau);
    }

    public void setIdRubrique(Long idRubrique) {
        rubrique.setIdRubrique(idRubrique);
    }

    public void setIntitule(String intitule) {
        rubrique.setIntitule(intitule);
    }

    public void setLangue(String langue) {
        rubrique.setLangue(langue);
    }

    public void setNomOnglet(String nomOnglet) {
        rubrique.setNomOnglet(nomOnglet);
    }

    public void setOrdre(String ordre) {
        rubrique.setOrdre(ordre);
    }

    public void setRequetesRubriquePublication(String requetesRubriquePublication) {
        rubrique.setRequetesRubriquePublication(requetesRubriquePublication);
    }

    public void setTypeRubrique(String typeRubrique) {
        rubrique.setTypeRubrique(typeRubrique);
    }

    public void setPageAccueil(String accueil) {
        rubrique.setPageAccueil(accueil);
    }

    @Override
    public String toString() {
        return rubrique.toString();
    }

    public void update() throws Exception {
        rubrique.update();
    }

    // wrapped methods
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Rubrique wrappee = new Rubrique();
        wrappee.init();
        wrappee.setCode(getCode());
        wrappee.setLangue(getLangue());
        wrappee.setIntitule(getIntitule());
        wrappee.setNomOnglet(getNomOnglet());
        wrappee.setTypeRubrique(getTypeRubrique());
        wrappee.setPageAccueil(getPageAccueil());
        wrappee.setCodeRubriqueMere(getCodeRubriqueMere());
        return new RubriqueWrapper(wrappee);
    }
}
