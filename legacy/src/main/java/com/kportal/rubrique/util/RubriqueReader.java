package com.kportal.rubrique.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @deprecated ancien développement v5 pour exporter des rubriques, il n'a jamais été fonctionnel...
 */
@Deprecated
public class RubriqueReader {

    private static final Logger logger = LoggerFactory.getLogger(RubriqueReader.class);

    public RubriqueProvider provider = null;

    public RubriqueReader(RubriqueProvider rubriqueProvider) {
        this.provider = rubriqueProvider;
    }

    public RubriqueProvider getProvider() {
        return provider;
    }

    public void setProvider(RubriqueProvider provider) {
        this.provider = provider;
    }

    /**
     * Instancie des rubriques K-Portal
     *
     * @param paths
     *            : fichier source
     * @return une map des rubriques trouvées (c.f. RubriqueProvider)
     */
    public Map<String, RubriqueWrapper> read(String paths) {
        logger.info("---> lecture : chemins = " + paths);
        Map<String, RubriqueWrapper> rubriques = null;
        if (provider == null) {
            logger.error("- problème de lecture : provider null (extension de fichier non reconnue)");
        } else {
            if (paths != null && !"".equals(paths.trim())) {
                paths = paths.trim();
                StringTokenizer pathTokenizer = new StringTokenizer(paths, ";");
                if (pathTokenizer.hasMoreElements()) {
                    String path = pathTokenizer.nextToken().trim();
                    if (path != null && !"".equals(path)) {
                        logger.info("> parcours du chemin : " + path);
                        File file = new File(path);
                        if (!file.exists()) {
                            logger.info("- chemin inexistant");
                        } else if (!file.canRead()) {
                            logger.info("- pas de droits de lecture sur ce chemin");
                        } else if (file.isHidden()) {
                            logger.info("- chemin caché : données ignorées");
                        } else {
                            if (file.isFile()) {
                                logger.info("- le chemin correspond à un fichier");
                                try {
                                    rubriques = provider.setRubrique(file);
                                    if (rubriques == null) {
                                        logger.info("- impossible de parser le fichier");
                                    } else {
                                        logger.info("- parsing ok");
                                    }
                                } catch (FileNotFoundException e) {
                                    logger.info("- problème lors de la lecture du fichier (fichier non trouvé) : " + e.getMessage());
                                } catch (Exception e) {
                                    logger.info("- problème lors de la lecture du fichier :" + e.getMessage());
                                }
                            } else if (file.isDirectory()) {
                                logger.info("- le chemin correspond à un répertoire : on ne fait rien");
                            } else {
                                logger.info("- chemin de type inconnu");
                            }
                        }
                        logger.info("< chemin parcouru : " + path);
                    }
                }
            }
        }
        logger.info("<--- fin de lecture : " + ((rubriques == null) ? "0" : rubriques.size()) + " rubriques traitées");
        return rubriques;
    }
}
