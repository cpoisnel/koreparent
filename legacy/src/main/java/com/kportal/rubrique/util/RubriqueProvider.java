package com.kportal.rubrique.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

/**
 * Source de rubriques
 * 
 * @author emmanuel clisson
 * @deprecated ancien développement v5 pour exporter des rubriques, il n'a jamais été fonctionnel...
 */
@Deprecated
public abstract class RubriqueProvider {

    /**
     * Donne une map de rubriques. Toutes les rubriques sont accessibles par leur code et sont rangées dans la liste des enfants des autres rubriques (suppression des cycles). les
     * rubriques possèdent l'observateur isRoot pour faciliter les traitements. is
     *
     * @param file
     * @return une map
     * @throws FileNotFoundException
     */
    public abstract Map<String, RubriqueWrapper> setRubrique(File file) throws Exception;
}
