package com.kportal.core.logging;

import java.util.Date;

import ch.qos.logback.classic.Level;

/**
 * @deprecated ne pas utiliser cette classe maison, utilisez plutôt slf4j et logback pour les logs.
 */
@Deprecated
public class ApplicationLog {

    public String server;

    public Date date;

    public Level level;

    public String description;

    public String exception;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
