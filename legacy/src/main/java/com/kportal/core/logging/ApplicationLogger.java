package com.kportal.core.logging;

import java.util.Date;
import java.util.LinkedList;

import com.kportal.core.cluster.ClusterHelper;

import ch.qos.logback.classic.Level;

/**
 * @deprecated ne pas utiliser cette classe maison, utilisez plutôt slf4j et logback pour les logs.
 */
@Deprecated
public class ApplicationLogger {

    public static LinkedList<ApplicationLog> pile = new LinkedList<>();

    public static void reset() {
        pile.clear();
    }

    public static void add(Level level, String message, String exception) {
        ApplicationLog log = new ApplicationLog();
        log.setServer(ClusterHelper.getCurrentJvmRoute());
        log.setDate(new Date(System.currentTimeMillis()));
        log.setLevel(level);
        log.setDescription(message);
        log.setException(exception);
        pile.addFirst(log);
    }

    public static void error(String message, Exception exception) {
        add(Level.ERROR, message, exception.getMessage());
    }

    public static void error(String message) {
        add(Level.ERROR, message, "");
    }

    public static void info(String message, Exception exception) {
        add(Level.INFO, message, exception.getMessage());
    }

    public static void info(String message) {
        add(Level.INFO, message, "");
    }
}
