package com.kportal.core.logging;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.PropertyConfigurer;
import com.kportal.core.webapp.WebAppUtil;

import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP;
import ch.qos.logback.core.rolling.TimeBasedFileNamingAndTriggeringPolicyBase;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;

/**
 * Classe permettant de générer des appender à la volée
 * @deprecated cette classe n'est plus utilisé, la configuration de logback est maintenant faite directement via logback.xml
 */
@Deprecated
public class DefaultRollingFileAppender extends RollingFileAppender<ILoggingEvent> {

    public static final String DEFAULT_NAME = "WEBAPP";

    private String fileName = null;

    private boolean append = true;

    public DefaultRollingFileAppender() {
        super();
        setName(DEFAULT_NAME);
    }

    public DefaultRollingFileAppender(final String name) {
        super();
        this.fileName = name;
        setName(name);
    }

    public DefaultRollingFileAppender(final String name, final String fileName) {
        super();
        this.fileName = fileName;
        setName(name);
    }

    @Override
    public void setAppend(final boolean append) {
        this.append = append;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void start() {
        initDefaultRollingFileAppender();
        super.setAppend(append);
        super.start();
    }

    public void initDefaultRollingFileAppender() {
        /* NB : la lecture des propriétés utilise ici directement la classe PropertyConfigurer car la classe utilitaire PropertyHelper n'est pas encore chargée à ce moment là
         * il est également impossible d'uiliser les raccourcis vers les paths de la classe WebAppUtil
         */
        String fileName = getFileName();
        if (StringUtils.isEmpty(fileName)) {
            fileName = PropertyConfigurer.getProperty(LogHelper.PROP_LOG_FILE);
            setFileName(fileName);
        }
        // on ajoute le prefix du serveur jvmroute
        fileName = LogHelper.getServerLogName(fileName);
        final String logPath = WebAppUtil.getLogsPath();
        setFile(logPath + File.separator + fileName + LogHelper.LOG_EXTENSION);
        // Pattern de l'encoder
        final PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setPattern(PropertyConfigurer.getProperty(LogHelper.PROP_LOG_LAYOUT_PATTERN));
        encoder.setContext(getContext());
        // Système de trigger
        TimeBasedFileNamingAndTriggeringPolicyBase<ILoggingEvent> timeTrigger;
        // taille max des fichiers
        if (StringUtils.isNotEmpty(PropertyConfigurer.getProperty(LogHelper.PROP_LOG_MAX_SIZE))) {
            timeTrigger = new SizeAndTimeBasedFNATP<>();
            ((SizeAndTimeBasedFNATP<ILoggingEvent>) timeTrigger).setMaxFileSize(PropertyConfigurer.getProperty(LogHelper.PROP_LOG_MAX_SIZE));
        } else {
            timeTrigger = new DefaultTimeBasedFileNamingAndTriggeringPolicy<>();
        }
        timeTrigger.setContext(getContext());
        // Système de roulement des fichiers (en fonction du jour)
        final TimeBasedRollingPolicy<ILoggingEvent> policy = new TimeBasedRollingPolicy<>();
        policy.setContext(getContext());
        // nom du fichier archivé
        policy.setFileNamePattern(logPath + File.separator + fileName + LogHelper.LOG_EXTENSION + "." + PropertyConfigurer.getProperty(LogHelper.PROP_LOG_FILENAME_PATTERN));
        policy.setTimeBasedFileNamingAndTriggeringPolicy(timeTrigger);
        // maximum d'archive
        if (StringUtils.isNotEmpty(PropertyConfigurer.getProperty(LogHelper.PROP_LOG_MAX_HISTORY))) {
            try {
                policy.setMaxHistory(Integer.parseInt(PropertyConfigurer.getProperty(LogHelper.PROP_LOG_MAX_HISTORY)));
            } catch (final Exception e) {
                System.err.println("La valeur de la propriété \"" + LogHelper.PROP_LOG_MAX_HISTORY + "\" n'est pas numérique");
            }
        }
        // clean des fichiers au démarrage
        if (StringUtils.isNotEmpty(PropertyConfigurer.getProperty(LogHelper.PROP_LOG_CLEAN_HISTORY_ON_START))) {
            policy.setCleanHistoryOnStart("1".equals(PropertyConfigurer.getProperty(LogHelper.PROP_LOG_CLEAN_HISTORY_ON_START)));
        }
        // Application des trigger
        timeTrigger.setTimeBasedRollingPolicy(policy);
        setEncoder(encoder);
        setRollingPolicy(policy);
        setTriggeringPolicy(timeTrigger);
        policy.setParent(this);
        encoder.start();
        policy.start();
    }
}
