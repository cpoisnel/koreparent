package com.jsbsoft.jtf.core;

import java.util.Enumeration;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.PropertyConfigurer;

/**
 * Classe contenant les paramètres de chaque processus
 * @deprecated Remplacer par {@link com.kportal.core.config.PropertyHelper]
 */
@Deprecated
public class ParametresProcessus {

    /**
     * Renvoie la classe associée au processus.
     *
     * @param key
     *            the key
     *
     * @return the property
     * @deprecated cette méthode est remplacée par {@link com.kportal.core.config.PropertyHelper#getCoreProperty(String)}.
     * Si le label n'est pas présent sur votre projet ou sur le core, mais qu'il est dans une extension, il faut utiliser
     * {@link com.kportal.core.config.PropertyHelper#getProperty(String, String)} ou le premier argument correspond à l'id de votre extension.
     */
    @Deprecated
    public static String getProperty(String key) {
        return StringUtils.trim(getProperties().getProperty(key));
    }

    /**
     * Rafraichissement des propriétés de l'application ne sfait plus rien
     */
    public static void refresh() {}

    private static Properties getProperties() {
        return PropertyConfigurer.getInstance().getProperties();
    }

    /**
     * Renvoie la classe associée au processus.
     *
     * @return the liste properties
     */
    public static Enumeration<?> getListeProperties() {
        return getProperties().keys();
    }
}
