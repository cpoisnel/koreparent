package com.jsbsoft.jtf.core;

import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.RequeteMgr;
import com.jsbsoft.jtf.exception.ErreurApplicative;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

/**
 * Logger générique appelable par n'importe quelle classe et basé sur slf4j.
 * @deprecated cette classe est totalement obsolête, il faut passer par le logger de slf4j.
 */
@Deprecated
public class Log {

    /** Le rootLogger de l'application. */
    private Logger log = null;

    /** Singleton. */
    private static Log instance = null;

    /**
     * Constructeur privé.
     */
    private Log() {
        this.log = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
    }

    /**
     * Renvoie le singleton.
     *
     * @return L'unique instance de cette classe
     */
    public static Log getInstance() {
        if (instance == null) {
            syncGetInstance();
        }
        return instance;
    }

    /**
     * Sync get instance.
     */
    private static synchronized void syncGetInstance() {
        if (instance == null) {
            instance = new Log();
        }
    }

    /**
     * Renvoie true si le niveau de log DEBUG est activé.
     *
     * @return true, if checks if is debug enabled
     */
    public static boolean isDebugEnabled() {
        return getLogger().isEnabledFor(Level.DEBUG);
    }

    /**
     * Renvoie true si le niveau de log INFO est activé.
     *
     * @return true, if checks if is info enabled
     */
    public static boolean isInfoEnabled() {
        return getLogger().isEnabledFor(Level.INFO);
    }

    /**
     * Renvoie true si le niveau de log WARN est activé.
     *
     * @return true, if checks if is warn enabled
     */
    public static boolean isWarnEnabled() {
        return getLogger().isEnabledFor(Level.WARN);
    }

    /**
     * Renvoie true si le niveau de log ERROR est activé.
     *
     * @return true, if checks if is error enabled
     */
    public static boolean isErrorEnabled() {
        return getLogger().isEnabledFor(Level.ERROR);
    }

    /**
     * Logge le message si le niveau de log DEBUG est activé.
     *
     * @param message
     *            the message
     */
    public static void debug(String message) {
        getLogger().debug(message);
    }

    /**
     * Logge le message si le niveau de log DEBUG est activé.
     *
     * @param message
     *            Le message
     * @param t
     *            L'exception
     */
    public static void debug(String message, Throwable t) {
        getLogger().debug(message, t);
    }

    /**
     * Logge le message si le niveau de log INFO est activé.
     *
     * @param message
     *            Le message
     */
    public static void info(String message) {
        getLogger().info(message);
    }

    /**
     * Logge le message si le niveau de log WARN est activé.
     *
     * @param message
     *            Le message
     */
    public static void warn(String message) {
        getLogger().warn(message);
    }

    /**
     * Logge le message si le niveau de log WARN est activé.
     *
     * @param message
     *            Le message
     * @param t
     *            the t
     */
    public static void warn(String message, Throwable t) {
        getLogger().warn(message, t);
    }

    /**
     * Logge le message si le niveau de log ERROR est activé.
     *
     * @param message
     *            Le message
     */
    public static void error(String message) {
        getLogger().error(message);
    }

    /**
     * Logge l'exception si le niveau de log ERROR est activé.
     *
     * @param message
     *            Le message
     * @param t
     *            L'exception
     */
    public static void error(String message, Throwable t) {
        getLogger().error(message, t);
        // affichage dump requete
        String dump = RequeteMgr.dumpDernieresRequetesNonTerminees();
        if (dump.length() > 0) {
            getLogger().warn("****  DUMP EXCEPTION         ****");
            getLogger().warn("**** LISTE REQUETES EN COURS ");
            getLogger().warn(dump);
            RequeteMgr.purgerRequetesNonTerminees();
            getLogger().warn("****  FIN DUMP               ****");
        }
    }

    /**
     * Logge l'exception si le niveau de log ERROR est activé.
     *
     * @param t
     *            L'exception
     */
    public static void error(Throwable t) {
        if (t instanceof ErreurApplicative) {
            warn(t.getMessage());
        } else {
            error(t.getMessage(), t);
        }
    }

    /**
     * Renvoie le rootLogger de l'application.
     *
     * @return t L'exception
     */
    private static Logger getLogger() {
        return getInstance().log;
    }
}
