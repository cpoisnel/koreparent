package com.jsbsoft.jtf.datasource.pool;

import java.sql.Connection;

/**
 * @deprecated utilisé un objet de type {@link com.univ.utils.ContexteDao}
 * en mode "try with resource" pour obtenir une connexion au datasource HikariCP.
 */
@Deprecated
public interface DataSourcePoolManager {

    public Connection getConnection();

    public void releaseConnection(Connection _connection);
}
