/*
 * Created on 21 févr. 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.jsbsoft.jtf.database;

import com.kportal.core.config.PropertyHelper;

/**
 * The Class DatabaseServerManager.
 *
 * @author administrateur
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 * @deprecated cette classe n'a plus d'utilité depuis pas mal de version...
 */
@Deprecated
public abstract class DatabaseServerManager {

    /** The instance. */
    private static DatabaseServerManager instance = null;

    /**
     * Gets the single instance of DatabaseServerManager.
     *
     * @return single instance of DatabaseServerManager
     */
    public static DatabaseServerManager getInstance() {
        if (instance == null) {
            final String serveur = PropertyHelper.getCoreProperty("datastore.default.serveur");
            if ((serveur == null) || (serveur.equals("mysql"))) {
                instance = new MySQLDatabaseManager();
            } else if (serveur.equals("sqlserver")) {
                instance = new SQLServerDatabaseManager();
            }
        }
        return instance;
    }
    //generation d'un ID pour un objet

    /**
     * Gets the object id.
     *
     * @return the object id
     */
    public static Long getObjectId() {
        return getInstance().genererObjectId();
    }

    /**
     * Gets the requete clause limit.
     *
     * @param table
     *            the table
     * @param baseQuery
     *            the base query
     * @param sqlSuffix
     *            the sql suffix
     * @param debut
     *            the debut
     * @param increment
     *            the increment
     *
     * @return the requete clause limit
     */
    public static String getRequeteClauseLimit(final String table, final String baseQuery, final String sqlSuffix, final int debut, final int increment) {
        return getInstance().genererRequeteClauseLimit(table, baseQuery, sqlSuffix, debut, increment);
    }
    //generéation des requêtes comportant une clause LIMIT

    /**
     * Gets the requete select count.
     *
     * @param table
     *            the table
     * @param qualifier
     *            the qualifier
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the requete select count
     */
    public static String getRequeteSelectCount(final String table, final String qualifier, final String sqlSuffix) {
        return getInstance().genererRequeteSelectCount(table, qualifier, sqlSuffix);
    }

    /**
     * Gets the requete select distinct count.
     *
     * @param table
     *            the table
     * @param qualifier
     *            the qualifier
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the requete select distinct count
     */
    public static String getRequeteSelectDistinctCount(final String table, final String qualifier, final String sqlSuffix) {//implémenter que en SLQ server
        return getInstance().genererRequeteSelectDistinctCount(table, qualifier, sqlSuffix);
    }
    //génération des requêtes du type SELECT COUNT(*) avec une clause ORDER BY (passe pas avec SqlServer)

    /**
     * Generer object id.
     *
     * @return the long
     */
    public abstract Long genererObjectId();

    /**
     * Generer requete clause limit.
     *
     * @param table
     *            the table
     * @param baseQuery
     *            the base query
     * @param sqlSuffix
     *            the sql suffix
     * @param debut
     *            the debut
     * @param increment
     *            the increment
     *
     * @return the string
     */
    public abstract String genererRequeteClauseLimit(String table, String baseQuery, String sqlSuffix, int debut, int increment);
    // génération des requêtes du type SELECT COUNT(DISTINCT *) avec une clause ORDER BY (passe pas avec SqlServer)

    /**
     * Generer requete select count.
     *
     * @param table
     *            the table
     * @param qualifier
     *            the qualifier
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the string
     */
    public abstract String genererRequeteSelectCount(String table, String qualifier, String sqlSuffix);

    /**
     * Generer requete select distinct count.
     *
     * @param table
     *            the table
     * @param qualifier
     *            the qualifier
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the string
     */
    public abstract String genererRequeteSelectDistinctCount(String table, String qualifier, String sqlSuffix);
}
