/*
 * Created on 22 févr. 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.jsbsoft.jtf.database;

/**
 * The Class SQLServerDatabaseManager.
 *
 * @author administrateur
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments

 * @deprecated cette classe n'a plus de raison d'être
 */
@Deprecated
public class SQLServerDatabaseManager extends DatabaseServerManager {

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.DatabaseServerManager#genererObjectId()
     */
    @Override
    public Long genererObjectId() {
        return System.currentTimeMillis();
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.DatabaseServerManager#genererRequeteClauseLimit(java.lang.String, java.lang.String, java.lang.String, int, int)
     */
    @Override
    public String genererRequeteClauseLimit(String table, String baseQuery, String sqlSuffix, int debut, int increment) {
        String requete = "select * from (select top " + increment + " * from (select top " + (debut + increment) + " * from " + table + " " + sqlSuffix + ") as T2 " + sqlSuffix + " DESC) as T1 " + sqlSuffix + "";
        return requete;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.DatabaseServerManager#genererRequeteSelectCount(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String genererRequeteSelectCount(String table, String qualifier, String sqlSuffix) {
        String sqlCount = "";
        if (sqlSuffix.contains("ORDER BY")) {
            sqlCount = sqlSuffix.substring(0, sqlSuffix.indexOf("ORDER BY"));
        } else {
            sqlCount = sqlSuffix;
        }
        return "SELECT COUNT(*) FROM " + qualifier + table + " T1 " + sqlCount;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.DatabaseServerManager#genererRequeteSelectDistinctCount(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String genererRequeteSelectDistinctCount(String table, String qualifier, String sqlSuffix) {
        String sqlCount = "";
        if (sqlSuffix.contains("ORDER BY")) {
            sqlCount = sqlSuffix.substring(0, sqlSuffix.indexOf("ORDER BY"));
        } else {
            sqlCount = sqlSuffix;
        }
        return "SELECT COUNT(DISTINCT T1.ID_" + table + ") FROM " + qualifier + table + " T1 " + sqlCount;
    }
}
