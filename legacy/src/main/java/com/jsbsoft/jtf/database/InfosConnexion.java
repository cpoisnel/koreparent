/*
 * Created on 28 juin 2004
 *
 * Permet de stocker les informations associées
 * à chaque connexion JDBC
 */
package com.jsbsoft.jtf.database;

import java.sql.Connection;

/**
 * The Class InfosConnexion.
 *
 * @author jean-sébastien steux
 *
 *         Permet de stocker les informations associées à chaque connexion JDBC
 * @deprecated cette classe permettait de récupérer les infos de l'ancien pool de connexion maison. Utilisez {@link com.jsbsoft.jtf.datasource.pool.PoolInfos} maintenant
 */
@Deprecated
public class InfosConnexion {

    /** The est disponible. */
    private boolean estDisponible = false;

    /** The est longue. */
    private boolean estLongue = false;

    /** The ts allocation. */
    private long tsAllocation = 0;

    /** The ts liberation. */
    private long tsLiberation = 0;

    /** The connection. */
    private Connection connection = null;

    private StackTraceElement[] stackTrace = null;

    /**
     * Gets the connection.
     *
     * @return Returns the connection.
     */
    protected Connection getConnection() {
        return connection;
    }

    /**
     * Sets the connection.
     *
     * @param connection
     *            The connection to set.
     */
    protected void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Gets the ts allocation.
     *
     * @return Returns the tsAllocation.
     */
    protected long getTsAllocation() {
        return tsAllocation;
    }

    /**
     * Sets the ts allocation.
     *
     * @param tsAllocation
     *            The tsAllocation to set.
     */
    protected void setTsAllocation(long tsAllocation) {
        this.tsAllocation = tsAllocation;
    }

    /**
     * Gets the ts liberation.
     *
     * @return Returns the tsLiberation.
     */
    protected long getTsLiberation() {
        return tsLiberation;
    }

    /**
     * Sets the ts liberation.
     *
     * @param tsLiberation
     *            The tsLiberation to set.
     */
    protected void setTsLiberation(long tsLiberation) {
        this.tsLiberation = tsLiberation;
    }

    /**
     * Est disponible.
     *
     * @return Returns the estDisponible.
     */
    public boolean estDisponible() {
        return estDisponible;
    }

    /**
     * Sets the disponible.
     *
     * @param estDisponible
     *            The estDisponible to set.
     */
    protected void setDisponible(boolean estDisponible) {
        this.estDisponible = estDisponible;
    }

    /**
     * Est longue.
     *
     * @return Returns the estLongue.
     */
    public boolean estLongue() {
        return estLongue;
    }

    /**
     * Sets the longue.
     *
     * @param estLongue
     *            The estLongue to set.
     */
    protected void setLongue(boolean estLongue) {
        this.estLongue = estLongue;
    }

    public StackTraceElement[] getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(StackTraceElement[] stackTrace) {
        this.stackTrace = stackTrace;
    }
}
