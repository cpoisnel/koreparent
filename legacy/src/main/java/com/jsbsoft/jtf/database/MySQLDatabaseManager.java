/*
 * Created on 22 févr. 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.jsbsoft.jtf.database;

/**
 * The Class MySQLDatabaseManager.
 *
 * @author administrateur
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 * @deprecated cette classe n'a plus de raison d'être
 */
@Deprecated
public class MySQLDatabaseManager extends DatabaseServerManager {

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.DatabaseServerManager#genererObjectId()
     */
    @Override
    public Long genererObjectId() {
        return (long) 0;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.DatabaseServerManager#genererRequeteClauseLimit(java.lang.String, java.lang.String, java.lang.String, int, int)
     */
    @Override
    public String genererRequeteClauseLimit(String table, String baseQuery, String sqlSuffix, int debut, int increment) {
        String requete = baseQuery + sqlSuffix + " LIMIT " + debut + "," + (debut + increment) + "";
        return requete;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.DatabaseServerManager#genererRequeteSelectCount(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String genererRequeteSelectCount(String table, String qualifier, String sqlSuffix) {
        return "SELECT COUNT(*) FROM " + qualifier + table + " T1 " + sqlSuffix;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.DatabaseServerManager#genererRequeteSelectDistinctCount(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String genererRequeteSelectDistinctCount(String table, String qualifier, String sqlSuffix) {
        // return "SELECT COUNT(DISTINCT *) FROM " + qualifier + table+" T1 " + sqlSuffix;
        return ""; //code à implémenter suivant les besoins
    }
}
