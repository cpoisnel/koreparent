package com.kportal.ihm.utils;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.manager.DataSourceDAOManager;
import com.jsbsoft.jtf.datasource.manager.impl.BasicDataSourceDAOManager;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.extension.module.composant.Menu;
import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.kportal.extension.module.plugin.rubrique.impl.BeanFichePageAccueil;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.PageLibre;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriqueMock;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.UnivWebFmt;

import mockit.Capturing;
import mockit.Expectations;
import mockit.Mocked;

public class FrontUtilTest {

    @Mocked(stubOutClassInitialization = true) ServiceManager mockedServiceManager;
    @Mocked(stubOutClassInitialization = true) ApplicationContextManager mockedAppCtx;

    @Test
    public void testIsAccueilSite(@Mocked("determineSite") ServiceInfosSiteProcessus mockedServiceInfosSiteProcessus) {
        final ServiceInfosSiteProcessus serviceInfosSiteProcessus = new ServiceInfosSiteProcessus();
        final InfosSiteImpl siteDeTest = new InfosSiteImpl();
        siteDeTest.setCodeRubrique("test");
        new Expectations() {{
            ApplicationContextManager.getCoreContextBean(DataSourceDAOManager.ID_BEAN, DataSourceDAOManager.class);
            result = new BasicDataSourceDAOManager();
            ServiceManager.getServiceForBean(InfosSiteImpl.class);
            result = serviceInfosSiteProcessus;
            serviceInfosSiteProcessus.determineSite(anyString, true);
            result = siteDeTest;
        }};
        FicheUniv ficheAccueilSite = new PageLibre();
        ficheAccueilSite.setCodeRubrique("test");

        Assert.assertFalse(FrontUtil.isAccueilSite(null), "Si la fiche est null elle ne peut être la page d'accueil du site");
        Assert.assertTrue(FrontUtil.isAccueilSite(ficheAccueilSite), "La fiche possede le bon code de rubrique, elle doit être la page d'accueil du site");
    }

    @Test
    public void testIsFicheAccueilRubrique(@Mocked("determineSite") ServiceInfosSiteProcessus mockedServiceInfosSiteProcessus,
        @Mocked(stubOutClassInitialization = true) final ReferentielObjets mockedReferentiel,
        @Mocked final PageAccueilRubriqueManager mockedPageManager) {
        final ServiceInfosSiteProcessus serviceInfosSiteProcessus = new ServiceInfosSiteProcessus();
        final ServiceRubrique serviceRubrique = new ServiceRubriqueMock().getMockInstance();
        final InfosSiteImpl siteDeTest = new InfosSiteImpl();
        siteDeTest.setCodeRubrique(ServiceRubriqueMock.CODE_SITE);
        new Expectations() {{
            ReferentielObjets.getNomObjet((FicheUniv) any);
            result = StringUtils.EMPTY;
            ServiceManager.getServiceForBean(InfosSiteImpl.class);
            result = serviceInfosSiteProcessus;
            ServiceManager.getServiceForBean(RubriqueBean.class);
            result = serviceRubrique;
            serviceInfosSiteProcessus.determineSite(anyString, true);
            result = siteDeTest;
            ApplicationContextManager.getCoreContextBean(DataSourceDAOManager.ID_BEAN, DataSourceDAOManager.class);
            result = new BasicDataSourceDAOManager();;
        }};
        FicheUniv ficheAccueilRubrique = new PageLibre();
        ficheAccueilRubrique.setCode(StringUtils.EMPTY);
        ficheAccueilRubrique.setLangue(StringUtils.EMPTY);
        ficheAccueilRubrique.setCodeRubrique(ServiceRubriqueMock.CODE_NAVIGATION);
        final PageAccueilRubriqueManager mockedManager = new PageAccueilRubriqueManager();
        new Expectations() {{
            PageAccueilRubriqueManager.getInstance();
            result = mockedManager;
            mockedManager.getBeanPageAccueil((RubriqueBean) any);
            result = new BeanFichePageAccueil();
        }};
        Assert.assertFalse(FrontUtil.isFicheAccueilRubrique(null), "Si la fiche est null, cela ne peut pas être la page d'accueil de rubrique");
        Assert.assertTrue(FrontUtil.isFicheAccueilRubrique(ficheAccueilRubrique), "la fiche doit être la page d'accueil de la rubrique");
        new Expectations() {{
            ReferentielObjets.getNomObjet((FicheUniv) any);
            result = "test";
        }};
        Assert.assertFalse(FrontUtil.isFicheAccueilRubrique(ficheAccueilRubrique), "Si le nom de l'objet ne correspond pas, cela ne peut pas être la page d'accueil du site");
        Assert.assertFalse(FrontUtil.isFicheAccueilRubrique(null, null), "Si la fiche et la rubrique sont null, ce n'est pas la page d'accueil");
        Assert.assertFalse(FrontUtil.isFicheAccueilRubrique(ficheAccueilRubrique, null), "Si la rubrique est null, ce n'est pas la page d'accueil");
        FicheUniv ficheAccueilRubriqueSite = new PageLibre();
        ficheAccueilRubriqueSite.setCodeRubrique(ServiceRubriqueMock.CODE_SITE);
        Assert.assertFalse(FrontUtil.isFicheAccueilRubrique(ficheAccueilRubriqueSite), "la fiche ne doit pas être la page d'accueil de la rubrique de site");
        FicheUniv ficheAccueilRubriqueLangue = new PageLibre();
        ficheAccueilRubriqueLangue.setCodeRubrique(ServiceRubriqueMock.CODE_LANGUE);
        Assert.assertFalse(FrontUtil.isFicheAccueilRubrique(ficheAccueilRubriqueLangue), "la fiche ne doit pas être la page d'accueil de la rubrique de langue");
        new Expectations() {{
            mockedManager.getBeanPageAccueil((RubriqueBean) any);
            result = null;
        }};
        Assert.assertFalse(FrontUtil.isFicheAccueilRubrique(ficheAccueilRubrique), "on ne peut pas avoir de page d'accueil si on ne peut pas determiner la page d'accueil de la rubrique");
    }

    @Test
    public void testGetMenuPrincipalParCategorie(@Capturing final ContexteUniv mockedCtx,  @Mocked("getById") ServiceMedia mockedServiceMedia, @Mocked(stubOutClassInitialization = true) UnivWebFmt univWebFmt) throws Exception {
        final ContexteUniv ctx = ContexteUtil.setContexteSansRequete();
        final ServiceMedia serviceMedia = new ServiceMedia();
        final InfosSiteImpl site = new InfosSiteImpl();
        site.setCodeRubrique(ServiceRubriqueMock.CODE_SITE);
        new Expectations() {{
            ctx.getInfosSite();
            result = site;
            ctx.getLangue();
            result = "0";
            ServiceManager.getServiceForBean(MediaBean.class);
            result = serviceMedia;
            ServiceManager.getServiceForBean(RubriqueBean.class);
            result = new ServiceRubriqueMock().getMockInstance();
            serviceMedia.getById(anyLong);
            result = null;
            UnivWebFmt.renvoyerUrlAccueilRubrique((ContexteUniv)any, anyString);
            result = StringUtils.EMPTY;
        }};
        List<Menu> result = FrontUtil.getMenuPrincipalParCategorie();
        Assert.assertNotNull(result, "La liste de menu ne peut pas être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir un seul élément de menu");
        Assert.assertEquals(result.get(0).getLibelle(), ServiceRubriqueMock.CODE_NIVEAU1, "l'element du menu doit avoir comme titre niveau1");
    }

    @Test
    public void testGetRubriqueDepuisCategorieEtRubriqueDeSite() {
        new Expectations() {{
            ServiceManager.getServiceForBean(RubriqueBean.class);
            result = new ServiceRubriqueMock().getMockInstance();
        }};
        final RubriqueBean rubriqueDeRetour = FrontUtil.getRubriqueDepuisCategorieEtRubriqueDeSite(FrontUtil.CATEGORIE_NAVIGATION, ServiceRubriqueMock.CODE_LANGUE);
        Assert.assertNotNull(rubriqueDeRetour, "La rubrique de retour ne peut etre null lorsque les paramètres sont corrects");
        Assert.assertEquals(rubriqueDeRetour.getCategorie(), FrontUtil.CATEGORIE_NAVIGATION, "la catégorie doit être " + FrontUtil.CATEGORIE_NAVIGATION);
        Assert.assertEquals(rubriqueDeRetour.getIntitule(), ServiceRubriqueMock.CODE_NAVIGATION, "l'intitulé doit être " + ServiceRubriqueMock.CODE_NAVIGATION);
        Assert.assertNull(FrontUtil.getRubriqueDepuisCategorieEtRubriqueDeSite(null, "foo"), "La rubrique est null si la catégorie est null");
        Assert.assertNull(FrontUtil.getRubriqueDepuisCategorieEtRubriqueDeSite("foo", null), "La rubrique est null si le code de rubrique est null");
        Assert.assertNull(FrontUtil.getRubriqueDepuisCategorieEtRubriqueDeSite("", "bar"), "La rubrique est null si la catégorie est vide");
        Assert.assertNull(FrontUtil.getRubriqueDepuisCategorieEtRubriqueDeSite("bar", ""), "La rubrique est null si le code de rubrique est vide");
        Assert.assertNull(FrontUtil.getRubriqueDepuisCategorieEtRubriqueDeSite("bar", ""), "La rubrique est null si la catégorie n'existe pas");
        Assert.assertNull(FrontUtil.getRubriqueDepuisCategorieEtRubriqueDeSite(FrontUtil.CATEGORIE_NAVIGATION, "bar"), "La rubrique est null si le code de rubrique n'existe pas");
    }
}