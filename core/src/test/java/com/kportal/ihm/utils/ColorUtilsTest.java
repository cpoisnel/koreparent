package com.kportal.ihm.utils;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by olivier.camon on 25/03/15.
 */
public class ColorUtilsTest {

    @Test
    public void testDarker() {
        Assert.assertEquals(ColorUtils.darker("foobar", 0), StringUtils.EMPTY, "la chaine doit être vide si ce n'est pas une valeur valide");
        Assert.assertEquals(ColorUtils.darker("CCD00", 0), StringUtils.EMPTY, "la chaine doit être vide si ce n'est pas une couleur valide");
        Assert.assertEquals(ColorUtils.darker("#CCD00", 0), StringUtils.EMPTY, "la chaine doit être vide si ce n'est pas une couleur valide");
        Assert.assertEquals(ColorUtils.darker("fff", 0), "#ffffff", "Si le ratio est de 0, la couleur n'est pas changé");
        Assert.assertEquals(ColorUtils.darker("#ffffff", 1), "#000000", "Si le ratio est de 1, la couleur est #000000");
        Assert.assertEquals(ColorUtils.darker("ffffff", 0.5), "#7f7f7f", "Si le ratio est de 0.5, la couleur est #7f7f7f");
        Assert.assertEquals(ColorUtils.darker("#d2004c", 0.8), "#2a000f", "La couleur est valide même si le code RGB est que sur un seul caractère par couleur (F au lieu de OF");
        Assert.assertEquals(ColorUtils.darker("#0f445d", 0.3), "#0a2f41", "La couleur est valide même si le code RGB est que sur un seul caractère par couleur (F au lieu de OF");
    }

    @Test
    public void testLighter() {
        Assert.assertEquals(ColorUtils.lighter("foobar", 0), StringUtils.EMPTY, "la chaine doit être vide si ce n'est pas une valeur valide");
        Assert.assertEquals(ColorUtils.lighter("CCD00", 0), StringUtils.EMPTY, "la chaine doit être vide si ce n'est pas une couleur valide");
        Assert.assertEquals(ColorUtils.lighter("#CCD00", 0), StringUtils.EMPTY, "la chaine doit être vide si ce n'est pas une couleur valide");
        Assert.assertEquals(ColorUtils.lighter("#CCD00F", 0), "#ccd00f", "La couleur est valide même si le code RGB est que sur un seul caractère par couleur (F au lieu de OF");
        Assert.assertEquals(ColorUtils.lighter("000", 0), "#000000", "Si le ratio est de 0, la couleur n'est pas changé");
        Assert.assertEquals(ColorUtils.lighter("#000000", 1), "#ffffff", "Si le ratio est de 1, la couleur est #ffffff");
        Assert.assertEquals(ColorUtils.lighter("000000", 0.5), "#7f7f7f", "Si le ratio est de 0.5, la couleur est #7f7f7f");
    }

    @Test
    public void testGetRGBaFromHexa() {
        Assert.assertNull(ColorUtils.getRGBaFromHexa(null, 0.1), "la chaine doit être null lorsque la valeur est null");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa(StringUtils.EMPTY, 0.1), StringUtils.EMPTY, "la chaine doit être vide lorsque la valeur est vide");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa("hello world", 0.1), "hello world", "la chaine doit être la même qu'en paramètre lorsque la valeur n'est pas valide");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa("FFFFFF", 0.1), "rgba(255,255,255,0.1)", "la chaine doit être rgba(255,255,255,0,1) lorsque la valeur est un hexa valide avec une tranparence de 0.1");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa("#FFFFFF", 0.1), "rgba(255,255,255,0.1)", "la chaine doit être rgba(255,255,255,0,1) lorsque la valeur est un hexa valide précédé de #");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa("FFFF", 0.1), "FFFF", "la chaine doit être la même lorsque la valeur est un hexa mais d'une taille incorrecte");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa("FFFFFF", 0), "rgba(255,255,255,0.0)", "la chaine doit être rgba(255,255,255,0) lorsque la valeur est un hexa valide");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa("FFFFFF", 1), "rgba(255,255,255,1.0)", "la chaine doit être rgba(255,255,255,1) lorsque la valeur est un hexa valide");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa("FFFFFF", 2), "FFFFFF", "la chaine doit être la même lorsque la valeur alpha n'est pas valide");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa("FFF", 0), "rgba(255,255,255,0.0)", "la chaine doit être rgba(255,255,255,0) lorsque la valeur est un hexa valide");
        Assert.assertEquals(ColorUtils.getRGBaFromHexa("#FFF", 0), "rgba(255,255,255,0.0)", "la chaine doit être rgba(255,255,255,0) lorsque la valeur est un hexa valide");
    }
}
