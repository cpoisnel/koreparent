package com.kportal.extension.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.kportal.extension.bean.ExtensionBean;

/**
 * Created by olivier.camon on 02/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/kportal/extension/dao/ExtensionDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class ExtensionDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    ExtensionDAO extensionDAO;

    @Test
    @DatabaseSetup("ExtensionDAOTest.add.xml")
    @ExpectedDatabase("ExtensionDAOTest.add-expected.xml")
    public void testAdd() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final ExtensionBean extensionBean = new ExtensionBean();
        extensionBean.setId(2L);
        extensionBean.setLibelle("LIBELLE2");
        extensionBean.setIdBean("ID_BEAN");
        extensionBean.setDateCreation(format.parse("1970-01-01 00:00:00"));
        extensionBean.setDateModification(format.parse("1970-01-01 00:00:00"));
        extensionBean.setEtat(0);
        extensionBean.setType(0);
        extensionBean.setTables("TABLES");
        extensionBean.setVersion("VERSION");
        extensionDAO.add(extensionBean);
    }

    @Test
    @DatabaseSetup("ExtensionDAOTest.add.xml")
    @ExpectedDatabase("ExtensionDAOTest.add-expected.xml")
    public void testAddWithForceId() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final ExtensionBean extensionBean = new ExtensionBean();
        extensionBean.setId(2L);
        extensionBean.setLibelle("LIBELLE2");
        extensionBean.setIdBean("ID_BEAN");
        extensionBean.setDateCreation(format.parse("1970-01-01 00:00:00"));
        extensionBean.setDateModification(format.parse("1970-01-01 00:00:00"));
        extensionBean.setEtat(0);
        extensionBean.setType(0);
        extensionBean.setTables("TABLES");
        extensionBean.setVersion("VERSION");
        extensionDAO.addWithForcedId(extensionBean);
    }

    @Test
    @DatabaseSetup("ExtensionDAOTest.update.xml")
    @ExpectedDatabase("ExtensionDAOTest.update-expected.xml")
    public void testUpdate() {
        final ExtensionBean extensionBean = extensionDAO.getById(2L);
        extensionBean.setIdBean("ID_BEAN2");
        extensionDAO.update(extensionBean);
    }

    @Test
    @DatabaseSetup("ExtensionDAOTest.add-expected.xml")
    @ExpectedDatabase("ExtensionDAOTest.add.xml")
    public void testDelete() {
        extensionDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("ExtensionDAOTest.update.xml")
    public void testById() {
        final ExtensionBean extensionBean = extensionDAO.getById(2L);
        Assert.assertNotNull(extensionBean);
        Assert.assertEquals(extensionBean.getLibelle(), "LIBELLE2", "le libelle doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("ExtensionDAOTest.update.xml")
    public void testAll() {
        List<ExtensionBean> results = extensionDAO.selectAll();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getLibelle(), "LIBELLE", "le libellé doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("ExtensionDAOTest.update.xml")
    public void testByTypeOrderByDateLabel() {
        List<ExtensionBean> results = extensionDAO.selectByTypeOrderByDateLabel(Collections.singletonList(0));
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getLibelle(), "LIBELLE", "le code doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("ExtensionDAOTest.update.xml")
    @ExpectedDatabase("ExtensionDAOTest.updateAll-expected.xml")
    public void testUpdateAllState() {
        extensionDAO.updateAllState(1);
    }


}
