package com.kportal.extension.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.bean.ExtensionBean;
import com.kportal.extension.bean.ModuleBean;
import com.kportal.extension.dao.ModuleDAO;
import com.kportal.extension.module.IModule;

import mockit.Expectations;
import mockit.Invocation;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.Verifications;

@Test
@ContextConfiguration("ServiceModule.test-context.xml")
public class ServiceModuleTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ServiceModule serviceModule;

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testSave() {
        serviceModule.save(null);
    }

    @Test
    public void testAdd(@Mocked("addWithForcedId") final ModuleDAO mockedModuleDAO) {
        final ModuleDAO moduleDAO = new ModuleDAO();
        serviceModule.setDao(moduleDAO);
        final ModuleBean moduleBean = new ModuleBean();
        serviceModule.add(moduleBean);
        new Verifications(){{
            mockedModuleDAO.addWithForcedId(moduleBean);
            times = 1;
        }};
    }

    @Test
    public void testUpdate(@Mocked("update") final ModuleDAO mockedModuleDAO) {
        final ModuleDAO moduleDAO = new ModuleDAO();
        serviceModule.setDao(moduleDAO);
        final ModuleBean moduleBean = new ModuleBean();
        serviceModule.update(moduleBean);
        new Verifications(){{
            mockedModuleDAO.update(moduleBean);
            times = 1;
        }};
    }

    @Test
    public void testDeleteNotRestorable(@Mocked("deleteByState") final ModuleDAO mockedModuleDAO) {
        final ModuleDAO moduleDAO = new ModuleDAO();
        serviceModule.setDao(moduleDAO);
        serviceModule.deleteNotRestorable();
        new Verifications(){{
            mockedModuleDAO.deleteByState(IModule.ETAT_A_RESTAURER);
            times = 1;
        }};
    }

    @Test
    public void testDeleteByExtensionId(@Mocked("deleteByExtension") final ModuleDAO mockedModuleDAO) {
        final ModuleDAO moduleDAO = new ModuleDAO();
        serviceModule.setDao(moduleDAO);
        serviceModule.deleteByExtensionId(1L);
        new Verifications(){{
            mockedModuleDAO.deleteByExtension(1L);
            times = 1;
        }};
    }

    @Test
    public void testGetByExtensionAndTypes(@Mocked("getByExtensionIdAndType") final ModuleDAO mockedModuleDAO) {
        final ModuleDAO moduleDAO = new ModuleDAO();
        serviceModule.setDao(moduleDAO);
        new Expectations(){{
            moduleDAO.getByExtensionIdAndType(1L, Arrays.asList(1,2,3));
            result = Collections.singletonList(new ModuleBean());
            moduleDAO.getByExtensionIdAndType(1L, Collections.<Integer>emptyList());
            result = Collections.emptyList();
        }};
        final List<ModuleBean> list1 = serviceModule.getByExtensionAndTypes(1L, Arrays.asList(1,2,3));
        Assert.assertNotNull(list1, "La liste renvoyée ne devrait pas être null");
        Assert.assertEquals(list1.size(), 1, "La liste renvoyée devrait contenir 1 élément");
        final List<ModuleBean> list2 = serviceModule.getByExtensionAndTypes(1L, Collections.<Integer>emptyList());
        Assert.assertNotNull(list2, "La liste renvoyée ne devrait pas être null");
        Assert.assertTrue(list2.isEmpty(), "La liste renvoyée devrait être vide");
    }

    @Test
    public void testSetConfig1(@Mocked("getById") final ServiceExtension mockedServiceExtension) {
        final IModule module = new DummyOverridedContextBean();
        new MockUp<ApplicationContextManager> () {
            @Mock
            public <T> T getBean(final String ctxName, final String beanName, final Class<T> clazz) {
                return (T) module;
            }
            @Mock
            public <T> T getBean(final String ctxName, final String beanName, final boolean includeOverrided, Class<T> clazz) {
                return (T) new DummyOverridedContextBean();
            }
        };
        new MockUp<MessageHelper>() {
            @Mock
            public String getCoreMessage(Invocation context, final String key) {
                return "Plip";
            }

            @Mock
            public String getMessage(final String idCtx, final String key) {
                return "Plap";
            }
        };
        final ExtensionBean extensionBean = new ExtensionBean();
        extensionBean.setIdExtension(1L);
        extensionBean.setIdBean("Truc");
        extensionBean.setLibelle("Lala");
        final ServiceExtension serviceExtension = new ServiceExtension();
        serviceModule.setServiceExtension(serviceExtension);
        final ModuleBean param = new ModuleBean();
        param.setIdExtension(1L);
        new Expectations(){{
            serviceExtension.getById(1L);
            result = extensionBean;
        }};
        final ModuleBean moduleBean = serviceModule.setConfig(param, null);
        Assert.assertNotNull(moduleBean, "Le module renvoyé ne devrait pas être null");
        Assert.assertTrue(StringUtils.isNotBlank(moduleBean.getIdBeanExtension()), "La propriété \"idBeanExtension\" ne devrait pas être vide");
        Assert.assertEquals(moduleBean.getIdBeanExtension(), "Truc", "La propriété \"idBeanExtension\" devrait être \"Truc\"");
        Assert.assertTrue(StringUtils.isNotBlank(moduleBean.getDescription()), "La propriété \"description\" ne devrait pas être vide");
        Assert.assertEquals(moduleBean.getDescription(), "PlopPlipPlapPlipPlap\"", "La propriété \"description\" devrait être \"PlopPlip\"");
        Assert.assertTrue(StringUtils.isNotBlank(moduleBean.getLibelleExtension()), "La propriété \"libelleExtension\" ne devrait pas être vide");
        Assert.assertEquals(moduleBean.getLibelleExtension(), "Lala", "La propriété \"libelle\" devrait être \"Lala\"");
    }

    @Test
    public void testSetConfig2(@Mocked("getById") final ServiceExtension mockedServiceExtension) {
        new MockUp<Object>() {
            @Mock
            public boolean equals(Object o) {
                return true;
            }
        };
        final IModule module = new DummyOverridedContextBean();
        new MockUp<ApplicationContextManager> () {
            @Mock
            public <T> T getBean(final String ctxName, final String beanName, final Class<T> clazz) {
                return (T) module;
            }
            @Mock
            public <T> T getBean(final String ctxName, final String beanName, final boolean includeOverrided, Class<T> clazz) {
                return (T) module;
            }
        };
        new MockUp<MessageHelper>() {
            @Mock
            public String getCoreMessage(Invocation context, final String key) {
                return "Plip";
            }

            @Mock
            public String getMessage(final String idCtx, final String key) {
                return "Plap";
            }
        };
        final ExtensionBean extensionBean = new ExtensionBean();
        extensionBean.setIdExtension(1L);
        extensionBean.setIdBean("Truc");
        extensionBean.setLibelle("Lala");
        final ServiceExtension serviceExtension = new ServiceExtension();
        serviceModule.setServiceExtension(serviceExtension);
        final ModuleBean param = new ModuleBean();
        param.setIdExtension(1L);
        new Expectations(){{
            serviceExtension.getById(1L);
            result = extensionBean;
        }};
        final ModuleBean moduleBean = serviceModule.setConfig(param, null);
        Assert.assertNotNull(moduleBean, "Le module renvoyé ne devrait pas être null");
        Assert.assertTrue(StringUtils.isNotBlank(moduleBean.getIdBeanExtension()), "La propriété \"idBeanExtension\" ne devrait pas être vide");
        Assert.assertEquals(moduleBean.getIdBeanExtension(), "Truc", "La propriété \"idBeanExtension\" devrait être \"Truc\"");
        Assert.assertTrue(StringUtils.isNotBlank(moduleBean.getDescription()), "La propriété \"description\" ne devrait pas être vide");
        Assert.assertEquals(moduleBean.getDescription(), "PlopPlipPlapPlipPlap\"", "La propriété \"description\" devrait être \"PlopPlip\"");
        Assert.assertTrue(StringUtils.isNotBlank(moduleBean.getLibelleExtension()), "La propriété \"libelleExtension\" ne devrait pas être vide");
        Assert.assertEquals(moduleBean.getLibelleExtension(), "Lala", "La propriété \"libelle\" devrait être \"Lala\"");
    }

    @Test
    public void testUpdateStateByExtensionId(@Mocked("updateStateByExtensionId") final ModuleDAO mockedModuleDAO) {
        final ModuleDAO moduleDAO = new ModuleDAO();
        serviceModule.setDao(moduleDAO);
        serviceModule.updateStateByExtensionId(0, 1L);
        new Verifications(){{
            mockedModuleDAO.updateStateByExtensionId(anyInt, 1L);
            times = 1;
        }};
    }
}
