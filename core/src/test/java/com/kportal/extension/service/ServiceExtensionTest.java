package com.kportal.extension.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.extension.DefaultExtensionImpl;
import com.kportal.extension.bean.ExtensionBean;
import com.kportal.extension.dao.ExtensionDAO;
import com.kportal.extension.exception.NoSuchExtensionException;

import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.Verifications;

@Test
@ContextConfiguration("ServiceExtension.test-context.xml")
public class ServiceExtensionTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ServiceExtension serviceExtension;

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testSave() {
        serviceExtension.save(null);
    }

    @Test
    public void testAdd(@Mocked("addWithForcedId") final ExtensionDAO mockedDao) {
        final ExtensionDAO extensionDAO = new ExtensionDAO();
        final ExtensionBean extensionBean = new ExtensionBean();
        serviceExtension.setDao(extensionDAO);
        serviceExtension.add(extensionBean);
        new Verifications(){{
            mockedDao.addWithForcedId(extensionBean);
            times = 1;
        }};
    }

    @Test
    public void testUpdate(@Mocked("update") final ExtensionDAO mockedDao) {
        final ExtensionDAO extensionDAO = new ExtensionDAO();
        final ExtensionBean extensionBean = new ExtensionBean();
        serviceExtension.setDao(extensionDAO);
        serviceExtension.update(extensionBean);
        new Verifications(){{
            mockedDao.update(extensionBean);
            times = 1;
        }};
    }

    @Test
    public void testGetAll(@Mocked("selectAll") final ExtensionDAO mockedDao) {
        final ExtensionDAO extensionDAO = new ExtensionDAO();
        final ExtensionBean extensionBean = new ExtensionBean();
        extensionBean.setId(1L);
        serviceExtension.setDao(extensionDAO);
        new Expectations(){{
            extensionDAO.selectAll();
            result = Collections.singletonList(extensionBean);
        }};
        final List<ExtensionBean> list1 = serviceExtension.getAll();
        Assert.assertNotNull(list1, "La liste renvoyée ne devrait pas être null");
        Assert.assertEquals(list1.size(), 1, "La liste renvoyée devrait contenir 1 élément");
        Assert.assertEquals((long) list1.get(0).getId(), 1L, "La liste renvoyée devrait contenir 1 élément");
        new Expectations(){{
            extensionDAO.selectAll();
            result = Collections.emptyList();
        }};
        final List<ExtensionBean> list2 = serviceExtension.getAll();
        Assert.assertNotNull(list2, "La liste renvoyée ne devrait pas être null");
        Assert.assertTrue(list2.isEmpty(), "La liste renvoyée devrait être vide");
    }

    @Test
    public void testSelectByTypeOrderByDateLabel(@Mocked("selectByTypeOrderByDateLabel") final ExtensionDAO mockedDao) {
        final ExtensionDAO extensionDAO = new ExtensionDAO();
        final ExtensionBean extensionBean = new ExtensionBean();
        extensionBean.setId(1L);
        serviceExtension.setDao(extensionDAO);
        new Expectations(){{
            extensionDAO.selectByTypeOrderByDateLabel(Arrays.asList(1,2,3,4));
            result = Collections.singletonList(extensionBean);
        }};
        final List<ExtensionBean> list1 = serviceExtension.getByTypeOrderByDateLabel(Arrays.asList(1,2,3,4));
        Assert.assertNotNull(list1, "La liste renvoyée ne devrait pas être null");
        Assert.assertEquals(list1.size(), 1, "La liste renvoyée devrait contenir 1 élément");
        Assert.assertEquals((long) list1.get(0).getId(), 1L, "La liste renvoyée devrait contenir 1 élément");
        new Expectations(){{
            extensionDAO.selectByTypeOrderByDateLabel(Collections.<Integer>emptyList());
            result = Collections.emptyList();
        }};
        final List<ExtensionBean> list2 = serviceExtension.getByTypeOrderByDateLabel(Collections.<Integer>emptyList());
        Assert.assertNotNull(list2, "La liste renvoyée ne devrait pas être null");
        Assert.assertTrue(list2.isEmpty(), "La liste renvoyée devrait être vide");
    }

    @Test
    public void testChangeAllState(@Mocked("updateAllState") final ExtensionDAO mockedDao) {
        final ExtensionDAO extensionDAO = new ExtensionDAO();
        serviceExtension.setDao(extensionDAO);
        serviceExtension.changeAllState(1);
        new Verifications(){{
            mockedDao.updateAllState(1);
            times = 1;
        }};
    }

    @Test
    public void testSetConfig() throws NoSuchExtensionException {
        new MockUp<ApplicationContextManager>() {

            @Mock
            public <T> T getBean(final String ctxName, final String beanName, final Class<T> clazz) {
                final DefaultExtensionImpl extension = new DefaultExtensionImpl();
                extension.setAuteur("Plop");
                extension.setLibelle("Extension plop");
                extension.setDescription("C'est l'extension plop");
                extension.setLogo("Logo plop");
                extension.setUrl("http://www.plop.com");
                extension.setCoreVersion("6.04.04");
                return (T) extension;
            }
        };
        final ExtensionBean extensionBean = new ExtensionBean();
        serviceExtension.setConfig(extensionBean);
        Assert.assertTrue(StringUtils.isNotBlank(extensionBean.getAuteur()), "La propriété \"auteur\" ne devrait pas être vide");
        Assert.assertTrue(StringUtils.isNotBlank(extensionBean.getLibelle()), "La propriété \"libelle\" ne devrait pas être vide");
        Assert.assertTrue(StringUtils.isNotBlank(extensionBean.getDescription()), "La propriété \"description\" ne devrait pas être vide");
        Assert.assertTrue(StringUtils.isNotBlank(extensionBean.getLogo()), "La propriété \"logo\" ne devrait pas être vide");
        Assert.assertTrue(StringUtils.isNotBlank(extensionBean.getUrl()), "La propriété \"url\" ne devrait pas être vide");
        Assert.assertTrue(StringUtils.isNotBlank(extensionBean.getCoreVersion()), "La propriété \"core version\" ne devrait pas être vide");
        Assert.assertEquals(extensionBean.getAuteur(), "Plop", "La propriété \"auteur\" devrait être égale à \"Plop\"");
        Assert.assertEquals(extensionBean.getLibelle(), "Extension plop", "La propriété \"libelle\" devrait être égale à \"Extension plop\"");
        Assert.assertEquals(extensionBean.getDescription(), "C'est l'extension plop", "La propriété \"description\" devrait être égale à \"C'est l'extension plop\"");
        Assert.assertEquals(extensionBean.getLogo(), "Logo plop", "La propriété \"auteur\" devrait être égale à \"Logo plop\"");
        Assert.assertEquals(extensionBean.getUrl(), "http://www.plop.com", "La propriété \"auteur\" devrait être égale à \"http://www.plop.com\"");
        Assert.assertEquals(extensionBean.getCoreVersion(), "6.04.04", "La propriété \"auteur\" devrait être égale à \"6.04.04\"");
    }

    @Test(expectedExceptions = NoSuchExtensionException.class)
    public void testSetConfigFailed() throws NoSuchExtensionException {
        new MockUp<ApplicationContextManager>() {

            @Mock
            public <T> T getBean(final String ctxName, final String beanName, final Class<T> clazz) {
                return null;
            }
        };
        serviceExtension.setConfig(new ExtensionBean());
    }
}
