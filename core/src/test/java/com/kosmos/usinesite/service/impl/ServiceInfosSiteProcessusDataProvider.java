package com.kosmos.usinesite.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.univ.multisites.bean.impl.InfosSiteImpl;

/**
 * Created on 09/10/15.
 */
public class ServiceInfosSiteProcessusDataProvider {

    public static List<InfosSiteImpl> getList() {
        return getList(5, false);
    }

    public static List<InfosSiteImpl> getList(int size, boolean hasPrincipal) {
        final List<InfosSiteImpl> infosSites = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            infosSites.add(getInfoSite(i, i == 3 && hasPrincipal));
        }
        return infosSites;
    }

    public static InfosSiteImpl getInfoSite(int number, boolean principal) {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        infosSite.setAlias("Site" + number);
        infosSite.setHttpHostname("Host" + number);
        infosSite.setCodeRubrique("Rubrique" + number);
        infosSite.setSitePrincipal(principal);
        return infosSite;
    }
}
