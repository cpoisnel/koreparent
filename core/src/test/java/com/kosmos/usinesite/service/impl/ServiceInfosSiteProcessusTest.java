package com.kosmos.usinesite.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.kosmos.tests.testng.annotations.ClearCache;
import com.kosmos.usinesite.utils.InfosSiteHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.dao.InfosSiteDao;
import com.univ.multisites.dao.impl.InfosSiteDaoProperties;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.utils.URLResolver;

import mockit.Expectations;
import mockit.Invocation;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.Verifications;

/**
 * Created on 27/05/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/kosmos/usinesite/service/impl/ServiceInfosSiteProcessus.test-context.xml"})
public class ServiceInfosSiteProcessusTest extends AbstractCacheTestngTests {

    @Autowired
    ServiceInfosSiteProcessus serviceInfosSiteProcessus;

    @Mocked
    private ServiceRubrique serviceRubrique;

    @BeforeClass
    public void initMock() {
        serviceRubrique = new ServiceRubrique();
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testSave() throws Exception {
        serviceInfosSiteProcessus.save(null);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testDelete() throws Exception {
        serviceInfosSiteProcessus.delete(null);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testGetById() throws Exception {
        serviceInfosSiteProcessus.getById(null);
    }

    @Test(expectedExceptions = Exception.class)
    public void testCreerFail1(@Mocked({"getInfosSite"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        infosSite.setAlias("monSite");
        final InfosSiteImpl infosSite1 = new InfosSiteImpl();
        infosSite1.setAlias("monSite");
        infosSite1.setIdInfosSite(2);
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getInfosSite(anyString);
            result = infosSite1;
        }};
        serviceInfosSiteProcessus.creer(infosSite);
    }

    @Test
    public void testCreerFail2(@Mocked({"getInfosSite", "creer"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        infosSite.setAlias("monSite2");
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getInfosSite(anyString);
            result = new ErreurDonneeNonTrouve("Plop");
            infosSiteDao.creer(infosSite);
        }};
        serviceInfosSiteProcessus.creer(infosSite);
    }

    @Test
    public void testCreer(@Mocked({"getInfosSite", "creer"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        infosSite.setAlias("monSite");
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getInfosSite(anyString);
            result = new InfosSiteImpl();
        }};
        serviceInfosSiteProcessus.creer(infosSite);
        new Expectations() {{
            infosSiteDao.getInfosSite(anyString);
            result = infosSite;
        }};
        final InfosSite infosSite1 = serviceInfosSiteProcessus.getInfosSite("monSite");
        Assert.assertNotNull(infosSite1, "Le premier appel doit retourner un infosSite");
        Assert.assertEquals(infosSite1.getCodeCreateur(), StringUtils.EMPTY, "L'infosite retourné doit contenir un code redacteur vide et non null");
        Assert.assertNotNull(infosSite1.getDateCreation(), "L'infosite retourné doit renvoyer une date de création");
        Assert.assertEquals(infosSite1.getCodeDernierModificateur(), StringUtils.EMPTY, "L'infosite retourné doit contenir un code dernier modificateur vide et non null");
        Assert.assertNotNull(infosSite1.getDateDerniereModification(), "L'infosite retourné doit renvoyer une date de dernière modification");
    }

    @Test(expectedExceptions = Exception.class)
    public void testModifierFail(@Mocked({"getInfosSite", "miseAJour"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        infosSite.setAlias("monSite");
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getInfosSite(anyString);
            result = infosSite;
        }};
        final InfosSite infosSite1 = serviceInfosSiteProcessus.getInfosSite("monSite");
        Assert.assertNotNull(infosSite1, "Le premier appel doit retourner un infosSite");
        Assert.assertTrue(infosSite1.getCodeRubrique().equals(infosSite.getCodeRubrique()), "Le permier appel doit renvoyer l'ancienne version de l'infosSite");
        infosSite.setCodeRubrique("maRubrique");
        new Expectations() {{
            infosSiteDao.miseAJour(infosSite);
            result = new Exception();
        }};
        serviceInfosSiteProcessus.modifier(infosSite);
    }

    @Test
    public void testModifier(@Mocked({"getInfosSite", "miseAJour"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        infosSite.setAlias("monSite");
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getInfosSite(anyString);
            result = infosSite;
        }};
        final InfosSite infosSite1 = serviceInfosSiteProcessus.getInfosSite("monSite");
        Assert.assertNotNull(infosSite1, "Le premier appel doit retourner un infosSite");
        Assert.assertTrue(infosSite1.getCodeRubrique().equals(infosSite.getCodeRubrique()), "Le permier appel doit renvoyer l'ancienne version de l'infosSite");
        infosSite.setCodeRubrique("maRubrique");
        serviceInfosSiteProcessus.modifier(infosSite);
        new Expectations() {{
            infosSiteDao.getInfosSite("monAutreSite");
            result = infosSite;
            infosSiteDao.getInfosSite("monSite");
            result = null;
        }};
        final InfosSite infosSite2 = serviceInfosSiteProcessus.getInfosSite("monSite");
        Assert.assertNull(infosSite2, "Le second appel doit retourner null");
        final InfosSite infosSite3 = serviceInfosSiteProcessus.getInfosSite("monAutreSite");
        Assert.assertNotNull(infosSite3, "Le troisième appel doit retourner un infosSite");
        Assert.assertTrue(infosSite3.getCodeRubrique().equals(infosSite.getCodeRubrique()), "Le troisième appel doit renvoyer la nouvelle version de l'infosSite");
    }

    @Test
    public void testSupprimer(@Mocked({"getInfosSite", "supprimer"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        infosSite.setAlias("monSiteASupprimer");
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getInfosSite("monSiteASupprimer");
            result = infosSite;
        }};
        final InfosSite infosSite1 = serviceInfosSiteProcessus.getInfosSite("monSiteASupprimer");
        Assert.assertNotNull(infosSite1, "Le premier appel doit retourner un infosSite");
        Assert.assertTrue(infosSite1.getCodeRubrique().equals(infosSite.getCodeRubrique()), "Le permier appel doit renvoyer l'ancienne version de l'infosSite");
        serviceInfosSiteProcessus.supprimer("monSiteASupprimer");
        new Expectations() {{
            infosSiteDao.getInfosSite("monSiteASupprimer");
            result = null;
        }};
        final InfosSite infosSite2 = serviceInfosSiteProcessus.getInfosSite("monSiteASupprimer");
        Assert.assertNull(infosSite2, "Le second appel doit retourner null");
    }

    @Test
    public void testGetInfosSiteFail(@Mocked({"getInfosSite"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getInfosSite("monSiteGetInfosSite");
            result = new ErreurDonneeNonTrouve("Aucun site ne correspond");
        }};
        final InfosSite infosSite = serviceInfosSiteProcessus.getInfosSite("monSiteGetInfosSite");
        Assert.assertNull(infosSite, "L'info site retourné doit être null");
    }

    @Test
    public void testGetListeTousInfosSites(@Mocked({"getListeInfosSites"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = new ArrayList<>();
        }};
        final Collection<InfosSite> infosSites = serviceInfosSiteProcessus.getListeTousInfosSites();
        Assert.assertNotNull(infosSites, "La liste ne peut pas être nulle");
        Assert.assertEquals(infosSites.size(), 0, "La liste doit être vide");
    }

    @Test(expectedExceptions = Exception.class)
    public void testGetListeTousInfosSitesFail(@Mocked({"getListeInfosSites"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = new Exception();
        }};
        serviceInfosSiteProcessus.getListeTousInfosSites();
    }

    @Test
    public void testEnregistrerFichier() throws Exception {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public boolean exists() {
                return true;
            }

            @Mock
            public boolean delete() {
                return true;
            }
        };
        new MockUp<FileUtils>() {

            @Mock
            public void moveFile(File srcFile, File destFile) throws IOException {
            }
        };
        serviceInfosSiteProcessus.enregistrerFichier(new InfosSiteImpl(), new File(StringUtils.EMPTY), StringUtils.EMPTY);
    }

    @Test(expectedExceptions = IOException.class)
    public void testEnregistrerFichierFail1() throws Exception {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public boolean exists() {
                return true;
            }

            @Mock
            public boolean delete() {
                return true;
            }
        };
        new MockUp<FileUtils>() {

            @Mock
            public void moveFile(File srcFile, File destFile) throws IOException {
                throw new IOException("Plop");
            }
        };
        serviceInfosSiteProcessus.enregistrerFichier(new InfosSiteImpl(), new File(StringUtils.EMPTY), StringUtils.EMPTY);
    }

    @Test
    public void testEnregistrerFichierFail2() throws Exception {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
                throw new ErreurDonneeNonTrouve("Plop");
            }
        };
        serviceInfosSiteProcessus.enregistrerFichier(new InfosSiteImpl(), new File(StringUtils.EMPTY), StringUtils.EMPTY);
    }

    @Test
    public void testEnregistrerFichierFail3() throws Exception {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public boolean exists() {
                return true;
            }

            @Mock
            public boolean delete() {
                return false;
            }
        };
        new MockUp<FileUtils>() {

            @Mock
            public void moveFile(File srcFile, File destFile) throws IOException {
            }
        };
        serviceInfosSiteProcessus.enregistrerFichier(new InfosSiteImpl(), new File(StringUtils.EMPTY), StringUtils.EMPTY);
    }

    @Test
    public void testSupprimerFichier() throws Exception {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public boolean exists() {
                return true;
            }

            @Mock
            public boolean delete() {
                return true;
            }
        };
        serviceInfosSiteProcessus.supprimerFichier(new InfosSiteImpl(), StringUtils.EMPTY);
    }

    @Test
    public void testSupprimerFichierFail1() throws Exception {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
                throw new ErreurDonneeNonTrouve("Plop");
            }
        };
        serviceInfosSiteProcessus.supprimerFichier(new InfosSiteImpl(), StringUtils.EMPTY);
    }

    @Test
    public void testSupprimerFichierFail2() throws Exception {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public boolean exists() {
                return true;
            }

            @Mock
            public boolean delete() {
                return false;
            }
        };
        serviceInfosSiteProcessus.supprimerFichier(new InfosSiteImpl(), StringUtils.EMPTY);
    }

    @Test
    public void testSupprimerTousFichiers() throws IOException {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public boolean exists() {
                return true;
            }
        };
        new MockUp<FileUtils>() {

            @Mock
            public void deleteDirectory(File directory) throws IOException {
            }
        };
        serviceInfosSiteProcessus.supprimerTousFichiers(new InfosSiteImpl());
    }

    @Test(expectedExceptions = IOException.class)
    public void testSupprimerTousFichiersFail() throws IOException {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public boolean exists() {
                return true;
            }
        };
        new MockUp<FileUtils>() {

            @Mock
            public void deleteDirectory(File directory) throws IOException {
                throw new IOException();
            }
        };
        serviceInfosSiteProcessus.supprimerTousFichiers(new InfosSiteImpl());
    }

    @Test
    public void testCleanFichiers1() throws IOException {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichiersTemplateDuSite(final InfosSite infosSite) {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public boolean exists() {
                return false;
            }
        };
        serviceInfosSiteProcessus.cleanFichiers(new InfosSiteImpl());
    }

    @Test
    public void testCleanFichiers2() throws IOException {
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichiersTemplateDuSite(final InfosSite infosSite) {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public File[] listFiles() {
                return new File[] {new File(StringUtils.EMPTY)};
            }

            @Mock
            public boolean exists() {
                return true;
            }

            @Mock
            public boolean isDirectory() {
                return false;
            }

            @Mock
            public boolean delete() {
                return false;
            }
        };
        serviceInfosSiteProcessus.cleanFichiers(new InfosSiteImpl());
    }

    @Test
    public void testCleanFichiers3(@Mocked("getProprieteComplementaire") InfosSiteImpl mockedInfosSite) throws IOException {
        final InfosSite infosSite = new InfosSiteImpl();
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichiersTemplateDuSite(final InfosSite infosSite) {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public File[] listFiles() {
                return new File[] {new File(StringUtils.EMPTY)};
            }

            @Mock
            public boolean exists() {
                return true;
            }

            @Mock
            public boolean isDirectory() {
                return true;
            }

            @Mock
            public String getName() {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<FileUtils>() {

            @Mock
            public void deleteDirectory(File directory) throws IOException {}
        };
        new Expectations() {{
            infosSite.getProprieteComplementaire(anyString);
            result = null;
        }};
        serviceInfosSiteProcessus.cleanFichiers(new InfosSiteImpl());
    }

    @Test
    public void testCleanFichiers4(@Mocked("getProprieteComplementaire") InfosSiteImpl mockedInfosSite) throws IOException {
        final InfosSite infosSite = new InfosSiteImpl();
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichiersTemplateDuSite(final InfosSite infosSite) {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public File[] listFiles() {
                return new File[] {new File(StringUtils.EMPTY)};
            }

            @Mock
            public boolean exists() {
                return true;
            }

            @Mock
            public boolean isDirectory() {
                return true;
            }

            @Mock
            public String getName() {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<FileUtils>() {

            @Mock
            public void deleteDirectory(File directory) throws IOException {}
        };
        new Expectations() {{
            infosSite.getProprieteComplementaire(anyString);
            result = "PLOP";
        }};
        serviceInfosSiteProcessus.cleanFichiers(new InfosSiteImpl());
    }

    @Test
    public void testCleanFichiers5(@Mocked("getProprieteComplementaire") InfosSiteImpl mockedInfosSite) throws IOException {
        final InfosSite infosSite = new InfosSiteImpl();
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichiersTemplateDuSite(final InfosSite infosSite) {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public File[] listFiles() {
                return new File[] {new File(StringUtils.EMPTY)};
            }

            @Mock
            public boolean exists() {
                return true;
            }

            @Mock
            public boolean isDirectory(Invocation context) {
                return context.getInvocationCount() != 2;
            }

            @Mock
            public String getName() {
                return StringUtils.EMPTY;
            }

            @Mock
            public boolean delete() {
                return true;
            }
        };
        new MockUp<FileUtils>() {

            @Mock
            public void deleteDirectory(File directory) throws IOException {}
        };
        new Expectations() {{
            infosSite.getProprieteComplementaire(anyString);
            result = "PLOP";
        }};
        serviceInfosSiteProcessus.cleanFichiers(new InfosSiteImpl());
    }

    @Test
    public void testCleanFichiers6(@Mocked("getProprieteComplementaire") InfosSiteImpl mockedInfosSite) throws IOException {
        final InfosSite infosSite = new InfosSiteImpl();
        new MockUp<InfosSiteHelper>() {

            @Mock
            public String getPathAbsoluFichiersTemplateDuSite(final InfosSite infosSite) {
                return StringUtils.EMPTY;
            }
        };
        new MockUp<File>() {

            @Mock
            public File[] listFiles() {
                return new File[] {};
            }

            @Mock
            public boolean exists() {
                return true;
            }
        };
        new MockUp<FileUtils>() {

            @Mock
            public void deleteDirectory(File directory) throws IOException {}
        };
        serviceInfosSiteProcessus.cleanFichiers(new InfosSiteImpl());
    }

    @Test
    @ClearCache(names = "ServiceInfosSiteProcessus.getByHost")
    public void testGetSiteByHost(@Mocked("getListeInfosSites") final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = ServiceInfosSiteProcessusDataProvider.getList();
        }};
        final InfosSite infosSite1 = serviceInfosSiteProcessus.getSiteByHost("Host1");
        Assert.assertNotNull(infosSite1, "Le premier appel doit retourner un infosSite");
        Assert.assertTrue(infosSite1.getHttpHostname().equals("Host1"), "Le premier appel doit renvoyer l'infoSite portant le host \"Host1\"");
        serviceInfosSiteProcessus.getSiteByHost("Host1");
        new Verifications() {{
            infosSiteDao.getListeInfosSites();
            times = 1;
        }};
        serviceInfosSiteProcessus.getSiteByHost("Host2");
        new Verifications() {{
            infosSiteDao.getListeInfosSites();
            times = 2;
        }};
    }

    @Test
    @ClearCache(names = "ServiceInfosSiteProcessus.getBySection")
    public void testGetSiteBySection(@Mocked("getListeInfosSites") final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = ServiceInfosSiteProcessusDataProvider.getList();
        }};
        final InfosSite infosSite1 = serviceInfosSiteProcessus.getSiteBySection("Rubrique1");
        Assert.assertNotNull(infosSite1, "Le premier appel doit retourner un infosSite");
        Assert.assertTrue(infosSite1.getCodeRubrique().equals("Rubrique1"), "Le premier appel doit renvoyer l'infoSite portant le host \"Rubrique1\"");
        serviceInfosSiteProcessus.getSiteBySection("Rubrique1");
        new Verifications() {{
            infosSiteDao.getListeInfosSites();
            times = 1;
        }};
        serviceInfosSiteProcessus.getSiteBySection("Rubrique2");
        new Verifications() {{
            infosSiteDao.getListeInfosSites();
            times = 2;
        }};
    }

    @Test
    @ClearCache(names = "ServiceInfosSiteProcessus.getSitePrincipal")
    public void testGetSitePrincipal(@Mocked("getListeInfosSites") final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = ServiceInfosSiteProcessusDataProvider.getList(5, true);
        }};
        final InfosSite infosSite1 = serviceInfosSiteProcessus.getPrincipalSite();
        Assert.assertNotNull(infosSite1, "Le premier appel doit retourner un infosSite");
        Assert.assertTrue(infosSite1.getCodeRubrique().equals("Rubrique3"), "Le premier appel doit renvoyer l'infoSite portant le host \"Rubrique3\"");
        serviceInfosSiteProcessus.getPrincipalSite();
        new Verifications() {{
            infosSiteDao.getListeInfosSites();
            times = 1;
        }};
    }

    @Test
    @ClearCache(names = "ServiceInfosSiteProcessus.getAliasHostList")
    public void testGetAliasHostList(@Mocked("getInfosSite") final InfosSiteDaoProperties dao, @Mocked(stubOutClassInitialization = true, value = "getBasePath") final URLResolver urlResolver) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        infosSite.setCodeRubrique("uneRubrique");
        infosSite.setHttpHostname("une.rubrique");
        infosSite.setHttpPort(8080);
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getInfosSite("uneRubrique");
            result = infosSite;
            URLResolver.getBasePath(infosSite, false);
            result = "http://une.rubrique:8080";
        }};
        final Collection<String> list = serviceInfosSiteProcessus.getAliasHostList("uneRubrique");
        Assert.assertNotNull(list, "Le premier appel doit retourner une liste de String");
        Assert.assertTrue(list.size() == 2, "Le premier appel doit renvoyer une liste ne comportant que deux éléments");
        Assert.assertTrue("http://une.rubrique:8080".equals(CollectionUtils.get(list, 0)), "Le premier appel doit renvoyer une string égale à \"http://une.rubrique:8080\"");
        serviceInfosSiteProcessus.getAliasHostList("uneRubrique");
        new Verifications() {{
            URLResolver.getBasePath(infosSite, false);
            times = 1;
        }};
    }

    @Test
    public void testDetermineSite1() {
        serviceInfosSiteProcessus.setServiceRubrique(serviceRubrique);
        final InfosSite infosSite1 = serviceInfosSiteProcessus.determineSite(StringUtils.EMPTY, false);
        Assert.assertNull(infosSite1, "Le premier appel doit renvoyer null");
        new Expectations() {{
            serviceRubrique.getRubriqueByCode(anyString);
            result = null;
        }};
        final InfosSite infosSite2 = serviceInfosSiteProcessus.determineSite("CODE_RUBRIQUE", false);
        Assert.assertNull(infosSite2, "Le second appel doit renvoyer null");
    }

    @Test
    public void testDetermineSite2(@Mocked("getListeInfosSites") final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        serviceInfosSiteProcessus.setServiceRubrique(serviceRubrique);
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = ServiceInfosSiteProcessusDataProvider.getList(5, true);
            serviceRubrique.getRubriqueByCode(anyString);
            result = null;
        }};
        final InfosSite infosSite2 = serviceInfosSiteProcessus.determineSite("CODE_RUBRIQUE", true);
        Assert.assertNotNull(infosSite2, "Le premier appel doit renvoyer un infoSite");
        Assert.assertEquals(infosSite2.getCodeRubrique(), "Rubrique3", "Le premier appel doit renvoyer l'infosSite portant le code rubrique \"Rubrique3\"");
    }

    @Test
    public void testDetermineSite3(@Mocked("getListeInfosSites") final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        final RubriqueBean rubriqueBean1 = new RubriqueBean();
        rubriqueBean1.setCode("Rubrique1");
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        serviceInfosSiteProcessus.setServiceRubrique(serviceRubrique);
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = ServiceInfosSiteProcessusDataProvider.getList(5, true);
            serviceRubrique.getRubriqueByCode("Rubrique1");
            result = rubriqueBean1;
        }};
        final InfosSite infosSite = serviceInfosSiteProcessus.determineSite("Rubrique1", true);
        Assert.assertNotNull(infosSite, "Le premier appel doit renvoyer un infoSite");
        Assert.assertEquals(infosSite.getHttpHostname(), "Host1", "Le premier appel doit renvoyer un infoSite avec un host égal à \"Host1\"");
        final RubriqueBean rubriqueBean2 = new RubriqueBean();
        rubriqueBean2.setCode("Rubrique");
        rubriqueBean2.setCodeRubriqueMere("Rubrique1");
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        serviceInfosSiteProcessus.setServiceRubrique(serviceRubrique);
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = ServiceInfosSiteProcessusDataProvider.getList(5, true);
            serviceRubrique.getRubriqueByCode("Rubrique");
            result = rubriqueBean2;
            serviceRubrique.getRubriqueByCode("Rubrique1");
            result = rubriqueBean1;
        }};
        final InfosSite infosSite2 = serviceInfosSiteProcessus.determineSite("Rubrique", true);
        Assert.assertNotNull(infosSite2, "Le second appel doit renvoyer un infoSite");
        Assert.assertEquals(infosSite2.getHttpHostname(), "Host1", "Le second appel doit renvoyer un infoSite avec un host égal à \"Host1\"");
    }

    @Test
    public void testDisplaySite1() throws Exception {
        serviceInfosSiteProcessus.setServiceRubrique(serviceRubrique);
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("Rubrique1");
        final FicheUniv fiche = new MockUp<FicheUniv>() {

            @Mock
            public String getCodeRubrique() {
                return "Rubrique1";
            }
        }.getMockInstance();
        new Expectations() {{
            serviceRubrique.getRubriqueByCode("Rubrique1");
            result = rubriqueBean;
        }};
        final InfosSite firstResult = serviceInfosSiteProcessus.displaySite(fiche);
        Assert.assertNotNull(firstResult, "Le premier appel doit renvoyer un infosSite");
        Assert.assertEquals(firstResult.getCodeRubrique(), "Rubrique1", "Le premier appel doit renvoyer un infosSite avec le code rubrique \"Rubrique1\"");
        Assert.assertEquals(firstResult.getHttpHostname(), "Host1", "Le premier appel doit renvoyer un infosSite avec un host \"Host1\"");
    }

    @Test
    public void testDisplaySite2(@Mocked("getListeInfosSites") final InfosSiteDaoProperties dao, @Mocked("getRubriqueDestByFicheUniv") final ServiceRubriquePublication mockedServiceRubriquePublication) throws Exception {
        final ServiceRubriquePublication serviceRubriquePublication = new ServiceRubriquePublication();
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        serviceInfosSiteProcessus.setServiceRubrique(serviceRubrique);
        serviceInfosSiteProcessus.setServiceRubriquePublication(serviceRubriquePublication);
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("Rubrique2");
        new Expectations() {{
            serviceRubriquePublication.getRubriqueDestByFicheUniv(null);
            result = Collections.singletonList("Rubrique2");
            infosSiteDao.getListeInfosSites();
            result = ServiceInfosSiteProcessusDataProvider.getList(5, true);
            serviceRubrique.getRubriqueByCode("Rubrique2");
            result = rubriqueBean;
        }};
        final InfosSite firstResult = serviceInfosSiteProcessus.displaySite(null);
        Assert.assertNotNull(firstResult, "Le premier appel doit renvoyer un infosSite");
        Assert.assertEquals(firstResult.getCodeRubrique(), "Rubrique2", "Le premier appel doit renvoyer un infosSite avec le code rubrique \"Rubrique1\"");
        Assert.assertEquals(firstResult.getHttpHostname(), "Host2", "Le premier appel doit renvoyer un infosSite avec un host \"Host1\"");
    }

    @Test
    public void testGetFullAliasHostList(@Mocked({"getListeInfosSites", "getInfosSite"}) final InfosSiteDaoProperties dao) throws Exception {
        final InfosSiteDao infosSiteDao = new InfosSiteDaoProperties();
        serviceInfosSiteProcessus.setInfosSiteDao(infosSiteDao);
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = ServiceInfosSiteProcessusDataProvider.getList(2, true);
            infosSiteDao.getInfosSite("Site0");
            result = ServiceInfosSiteProcessusDataProvider.getInfoSite(0, false);
            infosSiteDao.getInfosSite("Site1");
            result = ServiceInfosSiteProcessusDataProvider.getInfoSite(1, false);
        }};
        final Collection<String> aliases = serviceInfosSiteProcessus.getFullAliasHostList();
        Assert.assertTrue(CollectionUtils.isNotEmpty(aliases), "La liste retournée ne doit pas être nulle ou vide");
        Assert.assertTrue(aliases.size() == 4, "La liste doit contenir 4 résultats");
        new Expectations() {{
            infosSiteDao.getListeInfosSites();
            result = new Exception();
        }};
        final Collection<String> aliases2 = serviceInfosSiteProcessus.getFullAliasHostList();
        Assert.assertNotNull(aliases2, "La liste retournée ne doit pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(aliases2), "La liste retournée doit être vide");
    }
}
