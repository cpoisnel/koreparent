package com.kosmos.userfront.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.kosmos.userfront.bean.ModerationState;
import com.kosmos.userfront.bean.UserModerationData;

/**
 * Created by olivier.camon on 02/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/kosmos/userfront/dao/test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class UserModerationDAOTest extends AbstractDbUnitTestngTests {

    @Autowired UserModerationDAO userModerationDAO;

    @Test
    @DatabaseSetup("add.xml")
    @ExpectedDatabase("add-expected.xml")
    public void testAdd() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final UserModerationData userModerationData = new UserModerationData();
        userModerationData.setEmail("EMAIL2");
        userModerationData.setLogin("LOGIN");
        userModerationData.setFirstName("FIRST_NAME");
        userModerationData.setLastName("LAST_NAME");
        userModerationData.setModerationMessage("MODERATION_MESSAGE");
        userModerationData.setAliasSite("ktest");
        userModerationData.setIdLocaleKportal("0");
        userModerationData.setModerationState(ModerationState.VALIDATED);
        userModerationData.setCreationDate(format.parse("1970-01-01 00:00:00"));
        userModerationData.setParentIdRegistration(0L);
        userModerationDAO.add(userModerationData);
    }

    @Test
    @DatabaseSetup("update.xml")
    @ExpectedDatabase("update-expected.xml")
    public void testUpdate() {
        final UserModerationData userModerationData = userModerationDAO.getById(2L);
        userModerationData.setLogin("LOGIN2");
        userModerationDAO.update(userModerationData);
    }

    @Test
    @DatabaseSetup("add-expected.xml")
    @ExpectedDatabase("add.xml")
    public void testDelete() {
        userModerationDAO.delete(2L);
    }


    @Test
    @DatabaseSetup("update.xml")
    public void testById() {
        final UserModerationData userModerationData = userModerationDAO.getById(2L);
        Assert.assertNotNull(userModerationData);
        Assert.assertEquals(userModerationData.getEmail(), "EMAIL2", "le mail doit correspondre à la deuxième entrée");
    }


    @Test
    @DatabaseSetup("update.xml")
    public void testAllOrderByCreattionDate() {
        List<UserModerationData> results = userModerationDAO.selectAllOrderByCreattionDate();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "le code doit correspondre à la première entrée");
    }


}
