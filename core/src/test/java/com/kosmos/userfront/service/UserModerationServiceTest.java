package com.kosmos.userfront.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kosmos.userfront.bean.UserModerationData;
import com.kosmos.userfront.dao.UserModerationDAO;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;

@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/userfront/service/UserModerationService.test-context.xml"})
public class UserModerationServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private UserModerationService userModerationService;

    @Test
    public void testGetAllUserModerationOrderByCreation(@Mocked("selectAllOrderByCreattionDate") final UserModerationDAO mockedUserModerationDao) {
        final UserModerationDAO dao = new UserModerationDAO();
        userModerationService.setDao(dao);
        final List<UserModerationData> data = new ArrayList<>();
        data.add(new UserModerationData());
        data.add(new UserModerationData());
        data.add(new UserModerationData());
        new Expectations(){{
            dao.selectAllOrderByCreattionDate();
            result = data;
        }};
        final List<UserModerationData> result = userModerationService.getAllUserModerationOrderByCreation();
        Assert.assertNotNull(result, "La liste renvoyée ne devrait pas être null");
        Assert.assertEquals(result.size(), 3, "La liste renvoyée devrait contenir 3 éléments");
        new Verifications() {{
            mockedUserModerationDao.selectAllOrderByCreattionDate(); times = 1;
        }};
    }

}
