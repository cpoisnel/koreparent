package com.kosmos.registration.dao.impl;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.registration.action.ActionConfiguration;
import com.kosmos.registration.bean.Model;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;

/**
 * Created by olivier.camon on 02/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/kosmos/registration/dao/impl/DefaultModelDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class DefaultModelDAOTest extends AbstractDbUnitTestngTests {

    @Autowired DefaultModelDAO defaultModelDAO;

    @Test
    @DatabaseSetup("DefaultModelDAOTest.add.xml")
    @ExpectedDatabase("DefaultModelDAOTest.add-expected.xml")
    public void testAdd() throws ParseException {
        final Model model = new Model();
        model.setActionConfigurationById(new HashMap<String, ActionConfiguration>());
        model.setModelDescriptorId("MODEL_DESCRIPTOR_ID2");
        defaultModelDAO.add(model);
    }

    @Test
    @DatabaseSetup("DefaultModelDAOTest.update.xml")
    @ExpectedDatabase("DefaultModelDAOTest.update-expected.xml")
    public void testUpdate() {
        final Model model = defaultModelDAO.getById(2L);
        model.setModelDescriptorId("MODEL_DESCRIPTOR_ID3");
        defaultModelDAO.update(model);
    }

    @Test
    @DatabaseSetup("DefaultModelDAOTest.add-expected.xml")
    @ExpectedDatabase("DefaultModelDAOTest.add.xml")
    public void testDelete() {
        defaultModelDAO.delete(2L);
    }


    @Test
    @DatabaseSetup("DefaultModelDAOTest.update.xml")
    public void testById() {
        final Model model = defaultModelDAO.getById(2L);
        Assert.assertNotNull(model);
        Assert.assertEquals(model.getModelDescriptorId(), "MODEL_DESCRIPTOR_ID2", "le modelId doit correspondre à la deuxième entrée");
    }


    @Test
    @DatabaseSetup("DefaultModelDAOTest.update.xml")
    public void testByDescriptorID() {
        final List<Model> model = defaultModelDAO.getModelByDescriptorID("MODEL_DESCRIPTOR_ID2");
        Assert.assertNotNull(model);
        Assert.assertEquals(model.size(), 1, "il doit y avoir un seul résultat");
        Assert.assertEquals(model.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }


}
