package com.kosmos.registration.service.impl;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kosmos.registration.bean.Model;
import com.kosmos.registration.dao.ModelDao;
import com.kosmos.registration.dao.impl.DefaultModelDAO;
import com.kosmos.registration.exception.RegistrationModelNotFoundException;

import mockit.Expectations;
import mockit.Mocked;

@Test
@ContextConfiguration("classpath:/com/kosmos/registration/service/impl/ModelService.test-context.xml")
public class ModelServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ModelService modelService;

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testGetById() {
        modelService.getById(null);
    }

    @Test
    public void testGet1(@Mocked("getById") final ModelDao mockedModelDao) throws RegistrationModelNotFoundException {
        final ModelDao dao = new DefaultModelDAO();
        modelService.setDao(dao);
        new Expectations(){{
            dao.getById(anyLong);
            result = new Model();
        }};
        final Model result = modelService.get(0L);
        Assert.assertNotNull(result, "L'appel devrait retourner un Model");
    }

    @Test(expectedExceptions = RegistrationModelNotFoundException.class)
    public void testGet2(@Mocked("getById") final ModelDao mockedModelDao) throws RegistrationModelNotFoundException {
        final ModelDao dao = new DefaultModelDAO();
        modelService.setDao(dao);
        new Expectations(){{
            dao.getById(anyLong);
            result = null;
        }};
        modelService.get(0L);
    }

    @Test
    public void testCreate(@Mocked("add") final ModelDao mockedModelDao) {
        final ModelDao dao = new DefaultModelDAO();
        modelService.setDao(dao);
        final Model model = new Model();
        model.setModelDescriptorId("lala");
        new Expectations(){{
            dao.add(model);
            result = model;
        }};
        final Model result = modelService.create(model);
        Assert.assertNotNull(result, "Le model renvoyé ne devrait pas être null");
        Assert.assertEquals(result.getModelDescriptorId(), "lala", "L'id du modele descriptor devrait être \"lala\"");
    }

    @Test
    public void testUpdate(@Mocked("update") final ModelDao mockedModelDao) {
        final ModelDao dao = new DefaultModelDAO();
        modelService.setDao(dao);
        final Model model = new Model();
        model.setModelDescriptorId("lala");
        new Expectations(){{
            dao.update(model);
            result = model;
        }};
        final Model result = modelService.update(model);
        Assert.assertNotNull(result, "Le model renvoyé ne devrait pas être null");
        Assert.assertEquals(result.getModelDescriptorId(), "lala", "L'id du modele descriptor devrait être \"lala\"");
    }

    @Test
    public void testGetByModelDescriptorID(@Mocked("getModelByDescriptorID") final ModelDao mockedModelDao) {
        modelService.setDao(mockedModelDao);
        final Model model = new Model();
        model.setModelDescriptorId("lala");
        new Expectations(){{
            mockedModelDao.getModelByDescriptorID("lala");
            result = Collections.singletonList(model);
            mockedModelDao.getModelByDescriptorID("plop");
            result = Collections.emptyList();
        }};
        final Collection<Model> models1 = modelService.getByModelDescriptorID("lala");
        Assert.assertNotNull(models1, "La collection renvoyée ne devrait pas être null");
        Assert.assertEquals(models1.size(), 1, "La collection devrait contenir un élément");
        Assert.assertEquals(models1.iterator().next().getModelDescriptorId(), "lala", "La collection devrait contenir un élément dont le descriptor id est \"lala\"");
        final Collection<Model> models2 = modelService.getByModelDescriptorID("plop");
        Assert.assertNotNull(models2, "La collection renvoyée ne devrait pas être null");
        Assert.assertTrue(models2.isEmpty(), "La collection devrait être vide");
    }
}
