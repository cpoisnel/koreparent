package com.kosmos.registration.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.registration.dao.RegistrationDao;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;

@Test
@ContextConfiguration("classpath:/com/kosmos/registration/service/impl/RegistrationService.test-context.xml")
public class RegistrationServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private RegistrationService registrationService;

    @Test
    public void testCreate(@Mocked("add") final RegistrationDao mockedRegistrationDao) {
        registrationService.setDao(mockedRegistrationDao);
        final Registration registration = new Registration();
        final Date date = new Date();
        registration.setCreationDate(date);
        new Expectations() {{
            mockedRegistrationDao.add(registration);
            result = registration;
        }};
        final Registration result = registrationService.create(registration);
        Assert.assertNotNull(result, "Le résultat renvoyé ne doit pas être null");
        Assert.assertEquals(registration.getCreationDate(), date, "Le résultat renvoyé doit avoir une date égale à " + date);
        new Verifications(){{
            mockedRegistrationDao.add(registration); times = 1;
        }};
    }

    @Test
    public void testUpdate(@Mocked("update") final RegistrationDao mockedRegistrationDao) {
        registrationService.setDao(mockedRegistrationDao);
        final Registration registration = new Registration();
        final Date date = new Date();
        registration.setCreationDate(date);
        new Expectations() {{
            mockedRegistrationDao.update(registration);
            result = registration;
        }};
        final Registration result = registrationService.update(registration);
        Assert.assertNotNull(result, "Le résultat renvoyé ne doit pas être null");
        Assert.assertEquals(registration.getCreationDate(), date, "Le résultat renvoyé doit avoir une date égale à " + date);
        new Verifications(){{
            mockedRegistrationDao.update(registration); times = 1;
        }};
    }

    @Test
    public void testGetRegistrationsByModel(@Mocked("getByModel") final RegistrationDao mockedRegistrationDao) {
        registrationService.setDao(mockedRegistrationDao);
        final Registration registration = new Registration();
        new Expectations() {{
            mockedRegistrationDao.getByModel(0L);
            result = Collections.singletonList(registration);
            mockedRegistrationDao.getByModel(1L);
            result = Collections.emptyList();
        }};
        final Collection<Registration> result1 = registrationService.getRegistrationsByModel(0L);
        Assert.assertNotNull(result1, "Le résultat renvoyé ne doit pas être null");
        Assert.assertEquals(result1.size(), 1, "La collection renvoyée doit contenir 1 élément");
        final Collection<Registration> result2 = registrationService.getRegistrationsByModel(1L);
        Assert.assertNotNull(result2, "Le résultat renvoyé ne doit pas être null");
        Assert.assertTrue(result2.isEmpty(), "La collection renvoyée doit être vide");
        new Verifications(){{
            mockedRegistrationDao.getByModel(anyLong); times = 2;
        }};
    }

    @Test
    public void testGetRegistrationsByModelAndState(@Mocked("getByModelAndState") final RegistrationDao mockedRegistrationDao) {
        registrationService.setDao(mockedRegistrationDao);
        final Registration registration = new Registration();
        new Expectations() {{
            mockedRegistrationDao.getByModelAndState(0L, RegistrationState.FINISHED);
            result = Collections.singletonList(registration);
            mockedRegistrationDao.getByModelAndState(1L, RegistrationState.FINISHED);
            result = Collections.emptyList();
        }};
        final Collection<Registration> result1 = registrationService.getRegistrationsByModelAndState(0L, RegistrationState.FINISHED);
        Assert.assertNotNull(result1, "Le résultat renvoyé ne doit pas être null");
        Assert.assertEquals(result1.size(), 1, "La collection renvoyée doit contenir 1 élément");
        final Collection<Registration> result2 = registrationService.getRegistrationsByModelAndState(1L, RegistrationState.FINISHED);
        Assert.assertNotNull(result2, "Le résultat renvoyé ne doit pas être null");
        Assert.assertTrue(result2.isEmpty(), "La collection renvoyée doit être vide");
        new Verifications(){{
            mockedRegistrationDao.getByModelAndState(anyLong, RegistrationState.FINISHED); times = 2;
        }};
    }

    @Test
    public void testDeleteByModelId(@Mocked("deleteByModelId") final RegistrationDao mockedRegistrationDao) {
        registrationService.setDao(mockedRegistrationDao);
        registrationService.deleteByModelId(0L);
        new Verifications(){{
            mockedRegistrationDao.deleteByModelId(anyLong); times = 1;
        }};
    }

}
