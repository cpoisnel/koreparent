package com.univ.url.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.url.bean.UrlBean;

/**
 * Created by olivier.camon on 28/10/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/url/dao/UrlDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class UrlDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    UrlDAO urlDAO;

    @Test
    @DatabaseSetup("UrlDAOTest.add.xml")
    @ExpectedDatabase("UrlDAOTest.add-expected.xml")
    public void testAdd() {
        final UrlBean bean = new UrlBean();
        bean.setCodeSite("CODE_SITE");
        bean.setSectionCode("SECTION_CODE");
        bean.setUrl("URL2");
        urlDAO.add(bean);
    }

    @Test
    @DatabaseSetup("UrlDAOTest.add-expected.xml")
    @ExpectedDatabase("UrlDAOTest.add.xml")
    public void testDelete() {
        urlDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("UrlDAOTest.deleteAllForSection.xml")
    @ExpectedDatabase("UrlDAOTest.deleteAllForSection-expected.xml")
    public void testDeleteAllForSection() {
        urlDAO.deleteAllForSection("SECTION_CODE2");
    }

    @Test
    @DatabaseSetup("UrlDAOTest.update-expected.xml")
    @ExpectedDatabase("UrlDAOTest.add.xml")
    public void testDeleteAllForSite() {
        urlDAO.deleteAllForSite("CODE_SITE2");
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("UrlDAOTest.update.xml")
    public void testFill() {
        final UrlBean bean = urlDAO.getById(2L);
        Assert.assertNotNull(bean);
        Assert.assertEquals(bean.getUrl(), "URL2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("UrlDAOTest.update.xml")
    @ExpectedDatabase("UrlDAOTest.update-expected.xml")
    public void testUpdate() {
        final UrlBean bean = urlDAO.getById(2L);
        Assert.assertNotNull(bean);
        bean.setCodeSite("CODE_SITE2");
        bean.setSectionCode("SECTION_CODE2");
        urlDAO.update(bean);
    }

    @Test
    @DatabaseSetup("UrlDAOTest.update-expected.xml")
    public void testGetBySectionCode() {
        UrlBean result = urlDAO.getBySectionCode("SECTION_CODE");
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("UrlDAOTest.update-expected.xml")
    public void testGetByUrlAndSite() {
        UrlBean result = urlDAO.getByUrlAndSite("URL", "CODE_SITE");
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("UrlDAOTest.update.xml")
    public void testGetByUrl() {
        UrlBean result = urlDAO.getByUrl("URL");
        Assert.assertNotNull(result);
        Assert.assertEquals(result.getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

}
