package com.univ.url.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.kportal.extension.module.plugin.rubrique.impl.BeanFichePageAccueil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.url.bean.SectionUrlLinkBean;
import com.univ.url.bean.UrlBean;
import com.univ.url.dao.SectionUrlLinkDAO;
import com.univ.url.dao.UrlDAO;
import com.univ.utils.URLResolver;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;

/**
 * Created on 21/11/2015.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/url/services/ServiceUrlTest.test-context.xml"})
public class ServiceUrlTest extends AbstractCacheTestngTests {

    @Autowired
    private ServiceUrl serviceUrl;

    @Mocked
    private UrlDAO urlDAO;

    @Mocked
    private SectionUrlLinkDAO sectionUrlLinkDAO;

    @Mocked
    private ServiceRubrique serviceRubrique;

    @Mocked
    private ServiceInfosSiteProcessus serviceInfosSite;

    @BeforeClass
    public void initMock() {
        urlDAO = new UrlDAO();
        sectionUrlLinkDAO = new SectionUrlLinkDAO();
        serviceRubrique = new ServiceRubrique();
        serviceInfosSite = new ServiceInfosSiteProcessus();
        serviceInfosSite.setServiceRubrique(serviceRubrique);
        serviceUrl.setUrlDAO(urlDAO);
        serviceUrl.setSectionUrlLinkDAO(sectionUrlLinkDAO);
        serviceUrl.setServiceRubrique(serviceRubrique);
        serviceUrl.setServiceInfosSite(serviceInfosSite);
    }

    private UrlBean initUrlBean() {
        final UrlBean urlBean = new UrlBean();
        urlBean.setId(0L);
        urlBean.setCodeSite("CODE_SITE");
        urlBean.setSectionCode("SECTION_CODE");
        urlBean.setParentsSectionCodes(Arrays.asList("PARENT_SECTION", "LANGAGE_SECTION", "SITE_SECTION"));
        urlBean.setUrl("url");
        return urlBean;
    }

    @Test
    public void testSave() {
        final UrlBean urlBean = initUrlBean();
        serviceUrl.save(urlBean);
        new Verifications() {{
            urlDAO.add(urlBean);
            times = 1;
            sectionUrlLinkDAO.addAll((List) any);
            times = 1;
        }};
        urlBean.setId(1L);
        new Expectations() {{
            urlDAO.update(urlBean);
            result = urlBean;
        }};
        serviceUrl.save(urlBean);
        new Verifications() {{
            urlDAO.update(urlBean);
            times = 1;
            sectionUrlLinkDAO.deleteByUrlId(1L);
            times = 1;
            sectionUrlLinkDAO.addAll((List) any);
            times = 1;
        }};
        final UrlBean urlWithoutParent = new UrlBean();
        urlWithoutParent.setCodeSite("CODE_SITE");
        urlWithoutParent.setParentsSectionCodes(Collections.<String>emptyList());
        urlWithoutParent.setSectionCode("SECTION_CODE");
        urlWithoutParent.setUrl("url");
        serviceUrl.save(urlWithoutParent);
        new Verifications() {{
            urlDAO.add(urlWithoutParent);
            times = 1;
        }};
    }

    @Test
    public void testDelete() {
        serviceUrl.delete(1L);
        new Verifications() {{
            urlDAO.delete(1L);
            times = 1;
            sectionUrlLinkDAO.deleteByUrlId(1L);
            times = 1;
        }};
    }

    @Test
    public void testGetById() {
        final UrlBean urlBean = initUrlBean();
        urlBean.setId(1L);
        final SectionUrlLinkBean sectionUrlLinkBean = new SectionUrlLinkBean();
        sectionUrlLinkBean.setIdUrl(1L);
        sectionUrlLinkBean.setSectionCode("PARENT_SECTION");
        sectionUrlLinkBean.setId(1L);
        new Expectations() {{
            urlDAO.getById(1L);
            result = urlBean;
            urlDAO.getById(2L);
            result = null;
            sectionUrlLinkDAO.getByUrlId(1L);
            result = sectionUrlLinkBean;
        }};
        UrlBean result = serviceUrl.getById(1L);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertNotNull(result.getParentsSectionCodes(), "les rubriques parentes ne peuvent pas être null");
        Assert.assertEquals(result.getParentsSectionCodes().size(), 1, "il doit y avoir 1 seul element dans la liste");
        Assert.assertEquals(result.getParentsSectionCodes().get(0), sectionUrlLinkBean.getSectionCode(), "le resultat doit correspondre au code de rubrique 'PARENT_SECTION'");
        result = serviceUrl.getById(2L);
        Assert.assertNull(result, "sans données en base le résultat doit être null");
    }

    @Test
    public void testFindUrlByCode(@Mocked(stubOutClassInitialization = true) final PageAccueilRubriqueManager pageAccueilRubriqueManager, @Mocked(stubOutClassInitialization = true) URLResolver urlResolver) {
        String url = serviceUrl.findUrlByCode(null, null);
        Assert.assertNotNull(url, "l'url ne doit pas être null");
        Assert.assertEquals(url, StringUtils.EMPTY, "l'url doit être vide");
        url = serviceUrl.findUrlByCode(StringUtils.EMPTY, null);
        Assert.assertNotNull(url, "l'url ne doit pas être null");
        Assert.assertEquals(url, StringUtils.EMPTY, "l'url doit être vide");
        final UrlBean urlBean = initUrlBean();
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("SECTION_CODE2");
        rubriqueBean.setCodeRubriqueMere("ROOT");
        rubriqueBean.setIntitule("INTITULE");
        final RubriqueBean rubriqueRoot = new RubriqueBean();
        rubriqueRoot.setCode("ROOT");
        rubriqueRoot.setCodeRubriqueMere("");
        rubriqueRoot.setIntitule("INTITULE_ROOT");
        final InfosSite infosSite =  new InfosSiteImpl();
        ((InfosSiteImpl)infosSite).setAlias("SITE1");
        ((InfosSiteImpl)infosSite).setModeReecritureRubrique(0);
        ((InfosSiteImpl)infosSite).setCodeRubrique("ROOT");
        ((InfosSiteImpl)infosSite).setHttpHostname("host.fr");
        new Expectations() {{
            urlDAO.getBySectionCode("SECTION_CODE");
            result = urlBean;
            urlDAO.getBySectionCode("SECTION_CODE2");
            result = null;
            urlDAO.getBySectionCode("SECTION_CODE3");
            result = null;
            serviceRubrique.getRubriqueByCode("SECTION_CODE2");
            result = rubriqueBean;
            serviceRubrique.getRubriqueByCode("SECTION_CODE3");
            result = rubriqueBean;
            serviceRubrique.getRubriqueByCode("ROOT");
            result = rubriqueRoot;
            serviceRubrique.getRubriqueByCode("");
            result = null;
            serviceInfosSite.determineSite((RubriqueBean) any, Boolean.FALSE);
            result = infosSite;
            PageAccueilRubriqueManager.getInstance();
            result = pageAccueilRubriqueManager;
            pageAccueilRubriqueManager.getBeanPageAccueil(rubriqueBean);
            result = new BeanFichePageAccueil();
            URLResolver.getAbsoluteUrl("/intitule-root/intitule/", infosSite, 0);
            result = "http://host.fr/intitule-root/intitule/";
            URLResolver.getAbsoluteUrl("url", infosSite, 0);
            result = "http://host.fr/url/";
            urlDAO.getByUrlAndSite("/intitule-root/intitule/", infosSite.getAlias());
            result = null;
        }};
        url = serviceUrl.findUrlByCode("SECTION_CODE", infosSite);
        Assert.assertNotNull(url, "l'url ne doit pas être null");
        Assert.assertEquals(url, "http://host.fr/url/", "l'url doit être égale à celle du bean retourné par le mock + le host");
        url = serviceUrl.findUrlByCode("SECTION_CODE", infosSite);
        Assert.assertEquals(url, "http://host.fr/url/", "l'url doit être égale à celle du bean retourné par le mock + le host");
        new Verifications() {{
            urlDAO.getBySectionCode("SECTION_CODE");
            times = 1; // cache
        }};
        url = serviceUrl.findUrlByCode("SECTION_CODE2", infosSite);
        Assert.assertNull(url, "l'url doit être null lorsque la page d'accueil est une fiche et que le mode de réécriture est à 0");
        ((InfosSiteImpl) infosSite).setModeReecritureRubrique(1);
        url = serviceUrl.findUrlByCode("SECTION_CODE3", infosSite);
        Assert.assertNotNull(url, "lorsque le mode de réécriture est != 0, l'url ne doit pas être null");
        Assert.assertEquals(url, "http://host.fr/intitule-root/intitule/", "l'url doit correspondre au host + le nom de la rubrique de site + de sa rubrique courante");
    }
}
