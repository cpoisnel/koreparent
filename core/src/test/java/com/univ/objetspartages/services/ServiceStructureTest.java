package com.univ.objetspartages.services;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

import mockit.Expectations;
import mockit.Invocation;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

/**
 * Created on 22/10/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/objetspartages/services/ServiceStructureTest.test-context.xml"})
public class ServiceStructureTest extends AbstractCacheTestngTests {

    @Autowired
    private ServiceStructure serviceStructure;

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testSave() {
        serviceStructure.save(null);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testDelete() {
        serviceStructure.delete(null);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testGetById() {
        serviceStructure.getById(null);
    }

    @Test(expectedExceptions = ErreurApplicative.class)
    public void testCheckCodeFail() throws Exception {
        // Caractère interdit
        serviceStructure.checkCode("CODE 1", "0");
    }

    @Test(expectedExceptions = ErreurApplicative.class)
    public void testCheckCodeFail2() throws Exception {
        final StructureModele structure = new MockUp<StructureModele>() {

            @Mock
            public void init() {}

            @Mock
            public int selectCodeLangueEtat(String code, String lang, String etat) {
                return 1;
            }
        }.getMockInstance();
        new MockUp<ReferentielObjets>() {

            @Mock
            public List<String> getListeCodesObjet() {
                return Arrays.asList("dummy");
            }

            @Mock
            public FicheUniv instancierFiche(String any) {
                return structure;
            }

            @Mock
            public String getNomObjet(final String any) {
                return "Dummy";
            }

            @Mock
            public String getCodeObjet(final FicheUniv any) {
                return "Dummy";
            }
        };
        serviceStructure.checkCode("CODE1", "0");
    }

    @Test
    public void testCheckCode() throws Exception {
        final StructureModele structure = new MockUp<StructureModele>() {

            @Mock
            public void init() {}

            @Mock
            public int selectCodeLangueEtat(String code, String lang, String etat) {
                return 0;
            }
        }.getMockInstance();
        new MockUp<ReferentielObjets>() {

            @Mock
            public List<String> getListeCodesObjet() {
                return Arrays.asList("dummy");
            }

            @Mock
            public FicheUniv instancierFiche(String any) {
                return structure;
            }
        };
        final String code = serviceStructure.checkCode("CODE1", "0");
        Assert.assertEquals("CODE1", code, "On devrait récupérer le code \"CODE1\"");
    }

    @Test
    public void testIsVisibleInFront(@Mocked("getByCodeAndIdFiche") final ServiceMetatag mockedServiceMetatag) {
        final ServiceMetatag serviceMetatag = new ServiceMetatag();
        serviceStructure.setServiceMetatag(serviceMetatag);
        final MetatagBean meta = new MetatagBean();
        meta.setMetaInTree("1");
        meta.setMetaIdFiche(1L);
        final StructureModele fiche = new MockUp<StructureModele>() {

            @Mock
            public Long getIdFiche() {
                return 1L;
            }
        }.getMockInstance();
        new MockUp<ReferentielObjets>() {

            @Mock
            public String getCodeObjet(FicheUniv fiche) {
                return StringUtils.EMPTY;
            }
        };
        new Expectations() {{
            serviceMetatag.getByCodeAndIdFiche(anyString, fiche.getIdFiche());
            result = meta;
        }};
        Assert.assertTrue(serviceStructure.isVisibleInFront(fiche));
        new Expectations() {{
            serviceMetatag.getByCodeAndIdFiche(anyString, fiche.getIdFiche());
            result = null;
        }};
        Assert.assertFalse(serviceStructure.isVisibleInFront(fiche));
    }

    @Test
    public void testCheckCycle(@Mocked({"getByCodeLanguage", "contains"}) ServiceStructure mockedServiceStructure) throws ErreurApplicative {
        final StructureModele structureCode1 = new MockUp<StructureModele>() {}.getMockInstance();
        final StructureModele structureCode2 = new MockUp<StructureModele>() {}.getMockInstance();
        new Expectations() {{
            serviceStructure.getByCodeLanguage("CODE1", "0");
            result = structureCode1;
            serviceStructure.getByCodeLanguage("CODE2", "0");
            result = structureCode2;
            serviceStructure.contains(structureCode1, structureCode2);
            result = false;
        }};
        serviceStructure.checkCycle("CODE1", "0", "CODE2");
    }

    @Test(expectedExceptions = ErreurApplicative.class)
    public void testCheckCycleFail(@Mocked({"getByCodeLanguage", "contains"}) ServiceStructure mockedServiceStructure) throws ErreurApplicative {
        final StructureModele structureCode1 = new MockUp<StructureModele>() {}.getMockInstance();
        final StructureModele structureCode2 = new MockUp<StructureModele>() {}.getMockInstance();
        new Expectations() {{
            serviceStructure.getByCodeLanguage("CODE1", "0");
            result = structureCode1;
            serviceStructure.getByCodeLanguage("CODE2", "0");
            result = structureCode2;
            serviceStructure.contains(structureCode1, structureCode2);
            result = true;
        }};
        serviceStructure.checkCycle("CODE1", "0", "CODE2");
    }

    @Test
    public void testCheckPermission1() {
        final AutorisationBean autorisation = new MockUp<AutorisationBean>() {

            @Mock
            public boolean possedePermissionPartielleSurPerimetre(final PermissionBean permission, final Perimetre perimetre) {
                return true;
            }

            @Mock
            private Hashtable<String, String> chargerNiveauxApprobation() {
                return new Hashtable<>();
            }
        }.getMockInstance();
        Assert.assertTrue(serviceStructure.checkPermission(autorisation, StringUtils.EMPTY, StringUtils.EMPTY));
    }

    @Test
    public void testCheckPermission2() {
        final AutorisationBean autorisation = new MockUp<AutorisationBean>() {

            @Mock
            public boolean possedePermissionPartielleSurPerimetre(Invocation context, final PermissionBean permission, final Perimetre perimetre) {
                return context.getInvocationCount() == 3;
            }

            @Mock
            private Hashtable<String, String> chargerNiveauxApprobation() {
                return new Hashtable<>();
            }
        }.getMockInstance();
        Assert.assertFalse(serviceStructure.checkPermission(autorisation, "FICHE/0001/D", StringUtils.EMPTY));
        Assert.assertTrue(serviceStructure.checkPermission(autorisation, "FICHE/0001/M", StringUtils.EMPTY));
    }

    @Test
    public void testGetStructurePremierNiveau(@Mocked({"getByCodeLanguage", "getLevel"}) ServiceStructure mockedServiceStructure) {
        final StructureModele structureCode1 = new MockUp<StructureModele>() {

            @Mock
            public String getCodeRattachement() {
                return "CODE2";
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        final StructureModele structureCode2 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE2";
            }

            @Mock
            public String getCodeRattachement() {
                return null;
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        new Expectations() {{
            serviceStructure.getByCodeLanguage("CODE1", "0");
            result = structureCode1;
            serviceStructure.getLevel(structureCode1);
            result = 2;
            serviceStructure.getByCodeLanguage("CODE2", "0");
            result = structureCode2;
            serviceStructure.getLevel(structureCode2);
            result = 1;
        }};
        Assert.assertNull(serviceStructure.getStructurePremierNiveau(StringUtils.EMPTY, StringUtils.EMPTY), "Le premier appel doit retourner null");
        final StructureModele result = serviceStructure.getStructurePremierNiveau("CODE1", "0");
        Assert.assertNotNull(result, "Le second appel devrait retourner une structure.");
        Assert.assertEquals(result.getCode(), "CODE2", "Le second appel doit retourner la structure portant le code \"CODE2\"");
    }

    @Test
    public void testGetByCodeLanguage1() {
        final StructureModele structure = new MockUp<StructureModele>() {

            @Mock
            public void init() {}

            @Mock
            public int selectCodeLangueEtat(String code, String langue, String etat) throws Exception {
                throw new Exception();
            }
        }.getMockInstance();
        new MockUp<ContexteUtil>() {

            @Mock
            public ContexteUniv getContexteUniv() {
                return new MockUp<ContexteUniv>() {

                    @Mock
                    public void $clinit() { /* do nothing */ }
                }.getMockInstance();
            }
        };
        new MockUp<ReferentielObjets>() {

            @Mock
            public Collection<String> getListeCodesObjet() {
                return Arrays.asList("0001");
            }

            @Mock
            public FicheUniv instancierFiche(String nomOUcode) {
                return structure;
            }
        };
        new MockUp<LangueUtil>() {

            @Mock
            public String getIndiceLocaleDefaut() {
                return "0";
            }
        };
        Assert.assertNull(serviceStructure.getByCodeLanguage(StringUtils.EMPTY, "0"), "Le premier appel devrait renvoyer null");
        Assert.assertNull(serviceStructure.getByCodeLanguage("PLOP", "0"), "Le second appel devrait renvoyer null");
    }

    @Test
    public void testGetByCodeLanguage2() {
        final StructureModele structure = new MockUp<StructureModele>() {

            @Mock
            public void init() {}

            @Mock
            public String getCode() {
                return "Structure1";
            }

            @Mock
            public int selectCodeLangueEtat(String code, String langue, String etat) throws Exception {
                return 1;
            }

            @Mock
            public boolean nextItem(Invocation context) {
                return context.getInvocationCount() == 1;
            }
        }.getMockInstance();
        new MockUp<ContexteUtil>() {

            @Mock
            public ContexteUniv getContexteUniv() {
                return new MockUp<ContexteUniv>() {

                    @Mock
                    public void $clinit() { /* do nothing */ }
                }.getMockInstance();
            }
        };
        new MockUp<ReferentielObjets>() {

            @Mock
            public Collection<String> getListeCodesObjet() {
                return Arrays.asList("0001");
            }

            @Mock
            public FicheUniv instancierFiche(String nomOUcode) {
                return structure;
            }
        };
        new MockUp<LangueUtil>() {

            @Mock
            public String getIndiceLocaleDefaut() {
                return "0";
            }
        };
        final StructureModele resultStructure = serviceStructure.getByCodeLanguage("PLOP", "0");
        Assert.assertNotNull(resultStructure, "Le second appel devrait renvoyer une structure");
        Assert.assertEquals(resultStructure.getCode(), "Structure1", "Le second appel devrait renvoyer une structure avec le code \"Structure 1\"");
    }

    @Test
    public void testGetDisplayableLabel(@Mocked("getByCodeLanguage") ServiceStructure mockedServiceStructure) {
        Assert.assertEquals(serviceStructure.getDisplayableLabel(StringUtils.EMPTY, "0"), StringUtils.EMPTY, "Le premier appel devrait renvoyer une chaîne vide");
        Assert.assertEquals(serviceStructure.getDisplayableLabel(null, "0"), StringUtils.EMPTY, "Le second appel devrait renvoyer une chaîne vide");
        final StructureModele structureCode1 = new MockUp<StructureModele>() {

            @Mock
            public String getLibelleLong() {
                return "Structure long 1";
            }
        }.getMockInstance();
        final StructureModele structureCode2 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE2";
            }

            @Mock
            public String getLibelleLong() {
                return null;
            }

            @Mock
            public String getLibelleAffichable() {
                return "Structure 2";
            }
        }.getMockInstance();
        final StructureModele structureCode3 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE3";
            }

            @Mock
            public String getLibelleLong() {
                return null;
            }

            @Mock
            public String getLibelleAffichable() {
                return null;
            }
        }.getMockInstance();
        new Expectations() {{
            serviceStructure.getByCodeLanguage("CODE1", "0");
            result = structureCode1;
            serviceStructure.getByCodeLanguage("CODE2", "0");
            result = structureCode2;
            serviceStructure.getByCodeLanguage("CODE3", "0");
            result = structureCode3;
        }};
        final String result = serviceStructure.getDisplayableLabel("CODE1;CODE2;CODE3", "0");
        Assert.assertNotEquals(result, StringUtils.EMPTY, "Le troisième appel devrait renvoyer une chaîne");
        Assert.assertEquals(result, "Structure long 1;Structure 2;-", "Le troisième appel devrait renvoyer une chaîne");
    }

    @Test
    public void testGetFirstLevelLabel(@Mocked({"getStructurePremierNiveau", "getDisplayableLabel"}) ServiceStructure mockedServiceStructure) {
        Assert.assertEquals(serviceStructure.getFirstLevelLabel(null, "0"), "-", "Le premier appel devrait renvoyer \"-\"");
        final StructureModele structureCode1 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE1";
            }
        }.getMockInstance();
        new Expectations() {{
            serviceStructure.getStructurePremierNiveau("CODE2", "0");
            result = structureCode1;
            serviceStructure.getDisplayableLabel("CODE1", "0");
            result = "Structure1";
        }};
        Assert.assertEquals(serviceStructure.getFirstLevelLabel("CODE2", "0"), "Structure1", "Le second appel devrait renvoyer \"Structure1\"");
    }

    @Test
    public void testGetAttachementLabel(@Mocked({"getByCodeLanguage", "getLevel", "getDisplayableLabel"}) ServiceStructure mockedServiceStructure) {
        Assert.assertEquals(serviceStructure.getAttachementLabel(null, "0", false), StringUtils.EMPTY, "Le premier appel devrait renvoyer une chaîne vide.");
        final StructureModele structureCode1 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE1";
            }

            @Mock
            public String getCodeRattachement() {
                return "CODE2";
            }
        }.getMockInstance();
        final StructureModele structureCode2 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE2";
            }

            @Mock
            public String getCodeRattachement() {
                return null;
            }
        }.getMockInstance();
        final StructureModele structureCode3 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE3";
            }

            @Mock
            public String getCodeRattachement() {
                return "CODE1";
            }
        }.getMockInstance();
        new Expectations() {{
            serviceStructure.getByCodeLanguage("CODE1", "0");
            result = structureCode1;
            serviceStructure.getLevel(structureCode1);
            result = 2;
            serviceStructure.getDisplayableLabel("CODE1", "0");
            result = "Structure 1";
            serviceStructure.getByCodeLanguage("CODE2", "0");
            result = structureCode2;
            serviceStructure.getDisplayableLabel("CODE2", "0");
            result = "Structure 2";
            serviceStructure.getLevel(structureCode2);
            result = 1;
            serviceStructure.getByCodeLanguage("CODE3", "0");
            result = structureCode3;
            serviceStructure.getDisplayableLabel("CODE3", "0");
            result = "Structure 3";
            serviceStructure.getLevel(structureCode3);
            result = 3;
        }};
        Assert.assertEquals(serviceStructure.getAttachementLabel("CODE2", "0", false), StringUtils.EMPTY, "Le second appel devrait renvoyer une chaîne vide.");
        Assert.assertEquals(serviceStructure.getAttachementLabel("CODE2", "0", true), "Structure 2", "Le second appel devrait renvoyer \"Structure 2\".");
        Assert.assertEquals(serviceStructure.getAttachementLabel("CODE1", "0", true), "Structure 2<br />Structure 1", "Le troisième appel devrait renvoyer \"Structure 2<br />Structure 1\".");
        Assert.assertEquals(serviceStructure.getAttachementLabel("CODE3", "0", true), "Structure 2<br />Structure 1<br />Structure 3", "Le quatrième appel devrait renvoyer \"Structure 3<br />Structure 2<br />Structure 1\".");
    }

    @Test
    public void testGetAllStructures() {
        final StructureModele structure0 = new StructureModeleDummy(true, "Structure 0");
        final StructureModele structure1 = new StructureModeleDummy(false, "Structure 1");
        new MockUp<ReferentielObjets>() {

            @Mock
            public Collection<String> getListeCodesObjet() {
                return Collections.singletonList("0001");
            }

            @Mock
            public FicheUniv instancierFiche(Invocation context, String nomOUcode) {
                return context.getInvocationCount() == 1 ? structure0 : structure1;
            }
        };
        Assert.assertTrue(MapUtils.isEmpty(serviceStructure.getAllStructures()), "Le premier appel doit renvoyer une map vide");
        final Map<String, StructureModele> result = serviceStructure.getAllStructures(true);
        Assert.assertTrue(MapUtils.isNotEmpty(result), "Le second appel doit renvoyer une map avec un élément");
        Assert.assertTrue(result.containsKey("Structure 1"), "Le second appel doit renvoyer une map contenant \"Structure 1\" en clé");
        Assert.assertNotNull(result.get("Structure 1"), "Le second appel doit renvoyer une map contenant une structure non nulle pour la clé \"Structure 1\"");
        Assert.assertEquals(result.get("Structure 1").getCode(), "Structure 1", "Le second appel doit renvoyer une map contenant une structure dont le code est \"Structure 1\"");
    }

    @Test
    public void testGetSubstructures() {
        final StructureModele structure1 = new StructureModeleDummy(true, "CODE1");
        final StructureModele structure2 = new StructureModeleDummy(false, "CODE1");
        new MockUp<ReferentielObjets>() {

            @Mock
            public Collection<String> getListeCodesObjet() {
                return Arrays.asList("0001");
            }

            @Mock
            public FicheUniv instancierFiche(Invocation context, String nomOUcode) {
                return context.getInvocationCount() == 1 ? structure1 : structure2;
            }
        };
        final List<StructureModele> result1 = serviceStructure.getSubStructures("CODE1", "0", false);
        Assert.assertNotNull(result1, "Le premier appel doit renvoyer une liste non nulle");
        Assert.assertTrue(CollectionUtils.isEmpty(result1), "Le premier appel doit renvoyer une liste vide");
        serviceStructure.updateCache(structure1);
        final List<StructureModele> result2 = serviceStructure.getSubStructures("CODE1", "0", false);
        Assert.assertNotNull(result2, "Le second appel doit renvoyer une liste non nulle");
        Assert.assertTrue(CollectionUtils.isNotEmpty(result2) && result2.size() == 1, "Le second appel doit renvoyer une liste contenant 1 élément");
        Assert.assertTrue(result2.get(0).getCode().equals("CODE1"));
    }

    @Test
    public void testGetBreadCrumbs(@Mocked({"getByCodeLanguage", "getLevel"}) ServiceStructure mockedServiceStructure) {
        final StructureModele structureCode1 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE1";
            }

            @Mock
            public String getLibelleCourt() {
                return "Structure1";
            }

            @Mock
            public String getCodeRattachement() {
                return "CODE2";
            }
        }.getMockInstance();
        final StructureModele structureCode2 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE2";
            }

            @Mock
            public String getLibelleCourt() {
                return "Structure2";
            }

            @Mock
            public String getCodeRattachement() {
                return null;
            }
        }.getMockInstance();
        final StructureModele structureCode3 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE3";
            }

            @Mock
            public String getLibelleCourt() {
                return "Structure3";
            }

            @Mock
            public String getCodeRattachement() {
                return "CODE1";
            }
        }.getMockInstance();
        new Expectations() {{
            serviceStructure.getByCodeLanguage("CODE1", "0");
            result = structureCode1;
            serviceStructure.getLevel(structureCode1);
            result = 2;
            serviceStructure.getByCodeLanguage("CODE2", "0");
            result = structureCode2;
            serviceStructure.getLevel(structureCode2);
            result = 1;
            serviceStructure.getByCodeLanguage("CODE3", "0");
            result = structureCode3;
            serviceStructure.getLevel(structureCode3);
            result = 3;
        }};
        Assert.assertEquals(serviceStructure.getBreadCrumbs(null, "0"), StringUtils.EMPTY, "Le premier appel doit renvoyer une chaîne vide");
        Assert.assertEquals(serviceStructure.getBreadCrumbs("CODE2", "0"), "Structure2", "Le premier appel doit renvoyer la chaîne \"Structure2\"");
        final String breadCrumbs1 = serviceStructure.getBreadCrumbs("CODE1", "0");
        final String breadCrumbs2 = serviceStructure.getBreadCrumbs("CODE3", "0");
        Assert.assertEquals(breadCrumbs1, "Structure2 &gt; Structure1", "Le troisième appel devrait renvoyer \"Structure1 &gt; Structure2\"");
        Assert.assertEquals(breadCrumbs2, "Structure2 &gt; Structure1 &gt; Structure3", "Le quatrièeme appel devrait renvoyer \"Structure2 &gt; Structure1 &gt; Structure3\"");

    }

    @Test
    public void testGetLevel(@Mocked("getByCodeLanguage") ServiceStructure mockedServiceStructure) throws Exception {
        final StructureModele structureCode1 = new MockUp<StructureModele>() {

            @Mock
            public String getCodeRattachement() {
                return "CODE2";
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        final StructureModele structureCode2 = new MockUp<StructureModele>() {

            @Mock
            public String getCodeRattachement() {
                return null;
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        final StructureModele structureCode3 = new MockUp<StructureModele>() {

            @Mock
            public String getCodeRattachement() {
                return "CODE1";
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        new Expectations() {{
            serviceStructure.getByCodeLanguage("CODE1", "0");
            result = structureCode1;
            serviceStructure.getByCodeLanguage("CODE2", "0");
            result = structureCode2;
        }};
        final int levelCode1 = serviceStructure.getLevel(structureCode1);
        final int levelCode2 = serviceStructure.getLevel(structureCode2);
        final int levelCode3 = serviceStructure.getLevel(structureCode3);
        final int levelCode4 = serviceStructure.getLevel(null);
        Assert.assertEquals(levelCode1, 2, "La structure 1 devrait avoir un niveau égal à 2");
        Assert.assertEquals(levelCode2, 1, "La structure 2 devrait avoir un niveau égal à 1");
        Assert.assertEquals(levelCode3, 3, "La structure 3 devrait avoir un niveau égal à 3");
        Assert.assertEquals(levelCode4, 0, "La valeur \"null\" devrait avoir un niveau égal à 0");
    }

    @Test
    public void testContains(@Mocked({"getByCodeLanguage", "getSubStructures"}) ServiceStructure mockedServiceStructure) throws Exception {
        final StructureModele structureCode1 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE1";
            }

            @Mock
            public String getCodeRattachement() {
                return "CODE2";
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        final StructureModele structureCode2 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE2";
            }

            @Mock
            public String getCodeRattachement() {
                return null;
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        final StructureModele structureCode3 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE3";
            }

            @Mock
            public String getCodeRattachement() {
                return "CODE1";
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        new Expectations() {{
            serviceStructure.getSubStructures("CODE2", "0", true);
            result = Arrays.asList(structureCode1, structureCode3);
            serviceStructure.getSubStructures("CODE1", "0", true);
            result = Arrays.asList(structureCode3);
            serviceStructure.getSubStructures("CODE3", "0", true);
            result = Collections.EMPTY_LIST;
        }};
        final boolean contains1 = serviceStructure.contains(structureCode2, structureCode1);
        final boolean contains2 = serviceStructure.contains(structureCode2, structureCode3);
        final boolean contains3 = serviceStructure.contains(structureCode1, structureCode3);
        final boolean contains4 = serviceStructure.contains(structureCode3, structureCode1);
        final boolean contains5 = serviceStructure.contains(structureCode3, structureCode1);
        final boolean contains6 = serviceStructure.contains(structureCode3, structureCode2);
        final boolean contains7 = serviceStructure.contains(null, structureCode2);
        Assert.assertTrue(contains1, "La structure \"CODE2\" devrait contenir la structure  \"CODE1\"");
        Assert.assertTrue(contains2, "La structure \"CODE2\" devrait contenir la structure  \"CODE3\"");
        Assert.assertTrue(contains3, "La structure \"CODE1\" devrait contenir la structure  \"CODE3\"");
        Assert.assertFalse(contains4, "La structure \"CODE1\" ne devrait pas contenir la structure  \"CODE2\"");
        Assert.assertFalse(contains5, "La structure \"CODE3\" ne devrait pas contenir la structure  \"CODE1\"");
        Assert.assertFalse(contains6, "La structure \"CODE3\" ne devrait pas contenir la structure  \"CODE2\"");
        Assert.assertTrue(contains7, "La valeur \"null\" (racine) devrait contenir la structure  \"CODE2\"");
    }

    @Test
    public void testGetStructureListWithFullLabels(@Mocked({"getAllStructures", "getAttachementLabel"}) ServiceStructure mockedServiceStructure) {
        final StructureModele structureCode1 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE1";
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        final StructureModele structureCode2 = new MockUp<StructureModele>() {

            @Mock
            public String getCode() {
                return "CODE2";
            }

            @Mock
            public String getLangue() {
                return "0";
            }
        }.getMockInstance();
        final Map<String, StructureModele> allStructures = new HashMap<>();
        allStructures.put("CODE1", structureCode1);
        allStructures.put("CODE2", structureCode2);
        new Expectations(){{
            serviceStructure.getAllStructures();
            result = allStructures;
            serviceStructure.getAttachementLabel("CODE1", "0", true);
            result = "Structure 2<br />Structure 1";
            serviceStructure.getAttachementLabel("CODE2", "0", true);
            result = "Structure 2";
        }};
        final Map<String, String> result = serviceStructure.getStructureListWithFullLabels();
        Assert.assertTrue(MapUtils.isNotEmpty(result) && result.size() == 2, "La map renvoyer devrait contenir 2 éléments");
        Assert.assertEquals(result.get("CODE1"), "Structure 2 > Structure 1");
        Assert.assertEquals(result.get("CODE2"), "Structure 2");
    }
}
