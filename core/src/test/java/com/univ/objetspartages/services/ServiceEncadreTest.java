package com.univ.objetspartages.services;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.dao.impl.EncadreDAO;
import com.univ.objetspartages.om.Article;
import com.univ.objetspartages.om.PageLibre;
import com.univ.objetspartages.om.ReferentielObjets;

import mockit.Expectations;
import mockit.Mocked;

/**
 * Ajout de TU sur le service des encadrés.
 * Created on 29/10/2015.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/services/ServiceEncadreTest.test-context.xml"})
public class ServiceEncadreTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ServiceEncadre serviceEncadre;

    @Mocked
    private EncadreDAO encadreDAO;

    @BeforeClass
    public void initMock(){
        encadreDAO = new EncadreDAO();
        serviceEncadre.setDao(encadreDAO);
    }

    @Test
    public void testGetListObjets() {
        EncadreBean encadreBean = new EncadreBean();
        List<String> objetcs = serviceEncadre.getListObjets(encadreBean);
        Assert.assertNotNull(objetcs, "le résultat ne peut être null");
        Assert.assertTrue(objetcs.isEmpty(), "la liste doit être vide");
        encadreBean.setObjets("foo;bar;baz");
        objetcs = serviceEncadre.getListObjets(encadreBean);
        Assert.assertNotNull(objetcs, "le résultat ne peut être null");
        Assert.assertEquals(objetcs.size(), 3, "la taille doit être egale à 3");
        Assert.assertEquals(objetcs.get(0), "foo", "la première valeur doit être égale à foo");
    }

    @Test
    public void testSetListObjet() {
        EncadreBean encadreBean = new EncadreBean();
        serviceEncadre.setListObjet(encadreBean, null);
        Assert.assertNotNull(encadreBean.getObjets(), "le résultat ne peut être null");
        Assert.assertTrue(encadreBean.getObjets().isEmpty(), "le résultat doit être vide");
        serviceEncadre.setListObjet(encadreBean, Arrays.asList("foo", "bar", "baz"));
        Assert.assertNotNull(encadreBean.getObjets(), "le résultat ne peut être null");
        Assert.assertEquals(encadreBean.getObjets(), "foo;bar;baz", "la valeur doit être égale à foo;bar;baz");
    }

    @Test
    public void testDeleteByCodesRubriques() {
        final EncadreBean encadreBean = new EncadreBean();
        encadreBean.setCode("code");
        encadreBean.setCodeRubrique("codeRubriqueé");
        final List<String> codesRubriques = Collections.singletonList("codeRubriqueé");
        new Expectations() {{
            encadreDAO.add(encadreBean);
            result = encadreBean;
            encadreDAO.getByCodesRubrique(codesRubriques);
            result = Collections.<EncadreBean>emptyList();
        }};
        serviceEncadre.save(encadreBean);
        serviceEncadre.deleteByCodesRubriques(codesRubriques);
        List<EncadreBean> result = serviceEncadre.getByCodesRubrique(codesRubriques);
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être une liste vide");
    }

    @Test
    public void testGetByCode() {
        final EncadreBean encadreBean = new EncadreBean();
        encadreBean.setCode("code");
        new Expectations() {{
            encadreDAO.getByCode("noResult");
            result = null;
            encadreDAO.getByCode("code");
            result = encadreBean;
        }};
        final EncadreBean result = serviceEncadre.getByCode("code");
        Assert.assertNotNull(result);
        Assert.assertEquals(result, encadreBean, "le résultat doit correspondre à l'encadré ayant pour code : code");
        final EncadreBean nullResult = serviceEncadre.getByCode("noResult");
        Assert.assertNull(nullResult, "le résultat doit être null");
    }

    @Test
    public void testGetByCodesRubriques() {
        final EncadreBean encadreBean = new EncadreBean();
        encadreBean.setCode("code");
        encadreBean.setCodeRubrique("codeRubriqueé");
        final List<String> codesRubriques = Collections.singletonList("codeRubriqueé");
        new Expectations() {{
            encadreDAO.getByCodesRubrique(codesRubriques);
            result = Collections.singletonList(encadreBean);
        }};
        List<EncadreBean> result = serviceEncadre.getByCodesRubrique(codesRubriques);
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir un élément");
        Assert.assertEquals(result.get(0), encadreBean, "le résultat doit correspondre à l'encadré ayant pour code : code");
    }

    @Test
    public void testGetAllEncadres() {
        final EncadreBean encadreBean = new EncadreBean();
        encadreBean.setCode("code");
        new Expectations() {{
            encadreDAO.getAllEncadres();
            result = Collections.singletonList(encadreBean);
        }};
        List<EncadreBean> result = serviceEncadre.getAllEncadres();
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir un élément");
        Assert.assertEquals(result.get(0), encadreBean, "le résultat doit correspondre à l'encadré ayant pour code : code");
    }

    @Test
    public void testGetEncadreObjectList(@Mocked(stubOutClassInitialization = true) ReferentielObjets mockedReferentiel,
        @Mocked PageLibre mockedPagelibre, @Mocked final Article mockedArticle) {
        final EncadreBean encadreBean = new EncadreBean();
        encadreBean.setCode("codeEncadre");
        new Expectations() {{
            ReferentielObjets.getCodeObjetParClasse("com.univ.objetspartages.om.PageLibre");
            result = null;
            ReferentielObjets.getCodeObjetParClasse("com.univ.objetspartages.om.Article");
            result = "codeObjet";
            mockedArticle.getCodeRattachement();
            result = "codeRattachement";
            encadreDAO.getEncadreObjectList("codeRubrique", "codeRattachement", "codeObjet", "0");
            result = Collections.singletonList(encadreBean);
        }};
        List<EncadreBean> results = serviceEncadre.getEncadreObjectList("codeRubrique", null, "0");
        Assert.assertNotNull(results, "le résultat ne peut être null");
        Assert.assertTrue(results.isEmpty(), "le résultat doit être vide");
        results = serviceEncadre.getEncadreObjectList("codeRubrique", mockedPagelibre, "0");
        Assert.assertNotNull(results, "le résultat ne peut être null");
        Assert.assertTrue(results.isEmpty(), "le résultat doit être vide");
        results = serviceEncadre.getEncadreObjectList("codeRubrique", mockedArticle, "0");
        Assert.assertNotNull(results, "le résultat ne peut être vide");
        Assert.assertEquals(results.size(), 1, "le résultat doit être ine liste de un element");
        Assert.assertEquals(results.get(0).getCode(), encadreBean.getCode(), "le résultat doit avoir pour code codeEncadre");
    }

    @Test
    public void testGetEncadresList(@Mocked(stubOutClassInitialization = true) ReferentielObjets mockedReferentiel,
        @Mocked PageLibre mockedPagelibre, @Mocked final Article mockedArticle) {
        final EncadreBean encadreBean = new EncadreBean();
        encadreBean.setContenu("contenu encadre");
        new Expectations() {{
            ReferentielObjets.getCodeObjetParClasse("com.univ.objetspartages.om.PageLibre");
            result = null;
            ReferentielObjets.getCodeObjetParClasse("com.univ.objetspartages.om.Article");
            result = "codeObjet";
            mockedArticle.getCodeRattachement();
            result = "codeRattachement";
            encadreDAO.getEncadreObjectList("codeRubrique", "codeRattachement", "codeObjet", "0");
            result = Collections.singletonList(encadreBean);
        }};
        final List<String> resultPagelibre = serviceEncadre.getEncadresList("codeRubrique", mockedPagelibre, "0");
        Assert.assertNotNull(resultPagelibre, "le résultat ne peut être null");
        Assert.assertTrue(resultPagelibre.isEmpty(), "le résultat doit être vide");
        final List<String> resultArticle = serviceEncadre.getEncadresList("codeRubrique", mockedArticle, "0");
        Assert.assertNotNull(resultArticle, "le résultat ne peut pas être null");
        Assert.assertEquals(resultArticle.size(), 1, "le résultat doit être une liste de un element");
        Assert.assertEquals(resultArticle.get(0), encadreBean.getContenu(), "le résultat doit être égal au contenu de l'encadré");
    }

    @Test
    public void testGetEncadreSearchList(@Mocked(stubOutClassInitialization = true) ReferentielObjets mockedReferentiel) throws ReflectiveOperationException {
        new Expectations() {{
            ReferentielObjets.getListeCodesObjet();
            result = Collections.emptyList();
        }};
        final Map<String, String> noResult = serviceEncadre.getEncadreSearchList();
        Assert.assertNotNull(noResult, "le résultat ne peut être null");
        Assert.assertTrue(noResult.isEmpty(), "le résultat doit être vide");
        new Expectations() {{
            ReferentielObjets.getListeCodesObjet();
            result = Arrays.asList("0016", "0015", "0014");
            ReferentielObjets.gereEncadreRechercheEmbarquable("0016");
            result = false;
            ReferentielObjets.gereEncadreRechercheEmbarquable("0015");
            result = true;
            ReferentielObjets.gereEncadreRechercheEmbarquable("0014");
            result = new ReflectiveOperationException("exception expected");
            ReferentielObjets.getLibelleObjet("0015");
            result = "article";
        }};
        final Map<String, String> oneResult = serviceEncadre.getEncadreSearchList();
        Assert.assertNotNull(oneResult, "le résultat ne peut être null");
        Assert.assertEquals(oneResult.size(), 1, "le résultat doit être une liste de un element");
        Assert.assertEquals(oneResult.get("0015"), "article", "la map doit contenir l'entry 0015 -> article");
    }
}
