package com.univ.objetspartages.services;

import java.util.Date;
import java.util.UUID;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.om.StructureModele;

/**
 * Created on 29/10/15.
 */
public class StructureModeleDummy implements StructureModele {

    private static final long serialVersionUID = 3945911924918995608L;

    private boolean failSelect;

    private String code;

    private boolean nextItem = true;

    public StructureModeleDummy() {
    }

    public StructureModeleDummy(boolean failSelect, String code) {
        this.failSelect = failSelect;
        this.code = code;
    }

    @Override
    public String getLibelleCourt() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void setLibelleCourt(final String libelle) {
    }

    @Override
    public String getLibelleLong() {
        return null;
    }

    @Override
    public void setLibelleLong(final String libelle) {
    }

    @Override
    public String getOnglets() {
        return null;
    }

    @Override
    public void setOnglets(final String onglets) {
    }

    @Override
    public String getTypeStructure() {
        return null;
    }

    @Override
    public void setTypeStructure(final String typeStructure) {
    }

    @Override
    public Long getIdBandeau() {
        return null;
    }

    @Override
    public void setIdBandeau(final Long idBandeau) {
    }

    @Override
    public String getCouleurTitre() {
        return null;
    }

    @Override
    public void setCouleurTitre(final String couleurTitre) {
    }

    @Override
    public String getCouleurFond() {
        return null;
    }

    @Override
    public void setCouleurFond(final String couleurFond) {
    }

    @Override
    public String getAdresse() {
        return null;
    }

    @Override
    public void setAdresse(final String adresse) {
    }

    @Override
    public String getCodePostal() {
        return null;
    }

    @Override
    public void setCodePostal(final String codePostal) {
    }

    @Override
    public String getVille() {
        return null;
    }

    @Override
    public void setVille(final String ville) {
    }

    @Override
    public String getTelephone() {
        return null;
    }

    @Override
    public void setTelephone(final String telephone) {
    }

    @Override
    public String getTelecopie() {
        return null;
    }

    @Override
    public void setTelecopie(final String telecopie) {
    }

    @Override
    public String getAdresseMail() {
        return null;
    }

    @Override
    public void setAdresseMail(final String adresseMail) {
    }

    @Override
    public String getSiteWeb() {
        return null;
    }

    @Override
    public void setSiteWeb(final String siteWeb) {
    }

    @Override
    public Long getIdPlanAcces() {
        return null;
    }

    @Override
    public String getAttributSpecifique1() {
        return null;
    }

    @Override
    public String getAttributSpecifique2() {
        return null;
    }

    @Override
    public String getAttributSpecifique3() {
        return null;
    }

    @Override
    public String getAttributSpecifique4() {
        return null;
    }

    @Override
    public String getAttributSpecifique5() {
        return null;
    }

    @Override
    public String getComplementCoordonnees() {
        return null;
    }

    @Override
    public void setComplementCoordonnees(final String complements) {
    }

    @Override
    public String getActivite() {
        return null;
    }

    @Override
    public void setActivite(final String activite) {
    }

    @Override
    public String getCodeResponsable() {
        return null;
    }

    @Override
    public void setCodeResponsable(final String codeResponsable) {
    }

    @Override
    public void init() {
    }

    @Override
    public Long getIdFiche() {
        return null;
    }

    @Override
    public void setIdFiche(final Long idFiche) {
    }

    @Override
    public String getCodeRubrique() {
        return null;
    }

    @Override
    public void setCodeRubrique(final String codeRubrique) {
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getLangue() {
        return null;
    }

    @Override
    public String getCodeRedacteur() {
        return null;
    }

    @Override
    public String getCodeValidation() {
        return null;
    }

    @Override
    public String getCodeRattachement() {
        return null;
    }

    @Override
    public String getEtatObjet() {
        return null;
    }

    @Override
    public String getMessageAlerte() {
        return null;
    }

    @Override
    public String getMetaKeywords() {
        return null;
    }

    @Override
    public String getMetaDescription() {
        return null;
    }

    @Override
    public String getTitreEncadre() {
        return null;
    }

    @Override
    public String getContenuEncadre() {
        return null;
    }

    @Override
    public String getEncadreRecherche() {
        return null;
    }

    @Override
    public String getEncadreRechercheBis() {
        return null;
    }

    @Override
    public Date getDateAlerte() {
        return null;
    }

    @Override
    public Date getDateCreation() {
        return null;
    }

    @Override
    public Date getDateModification() {
        return null;
    }

    @Override
    public Date getDateProposition() {
        return null;
    }

    @Override
    public Date getDateValidation() {
        return null;
    }

    @Override
    public Long getNbHits() {
        return null;
    }

    @Override
    public void setCode(final String code) {
        this.code = code;
    }

    @Override
    public void setLangue(final String langue) {
    }

    @Override
    public void setCtx(final OMContext ctx) {
    }

    @Override
    public void setCodeRedacteur(final String codeRedacteur) {
    }

    @Override
    public void setCodeValidation(final String codeValidation) {
    }

    @Override
    public void setCodeRattachement(final String codeRattachement) {
    }

    @Override
    public void setEtatObjet(final String etatObjet) {
    }

    @Override
    public void setMessageAlerte(final String messageAlerte) {
    }

    @Override
    public void setMetaKeywords(final String metaKeywords) {
    }

    @Override
    public void setMetaDescription(final String metaDescription) {
    }

    @Override
    public void setTitreEncadre(final String titreEncadre) {
    }

    @Override
    public void setContenuEncadre(final String contenuEncadre) {
    }

    @Override
    public void setEncadreRecherche(final String encadreRecherche) {
    }

    @Override
    public void setEncadreRechercheBis(final String encadreRecherche) {
    }

    @Override
    public void setDateAlerte(final Date dateAlerte) {
    }

    @Override
    public void setDateCreation(final Date dateCreation) {
    }

    @Override
    public void setDateModification(final Date dateCreation) {
    }

    @Override
    public void setDateProposition(final Date dateProposition) {
    }

    @Override
    public void setDateValidation(final Date dateValidation) {
    }

    @Override
    public void setNbHits(final Long nbHits) {
    }

    @Override
    public String getLibelleAffichable() {
        return null;
    }

    @Override
    public String getReferences() {
        return null;
    }

    @Override
    public void dupliquer(final String nouvelleLangue) throws Exception {
    }

    @Override
    public int selectCodeLangueEtat(final String code, final String langue, final String etat) throws Exception {
        return 0;
    }

    @Override
    public int traiterRequete(final String requete) throws Exception {
        return 0;
    }

    @Override
    public int selectParCodeRubrique(final String codeRubrique, final String langue) throws Exception {
        return 0;
    }

    @Override
    public void add() throws Exception {
    }

    @Override
    public void addWithForcedId() throws Exception {
    }

    @Override
    public void delete() throws Exception {
    }

    @Override
    public void update() throws Exception {
    }

    @Override
    public int select(final String requete) throws Exception {
        if(failSelect) {
            throw new Exception();
        }
        return 1;
    }

    @Override
    public void retrieve() throws Exception {
    }

    @Override
    public boolean nextItem() throws Exception {
        if(nextItem) {
            nextItem = false;
            return true;
        } else {
            return false;
        }
    }
}
