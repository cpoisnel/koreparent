package com.univ.objetspartages.services;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.cache.CacheLibelleManager;
import com.univ.objetspartages.dao.impl.LabelDAO;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.sql.RequeteSQL;

import mockit.Capturing;
import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.Verifications;

/**
 * Created by camille.lebugle on 03/11/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/objetspartages/services/ServiceLabelTest.test-context.xml"})
public class ServiceLabelTest extends AbstractCacheTestngTests {

    @Autowired
    ServiceLabel serviceLabel;

    @Mocked
    LabelDAO labelDAO;

    @BeforeClass
    public void init()
    {
        this.labelDAO = new LabelDAO();
        this.serviceLabel.setDao(labelDAO);
    }

    @Test
    public void testCreateNewLabel(@Mocked("getListeInfosLibelles") CacheLibelleManager cacheManager1){
        final CacheLibelleManager cacheManager = new CacheLibelleManager();
        serviceLabel.setCacheLibelleManager(cacheManager);
        final Map<String, LabelBean> mapLabel = new HashMap<>();
        final LabelBean labelBean = new LabelBean();
        labelBean.setCode("UPDATE");
        labelBean.setLangue("langue");
        labelBean.setType("type");
        labelBean.setId(1L);
        mapLabel.put(String.format("%s%s%s", labelBean.getType(), labelBean.getCode(), labelBean.getLangue()), labelBean);
        new Expectations() {{
            cacheManager.getListeInfosLibelles();
            result = mapLabel;
        }};

        //Test de l'ajout d'un bean
        serviceLabel.createNewLabel("type", "ADD", "LabelBean1", "langue");
        new Verifications(){{
            labelDAO.add(withAny(new LabelBean()));
        }};

        //Test de la mise à jour du bean
        serviceLabel.createNewLabel("type", "UPDATE", "LabelBean1", "langue");
        new Verifications(){{
            labelDAO.update(withAny(new LabelBean()));
        }};

    }

    @Test
    public void testGetAllLabels(){
        new Expectations(){{
            labelDAO.getAll();
            result =  Arrays.asList(new LabelBean(), new LabelBean());
        }};
        List<LabelBean> listeLabels = serviceLabel.getAllLabels();
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeLabels), "La liste retournée ne doit pas être vide");
        Assert.assertEquals(listeLabels.size(), 2, "La liste retournée doit contenir 2 éléments");
    }

    @Test
    public void testGetByCodeLanguage(){
        new Expectations(){{
            labelDAO.getByCodeLanguage("code", "language1");
            result = Arrays.asList(new LabelBean(), new LabelBean());
            labelDAO.getByCodeLanguage("code", "language2");
            result = Collections.emptyList();
        }};
        List<LabelBean> listeLabels1 = serviceLabel.getByCodeLanguage("code", "language1");
        List<LabelBean> listeLabels2 = serviceLabel.getByCodeLanguage("code", "language2");
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeLabels1), "La liste retournée lors du premier appel ne doit pas être vide");
        Assert.assertEquals(listeLabels1.size(), 2, "La liste retournée lors du premier appel doit contenir 2 éléments");
        Assert.assertTrue(CollectionUtils.isEmpty(listeLabels2), "La liste retournée lors du second appel ne doit pas être vide");
    }

    @Test
    public void testGetByLibelleTypeLanguage(){
        new Expectations(){{
            labelDAO.getByLibelleTypeLanguage("libelle", "code", "language1");
            result = new LabelBean();
            labelDAO.getByLibelleTypeLanguage("libelle", "code", "language2");
            result = null;
        }};
        LabelBean labelBean1 = serviceLabel.getByLibelleTypeLanguage("libelle", "code", "language1");
        LabelBean labelBean2 = serviceLabel.getByLibelleTypeLanguage("libelle", "code", "language2");
        Assert.assertNotNull(labelBean1, "L'objet retourné lors du premier appel ne doit pas être nul");
        Assert.assertNull(labelBean2, "L'objet retourné lors du second appel doit être nul");
    }

    @Test
    public void testGetByType(){
        new Expectations(){{
            labelDAO.getByType("type1");
            result = Arrays.asList(new LabelBean(), new LabelBean());
            labelDAO.getByType("type2");
            result = Collections.emptyList();
        }};
        List<LabelBean> listeLabels1 = serviceLabel.getByType("type1");
        List<LabelBean> listeLabels2 = serviceLabel.getByType("type2");
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeLabels1), "La liste retournée lors du premier appel ne doit pas être vide");
        Assert.assertEquals(listeLabels1.size(), 2, "La liste retournée lors du premier appel doit contenir 2 éléments");
        Assert.assertTrue(CollectionUtils.isEmpty(listeLabels2), "La liste retournée lors du second appel ne doit pas être vide");
    }

    @Test
    public void testGetByTypeCode(){
        new Expectations(){{
            labelDAO.getByTypeCode("type1", "code");
            result = Arrays.asList(new LabelBean(), new LabelBean());
            labelDAO.getByTypeCode("type2", "code");
            result = Collections.emptyList();
        }};
        List<LabelBean> listeLabels1 = serviceLabel.getByTypeCode("type1", "code");
        List<LabelBean> listeLabels2 = serviceLabel.getByTypeCode("type2", "code");
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeLabels1), "La liste retournée lors du premier appel ne doit pas être vide");
        Assert.assertEquals(listeLabels1.size(), 2, "La liste retournée lors du premier appel doit contenir 2 éléments");
        Assert.assertTrue(CollectionUtils.isEmpty(listeLabels2), "La liste retournée lors du second appel ne doit pas être vide");
    }

    @Test
    public void testGetByTypeCodeLanguage(@Mocked("getListeInfosLibelles") CacheLibelleManager cacheManager1){
        final CacheLibelleManager cacheManager = new CacheLibelleManager();
        serviceLabel.setCacheLibelleManager(cacheManager);
        final Map<String, LabelBean> mapLabel = new HashMap<>();
        final LabelBean labelBean = new LabelBean();
        labelBean.setCode("code");
        labelBean.setLangue("language");
        labelBean.setType("type");
        mapLabel.put(String.format("%s%s%s", labelBean.getType(), labelBean.getCode(), labelBean.getLangue()), labelBean);
        new Expectations() {{
                cacheManager.getListeInfosLibelles();
                result = mapLabel;
        }};
        LabelBean labelBean1 = serviceLabel.getByTypeCodeLanguage("type", "code", "language");
        Assert.assertNotNull(labelBean1, "L'objet retourné lors du premier appel ne doit pas être nul");
    }

    @Test
    public void testGetByTypeCodesLanguage(){
        final List<String> listeCode1 = Arrays.asList("Code1", "Code2");
        final List<String> listeCode2 = Arrays.asList("Code3");
        new Expectations(){{
            labelDAO.getByTypeCodesLanguage("type", listeCode1, "language");
            result = Arrays.asList(new LabelBean(), new LabelBean());
            labelDAO.getByTypeCodesLanguage("type", listeCode2, "language");
            result = Collections.emptyList();
        }};
        List<LabelBean> listeLabels1 = serviceLabel.getByTypeCodesLanguage("type", listeCode1, "language");
        List<LabelBean> listeLabels2 = serviceLabel.getByTypeCodesLanguage("type", listeCode2, "language");
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeLabels1), "La liste retournée lors du premier appel ne doit pas être vide");
        Assert.assertEquals(listeLabels1.size(), 2, "La liste retournée lors du premier appel doit contenir 2 éléments");
        Assert.assertTrue(CollectionUtils.isEmpty(listeLabels2), "La liste retournée lors du second appel ne doit pas être vide");
    }

    @Test
    public void testGetByTypeLanguage(){
        new Expectations(){{
            labelDAO.getByTypeLanguage("type1", "language");
            result = Arrays.asList(new LabelBean(), new LabelBean());
            labelDAO.getByTypeLanguage("type2", "language");
            result = Collections.emptyList();
        }};
        List<LabelBean> listeLabels1 = serviceLabel.getByTypeLanguage("type1", "language");
        List<LabelBean> listeLabels2 = serviceLabel.getByTypeLanguage("type2", "language");
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeLabels1), "La liste retournée lors du premier appel ne doit pas être vide");
        Assert.assertEquals(listeLabels1.size(), 2, "La liste retournée lors du premier appel doit contenir 2 éléments");
        Assert.assertTrue(CollectionUtils.isEmpty(listeLabels2), "La liste retournée lors du second appel ne doit pas être vide");
    }

    @Test
    public void testGetByTypeLibelle(){
        new Expectations(){{
            labelDAO.getByTypeLibelle("type1", "libelle");
            result = Arrays.asList(new LabelBean(), new LabelBean());
            labelDAO.getByTypeLibelle("type2", "libelle");
            result = Collections.emptyList();
        }};
        List<LabelBean> listeLabels1 = serviceLabel.getByTypeMatchingLibelle("type1", "libelle");
        List<LabelBean> listeLabels2 = serviceLabel.getByTypeMatchingLibelle("type2", "libelle");
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeLabels1), "La liste retournée lors du premier appel ne doit pas être vide");
        Assert.assertEquals(listeLabels1.size(), 2, "La liste retournée lors du premier appel doit contenir 2 éléments");
        Assert.assertTrue(CollectionUtils.isEmpty(listeLabels2), "La liste retournée lors du second appel ne doit pas être vide");
    }

    @Test
    public void testGetCodeSite(){
        LabelBean labelBean1 = new LabelBean();
        labelBean1.setLibelle("[Yeah]Yeah");
        LabelBean labelBean2 = new LabelBean();
        labelBean2.setLibelle("[Yo");
        LabelBean labelBean3 = new LabelBean();
        labelBean3.setLibelle("[Yep");
        Assert.assertTrue(serviceLabel.getCodeSite(labelBean1).equals("Yeah"), "Le premier appel doit retourner la chaine Yeah");
        Assert.assertTrue(StringUtils.isEmpty(serviceLabel.getCodeSite(labelBean2)), "Le second appel doit retourner une chaine vide");
        Assert.assertTrue(StringUtils.isEmpty(serviceLabel.getCodeSite(labelBean3)), "Le troisième appel doit retourner une chaine vide");
    }

    @Test
    public void testGetDisplayableLibelle(@Capturing final ContexteUniv contexteUniv, @Mocked ContexteUtil contexteUtil){

        new Expectations(){{
            ContexteUtil.getContexteUniv();
            result = contexteUniv;
        }};
        LabelBean labelBean1 = new LabelBean();
        labelBean1.setLibelle("[Yeah]Yeah");
        LabelBean labelBean2 = new LabelBean();
        labelBean2.setLibelle("[Yo");
        LabelBean labelBean3 = new LabelBean();
        labelBean3.setLibelle("[Yep");
        Assert.assertTrue(serviceLabel.getDisplayableLibelle(labelBean1).equals("Yeah"), "Le premier appel doit retourner la chaine Yeah");
        Assert.assertTrue(serviceLabel.getDisplayableLibelle(labelBean2).equals("[Yo"), "Le second appel doit retourner une chaine vide");
        Assert.assertTrue(serviceLabel.getDisplayableLibelle(labelBean3).equals("[Yep"), "Le troisième appel doit retourner une chaine vide");
    }

    @Test
    public void testGetMatchingLabelForCombo() {
        final LabelBean type1 = new LabelBean();
        type1.setCode("code1");
        type1.setLibelle("libelle1");
        type1.setType("type1");
        new Expectations(){{
            labelDAO.getByTypeLibelle("type1", "libelle1");
            result = Collections.singletonList(type1);
            labelDAO.getByTypeLibelle("type2", "libelle2");
            result = Collections.emptyList();
        }};
        Map<String, String> result = serviceLabel.getMatchingLabelForCombo("type1", "libelle1");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit avoir une taille d'un element");
        Assert.assertTrue(result.containsKey("code1"), "le résultat doit contenir la clé code1");
        Assert.assertTrue(result.containsValue("libelle1"), "le résultat doit contenir la valeur libelle1");
        result = serviceLabel.getMatchingLabelForCombo("type2", "libelle2");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être une liste vide");
    }

    @Test
    public void testGetFromRequest(@Capturing ContexteUniv mockedCtx) {
        final RequeteSQL requeteSQL = new RequeteSQL();
        final String strRequete = requeteSQL.formaterRequete();
        new Expectations(){{
            labelDAO.select(strRequete);
            result =  Arrays.asList(new LabelBean(), new LabelBean());
        }};
        List<LabelBean> listeLabels = serviceLabel.getFromRequest(requeteSQL);
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeLabels), "La liste retournée ne doit pas être vide");
        Assert.assertEquals(listeLabels.size(), 2, "La liste retournée doit contenir 2 éléments");
    }

    @Test
    public void testGetLabelForCombo(@Capturing final InfosSite infosSite, @Capturing final ContexteUniv contexteUniv, @Mocked ContexteUtil contexteUtil, @Mocked LangueUtil langueUtil){
        final LabelBean labelBean1 = new LabelBean();
        labelBean1.setCode("code1");
        labelBean1.setLibelle("Yup");
        final LabelBean labelBean2 = new LabelBean();
        labelBean2.setCode("code2");
        labelBean2.setLibelle("Yup");
        final Locale locale = new Locale("fr");
        new MockUp<ServiceLabel>(){
            @Mock
            List<LabelBean> getByTypeLanguage(final String type, final String language) {
                return  Arrays.asList(labelBean1, labelBean2);
            }
            @Mock
            public String getCodeSite(final LabelBean labelBean) {
                return  labelBean.getLibelle();
            }
            @Mock
            public String getDisplayableLibelle(final LabelBean labelBean) {
                return  "Yup";
            }
        };
        new Expectations(){{
            ContexteUtil.getContexteUniv();
            result = contexteUniv;
            infosSite.getAlias();
            result = "Yup";
            contexteUniv.getInfosSite();
            result = infosSite;
            LangueUtil.getIndiceLocale(locale);
            result = 1;
        }};
        Map<String, String> map1 = serviceLabel.getLabelForCombo("type", locale);
        Assert.assertEquals(map1.size(), 2, "La map retournée lors du premier appel doit contenir deux éléments.");
        labelBean2.setLibelle("Yop");
        Map<String, String> map2 = serviceLabel.getLabelForCombo("type", locale);
        Assert.assertEquals(map2.size(), 1, "La map retournée lors du second appel doit contenir un seul élément.");
        new Expectations(){{
            ContexteUtil.getContexteUniv();
            result = null;
        }};
        Map<String, String> map3 = serviceLabel.getLabelForCombo("type", locale);
        Assert.assertEquals(map3.size(),2, "La map retournée lors du troisième appel doit contenir deux éléments.");
    }

    @Test
    public void testGetLabelsLibelles(@Mocked MessageHelper messageHelper, @Mocked LangueUtil langueUtil) {
        final LabelBean labelBean1 = new LabelBean();
        labelBean1.setLibelle("Yup");
        final LabelBean labelBean2 = new LabelBean();
        labelBean2.setLibelle("Yop");
        final Locale locale = new Locale("fr");
        new MockUp<ServiceLabel>(){
            @Mock
            public LabelBean getByTypeCodeLanguage(final String type, final String code, final String language) {
                if (code.equals("yup")) {
                    return labelBean1;
                } else if(code.equals("yop")) {
                    return labelBean2;
                } else {
                    return null;
                }
            }
        };
        new Expectations(){{
            MessageHelper.getCoreMessage("LIBELLE.INCONNU");
            result = "LIBELLE.INCONNU";
            LangueUtil.getLangueLocale(locale);
            result = "langue";
        }};
        final List<String> listeCodes = Arrays.asList("Yup","Yop","Hip");
        List<String> listeLibelle = serviceLabel.getLabelsLibelles("type",listeCodes,locale);
        Assert.assertEquals(listeLibelle.size(),3,"La liste retournée doit contenir 3 éléments");
        Assert.assertTrue(listeLibelle.contains("LIBELLE.INCONNU"), "La liste retoutnée doit contenir le libellé \"LIBELLE.INCONNU\"");

    }

}

