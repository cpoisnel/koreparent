package com.univ.objetspartages.services;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.univ.objetspartages.dao.impl.ServiceDAO;
import com.univ.objetspartages.om.ServiceBean;

import mockit.Expectations;
import mockit.Mocked;

/**
 * Created by camille.lebugle on 03/11/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/objetspartages/services/ServiceServiceExterneTest.test-context.xml"})
public class ServiceServiceExterneTest extends AbstractCacheTestngTests {

    @Autowired
    ServiceServiceExterne serviceServiceExterne;

    @Test
    public void testGetByCode(@Mocked("selectByCode") ServiceDAO dao1) {
        final ServiceDAO dao = new ServiceDAO();
        serviceServiceExterne.setDao(dao);
        final ServiceBean serviceBean = new ServiceBean();
        serviceBean.setCode("code1");
        new Expectations() {{
            dao.selectByCode("code1");
            result = serviceBean;
            dao.selectByCode("code2");
            result = null;
        }};
        final ServiceBean serviceBeanTest1 = serviceServiceExterne.getByCode("code1");
        final ServiceBean serviceBeanTest2 = serviceServiceExterne.getByCode("code2");
        Assert.assertNotNull(serviceBeanTest1, "Le premier appel doit renvoyer un bean non null");
        Assert.assertNull(serviceBeanTest2, "Le second appel doit renvoyer un bean null");
    }

    @Test
    public void testGetAll(@Mocked("selectAll") ServiceDAO dao1) {
        final ServiceDAO dao = new ServiceDAO();
        serviceServiceExterne.setDao(dao);
        new Expectations() {{
            dao.selectAll();
            result = Arrays.asList(new ServiceBean(), new ServiceBean());
        }};
        final List<ServiceBean> listeService = serviceServiceExterne.getAll();
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeService), "La liste retournée ne doit pas être vide");
        Assert.assertEquals(listeService.size(), 2, "La liste retournée doit contenir 2 éléments.");
    }
}
