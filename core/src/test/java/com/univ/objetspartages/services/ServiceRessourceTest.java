package com.univ.objetspartages.services;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.dao.impl.RessourceDAO;
import com.univ.objetspartages.om.Article;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

import mockit.Expectations;
import mockit.Mock;
import mockit.Mocked;
import mockit.Verifications;

/**
 * Created by olivier.camon on 24/12/15.
 */
@Test
@ContextConfiguration("ServiceRessourceTest.test-context.xml")
public class ServiceRessourceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ServiceRessource serviceRessource;

    @Mocked
    private RessourceDAO dao;


    @BeforeClass
    public void initMock() {
        dao = new RessourceDAO();
        serviceRessource.setDao(dao);
    }

    @Test
    public void testGetFileFiche(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final String codeParent = "1,TYPE=FICHIER_0015,NO=1";
        final RessourceBean bean = new RessourceBean();
        bean.setCodeParent(codeParent);
        final Article article = new ArticleMockupWithResult().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
            dao.getByCodeParent(codeParent, "ID_RESSOURCE");
            result = Collections.singletonList(bean);
        }};
        RessourceBean result = serviceRessource.getFile(article);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, bean, "le résultat doit correspondre au bean instancié ci-dessus");
        final Article nullArticle = null;
        result = serviceRessource.getFile(nullArticle);
        Assert.assertNull(result, "le résultat doit être null si on passe un objet null");
    }

    @Test
    public void testGetFileIdCode() {
        final String codeParent = "1,TYPE=FICHIER_0015,NO=1";
        final RessourceBean bean = new RessourceBean();
        bean.setCodeParent(codeParent);
        new Expectations() {{
            dao.getByCodeParent(codeParent, "ID_RESSOURCE");
            result = Collections.singletonList(bean);
            dao.getByCodeParent(anyString, "ID_RESSOURCE");
            result = Collections.emptyList();
        }};
        RessourceBean result = serviceRessource.getFile(1L, "0015");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, bean, "le résultat doit correspondre au bean instancié ci-dessus");
        final Article nullArticle = null;
        result = serviceRessource.getFile(2L, null);
        Assert.assertNull(result, "le résultat doit être null si on passe un objet null");
    }

    @Test
    public void testGetFileFicheIndice(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final String codeParent = "1,TYPE=FICHIER_0015,NO=1";
        final RessourceBean bean = new RessourceBean();
        bean.setCodeParent(codeParent);
        final Article article = new ArticleMockupWithResult().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
            dao.getByCodeParent(codeParent, "ID_RESSOURCE");
            result = Collections.singletonList(bean);
        }};
        RessourceBean result = serviceRessource.getFile(article, "1");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, bean, "le résultat doit correspondre au bean instancié ci-dessus");
        final Article nullArticle = null;
        result = serviceRessource.getFile(nullArticle, "1");
        Assert.assertNull(result, "le résultat doit être null si on passe un objet null");
    }

    @Test
    public void testGetFileIdCodeIndice() {
        final String codeParent = "1,TYPE=FICHIER_0015,NO=1";
        final RessourceBean bean = new RessourceBean();
        bean.setCodeParent(codeParent);
        new Expectations() {{
            dao.getByCodeParent(codeParent, "ID_RESSOURCE");
            result = Collections.singletonList(bean);
            dao.getByCodeParent(anyString, "ID_RESSOURCE");
            result = Collections.emptyList();
        }};
        RessourceBean result = serviceRessource.getFile(1L, "0015", "1");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, bean, "le résultat doit correspondre au bean instancié ci-dessus");
        final Article nullArticle = null;
        result = serviceRessource.getFile(2L, null, "1");
        Assert.assertNull(result, "le résultat doit être null si on passe un objet null");
    }

    @Test
    public void testGetFileCode() {
        final String codeParent = "CODE_PARENt";
        final RessourceBean bean = new RessourceBean();
        bean.setCodeParent(codeParent);
        new Expectations() {{
            dao.getByCodeParent(codeParent, "ID_RESSOURCE");
            result = Collections.singletonList(bean);
        }};
        RessourceBean result = serviceRessource.getFile(codeParent);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, bean, "le résultat doit correspondre au bean instancié ci-dessus");
    }

    @Test
    public void testGetFiles(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final String codeParent = "1,TYPE=0015%";
        final RessourceBean ressourceBean = new RessourceBean();
        ressourceBean.setCodeParent(codeParent);
        final Article article = new ArticleMockupWithResult().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
            dao.getByCodeParent(codeParent, "ORDRE");
            result = Collections.singletonList(ressourceBean);
        }};
        List<RessourceBean> result = serviceRessource.getFiles(article);
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertEquals(result.get(0), ressourceBean, "le résultat doit être égal au bean instancié ci-dessus");
        final Article nullArticle = null;
        result = serviceRessource.getFiles(nullArticle);
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetFilesOrderByFiche(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final String codeParent = "1,TYPE=0015%";
        final String orderBy = "order";
        final RessourceBean ressourceBean = new RessourceBean();
        ressourceBean.setCodeParent(codeParent);
        final Article article = new ArticleMockupWithResult().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
            dao.getByCodeParent(codeParent, orderBy);
            result = Collections.singletonList(ressourceBean);
            dao.getByCodeParent(anyString, anyString);
            result = Collections.emptyList();
        }};
        List<RessourceBean> result = serviceRessource.getFilesOrderBy(article, orderBy);
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertEquals(result.get(0), ressourceBean, "le résultat doit être égal au bean instancié ci-dessus");
        result = serviceRessource.getFilesOrderBy(article, "NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
        final Article nullArticle = null;
        result = serviceRessource.getFilesOrderBy(nullArticle, "NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetFilesOrderByCodeParent() {
        final String codeParent = "1,TYPE=0015%";
        final String orderBy = "order";
        final RessourceBean ressourceBean = new RessourceBean();
        ressourceBean.setCodeParent(codeParent);
        new Expectations() {{
            dao.getByCodeParent(codeParent, orderBy);
            result = Collections.singletonList(ressourceBean);
            dao.getByCodeParent(anyString, anyString);
            result = Collections.emptyList();
        }};
        List<RessourceBean> result = serviceRessource.getFilesOrderBy(codeParent, orderBy);
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertEquals(result.get(0), ressourceBean, "le résultat doit être égal au bean instancié ci-dessus");
        result = serviceRessource.getFilesOrderBy("NO_RESULT", "NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetFilesIndexFiche(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final String codeParent = "1,TYPE=FICHIER_0015%";
        final RessourceBean ressourceBean = new RessourceBean();
        ressourceBean.setCodeParent(codeParent);
        final Article article = new ArticleMockupWithResult().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
            dao.getByCodeParent(codeParent, "ORDRE");
            result = Collections.singletonList(ressourceBean);
        }};
        List<RessourceBean> result = serviceRessource.getFilesIndex(article);
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertEquals(result.get(0), ressourceBean, "le résultat doit être égal au bean instancié ci-dessus");
        final Article nullArticle = null;
        result = serviceRessource.getFilesOrderBy(nullArticle, "NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetFilesIndexFicheIndice(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final String codeParent = "1,TYPE=FICHIER_0015,NO=1";
        final RessourceBean ressourceBean = new RessourceBean();
        ressourceBean.setCodeParent(codeParent);
        final Article article = new ArticleMockupWithResult().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
            dao.getByCodeParent(codeParent, "ORDRE");
            result = Collections.singletonList(ressourceBean);
        }};
        List<RessourceBean> result = serviceRessource.getFilesIndex(article, "1");
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertEquals(result.get(0), ressourceBean, "le résultat doit être égal au bean instancié ci-dessus");
        final Article nullArticle = null;
        result = serviceRessource.getFilesIndex(nullArticle, "1");
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByCodeParent() {
        final String codeParent = "CODE_PARENt";
        final RessourceBean bean = new RessourceBean();
        bean.setCodeParent(codeParent);
        new Expectations() {{
            dao.getByCodeParent(codeParent, "ID_RESSOURCE");
            result = Collections.singletonList(bean);
            dao.getByCodeParent("NO_RESULT", "ID_RESSOURCE");
            result = Collections.emptyList();
        }};
        List<RessourceBean> result = serviceRessource.getByCodeParent(codeParent);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertFalse(result.isEmpty(), "le resultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "le résultat doit correspondre au bean instancié ci-dessus");
        result = serviceRessource.getByCodeParent("NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
    }

    @Test
    public void testGetByMediaId() {
        final RessourceBean bean = new RessourceBean();
        bean.setIdMedia(1L);
        new Expectations() {{
            dao.getByMediaId(bean.getIdMedia());
            result = Collections.singletonList(bean);
            dao.getByMediaId(0L);
            result = Collections.emptyList();
        }};
        List<RessourceBean> result = serviceRessource.getByMediaId(1L);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertFalse(result.isEmpty(), "le resultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "le résultat doit correspondre au bean instancié ci-dessus");
        result = serviceRessource.getByMediaId(0L);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
    }

    @Test
    public void testAdd() {
        final RessourceBean ressourceBean = new RessourceBean();
        ressourceBean.setIdMedia(1L);
        ressourceBean.setCodeParent("CODE_PARENT");
        final RessourceBean resultAfterAdd = new RessourceBean();
        resultAfterAdd.setIdMedia(1L);
        resultAfterAdd.setCodeParent("CODE_PARENT");
        resultAfterAdd.setId(1L);
        new Expectations() {{
            dao.add(ressourceBean);
            result = resultAfterAdd;
        }};
        final RessourceBean result = serviceRessource.add(ressourceBean);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertEquals(result.getId(), Long.valueOf(1), "l'id de la ressource doit être passer à 1");
        Assert.assertEquals(result.getCodeParent(), ressourceBean.getCodeParent(), "les données doivent correspondre à la valeur ajouter");
    }

    @Test
    public void testUpdate() {
        final RessourceBean ressourceBean = new RessourceBean();
        ressourceBean.setId(1L);
        ressourceBean.setIdMedia(1L);
        ressourceBean.setCodeParent("CODE_PARENT");
        new Expectations() {{
            dao.update(ressourceBean);
            result = ressourceBean;
        }};
        final RessourceBean result = serviceRessource.update(ressourceBean);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertEquals(result, ressourceBean, "les données doivent correspondre à la valeur updater");
    }

    @Test
    public void testGetRessourceToPurge() {
        final RessourceBean resssource = new RessourceBean();
        resssource.setEtat("0");
        new Expectations() {{
            dao.getRessourceToPurge();
            result = Collections.singletonList(resssource);
        }};
        final List<RessourceBean> result = serviceRessource.getRessourceToPurge();
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), resssource, "la seule entrée doit correspondre au bean instancié ci-dessus");
    }

    @Test
    public void testGetFromWhere() {
        final RessourceBean bean = new RessourceBean();
        bean.setEtat("0");
        final ClauseWhere where = new ClauseWhere(ConditionHelper.egalVarchar("ETAT", "0"));
        new Expectations() {{
            dao.select(where.formaterSQL());
            result = Collections.singletonList(bean);
            dao.select(anyString);
            result = Collections.emptyList();
        }};
        List<RessourceBean> result = serviceRessource.getFromWhere(where);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "la seule entrée doit correspondre au bean instancié ci-dessus");
        result = serviceRessource.getFromWhere(new ClauseWhere());
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
        result = serviceRessource.getFromWhere(null);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetFromRequest() {
        final RessourceBean bean = new RessourceBean();
        bean.setEtat("0");
        final ClauseWhere where = new ClauseWhere(ConditionHelper.egalVarchar("ETAT", "0"));
        final RequeteSQL requete = new RequeteSQL();
        requete.where(where);
        new Expectations() {{
            dao.select(requete.formaterRequete());
            result = Collections.singletonList(bean);
            dao.select(anyString);
            result = Collections.emptyList();
        }};
        List<RessourceBean> result = serviceRessource.getFromRequest(requete);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "la seule entrée doit correspondre au bean instancié ci-dessus");
        result = serviceRessource.getFromRequest(new RequeteSQL());
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
        result = serviceRessource.getFromRequest(null);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByMediaIdAndCodeParent() {
        final RessourceBean bean = new RessourceBean();
        bean.setIdMedia(1L);
        bean.setCodeParent("CODE_PARENT");
        new Expectations() {{
            dao.getByMediaIdAndCodeParent(bean.getIdMedia(), bean.getCodeParent());
            result = bean;
            dao.getByMediaIdAndCodeParent(anyLong, anyString);
            result = null;
        }};
        RessourceBean result = serviceRessource.getByMediaIdAndCodeParent(bean.getIdMedia(), bean.getCodeParent());
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, bean, "le résultat doit correspondre au bean instancié ci-dessus");
        result = serviceRessource.getByMediaIdAndCodeParent(0L, "NO_RESULT");
        Assert.assertNull(result, "le résultat doit être null");
    }

    @Test
    public void testDeleteFilesCode(@Mocked("delete") final ServiceMedia serviceMediaMocked) {
        serviceRessource.setServiceMedia(serviceMediaMocked);
        final RessourceBean bean = new RessourceBean();
        bean.setIdMedia(1L);
        bean.setCodeParent("1,TYPE=0015%");
        final RessourceBean beanWithoutMedia = new RessourceBean();
        beanWithoutMedia.setCodeParent("1,TYPE=0016%");;
        new Expectations() {{
            dao.getByCodeParent(bean.getCodeParent(), "CODE_PARENT");
            result = Collections.singletonList(bean);
            dao.getByCodeParent(beanWithoutMedia.getCodeParent(), "CODE_PARENT");
            result = Collections.singletonList(beanWithoutMedia);
            dao.getByCodeParent(anyString, anyString);
            result = Collections.emptyList();
            dao.getByMediaId(bean.getIdMedia());
            result = Collections.singletonList(bean);
        }};
        serviceRessource.deleteFiles(bean.getCodeParent());
        serviceRessource.deleteFiles(beanWithoutMedia.getCodeParent());
        serviceRessource.deleteFiles("NO_RESULT");
        new Verifications() {{
            dao.delete(anyLong);
            times = 2;
            serviceMediaMocked.delete(anyLong);
            times = 1;
        }};
    }

    @Test
    public void testDeleteFilesFiche(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final RessourceBean bean = new RessourceBean();
        bean.setCodeParent("1,TYPE=%0015%");
        final Article articleApercu = new EmptyArticleMockup() {
            @Override
            public String getEtatObjet() {
                return EtatFiche.APERCU.getEtat();
            }
        }.getMockInstance();
        final Article articleMessageAlerte = new EmptyArticleMockup() {
            @Mock
            public String getMessageAlerte() {
                return "destruction_sauvegarde_courante";
            }
        }.getMockInstance();
        final Article article = new EmptyArticleMockup() {
            @Mock
            public String getMessageAlerte() {
                return StringUtils.EMPTY;
            }
        }.getMockInstance();
        final Article nullArticle = null;
        new Expectations() {{
            ReferentielObjets.getCodeObjet(articleApercu);
            result = "0015";
            ReferentielObjets.getCodeObjet(articleMessageAlerte);
            result = "0015";
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
            dao.getByCodeParent(bean.getCodeParent(), StringUtils.EMPTY);
            result = Collections.singletonList(bean);
        }};
        serviceRessource.deleteFiles(articleApercu);
        serviceRessource.deleteFiles(articleMessageAlerte);
        serviceRessource.deleteFiles(article);
        serviceRessource.deleteFiles(nullArticle);
        new Verifications() {{
            dao.update((RessourceBean) any);
            times = 2;
            dao.delete(anyLong);
            times = 1;
        }};
    }

    @Test
    public void testSyncFileCode(@Mocked("delete") ServiceMedia serviceMediaMock) {
        serviceRessource.setServiceMedia(serviceMediaMock);
        final RessourceBean bean = new RessourceBean();
        bean.setId(1L);
        bean.setEtat("0");
        final RessourceBean beanCodeParent = new RessourceBean();
        beanCodeParent.setCodeParent("codeParent");
        beanCodeParent.setId(2L);
        final RessourceBean beanCodeParent2 = new RessourceBean();
        beanCodeParent2.setCodeParent("codeParent3");
        beanCodeParent2.setId(3L);
        new Expectations() {{
            dao.getById(bean.getId());
            result = bean;
            dao.getById(beanCodeParent2.getId());
            result = beanCodeParent2;
            dao.getByCodeParent("codeParent", "ID_RESSOURCE");
            result = Collections.singletonList(beanCodeParent);
            dao.getByCodeParent("codeParentAutre", "ID_RESSOURCE");
            result = Collections.singletonList(beanCodeParent);
        }};
        serviceRessource.syncFile("codeParent", "1");
        serviceRessource.syncFile("codeParent2", "foo");
        serviceRessource.syncFile("codeParentAutre", "3");
        serviceRessource.syncFile("NO_RESULT", StringUtils.EMPTY);
        serviceRessource.syncFile("NO_RESULT", "0");
        new Verifications() {{
            dao.update((RessourceBean) any);
            times = 1;
            dao.add((RessourceBean) any);
            times = 1;
            dao.delete(anyLong);
            times = 2;
        }};
    }

    @Test
    public void testSyncFileFiche(@Mocked("delete") ServiceMedia serviceMediaMock, @Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        serviceRessource.setServiceMedia(serviceMediaMock);
        final Article article = new EmptyArticleMockup() {
            @Override
            public String getEtatObjet() {
                return EtatFiche.APERCU.getEtat();
            }
        }.getMockInstance();
        final Article onlineArticle = new EmptyArticleMockup() {
            @Override
            public String getEtatObjet() {
                return EtatFiche.EN_LIGNE.getEtat();
            }
        }.getMockInstance();
        final RessourceBean bean = new RessourceBean();
        bean.setId(1L);
        bean.setEtat("0");
        final RessourceBean beanCodeParent = new RessourceBean();
        beanCodeParent.setCodeParent(article.getIdFiche() + ",TYPE=FICHIER_0015,NO=1");
        beanCodeParent.setId(2L);
        beanCodeParent.setIdMedia(1L);
        final RessourceBean beanCodeParent2 = new RessourceBean();
        beanCodeParent2.setCodeParent("codeParent3");
        beanCodeParent2.setId(3L);
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
            dao.getById(bean.getId());
            result = bean;
            dao.getById(beanCodeParent2.getId());
            result = beanCodeParent2;
            dao.getByCodeParent(beanCodeParent.getCodeParent() , "ID_RESSOURCE");
            result = Collections.singletonList(beanCodeParent);
            dao.getByMediaId(beanCodeParent.getIdMedia());
            result = Collections.singletonList(beanCodeParent);
        }};
        serviceRessource.syncFile(article, "1", 1);
        bean.setCodeParent(StringUtils.EMPTY);
        serviceRessource.syncFile(onlineArticle, "1", 1);
        serviceRessource.syncFile(article, "3", 1);
        serviceRessource.syncFile(article, "foo", 1);
        new Verifications() {{
            dao.update((RessourceBean) any);
            times = 2;
            dao.add((RessourceBean) any);
            times = 2;
            dao.delete(anyLong);
            times = 3;
        }};
    }

    @Test
    public void testDuplicateFile() {
        final RessourceBean bean = new RessourceBean();
        bean.setEtat("etatBean");
        bean.setCodeParent("codeParentBean");
        bean.setIdMedia(1L);
        final RessourceBean resultWithState = serviceRessource.duplicateFile(bean, "codeParent", "etat");
        Assert.assertNotNull(resultWithState, "le résultat ne peut être null");
        Assert.assertEquals(resultWithState.getIdMedia(), bean.getIdMedia(), "le résultat doit avoir le même idmedia que le bean instancié ci-dessus");
        Assert.assertEquals(resultWithState.getEtat(), "etat", "le résultat doit avoir le même etat que celui passé en paramètre");
        final RessourceBean resultWithoutState = serviceRessource.duplicateFile(bean, "codeParent", StringUtils.EMPTY);
        Assert.assertNotNull(resultWithoutState, "le résultat ne peut être null");
        Assert.assertEquals(resultWithoutState.getIdMedia(), bean.getIdMedia(), "le résultat doit avoir le même idmedia que le bean instancié ci-dessus");
        Assert.assertEquals(resultWithoutState.getEtat(), bean.getEtat(), "le résultat doit avoir le même etat que celui du bean passé en paramètre");
        final RessourceBean nullExpected = serviceRessource.duplicateFile(null, "NO_RESULT", "NO_RESULT");
        Assert.assertNull(nullExpected, "le résultat doit être null");
        new Verifications() {{
            dao.add((RessourceBean) any);
            times = 2;
        }};
    }

    @Test
    public void syncFileFiche(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final Article article = new EmptyArticleMockup().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
        }};
        serviceRessource.syncFiles(article, "1", 1);
        serviceRessource.syncFiles(article, StringUtils.EMPTY, 1);
        serviceRessource.syncFiles(null, StringUtils.EMPTY, 1);
    }

    @Test
    public void syncFilesCode(@Mocked("delete") ServiceMedia serviceMediaMock) {
        serviceRessource.setServiceMedia(serviceMediaMock);
        final RessourceBean bean = new RessourceBean();
        bean.setId(1L);
        bean.setIdMedia(1L);
        bean.setEtat("0");
        final RessourceBean beanToDelete = new RessourceBean();
        beanToDelete.setIdMedia(2L);
        beanToDelete.setId(2L);
        final RessourceBean beanWithParentCode = new RessourceBean();
        beanWithParentCode.setCodeParent("parentCode");
        beanWithParentCode.setId(3L);
        beanWithParentCode.setIdMedia(3L);
        new Expectations() {{
            dao.getById(1L);
            result = bean;
            dao.getById(3L);
            result = beanWithParentCode;
            dao.getByCodeParent("codeParent", "ID_RESSOURCE");
            result = Arrays.asList(bean, beanToDelete);
            dao.getByCodeParent("codeParent2", "ID_RESSOURCE");
            result = Arrays.asList(beanWithParentCode, beanToDelete);
            dao.getByMediaId(2L);
            result = Collections.singletonList(beanToDelete);
            dao.getByMediaId(3L);
            result = Collections.emptyList();
        }};
        serviceRessource.syncFiles("codeParent", "1");
        serviceRessource.syncFiles("codeParent2", "3|");
        new Verifications() {{
            dao.update(bean);
            times = 1;
            dao.add((RessourceBean) any);
            times = 1;
            dao.delete(anyLong);
            times = 3;
        }};
    }

    @Test
    public void testGetFullFileList(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final RessourceBean bean = new RessourceBean();
        bean.setCodeParent("1,TYPE=%0015%");
        final Article article = new ArticleMockupWithResult().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "0015";
            dao.getByCodeParent(bean.getCodeParent(), StringUtils.EMPTY);
            result = Collections.singletonList(bean);
        }};
        List<RessourceBean> result = serviceRessource.getFullFileList(article);
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "le résultat doit être égal au bean instancié ci-dessus");
        result = serviceRessource.getFullFileList(null);
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }
}
