package com.univ.objetspartages.services;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.univ.objetspartages.bean.SiteExterneBean;
import com.univ.objetspartages.dao.impl.SiteExterneDAO;

import mockit.Expectations;
import mockit.Mocked;

/**
 * Created by camille.lebugle on 03/11/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/objetspartages/services/ServiceSiteExterneTest.test-context.xml"})
public class ServiceSiteExterneTest extends AbstractCacheTestngTests {

    @Autowired
    ServiceSiteExterne serviceSiteExterne;

    @Test
    public void testGetAllExernalWebSite(@Mocked("getAllSite") SiteExterneDAO dao1) {
        final SiteExterneDAO dao = new SiteExterneDAO();
        serviceSiteExterne.setDao(dao);
        new Expectations() {{
            dao.getAllSite();
            result = Arrays.asList(new SiteExterneBean(), new SiteExterneBean());
        }};
        final List<SiteExterneBean> listeSiteExterne = serviceSiteExterne.getAllExernalWebSite();
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeSiteExterne), "La liste retournée ne doit pas être vide");
        Assert.assertEquals(listeSiteExterne.size(), 2, "La liste retournée doit contenir 2 éléments.");
    }
}
