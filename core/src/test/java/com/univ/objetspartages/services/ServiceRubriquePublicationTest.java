package com.univ.objetspartages.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.univ.multisites.InfosFicheReferencee;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.dao.impl.RubriquepublicationDAO;
import com.univ.objetspartages.om.Article;
import com.univ.objetspartages.om.ReferentielObjets;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;

/**
 * Created by olivier.camon on 23/12/15.
 */
@Test
@ContextConfiguration("classpath:/com/univ/objetspartages/services/ServiceRubriquePublicationTest.test-context.xml")
public class ServiceRubriquePublicationTest  extends AbstractTestNGSpringContextTests {

    @Autowired
    ServiceRubriquePublication serviceRubriquePublication;

    @Mocked
    private RubriquepublicationDAO dao;

    @BeforeClass
    public void initMock() {
        dao = new RubriquepublicationDAO();
        serviceRubriquePublication.setDao(dao);
    }

    @Test
    public void testDeleteByIds() {
        serviceRubriquePublication.deleteByIds(Collections.singleton(1L));
        serviceRubriquePublication.deleteByIds(Collections.<Long>emptyList());
        serviceRubriquePublication.deleteByIds(null);
        new Verifications() {{
            dao.deleteByIds((Collection) any);
            times = 3;
        }};
    }

    @Test
    public void testDeleteByRubPub() {
        final RubriquepublicationBean rubpub = new RubriquepublicationBean();
        rubpub.setId(1L);
        serviceRubriquePublication.deleteByRubPub(Collections.singleton(rubpub));
        serviceRubriquePublication.deleteByIds(Collections.<Long>emptyList());
        serviceRubriquePublication.deleteByIds(null);
        new Verifications() {{
            dao.deleteByIds((Collection) any);
            times = 3;
        }};
    }

    @Test
    public void testGetByTypeCodeLanguage() {
        final RubriquepublicationBean bean = new RubriquepublicationBean();
        bean.setTypeFicheOrig("TYPE");
        bean.setCodeFicheOrig("CODE");
        bean.setLangueFicheOrig("0");
        new Expectations() {{
            dao.getByTypeCodeLanguage(bean.getTypeFicheOrig(), bean.getCodeFicheOrig(), bean.getLangueFicheOrig());
            result = Collections.singletonList(bean);
            dao.getByTypeCodeLanguage("NO_RESULT","NO_RESULT","NO_RESULT");
            result = Collections.emptyList();
        }};
        List<RubriquepublicationBean> result = serviceRubriquePublication.getByTypeCodeLanguage(bean.getTypeFicheOrig(), bean.getCodeFicheOrig(), bean.getLangueFicheOrig());
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "le résultat doit correspondre au bean contenant les même données pour type/code/langue");
        result = serviceRubriquePublication.getByTypeCodeLanguage("NO_RESULT","NO_RESULT","NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByTypeRubriqueDestLangue() {
        final RubriquepublicationBean bean = new RubriquepublicationBean();
        bean.setTypeFicheOrig("TYPE");
        bean.setRubriqueDest("RUBRIQUE_DEST");
        bean.setLangueFicheOrig("0");
        new Expectations() {{
            dao.getByTypeRubriqueDestLangue(bean.getTypeFicheOrig(), bean.getRubriqueDest(), bean.getLangueFicheOrig());
            result = Collections.singletonList(bean);
            dao.getByTypeRubriqueDestLangue("NO_RESULT","NO_RESULT","NO_RESULT");
            result = Collections.emptyList();
        }};
        List<RubriquepublicationBean> result = serviceRubriquePublication.getByTypeRubriqueDestLangue(bean.getTypeFicheOrig(), bean.getRubriqueDest(), bean.getLangueFicheOrig());
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "le résultat doit correspondre au bean contenant les même données pour type/rubrique dest/langue");
        result = serviceRubriquePublication.getByTypeRubriqueDestLangue("NO_RESULT","NO_RESULT","NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByType() {
        final RubriquepublicationBean bean = new RubriquepublicationBean();
        bean.setTypeFicheOrig("TYPE");
        new Expectations() {{
            dao.getByType(bean.getTypeFicheOrig());
            result = Collections.singletonList(bean);
            dao.getByType("NO_RESULT");
            result = Collections.emptyList();
        }};
        List<RubriquepublicationBean> result = serviceRubriquePublication.getByType(bean.getTypeFicheOrig());
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "le résultat doit correspondre au bean contenant les même données pour type/rubrique dest/langue");
        result = serviceRubriquePublication.getByType("NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByRubriqueDestWithoutSource() {
        final RubriquepublicationBean bean = new RubriquepublicationBean();
        bean.setRubriqueDest("RUBRIQUE_DEST");
        new Expectations() {{
            dao.getByRubriqueDestAndSource(bean.getRubriqueDest(), StringUtils.EMPTY);
            result = Collections.singletonList(bean);
            dao.getByRubriqueDestAndSource("NO_RESULT", StringUtils.EMPTY);
            result = Collections.emptyList();
        }};
        List<RubriquepublicationBean> result = serviceRubriquePublication.getByRubriqueDestWithoutSource(bean.getRubriqueDest());
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "le résultat doit correspondre au bean contenant une rubrique dest sans source");
        result = serviceRubriquePublication.getByRubriqueDestWithoutSource("NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByRubriqueDestAndSource() {
        final RubriquepublicationBean bean = new RubriquepublicationBean();
        bean.setRubriqueDest("RUBRIQUE_DEST");
        bean.setSourceRequete("SOURCE_REQUETE");
        new Expectations() {{
            dao.getByRubriqueDestAndSource(bean.getRubriqueDest(), bean.getSourceRequete());
            result = Collections.singletonList(bean);
            dao.getByRubriqueDestAndSource("NO_RESULT", "NO_RESULT");
            result = Collections.emptyList();
        }};
        List<RubriquepublicationBean> result = serviceRubriquePublication.getByRubriqueDestAndSource(bean.getRubriqueDest(), bean.getSourceRequete());
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), bean, "le résultat doit correspondre au bean contenant une rubrique dest et une source");
        result = serviceRubriquePublication.getByRubriqueDestAndSource("NO_RESULT", "NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    private void setFicheUnivExpectation(final Article article, final RubriquepublicationBean rubPub) {
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = rubPub.getTypeFicheOrig();
            dao.getByTypeCodeLanguage(rubPub.getTypeFicheOrig(), article.getCode(), article.getLangue());
            result = Collections.singletonList(rubPub);
        }};
    }
    @Test
    public void testGetByFicheUniv(@Mocked(stubOutClassInitialization = true)ReferentielObjets referentielObjets) {
        final Article article = new ArticleMockupWithNoResult().getMockInstance();
        final RubriquepublicationBean rubPub = new RubriquepublicationBean();
        rubPub.setTypeFicheOrig("CODE_OBJET");
        rubPub.setCodeFicheOrig(article.getCode());
        rubPub.setLangueFicheOrig(article.getLangue());
        setFicheUnivExpectation(article, rubPub);
        List<RubriquepublicationBean> result = serviceRubriquePublication.getByFicheUniv(article);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.get(0), rubPub, "le résultat doit correspondre au bean de la fiche article");
        result = serviceRubriquePublication.getByFicheUniv(null);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetRubriqueDestByFicheUniv(@Mocked(stubOutClassInitialization = true)ReferentielObjets referentielObjets) {
        final Article article = new ArticleMockupWithNoResult().getMockInstance();
        final RubriquepublicationBean rubPub = new RubriquepublicationBean();
        rubPub.setTypeFicheOrig("CODE_OBJET");
        rubPub.setCodeFicheOrig(article.getCode());
        rubPub.setLangueFicheOrig(article.getLangue());
        rubPub.setRubriqueDest("CODE_RUBRIQUE_DEST");
        setFicheUnivExpectation(article, rubPub);
        Collection<String> result = serviceRubriquePublication.getRubriqueDestByFicheUniv(article);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.iterator().next(), rubPub.getRubriqueDest(), "le résultat doit correspondre au code de la rubrique");
        result = serviceRubriquePublication.getRubriqueDestByFicheUniv(null);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetRubriqueDestByFicheWithSourceControl(@Mocked(stubOutClassInitialization = true)ReferentielObjets referentielObjets) {
        final Article article = new ArticleMockupWithNoResult().getMockInstance();
        final RubriquepublicationBean rubPub = new RubriquepublicationBean();
        rubPub.setTypeFicheOrig("CODE_OBJET");
        rubPub.setCodeFicheOrig(article.getCode());
        rubPub.setLangueFicheOrig(article.getLangue());
        rubPub.setRubriqueDest("CODE_RUBRIQUE_DEST");
        rubPub.setSourceRequete("SOURCE_REQUETE");
        setFicheUnivExpectation(article, rubPub);
        Collection<String> result = serviceRubriquePublication.getRubriqueDestByFicheWithSourceControl(article);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.iterator().next(), "#AUTO#" + rubPub.getRubriqueDest(), "le résultat doit correspondre au code de la rubrique préfixé d'#AUTO#");
        result = serviceRubriquePublication.getRubriqueDestByFicheWithSourceControl(null);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetRubriqueDestByFicheUnivAndRubrique(@Mocked(stubOutClassInitialization = true)ReferentielObjets referentielObjets, @Mocked final ServiceRubrique serviceRubrique) {
        serviceRubriquePublication.setServiceRubrique(serviceRubrique);
        final Article article = new ArticleMockupWithNoResult().getMockInstance();
        final RubriquepublicationBean rubPub = new RubriquepublicationBean();
        rubPub.setTypeFicheOrig("CODE_OBJET");
        rubPub.setCodeFicheOrig(article.getCode());
        rubPub.setLangueFicheOrig(article.getLangue());
        rubPub.setRubriqueDest("SOUS_RUBRIQUE");
        setFicheUnivExpectation(article, rubPub);
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("CODE_RUBRIQUE");
        final RubriqueBean sousRubrique = new RubriqueBean();
        sousRubrique.setCode("SOUS_RUBRIQUE");
        sousRubrique.setCodeRubriqueMere(rubriqueBean.getCode());
        new Expectations() {{
            serviceRubrique.getRubriqueByCode("CODE_RUBRIQUE");
            result = rubriqueBean;
            serviceRubrique.getAllChilds(rubriqueBean.getCode());
            result = Collections.singletonList(sousRubrique);
        }};
        Collection<String> result = serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubrique(article, "CODE_RUBRIQUE");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.iterator().next(), rubPub.getRubriqueDest(), "le résultat doit correspondre au code de la rubrique préfixé d'#AUTO#");
        result = serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubrique(null, null);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetRubriqueDestByFicheUnivAndRubriqueWithSourceControl(@Mocked(stubOutClassInitialization = true)ReferentielObjets referentielObjets, @Mocked final ServiceRubrique serviceRubrique) {
        serviceRubriquePublication.setServiceRubrique(serviceRubrique);
        final Article article = new ArticleMockupWithNoResult().getMockInstance();
        final RubriquepublicationBean rubPub = new RubriquepublicationBean();
        rubPub.setTypeFicheOrig("CODE_OBJET");
        rubPub.setCodeFicheOrig(article.getCode());
        rubPub.setLangueFicheOrig(article.getLangue());
        rubPub.setRubriqueDest("SOUS_RUBRIQUE");
        rubPub.setSourceRequete("SOURCE_REQUETE");
        setFicheUnivExpectation(article, rubPub);
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("CODE_RUBRIQUE");
        final RubriqueBean sousRubrique = new RubriqueBean();
        sousRubrique.setCode("SOUS_RUBRIQUE");
        sousRubrique.setCodeRubriqueMere(rubriqueBean.getCode());
        new Expectations() {{
            serviceRubrique.getRubriqueByCode("CODE_RUBRIQUE");
            result = rubriqueBean;
            serviceRubrique.getAllChilds(rubriqueBean.getCode());
            result = Collections.singletonList(sousRubrique);
        }};
        Collection<String> result = serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubriqueWithSourceControl(article, "CODE_RUBRIQUE");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertFalse(result.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(result.iterator().next(), "#AUTO#" + rubPub.getRubriqueDest(), "le résultat doit correspondre au code de la rubrique préfixé d'#AUTO#");
        result = serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubriqueWithSourceControl(null, null);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testDeleteByRubriqueDestAndNoSource() {
        serviceRubriquePublication.deleteByRubriqueDestAndNoSource("CODE_RUBRIQUE");
        serviceRubriquePublication.deleteByRubriqueDestAndNoSource(StringUtils.EMPTY);
        serviceRubriquePublication.deleteByRubriqueDestAndNoSource(null);
        new Verifications() {{
            dao.deleteByRubriqueDest(anyString);
            times = 3;
        }};
    }

    @Test
    public void testDeleteByRubriqueDestAndSource() {
        serviceRubriquePublication.deleteByRubriqueDestAndSource("CODE_RUBRIQUE", "SOURCE");
        serviceRubriquePublication.deleteByRubriqueDestAndSource(StringUtils.EMPTY, StringUtils.EMPTY);
        serviceRubriquePublication.deleteByRubriqueDestAndSource(null, null);
        new Verifications() {{
            dao.deleteByRubriqueDestAndSource(anyString, anyString);
            times = 3;
        }};
    }

    @Test
    public void testDeleteByFicheReference() {
        InfosFicheReferencee infos = new InfosFicheReferencee();
        infos.setCode("CODE");
        infos.setType("TYPE");
        infos.setLangue("0");
        infos.setRequete("REQUETE");
        serviceRubriquePublication.deleteByFicheReference(infos);
        serviceRubriquePublication.deleteByFicheReference(null);
        new Verifications() {{
            dao.deleteByTypeCodeLanguageAndSource(anyString, anyString, anyString, anyString);
            times = 1;
        }};
    }

    @Test
    public void testDeleteByFicheReferenceWithoutSource() {
        InfosFicheReferencee infos = new InfosFicheReferencee();
        infos.setCode("CODE");
        infos.setType("TYPE");
        infos.setLangue("0");
        infos.setRequete("REQUETE");
        serviceRubriquePublication.deleteByFicheReferenceWithoutSource(infos);
        serviceRubriquePublication.deleteByFicheReferenceWithoutSource(null);
        new Verifications() {{
            dao.deleteByTypeCodeLanguageAndSource(anyString, anyString, anyString, StringUtils.EMPTY);
            times = 1;
        }};
    }

    @Test
    public void testDeleteByFiche(@Mocked(stubOutClassInitialization = true)ReferentielObjets referentielObjets) {
        final Article article = new ArticleMockupWithNoResult().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getCodeObjet(article);
            result = "TYPE";
        }};
        serviceRubriquePublication.deleteByFiche(article);
        serviceRubriquePublication.deleteByFiche(null);
        new Verifications() {{
            dao.deleteByTypeCodeLanguage(anyString, anyString, anyString);
            times = 1;
        }};
    }

    @Test
    public void testDeleteByTypeCodeLangue() {
        serviceRubriquePublication.deleteByTypeCodeLangue("CODE_RUBRIQUE", "SOURCE", "0");
        serviceRubriquePublication.deleteByTypeCodeLangue(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        serviceRubriquePublication.deleteByTypeCodeLangue(null, null, null);
        new Verifications() {{
            dao.deleteByTypeCodeLanguage(anyString, anyString, anyString);
            times = 3;
        }};
    }

    @Test
    public void testSaveAllRubPubForFiche() {
        InfosFicheReferencee infos = new InfosFicheReferencee();
        infos.setCode("CODE");
        infos.setType("TYPE");
        infos.setLangue("0");
        infos.setRequete("REQUETE");
        serviceRubriquePublication.saveAllRubPubForFiche(infos, Collections.singletonList("CODE_RUBRIQUE"));
        serviceRubriquePublication.saveAllRubPubForFiche(null, null);
        new Verifications() {{
            dao.deleteByTypeCodeLanguageAndSource(anyString, anyString, anyString, StringUtils.EMPTY);
            times = 1;
            dao.add((RubriquepublicationBean)any);
            times = 1;
        }};
    }

    @Test
    public void testSaveAllFicheForRubPub() {
        InfosFicheReferencee infos = new InfosFicheReferencee();
        infos.setCode("CODE");
        infos.setType("TYPE");
        infos.setLangue("0");
        infos.setRequete("REQUETE");
        serviceRubriquePublication.saveAllFicheForRubPub("CODE_RUBRIQUE", Collections.singletonList(infos));
        serviceRubriquePublication.saveAllFicheForRubPub(null, null);
        new Verifications() {{
            dao.deleteByRubriqueDest(anyString);
            times = 2;
            dao.add((RubriquepublicationBean)any);
            times = 1;
        }};
    }

    @Test
    public void testGetInfosFichesReferencees() {
        final RubriquepublicationBean rubPub = new RubriquepublicationBean();
        rubPub.setCodeFicheOrig("CODE");
        rubPub.setTypeFicheOrig("TYPE");
        rubPub.setLangueFicheOrig("0");
        rubPub.setSourceRequete("SOURCE");
        rubPub.setRubriqueDest("CODE_RUBRIQUE");
        new Expectations() {{
            dao.getByRubriqueDestAndSource("CODE_RUBRIQUE", anyString);
            List<RubriquepublicationBean> listeRetour = new ArrayList<>();
            listeRetour.add(rubPub);
            result=listeRetour;
        }};
        Set<InfosFicheReferencee> references = serviceRubriquePublication.getInfosFichesReferencees("CODE_RUBRIQUE", "REQUETE");
        Assert.assertNotNull(references, "le résultat ne doit pas être null");
        Assert.assertFalse(references.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(references.iterator().next().getCode(), rubPub.getCodeFicheOrig(), "l'info fiche doit être équivalent à la rubrique de publication");
        references = serviceRubriquePublication.getInfosFichesReferencees("CODE_RUBRIQUE", "OLD/FORMAT/REQUETE//JESAISPASPOURQUOI");
        Assert.assertNotNull(references, "le résultat ne doit pas être null");
        Assert.assertFalse(references.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(references.iterator().next().getCode(), rubPub.getCodeFicheOrig(), "l'info fiche doit être équivalent à la rubrique de publication");
        references = serviceRubriquePublication.getInfosFichesReferencees(null, null);
        Assert.assertNotNull(references, "le résultat ne doit pas être null");
        Assert.assertTrue(references.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testDeleteRubPubAuto() {
        final RubriquepublicationBean rubPub = new RubriquepublicationBean();
        rubPub.setCodeFicheOrig("CODE");
        rubPub.setTypeFicheOrig("TYPE");
        rubPub.setLangueFicheOrig("0");
        rubPub.setSourceRequete("SOURCE");
        rubPub.setRubriqueDest("CODE_RUBRIQUE");
        new Expectations() {{
            dao.getByRubriqueDestAndSource("CODE_RUBRIQUE", anyString);
            List<RubriquepublicationBean> listeRetour = new ArrayList<>();
            listeRetour.add(rubPub);
            result = listeRetour;
        }};
        Set<InfosFicheReferencee> references = serviceRubriquePublication.deleteRubPubAuto("CODE_RUBRIQUE", "REQUETE");
        Assert.assertNotNull(references, "le résultat ne doit pas être null");
        Assert.assertFalse(references.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(references.iterator().next().getCode(), rubPub.getCodeFicheOrig(), "l'info fiche doit être équivalent à la rubrique de publication");
        references = serviceRubriquePublication.deleteRubPubAuto("CODE_RUBRIQUE", "OLD/FORMAT/REQUETE//JESAISPASPOURQUOI");
        Assert.assertNotNull(references, "le résultat ne doit pas être null");
        Assert.assertFalse(references.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(references.iterator().next().getCode(), rubPub.getCodeFicheOrig(), "l'info fiche doit être équivalent à la rubrique de publication");
        references = serviceRubriquePublication.deleteRubPubAuto(null, null);
        Assert.assertNotNull(references, "le résultat ne doit pas être null");
        Assert.assertTrue(references.isEmpty(), "le résultat doit être vide");
        new Verifications() {{
            dao.deleteByRubriqueDestAndSource(anyString, anyString);
            times = 3;
        }};
    }

    @Test
    public void testGetListeFichesReferences(@Mocked(stubOutClassInitialization = true) ReferentielObjets referentielObjets) {
        final RubriquepublicationBean rubPub = new RubriquepublicationBean();
        rubPub.setCodeFicheOrig("CODE");
        rubPub.setTypeFicheOrig("TYPE");
        rubPub.setLangueFicheOrig("0");
        rubPub.setSourceRequete("SOURCE");
        rubPub.setRubriqueDest("CODE_RUBRIQUE");
        final Article article = new ArticleMockupWithResult().getMockInstance();
        new Expectations() {{
            ReferentielObjets.getNomObjet("TYPE");
            result = "article";
            ReferentielObjets.instancierFiche("article");
            result = article;
            dao.getByRubriqueDestAndSource("CODE_RUBRIQUE", StringUtils.EMPTY);
            result = Collections.singletonList(rubPub);
            ReferentielObjets.getNomObjet("TYPE2");
            result = "articleInexistant";
            ReferentielObjets.instancierFiche("articleInexistant");
            result = null;
        }};
        try {
            List<InfosFicheReferencee> infosReferences = serviceRubriquePublication.getListeFichesReferences("CODE_RUBRIQUE", true);
            Assert.assertNotNull(infosReferences, "le résultat ne doit pas être null");
            Assert.assertFalse(infosReferences.isEmpty(), "le résultat ne doit pas être vide");
            InfosFicheReferencee result = infosReferences.iterator().next();
            Assert.assertEquals(result.getCode(), rubPub.getCodeFicheOrig(), "l'info fiche doit être équivalent à la rubrique de publication");
            Assert.assertEquals(result.getIntitule(), article.getLibelleAffichable(), "l'info fiche doit avoir le libelle de l'article d'origine");
            infosReferences = serviceRubriquePublication.getListeFichesReferences("CODE_RUBRIQUE", false);
            Assert.assertNotNull(infosReferences, "le résultat ne doit pas être null");
            Assert.assertFalse(infosReferences.isEmpty(), "le résultat ne doit pas être vide");
            result = infosReferences.iterator().next();
            Assert.assertEquals(result.getCode(), rubPub.getCodeFicheOrig(), "l'info fiche doit être équivalent à la rubrique de publication");
            Assert.assertEquals(result.getIntitule(), StringUtils.EMPTY, "l'info fiche doit avoir le libelle vide si on ne mets pas les infos de fiches");
        } catch (ErreurApplicative erreurApplicative) {
            Assert.fail("aucune exception ne doit être levé dans cet appel");
        }
        new Expectations() {{
            ReferentielObjets.instancierFiche("article");
            result = new ArticleMockupWithException().getMockInstance();
        }};
        try {
            List<InfosFicheReferencee> infosReferences = serviceRubriquePublication.getListeFichesReferences("CODE_RUBRIQUE", true);
            Assert.fail("une exception est attendu dans ce cas");
        } catch (ErreurApplicative erreurApplicative) {
            Assert.assertEquals(erreurApplicative.getMessage(), "An error occured requesting on a 'ficheUniv'");
        }
        new Expectations() {{
            ReferentielObjets.instancierFiche("article");
            result = new ArticleMockupWithNoResult().getMockInstance();
        }};
        try {
            List<InfosFicheReferencee> infosReferences = serviceRubriquePublication.getListeFichesReferences("CODE_RUBRIQUE", true);
            Assert.assertNotNull(infosReferences, "le résultat ne doit pas être null");
            Assert.assertTrue(infosReferences.isEmpty(), "le résultat doit être vide");
            rubPub.setTypeFicheOrig("TYPE2");
            infosReferences = serviceRubriquePublication.getListeFichesReferences("CODE_RUBRIQUE", true);
            Assert.assertNotNull(infosReferences, "le résultat ne doit pas être null");
            Assert.assertTrue(infosReferences.isEmpty(), "le résultat doit être vide");
        } catch (ErreurApplicative erreurApplicative) {
            Assert.fail("aucune exception ne doit être levé dans cet appel");
        }
    }
}
