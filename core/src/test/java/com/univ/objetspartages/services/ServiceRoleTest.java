package com.univ.objetspartages.services;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.univ.objetspartages.bean.RoleBean;
import com.univ.objetspartages.dao.impl.RoleDAO;

import mockit.Expectations;
import mockit.Mocked;

/**
 * Created by camille.lebugle on 03/11/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/objetspartages/services/ServiceRoleTest.test-context.xml"})
public class ServiceRoleTest extends AbstractCacheTestngTests {

    @Autowired
    ServiceRole serviceRole;

    @Test
    public void testGetByCode(@Mocked("getByCode") RoleDAO dao1) {
        final RoleDAO dao = new RoleDAO();
        serviceRole.setDao(dao);
        final RoleBean roleBean = new RoleBean();
        roleBean.setCode("code1");
        new Expectations() {{
            dao.getByCode("code1");
            result = roleBean;
            dao.getByCode("code2");
            result = null;
        }};
        final RoleBean roleBean1 = serviceRole.getByCode("code1");
        final RoleBean roleBean2 = serviceRole.getByCode("code2");
        Assert.assertNotNull(roleBean1, "Le premier appet doit renvoyer un bean non null");
        Assert.assertNull(roleBean2, "Le second appel doit renvoyer un bean null");
    }

    @Test
    public void testGetAllWithoutCollab(@Mocked("getAllWithoutCollab") RoleDAO dao1) {
        final RoleDAO dao = new RoleDAO();
        serviceRole.setDao(dao);
        new Expectations() {{
            dao.getAllWithoutCollab();
            result = Arrays.asList(new RoleBean(), new RoleBean());
        }};
        List<RoleBean> listeRole = serviceRole.getAllWithoutCollab();
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeRole), "La liste retournée ne doit pas être vide");
        Assert.assertEquals(listeRole.size(), 2, "La liste retournée doit contenir 2 éléments.");
    }

    @Test
    public void testGetAll(@Mocked("getAll") RoleDAO dao1) {
        final RoleDAO dao = new RoleDAO();
        serviceRole.setDao(dao);
        new Expectations() {{
            dao.getAll();
            result = Arrays.asList(new RoleBean(), new RoleBean());
        }};
        List<RoleBean> listeRole = serviceRole.getAll();
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeRole), "La liste retournée ne doit pas être vide");
        Assert.assertEquals(listeRole.size(), 2, "La liste retournée doit contenir 2 éléments.");
    }

    @Test
    public void testGetByPermission(@Mocked("getByPermission") RoleDAO dao1) {
        final RoleDAO dao = new RoleDAO();
        serviceRole.setDao(dao);
        final RoleBean roleBean1 = new RoleBean();
        final RoleBean roleBean12 = new RoleBean();
        new Expectations() {{
            dao.getByPermission("permission1");
            result = Collections.singletonList(roleBean1);
            dao.getByPermission("permission12");
            result = Arrays.asList(roleBean1, roleBean12);
            dao.getByPermission("permission3");
            result = null;
        }};
        List<RoleBean> listeRole1 = serviceRole.getByPermission("permission1");
        List<RoleBean> listeRole12 = serviceRole.getByPermission("permission12");
        List<RoleBean> listeRole3 = serviceRole.getByPermission("permission3");
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeRole1), "La liste retournée du premier appel ne doit pas etre vide");
        Assert.assertEquals(listeRole1.size(), 1, "La liste retournée du premier appel ne doit contenir qu'un seul élément");
        Assert.assertEquals(listeRole12.size(), 2, "La liste retournée du deuxième appel doit contenir deux éléments");
        Assert.assertTrue(CollectionUtils.isEmpty(listeRole3), "La liste retournée du troisième appel doit petre vide");
    }
}
