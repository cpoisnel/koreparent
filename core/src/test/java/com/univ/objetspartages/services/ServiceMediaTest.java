package com.univ.objetspartages.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.publish.ServiceCorePublisher;
import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.dao.impl.MediaDAO;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.FileUtil;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseWhere;

import mockit.Expectations;
import mockit.Invocation;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

/**
 * Created on 09/11/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/objetspartages/services/ServiceMediaTest.test-context.xml"})
public class ServiceMediaTest  extends AbstractCacheTestngTests {

    @Autowired
    private ServiceMedia serviceMedia;

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testSaveUnsupported() {
        serviceMedia.save(null);
    }

    @Test
    public void testSave(@Mocked({"add", "update", "getByFilledCodeRubrique"})final MediaDAO mediaDAO, @Mocked(stubOutClassInitialization = true)MediaUtils mediaUtils,
        @Mocked(stubOutClassInitialization = true)FileUtil fileUtil, @Mocked(stubOutClassInitialization = true) MediathequeHelper mediathequeHelper,
        @Mocked(stubOutClassInitialization = true) RessourceUtils ressourceUtils,
        @Mocked({"publish"}) final ServiceCorePublisher serviceCorePublisher
       ) throws IOException {
        serviceMedia.setDao(mediaDAO);
        serviceMedia.setServiceCorePublisher(serviceCorePublisher);
        final MediaBean mediaAdd = new MediaBean();
        mediaAdd.setCodeRubrique("TEST_SAVE");
        mediaAdd.setUrl("TEST_SAVE");
        mediaAdd.setTypeRessource("TEST_SAVE");
        mediaAdd.setUrl("TEST_SAVE");
        final MediaBean mediaUpdate = new MediaBean();
        mediaUpdate.setId(1L);
        mediaUpdate.setCodeRubrique("TEST_SAVE");
        mediaUpdate.setUrl("TEST_SAVE");
        mediaUpdate.setTypeRessource("TEST_SAVE");
        mediaUpdate.setUrl("TEST_SAVE");
        new MockUp<File>() {
            @Mock
            public boolean exists() {
                return true;
            }
        };
        new Expectations() {{
            MediaUtils.isLocal(mediaAdd);
            result = Boolean.TRUE;
            MediaUtils.isPublic(mediaAdd);
            result = Boolean.TRUE;
            MediaUtils.isLocal(mediaUpdate);
            result = Boolean.TRUE;
            MediaUtils.isPublic(mediaUpdate);
            result = Boolean.FALSE;
            RessourceUtils.getAbsolutePath();
            result = "/path/directory/";
            MediaUtils.getPathAbsolu(mediaAdd);
            result = "/old/path";
            MediaUtils.getPathVignetteAbsolu(mediaAdd);
            result = "/old/thumb/path";
            MediathequeHelper.getAbsolutePath();
            result = "/path/directory/";
        }};
        try {
            serviceMedia.save(mediaAdd, "/new/path", "/new/thumb/path", "fileName", "thumbName");
            serviceMedia.save(mediaUpdate, "/new/path", "/new/thumb/path", "fileName", "thumbName");
            new Verifications() {{
                mediaDAO.add((MediaBean) any);
                times = 1;
                mediaDAO.update((MediaBean) any);
                times = 1;
                serviceCorePublisher.publish(withAny(MediaBean.class),"media","save",anyLong);
                times = 2;
            }};
        } catch (ErreurApplicative erreurApplicative) {
            Assert.fail("no exception expected");
        }
    }
    @Test
    public void testDelete(@Mocked({"getById", "delete", "getByFilledCodeRubrique"})final MediaDAO mediaDAO,
        @Mocked({"publish"}) final ServiceCorePublisher serviceCorePublisher) {
        serviceMedia.setDao(mediaDAO);
        serviceMedia.setServiceCorePublisher(serviceCorePublisher);
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setId(1L);
        mediaBean.setCodeRubrique("TEST_DELETE");
        mediaBean.setUrl("TEST_DELETE");
        new Expectations() {{
            mediaDAO.getByFilledCodeRubrique();
            result = Collections.singletonList(mediaBean);
            serviceCorePublisher.publish(1L,"media","delete",1L);
            result = 1L;
        }};
        new NonStrictExpectations() {{
            mediaDAO.getById(anyLong);
            result = mediaBean;
        }};
        final String rubrique = serviceMedia.getCodeRubrique("TEST_DELETE");
        Assert.assertNotNull(rubrique, "Le premier appel doit retourner le code de la rubrique");
        Assert.assertEquals(rubrique, mediaBean.getCodeRubrique(), "Le premier appel doit renvoyer le code de rubrique portant le code TEST_DELETE");
        serviceMedia.delete(1L);
        new Expectations() {{
            mediaDAO.getByFilledCodeRubrique();
            result = Collections.emptyList();
        }};
        final String rubrique1 = serviceMedia.getCodeRubrique("TEST_DELETE");
        Assert.assertNull(rubrique1, "Le second appel ne doit pas passer par le cache et donc faire appel au dao qui nous renvoit \"null\"");
    }

    @Test
    public void testGetCodeRubrique(@Mocked("getByFilledCodeRubrique")final MediaDAO mediaDAO) {
        serviceMedia.setDao(mediaDAO);
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setId(1L);
        mediaBean.setCodeRubrique("TEST_GETCODERUBRIQUE");
        mediaBean.setUrl("TEST_GETCODERUBRIQUE");
        new Expectations() {{
            mediaDAO.getByFilledCodeRubrique();
            result = Collections.singletonList(mediaBean);
        }};
        final String rubrique = serviceMedia.getCodeRubrique("TEST_GETCODERUBRIQUE");
        Assert.assertNotNull(rubrique, "Le premier appel doit retourner le code de la rubrique");
        Assert.assertEquals(rubrique, mediaBean.getCodeRubrique(), "Le premier appel doit renvoyer le code de rubrique portant le code TEST_DELETE");
        final String rubrique1 = serviceMedia.getCodeRubrique("TEST_GETCODERUBRIQUE");
        new Verifications() {{
            mediaDAO.getByFilledCodeRubrique();
            times = 1;
        }};
        Assert.assertEquals(rubrique1, rubrique, "Le second appel doit passer par le cache et donc renvoyer le même résultat");
    }

    @Test
    public void testGetTraductionData() {
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setTraductionData("#\n" +
            "#Mon Nov 09 12:03:26 CET 2015\n" +
            "TITRE_1=Southern Himalayan Mountains.\n");
        final String failProperty = serviceMedia.getTraductionData(mediaBean, "PLOP");
        Assert.assertNotNull(failProperty, "La propriété renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(StringUtils.isBlank(failProperty), "La propriété renvoyée par le premier appel doit être une chaîne vide");
        final String property = serviceMedia.getTraductionData(mediaBean, "TITRE_1");
        Assert.assertNotNull(property, "La propriété renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(StringUtils.isNotBlank(property), "La propriété renvoyée par le premier appel ne doit pas être vide");
        Assert.assertEquals(property, "Southern Himalayan Mountains.","La propriété renvoyée par le premier appel doit être égale à \"Southern Himalayan Mountains.\"");
    }

    @Test
    public void testGetTraductionDataFail(@Mocked("load") final Properties properties) throws IOException {
        new Expectations() {{
            properties.load((InputStream) any);
            result = new IOException();

        }};
        final String failProperty = serviceMedia.getTraductionData(new MediaBean(), "PLOP");
        Assert.assertNotNull(failProperty, "La propriété renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(StringUtils.isBlank(failProperty), "La propriété renvoyée par le premier appel doit être une chaîne vide");
    }

    @Test
    public void testGetSpecificData() {
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setSpecificData("#Mon Nov 09 12:03:26 CET 2015\n" +
            "HAUTEUR=350\n");
        final String failProperty = serviceMedia.getSpecificData(mediaBean, "PLOP");
        Assert.assertNotNull(failProperty, "La propriété renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(StringUtils.isBlank(failProperty), "La propriété renvoyée par le premier appel doit être une chaîne vide");
        final String property = serviceMedia.getSpecificData(mediaBean, "HAUTEUR");
        Assert.assertNotNull(property, "La propriété renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(StringUtils.isNotBlank(property), "La propriété renvoyée par le premier appel ne doit pas être vide");
        Assert.assertEquals(property, "350","La propriété renvoyée par le premier appel doit être égale à \"350\"");
    }

    @Test
    public void testGetSpecificDataFail(@Mocked("load") final Properties properties) throws IOException {
        new Expectations() {{
            properties.load((InputStream) any);
            result = new IOException();

        }};
        final String failProperty = serviceMedia.getSpecificData(new MediaBean(), "PLOP");
        Assert.assertNotNull(failProperty, "La propriété renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(StringUtils.isBlank(failProperty), "La propriété renvoyée par le premier appel doit être une chaîne vide");
    }

    @Test
    public void testGetSpecificDataAsString() {
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setSpecificData("#Mon Nov 09 12:03:26 CET 2015\n" +
                                  "HAUTEUR=350\n" +
                                  "LARGEUR=250\n");
        final String property = serviceMedia.getSpecificDataAsString(mediaBean);
        Assert.assertNotNull(property, "La propriété renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(StringUtils.isNotBlank(property), "La propriété renvoyée par le premier appel ne doit pas être une chaîne vide");
        Assert.assertEquals(property, "largeur : 250\nhauteur : 350", "La propriété renvoyée par le premier appel doit être \"hauteur : 350\\n" + "largeur : 250\"");
    }

    @Test
    public void testGetSpecificDataAsStringFail(@Mocked("load") final Properties properties) throws IOException {
        new Expectations() {{
            properties.load((InputStream) any);
            result = new IOException();

        }};
        final String failProperty = serviceMedia.getSpecificDataAsString(new MediaBean());
        Assert.assertNotNull(failProperty, "La propriété renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(StringUtils.isBlank(failProperty), "La propriété renvoyée par le premier appel doit être une chaîne vide");
    }

    @Test
    public void testGetMediasFromWhere(@Mocked("select") final MediaDAO mockedDao) throws IOException {
        final ClauseWhere where = new ClauseWhere();
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.select(anyString);
            result = Collections.EMPTY_LIST;

        }};
        final List<MediaBean> emptyList = serviceMedia.getMediasFromWhere(where);
        Assert.assertNotNull(emptyList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(emptyList), "La liste renvoyée par le premier appel doit être vide");
        new Expectations() {{
            mediaDAO.select(anyString);
            result = Collections.singletonList(new MediaBean());

        }};
        final List<MediaBean> singleList = serviceMedia.getMediasFromWhere(where);
        Assert.assertNotNull(singleList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertEquals(singleList.size(), 1, "La liste renvoyée par le premier appel doit être une liste contenant 1 élément");
    }

    @Test
    public void testGetMediasFromRequest(@Mocked("select") final MediaDAO mockedDao) throws IOException {
        final RequeteSQL request = new RequeteSQL();
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.select(anyString);
            result = Collections.EMPTY_LIST;

        }};
        final List<MediaBean> emptyList = serviceMedia.getMediasFromRequest(request);
        Assert.assertNotNull(emptyList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(emptyList), "La liste renvoyée par le premier appel doit être vide");
        new Expectations() {{
            mediaDAO.select(anyString);
            result = Collections.singletonList(new MediaBean());

        }};
        final List<MediaBean> singleList = serviceMedia.getMediasFromRequest(request);
        Assert.assertNotNull(singleList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertEquals(singleList.size(), 1, "La liste renvoyée par le premier appel doit être une liste contenant 1 élément");
    }

    @Test
    public void testGetAll(@Mocked("getAll") final MediaDAO mockedDao) throws IOException {
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.getAll();
            result = Collections.EMPTY_LIST;

        }};
        final List<MediaBean> emptyList = serviceMedia.getAll();
        Assert.assertNotNull(emptyList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(emptyList), "La liste renvoyée par le premier appel doit être vide");
        new Expectations() {{
            mediaDAO.getAll();
            result = Arrays.asList(new MediaBean(), new MediaBean());

        }};
        final List<MediaBean> singleList = serviceMedia.getAll();
        Assert.assertNotNull(singleList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertEquals(singleList.size(), 2, "La liste renvoyée par le premier appel doit être une liste contenant 2 éléments");
    }

    @Test
    public void testGetByFilledCodeRubrique(@Mocked("getByFilledCodeRubrique") final MediaDAO mockedDao) throws IOException {
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.getByFilledCodeRubrique();
            result = Collections.EMPTY_LIST;

        }};
        final List<MediaBean> emptyList = serviceMedia.getByFilledCodeRubrique();
        Assert.assertNotNull(emptyList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(emptyList), "La liste renvoyée par le premier appel doit être vide");
        new Expectations() {{
            mediaDAO.getByFilledCodeRubrique();
            result = Arrays.asList(new MediaBean(), new MediaBean());

        }};
        final List<MediaBean> singleList = serviceMedia.getByFilledCodeRubrique();
        Assert.assertNotNull(singleList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertEquals(singleList.size(), 2, "La liste renvoyée par le premier appel doit être une liste contenant 2 éléments");
    }

    @Test
    public void testGetByResourceType(@Mocked("getByResourceType") final MediaDAO mockedDao) throws IOException {
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.getByResourceType(anyString);
            result = Collections.EMPTY_LIST;

        }};
        final List<MediaBean> emptyList = serviceMedia.getByResourceType("PLOP");
        Assert.assertNotNull(emptyList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(emptyList), "La liste renvoyée par le premier appel doit être vide");
        new Expectations() {{
            mediaDAO.getByResourceType(anyString);
            result = Arrays.asList(new MediaBean(), new MediaBean());

        }};
        final List<MediaBean> singleList = serviceMedia.getByResourceType("PLOP");
        Assert.assertNotNull(singleList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertEquals(singleList.size(), 2, "La liste renvoyée par le premier appel doit être une liste contenant 2 éléments");
    }

    @Test
    public void testUpdate(@Mocked("update") final MediaDAO mockedDao) throws IOException {
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setId(10L);
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.update(mediaBean);
            result = mediaBean;

        }};
        final MediaBean updated = serviceMedia.update(mediaBean);
        Assert.assertNotNull(updated, "La bean renvoyé par le premier appel ne doit pas être null");
        Assert.assertEquals(mediaBean.getId(), new Long(10), "Le bean renvoyée par le premier appel doit avoir un id égal à 10");
    }

    @Test
    public void testAdd(@Mocked("add") final MediaDAO mockedDao) throws IOException {
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setId(0L);
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.add(mediaBean);
            result = mediaBean;
            mediaBean.setId(1L);

        }};
        final MediaBean saved = serviceMedia.add(mediaBean);
        Assert.assertNotNull(saved, "La bean renvoyé par le premier appel ne doit pas être null");
        Assert.assertEquals(mediaBean.getId(), new Long(1), "Le bean renvoyée par le premier appel doit avoir un id égal à 1");
    }

    @Test
    public void testGetByUrl(@Mocked("getByUrl") final MediaDAO mockedDao) throws IOException {
        final MediaBean mediaBean = new MediaBean();
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.getByUrl(anyString);
            result = null;

        }};
        final MediaBean failed = serviceMedia.getByUrl(StringUtils.EMPTY);
        Assert.assertNull(failed, "La bean renvoyé par le premier appel doit être null");
        new Expectations() {{
            mediaDAO.getByUrl(anyString);
            result = mediaBean;

        }};
        final MediaBean bean = serviceMedia.getByUrl(StringUtils.EMPTY);
        Assert.assertNotNull(bean, "La bean renvoyé par le premier appel ne doit pas être null");
    }

    @Test
    public void testGetByFilledUrlVignette(@Mocked("getByFilledUrlVignette") final MediaDAO mockedDao) throws IOException {
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.getByFilledUrlVignette();
            result = Collections.EMPTY_LIST;

        }};
        final List<MediaBean> emptyList = serviceMedia.getByFilledUrlVignette();
        Assert.assertNotNull(emptyList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(emptyList), "La liste renvoyée par le premier appel doit être vide");
        new Expectations() {{
            mediaDAO.getByFilledUrlVignette();
            result = Arrays.asList(new MediaBean(), new MediaBean());

        }};
        final List<MediaBean> singleList = serviceMedia.getByFilledUrlVignette();
        Assert.assertNotNull(singleList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertEquals(singleList.size(), 2, "La liste renvoyée par le premier appel doit être une liste contenant 2 éléments");
    }

    @Test
    public void testGetByTypeAndTypeMedia(@Mocked("getByTypeAndTypeMedia") final MediaDAO mockedDao) throws IOException {
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.getByTypeAndTypeMedia(anyString, anyString);
            result = Collections.EMPTY_LIST;

        }};
        final List<MediaBean> emptyList = serviceMedia.getByTypeAndTypeMedia(StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertNotNull(emptyList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(emptyList), "La liste renvoyée par le premier appel doit être vide");
        new Expectations() {{
            mediaDAO.getByTypeAndTypeMedia(anyString, anyString);
            result = Arrays.asList(new MediaBean(), new MediaBean());

        }};
        final List<MediaBean> singleList = serviceMedia.getByTypeAndTypeMedia(StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertNotNull(singleList, "La liste renvoyée par le premier appel ne doit pas être null");
        Assert.assertEquals(singleList.size(), 2, "La liste renvoyée par le premier appel doit être une liste contenant 2 éléments");
    }

    @Test
    public void testGetDescription() {
        new MockUp<LangueUtil>() {

            @Mock
            public int getIndiceLocale(Invocation context, final Locale locale) {
                return context.getInvocationCount() - 1;
            }
        };
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setDescription("PLOP");
        mediaBean.setTraductionData("#\n" +
            "#Mon Nov 09 12:03:26 CET 2015\n" +
            "DESCRIPTION_1=Southern Himalayan Mountains.\n");
        final String description0 = serviceMedia.getDescription(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(description0, "Le premier appel ne devrait pas renvoyer une description null");
        Assert.assertTrue(StringUtils.isNotBlank(description0), "Le premier appel ne devrait pas renvoyer une description vide");
        Assert.assertEquals(description0, "PLOP", "Le premier appel devrait renvoyer une description égale à \"PLOP\"");
        mediaBean.setIsMutualise("0");
        final String description1 = serviceMedia.getDescription(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(description1, "Le second appel ne devrait pas renvoyer une description null");
        Assert.assertTrue(StringUtils.isNotBlank(description1), "Le second appel ne devrait pas renvoyer une description vide");
        Assert.assertEquals(description1, "Southern Himalayan Mountains.", "Le second appel devrait renvoyer une description égale à \"Southern Himalayan Mountains.\"");
        final String description2 = serviceMedia.getDescription(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(description2, "Le troisième appel ne devrait pas renvoyer une description null");
        Assert.assertTrue(StringUtils.isBlank(description2), "Le troisième appel devrait renvoyer une description vide");
    }

    @Test
    public void testGetTitre() {
        new MockUp<LangueUtil>() {

            @Mock
            public int getIndiceLocale(Invocation context, final Locale locale) {
                return context.getInvocationCount() - 1;
            }
        };
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setTitre("PLOP");
        mediaBean.setTraductionData("#\n" +
            "#Mon Nov 09 12:03:26 CET 2015\n" +
            "TITRE_1=Southern Himalayan Mountains.\n");
        final String titre0 = serviceMedia.getTitre(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(titre0, "Le premier appel ne devrait pas renvoyer un titre null");
        Assert.assertTrue(StringUtils.isNotBlank(titre0), "Le premier appel ne devrait pas renvoyer un titre vide");
        Assert.assertEquals(titre0, "PLOP", "Le premier appel devrait renvoyer un titre égal à \"PLOP\"");
        mediaBean.setIsMutualise("0");
        final String titre1 = serviceMedia.getTitre(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(titre1, "Le second appel ne devrait pas renvoyer un titre null");
        Assert.assertTrue(StringUtils.isNotBlank(titre1), "Le second appel ne devrait pas renvoyer un titre vide");
        Assert.assertEquals(titre1, "Southern Himalayan Mountains.", "Le second appel devrait renvoyer un titre égal à \"Southern Himalayan Mountains.\"");
        final String titre2 = serviceMedia.getTitre(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(titre2, "Le troisième appel ne devrait pas renvoyer un titre null");
        Assert.assertTrue(StringUtils.isBlank(titre2), "Le troisième appel devrait renvoyer un titre vide");
    }

    @Test
    public void testGetAuteur() {
        new MockUp<LangueUtil>() {

            @Mock
            public int getIndiceLocale(Invocation context, final Locale locale) {
                return context.getInvocationCount() - 1;
            }
        };
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setAuteur("PLOP");
        mediaBean.setTraductionData("#\n" +
            "#Mon Nov 09 12:03:26 CET 2015\n" +
            "AUTEUR_1=Southern Himalayan Mountains.\n");
        final String auteur0 = serviceMedia.getAuteur(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(auteur0, "Le premier appel ne devrait pas renvoyer un auteur null");
        Assert.assertTrue(StringUtils.isNotBlank(auteur0), "Le premier appel ne devrait pas renvoyer un auteur vide");
        Assert.assertEquals(auteur0, "PLOP", "Le premier appel devrait renvoyer un auteur égal à \"PLOP\"");
        mediaBean.setIsMutualise("0");
        final String auteur1 = serviceMedia.getAuteur(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(auteur1, "Le second appel ne devrait pas renvoyer un auteur null");
        Assert.assertTrue(StringUtils.isNotBlank(auteur1), "Le second appel ne devrait pas renvoyer un auteur vide");
        Assert.assertEquals(auteur1, "Southern Himalayan Mountains.", "Le second appel devrait renvoyer un auteur égal à \"Southern Himalayan Mountains.\"");
        final String auteur2 = serviceMedia.getAuteur(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(auteur2, "Le troisième appel ne devrait pas renvoyer un auteur null");
        Assert.assertTrue(StringUtils.isBlank(auteur2), "Le troisième appel devrait renvoyer un auteur vide");
    }

    @Test
    public void testGetCopyright() {
        new MockUp<LangueUtil>() {

            @Mock
            public int getIndiceLocale(Invocation context, final Locale locale) {
                return context.getInvocationCount() - 1;
            }
        };
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setCopyright("PLOP");
        mediaBean.setTraductionData("#\n" +
            "#Mon Nov 09 12:03:26 CET 2015\n" +
            "COPYRIGHT_1=Southern Himalayan Mountains.\n");
        final String copyright0 = serviceMedia.getCopyright(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(copyright0, "Le premier appel ne devrait pas renvoyer un copyright null");
        Assert.assertTrue(StringUtils.isNotBlank(copyright0), "Le premier appel ne devrait pas renvoyer un copyright vide");
        Assert.assertEquals(copyright0, "PLOP", "Le premier appel devrait renvoyer un copyright égal à \"PLOP\"");
        mediaBean.setIsMutualise("0");
        final String copyright1 = serviceMedia.getCopyright(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(copyright1, "Le second appel ne devrait pas renvoyer un copyright null");
        Assert.assertTrue(StringUtils.isNotBlank(copyright1), "Le second appel ne devrait pas renvoyer un copyright vide");
        Assert.assertEquals(copyright1, "Southern Himalayan Mountains.", "Le second appel devrait renvoyer un copyright égal à \"Southern Himalayan Mountains.\"");
        final String copyright2 = serviceMedia.getCopyright(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(copyright2, "Le troisième appel ne devrait pas renvoyer un copyright null");
        Assert.assertTrue(StringUtils.isBlank(copyright2), "Le troisième appel devrait renvoyer un copyright vide");
    }

    @Test
    public void testGetLegende() {
        new MockUp<LangueUtil>() {

            @Mock
            public int getIndiceLocale(Invocation context, final Locale locale) {
                return context.getInvocationCount() - 1;
            }
        };
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setLegende("PLOP");
        mediaBean.setTraductionData("#\n" +
            "#Mon Nov 09 12:03:26 CET 2015\n" +
            "LEGENDE_1=Southern Himalayan Mountains.\n");
        final String legende0 = serviceMedia.getLegende(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(legende0, "Le premier appel ne devrait pas renvoyer une legende null");
        Assert.assertTrue(StringUtils.isNotBlank(legende0), "Le premier appel ne devrait pas renvoyer une legende vide");
        Assert.assertEquals(legende0, "PLOP", "Le premier appel devrait renvoyer une legende égal à \"PLOP\"");
        mediaBean.setIsMutualise("0");
        final String legende1 = serviceMedia.getLegende(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(legende1, "Le second appel ne devrait pas renvoyer une legende null");
        Assert.assertTrue(StringUtils.isNotBlank(legende1), "Le second appel ne devrait pas renvoyer une legende vide");
        Assert.assertEquals(legende1, "Southern Himalayan Mountains.", "Le second appel devrait renvoyer une legende égal à \"Southern Himalayan Mountains.\"");
        final String legende2 = serviceMedia.getLegende(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(legende2, "Le troisième appel ne devrait pas renvoyer une legende null");
        Assert.assertTrue(StringUtils.isBlank(legende2), "Le troisième appel devrait renvoyer une legende vide");
    }

    @Test
    public void testGetMetaKeywords() {
        new MockUp<LangueUtil>() {

            @Mock
            public int getIndiceLocale(Invocation context, final Locale locale) {
                return context.getInvocationCount() - 1;
            }
        };
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setMetaKeywords("PLOP");
        mediaBean.setTraductionData("#\n" +
            "#Mon Nov 09 12:03:26 CET 2015\n" +
            "META_KEYWORDS_1=Southern Himalayan Mountains.\n");
        final String metaKeywords0 = serviceMedia.getMetaKeywords(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(metaKeywords0, "Le premier appel ne devrait pas renvoyer un metaKeywords null");
        Assert.assertTrue(StringUtils.isNotBlank(metaKeywords0), "Le premier appel ne devrait pas renvoyer un metaKeywords vide");
        Assert.assertEquals(metaKeywords0, "PLOP", "Le premier appel devrait renvoyer un metaKeywords égal à \"PLOP\"");
        mediaBean.setIsMutualise("0");
        final String metaKeywords1 = serviceMedia.getMetaKeywords(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(metaKeywords1, "Le second appel ne devrait pas renvoyer un metaKeywords null");
        Assert.assertTrue(StringUtils.isNotBlank(metaKeywords1), "Le second appel ne devrait pas renvoyer un metaKeywords vide");
        Assert.assertEquals(metaKeywords1, "Southern Himalayan Mountains.", "Le second appel devrait renvoyer un metaKeywords égal à \"Southern Himalayan Mountains.\"");
        final String metaKeywords2 = serviceMedia.getMetaKeywords(mediaBean, Locale.FRANCE);
        Assert.assertNotNull(metaKeywords2, "Le troisième appel ne devrait pas renvoyer un metaKeywords null");
        Assert.assertTrue(StringUtils.isBlank(metaKeywords2), "Le troisième appel devrait renvoyer un metaKeywords vide");
    }

    @Test
    public void testGetCountFromWhere(@Mocked("getCountFromWhere") final MediaDAO mockedDao) throws IOException {
        final ClauseWhere where = new ClauseWhere();
        final MediaDAO mediaDAO = new MediaDAO();
        serviceMedia.setDao(mediaDAO);
        new Expectations() {{
            mediaDAO.getCountFromWhere((ClauseWhere) any);
            result = 0;
        }};
        final int count0 = serviceMedia.getCountForWhere(where);
        Assert.assertEquals(count0, 0, "Le premier appel devrait renvoyer 0");
        new Expectations() {{
            mediaDAO.getCountFromWhere((ClauseWhere) any);
            result = 1;
        }};
        final int count1 = serviceMedia.getCountForWhere(where);
        Assert.assertEquals(count1, 1, "Le second appel devrait renvoyer 1");
    }
}
