package com.univ.objetspartages.services;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.email.JSBMailbox;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.PassRequestBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.dao.impl.PassRequestDAO;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;

import mockit.Deencapsulation;
import mockit.Expectations;
import mockit.Invocation;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

/**
 * Created on 30/10/15.
 */
@Test
@ContextConfiguration("ServiceUserPassTest.test-context.xml")
public class ServiceUserPassTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ServiceUserPass serviceUserPass;

    @Test
    public void testGetLongueurChampMotDePasse() {
        new MockUp<PropertyHelper>() {

            @Mock
            public String getCoreProperty(Invocation context, final String key) {
                switch (context.getInvocationCount()) {
                    case 1:
                        return null;
                    case 2:
                        return StringUtils.EMPTY;
                    default:
                        return "100";
                }
            }
        };
        Assert.assertEquals(ServiceUserPass.getLongueurChampMotDePasse(), 64, "La taille retournée devrait être 64");
        Assert.assertEquals(ServiceUserPass.getLongueurChampMotDePasse(), 64, "La taille retournée devrait être 64");
        Assert.assertEquals(ServiceUserPass.getLongueurChampMotDePasse(), 100, "La taille retournée devrait être 100");
    }

    @Test
    public void testRequestNewPass(@Mocked({"getByRequestId", "delete"}) final PassRequestDAO dao) {
        final PassRequestDAO passRequestDAO = new PassRequestDAO();
        serviceUserPass.setDao(passRequestDAO);
        final ContexteUniv contexteUniv = new MockUp<ContexteUniv>() {

            @Mock
            public void $init(String uri) {}

            @Mock
            public InfosSite getInfosSite() {
                final InfosSiteImpl infosSite = new InfosSiteImpl();
                infosSite.setIntitule("K-Sup");
                return infosSite;
            }
        }.getMockInstance();
        new MockUp<ContexteUtil>() {

            @Mock
            public ContexteUniv getContexteUniv() {
                return contexteUniv;
            }
        };
        new MockUp<MessageHelper>() {

            @Mock
            public String getCoreMessage(String key) {
                return "plop";
            }
        };
        new MockUp<URLResolver>() {

            @Mock
            public String getAbsoluteUrl(final String relativeUrl, final OMContext ctx) {
                return "/url/absolue/";
            }
        };
        new MockUp<JSBMailbox>() {

            @Mock
            public void $init(boolean debug) {}

            @Mock
            public void sendSystemMsg(Invocation context, final String to, final String subject, final String message) throws MessagingException, EmailException {
                Assert.assertEquals(to, "test@kosmos.fr");
                Assert.assertEquals(subject, "K-Sup plop");
                Assert.assertEquals(message, "plop/url/absolue/plopK-Sup");
                if(context.getInvocationCount() > 1) {
                    throw new EmailException();
                }
            }
        };
        final UtilisateurBean utilisateurBean = new UtilisateurBean();
        utilisateurBean.setCode("test");
        final PassRequestBean passRequestBean = new PassRequestBean();
        passRequestBean.setId(1L);
        final UUID uuid = UUID.randomUUID();
        new Expectations(serviceUserPass){{
            Deencapsulation.invoke(serviceUserPass, "savePassRequest", anyString, anyString);
            result = uuid;
            dao.getByRequestId(uuid);
            result = passRequestBean;
        }};
        // Le premier appel se déroule normalement
        serviceUserPass.requestNewPass(utilisateurBean, "test@kosmos.fr");
        // Lors du second appel, le mock JSBMailBox lève une exception
        serviceUserPass.requestNewPass(utilisateurBean, "test@kosmos.fr");
    }

    @Test
    public void testSavePassRequest(@Mocked("add") final PassRequestDAO dao) {
        final PassRequestDAO passRequestDAO = new PassRequestDAO();
        serviceUserPass.setDao(passRequestDAO);
        final UUID uuid = Deencapsulation.invoke(serviceUserPass, "savePassRequest", "test", "test@kosmos.fr");
        Assert.assertNotNull(uuid, "L'uuid retourné ne devrait pas être null");
    }

    @Test
    public void testHandlePassRequest(@Mocked({"getByRequestId", "delete", "getByCode"}) final PassRequestDAO dao) {
        final PassRequestDAO passRequestDAO = new PassRequestDAO();
        serviceUserPass.setDao(passRequestDAO);
        final PassRequestBean passRequestBean = new PassRequestBean();
        passRequestBean.setId(1L);
        passRequestBean.setCode("request");
        passRequestBean.setEmail("test@kosmos.fr");
        final UUID uuid = UUID.randomUUID();
        new Expectations() {{
            dao.getByRequestId(uuid);
            result = null;
        }};
        final Map<String, String> map1 = serviceUserPass.handlePassRequest(null);
        Assert.assertNotNull(map1, "La map retournée ne devrait pas être null");
        Assert.assertTrue(MapUtils.isEmpty(map1), "La map retournée devrait être vide");
        final Map<String, String> map2 = serviceUserPass.handlePassRequest(uuid);
        Assert.assertNotNull(map2, "La map retournée ne devrait pas être null");
        Assert.assertTrue(MapUtils.isEmpty(map2), "La map retournée devrait être vide");
        new Expectations(){{
            dao.getByRequestId(uuid);
            result = passRequestBean;
            dao.getByCode("request");
            result = Arrays.asList(passRequestBean);
        }};
        final Map<String, String> map3 = serviceUserPass.handlePassRequest(uuid);
        Assert.assertNotNull(map3, "La map retournée ne devrait pas être null");
        Assert.assertTrue(MapUtils.isNotEmpty(map3), "La map retournée ne devrait pas être vide");
        Assert.assertTrue(map3.containsKey("code") && map3.containsKey("email") && map3.containsKey("motDePasse"), "La map retournée devrait contenir les clés \"code\", \"email\" et \"motDePasse\"");
        Assert.assertEquals(map3.get("code"), "request", "La valeur de \"code\" devrait être \"request\"");
        Assert.assertEquals(map3.get("email"), "test@kosmos.fr", "La valeur de \"email\" devrait être \"test@kosmos.fr\"");
        Assert.assertTrue(map3.get("motDePasse").length() == 10, "La valeur de \"motDePasse\" devrait être une chaîne d'exactement 10 caractères.");
    }
}
