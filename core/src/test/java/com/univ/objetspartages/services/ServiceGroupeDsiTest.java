package com.univ.objetspartages.services;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.kosmos.tests.testng.annotations.ClearCache;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.ExtensionManager;
import com.kportal.extension.IExtension;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.dao.impl.GroupeDsiDAO;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.InfosRequeteGroupe;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.util.InfosRolesUtils;

import mockit.Expectations;
import mockit.Invocation;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

/**
 * Created on 27/05/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/objetspartages/services/ServiceGroupeDsiTest.test-context.xml"})
public class ServiceGroupeDsiTest extends AbstractCacheTestngTests {

    @Autowired
    ServiceGroupeDsi serviceGroupeDsi;

    @Test
    public void testGetAll(@Mocked("selectAll") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.selectAll();
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
        }};
        final List<GroupeDsiBean> groups = serviceGroupeDsi.getAll();
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups) && groups.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
    }

    @Test
    public void testGetAllDynamics(@Mocked("selectAllDynamics") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.selectAllDynamics();
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
        }};
        final List<GroupeDsiBean> groups = serviceGroupeDsi.getAllDynamics();
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups) && groups.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
    }

    @Test
    public void testGetByLabel(@Mocked("selectByLabel") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        final GroupeDsiBean group1 = new GroupeDsiBean();
        group1.setCode("CODE1");
        group1.setLibelle("Groupe 1");
        final GroupeDsiBean group2 = new GroupeDsiBean();
        group1.setCode("CODE12");
        group1.setLibelle("Groupe 12");
        new Expectations() {{
            dao.selectByLabel("Groupe 1");
            result = Arrays.asList(group1, group2);
            dao.selectByLabel("Groupe 12");
            result = Collections.singletonList(group2);
            dao.selectByLabel("Groupe 3");
            result = Collections.EMPTY_LIST;
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByLabel("Groupe 1");
        final List<GroupeDsiBean> groups2 = serviceGroupeDsi.getByLabel("Groupe 12");
        final List<GroupeDsiBean> groups3 = serviceGroupeDsi.getByLabel("Groupe 3");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel devrait renvoyer une liste contenant 2 éléments");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups2) && groups2.size() == 1, "Le second appel devrait renvoyer une liste contenant 1 élément");
        Assert.assertTrue(groups3 != null && CollectionUtils.isEmpty(groups3), "Le troisième appel devrait renvoyer une liste vide");
    }

    @Test
    public void testGetByLdapRequest(@Mocked("selectByLdapRequest") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.selectByLdapRequest("PLOP");
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
            dao.selectByLdapRequest(StringUtils.EMPTY);
            result = Collections.EMPTY_LIST;
            dao.selectByLdapRequest(null);
            result = Collections.EMPTY_LIST;
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByLdapRequest("PLOP");
        final List<GroupeDsiBean> groups2 = serviceGroupeDsi.getByLdapRequest(StringUtils.EMPTY);
        final List<GroupeDsiBean> groups3 = serviceGroupeDsi.getByLdapRequest(null);
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
        Assert.assertTrue(groups2 != null && CollectionUtils.isEmpty(groups2), "Le second appel devrait renvoyer une liste vide");
        Assert.assertTrue(groups3 != null && CollectionUtils.isEmpty(groups3), "Le troisième appel devrait renvoyer une liste vide");
    }

    @Test
    public void testGetByImportSource(@Mocked("selectByImportSource") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.selectByImportSource("PLOP");
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
            dao.selectByImportSource(StringUtils.EMPTY);
            result = Collections.EMPTY_LIST;
            dao.selectByImportSource(null);
            result = Collections.EMPTY_LIST;
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByImportSource("PLOP");
        final List<GroupeDsiBean> groups2 = serviceGroupeDsi.getByImportSource(StringUtils.EMPTY);
        final List<GroupeDsiBean> groups3 = serviceGroupeDsi.getByImportSource(null);
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
        Assert.assertTrue(groups2 != null && CollectionUtils.isEmpty(groups2), "Le second appel devrait renvoyer une liste vide");
        Assert.assertTrue(groups3 != null && CollectionUtils.isEmpty(groups3), "Le troisième appel devrait renvoyer une liste vide");
    }

    @Test
    public void testGetByImportSourceAndParentGroup(@Mocked("selectByImportSourceAndParentGroup") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.selectByImportSourceAndParentGroup("PLIP", "PLOP");
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
            dao.selectByImportSourceAndParentGroup("PLIP", null);
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
            dao.selectByImportSourceAndParentGroup("PLIP", StringUtils.EMPTY);
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
            dao.selectByImportSourceAndParentGroup(StringUtils.EMPTY, anyString);
            result = Collections.EMPTY_LIST;
            dao.selectByImportSourceAndParentGroup(null, anyString);
            result = Collections.EMPTY_LIST;
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByImportSourceAndParentGroup("PLIP", "PLOP");
        final List<GroupeDsiBean> groups2 = serviceGroupeDsi.getByImportSourceAndParentGroup("PLIP", StringUtils.EMPTY);
        final List<GroupeDsiBean> groups3 = serviceGroupeDsi.getByImportSourceAndParentGroup("PLIP", null);
        final List<GroupeDsiBean> groups4 = serviceGroupeDsi.getByImportSourceAndParentGroup(StringUtils.EMPTY, null);
        final List<GroupeDsiBean> groups5 = serviceGroupeDsi.getByImportSourceAndParentGroup(null, null);
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups2) && groups2.size() == 2, "Le second appel doit renvoyer une liste avec 2 éléments.");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups3) && groups3.size() == 2, "Le troisième appel doit renvoyer une liste avec 2 éléments.");
        Assert.assertTrue(groups4 != null && CollectionUtils.isEmpty(groups4), "Le quatrième appel devrait renvoyer une liste vide");
        Assert.assertTrue(groups5 != null && CollectionUtils.isEmpty(groups5), "Le cinquième appel devrait renvoyer une liste vide");
    }

    @Test
    public void testGetByCodeTypeLabelStructureAndCache(@Mocked("getByCodeTypeLabelStructureAndCache") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.getByCodeTypeLabelStructureAndCache("PLIP", "PLOP", "PLOUP", "PLAP", "PLUP");
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByCodeTypeLabelStructureAndCache("PLIP", "PLOP", "PLOUP", "PLAP", "PLUP");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
    }

    @Test
    public void testGetByCodeAndType(@Mocked("getByCodeAndType") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.getByCodeAndType(anyString, anyString);
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByCodeAndType("PLIP", "PLOP");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
    }

    @Test
    public void testGetByCodeAndCache(@Mocked("getByCodeAndCache") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.getByCodeAndCache(anyString, anyString);
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByCodeAndCache("PLIP", "PLOP");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
    }

    @Test
    public void testGetByParentGroup(@Mocked("selectByParentGroup") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        final GroupeDsiBean group1 = new GroupeDsiBean();
        group1.setCode("GROUP1");
        final GroupeDsiBean group2 = new GroupeDsiBean();
        group2.setCode("GROUP2");
        new Expectations() {{
            dao.selectByParentGroup("GROUP_PARENT");
            result = Collections.singletonList(group2);
            dao.selectByParentGroup(StringUtils.EMPTY);
            result = Collections.singletonList(group1);
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByParentGroup("");
        final List<GroupeDsiBean> groups2 = serviceGroupeDsi.getByParentGroup("GROUP_PARENT");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 1, "Le second appel doit renvoyer une liste avec 1 élément.");
        Assert.assertTrue(groups1.get(0).getCode().equals("GROUP1"), "Le second appel doit renvoyer une liste avec 1 élément dont le code est \"GROUP1\".");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups2) && groups2.size() == 1, "Le deuxième appel doit renvoyer une liste avec 1 élément.");
        Assert.assertTrue(groups2.get(0).getCode().equals("GROUP2"), "Le deuxième appel doit renvoyer une liste avec 1 élément dont le code est \"GROUP2\".");
    }

    @Test
    public void testGetByCodeAndImportSource(@Mocked("selectByCodeAndImportSource") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.selectByCodeAndImportSource(anyString, anyString);
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByCodeAndImportSource("PLIP", "PLOP");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
    }

    @Test
    public void testGetByStructure(@Mocked("getByStructure") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.getByStructure(anyString);
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByStructure("PLIP");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
    }

    @Test
    public void testGetByStructureAndImportSource(@Mocked("getByStructureAndImportSource") final GroupeDsiDAO dao1) {
        final GroupeDsiDAO dao = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(dao);
        new Expectations() {{
            dao.getByStructureAndImportSource(anyString, anyString);
            result = Arrays.asList(new GroupeDsiBean(), new GroupeDsiBean());
        }};
        final List<GroupeDsiBean> groups1 = serviceGroupeDsi.getByStructureAndImportSource("PLIP", "PLOP");
        Assert.assertTrue(CollectionUtils.isNotEmpty(groups1) && groups1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments.");
    }

    @Test
    @ClearCache(names = "ServiceGroupeDsi.getByCode")
    public void testGetByCode(@Mocked("selectByCode") final GroupeDsiDAO dao1) throws ParseException {
        final GroupeDsiBean groupeDsiBean1 = new GroupeDsiBean();
        groupeDsiBean1.setCode("TEST_GETBYCODE");
        groupeDsiBean1.setLibelle("Group1");
        final GroupeDsiDAO groupeDsiDAO = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(groupeDsiDAO);
        new Expectations() {{
            groupeDsiDAO.selectByCode(anyString);
            result = groupeDsiBean1;
        }};
        final GroupeDsiBean groupByCode = serviceGroupeDsi.getByCode("TEST_GETBYCODE");
        Assert.assertNotNull(groupByCode, "Le premier appel doit retourner un groupe");
        Assert.assertTrue(groupByCode.getCode().equals(groupeDsiBean1.getCode()), "Le premier appel doit renvoyer le groupe portant le code TEST_GETBYCODE");
        serviceGroupeDsi.getByCode("TEST_GETBYCODE");
        final GroupeDsiBean emptyCode = serviceGroupeDsi.getByCode(StringUtils.EMPTY);
        Assert.assertNull(emptyCode, "le résultat doit être null");
        new Verifications() {{
            groupeDsiDAO.selectByCode(anyString);
            times = 1;
        }};
    }

    @Test
    public void testDelete(@Mocked({"selectByCode", "delete", "getById"}) final GroupeDsiDAO dao) throws ParseException {
        final GroupeDsiDAO groupeDsiDAO = new GroupeDsiDAO();
        final GroupeDsiBean groupeDsiBean1 = new GroupeDsiBean();
        groupeDsiBean1.setCode("TEST_DELETE");
        groupeDsiBean1.setLibelle("Groupe delete");
        serviceGroupeDsi.setDao(groupeDsiDAO);
        new Expectations() {{
            groupeDsiDAO.selectByCode(anyString);
            result = groupeDsiBean1;
        }};
        final GroupeDsiBean groupByCode = serviceGroupeDsi.getByCode("TEST_DELETE");
        Assert.assertNotNull(groupByCode, "Le premier appel doit retourner le groupe");
        Assert.assertTrue(groupByCode.getCode().equals(groupeDsiBean1.getCode()), "Le premier appel doit renvoyer le groupe portant le code TEST_DELETE");
        new Expectations() {{
            groupeDsiDAO.getById(anyLong);
            result = groupeDsiBean1;
        }};
        serviceGroupeDsi.delete(0L);
        serviceGroupeDsi.getByCode("TEST_DELETE");
        new Verifications() {{
            groupeDsiDAO.selectByCode(anyString);
            times = 1;
        }};
    }

    @Test
    public void testSave(@Mocked({"selectByCode", "add", "update"}) final GroupeDsiDAO dao) throws ParseException {
        final GroupeDsiDAO groupeDsiDAO = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(groupeDsiDAO);
        new Expectations() {{
            groupeDsiDAO.selectByCode(anyString);
            result = null;
        }};
        final GroupeDsiBean groupByCode1 = serviceGroupeDsi.getByCode("TEST_SAVE");
        Assert.assertNull(groupByCode1, "Le premier appel ne doit pas retourner de groupe");
        final GroupeDsiBean groupeDsiBean1 = new GroupeDsiBean();
        groupeDsiBean1.setCode("TEST_SAVE");
        groupeDsiBean1.setLibelle("Group1");
        new Expectations() {{
            groupeDsiDAO.add(groupeDsiBean1);
            result = groupeDsiBean1;
            groupeDsiDAO.selectByCode("TEST_SAVE");
            result = groupeDsiBean1;
        }};
        serviceGroupeDsi.save(groupeDsiBean1);
        final GroupeDsiBean groupByCode2 = serviceGroupeDsi.getByCode("TEST_SAVE");
        Assert.assertNotNull(groupByCode2, "Le second appel doit retourner un groupe");
        Assert.assertTrue(groupByCode2.getCode().equals(groupeDsiBean1.getCode()), "Le second appel doit renvoyer le groupe portant le code TEST_SAVE");
        final GroupeDsiBean groupeDsiBean2 = new GroupeDsiBean();
        groupeDsiBean2.setCode("TEST_SAVE");
        groupeDsiBean2.setLibelle("Group2");
        new Expectations() {{
            groupeDsiDAO.update(groupeDsiBean1);
            result = groupeDsiBean2;
            groupeDsiDAO.selectByCode("TEST_SAVE");
            result = groupeDsiBean2;
        }};
        groupeDsiBean1.setId(1L);
        serviceGroupeDsi.save(groupeDsiBean1);
        final GroupeDsiBean groupByCode3 = serviceGroupeDsi.getByCode("TEST_SAVE");
        Assert.assertNotNull(groupByCode3, "Le troisième appel doit retourner un groupe");
        Assert.assertTrue(groupByCode3.getLibelle().equals("Group2"), "Le troisième appel doit fournir la nouvelle version de TEST_SAVE");
    }

    @Test
    @ClearCache(names = "ServiceGroupeDsi.getLevel")
    public void testGetLevel(@Mocked("selectByCode") final GroupeDsiDAO dao) throws ParseException {
        final GroupeDsiDAO groupeDsiDAO = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(groupeDsiDAO);
        final GroupeDsiBean root = new GroupeDsiBean();
        root.setCode(StringUtils.EMPTY);
        final GroupeDsiBean child1 = new GroupeDsiBean();
        child1.setCode("CHILD1");
        child1.setCodeGroupePere(StringUtils.EMPTY);
        final GroupeDsiBean child2 = new GroupeDsiBean();
        child2.setCode("CHILD2");
        child2.setCodeGroupePere(StringUtils.EMPTY);
        final GroupeDsiBean child3 = new GroupeDsiBean();
        child3.setCode("CHILD3");
        child3.setCodeGroupePere("CHILD2");
        final GroupeDsiBean child4 = new GroupeDsiBean();
        child4.setCode("CHILD4");
        child4.setCodeGroupePere("CHILD3");
        new NonStrictExpectations() {{
            groupeDsiDAO.selectByCode(StringUtils.EMPTY);
            result = root;
            groupeDsiDAO.selectByCode("CHILD1");
            result = child1;
            groupeDsiDAO.selectByCode("CHILD2");
            result = child2;
            groupeDsiDAO.selectByCode("CHILD3");
            result = child3;
            groupeDsiDAO.selectByCode("CHILD4");
            result = child4;
        }};
        int levelRoot = serviceGroupeDsi.getLevel(serviceGroupeDsi.getByCode(StringUtils.EMPTY));
        int levelChild1 = serviceGroupeDsi.getLevel(serviceGroupeDsi.getByCode("CHILD1"));
        int levelChild2 = serviceGroupeDsi.getLevel(serviceGroupeDsi.getByCode("CHILD2"));
        int levelChild3 = serviceGroupeDsi.getLevel(serviceGroupeDsi.getByCode("CHILD3"));
        int levelChild4 = serviceGroupeDsi.getLevel(serviceGroupeDsi.getByCode("CHILD4"));
        Assert.assertEquals(levelRoot, 0, "Le niveau de la racine devrait être 0");
        Assert.assertEquals(levelChild1, 1, "Le niveau de l'enfant 1 devrait être 1");
        Assert.assertEquals(levelChild2, 1, "Le niveau de l'enfant 2 devrait être 1");
        Assert.assertEquals(levelChild3, 2, "Le niveau de l'enfant 3 devrait être 2");
        Assert.assertEquals(levelChild4, 3, "Le niveau de l'enfant 4 devrait être 3");
    }

    @Test
    @ClearCache(names = "ServiceGroupeDsi.getAllSubGroups")
    public void testGetAllSubGroups(@Mocked("getByParentGroup") final ServiceGroupeDsi mockedServiceGroupeDsi) throws ParseException {
        final GroupeDsiBean root = new GroupeDsiBean();
        root.setCode(StringUtils.EMPTY);
        final GroupeDsiBean child1 = new GroupeDsiBean();
        child1.setCode("CHILD1");
        child1.setCodeGroupePere(StringUtils.EMPTY);
        final GroupeDsiBean child2 = new GroupeDsiBean();
        child2.setCode("CHILD2");
        child2.setCodeGroupePere(StringUtils.EMPTY);
        final GroupeDsiBean child3 = new GroupeDsiBean();
        child3.setCode("CHILD3");
        child3.setCodeGroupePere("CHILD2");
        final GroupeDsiBean child4 = new GroupeDsiBean();
        child4.setCode("CHILD4");
        child4.setCodeGroupePere("CHILD3");
        new Expectations() {{
            serviceGroupeDsi.getByParentGroup(StringUtils.EMPTY);
            result = Arrays.asList(child1, child2);
            serviceGroupeDsi.getByParentGroup("CHILD1");
            result = Collections.EMPTY_LIST;
            serviceGroupeDsi.getByParentGroup("CHILD2");
            result = Arrays.asList(child3);
            serviceGroupeDsi.getByParentGroup("CHILD3");
            result = Arrays.asList(child4);
            serviceGroupeDsi.getByParentGroup("CHILD4");
            result = Collections.EMPTY_LIST;
        }};
        final List<GroupeDsiBean> rootChildren = serviceGroupeDsi.getAllSubGroups(StringUtils.EMPTY);
        final List<GroupeDsiBean> child1Children = serviceGroupeDsi.getAllSubGroups("CHILD1");
        final List<GroupeDsiBean> child2Children = serviceGroupeDsi.getAllSubGroups("CHILD2");
        final List<GroupeDsiBean> child3Children = serviceGroupeDsi.getAllSubGroups("CHILD3");
        final List<GroupeDsiBean> child4Children = serviceGroupeDsi.getAllSubGroups("CHILD4");
        Assert.assertTrue(rootChildren != null && CollectionUtils.isNotEmpty(rootChildren), "Le premier appel doit renvoyer une liste avec des éléments");
        Assert.assertTrue(rootChildren.size() == 4, "Le premier appel doit renvoyer une liste avec 4 éléments");
        Assert.assertTrue(child1Children != null && CollectionUtils.isEmpty(child1Children), "Le second appel doit renvoyer une liste vide");
        Assert.assertTrue(child2Children != null && CollectionUtils.isNotEmpty(child2Children), "Le troisième appel doit renvoyer une liste avec des éléments");
        Assert.assertTrue(child2Children.size() == 2, "Le troisième appel doit renvoyer une liste avec 2 éléments");
        Assert.assertTrue(child3Children != null && CollectionUtils.isNotEmpty(child3Children), "Le quatrième appel doit renvoyer une liste avec des éléments");
        Assert.assertTrue(child3Children.size() == 1, "Le quatrième appel doit renvoyer une liste avec 1 élément");
        Assert.assertTrue(child4Children != null && CollectionUtils.isEmpty(child4Children), "Le cinquième appel doit renvoyer une liste vide");
        // Cache
        child1.setCodeGroupePere("CHILD4");
        final List<GroupeDsiBean> cacheChildren = serviceGroupeDsi.getAllSubGroups("CHILD4");
        Assert.assertTrue(cacheChildren != null && CollectionUtils.isEmpty(cacheChildren), "Le dernier appel doit passer par le cache et renvoyer une liste vide");
    }

    @Test
    @ClearCache(names = "ServiceGroupeDsi.getByType")
    public void testGetByType(@Mocked("selectAll") final GroupeDsiDAO dao) throws ParseException {
        final GroupeDsiDAO groupeDsiDAO = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(groupeDsiDAO);
        final GroupeDsiBean root = new GroupeDsiBean();
        root.setCode(StringUtils.EMPTY);
        final GroupeDsiBean child1 = new GroupeDsiBean();
        child1.setCode("CHILD1");
        child1.setCodeGroupePere(StringUtils.EMPTY);
        child1.setType("TYPE1");
        final GroupeDsiBean child2 = new GroupeDsiBean();
        child2.setCode("CHILD2");
        child2.setCodeGroupePere(StringUtils.EMPTY);
        child2.setType("TYPE1");
        final GroupeDsiBean child3 = new GroupeDsiBean();
        child3.setCode("CHILD3");
        child3.setCodeGroupePere("CHILD2");
        child3.setType("TYPE2");
        final GroupeDsiBean child4 = new GroupeDsiBean();
        child4.setCode("CHILD4");
        child4.setCodeGroupePere("CHILD3");
        child4.setType("TYPE3");
        new Expectations() {{
            groupeDsiDAO.selectAll();
            result = Arrays.asList(child1, child2, child3, child4);
        }};
        final List<GroupeDsiBean> type1 = serviceGroupeDsi.getByType("TYPE1");
        final List<GroupeDsiBean> type2 = serviceGroupeDsi.getByType("TYPE2");
        final List<GroupeDsiBean> type3 = serviceGroupeDsi.getByType("TYPE3");
        final List<GroupeDsiBean> type4 = serviceGroupeDsi.getByType(StringUtils.EMPTY);
        Assert.assertTrue(type1 != null && CollectionUtils.isNotEmpty(type1), "Le premier appel doit renvoyer une liste avec des éléments");
        Assert.assertTrue(type1.size() == 2, "Le premier appel doit renvoyer une liste avec 2 éléments");
        Assert.assertTrue(type2 != null && CollectionUtils.isNotEmpty(type2), "Le second appel doit renvoyer une liste avec des éléments");
        Assert.assertTrue(type2.size() == 1, "Le second appel doit renvoyer une liste avec 1 élément");
        Assert.assertTrue(type3 != null && CollectionUtils.isNotEmpty(type3), "Le troisième appel doit renvoyer une liste avec des éléments");
        Assert.assertTrue(type3.size() == 1, "Le troisième appel doit renvoyer une liste avec 1 élément");
        Assert.assertTrue(type4 != null && CollectionUtils.isEmpty(type4), "Le quatrième appel doit renvoyer une liste vide");
        // Cache
        child1.setType("TYPE3");
        final List<GroupeDsiBean> cacheType = serviceGroupeDsi.getByType("TYPE1");
        Assert.assertTrue(cacheType != null && CollectionUtils.isNotEmpty(cacheType), "Le dernier appel doit renvoyer une liste avec des éléments");
        Assert.assertTrue(cacheType.size() == 2, "Le dernier appel doit renvoyer une liste avec 2 éléments (passe par le cache)");
        new Verifications() {{
            groupeDsiDAO.selectAll();
            times = 4;
        }};
    }

    @Test
    public void testContains(@Mocked("getAllSubGroups") final ServiceGroupeDsi serviceGroupeDsi) throws ParseException {
        final GroupeDsiDAO groupeDsiDAO = new GroupeDsiDAO();
        serviceGroupeDsi.setDao(groupeDsiDAO);
        final GroupeDsiBean root = new GroupeDsiBean();
        root.setCode(StringUtils.EMPTY);
        final GroupeDsiBean child1 = new GroupeDsiBean();
        child1.setCode("CHILD1");
        child1.setCodeGroupePere(StringUtils.EMPTY);
        final GroupeDsiBean child2 = new GroupeDsiBean();
        child2.setCode("CHILD2");
        child2.setCodeGroupePere(StringUtils.EMPTY);
        final GroupeDsiBean child3 = new GroupeDsiBean();
        child3.setCode("CHILD3");
        child3.setCodeGroupePere("CHILD2");
        final GroupeDsiBean child4 = new GroupeDsiBean();
        child4.setCode("CHILD4");
        child4.setCodeGroupePere("CHILD3");
        new Expectations() {{
            serviceGroupeDsi.getAllSubGroups(StringUtils.EMPTY);
            result = Arrays.asList(child1, child2, child3, child4);
            serviceGroupeDsi.getAllSubGroups("CHILD1");
            result = Collections.EMPTY_LIST;
            serviceGroupeDsi.getAllSubGroups("CHILD2");
            result = Arrays.asList(child3, child4);
            serviceGroupeDsi.getAllSubGroups("CHILD3");
            result = Arrays.asList(child4);
            serviceGroupeDsi.getAllSubGroups("CHILD4");
            result = Collections.EMPTY_LIST;
        }};
        Assert.assertFalse(serviceGroupeDsi.contains(root, null));
        Assert.assertTrue(serviceGroupeDsi.contains(root, child1));
        Assert.assertTrue(serviceGroupeDsi.contains(root, child2));
        Assert.assertTrue(serviceGroupeDsi.contains(root, child3));
        Assert.assertTrue(serviceGroupeDsi.contains(root, child4));
        Assert.assertFalse(serviceGroupeDsi.contains(child1, root));
        Assert.assertFalse(serviceGroupeDsi.contains(child1, child2));
        Assert.assertFalse(serviceGroupeDsi.contains(child1, child3));
        Assert.assertFalse(serviceGroupeDsi.contains(child1, child4));
        Assert.assertFalse(serviceGroupeDsi.contains(child2, root));
        Assert.assertTrue(serviceGroupeDsi.contains(child2, child3));
        Assert.assertTrue(serviceGroupeDsi.contains(child2, child4));
        Assert.assertTrue(serviceGroupeDsi.contains(child2, child4));
        Assert.assertFalse(serviceGroupeDsi.contains(child3, root));
        Assert.assertTrue(serviceGroupeDsi.contains(child3, child4));
        Assert.assertFalse(serviceGroupeDsi.contains(child4, root));
        Assert.assertTrue(serviceGroupeDsi.contains(child1, child1));
        Assert.assertTrue(serviceGroupeDsi.contains(child2, child2));
        Assert.assertTrue(serviceGroupeDsi.contains(child3, child3));
        Assert.assertTrue(serviceGroupeDsi.contains(child4, child4));
    }

    @Test
    public void testGetGroupRequestList(@Mocked("getExtensions") final ExtensionManager extensionManager) {
        final IExtension extension = new MockUp<IExtension>() {

            @Mock
            public Properties getProperties(Invocation context) {
                final Properties properties = new Properties();
                properties.put("requete_groupe.plop.classe", "com.kosmos.test.plop");
                properties.put("requete_groupe.plop.intitule", "Plop");
                properties.put("requete_groupe.plop.template_jsp", "Plop.jsp");
                if (context.getInvocationCount() == 2) {
                    properties.put("requete_groupe.plop.expiration_cache_utilisateur", "10");
                }
                return properties;
            }

            @Mock
            public String getRelativePath() {
                return "/";
            }
        }.getMockInstance();
        new MockUp<ExtensionHelper>() {

            @Mock
            public ExtensionManager getExtensionManager() {
                return extensionManager;
            }
        };
        new Expectations() {{
            extensionManager.getExtensions();
            result = MapUtils.EMPTY_MAP;
        }};
        final Map<String, InfosRequeteGroupe> map1 = serviceGroupeDsi.getGroupRequestList();
        Assert.assertTrue(map1 != null && MapUtils.isEmpty(map1), "La première map renvoyée devrait être vide");
        final Map<String, IExtension> extensions = new HashMap<>();
        extensions.put("plop", extension);
        new Expectations() {{
            extensionManager.getExtensions();
            result = extensions;
        }};
        final Map<String, InfosRequeteGroupe> map2 = serviceGroupeDsi.getGroupRequestList();
        Assert.assertTrue(map2 != null && MapUtils.isNotEmpty(map2), "La seconde map renvoyée ne devrait pas être vide");
        Assert.assertTrue(map2.containsKey("plop"), "La seconde map renvoyée devrait contenir la clé \"plop\"");
        Assert.assertNotNull(map2.get("plop"), "La valeur associée à la clé \"plop\" ne devrait pas être nulle");
        Assert.assertEquals(map2.get("plop").getAlias(), "plop", "L'info requête devrait avoir l'alias \"plop\"");
        Assert.assertEquals(map2.get("plop").getIntitule(), "Plop", "L'info requête devrait avoir l'intitulé \"Plop\"");
        Assert.assertEquals(map2.get("plop").getNomClasse(), "com.kosmos.test.plop", "L'info requête devrait avoir la classe \"com.kosmos.test.plop\"");
        Assert.assertEquals(map2.get("plop").getTsCacheGroupesUtilisateur(), 0L, "L'info requête devrait avoir un ts cache égal à 0");
        Assert.assertEquals(map2.get("plop").getTemplateJSP(), "/Plop.jsp", "L'info requête devrait avoir pour template JSP \"/Plop.jsp\"");
        final Map<String, InfosRequeteGroupe> map3 = serviceGroupeDsi.getGroupRequestList();
        Assert.assertTrue(map3 != null && MapUtils.isNotEmpty(map3), "La troisième map renvoyée ne devrait pas être vide");
        Assert.assertTrue(map3.containsKey("plop"), "La troisième map renvoyée devrait contenir la clé \"plop\"");
        Assert.assertEquals(map3.get("plop").getTsCacheGroupesUtilisateur(), 10L, "L'info requête devrait avoir un ts cache égal à 10");
    }

    @Test
    public void testGetBreadCrumbs(@Mocked("getBreadCrumbsAsList") final ServiceGroupeDsi mockedServiceGroupeDsi) {
        new Expectations() {{
            serviceGroupeDsi.getBreadCrumbsAsList(anyString);
            result = Arrays.asList("Group1", "Group2", "Group3");
        }};
        Assert.assertEquals(serviceGroupeDsi.getBreadCrumbs("PLOP", StringUtils.EMPTY), "Group1 &gt; Group2 &gt; Group3");
        Assert.assertEquals(serviceGroupeDsi.getBreadCrumbs("PLOP", null), "Group1 &gt; Group2 &gt; Group3");
        Assert.assertEquals(serviceGroupeDsi.getBreadCrumbs("PLOP", " <> "), "Group1 <> Group2 <> Group3");
    }

    @Test
    public void testGetBreadCrumbsAsList(@Mocked("getByCode") final ServiceGroupeDsi mockedServiceGroupeDsi) {
        final GroupeDsiBean root = new GroupeDsiBean();
        root.setCode(StringUtils.EMPTY);
        final GroupeDsiBean child1 = new GroupeDsiBean();
        child1.setCode("CHILD1");
        child1.setLibelle("Group1");
        child1.setCodeGroupePere(StringUtils.EMPTY);
        final GroupeDsiBean child2 = new GroupeDsiBean();
        child2.setCode("CHILD2");
        child2.setLibelle("Group2");
        child2.setCodeGroupePere(StringUtils.EMPTY);
        final GroupeDsiBean child3 = new GroupeDsiBean();
        child3.setCode("CHILD3");
        child3.setLibelle("Group3");
        child3.setCodeGroupePere("CHILD2");
        final GroupeDsiBean child4 = new GroupeDsiBean();
        child4.setCode("CHILD4");
        child4.setLibelle("Group4");
        child4.setCodeGroupePere("CHILD3");
        new Expectations() {{
            serviceGroupeDsi.getByCode("CHILD1");
            result = child1;
            serviceGroupeDsi.getByCode("CHILD2");
            result = child2;
            serviceGroupeDsi.getByCode("CHILD3");
            result = child3;
            serviceGroupeDsi.getByCode("CHILD4");
            result = child4;
        }};
        final List<String> breadCrumbs1 = serviceGroupeDsi.getBreadCrumbsAsList("CHILD1");
        final List<String> breadCrumbs2 = serviceGroupeDsi.getBreadCrumbsAsList("CHILD2");
        final List<String> breadCrumbs3 = serviceGroupeDsi.getBreadCrumbsAsList("CHILD3");
        final List<String> breadCrumbs4 = serviceGroupeDsi.getBreadCrumbsAsList("CHILD4");
        Assert.assertTrue(CollectionUtils.isNotEmpty(breadCrumbs1), "Le premier appel devrait renvoyer une liste avec des éléments");
        Assert.assertTrue(breadCrumbs1.size() == 1 && breadCrumbs1.get(0).equals("Group1"), "Le premier appel devrait renvoyer une liste avec 1 élément \"Group1\"");
        Assert.assertTrue(CollectionUtils.isNotEmpty(breadCrumbs2), "Le second appel devrait renvoyer une liste avec des éléments");
        Assert.assertTrue(breadCrumbs2.size() == 1 && breadCrumbs2.get(0).equals("Group2"), "Le second appel devrait renvoyer une liste avec 1 élément \"Group2\"");
        Assert.assertTrue(CollectionUtils.isNotEmpty(breadCrumbs3), "Le troisième appel devrait renvoyer une liste avec des éléments");
        Assert.assertTrue(breadCrumbs3.size() == 2 && breadCrumbs3.get(0).equals("Group2") && breadCrumbs3.get(1).equals("Group3"), "Le troisième appel devrait renvoyer une liste avec 2 éléments \"Group2\" et \"Groupe3\" dans cet ordre");
        Assert.assertTrue(CollectionUtils.isNotEmpty(breadCrumbs4), "Le quatrième appel devrait renvoyer une liste avec des éléments");
        Assert.assertTrue(breadCrumbs4.size() == 3 && breadCrumbs4.get(0).equals("Group2") && breadCrumbs4.get(1).equals("Group3") && breadCrumbs4.get(2).equals("Group4"), "Le quatrième appel devrait renvoyer une liste avec 3 éléments \"Group2\", \"Groupe3\" et \"Groupe4\" dans cet ordre");
    }

    @Test
    public void testGetIntitules(@Mocked("getIntitule") final ServiceGroupeDsi serviceGroupeDsi) {
        Assert.assertTrue(serviceGroupeDsi.getIntitules(StringUtils.EMPTY).equals(StringUtils.EMPTY), "La chaîne retournée lors du premier appel doit être vide et non null");
        new Expectations() {{
            serviceGroupeDsi.getIntitule("GROUP1");
            result = "Groupe 1";
            serviceGroupeDsi.getIntitule("GROUP2");
            result = "Groupe 2";
            serviceGroupeDsi.getIntitule("GROUP3");
            result = "Groupe 3";
        }};
        final String labels = serviceGroupeDsi.getIntitules("GROUP1;GROUP2;GROUP3");
        Assert.assertNotNull(labels, "La chaîne retournée ne doit pas être null");
        Assert.assertEquals(labels, "Groupe 1;Groupe 2;Groupe 3", "La chaîne retournée doit être \"Groupe1;Groupe2;Groupe3\"");
        final String label1 = serviceGroupeDsi.getIntitules("GROUP1");
        Assert.assertNotNull(label1, "La chaîne retournée ne doit pas être null");
        Assert.assertEquals(label1, "Groupe 1", "La chaîne retournée doit être \"Groupe1\"");
    }

    @Test
    public void testGetIntitule(@Mocked("getByCode") final ServiceGroupeDsi mockedServiceGroupeDsi) {
        new MockUp<MessageHelper>() {

            @Mock
            public String getCoreMessage(String key) {
                return "Groupe inexistant";
            }
        };
        Assert.assertTrue(serviceGroupeDsi.getIntitule(StringUtils.EMPTY).equals(StringUtils.EMPTY), "La chaîne retournée lors du premier appel doit être vide et non null");
        final GroupeDsiBean child1 = new GroupeDsiBean();
        child1.setCode("CHILD1");
        child1.setLibelle("Group1");
        final GroupeDsiBean child2 = new GroupeDsiBean();
        child2.setCode("CHILD2");
        child2.setLibelle("Group2");
        final GroupeDsiBean child3 = new GroupeDsiBean();
        child3.setCode("CHILD3");
        new Expectations() {{
            serviceGroupeDsi.getByCode("CHILD1");
            result = child1;
            serviceGroupeDsi.getByCode("CHILD2");
            result = child2;
            serviceGroupeDsi.getByCode("CHILD3");
            result = child3;
            serviceGroupeDsi.getByCode(anyString);
            result = null;
        }};
        Assert.assertEquals(serviceGroupeDsi.getIntitule("CHILD1"), "Group1", "Le premier appel doit retourner \"Group1\"");
        Assert.assertEquals(serviceGroupeDsi.getIntitule("CHILD2"), "Group2", "Le second appel doit retourner \"Group2\"");
        Assert.assertEquals(serviceGroupeDsi.getIntitule("CHILD3"), "Groupe inexistant", "Le troisième appel doit retourner \"Groupe inexistant\"");
        Assert.assertEquals(serviceGroupeDsi.getIntitule("CHILD4"), "Groupe inexistant", "Le quatrième appel doit retourner \"Groupe inexistant\"");
    }

    @Test
    public void testGetListeRequetesGroupesPourAffichage(@Mocked("getGroupRequestList") final ServiceGroupeDsi mockedServiceGroupeDsi) {
        new Expectations() {{
            serviceGroupeDsi.getGroupRequestList();
            result = MapUtils.EMPTY_MAP;
        }};
        Assert.assertNotNull(serviceGroupeDsi.getListeRequetesGroupesPourAffichage(), "La map renvoyée ne devrait pas être null");
        Assert.assertTrue(MapUtils.isEmpty(serviceGroupeDsi.getListeRequetesGroupesPourAffichage()), "La map renvoyée devrait être vide");
        final InfosRequeteGroupe infosRequeteGroupe = new InfosRequeteGroupe();
        infosRequeteGroupe.setAlias("requete1");
        infosRequeteGroupe.setIntitule("Requête 1");
        final Map<String, InfosRequeteGroupe> map = new HashMap<>();
        map.put(infosRequeteGroupe.getAlias(), infosRequeteGroupe);
        new Expectations() {{
            serviceGroupeDsi.getGroupRequestList();
            result = map;
        }};
        final Map<String, String> result = serviceGroupeDsi.getListeRequetesGroupesPourAffichage();
        Assert.assertTrue(MapUtils.isNotEmpty(result), "La map renvoyée ne devrait pas être vide");
        Assert.assertTrue(result.containsKey("requete1"), "La map renvoyée devrait contenir la clé \"requete1\"");
        Assert.assertEquals(result.get("requete1"), "Requête 1", "La map renvoyée devrait contenir la valeur \"Requête 1\"");
    }

    @Test
    public void testRenvoyerGroupesEtPerimetres(@Mocked("getAll") final ServiceGroupeDsi mockedServiceGroupeDsi) {
        new MockUp<InfosRolesUtils>() {

            @Mock
            public Vector<Perimetre> renvoyerPerimetresAffectation(Invocation context, final String roles, final String _codeRole, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif) {
                if (context.getInvocationCount() == 1) {
                    return new Vector<>();
                }
                final Perimetre perimetre = new Perimetre("////");
                final Vector<Perimetre> perimetres = new Vector<>();
                perimetres.add(perimetre);
                return perimetres;
            }
        };
        new Expectations() {{
            serviceGroupeDsi.getAll();
            result = Collections.EMPTY_LIST;
        }};
        final GroupeDsiBean group = new GroupeDsiBean();
        group.setCode("GROUP1");
        final Map<String, List<Perimetre>> perimetres = serviceGroupeDsi.renvoyerGroupesEtPerimetres(StringUtils.EMPTY, Collections.EMPTY_LIST, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertTrue(perimetres != null && MapUtils.isEmpty(perimetres), "La map retournée devrait vide et non null");
        new Expectations() {{
            serviceGroupeDsi.getAll();
            result = Collections.singletonList(group);
        }};
        final Map<String, List<Perimetre>> perimetres2 = serviceGroupeDsi.renvoyerGroupesEtPerimetres(StringUtils.EMPTY, Collections.EMPTY_LIST, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertTrue(perimetres2 != null && MapUtils.isEmpty(perimetres2), "La map retournée devrait être vide");
        new Expectations() {{
            serviceGroupeDsi.getAll();
            result = Collections.singletonList(group);
        }};
        final Map<String, List<Perimetre>> perimetres3 = serviceGroupeDsi.renvoyerGroupesEtPerimetres(StringUtils.EMPTY, Collections.EMPTY_LIST, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertTrue(perimetres3 != null && MapUtils.isNotEmpty(perimetres3), "La map retournée ne devrait pas être vide");
        Assert.assertTrue(perimetres3.containsKey("GROUP1"), "La map retournée devrait contenir la clé \"GROUP1\"");
        Assert.assertTrue(perimetres3.get("GROUP1").size() == 1, "La map retournée devrait contenir une liste vide");
    }

    @Test
    public void testControlerPermission() {
        final AutorisationBean autorisation = new MockUp<AutorisationBean>() {

            @Mock
            public void $clinit() { /* do nothing */ }

            @Mock
            public boolean possedePermissionPartielleSurPerimetre(Invocation context, final PermissionBean permission, final Perimetre perimetre) {
                return context.getInvocationCount() > 1;
            }
        }.getMockInstance();
        Assert.assertFalse(serviceGroupeDsi.controlerPermission(autorisation, StringUtils.EMPTY, StringUtils.EMPTY));
        Assert.assertTrue(serviceGroupeDsi.controlerPermission(autorisation, StringUtils.EMPTY, StringUtils.EMPTY));
    }
}
