package com.univ.objetspartages.services;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.security.MySQLHelper;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RoleBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.dao.impl.UtilisateurDAO;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.util.InfosRolesUtils;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;

/**
 * Created by on 19/11/2015.
 */
@Test
@ContextConfiguration("ServiceUserTest.test-context.xml")
public class ServiceUserTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ServiceUser serviceUser;

    @Mocked
    private UtilisateurDAO utilisateurDAO;

    @Mocked
    private ServiceUserPass serviceUserPass;

    @Mocked
    private ServiceGroupeUtilisateur serviceGroupeUtilisateur;

    @Mocked
    private ServiceRole serviceRole;

    @Mocked
    private ServiceGroupeDsi serviceGroupeDsi;

    @BeforeClass
    public void initMock() {
        final UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
        final ServiceUserPass serviceUserPass = new ServiceUserPass();
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = new ServiceGroupeUtilisateur();
        final ServiceRole serviceRole = new ServiceRole();
        final ServiceGroupeDsi serviceGroupeDsi = new ServiceGroupeDsi();
        serviceUser.setDao(utilisateurDAO);
        serviceUser.setServiceUserPass(serviceUserPass);
        serviceUser.setServiceGroupeUtilisateur(serviceGroupeUtilisateur);
        serviceUser.setServiceRole(serviceRole);
        serviceUser.setServiceGroupeDsi(serviceGroupeDsi);
    }

    @Test
    public void testGetLongueurChampMotDePasse() {
        new Expectations() {{
            ServiceUserPass.getLongueurChampMotDePasse();
            result = ServiceUserPass.LONGUEUR_MOT_DE_PASSE_DEFAUT;
        }};
        int result = ServiceUser.getLongueurChampMotDePasse();
        Assert.assertEquals(result, ServiceUserPass.LONGUEUR_MOT_DE_PASSE_DEFAUT, "le mot de passe doit être égal à la valeur du mock");
    }

    @Test
    public void testRequestNewPass(@Mocked(stubOutClassInitialization = true) MessageHelper mockedMessage) {
        final UtilisateurBean user = new UtilisateurBean();
        new Expectations() {{
            utilisateurDAO.getByMailAndCode(null, null);
            result = Collections.emptyList();
            MessageHelper.getCoreMessage("ST_ERR_DEMANDE_MDP_COMPTES");
            result = "expected";
            MessageHelper.getCoreMessage("JTF_ERR_FMT_EMAIL");
            result = "expected";
            utilisateurDAO.getByMailAndCode("email@email", "login");
            result = Collections.singletonList(user);
        }};
        InfoBean infoBean = new InfoBean();
        try {
            serviceUser.requestNewPass(infoBean);
            Assert.fail("la méthode doit lever une exception");
        } catch (ErreurApplicative expected) {
            Assert.assertEquals(expected.getMessage(), "expected");
        }
        infoBean.set("EMAIL", "email");
        try {
            serviceUser.requestNewPass(infoBean);
            Assert.fail("la méthode doit lever une exception");
        } catch (ErreurApplicative expected) {
            Assert.assertEquals(expected.getMessage(), "expected");
        }
        infoBean.set("EMAIL", "email@email");
        infoBean.set("LOGIN", "login");
        try {
            serviceUser.requestNewPass(infoBean);
        } catch (ErreurApplicative expected) {
            Assert.fail("la méthode ne doit pas lever une exception");
        }
    }


    @Test
    public void testHandlePassRequest(@Mocked(stubOutClassInitialization = true) MySQLHelper mockedMysqlHelper) throws UnsupportedEncodingException {
        Map<String, String> result = serviceUser.handlePassRequest(null);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, Collections.<String, String>emptyMap(), "Si l'UUID est null, le résultat doit être vide");
        result = serviceUser.handlePassRequest("ebb98f7a-f37a-4388-ba96-8754139409");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, Collections.<String, String>emptyMap(), "Si l'UUID est trop court, le résultat doit être vide");
        result = serviceUser.handlePassRequest("dcdd7d80-8ed1-11e5-8994-feff819cdc9fsdfsdf");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, Collections.<String, String>emptyMap(), "Si l'UUID est trop long, le résultat doit être vide");
        final Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("motDePasse", "motDePasse");
        expectedMap.put("code", "code");
        final Map<String, String> exceptionExpectedMap = new HashMap<>();
        exceptionExpectedMap.put("motDePasse", "motDePasse2");
        exceptionExpectedMap.put("code", "code2");
        final UtilisateurBean expectedUser = new UtilisateurBean();
        expectedUser.setCode("code");
        expectedUser.setMotDePasse("");
        new Expectations() {{
            MySQLHelper.encodePassword("motDePasse");
            result = "motDePasse";
            serviceUserPass.handlePassRequest(UUID.fromString("dcdd7d80-8ed1-11e5-8994-feff819cdc9f"));
            result = expectedMap;
            serviceUserPass.handlePassRequest(UUID.fromString("987909e1-c922-4606-b0f3-2f7fb60c6b2c"));
            result = exceptionExpectedMap;
            MySQLHelper.encodePassword("motDePasse2");
            result = new UnsupportedEncodingException();
            utilisateurDAO.getByCode("code");
            result = expectedUser;
        }};
        result = serviceUser.handlePassRequest("dcdd7d80-8ed1-11e5-8994-feff819cdc9f");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertFalse(result.isEmpty(), "Si l'UUID est correct, le résultat ne doit pas être vide");
        result = serviceUser.handlePassRequest("987909e1-c922-4606-b0f3-2f7fb60c6b2c");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "Si l'UUID est correct, mais une excception est levé lors de l'encodage du mdp, le résultat st vide");
    }

    @Test
    public void testGetAdresseMail() {
        final UtilisateurBean user = new UtilisateurBean();
        user.setCode("code");
        user.setAdresseMail("foo@bar.com");
        new Expectations() {{
            utilisateurDAO.getByCode(null);
            result = null;
            utilisateurDAO.getByCode(StringUtils.EMPTY);
            result = null;
            utilisateurDAO.getByCode("code");
            result = user;
        }};
        String result = serviceUser.getAdresseMail(null);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, StringUtils.EMPTY, "le résultat doit être vide");
        result = serviceUser.getAdresseMail(StringUtils.EMPTY);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, StringUtils.EMPTY, "le résultat doit être vide");
        result = serviceUser.getAdresseMail("code");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, "foo@bar.com");
    }


    @Test
    public void testGetAdresseMailMeta() {
        final UtilisateurBean user = new UtilisateurBean();
        user.setCode("code");
        user.setAdresseMail("foo@bar.com");
        final MetatagBean meta = new MetatagBean();
        meta.setMetaMailAnonyme("foo@baz.com");
        new Expectations() {{
            utilisateurDAO.getByCode("code");
            result = user;
        }};
        String result = serviceUser.getAdresseMail(null, null);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, StringUtils.EMPTY, "le résultat doit être vide");
        result = serviceUser.getAdresseMail(StringUtils.EMPTY, new MetatagBean());
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, StringUtils.EMPTY, "le résultat doit être vide");
        result = serviceUser.getAdresseMail("code", meta);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, "foo@bar.com");
        result = serviceUser.getAdresseMail(StringUtils.EMPTY, meta);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, "foo@baz.com");
    }

    @Test
    public void testGetLibelle() {
        final UtilisateurBean user = new UtilisateurBean();
        user.setCode("code");
        user.setNom("nom");
        user.setPrenom("prenom");
        new Expectations() {{
            utilisateurDAO.getByCode(null);
            result = null;
            utilisateurDAO.getByCode(StringUtils.EMPTY);
            result = null;
            utilisateurDAO.getByCode(user.getCode());
            result = user;

        }};
        final String nullString = null;
        String result = serviceUser.getLibelle(nullString);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, StringUtils.EMPTY, "le résultat doit être vide");
        result = serviceUser.getLibelle(StringUtils.EMPTY);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, StringUtils.EMPTY, "le résultat doit être vide");
        result = serviceUser.getLibelle(user.getCode());
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, "prenom nom", "le résultat doit contenir la concaténation du prénom et du nom (dans cette ordre)");
    }

    @Test
    public void testGetLibelleUser() {
        final UtilisateurBean user = new UtilisateurBean();
        user.setCode("code");
        user.setNom("nom");
        user.setPrenom("prenom");
        final UtilisateurBean nullUser = null;
        String result = serviceUser.getLibelle(nullUser);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, StringUtils.EMPTY, "le résultat doit être vide");
        result = serviceUser.getLibelle(user);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result, "prenom nom", "le résultat doit contenir la concaténation du prénom et du nom (dans cette ordre)");
    }

    @Test
    public void testChangerMailPassword(@Mocked(stubOutClassInitialization = true) PropertyHelper propertyHelper, @Mocked(stubOutClassInitialization = true) MySQLHelper mySQLHelper) throws UnsupportedEncodingException {
        final UtilisateurBean user = new UtilisateurBean();
        user.setId(1L);
        user.setCode("code");
        user.setAdresseMail("foo@com");
        user.setMotDePasse("password");
        new Expectations() {{
            utilisateurDAO.getByCode(null);
            result = null;
            utilisateurDAO.getByCode("code");
            result = user;
            PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.max", -1);
            result = -1;
            PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.min", -1);
            result = 5;
            MySQLHelper.encodePassword("ExceptionExpected");
            result = new UnsupportedEncodingException();
        }};
        try {
            serviceUser.changerMailPassword(null, null, null);
            serviceUser.changerMailPassword("code", "foo@bar.com", null);
            serviceUser.changerMailPassword("code", "foo@bar.com", "password");
            new Verifications() {{
                utilisateurDAO.update(user);
                times = 2;
            }};
        } catch (ErreurApplicative | UnsupportedEncodingException erreurApplicative) {
            Assert.fail("aucune exception ne doit être levée dans ce test");
        }
        try {
            serviceUser.changerMailPassword("code", "foo@bar.com", "ExceptionExpected");
            Assert.fail("une exception doit être levée");
        } catch (ErreurApplicative erreurApplicative) {
            Assert.fail("l'exception ne doit pas être une erreur applicative");
        } catch (UnsupportedEncodingException e) {
            //expected
        }
        try {
            serviceUser.changerMailPassword("code", "foo@bar.com", "pass");
            Assert.fail("une exception doit être levée");
        } catch (ErreurApplicative erreurApplicative) {
            //expected
        } catch (UnsupportedEncodingException e) {
            Assert.fail("l'exception ne doit pas être une UnsupportedEncodingException");
        }
    }

    @Test
    public void testCheckPassword(@Mocked(stubOutClassInitialization = true) PropertyHelper propertyHelper) {
        new Expectations() {{
            PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.max", -1);
            result = 6;
            PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.min", -1);
            result = 6;
        }};
        try {
            serviceUser.checkPass("pass");
            Assert.fail("le mot de passe est trop court, une exception doit être levée");
        } catch (ErreurApplicative erreurApplicative) {
            // expected
        }
        try {
            serviceUser.checkPass("password");
            Assert.fail("le mot de passe est trop long, une exception doit être levée");
        } catch (ErreurApplicative erreurApplicative) {
            // expected
        }
        try {
            serviceUser.checkPass("foobar");
        } catch (ErreurApplicative erreurApplicative) {
            Assert.fail("le mot de passe est correct, aucune exception doit être levée");
        }

    }
    @Test
    public void testGetUtilisateursPossedantPermission() throws Exception {
        final PermissionBean permissionWithUser = new PermissionBean("type", "objet", "action");
        final PermissionBean permissionWithoutUser = new PermissionBean("type", "objet", "action2");
        final PermissionBean permissionWithoutRole = new PermissionBean("type", "objet", "action3");
        final RoleBean roleWithUser = new RoleBean();
        roleWithUser.setCode("codeRole");
        final RoleBean roleWithoutUser = new RoleBean();
        roleWithoutUser.setCode("codeRole2");
        final UtilisateurBean user = new UtilisateurBean();
        user.setCode("userCode");
        new Expectations() {{
            serviceRole.getByPermission(permissionWithUser.getChaineSerialisee());
            result = Collections.singletonList(roleWithUser);
            serviceRole.getByPermission(permissionWithoutUser.getChaineSerialisee());
            result = Collections.singletonList(roleWithoutUser);
            serviceRole.getByPermission(permissionWithoutRole.getChaineSerialisee());
            result = Collections.emptyList();
            serviceGroupeDsi.renvoyerGroupesEtPerimetres(roleWithUser.getCode(), Collections.<String>emptyList(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
            result = Collections.singletonMap("codeGroupe", Collections.emptyList());
            serviceGroupeUtilisateur.getAllUserCodeByGroup(Collections.singleton("codeGroupe"));
            result = Collections.singletonList("userCode");
            serviceGroupeDsi.renvoyerGroupesEtPerimetres(roleWithoutUser.getCode(), Collections.<String>emptyList(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
            result = Collections.emptyMap();
            utilisateurDAO.getByCode(user.getCode());
            result = user;
            utilisateurDAO.select(anyString);
            result = Collections.emptyList();
        }};
        List<UtilisateurBean> result = serviceUser.getUtilisateursPossedantPermission(permissionWithUser);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir un résultat");
        Assert.assertEquals(result.get(0), user, "le résultat doit correspondre à l'utilisateur userCode");
        result = serviceUser.getUtilisateursPossedantPermission(permissionWithoutUser);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "il ne doit pas y avoir de résultat");
        result = serviceUser.getUtilisateursPossedantPermission(permissionWithoutRole);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "il ne doit pas y avoir de résultat");
    }

    @Test
    public void testGetListeUtilisateursPossedantPermission(@Mocked(stubOutClassInitialization = true)InfosRolesUtils infosRolesUtils) throws Exception {
        final PermissionBean permissionWithUser = new PermissionBean("type", "objet", "action");
        final PermissionBean permissionWithoutUser = new PermissionBean("type", "objet", "action2");
        final PermissionBean permissionWithoutRole = new PermissionBean("type", "objet", "action3");
        final PermissionBean permissionWithDirectUser = new PermissionBean("type", "objet", "action4");
        final RoleBean roleWithUser = new RoleBean();
        roleWithUser.setCode("codeRole");
        final RoleBean roleWithoutUser = new RoleBean();
        roleWithoutUser.setCode("codeRole2");
        final RoleBean roleWithDirectUser = new RoleBean();
        roleWithDirectUser.setCode("codeRole3");
        final UtilisateurBean user = new UtilisateurBean();
        user.setCode("userCode");
        user.setAdresseMail("m@il");
        user.setNom("nom");
        user.setPrenom("prenom");
        final UtilisateurBean directUser = new UtilisateurBean();
        directUser.setCode("userCode3");
        directUser.setAdresseMail("m@il");
        directUser.setNom("nom");
        directUser.setPrenom("prenom");
        final List<Perimetre> rolesUtilsMock = new Vector<>();
        rolesUtilsMock.add(new Perimetre(StringUtils.EMPTY));
        new Expectations() {{
            serviceRole.getByPermission(permissionWithUser.getChaineSerialisee());
            result = Collections.singletonList(roleWithUser);
            serviceRole.getByPermission(permissionWithoutUser.getChaineSerialisee());
            result = Collections.singletonList(roleWithoutUser);
            serviceRole.getByPermission(permissionWithoutRole.getChaineSerialisee());
            result = Collections.emptyList();
            serviceRole.getByPermission(permissionWithDirectUser.getChaineSerialisee());
            result = Collections.singletonList(roleWithDirectUser);
            serviceGroupeDsi.renvoyerGroupesEtPerimetres(roleWithUser.getCode(), Collections.singletonList("codeStructure"), "codeRubrique", "publicsVises", "codeEspaceCollaboratif");
            result = Collections.singletonMap("codeGroupe", Collections.emptyList());
            serviceGroupeUtilisateur.getAllUserCodeByGroup(Collections.singleton("codeGroupe"));
            result = Collections.singletonList("userCode");
            serviceGroupeDsi.renvoyerGroupesEtPerimetres(roleWithDirectUser.getCode(), Collections.singletonList("codeStructure"), "codeRubrique", "publicsVises", "codeEspaceCollaboratif");
            result = Collections.singletonMap("codeGroupe3", Collections.emptyList());
            serviceGroupeUtilisateur.getAllUserCodeByGroup(Collections.singleton("codeGroupe3"));
            result = Collections.emptyList();
            serviceGroupeDsi.renvoyerGroupesEtPerimetres(roleWithoutUser.getCode(), Collections.<String>emptyList(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
            result = Collections.emptyMap();
            utilisateurDAO.getByCode(user.getCode());
            result = user;
            utilisateurDAO.select(" WHERE ROLES LIKE '%[codeRole;%'");
            result = Collections.emptyList();
            utilisateurDAO.select(" WHERE ROLES LIKE '%[codeRole3;%'");
            result = Collections.singletonList(directUser);
            InfosRolesUtils.renvoyerPerimetresAffectation(anyString, anyString, Collections.singletonList("codeStructure"), anyString, anyString, anyString);
            result = rolesUtilsMock;
        }};
        Collection<String> result = serviceUser.getListeUtilisateursPossedantPermission(permissionWithUser, Collections.singletonList("codeStructure"), "codeRubrique", "publicsVises", "codeEspaceCollaboratif", false);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir un résultat");
        Assert.assertEquals(result.iterator().next(), user.getCode(), "le résultat doit correspondre à l'utilisateur userCode");
        result = serviceUser.getListeUtilisateursPossedantPermission(permissionWithUser, Collections.singletonList("codeStructure"), "codeRubrique", "publicsVises", "codeEspaceCollaboratif", true);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir un résultat");
        result = serviceUser.getListeUtilisateursPossedantPermission(permissionWithDirectUser, Collections.singletonList("codeStructure"), "codeRubrique", "publicsVises", "codeEspaceCollaboratif", false);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir un résultat");
        Assert.assertEquals(result.iterator().next(), directUser.getCode(), "le résultat doit correspondre à l'utilisateur userCode");
        result = serviceUser.getListeUtilisateursPossedantPermission(permissionWithDirectUser, Collections.singletonList("codeStructure"), "codeRubrique", "publicsVises", "codeEspaceCollaboratif", true);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir un résultat");
        Assert.assertEquals(result.iterator().next(), directUser.getAdresseMail() + ";" + directUser.getNom() + " " + directUser.getPrenom() + ";" + directUser.getCode(), "le résultat doit correspondre à l'utilisateur userCode");
        result = serviceUser.getListeUtilisateursPossedantPermission(permissionWithoutUser, Collections.<String>emptyList(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "il ne doit pas y avoir de résultat");
        result = serviceUser.getListeUtilisateursPossedantPermission(permissionWithoutRole, Collections.<String>emptyList(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "il ne doit pas y avoir de résultat");
    }

    @Test
    public void testIsAlterationForbiddenBySource(@Mocked(stubOutClassInitialization = true) PropertyHelper mockedPropertyHelper) {
        UtilisateurBean userFromLdap = new UtilisateurBean();
        userFromLdap.setCode("userCode");
        userFromLdap.setCodeLdap("codeLdap");
        new Expectations() {{
            PropertyHelper.getCoreProperty("authentification.source");
            result = "ctrust";
        }};
        Assert.assertTrue(serviceUser.isAlterationForbiddenBySource(userFromLdap), "l'utilisateur est importé de cleartrust");
        Assert.assertFalse(serviceUser.isAlterationForbiddenBySource(new UtilisateurBean()), "l'utilisateur n'est pas importé d'une source externe");
        Assert.assertFalse(serviceUser.isAlterationForbiddenBySource(null), "l'utilisateur est null");
    }

    @Test
    public void testFromLdap() {
        UtilisateurBean userFromLdap = new UtilisateurBean();
        userFromLdap.setCode("userCode");
        userFromLdap.setCodeLdap("codeLdap");
        Assert.assertTrue(serviceUser.fromLdap(userFromLdap));
        Assert.assertFalse(serviceUser.fromLdap(new UtilisateurBean()));
        Assert.assertFalse(serviceUser.fromLdap(null));
    }

    @Test
    public void testGetCentresInteret() {
        UtilisateurBean user = new UtilisateurBean();
        user.setCentresInteret("foo;bar;baz");
        Collection<String> result = serviceUser.getCentresInteret(user);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 3, "il doit y avoir 3 résultats");
        result = serviceUser.getCentresInteret(new UtilisateurBean());
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
        result = serviceUser.getCentresInteret(null);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByCode() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setCode("TEST_GETBYCODE");
        new Expectations() {{
            utilisateurDAO.getByCode("TEST_GETBYCODE");
            result = userBean;
            utilisateurDAO.getByCode("NO_RESULT");
            result = null;
        }};
        final UtilisateurBean userByCode = serviceUser.getByCode("TEST_GETBYCODE");
        Assert.assertNotNull(userByCode, "Le premier appel doit retourner un utilisateur");
        Assert.assertEquals(userByCode.getCode(), userBean.getCode(), "Le premier appel doit renvoyer l'utilisateur ayant le code TEST_GETBYCODE");
        final UtilisateurBean userByCode2 = serviceUser.getByCode("NO_RESULT");
        Assert.assertEquals(userByCode2, null, "le second appel doit être null");
    }

    @Test
    public void testGetByCodeLdap() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setCodeLdap("TEST_GETBYCODELDAP");
        new Expectations() {{
            utilisateurDAO.getByCodeLdap("TEST_GETBYCODELDAP");
            result = userBean;
            utilisateurDAO.getByCodeLdap("NO_RESULT");
            result = null;
        }};
        final UtilisateurBean userByCode = serviceUser.getByCodeLdap("TEST_GETBYCODELDAP");
        Assert.assertNotNull(userByCode, "Le premier appel doit retourner un utilisateur");
        Assert.assertEquals(userByCode.getCodeLdap(), userBean.getCodeLdap(), "Le premier appel doit renvoyer l'utilisateur ayant le code ldap TEST_GETBYCODELDAP");
        final UtilisateurBean userByCode2 = serviceUser.getByCodeLdap("NO_RESULT");
        Assert.assertEquals(userByCode2, null, "le second appel doit être vide");
    }

    @Test
    public void testGetByNom() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setNom("TEST_GETBYNOM");
        new Expectations() {{
            utilisateurDAO.getByNom("TEST_GETBYNOM");
            result = Collections.singletonList(userBean);
            utilisateurDAO.getByNom("NO_RESULT");
            result = Collections.emptyList();
        }};
        final List<UtilisateurBean> usersByName = serviceUser.getByNom("TEST_GETBYNOM");
        Assert.assertNotNull(usersByName, "Le résultat ne doit pas être null");
        Assert.assertFalse(usersByName.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(usersByName.get(0).getNom(), userBean.getNom(), "Le premier appel doit renvoyer l'utilisateur ayant le nom TEST_GETBYNOM");
        final List<UtilisateurBean> userByName2 = serviceUser.getByNom("NO_RESULT");
        Assert.assertNotNull(userByName2, "Le résultat ne doit pas être null");
        Assert.assertTrue(userByName2.isEmpty(), "le second appel doit être une liste vide");
    }

    @Test
    public void testGetByMailAndCode() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setCode("TEST_GETBYMAILANDCODE");
        userBean.setAdresseMail("m@il");
        new Expectations() {{
            utilisateurDAO.getByMailAndCode(userBean.getCode(), userBean.getAdresseMail());
            result = Collections.singletonList(userBean);
            utilisateurDAO.getByMailAndCode("NO_RESULT", "NO_RESULT");
            result = Collections.emptyList();
        }};
        final List<UtilisateurBean> usersByMailAndCode = serviceUser.getByMailAndCode(userBean.getCode(), userBean.getAdresseMail());
        Assert.assertNotNull(usersByMailAndCode, "Le résultat ne doit pas être null");
        Assert.assertFalse(usersByMailAndCode.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(usersByMailAndCode.get(0).getCode(), userBean.getCode(), "Le premier appel doit renvoyer l'utilisateur ayant le code TEST_GETBYMAILANDCODE");
        final List<UtilisateurBean> usersByMailAndCode2 = serviceUser.getByMailAndCode("NO_RESULT", "NO_RESULT");
        Assert.assertNotNull(usersByMailAndCode2, "Le résultat ne doit pas être null");
        Assert.assertTrue(usersByMailAndCode2.isEmpty(), "le second appel doit être une liste vide");
    }

    @Test
    public void testGetUser() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setAdresseMail("email");
        userBean.setMotDePasse("pass");
        userBean.setCode("code");
        new Expectations() {{
            utilisateurDAO.getUser(userBean.getAdresseMail(), userBean.getMotDePasse(), userBean.getCode());
            result = userBean;
            utilisateurDAO.getUser("NO_RESULT", "NO_RESULT", "NO_RESULT");
            result = null;
        }};
        final UtilisateurBean userByCode = serviceUser.getUser(userBean.getAdresseMail(), userBean.getMotDePasse(), userBean.getCode());
        Assert.assertNotNull(userByCode, "Le premier appel doit retourner un utilisateur");
        Assert.assertEquals(userByCode.getCode(), userBean.getCode(), "Le premier appel doit renvoyer l'utilisateur ayant le code 'code'");
        final UtilisateurBean userByCode2 = serviceUser.getUser("NO_RESULT", "NO_RESULT", "NO_RESULT");
        Assert.assertEquals(userByCode2, null, "le second appel doit être vide");
    }

    @Test
    public void testAppartientAGroupeOuSousGroupes() {
        final GroupeDsiBean rootGroup = new GroupeDsiBean();
        rootGroup.setCode("TEST_APPARTIENTAGROUPEOUSOUSGROUPES");
        final GroupeDsiBean subGroup = new GroupeDsiBean();
        subGroup.setCode("TEST_APPARTIENTAGROUPEOUSOUSGROUPES2");
        final GroupeDsiBean noSubGroup = new GroupeDsiBean();
        noSubGroup.setCode("TEST_APPARTIENTAGROUPEOUSOUSGROUPES3");
        new Expectations() {{
            serviceGroupeDsi.getByCode("TEST_APPARTIENTAGROUPEOUSOUSGROUPES");
            result = rootGroup;
            serviceGroupeDsi.getByCode("TEST_APPARTIENTAGROUPEOUSOUSGROUPES2");
            result = subGroup;
            serviceGroupeDsi.getByCode("TEST_APPARTIENTAGROUPEOUSOUSGROUPES3");
            result = noSubGroup;
            serviceGroupeDsi.contains(rootGroup, subGroup);
            result = true;
            serviceGroupeDsi.contains(rootGroup, noSubGroup);
            result = false;
        }};
        Assert.assertTrue(serviceUser.appartientAGroupeOuSousGroupes(rootGroup.getCode(), Collections.singletonList(subGroup.getCode())), "le sous groupe appartient bien au groupe fourni");
        Assert.assertFalse(serviceUser.appartientAGroupeOuSousGroupes(rootGroup.getCode(), Collections.singletonList(noSubGroup.getCode())), "le sous groupe n'appartient pas au groupe fourni");
        Assert.assertFalse(serviceUser.appartientAGroupeOuSousGroupes(rootGroup.getCode(), Collections.<String>emptyList()), "Sans sous groupe, le résultat est forcément faux");
    }

    @Test
    public void testGetByCodeLdapSource() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setCodeLdap("TEST_GETBYCODELDAPSOURCE");
        userBean.setSourceImport("sourceImport");
        new Expectations() {{
            utilisateurDAO.getByCodeLdap("TEST_GETBYCODELDAPSOURCE", "sourceImport");
            result = userBean;
            utilisateurDAO.getByCodeLdap("NO_RESULT", "NO_RESULT");
            result = null;
        }};
        final UtilisateurBean userByCode = serviceUser.getByCodeLdap("TEST_GETBYCODELDAPSOURCE", "sourceImport");
        Assert.assertNotNull(userByCode, "Le premier appel doit retourner un utilisateur");
        Assert.assertEquals(userByCode.getCodeLdap(), userBean.getCodeLdap(), "Le premier appel doit renvoyer l'utilisateur ayant le code ldap TEST_GETBYCODELDAPSOURCE");
        final UtilisateurBean userByCode2 = serviceUser.getByCodeLdap("NO_RESULT", "NO_RESULT");
        Assert.assertEquals(userByCode2, null, "le second appel doit être vide");
    }

    @Test
    public void testGetByCodeAndPass() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setCode("TEST_GETBYCODEANDPASS");
        userBean.setMotDePasse("pass");
        new Expectations() {{
            utilisateurDAO.getByCodeAndPass(userBean.getCode(), userBean.getMotDePasse());
            result = userBean;
            utilisateurDAO.getByCodeAndPass("NO_RESULT", "NO_RESULT");
            result = null;
        }};
        final UtilisateurBean userByCode = serviceUser.getByCodeAndPass(userBean.getCode(), userBean.getMotDePasse());
        Assert.assertNotNull(userByCode, "Le premier appel doit retourner un utilisateur");
        Assert.assertEquals(userByCode.getCodeLdap(), userBean.getCodeLdap(), "Le premier appel doit renvoyer l'utilisateur ayant le code TEST_GETBYCODEANDPASS");
        final UtilisateurBean userByCode2 = serviceUser.getByCodeAndPass("NO_RESULT", "NO_RESULT");
        Assert.assertEquals(userByCode2, null, "le second appel doit être vide");
    }

    @Test
    public void testGetByCodes() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setCode("TEST_GETBYCODES");
        new Expectations() {{
            utilisateurDAO.getByCodes(Collections.singletonList(userBean.getCode()));
            result = Collections.singletonList(userBean);
            utilisateurDAO.getByCodes(Collections.singletonList("NO_RESULT"));
            result = Collections.emptyList();
        }};
        final List<UtilisateurBean> usersByCode = serviceUser.getByCodes(userBean.getCode());
        Assert.assertNotNull(usersByCode, "Le résultat ne doit pas être null");
        Assert.assertFalse(usersByCode.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(usersByCode.get(0).getCode(), userBean.getCode(), "Le premier appel doit renvoyer l'utilisateur ayant le code TEST_GETBYCODES");
        final List<UtilisateurBean> usersByCode2 = serviceUser.getByCodes("NO_RESULT");
        Assert.assertNotNull(usersByCode2, "Le résultat ne doit pas être null");
        Assert.assertTrue(usersByCode2.isEmpty(), "le second appel doit être une liste vide");
        final List<UtilisateurBean> usersByCode3 = serviceUser.getByCodes();
        Assert.assertNotNull(usersByCode3, "Le résultat ne doit pas être null");
        Assert.assertTrue(usersByCode3.isEmpty(), "le troisième appel doit être une liste vide");
    }

    @Test
    public void testGetAllUsers() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setCode("TEST_GETALLUSERS");
        new Expectations() {{
            utilisateurDAO.getAllUsers();
            result = Collections.singletonList(userBean);
        }};
        final List<UtilisateurBean> allUsers = serviceUser.getAllUsers();
        Assert.assertNotNull(allUsers, "Le résultat ne doit pas être null");
        Assert.assertFalse(allUsers.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(allUsers.get(0).getCode(), userBean.getCode(), "L'appel doit renvoyer l'utilisateur ayant le code TEST_GETALLUSERS");
    }

    @Test
    public void testGetByImportSource() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setSourceImport("TEST_GETBYIMPORTSOURCE");
        new Expectations() {{
            utilisateurDAO.getByImportSource(userBean.getSourceImport());
            result = Collections.singletonList(userBean);
            utilisateurDAO.getByImportSource("NO_RESULT");
            result = Collections.emptyList();
        }};
        final List<UtilisateurBean> usersByImportSource = serviceUser.getByImportSource(userBean.getSourceImport());
        Assert.assertNotNull(usersByImportSource, "Le résultat ne doit pas être null");
        Assert.assertFalse(usersByImportSource.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(usersByImportSource.get(0).getSourceImport(), userBean.getSourceImport(), "Le premier appel doit renvoyer l'utilisateur ayant la source d'import TEST_GETBYIMPORTSOURCE");
        final List<UtilisateurBean> usersByImportSource2 = serviceUser.getByImportSource("NO_RESULT");
        Assert.assertNotNull(usersByImportSource2, "Le résultat ne doit pas être null");
        Assert.assertTrue(usersByImportSource2.isEmpty(), "le second appel doit être une liste vide");
    }

    @Test
    public void testSelect() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setCode("TEST_SELECT");
        userBean.setNom("NOM");
        userBean.setPrenom("PRENOM");
        userBean.setAdresseMail("m@il");
        userBean.setCodeRattachement("CODE_RATTACHEMENT");
        userBean.setProfilDefaut("PROFIL");
        userBean.setGroupesDsi("GROUPEDSI");
        new Expectations() {{
            utilisateurDAO.select(userBean.getCode(), userBean.getNom(), userBean.getPrenom(), StringUtils.EMPTY, userBean.getProfilDefaut(), userBean.getGroupesDsi(), userBean.getCodeRattachement(), userBean.getAdresseMail());
            result = Collections.singletonList(userBean);
            utilisateurDAO.select("NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT");
            result = Collections.emptyList();
        }};
        final List<UtilisateurBean> usersSelect = serviceUser.select(userBean.getCode(), userBean.getNom(), userBean.getPrenom(), StringUtils.EMPTY, userBean.getProfilDefaut(), userBean.getGroupesDsi(), userBean.getCodeRattachement(), userBean.getAdresseMail());
        Assert.assertNotNull(usersSelect, "Le résultat ne doit pas être null");
        Assert.assertFalse(usersSelect.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(usersSelect.get(0).getCode(), userBean.getCode(), "Le premier appel doit renvoyer l'utilisateur ayant le code TEST_SELECT");
        final List<UtilisateurBean> usersSelect2 = serviceUser.select("NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT", "NO_RESULT");
        Assert.assertNotNull(usersSelect2, "Le résultat ne doit pas être null");
        Assert.assertTrue(usersSelect2.isEmpty(), "le second appel doit être une liste vide");
    }

    @Test
    public void testGetByChaineDiffusion() {
        final UtilisateurBean userBean = new UtilisateurBean();
        userBean.setCode("GROUPES_DSI");
        new Expectations() {{
            utilisateurDAO.getByChaineDiffusion("[/GROUPES_DSI]");
            result = Collections.singletonList(userBean);
            utilisateurDAO.getByChaineDiffusion("NO_RESULT");
            result = Collections.emptyList();
        }};
        final List<UtilisateurBean> usersByChaineDiffusion = serviceUser.getByChaineDiffusion("[/GROUPES_DSI]");
        Assert.assertNotNull(usersByChaineDiffusion, "Le résultat ne doit pas être null");
        Assert.assertFalse(usersByChaineDiffusion.isEmpty(), "le résultat ne doit pas être vide");
        Assert.assertEquals(usersByChaineDiffusion.get(0).getCode(), userBean.getCode(), "Le premier appel doit renvoyer l'utilisateur ayant le code GROUPES_DSI");
        final List<UtilisateurBean> usersByChaineDiffusion2 = serviceUser.getByChaineDiffusion("NO_RESULT");
        Assert.assertNotNull(usersByChaineDiffusion2, "Le résultat ne doit pas être null");
        Assert.assertTrue(usersByChaineDiffusion2.isEmpty(), "le second appel doit être une liste vide");
    }
}
