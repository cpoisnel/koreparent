package com.univ.objetspartages.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kosmos.tests.testng.AbstractCacheTestngTests;
import com.kosmos.tests.testng.annotations.ClearCache;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.dao.impl.ProfildsiDAO;

import mockit.Expectations;
import mockit.Invocation;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.Verifications;

/**
 * Created on 27/05/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/objetspartages/services/ServiceProfilDsiTest.test-context.xml"})
public class ServiceProfilDsiTest extends AbstractCacheTestngTests {

    @Autowired
    ServiceProfildsi serviceProfildsi;

    @Test
    public void testExistCode(@Mocked("getByCode") final ServiceProfildsi mockedServiceProfilDsi) {
        new Expectations(){{
            serviceProfildsi.getByCode(anyString);
            result = null;
        }};
        Assert.assertFalse(serviceProfildsi.existCode("PLOP"));
        new Expectations(){{
            serviceProfildsi.getByCode(anyString);
            result = new ProfildsiBean();
        }};
        Assert.assertTrue(serviceProfildsi.existCode("PLOP"));
    }

    @Test
    public void testSave(@Mocked({"add", "update"}) final ProfildsiDAO dao1) {
        final ProfildsiDAO profildsiDAO = new ProfildsiDAO();
        serviceProfildsi.setDao(profildsiDAO);
        final ProfildsiBean profil = new ProfildsiBean();
        profil.setCode("PROFIL");
        serviceProfildsi.save(profil);
        profil.setId(10L);
        serviceProfildsi.save(profil);
    }

    @Test
    public void testDelete(@Mocked("delete") final ProfildsiDAO dao1, @Mocked("getById") final ServiceProfildsi mockedServiceProfilDsi) {
        final ProfildsiDAO profildsiDAO = new ProfildsiDAO();
        final ProfildsiBean profil = new ProfildsiBean();
        profil.setCode("PROFIL");
        profil.setId(10L);
        new Expectations(){{
            serviceProfildsi.getById(10L);
            result = profil;
        }};
        serviceProfildsi.setDao(profildsiDAO);
        serviceProfildsi.delete(10L);
    }

    @Test
    @ClearCache(names = "getByCode")
    public void testGetByCode(@Mocked("getByCode") final ProfildsiDAO dao1) throws ParseException {
        final ProfildsiBean profildsiBean1 = new ProfildsiBean();
        profildsiBean1.setCode("TEST_GETBYCODE");
        profildsiBean1.setLibelle("Profil 1");
        final ProfildsiDAO profildsiDAO = new ProfildsiDAO();
        serviceProfildsi.setDao(profildsiDAO);
        new Expectations() {{
            profildsiDAO.getByCode(anyString);
            result = profildsiBean1;
        }};
        final ProfildsiBean profildsiByCode = serviceProfildsi.getByCode("TEST_GETBYCODE");
        Assert.assertNotNull(profildsiByCode, "Le premier appel doit retourner un profil");
        Assert.assertTrue(profildsiByCode.getCode().equals(profildsiBean1.getCode()), "Le premier appel doit renvoyer le profil portant le code TEST_GETBYCODE");
        serviceProfildsi.getByCode("TEST_GETBYCODE");
        new Verifications() {{
            profildsiDAO.getByCode(anyString);
            times = 1;
        }};
    }

    @Test
    public void testGetAll(@Mocked("selectAll") final ProfildsiDAO dao) {
        final ProfildsiDAO profildsiDAO = new ProfildsiDAO();
        serviceProfildsi.setDao(profildsiDAO);
        new Expectations() {{
            profildsiDAO.selectAll();
            result = Collections.EMPTY_LIST;
        }};
        final List<ProfildsiBean> result1 = serviceProfildsi.getAll();
        Assert.assertNotNull(result1, "La liste retournée ne devrait pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(result1), "La liste retournée devrait être vide");
        final ProfildsiBean profil = new ProfildsiBean();
        profil.setCode("PROFIL1");
        profil.setLibelle("Profil 1");
        final List<ProfildsiBean> profils = new ArrayList<>();
        profils.add(profil);
        new Expectations() {{
            profildsiDAO.selectAll();
            result = profils;
        }};
        final List<ProfildsiBean> result2 = serviceProfildsi.getAll();
        Assert.assertNotNull(result2, "La liste retournée ne devrait pas être null");
        Assert.assertTrue(CollectionUtils.isNotEmpty(result2), "La liste retournée ne devrait pas être vide");
        Assert.assertTrue(result2.get(0).getCode().equals("PROFIL1"), "La liste retournée devrait contenir le profil ayant pour code \"PROFIL1\"");
    }

    @Test
    public void testGetDisplayableProfiles(@Mocked("getAll") final ServiceProfildsi mockedServiceProfilDsi) {
        new Expectations() {{
            serviceProfildsi.getAll();
            result = Collections.EMPTY_LIST;
        }};
        final Map<String, String> result1 = serviceProfildsi.getDisplayableProfiles();
        Assert.assertNotNull(result1, "La map retournée ne devrait pas être null");
        Assert.assertTrue(MapUtils.isEmpty(result1), "La map retournée devrait être vide");

        final ProfildsiBean profil = new ProfildsiBean();
        profil.setCode("PROFIL1");
        profil.setLibelle("Profil 1");
        final List<ProfildsiBean> profils = new ArrayList<>();
        profils.add(profil);
        new Expectations() {{
            serviceProfildsi.getAll();
            result = profils;
        }};
        final Map<String, String> result2 = serviceProfildsi.getDisplayableProfiles();
        Assert.assertNotNull(result2, "La map retournée ne devrait pas être null");
        Assert.assertTrue(MapUtils.isNotEmpty(result2), "La map retournée ne devrait pas être vide");
        Assert.assertTrue(result2.containsKey("PROFIL1"), "La map retournée devrait contenir la clé \"PROFIL1\"");
        Assert.assertEquals(result2.get("PROFIL1"), "Profil 1", "La map retournée devrait contenir la valeur \"Profil 1\"");
    }

    @Test
    public void testGetDisplayableLabel(@Mocked("getByCode") final ServiceProfildsi mockedServiceProfilDsi) {
        new MockUp<MessageHelper>() {
            @Mock
            public String getCoreMessage(Invocation context, String key) {
                return "Profil inexistant";
            }
        };
        Assert.assertEquals(serviceProfildsi.getDisplayableLabel(StringUtils.EMPTY), "Profil inexistant", "La chaîne retournée devrait être vide");
        new Expectations(){{
            serviceProfildsi.getByCode("PLOP");
            result = null;
        }};
        Assert.assertEquals(serviceProfildsi.getDisplayableLabel("PLOP"), "Profil inexistant", "La chaîne retournée devrait être vide");
        final ProfildsiBean profil = new ProfildsiBean();
        profil.setCode("PLOP");
        profil.setLibelle("Plop");
        new Expectations(){{
            serviceProfildsi.getByCode("PLOP");
            result = profil;
        }};
        final String label = serviceProfildsi.getDisplayableLabel("PLOP");
        Assert.assertTrue(StringUtils.isNotBlank(label), "La chaîne retournée ne doit pas être vide");
        Assert.assertEquals(label, "Plop", "La chaîne retournée doit être \"Plop\"");
    }

    @Test
    public void testGetListeProfilsDSIParGroupes(@Mocked({"renvoyerProfilsGroupes", "getByCode"}) final ServiceProfildsi mockedServiceProfilDsi) {
        new Expectations(){{
            serviceProfildsi.renvoyerProfilsGroupes(withAny(new ArrayList<String>()));
            result = Collections.EMPTY_LIST;
        }};
        final Map<String, String> result1 = serviceProfildsi.getListeProfilsDSIParGroupes(new ArrayList<String>());
        Assert.assertNotNull(result1, "La map retournée ne devrait pas être null");
        Assert.assertTrue(MapUtils.isEmpty(result1), "La map retournée devrait être vide");

        final ProfildsiBean profildsi1 = new ProfildsiBean();
        profildsi1.setCode("PROFIL1");
        profildsi1.setLibelle("Profil1");
        final ProfildsiBean profildsi2 = new ProfildsiBean();
        profildsi2.setCode("PROFIL2");
        profildsi2.setLibelle("Profil2");
        final ProfildsiBean profildsi3 = new ProfildsiBean();
        profildsi3.setCode("PROFIL3");
        new Expectations(){{
            serviceProfildsi.renvoyerProfilsGroupes(withAny(new ArrayList<String>()));
            result = Arrays.asList("PROFIL1", "PROFIL2", "PROFIL3");
            serviceProfildsi.getByCode("PROFIL1");
            result = profildsi1;
            serviceProfildsi.getByCode("PROFIL2");
            result = profildsi2;
            serviceProfildsi.getByCode("PROFIL3");
            result = profildsi3;
        }};
        final Map<String, String> result2 = serviceProfildsi.getListeProfilsDSIParGroupes(Arrays.asList("Plop"));
        Assert.assertNotNull(result2, "La map retournée ne devrait pas être null");
        Assert.assertTrue(MapUtils.isNotEmpty(result2), "La map retournée ne devrait pas être vide");
        Assert.assertTrue(result2.size() == 2, "La map retournée devrait contenir 2 éléments");
        Assert.assertTrue(result2.containsKey("PROFIL1"), "La map retournée devrait contenir la clé \"PROFIL1\"");
        Assert.assertTrue(result2.containsKey("PROFIL2"), "La map retournée devrait contenir la clé \"PROFIL2\"");
        Assert.assertFalse(result2.containsKey("PROFIL3"), "La map retournée devrait contenir la clé \"PROFIL3\"");
    }

    @Test
    public void testRenvoyerProfilsGroupes(@Mocked("getAll") final ServiceProfildsi mockedServiceProfilDsi, @Mocked final ServiceGroupeDsi mockedServicegroupeDsi) {
        new Expectations() {{
            serviceProfildsi.getAll();
            result = Collections.EMPTY_LIST;
        }};
        final List<String> result1 = serviceProfildsi.renvoyerProfilsGroupes(new ArrayList<String>());
        Assert.assertNotNull(result1, "La liste renvoyée ne devrait pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(result1), "La liste renvoyée devrait être vide");
        final ServiceGroupeDsi serviceGroupeDsi = new ServiceGroupeDsi();
        serviceProfildsi.setServiceGroupeDsi(serviceGroupeDsi);
        final ProfildsiBean profildsi = new ProfildsiBean();
        profildsi.setCode("PROFIL1");
        profildsi.setGroupes(Arrays.asList("GROUPE1"));
        final GroupeDsiBean groupe = new GroupeDsiBean();
        groupe.setCode("GROUPE1");
        new Expectations() {{
            serviceProfildsi.getAll();
            result = Arrays.asList(profildsi);
            serviceGroupeDsi.getByCode("GROUPE1");
            result = groupe;
            serviceGroupeDsi.contains(withAny(groupe), withAny(groupe));
            result = true;
        }};
        final List<String> result2 = serviceProfildsi.renvoyerProfilsGroupes(Arrays.asList("GROUPE1"));
        Assert.assertNotNull(result2, "La liste retournée ne doit pas être null");
        Assert.assertTrue(result2.size() == 1, "La liste retournée doit contenir 1 élément");
        Assert.assertEquals(result2.get(0), "PROFIL1", "La liste retournée doit contenir 1 élément");
        new Expectations() {{
            serviceProfildsi.getAll();
            result = Arrays.asList(profildsi);
            serviceGroupeDsi.getByCode("GROUPE1");
            result = groupe;
            serviceGroupeDsi.contains(withAny(groupe), withAny(groupe));
            result = false;
        }};
        final List<String> result3 = serviceProfildsi.renvoyerProfilsGroupes(Arrays.asList("GROUPE1"));
        Assert.assertNotNull(result3, "La liste retournée ne doit pas être null");
        Assert.assertTrue(CollectionUtils.isEmpty(result3), "La liste retournée doit être vide");
    }
}
