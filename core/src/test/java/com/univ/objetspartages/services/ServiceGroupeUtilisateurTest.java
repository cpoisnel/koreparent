package com.univ.objetspartages.services;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.dao.impl.GroupeUtilisateurDAO;
import com.univ.objetspartages.om.InfosRequeteGroupe;
import com.univ.utils.RequeteGroupeUtil;

import mockit.Expectations;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

/**
 * Created by olivier.camon on 29/10/15.
 */

@Test
@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/services/ServiceGroupeUtilisateurTest.test-context.xml"})
public class ServiceGroupeUtilisateurTest extends AbstractTestNGSpringContextTests {

    @Autowired
    ServiceGroupeUtilisateur serviceGroupeUtilisateur;

    @Mocked
    GroupeUtilisateurDAO groupeUtilisateurDAO;

    @Mocked
    ServiceGroupeDsi serviceGroupeDsi;

    @BeforeClass
    public void initMock(){
        groupeUtilisateurDAO = new GroupeUtilisateurDAO();
        serviceGroupeDsi = new ServiceGroupeDsi();
        serviceGroupeUtilisateur.setDao(groupeUtilisateurDAO);
        serviceGroupeUtilisateur.setServiceGroupeDsi(serviceGroupeDsi);
    }

    @Test
    public void testDeleteByGroup() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        new Expectations() {{
            groupeUtilisateurDAO.add(groupeUtilisateurBean);
            result = groupeUtilisateurBean;
            groupeUtilisateurDAO.selectByGroupCode("codeGroupe");
            result = Collections.<GroupeUtilisateurBean>emptyList();
        }};
        serviceGroupeUtilisateur.save(groupeUtilisateurBean);
        serviceGroupeUtilisateur.deleteByGroup("codeGroupe");
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByGroupCode("codeGroupe");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être une liste vide");
    }

    @Test
    public void testDeleteByUser() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        new Expectations() {{
            groupeUtilisateurDAO.add(groupeUtilisateurBean);
            result = groupeUtilisateurBean;
            groupeUtilisateurDAO.selectByUserCode("codeUtilisateur");
            result = Collections.<GroupeUtilisateurBean>emptyList();
        }};
        serviceGroupeUtilisateur.save(groupeUtilisateurBean);
        serviceGroupeUtilisateur.deleteByUser("codeUtilisateur");
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByUserCode("codeUtilisateur");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être une liste vide");
    }

    @Test
    public void testDeleteByUserAndGroup() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        new Expectations() {{
            groupeUtilisateurDAO.add(groupeUtilisateurBean);
            result = groupeUtilisateurBean;
            groupeUtilisateurDAO.selectByGroupCode("codeGroupe");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByUserCode("codeUtilisateur");
            result = Collections.<GroupeUtilisateurBean>emptyList();
        }};
        serviceGroupeUtilisateur.save(groupeUtilisateurBean);
        serviceGroupeUtilisateur.deleteByUserAndGroup("codeUtilisateur", "codeGroupe");
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByGroupCode("codeGroupe");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être une liste vide");
        result = serviceGroupeUtilisateur.getByUserCode("codeUtilisateur");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être une liste vide");
    }

    @Test
    public void testDeleteByGroupAndImportSource() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        groupeUtilisateurBean.setSourceImport("sourceImport");
        new Expectations() {{
            groupeUtilisateurDAO.add(groupeUtilisateurBean);
            result = groupeUtilisateurBean;
            groupeUtilisateurDAO.selectByGroupCode("codeGroupe");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByImportSource("sourceImport");
            result = Collections.<GroupeUtilisateurBean>emptyList();

        }};
        serviceGroupeUtilisateur.save(groupeUtilisateurBean);
        serviceGroupeUtilisateur.deleteByGroupAndImportSource("codeGroupe", "sourceImport");
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByGroupCode("codeGroupe");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être une liste vide");
        result = serviceGroupeUtilisateur.getByImportSource("sourceImport");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être une liste vide");
    }

    @Test
    public void testDeleteByUserAndImportSource() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        groupeUtilisateurBean.setSourceImport("sourceImport");
        new Expectations() {{
            groupeUtilisateurDAO.add(groupeUtilisateurBean);
            result = groupeUtilisateurBean;
            groupeUtilisateurDAO.selectByUserCode("codeUtilisateur");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByImportSource("sourceImport");
            result = Collections.<GroupeUtilisateurBean>emptyList();

        }};
        serviceGroupeUtilisateur.save(groupeUtilisateurBean);
        serviceGroupeUtilisateur.deleteByUserAndImportSource("codeUtilisateur", "sourceImport");
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByUserCode("codeUtilisateur");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être une liste vide");
        result = serviceGroupeUtilisateur.getByImportSource("sourceImport");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être une liste vide");
    }

    @Test
    public void testGetByUserCode() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserCode("noResult");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByUserCode("codeUtilisateur");
            result = Collections.singletonList(groupeUtilisateurBean);
        }};
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByUserCode("codeUtilisateur");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.get(0), groupeUtilisateurBean, "le résultat doit correspondre au groupe utilisateur ayant pour code utilisateur: codeUtilisateur");
        result = serviceGroupeUtilisateur.getByUserCode("noResult");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByImportSource() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setSourceImport("sourceImport");
        new Expectations() {{
            groupeUtilisateurDAO.selectByImportSource("noResult");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByImportSource("sourceImport");
            result = Collections.singletonList(groupeUtilisateurBean);
        }};
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByImportSource("sourceImport");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.get(0), groupeUtilisateurBean, "le résultat doit correspondre au groupe utilisateur ayant pour source d'import : sourceImport");
        result = serviceGroupeUtilisateur.getByImportSource("noResult");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByUserCodeAndImportSource() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setSourceImport("sourceImport");
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserCodeAndImportSource("noResult", "noResult");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByUserCodeAndImportSource("codeUtilisateur", "sourceImport");
            result = Collections.singletonList(groupeUtilisateurBean);
        }};
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByUserCodeAndImportSource("codeUtilisateur", "sourceImport");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.get(0), groupeUtilisateurBean, "le résultat doit correspondre au groupe utilisateur ayant pour source d'import : sourceImport et code utilisateur : codeUtilisateur");
        result = serviceGroupeUtilisateur.getByUserCodeAndImportSource("noResult", "noResult");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }


    @Test
    public void testGetByUserNotInGroupCodeAndImportSource() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        groupeUtilisateurBean.setSourceImport("sourceImport");
        final GroupeUtilisateurBean notInFirstResult = new GroupeUtilisateurBean();
        notInFirstResult.setCodeGroupe("noResult");
        notInFirstResult.setCodeUtilisateur("codeUtilisateur");
        notInFirstResult.setSourceImport("sourceImport");
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserNotInGroupCodeAndImportSource("codeUtilisateur", Collections.singletonList("noResult"), "sourceImport");
            result = Collections.singletonList(groupeUtilisateurBean);
            groupeUtilisateurDAO.selectByUserNotInGroupCodeAndImportSource("codeUtilisateur", Collections.<String>emptyList(), "sourceImport");
            result = Arrays.asList(groupeUtilisateurBean, notInFirstResult);
        }};
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByUserNotInGroupCodeAndImportSource("codeUtilisateur", Collections.singletonList("noResult"),
            "sourceImport");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.get(0), groupeUtilisateurBean, "le résultat doit correspondre au groupe utilisateur ayant comme code groupe : codeGroupe");
        result = serviceGroupeUtilisateur.getByUserNotInGroupCodeAndImportSource("codeUtilisateur", Collections.<String>emptyList(), "sourceImport");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 2, "le résultat doit contenir deux elements");
    }

    @Test
    public void testGetByGroupCodeAndImportSource() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        groupeUtilisateurBean.setSourceImport("sourceImport");
        new Expectations() {{
            groupeUtilisateurDAO.selectByGroupCodeAndImportSource("noResult", "noResult");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByGroupCodeAndImportSource("codeGroupe", "sourceImport");
            result = Collections.singletonList(groupeUtilisateurBean);
        }};
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByGroupCodeAndImportSource("codeGroupe", "sourceImport");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.get(0), groupeUtilisateurBean, "le résultat doit correspondre au groupe utilisateur ayant pour source d'import : sourceImport et code groupe : codeGroupe");
        result = serviceGroupeUtilisateur.getByGroupCodeAndImportSource("noResult", "noResult");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByUserCodeAndGroupCode() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserAndGroup("noResult", "noResult");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe");
            result = Collections.singletonList(groupeUtilisateurBean);
        }};
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByUserCodeAndGroupCode("codeUtilisateur", "codeGroupe");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.get(0), groupeUtilisateurBean, "le résultat doit correspondre au groupe utilisateur ayant pour code utilisateur codeUtilisateur et code groupe : codeGroupe");
        result = serviceGroupeUtilisateur.getByUserCodeAndGroupCode("noResult", "noResult");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetByGroupCode() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        new Expectations() {{
            groupeUtilisateurDAO.selectByGroupCode("noResult");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByGroupCode("codeGroupe");
            result = Collections.singletonList(groupeUtilisateurBean);
        }};
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getByGroupCode("codeGroupe");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.get(0), groupeUtilisateurBean, "le résultat doit correspondre au groupe utilisateur ayant pour code utilisateur codeUtilisateur et code groupe : codeGroupe");
        result = serviceGroupeUtilisateur.getByGroupCode("noResult");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetInGroupCode() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        final GroupeUtilisateurBean notInFirstResult = new GroupeUtilisateurBean();
        notInFirstResult.setCodeGroupe("noResult");
        new Expectations() {{
            groupeUtilisateurDAO.selectInGroupCode(Collections.singletonList("codeGroupe"));
            result = Collections.singletonList(groupeUtilisateurBean);
            groupeUtilisateurDAO.selectInGroupCode(Arrays.asList("codeGroupe", "noResult"));
            result = Arrays.asList(groupeUtilisateurBean, notInFirstResult);
        }};
        List<GroupeUtilisateurBean> result = serviceGroupeUtilisateur.getInGroupCode(Collections.singletonList("codeGroupe"));
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.get(0), groupeUtilisateurBean, "le résultat doit correspondre au groupe utilisateur ayant comme code groupe : codeGroupe");
        result = serviceGroupeUtilisateur.getInGroupCode(Arrays.asList("codeGroupe", "noResult"));
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 2, "le résultat doit contenir deux elements");
    }

    @Test
    public void testGetByUserCodeGroupCodeAndImportSource() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        groupeUtilisateurBean.setSourceImport("sourceImport");
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserGroupAndImportSource("codeUtilisateur", "codeGroupe", "sourceImport");
            result = groupeUtilisateurBean;
            groupeUtilisateurDAO.selectByUserGroupAndImportSource("noResult", "noResult", "sourceImport");
            result = null;
        }};
        GroupeUtilisateurBean result = serviceGroupeUtilisateur.getByUserCodeGroupCodeAndImportSource("codeUtilisateur", "codeGroupe", "sourceImport");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertEquals(result, groupeUtilisateurBean, "le résultat doit être le premier element");
        result = serviceGroupeUtilisateur.getByUserCodeGroupCodeAndImportSource("noResult", "noResult", "sourceImport");
        Assert.assertNull(result, "le résultat doit être null");
    }

    @Test
    public void testGetUserCodeByGroupAndImportSource() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        groupeUtilisateurBean.setSourceImport("sourceImport");
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        new Expectations() {{
            groupeUtilisateurDAO.selectByGroupCodeAndImportSource("noResult", "noResult");
            result = Collections.emptyList();
            groupeUtilisateurDAO.selectByGroupCodeAndImportSource("codeGroupe", "sourceImport");
            result = Collections.singletonList(groupeUtilisateurBean);
        }};
        Collection<String> result = serviceGroupeUtilisateur.getUserCodeByGroupAndImportSource("codeGroupe", "sourceImport");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.iterator().next(), "codeUtilisateur", "le résultat doit être codeUtilisateur");
        result = serviceGroupeUtilisateur.getUserCodeByGroupAndImportSource("noResult", "noResult");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit etre vide");
    }

    @Test
    public void testGetGroupCodeByUserAndImportSource() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("codeGroupe");
        groupeUtilisateurBean.setSourceImport("sourceImport");
        groupeUtilisateurBean.setCodeUtilisateur("codeUtilisateur");
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserCodeAndImportSource("noResult", "noResult");
            result = Collections.<GroupeUtilisateurBean>emptyList();
            groupeUtilisateurDAO.selectByUserCodeAndImportSource("codeUtilisateur", "sourceImport");
            result = Collections.singletonList(groupeUtilisateurBean);
        }};
        Collection<String> result = serviceGroupeUtilisateur.getGroupCodeByUserAndImportSource("codeUtilisateur", "sourceImport");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.iterator().next(), "codeGroupe", "le résultat doit être codeGroupe");
        result = serviceGroupeUtilisateur.getGroupCodeByUserAndImportSource("noResult", "noResult");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit etre vide");
    }

    @Test
    public void testGetAllUserCodeByGroup(@Mocked(stubOutClassInitialization = true) RequeteGroupeUtil mockedRequetGroupeUtil) throws Exception {
        String twoGroupsCodes = "codeGroupe;codeGroupe2";
        final GroupeDsiBean firstResult = new GroupeDsiBean();
        firstResult.setCode("codeGroupe");
        final GroupeDsiBean secondResult = new GroupeDsiBean();
        secondResult.setCode("codeGroupe2");
        secondResult.setRequeteGroupe("requeteGroupe");
        final GroupeDsiBean subGroup = new GroupeDsiBean();
        subGroup.setCode("subGroup");
        subGroup.setCodeGroupePere("codeGroupe");
        final GroupeUtilisateurBean firstResultGU = new GroupeUtilisateurBean();
        firstResultGU.setCodeUtilisateur("codeUtilisateur");
        final GroupeUtilisateurBean subGroupGU = new GroupeUtilisateurBean();
        subGroupGU.setCodeUtilisateur("subCodeUtilisateur");
        new Expectations() {{
            serviceGroupeDsi.getByCode("codeGroupe");
            result = firstResult;
            serviceGroupeDsi.getByCode("codeGroupe2");
            result = secondResult;
            serviceGroupeDsi.getAllSubGroups("codeGroupe");
            result = Collections.singletonList(subGroup);
            RequeteGroupeUtil.getVecteurUtilisateurs("codeGroupe2", null);
            result = Collections.singletonList("secondCodeUtilisateur");
        }};
        new NonStrictExpectations() {{
            groupeUtilisateurDAO.selectInGroupCode(Arrays.asList("codeGroupe", "subGroup"));
            result = Arrays.asList(firstResultGU, subGroupGU);
            groupeUtilisateurDAO.selectInGroupCode(Arrays.asList("subGroup", "codeGroupe"));
            result = Arrays.asList(firstResultGU, subGroupGU);
        }};
        String nullString = null;
        Collection<String> codeUsers = serviceGroupeUtilisateur.getAllUserCodeByGroup(nullString);
        Assert.assertNotNull(codeUsers, "le résultat ne peut pas être vide");
        Assert.assertTrue(codeUsers.isEmpty(), "le résultat doit être vide");
        codeUsers = serviceGroupeUtilisateur.getAllUserCodeByGroup(Collections.<String>emptyList());
        Assert.assertNotNull(codeUsers, "le résultat ne peut pas être vide");
        Assert.assertTrue(codeUsers.isEmpty(), "le résultat doit être vide");
        codeUsers = serviceGroupeUtilisateur.getAllUserCodeByGroup(twoGroupsCodes);
        Assert.assertNotNull(codeUsers, "le résultat ne peut pas être null");
        Assert.assertEquals(codeUsers.size(), 3, "le résultat doit contenir 3 éléments");
        Assert.assertTrue(codeUsers.contains("secondCodeUtilisateur"), "le résultat doit contenir les trois valeurs des codes utilisateurs");
        Assert.assertTrue(codeUsers.contains("codeUtilisateur") , "le résultat doit contenir les trois valeurs des codes utilisateurs");
        Assert.assertTrue(codeUsers.contains("subCodeUtilisateur") , "le résultat doit contenir les trois valeurs des codes utilisateurs");
    }

    @Test
    public void testSetUserForGroup() {
        final GroupeUtilisateurBean firstResult = new GroupeUtilisateurBean();
        firstResult.setCodeGroupe("codeGroupe");
        firstResult.setCodeUtilisateur("codeUtilisateur");
        firstResult.setId(1L);
        final GroupeUtilisateurBean secondResult = new GroupeUtilisateurBean();
        secondResult.setCodeGroupe("codeGroupe");
        secondResult.setCodeUtilisateur("codeUtilisateur2");
        secondResult.setId(2L);
        new Expectations() {{
            groupeUtilisateurDAO.selectByGroupCode("codeGroupe");
            result = Arrays.asList(firstResult, secondResult);
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe");
            result = Collections.emptyList();
        }};
        serviceGroupeUtilisateur.setUsersForGroup(Collections.singletonList("codeUtilisateur"), "codeGroupe");
        new Verifications() {{
            groupeUtilisateurDAO.delete(2L);
            times = 1;
            groupeUtilisateurDAO.add((GroupeUtilisateurBean) any);
            times = 1;
        }};
    }

    @Test
    public void testSynchronizeUsersForGroup() {
        final GroupeUtilisateurBean firstResult = new GroupeUtilisateurBean();
        firstResult.setCodeGroupe("codeGroupe");
        firstResult.setCodeUtilisateur("codeUtilisateur");
        firstResult.setId(1L);
        final GroupeUtilisateurBean secondResult = new GroupeUtilisateurBean();
        secondResult.setCodeGroupe("codeGroupe");
        secondResult.setCodeUtilisateur("codeUtilisateur");
        secondResult.setId(2L);
        final GroupeUtilisateurBean thirdResult = new GroupeUtilisateurBean();
        thirdResult.setCodeGroupe("codeGroupe");
        thirdResult.setCodeUtilisateur("codeUtilisateur3");
        thirdResult.setId(3L);
        final GroupeUtilisateurBean groupToDelete = new GroupeUtilisateurBean();
        groupToDelete.setId(4L);
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe");
            result = Arrays.asList(firstResult, secondResult);
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur2", "codeGroupe");
            result = Collections.emptyList();
            groupeUtilisateurDAO.selectByGroupCodeAndImportSource("codeGroupe", "sourceImport");
            result = Collections.singletonList(thirdResult);
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur3", "codeGroupe");
            result = groupToDelete;
        }};
        serviceGroupeUtilisateur.synchronizeUsersForGroup(Arrays.asList("codeUtilisateur", "codeUtilisateur2"), "codeGroupe", "sourceImport");
        new Verifications() {{
            groupeUtilisateurDAO.delete(4L);
            times = 1;
            groupeUtilisateurDAO.add((GroupeUtilisateurBean) any);
            times = 1;
            groupeUtilisateurDAO.update((GroupeUtilisateurBean) any);
            times = 2;
        }};
    }

    @Test
    public void testSetGroupsForUser(@Mocked(stubOutClassInitialization = true) RequeteGroupeUtil mockedRequetGroupeUtil) throws Exception {
        final GroupeDsiBean firstResultdsi = new GroupeDsiBean();
        firstResultdsi.setCode("codeGroupe");
        final GroupeDsiBean secondResultdsi = new GroupeDsiBean();
        secondResultdsi.setCode("codeGroupe2");
        secondResultdsi.setRequeteGroupe("requeteGroupe");
        final InfosRequeteGroupe infosRequeteGroupe = new InfosRequeteGroupe();
        infosRequeteGroupe.setAlias("alias");
        final GroupeUtilisateurBean thirdResult = new GroupeUtilisateurBean();
        thirdResult.setCodeGroupe("codeGroupe3");
        thirdResult.setCodeUtilisateur("codeUtilisateur2");
        thirdResult.setId(3L);
        final GroupeUtilisateurBean fourthResult = new GroupeUtilisateurBean();
        fourthResult.setCodeGroupe("codeGroupe4");
        fourthResult.setCodeUtilisateur("codeUtilisateur2");
        fourthResult.setId(4L);
        new Expectations() {{
            serviceGroupeDsi.getByCode("codeGroupe");
            result = firstResultdsi;
            serviceGroupeDsi.getByCode("codeGroupe2");
            result = secondResultdsi;
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe");
            result = Collections.emptyList();
            serviceGroupeDsi.getGroupRequestList();
            result = Collections.singletonMap("infosRequete", infosRequeteGroupe);
            RequeteGroupeUtil.getGroupesUtilisateur(infosRequeteGroupe, "codeUtilisateur", anyLong);
            result = Collections.singletonList("codeGroupe2");
            groupeUtilisateurDAO.selectByUserCode("codeUtilisateur");
            result = Arrays.asList(thirdResult, fourthResult);
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe4");
            result = Collections.singletonList(fourthResult);
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe3");
            result = Collections.singletonList(thirdResult);
        }};
        serviceGroupeUtilisateur.setGroupsForUser("codeUtilisateur", Arrays.asList("codeGroupe", "codeGroupe2", "#AUTO#codeGroupe"));
        new Verifications() {{
            groupeUtilisateurDAO.delete(anyLong);
            times = 2;
            groupeUtilisateurDAO.add((GroupeUtilisateurBean) any);
            times = 1;
        }};
    }

    @Test
    public void testAddGroupsToUser() {
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe");
            result = Collections.singletonList(new GroupeUtilisateurBean());
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe2");
            result = Collections.emptyList();
        }};
        serviceGroupeUtilisateur.addGroupsToUser(Arrays.asList("codeGroupe", "codeGroupe2"), "codeUtilisateur");
        serviceGroupeUtilisateur.addGroupsToUser(Collections.<String>emptyList(), "codeUtilisateur");
        new Verifications() {{
            groupeUtilisateurDAO.add((GroupeUtilisateurBean) any);
            times = 1;
        }};
    }

    @Test
    public void testSynchronizeGroupsForUser() {
        final GroupeUtilisateurBean firstResult = new GroupeUtilisateurBean();
        firstResult.setId(1L);
        final GroupeUtilisateurBean secondResult = new GroupeUtilisateurBean();
        secondResult.setCodeGroupe("codeGroupe2");
        final GroupeUtilisateurBean thirdResult = new GroupeUtilisateurBean();
        thirdResult.setCodeGroupe("codeGroupe3");
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe");
            result = Collections.singletonList(firstResult);
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe2");
            result = Collections.emptyList();
            groupeUtilisateurDAO.selectByUserCodeAndImportSource("codeUtilisateur", "sourceImport");
            result = Arrays.asList(secondResult, thirdResult);
            groupeUtilisateurDAO.selectByUserAndGroup("codeUtilisateur", "codeGroupe3");
            result = Collections.singletonList(thirdResult);
        }};
        serviceGroupeUtilisateur.synchronizeGroupsForUser(Arrays.asList("codeGroupe","codeGroupe2"), "codeUtilisateur", "sourceImport");
        new Verifications() {{
            groupeUtilisateurDAO.add((GroupeUtilisateurBean) any);
            times = 1;
            groupeUtilisateurDAO.update((GroupeUtilisateurBean) any);
            times = 1;
            groupeUtilisateurDAO.delete(anyLong);
            times = 1;
        }};
    }

    @Test
    public void testGetAllDatasourceGroupsCodesByUserCode() {
        final Collection<String> emptyList = serviceGroupeUtilisateur.getAllDatasourceGroupsCodesByUserCode(null);
        Assert.assertNotNull(emptyList, "le résultat ne peut pas être null");
        Assert.assertTrue(emptyList.isEmpty(), "le résultat doit être vide");
        final GroupeUtilisateurBean firstResult = new GroupeUtilisateurBean();
        firstResult.setCodeGroupe("codeGroupe");
        new Expectations() {{
            groupeUtilisateurDAO.selectByUserCode("codeUtilisateur");
            result = Collections.singletonList(firstResult);
            groupeUtilisateurDAO.selectByUserCode("noResult");
            result = Collections.emptyList();
        }};
        final Collection<String> result = serviceGroupeUtilisateur.getAllDatasourceGroupsCodesByUserCode("codeUtilisateur");
        Assert.assertNotNull(result, "le résultat ne peut pas être null");
        Assert.assertEquals(result.size(),1, "le résultat doit contenir un élément");
        Assert.assertTrue(result.contains("codeGroupe"), "le code du groupe doit être codeGroupe");
        final Collection<String> noResult = serviceGroupeUtilisateur.getAllDatasourceGroupsCodesByUserCode("noResult");
        Assert.assertNotNull(noResult, "le résultat ne peut pas être null");
        Assert.assertTrue(noResult.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testGetAllDynamicGroupsCodesByUserCode(@Mocked(stubOutClassInitialization = true) RequeteGroupeUtil mockedRequetGroupeUtil) throws Exception {
        final InfosRequeteGroupe infosAvecAlias = new InfosRequeteGroupe();
        infosAvecAlias.setAlias("alias");
        final Map<String, InfosRequeteGroupe> mapInfosRequetes = new HashMap<>();
        mapInfosRequetes.put("avecAlias", infosAvecAlias);
        mapInfosRequetes.put("sansAlias", new InfosRequeteGroupe());
        new Expectations() {{
            serviceGroupeDsi.getGroupRequestList();
            result = mapInfosRequetes;
            RequeteGroupeUtil.getGroupesUtilisateur(infosAvecAlias, "codeUtilisateur", anyLong);
            result = Collections.singletonList("codeGroupe2");
        }};
        final Collection<String> result = serviceGroupeUtilisateur.getAllDynamicGroupsCodesByUserCode("codeUtilisateur");
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 1);
        Assert.assertTrue(result.contains("codeGroupe2"));
    }
}
