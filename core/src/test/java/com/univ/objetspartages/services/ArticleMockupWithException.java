package com.univ.objetspartages.services;

import com.jsbsoft.jtf.exception.ErreurApplicative;

import mockit.Mock;

/**
 * Created by olivier.camon on 24/12/15.
 */
public class ArticleMockupWithException  extends EmptyArticleMockup{

    @Mock
    public int selectCodeLangueEtat(String code, String langue, String etat) throws Exception {
        throw new ErreurApplicative("Expected");
    }
}
