package com.univ.objetspartages.services;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.dao.impl.RubriqueDAO;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

import mockit.Capturing;
import mockit.Expectations;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;

/**
 * Created by olivier.camon on 12/10/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:/com/univ/objetspartages/services/ServiceRubriqueTest.test-context.xml"})
public class ServiceRubriqueTest extends AbstractTestNGSpringContextTests {

    @Autowired
    ServiceRubrique serviceRubrique;

    @Mocked
    private RubriqueDAO rubriqueDAO;

    @Mocked
    private ServiceRubriquePublication serviceRubriquePublication;

    @Mocked ServiceMetatag serviceMetatag;

    @BeforeClass
    public void initMock(){
        rubriqueDAO = new RubriqueDAO();
        serviceRubriquePublication = new ServiceRubriquePublication();
        serviceMetatag = new ServiceMetatag();
        serviceRubrique.setDao(rubriqueDAO);
        serviceRubrique.setServiceRubriquePublication(serviceRubriquePublication);
        serviceRubrique.setServiceMetatag(serviceMetatag);
    }

    @Test
    public void testSave() {
        new Expectations() {{
            rubriqueDAO.selectByCode("TEST_SAVE");
            result = null;
        }};
        final RubriqueBean rubriqueByCode = serviceRubrique.getRubriqueByCode("TEST_SAVE");
        Assert.assertNull(rubriqueByCode, "Le premier appel ne doit pas retourner de rubrique");
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_SAVE");
        rubriqueBean.setIntitule("Rubrique1");
        new Expectations() {{
            rubriqueDAO.add(rubriqueBean);
            result = rubriqueBean;
            rubriqueDAO.selectByCode("TEST_SAVE");
            result = rubriqueBean;
        }};
        serviceRubrique.save(rubriqueBean);
        final RubriqueBean rubriqueByCode1 = serviceRubrique.getRubriqueByCode("TEST_SAVE");
        Assert.assertNotNull(rubriqueByCode1, "Le second appel doit retourner une rubrique");
        Assert.assertTrue(rubriqueByCode1.getCode().equals(rubriqueBean.getCode()), "Le second appel doit renvoyer la rubrique portant le code TEST_SAVE");
        final RubriqueBean rubriqueBean1 = new RubriqueBean();
        rubriqueBean1.setCode("TEST_SAVE");
        rubriqueBean1.setIntitule("Rubrique2");
        new Expectations() {{
            rubriqueDAO.update(rubriqueBean1);
            result = rubriqueBean1;
            rubriqueDAO.selectByCode("TEST_SAVE");
            result = rubriqueBean1;
            rubriqueDAO.getById(1L);
            result = rubriqueBean1;
        }};
        rubriqueBean1.setId(1L);
        serviceRubrique.save(rubriqueBean1);
        final RubriqueBean rubriqueByCode2 = serviceRubrique.getRubriqueByCode("TEST_SAVE");
        Assert.assertNotNull(rubriqueByCode2, "Le troisième appel doit retourner une rubrique");
        Assert.assertTrue(rubriqueByCode2.getIntitule().equals("Rubrique2"), "Le troisième appel doit fournir la nouvelle version de TEST_SAVE");
        final RubriqueBean rubriqueDiffLabel = new RubriqueBean();
        rubriqueDiffLabel.setId(1L);
        rubriqueDiffLabel.setCode("TEST_SAVE");
        rubriqueDiffLabel.setIntitule("Autre label");
        new Expectations() {{
            rubriqueDAO.update(rubriqueDiffLabel);
            result = rubriqueDiffLabel;
            rubriqueDAO.selectByCode("TEST_SAVE");
            result = rubriqueDiffLabel;
            rubriqueDAO.getById(1L);
            result = rubriqueBean1;
        }};
        serviceRubrique.save(rubriqueDiffLabel);
        final RubriqueBean rubriqueByCode3 = serviceRubrique.getRubriqueByCode("TEST_SAVE");
        Assert.assertNotNull(rubriqueByCode3, "Le quatrième appel doit retourner une rubrique");
        Assert.assertTrue(rubriqueByCode3.getIntitule().equals("Autre label"), "Le quatrième appel doit fournir la nouvelle version de TEST_SAVE");
    }

    @Test
    public void testGetRubriqueByCode() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETBYCODE");
        rubriqueBean.setIntitule("Rubrique1");
        new Expectations() {{
            rubriqueDAO.selectByCode(anyString);
            result = rubriqueBean;
        }};
        final RubriqueBean rubriqueByCode = serviceRubrique.getRubriqueByCode("TEST_GETBYCODE");
        Assert.assertNotNull(rubriqueByCode, "Le premier appel doit retourner une rubrique");
        Assert.assertTrue(rubriqueByCode.getCode().equals(rubriqueBean.getCode()), "Le premier appel doit renvoyer la rubrique portant le code TEST_GETBYCODE");
        final RubriqueBean rubriqueByCode2 = serviceRubrique.getRubriqueByCode("TEST_GETBYCODE");
        new Verifications() {{
            rubriqueDAO.selectByCode(anyString);
            times = 1;
        }};
        Assert.assertEquals(rubriqueByCode2, rubriqueByCode, "le second appel doit être retourné du cache");
    }

    @Test
    public void testGetRubriqueByCodeParent() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCodeRubriqueMere("TEST_GETBYCODEPARENT");
        rubriqueBean.setIntitule("Rubrique1");
        new Expectations() {{
            rubriqueDAO.selectByCodeParent(anyString);
            result = Collections.singletonList(rubriqueBean);
        }};
        final List<RubriqueBean> rubriquesByCodeParent = serviceRubrique.getRubriqueByCodeParent("TEST_GETBYCODEPARENT");
        Assert.assertNotNull(rubriquesByCodeParent, "Le premier appel doit retourner une liste non null");
        Assert.assertEquals(rubriquesByCodeParent.size(), 1, "Le premier appel doit retourner liste contenant un element");
        Assert.assertTrue(rubriquesByCodeParent.get(0).getCodeRubriqueMere().equals(rubriqueBean.getCodeRubriqueMere()), "Le premier appel doit renvoyer la rubrique portant le codeParent TEST_GETBYCODEPARENT");
        final List<RubriqueBean> rubriquesByCodeParent2 = serviceRubrique.getRubriqueByCodeParent("TEST_GETBYCODEPARENT");
        new Verifications() {{
            rubriqueDAO.selectByCodeParent(anyString);
            times = 1;
        }};
        Assert.assertEquals(rubriquesByCodeParent2, rubriquesByCodeParent, "le second appel doit être retourné du cache");
    }

    @Test
    public void testGetRubriqueByCodeParentFront() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCodeRubriqueMere("TEST_GETBYCODEPARENTFRONT");
        rubriqueBean.setIntitule("Rubrique1");
        final RubriqueBean rubriqueRestriente = new RubriqueBean();
        rubriqueRestriente.setCodeRubriqueMere("TEST_GETBYCODEPARENTFRONT");
        rubriqueRestriente.setIntitule("Rubrique2");
        rubriqueRestriente.setGroupesDsi("GROUPES_DSI");
        final RubriqueBean rubriqueHidden = new RubriqueBean();
        rubriqueHidden.setCodeRubriqueMere("TEST_GETBYCODEPARENTFRONT");
        rubriqueHidden.setIntitule("Rubrique3");
        rubriqueHidden.setCategorie("HIDDEN");
        new Expectations() {{
            rubriqueDAO.selectByCodeParent(anyString);
            result = Arrays.asList(rubriqueBean, rubriqueRestriente, rubriqueHidden);
            rubriqueDAO.selectByCode("TEST_GETBYCODEPARENTFRONT");
            result = null;
        }};
        final List<RubriqueBean> rubriquesByCodeParent = serviceRubrique.getRubriqueByCodeParentFront("TEST_GETBYCODEPARENTFRONT", Collections.<String>emptySet());
        Assert.assertNotNull(rubriquesByCodeParent, "Le premier appel doit retourner une liste non null");
        Assert.assertEquals(rubriquesByCodeParent.size(), 1, "Le premier appel doit retourner liste contenant un element");
        Assert.assertTrue(rubriquesByCodeParent.get(0).getIntitule().equals(rubriqueBean.getIntitule()), "Le premier appel doit renvoyer la rubrique ayant pour intitule Rubrique1");
        final List<RubriqueBean> rubriquesByCodeParent2 = serviceRubrique.getRubriqueByCodeParentFront("TEST_GETBYCODEPARENTFRONT", Collections.<String>emptySet());
        new Verifications() {{
            rubriqueDAO.selectByCodeParent(anyString);
            times = 1;
        }};
        Assert.assertEquals(rubriquesByCodeParent2, rubriquesByCodeParent, "le second appel doit être retourné du cache");
    }

    @Test
    public void testGetAllRubrique() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCodeRubriqueMere("TEST_GETALL");
        rubriqueBean.setIntitule("Rubrique1");
        final RubriqueBean rubriqueRestriente = new RubriqueBean();
        rubriqueRestriente.setCodeRubriqueMere("TEST_GETALL");
        rubriqueRestriente.setIntitule("Rubrique2");
        rubriqueRestriente.setGroupesDsi("GROUPES_DSI");
        new Expectations() {{
            rubriqueDAO.selectAllRubriques();
            result = Arrays.asList(rubriqueBean, rubriqueRestriente);
        }};
        final List<RubriqueBean> allSections = serviceRubrique.getAllRubriques();
        Assert.assertNotNull(allSections, "Le premier appel doit retourner une liste non null");
        Assert.assertEquals(allSections.size(), 2, "Le premier appel doit retourner liste contenant deux elements");
        Assert.assertTrue(allSections.get(0).getIntitule().equals(rubriqueBean.getIntitule()), "Le premier appel doit renvoyer la rubrique ayant pour intitule Rubrique1");
        new Expectations() {{
            rubriqueDAO.selectAllRubriques();
            result = Collections.singletonList(rubriqueRestriente);
        }};
        final List<RubriqueBean> allSections2 = serviceRubrique.getAllRubriques();
        Assert.assertNotEquals(allSections2, allSections, "le second appel ne doit pas être retourné du cache");
        Assert.assertTrue(allSections2.get(0).getIntitule().equals(rubriqueRestriente.getIntitule()), "Le second appel doit renvoyer la rubrique ayant pour intitule Rubrique2");
    }

    @Test
    public void testGetAllWithoutUrl() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCodeRubriqueMere("TEST_GETALL");
        rubriqueBean.setIntitule("Rubrique1");
        final RubriqueBean rubriqueRestriente = new RubriqueBean();
        rubriqueRestriente.setCodeRubriqueMere("TEST_GETALL");
        rubriqueRestriente.setIntitule("Rubrique2");
        rubriqueRestriente.setGroupesDsi("GROUPES_DSI");
        new Expectations() {{
            rubriqueDAO.selectAllWithoutUrl();
            result = Arrays.asList(rubriqueBean, rubriqueRestriente);
        }};
        final List<RubriqueBean> allSections = serviceRubrique.getAllWithoutUrl();
        Assert.assertNotNull(allSections, "Le premier appel doit retourner une liste non null");
        Assert.assertEquals(allSections.size(), 2, "Le premier appel doit retourner liste contenant deux elements");
        Assert.assertTrue(allSections.get(0).getIntitule().equals(rubriqueBean.getIntitule()), "Le premier appel doit renvoyer la rubrique ayant pour intitule Rubrique1");
        new Expectations() {{
            rubriqueDAO.selectAllWithoutUrl();
            result = Collections.singletonList(rubriqueRestriente);
        }};
        final List<RubriqueBean> allSections2 = serviceRubrique.getAllWithoutUrl();
        Assert.assertNotEquals(allSections2, allSections, "le second appel ne doit pas être retourné du cache");
        Assert.assertTrue(allSections2.get(0).getIntitule().equals(rubriqueRestriente.getIntitule()), "Le premier appel doit renvoyer la rubrique ayant pour intitule Rubrique2");
    }

    @Test
    public void testGetRubriqueInCodes() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETRUBRIQUEINCODES");
        rubriqueBean.setIntitule("Rubrique1");
        final RubriqueBean rubriqueRestriente = new RubriqueBean();
        rubriqueRestriente.setCode("TEST_GETRUBRIQUEINCODES2");
        rubriqueRestriente.setIntitule("Rubrique2");
        rubriqueRestriente.setGroupesDsi("GROUPES_DSI");
        final RubriqueBean rubriqueHidden = new RubriqueBean();
        rubriqueHidden.setCode("TEST_GETRUBRIQUEINCODES3");
        rubriqueHidden.setIntitule("Rubrique3");
        rubriqueHidden.setCategorie("HIDDEN");
        final Collection<String> emptyCode = Collections.emptyList();
        new Expectations() {{
            rubriqueDAO.selectByCodes(Arrays.asList("TEST_GETRUBRIQUEINCODES", "TEST_GETRUBRIQUEINCODES2"));
            result = Arrays.asList(rubriqueBean, rubriqueRestriente);
        }};
        final List<RubriqueBean> rubriqueInCodes = serviceRubrique.getRubriqueInCodes(Arrays.asList("TEST_GETRUBRIQUEINCODES", "TEST_GETRUBRIQUEINCODES2"));
        Assert.assertNotNull(rubriqueInCodes, "Le premier appel doit retourner une liste non null");
        Assert.assertEquals(rubriqueInCodes.size(), 2, "Le premier appel doit retourner liste contenant deux elements");
        Assert.assertTrue(rubriqueInCodes.get(0).getIntitule().equals(rubriqueBean.getIntitule()), "Le premier appel doit renvoyer la rubrique ayant pour intitule Rubrique1");
        final List<RubriqueBean> rubriquesInCodes2 = serviceRubrique.getRubriqueInCodes(emptyCode);
        Assert.assertNotNull(rubriquesInCodes2, "Le seconds appel doit retourner une liste non null");
        Assert.assertEquals(rubriquesInCodes2.size(), 0, "le second appel doit être retourné une liste vide");
    }

    @Test
    public void testDelete() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_DELETE");
        rubriqueBean.setIntitule("Rubrique delete");
        new Expectations() {{
            rubriqueDAO.selectByCode(anyString);
            result = rubriqueBean;
        }};
        final RubriqueBean rubriqueByCode = serviceRubrique.getRubriqueByCode("TEST_DELETE");
        Assert.assertNotNull(rubriqueByCode, "Le premier appel doit retourner la rubrique");
        Assert.assertTrue(rubriqueByCode.getCode().equals(rubriqueBean.getCode()), "Le premier appel doit renvoyer la rubrique portant le code TEST_DELETE");
        new NonStrictExpectations() {{
            rubriqueDAO.getById(anyLong);
            result = rubriqueBean;
        }};
        serviceRubrique.delete(0L);
        new Expectations() {{
            rubriqueDAO.selectByCode(anyString);
            result = null;
        }};
        final RubriqueBean rubriqueByCode1 = serviceRubrique.getRubriqueByCode("TEST_DELETE");
        Assert.assertNull(rubriqueByCode1, "Le second appel ne doit pas passer par le cache et donc faire appel au dao qui nous renvoit \"null\"");
    }

    @Test
    public void testHasContent() {
        Assert.assertFalse(serviceRubrique.hasContent(StringUtils.EMPTY), "si le code est vide, le résultat est forcement faux");
        Assert.assertFalse(serviceRubrique.hasContent(null), "si le code est null, le résultat est forcement faux");
        new Expectations() {{
            serviceMetatag.getMetasForRubriqueAndStates(anyString, Arrays.asList(EtatFiche.BROUILLON.getEtat(),EtatFiche.A_VALIDER.getEtat(), EtatFiche.EN_LIGNE.getEtat()));
            result = Collections.emptyList();
            rubriqueDAO.selectByCodeParent(anyString);
            result = Collections.emptyList();
            serviceRubriquePublication.getByRubriqueDestWithoutSource("noResult");
            result = Collections.emptyList();
            serviceRubriquePublication.getByRubriqueDestWithoutSource("resultForRubPub");
            result = Collections.singletonList(new RubriquepublicationBean());
        }};
        Assert.assertFalse(serviceRubrique.hasContent("noResult"), "il ne doit pas y avoir de résultat pour le code noResult");
        Assert.assertTrue(serviceRubrique.hasContent("resultForRubPub"), "il ne doit pas y avoir de résultat pour le code noResult");
    }
    @Test
    public void testHasChilds() {
        Assert.assertFalse(serviceRubrique.hasChilds(StringUtils.EMPTY), "si le code est vide, le résultat est forcement faux");
        Assert.assertFalse(serviceRubrique.hasChilds(null), "si le code est null, le résultat est forcement faux");
        new Expectations() {{
            rubriqueDAO.selectByCodeParent("noResult");
            result = Collections.emptyList();
            rubriqueDAO.selectByCodeParent("hasChilds");
            result = Collections.singletonList(new RubriqueBean());
        }};
        Assert.assertFalse(serviceRubrique.hasChilds("noResult"), "il ne doit pas y avoir de résultat pour le code noResult");
        Assert.assertTrue(serviceRubrique.hasChilds("hasChilds"), "il doit y avoir du contenu pour le code rubrique");
    }
    @Test
    public void testHasMetas() {
        Assert.assertFalse(serviceRubrique.hasMetas(StringUtils.EMPTY), "si le code est vide, le résultat est forcement faux");
        Assert.assertFalse(serviceRubrique.hasMetas(null), "si le code est null, le résultat est forcement faux");
        new Expectations() {{
            serviceMetatag.getMetasForRubriqueAndStates("noResult", Arrays.asList(EtatFiche.BROUILLON.getEtat(),EtatFiche.A_VALIDER.getEtat(), EtatFiche.EN_LIGNE.getEtat()));
            result = Collections.emptyList();
            serviceMetatag.getMetasForRubriqueAndStates("rubrique", Arrays.asList(EtatFiche.BROUILLON.getEtat(),EtatFiche.A_VALIDER.getEtat(), EtatFiche.EN_LIGNE.getEtat()));
            result = Collections.singletonList(new MetatagBean());
        }};
        Assert.assertFalse(serviceRubrique.hasMetas("noResult"), "il ne doit pas y avoir de résultat pour le code noResult");
        Assert.assertTrue(serviceRubrique.hasMetas("rubrique"), "il doit y avoir du contenu pour le code rubrique");
    }
    @Test
    public void testHasContentOnRubPub() {
        Assert.assertFalse(serviceRubrique.hasContentOnRubPub(StringUtils.EMPTY), "si le code est vide, le résultat est forcement faux");
        Assert.assertFalse(serviceRubrique.hasContentOnRubPub(null), "si le code est null, le résultat est forcement faux");
        new Expectations() {{
            serviceRubriquePublication.getByRubriqueDestWithoutSource("noResult");
            result = Collections.emptyList();
            serviceRubriquePublication.getByRubriqueDestWithoutSource("rubrique");
            result = Collections.singletonList(new RubriquepublicationBean());
        }};
        Assert.assertFalse(serviceRubrique.hasContentOnRubPub("noResult"), "il ne doit pas y avoir de résultat pour le code noResult");
        Assert.assertTrue(serviceRubrique.hasContentOnRubPub("rubrique"), "il doit y avoir du contenu pour le code rubrique");
    }

    @Test
    public void testDeleteById() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_DELETEBYID");
        rubriqueBean.setIntitule("Rubrique delete");
        new Expectations() {{
            rubriqueDAO.selectByCode(anyString);
            result = rubriqueBean;
            rubriqueDAO.selectByCodeParent(anyString);
            result = Collections.emptyList();

        }};
        final RubriqueBean rubriqueByCode = serviceRubrique.getRubriqueByCode("TEST_DELETEBYID");
        Assert.assertNotNull(rubriqueByCode, "Le premier appel doit retourner la rubrique");
        Assert.assertTrue(rubriqueByCode.getCode().equals(rubriqueBean.getCode()), "Le premier appel doit renvoyer la rubrique portant le code TEST_DELETEBYID");
        new NonStrictExpectations() {{
            rubriqueDAO.getById(anyLong);
            result = rubriqueBean;
        }};
        try {
            serviceRubrique.deletebyId(0L);
        } catch (ErreurApplicative erreurApplicative) {
            Assert.fail("aucune exception ne doit être lever dans cet appel");
        }
        new Expectations() {{
            rubriqueDAO.selectByCode(anyString);
            result = null;
        }};
        final RubriqueBean rubriqueByCode1 = serviceRubrique.getRubriqueByCode("TEST_DELETEBYID");
        Assert.assertNull(rubriqueByCode1, "Le second appel ne doit pas passer par le cache et donc faire appel au dao qui nous renvoit \"null\"");
    }

    @Test
    public void testDeleteByCode() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_DELETEBYCODE");
        rubriqueBean.setIntitule("Rubrique delete");
        new Expectations() {{
            rubriqueDAO.selectByCode("TEST_DELETEBYCODE");
            result = rubriqueBean;
            rubriqueDAO.selectByCodeParent("TEST_DELETEBYCODE");
            result = Collections.emptyList();
            rubriqueDAO.selectByCodeParent("TEST_DELETEBYCODE_WITH_CONTENT");
            result = Collections.singletonList(new RubriqueBean());
            serviceMetatag.getMetasForRubriqueAndStates("TEST_DELETEBYCODE_WITH_META", Arrays.asList(EtatFiche.BROUILLON.getEtat(),EtatFiche.A_VALIDER.getEtat(), EtatFiche.EN_LIGNE.getEtat()));
            result = Collections.singletonList(new MetatagBean());
        }};
        final RubriqueBean rubriqueByCode = serviceRubrique.getRubriqueByCode("TEST_DELETEBYCODE");
        Assert.assertNotNull(rubriqueByCode, "Le premier appel doit retourner la rubrique");
        Assert.assertTrue(rubriqueByCode.getCode().equals(rubriqueBean.getCode()), "Le premier appel doit renvoyer la rubrique portant le code TEST_DELETEBYID");
        new NonStrictExpectations() {{
            rubriqueDAO.getById(anyLong);
            result = rubriqueBean;
        }};
        try {
            serviceRubrique.deleteByCode("TEST_DELETEBYCODE");
        } catch (ErreurApplicative erreurApplicative) {
            Assert.fail("aucune exception ne doit être lever dans cet appel");
        }
        try {
            serviceRubrique.deleteByCode("TEST_DELETEBYCODE_WITH_CONTENT");
            Assert.fail("l'exception doit être lever dans cet appel");
        } catch (ErreurApplicative erreurApplicative) {
            // exception expected
        }
        try {
            serviceRubrique.deleteByCode("TEST_DELETEBYCODE_WITH_META");
            Assert.fail("l'exception doit être lever dans cet appel");
        } catch (ErreurApplicative erreurApplicative) {
            // exception expected
        }
        new Expectations() {{
            rubriqueDAO.selectByCode("TEST_DELETEBYCODE");
            result = null;
        }};
        final RubriqueBean rubriqueByCode1 = serviceRubrique.getRubriqueByCode("TEST_DELETEBYCODE");
        Assert.assertNull(rubriqueByCode1, "Le second appel ne doit pas passer par le cache et donc faire appel au dao qui nous renvoit \"null\"");
    }

    @Test
    public void testDeleteByCodes() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_DELETEBYCODES");
        rubriqueBean.setIntitule("Rubrique delete");
        new Expectations() {{
            rubriqueDAO.selectByCode(anyString);
            result = rubriqueBean;
        }};
        final RubriqueBean rubriqueByCode = serviceRubrique.getRubriqueByCode("TEST_DELETEBYCODES");
        Assert.assertNotNull(rubriqueByCode, "Le premier appel doit retourner la rubrique");
        Assert.assertTrue(rubriqueByCode.getCode().equals(rubriqueBean.getCode()), "Le premier appel doit renvoyer la rubrique portant le code TEST_DELETEBYCODE");
        serviceRubrique.deleteByCodes(Collections.singletonList("TEST_DELETEBYCODES"));
        new Expectations() {{
            rubriqueDAO.selectByCode(anyString);
            result = null;
        }};
        final RubriqueBean rubriqueByCode1 = serviceRubrique.getRubriqueByCode("TEST_DELETEBYCODES");
        Assert.assertNull(rubriqueByCode1, "Le second appel ne doit pas passer par le cache et donc faire appel au dao qui nous renvoit \"null\"");
    }

    @Test
    public void testCodeAlreadyExist() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_CODEALREADYEXIST");
        rubriqueBean.setIntitule("Rubrique1");
        new Expectations() {{
            rubriqueDAO.selectByCode("TEST_CODEALREADYEXIST");
            result = rubriqueBean;
            rubriqueDAO.selectByCode("TEST_CODEALREADYEXIST2");
            result = null;
        }};
        final boolean alreadyExist = serviceRubrique.codeAlreadyExist("TEST_CODEALREADYEXIST");
        Assert.assertTrue(alreadyExist, "Le premier appel doit déjà exister");
        final boolean alreadyExist2 = serviceRubrique.codeAlreadyExist("TEST_CODEALREADYEXIST2");
        Assert.assertFalse(alreadyExist2, "Le second appel ne doit pas exister");
    }

    @Test
    public void testGetMaxOrderAvailable() {
        new Expectations() {{
            rubriqueDAO.selectMaxOrder("TEST_GETMAXORDER");
            result = 1;
            rubriqueDAO.selectMaxOrder("TEST_GETMAXORDER2");
            result = 0;
        }};
        final int maxOrder = serviceRubrique.getMaxOrderAvailable("TEST_GETMAXORDER");
        Assert.assertEquals(maxOrder, 2, "Le premier appel doit retourner 2");
        final int maxOrder2 = serviceRubrique.getMaxOrderAvailable("TEST_GETMAXORDER2");
        Assert.assertEquals(maxOrder2, 1, "Le second appel doit retourner 1");
        final int maxOrder3 = serviceRubrique.getMaxOrderAvailable(StringUtils.EMPTY);
        Assert.assertEquals(maxOrder3, 1, "Le troisième appel doit retourner 1");
    }

    @Test
    public void testGetAllChilds() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETALLCHILDS2");
        rubriqueBean.setCodeRubriqueMere("TEST_GETALLCHILDS");
        rubriqueBean.setIntitule("Rubrique1");
        new Expectations() {{
            rubriqueDAO.selectByCodeParent("TEST_GETALLCHILDS");
            result = Collections.singletonList(rubriqueBean);
            rubriqueDAO.selectByCodeParent("TEST_GETALLCHILDS2");
            result = Collections.emptyList();
        }};
        final List<RubriqueBean> allChilds = serviceRubrique.getAllChilds("TEST_GETALLCHILDS");
        Assert.assertNotNull(allChilds, "Le premier appel doit retourner une liste non null");
        Assert.assertEquals(allChilds.size(), 1, "Le premier appel doit retourner liste contenant un element");
        Assert.assertTrue(allChilds.get(0).getIntitule().equals(rubriqueBean.getIntitule()), "La rubrique doit avoir pour intitule Rubrique1");
        final List<RubriqueBean> allChilds2 = serviceRubrique.getAllChilds("TEST_GETALLCHILDS2");
        Assert.assertNotNull(allChilds2, "Le second appel doit retourner une liste non null");
        Assert.assertTrue(allChilds2.isEmpty(), "Le second appel doit retourner une liste vide");
    }

    @Test
    public void testGetLabelWithAscendantsLabels() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETLABELWITHASCENDANTSLABELS");
        rubriqueBean.setCodeRubriqueMere("TEST_GETLABELWITHASCENDANTSLABELS2");
        rubriqueBean.setNomOnglet("NomOnglet");
        rubriqueBean.setIntitule("Rubrique1");
        final RubriqueBean rubriqueBean2 = new RubriqueBean();
        rubriqueBean2.setCode("TEST_GETLABELWITHASCENDANTSLABELS2");
        rubriqueBean2.setCodeRubriqueMere("TEST_GETLABELWITHASCENDANTSLABELS3");
        rubriqueBean2.setNomOnglet("NomOnglet2");
        rubriqueBean2.setIntitule("Rubrique2");
        final RubriqueBean rubriqueBean3 = new RubriqueBean();
        rubriqueBean3.setCode("TEST_GETLABELWITHASCENDANTSLABELS3");
        rubriqueBean3.setCodeRubriqueMere("TEST_GETLABELWITHASCENDANTSLABELS4");
        rubriqueBean3.setNomOnglet("NomOnglet3");
        rubriqueBean3.setIntitule("Rubrique3");
        new Expectations() {{
            rubriqueDAO.selectByCode("TEST_GETLABELWITHASCENDANTSLABELS");
            result = rubriqueBean;
            rubriqueDAO.selectByCode("TEST_GETLABELWITHASCENDANTSLABELS2");
            result = rubriqueBean2;
            rubriqueDAO.selectByCode("TEST_GETLABELWITHASCENDANTSLABELS3");
            result = rubriqueBean3;
            rubriqueDAO.selectByCode("TEST_GETLABELWITHASCENDANTSLABELS4");
            result = null;
        }};
        final String label = serviceRubrique.getLabelWithAscendantsLabels("TEST_GETLABELWITHASCENDANTSLABELS");
        Assert.assertNotNull(label, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(label, "Rubrique3&nbsp;&gt;&nbsp;Rubrique2&nbsp;&gt;&nbsp;Rubrique1", "Le premier appel doit retourner le label 'Rubrique3 < Rubrique2 < Rubrique1'");
        final String label2 = serviceRubrique.getLabelWithAscendantsLabels("TEST_GETLABELWITHASCENDANTSLABELS4");
        Assert.assertNotNull(label2, "Le second appel doit retourner une valeur non null");
        Assert.assertTrue(StringUtils.isEmpty(label2), "Le second appel doit retourner une chaine vide");
    }

    @Test
    public void testGetAscendantsLabelsOnly() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETASCENDANTSLABELSONLY");
        rubriqueBean.setCodeRubriqueMere("TEST_GETASCENDANTSLABELSONLY2");
        rubriqueBean.setNomOnglet("NomOnglet1");
        rubriqueBean.setIntitule("Rubrique1");
        final RubriqueBean rubriqueBean2 = new RubriqueBean();
        rubriqueBean2.setCode("TEST_GETASCENDANTSLABELSONLY2");
        rubriqueBean2.setNomOnglet("NomOnglet2");
        rubriqueBean2.setIntitule("Rubrique2");
        final String nullString = null;
        new Expectations() {{
            rubriqueDAO.selectByCode("TEST_GETASCENDANTSLABELSONLY");
            result = rubriqueBean;
            rubriqueDAO.selectByCode("TEST_GETASCENDANTSLABELSONLY2");
            result = rubriqueBean2;
        }};
        final String label = serviceRubrique.getAscendantsLabelOnly("TEST_GETASCENDANTSLABELSONLY");
        Assert.assertNotNull(label, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(label, "Rubrique2", "Le premier appel doit retourner le label 'Rubrique2'");
        final String label2 = serviceRubrique.getAscendantsLabelOnly(nullString);
        Assert.assertNotNull(label2, "Le second appel doit retourner une valeur non null");
        Assert.assertTrue(StringUtils.isEmpty(label2), "Le second appel doit retourner une chaine vide");
    }

    @Test
    public void testGetAllAscendant() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETALLASCENDANT");
        rubriqueBean.setCodeRubriqueMere("TEST_GETALLASCENDANT2");
        rubriqueBean.setNomOnglet("NomOnglet1");
        rubriqueBean.setIntitule("Rubrique1");
        final RubriqueBean rubriqueBean2 = new RubriqueBean();
        rubriqueBean2.setCode("TEST_GETALLASCENDANT2");
        rubriqueBean2.setCodeRubriqueMere("TEST_GETALLASCENDANT3");
        rubriqueBean2.setNomOnglet("NomOnglet2");
        rubriqueBean2.setIntitule("Rubrique2");
        final RubriqueBean rubriqueBean3 = new RubriqueBean();
        rubriqueBean3.setCode("TEST_GETALLASCENDANT3");
        rubriqueBean3.setNomOnglet("NomOnglet3");
        rubriqueBean3.setIntitule("Rubrique3");
        new Expectations() {{
            rubriqueDAO.selectByCode("TEST_GETALLASCENDANT");
            result = rubriqueBean;
            rubriqueDAO.selectByCode("TEST_GETALLASCENDANT2");
            result = rubriqueBean2;
            rubriqueDAO.selectByCode("TEST_GETALLASCENDANT3");
            result = rubriqueBean3;
        }};
        final List<RubriqueBean> allAscendant = serviceRubrique.getAllAscendant("TEST_GETALLASCENDANT");
        Assert.assertNotNull(allAscendant, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(allAscendant.size(), 2, "Le premier appel doit retourner une liste de deux élements");
        final List<RubriqueBean> allAscendant2 = serviceRubrique.getAllAscendant("TEST_GETALLASCENDANT4");
        Assert.assertNotNull(allAscendant2, "Le second appel doit retourner une valeur non null");
        Assert.assertTrue(allAscendant2.isEmpty(), "Le second appel doit retourner une liste vide");
    }

    @Test
    public void testGetRubriqueByCodeLanguageLabel() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETRUBRIQUEBYCODELANGUAGELABEL");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setLangue("0");
        new Expectations() {{
            rubriqueDAO.getRubriqueByCodeLanguageLabel("TEST_GETRUBRIQUEBYCODELANGUAGELABEL", "0", "Rubrique1");
            result = rubriqueBean;
            rubriqueDAO.getRubriqueByCodeLanguageLabel("TEST_GETRUBRIQUEBYCODELANGUAGELABEL", "0", "Rubrique2");
            result = Collections.emptyList();
        }};
        final List<RubriqueBean> rubriqueByCodeLanguageLabel = serviceRubrique.getRubriqueByCodeLanguageLabel("TEST_GETRUBRIQUEBYCODELANGUAGELABEL", "0", "Rubrique1");
        Assert.assertNotNull(rubriqueByCodeLanguageLabel, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(rubriqueByCodeLanguageLabel.size(), 1, "Le premier appel doit retourner une liste d'un élement");
        Assert.assertEquals(rubriqueByCodeLanguageLabel.get(0), rubriqueBean, "La rubrique doit être égale à Rubrique1");
        final List<RubriqueBean> allAscendant2 = serviceRubrique.getRubriqueByCodeLanguageLabel("TEST_GETRUBRIQUEBYCODELANGUAGELABEL", "0", "Rubrique2");
        Assert.assertNotNull(allAscendant2, "Le second appel doit retourner une valeur non null");
        Assert.assertTrue(allAscendant2.isEmpty(), "Le second appel doit retourner une liste vide");
    }

    @Test
    public void testGetRubriqueByCodeLanguageLabelCategory() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETRUBRIQUEBYCODELANGUAGELABELCATEGORY");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setLangue("0");
        new Expectations() {{
            rubriqueDAO.getRubriqueByCodeLanguageLabelCategory("TEST_GETRUBRIQUEBYCODELANGUAGELABELCATEGORY", "0", "Rubrique1", "category");
            result = rubriqueBean;
            rubriqueDAO.getRubriqueByCodeLanguageLabelCategory("TEST_GETRUBRIQUEBYCODELANGUAGELABELCATEGORY", "0", "Rubrique2", "category");
            result = Collections.emptyList();
        }};
        final List<RubriqueBean> rubriqueByCodeLanguageLabel = serviceRubrique.getRubriqueByCodeLanguageLabelCategory("TEST_GETRUBRIQUEBYCODELANGUAGELABELCATEGORY", "0", "Rubrique1", "category");
        Assert.assertNotNull(rubriqueByCodeLanguageLabel, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(rubriqueByCodeLanguageLabel.size(), 1, "Le premier appel doit retourner une liste d'un élement");
        Assert.assertEquals(rubriqueByCodeLanguageLabel.get(0), rubriqueBean, "La rubrique doit être égale à Rubrique1");
        final List<RubriqueBean> allAscendant2 = serviceRubrique.getRubriqueByCodeLanguageLabelCategory("TEST_GETRUBRIQUEBYCODELANGUAGELABELCATEGORY", "0", "Rubrique2", "category");
        Assert.assertNotNull(allAscendant2, "Le second appel doit retourner une valeur non null");
        Assert.assertTrue(allAscendant2.isEmpty(), "Le second appel doit retourner une liste vide");
    }

    @Test
    public void testGetAllRubriqueWithPublicationRubrique() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETALLRUBRIQUEWITHPUBLICATIONRUBRIQUE");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setRequetesRubriquePublication("REQUETES_RUBPUB");
        new Expectations() {{
            rubriqueDAO.selectAllRubriquesWithRubPub();
            result = Collections.singletonList(rubriqueBean);
        }};
        final List<RubriqueBean> allRubriqueWithPublicationRubrique = serviceRubrique.getAllRubriqueWithPublicationRubrique();
        Assert.assertNotNull(allRubriqueWithPublicationRubrique, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(allRubriqueWithPublicationRubrique.size(), 1, "Le premier appel doit retourner une liste d'un élement");
        Assert.assertEquals(allRubriqueWithPublicationRubrique.get(0), rubriqueBean, "La rubrique doit être égale à Rubrique1");
    }

    @Test
    public void testGetByBandeau() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETBYBANDEAU");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setIdBandeau(1L);
        new Expectations() {{
            rubriqueDAO.selectByBandeau(1L);
            result = Collections.singletonList(rubriqueBean);
        }};
        final List<RubriqueBean> rubriqueByBandeau = serviceRubrique.getByBandeau(1L);
        Assert.assertNotNull(rubriqueByBandeau, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(rubriqueByBandeau.size(), 1, "Le premier appel doit retourner une liste d'un élement");
        Assert.assertEquals(rubriqueByBandeau.get(0), rubriqueBean, "La rubrique doit être égale à Rubrique1");
    }

    @Test
    public void testGetRubriqueByReferences() throws IOException {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETRUBRIQUEBYREFENRENCES");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setPageAccueil("{\"code\":\"codeFiche\",\"langue\":\"language\",\"objet\":\"nomObjet\"}");
        rubriqueBean.setEncadre("foo [id-fiche]nomObjet[/id-fiche] bar");
        rubriqueBean.setAccroche("baz [id-fiche]nomObjet[/id-fiche]");
        new Expectations() {{
            rubriqueDAO.selectByReferences("{\"code\":\"codeFiche\",\"langue\":\"language\",\"objet\":\"nomObjet\"}", "[id-fiche]nomObjet[/id-fiche]");
            result = Collections.singletonList(rubriqueBean);
        }};
        final List<RubriqueBean> rubriqueByReferences = serviceRubrique.getRubriqueByReferences("codeFiche", "language", "nomObjet");
        Assert.assertNotNull(rubriqueByReferences, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(rubriqueByReferences.size(), 1, "Le premier appel doit retourner une liste d'un élement");
        Assert.assertEquals(rubriqueByReferences.get(0), rubriqueBean, "La rubrique doit être égale à Rubrique1");
    }

    @Test
    public void testGetRubriqueByLabel() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETRUBRIQUEBYLABEL");
        rubriqueBean.setIntitule("Rubrique1");
        new Expectations() {{
            rubriqueDAO.selectByLabel("Rubrique1");
            result = Collections.singletonList(rubriqueBean);
        }};
        final List<RubriqueBean> rubriqueByLabel = serviceRubrique.getRubriqueByLabel("Rubrique1");
        Assert.assertNotNull(rubriqueByLabel, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(rubriqueByLabel.size(), 1, "Le premier appel doit retourner une liste d'un élement");
        Assert.assertEquals(rubriqueByLabel.get(0), rubriqueBean, "La rubrique doit être égale à Rubrique1");
    }

    @Test
    public void testGetFirstOfCategoryInSubTree() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setCategorie("CATEGORIE");
        rubriqueBean.setId(1L);
        rubriqueBean.setCodeRubriqueMere("TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE2");
        final RubriqueBean rubriqueBean2 = new RubriqueBean();
        rubriqueBean2.setCode("TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE2");
        rubriqueBean2.setIntitule("Rubrique2");
        rubriqueBean2.setId(2L);
        rubriqueBean2.setCategorie("CATEGORIE");
        rubriqueBean2.setCodeRubriqueMere("TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE_RACINE");
        final RubriqueBean rubriqueBean3 = new RubriqueBean();
        rubriqueBean3.setCode("TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE_RACINE");
        rubriqueBean3.setIntitule("Rubrique3");
        rubriqueBean3.setId(3L);
        new Expectations() {{
            rubriqueDAO.selectByCategories(Collections.singletonList("CATEGORIE"));
            result = Arrays.asList(rubriqueBean2, rubriqueBean);
            rubriqueDAO.selectByCode("TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE_RACINE");
            result = rubriqueBean3;
            rubriqueDAO.selectByCode("NO_RESULT");
            result = null;
        }};
        final RubriqueBean firstRubriquesByCategorieInSubTree = serviceRubrique.getFirstOfCategoryInSubTree("TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE_RACINE", "CATEGORIE");
        Assert.assertNotNull(firstRubriquesByCategorieInSubTree, "le résultat ne doit âs être null");
        Assert.assertEquals(firstRubriquesByCategorieInSubTree.getCode(), "TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE2");
        RubriqueBean nullExpected = serviceRubrique.getFirstOfCategoryInSubTree("TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE_RACINE", "NO_RESULT");
        Assert.assertNull(nullExpected, "il ne doit pas y avoir de résultat");
        nullExpected = serviceRubrique.getFirstOfCategoryInSubTree("NO_RESULT", "CATEGORIE");
        Assert.assertNull(nullExpected, "il ne doit pas y avoir de résultat");
        nullExpected = serviceRubrique.getFirstOfCategoryInSubTree("TEST_GETFIRSTRUBRIQUESBYCATEGORIEINSUBTREE_RACINE", StringUtils.EMPTY);
        Assert.assertNull(nullExpected, "il ne doit pas y avoir de résultat");
    }

    @Test
    public void testGetRubriquesByCategorieInSubTree() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETRUBRIQUESBYCATEGORIEINSUBTREE");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setCategorie("CATEGORIE");
        rubriqueBean.setId(1L);
        rubriqueBean.setCodeRubriqueMere("TEST_GETRUBRIQUESBYCATEGORIEINSUBTREE2");
        final RubriqueBean rubriqueBean2 = new RubriqueBean();
        rubriqueBean2.setCode("TEST_GETRUBRIQUESBYCATEGORIEINSUBTREE2");
        rubriqueBean2.setIntitule("Rubrique2");
        rubriqueBean2.setId(2L);
        rubriqueBean2.setCodeRubriqueMere("TEST_GETRUBRIQUESBYCATEGORIEINSUBTREE_RACINE");
        final RubriqueBean rubriqueBean3 = new RubriqueBean();
        rubriqueBean3.setCode("TEST_GETRUBRIQUESBYCATEGORIEINSUBTREE_RACINE");
        rubriqueBean3.setIntitule("Rubrique3");
        rubriqueBean3.setId(3L);
        rubriqueBean3.setCategorie("CATEGORIE");
        new Expectations() {{
            rubriqueDAO.selectByCategories(Collections.singletonList("CATEGORIE"));
            result = Arrays.asList(rubriqueBean3, rubriqueBean);
            rubriqueDAO.selectByCode("TEST_GETRUBRIQUESBYCATEGORIEINSUBTREE_RACINE");
            result = rubriqueBean3;
            rubriqueDAO.selectByCode("TEST_GETRUBRIQUESBYCATEGORIEINSUBTREE2");
            result = rubriqueBean2;
        }};
        final List<RubriqueBean> rubriquesByCategorieInSubTree = serviceRubrique.getRubriquesByCategorieInSubTree("TEST_GETRUBRIQUESBYCATEGORIEINSUBTREE_RACINE", Collections.singletonList("CATEGORIE"));
        Assert.assertNotNull(rubriquesByCategorieInSubTree, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(rubriquesByCategorieInSubTree.size(), 2, "Le premier appel doit retourner une liste de deux élements");
        Assert.assertEquals(rubriquesByCategorieInSubTree.get(0), rubriqueBean3, "La première rubrique doit être égale à Rubrique3");
    }


    @Test
    public void testDeterminerListeSousRubriquesAutorisees() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEES");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setGroupesDsi("GROUPE_DSI");
        rubriqueBean.setCodeRubriqueMere("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEES2");
        final RubriqueBean rubriqueBean2 = new RubriqueBean();
        rubriqueBean2.setCode("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEES2");
        rubriqueBean2.setIntitule("Rubrique2");
        rubriqueBean2.setCodeRubriqueMere("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEES3");
        new Expectations() {{
            rubriqueDAO.selectByCodeParent("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEES3");
            result = Collections.singletonList(rubriqueBean2);
            rubriqueDAO.selectByCodeParent("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEES2");
            result = Collections.singletonList(rubriqueBean);
        }};
        Collection<RubriqueBean> determinerlistesousrubriquesautorisees = serviceRubrique.determinerListeSousRubriquesAutorisees(Collections.<String>emptySet(), "TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEES3");
        Assert.assertNotNull(determinerlistesousrubriquesautorisees, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(determinerlistesousrubriquesautorisees.size(), 1, "Le premier appel doit retourner une liste d'un élement");
        final Collection<RubriqueBean> determinerlistesousrubriquesautorisees2 = serviceRubrique.determinerListeSousRubriquesAutorisees(Collections.singleton("GROUPE_DSI"), "TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEES3");
        Assert.assertNotNull(determinerlistesousrubriquesautorisees2, "Le second appel doit retourner une valeur non null");
        Assert.assertEquals(determinerlistesousrubriquesautorisees2.size(), 2, "Le second appel doit retourner une liste de deux élements");
    }

    @Test
    public void testDeterminerListeSousRubriquesAutoriseesRubriqueBean() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEESRUBRIQUEBEAN");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setGroupesDsi("GROUPE_DSI");
        rubriqueBean.setCodeRubriqueMere("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEESRUBRIQUEBEAN2");
        final RubriqueBean rubriqueBean2 = new RubriqueBean();
        rubriqueBean2.setCode("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEESRUBRIQUEBEAN2");
        rubriqueBean2.setIntitule("Rubrique2");
        rubriqueBean2.setCodeRubriqueMere("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEES3");
        final RubriqueBean rubriqueBean3 = new RubriqueBean();
        rubriqueBean3.setCode("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEESRUBRIQUEBEAN3");
        new Expectations() {{
            rubriqueDAO.selectByCodeParent("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEESRUBRIQUEBEAN3");
            result = Collections.singletonList(rubriqueBean2);
            rubriqueDAO.selectByCodeParent("TEST_DETERMINERLISTESOUSRUBRIQUESAUTORISEESRUBRIQUEBEAN2");
            result = Collections.singletonList(rubriqueBean);
        }};
        final Collection<RubriqueBean> determinerlistesousrubriquesautorisees = serviceRubrique.determinerListeSousRubriquesAutorisees(Collections.<String>emptySet(), rubriqueBean3);
        Assert.assertNotNull(determinerlistesousrubriquesautorisees, "Le premier appel doit retourner une valeur non null");
        Assert.assertEquals(determinerlistesousrubriquesautorisees.size(), 1, "Le premier appel doit retourner une liste d'un élement");
        final Collection<RubriqueBean> determinerlistesousrubriquesautorisees2 = serviceRubrique.determinerListeSousRubriquesAutorisees(Collections.singleton("GROUPE_DSI"), rubriqueBean3);
        Assert.assertNotNull(determinerlistesousrubriquesautorisees2, "Le second appel doit retourner une valeur non null");
        Assert.assertEquals(determinerlistesousrubriquesautorisees2.size(), 2, "Le second appel doit retourner une liste de deux élements");

    }

    @Test
    public void testControlerRestrictionRubrique() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_CONTROLERRESTRICTIONRUBRIQUE");
        rubriqueBean.setIntitule("Rubrique1");
        rubriqueBean.setGroupesDsi("GROUPE_DSI");
        rubriqueBean.setCodeRubriqueMere("TEST_CONTROLERRESTRICTIONRUBRIQUE2");
        final RubriqueBean rubriqueBean2 = new RubriqueBean();
        rubriqueBean2.setCode("TEST_CONTROLERRESTRICTIONRUBRIQUE2");
        rubriqueBean2.setIntitule("Rubrique2");
        rubriqueBean2.setCodeRubriqueMere("TEST_CONTROLERRESTRICTIONRUBRIQUE3");
        final RubriqueBean rubriqueBean3 = new RubriqueBean();
        rubriqueBean3.setCode("TEST_CONTROLERRESTRICTIONRUBRIQUE3");
        new Expectations() {{
            rubriqueDAO.selectByCode("TEST_CONTROLERRESTRICTIONRUBRIQUE");
            result = rubriqueBean;
            rubriqueDAO.selectByCode("TEST_CONTROLERRESTRICTIONRUBRIQUE2");
            result = rubriqueBean2;
        }};
        boolean restriction = serviceRubrique.controlerRestrictionRubrique(Collections.<String>emptySet(), "");
        Assert.assertTrue(restriction, "Le premier appel doit retourner une valeur vrai");
        restriction = serviceRubrique.controlerRestrictionRubrique(Collections.<String>emptySet(), "TEST_CONTROLERRESTRICTIONRUBRIQUE");
        Assert.assertFalse(restriction, "Le deuxième appel doit retourner une valeur fause");
        restriction = serviceRubrique.controlerRestrictionRubrique(Collections.<String>emptySet(), "TEST_CONTROLERRESTRICTIONRUBRIQUE2");
        Assert.assertTrue(restriction, "Le troisième appel doit retourner une valeur vrai");
        restriction = serviceRubrique.controlerRestrictionRubrique(Collections.singleton("GROUPE_DSI"), "TEST_CONTROLERRESTRICTIONRUBRIQUE");
        Assert.assertTrue(restriction, "Le quatrième appel doit retourner une valeur vrai");
    }

    @Test
    public void testGetLevel() {
        final RubriqueBean root = new RubriqueBean();
        root.setCode(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        final RubriqueBean child1 = new RubriqueBean();
        child1.setCode("CHILD1");
        child1.setCodeRubriqueMere(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        final RubriqueBean child2 = new RubriqueBean();
        child2.setCode("CHILD2");
        child2.setCodeRubriqueMere(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        final RubriqueBean child3 = new RubriqueBean();
        child3.setCode("CHILD3");
        child3.setCodeRubriqueMere("CHILD2");
        final RubriqueBean child4 = new RubriqueBean();
        child4.setCode("CHILD4");
        child4.setCodeRubriqueMere("CHILD3");
        new NonStrictExpectations() {{
            rubriqueDAO.selectByCode(ServiceRubrique.CODE_RUBRIQUE_ROOT);
            result = root;
            rubriqueDAO.selectByCode("CHILD1");
            result = child1;
            rubriqueDAO.selectByCode("CHILD2");
            result = child2;
            rubriqueDAO.selectByCode("CHILD3");
            result = child3;
            rubriqueDAO.selectByCode("CHILD4");
            result = child4;
        }};
        int levelRoot = serviceRubrique.getLevel(serviceRubrique.getRubriqueByCode(ServiceRubrique.CODE_RUBRIQUE_ROOT));
        int levelChild1 = serviceRubrique.getLevel(serviceRubrique.getRubriqueByCode("CHILD1"));
        int levelChild2 = serviceRubrique.getLevel(serviceRubrique.getRubriqueByCode("CHILD2"));
        int levelChild3 = serviceRubrique.getLevel(serviceRubrique.getRubriqueByCode("CHILD3"));
        int levelChild4 = serviceRubrique.getLevel(serviceRubrique.getRubriqueByCode("CHILD4"));
        Assert.assertEquals(levelRoot, 0, "Le niveau de la racine devrait être 0");
        Assert.assertEquals(levelChild1, 1, "Le niveau de l'enfant 1 devrait être 1");
        Assert.assertEquals(levelChild2, 1, "Le niveau de l'enfant 2 devrait être 1");
        Assert.assertEquals(levelChild3, 2, "Le niveau de l'enfant 3 devrait être 2");
        Assert.assertEquals(levelChild4, 3, "Le niveau de l'enfant 4 devrait être 3");
    }

    @Test
    public void testGetLevelFromParentCode() {
        final RubriqueBean root = new RubriqueBean();
        root.setCode(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        final RubriqueBean child1 = new RubriqueBean();
        child1.setCode("CHILD1");
        child1.setCodeRubriqueMere(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        final RubriqueBean child2 = new RubriqueBean();
        child2.setCode("CHILD2");
        child2.setCodeRubriqueMere(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        final RubriqueBean child3 = new RubriqueBean();
        child3.setCode("CHILD3");
        child3.setCodeRubriqueMere("CHILD2");
        final RubriqueBean child4 = new RubriqueBean();
        child4.setCode("CHILD4");
        child4.setCodeRubriqueMere("CHILD3");
        new NonStrictExpectations() {{
            rubriqueDAO.selectByCode(ServiceRubrique.CODE_RUBRIQUE_ROOT);
            result = root;
            rubriqueDAO.selectByCode("CHILD1");
            result = child1;
            rubriqueDAO.selectByCode("CHILD2");
            result = child2;
            rubriqueDAO.selectByCode("CHILD3");
            result = child3;
            rubriqueDAO.selectByCode("CHILD4");
            result = child4;
        }};
        int levelRoot = serviceRubrique.getLevelFromParentCode(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        int levelChild1 = serviceRubrique.getLevelFromParentCode("CHILD1");
        int levelChild2 = serviceRubrique.getLevelFromParentCode("CHILD2");
        int levelChild3 = serviceRubrique.getLevelFromParentCode("CHILD3");
        int levelChild4 = serviceRubrique.getLevelFromParentCode("CHILD4");
        Assert.assertEquals(levelRoot, 1, "Le niveau de la racine devrait être 1");
        Assert.assertEquals(levelChild1, 2, "Le niveau de l'enfant 1 devrait être 2");
        Assert.assertEquals(levelChild2, 2, "Le niveau de l'enfant 2 devrait être 2");
        Assert.assertEquals(levelChild3, 3, "Le niveau de l'enfant 3 devrait être 3");
        Assert.assertEquals(levelChild4, 4, "Le niveau de l'enfant 4 devrait être 4");
    }

    @Test
    public void testIsParentSection() {
        final RubriqueBean root = new RubriqueBean();
        root.setCode(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        final RubriqueBean child1 = new RubriqueBean();
        child1.setCode("TEST_ISPARENTSECTION1");
        child1.setCodeRubriqueMere(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        final RubriqueBean child2 = new RubriqueBean();
        child2.setCode("TEST_ISPARENTSECTION2");
        child2.setCodeRubriqueMere(ServiceRubrique.CODE_RUBRIQUE_ROOT);
        final RubriqueBean child3 = new RubriqueBean();
        child3.setCode("TEST_ISPARENTSECTION3");
        child3.setCodeRubriqueMere("TEST_ISPARENTSECTION2");
        final RubriqueBean child4 = new RubriqueBean();
        child4.setCode("TEST_ISPARENTSECTION4");
        child4.setCodeRubriqueMere("TEST_ISPARENTSECTION3");
        new NonStrictExpectations() {{
            rubriqueDAO.selectByCode(ServiceRubrique.CODE_RUBRIQUE_ROOT);
            result = root;
            rubriqueDAO.selectByCode("TEST_ISPARENTSECTION1");
            result = child1;
            rubriqueDAO.selectByCode("TEST_ISPARENTSECTION2");
            result = child2;
            rubriqueDAO.selectByCode("TEST_ISPARENTSECTION3");
            result = child3;
            rubriqueDAO.selectByCode("TEST_ISPARENTSECTION4");
            result = child4;
        }};
        Assert.assertTrue(serviceRubrique.isParentSection(root, root), "deux rubrique egales sont bien parentes (c'est historique)");
        Assert.assertTrue(serviceRubrique.isParentSection(root, child1), "la rubrique child1 est bien fille de root");
        Assert.assertTrue(serviceRubrique.isParentSection(root, child4), "la rubrique child4 est bien fille de root");
        Assert.assertFalse(serviceRubrique.isParentSection(child3, child1), "la rubrique child1 n'est pas fille de child3");
        Assert.assertFalse(serviceRubrique.isParentSection(child2, child1), "la rubrique child1 n'est pas fille de child2");
    }

    @Test
    public void testGetRubriquePublication(@Capturing ContexteUniv mockedCtx, @Mocked InfosSiteImpl infosSiteMocked) {
        final ContexteUniv ctx = ContexteUtil.setContexteSansRequete();
        final InfosSiteImpl site = new InfosSiteImpl();
        site.setCodeRubrique("TEST_GETRUBRIQUEPUBLICATION");
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("TEST_GETRUBRIQUEPUBLICATION");
        rubriqueBean.setIntitule("Rubrique1");
        new Expectations() {{
            ctx.getInfosSite();
            result = site;
            ctx.getGroupesDsiAvecAscendants();
            result = StringUtils.EMPTY;
            site.isRubriqueVisibleInSite((RubriqueBean)any);
            result = true;
            rubriqueDAO.selectByCode("TEST_GETRUBRIQUEPUBLICATION");
            result = rubriqueBean;
        }};
        String rubriquePublication = serviceRubrique.getRubriquePublication(ctx, rubriqueBean, Collections.singletonList("TEST_GETRUBRIQUEPUBLICATION"), false);
        Assert.assertEquals(rubriquePublication, "TEST_GETRUBRIQUEPUBLICATION", "le resultat doit être TEST_GETRUBRIQUEPUBLICATION");
        rubriquePublication = serviceRubrique.getRubriquePublication(ctx, rubriqueBean, Collections.singletonList("TEST_GETRUBRIQUEPUBLICATION"), true);
        Assert.assertEquals(rubriquePublication, "TEST_GETRUBRIQUEPUBLICATION", "le resultat doit être TEST_GETRUBRIQUEPUBLICATION");
        rubriquePublication = serviceRubrique.getRubriquePublication(ctx, rubriqueBean, Collections.<String>emptyList(), true);
        Assert.assertNull(rubriquePublication, "le resultat doit tre null");
    }
}
