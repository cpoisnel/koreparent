package com.univ.objetspartages.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.kportal.core.config.PropertyHelper;
import com.kportal.extension.module.plugin.toolbox.PluginTagHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.dao.impl.MetatagDAO;
import com.univ.objetspartages.om.Article;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

import mockit.Expectations;
import mockit.Mock;
import mockit.Mocked;

/**
 * Created by olivier.camon on 02/11/15.
 */
@Test
@ContextConfiguration("ServiceMetatagTest.test-context.xml")
public class ServiceMetatagTest extends AbstractTestNGSpringContextTests {

    @Autowired
    ServiceMetatag serviceMetatag;

    @Mocked
    MetatagDAO metatagDAO;

    @BeforeClass
    public void initMock() {
        metatagDAO = new MetatagDAO();
        serviceMetatag.setDao(metatagDAO);
    }

    @Test
    public void testAddWithForcedId() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaIdFiche(2L);
        metatagBean.setMetaCodeObjet("CODE");
        metatagBean.setMetaLibelleObjet("META_LIBELLE_OBJET");
        metatagBean.setMetaHistorique("META_HISTORIQUE");
        metatagBean.setMetaDateArchivage(new Date(0));
        metatagBean.setMetaListeReferences("META_LISTE_REFERENCES");
        metatagBean.setMetaForum("0");
        metatagBean.setMetaForumAno("0");
        metatagBean.setMetaSaisieFront("0");
        metatagBean.setMetaMailAnonyme("META_MAIL_ANONYME");
        metatagBean.setMetaNotificationMail("0");
        metatagBean.setMetaInTree("0");
        metatagBean.setMetaDocumentFichiergw("0");
        metatagBean.setMetaRubriquesPublication("META_RUBRIQUES_PUBLICATION");
        metatagBean.setMetaNiveauApprobation("MNA");
        metatagBean.setMetaLibelleFiche("META_LIBELLE_FICHE");
        metatagBean.setMetaCode("META_CODE");
        metatagBean.setMetaCodeRattachement("META_CODE_RATTACHEMENT");
        metatagBean.setMetaCodeRubrique("META_CODE_RUBRIQUE");
        metatagBean.setMetaKeywords("META_META_KEYWORDS");
        metatagBean.setMetaDescription("META_META_DESCRIPTION");
        metatagBean.setMetaDateCreation(new Date(0));
        metatagBean.setMetaDateProposition(new Date(0));
        metatagBean.setMetaDateValidation(new Date(0));
        metatagBean.setMetaDateModification(new Date(0));
        metatagBean.setMetaDateOperation(new Date(0));
        metatagBean.setMetaDateMiseEnLigne(new Date(0));
        metatagBean.setMetaDateSuppression(new Date(0));
        metatagBean.setMetaDateRubriquage(new Date(0));
        metatagBean.setMetaCodeRedacteur("META_CODE_REDACTEUR");
        metatagBean.setMetaCodeValidation("META_CODE_VALIDATION");
        metatagBean.setMetaLangue("0");
        metatagBean.setMetaEtatObjet(EtatFiche.EN_LIGNE.toString());
        metatagBean.setMetaNbHits(0L);
        metatagBean.setMetaSourceImport("META_SOURCE_IMPORT");
        metatagBean.setMetaCodeRattachementAutres("META_CODE_RATTACHEMENT_AUTRES");
        metatagBean.setMetaDiffusionPublicVise("META_DIFFUSION_PUBLIC_VISE");
        metatagBean.setMetaDiffusionModeRestriction("0");
        metatagBean.setMetaDiffusionPublicViseRestriction("META_DIFFUSION_PUBLIC_VISE_RESTRICTION");
        metatagBean.setMetaCodeRubriquage("META_CODE_RUBRIQUAGE");
        new Expectations() {{
            metatagDAO.addWithForcedId(metatagBean);
            result = metatagBean;
        }};
        final MetatagBean result = serviceMetatag.addWithForcedId(metatagBean);
        Assert.assertNotNull(result, "le résultat ne peut pas être null si l'on ne passe pas null");
        Assert.assertEquals(result, metatagBean, "le résultat ne doit pas être changé");
    }

    @Test
    public void testGetByCodeAndIdFiche() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("CODE_OBJET");
        metatagBean.setMetaIdFiche(1L);
        new Expectations() {{
            metatagDAO.getByCodeAndIdFiche("CODE_OBJET", 1L);
            result = metatagBean;
            metatagDAO.getByCodeAndIdFiche("NO_RESULT", 0L);
            result = null;
        }};
        MetatagBean result = serviceMetatag.getByCodeAndIdFiche("CODE_OBJET", 1L);
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertEquals(result, metatagBean, "le résultat doit être le premier element");
        result = serviceMetatag.getByCodeAndIdFiche("NO_RESULT", 0L);
        Assert.assertNull(result, "le résultat doit être null");
    }

    @Test
    public void testGetMetasSince(@Mocked(stubOutClassInitialization = true) PropertyHelper mockedPropertyHelper) {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("CODE_OBJET");
        metatagBean.setMetaIdFiche(1L);
        final Date currentDate = new Date();
        new Expectations() {{
            PropertyHelper.getCoreProperty("search.restriction_objet");
            result = StringUtils.EMPTY;
            metatagDAO.getMetasSinceExludingObjects(currentDate, Collections.<String>emptyList());
            result = Collections.emptyList();
            metatagDAO.getAllMetas(Collections.<String>emptyList());
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getMetasSince(currentDate);
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "il ne doit pas y avoir de résultat");
        result = serviceMetatag.getMetasSince(null);
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(result.get(0), metatagBean, "le résultat doit correspondre au meta initialisé");
    }

    @Test
    public void testGetMetasForRubriquesAndObjet() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("CODE_OBJET");
        metatagBean.setMetaIdFiche(1L);
        new Expectations() {{
            metatagDAO.getMetasForRubriquesAndObjet(Collections.<String>emptyList(), "NO_RESULT");
            result = Collections.emptyList();
            metatagDAO.getMetasForRubriquesAndObjet(Collections.<String>emptyList(), "CODE_OBJET");
            result = Collections.singletonList(metatagBean);
            metatagDAO.getMetasForRubriquesAndObjet(Collections.<String>emptyList(), "CODE_OBJET", "NO_RESULT");
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getMetasForRubriquesAndObjet(Collections.<String>emptyList(), "NO_RESULT");
        Assert.assertNotNull(result, "le resultat ne peut être null");
        Assert.assertTrue(result.isEmpty(), "il ne doit pas y avoir de résultat");
        result = serviceMetatag.getMetasForRubriquesAndObjet(Collections.<String>emptyList(), "CODE_OBJET");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(result.get(0), metatagBean, "le résultat doit correspondre au meta initialisé");
        result = serviceMetatag.getMetasForRubriquesAndObjet(Collections.<String>emptyList(), "CODE_OBJET", "NO_RESULT");
        Assert.assertNotNull(result, "le résultat ne peut être null");
        Assert.assertEquals(result.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(result.get(0), metatagBean, "le résultat doit correspondre au meta initialisé");
    }

    @Test
    public void testGetByObjectCode() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("codeObjet");
        new Expectations() {{
            metatagDAO.getByObjectCode("noResult");
            result = Collections.<MetatagBean>emptyList();
            metatagDAO.getByObjectCode("codeObjet");
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getByObjectCode("codeObjet");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir un element");
        Assert.assertEquals(result.get(0), metatagBean, "le résultat doit correspondre au groupe utilisateur ayant pour code utilisateur codeUtilisateur et code groupe : codeGroupe");
        result = serviceMetatag.getByObjectCode("noResult");
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }


    @Test
    public void testDeleteForCodeAndFicheIds() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("codeObjet");
        metatagBean.setMetaIdFiche(1L);
        new Expectations() {{
            metatagDAO.add(metatagBean);
            result = metatagBean;
            metatagDAO.getByCodeAndIdFiche("codeObjet", 1L);
            result = null;
        }};
        serviceMetatag.save(metatagBean);
        serviceMetatag.deleteForCodeAndFicheIds("codeObjet", Collections.singletonList(1L));
        MetatagBean result = serviceMetatag.getByCodeAndIdFiche("codeObjet", 1L);
        Assert.assertNull(result, "le resultat doit être null");
    }

    @Test
    public void testGetByCodeAndIdsFiche() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("codeObjet");
        metatagBean.setMetaIdFiche(1L);
        final MetatagBean metatagBean2 = new MetatagBean();
        metatagBean2.setMetaCodeObjet("codeObjet");
        metatagBean2.setMetaIdFiche(2L);
        new Expectations() {{
            metatagDAO.getByCodeAndIdsFiche("noResult", 1L);
            result = Collections.<MetatagBean>emptyList();
            metatagDAO.getByCodeAndIdsFiche("codeObjet", 1L);
            result = Collections.singletonList(metatagBean);
            metatagDAO.getByCodeAndIdsFiche("codeObjet", 1L, 2L);
            result = Arrays.asList(metatagBean, metatagBean2);
        }};
        List<MetatagBean> result = serviceMetatag.getByCodeAndIdsFiche("codeObjet", 1L);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
        result = serviceMetatag.getByCodeAndIdsFiche("codeObjet", 1L, 2L);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 2, "le resultat doit contenir deux élements");
        Assert.assertEquals(result.get(0), metatagBean, "le premier resultat doit être égal à metatagBean");
        result = serviceMetatag.getByCodeAndIdsFiche("noResult", 1L);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getByCodeAndIdsFiche("noResult");
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
    }

    @Test
    public void testGetByIds() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setId(1L);
        final MetatagBean metatagBean2 = new MetatagBean();
        metatagBean2.setId(2L);
        new Expectations() {{
            metatagDAO.getByIds(3L);
            result = Collections.<MetatagBean>emptyList();
            metatagDAO.getByIds(1L);
            result = Collections.singletonList(metatagBean);
            metatagDAO.getByIds(1L, 2L);
            result = Arrays.asList(metatagBean, metatagBean2);
        }};
        List<MetatagBean> result = serviceMetatag.getByIds(1L);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
        result = serviceMetatag.getByIds(1L, 2L);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 2, "le resultat doit contenir deux élements");
        Assert.assertEquals(result.get(0), metatagBean, "le premier resultat doit être égal à metatagBean");
        result = serviceMetatag.getByIds(3L);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getByIds();
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
    }

    @Test
    public void testGetMetasForRubrique() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeRubrique("codeRubrique");
        new Expectations() {{
            metatagDAO.getMetasForRubrique("noResult");
            result = Collections.<MetatagBean>emptyList();
            metatagDAO.getMetasForRubrique(null);
            result = Collections.<MetatagBean>emptyList();
            metatagDAO.getMetasForRubrique("codeRubrique");
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getMetasForRubrique("codeRubrique");
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
        result = serviceMetatag.getMetasForRubrique("noResult");
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getMetasForRubrique(null);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
    }

    @Test
    public void testGetMetasForRubriquesAndStates() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeRubrique("codeRubrique");
        metatagBean.setMetaEtatObjet("0003");
        final MetatagBean metatagBean2 = new MetatagBean();
        metatagBean2.setMetaCodeRubrique("codeRubrique");
        metatagBean2.setMetaEtatObjet("0004");
        new Expectations() {{
            metatagDAO.getMetasForRubriqueAndStates("noResult", Collections.<String>emptyList());
            result = Collections.<MetatagBean>emptyList();
            metatagDAO.getMetasForRubriqueAndStates(null, null);
            result = Collections.<MetatagBean>emptyList();
            metatagDAO.getMetasForRubriqueAndStates("codeRubrique", Collections.singletonList("0003"));
            result = Collections.singletonList(metatagBean);
            metatagDAO.getMetasForRubriqueAndStates("codeRubrique", Arrays.asList("0003", "0004"));
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getMetasForRubriqueAndStates("codeRubrique", Collections.singletonList("0003"));
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
        result = serviceMetatag.getMetasForRubriqueAndStates("codeRubrique", Arrays.asList("0003", "0004"));
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
        result = serviceMetatag.getMetasForRubriqueAndStates("noResult", Collections.<String>emptyList());
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getMetasForRubriqueAndStates(null, null);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
    }

    @Test
    public void testGetListeObjetsRedacteur() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeRedacteur("userCode");
        metatagBean.setMetaCodeObjet("codeObjet1");
        final MetatagBean metatagBean2 = new MetatagBean();
        metatagBean2.setMetaCodeRedacteur("userCode");
        metatagBean2.setMetaCodeObjet("codeObjet2");
        new Expectations() {{
            metatagDAO.getByUserCode("noResult");
            result = Collections.<MetatagBean>emptyList();
            metatagDAO.getByUserCode("userCode");
            result = Arrays.asList(metatagBean, metatagBean2);
        }};
        Map<String, String> result = serviceMetatag.getListeObjetsRedacteur("userCode");
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 2, "le resultat doit contenir deux élements");
        Assert.assertTrue(result.containsKey("codeObjet1"), "la map doit contenir codeObjet1 en clé");
        Assert.assertTrue(result.containsKey("codeObjet2"), "la map doit contenir codeObjet2 en clé");
        result = serviceMetatag.getListeObjetsRedacteur("noResult");
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
    }

    @Test
    public void testGetLastModificationDate() throws ParseException {
        final MetatagBean metaWithoutHistory = new MetatagBean();
        final MetatagBean metaWithIncorrectHistory = new MetatagBean();
        metaWithIncorrectHistory.setMetaHistorique("foobar");
        final MetatagBean metaWithHistory = new MetatagBean();
        final String currentDate = "20140314174437";
        final DateFormat format = new SimpleDateFormat("YYYYMMDDHHmmss");
        final Date expectedResult = format.parse(currentDate);
        metaWithHistory.setMetaHistorique("[MODIFICATION/" + currentDate + "/skosmos/0003];[MODIFICATION/20140108165916/skosmos/0003];[MODIFICATION/20100610112942/skosmos/0003];[MODIFICATION/20100526121257/skosmos/0003];[MODIFICATION/20100526121257/skosmos/0003];[MODIFICATION/20100526111933/skosmos/0003];[MODIFICATION/20100524144547/skosmos/0003];[MODIFICATION/20100524144214/skosmos/0003];[MODIFICATION/20100510153233/skosmos/0003];[MODIFICATION/20100504105539/skosmos/0003];[MODIFICATION/20100422162114/skosmos/0003];[MODIFICATION/20100422161515/skosmos/0003];[MODIFICATION/20100420165908/skosmos/0003];[MODIFICATION/20100420165100/skosmos/0003];[MODIFICATION/20100420112330/skosmos/0003];[MODIFICATION/20100420111944/skosmos/0003];[MODIFICATION/20100420111906/skosmos/0003];[MODIFICATION/20100420104418/skosmos/0003];[MODIFICATION/20100420102451/skosmos/0003];[MODIFICATION/20100420102158/skosmos/0003];");
        Date result = serviceMetatag.getLastModificationDate(metaWithoutHistory);
        Assert.assertNull(result, "le résultat doit être null lorsque l'on a pas d'historique");
        result = serviceMetatag.getLastModificationDate(metaWithIncorrectHistory);
        Assert.assertNull(result, "le résultat doit être null lorsque l'historique n'est pas correct");
        result = serviceMetatag.getLastModificationDate(metaWithHistory);
        Assert.assertNotNull(result, "le résultat ne doit pas être null si l'historique est correct");
        Assert.assertEquals(result, expectedResult, "la date doit correspondre à la date 14/03/2014 à 17h44 37s");
    }

    @Test
    public void testControlerCoherenceAvecFiche() {
        final Article article = new EmptyArticleMockup().getMockInstance();
        final MetatagBean metatagBean = new MetatagBean();
        List<String> result = serviceMetatag.controlerCoherenceAvecFiche(metatagBean, article);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
        final Article articleWithDiff = new EmptyArticleMockup() {

            @Mock
            public String getCode() {
                return "codeDiff";
            }
        }.getMockInstance();
        result = serviceMetatag.controlerCoherenceAvecFiche(metatagBean, articleWithDiff);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir une entrée");
        Assert.assertEquals(result.get(0), "champ: code fiche: codeDiff meta:", "le résultat doit contenir une entrée");
    }

    @Test
    public void testSynchroniser(@Mocked(stubOutClassInitialization = true)ReferentielObjets mockedReferentiel) {
        final Article article = new EmptyArticleMockup().getMockInstance();
        final MetatagBean metatagBean = new MetatagBean();
        new Expectations() {{
            ReferentielObjets.getLibelleObjet(anyString);
            result = "article";
        }};
        serviceMetatag.synchroniser(metatagBean, article, false);
        List<String> result = serviceMetatag.controlerCoherenceAvecFiche(metatagBean, article);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
        final Article articleWithCode = new EmptyArticleMockup() {

            @Mock
            public String getCode() {
                return "code";
            }
        }.getMockInstance();
        serviceMetatag.synchroniser(metatagBean, articleWithCode, false);
        result = serviceMetatag.controlerCoherenceAvecFiche(metatagBean, articleWithCode);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
    }

    @Test
    public void testCalculerReferences(@Mocked(stubOutClassInitialization = true)PluginTagHelper pluginTagHelper) {
        final Article article = new EmptyArticleMockup().getMockInstance();
        final MetatagBean metatagBean = new MetatagBean();
        new Expectations() {{
            PluginTagHelper.getReferencesTags(anyString);
            result = StringUtils.EMPTY;
        }};
        serviceMetatag.calculerReferences(metatagBean, article.getReferences(), article.getContenuEncadre(), article.getLangue());
        Assert.assertEquals(metatagBean.getMetaListeReferences(), StringUtils.EMPTY, "les références doivent être vide");
        final Article articleWithReferences = new EmptyArticleMockup() {
            @Mock
            public String getReferences() {
                return "[id-fiche]code;0[/id-fiche][id-document]code2;0[/id-document]";
            }
        }.getMockInstance();
        serviceMetatag.calculerReferences(metatagBean, articleWithReferences.getReferences(), articleWithReferences.getContenuEncadre(), articleWithReferences.getLangue());
        Assert.assertEquals(metatagBean.getMetaListeReferences(), "[code;0;][document;code2;0;]", "les références doivent contenir une référence à une fiche et une référence à un document");
        final Article articleWithReferencesAndLocale = new EmptyArticleMockup() {
            @Mock
            public String getReferences() {
                return "[id-fiche]codeObjet;codeFiche,LANGUE=0[/id-fiche][id-document]codeFiche,LANGUE=0[/id-document]";
            }
        }.getMockInstance();
        serviceMetatag.calculerReferences(metatagBean, articleWithReferencesAndLocale.getReferences(), articleWithReferencesAndLocale.getContenuEncadre(), articleWithReferencesAndLocale.getLangue());
        Assert.assertEquals(metatagBean.getMetaListeReferences(), "[codeObjet;codeFiche;0][document;codeFiche;0]", "les références doivent contenir une référence à une fiche et une référence à un document");
    }

    @Test
    public void testGetHistory() {
        final MetatagBean metaNull = null;
        final MetatagBean metaWithoutHistory = new MetatagBean();
        final MetatagBean metaWithIncorrectHistory = new MetatagBean();
        metaWithIncorrectHistory.setMetaHistorique("foobar");
        final MetatagBean metaWithHistory = new MetatagBean();
        metaWithHistory.setMetaHistorique("[MODIFICATION/20140314174437/skosmos/0003];[MODIFICATION/20140108165916/skosmos/0003];[MODIFICATION/20100610112942/skosmos/0003];[MODIFICATION/20100526121257/skosmos/0003];[MODIFICATION/20100526121257/skosmos/0003];[MODIFICATION/20100526111933/skosmos/0003];[MODIFICATION/20100524144547/skosmos/0003];[MODIFICATION/20100524144214/skosmos/0003];[MODIFICATION/20100510153233/skosmos/0003];[MODIFICATION/20100504105539/skosmos/0003];[MODIFICATION/20100422162114/skosmos/0003];[MODIFICATION/20100422161515/skosmos/0003];[MODIFICATION/20100420165908/skosmos/0003];[MODIFICATION/20100420165100/skosmos/0003];[MODIFICATION/20100420112330/skosmos/0003];[MODIFICATION/20100420111944/skosmos/0003];[MODIFICATION/20100420111906/skosmos/0003];[MODIFICATION/20100420104418/skosmos/0003];[MODIFICATION/20100420102451/skosmos/0003];[MODIFICATION/20100420102158/skosmos/0003];");
        List<String> result = serviceMetatag.getHistory(metaNull);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
        result = serviceMetatag.getHistory(metaWithoutHistory);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le résultat doit être vide");
        result = serviceMetatag.getHistory(metaWithIncorrectHistory);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le résultat doit contenir une entrée");
        Assert.assertEquals(result.get(0), "foobar", "le résultat doit contenir l'historique même si mal formatté");
        result = serviceMetatag.getHistory(metaWithHistory);
        Assert.assertNotNull(result, "le résultat ne doit pas être null");
        Assert.assertEquals(result.size(), 20, "le résultat doit contenir 20 résultats");
        Assert.assertTrue(result.contains("[MODIFICATION/20140314174437/skosmos/0003]"), "le résultat doit contenir [MODIFICATION/20140314174437/skosmos/0003]");
    }

    @Test
    public void testSetHistory() {
        final MetatagBean metaNull = null;
        final MetatagBean metaWithoutHistory = new MetatagBean();
        final MetatagBean metaWithIncorrectHistory = new MetatagBean();
        final MetatagBean metaWithHistory = new MetatagBean();
        serviceMetatag.setHistory(metaNull, null);
        Assert.assertNull(metaNull, "le résultat doit être null");
        serviceMetatag.setHistory(metaWithoutHistory, null);
        Assert.assertNotNull(metaWithoutHistory, "le résultat ne doit pas être null");
        Assert.assertNotNull(metaWithoutHistory.getMetaHistorique(), "le résultat ne doit pas être null");
        serviceMetatag.setHistory(metaWithIncorrectHistory, Collections.singletonList("foobar"));
        Assert.assertNotNull(metaWithIncorrectHistory, "le résultat ne doit pas être null");
        Assert.assertEquals(metaWithIncorrectHistory.getMetaHistorique(), "foobar", "le résultat doit être foobar");
        serviceMetatag.setHistory(metaWithHistory, Arrays.asList("[MODIFICATION/20140314174437/skosmos/0003]", "[MODIFICATION/20140108165916/skosmos/0003]", "[MODIFICATION/20100610112942/skosmos/0003]", "[MODIFICATION/20100526121257/skosmos/0003]"));
        Assert.assertNotNull(metaWithHistory, "le résultat ne doit pas être null");
        Assert.assertEquals(metaWithHistory.getMetaHistorique(), "[MODIFICATION/20140314174437/skosmos/0003];[MODIFICATION/20140108165916/skosmos/0003];[MODIFICATION/20100610112942/skosmos/0003];[MODIFICATION/20100526121257/skosmos/0003]",
            "le résultat doit être la chaine concatener avec des ;");
    }

    @Test
    public void testAddHistory() {
        final MetatagBean metaNull = null;
        final MetatagBean metaWithoutHistory = new MetatagBean();
        final MetatagBean metaWithIncorrectHistory = new MetatagBean();
        metaWithIncorrectHistory.setMetaHistorique("foobar");
        final MetatagBean metaWithHistory = new MetatagBean();
        metaWithHistory.setMetaHistorique("[MODIFICATION/20140314174437/skosmos/0003];[MODIFICATION/20140108165916/skosmos/0003];[MODIFICATION/20100610112942/skosmos/0003];[MODIFICATION/20100526121257/skosmos/0003];[MODIFICATION/20100526121257/skosmos/0003];[MODIFICATION/20100526111933/skosmos/0003];[MODIFICATION/20100524144547/skosmos/0003];[MODIFICATION/20100524144214/skosmos/0003];[MODIFICATION/20100510153233/skosmos/0003];[MODIFICATION/20100504105539/skosmos/0003];[MODIFICATION/20100422162114/skosmos/0003];[MODIFICATION/20100422161515/skosmos/0003];[MODIFICATION/20100420165908/skosmos/0003];[MODIFICATION/20100420165100/skosmos/0003];[MODIFICATION/20100420112330/skosmos/0003];[MODIFICATION/20100420111944/skosmos/0003];[MODIFICATION/20100420111906/skosmos/0003];[MODIFICATION/20100420104418/skosmos/0003];[MODIFICATION/20100420102451/skosmos/0003];[MODIFICATION/20100420102158/skosmos/0003];");
        serviceMetatag.addHistory(metaNull, null, null, null);
        Assert.assertNull(metaNull, "le résultat doit être null");
        serviceMetatag.addHistory(metaWithoutHistory, null, null, null);
        Assert.assertNotNull(metaWithoutHistory.getMetaHistorique(), "le résultat ne doit pas être null");
        Assert.assertTrue(!metaWithoutHistory.getMetaHistorique().isEmpty(), "le résultat ne doit pas être vide");
        serviceMetatag.addHistory(metaWithIncorrectHistory, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertNotNull(metaWithIncorrectHistory.getMetaHistorique(), "le résultat ne doit pas être null");
        Assert.assertTrue(metaWithIncorrectHistory.getMetaHistorique().contains("//];foobar"), "le résultat doit contenir //];foobar");
        serviceMetatag.addHistory(metaWithHistory, "MODIFICATION", "codeRedacteur", "0003");
        Assert.assertNotNull(metaWithHistory.getMetaHistorique(), "le résultat ne doit pas être null");
        Assert.assertTrue(metaWithHistory.getMetaHistorique().contains("/codeRedacteur/0003];[MODIFICATION/20140314174437/skosmos/0003];"),
            "le résultat doit être la chaine concatener avec des ;");
    }


    @Test
    public void testGetByReferences() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaListeReferences("[actualite;1167917177523;0][actualite;1167917177523;0]");
        final MetatagBean metatagBeanWithPhoto = new MetatagBean();
        metatagBeanWithPhoto.setMetaListeReferences("[actualite;1167917177523;0][actualite;1167917177523;0][photo;1]");
        new Expectations() {{
            metatagDAO.getByReferences("noResult", "noResult", "noResult", 0L);
            result = Collections.<MetatagBean>emptyList();
            metatagDAO.getByReferences("actualite", "1167917177523", "0", 0L);
            result = Collections.singletonList(metatagBean);
            metatagDAO.getByReferences("actualite", "1167917177523", "0", 1L);
            result = Collections.singletonList(metatagBeanWithPhoto);
        }};
        List<MetatagBean> result = serviceMetatag.getByReferences("noResult", "noResult", "noResult", 0L);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getByReferences("actualite", "1167917177523", "0", 0L);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
        result = serviceMetatag.getByReferences("actualite", "1167917177523", "0", 1L);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBeanWithPhoto, "le resultat doit être égal à metatagBeanWithPhoto");
    }


    @Test
    public void testGetAllMetas() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setId(1L);
        final MetatagBean metatagBean2 = new MetatagBean();
        metatagBean2.setId(2L);
        new Expectations() {{
            metatagDAO.getAllMetas((Collection<String>)any);
            result = Arrays.asList(metatagBean, metatagBean2);
        }};
        List<MetatagBean> result = serviceMetatag.getAllMetas();
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 2, "le resultat doit contenir deux élements");
        Assert.assertEquals(result.get(0), metatagBean, "le premier resultat doit être égal à metatagBean");
    }

    @Test
    public void testGetMetas() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setId(1L);
        final MetatagBean metatagBean2 = new MetatagBean();
        metatagBean2.setId(2L);
        new Expectations() {{
            metatagDAO.getMetas(5);
            result = Arrays.asList(metatagBean, metatagBean2);
            metatagDAO.getMetas(1);
            result = Collections.singletonList(metatagBean);
            metatagDAO.getMetas(0);
            result = Collections.emptyList();
        }};
        List<MetatagBean> result = serviceMetatag.getMetas(0);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getMetas(1);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
        result = serviceMetatag.getMetas(5);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 2, "le resultat doit contenir deux élements");
        Assert.assertEquals(result.get(0), metatagBean, "le premier resultat doit être égal à metatagBean");
    }


    @Test
    public void testGetByMetaCodeObjetCodeLangueEtat() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("metaCodeObjet");
        metatagBean.setMetaCode("code");
        metatagBean.setMetaLangue("0");
        metatagBean.setMetaEtatObjet("0003");
        final MetatagBean metatagBean2 = new MetatagBean();
        metatagBean2.setMetaCodeObjet("metaCodeObjet");
        metatagBean2.setMetaCode("code");
        metatagBean2.setMetaLangue("0");
        metatagBean2.setMetaEtatObjet("0004");
        new Expectations() {{
            metatagDAO.getByMetaCodeObjetCodeLangueEtat("noResult", "noResult", "noResult", "noResult");
            result = null;
            metatagDAO.getByMetaCodeObjetCodeLangueEtat("metaCodeObjet", "code", "0", "0003");
            result = metatagBean;
            metatagDAO.getByMetaCodeObjetCodeLangueEtat("metaCodeObjet", "code", "0", "0004");
            result = metatagBean2;
        }};
        MetatagBean result = serviceMetatag.getByMetaCodeObjetCodeLangueEtat("noResult", "noResult", "noResult", "noResult");
        Assert.assertNull(result, "le resultat doit être null");
        result = serviceMetatag.getByMetaCodeObjetCodeLangueEtat("metaCodeObjet", "code", "0", "0003");
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result, metatagBean, "le resultat doit être égal à metatagBean");
        result = serviceMetatag.getByMetaCodeObjetCodeLangueEtat("metaCodeObjet", "code", "0", "0004");
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result, metatagBean2, "le resultat doit être égal à metatagBean2");
    }

    @Test
    public void testGetOnlineMetas() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("metaCodeObjet");
        metatagBean.setMetaCode("code");
        metatagBean.setMetaEtatObjet("0003");
        new Expectations() {{
            metatagDAO.getMetaByObjectCodeAndState(Collections.singletonList("noResult"), EtatFiche.EN_LIGNE);
            result = Collections.emptyList();
            metatagDAO.getMetaByObjectCodeAndState(Collections.singletonList("metaCodeObjet"), EtatFiche.EN_LIGNE);
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getOnlineMetas("noResult");
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getOnlineMetas("metaCodeObjet");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
    }

    @Test
    public void testGetToValidateMetas() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("metaCodeObjet");
        metatagBean.setMetaCode("code");
        metatagBean.setMetaEtatObjet("0002");
        new Expectations() {{
            metatagDAO.getMetaByObjectCodeAndState(Collections.singletonList("noResult"), EtatFiche.A_VALIDER);
            result = Collections.emptyList();
            metatagDAO.getMetaByObjectCodeAndState(Collections.singletonList("metaCodeObjet"), EtatFiche.A_VALIDER);
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getToValidateMetas(Collections.singletonList("noResult"));
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getToValidateMetas(Collections.singletonList("metaCodeObjet"));
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
    }

    @Test
    public void testGetByCodeRedacteurAndCodeRubrique() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeRubrique("codeRubrique");
        metatagBean.setMetaCodeRedacteur("codeRedacteur");
        new Expectations() {{
            metatagDAO.getByCodeRedacteurAndCodeRubrique("noResult", "noResult");
            result = Collections.emptyList();
            metatagDAO.getByCodeRedacteurAndCodeRubrique("codeRubrique", "codeRedacteur");
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getByCodeRedacteurAndCodeRubrique("noResult", "noResult");
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getByCodeRedacteurAndCodeRubrique("codeRubrique", "codeRedacteur");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
    }

    @Test
    public void testGetByPhotoReferences() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaListeReferences("[photo;foobar]");
        new Expectations() {{
            metatagDAO.getByPhotoReferences();
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getByPhotoReferences();
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
    }

    @Test
    public void testGetFromRequest() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("metaCodeObjet");
        final RequeteSQL requeteSQL = new RequeteSQL();
        final ClauseWhere clauseWhere = new ClauseWhere(ConditionHelper.egalVarchar("META_CODE_OBJET", "metaCodeObjet"));
        requeteSQL.where(clauseWhere);
        new Expectations() {{
            metatagDAO.select(requeteSQL.formaterRequete());
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getFromRequest(null);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getFromRequest(requeteSQL);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
    }

    @Test
    public void testGetFromWhereClause() {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaCodeObjet("metaCodeObjet");
        final ClauseWhere clauseWhere = new ClauseWhere(ConditionHelper.egalVarchar("META_CODE_OBJET", "metaCodeObjet"));
        new Expectations() {{
            metatagDAO.select(clauseWhere.formaterSQL());
            result = Collections.singletonList(metatagBean);
        }};
        List<MetatagBean> result = serviceMetatag.getFromWhereClause(null);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertTrue(result.isEmpty(), "le resultat doit être vide");
        result = serviceMetatag.getFromWhereClause(clauseWhere);
        Assert.assertNotNull(result, "le resultat ne doit pas être null");
        Assert.assertEquals(result.size(), 1, "le resultat doit contenir un élement");
        Assert.assertEquals(result.get(0), metatagBean, "le resultat doit être égal à metatagBean");
    }

}
