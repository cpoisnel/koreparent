package com.univ.objetspartages.dao.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.LabelBean;

/**
 * Created by olivier.camon on 01/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/LabelDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class LabelDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    LabelDAO labelDAO;

    @Test
    @DatabaseSetup("LabelDAOTest.add.xml")
    @ExpectedDatabase("LabelDAOTest.add-expected.xml")
    public void testAdd() {
        final LabelBean labelBean = new LabelBean();
        labelBean.setType("TYPE");
        labelBean.setCode("CODE2");
        labelBean.setLibelle("LIBELLE2");
        labelBean.setLangue("0");
        labelDAO.add(labelBean);
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    @ExpectedDatabase("LabelDAOTest.update-expected.xml")
    public void testUpdate() {
        final LabelBean labelBean = labelDAO.getById(2L);
        labelBean.setLangue("1");
        labelDAO.update(labelBean);
    }

    @Test
    @DatabaseSetup("LabelDAOTest.add-expected.xml")
    @ExpectedDatabase("LabelDAOTest.add.xml")
    public void testDelete() {
        labelDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    public void testAll() {
        List<LabelBean> results = labelDAO.getAll();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    public void testByTypeCodeLanguage() {
        LabelBean labelBean = labelDAO.getByTypeCodeLanguage("TYPE", "CODE2", "0");
        Assert.assertNotNull(labelBean);
        Assert.assertEquals(labelBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    public void testByTypeCodesLanguage() {
        List<LabelBean> results = labelDAO.getByTypeCodesLanguage("TYPE", Collections.singletonList("CODE"), "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    public void testByTypeLanguage() {
        List<LabelBean> results = labelDAO.getByTypeLanguage("TYPE", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    public void testByTypeLibelle() {
        List<LabelBean> results = labelDAO.getByTypeLibelle("TYPE", "LIBELLE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    public void testByTypeCode() {
        List<LabelBean> results = labelDAO.getByTypeCode("TYPE", "CODE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    public void testByCodeLanguage() {
        List<LabelBean> results = labelDAO.getByCodeLanguage("CODE", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    public void testByLibelleTypeLanguage() {
        LabelBean labelBean = labelDAO.getByLibelleTypeLanguage("LIBELLE", "TYPE", "0");
        Assert.assertNotNull(labelBean);
        Assert.assertEquals(labelBean.getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("LabelDAOTest.update.xml")
    public void testByType() {
        List<LabelBean> results = labelDAO.getByType("TYPE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

}
