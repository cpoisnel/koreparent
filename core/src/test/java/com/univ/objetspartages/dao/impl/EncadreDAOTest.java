package com.univ.objetspartages.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceRubriqueMock;
import com.univ.objetspartages.services.ServiceStructure;

import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

/**
 * Created on 27/05/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/EncadreDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class EncadreDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    EncadreDAO encadreDAO;

    @BeforeClass
    public void initMock() {
        encadreDAO.setServiceRubrique(new ServiceRubriqueMock().getMockInstance());
    }

    @Test
    @DatabaseSetup("EncadreDAOTest.add.xml")
    @ExpectedDatabase("EncadreDAOTest.add-expected.xml")
    public void testAdd() {
        final EncadreBean encadreBean = new EncadreBean();
        encadreBean.setIntitule("Test encadré 2");
        encadreBean.setActif("1");
        encadreBean.setLangue("0");
        encadreBean.setPoids(2);
        encadreBean.setContenu("Test 2");
        encadreBean.setObjets("Test 2/");
        encadreBean.setCode("2");
        encadreBean.setCodeRattachement("2");
        encadreBean.setCodeRubrique("2");
        encadreDAO.add(encadreBean);
    }

    @Test
    @DatabaseSetup("EncadreDAOTest.update.xml")
    @ExpectedDatabase("EncadreDAOTest.update-expected.xml")
    public void testUpdate() {
        final EncadreBean encadreBean = encadreDAO.getById(2L);
        encadreBean.setIntitule("Test encadré 3");
        encadreBean.setActif("1");
        encadreBean.setLangue("0");
        encadreBean.setPoids(3);
        encadreBean.setContenu("Test 3");
        encadreBean.setObjets("Test 3/");
        encadreBean.setCode("3");
        encadreBean.setCodeRattachement("3");
        encadreBean.setCodeRubrique("3");
        encadreDAO.update(encadreBean);
    }

    @Test
    @DatabaseSetup("EncadreDAOTest.add-expected.xml")
    @ExpectedDatabase("EncadreDAOTest.add.xml")
    public void testDelete() {
        encadreDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("EncadreDAOTest.add-expected.xml")
    public void testByCode() {
        final EncadreBean encadreBean = encadreDAO.getByCode("2");
        Assert.assertNotNull(encadreBean);
        Assert.assertEquals(encadreBean.getIntitule(), "Test encadré 2");
    }

    @Test
    @DatabaseSetup("EncadreDAOTest.add-expected.xml")
    public void testByCodesRubrique() {
        final List<String> codes = new ArrayList<>();
        codes.add("1");
        codes.add("2");
        final List<EncadreBean> encadres = encadreDAO.getByCodesRubrique(codes);
        Assert.assertNotNull(encadres);
        Assert.assertTrue(encadres.size() == 2, "Cette méthode devrait retourner exactement 2 éléments");
    }

    @Test
    @DatabaseSetup("EncadreDAOTest.add-expected.xml")
    public void testAllEncadres() {
        final List<EncadreBean> encadres = encadreDAO.getAllEncadres();
        Assert.assertNotNull(encadres);
        Assert.assertTrue(encadres.size() == 2, "Cette méthode devrait retourner exactement 2 éléments");
    }

    @Test
    @DatabaseSetup("EncadreDAOTest.add-expected.xml")
    public void testByCodesRubriques() {
        final List<String> codes = new ArrayList<>();
        codes.add("1");
        codes.add("2");
        encadreDAO.deleteByCodesRubriques(codes);
        final List<EncadreBean> encadres = encadreDAO.getAllEncadres();
        Assert.assertNotNull(encadres);
        Assert.assertTrue(CollectionUtils.isEmpty(encadres), "Cette méthode ne devrait retourner aucun élément");
    }

    //String codeRubrique, String codeStructure, String codeObjet, String langue) {
    @Test
    @DatabaseSetup("EncadreDAOTest.add-expected.xml")
    public void testEncadreObjectList(@Mocked({"getByCodeLanguage", "getLevel"}) final ServiceStructure mockedServiceStructure) {
        new MockUp<LangueUtil>() {
            @Mock
            public String getIndiceLocaleDefaut() {
                return "0";
            }
        };
        final StructureModele structure = new MockUp<StructureModele>() {}.getMockInstance();
        final ServiceStructure serviceStructure = new ServiceStructure();
        encadreDAO.setServiceStructure(serviceStructure);
        new Expectations() {{
            serviceStructure.getByCodeLanguage(anyString, anyString);
            result = structure;
            serviceStructure.getLevel(structure);
            result = 0;
        }};
        final List<EncadreBean> encadres = encadreDAO.getEncadreObjectList("1", "1", "Test", "0");
        Assert.assertNotNull(encadres);
        Assert.assertEquals(encadres.size(), 1, "Cette méthode devrait retourner un élément");
        Assert.assertEquals(encadres.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

}
