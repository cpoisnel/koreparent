package com.univ.objetspartages.dao.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.ProfildsiBean;

/**
 * Created by olivier.camon on 28/05/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/ProfildsiDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class ProfildsiDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    ProfildsiDAO profildsiDAO;


    @Test
    @DatabaseSetup("ProfildsiDAOTest.add.xml")
    @ExpectedDatabase("ProfildsiDAOTest.add-expected.xml")
    public void testAdd() {
        final ProfildsiBean profildsiBean = new ProfildsiBean();
        profildsiBean.setCode("CODE2");
        profildsiBean.setLibelle("LIBELLE");
        profildsiBean.setCodeRubriqueAccueil("CODE_RUBRIQUE_ACCUEIL");
        profildsiBean.setRoles("ROLES");
        profildsiBean.setGroupes(Collections.singletonList("GROUPES"));
        profildsiBean.setCodeRattachement("CODE_RATTACHEMENT");
        profildsiDAO.add(profildsiBean);
    }

    @Test
    @DatabaseSetup("ProfildsiDAOTest.add-expected.xml")
    @ExpectedDatabase("ProfildsiDAOTest.add.xml")
    public void testDelete() {
        profildsiDAO.delete(2L);
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("ProfildsiDAOTest.update.xml")
    public void testFill() {
        final ProfildsiBean profildsiBean = profildsiDAO.getById(2L);
        Assert.assertNotNull(profildsiBean);
        Assert.assertEquals(profildsiBean.getCode(), "CODE2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("ProfildsiDAOTest.update.xml")
    @ExpectedDatabase("ProfildsiDAOTest.update-expected.xml")
    public void testUpdate() {
        final ProfildsiBean profildsiBean = profildsiDAO.getById(2L);
        Assert.assertNotNull(profildsiBean);
        profildsiBean.setLibelle("LIBELLE2");
        profildsiDAO.update(profildsiBean);
    }


    @Test
    @DatabaseSetup("ProfildsiDAOTest.update.xml")
    public void testAll() {
        List<ProfildsiBean> results = profildsiDAO.selectAll();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("ProfildsiDAOTest.update.xml")
    public void testByCode() {
        ProfildsiBean profildsiBean = profildsiDAO.getByCode("CODE2");
        Assert.assertNotNull(profildsiBean);
        Assert.assertEquals(profildsiBean.getId(), Long.valueOf(2), "l'intitulé doit correspondre à la deuxième entrée");
    }

}
