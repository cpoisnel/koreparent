package com.univ.objetspartages.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.dbunit.dataset.DataSetException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

/**
 * Created by olivier.camon on 28/05/15.
 */


@Test
@ContextHierarchy(@ContextConfiguration(locations = { "classpath:/com/univ/objetspartages/dao/impl/UtilisateurDAOTest.test-context.xml" }))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class UtilisateurDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    private UtilisateurDAO utilisateurDAO;

    @Test
    @DatabaseSetup("UtilisateurDAOTest.add.xml")
    @ExpectedDatabase("UtilisateurDAOTest.add-expected.xml")
    public void testAdd() {
        final UtilisateurBean utilisateurBean = new UtilisateurBean();
        utilisateurBean.setCode("CODE2");
        utilisateurBean.setMotDePasse("MOT_DE_PASSE");
        utilisateurBean.setDateNaissance(new Date(0));
        utilisateurBean.setCivilite("M.");
        utilisateurBean.setNom("NOM");
        utilisateurBean.setPrenom("PRENOM");
        utilisateurBean.setCodeRattachement("CODE_RATTACHEMENT");
        utilisateurBean.setGroupes("GROUPES");
        utilisateurBean.setAdresseMail("ADRESSE_MAIL");
        utilisateurBean.setRestrictionValidation("RESTRICTION_VALIDATION");
        utilisateurBean.setExtensionModification("EXTENSION_MODIFICATION");
        utilisateurBean.setCentresInteret("CENTRES_INTERET");
        utilisateurBean.setProfilDsi("PROFIL_DSI");
        utilisateurBean.setGroupesDsi("GROUPES_DSI");
        utilisateurBean.setCodeLdap("CODE_LDAP2");
        utilisateurBean.setGroupesDsiImport("GROUPES_DSI_IMPORT");
        utilisateurBean.setRoles("ROLES");
        utilisateurBean.setDateDerniereSession(new Date(0));
        utilisateurBean.setProfilDefaut("PROFIL_DEFAUT");
        utilisateurBean.setSourceImport("SOURCE_IMPORT");
        utilisateurBean.setFormatEnvoi("0");
        utilisateurBean.setModeSaisieExpert("1");
        utilisateurBean.setTsCacheGroupes(0L);
        utilisateurDAO.add(utilisateurBean);
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.add-expected.xml")
    @ExpectedDatabase("UtilisateurDAOTest.add.xml")
    public void testDelete() {
        utilisateurDAO.delete(2L);
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testFill() {
        final UtilisateurBean utilisateurBean = utilisateurDAO.getById(2L);
        Assert.assertNotNull(utilisateurBean);
        Assert.assertEquals(utilisateurBean.getCode(), "CODE2", "le code doit correspondre à la deuxième entrée");
    }


    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testByCode() {
        UtilisateurBean utilisateurBean = utilisateurDAO.getByCode("CODE2");
        Assert.assertNotNull(utilisateurBean);
        Assert.assertEquals(utilisateurBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
        utilisateurBean = utilisateurDAO.getByCode("UNEXPECTED");
        Assert.assertNull(utilisateurBean);
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testByCodeLdap() {
        UtilisateurBean utilisateurBean = utilisateurDAO.getByCodeLdap("CODE_LDAP2", "SOURCE_IMPORT");
        Assert.assertNotNull(utilisateurBean);
        Assert.assertEquals(utilisateurBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
        utilisateurBean = utilisateurDAO.getByCodeLdap("UNEXPECTED", "SOURCE_IMPORT");
        Assert.assertNull(utilisateurBean);
        utilisateurBean = utilisateurDAO.getByCodeLdap("CODE_LDAP2", "UNEXPECTED");
        Assert.assertNull(utilisateurBean);
        utilisateurBean = utilisateurDAO.getByCodeLdap(null, null);
        Assert.assertNull(utilisateurBean);
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testFromCodeLdap() {
        UtilisateurBean utilisateurBean = utilisateurDAO.getByCodeLdap("CODE_LDAP2");
        Assert.assertNotNull(utilisateurBean);
        Assert.assertEquals(utilisateurBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
        utilisateurBean = utilisateurDAO.getByCodeLdap(null);
        Assert.assertNull(utilisateurBean);
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testByName() {
        List<UtilisateurBean> results = utilisateurDAO.getByNom("NOM");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir deux résultats");
        Assert.assertEquals(results.get(0).getCode(), "CODE", "le code doit correspondre à la premère entrée");
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testByMailAndCode() {
        List<UtilisateurBean> results= utilisateurDAO.getByMailAndCode("ADRESSE_MAIL","CODE2");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir un résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "le code doit correspondre à la deuxième entrée");
        results = utilisateurDAO.getByMailAndCode("ADRESSE_MAIL", null);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir deux résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "le code doit correspondre à la première entrée");
        results = utilisateurDAO.getByMailAndCode(null, null);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 0);
    }


    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testByCodeAndPass() {
        UtilisateurBean utilisateurBean = utilisateurDAO.getByCodeAndPass("CODE2", "MOT_DE_PASSE");
        Assert.assertNotNull(utilisateurBean);
        Assert.assertEquals(utilisateurBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
        utilisateurBean = utilisateurDAO.getByCodeAndPass("CODE2", null);
        Assert.assertNull(utilisateurBean);
        utilisateurBean = utilisateurDAO.getByCodeAndPass(null, "MOT_DE_PASSE");
        Assert.assertNull(utilisateurBean);
    }


    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testByCodes() {
        List<UtilisateurBean> results = utilisateurDAO.getByCodes(Collections.singletonList("CODE"));
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testAll() {
        List<UtilisateurBean> results = utilisateurDAO.getAllUsers();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testByImportSource() {
        List<UtilisateurBean> results = utilisateurDAO.getByImportSource("SOURCE_IMPORT");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    public void testByUser() {
        UtilisateurBean utilisateurBean = utilisateurDAO.getUser("ADRESSE_MAIL", "MOT_DE_PASSE", "CODE");
        Assert.assertNotNull(utilisateurBean);
        Assert.assertEquals(utilisateurBean.getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }


    @Test
    @DatabaseSetup("UtilisateurDAOTest.update.xml")
    @ExpectedDatabase("UtilisateurDAOTest.update-expected.xml")
    public void testUpdate() {
        final UtilisateurBean utilisateurBean = utilisateurDAO.getById(2L);
        Assert.assertNotNull(utilisateurBean);
        utilisateurBean.setMotDePasse("MOT_DE_PASSE2");
        utilisateurDAO.update(utilisateurBean);
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.select.xml")
    public void testByChaineDiffusion(@Mocked("getServiceForBean") ServiceManager mockedServiceManager, @Mocked({"getByCode", "getByParentGroup"}) final ServiceGroupeDsi mockedServiceGroupeDsi) {
        final ServiceGroupeDsi serviceGroupeDsi = new ServiceGroupeDsi();
        final GroupeDsiBean groupeDsiBean = new GroupeDsiBean();
        utilisateurDAO.setServiceGroupeDsi(serviceGroupeDsi);
        groupeDsiBean.setCode("GROUPES_DSI");
        new Expectations() {{
            serviceGroupeDsi.getByCode(anyString);
            result = groupeDsiBean;
            serviceGroupeDsi.getByParentGroup(anyString);
            result = new ArrayList<>();
        }};
        List<UtilisateurBean> results = utilisateurDAO.getByChaineDiffusion("[/GROUPES_DSI]");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("UtilisateurDAOTest.select.xml")
    public void testSelect(@Mocked("getServiceForBean") ServiceManager mockedServiceManager, @Mocked({"getByCode", "getByParentGroup"}) final ServiceGroupeDsi mockedServiceGroupeDsi,
                           @Mocked("getByCode") final ServiceProfildsi mockedProfil, @Mocked({"getByCodeLanguage", "getLevel"}) final ServiceStructure mockedServiceStructure) throws DataSetException {
        final ServiceProfildsi serviceProfildsi = new ServiceProfildsi();
        utilisateurDAO.setServiceProfildsi(serviceProfildsi);
        final ProfildsiBean profildsiBean = new ProfildsiBean();
        profildsiBean.setCode("PROFIL_DSI");
        profildsiBean.setGroupes(Collections.singletonList("GROUPES_DSI"));
        final ServiceGroupeDsi serviceGroupeDsi = new ServiceGroupeDsi();
        final GroupeDsiBean groupeDsiBean = new GroupeDsiBean();
        groupeDsiBean.setCode("GROUPES_DSI");
        final StructureModele structure = new MockUp<StructureModele>() {}.getMockInstance();
        final ServiceStructure serviceStructure = new ServiceStructure();
        new MockUp<ConditionHelper>() {
            @Mock
            public Condition getConditionStructure(final String nomColonne, String codeStructure) {
                return ConditionHelper.in(nomColonne, Collections.singleton(codeStructure));
            }
        };
        new Expectations() {{
            serviceProfildsi.getByCode(anyString);
            result = profildsiBean;
            serviceGroupeDsi.getByCode(anyString);
            result = groupeDsiBean;
            serviceGroupeDsi.getByParentGroup(anyString);
            result = new ArrayList<>();
        }};
        List<UtilisateurBean> results = utilisateurDAO.select("CODE", "NOM", "PRENOM", "[/GROUPES_DSI]", "PROFIL_DSI", "GROUPES_DSI", "CODE_RATTACHEMENT", "ADRESSE_MAIL");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

}
