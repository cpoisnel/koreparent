package com.univ.objetspartages.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.RessourceBean;

/**
 * Created on 27/05/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/RessourceDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class RessourceDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    RessourceDAO ressourceDao;

    @Test
    @DatabaseSetup("RessourceDAOTest.add.xml")
    @ExpectedDatabase("RessourceDAOTest.add-expected.xml")
    public void testAdd() {
        final RessourceBean ressourceBean = new RessourceBean();
        ressourceBean.setIdMedia(9L);
        ressourceBean.setCodeParent("30,TYPE=FICHIER_0015,NO=1");
        ressourceBean.setEtat("1");
        ressourceBean.setOrdre(0);
        ressourceDao.add(ressourceBean);
    }

    @Test
    @DatabaseSetup("RessourceDAOTest.update.xml")
    @ExpectedDatabase("RessourceDAOTest.update-expected.xml")
    public void testUpdate() {
        final RessourceBean ressourceBean = ressourceDao.getById(2L);
        Assert.assertNotNull(ressourceBean);
        ressourceBean.setIdMedia(5L);
        ressourceBean.setCodeParent("30,TYPE=FICHIER_0015");
        ressourceBean.setEtat("0");
        ressourceBean.setOrdre(5);
        ressourceDao.update(ressourceBean);
    }

    @Test
    @DatabaseSetup("RessourceDAOTest.add-expected.xml")
    @ExpectedDatabase("RessourceDAOTest.add.xml")
    public void testDelete() {
        ressourceDao.delete(2L);
    }

    @Test
    @DatabaseSetup("RessourceDAOTest.add-expected.xml")
    public void testByCodeParent() {
        final List<RessourceBean> ressources = ressourceDao.getByCodeParent("1239789414427,TYPE=LOGO_ESPACE", "ID_RESSOURCE");
        Assert.assertNotNull(ressources);
        Assert.assertTrue(ressources.size() == 1, "Une seule ressource doit être sélectionnée par ce code (1239789414427,TYPE=LOGO_ESPACE)");
        final RessourceBean ressourceBean = ressources.get(0);
        Assert.assertEquals((long) ressourceBean.getIdMedia(), 306);
        Assert.assertEquals((int) ressourceBean.getOrdre(), 0);
        Assert.assertEquals(ressourceBean.getEtat(), "0");
    }

    @Test
    @DatabaseSetup("RessourceDAOTest.add-expected.xml")
    public void testRessourceToPurge() {
        final List<RessourceBean> ressources = ressourceDao.getRessourceToPurge();
        Assert.assertNotNull(ressources);
        Assert.assertTrue(ressources.size() == 1, "Une seule ressource doit être sélectionnée");
        final RessourceBean ressourceBean = ressources.get(0);
        Assert.assertEquals(ressourceBean.getEtat(), "0");
    }

    @Test
    @DatabaseSetup("RessourceDAOTest.add-expected.xml")
    public void testByMediaId() {
        final List<RessourceBean> ressources = ressourceDao.getByMediaId(9L);
        Assert.assertNotNull(ressources);
        Assert.assertTrue(ressources.size() == 1, "Une seule ressource doit être sélectionnée par l'idMedia 9L");
        final RessourceBean ressourceBean = ressources.get(0);
        Assert.assertEquals((int) ressourceBean.getOrdre(), 0);
        Assert.assertEquals(ressourceBean.getEtat(), "1");
    }

    @Test
    @DatabaseSetup("RessourceDAOTest.add-expected.xml")
    public void testByMediaIdAndCodeParent() {
        final RessourceBean ressource = ressourceDao.getByMediaIdAndCodeParent(306L, "1239789414427,TYPE=LOGO_ESPACE");
        Assert.assertNotNull(ressource);
        Assert.assertEquals((int) ressource.getOrdre(), 0);
        Assert.assertEquals(ressource.getEtat(), "0");
    }
}
