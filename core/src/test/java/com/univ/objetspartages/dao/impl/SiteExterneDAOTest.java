package com.univ.objetspartages.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.SiteExterneBean;

/**
 * Created by olivier.camon on 28/05/15.
 */

@Test
@ContextHierarchy(@ContextConfiguration(locations = { "classpath:/com/univ/objetspartages/dao/impl/SiteExterneDAOTest.test-context.xml" }))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class SiteExterneDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    SiteExterneDAO siteExterneDAO;
    
    @Test
    @DatabaseSetup("SiteExterneDAOTest.add.xml")
    @ExpectedDatabase("SiteExterneDAOTest.add-expected.xml")
    public void testAdd() {
        final SiteExterneBean siteExterneBean = new SiteExterneBean();
        siteExterneBean.setCode("CODE2");
        siteExterneBean.setLibelle("LIBELLE");
        siteExterneBean.setUrl("URL");
        siteExterneBean.setRegExpAccepte("REG_EXP_ACCEPTE");
        siteExterneBean.setNiveauProfondeur(0);
        siteExterneBean.setRegExpRefuse("REG_EXP_REFUSE");
        siteExterneBean.setLangue("0");
        siteExterneDAO.add(siteExterneBean);
    }

    @Test
    @DatabaseSetup("SiteExterneDAOTest.add-expected.xml")
    @ExpectedDatabase("SiteExterneDAOTest.add.xml")
    public void testDelete() {
        siteExterneDAO.delete(2L);
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("SiteExterneDAOTest.update.xml")
    public void testFill() {
        final SiteExterneBean siteExterneBean = siteExterneDAO.getById(2L);
        Assert.assertNotNull(siteExterneBean);
        Assert.assertEquals(siteExterneBean.getCode(), "CODE2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("SiteExterneDAOTest.update.xml")
    @ExpectedDatabase("SiteExterneDAOTest.update-expected.xml")
    public void testUpdate() {
        final SiteExterneBean siteExterneBean = siteExterneDAO.getById(2L);
        Assert.assertNotNull(siteExterneBean);
        siteExterneBean.setLibelle("LIBELLE2");
        siteExterneDAO.update(siteExterneBean);
    }


    @Test
    @DatabaseSetup("SiteExterneDAOTest.update.xml")
    public void testAll() {
        List<SiteExterneBean> results = siteExterneDAO.getAllSite();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }
}
