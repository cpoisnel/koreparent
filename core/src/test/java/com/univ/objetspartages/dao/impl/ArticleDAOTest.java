package com.univ.objetspartages.dao.impl;

import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.kosmos.tests.testng.TestFicheUtils;
import com.univ.objetspartages.bean.ArticleBean;
import com.univ.objetspartages.om.EtatFiche;

/**
 * Created on 27/05/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/ArticleDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class ArticleDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    ArticleDAO articleDAO;

    @Test
    @DatabaseSetup("ArticleDAOTest.add.xml")
    @ExpectedDatabase("ArticleDAOTest.add-expected.xml")
    public void testAdd() throws ParseException {
        final ArticleBean article = new ArticleBean();
        TestFicheUtils.fillCommonFields(article);
        article.setTitre("2");
        article.setSousTitre("2");
        article.setDateArticle(TestFicheUtils.simpleFormat.parse("2007-01-04"));
        article.setIdVignette(2L);
        article.setChapeau("2");
        article.setCorps("2");
        article.setThematique("2");
        article.setOrdre(2);
        articleDAO.add(article);
    }
    @Test
    @DatabaseSetup("ArticleDAOTest.add.xml")
    @ExpectedDatabase("ArticleDAOTest.add-expected.xml")
    public void testAddWithForceId() throws ParseException {
        final ArticleBean article = new ArticleBean();
        TestFicheUtils.fillCommonFields(article);
        article.setId(2L);
        article.setTitre("2");
        article.setSousTitre("2");
        article.setDateArticle(TestFicheUtils.simpleFormat.parse("2007-01-04"));
        article.setIdVignette(2L);
        article.setChapeau("2");
        article.setCorps("2");
        article.setThematique("2");
        article.setOrdre(2);
        articleDAO.addWithForcedId(article);
    }

    @Test
    @DatabaseSetup("ArticleDAOTest.update.xml")
    @ExpectedDatabase("ArticleDAOTest.update-expected.xml")
    public void testUpdate() throws ParseException {
        final ArticleBean article = articleDAO.getById(2L);
        Assert.assertNotNull(article);
        article.setTitre("3");
        article.setSousTitre("3");
        article.setDateArticle(TestFicheUtils.simpleFormat.parse("2007-01-04"));
        article.setIdVignette(3L);
        article.setChapeau("3");
        article.setCorps("3");
        article.setThematique("3");
        article.setOrdre(3);
        articleDAO.update(article);
    }

    @Test
    @DatabaseSetup("ArticleDAOTest.add-expected.xml")
    @ExpectedDatabase("ArticleDAOTest.add.xml")
    public void testDelete() {
        articleDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("ArticleDAOTest.update.xml")
    public void testParCodeRubrique() {
        List<ArticleBean> results = articleDAO.selectParCodeRubrique("CODE_RUBRIQUE", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée avec l'order by sur titre");
        results = articleDAO.selectParCodeRubrique(StringUtils.EMPTY, "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée avec l'order by sur titre");
        results = articleDAO.selectParCodeRubrique(StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée avec l'order by sur titre");
        results = articleDAO.selectParCodeRubrique("CODE_RUBRIQUE", StringUtils.EMPTY);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée avec l'order by sur titre");
        results = articleDAO.selectParCodeRubrique("CODE_INEXISTANT", StringUtils.EMPTY);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 0, "Cette méthode devrait retourner aucun élément");
    }

    @Test
    @DatabaseSetup("ArticleDAOTest.update.xml")
    public void testCodeLangueEtat() {
        List<ArticleBean> results = articleDAO.selectCodeLangueEtat("CODE", "0", EtatFiche.EN_LIGNE.toString());
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée avec l'order by sur titre");
        results = articleDAO.selectCodeLangueEtat(StringUtils.EMPTY, "0", EtatFiche.EN_LIGNE.toString());
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée avec l'order by sur titre");
        results = articleDAO.selectCodeLangueEtat(StringUtils.EMPTY, StringUtils.EMPTY, EtatFiche.EN_LIGNE.toString());
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée avec l'order by sur titre");
        results = articleDAO.selectCodeLangueEtat("CODE", StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée avec l'order by sur titre");
        results = articleDAO.selectCodeLangueEtat("CODE_INEXISTANT", StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 0, "Cette méthode devrait retourner aucun élément");
    }
}
