package com.univ.objetspartages.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.PassRequestBean;

/**
 * Created by olivier.camon on 28/05/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/PassRequestDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class PassRequestDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    PassRequestDAO passRequestDAO;

    @Test
    @DatabaseSetup("PassRequestDAOTest.add.xml")
    @ExpectedDatabase("PassRequestDAOTest.add-expected.xml")
    public void testAdd() {
        final PassRequestBean passRequestBean = new PassRequestBean();
        passRequestBean.setCode("CODE2");
        passRequestBean.setEmail("EMAIL");
        passRequestBean.setUuid(UUID.fromString("9c6137ec-1337-4265-8f24-d70bff902b76"));
        passRequestBean.setDateDemande(new Date(0));
        passRequestDAO.add(passRequestBean);
    }

    @Test
    @DatabaseSetup("PassRequestDAOTest.add-expected.xml")
    @ExpectedDatabase("PassRequestDAOTest.add.xml")
    public void testDelete() {
        passRequestDAO.delete(2L);
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("PassRequestDAOTest.update.xml")
    public void testFill() {
        final PassRequestBean passRequestBean = passRequestDAO.getById(2L);
        Assert.assertNotNull(passRequestBean);
        Assert.assertEquals(passRequestBean.getCode(), "CODE2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("PassRequestDAOTest.update.xml")
    @ExpectedDatabase("PassRequestDAOTest.update-expected.xml")
    public void testUpdate() {
        final PassRequestBean passRequestBean = passRequestDAO.getById(2L);
        Assert.assertNotNull(passRequestBean);
        passRequestBean.setEmail("EMAIL2");
        passRequestDAO.update(passRequestBean);
    }

    @Test
    @DatabaseSetup("PassRequestDAOTest.update.xml")
    public void testByRequestId() {
        final PassRequestBean passRequestBean = passRequestDAO.getByRequestId(UUID.fromString("9c6137ec-1337-4265-8f24-d70bff902b76"));
        Assert.assertNotNull(passRequestBean);
        Assert.assertEquals(passRequestBean.getCode(), "CODE2", "le code doit correspondre à la deuxième entrée");
    }


    @Test
    @DatabaseSetup("PassRequestDAOTest.update.xml")
    public void testByCode() {
        List<PassRequestBean> results = passRequestDAO.getByCode("CODE2");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit n'y avoir qu'un seul résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre au 2eme résultat");
    }
}
