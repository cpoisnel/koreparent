package com.univ.objetspartages.dao.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.ReferentielObjets;

import mockit.Expectations;
import mockit.Mocked;

/**
 * Created by olivier.camon on 29/05/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/MetatagDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public final class MetatagDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    MetatagDAO metatagDAO;

    public final static DateFormat HOUR_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @Test
    @DatabaseSetup("MetatagDAOTest.add.xml")
    @ExpectedDatabase("MetatagDAOTest.add-expected.xml")
    public void testAdd() throws ParseException {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setMetaIdFiche(2L);
        metatagBean.setMetaCodeObjet("CODE");
        metatagBean.setMetaLibelleObjet("META_LIBELLE_OBJET");
        metatagBean.setMetaHistorique("META_HISTORIQUE");
        metatagBean.setMetaDateArchivage(new Date(0));
        metatagBean.setMetaListeReferences("META_LISTE_REFERENCES");
        metatagBean.setMetaForum("0");
        metatagBean.setMetaForumAno("0");
        metatagBean.setMetaSaisieFront("0");
        metatagBean.setMetaMailAnonyme("META_MAIL_ANONYME");
        metatagBean.setMetaNotificationMail("0");
        metatagBean.setMetaInTree("0");
        metatagBean.setMetaDocumentFichiergw("0");
        metatagBean.setMetaRubriquesPublication("META_RUBRIQUES_PUBLICATION");
        metatagBean.setMetaNiveauApprobation("MNA");
        metatagBean.setMetaLibelleFiche("META_LIBELLE_FICHE");
        metatagBean.setMetaCode("META_CODE");
        metatagBean.setMetaCodeRattachement("META_CODE_RATTACHEMENT");
        metatagBean.setMetaCodeRubrique("META_CODE_RUBRIQUE");
        metatagBean.setMetaKeywords("META_META_KEYWORDS");
        metatagBean.setMetaDescription("META_META_DESCRIPTION");
        metatagBean.setMetaDateCreation(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateProposition(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateValidation(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateModification(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateOperation(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateMiseEnLigne(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateSuppression(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateRubriquage(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaCodeRedacteur("META_CODE_REDACTEUR");
        metatagBean.setMetaCodeValidation("META_CODE_VALIDATION");
        metatagBean.setMetaLangue("0");
        metatagBean.setMetaEtatObjet(EtatFiche.EN_LIGNE.toString());
        metatagBean.setMetaNbHits(0L);
        metatagBean.setMetaSourceImport("META_SOURCE_IMPORT");
        metatagBean.setMetaCodeRattachementAutres("META_CODE_RATTACHEMENT_AUTRES");
        metatagBean.setMetaDiffusionPublicVise("META_DIFFUSION_PUBLIC_VISE");
        metatagBean.setMetaDiffusionModeRestriction("0");
        metatagBean.setMetaDiffusionPublicViseRestriction("META_DIFFUSION_PUBLIC_VISE_RESTRICTION");
        metatagBean.setMetaCodeRubriquage("META_CODE_RUBRIQUAGE");
        metatagDAO.add(metatagBean);
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.add.xml")
    @ExpectedDatabase("MetatagDAOTest.add-expected.xml")
    public void testAddWithForceId() throws ParseException {
        final MetatagBean metatagBean = new MetatagBean();
        metatagBean.setId(2L);
        metatagBean.setMetaIdFiche(2L);
        metatagBean.setMetaCodeObjet("CODE");
        metatagBean.setMetaLibelleObjet("META_LIBELLE_OBJET");
        metatagBean.setMetaHistorique("META_HISTORIQUE");
        metatagBean.setMetaDateArchivage(new Date(0));
        metatagBean.setMetaListeReferences("META_LISTE_REFERENCES");
        metatagBean.setMetaForum("0");
        metatagBean.setMetaForumAno("0");
        metatagBean.setMetaSaisieFront("0");
        metatagBean.setMetaMailAnonyme("META_MAIL_ANONYME");
        metatagBean.setMetaNotificationMail("0");
        metatagBean.setMetaInTree("0");
        metatagBean.setMetaDocumentFichiergw("0");
        metatagBean.setMetaRubriquesPublication("META_RUBRIQUES_PUBLICATION");
        metatagBean.setMetaNiveauApprobation("MNA");
        metatagBean.setMetaLibelleFiche("META_LIBELLE_FICHE");
        metatagBean.setMetaCode("META_CODE");
        metatagBean.setMetaCodeRattachement("META_CODE_RATTACHEMENT");
        metatagBean.setMetaCodeRubrique("META_CODE_RUBRIQUE");
        metatagBean.setMetaKeywords("META_META_KEYWORDS");
        metatagBean.setMetaDescription("META_META_DESCRIPTION");
        metatagBean.setMetaDateCreation(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateProposition(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateValidation(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateModification(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateOperation(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateMiseEnLigne(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateSuppression(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaDateRubriquage(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        metatagBean.setMetaCodeRedacteur("META_CODE_REDACTEUR");
        metatagBean.setMetaCodeValidation("META_CODE_VALIDATION");
        metatagBean.setMetaLangue("0");
        metatagBean.setMetaEtatObjet(EtatFiche.EN_LIGNE.toString());
        metatagBean.setMetaNbHits(0L);
        metatagBean.setMetaSourceImport("META_SOURCE_IMPORT");
        metatagBean.setMetaCodeRattachementAutres("META_CODE_RATTACHEMENT_AUTRES");
        metatagBean.setMetaDiffusionPublicVise("META_DIFFUSION_PUBLIC_VISE");
        metatagBean.setMetaDiffusionModeRestriction("0");
        metatagBean.setMetaDiffusionPublicViseRestriction("META_DIFFUSION_PUBLIC_VISE_RESTRICTION");
        metatagBean.setMetaCodeRubriquage("META_CODE_RUBRIQUAGE");
        metatagDAO.addWithForcedId(metatagBean);
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.add-expected.xml")
    @ExpectedDatabase("MetatagDAOTest.add.xml")
    public void testDelete() {
        metatagDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testById() {
        final MetatagBean metatagBean = metatagDAO.getById(2L);
        Assert.assertNotNull(metatagBean);
        Assert.assertEquals(metatagBean.getMetaIdFiche(), Long.valueOf(2), "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByCodeAndIdFiche() {
        final MetatagBean metatagBean = metatagDAO.getByCodeAndIdFiche("CODE", 2L);
        Assert.assertNotNull(metatagBean);
        Assert.assertEquals(metatagBean.getMetaIdFiche(), Long.valueOf(2), "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testAllSinceExludingObjects() {
        List<MetatagBean> results = metatagDAO.getMetasSinceExludingObjects(new Date(0), Collections. < String > emptyList());
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = metatagDAO.getMetasSinceExludingObjects(new Date(), Collections.<String> emptyList());
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 0, "il doit y avoir aucun résultat");
        results = metatagDAO.getMetasSinceExludingObjects(null, null);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 0, "il doit y avoir aucun résultat");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testAll() {
        List<MetatagBean> results = metatagDAO.getAllMetas(Collections.<String>emptyList());
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }
    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testMetasForRubriquesAndObjet() {
        List<MetatagBean> results = metatagDAO.getMetasForRubriquesAndObjet(Collections.singletonList("META_CODE_RUBRIQUE"), "CODE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = metatagDAO.getMetasForRubriquesAndObjet(Collections.singletonList("META_CODE_RUBRIQUE"));
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = metatagDAO.getMetasForRubriquesAndObjet(Collections.singletonList("META_CODE_RUBRIQUE"), null);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = metatagDAO.getMetasForRubriquesAndObjet(Collections.<String>emptyList(), null);
        Assert.assertNotNull(results);
        Assert.assertTrue(results.isEmpty(), "La liste renvoyée doit être vide");
        results = metatagDAO.getMetasForRubriquesAndObjet(null, null);
        Assert.assertNotNull(results);
        Assert.assertTrue(results.isEmpty(), "La liste renvoyée doit être vide");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByObjectCode() {
        List<MetatagBean> results = metatagDAO.getByObjectCode("CODE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.add-expected.xml")
    @ExpectedDatabase("MetatagDAOTest.add.xml")
    public void testDeleteForCodeAndIds() {
        metatagDAO.deleteForCodeAndIds("CODE", Collections.singletonList(2L));
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByCodeAndIdsFiche() {
        List<MetatagBean> results = metatagDAO.getByCodeAndIdsFiche("CODE", 2L);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir un seul résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByIds() {
        List<MetatagBean> results = metatagDAO.getByIds(1L, 2L);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByRubrique() {
        List<MetatagBean> results = metatagDAO.getMetasForRubrique("META_CODE_RUBRIQUE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByRubriqueAndStates() {
        List<MetatagBean> results = metatagDAO.getMetasForRubriqueAndStates("META_CODE_RUBRIQUE", Collections.singleton("0003"));
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = metatagDAO.getMetasForRubriqueAndStates("META_CODE_RUBRIQUE", Collections.<String>emptyList());
        Assert.assertNotNull(results);
        Assert.assertTrue(CollectionUtils.isNotEmpty(results), "La liste renvoyée ne doit pas être vide");
        Assert.assertEquals(results.size(), 2, "La liste renvoyée doit contenir 2 résultats");
        results = metatagDAO.getMetasForRubriqueAndStates(StringUtils.EMPTY, Collections.<String>emptyList());
        Assert.assertNotNull(results);
        Assert.assertTrue(results.isEmpty(), "La liste renvoyée doit être vide");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByUserCode() {
        List<MetatagBean> results = metatagDAO.getByUserCode("META_CODE_REDACTEUR");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testWithLimit() {
        List<MetatagBean> results = metatagDAO.getMetas(1);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir un seul résulat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = metatagDAO.getMetas(2);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByObjectCodeAndState() {
        List<MetatagBean> results = metatagDAO.getMetaByObjectCodeAndState(Collections.singletonList("CODE"), EtatFiche.EN_LIGNE);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByMetaCodeObjetCodeLangueEtat() {
        MetatagBean metatagBean = metatagDAO.getByMetaCodeObjetCodeLangueEtat("CODE", "META_CODE2", "0", EtatFiche.EN_LIGNE.toString());
        Assert.assertNotNull(metatagBean);
        Assert.assertEquals(metatagBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByReference(@Mocked(stubOutClassInitialization = true) final ReferentielObjets mockedReferentiel) {
        new Expectations() {{
            ReferentielObjets.getNomObjet((String) any);
            result = "pagelibre";
        }};
        List<MetatagBean> results = metatagDAO.getByReferences("0016", "xxxxxxxx", "0", 0L);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir un résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByCodeRedacteurAndCodeRubrique() {
        List<MetatagBean> results = metatagDAO.getByCodeRedacteurAndCodeRubrique("META_CODE_REDACTEUR", "META_CODE_RUBRIQUE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    public void testByPhotoReferences() {
        List<MetatagBean> results = metatagDAO.getByPhotoReferences();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 0, "il doit n'y avoir aucun résultat");
    }

    @Test
    @DatabaseSetup("MetatagDAOTest.update.xml")
    @ExpectedDatabase("MetatagDAOTest.update-expected.xml")
    public void testUpdate() {
        final MetatagBean metatagBean = metatagDAO.getById(2L);
        Assert.assertNotNull(metatagBean);
        metatagBean.setMetaCodeObjet("2");
        metatagDAO.update(metatagBean);
    }

}
