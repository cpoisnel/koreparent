package com.univ.objetspartages.dao.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.RoleBean;
import com.univ.objetspartages.om.PermissionBean;

/**
 * Created by olivier.camon on 28/05/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/RoleDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class RoleDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    RoleDAO roleDAO;

    @Test
    @DatabaseSetup("RoleDAOTest.add.xml")
    @ExpectedDatabase("RoleDAOTest.add-expected.xml")
    public void testAdd() {
        final RoleBean roleBean = new RoleBean();
        roleBean.setCode("CODE2");
        roleBean.setLibelle("LIBELLE");
        roleBean.setPerimetre("PERIMETRE");
        PermissionBean permission = new PermissionBean("type","objet","action");
        roleBean.setPermissions(Collections.singletonList(permission));
        roleDAO.add(roleBean);
    }

    @Test
    @DatabaseSetup("RoleDAOTest.add-expected.xml")
    @ExpectedDatabase("RoleDAOTest.add.xml")
    public void testDelete() {
        roleDAO.delete(2L);
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("RoleDAOTest.update.xml")
    public void testFill() {
        final RoleBean roleBean = roleDAO.getById(2L);
        Assert.assertNotNull(roleBean);
        Assert.assertEquals(roleBean.getCode(), "CODE2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("RoleDAOTest.update.xml")
    @ExpectedDatabase("RoleDAOTest.update-expected.xml")
    public void testUpdate() {
        final RoleBean roleBean = roleDAO.getById(2L);
        Assert.assertNotNull(roleBean);
        roleBean.setLibelle("LIBELLE2");
        roleDAO.update(roleBean);
    }


    @Test
    @DatabaseSetup("RoleDAOTest.update.xml")
    public void testAll() {
        List<RoleBean> results = roleDAO.getAll();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }


    @Test
    @DatabaseSetup("RoleDAOTest.update.xml")
    public void testAllWithoutCollab() {
        List<RoleBean> results = roleDAO.getAllWithoutCollab();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }


    @Test
    @DatabaseSetup("RoleDAOTest.update.xml")
    public void testByCode() {
        RoleBean roleBean = roleDAO.getByCode("CODE2");
        Assert.assertNotNull(roleBean);
        Assert.assertEquals(roleBean.getId(), Long.valueOf(2), "l'intitulé doit correspondre à la deuxième entrée");
    }


    @Test
    @DatabaseSetup("RoleDAOTest.update.xml")
    public void testByPermission() {
        List<RoleBean> results = roleDAO.getByPermission("type/objet/action");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

}
