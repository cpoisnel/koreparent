package com.univ.objetspartages.dao.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.utils.sql.clause.ClauseWhere;

/**
 * Created by olivier.camon on 01/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/MediaDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class MediaDAOTest extends AbstractDbUnitTestngTests {

    private final static DateFormat HOUR_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    MediaDAO mediaDAO;

    @Test
    @DatabaseSetup("MediaDAOTest.add.xml")
    @ExpectedDatabase("MediaDAOTest.add-expected.xml")
    public void testAdd() throws ParseException {
        final MediaBean mediaBean = new MediaBean();
        mediaBean.setTitre("TITRE2");
        mediaBean.setLegende("LEGENDE");
        mediaBean.setDescription("DESCRIPTION");
        mediaBean.setAuteur("AUTEUR");
        mediaBean.setCopyright("COPYRIGHT");
        mediaBean.setTypeRessource("TYPE_RESSOURCE");
        mediaBean.setTypeMedia("TYPE_MEDIA");
        mediaBean.setSource("SOURCE");
        mediaBean.setFormat("FORMAT");
        mediaBean.setCodeRubrique("CODE_RUBRIQUE");
        mediaBean.setCodeRattachement("CODE_RATTACHEMENT");
        mediaBean.setUrl("URL");
        mediaBean.setUrlVignette("URL_VIGNETTE");
        mediaBean.setPoids(0L);
        mediaBean.setCodeRedacteur("CODE_REDACTEUR");
        mediaBean.setDateCreation(HOUR_FORMAT.parse("1970-01-01 00:00:00"));
        mediaBean.setThematique("THEMATIQUE");
        mediaBean.setMetaKeywords("META_KEYWORDS");
        mediaBean.setSpecificData("SPECIFIC_DATA");
        mediaBean.setTraductionData("TRADUCTION_DATA");
        mediaBean.setAccessibilityData("ACCESSIBILITY_DATA");
        mediaBean.setIsMutualise("0");
        mediaDAO.add(mediaBean);
    }

    @Test
    @DatabaseSetup("MediaDAOTest.update.xml")
    @ExpectedDatabase("MediaDAOTest.update-expected.xml")
    public void testUpdate() {
        final MediaBean mediaBean = mediaDAO.getById(2L);
        mediaBean.setLegende("LEGENDE2");
        mediaDAO.update(mediaBean);
    }

    @Test
    @DatabaseSetup("MediaDAOTest.add-expected.xml")
    @ExpectedDatabase("MediaDAOTest.add.xml")
    public void testDelete() {
        mediaDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("MediaDAOTest.update.xml")
    public void testAll() {
        List<MediaBean> results = mediaDAO.getAll();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MediaDAOTest.update.xml")
    public void testByFilledCodeRubrique() {
        List<MediaBean> results = mediaDAO.getByFilledCodeRubrique();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MediaDAOTest.update.xml")
    public void testByFilledUrlVignette() {
        List<MediaBean> results = mediaDAO.getByFilledUrlVignette();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MediaDAOTest.update.xml")
    public void testByResourceType() {
        List<MediaBean> results = mediaDAO.getByResourceType("TYPE_RESSOURCE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MediaDAOTest.update.xml")
    public void testByUrl() {
        MediaBean mediaBean = mediaDAO.getByUrl("URL");
        Assert.assertNotNull(mediaBean);
        Assert.assertEquals(mediaBean.getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MediaDAOTest.update.xml")
    public void testByTypeAndTypeMedia() {
        List<MediaBean> results = mediaDAO.getByTypeAndTypeMedia("TYPE_RESSOURCE", "TYPE_MEDIA");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("MediaDAOTest.update.xml")
    public void testCountFromWhere() {
        ClauseWhere emptyWhere = new ClauseWhere();
        int results = mediaDAO.getCountFromWhere(emptyWhere);
        Assert.assertNotNull(results);
        Assert.assertEquals(results, 2, "il doit y avoir 2 résultats");
    }

}
