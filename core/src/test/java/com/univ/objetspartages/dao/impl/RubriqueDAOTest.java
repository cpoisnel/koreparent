package com.univ.objetspartages.dao.impl;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.RubriqueBean;

/**
 * Created by olivier.camon on 27/05/15.
 */

@Test
@ContextHierarchy(@ContextConfiguration(locations = { "classpath:/com/univ/objetspartages/dao/impl/RubriqueDAOTest.test-context.xml" }))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class RubriqueDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    RubriqueDAO rubriqueDAO;

    @Test
    @DatabaseSetup("RubriqueDAOTest.add.xml")
    @ExpectedDatabase("RubriqueDAOTest.add-expected.xml")
    public void testAdd() {
        final RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode("CODE2");
        rubriqueBean.setLangue("0");
        rubriqueBean.setIntitule("INTITULE2");
        rubriqueBean.setNomOnglet("NOM_ONGLET");
        rubriqueBean.setAccroche("ACCROCHE2");
        rubriqueBean.setNomOnglet("NOM_ONGLET2");
        rubriqueBean.setCodeRubriqueMere("CODE_RUBRIQUE_MERE");
        rubriqueBean.setTypeRubrique("0001");
        rubriqueBean.setPageAccueil("{\"code\":\"75728968\",\"langue\":\"0\",\"objet\":\"pagelibre\"}");
        rubriqueBean.setIdBandeau(0L);
        rubriqueBean.setCouleurFond("COULEUR_FOND");
        rubriqueBean.setCouleurTitre("COULEUR_TITRE");
        rubriqueBean.setEncadre("ENCADRE");
        rubriqueBean.setEncadreSousRubrique("0");
        rubriqueBean.setGestionEncadre("0");
        rubriqueBean.setOrdre("2");
        rubriqueBean.setContact("foo@bar.com");
        rubriqueBean.setGroupesDsi("[1174468224946]");
        rubriqueBean.setRequetesRubriquePublication("REQUETES_RUBRIQUE_PUBLICATION");
        rubriqueBean.setCategorie("0000");
        rubriqueBean.setIdPicto(0L);
        rubriqueDAO.add(rubriqueBean);
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.add-expected.xml")
    @ExpectedDatabase("RubriqueDAOTest.add.xml")
    public void testDelete() {
        rubriqueDAO.delete(2L);
    }


    @Test
    @DatabaseSetup("RubriqueDAOTest.add-expected.xml")
    @ExpectedDatabase("RubriqueDAOTest.add.xml")
    public void testDeleteByCodes() {
        rubriqueDAO.deleteByCodes(Collections.singletonList("CODE2"));
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testFill() {
        final RubriqueBean rubriqueBean = rubriqueDAO.getById(2L);
        Assert.assertNotNull(rubriqueBean);
        Assert.assertEquals(rubriqueBean.getIntitule(), "INTITULE2", "l'intitulé doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testByCode() {
        RubriqueBean rubriqueBean = rubriqueDAO.selectByCode("CODE2");
        Assert.assertNotNull(rubriqueBean);
        Assert.assertEquals(rubriqueBean.getIntitule(), "INTITULE2", "l'intitulé doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testByLabel() {
        List<RubriqueBean> results = rubriqueDAO.selectByLabel("INTITULE2");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il ne doit y avoir qu'un seul résultat");
        Assert.assertEquals(results.get(0).getCode(), "CODE2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testByCodeParent() {
        List<RubriqueBean> results = rubriqueDAO.selectByCodeParent("CODE_RUBRIQUE_MERE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir deux résultats");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testByBandeau() {
        List<RubriqueBean> results = rubriqueDAO.selectByBandeau(0L);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getCode(), "CODE", "le code doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testAll() {
        List<RubriqueBean> results = rubriqueDAO.selectAllRubriques();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getCode(), "CODE", "le code doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testAllWithRubPub() {
        List<RubriqueBean> results = rubriqueDAO.selectAllRubriquesWithRubPub();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getCode(), "CODE", "le code doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testBylanguage() {
        List<RubriqueBean> results = rubriqueDAO.selectByLanguage("0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getCode(), "CODE", "le code doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testByCodes() {
        List<RubriqueBean> results = rubriqueDAO.selectByCodes(Collections.singletonList("CODE"));
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getIntitule(), "INTITULE", "le code doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testByCategories() {
        List<RubriqueBean> results = rubriqueDAO.selectByCategories(Collections.singletonList("0000"));
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultat");
        Assert.assertEquals(results.get(0).getIntitule(), "INTITULE", "le code doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testByReferences() {
        List<RubriqueBean> results = rubriqueDAO.selectByReferences("{\"code\":\"75728968\",\"langue\":\"0\",\"objet\":\"pagelibre\"}","");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getCode(), "CODE", "le code doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testMaxOrder() {
        int maxOrder = rubriqueDAO.selectMaxOrder("CODE_RUBRIQUE_MERE");
        Assert.assertEquals(maxOrder, 2, "l'ordre doit forcement être 2 (2eme valeurs renseigné dans update.xml)");
    }


    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testGetRubriqueByCodeLanguageLabel() {
        final List<RubriqueBean> results = rubriqueDAO.getRubriqueByCodeLanguageLabel("CODE", "0", "INTITULE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        final List<RubriqueBean> results2 = rubriqueDAO.getRubriqueByCodeLanguageLabel("CODE3", StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertNotNull(results2);
        Assert.assertTrue(results2.isEmpty(), "il ne doit pas y avoir de résultat");
        final List<RubriqueBean> results3 = rubriqueDAO.getRubriqueByCodeLanguageLabel(StringUtils.EMPTY, "0", StringUtils.EMPTY);
        Assert.assertNotNull(results3);
        Assert.assertEquals(results3.size(), 2, "il doit y avoir 2 résultats");

    }


    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    @ExpectedDatabase("RubriqueDAOTest.update-expected.xml")
    public void testUpdate() {
        final RubriqueBean rubriqueBean = rubriqueDAO.getById(2L);
        Assert.assertNotNull(rubriqueBean);
        rubriqueBean.setAccroche("ACCROCHE");
        rubriqueDAO.update(rubriqueBean);
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testGetRubriqueByCodeLanguageLabelCategory() {
        List<RubriqueBean> result = rubriqueDAO.getRubriqueByCodeLanguageLabelCategory("CODE", "0", "INTITULE", "0000");
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 1, "Il doit y avoir un seul résultat");
        Assert.assertEquals(result.get(0).getCode(), "CODE", "le résultat doit correspondre au champ requêté");
        result = rubriqueDAO.getRubriqueByCodeLanguageLabelCategory("CODE", "0", "INTITULE", StringUtils.EMPTY);
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 1, "Il doit y avoir un seul résultat");
        Assert.assertEquals(result.get(0).getCode(), "CODE", "le résultat doit correspondre au champ requêté");
        result = rubriqueDAO.getRubriqueByCodeLanguageLabelCategory("CODE", "0", "INTITULE", "0001");
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 0, "Il doit y avoir aucun résultat");
        result = rubriqueDAO.getRubriqueByCodeLanguageLabelCategory("CODE", StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 1, "Il doit y avoir un seul résultat");
        Assert.assertEquals(result.get(0).getCode(), "CODE", "le résultat doit correspondre au champ requêté");
        result = rubriqueDAO.getRubriqueByCodeLanguageLabelCategory(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 2, "Il doit y avoir deux résultats");
    }

    @Test
    @DatabaseSetup("RubriqueDAOTest.update.xml")
    public void testAllWithoutUrl() {
        final List<RubriqueBean> result = rubriqueDAO.selectAllWithoutUrl();
        Assert.assertNotNull(result);
        Assert.assertEquals(result.size(), 2, "il doit y avoir deux résultats");
    }

}
