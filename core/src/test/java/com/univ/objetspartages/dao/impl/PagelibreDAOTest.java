package com.univ.objetspartages.dao.impl;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.kosmos.tests.testng.TestFicheUtils;
import com.univ.objetspartages.bean.PagelibreBean;

/**
 * Created by olivier.camon on 29/05/15.
 */

@Test
@ContextHierarchy(@ContextConfiguration(locations = { "classpath:/com/univ/objetspartages/dao/impl/PagelibreDAOTest.test-context.xml" }))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class PagelibreDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    PagelibreDAO pagelibreDAO;


    @Test
    @DatabaseSetup("PagelibreDAOTest.add.xml")
    @ExpectedDatabase("PagelibreDAOTest.add-expected.xml")
    public void testAdd() throws ParseException {
        final PagelibreBean pagelibreBean = new PagelibreBean();
        TestFicheUtils.fillCommonFields(pagelibreBean);
        pagelibreBean.setCode("CODE2");
        pagelibreBean.setTitre("TITRE");
        pagelibreBean.setContenu("CONTENU");
        pagelibreBean.setComplements("COMPLEMENTS");
        pagelibreBean.setRattachementBandeau("R");
        pagelibreDAO.add(pagelibreBean);
    }

    @Test
    @DatabaseSetup("PagelibreDAOTest.add.xml")
    @ExpectedDatabase("PagelibreDAOTest.add-expected.xml")
    public void testAddWithForceId() throws ParseException {
        final PagelibreBean pagelibreBean = new PagelibreBean();
        TestFicheUtils.fillCommonFields(pagelibreBean);
        pagelibreBean.setId(2L);
        pagelibreBean.setCode("CODE2");
        pagelibreBean.setTitre("TITRE");
        pagelibreBean.setContenu("CONTENU");
        pagelibreBean.setComplements("COMPLEMENTS");
        pagelibreBean.setRattachementBandeau("R");
        pagelibreDAO.addWithForcedId(pagelibreBean);
    }

    @Test
    @DatabaseSetup("PagelibreDAOTest.update.xml")
    @ExpectedDatabase("PagelibreDAOTest.update-expected.xml")
    public void testUpdate() throws ParseException {
        final PagelibreBean pagelibreBean = pagelibreDAO.getById(2L);
        Assert.assertNotNull(pagelibreBean);
        pagelibreBean.setTitre("TITRE2");
        pagelibreDAO.update(pagelibreBean);
    }

    @Test
    @DatabaseSetup("PagelibreDAOTest.add-expected.xml")
    @ExpectedDatabase("PagelibreDAOTest.add.xml")
    public void testDelete() {
        pagelibreDAO.delete(2L);
    }
}
