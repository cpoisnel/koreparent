package com.univ.objetspartages.dao.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.RubriquepublicationBean;

/**
 * TU pour l'accès au données sur les Rubriques de publication.
 * Created by olivier.camon on 28/05/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = { "classpath:/com/univ/objetspartages/dao/impl/RubriquepublicationDAOTest.test-context.xml" }))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class RubriquepublicationDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    RubriquepublicationDAO rubriquepublicationDAO;

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.add.xml")
    @ExpectedDatabase("RubriquepublicationDAOTest.add-expected.xml")
    public void testAdd() {
        final RubriquepublicationBean rubriquepublicationBean = new RubriquepublicationBean();
        rubriquepublicationBean.setTypeFicheOrig("TFO");
        rubriquepublicationBean.setCodeFicheOrig("CODE_FICHE_ORIG2");
        rubriquepublicationBean.setLangueFicheOrig("LFO");
        rubriquepublicationBean.setRubriqueDest("RUBRIQUE_DEST");
        rubriquepublicationBean.setSourceRequete("SOURCE_REQUETE");
        rubriquepublicationDAO.add(rubriquepublicationBean);
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.add-expected.xml")
    @ExpectedDatabase("RubriquepublicationDAOTest.add.xml")
    public void testDelete() {
        rubriquepublicationDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.update.xml")
    public void testFill() {
        final RubriquepublicationBean rubriquepublicationBean = rubriquepublicationDAO.getById(2L);
        Assert.assertNotNull(rubriquepublicationBean);
        Assert.assertEquals(rubriquepublicationBean.getCodeFicheOrig(), "CODE_FICHE_ORIG2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.update.xml")
    @ExpectedDatabase("RubriquepublicationDAOTest.update-expected.xml")
    public void testUpdate() {
        final RubriquepublicationBean rubriquepublicationBean = rubriquepublicationDAO.getById(2L);
        Assert.assertNotNull(rubriquepublicationBean);
        rubriquepublicationBean.setTypeFicheOrig("TFO2");
        rubriquepublicationBean.setRubriqueDest("RUBRIQUE_DEST2");
        rubriquepublicationDAO.update(rubriquepublicationBean);
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.update.xml")
    public void testByTypeCodeLanguage() {
        List<RubriquepublicationBean> results = rubriquepublicationDAO.getByTypeCodeLanguage("TFO","CODE_FICHE_ORIG","LFO");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.update-expected.xml")
    public void testByTypeRubriqueDestLangue() {
        List<RubriquepublicationBean> results = rubriquepublicationDAO.getByTypeRubriqueDestLangue("TFO","RUBRIQUE_DEST","LFO");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }


    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.update.xml")
    public void testByRubriqueDestAndSource() {
        List<RubriquepublicationBean> results = rubriquepublicationDAO.getByRubriqueDestAndSource("RUBRIQUE_DEST","SOURCE_REQUETE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.update.xml")
    public void testByType() {
        List<RubriquepublicationBean> results = rubriquepublicationDAO.getByType("TFO");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.add-expected.xml")
    @ExpectedDatabase("RubriquepublicationDAOTest.deleteall-expected.xml")
    public void testDeleteByRubriqueDestAndSource() {
        rubriquepublicationDAO.deleteByRubriqueDestAndSource("RUBRIQUE_DEST","SOURCE_REQUETE");
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.add-expected.xml")
    @ExpectedDatabase("RubriquepublicationDAOTest.add.xml")
    public void testDeleteByTypeCodeLanguageAndSource() {
        rubriquepublicationDAO.deleteByTypeCodeLanguageAndSource("TFO", "CODE_FICHE_ORIG2", "LFO", "SOURCE_REQUETE");
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.add-expected.xml")
    @ExpectedDatabase("RubriquepublicationDAOTest.add.xml")
    public void testDeleteByTypeCodeLanguage() {
        rubriquepublicationDAO.deleteByTypeCodeLanguage("TFO", "CODE_FICHE_ORIG2", "LFO");
    }

    @Test
    @DatabaseSetup("RubriquepublicationDAOTest.add-expected.xml")
    @ExpectedDatabase("RubriquepublicationDAOTest.deleteall-expected.xml")
    public void testDeleteByIds() {
        rubriquepublicationDAO.deleteByIds(Arrays.asList(1L, 2L));
    }


}
