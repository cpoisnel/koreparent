package com.univ.objetspartages.dao.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.om.ServiceBean;

/**
 * Created by olivier.camon on 28/05/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = { "classpath:/com/univ/objetspartages/dao/impl/ServiceDAOTest.test-context.xml" }))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class ServiceDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    ServiceDAO serviceDAO;

    @Test
    @DatabaseSetup("ServiceDAOTest.add.xml")
    @ExpectedDatabase("ServiceDAOTest.add-expected.xml")
    public void testAdd() {
        final ServiceBean serviceBean = new ServiceBean();
        serviceBean.setCode("CODE2");
        serviceBean.setIntitule("INTITULE");
        serviceBean.setJetonKportal("J");
        serviceBean.setUrl("URL");
        serviceBean.setUrlPopup("U");
        serviceBean.setExpirationCache(0);
        serviceBean.setVueReduiteUrl("VUE_REDUITE_URL");
        serviceBean.setDiffusionMode("0");
        serviceBean.setDiffusionPublicVise(Collections.singletonList("DIFFUSION_PUBLIC_VISE"));
        serviceBean.setDiffusionModeRestriction("D");
        serviceBean.setDiffusionPublicViseRestriction(Collections.singletonList("DIFFUSION_PUBLIC_VISE_RESTRICTION"));
        serviceBean.setProxyCas("0");
        serviceDAO.add(serviceBean);
    }

    @Test
    @DatabaseSetup("ServiceDAOTest.add-expected.xml")
    @ExpectedDatabase("ServiceDAOTest.add.xml")
    public void testDelete() {
        serviceDAO.delete(2L);
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("ServiceDAOTest.update.xml")
    public void testFill() {
        final ServiceBean serviceBean = serviceDAO.getById(2L);
        Assert.assertNotNull(serviceBean);
        Assert.assertEquals(serviceBean.getCode(), "CODE2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("ServiceDAOTest.update.xml")
    @ExpectedDatabase("ServiceDAOTest.update-expected.xml")
    public void testUpdate() {
        final ServiceBean serviceBean = serviceDAO.getById(2L);
        Assert.assertNotNull(serviceBean);
        serviceBean.setIntitule("INTITULE2");
        serviceDAO.update(serviceBean);
    }


    @Test
    @DatabaseSetup("ServiceDAOTest.update.xml")
    public void testAll() {
        List<ServiceBean> results = serviceDAO.selectAll();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }


    @Test
    @DatabaseSetup("ServiceDAOTest.update.xml")
    public void testByCode() {
        ServiceBean serviceBean = serviceDAO.selectByCode("CODE2");
        Assert.assertNotNull(serviceBean);
        Assert.assertEquals(serviceBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

}
