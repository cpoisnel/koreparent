package com.univ.objetspartages.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.GroupeDsiBean;

/**
 * Created by olivier.camon on 01/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/GroupeDsiDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class GroupeDsiDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    GroupeDsiDAO groupeDsiDAO;

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add.xml")
    @ExpectedDatabase("GroupeDsiDAOTest.add-expected.xml")
    public void testAdd() {
        final GroupeDsiBean groupeDsiBean = new GroupeDsiBean();
        groupeDsiBean.setCode("CODE2");
        groupeDsiBean.setLibelle("LIBELLE");
        groupeDsiBean.setType("TYPE");
        groupeDsiBean.setCodeStructure("CODE_STRUCTURE");
        groupeDsiBean.setCodePageTete("CODE_PAGE_TETE");
        groupeDsiBean.setRoles("ROLES");
        groupeDsiBean.setCodeGroupePere("CODE_GROUPE_PERE");
        groupeDsiBean.setRequeteGroupe("REQUETE_GROUPE");
        groupeDsiBean.setRequeteLdap("REQUETE_LDAP");
        groupeDsiBean.setSourceImport("SOURCE_IMPORT");
        groupeDsiBean.setGestionCache("0");
        groupeDsiBean.setDelaiExpirationCache(0L);
        groupeDsiBean.setDerniereMajCache(0L);
        groupeDsiBean.setSelectionnable("0");
        groupeDsiDAO.add(groupeDsiBean);
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.update.xml")
    @ExpectedDatabase("GroupeDsiDAOTest.update-expected.xml")
    public void testUpdate() {
        final GroupeDsiBean groupeDsiBean = groupeDsiDAO.getById(2L);
        groupeDsiBean.setLibelle("LIBELLE2");
        groupeDsiDAO.update(groupeDsiBean);
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    @ExpectedDatabase("GroupeDsiDAOTest.add.xml")
    public void testDelete() {
        groupeDsiDAO.delete(2L);
    }


    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByCode() {
        final GroupeDsiBean groupeDsiBean = groupeDsiDAO.selectByCode("CODE2");
        Assert.assertNotNull(groupeDsiBean);
        Assert.assertEquals(groupeDsiBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testAll() {
        final List<GroupeDsiBean> results = groupeDsiDAO.selectAll();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }
    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testAllDynamics() {
        final List<GroupeDsiBean> results = groupeDsiDAO.selectAllDynamics();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByLabel() {
        final List<GroupeDsiBean> results = groupeDsiDAO.selectByLabel("LIBELLE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByLdapRequest() {
        final List<GroupeDsiBean> results = groupeDsiDAO.selectByLdapRequest("REQUETE_LDAP");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByImportSource() {
        final List<GroupeDsiBean> results = groupeDsiDAO.selectByImportSource("SOURCE_IMPORT");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByParentGroup() {
        final List<GroupeDsiBean> results = groupeDsiDAO.selectByParentGroup("CODE_GROUPE_PERE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }


    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByImportSourceAndParentGroup() {
        final List<GroupeDsiBean> results = groupeDsiDAO.selectByImportSourceAndParentGroup("SOURCE_IMPORT", "CODE_GROUPE_PERE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByCodeAndImportSource() {
        final List<GroupeDsiBean> results = groupeDsiDAO.selectByCodeAndImportSource("CODE", "SOURCE_IMPORT");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "Cette méthode devrait retourner exactement 1 élément");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByStructure() {
        final List<GroupeDsiBean> results = groupeDsiDAO.getByStructure("CODE_STRUCTURE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByStructureAndImportSource() {
        final List<GroupeDsiBean> results = groupeDsiDAO.getByStructureAndImportSource("CODE_STRUCTURE", "SOURCE_IMPORT");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByCodeTypeLabelStructureAndCache() {
        List<GroupeDsiBean> results = groupeDsiDAO.getByCodeTypeLabelStructureAndCache("CODE", "TYPE", "LIBELLE", "CODE_STRUCTURE", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "Cette méthode devrait retourner exactement 1 élément");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeTypeLabelStructureAndCache("", "TYPE", "LIBELLE", "CODE_STRUCTURE", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeTypeLabelStructureAndCache("", "", "LIBELLE", "CODE_STRUCTURE", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeTypeLabelStructureAndCache("", "", "", "CODE_STRUCTURE", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeTypeLabelStructureAndCache("", "", "", "", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeTypeLabelStructureAndCache("", "", "", "", "");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeTypeLabelStructureAndCache("CODE", "", "", "", "");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "Cette méthode devrait retourner exactement 1 élément");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByCodeAndType() {
        List<GroupeDsiBean> results = groupeDsiDAO.getByCodeAndType("CODE", "TYPE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "Cette méthode devrait retourner exactement 1 élément");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeAndType("", "TYPE");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeAndType("", "");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeAndType("CODE", "");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "Cette méthode devrait retourner exactement 1 élément");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeDsiDAOTest.add-expected.xml")
    public void testByCodeAndCache() {
        List<GroupeDsiBean> results = groupeDsiDAO.getByCodeAndCache("CODE", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "Cette méthode devrait retourner exactement 1 élément");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeAndCache("", "0");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeAndType("", "");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "Cette méthode devrait retourner exactement 2 éléments");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
        results = groupeDsiDAO.getByCodeAndType("CODE", "");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "Cette méthode devrait retourner exactement 1 élément");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }



}
