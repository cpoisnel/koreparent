package com.univ.utils.json;

import java.util.ArrayList;
import java.util.Collection;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.type.TypeReference;


public class CodecJSonTest {

    @Test
    public void testDecodeStringJSonToClass() throws Exception {
        Collection<String> lts = new ArrayList<>();
        lts.add("1664");
        lts.add("");
        String encode = CodecJSon.encodeObjectToJSonInString(lts);
        Assert.assertEquals(CodecJSon.decodeStringJSonToClass(encode, new TypeReference<Collection<String>>() {}), lts);
    }
}
