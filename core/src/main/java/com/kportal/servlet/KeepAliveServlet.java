package com.kportal.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet vide permettant de garder uniquement la connexion de l'utilisateur
 *
 * @author olivier.camon
 *
 */
public class KeepAliveServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 6326054464282713458L;

    @Override
    protected void doGet(final HttpServletRequest arg0, final HttpServletResponse arg1) throws ServletException, IOException {}
}
