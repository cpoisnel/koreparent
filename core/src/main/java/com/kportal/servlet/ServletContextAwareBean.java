package com.kportal.servlet;

import javax.servlet.ServletContext;

import org.springframework.web.context.ServletContextAware;

public class ServletContextAwareBean implements ServletContextAware {

    private ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext context) {
        servletContext = context;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }
}
