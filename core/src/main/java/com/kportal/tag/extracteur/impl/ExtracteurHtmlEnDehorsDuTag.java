package com.kportal.tag.extracteur.impl;

import org.apache.commons.lang3.StringUtils;

import com.kportal.tag.extracteur.ExtracteurTag;

public class ExtracteurHtmlEnDehorsDuTag implements ExtracteurTag {

    private String identifiantTag;

    public String getIdentifiantTag() {
        return identifiantTag;
    }

    public void setIdentifiantTag(String identifiantTag) {
        this.identifiantTag = identifiantTag;
    }

    @Override
    public String getContenuTagPresentDansTexte(String texte, String baliseOuvrante, String baliseFermante) {
        String contenuDuTag = StringUtils.EMPTY;
        int indexIdentifiant = StringUtils.indexOf(texte, identifiantTag);
        if (indexIdentifiant >= 0) {
            String texteAvantId = StringUtils.substring(texte, 0, indexIdentifiant);
            int indexBaliseOuvrante = StringUtils.lastIndexOf(texteAvantId, baliseOuvrante);
            int indexBaliseFermante = StringUtils.indexOf(texte, baliseFermante, indexBaliseOuvrante);
            if (indexBaliseOuvrante != -1 && indexBaliseFermante != -1 && indexBaliseOuvrante <= indexBaliseFermante) {
                contenuDuTag = StringUtils.substring(texte, indexBaliseOuvrante, indexBaliseFermante + baliseFermante.length());
            }
        }
        return contenuDuTag;
    }
}
