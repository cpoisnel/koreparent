package com.kportal.tag.extracteur;

/**
 * Permet de chercher dans une chaine de caractère si un tag est présent ou non. Utiliser pour chercher dans une chaine venant généralement de la toolbox si un tag est présent.
 *
 * @author olivier.camon
 *
 */
public interface ExtracteurTag {

    /**
     *
     * @param texte
     *            le texte pouvant contenir le tag
     * @param baliseOuvrante
     *            la balise de début de tag
     * @param baliseFermante
     *            la balise de fin du tag
     * @return le contenu du tag ainsi que ses balises ouvrante / fermante si il est présent ou null si il n'est pas présent.
     */
    String getContenuTagPresentDansTexte(String texte, String baliseOuvrante, String baliseFermante);
}
