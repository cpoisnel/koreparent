package com.kportal.tag.util;

import java.util.*;
import java.util.Map.Entry;

import com.kportal.extension.module.plugin.toolbox.IPluginTag;

public abstract class ContexteTagUtil {

    /**
     * Un tag défini comme contexte DEFAUT correspond à un tag devant s'executer dans tout les contextes défini dans l'application sauf si il est surchargé dans un contexte précis.
     */
    public static final String DEFAUT = "DEFAUT";

    /**
     * Le contexte INDEXATION correspond à l'interpretation d'un tag lors de l'indexation lucene. Il peut être différent du tag par défaut (par exemple le tag mailto)
     */
    public static final String INDEXATION = "INDEXATION";

    /**
     *
     * A partir de la liste de tagsAInterpreter, on construit une map qui classe les tags par contexte de traitement (DEFAUT,SIMPLIFIE...). Si aucun contexte n'est défini pour le
     * tag courant, il est considéré comme tag par défaut. Si aucun tag n'est défini pour le contexte courant, le tag par défaut est pris en compte. Si un tag est défini pour un
     * contexte spécifique mais aucun comme défaut, alors il est interprété que dans son contexte spécifique.
     *
     * @param tagsAInterpreter
     *            la listes des tags défini dans Spring
     * @return une map d'interpreteur de tags classé par contexte.
     */
    public static Map<String, Collection<IPluginTag>> getTagsParContexte(Collection<IPluginTag> tagsAInterpreter) {
        Map<String, Collection<IPluginTag>> tagsParIdentifiant = new HashMap<>();
        Set<String> contexteDesTags = getContexteDeTagsParDefaut();
        for (IPluginTag tagCourant : tagsAInterpreter) {
            tagsParIdentifiant = ajouterTagDansMap(tagsParIdentifiant, tagCourant.getIdentifiantTag(), tagCourant);
            if (tagCourant.getContexte().isEmpty()) {
                tagCourant.setContexteTag(Collections.singletonList(DEFAUT));
            }
            contexteDesTags.addAll(tagCourant.getContexte());
        }
        Map<String, Collection<IPluginTag>> tagsAInterpreterParContexte = new HashMap<>();
        for (Entry<String, Collection<IPluginTag>> tagsParId : tagsParIdentifiant.entrySet()) {
            for (String contexteDeTag : contexteDesTags) {
                IPluginTag tagPourContexteCourant = getTagPourContexteCourant(contexteDeTag, tagsParId.getValue());
                if (tagPourContexteCourant != null) {
                    tagsAInterpreterParContexte = ajouterTagDansMap(tagsAInterpreterParContexte, contexteDeTag, tagPourContexteCourant);
                }
            }
        }
        return tagsAInterpreterParContexte;
    }

    /**
     * Initialise les contextes des tags avec les valeurs présentes dans le produit uniquement.
     *
     * @return un Set contenant les contextes par défaut.
     */
    private static Set<String> getContexteDeTagsParDefaut() {
        Set<String> contexteDeTags = new HashSet<>();
        contexteDeTags.add(DEFAUT);
        contexteDeTags.add(INDEXATION);
        return contexteDeTags;
    }

    /**
     * Recherche le tag correspondant au contexte demandé.
     *
     * @param contexteDeTag
     * @param tagsPossible
     * @return
     */
    private static IPluginTag getTagPourContexteCourant(String contexteDeTag, Collection<IPluginTag> tagsPossible) {
        IPluginTag tagDuContexteCourant = null;
        IPluginTag tagDefaut = null;
        for (IPluginTag tag : tagsPossible) {
            if (tag.getContexte().contains(contexteDeTag)) {
                tagDuContexteCourant = tag;
            }
            if (tag.getContexte().contains(DEFAUT)) {
                tagDefaut = tag;
            }
        }
        if (tagDuContexteCourant == null && tagDefaut != null) {
            tagDuContexteCourant = tagDefaut;
        }
        return tagDuContexteCourant;
    }

    /**
     * Méthode utilitaire permettant d'ajouter les elements à une map.
     *
     * @param mapAModifier
     * @param cle
     * @param tagAAJouter
     * @return
     */
    private static Map<String, Collection<IPluginTag>> ajouterTagDansMap(Map<String, Collection<IPluginTag>> mapAModifier, String cle, IPluginTag tagAAJouter) {
        Collection<IPluginTag> listeTagCourant = mapAModifier.get(cle);
        if (listeTagCourant == null) {
            listeTagCourant = new ArrayList<>();
            mapAModifier.put(cle, listeTagCourant);
        }
        listeTagCourant.add(tagAAJouter);
        return mapAModifier;
    }
}
