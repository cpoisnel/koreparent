package com.kportal.tag.interpreteur.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.kportal.tag.interpreteur.InterpreteurTag;

public abstract class AbstractInterpreteurTag implements InterpreteurTag {

    public List<String> typesInterpreteur;

    public void setTypesInterpreteur(final List<String> typesInterpreteur) {
        this.typesInterpreteur = typesInterpreteur;
    }

    @Override
    public List<String> getTypesInterpreteur() {
        return typesInterpreteur;
    }

    protected String supprimerBalisesDuTexte(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        return StringUtils.substring(texteAInterpreter, baliseOuvrante.length(), texteAInterpreter.length() - baliseFermante.length());
    }
}
