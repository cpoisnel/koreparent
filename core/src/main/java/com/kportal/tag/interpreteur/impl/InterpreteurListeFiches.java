package com.kportal.tag.interpreteur.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.Formateur;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.AbstractRequeteur;
import com.univ.utils.AttributRequeteur;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.IRequeteurConstantes;
import com.univ.utils.RequeteUtil;
import com.univ.utils.RequeteurFiches;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;
import com.univ.utils.cache.CacheRequestManager;
import com.univ.utils.cache.RequeteurFicheCacheRequest;

public class InterpreteurListeFiches extends AbstractInterpreteurTag {

    private static final Logger LOG = LoggerFactory.getLogger(InterpreteurListeFiches.class);

    @Override
    public String interpreterTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) throws Exception {
        String param = StringUtils.substringBetween(texteAInterpreter, baliseOuvrante, baliseFermante);
        param = StringEscapeUtils.unescapeHtml4(param);
        param = StringUtils.replace(param, "#", "&");
        return formatTagListeFiche(ContexteUtil.getContexteUniv(), param);
    }

    /**
     * @param _ctx
     * @param paramExit
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws Exception
     */
    private String formatTagListeFiche(final ContexteUniv _ctx, final String paramExit) throws Exception {
        String res = "";
        if (paramExit.length() > 0) {
            final boolean afficheListe = AbstractRequeteur.getBooleanValue(RequeteUtil.renvoyerParametre(paramExit, "LISTE"));
            final boolean afficheTableau = AbstractRequeteur.getBooleanValue(RequeteUtil.renvoyerParametre(paramExit, "TAB"));
            // requeteur
            final String mode = RequeteUtil.renvoyerParametre(paramExit, IRequeteurConstantes.MODE, true);
            final String actions = RequeteUtil.renvoyerParametre(paramExit, IRequeteurConstantes.ACTIONS, true);
            final String pagination = RequeteUtil.renvoyerParametre(paramExit, IRequeteurConstantes.AVEC_PAGINATION);
            final String ajout = RequeteUtil.renvoyerParametre(paramExit, IRequeteurConstantes.AVEC_AJOUT);
            final AbstractRequeteur requeteur = new RequeteurFiches(mode, actions, pagination, ajout, "");
            String parametreAttribut = "";
            final String titre = RequeteUtil.renvoyerParametre(paramExit, "TITRE");
            if (titre.length() > 0) {
                requeteur.setAttribut("TITRE", titre, false, false);
            }
            final String etat = RequeteUtil.renvoyerParametre(paramExit, "ETAT");
            if (etat.length() > 0) {
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_ETAT_OBJET, etat);
            }
            String codeObjet = RequeteUtil.renvoyerParametre(paramExit, "OBJET", true);
            String codeRedacteur = RequeteUtil.renvoyerParametre(paramExit, "CODE_REDACTEUR");
            if (codeRedacteur.contains("|")) {
                parametreAttribut = codeRedacteur.substring(codeRedacteur.indexOf("|"), codeRedacteur.length());
                codeRedacteur = codeRedacteur.substring(0, codeRedacteur.indexOf("|"));
            }
            if (_ctx.getAutorisation() != null && codeRedacteur.contains(IRequeteurConstantes.CODE_DYNAMIQUE)) {
                codeRedacteur = _ctx.getAutorisation().getCode();
                parametreAttribut = "|0|0";
            }
            if (codeRedacteur.length() > 0 || parametreAttribut.length() > 0) {
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_CODE_REDACTEUR, codeRedacteur + parametreAttribut);
            }
            parametreAttribut = "";
            String codeRubrique = RequeteUtil.renvoyerParametre(paramExit, "CODE_RUBRIQUE");
            if (codeRubrique.length() > 0) {
                if (codeRubrique.contains(IRequeteurConstantes.CODE_DYNAMIQUE)) {
                    codeRubrique = _ctx.getCodeRubriquePageCourante();
                }
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_CODE_RUBRIQUE, codeRubrique);
            }
            final String codeRattachement = RequeteUtil.renvoyerParametre(paramExit, "CODE_RATTACHEMENT");
            if (codeRattachement.length() > 0) {
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_CODE_RATTACHEMENT, codeRattachement);
            }
            String publicVise = RequeteUtil.renvoyerParametre(paramExit, "DIFFUSION_PUBLIC_VISE");
            if (publicVise.contains("|")) {
                parametreAttribut = publicVise.substring(publicVise.indexOf("|"), publicVise.length());
                publicVise = publicVise.substring(0, publicVise.indexOf("|"));
            }
            if (_ctx.getAutorisation() != null && publicVise.contains(IRequeteurConstantes.CODE_DYNAMIQUE)) {
                publicVise = Chaine.convertirPointsVirgules(_ctx.getGroupesDsi());
                parametreAttribut = "|0|1";
            }
            if (publicVise.length() > 0 || parametreAttribut.length() > 0) {
                if (publicVise.contains(",")) {
                    parametreAttribut = "|0|1";
                }
                publicVise = publicVise.replaceAll(",", ";");
                publicVise = publicVise.replaceAll("@", ",");
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_DIFFUSION_PUBLIC_VISE, publicVise + parametreAttribut);
            }
            parametreAttribut = "";
            final String codeEspace = RequeteUtil.renvoyerParametre(paramExit, "ESPACE");
            if (codeEspace.length() > 0) {
                requeteur.setAttribut("ESPACE", codeEspace);
            }
            String modeRestriction = "";
            String publicViseRestriction = "";
            // cas particulier des espaces collaboratifs
            if (codeEspace.length() > 0) {
                modeRestriction = "4";
                publicViseRestriction = codeEspace;
            } else {
                modeRestriction = RequeteUtil.renvoyerParametre(paramExit, "DIFFUSION_MODE_RESTRICTION");
                publicViseRestriction = RequeteUtil.renvoyerParametre(paramExit, "DIFFUSION_PUBLIC_VISE_RESTRICTION");
            }
            if ("TOUS".equals(codeObjet) || "".equals(codeObjet) || "0000".equals(codeObjet)) {
                codeObjet = "";
                for (final Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
                    if ("".equals(modeRestriction)) {
                        // collaboratif exclu
                        if (!objet.isStrictlyCollaboratif()) {
                            if (codeObjet.length() > 0) {
                                codeObjet += ",";
                            }
                            codeObjet += objet.getNomObjet();
                        }
                    } else if ("4".equals(modeRestriction)) {
                        // collaboratif uniquement
                        if (objet.isCollaboratif()) {
                            if (codeObjet.length() > 0) {
                                codeObjet += ",";
                            }
                            codeObjet += objet.getNomObjet();
                        }
                    }
                }
            }
            if (codeObjet.length() > 0) {
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_CODE_OBJET, codeObjet + parametreAttribut);
            }
            if (modeRestriction.length() > 0) {
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_DIFFUSION_MODE_RESTRICTION, modeRestriction);
            }
            if (publicViseRestriction.length() > 0) {
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION, publicViseRestriction);
            }
            final String selection = RequeteUtil.renvoyerParametre(paramExit, "SELECTION");
            Date dateDebut = null;
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String chaineDateDebut = RequeteUtil.renvoyerParametre(paramExit, "DATE_DEBUT");
            if (StringUtils.isNoneBlank(selection)) {
                dateDebut = AbstractRequeteur.renvoyerDateSelection(selection);
            }
            if (Formateur.estSaisie(dateDebut)) {
                chaineDateDebut = sdf.format(dateDebut) + chaineDateDebut;
            }
            if (chaineDateDebut.length() > 0) {
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_DATE_DEBUT, chaineDateDebut);
            }
            final AttributRequeteur attributDateDebut = requeteur.getAttribut(IRequeteurConstantes.ATTRIBUT_DATE_DEBUT);
            requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_TYPE_DATE, "DATE_MODIFICATION", attributDateDebut.isRequetable(), attributDateDebut.isTriable());
            final String chaineDateFin = RequeteUtil.renvoyerParametre(paramExit, "DATE_FIN");
            if (chaineDateFin.length() > 0) {
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_DATE_FIN, chaineDateFin);
            }
            final String langue = RequeteUtil.renvoyerParametre(paramExit, "LANGUE");
            if (langue.length() > 0) {
                requeteur.setAttribut(IRequeteurConstantes.ATTRIBUT_LANGUE, langue);
            }
            final String tri = RequeteUtil.renvoyerParametre(paramExit, "TRI");
            final String increment = RequeteUtil.renvoyerParametre(paramExit, "INCREMENT");
            int inc = 5;
            int nb = 0;
            try {
                inc = Integer.parseInt(increment);
            } catch (final NumberFormatException e) {
                LOG.debug("unable to parse the given value", e);
            }
            if (tri.equals(IRequeteurConstantes.TRI_OBJET)) {
                nb = inc;
                inc = 0;
            }
            if (afficheListe || afficheTableau) {
                // Optimisation des requetes en front : ajout d'une variable dans le contexte
                _ctx.getDatas().put("optimizedLimit", "1000");
                // Activation du controle dsi
                _ctx.setCalculListeResultatsFront(true);
                final Map<String, MetatagBean> tMap = performRequest(requeteur, tri, inc);
                _ctx.setCalculListeResultatsFront(false);
                // Optimisation des requetes en front : suppression de la variable
                _ctx.getDatas().remove("optimizedLimit");
                final String separateur = RequeteUtil.renvoyerParametre(paramExit, "SEPARATEUR");
                String lien = RequeteUtil.renvoyerParametre(paramExit, "LIEN");
                if ("".equals(lien)) {
                    lien = "Toutes les fiches";
                }
                final String title = RequeteUtil.renvoyerParametre(paramExit, "TITLE");

                if (afficheListe) {
                    String lastTypeFiche = "";
                    int compteur = 1;
                    String classCss = RequeteUtil.renvoyerParametre(paramExit, "CLASSE_CSS");
                    if (classCss.length() > 0) {
                        classCss = "class=\"" + classCss.toLowerCase() + "\"";
                    }
                    for (final MetatagBean bean : tMap.values()) {
                        boolean affiche = true;
                        final String idFiche = bean.getMetaIdFiche().toString();
                        final String typeFiche = bean.getMetaCodeObjet();
                        final String nomObjet = ReferentielObjets.getNomObjet(typeFiche);
                        final FicheUniv fiche = ReferentielObjets.instancierFiche(typeFiche);
                        final String etatObjet = bean.getMetaEtatObjet();
                        final String libelleFiche = bean.getMetaLibelleFiche();
                        if (tri.equals(IRequeteurConstantes.TRI_OBJET)) {
                            if (typeFiche.equals(lastTypeFiche)) {
                                compteur++;
                            } else {
                                if (res.length() > 0) {
                                    res += "<br /><br />";
                                }
                                res += "<b>" + ReferentielObjets.getLibelleObjet(typeFiche) + "</b>";
                                compteur = 1;
                            }
                            if (compteur > nb) {
                                affiche = false;
                            }
                        }
                        if (affiche) {
                            if (res.length() > 0 && "br".equalsIgnoreCase(separateur)) {
                                res += "<br />";
                            } else if ("li".equalsIgnoreCase(separateur)) {
                                res += "<li>";
                            } else if (res.length() > 0) {
                                res += "<br />";
                            }
                            String codeB = "";
                            if (mode.equals(IRequeteurConstantes.MODE_VALIDATION) || mode.equals(IRequeteurConstantes.MODE_MODIFICATION)) {
                                if ("0001".equals(etatObjet)) {
                                    codeB = " (B)";
                                } else if ("0002".equals(etatObjet)) {
                                    codeB = " (V)";
                                }
                                if (!FicheAnnotationHelper.isSaisieFrontOffice(fiche)) {
                                    final String parametres = "EXT=" + ReferentielObjets.getExtension(typeFiche) + "&amp;PROC=" + ReferentielObjets.getProcessus(typeFiche) + "&amp;ACTION=MODIFIER&amp;ID_FICHE=" + idFiche;
                                    if (_ctx.getAutorisation().possedeModeExpert()) {
                                        res += "<a " + classCss + " " + (StringUtils.isNotBlank(title) ? String.format("title=\"%s\" ", title) : "") + "href=\"#\" onclick=\"window.open('" + URLResolver.getAbsoluteUrl(WebAppUtil.SG_PATH + "?" + parametres, _ctx) + "','_blank','');return false\">" + libelleFiche + "</a>" + codeB;
                                    } else {
                                        res += libelleFiche + " (non modifiable)";
                                    }
                                } else {
                                    String parametres = WebAppUtil.SG_PATH + "?EXT=" + ReferentielObjets.getExtension(typeFiche) + "&amp;ACTION=MODIFIER&amp;SAISIE_FRONT=TRUE" + "&amp;ID_FICHE=" + idFiche + "&amp;PROC=SAISIE_" + nomObjet.toUpperCase() + "_FRONT";
                                    if (_ctx.getFicheCourante() != null) {
                                        parametres += "&amp;URL_REDIRECT=" + URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(_ctx, _ctx.getFicheCourante()), _ctx);
                                    }
                                    res += "<a " + classCss + " " + (StringUtils.isNotBlank(title) ? String.format("title=\"%s\" ", title) : "") + "href=\"" + URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlRelative(_ctx, parametres, true), _ctx) + "\">" + libelleFiche + "</a>" + codeB;
                                }
                            } else {
                                res += "<a " + classCss + " " + (StringUtils.isNotBlank(title) ? String.format("title=\"%s\" ", title) : "") + "href=\"" + URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(_ctx, nomObjet, bean.getMetaCode()), _ctx) + "\">" + libelleFiche + "</a>";
                            }
                            if ("li".equalsIgnoreCase(separateur)) {
                                res += "</li>";
                            }
                            lastTypeFiche = typeFiche;
                        }
                    }
                }
                if ("li".equalsIgnoreCase(separateur)) {
                    res = "<ul>" + res + "</ul>";
                }
                if (afficheTableau) {
                    if (tMap.size() > 0) {
                        if (res.length() > 0) {
                            res += "<br /><br />";
                        }
                        res += "<a " + (StringUtils.isNotBlank(title) ? String.format("title=\"%s\" ", title) : "") + "href=\"" + getTableUrl(_ctx, requeteur.genererUrlRequete(), tri) + "\">" + lien + "</a>";
                    }
                }
            }
        }
        return res;
    }

    private String getTableUrl(ContexteUniv ctx, String request, String sort) {
        final StringBuilder url = new StringBuilder(URLResolver.getAbsoluteUrl(request + "&amp;RH=" + ctx.getCodeRubriquePageCourante(), ctx));
        if(StringUtils.isNotBlank(sort)) {
            url.append("&amp;TRI=").append(sort);
        }
        return url.toString();
    }

    /**
     * Méthode exécutant la requête
     * @param requeteur
     * @param tri
     * @param inc
     * @return
     * @throws Exception
     */
    protected Map<String, MetatagBean> performRequest(final AbstractRequeteur requeteur, final String tri, final int inc ) throws Exception {
        final CacheRequestManager cacheRequestManager = (CacheRequestManager) ApplicationContextManager.getCoreContextBean(CacheRequestManager.ID_BEAN);
        return (Map<String, MetatagBean>) cacheRequestManager.call(RequeteurFicheCacheRequest.ID_BEAN, requeteur, tri, StringUtils.EMPTY, 0, inc, IRequeteurConstantes.REQUETE_SANS_COUNT);
    }

    @Override
    public String getReferenceTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        try {
            return interpreterTag(texteAInterpreter, baliseOuvrante, baliseFermante);
        } catch (final Exception e) {
            LOG.debug("unable to interpret the current tag", e);
            return StringUtils.EMPTY;
        }
    }
}
