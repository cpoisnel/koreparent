package com.kportal.tag.interpreteur.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.om.AnnuaireModele;
import com.univ.objetspartages.om.FicheAnnuaire;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.RequeteUtil;

public class InterpreteurMailto extends AbstractInterpreteurTag {

    private static final Logger LOG = LoggerFactory.getLogger(InterpreteurMailto.class);

    protected static String baliseInterneOuvrante = "[mailto]";

    protected static String baliseInterneFermante = "[/mailto]";

    @Override
    public String interpreterTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final String contenuTagMailto = StringUtils.substringBetween(texteAInterpreter, baliseInterneOuvrante, baliseInterneFermante);
        String texteInterprete = StringUtils.EMPTY;
        // nouveau tag de la forme [mailto]email=annuaire;549805613&subject=[...]&body=[...][/mailto]
        String mail;
        if (contenuTagMailto.startsWith("email=")) {
            mail = RequeteUtil.renvoyerParametre(contenuTagMailto, "email");
            if (mail.contains(";")) {
                mail = getAdresseMailFromAnnuaire(ctx, mail);
            }
        } else {
            mail = getAdresseMailFromAnnuaire(ctx, contenuTagMailto);
        }
        if (StringUtils.isNotBlank(mail) && mail.contains("@")) {
            texteInterprete = "mailto:" + mail;
        }
        if (StringUtils.isNotBlank(texteInterprete)) {
            texteInterprete = StringUtils.replace(texteAInterpreter, baliseInterneOuvrante + contenuTagMailto + baliseInterneFermante, texteInterprete);
        }
        return texteInterprete;
    }

    @Override
    public String getReferenceTag(String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        String codeFiche = StringUtils.EMPTY;
        texteAInterpreter = supprimerBalisesDuTexte(texteAInterpreter, baliseOuvrante, baliseFermante);
        // nouveau tag de la forme [mailto]email=annuaire;549805613&subject=[...]&body=[...][/mailto]
        if (texteAInterpreter.startsWith("email=")) {
            final String mail = RequeteUtil.renvoyerParametre(texteAInterpreter, "email");
            codeFiche = StringUtils.substringAfter(mail, ";");
        } else {
            // old tag de la forme [mailto]annuaire;549805613[/mailto]
            codeFiche = StringUtils.substringAfter(texteAInterpreter, ";");
        }
        if (StringUtils.isNotBlank(codeFiche)) {
            codeFiche = "[annuaire;" + codeFiche + ";0]";
        }
        return codeFiche;
    }

    /**
     * old tag de la forme [mailto]annuaire;549805613[/mailto]
     *
     * @param ctx
     * @param code
     * @return
     */
    protected String getAdresseMailFromAnnuaire(final OMContext ctx, final String code) {
        final String[] arguments = code.split(";");
        String codeAnnuaire = "";
        if (arguments.length >= 2) {
            codeAnnuaire = arguments[1];
        }
        codeAnnuaire = StringUtils.replace(codeAnnuaire, "%20", " ");
        AnnuaireModele annuaire = null;
        try {
            annuaire = FicheAnnuaire.getFicheAnnuaire(codeAnnuaire, LangueUtil.getLangueLocale(ctx.getLocale()));
        } catch (final Exception e) {
            LOG.debug("pas de fiche trouvé, on ne fait rien", e);
        }
        if (annuaire != null && annuaire.getAdresseMail().length() > 0) {
            return annuaire.getAdresseMail();
        }
        return StringUtils.EMPTY;
    }
}
