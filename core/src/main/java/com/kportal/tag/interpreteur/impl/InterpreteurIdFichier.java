package com.kportal.tag.interpreteur.impl;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FileUtil;
import com.univ.utils.URLResolver;

public class InterpreteurIdFichier extends AbstractInterpreteurTag {

    protected static final String baliseInterneOuvrante = "[id-fichier]";

    protected static final String baliseInterneFermante = "[/id-fichier]";

    private ServiceMedia serviceMedia;

    public void setServiceMedia(final ServiceMedia serviceMedia) {
        this.serviceMedia = serviceMedia;
    }

    @Override
    public String interpreterTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        // texteAInterpreter = <a href="[id-fichier]654646465654[/id-fichier]">bla bla bla</a>
        final StringBuilder newTexte = new StringBuilder();
        String idFichier = StringUtils.substringBetween(texteAInterpreter, baliseInterneOuvrante, baliseInterneFermante);
        if (idFichier.startsWith(";")) {
            idFichier = idFichier.substring(1);
        }
        final MediaBean media = serviceMedia.getById(Long.valueOf(idFichier));
        if (media != null) {
            // <a href="
            newTexte.append(StringUtils.substringBefore(texteAInterpreter, baliseInterneOuvrante));
            String url = MediaUtils.getUrlAbsolue(media);
            if (!MediaUtils.isPublic(media) && ContexteUtil.getContexteUniv().getFicheCourante() != null) {
                url += (url.contains("?") ? "&" : "?") + "ID_FICHE=" + ContexteUtil.getContexteUniv().getFicheCourante().getIdFiche();
            }
            final String typeDisposition = StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty("telechargement.disposition"), "attachment");
            if ("attachment".equalsIgnoreCase(typeDisposition)) {
                url += (url.contains("?") ? "&" : "?") + "INLINE=FALSE";
            }
            // <a href="http://www.site.com/media
            newTexte.append(URLResolver.getAbsoluteUrl(url, ContexteUtil.getContexteUniv()));
            // ajout de l'extension et du poids du fichier à la fin du libellé
            final String title = StringUtils.isBlank(media.getTitre()) ? media.getSource() : media.getTitre();
            String weight = "%d %s";
            if (media.getPoids() > 1000000) {
                weight = String.format(weight, media.getPoids() / 1000000, "Go");
            } else if (media.getPoids() > 1000) {
                weight = String.format(weight, media.getPoids() / 1000, "Mo");
            } else {
                weight = String.format(weight, media.getPoids(), "Ko");
            }
            // <a href="http://www.site.com/media" title="Télécharger 'title' [jpeg - 20ko]
            newTexte.append(String.format("\" title=\"%s '%s' [%s - %s]", MessageHelper.getCoreMessage("DEFAUT.TELECHARGER"), title, FileUtil.getExtension(media.getSource()).toUpperCase(), weight));
            // <a href="http://www.site.com/media" title="Télécharger 'title' [jpeg - 20ko]">bla bla bla</a>
            newTexte.append(StringUtils.substringAfter(texteAInterpreter, baliseInterneFermante));
        }
        return newTexte.toString();
    }

    @Override
    public String getReferenceTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        return interpreterTag(texteAInterpreter, baliseOuvrante, baliseFermante);
    }
}
