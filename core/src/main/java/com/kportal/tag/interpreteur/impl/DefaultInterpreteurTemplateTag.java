package com.kportal.tag.interpreteur.impl;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.module.plugin.toolbox.IPluginTag;
import com.kportal.frontoffice.util.JSPIncludeHelper;
import com.kportal.tag.interpreteur.InterpreteurTemplateTag;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

public class DefaultInterpreteurTemplateTag extends AbstractInterpreteurTag implements InterpreteurTemplateTag {

    public IPluginTag pluginTag;

    public String pathJsp;

    @Override
    public String getPathJsp() {
        return pathJsp;
    }

    @Override
    public void setPathJsp(final String pathJsp) {
        this.pathJsp = pathJsp;
    }

    @Override
    public IPluginTag getPluginTag() {
        return pluginTag;
    }

    @Override
    public void setPluginTag(final IPluginTag pluginTag) {
        this.pluginTag = pluginTag;
    }

    @Override
    public String getOutputJsp(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String path = ExtensionHelper.getTemplateExtension(pluginTag.getIdExtension(), pathJsp, Boolean.TRUE);
        return JSPIncludeHelper.getOutputJsp(context, request, response, path);
    }

    @Override
    public String interpreterTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) throws Exception {
        String param = StringUtils.substringBetween(texteAInterpreter, baliseOuvrante, baliseFermante);
        param = StringEscapeUtils.unescapeHtml4(param);
        param = StringUtils.replace(param, "#", "&");
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        // test que le tag est genere pour une jsp en front
        if (pluginTag != null && StringUtils.isNotEmpty(pathJsp) && ctx.getRequeteHTTP() != null) {
            ctx.getRequeteHTTP().setAttribute("PARAM", param);
            return getOutputJsp(ctx.getJspWriter(), ctx.getServletContext(), ctx.getRequeteHTTP(), ctx.getReponseHTTP());
        }
        return "";
    }

    @Override
    public String getReferenceTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        return "";
    }
}
