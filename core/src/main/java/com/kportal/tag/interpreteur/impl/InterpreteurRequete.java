package com.kportal.tag.interpreteur.impl;

import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.textsearch.ResultatRecherche;
import com.kportal.cms.objetspartages.ObjetPartageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.frontoffice.util.JSPIncludeHelper;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.RequeteUtil;
import com.univ.utils.cache.CacheRequestManager;
import com.univ.utils.cache.ObjetsCacheRequest;

public class InterpreteurRequete extends AbstractInterpreteurTag {

    @Override
    public String interpreterTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) throws Exception {
        String retour = StringUtils.EMPTY;
        String param = StringUtils.substringBetween(texteAInterpreter, baliseOuvrante, baliseFermante);
        param = StringEscapeUtils.unescapeHtml4(param);
        param = StringUtils.replace(param, "#", "&");
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        //on ajoute au contexte la contrainte DSI (elle est utilisée pour filtrer les objets renvoyés)
        ctx.setCalculListeResultatsFront(true);
        // on vérifie si cette requête est déjà présente en cache; sinon, on l'exécute et on l'y met
        final List<ResultatRecherche> listeResultats = performRequest(param);
        // on supprime du contexte la contrainte dsi (elle n'est utile que pour filtrer les objets envoyés)
        ctx.setCalculListeResultatsFront(false);
        /* Les objets renvoyés par une requête sont forcéments tous de même type
           On récupère donc le type d'objets constituant la requête en se basant sur le type du 1er objet de la liste
           Ce 1e objet peut être différent du 1e élément de la liste, si ce dernier est null par exemple */
        if (listeResultats.size() > 0) {
            // pas de page d'exit : traitement par defaut
            int i = 0;
            FicheUniv fiche = null;
            while (fiche == null && i < listeResultats.size()) {
                fiche = RequeteUtil.lireFiche(listeResultats.get(i));
                i++;
            }
            if (fiche != null) {
                final String nomObjet = ReferentielObjets.getNomObjet(fiche);
                final String style = RequeteUtil.renvoyerParametre(param, "STYLE");
                final String affichage = PropertyHelper.getProperty(ReferentielObjets.getExtension(fiche), "fiche." + nomObjet.toUpperCase() + ".style_affichage");
                String template = "";
                if ("2".equals(affichage) || ("1".equals(affichage) && style.length() > 0) || FicheUnivMgr.isFicheCollaborative(fiche)) {
                    template = ObjetPartageHelper.getTemplateObjet(ObjetPartageHelper.TEMPLATE_REQUETE_RESULTAT, nomObjet.toLowerCase());
                }
                if (StringUtils.isEmpty(template)) {
                    template = ObjetPartageHelper.getTemplateObjet(ObjetPartageHelper.TEMPLATE_REQUETE_RESULTAT, ObjetPartageHelper.PATH_TEMPLATE_REQUETE_RESULTAT_DEFAULT);
                }
                if (StringUtils.isNotEmpty(template) && ctx.getRequeteHTTP() != null) {
                    ctx.getRequeteHTTP().setAttribute("param", param);
                    ctx.getRequeteHTTP().setAttribute("listeFiches", listeResultats);
                    retour = JSPIncludeHelper.getOutputJsp(ctx.getServletContext(), ctx.getRequeteHTTP(), ctx.getReponseHTTP(), template);
                }
            }
        }
        return retour;
    }

    /**
     * Méthode exécutant la requête
     * @param param
     * @return
     * @throws Exception
     */
    protected List<ResultatRecherche> performRequest(final String param) throws Exception {
        final CacheRequestManager cacheRequestManager = (CacheRequestManager) ApplicationContextManager.getCoreContextBean(CacheRequestManager.ID_BEAN);
        return (List<ResultatRecherche>) cacheRequestManager.call(ObjetsCacheRequest.ID_BEAN, param);
    }

    @Override
    public String getReferenceTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        return "";
    }
}
