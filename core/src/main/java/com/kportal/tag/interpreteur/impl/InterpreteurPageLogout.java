package com.kportal.tag.interpreteur.impl;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;

public class InterpreteurPageLogout extends AbstractInterpreteurTag {

    @Override
    public String interpreterTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        return "<a href=\"" + URLResolver.getAbsoluteUrl(URLResolver.getAbsoluteUrl(UnivWebFmt.getUrlDeconnexion(ctx), ctx), ctx) + "\">" + getContenuDuLien(texteAInterpreter) + "</a>";
    }

    @Override
    public String getReferenceTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        return StringUtils.EMPTY;
    }

    private String getContenuDuLien(final String texteAInterpreter) {
        String texteDuLien = StringUtils.EMPTY;
        final int indexDebutContenuLien = StringUtils.indexOf(texteAInterpreter, ">");
        final int indexFinContenuLien = StringUtils.lastIndexOf(texteAInterpreter, "<");
        if (indexDebutContenuLien < indexFinContenuLien) {
            texteDuLien = StringUtils.substring(texteAInterpreter, indexDebutContenuLien + 1, indexFinContenuLien);
        }
        return texteDuLien;
    }
}
