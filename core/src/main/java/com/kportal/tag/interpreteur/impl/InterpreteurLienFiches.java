package com.kportal.tag.interpreteur.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheObjet;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.IRequeteurConstantes;
import com.univ.utils.RequeteUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;

public class InterpreteurLienFiches extends AbstractInterpreteurTag {

    private static final Logger LOG = LoggerFactory.getLogger(InterpreteurLienFiches.class);

    private ServiceRubrique serviceRubrique;

    private ServiceGroupeDsi serviceGroupeDsi;

    private ServiceStructure serviceStructure;

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void setServiceGroupeDsi(final ServiceGroupeDsi serviceGroupeDsi) {
        this.serviceGroupeDsi = serviceGroupeDsi;
    }

    public void setServiceStructure(final ServiceStructure serviceStructure) {
        this.serviceStructure = serviceStructure;
    }

    @Override
    public String interpreterTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) throws Exception {
        String param = StringUtils.substringBetween(texteAInterpreter, baliseOuvrante, baliseFermante);
        param = StringEscapeUtils.unescapeHtml4(param);
        param = StringUtils.replace(param, "#", "&");
        return formatTagLienFiche(ContexteUtil.getContexteUniv(), param);
    }

    /**
     * @param _ctx
     * @param paramExit
     * @throws Exception
     * @throws UnsupportedEncodingException
     */
    private String formatTagLienFiche(final ContexteUniv _ctx, final String paramExit) throws Exception {
        String res = "";
        boolean erreur = false;
        boolean modificationFiche = false;
        boolean suppressionFiche = false;
        boolean creationFiche = false;
        boolean validationFiche = false;
        final Map<String, Object> treeNomObjet = new TreeMap<>();
        final String chaineObjet = RequeteUtil.renvoyerParametre(paramExit, "OBJET", true);
        final String actionLien = RequeteUtil.renvoyerParametre(paramExit, "ACTION", true);
        final String idFiche = RequeteUtil.renvoyerParametre(paramExit, "ID_FICHE");
        String indiceAction = "";
        // on teste d'abord la validite de l'action
        if ("AJOUTER".equals(actionLien)) {
            creationFiche = true;
            indiceAction = "C";
        } else if ("MODIFIER".equals(actionLien)) {
            modificationFiche = true;
            indiceAction = "M";
        } else if ("SUPPRIMER".equals(actionLien)) {
            suppressionFiche = true;
            indiceAction = "S";
        } else if ("VALIDER".equals(actionLien)) {
            validationFiche = true;
            indiceAction = "V";
        } else {
            erreur = true;
        }
        // modification ou suppression on teste la fiche courante
        if (modificationFiche || suppressionFiche) {
            if (idFiche.length() > 0 && chaineObjet.length() > 0) {
                final FicheUniv fiche = ReferentielObjets.instancierFiche(chaineObjet.toUpperCase());
                if (fiche != null) {
                    fiche.init();
                    fiche.setCtx(_ctx);
                    try {
                        final String nomObjet = chaineObjet.toUpperCase();
                        fiche.setIdFiche(Long.parseLong(idFiche));
                        fiche.retrieve();
                        treeNomObjet.put(nomObjet, fiche);
                    } catch (final Exception e) {
                        LOG.debug("an error occured while trying to get the ficheUniv", e);
                        erreur = true;
                    }
                }
            } else if (_ctx.getFicheCourante() != null) {
                final FicheUniv fiche = _ctx.getFicheCourante();
                final String nomObjet = ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjet(_ctx.getFicheCourante())).toUpperCase();
                treeNomObjet.put(nomObjet, fiche);
            } else {
                erreur = true;
            }
        }
        if (creationFiche) {
            Collection<String> listeObjets = null;
            if (chaineObjet.length() > 0) {
                listeObjets = Arrays.asList(chaineObjet.split(",", -2));
            } else {
                listeObjets = ReferentielObjets.getListeNomsObjet();
            }
            for (final String nomObj : listeObjets) {
                final FicheUniv fiche = ReferentielObjets.instancierFiche(nomObj);
                if (fiche != null) {
                    fiche.init();
                    if (fiche instanceof DiffusionSelective) {
                        ((DiffusionSelective) fiche).setDiffusionPublicVise("");
                        if (StringUtils.isNotEmpty(_ctx.getEspaceCourant())) {
                            ((DiffusionSelective) fiche).setDiffusionModeRestriction("4");
                            ((DiffusionSelective) fiche).setDiffusionPublicViseRestriction(_ctx.getEspaceCourant());
                        } else {
                            ((DiffusionSelective) fiche).setDiffusionModeRestriction("");
                            ((DiffusionSelective) fiche).setDiffusionPublicViseRestriction("");
                        }
                    }
                    treeNomObjet.put(nomObj, fiche);
                }
            }
        }
        // validation test generique sur le droit de validation on met une fiche bidon
        if (validationFiche) {
            for (final String nom : ReferentielObjets.getListeNomsObjet()) {
                final FicheUniv ficheInstancie = ReferentielObjets.instancierFiche(nom);
                if (ficheInstancie != null) {
                    treeNomObjet.put(nom.toUpperCase(), ficheInstancie);
                    break;
                }
            }
        }
        if (treeNomObjet.size() == 0) {
            erreur = true;
        }
        if (!erreur) {
            String title = RequeteUtil.renvoyerParametre(paramExit, "TITLE");
            String libelle = RequeteUtil.renvoyerParametre(paramExit, "LIBELLE");
            String classCss = RequeteUtil.renvoyerParametre(paramExit, "CLASSE_CSS");
            if (classCss.length() > 0) {
                classCss = "class=\"" + classCss.toLowerCase() + "\"";
            }
            final String separateur = RequeteUtil.renvoyerParametre(paramExit, "SEPARATEUR");
            String codeRattachement = RequeteUtil.renvoyerParametre(paramExit, "CODE_RATTACHEMENT");
            String codeRubrique = RequeteUtil.renvoyerParametre(paramExit, "CODE_RUBRIQUE");
            String chaineDsi = RequeteUtil.renvoyerParametre(paramExit, "DIFFUSION_PUBLIC_VISE");
            String urlRedirect = RequeteUtil.renvoyerParametre(paramExit, "URL_REDIRECT");
            InfoBean infoBean = null;
            if (_ctx.getRequeteHTTP() != null) {
                infoBean = (InfoBean) _ctx.getRequeteHTTP().getAttribute("infoBean");
            }
            final List<String> listeGroupe = new ArrayList<>();
            // Permet de gérer le URL redirect
            if (StringUtils.isEmpty(urlRedirect) && _ctx.getRequeteHTTP() != null && !suppressionFiche) {
                if (_ctx.getRequeteHTTP().getAttribute("URL_REDIRECT") != null) {
                    urlRedirect += _ctx.getRequeteHTTP().getAttribute("URL_REDIRECT");
                } else if (infoBean != null && infoBean.get("URL_REDIRECT") != null) {
                    urlRedirect += ((InfoBean) _ctx.getRequeteHTTP().getAttribute("infoBean")).get("URL_REDIRECT");
                } else if (StringUtils.isNotBlank(_ctx.getUrlPageCourante())) {
                    urlRedirect = _ctx.getUrlPageCourante();
                }
            }
            // boucle sur les fiches
            for (final String nomObjet : treeNomObjet.keySet()) {
                // on reinitialise les variables a chaque objet
                boolean afficheLien = true;
                boolean saisieAnonyme = false;
                boolean prevaloriseStructure = false;
                boolean prevaloriseRubrique = false;
                boolean prevalorisePublicVise = false;
                final FicheUniv fiche = (FicheUniv) treeNomObjet.get(nomObjet);
                final String codeObjet = ReferentielObjets.getCodeObjet(fiche);
                if (creationFiche) {
                    if (StringUtils.isBlank(libelle)) {
                        libelle = "Saisir une fiche " + ReferentielObjets.getLibelleObjet(codeObjet);
                    }
                    if (StringUtils.isBlank(title)) {
                        title = "Saisir une fiche " + ReferentielObjets.getLibelleObjet(codeObjet);
                    }
                }
                if (modificationFiche) {
                    if (StringUtils.isBlank(libelle)) {
                        libelle = "Modifier la fiche " + ReferentielObjets.getLibelleObjet(codeObjet);
                    }
                    if (StringUtils.isBlank(title)) {
                        title = "Modifier la fiche " + ReferentielObjets.getLibelleObjet(codeObjet);
                    }
                }
                if (suppressionFiche) {
                    if (StringUtils.isBlank(libelle)) {
                        libelle = "Supprimer la fiche " + ReferentielObjets.getLibelleObjet(codeObjet);
                    }
                    if (StringUtils.isBlank(title)) {
                        title = "Supprimer la fiche " + ReferentielObjets.getLibelleObjet(codeObjet);
                    }
                }
                if (validationFiche) {
                    if (StringUtils.isBlank(libelle)) {
                        libelle = "Valider mes fiches";
                    }
                    if (StringUtils.isBlank(title)) {
                        title = "Valider mes fiches";
                    }
                }
                final String anonyme = PropertyHelper.getCoreProperty(nomObjet.toLowerCase() + ".anonyme");
                // si mode anonyme ou bien utilisateur deja loggue
                if (creationFiche && (anonyme != null && "1".equals(anonyme))) {
                    saisieAnonyme = true;
                }
                // prevalorisation en creation
                if (creationFiche) {
                    // la structure est renseignee pour prevalorisation
                    if (codeRattachement.length() > 0) {
                        // on regarde si la structure existe bien
                        if (serviceStructure.getByCodeLanguage(codeRattachement, _ctx.getLangue(), false) != null) {
                            fiche.setCodeRattachement(codeRattachement);
                            prevaloriseStructure = true;
                        }
                    } else {
                        if (_ctx.getAutorisation() != null && _ctx.getAutorisation().possedePermission(new PermissionBean("FICHE", ReferentielObjets.getCodeObjet(fiche), indiceAction))) {
                            final List<Perimetre> vPerimetre = _ctx.getAutorisation().getListePerimetres(new PermissionBean("FICHE", ReferentielObjets.getCodeObjet(fiche), indiceAction));
                            final String structureUtilisateur = _ctx.getAutorisation().getCodeStructure();
                            /*
                             * Si l'utilisateur a une structure de rattachement
                             * ET structure inclue dans le perimetre alors
                             * coderattachement = codestructure Sinon code
                             * structure un la premiere structure du perimïetre
                             */
                            if (structureUtilisateur.length() > 0) {
                                // On regarde si l'autorisation est
                                // compatible avec la rubrique de plus
                                // haut niveau
                                if (_ctx.getAutorisation().possedePermission(new PermissionBean("FICHE", ReferentielObjets.getCodeObjet(fiche), indiceAction), new Perimetre(structureUtilisateur, "*", "*", "*", ""))) {
                                    codeRattachement = structureUtilisateur;
                                }
                                fiche.setCodeRattachement(codeRattachement);
                                prevaloriseStructure = true;
                            } else {
                                for (final Perimetre perimetreCourant : vPerimetre) {
                                    if (perimetreCourant != null && !"".equals(perimetreCourant.getCodeStructure())) {
                                        codeRattachement = perimetreCourant.getCodeStructure();
                                        fiche.setCodeRattachement(codeRattachement);
                                        prevaloriseStructure = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    // la rubrique est renseignee pour prevalorisation
                    if (codeRubrique.length() > 0) {
                        // rubrique dynamique
                        if (codeRubrique.equals(IRequeteurConstantes.CODE_DYNAMIQUE)) {
                            codeRubrique = _ctx.getCodeRubriquePageCourante();
                        } else if ("ALL".equals(codeRubrique)) {
                            codeRubrique = "*";
                        } else if (!serviceRubrique.codeAlreadyExist(codeRubrique)) {
                            codeRubrique = "";
                        }
                    }
                    if (codeRubrique.length() > 0) {
                        fiche.setCodeRubrique(codeRubrique);
                        prevaloriseRubrique = true;
                    }
                    // le public vise est renseigne pour prevalorisation
                    if (chaineDsi.length() > 0) {
                        if (chaineDsi.equals(IRequeteurConstantes.CODE_DYNAMIQUE)) {
                            // on prend les groupes de l'utilisaeur
                            if (_ctx.getAutorisation() != null) {
                                if (_ctx.getGroupesDsi() != null) {
                                    for (final String codeGroupe : _ctx.getGroupesDsi()) {
                                        listeGroupe.add("[/" + codeGroupe + "]");
                                    }
                                }
                            }
                        } else {
                            // soit les couples profil/groupe existent et sont
                            // prevalorise
                            final String[] chaineDsiItem = chaineDsi.split(",");
                            chaineDsi = "";
                            for (final String element : chaineDsiItem) {
                                String itemGroupe = element;
                                // Pb de codes groupes contenant des virgules on a remplace les ',' par des '@'
                                itemGroupe = itemGroupe.replaceAll("@", ",");
                                final GroupeDsiBean group = serviceGroupeDsi.getByCode(itemGroupe);
                                if (group != null && !"".equals(group.getCode())) {
                                    listeGroupe.add("[/" + itemGroupe + "]");
                                }
                            }
                        }
                    }
                    if (fiche instanceof DiffusionSelective && listeGroupe.size() > 0) {
                        chaineDsi = StringUtils.join(listeGroupe, ";");
                        ((DiffusionSelective) fiche).setDiffusionPublicVise(chaineDsi);
                        prevalorisePublicVise = true;
                    }
                }
                // si un ou plusieurs public vise est specifie dans le lien on verifie que l'utilisateur
                // dispose des droits de diffusion pour tous les publics public vise.
                final PermissionBean permBean = new PermissionBean("TECH", "dsi", "");
                boolean permissionDsi = true;
                // si au moins un groupe dsi est spécifié sur le lien ...
                if (_ctx.getAutorisation() != null && StringUtils.isNotBlank(chaineDsi)) {
                    // on vérifie que l'utilisateur a le droit pour chacun de ces groupes
                    final String[] codesGroupeDsi = StringUtils.remove(StringUtils.remove(chaineDsi, "[/"), "]").split(";");
                    for (int i = 0; i < codesGroupeDsi.length && permissionDsi; i++) {
                        permissionDsi = _ctx.getAutorisation().possedePermissionPartielleSurPerimetre(permBean, new Perimetre("", "", "", codesGroupeDsi[i], ""));
                    }
                }
                // controle des droits on regarde si l'utilisateur logge
                // a les droits sur l'action et le perimetre donne
                if (!saisieAnonyme) {
                    if (_ctx.getAutorisation() != null) {
                        if (suppressionFiche) {
                            afficheLien = _ctx.getAutorisation().estAutoriseASupprimerLaFiche(fiche) && permissionDsi;
                        } else if (modificationFiche) {
                            afficheLien = _ctx.getAutorisation().estAutoriseAModifierLaFiche(fiche) && permissionDsi;
                        } else if (creationFiche) {
                            afficheLien = _ctx.getAutorisation().getAutorisationParFiche(fiche, AutorisationBean.INDICE_CREATION) && permissionDsi;
                        } else if (validationFiche) {
                            afficheLien = _ctx.getAutorisation().isValidateur() && permissionDsi;
                        }
                    } else {
                        afficheLien = false;
                    }
                }
                // si on a saisi '*' comme code rubrique (permet de valider sur tout perimetre)
                if ("*".equals(codeRubrique)) {
                    codeRubrique = "";
                }
                if (afficheLien) {
                    String lien = "";
                    String libelleLien = libelle;
                    if (creationFiche && "objet".equalsIgnoreCase(libelle)) {
                        libelleLien = ReferentielObjets.getLibelleObjet(codeObjet);
                    }
                    if (!validationFiche && FicheAnnotationHelper.isSaisieFrontOffice(fiche) && !(creationFiche && (fiche instanceof FicheObjet))) {
                        String requete = WebAppUtil.SG_PATH + "?EXT=" + ReferentielObjets.getExtension(codeObjet) + "&amp;PROC=SAISIE_" + nomObjet.toUpperCase() + "_FRONT&amp;ACTION=" + actionLien + "&amp;SAISIE_FRONT=TRUE";
                        if (suppressionFiche) {
                            requete += "&amp;TYPE_FICHE=" + nomObjet;
                        }
                        if (modificationFiche || suppressionFiche) {
                            requete += "&amp;ID_FICHE=" + fiche.getIdFiche().toString();
                        }
                        if (prevaloriseRubrique && !suppressionFiche) {
                            requete += "&amp;PREV_CODE_RUBRIQUE=" + codeRubrique;
                        }
                        if (prevaloriseStructure && !suppressionFiche) {
                            requete += "&amp;PREV_CODE_RATTACHEMENT=" + codeRattachement;
                        }
                        if (prevalorisePublicVise && !suppressionFiche) {
                            requete += "&amp;PREV_DIFFUSION_PUBLIC_VISE=" + chaineDsi;
                        }
                        if (urlRedirect.length() > 0) {
                            requete += "&amp;URL_REDIRECT=" + URLEncoder.encode(urlRedirect, CharEncoding.DEFAULT);
                        }
                        lien = "<a " + classCss + " " + (StringUtils.isNotBlank(title) ? "title=\"" + title + "\" " : StringUtils.EMPTY) + "href=\"" + URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlRelative(_ctx, requete, true), _ctx) + "\">" + libelleLien + "</a>";
                    } else {
                        if (_ctx.getAutorisation() != null && _ctx.getAutorisation().possedeModeExpert() && !(creationFiche && (fiche instanceof FicheObjet))) {
                            if (creationFiche || modificationFiche || suppressionFiche) {
                                String requete = WebAppUtil.SG_PATH + "?EXT=" + ReferentielObjets.getExtension(codeObjet) + "&amp;PROC=" + ReferentielObjets.getProcessus(codeObjet) + "&amp;ACTION=" + actionLien;
                                if (modificationFiche || suppressionFiche) {
                                    requete += "&amp;ID_FICHE=" + fiche.getIdFiche().toString();
                                } else {
                                    if (prevaloriseRubrique) {
                                        requete += "&amp;PREV_CODE_RUBRIQUE=" + codeRubrique;
                                    }
                                    if (prevaloriseStructure) {
                                        requete += "&amp;PREV_CODE_RATTACHEMENT=" + codeRattachement;
                                    }
                                    if (prevalorisePublicVise) {
                                        requete += "&amp;PREV_DIFFUSION_PUBLIC_VISE=" + chaineDsi;
                                    }
                                }
                                lien = "<a " + classCss + " " + (StringUtils.isNotBlank(title) ? "title=\"" + title + "\" " : StringUtils.EMPTY) + "href=\"" + URLResolver.getAbsoluteBoUrl(requete, _ctx.getInfosSite()) + "\" onclick=\"window.open(this.href,'_blank','');return false;\">" + libelleLien + "</a>";
                            } else if (validationFiche) {
                                lien = "<a " + classCss + " " + (StringUtils.isNotBlank(title) ? "title=\"" + title + "\" " : StringUtils.EMPTY) + "href=\"" + URLResolver.getAbsoluteBoUrl(WebAppUtil.SG_PATH + "?EXT=core&amp;PROC=VALIDATION&amp;ACTION=LISTE", _ctx.getInfosSite()) + "\" onclick=\"window.open(this.href,'_blank','');return false;\">" + libelleLien + "</a>";
                            }
                        }
                    }
                    if (lien.length() > 0) {
                        if ("li".equalsIgnoreCase(separateur)) {
                            res += "<li>" + lien + "</li>\r\n";
                        } else if (res.length() > 0 && "br".equalsIgnoreCase(separateur)) {
                            res += "<br />" + lien;
                        } else if (res.length() > 0) {
                            res += "&nbsp;" + lien;
                        } else {
                            res += lien;
                        }
                    }
                }
            }// fin de boucle sur les fiches
            if ("li".equalsIgnoreCase(separateur)) {
                res = "<ul>" + res + "</ul>";
            }
        }
        return res;
    }

    @Override
    public String getReferenceTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        try {
            return interpreterTag(texteAInterpreter, baliseOuvrante, baliseFermante);
        } catch (final Exception e) {
            LOG.debug("unable to interpret the given tag", e);
            return StringUtils.EMPTY;
        }
    }
}
