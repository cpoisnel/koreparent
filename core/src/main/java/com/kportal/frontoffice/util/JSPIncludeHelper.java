package com.kportal.frontoffice.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.cms.objetspartages.ObjetPartageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.module.plugin.objetspartages.IPluginFiche;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper;
import com.kportal.servlet.BufferedHttpResponseWrapper;
import com.univ.utils.ContexteUtil;

/**
 * Classe apportant des méthodes utilitaire pour inclure des JSP en dynamique.
 *
 */
public class JSPIncludeHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(JSPIncludeHelper.class);

    /**
     * Calcule la JSP à inclure pour l'écran de processus courant.
     * @param infoBean l'infoBean courant
     * @param front est ce qu'on est sur un processus front?
     * @return la JSP à inclure
     * @throws FileNotFoundException lorsque la JSP n'est pas trouvé
     * @deprecated cette méthode ne doit plus être appeler dans les JSP principales, il faut uniquement appeler infoBean.getEcranPhysique()
     */
    @Deprecated
    public static String getJSPAInclure(final InfoBean infoBean, final boolean front) throws FileNotFoundException {
        String path = infoBean.getEcranPhysique();
        path = ExtensionHelper.getTemplateExtension(infoBean.getNomExtension(), path, front);
        if (StringUtils.isEmpty(path)) {
            throw new FileNotFoundException(String.format("pas d'ecran physique trouvé pour l'extension : %s, ayant pour écran physique d'origine %s, et pour le contexte front %s", infoBean.getNomExtension(), infoBean.getEcranPhysique(), front));
        }
        return path;
    }

    /**
     * Renvoie le flux d'un template jsp
     *
     * @param out le flux de sortie de la JSP
     * @param context le contexte pour récupérer le dispatcher
     * @param request la requete d'origine
     * @param response la réponse à retourner
     * @param path le chemin de la JSP à appeler
     * @return le contenu interpreter de la JSP
     * @throws ServletException
     * @throws IOException
     * @deprecated cette méthode inclue le JspWriter sans raison.
     */
    @Deprecated
    public static String getOutputJsp(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String path) throws ServletException, IOException {
        return getOutputJsp(context, request, response, path);
    }

    /**
     * Renvoie le flux d'un template jsp
     *
     * @param context le contexte pour récupérer le dispatcher
     * @param request la requete d'origine
     * @param response la réponse à retourner
     * @param path le chemin de la JSP à appeler
     * @return le contenu interpreter de la JSP
     * @throws ServletException
     * @throws IOException
     */
    public static String getOutputJsp(final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String path) throws ServletException, IOException {
        if (isNotValidParameter(context, request, response, path)) {
            return StringUtils.EMPTY;
        }
        final BufferedHttpResponseWrapper wrapper = new BufferedHttpResponseWrapper(response);
        final RequestDispatcher dispatcher = context.getRequestDispatcher(path);
        dispatcher.include(request, wrapper);
        return StringUtils.replaceChars(StringUtils.replaceChars(wrapper.getOutput(), "\r", ""), "\n", "");
    }

    private static boolean isNotValidParameter(final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String path) {return !isValide(path) || context == null || request == null || response == null;}

    /**
     * Permet d'inclure une jsp
     *
     * @param out
     *            le writer dans lequel on va écrire la réponse de(s) JSP
     * @param context le servlet context pour récupérer le dispatcher
     * @param request la requete courante
     * @param response la réponse courante
     * @param path
     *            relatif par rapport à la racine de la webapp
     */
    public static void includeJsp(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String path) {
        includeJsp(out, context, request, response, path, Boolean.FALSE);
    }

    /**
     *
     * @param out le writer dans lequel on va écrire la réponse de(s) JSP
     * @param context le servlet context pour récupérer le dispatcher
     * @param request la requete courante
     * @param response la réponse courante
     * @param path relatif par rapport à la racine de la webapp
     * @param valid doit on vérifier que la JSP existe?
     */
    public static void includeJsp(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String path, final boolean valid) {
        if (StringUtils.isEmpty(path) || (!valid && !isValide(path))) {
            return;
        }
        // inclusion
        try {
            out.flush();
            context.getRequestDispatcher(path).include(request, response);
            out.flush();
        } catch (final IOException | ServletException e) {
            LOGGER.error("Impossible de faire un include pour le path " + path, e);
        }
    }

    /**
     * Permet d'inclure le template d'un objet
     *
     * @param out
     *            le writer dans lequel on va écrire la réponse de(s) JSP
     * @param context
     *            le context pour pouvoir appeler le dispatcher de(s) JSP
     * @param request
     *            la request à fournir au dispatcher lors de l'include
     * @param response
     *            la response à fournir au dispatcher lors de l'include
     * @param objet
     *            le nom de l'objet
     * @param templateJsp
     *            le nom du template jsp ex : resultat-requete cf ObjetPartageHelper
     */
    public static void includeObjetTemplate(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String objet, final String templateJsp) {
        includeJsp(out, context, request, response, ObjetPartageHelper.getTemplateObjet(templateJsp, objet), Boolean.TRUE);
    }

    /**
     * Permet d'inclure une jsp relative d'une extension
     *
     * @param out
     *            le writer dans lequel on va écrire la réponse de(s) JSP
     * @param context
     *            le context pour pouvoir appeler le dispatcher de(s) JSP
     * @param request
     *            la request à fournir au dispatcher lors de l'include
     * @param response
     *            la response à fournir au dispatcher lors de l'include
     * @param idExtension
     *            l'id de l'extension si besoin
     * @param pathRelatif
     *            le path relatif de la jsp
     */
    public static void includeExtensionTemplate(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String idExtension, final String pathRelatif) {
        includeJsp(out, context, request, response, ExtensionHelper.getTemplateExtension(idExtension, pathRelatif));
    }

    /**
     * Permet d'inclure les JSP d'affichage FO des plugins associés à la fiche courante
     *
     * @param out
     *            le writer dans lequel on va écrire la réponse de(s) JSP
     * @param context
     *            le context pour pouvoir appeler le dispatcher de(s) JSP
     * @param request
     *            la request à fournir au dispatcher lors de l'include
     * @param response
     *            la response à fournir au dispatcher lors de l'include
     */
    public static void includePluginFicheTemplates(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response) {
        if (ContexteUtil.getContexteUniv().getFicheCourante() != null) {
            includePluginFicheJsp(out, context, request, response, ContexteUtil.getContexteUniv().getFicheCourante().getClass().getName(), PluginFicheHelper.TEMPLATE_FO);
        }
    }

    /**
     * Permet d'inclure les JSP de saisie BO des plugins associés à la fiche courante
     *
     * @param out
     *            le writer dans lequel on va écrire la réponse de(s) JSP
     * @param context
     *            le context pour pouvoir appeler le dispatcher de(s) JSP
     * @param request
     *            la request à fournir au dispatcher lors de l'include
     * @param response
     *            la response à fournir au dispatcher lors de l'include
     * @param classe
     *            la classe de l'objet contenant peut être des plugins
     */
    public static void includePluginFicheSaisiesBo(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String classe) {
        includePluginFicheJsp(out, context, request, response, classe, PluginFicheHelper.SAISIE_BO);
    }

    /**
     * Permet d'inclure les JSP de saisie FO des plugins associés à la fiche courante
     *
     * @param out
     *            le writer dans lequel on va écrire la réponse de(s) JSP
     * @param context
     *            le context pour pouvoir appeler le dispatcher de(s) JSP
     * @param request
     *            la request à fournir au dispatcher lors de l'include
     * @param response
     *            la response à fournir au dispatcher lors de l'include
     */
    public static void includePluginFicheSaisiesFo(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String classe) {
        includePluginFicheJsp(out, context, request, response, classe, PluginFicheHelper.SAISIE_FO);
    }

    private static void includePluginFicheJsp(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String classe, final int mode) {
        for (final IPluginFiche controleur : PluginFicheHelper.getPlugins()) {
            if (controleur.isActive(classe)) {
                String pathRelatif = "";
                switch (mode) {
                    case PluginFicheHelper.SAISIE_BO:
                        pathRelatif = controleur.getPathSaisieBo();
                        break;
                    case PluginFicheHelper.SAISIE_FO:
                        pathRelatif = controleur.getPathSaisieFo();
                        break;
                    case PluginFicheHelper.TEMPLATE_FO:
                        pathRelatif = controleur.getPathTemplateFo();
                        break;
                    default:
                        break;
                }
                if (StringUtils.isNotEmpty(pathRelatif)) {
                    includeJsp(out, context, request, response, ExtensionHelper.getTemplateExtension(controleur.getIdExtension(), pathRelatif), Boolean.TRUE);
                }
            }
        }
    }

    private static boolean isValide(final String path) {
        boolean res = Boolean.FALSE;
        // teste que le path (relatif par rapport à la racine de l'application) existe bien physiquement
        final String webAppPath = WebAppUtil.getAbsolutePath();
        if (!StringUtils.isEmpty(path) && webAppPath.length() > 0) {
            if (new File(webAppPath + path).exists()) {
                res = Boolean.TRUE;
            }
        }
        return res;
    }
}
