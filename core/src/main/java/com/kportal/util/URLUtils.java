package com.kportal.util;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.EscapeString;

public class URLUtils {

    public static String addParameter(String ecranRedirection, String parametre, String value) {
        if (StringUtils.isNotEmpty(parametre) && StringUtils.isNotEmpty(value)) {
            if (ecranRedirection.contains("?")) {
                ecranRedirection += "&";
            } else {
                ecranRedirection += "?";
            }
            ecranRedirection += parametre + "=" + EscapeString.escapeURL(value);
        }
        return ecranRedirection;
    }
}
