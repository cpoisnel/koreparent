package com.kportal.util.compress;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
// TODO: Auto-generated Javadoc

/**
 * The Class Zip.
 */
public class Zip {

    /** The Constant ZIP_EXTENSION. */
    private static final String ZIP_EXTENSION = ".zip";

    /** The Constant DEFAULT_LEVEL_COMPRESSION. */
    private static final int DEFAULT_LEVEL_COMPRESSION = ZipArchiveOutputStream.DEFAULT_COMPRESSION;

    private static int BUFFER_SIZE = 8192;
    // Remplace l'extension si le fichier cible ne fini pas par '.zip'

    /**
     * Gets the zip type file.
     *
     * @param source
     *            the source
     * @param target
     *            the target
     *
     * @return the zip type file
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private static File getZipTypeFile(final File source, final File target) throws IOException {
        if (target.getName().toLowerCase().endsWith(ZIP_EXTENSION)) {
            return target;
        }
        final String tName = target.isDirectory() ? source.getName() : target.getName();
        final int index = tName.lastIndexOf('.');
        return new File((target.isDirectory() ? target.getCanonicalPath() : target.getParentFile().getCanonicalPath()) + File.separatorChar + (index < 0 ? tName : tName.substring(0, index)) + ZIP_EXTENSION);
    }
    // Compresse un fichier

    /**
     * Compress file.
     *
     * @param out
     *            the out
     * @param parentFolder
     *            the parent folder
     * @param file
     *            the file
     * @param inclureParentFolder
     *            the inclure parent folder
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private static void compressFile(final ZipArchiveOutputStream out, final String parentFolder, final File file, boolean inclureParentFolder) throws IOException {
        String zipName = "";
        if (!file.isDirectory() || inclureParentFolder) {
            zipName = parentFolder + file.getName() + (file.isDirectory() ? "/" : "");
            // Définition des attributs du fichier
            final ZipArchiveEntry entry = new ZipArchiveEntry(file, zipName);
            entry.setSize(file.length());
            entry.setTime(file.lastModified());
            out.putArchiveEntry(entry);
            if (!file.isDirectory()) {
                // Ecriture du fichier dans le zip
                try (InputStream in = new BufferedInputStream(new FileInputStream(file))) {
                    final byte[] buf = new byte[BUFFER_SIZE];
                    int bytesRead;
                    while (-1 != (bytesRead = in.read(buf))) {
                        out.write(buf, 0, bytesRead);
                    }
                }
            }
            out.closeArchiveEntry();
        }
        // Traitement récursif s'il s'agit d'un répertoire
        if (file.isDirectory()) {
            File[] tf = file.listFiles();
            for (File element : tf) {
                compressFile(out, zipName, element, true);
            }
        }
    }
    // Compresse un fichier à l'adresse pointée par le fichier cible.
    // Remplace le fichier cible s'il existe déjà.

    /**
     * Compress.
     *
     * @param file
     *            the file
     * @param target
     *            the target
     * @param compressionLevel
     *            the compression level
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final File file, final File target, final int compressionLevel) throws IOException {
        compress(file, target, compressionLevel, true);
    }
    // Compresse un fichier à l'adresse pointée par le fichier cible.
    // Remplace le fichier cible s'il existe déjà.

    /**
     * Compress.
     *
     * @param file
     *            the file
     * @param target
     *            the target
     * @param compressionLevel
     *            the compression level
     * @param inclureParentFolder
     *            the inlure parent folder
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final File file, final File target, final int compressionLevel, boolean inclureParentFolder) throws IOException {
        final File source = file.getCanonicalFile();
        // Création du fichier zip
        final ZipArchiveOutputStream out = new ZipArchiveOutputStream(new FileOutputStream(getZipTypeFile(source, target.getCanonicalFile())));
        out.setMethod(ZipArchiveOutputStream.DEFLATED);
        out.setLevel(compressionLevel);
        // Ajout du(es) fichier(s) au zip
        compressFile(out, "", source, inclureParentFolder);
        out.close();
    }

    /**
     * Compress.
     *
     * @param file
     *            the file
     * @param compressionLevel
     *            the compression level
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final File file, final int compressionLevel) throws IOException {
        compress(file, file, compressionLevel);
    }

    /**
     * Compress.
     *
     * @param file
     *            the file
     * @param target
     *            the target
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final File file, final File target) throws IOException {
        compress(file, target, DEFAULT_LEVEL_COMPRESSION);
    }

    /**
     * Compress.
     *
     * @param file
     *            the file
     * @param target
     *            the target
     * @param inclureParentFolder
     *            the inclure parent folder
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final File file, final File target, boolean inclureParentFolder) throws IOException {
        compress(file, target, DEFAULT_LEVEL_COMPRESSION, inclureParentFolder);
    }

    /**
     * Compress.
     *
     * @param file
     *            the file
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final File file) throws IOException {
        compress(file, file, DEFAULT_LEVEL_COMPRESSION);
    }

    /**
     * Compress.
     *
     * @param fileName
     *            the file name
     * @param targetName
     *            the target name
     * @param compressionLevel
     *            the compression level
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final String fileName, final String targetName, final int compressionLevel) throws IOException {
        compress(new File(fileName), new File(targetName), compressionLevel);
    }

    /**
     * Compress.
     *
     * @param fileName
     *            the file name
     * @param compressionLevel
     *            the compression level
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final String fileName, final int compressionLevel) throws IOException {
        compress(new File(fileName), new File(fileName), compressionLevel);
    }

    /**
     * Compress.
     *
     * @param fileName
     *            the file name
     * @param targetName
     *            the target name
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final String fileName, final String targetName) throws IOException {
        compress(new File(fileName), new File(targetName), DEFAULT_LEVEL_COMPRESSION);
    }

    /**
     * Compress.
     *
     * @param fileName
     *            the file name
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void compress(final String fileName) throws IOException {
        compress(new File(fileName), new File(fileName), DEFAULT_LEVEL_COMPRESSION);
    }
    // Décompresse un fichier zip à l'adresse indiquée par le dossier

    /**
     * Decompress.
     *
     * @param file
     *            the file
     * @param folder
     *            the folder
     * @param deleteZipAfter
     *            the delete zip after
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void decompress(final File file, final File folder, final boolean deleteZipAfter) throws IOException {
        ArchiveEntry ze;
        try (ZipArchiveInputStream zis = new ZipArchiveInputStream(new BufferedInputStream(new FileInputStream(file.getCanonicalFile())))) {
            // Parcourt tous les fichiers
            while (null != (ze = zis.getNextEntry())) {
                final File f = new File(folder.getCanonicalPath(), ze.getName());
                if (f.exists()) {
                    f.delete();
                }
                // Création des dossiers
                if (ze.isDirectory()) {
                    f.mkdirs();
                    continue;
                }
                f.getParentFile().mkdirs();
                // Ecriture des fichiers
                try {
                    try (OutputStream fos = new BufferedOutputStream(new FileOutputStream(f))) {
                        final byte[] buf = new byte[BUFFER_SIZE];
                        int bytesRead;
                        while (-1 != (bytesRead = zis.read(buf))) {
                            fos.write(buf, 0, bytesRead);
                        }
                    }
                } catch (final IOException ioe) {
                    f.delete();
                    throw ioe;
                }
            }
        }
        if (deleteZipAfter) {
            file.delete();
        }
    }

    /**
     * Decompress.
     *
     * @param fileName
     *            the file name
     * @param folderName
     *            the folder name
     * @param deleteZipAfter
     *            the delete zip after
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void decompress(final String fileName, final String folderName, final boolean deleteZipAfter) throws IOException {
        decompress(new File(fileName), new File(folderName), deleteZipAfter);
    }

    /**
     * Decompress.
     *
     * @param fileName
     *            the file name
     * @param folderName
     *            the folder name
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void decompress(final String fileName, final String folderName) throws IOException {
        decompress(new File(fileName), new File(folderName), false);
    }

    /**
     * Decompress.
     *
     * @param file
     *            the file
     * @param deleteZipAfter
     *            the delete zip after
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void decompress(final File file, final boolean deleteZipAfter) throws IOException {
        decompress(file, file.getCanonicalFile().getParentFile(), deleteZipAfter);
    }

    /**
     * Decompress.
     *
     * @param fileName
     *            the file name
     * @param deleteZipAfter
     *            the delete zip after
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void decompress(final String fileName, final boolean deleteZipAfter) throws IOException {
        decompress(new File(fileName), deleteZipAfter);
    }

    /**
     * Decompress.
     *
     * @param file
     *            the file
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void decompress(final File file) throws IOException {
        decompress(file, file.getCanonicalFile().getParentFile(), false);
    }

    /**
     * Decompress.
     *
     * @param fileName
     *            the file name
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void decompress(final String fileName) throws IOException {
        decompress(new File(fileName));
    }
}