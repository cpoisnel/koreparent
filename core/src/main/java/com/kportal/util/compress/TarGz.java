package com.kportal.util.compress;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.lang3.StringUtils;

/**
 * Classe utilitaire pour générer des fichiers .tar ou .tar.gz
 */
public class TarGz {

    /**
     * l'extension des fichiers tar.
     */
    private static final String TAR_EXTENSION = ".tar";

    /**
     * l'extension des fichiers tgz.
     */
    private static final String GZ_EXTENSION = ".tar.gz";

    private static int BUFFER_SIZE = 8192;

    /**
     * Gets the zip type file.
     *
     * @param source the source
     * @param target the target
     * @return the zip type file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static File getTypeFile(final File source, final File target, String extension) throws IOException {
        if (target.getName().toLowerCase().endsWith(extension)) {
            return target;
        }
        final String tName = target.isDirectory() ? source.getName() : target.getName();
        final int index = tName.lastIndexOf('.');
        return new File((target.isDirectory() ? target.getCanonicalPath() : target.getParentFile().getCanonicalPath()) + File.separatorChar + (index < 0 ? tName : tName.substring(0, index)) + extension);
    }

    private static File getTarFile(File compress) throws IOException {
        if (compress.getName().toLowerCase().endsWith(TAR_EXTENSION)) {
            return compress;
        } else if (compress.getName().toLowerCase().endsWith(GZ_EXTENSION)) {
            return getTarFile(new File(StringUtils.removeEnd(compress.getCanonicalPath(), GZ_EXTENSION)));
        } else {
            return getTypeFile(compress, compress, TAR_EXTENSION);
        }
    }

    /**
     * Compresse un fichier
     *
     * @param out                 the out
     * @param parentFolder        the parent folder
     * @param file                the file
     * @param inclureParentFolder the inclure parent folder
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void tarFile(final TarArchiveOutputStream out, final String parentFolder, final File file, boolean inclureParentFolder) throws IOException {
        String zipName = "";
        if (!file.isDirectory() || inclureParentFolder) {
            zipName = parentFolder + file.getName() + (file.isDirectory() ? "/" : "");
            // Définition des attributs du fichier
            final TarArchiveEntry entry = new TarArchiveEntry(file, zipName);
            entry.setSize(file.length());
            entry.setModTime(file.lastModified());
            out.putArchiveEntry(entry);
            if (!file.isDirectory()) {
                // Ecriture du fichier dans le zip
                try (InputStream in = new BufferedInputStream(new FileInputStream(file))) {
                    final byte[] buf = new byte[BUFFER_SIZE];
                    int bytesRead;
                    while (-1 != (bytesRead = in.read(buf))) {
                        out.write(buf, 0, bytesRead);
                    }
                }
            }
            out.closeArchiveEntry();
        }
        // Traitement récursif s'il s'agit d'un répertoire
        if (file.isDirectory()) {
            File[] tf = file.listFiles();
            for (File element : tf) {
                tarFile(out, zipName, element, true);
            }
        }
    }

    /**
     * Compress.
     *
     * @param file               the file
     * @param target             the target
     * @param inlureParentFolder the inlure parent folder
     * @param deleteFileAfter    do we need to delete the original file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void compress(final File file, final File target, boolean inlureParentFolder, boolean deleteFileAfter) throws IOException {
        final File source = file.getCanonicalFile();
        // création du fichier tar
        File tarFile = getTypeFile(source, target.getCanonicalFile(), TAR_EXTENSION);
        final TarArchiveOutputStream tarOut = new TarArchiveOutputStream(new FileOutputStream(tarFile));
        tarOut.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
        tarOut.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
        // ajout du(es) fichier(s) au tar
        tarFile(tarOut, "", source, inlureParentFolder);
        tarOut.close();
        // gzip du fichier tar
        File gzFile = getTypeFile(source, tarFile.getCanonicalFile(), GZ_EXTENSION);
        final GzipCompressorOutputStream gzOut = new GzipCompressorOutputStream(new FileOutputStream(gzFile));
        gzipFile(gzOut, "", tarFile);
        gzOut.close();
        // suppression des sources
        tarFile.delete();
        if (deleteFileAfter) {
            source.delete();
        }
    }

    /**
     * Construire une archive "tar.gz" contenant al liste de fichier passée en paramétre. Le dosseir parent de chaque fichier sera présent dans l'archive.
     *
     * @param files            Liste des fichiers à archiver
     * @param archive          Archive de destination. Le fichier ne doit pas exister, il sera créé par la fonction.
     * @param deleteSourceFile Indiquer si on souhaite supprimer ou pas les fichiers sources aprés avoir réaliser l'archive.
     * @throws IOException Erreur durant les lectures ou écritures des différents fichiers.
     */
    public static void compressFiles(final Collection<File> files, final File archive, boolean deleteSourceFile) throws IOException {
        final File saveFolder = archive.getParentFile();
        final File tarFile = File.createTempFile("TMP_", TAR_EXTENSION, saveFolder);
        try {
            try (final FileOutputStream fosTar = new FileOutputStream(tarFile);
                 final TarArchiveOutputStream tarOut = new TarArchiveOutputStream(fosTar)) {
                tarOut.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
                tarOut.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
                for (File file : files) {
                    if (file.isFile()) {
                        final String parentFolder = file.getParent() + "/";
                        tarFile(tarOut, parentFolder, file, Boolean.FALSE);
                    }
                }
            }
            try (final FileOutputStream fosGz = new FileOutputStream(archive);
                 final GzipCompressorOutputStream gzOut = new GzipCompressorOutputStream(fosGz)) {
                gzipFile(gzOut, StringUtils.EMPTY, tarFile);
            }
            if (deleteSourceFile) {
                for (File file : files) {
                    if (file.isFile()) {
                        file.delete();
                    }
                }
            }
        } finally {
            tarFile.delete();
        }
    }

    private static void gzipFile(GzipCompressorOutputStream gzOut, String string, File gzFile) throws IOException {
        if (gzFile.exists()) {
            final InputStream in = new BufferedInputStream(new FileInputStream(gzFile));
            final byte[] buffer = new byte[BUFFER_SIZE];
            int n = 0;
            while (-1 != (n = in.read(buffer))) {
                gzOut.write(buffer, 0, n);
            }
            in.close();
        }
    }

    /**
     * Compress.
     *
     * @param file             the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void compress(final File file) throws IOException {
        compress(file, file);
    }

    /**
     * Compress.
     *
     * @param file   the file
     * @param target the target
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void compress(final File file, final File target) throws IOException {
        compress(file, target, false, false);
    }

    /**
     * Compress.
     *
     * @param fileName         the file name
     * @param targetName       the target name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void compress(final String fileName, final String targetName) throws IOException {
        compress(new File(fileName), new File(targetName));
    }

    /**
     * Compress.
     *
     * @param fileName         the file name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void compress(final String fileName) throws IOException {
        compress(new File(fileName), new File(fileName));
    }

    /**
     * Décompresse un fichier zip à l'adresse indiquée par le dossier
     *
     * @param file            the file
     * @param folder          the folder
     * @param deleteFileAfter the delete zip after
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void untar(final File file, final File folder, final boolean deleteFileAfter) throws IOException {
        ArchiveEntry ze;
        try (TarArchiveInputStream zis = new TarArchiveInputStream(new BufferedInputStream(new FileInputStream(file.getCanonicalFile())))) {
            // Parcourt tous les fichiers
            while (null != (ze = zis.getNextEntry())) {
                final File f = new File(folder.getCanonicalPath(), ze.getName());
                if (f.exists()) {
                    f.delete();
                }
                // Création des dossiers
                if (ze.isDirectory()) {
                    f.mkdirs();
                    continue;
                }
                f.getParentFile().mkdirs();
                // Ecriture des fichiers
                try {
                    try (OutputStream fos = new BufferedOutputStream(new FileOutputStream(f))) {
                        final byte[] buf = new byte[BUFFER_SIZE];
                        int bytesRead;
                        while (-1 != (bytesRead = zis.read(buf))) {
                            fos.write(buf, 0, bytesRead);
                        }
                    }
                } catch (final IOException ioe) {
                    f.delete();
                    throw ioe;
                }
            }
        }
        if (deleteFileAfter) {
            file.delete();
        }
    }

    /**
     * Decompress.
     *
     * @param deleteZipAfter the delete zip after
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void decompress(File compress, File target, boolean deleteZipAfter) throws IOException {
        File tarFile = getTarFile(compress);
        ungzip(compress, tarFile);
        untar(tarFile, target, deleteZipAfter);
    }

    private static void ungzip(File file, File target) throws IOException {
        FileInputStream fin = new FileInputStream(file);
        BufferedInputStream in = new BufferedInputStream(fin);
        FileOutputStream out = new FileOutputStream(target);
        GzipCompressorInputStream gzIn = new GzipCompressorInputStream(in);
        final byte[] buffer = new byte[BUFFER_SIZE];
        int n = 0;
        while (-1 != (n = gzIn.read(buffer))) {
            out.write(buffer, 0, n);
        }
        out.close();
        gzIn.close();
    }

    /**
     * Decompress.
     *
     * @param fileName   the file name
     * @param folderName the folder name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void decompress(final String fileName, final String folderName) throws IOException {
        decompress(new File(fileName), new File(folderName), false);
    }

    /**
     * Decompress.
     *
     * @param file           the file
     * @param deleteZipAfter the delete zip after
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void decompress(final File file, final boolean deleteZipAfter) throws IOException {
        decompress(file.getCanonicalFile(), file.getCanonicalFile().getParentFile(), deleteZipAfter);
    }

    /**
     * Decompress.
     *
     * @param fileName       the file name
     * @param deleteZipAfter the delete zip after
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void decompress(final String fileName, final boolean deleteZipAfter) throws IOException {
        decompress(new File(fileName), deleteZipAfter);
    }

    /**
     * Decompress.
     *
     * @param file the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void decompress(final File file) throws IOException {
        decompress(file, file.getCanonicalFile().getParentFile(), false);
    }

    /**
     * Decompress.
     *
     * @param fileName the file name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void decompress(final String fileName) throws IOException {
        decompress(new File(fileName));
    }
}