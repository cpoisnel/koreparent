package com.kportal.ihm;

import java.beans.Introspector;
import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.univ.url.BeanFrontManager;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

/**
 * Filtre permettant d'initialiser les beans utiliser en FO.
 */
public class FrontOfficeFilter implements Filter {

    @Override
    public void destroy() {}

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        final String path = ((HttpServletRequest) request).getRequestURI();
        if (!StringUtils.startsWith(path, "/adminsite")) {
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            ctx.calculerCodeRubriquePageCourante();
            final Map<String, BeanFrontManager> beansFrontParIdBean = ApplicationContextManager.getCoreContextBeansOfType(BeanFrontManager.class);
            for (final BeanFrontManager bean : beansFrontParIdBean.values()) {
                final Object beanFront = bean.initialiseBeanFront();
                if (beanFront != null) {
                    final String className = beanFront.getClass().getSimpleName();
                    request.setAttribute(Introspector.decapitalize(className), beanFront);
                }
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void init(final FilterConfig arg0) throws ServletException {}
}
