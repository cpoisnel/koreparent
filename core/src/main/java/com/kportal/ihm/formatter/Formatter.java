/**
 *
 */
package com.kportal.ihm.formatter;

/**
 * @author Kosmos
 *
 */
public interface Formatter<T> {

    String format(String type, T bean);
}
