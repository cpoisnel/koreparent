package com.kportal.ihm.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.kportal.extension.module.composant.ComposantHelper;
import com.kportal.extension.module.composant.IComposant;
import com.kportal.extension.module.composant.Menu;
import com.univ.objetspartages.om.AutorisationBean;

public class ServiceMenuBack {

    /**
     * Récupère le menu Back Office en fonction des autorisations fourni en paramètre
     *
     * @param autorisation les autorisations de l'utilisateurs
     * @return le menu principal du back office
     */
    public static Menu getMenuBackOfficeParUtilisateur(final AutorisationBean autorisation) {
        final Menu menuPrincipal = new Menu();
        final Map<String, Menu> menusPrincipaux = new HashMap<>();
        final Collection<Menu> sousMenus = new ArrayList<>();
        final Collection<IComposant> menuNonConstruit = ComposantHelper.getComposants();
        if (CollectionUtils.isNotEmpty(menuNonConstruit)) {
            for (final IComposant composant : menuNonConstruit) {
                if (composant.isVisible(autorisation)) {
                    final Menu menuCourant = construitMenuDepuisComposant(composant);
                    if (StringUtils.isBlank(composant.getIdMenuBoParent())) {
                        menusPrincipaux.put(composant.getId(), menuCourant);
                    } else {
                        sousMenus.add(menuCourant);
                    }
                }
            }
            for (final Menu menuCourant : sousMenus) {
                final Menu menuParent = menusPrincipaux.get(menuCourant.getIdMenuParent());
                if (menuParent != null) {
                    menuParent.addSousMenu(menuCourant);
                }
            }
            for (final Menu menuCourant : menusPrincipaux.values()) {
                Collections.sort(menuCourant.getSousMenu());
            }
        }
        menuPrincipal.addAllSousMenu(menusPrincipaux.values());
        Collections.sort(menuPrincipal.getSousMenu());
        return menuPrincipal;
    }

    public static Menu construitMenuDepuisComposant(final IComposant composant) {
        final Menu menuNiveau = new Menu();
        menuNiveau.setIdMenu(composant.getId());
        menuNiveau.setCode("menu_" + composant.getId());
        menuNiveau.setIdMenuParent(composant.getIdMenuBoParent());
        menuNiveau.setLibelle(composant.getLibelleAffichable());
        menuNiveau.setOrdre(composant.getOrdre());
        menuNiveau.setUrl(StringUtils.replace(composant.getUrlAccueilBo(), "&", "&amp;"));
        return menuNiveau;
    }
}
