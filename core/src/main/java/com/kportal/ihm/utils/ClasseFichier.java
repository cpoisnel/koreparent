package com.kportal.ihm.utils;

import java.util.Hashtable;

public final class ClasseFichier {

    private static ClasseFichier instance = new ClasseFichier();

    private final Hashtable<String, String> classesFichier = new Hashtable<>();

    private ClasseFichier() {
        classesFichier.put("application/pdf", "pdf");
        classesFichier.put("application/msword", "doc");
        classesFichier.put("application/vnd.ms-word", "doc");
        classesFichier.put("application/vnd.ms-excel", "xls");
        classesFichier.put("application/vnd.ms-powerpoint", "ppt");
        classesFichier.put("application/vnd.openxmlformats-officedocument.presentationml.presentation", "ppt");
        classesFichier.put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xls");
        classesFichier.put("application/vnd.openxmlformats-officedocument.wordprocessingml.document", "doc");
        classesFichier.put("application/x-zip-compressed", "zip");
        classesFichier.put("application/x-shockwave-flash", "swf");
        classesFichier.put("application/vnd.sun.xml.writer", "sxw");
        classesFichier.put("application/vnd.sun.xml.impress", "sxi");
        classesFichier.put("application/vnd.sun.xml.calc", "sxc");
        classesFichier.put("audio/x-mp3", "mp3");
        classesFichier.put("image/jpeg", "image");
        classesFichier.put("image/gif", "image");
        classesFichier.put("image/pjpeg", "image");
        classesFichier.put("image/png", "image");
        classesFichier.put("text/plain", "txt");
        classesFichier.put("video/x-flv", "flv");
    }

    public static ClasseFichier getInstance() {
        return instance;
    }

    public String get(final String key) {
        return classesFichier.get(key);
    }

    public boolean containsKey(final String key) {
        return classesFichier.containsKey(key);
    }
}
