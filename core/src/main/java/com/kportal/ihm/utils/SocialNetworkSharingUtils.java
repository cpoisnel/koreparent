package com.kportal.ihm.utils;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.ihm.utils.sharing.SocialNetworkUrl;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

/**
 * Classe utilitaire permettant de gérer des liens de partages vers twitter/facebook...
 */
public class SocialNetworkSharingUtils {

    public static final String TWITTER = "twitter";

    public static final String FACEBOOK = "facebook";

    public static final String GOOGLEPLUS = "googleplus";

    public static final String VIADEO = "viadeo";

    public static final String LINKEDIN = "linkedin";

    /**
     * Calcule l'URL de partage de la fiche courante pour un réseau social donné. Cette méthode est une méthode surchargée
     *
     * @param reseau
     *            Réseau social à utiliser
     * @return URL de partage calculée
     */
    public static String getSharingUrl(final String reseau) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        return getSharingUrl(ctx.getFicheCourante(), reseau);
    }

    /**
     * Calcule l'URL de partage d'une fiche pour un réseau social donné
     *
     * @param ficheUniv
     *            Fiche à utiliser
     * @param reseau
     *            Réseau social à utiliser
     * @return URL de partage calculée
     */
    public static String getSharingUrl(final FicheUniv ficheUniv, final String reseau) {
        final Map<String, SocialNetworkUrl> socialsNetworks = ApplicationContextManager.getAllBeansOfType(SocialNetworkUrl.class);
        String sharingUrl = StringUtils.EMPTY;
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        for (final SocialNetworkUrl socialNetwork : socialsNetworks.values()) {
            if (socialNetwork.getSocialNetworkName().equals(reseau)) {
                sharingUrl = socialNetwork.getSharingUrl(ficheUniv, ctx);
            }
        }
        return sharingUrl;
    }
}
