package com.kportal.ihm.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kosmos.usinesite.utils.InfosSiteHelper;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.composant.Menu;
import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;
import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.kportal.extension.module.plugin.rubrique.impl.BeanFichePageAccueil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.LigneContenu;
import com.univ.objetspartages.om.ParagrapheBean;
import com.univ.objetspartages.om.ParagrapheContenuHelper;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FileUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;

public class FrontUtil {

    public static final String NOM_PROPRIETE_LOGO = "LOGO";

    public static final String NOM_PROPRIETE_BASELINE = "BASELINE";

    public static final String NOM_PROPRIETE_COULEUR_PRINCIPALE = "COULEUR_PRINCIPAL";

    public static final String NOM_PROPRIETE_COULEUR_SECONDAIRE = "COULEUR_SECONDAIRE";

    public static final String NOM_PROPRIETE_MENU_PRINCIPAL = "MENU_PRINCIPAL";

    public static final String NOM_PROPRIETE_MENU_PIED_PAGE = "MENU_PIED_PAGE";

    public static final String NOM_PROPRIETE_NIVEAU_MENU_SECONDAIRE = "NIVEAU_MENU_SECONDAIRE";

    public static final String CATEGORIE_NAVIGATION = "NAV";

    public static final String CATEGORIE_LANGUE = "LANGUE";

    public static final String CATEGORIE_ACCES_DIRECT = "ACCES";

    public static final String CATEGORIE_PIED_PAGE = "PIEDPAGE";

    public static final String CATEGORIE_RESEAUX_SOCIAUX = "RX_SOC";

    public static final String RGBA_TEMPLATE = "rgba(%s,%s,%s,%s)";

    public static final String COLOR_PREFIX = "#";

    private static final Logger LOG = LoggerFactory.getLogger(FrontUtil.class);

    private static final int NIVEAU_MENU_SECONDAIRE = 2;

    /**
     * On gère 2 cas particuliers d'affichage : Cas de la page d'accueil du site : la fiche est page de tête de rubriqueSite Cas de la page d'accueil de rubrique : Si la fiche est
     * page de tête de rubrique de type langue alors c'est une page d'accueil de site.
     *
     * @return true si c'est l'accueil de site...
     */
    public static boolean isAccueilSite(final FicheUniv ficheUniv) {
        boolean isAccueil = Boolean.FALSE;
        if (ficheUniv != null && StringUtils.isNotBlank(ficheUniv.getCodeRubrique())) {
            final String codeRubriquePageCourante = ficheUniv.getCodeRubrique();
            final InfosSite siteCourant = getSiteCodeRubrique(codeRubriquePageCourante);
            if (siteCourant != null) {
                isAccueil = codeRubriquePageCourante.equals(siteCourant.getCodeRubrique());
                if (!isAccueil) {
                    isAccueil = isAccueilRubriqueLangue(codeRubriquePageCourante, siteCourant.getCodeRubrique());
                }
            }
        }
        return isAccueil;
    }

    private static boolean isAccueilRubriqueLangue(final String codeRubriquePageCourante, final String codeRubriqueSiteCourant) {
        Boolean isAccueil = Boolean.FALSE;
        final List<RubriqueBean> rubriquesLangues = getListeSousRubriquesDepuisCategorie(CATEGORIE_LANGUE, codeRubriqueSiteCourant);
        int i = 0;
        while (i < rubriquesLangues.size() && !isAccueil) {
            final RubriqueBean rubriqueLangue = rubriquesLangues.get(i);
            isAccueil = codeRubriquePageCourante.equals(rubriqueLangue.getCode());
            i++;
        }
        return isAccueil;
    }

    private static InfosSite getSiteCodeRubrique(final String codeRubriquePageCourante) {
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        InfosSite siteCourant = serviceInfosSite.determineSite(codeRubriquePageCourante, true);
        if (siteCourant == null) {
            siteCourant = serviceInfosSite.getPrincipalSite();
        }
        return siteCourant;
    }

    /**
     * Cette fonction determine si la fiche en court de consultation est la page d'accueil d'une rubrique.
     *
     * @param ficheUniv
     *            la fiche à vérifier
     * @return TRUE s'il sagit d'une page d'accueil dans le cas inverse vous pouvez devinez la valeur qui sera retourné:p.
     */
    public static boolean isFicheAccueilRubrique(final FicheUniv ficheUniv) {
        if (ficheUniv != null && !isAccueilSite(ficheUniv)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            return isFicheAccueilRubrique(ficheUniv, serviceRubrique.getRubriqueByCode(ficheUniv.getCodeRubrique()));
        }
        return Boolean.FALSE;
    }

    /**
     * Cette fonction determine si la fiche en court de consultation est la page d'accueil d'une rubrique.
     *
     * @param ficheUniv
     *            Fiche en court de consultation
     * @param rubrique
     *            La rubrique pour laquelle on veut savoir si la fiche est la page de tête.
     * @return TRUE s'il sagit d'une page d'accueil sinon FALSE.
     */
    public static boolean isFicheAccueilRubrique(final FicheUniv ficheUniv, final RubriqueBean rubrique) {
        Boolean isAccueilRubrique = Boolean.FALSE;
        if (ficheUniv != null && rubrique != null) {
            final BeanPageAccueil beanAccueil = PageAccueilRubriqueManager.getInstance().getBeanPageAccueil(rubrique);
            if (beanAccueil != null && beanAccueil instanceof BeanFichePageAccueil) {
                isAccueilRubrique = ficheUniv.getCode().equals(((BeanFichePageAccueil) beanAccueil).getCode()) && ficheUniv.getLangue().equals(((BeanFichePageAccueil) beanAccueil).getLangue()) && ReferentielObjets.getNomObjet(ficheUniv).equals(((BeanFichePageAccueil) beanAccueil).getObjet());
            }
        }
        return isAccueilRubrique;
    }

    /**
     * Permet de savoir si on est en saisie front ou pas. A l'heure actuelle, on doit vérifier dans les paramètres SAISIE_FRONT & PROC de la requête.
     *
     * @param paramSaisieFront
     *            si il a pour valeur "true" on est en saisie front
     * @param paramProcessus
     *            si le nom du processus fini par _FRONT on est en saisie fron
     * @return vrai ssi SAISIE_FRONT=true et que le nom du processus termine par _FRONT...
     */
    public static boolean isSaisieFront(final String paramSaisieFront, final String paramProcessus) {
        return "true".equalsIgnoreCase(paramSaisieFront) || StringUtils.endsWith(paramProcessus, "_FRONT");
    }

    /**
     * Vérifie si le menu fourni en paramètre vient de la rubrique courante ou non.
     *
     * @param menuAVerifier menu dont on souhaite savoir si il est bien le menu courant
     * @param codeRubriqueCourante le code de la rubrique de la page
     * @return True si la rubrique du menu contient la rubrique de codeRubriqueCourante .
     */
    public static boolean isMenuCourant(final Menu menuAVerifier, final String codeRubriqueCourante) {
        boolean isSousRubrique = Boolean.FALSE;
        if (StringUtils.isNotBlank(menuAVerifier.getCodeRubriqueOrigine()) && StringUtils.isNotBlank(codeRubriqueCourante)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(codeRubriqueCourante);
            isSousRubrique = menuAVerifier.getCodeRubriqueOrigine().equals(codeRubriqueCourante);
            while (!isSousRubrique && rubriqueCourante != null) {
                rubriqueCourante = serviceRubrique.getRubriqueByCode(rubriqueCourante.getCodeRubriqueMere());
                if (rubriqueCourante != null) {
                    isSousRubrique = menuAVerifier.getCodeRubriqueOrigine().equals(rubriqueCourante.getCode());
                }
            }
        }
        return isSousRubrique;
    }

    /**
     * Vérifie si le menu fourni en paramètre contient un enfant actif ou non (si un des enfants est le menu courant).
     *
     * @param menuAVerifier le menu dont on souhaite savoir si il est le menu courant. Ne peut être null
     * @return True si le menu contient un enfant actif.
     */
    public static boolean hasActiveChild(final Menu menuAVerifier) {
        for (final Menu elementSousMenu : menuAVerifier.getSousMenu()) {
            if (elementSousMenu.isMenuCourant()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Récupère à partir d'une ficheUniv sur le champ "contenu" les lignes & paragraphe défini dans ce champ. Si la fiche ne contient pas de valeur, on renvoie une liste vide.
     *
     * @param fiche
     *            la fiche à analyser. Si elle est null on retourne une liste vide
     * @return la liste de {@link LigneContenu} ou rien si rien n'est défini
     */
    public static Collection<LigneContenu> getLignesFicheCourante(final FicheUniv fiche) {
        Collection<LigneContenu> ligneFicheCourante = new ArrayList<>();
        String contenu = StringUtils.EMPTY;
        try {
            if (fiche != null) {
                contenu = (String) PropertyUtils.getProperty(fiche, "contenu");
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            LOG.debug("pas de propriété contenu sur cette fiche", e);
        }
        if (StringUtils.isNotBlank(contenu) && StringUtils.contains(contenu, "[paragraphe")) {
            ligneFicheCourante = ParagrapheContenuHelper.getLignes(contenu);
        }
        return ligneFicheCourante;
    }

    /**
     * Calcule la liste de paragraphe contenu dans la fiche fourni en paramètre.
     *
     * @param fiche la fiche dont on souhaite calculer les paragraphes
     * @return la liste de paragraphe contenu de la fiche ou une liste vide
     */
    public static Collection<ParagrapheBean> getParagrapheDepuisFicheCourante(final FicheUniv fiche) {
        final Collection<ParagrapheBean> paragraphe = new ArrayList<>();
        final Collection<LigneContenu> lignes = getLignesFicheCourante(fiche);
        for (final LigneContenu ligne : lignes) {
            paragraphe.addAll(ligne.getParagraphes());
        }
        return paragraphe;
    }

    /**
     * Méthode là uniquement pour la retro-compat...
     * @param ctx le contexte de la page courante
     * @param ficheUniv pour rien...
     * @param sizeListe le nivveau de profondeur que l'on souhaite atteindre au maximum
     * @return la liste de l'arbo des rubriques de la page courante
     * @deprecated méthode inutile vu qu'on ne passe pas l'attribut ficheUniv. Utiliser {@link FrontUtil#calculerRubriquesPageCourante(ContexteUniv, int)}
     */
    @Deprecated
    public static List<RubriqueBean> calculerRubriquesPageCourante(final ContexteUniv ctx, final FicheUniv ficheUniv, final int sizeListe) {
        return calculerRubriquesPageCourante(ctx, sizeListe);
    }

    /**
     * Recopie de fonctions.jsp méthode valorisant les différentes rubriques utilisées pour gérer la navigation on lui passe en paramètre le contexte et la ficheUniv courantes
     * (pour trouver la rubrique courante), et le nombre de rubriques à récupérer (=la taille de la liste).
     *
     * @param ctx le contexte de la page courante
     * @param sizeListe le nivveau de profondeur que l'on souhaite atteindre au maximum
     * @return la liste de l'arbo des rubriques de la page courante
     */
    public static List<RubriqueBean> calculerRubriquesPageCourante(final ContexteUniv ctx, final int sizeListe) {
        final List<RubriqueBean> listeRubriques = new ArrayList<>();
        final String codeRubriqueCourante = ctx.getCodeRubriquePageCourante();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        RubriqueBean currentSection = serviceRubrique.getRubriqueByCode(codeRubriqueCourante);
        int currentLevel = serviceRubrique.getLevel(currentSection);
        while (currentSection != null && listeRubriques.size() < sizeListe) {
            if (currentLevel < sizeListe) {
                listeRubriques.add(currentSection);
            }
            currentSection = serviceRubrique.getRubriqueByCode(currentSection.getCodeRubriqueMere());
            currentLevel--;
        }
        Collections.reverse(listeRubriques);
        while (listeRubriques.size() < sizeListe) {
            listeRubriques.add(null);
        }
        return listeRubriques;
    }

    public static String getUrlBandeauCourant(final ContexteUniv ctx) {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        String urlBandeau = StringUtils.EMPTY;
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(ctx.getCodeRubriquePageCourante());
        while (rubriqueCourante != null && StringUtils.isNotBlank(rubriqueCourante.getCode())) {
            if (rubriqueCourante.getIdBandeau() != 0l) {
                urlBandeau = MediaUtils.getUrlAbsolue(serviceMedia.getById(rubriqueCourante.getIdBandeau()));
                break;
            }
            rubriqueCourante = serviceRubrique.getRubriqueByCode(rubriqueCourante.getCodeRubriqueMere());
        }
        return urlBandeau;
    }

    /**
     * Recopie de fonctions.jsp. Méthode formatant les fichiers joints d'une fiche sous forme de liste à puces, englobée d'un UL déterminé.
     *
     * @param ctx le contexte de la page courante
     * @param ficheUniv la fiche dont on souhaite connaitre la liste des fichiers joints
     * @return la liste ul / li des fichiers joints...
     */
    public static String calculerListeFichiersJoints(final ContexteUniv ctx, final FicheUniv ficheUniv) {
        String res = calculerContenuListeFichiersJoints(ctx, ficheUniv);
        if (res.length() > 0) {
            res = "<ul id=\"telecharger\">" + res + "</ul>";
        }
        return res;
    }

    /**
     * Recopie de fonctions.jsp. Méthode formatant les fichiers joints d'une fiche sous forme de liste à puces.
     *
     * @param ctx le contexte de la page courante
     * @param ficheUniv la fiche dont on souhaite connaitre la liste des fichiers joints
     * @return la liste ul / li des fichiers joints...
     */
    public static String calculerContenuListeFichiersJoints(final ContexteUniv ctx, final FicheUniv ficheUniv) {
        final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
        final StringBuilder res = new StringBuilder();
        Collection<RessourceBean> fichiers = serviceRessource.getFiles(ficheUniv);
        final String classeFichierDefaut = "defaut";
        String classeFichier;
        for (final RessourceBean fichier : fichiers) {
            if (ClasseFichier.getInstance().containsKey(RessourceUtils.getFormat(fichier))) {
                classeFichier = ClasseFichier.getInstance().get(RessourceUtils.getFormat(fichier));
            } else {
                classeFichier = classeFichierDefaut;
            }
            res.append("<li class=\"").append(classeFichier).append("\">");
            res.append("<a href=\"").append(URLResolver.getRessourceUrl(RessourceUtils.getUrl(fichier), ctx)).append("\">");
            if (StringUtils.isNotBlank(RessourceUtils.getTitre(fichier))) {
                res.append(RessourceUtils.getTitre(fichier));
            } else {
                res.append(RessourceUtils.getSource(fichier));
            }
            res.append("</a> ");
            /* extension du fichier + poids en Ko */
            res.append("<span class='extension-poids-fichiers'> (");
            if (RessourceUtils.isLocal(fichier) && StringUtils.isNotBlank(FileUtil.getExtension(RessourceUtils.getSource(fichier)))) {
                res.append(FileUtil.getExtension(RessourceUtils.getSource(fichier)).toUpperCase());
            } else if (classeFichier.equals(classeFichierDefaut)) {
                res.append(MessageHelper.getCoreMessage("ST_EXTENSION_AUTRE"));
            } else {
                res.append(classeFichier.toUpperCase());
            }
            if (RessourceUtils.getPoids(fichier) > 0) {
                res.append(", ").append(RessourceUtils.getPoids(fichier)).append(" ").append(MessageHelper.getCoreMessage("ST_KILO_OCTETS"));
            }
            res.append(")</span>");
            res.append("</li>");
        }
        return res.toString();
    }

    /**
     * Calcule le titre de la page courante à partir du composant sur lequel on est exple : pagelibre -> Saisie Page libre ...
     *
     * @param infoBean l'infobean courant
     * @return le titre de l'écran si il est défini, le titre du composant sinon
     */
    public static String getTitrePageCourante(final InfoBean infoBean) {
        return AdminsiteUtils.getTitrePageCourante(infoBean);
    }

    /**
     * On affiche le visuel de rubrique uniquement si :
     * - le visuel est renseigné
     * - on est pas sur l'accueil d'un site - on est pas sur l'accueil d'une rubrique de niveau 1.
     *
     * @param ficheUniv
     *            la fiche courante.
     * @param visuelRubrique
     *            le visuel à afficher
     * @return vrai si les trois conditions ci-dessus sont vérifiées.
     */
    public static boolean isAffichageVisuelRubrique(final FicheUniv ficheUniv, final String visuelRubrique) {
        final List<RubriqueBean> rubriquesPageCourante = FrontUtil.calculerRubriquesPageCourante(ContexteUtil.getContexteUniv(), 2);
        final RubriqueBean rubriqueNiveau1 = rubriquesPageCourante.get(1);
        boolean affichageVisuel = ficheUniv != null;
        if (affichageVisuel) {
            affichageVisuel = !FrontUtil.isAccueilSite(ficheUniv) && StringUtils.isNotBlank(visuelRubrique) && (rubriqueNiveau1 != null && ficheUniv.getCode().equals(rubriqueNiveau1.getCode()));
        }
        return affichageVisuel;
    }

    /**
     * Calcule le menu de langue de l'application :
     * <ul>
     * <li>Si la rubrique de site est aussi la rubrique de langue, recherche des rubrique de langue depuis son parent</li>
     * <li>Si la rubrique de site n'est pas la rubrique de langue, recherche à partir de celle-ci les rubriques de langues</li>
     * </ul>.
     *
     * @return Une liste de bean Menu ou une liste vide si non trouvé
     */
    public static List<Menu> getMenuLangueParCategorie() {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueSite = serviceRubrique.getRubriqueByCode(ContexteUtil.getContexteUniv().getInfosSite().getCodeRubrique());
        if (rubriqueSite != null && CATEGORIE_LANGUE.equals(rubriqueSite.getCategorie())) {
            return getMenuParRubriqueParente(serviceRubrique.getRubriqueByCode(rubriqueSite.getCodeRubriqueMere()));
        } else {
            return getMenuParRubriqueParente(rubriqueSite);
        }
    }

    /**
     * Calcule le menu des réseaux sociaux par catégorie de rubrique.
     *
     * @return Une liste de bean Menu ou une liste vide si non trouvé
     */
    public static List<Menu> getMenuReseauxSociauxParCategorie() {
        return FrontUtil.getMenuParRubriqueParente(FrontUtil.getRubriqueDepuisCategorie(CATEGORIE_RESEAUX_SOCIAUX));
    }

    /**
     * Calcule le menu principal de l'application.
     *
     * @return Une liste de bean Menu ou une liste vide si non trouvé
     */
    public static List<Menu> getMenuPrincipalParCategorie() {
        return getMenu(getRubriqueDepuisCategorie(CATEGORIE_NAVIGATION), 2);
    }

    /**
     * Calcule le menu du pied de page de l'application.
     *
     * @return Une liste de bean Menu ou une liste vide si non trouvé
     */
    public static List<Menu> getMenuPiedDePageParCategorie() {
        return getMenu(getRubriqueDepuisCategorie(CATEGORIE_PIED_PAGE), 2);
    }

    /**
     * Calcule le menu d'accès direct par catégorie de rubrique.
     *
     * @return Une liste de bean Menu ou une liste vide si non trouvé
     */
    public static List<Menu> getMenuAccesDirectParCategorie() {
        return getMenuParRubriqueParente(getRubriqueDepuisCategorie(CATEGORIE_ACCES_DIRECT));
    }

    public static List<Menu> getMenuPlanSite() {
        return getMenu(getRubriqueDepuisCategorie(CATEGORIE_NAVIGATION), 2);
    }

    /**
     * calcule le menu secondaire de la page courante.
     * @return la liste de menu qui
     */
    public static List<Menu> getMenuSecondairePageCourante() {
        return getMenuSecondaireDepuisMenuPrincipal(getMenuPrincipalParCategorie());
    }

    /**
     * calcule le menu secondaire de la page courante.
     * @return la liste de menu qui
     */
    public static List<Menu> getMenuSecondaireDepuisMenuPrincipal(Collection<Menu> menusPrincipal) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final String codeRubriquePageCourante = ctx.getCodeRubriquePageCourante();
        final RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(codeRubriquePageCourante);
        RubriqueBean rubriqueParente = null;
        if (rubriqueCourante != null) {
            for (final Menu itemMenuPrincipal : menusPrincipal) {
                final RubriqueBean rubriqueMenuPrincipal = serviceRubrique.getRubriqueByCode(itemMenuPrincipal.getCodeRubriqueOrigine());
                if (rubriqueMenuPrincipal != null && (rubriqueMenuPrincipal.getCode().equals(rubriqueCourante.getCode()) || serviceRubrique.isParentSection(rubriqueMenuPrincipal,
                    rubriqueCourante))) {
                    rubriqueParente = rubriqueMenuPrincipal;
                    break;
                }
            }
        }
        final List<Menu> menuSecondaire = new ArrayList<>();
        if (rubriqueParente != null) {
            menuSecondaire.addAll(getMenu(serviceRubrique.getRubriqueByCode(rubriqueParente.getCode()), NIVEAU_MENU_SECONDAIRE));
        }
        return menuSecondaire;
    }

    /**
     * Calcule un objet menu et ses enfants jusqu'à une certaine profondeur.
     *
     * @param rubriqueParente
     *            Rubrique d'origine
     * @return Objet Menu contenant tous les menus de premier niveau
     */
    public static List<Menu> getMenuParRubriqueParente(final RubriqueBean rubriqueParente) {
        final List<Menu> menusParRubriqueParente = new ArrayList<>();
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        if (rubriqueParente != null && StringUtils.isNotBlank(rubriqueParente.getCode())) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            for (final RubriqueBean rubrique : serviceRubrique.getRubriqueByCodeParentFront(rubriqueParente.getCode(), ctx.getGroupesDsiAvecAscendants())) {
                menusParRubriqueParente.add(getMenuParRubrique(rubrique));
            }
        }
        return menusParRubriqueParente;
    }

    /**
     * Calcule un menu à partir de la rubrique parente fourni en paramètre & le niveau de profondeur choisi.
     *
     * @param rubriqueParente
     *            la rubrique à partir de laquelle on calcule le menu
     * @param profondeur
     *            le niveau de profondeur des rubriques a aller chercher
     * @return une collection de {@link Menu} correspondant au sous rubrique de la rubrique fourni en paramètre ou une liste vide si non trouvé
     */
    public static List<Menu> getMenu(final RubriqueBean rubriqueParente, final int profondeur) {
        final List<Menu> menuCourant = new ArrayList<>();
        if (rubriqueParente != null && StringUtils.isNotBlank(rubriqueParente.getCode())) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            for (final RubriqueBean rubrique : serviceRubrique.getRubriqueByCodeParentFront(rubriqueParente.getCode(), ContexteUtil.getContexteUniv().getGroupesDsiAvecAscendants())) {
                final Menu itemMenuCourant = getMenuParRubrique(rubrique);
                if (profondeur > 0) {
                    itemMenuCourant.addAllSousMenu(getMenu(rubrique, profondeur - 1));
                }
                menuCourant.add(itemMenuCourant);
            }
        }
        return menuCourant;
    }

    /**
     * Calcul un objet menu en indiquant son titre, son url, …
     *
     * @param rubrique
     *            Rubrique de référence
     * @return Objet Menu valorisé en fonction de la rubrique.
     */
    public static Menu getMenuParRubrique(final RubriqueBean rubrique) {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        final Menu menuRubriqueCourante = new Menu();
        if (rubrique != null && StringUtils.isNotBlank(rubrique.getCode())) {
            menuRubriqueCourante.setCode(rubrique.getCode());
            menuRubriqueCourante.setCodeRubriqueOrigine(rubrique.getCode());
            menuRubriqueCourante.setLibelle(rubrique.getIntitule());
            menuRubriqueCourante.setLangue(rubrique.getLangue());
            menuRubriqueCourante.setType(rubrique.getTypeRubrique());
            menuRubriqueCourante.setVisuel(MediaUtils.getUrlAbsolue(serviceMedia.getById(rubrique.getIdBandeau())));
            menuRubriqueCourante.setPicto(MediaUtils.getUrlAbsolue(serviceMedia.getById(rubrique.getIdPicto())));
            menuRubriqueCourante.setAccroche(rubrique.getAccroche());
            try {
                final ContexteUniv ctx = ContexteUtil.getContexteUniv();
                menuRubriqueCourante.setUrl(UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, rubrique.getCode()));
                menuRubriqueCourante.setMenuCourant(isMenuCourant(menuRubriqueCourante, ctx.getCodeRubriquePageCourante()));
            } catch (final Exception e) {
                LOG.debug("no home page for section", e);
                LOG.error("Pas de page d'accueil pour la rubrique « " + rubrique.getCode() + " ».", e);
                menuRubriqueCourante.setUrl(StringUtils.EMPTY);
            }
        }
        return menuRubriqueCourante;
    }

    /**
     * Renvoie la rubrique correspondant à la catégorie en fonction du site du contexte courant. Renvoie null si aucune rubrique de cette catégorie n'est trouvée.
     *
     * @param categorie la catégorie à rechercher
     * @return la première rubrique de la catégorie ou null si non trouvé.
     */
    public static RubriqueBean getRubriqueDepuisCategorie(final String categorie) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        String codeRubriqueSiteLangue = ctx.getInfosSite().getCodeRubrique();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(codeRubriqueSiteLangue);
        if (rubrique != null && !CATEGORIE_LANGUE.equals(rubrique.getCategorie())) {
            final String langueCourante = ctx.getLangue();
            codeRubriqueSiteLangue = getCodeRubriqueLangueParCodeLangue(codeRubriqueSiteLangue, langueCourante);
        }
        return serviceRubrique.getFirstOfCategoryInSubTree(codeRubriqueSiteLangue, categorie);
    }

    /**
     * Récupère le code de rubrique de catégorie {@link FrontUtil#CATEGORIE_LANGUE} ayant pour langue la langue fourni en paramètre.
     *
     * @param codeRubriqueSiteLangue
     *            le code de rubrique de site ou de langue
     * @param langueCourante
     *            la langue de la rubrique que l'on souhaite récupérer
     * @return le code rubrique de la rubrique de langue ou {@link StringUtils#EMPTY} si non trouvé
     */
    public static String getCodeRubriqueLangueParCodeLangue(final String codeRubriqueSiteLangue, final String langueCourante) {
        final List<RubriqueBean> rubriques = getListeSousRubriquesDepuisCategorie(CATEGORIE_LANGUE, codeRubriqueSiteLangue);
        String codeRubrique = StringUtils.EMPTY;
        for (final RubriqueBean rubrique : rubriques) {
            if (langueCourante.equals(rubrique.getLangue())) {
                codeRubrique = rubrique.getCode();
            }
        }
        return codeRubrique;
    }

    /**
     * Récupère les {@link RubriqueBean} de la catégorie et de la rubrique parente fourni en paramètre.
     *
     * @param categorie
     *            la catégorie de rubrique à rechercher
     * @param codeRubriqueParente
     *            la rubrique à partir de laquelle on recherche les {@link RubriqueBean} à retourner
     * @return La liste des {@link RubriqueBean} correspondant à la catégorie ou une liste vide sinon.
     */
    public static List<RubriqueBean> getListeSousRubriquesDepuisCategorie(final String categorie, final String codeRubriqueParente) {
        final List<RubriqueBean> rubriques = new ArrayList<>();
        if (StringUtils.isNotBlank(codeRubriqueParente)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final List<RubriqueBean> childSections = serviceRubrique.getRubriqueByCodeParent(codeRubriqueParente);
            for (RubriqueBean currentChild : childSections) {
                if (categorie.equals(currentChild.getCategorie())) {
                    rubriques.add(currentChild);
                } else {
                    rubriques.addAll(getListeSousRubriquesDepuisCategorie(categorie, currentChild.getCode()));
                }
            }
        }
        return rubriques;
    }

    /**
     * Renvoie la rubrique en fonction de la catégorie et du code site. Renvoie null si aucune sous-rubrique de cette catégorie n'est trouvée.
     *
     * @param categorie la catégorie de la rubrique que l'on souhaite récupérer
     * @param codeRubriqueSiteLangue le code de la rubrique mère
     * @return Le {@link RubriqueBean} correspondant ou null si non trouvé.
     */
    public static RubriqueBean getRubriqueDepuisCategorieEtRubriqueDeSite(final String categorie, final String codeRubriqueSiteLangue) {
        RubriqueBean resultat = null;
        if (StringUtils.isNotEmpty(categorie)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            for (final RubriqueBean rubrique : serviceRubrique.getRubriqueByCodeParent(codeRubriqueSiteLangue)) {
                if (categorie.equals(rubrique.getCategorie())) {
                    resultat = rubrique;
                    break;
                }
            }
        }
        return resultat;
    }

    /**
     * Calcule le logo du site courant.
     * @return l'url du logo ou null si non trouvé
     */
    public static String getLogoUrl() {
        try {
            return InfosSiteHelper.getURLRelativeFichierPropertyTemplate(ContexteUtil.getContexteUniv().getInfosSite(), NOM_PROPRIETE_LOGO);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("unable to find any file", e);
            return StringUtils.EMPTY;
        }
    }

    /**
     * Renvoie la baseline défini sur la déclaration du site.
     * @return la baseline ou null si non trouvé.
     */
    public static String getBaseline() {
        return ContexteUtil.getContexteUniv().getInfosSite().getProprieteComplementaireString(NOM_PROPRIETE_BASELINE);
    }

    /**
     * Calcule la couleur principale spécifiée sur le site courant.
     * @return la couleur ou null si non trovué
     */
    public static String getCouleurPrincipale() {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final InfosSite siteCourant = ctx.getInfosSite();
        return siteCourant.getProprieteComplementaireString(NOM_PROPRIETE_COULEUR_PRINCIPALE);
    }

    /**
     * Calcule la couleur secondaire spécifiée sur le site courant.
     * @return la couleur ou null si non trovué
     */
    public static String getCouleurSecondaire() {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final InfosSite siteCourant = ctx.getInfosSite();
        return siteCourant.getProprieteComplementaireString(NOM_PROPRIETE_COULEUR_SECONDAIRE);
    }

    /**
     * Retourne une valeur rgba() à partir d'un code hexadécimal de couleur et d'un taux d'opacité.
     *
     * @param colorToTransform
     *            Le code de la couleur à rendre translucide. Il doit être de la forme suivante : ^#?[0-9a-fA-F]{6}$
     * @param alpha
     *            Le taux d'opacité, chaîne de caractères dont la valeur décimale doit être comprise entre 0 et 1 inclus.
     * @return Retourne une chaîne formatée rgba() pour le CSS. Si le code couleur n'est pas saisi dans le bon format, une chaîne vide est retournée
     * @deprecated utiliser {@link ColorUtils#getRGBaFromHexa(String, double)}
     */
    @Deprecated
    public static String getRGBaFromHexa(final String colorToTransform, final double alpha) {
        return ColorUtils.getRGBaFromHexa(colorToTransform, alpha);
    }

    /**
     * Permet de vérifier si la valeur fourni en paramètre correspond à une valeur Hexa valide pour l'html.
     * @param colorValue la chaine à tester pour vérifier que c'est bien une couleur
     * @return vrai si la chaine est de la forme 090909 #090909
     * @deprecated utiliser {@link ColorUtils#isValidHtmlHexaValue(String)}
     */
    @Deprecated
    public static boolean isValidHtmlHexaValue(String colorValue) {
        return StringUtils.isNotBlank(colorValue) && colorValue.matches("^(?:#|)[0-9a-fA-F]{6}$");
    }

    /**
     * Retourne une couleur plus clair du taux de clarté passé en paramêtre.
     *
     * @param colorToTransform
     *            la valeur heaxdécimale de la couleur
     * @param lumi
     *            la clarté à apporter (%)
     * @return la chaine css hsl
     * @deprecated utiliser {@link ColorUtils#getHslFromHexa(String, double)}
     */
    @Deprecated
    public static String getHslFromHexa(final String colorToTransform, final double lumi) {
        return ColorUtils.getHslFromHexa(colorToTransform, lumi);
    }

    /**
     * Permet de calculer si au moins une des sequences fourni en paramètre est non vide.
     *
     * @param sequences les chaines de caractères à vérifier
     * @return vrai ssi une des chaines fourni en paramètre n'est pas vide ou rempli d'espace.
     */
    public static boolean isAnyNotBlank(final CharSequence... sequences) {
        boolean isAnyNotBlank = Boolean.FALSE;
        if (ArrayUtils.isNotEmpty(sequences)) {
            int i = 0;
            while (!isAnyNotBlank && i < sequences.length) {
                isAnyNotBlank = StringUtils.isNotBlank(sequences[i]);
                i++;
            }
        }
        return isAnyNotBlank;
    }
}
