package com.kportal.ihm.utils.sharing;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import com.univ.objetspartages.om.FicheUniv;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;

public class DefaultSocialNetworkUrl implements SocialNetworkUrl {

    private String socialNetworkUrl;

    private String socialNetworkName;

    public String getSocialNetworkUrl() {
        return socialNetworkUrl;
    }

    public void setSocialNetworkUrl(final String socialNetworkName) {
        this.socialNetworkUrl = socialNetworkName;
    }

    public String getSocialNetworkName() {
        return socialNetworkName;
    }

    public void setSocialNetworkName(final String socialNetworkName) {
        this.socialNetworkName = socialNetworkName;
    }

    @Override
    public String getSharingUrl(final FicheUniv fiche, final ContexteUniv ctx) {
        String urlPartage = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(socialNetworkUrl)) {
            final String urlFiche = URLResolver.getAbsoluteUrlFiche(fiche, ctx);
            urlPartage = MessageFormat.format(socialNetworkUrl, urlFiche);
        }
        return urlPartage;
    }
}
