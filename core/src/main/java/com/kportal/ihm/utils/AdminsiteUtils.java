package com.kportal.ihm.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.ProcessusHelper;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.ModuleHelper;
import com.kportal.extension.module.composant.Composant;
import com.kportal.extension.module.composant.IComposant;
import com.kportal.extension.module.composant.Menu;
import com.kportal.ihm.service.ServiceMenuBack;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheObjet;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.RechercheFicheHelper;
import com.univ.utils.URLResolver;
import com.univ.utils.json.CodecJSon;

/**
 *Classe utilitaire permettant de virer du code java dans les JSP.
 * @author olivier.camon
 *
 */
public final class AdminsiteUtils {

    private static final Logger LOG = LoggerFactory.getLogger(AdminsiteUtils.class);

    private static final String ETAT_OBJET = "ETAT_OBJET";

    private static final String AFFICHER_POPUP_RESTAURATION = "AFFICHER_POPUP_RESTAURATION";

    private static final String GRS_AUTORISATION_SUPPRESSION = "GRS_AUTORISATION_SUPPRESSION";

    private AdminsiteUtils(){}

    /**
     * Pour la page courante, on récupère son composant et on retourne son menu d'action. Pour une fiche : créer / rechercher ...
     *
     * @param infoBean
     *            permet de récupérer le composant courant
     * @param infosSession
     *            permet de récuperer les autorisations de l'utilisateur
     * @return un bean Menu contenant les actions dispo pour l'utilisateur du composant courant
     */
    public static Menu getMenuActionsCourante(final InfoBean infoBean, final Map<String, Object> infosSession) {
        final AutorisationBean autorisation = (AutorisationBean) infosSession.get(SessionUtilisateur.AUTORISATIONS);
        final Composant composantCourant = retrouverComposant(infoBean);
        return composantCourant.getMenuGeneral(autorisation, infoBean);
    }

    /**
     * Calcule l'action courante de l'utilisateur pour lui fournir la jsp correspondante (liste d'actu, saisie de rubrique...).
     *
     * @return le chemin et le nom de la jsp à inclure
     */
    public static String getJspBackOffice(final InfoBean infoBean) {
        final String fichierEcranPhysique = infoBean.getEcranPhysique();
        if (StringUtils.isEmpty(fichierEcranPhysique)) {
            LOG.error("pas d'ecran physique!");
        }
        return "/" + fichierEcranPhysique + ".jsp";
    }

    /**
     * Calcule le menu principal de l'utilisateur courant en fonction de ses droits et retourne un {@link Menu} à afficher en FO.
     *
     * @return
     */
    public static Menu getMenuPrincipalParUtilisateur(final Map<String, Object> infosSession) {
        final AutorisationBean autorisation = (AutorisationBean) infosSession.get(SessionUtilisateur.AUTORISATIONS);
        return ServiceMenuBack.getMenuBackOfficeParUtilisateur(autorisation);
    }

    /**
     * Calcule le titre de la page courante à partir du composant sur lequel on est exple : pagelibre -> Saisie Page libre ...
     *
     * @param infoBean
     * @return le titre de l'écran si il est défini, le titre du composant sinon
     */
    public static String getTitrePageCourante(final InfoBean infoBean) {
        String libelle = StringUtils.EMPTY;
        final String titreEcran = infoBean.getTitreEcran();
        if (StringUtils.isNotBlank(titreEcran)) {
            libelle = titreEcran;
        } else {
            libelle = calculerLibelleDepuisLocale(infoBean);
        }
        return libelle;
    }

    /**
     * Récupère le libellé de la page courante pour l'utilisateur.
     *
     * @param infoBean
     *            permet de savoir sur quel page est l'utilisateur
     * @return le libellé en fonction de la locale du BO
     */
    private static String calculerLibelleDepuisLocale(final InfoBean infoBean) {
        final StringBuilder codeLibelle = new StringBuilder();
        String libelle = "";
        String key = "";
        if (StringUtils.isNotEmpty(infoBean.getNomProcessus())) {
            key = infoBean.getNomProcessus();
        } else {
            key = retrouverComposant(infoBean).id;
        }
        if (StringUtils.isNotEmpty(infoBean.getEcranLogique())) {
            codeLibelle.append(".").append(infoBean.getEcranLogique());
        }
        if (StringUtils.isNotEmpty(infoBean.getActionUtilisateur())) {
            codeLibelle.append(".").append(infoBean.getActionUtilisateur());
        }
        codeLibelle.insert(0, key);
        if (StringUtils.isNotBlank(codeLibelle.toString())) {
            libelle = MessageHelper.getMessage(infoBean.getNomExtension(), codeLibelle.toString());
            if (StringUtils.isBlank(libelle)) {
                codeLibelle.replace(0, key.length(), "DEFAUT");
                libelle = MessageHelper.getMessage(infoBean.getNomExtension(), codeLibelle.toString());
            }
        }
        LOG.debug(codeLibelle + " " + libelle);
        return libelle;
    }

    /**
     * Permet de connaitre dans quel menu l'utilisateur est.
     *
     * @param infoBean
     * @return
     */
    public static Menu getMenuDepuisInfoBean(final InfoBean infoBean) {
        final Composant composant = retrouverComposant(infoBean);
        return ServiceMenuBack.construitMenuDepuisComposant(composant);
    }

    /**
     * Retrouve le composant à partir des données contenus dans l'infoBean.
     *
     * @param infoBean
     * @return
     */
    public static Composant retrouverComposant(final InfoBean infoBean) {
        final String idComposant = infoBean.getIdComposant();
        final String extension = infoBean.getNomExtension();
        Composant composantCourant = null;
        if (StringUtils.isNotBlank(idComposant)) {
            if (StringUtils.isNotEmpty(extension)) {
                composantCourant = (Composant) ModuleHelper.getModule(extension, idComposant);
            } else {
                composantCourant = (Composant) ModuleHelper.getModule(ApplicationContextManager.DEFAULT_CORE_CONTEXT, idComposant);
            }
        }
        if (composantCourant == null) {
            composantCourant = (Composant) ModuleHelper.getModule(ApplicationContextManager.DEFAULT_CORE_CONTEXT, "menuAccueil");
        }
        return composantCourant;
    }

    /**
     * Pour un type de fiche et les autorisations données, est il possible de créer une fiche.
     *
     * @param codeObjet
     * @param autorisation
     * @return
     */
    public static boolean isCreationFichePossible(final String codeObjet, final AutorisationBean autorisation) {
        boolean isCreation = Boolean.FALSE;
        if (StringUtils.isNotBlank(codeObjet)) {
            final FicheUniv fiche = ReferentielObjets.instancierFiche(codeObjet);
            isCreation = autorisation.getAutorisation(codeObjet, AutorisationBean.INDICE_CREATION) && !(fiche instanceof FicheObjet);
        }
        return isCreation;
    }

    /**
     * Vieille méthode pour savoir si on est en mode popup... Il n'y a pas trop d'autre possibilité à l'heure actuelle.
     *
     * @param infoBean
     * @return
     */
    public static boolean isNotEcranPopup(final InfoBean infoBean) {
        return StringUtils.isBlank(infoBean.getString("FCK_PLUGIN")) && StringUtils.isBlank(infoBean.getString("TOOLBOX"));
    }

    /**
     * Calcule si menu courant doit contenir une classe CSS ou non.
     *
     * @param composantCourant
     * @param menuCourant
     * @return
     */
    public static String getClassMenuBackOffice(final Composant composantCourant, final Menu menuCourant) {
        String classeComposant = StringUtils.EMPTY;
        if (menuCourant.getIdMenu().equals(composantCourant.getIdMenuBoParent()) || menuCourant.getIdMenu().equals(composantCourant.getId())) {
            classeComposant = "focus ";
        }
        if (CollectionUtils.isNotEmpty(menuCourant.getSousMenu())) {
            classeComposant += "sous_menu";
        }
        return classeComposant;
    }

    /**
     * CQCB?
     *
     * @return
     * @deprecated Cette méthode ne devrait pas être dans cette classe, elle n'est plus utilisée sur le produit & ne fait pas ce que son nom indique car elle calcule pour tout les
     *             objets du back le lien d'ajout sans savoir si il est éditable ou non. De plus elle retourne une hashtable...
     */
    @Deprecated
    public static Hashtable<String, String> getListeObjetsEditables() {
        final Hashtable<String, String> resultat = new Hashtable<>();
        for (final Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
            final String url = WebAppUtil.SG_PATH + "?EXT=" + objet.getIdExtension() + "&PROC=" + objet.getParametreProcessus() + "&ACTION=AJOUTER";
            resultat.put(url, objet.getLibelleObjet());
        }
        return resultat;
    }

    /**
     * Calcule l'ensemble des liens de déconnexion de l'application (si +ieurs sites...) et les retournes dans une chaine de caractère encodé en JSON.
     *
     * @return
     */
    public static String getLiensPropagationDeconnexionFormatter() {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        String urlEnJSON = StringUtils.EMPTY;
        try {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final Collection<String> urlsDuSite = new ArrayList<>();
            for (final InfosSite infosSite : serviceInfosSite.getSitesList().values()) {
                if (!infosSite.getAlias().equals(ctx.getInfosSite().getAlias()) && infosSite.isSso()) {
                    urlsDuSite.add(URLResolver.getAbsoluteBoUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION_FRONT&ACTION=DECONNECTER&REDIRECT=FALSE", infosSite));
                }
            }
            urlEnJSON = CodecJSon.encodeObjectToJSonInString(urlsDuSite);
        } catch (final IOException e) {
            LOG.error("impossible de charger la liste des sites", e);
        }
        return StringEscapeUtils.escapeHtml4(urlEnJSON);
    }

    /**
     * permet de récupérer les critères de recherche à afficher dans la page de résultat de recherche.
     *
     * @param infoBean
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Collection<CritereRecherche> getCriteresRechercheAAfficher(final InfoBean infoBean) {
        final Object objetCritere = infoBean.get(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES);
        if (objetCritere != null && objetCritere instanceof List<?>) {
            final List<CritereRecherche> criteres = (List<CritereRecherche>) objetCritere;
            return CollectionUtils.select(criteres, new Predicate() {

                @Override
                public boolean evaluate(final Object element) {
                    return ((CritereRecherche) element).isAffichageFront();
                }
            });
        }
        return Collections.emptyList();
    }

    /**
     * Calcule l'url d'un recherche avancée d'un datagrid avec la prévalorisation des critères de recherche.
     *
     * @param module
     * @param infoBean
     * @return
     */
    public static String getUrlRechercheAvanceeDatagrid(final IComposant module, final InfoBean infoBean) {
        final Object objetCritere = infoBean.get(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES);
        String[][] params = new String[][] {};
        if (objetCritere != null && objetCritere instanceof List<?>) {
            @SuppressWarnings("unchecked")
            final List<CritereRecherche> criteres = (List<CritereRecherche>) objetCritere;
            if (!criteres.isEmpty()) {
                params = new String[criteres.size()][2];
                int i = 0;
                for (final CritereRecherche parametre : criteres) {
                    params[i] = new String[] {parametre.getNomChamp(), parametre.getValeurARechercher()};
                    i++;
                }
            }
        }
        return ProcessusHelper.getUrlProcessAction(infoBean, module.getIdExtension(), module.getParametreProcessus(), "RECHERCHER", params);
    }

    /**
     * Est ce qu'il existe une autre langue pour la fiche courante?
     *
     * @param infoBean
     * @return
     */
    public static boolean existeAutreLangue(final InfoBean infoBean) {
        final Map<String, String> listeAutreLangue = infoBean.getMap("LISTE_AUTRES_LANGUES");
        return infoBean.get("FICHES_AUTRE_LANGUE") != null || (!"0006".equals(infoBean.get(ETAT_OBJET)) && "1".equals(infoBean.getString("GRS_AUTORISATION_TRADUCTION")) && MapUtils.isNotEmpty(listeAutreLangue));
    }

    /**
     * Calcule si l'action de suppression est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut supprimer la fiche
     */
    public static boolean isSuppressionPossible(final InfoBean infoBean) {
        return InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && ("1".equals(infoBean.getString(GRS_AUTORISATION_SUPPRESSION)) || "0006".equals(infoBean.get(ETAT_OBJET))) && !"0004".equals(infoBean.get(ETAT_OBJET));
    }

    /**
     * Calcule si l'action d'archivage est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut archiver la fiche
     */
    public static boolean isArchivagePossible(final InfoBean infoBean) {
        return InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && "1".equals(infoBean.getString(GRS_AUTORISATION_SUPPRESSION)) && !EtatFiche.A_VALIDER.getEtat().equals(infoBean.get(ETAT_OBJET)) && !"0004".equals(infoBean.get(ETAT_OBJET)) && !"0006".equals(infoBean.get(ETAT_OBJET)) && !"0007".equals(infoBean.get(ETAT_OBJET));
    }

    /**
     * Calcule si l'action de duplication est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut dupliquer la fiche
     */
    public static boolean isDuplicationPossible(final InfoBean infoBean) {
        return InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && "1".equals(infoBean.getString("GRS_AUTORISATION_DUPLICATION")) && !EtatFiche.A_VALIDER.getEtat().equals(infoBean.get(ETAT_OBJET)) && !"0006".equals(infoBean.get(ETAT_OBJET));
    }

    /**
     * Calcule si l'action de publication est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut publier la fiche
     */
    public static boolean isPublicationPossible(final InfoBean infoBean) {
        return EtatFiche.isEtatEnregistrable(infoBean.getString(ETAT_OBJET)) && (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet()) || EtatFiche.BROUILLON.getEtat().equals(infoBean.get(ETAT_OBJET)));
    }

    /**
     * Calcule si l'action d'enregistrer en brouillon est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut enregistrer en brouillon la fiche
     */
    public static boolean isBrouillonPossible(final InfoBean infoBean) {
        return isEnregistrerPossible(infoBean) && !EtatFiche.ARCHIVE.getEtat().equals(infoBean.get(ETAT_OBJET));
    }

    /**
     * Calcule si l'action d'enregistrer est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut enregistrer la fiche
     */
    public static boolean isEnregistrerPossible(final InfoBean infoBean) {
        return EtatFiche.isEtatEnregistrable(infoBean.getString(ETAT_OBJET)) && !isPublicationPossible(infoBean);
    }

    /**
     * Calcule si l'action d'annuler une suppression est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut annuler une suppression la fiche
     */
    public static boolean isAnnulerSuppressionPossible(final InfoBean infoBean) {
        return InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && "1".equals(infoBean.getString(GRS_AUTORISATION_SUPPRESSION)) && "0004".equals(infoBean.get(ETAT_OBJET)) && infoBean.get(AFFICHER_POPUP_RESTAURATION) != null;
    }

    /**
     * Calcule si l'action d'annuler une suppression en brouillon est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut annuler une suppression en brouillon la fiche
     */
    public static boolean isAnnulerSuppressionBrouillonPossible(final InfoBean infoBean) {
        return InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && "1".equals(infoBean.getString(GRS_AUTORISATION_SUPPRESSION)) && "0004".equals(infoBean.get(ETAT_OBJET)) && infoBean.get(AFFICHER_POPUP_RESTAURATION) == null;
    }

    /**
     * Calcule si l'action de restaurer une sauvegarde est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut restaurer une sauvegarde la fiche
     */
    public static boolean isRestaurerSauvegardePossible(final InfoBean infoBean) {
        return InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && "0006".equals(infoBean.get(ETAT_OBJET)) && infoBean.get(AFFICHER_POPUP_RESTAURATION) != null;
    }

    /**
     * Calcule si l'action de restaurer une sauvegarde en brouillon est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut restaurer une sauvegarde en brouillon la fiche
     */
    public static boolean isRestaurerSauvegardeBrouillonPossible(final InfoBean infoBean) {
        return InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && "0006".equals(infoBean.get(ETAT_OBJET)) && infoBean.get(AFFICHER_POPUP_RESTAURATION) == null;
    }

    /**
     * Calcule si l'action de restauration est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut restaurer la fiche
     */
    public static boolean isRestaurerArchivePossible(final InfoBean infoBean) {
        return InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && "1".equals(infoBean.getString(GRS_AUTORISATION_SUPPRESSION)) && "0007".equals(infoBean.get(ETAT_OBJET)) && infoBean.get(AFFICHER_POPUP_RESTAURATION) != null;
    }

    /**
     * Calcule si l'action de restaurer une archive est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut restaurer en brouillon la fiche
     */
    public static boolean isRestaurerArchiveBrouillonPossible(final InfoBean infoBean) {
        return InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && "1".equals(infoBean.getString(GRS_AUTORISATION_SUPPRESSION)) && "0007".equals(infoBean.get(ETAT_OBJET)) && infoBean.get(AFFICHER_POPUP_RESTAURATION) == null;
    }

    /**
     * Calcule si l'action de validation est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut valider la fiche
     */
    public static boolean isValidationPossible(final InfoBean infoBean) {
        return EtatFiche.A_VALIDER.getEtat().equals(infoBean.get(ETAT_OBJET)) && "1".equals(infoBean.getString("GRS_AUTORISATION_VALIDATION"));
    }

    /**
     * Calcule si l'action d'enregistrer en brouillon est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut annuler une demande de validation la fiche
     */
    public static boolean isAnnulerDemandeValidationPossible(final InfoBean infoBean) {
        return EtatFiche.A_VALIDER.getEtat().equals(infoBean.get(ETAT_OBJET)) && !"1".equals(infoBean.getString("GRS_AUTORISATION_VALIDATION"));
    }

    /**
     * Calcule si l'action d'aperçu est possible en fonction des données présente dans l'infobean.
     *
     * @param infoBean
     * @return est ce que 'lutilisateur peut faire un aperçu de la fiche
     */
    public static boolean isApercuPossible(final InfoBean infoBean) {
        return "1".equals(infoBean.getString("GRS_APERCU"));
    }
}
