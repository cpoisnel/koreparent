package com.kportal.ihm.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.ObjetPartageHelper;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.frontoffice.util.JSPIncludeHelper;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.SousParagrapheBean;
import com.univ.objetspartages.services.ServiceEncadre;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.UnivWebFmt;

/**
 * Classe utilitaire calculant les encadrés de la page courante
 */
public class EncadresFrontUtils {

    public static final String GENERIQUE = "encadre_generique";

    public static final String NAV_AUTO = "encadre_nav_auto";

    public static final String RUBRIQUE = "encadre_rubrique";

    public static final String FICHE = "encadre_fiche";

    public static final String AUTO_FICHE = "encadre_auto_fiche";

    public static final String RECHERCHE_EXTERNE = "encadre_recherche_externe";

    public static final String EXTERNE = "encadre_externe";

    private static final Logger LOG = LoggerFactory.getLogger(EncadresFrontUtils.class);

    /**
     * Retourne l'ensemble des encadrés de la page courante
     *
     * @return l'ensemble des encadrés classé par type.
     */
    public static Map<String, List<SousParagrapheBean>> getEncadresParType() {
        final Map<String, List<SousParagrapheBean>> encadresPageCourante = new HashMap<>();
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final FicheUniv fiche = ctx.getFicheCourante();
        encadresPageCourante.put(GENERIQUE, getEncadresGeneriques(ctx, fiche));
        encadresPageCourante.put(NAV_AUTO, getEncadreContenuRubrique(ctx));
        encadresPageCourante.put(RUBRIQUE, getEncadresRubrique(ctx.getCodeRubriquePageCourante()));
        if (fiche != null) {
            encadresPageCourante.put(FICHE, getContenuEncadreFiche(fiche));
        } else {
            encadresPageCourante.put(RECHERCHE_EXTERNE, getEncadresRechercheExterne(ctx));
        }
        encadresPageCourante.put(EXTERNE, getEncadresExterne(ctx));
        return encadresPageCourante;
    }

    /**
     * Calcule les encadrés automatique de la fiche courante.
     *
     * @param ctx
     *            le contexte pour appeler une JSP
     * @param fiche
     *            la fiche pour récupérer les encadrés automatiques de cette fiche
     * @return le code html de l'encadré
     */
    public static String getEncadresAutoFiche(final ContexteUniv ctx, final FicheUniv fiche) {
        String resultat = StringUtils.EMPTY;
        if (fiche != null) {
            final String nomObjet = ReferentielObjets.getNomObjet(fiche);
            final String path = ObjetPartageHelper.getTemplateObjet(ObjetPartageHelper.TEMPLATE_ENCADRE_AUTOFICHE, nomObjet);
            try {
                resultat = JSPIncludeHelper.getOutputJsp(ctx.getServletContext(), ctx.getRequeteHTTP(), ctx.getReponseHTTP(), path);
            } catch (ServletException | IOException e) {
                LOG.error("impossible d'appeler la jsp des encadrés automatiques de fiches pour l'objet " + nomObjet, e);
            }
        }
        return resultat;
    }

    /**
     * Calcule les encadrés de la fiche courante.
     *
     * @param fiche
     *            la fiche pour laquelle on doit récupérer les encadrés
     * @return l'ensemble des encadrés renseigné sur la fiche != null
     */
    public static List<SousParagrapheBean> getContenuEncadreFiche(final FicheUniv fiche) {
        final List<SousParagrapheBean> encadres = new ArrayList<>();
        final String contenuEncadreFiche = fiche.getContenuEncadre();
        if (StringUtils.isNotBlank(contenuEncadreFiche)) {
            encadres.addAll(SousParagrapheBean.getSousParagraphes(contenuEncadreFiche));
        }
        return encadres;
    }

    /**
     * Calcule les encadrés automatiques de la page courante
     *
     * @param ctx
     *            pour récupérer le code de rubrique de la page courante
     * @return la liste des encadrés renseigné sur la rubrique. != null
     * @deprecated Fonctionnalité plus utilisée, méthode à supprimer
     */
    @Deprecated
    public static List<SousParagrapheBean> getEncadreContenuRubrique(final ContexteUniv ctx) {
        final List<SousParagrapheBean> encadres = new ArrayList<>();
        try {
            final String contenuEncadreRubrique = UnivWebFmt.getEncadreContenuRubriqueCourante(ctx, null, "", "", "", "", "", false, true, false);
            if (StringUtils.isNotBlank(contenuEncadreRubrique)) {
                encadres.addAll(SousParagrapheBean.getSousParagraphes(contenuEncadreRubrique));
            }
        } catch (final Exception e) {
            LOG.error("Impossible de récuperer le contenu de l'encadre de rubrique", e);
        }
        return encadres;
    }

    /**
     * On récupère les encadrés de rubrique de la page courante (rubrique courante et éventuellement ses rubriques mère)
     *
     * @param codeRubriquePageCourante
     *            le code de la rubrique de la page courante
     * @return les encadrés de rubrique de la rubrique de la page courante != null
     */
    public static List<SousParagrapheBean> getEncadresRubrique(final String codeRubriquePageCourante) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(codeRubriquePageCourante);
        final List<SousParagrapheBean> encadres = new ArrayList<>();
        if (rubrique != null) {
            encadres.addAll(SousParagrapheBean.getSousParagraphes(rubrique.getEncadre()));
            RubriqueBean rubriqueMere = rubrique;
            while (rubriqueMere != null && serviceRubrique.getLevel(rubriqueMere) > 0) {
                rubriqueMere = serviceRubrique.getRubriqueByCode(rubriqueMere.getCodeRubriqueMere());
                if (rubriqueMere != null && "1".equals(rubriqueMere.getEncadreSousRubrique()) && StringUtils.isNotBlank(rubriqueMere.getEncadre())) {
                    encadres.addAll(SousParagrapheBean.getSousParagraphes(rubriqueMere.getEncadre()));
                }
            }
        }
        return encadres;
    }

    /**
     * Calcule la liste des encadrés génériques (les objets {@link com.univ.objetspartages.bean.EncadreBean} )
     *
     * @param ctx
     *            Le contexte pour récupérer la langue et le code de rubrique de la page courante..
     * @param fiche
     *            la fiche courante
     * @return la liste des encadrés génériques != null
     */
    public static List<SousParagrapheBean> getEncadresGeneriques(final ContexteUniv ctx, final FicheUniv fiche) {
        final ServiceEncadre serviceEncadre = ServiceManager.getServiceForBean(EncadreBean.class);
        final List<SousParagrapheBean> encadres = new ArrayList<>();
        final List<String> encadresFiche = serviceEncadre.getEncadresList(ctx.getCodeRubriquePageCourante(), fiche, ctx.getLangue());
        for (final String contenuEncadre : encadresFiche) {
            if (StringUtils.isNotBlank(contenuEncadre)) {
                encadres.addAll(SousParagrapheBean.getSousParagraphes(contenuEncadre));
            }
        }
        return encadres;
    }

    /**
     * On récupère les encadrés de recherche externe
     *
     * @param ctx
     *            c'est le contexte qui contient ses encadrés
     * @return la liste des encadrés != null
     */
    public static List<SousParagrapheBean> getEncadresRechercheExterne(final ContexteUniv ctx) {
        final List<SousParagrapheBean> encadres = new ArrayList<>();
        final List<String> encadresExternes = ctx.getEncadresRechercheExternes();
        for (final String contenuEncadre : encadresExternes) {
            if (StringUtils.isNotBlank(contenuEncadre)) {
                encadres.addAll(SousParagrapheBean.getSousParagraphes(contenuEncadre));
            }
        }
        return encadres;
    }

    /**
     * On récupère les encadrés "externe" cad ceux utiliser avec un connecteur PHP par exemple
     *
     * @param ctx
     *            c'est le contexte qui contient ses encadrés
     * @return la liste des encadrés externe != null
     */
    public static List<SousParagrapheBean> getEncadresExterne(final ContexteUniv ctx) {
        return ctx.getEncadresExternes();
    }

    /**
     * Calcule en fonction de la fiche courante l'encadré de recherche à afficher ou non.
     *
     * @param fiche
     *            la fiche pour laquelle on veut récupérer les encadrés
     * @return le contenu de l'encadré de recherche de la fiche courante
     */
    public static List<String> getEncadresRechercheFiche(final FicheUniv fiche) {
        final List<String> encadres = new ArrayList<>();
        if (fiche != null) {
            final List<String> codeObjetsEncadres = getCodesObjetsEncadres(fiche);
            for (final String codeObjet : codeObjetsEncadres) {
                encadres.add(ObjetPartageHelper.getTemplateObjet(ObjetPartageHelper.TEMPLATE_ENCADRE_RECHERCHE, ReferentielObjets.getNomObjet(codeObjet)));
            }
        }
        return encadres;
    }

    private static List<String> getCodesObjetsEncadres(final FicheUniv fiche) {
        final List<String> codeObjetsEncadres = new ArrayList<>();
        if (isEncadreRechercheEmbarqueValide(fiche.getEncadreRecherche())) {
            codeObjetsEncadres.add(fiche.getEncadreRecherche());
        }
        if (isEncadreRechercheEmbarqueValide(fiche.getEncadreRechercheBis())) {
            codeObjetsEncadres.add(fiche.getEncadreRechercheBis());
        }
        if (isEncadreRechercheValide(ReferentielObjets.getCodeObjet(fiche))) {
            codeObjetsEncadres.add(ReferentielObjets.getCodeObjet(fiche));
        }
        return codeObjetsEncadres;
    }

    private static boolean isEncadreRechercheEmbarqueValide(final String codeEncadreRecherche) {
        boolean isValide = Boolean.FALSE;
        if (StringUtils.isNotBlank(codeEncadreRecherche) && !"0000".equals(codeEncadreRecherche)) {
            try {
                final Class<?> classeObjet = Class.forName(ReferentielObjets.getObjetByCode(codeEncadreRecherche).getNomClasse());
                isValide = FicheAnnotationHelper.isEncadreRechercheEmbarquable(classeObjet.newInstance());
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                LOG.error("impossible de recuperer les infos des encadres pour le code suivant : " + codeEncadreRecherche, e);
            }
        }
        return isValide;
    }

    private static boolean isEncadreRechercheValide(final String codeEncadreRecherche) {
        boolean isValide = Boolean.FALSE;
        try {
            isValide = StringUtils.isNotBlank(codeEncadreRecherche) && !"0000".equals(codeEncadreRecherche) && ReferentielObjets.gereEncadreRecherche(codeEncadreRecherche);
        } catch (final Exception e) {
            LOG.error("impossible de recuperer les infos des encadres pour le code suivant : " + codeEncadreRecherche, e);
        }
        return isValide;
    }
}
