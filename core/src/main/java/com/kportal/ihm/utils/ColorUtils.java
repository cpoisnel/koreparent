package com.kportal.ihm.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Classe utilitaire permettant de manipuler des couleurs RGB et de les transformer.
 */
public final class ColorUtils {

    private static final int RADIX = 16;

    private static final String RGBA_TEMPLATE = "rgba(%s,%s,%s,%s)";

    private static final String COLOR_PREFIX = "#";

    private static final String HSL_TEMPLATE = "hsl(%s,%s%%,%s%%)";

    private static final String RGB_VALUE_REGEXP = "^(?:#|)[0-9a-fA-F]{3}([0-9a-fA-F]{3})?$";

    /**
     * Ne pas instancier une classe utilitaire.
     */
    private ColorUtils() {
    }

    /**
     * Retourne une valeur rgba() à partir d'un code hexadécimal de couleur et d'un taux d'opacité.
     *
     * @param colorToTransform
     *            Le code de la couleur à rendre translucide. Il doit être de la forme suivante : #000 #000000 000 ou 000000
     * @param alpha
     *            Le taux d'opacité, chaîne de caractères dont la valeur décimale doit être comprise entre 0 et 1 inclus.
     * @return Retourne une chaîne formatée rgba() pour le CSS. Si le code couleur n'est pas saisi dans le bon format, une chaîne vide est retournée
     */
    public static String getRGBaFromHexa(final String colorToTransform, final double alpha) {
        final String hex = getValidRGB(colorToTransform);
        if (StringUtils.isEmpty(hex) || alpha > 1) {
            return colorToTransform;
        }
        final int r = Integer.valueOf(hex.substring(0, 2), RADIX);
        final int g = Integer.valueOf(hex.substring(2, 4), RADIX);
        final int b = Integer.valueOf(hex.substring(4, 6), RADIX);
        return String.format(RGBA_TEMPLATE, r, g, b, alpha);
    }

    /**
     * Permet de vérifier si la valeur fourni en paramètre correspond à une valeur Hexa valide pour l'html.
     * @param colorValue la chaine à tester pour vérifier que c'est bien une couleur
     * @return vrai si la chaine est de la forme 090909, #090909, fff ou #fff
     */
    public static boolean isValidHtmlHexaValue(final String colorValue) {
        return StringUtils.isNotBlank(colorValue) && colorValue.matches(RGB_VALUE_REGEXP);
    }

    /**
     * Retourne une couleur plus clair du taux de clarté passé en paramêtre.
     *
     * @param colorToTransform la valeur heaxdécimale de la couleur. elle doit être de la forme suivante : #000 #000000 000 ou 000000
     * @param lumi la clarté à apporter (%)
     * @return la chaine css hsl
     */
    public static String getHslFromHexa(final String colorToTransform, final double lumi) {
        final String hex = getValidRGB(colorToTransform);
        if (StringUtils.isEmpty(hex)) {
            return colorToTransform;
        }
        final int r = Integer.valueOf(hex.substring(0, 2), RADIX);
        final int g = Integer.valueOf(hex.substring(2, 4), RADIX);
        final int b = Integer.valueOf(hex.substring(4, 6), RADIX);
        //    Minimum and Maximum RGB values are used in the HSL calculations
        final double rp = r / 255D;
        final double gp = g / 255D;
        final double bp = b / 255D;
        final double min = Math.min(rp, Math.min(gp, bp));
        final double max = Math.max(rp, Math.max(gp, bp));
        //  Calculate the Hue
        final double delta = max - min;
        double h = 0f;
        final double s;
        final double l = (max + min) / 2;
        if (Double.doubleToRawLongBits(max) ==  Double.doubleToRawLongBits(rp)) {
            h = (60 * ((gp - bp) / delta % 6));
        } else if (Double.doubleToRawLongBits(max) ==   Double.doubleToRawLongBits(gp)) {
            h = (60 * ((bp - rp) / delta + 2));
        } else if (Double.doubleToRawLongBits(max) ==   Double.doubleToRawLongBits(bp)) {
            h = (60 * ((rp - gp) / delta + 4));
        }
        if (Double.doubleToRawLongBits(delta) == 0) {
            s = 0;
        } else {
            s = delta / (1 - Math.abs(2 * l - 1));
        }
        return String.format(HSL_TEMPLATE, Math.round(h), s * 100, l * 100 + lumi);
    }

    /**
     * Calcule une couleur assombrie à partir d'un code hexadécimal de couleur et d'un ratio.
     *
     * @param colorToTransform Le code de la couleur à assombrir. Il doit être de la forme suivante : #000 #000000 000 ou 000000
     * @param ratio Le ratio dont la valeur doit être comprise entre 0 et 1 inclus.
     * @return Hexadecimal de la couleur assombrie
     */
    public static String darker(final String colorToTransform, final double ratio) {
        String newColor = getValidRGB(colorToTransform);
        if (StringUtils.isNotEmpty(newColor) && ratio >= 0 && ratio <= 1) {
            final String red = StringUtils.substring(newColor, 0, 2);
            final String green = StringUtils.substring(newColor, 2, 4);
            final String blue = StringUtils.substring(newColor, 4, 6);
            final Integer redHexa = Integer.parseInt(red, RADIX);
            final Integer greenHexa = Integer.parseInt(green, RADIX);
            final Integer blueHexa = Integer.parseInt(blue, RADIX);
            final String newRed = convertIntToColorString((int) (redHexa - (redHexa * ratio)));
            final String newGreen = convertIntToColorString((int) (greenHexa - (greenHexa * ratio)));
            final String newBlue = convertIntToColorString((int) (blueHexa - (blueHexa * ratio)));
            newColor = COLOR_PREFIX + getValidRGB(newRed + newGreen + newBlue);
        }
        return newColor;
    }

    /**
     * Calcule une couleur éclaircie à partir d'un code hexadécimal de couleur et d'un ratio.
     *
     * @param colorToTransform Le code de la couleur à eclaircir. Il doit être de la forme suivante : #000 #000000 000 ou 000000
     * @param ratio Le ratio dont la valeur doit être comprise entre 0 et 1 inclus.
     *
     * @return Hexadecimal de la couleur éclaircie
     */
    public static String lighter(final String colorToTransform, final double ratio) {
        String newColor = getValidRGB(colorToTransform);
        if (StringUtils.isNotEmpty(newColor) && ratio >= 0 && ratio <= 1) {
            final String red = StringUtils.substring(newColor, 0, 2);
            final String green = StringUtils.substring(newColor, 2, 4);
            final String blue = StringUtils.substring(newColor, 4, 6);
            final Integer redHexa = Integer.parseInt(red, RADIX);
            final Integer greenHexa = Integer.parseInt(green, RADIX);
            final Integer blueHexa = Integer.parseInt(blue, RADIX);
            final String newRed = convertIntToColorString((int) ((0xFF - redHexa) * ratio + redHexa));
            final String newGreen = convertIntToColorString((int) ((0xFF - greenHexa) * ratio + greenHexa));
            final String newBlue = convertIntToColorString((int) ((0xFF - blueHexa) * ratio + blueHexa));
            newColor = COLOR_PREFIX + getValidRGB(newRed + newGreen + newBlue);
        }
        return newColor;
    }

    /**
     * Permet d'avoir la couleur en chaine de caractère, si la couleur n'a qu'un seul caractère on rajoute le 0 devant pour ne pas se retrouver avec une couleur sur 4 caractères hexa (donc non valide).
     * @param currentColor la couleur en int que l'on souhaite convertir
     * @return la chaine de caractère correspondante.
     */
    private static String convertIntToColorString(int currentColor) {
        String colorValue = Integer.toHexString(currentColor);
        if (colorValue.length() == 1) {
            colorValue = "0" + colorValue;
        }
        return colorValue;
    }
    /**
     * On vérifie que la chaine fourni en paramètre correspond bien à une valeur RGB valide.
     * @param value la chaine à formatter
     * @return Si la chaine est valide, on retourne une chaine RGB de 6 caractères sans COLOR_PREFIX devant, sinon retourne une chaine vide.
     */
    private static String getValidRGB(String value) {
        String result = StringUtils.EMPTY;
        if (isValidHtmlHexaValue(value)) {
            final String hexaValue = StringUtils.removeStart(value, COLOR_PREFIX);
            if (hexaValue.length() == 3) {
                result = hexaValue.substring(0, 1) + hexaValue.substring(0, 1) + hexaValue.substring(1, 2) + hexaValue.substring(1, 2) + hexaValue.substring(2, 3) + hexaValue.substring(2, 3);
            } else {
                result = hexaValue;
            }
        }
        return result;
    }
}
