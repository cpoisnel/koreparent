package com.kportal.cms.mail;

import java.util.List;
import java.util.Map;

import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.FicheUniv;

public interface MailNotificationFormater {

    String getMessageRetourMiseEnLigne(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean validateur) throws Exception;

    String getMessageRetourNonValide(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean validateur) throws Exception;

    String getMessageDemandeValidation(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean utilisateur_demandeur) throws Exception;

    String getMessageNotificationCollaboratif(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean utilisateur) throws Exception;

    String getMessageNotification(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean utilisateur_demandeur) throws Exception;

    List<String> getClasses();
}
