package com.kportal.cms.mail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.extension.module.AbstractBeanManager;

public class MailFormaterManager extends AbstractBeanManager {

    public static final String ID_BEAN = "mailFormaterManager";

    public Map<String, List<MailNotificationFormater>> formaters;

    public List<MailNotificationFormater> defaultFormaters;

    public List<MailNotificationFormater> getFormaters(String classe) {
        if (formaters.get(classe) != null) {
            return formaters.get(classe);
        } else {
            return defaultFormaters;
        }
    }

    @Override
    public void refresh() {
        Map<String, List<MailNotificationFormater>> tmpMap = new HashMap<>();
        List<MailNotificationFormater> tmpList = new ArrayList<>();
        final Collection<MailNotificationFormater> beans = ApplicationContextManager.getAllBeansOfType(MailNotificationFormater.class).values();
        for (final MailNotificationFormater formater : beans) {
            if (formater.getClasses() != null) {
                for (String classe : formater.getClasses()) {
                    if (tmpMap.get(classe) == null) {
                        List<MailNotificationFormater> l = new ArrayList<>();
                        tmpMap.put(classe, l);
                    }
                    tmpMap.get(classe).add(formater);
                }
            } else {
                tmpList.add(formater);
            }
        }
        formaters = tmpMap;
        defaultFormaters = tmpList;
    }

    public Map<String, List<MailNotificationFormater>> getFormaters() {
        return formaters;
    }

    public void setFormaters(Map<String, List<MailNotificationFormater>> formaters) {
        this.formaters = formaters;
    }

    public List<MailNotificationFormater> getDefaultFormaters() {
        return defaultFormaters;
    }

    public void setDefaultFormaters(List<MailNotificationFormater> defaultFormaters) {
        this.defaultFormaters = defaultFormaters;
    }
}
