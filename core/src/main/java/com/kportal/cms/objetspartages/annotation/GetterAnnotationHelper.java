package com.kportal.cms.objetspartages.annotation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class GetterAnnotationHelper.
 */
public class GetterAnnotationHelper {

    /**
     * teste la présense de l'annotation getterAnnotation et si la méthode set un contenu de toolbox.
     *
     * @param getterMethod
     *            the getter method
     * @return true, if is toolbox
     */
    public static boolean isToolbox(Method getterMethod) {
        GetterAnnotation getterAnnotation = getterMethod.getAnnotation(GetterAnnotation.class);
        return getterAnnotation != null && getterAnnotation.isToolbox();
    }

    /**
     * Renvoit la liste des méthodes d'une fiche correspondant à des getter de contenu toolbox.
     *
     * @param objet
     *            the fiche univ
     * @return the liste toolbox
     */
    public static List<Method> getMethodToolbox(Object objet) {
        ArrayList<Method> liste = new ArrayList<>();
        for (Method method : objet.getClass().getMethods()) {
            if (isToolbox(method)) {
                liste.add(method);
            }
        }
        return liste;
    }

    /**
     * teste la présense de l'annotation getterAnnotation et si la méthode set un contenu de toolbox.
     *
     * @param getterMethod
     *            the getter method
     * @return true, if is toolbox
     */
    public static boolean isIdMedia(Method getterMethod) {
        GetterAnnotation getterAnnotation = getterMethod.getAnnotation(GetterAnnotation.class);
        return getterAnnotation != null && getterAnnotation.isIdMedia();
    }

    /**
     * Renvoit la liste des méthodes d'une fiche correspondant à des getter d'id media.
     *
     * @param objet
     *            the fiche univ
     * @return the liste toolbox
     */
    public static List<Method> getMethodIdMedia(Object objet) {
        ArrayList<Method> liste = new ArrayList<>();
        for (Method method : objet.getClass().getMethods()) {
            if (isIdMedia(method)) {
                liste.add(method);
            }
        }
        return liste;
    }
}
