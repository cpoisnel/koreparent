package com.kportal.cms.objetspartages.om;

public interface ContentRedirect {

    String getUrlRedirect();
}
