package com.kportal.cms.objetspartages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.kportal.extension.module.AbstractBeanManager;
import com.kportal.extension.module.IModule;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.IEtat;
import com.univ.objetspartages.om.StructureModele;

/**
 * The Class ReferentielManager.
 */
public class ReferentielManager extends AbstractBeanManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "referentielManager";

    /** Logger available to subclasses. */
    private static final Logger LOG = LoggerFactory.getLogger(ReferentielManager.class);

    /** The objets by code. */
    private Map<String, Objetpartage> objetsByCode;

    /** The codes objets by nom. */
    private Map<String, String> codesObjetsByNom;

    /** The codes objets by classe. */
    private Map<String, String> codesObjetsByClasse;

    /** The codes objets structure. */
    private Collection<String> codesObjetsStructure;

    /** The codes objets tries. */
    private List<String> codesObjetsTries;

    private List<String> etatsObjet;

    private List<String> etatsObjetFront;

    private List<Objetpartage> objetsTries;

    /**
     * Inits the.
     */
    @Override
    public void refresh() {
        final Collection<Objetpartage> objetsPartages = moduleManager.getModules(Objetpartage.class);
        objetsByCode = new HashMap<>();
        codesObjetsByNom = new HashMap<>();
        codesObjetsByClasse = new HashMap<>();
        codesObjetsStructure = new ArrayList<>();
        objetsTries = new ArrayList<>();
        for (final IModule module : objetsPartages) {
            final Objetpartage objet = (Objetpartage) module;
            String nomObjet = objet.getNomObjet().toLowerCase();
            String codeObjet = objet.getCodeObjet();
            if (objetsByCode.get(codeObjet) == null && codesObjetsByNom.get(nomObjet) == null) {
                try {
                    final Class<?> classeObjet = Class.forName(objet.getNomClasse());
                    if (classeObjet.newInstance() instanceof StructureModele) {
                        codesObjetsStructure.add(codeObjet);
                    }
                } catch (final ReflectiveOperationException e) {
                    LOG.debug("impossible d'instancier la classe", e);
                    LOG.error("La classe " + objet.getNomClasse() + " n'a pas pu être instanciée, l'objet " + nomObjet + " est ignoré");
                    continue;
                }
                objet.setNomObjet(nomObjet);
                objetsByCode.put(codeObjet, objet);
                objetsTries.add(objet);
                codesObjetsByNom.put(nomObjet, codeObjet);
                codesObjetsByClasse.put(objet.getNomClasse(), codeObjet);
            } else {
                LOG.warn("L'objet " + objet.getNomObjet() + " n'a pas été chargé car son code ou nom existe déjà!");
            }
        }
        codesObjetsTries = new ArrayList<>(objetsByCode.keySet());
        Collections.sort(codesObjetsTries);
        // chargement des états
        etatsObjet = new ArrayList<>();
        etatsObjetFront = new ArrayList<>();
        // Enum par défaut
        for (final EtatFiche etat : EtatFiche.values()) {
            etatsObjet.add(etat.getEtat());
            if (etat.isFront()) {
                etatsObjetFront.add(etat.getEtat());
            }
        }
        // Chargement dynamique d'états supplémentaires
        for (final IEtat etat : ClassBeanManager.getInstance().getBeanOfType(IEtat.class)) {
            etatsObjet.add(etat.getEtat());
            if (etat.isFront()) {
                etatsObjetFront.add(etat.getEtat());
            }
        }
        //On trie une fois pour toute la liste
        Collections.sort(objetsTries);
    }

    /**
     * Gets the objet by nom.
     *
     * @param nom
     *            the nom
     * @return the objet by nom
     */
    public Objetpartage getObjetByNom(final String nom) {
        Objetpartage result = null;
        if (StringUtils.isNotBlank(nom)) {
            String code = codesObjetsByNom.get(nom.toLowerCase());
            if (StringUtils.isNotBlank(code)) {
                result = objetsByCode.get(code);
            }
        }
        return result;
    }

    /**
     * Gets the objets by code.
     *
     * @return the objets by code
     */
    public Map<String, Objetpartage> getObjetsByCode() {
        return objetsByCode;
    }

    /**
     * Gets the codes objets by nom.
     *
     * @return the codes objets by nom
     */
    public Map<String, String> getCodesObjetsByNom() {
        return codesObjetsByNom;
    }

    /**
     * Gets the codes objets by classe.
     *
     * @return the codes objets by classe
     */
    public Map<String, String> getCodesObjetsByClasse() {
        return codesObjetsByClasse;
    }

    /**
     * Gets the codes objets structure.
     *
     * @return the codes objets structure
     */
    public Collection<String> getCodesObjetsStructure() {
        return codesObjetsStructure;
    }

    /**
     * Gets the codes objets tries.
     *
     * @return the codes objets tries
     */
    public List<String> getCodesObjetsTries() {
        return codesObjetsTries;
    }

    public List<Objetpartage> getObjetsTries() {
        return objetsTries;
    }

    public List<String> getEtatsObjet() {
        return etatsObjet;
    }

    public List<String> getEtatsObjetFront() {
        return etatsObjetFront;
    }
}
