package com.kportal.cms.objetspartages.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//Annotation accessible à l'execution
@Retention(RetentionPolicy.RUNTIME)
//L'annotation se place sur une méthode
@Target(ElementType.METHOD)
public @interface SetterAnnotation {

    boolean isToolbox() default false;

    boolean isIdMedia() default false;
}
