package com.kportal.cms.objetspartages.annotation;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;

public class FicheAnnotationHelper {

    /**
     * Est ce que la fiche est une fiche affichable en front office?
     *
     * @param fiche
     *            la fiche à tester
     * @return vrai si fiche.[TYPE_FICHE].fiche_frontoffice=1 ou si cette propriété est vide, si {@link FicheAnnotation#isFicheFrontOffice()} est vrai
     */
    public static boolean isFicheFrontOffice(final FicheUniv fiche) {
        final FicheAnnotation annotation = fiche.getClass().getAnnotation(FicheAnnotation.class);
        final String propriete = getProperty(fiche, "fiche_frontoffice");
        boolean isFicheFrontOffice = annotation == null || annotation.isFicheFrontOffice();
        if (StringUtils.isNotBlank(propriete)) {
            isFicheFrontOffice = "1".equals(propriete);
        }
        return isFicheFrontOffice;
    }

    /**
     * Est ce que la fiche est une fiche saisissable en front office?
     *
     * @param fiche
     *            la fiche à tester
     * @return vrai si fiche.[TYPE_FICHE].saisie_frontoffice=1 ou si cette propriété est vide, si {@link FicheAnnotation#isSaisieFrontOffice()} est vrai
     */
    public static boolean isSaisieFrontOffice(final FicheUniv fiche) {
        final FicheAnnotation annotation = fiche.getClass().getAnnotation(FicheAnnotation.class);
        final String propriete = getProperty(fiche, "saisie_frontoffice");
        boolean isSaisieFrontOffice = annotation == null || annotation.isSaisieFrontOffice();
        if (StringUtils.isNotBlank(propriete)) {
            isSaisieFrontOffice = "1".equals(propriete);
        }
        return isSaisieFrontOffice;
    }

    /**
     * Est ce que l'on peut faire un lien interne via la toolbox sur ce type de fiche?
     *
     * @param ficheOuStructure
     *            la fiche à tester
     * @return vrai si fiche.[TYPE_FICHE].lien_interne=1 ou si cette propriété est vide, si {@link FicheAnnotation#isLienInterne()} est vrai
     */
    public static boolean isLienInterne(final Object ficheOuStructure) {
        final FicheAnnotation annotation = ficheOuStructure.getClass().getAnnotation(FicheAnnotation.class);
        final String propriete = getProperty(ficheOuStructure, "lien_interne");
        boolean isLienInterne = annotation == null || annotation.isLienInterne();
        if (StringUtils.isNotBlank(propriete)) {
            isLienInterne = "1".equals(propriete);
        }
        return isLienInterne;
    }

    /**
     * Est ce que l'on peut faire un lien de requête via la toolbox sur ce type de fiche?
     *
     * @param ficheOuStructure
     *            la fiche à tester
     * @return vrai si fiche.[TYPE_FICHE].lien_requete=1 ou si cette propriété est vide, si {@link FicheAnnotation#isLienRequete()} est vrai
     */
    public static boolean isLienRequete(final Object ficheOuStructure) {
        final FicheAnnotation annotation = ficheOuStructure.getClass().getAnnotation(FicheAnnotation.class);
        final String propriete = getProperty(ficheOuStructure, "lien_requete");
        boolean isLienRequete = annotation == null || annotation.isLienRequete();
        if (StringUtils.isNotBlank(propriete)) {
            isLienRequete = "1".equals(propriete);
        }
        return isLienRequete;
    }

    /**
     * Est ce que la fiche est indexable pour la recherche fulltext?
     *
     * @param fiche
     *            la fiche à tester
     * @return vrai si fiche.[TYPE_FICHE].indexable=1 ou si cette propriété est vide, si {@link FicheAnnotation#isIndexable()} est vrai
     */
    public static boolean isIndexable(final FicheUniv fiche) {
        final FicheAnnotation annotation = fiche.getClass().getAnnotation(FicheAnnotation.class);
        final String propriete = getProperty(fiche, "indexable");
        boolean isIndexable = annotation == null || annotation.isIndexable();
        if (StringUtils.isNotBlank(propriete)) {
            isIndexable = "1".equals(propriete);
        }
        return isIndexable;
    }

    /**
     * Permet de savoir si la fiche courante possède un encadré de recherche avancée de son type ou non. Exemple : une formation a t elle un encadré de recherche de formation?
     *
     * @param ficheOuStructure
     *            la fiche ou la structure à tester
     * @return vrai si fiche.[TYPE_FICHE].encadre_recherche=1 ou si cette propriété est vide, si {@link FicheAnnotation#isEncadreRecherche()} est vrai
     */
    public static boolean isEncadreRechercheSurFicheCourante(final Object ficheOuStructure) {
        final FicheAnnotation annotation = ficheOuStructure.getClass().getAnnotation(FicheAnnotation.class);
        final String propriete = getProperty(ficheOuStructure, "encadre_recherche");
        boolean isEncadreRecherche = annotation == null || annotation.isEncadreRecherche();
        if (StringUtils.isNotBlank(propriete)) {
            isEncadreRecherche = "1".equals(propriete);
        }
        return isEncadreRecherche;
    }

    /**
     * Permet de savoir si une fiche de type X possède un formulaire de recherche qui peut être mis dans un encadré de recherche d'un autre type de fiche. Exemple : une page libre
     * peut elle avoir un encadré de recherche d'une fiche formation?
     *
     * @param ficheOuStructure
     *            la fiche ou la structure à tester
     * @return vrai si fiche.[TYPE_FICHE].encadre_recherche_embarquable=1 ou si cette propriété est vide, si {@link FicheAnnotation#isEncadreRechercheEmbarquable()} est vrai
     */
    public static boolean isEncadreRechercheEmbarquable(final Object ficheOuStructure) {
        final FicheAnnotation annotation = ficheOuStructure.getClass().getAnnotation(FicheAnnotation.class);
        final String propriete = getProperty(ficheOuStructure, "encadre_recherche_embarquable");
        boolean isEncadreRecherche = annotation == null || annotation.isEncadreRechercheEmbarquable();
        if (StringUtils.isNotBlank(propriete)) {
            isEncadreRecherche = "1".equals(propriete);
        }
        return isEncadreRecherche;
    }

    /**
     * Est ce que la fiche est accessible depuis le backoffice?
     *
     * @param ficheOuStructure
     *            la fiche à tester
     * @return vrai si fiche.[TYPE_FICHE].is_accessible_bo=1 ou si cette propriété est vide, si {@link FicheAnnotation#isAccessibleBo()} est vrai
     */
    public static boolean isAccessibleBo(final Object ficheOuStructure) {
        final FicheAnnotation annotation = ficheOuStructure.getClass().getAnnotation(FicheAnnotation.class);
        final String propriete = getProperty(ficheOuStructure, "is_accessible_bo");
        boolean isAccessibleBo = annotation == null || annotation.isAccessibleBo();
        if (StringUtils.isNotBlank(propriete)) {
            isAccessibleBo = "1".equals(propriete);
        }
        return isAccessibleBo;
    }

    /**
     * Est ce que le contenu de la fiche est duplicable en JSON ?
     *
     * @param fiche
     *            la fiche à tester
     * @return vrai si fiche.[TYPE_FICHE].contenu_duplicable=1 ou si cette propriété est vide, si {@link FicheAnnotation#isContenuDuplicable()} est vrai
     */
    public static boolean isContenuDuplicable(final FicheUniv fiche) {
        final FicheAnnotation annotation = fiche.getClass().getAnnotation(FicheAnnotation.class);
        final String propriete = getProperty(fiche, "contenu_duplicable");
        boolean isIndexable = annotation == null || annotation.isContenuDuplicable();
        if (StringUtils.isNotBlank(propriete)) {
            isIndexable = "1".equals(propriete);
        }
        return isIndexable;
    }

    private static String getProperty(final Object ficheOuStructure, final String nomProperty) {
        String propriete = StringUtils.EMPTY;
        if (ficheOuStructure instanceof FicheUniv) {
            final FicheUniv fiche = (FicheUniv) ficheOuStructure;
            final String idExtension = ReferentielObjets.getExtension(fiche);
            final String nomObjet = ReferentielObjets.getNomObjet(fiche);
            propriete = PropertyHelper.getProperty(idExtension, "fiche." + StringUtils.upperCase(nomObjet) + "." + nomProperty);
        }
        return propriete;
    }
}
