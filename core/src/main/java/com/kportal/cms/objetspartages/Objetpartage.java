package com.kportal.cms.objetspartages;

import java.util.List;

import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.module.composant.Composant;
import com.univ.objetspartages.dao.AbstractFicheDAO;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;

/**
 * The Class Objetpartage.
 */
public class Objetpartage extends Composant {

    /** The nom classe. */
    public String nomClasse = "";

    /** The nom objet. */
    public String nomObjet = "";

    /** The code objet. */
    public String codeObjet = "";

    /** The Libelle objet. */
    public String libelleObjet = "";

    public String pathTemplateJsp = "";

    private AbstractFicheDAO<?> ficheDAO;

    @Override
    public boolean isAccessibleBO() {
        final Object fiche = ReferentielObjets.instancierObjet(this.getCodeObjet());
        return FicheAnnotationHelper.isAccessibleBo(fiche);
    }

    /**
     * Gets the nom classe.
     *
     * @return the nom classe
     */
    public String getNomClasse() {
        return nomClasse;
    }

    /**
     * Sets the nom classe.
     *
     * @param nomClasse
     *            the new nom classe
     */
    public void setNomClasse(final String nomClasse) {
        this.nomClasse = nomClasse;
    }

    /**
     * Gets the nom objet.
     *
     * @return the nom objet
     */
    public String getNomObjet() {
        return nomObjet;
    }

    /**
     * Sets the nom objet.
     *
     * @param nomObjet
     *            the new nom objet
     */
    public void setNomObjet(final String nomObjet) {
        this.nomObjet = nomObjet;
    }

    /**
     * Gets the code objet.
     *
     * @return the code objet
     */
    public String getCodeObjet() {
        return codeObjet;
    }

    /**
     * Sets the code objet.
     *
     * @param codeObjet
     *            the new code objet
     */
    public void setCodeObjet(final String codeObjet) {
        this.codeObjet = codeObjet;
    }

    /**
     * Gets the libelle objet.
     *
     * @return the libelle objet
     */
    public String getLibelleObjet() {
        return ExtensionHelper.getMessage(getIdExtension(), libelleObjet);
    }

    /**
     * Sets the libelle objet.
     *
     * @param libelle
     *            the new libelle objet
     */
    public void setLibelleObjet(final String libelle) {
        libelleObjet = libelle;
    }

    public String getPathTemplateJsp() {
        return pathTemplateJsp;
    }

    public void setPathTemplateJsp(final String pathTemplateJsp) {
        this.pathTemplateJsp = pathTemplateJsp;
    }

    /**
     * Teste si l'objet supporte le collaboratif (param : fiche.XXX.collaboratif = 1 ou 2)
     *
     * @return true si on peut ajouter l'objet comme service du collaboratif
     */
    public boolean isCollaboratif() {
        final String param = getProperty("fiche." + getNomObjet().toUpperCase() + ".collaboratif");
        return "1".equals(param) || "2".equals(param);
    }

    /**
     * Teste si l'objet supporte la recherche avancée (param fiche.XXX.recherche_avancee=1)
     *
     * @return true si on peut rechercher l'objet
     */
    public boolean isRecherchable() {
        final String param = getProperty("fiche." + getNomObjet().toUpperCase() + ".recherche_avancee");
        return "1".equals(param);
    }

    /**
     * Teste si l'objet est strictement collaboratif (param : fiche.XXX.collaboratif = 1)
     *
     * @return true si on peut ajouter l'objet comme service du collaboratif
     */
    public boolean isStrictlyCollaboratif() {
        final String param = getProperty("fiche." + getNomObjet().toUpperCase() + ".collaboratif");
        return "1".equals(param);
    }

    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        if (!isAccessibleBO()) {
            return Boolean.FALSE;
        }
        final FicheUniv ficheObjetCourant = ReferentielObjets.instancierFiche(getCodeObjet());
        if (autorisation == null || ficheObjetCourant == null) {
            return Boolean.FALSE;
        }
        final List<String> objetsUtilisateurs = autorisation.getListeObjets();
        return objetsUtilisateurs != null && objetsUtilisateurs.contains(codeObjet);
    }

    @Override
    public boolean isActionVisible(final AutorisationBean autorisation, final String code) {
        if (!isVisible(autorisation)) {
            return Boolean.FALSE;
        }
        int indiceAutorisation = 0;
        switch (code) {
            case "AJOUTER":
                indiceAutorisation = AutorisationBean.INDICE_CREATION;
                break;
            case "MODIFIER":
                indiceAutorisation = AutorisationBean.INDICE_MODIFICATION;
                break;
            case "TRADUIRE":
                indiceAutorisation = AutorisationBean.INDICE_TRADUCTION;
                break;
            case "VALIDER":
                indiceAutorisation = AutorisationBean.INDICE_VALIDATION;
                break;
            case "SUPPRIMER_UNITAIRE":
                indiceAutorisation = AutorisationBean.INDICE_SUPPRESSION_UNITAIRE;
                break;
            case "SUPPRIMER":
                indiceAutorisation = AutorisationBean.INDICE_SUPPRESSION;
                break;
        }
        return autorisation.getAutorisation(codeObjet, indiceAutorisation);
    }

    @Override
    public String getLibelleMenu() {
        return ExtensionHelper.getMessage(getIdExtension(), getLibelleObjet());
    }

    @Override
    public String getLibelleAffichable() {
        return getLibelleMenu();
    }

    public AbstractFicheDAO<?> getFicheDAO() {
        return ficheDAO;
    }

    public void setFicheDAO(AbstractFicheDAO<?> ficheDAO) {
        this.ficheDAO = ficheDAO;
    }
}
