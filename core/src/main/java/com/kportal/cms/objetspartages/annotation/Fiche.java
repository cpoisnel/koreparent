package com.kportal.cms.objetspartages.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.fasterxml.jackson.annotation.JacksonAnnotation;
import com.univ.objetspartages.om.ReferentielObjets;

/**
 * The Annotation Fiche.
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotation
public @interface Fiche {

	/**
	 * Récupère le nom de l'objet {@link ReferentielObjets} cible du rattachement.
	 *
	 * @return String nomObjet - Le nom de l'objet. Ne retourne jamais <code>null</code>.
	 */
	String nomObjet();

	/**
	 * Récupère le contexte de l'extension associé (si défini).
	 *
	 * @return String contexte - Le contexte de l'extension attendu sinon <code>core</code>.
	 */
	String contexte() default "core";
}
