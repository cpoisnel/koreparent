package com.kportal.cms.objetspartages.oi;

import com.univ.objetspartages.om.FicheUniv;

public interface ObjetMail {

    /**
     * Renvoie le nom de la personne à qui on envoie l'email.
     *
     * @return the libelle mailto
     *
     */
    String getLibelle(FicheUniv ficheUniv);

    /**
     * Renvoie l'email de la personne à qui on écrit.
     *
     * @return the mailto
     *
     */
    String getAdresse(FicheUniv ficheUniv);
}
