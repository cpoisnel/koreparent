package com.kportal.cms.objetspartages.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import com.fasterxml.jackson.annotation.JacksonAnnotation;
import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.kportal.cms.objetspartages.annotation.strategy.LabelStrategy;
import com.kportal.cms.objetspartages.annotation.strategy.ServiceLabelStrategy;

/**
 * Annotation sur les Labels.
 *
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
public @interface Label {

    /**
     * Définit le type de Libelle (issu de la table LIBELLE).
     */
    String type() default "";


    /**
     * Stratégie de mapping permettant d'associer le code de libellé avec une valeur.
     */
    Class<? extends LabelStrategy> strategy() default ServiceLabelStrategy.class;

    /**
     * Contexte d'extension.
     * <p>Par défaut, contexte "core"</p>
     * @return
     */
    String contexte() default "core";
}

