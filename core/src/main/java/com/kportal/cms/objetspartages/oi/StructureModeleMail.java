package com.kportal.cms.objetspartages.oi;

import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.StructureModele;

/**
 * Classe renvoyant le nom et l'adresse mail du responsable d'une structure.
 *
 * @author yacouba.kone
 *
 */
public class StructureModeleMail implements ObjetMail {

    /**
     * Renvoie le nom du responsable d'une structure à qui on envoie l'email.
     *
     * @return the nom responsable
     */
    @Override
    public String getLibelle(final FicheUniv ficheUniv) {
        return ficheUniv.getLibelleAffichable();
    }

    /**
     * Renvoie l'email du responsable d'une structure à qui on écrit.
     *
     * @return the mailto
     */
    @Override
    public String getAdresse(final FicheUniv ficheUniv) {
        return ((StructureModele) ficheUniv).getAdresseMail();
    }
}
