package com.kportal.core;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.univ.multisites.InfosSite;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;

/**
 */
public class ErrorServlet extends HttpServlet {

    /**
     * ID Serialisation
     */
    private static final long serialVersionUID = 6792391717404684763L;

    private static final Logger LOG = LoggerFactory.getLogger(ErrorServlet.class);

    private static final String CONTENT_TYPE = "text/html";

    @Override
    public void doGet(final HttpServletRequest servletRequest, final HttpServletResponse servletResponse) {
        servletResponse.setContentType(CONTENT_TYPE);
        try {
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            final int code = NumberUtils.toInt(servletRequest.getParameter("CODE"), HttpServletResponse.SC_NOT_FOUND);
            String urlDemandee = StringUtils.EMPTY;
            final Object requestURI = servletRequest.getAttribute("javax.servlet.forward.request_uri");
            if (requestURI != null) {
                urlDemandee = requestURI.toString();
            }
            final Object queryString = servletRequest.getAttribute("javax.servlet.forward.query_string");
            if (queryString != null && StringUtils.isNotEmpty(urlDemandee)) {
                urlDemandee += "?" + queryString.toString();
            }
            final String referer = servletRequest.getHeader("referer");
            // on ne forwarde que les pages, pas les images introuvables par ex.
            if (referer == null || referer.contains(urlDemandee)) {
                LOG.error("Page introuvable : " + URLResolver.getAbsoluteUrl(urlDemandee, ctx));
                servletResponse.setStatus(code);
                final InfosSite siteCourant = ctx.getInfosSite();
                final String urlRelativeJspError = siteCourant.getJspFo() + "/error/" + code + ".jsp?URL_DEMANDEE=" + URLEncoder.encode(urlDemandee, CharEncoding.DEFAULT);
                //String urlRelativeJspErrorcontextuelle = URLResolver.getRelativeURLContextuelle(siteCourant, urlRelativeJspError);
                final RequestDispatcher rd = getServletConfig().getServletContext().getRequestDispatcher(urlRelativeJspError);
                rd.forward(servletRequest, servletResponse);
            } else {
                LOG.error("Contenu introuvable : " + urlDemandee);
            }
        } catch (final ServletException | IOException e) {
            LOG.error("unable to send the error response", e);
        }
    }

    @Override
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) {
        doGet(request, response);
    }
}
