package com.kportal.core.context;

public class OverridedContextBeanImpl implements OverridedContextBean {

    String idBean = "";

    String idExtension = "";

    String idBeanToOverride = "";

    String idExtensionToOverride = "";

    @Override
    public String getIdBean() {
        return idBean;
    }

    @Override
    public void setIdBean(String idBean) {
        this.idBean = idBean;
    }

    @Override
    public String getIdExtensionToOverride() {
        return idExtensionToOverride;
    }

    @Override
    public void setIdExtensionToOverride(String idExtensionToOverrride) {
        this.idExtensionToOverride = idExtensionToOverrride;
    }

    @Override
    public String getIdBeanToOverride() {
        return idBeanToOverride;
    }

    @Override
    public void setIdBeanToOverride(String idBean) {
        idBeanToOverride = idBean;
    }

    @Override
    public String getIdExtension() {
        return idExtension;
    }

    @Override
    public void setIdExtension(String idExt) {
        idExtension = idExt;
    }
}
