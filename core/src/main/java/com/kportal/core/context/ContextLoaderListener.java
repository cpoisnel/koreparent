package com.kportal.core.context;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRegistration.Dynamic;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.jsbsoft.jtf.core.LogAppFilter;
import com.jsbsoft.jtf.core.SG;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kosmos.toolbox.servlet.CkeditorConfigurationServlet;
import com.kosmos.usinesite.servlet.FichiersUASServlet;
import com.kportal.core.ErrorServlet;
import com.kportal.core.config.PropertiesLoader;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.ihm.FrontOfficeFilter;
import com.kportal.pdf.PDFFilter;
import com.kportal.pdf.PDFServlet;
import com.kportal.servlet.KeepAliveServlet;
import com.univ.autocomplete.servlet.AutoCompletionServlet;
import com.univ.collaboratif.utils.LectureFichiergw;
import com.univ.datagrid.servlet.DatagridServlet;
import com.univ.datagrid.servlet.LienPopupServlet;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.tree.servlet.JsTreeServlet;
import com.univ.url.ContentServlet;
import com.univ.url.RubriqueServlet;
import com.univ.utils.FileReaderServlet;
import com.univ.utils.LectureImageToolbox;
import com.univ.utils.filter.ContexteFilter;
import com.univ.utils.filter.ForwardContexteFilter;

import ro.isdc.wro.http.WroContextFilter;
import ro.isdc.wro.http.WroFilter;

public class ContextLoaderListener implements ServletContextListener {

    public static final String ERROR_SERVLET_NAME = "errorServlet";

    public static final String MEDIA_URL_PATTERN = "media.url-pattern";

    public static final String CONTENT_URL_PATTERN = "content.url-pattern";

    private static final Logger LOG = LoggerFactory.getLogger(ContextLoaderListener.class);

    private static final String JTF_PROPERTIES_FILE = "jtf.properties";

    private static final String ENV_PROPERTIES_FILE = "env.properties";

    private static final String DEFAULT_RUBRIQUE_URL_PATTERN = "/";

    private static final String DEFAULT_MEDIA_URL_PATTERN = "/medias/*";

    private static final String DEFAULT_CONTENT_URL_PATTERN = "*.kjsp";

    private static final String RUBRIQUE_URL_PATTERN = "rubrique.url-pattern";

    private static final String PATH_ENV_PROPERTIES = "path.properties";

    private static final String ENV_ETOILE_PROPERTIES_FILE = "env*.properties";

    private static ServletContext context = null;

    public static void ajouterServlet(final String servletName, final Class<? extends HttpServlet> servletClass, final int loadOnStartup, final List<String> mapping) {
        final ServletRegistration servletRegistration = context.getServletRegistration(servletName);
        if (servletRegistration == null) {
            final Dynamic servlet = context.addServlet(servletName, servletClass);
            for (final String urlMapping : mapping) {
                servlet.addMapping(urlMapping);
            }
            if (loadOnStartup != -1) {
                servlet.setLoadOnStartup(loadOnStartup);
            }
        }
    }

    public static void ajouterServlet(final HttpServlet declaration) {
        final WebServlet conf = declaration.getClass().getAnnotation(WebServlet.class);
        if (conf != null) {
            ajouterServlet(conf.name(), declaration.getClass(), conf.loadOnStartup(), Arrays.asList(conf.urlPatterns()));
        }
    }

    public static void ajouterFilter(final Filter filter) {
        final WebFilter conf = filter.getClass().getAnnotation(WebFilter.class);
        if (conf != null) {
            final Map<String, String> h = new HashMap<>();
            if (conf.initParams().length > 0) {
                for (final WebInitParam param : conf.initParams()) {
                    h.put(param.name(), param.value());
                }
            }
            final Map<String, List<DispatcherType>> dispatchersByPattern = new HashMap<>();
            for (final String pattern : conf.urlPatterns()) {
                dispatchersByPattern.put(pattern, Arrays.asList(conf.dispatcherTypes()));
            }
            final Map<String, List<DispatcherType>> dispatchersByServlet = new HashMap<>();
            for (final String servlet : conf.servletNames()) {
                dispatchersByServlet.put(servlet, Arrays.asList(conf.dispatcherTypes()));
            }
            ajouterFilter(conf.filterName(), filter.getClass(), dispatchersByPattern, dispatchersByServlet, h, conf.asyncSupported());
        }
    }

    private static void ajouterFilter(final String filterName, final Class<? extends Filter> filterClass, final Map<String, List<DispatcherType>> dispatcherByPattern, final Map<String, List<DispatcherType>> dispatchersByServlet, final Map<String, String> parameters) {
        ajouterFilter(filterName, filterClass, dispatcherByPattern, dispatchersByServlet, parameters, false);
    }

    public static void ajouterFilter(final String filterName, final Class<? extends Filter> filterClass, final Map<String, List<DispatcherType>> dispatchersByPattern, final Map<String, List<DispatcherType>> dispatchersByServlet, final Map<String, String> parameters, final boolean asyncSupported) {
        final FilterRegistration filterRegistration = context.getFilterRegistration(filterName);
        if (filterRegistration == null) {
            final FilterRegistration.Dynamic filter = context.addFilter(filterName, filterClass);
            if (parameters != null && MapUtils.isNotEmpty(parameters)) {
                filter.setInitParameters(parameters);
            }
            for (final Entry<String, List<DispatcherType>> pattern : dispatchersByPattern.entrySet()) {
                for (final DispatcherType dispatcher : pattern.getValue()) {
                    filter.addMappingForUrlPatterns(EnumSet.of(dispatcher), false, pattern.getKey());
                }
            }
            for (final Entry<String, List<DispatcherType>> servlet : dispatchersByServlet.entrySet()) {
                for (final DispatcherType dispatcher : servlet.getValue()) {
                    filter.addMappingForServletNames(EnumSet.of(dispatcher), false, servlet.getKey());
                }
            }
            if (asyncSupported) {
                filter.setAsyncSupported(true);
            }
        }
    }

    @Override
    public void contextDestroyed(final ServletContextEvent arg0) {
    }

    @Override
    public void contextInitialized(final ServletContextEvent event) {
        context = event.getServletContext();
        // chargement des properties
        initSystemProperties();
        // chargement des servlets
        // lecture des contenus
        ContextLoaderListener.ajouterServlet("contentServlet", ContentServlet.class, -1, Collections.singletonList(System.getProperty(CONTENT_URL_PATTERN)));
        // lecture des pages de tête de rubrique
        ContextLoaderListener.ajouterServlet("rubriqueServlet", RubriqueServlet.class, -1, Collections.singletonList(System.getProperty(RUBRIQUE_URL_PATTERN)));
        // servlet générale pour tous les processus
        ContextLoaderListener.ajouterServlet("sg", SG.class, -1, Collections.singletonList(WebAppUtil.SG_PATH));
        // récuperation des fichiers de l'UAS.
        ContextLoaderListener.ajouterServlet("uasServlet", FichiersUASServlet.class, 0, Collections.singletonList(WebAppUtil.getRelatifFichiersSitesPath() + "*"));
        // génération d’un pdf à partir d’une url
        ContextLoaderListener.ajouterServlet("pdfServlet", PDFServlet.class, -1, Collections.singletonList("/servlet/com.kportal.pdf.PDFServlet"));
        // lecture des ressources numériques
        ContextLoaderListener.ajouterServlet("lectureFichiergw", LectureFichiergw.class, -1, Arrays.asList("/servlet/com.univ.collaboratif.utils.LectureFichiergw", System.getProperty(MEDIA_URL_PATTERN)));
        // lecture de rapport ou fichier
        ContextLoaderListener.ajouterServlet("fileReaderServlet", FileReaderServlet.class, -1, Collections.singletonList("/servlet/com.univ.utils.FileReaderServlet"));
        // lecture spécifique des images dans la toolbox
        ContextLoaderListener.ajouterServlet("lectureImageToolbox", LectureImageToolbox.class, -1, Collections.singletonList("/servlet/com.univ.utils.LectureImageToolbox"));
        // retourne les résultats JSON pour les datagrids
        ContextLoaderListener.ajouterServlet("datagridServlet", DatagridServlet.class, -1, Collections.singletonList(DatagridUtils.BASE_URL_DATAGRID));
        // retourne les résultats de recherche en JSON pour la recherche full text
        ContextLoaderListener.ajouterServlet("autoCompletionServlet", AutoCompletionServlet.class, -1, Collections.singletonList("/servlet/com.kportal.servlet.autoCompletionServlet/*"));
        // calcul de l’url d’une popup de saisie BO
        ContextLoaderListener.ajouterServlet("lienPopupServlet", LienPopupServlet.class, -1, Collections.singletonList(LienPopupServlet.LIEN_SERVLET));
        // affichage de la vue arborescente
        ContextLoaderListener.ajouterServlet("jsTreeServlet", JsTreeServlet.class, -1, Collections.singletonList("/servlet/com.kportal.servlet.JsTreeServlet/*"));
        // Gestion de la connexion continue
        ContextLoaderListener.ajouterServlet("keepAliveServlet", KeepAliveServlet.class, -1, Collections.singletonList("/servlet/com.kportal.servlet.KeepAliveServlet"));
        // Gestion de la connexion continue
        ContextLoaderListener.ajouterServlet("ckeditorServlet", CkeditorConfigurationServlet.class, -1, Collections.singletonList("/servlet/ckeditor/configuration"));
        // traitement des erreurs
        ContextLoaderListener.ajouterServlet(ERROR_SERVLET_NAME, ErrorServlet.class, -1, Collections.singletonList("/servlet/com.kportal.core.ErrorServlet"));
        // chargement des filtres, attention l'ordre d'execution est celui d'ajout
        final Map<String, String> parameters = new HashMap<>();
        final Map<String, List<DispatcherType>> dispatchersByPattern = new HashMap<>();
        final Map<String, List<DispatcherType>> dispatchersByServlet = new HashMap<>();
        // force l’encodage en utf-8 pour toutes les requêtes
        parameters.put("encoding", CharEncoding.DEFAULT);
        dispatchersByPattern.put("/*", Collections.singletonList(DispatcherType.REQUEST));
        ContextLoaderListener.ajouterFilter("setCharacterEncodingFilter", CharacterEncodingFilter.class, dispatchersByPattern, dispatchersByServlet, parameters, true);
        dispatchersByPattern.clear();
        parameters.clear();
        // initialise contexte applicatif dans un thread local pour la requête courante
        dispatchersByPattern.put("/*", Collections.singletonList(DispatcherType.REQUEST));
        dispatchersByServlet.put(ERROR_SERVLET_NAME, Collections.singletonList(DispatcherType.ERROR));
        ContextLoaderListener.ajouterFilter("contexteFilter", ContexteFilter.class, dispatchersByPattern, dispatchersByServlet, Collections.<String, String>emptyMap());
        dispatchersByPattern.clear();
        dispatchersByServlet.clear();
        // transfert du contexte en cas de forward de la requête
        dispatchersByPattern.put("/*", Collections.singletonList(DispatcherType.FORWARD));
        ContextLoaderListener.ajouterFilter("forwardContexteFilter", ForwardContexteFilter.class, dispatchersByPattern, dispatchersByServlet, Collections.<String, String>emptyMap());
        dispatchersByPattern.clear();
        // Filtre permettant d'imprimer une page en PDF
        dispatchersByServlet.put("contentServlet", Collections.singletonList(DispatcherType.REQUEST));
        dispatchersByServlet.put("rubriqueServlet", Collections.singletonList(DispatcherType.REQUEST));
        ContextLoaderListener.ajouterFilter("pdfFilter", PDFFilter.class, dispatchersByPattern, dispatchersByServlet, Collections.<String, String>emptyMap());
        dispatchersByServlet.clear();
        // initialise le front office bean, calcule le code rubrique de la page
        dispatchersByPattern.put("*.jsp", Arrays.asList(DispatcherType.REQUEST, DispatcherType.FORWARD));
        dispatchersByServlet.put(ERROR_SERVLET_NAME, Collections.singletonList(DispatcherType.ERROR));
        ContextLoaderListener.ajouterFilter("frontOfficeFilter", FrontOfficeFilter.class, dispatchersByPattern, dispatchersByServlet, Collections.<String, String>emptyMap());
        dispatchersByPattern.clear();
        dispatchersByServlet.clear();
        // traitement des urls wro générant les ressources statiques js et css
        dispatchersByPattern.put("/wro/*", Arrays.asList(DispatcherType.REQUEST, DispatcherType.REQUEST));
        ContextLoaderListener.ajouterFilter("wroFilter", WroFilter.class, dispatchersByPattern, dispatchersByServlet, Collections.<String, String>emptyMap());
        dispatchersByPattern.clear();
        dispatchersByPattern.put("/*", Collections.singletonList(DispatcherType.REQUEST));
        dispatchersByServlet.put(ERROR_SERVLET_NAME, Collections.singletonList(DispatcherType.ERROR));
        ContextLoaderListener.ajouterFilter("wroContextFilter", WroContextFilter.class, dispatchersByPattern, dispatchersByServlet, Collections.<String, String>emptyMap());
        dispatchersByPattern.clear();
        dispatchersByServlet.clear();
        // initialise contexte wro dans un thread local pour la requête courante
        dispatchersByPattern.put("/*", Collections.singletonList(DispatcherType.REQUEST));
        ContextLoaderListener.ajouterFilter("logAppFilter", LogAppFilter.class, dispatchersByPattern, dispatchersByServlet, Collections.<String, String>emptyMap());
        dispatchersByPattern.clear();
    }

    private void initSystemProperties() {
        System.setProperty(CONTENT_URL_PATTERN, DEFAULT_CONTENT_URL_PATTERN);
        System.setProperty(MEDIA_URL_PATTERN, DEFAULT_MEDIA_URL_PATTERN);
        System.setProperty(RUBRIQUE_URL_PATTERN, DEFAULT_RUBRIQUE_URL_PATTERN);
        // chargement de la conf spécifique
        final GenericApplicationContext gctx = new GenericApplicationContext();
        Properties propsFromFile = new Properties();
        try {
            // on place les proprietes "path.properties" et "env.properties"(OLD) dans le system pour etre ensuite relue dans le context spring
            // <bean id="propertyConfigurer" name="propertyPlaceholder"
            // ...
            // <value>${env.properties}</value>
            //  <value>${path.properties}env_xxx.properties</value>
            if (StringUtils.isNotEmpty(WebAppUtil.getConfDir())) {
                // file:///${conf.dir}
                System.setProperty(PATH_ENV_PROPERTIES, "file:///" + WebAppUtil.getConfDir() + File.separator);
                // file:///${conf.dir}/env*.properties
                System.setProperty(ENV_PROPERTIES_FILE, "file:///" + WebAppUtil.getConfDir() + File.separator + ENV_ETOILE_PROPERTIES_FILE);
            } else {
                // classpath*
                System.setProperty(PATH_ENV_PROPERTIES, "classpath*:");
                // classpath*:env*.properties
                System.setProperty(ENV_PROPERTIES_FILE, "classpath*:" + ENV_ETOILE_PROPERTIES_FILE);
            }
            Resource[] resources = gctx.getResources(System.getProperty(ENV_PROPERTIES_FILE));
            // si aucune resource env*.properties trouvée on charge le jtf du classpath
            if (resources.length == 0) {
                resources = gctx.getResources("classpath*:" + JTF_PROPERTIES_FILE);
            }
            if (resources.length > 0) {
                final PropertiesLoader pl = new PropertiesLoader();
                pl.setLocations(resources);
                propsFromFile = pl.loadProperties();
            }
        } catch (final IOException e) {
            LOG.error("an error occured getting the project's resources", e);
        }
        for (final String prop : propsFromFile.stringPropertyNames()) {
            System.setProperty(prop, propsFromFile.getProperty(prop));
        }
        setPropertiesNonSurchargeable();
    }

    /**
     * Ces propriétés sont système et ne doivent pas être surchargés par les properties
     */
    private void setPropertiesNonSurchargeable() {
        final String serverPath = StringUtils.endsWith(context.getRealPath(StringUtils.EMPTY), "/") ? context.getRealPath(StringUtils.EMPTY) : context.getRealPath(StringUtils.EMPTY) + "/";
        System.setProperty(WebAppUtil.PROP_SERVER_HTML_PATH, serverPath);
    }
}
