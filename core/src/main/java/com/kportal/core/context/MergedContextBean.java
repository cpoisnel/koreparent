package com.kportal.core.context;

public interface MergedContextBean {

    String getIdBeanToMerge();

    String getIdExtensionToMerge();

    void setIdBeanToMerge(String idBeanToMerge);

    void setIdExtensionToMerge(String idExtensionToMerge);

    void merge() throws Exception;

    boolean isMandatory();
}
