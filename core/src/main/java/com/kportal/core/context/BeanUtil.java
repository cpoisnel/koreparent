package com.kportal.core.context;

import org.apache.commons.lang3.StringUtils;

public class BeanUtil {

    private static final String SEPARATOR = "#";

    public static String getBeanKey(String idBean, String idExtension) {
        return idBean + SEPARATOR + idExtension;
    }

    public static String getIdExtensionFromKey(String key) {
        return StringUtils.substringAfter(key, SEPARATOR);
    }

    public static String getIdBeanFromKey(String key) {
        return StringUtils.substringBefore(key, SEPARATOR);
    }
}
