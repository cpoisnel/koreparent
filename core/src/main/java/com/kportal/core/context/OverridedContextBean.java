package com.kportal.core.context;

public interface OverridedContextBean {

    String getIdBean();

    void setIdBean(String idbean);

    String getIdExtension();

    void setIdExtension(String idExtension);

    String getIdBeanToOverride();

    void setIdBeanToOverride(String idBean);

    String getIdExtensionToOverride();

    void setIdExtensionToOverride(String idExtension);
}
