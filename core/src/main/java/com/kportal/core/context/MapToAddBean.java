package com.kportal.core.context;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.jsbsoft.jtf.core.ApplicationContextManager;

public class MapToAddBean implements MergedContextBean {

    boolean mandatory = false;

    String idBeanToMerge = "";

    String idExtensionToMerge = "";

    String mapToMerge = "";

    Map<Object, Object> add = new HashMap<>();

    @Override
    public String getIdBeanToMerge() {
        return idBeanToMerge;
    }

    @Override
    public void setIdBeanToMerge(final String idBeanToMerge) {
        this.idBeanToMerge = idBeanToMerge;
    }

    @Override
    public String getIdExtensionToMerge() {
        return idExtensionToMerge;
    }

    @Override
    public void setIdExtensionToMerge(final String idExtensionToMerge) {
        this.idExtensionToMerge = idExtensionToMerge;
    }

    @Override
    public void merge() throws Exception {
        getMap().putAll(add);
    }

    public void setAdd(final Map<Object, Object> add) {
        this.add = add;
    }

    @SuppressWarnings("unchecked")
    private Map<Object, Object> getMap() throws Exception {
        final Object beanToMerge = ApplicationContextManager.getBean(idExtensionToMerge, idBeanToMerge);
        if (beanToMerge instanceof Map<?, ?>) {
            return (Map<Object, Object>) beanToMerge;
        }
        final String methodName = "get" + mapToMerge.substring(0, 1).toUpperCase() + mapToMerge.substring(1);
        final Class<?> noparams[] = {};
        final Object noargs[] = {};
        final Method method = beanToMerge.getClass().getMethod(methodName, noparams);
        return (Map<Object, Object>) method.invoke(beanToMerge, noargs);
    }

    @Override
    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(final boolean mandatory) {
        this.mandatory = mandatory;
    }

    public void setMapToMerge(final String mapToMerge) {
        this.mapToMerge = mapToMerge;
    }
}
