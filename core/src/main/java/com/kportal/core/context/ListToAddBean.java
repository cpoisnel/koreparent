package com.kportal.core.context;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.jsbsoft.jtf.core.ApplicationContextManager;

public class ListToAddBean implements MergedContextBean {

    boolean mandatory = false;

    String idBeanToMerge = "";

    String idExtensionToMerge = "";

    String listToMerge = "";

    List<Object> add = new ArrayList<>();

    @Override
    public String getIdBeanToMerge() {
        return idBeanToMerge;
    }

    @Override
    public void setIdBeanToMerge(final String idBeanToMerge) {
        this.idBeanToMerge = idBeanToMerge;
    }

    @Override
    public String getIdExtensionToMerge() {
        return idExtensionToMerge;
    }

    @Override
    public void setIdExtensionToMerge(final String idExtensionToMerge) {
        this.idExtensionToMerge = idExtensionToMerge;
    }

    public String getListToMerge() {
        return listToMerge;
    }

    public void setListToMerge(final String listNameToMerge) {
        this.listToMerge = listNameToMerge;
    }

    public void setAdd(final List<Object> add) {
        this.add = add;
    }

    @Override
    public void merge() throws Exception {
        getList().addAll(add);
    }

    private List<Object> getList() throws Exception {
        final Object beanToMerge = ApplicationContextManager.getBean(idExtensionToMerge, idBeanToMerge);
        if (beanToMerge instanceof List<?>) {
            return (List<Object>) beanToMerge;
        }
        final String methodName = "get" + listToMerge.substring(0, 1).toUpperCase() + listToMerge.substring(1);
        final Class<?> noparams[] = {};
        final Object noargs[] = {};
        final Method method = beanToMerge.getClass().getMethod(methodName, noparams);
        return (List<Object>) method.invoke(beanToMerge, noargs);
    }

    @Override
    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(final boolean mandatory) {
        this.mandatory = mandatory;
    }
}
