package com.kportal.core.webapp;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.PropertyHelper;
import com.kportal.core.logging.LogHelper;
import com.univ.utils.ContexteUtil;

public class WebAppUtil {

    private static final Logger LOG = LoggerFactory.getLogger(WebAppUtil.class);

    //  Propriétés non surchargeables via des properties
    public static final String PROP_SERVER_HTML_PATH = "server.html_path";

    public static final String SG_PATH = "/servlet/com.jsbsoft.jtf.core.SG";

    public static final String CONNEXION_BO = SG_PATH + "?PROC=IDENTIFICATION&ACTION=CONNECTER";

    public static final String CONNEXION_FO = SG_PATH + "?PROC=IDENTIFICATION_FRONT&ACTION=CONNECTER";

    public final static String EXTERNAL_HTML_DIRECTORY = "html";

    private static final String CONF_DIR = "conf.dir";

    private static final String STORAGE_DIR = "storage.dir";

    /**
     * Argument settable au démarrage de la jvm pour spécifier un chemin spécifique à la configuration des sites.
     */
    private static final String SITES_PATH = "/sites/";

    private static final String WEB_INF = "/WEB-INF/";

    private static final String ADMIN_PATH = "/adminsite/";

    private static final String LIB_PATH = "/WEB-INF/lib/";

    private static final String SQL_PATH = "/WEB-INF/conf/sql/";

    private static final String CLASS_PATH = "/WEB-INF/classes/";

    /**
     * Propriétés surchargeables via les properties de l'application
     */
    private static final String PROP_EXTENSIONS_PATH = "extensions.path";

    private static final String DEFAULT_EXTENSIONS_PATH = "/extensions/";

    private static final String PROP_SAUVEGARDE_PATH = "sauvegarde.path";

    private static final String DEFAULT_SAUVEGARDE_PATH = "save";

    private static final String PROP_WORK_DEFAULTDIR = "work.defaultdir";

    private static final String DEFAULT_WORK_DEFAULTDIR = "tmp";

    private static final String PROP_UPLOAD_DEFAULTDIR = "upload.defaultdir";

        private static final String PROP_CONNECTEURS_SESSION_PATH = "connecteurs.session_path";

    private static final String DEFAULT_CONNECTEURS_SESSION_PATH = "sessions";

    private static final String PROP_XSL_PATH = "transformation.xsl_path";

    private static final String DEFAULT_XSL_PATH = "/WEB-INF/xsl";

    private static final String PROP_IMPORT_BATCH_SOURCE = "import.batch.source";

    private static final String DEFAULT_IMPORT_BATCH_SOURCE = "tmp/import";

    private static final String DEFAULT_CONF_PATH = "/WEB-INF/conf/";

    private static final String DEFAULT_FICHIERS_UAS = "/uas/";

    private static final String PROP_DATASTORE_SERVER = "datastore.default.server";

    /**
     * Calcule le chemin du répertoire conf de l'application.
     *
     * @return Le chemin vers l'application ou {@code null} si il n'est pas défini dans les paramètres de la jvm.
     */
    public static String getConfDir() {
        return System.getProperty(CONF_DIR);
    }

    /**
     * Calcule le chemin du répertoire storage de l'application.
     *
     * @return Le chemin vers l'application ou {@code null} si il n'est pas défini dans les paramètres de la jvm.
     */
    public static String getStorageDir() {
        String storagePath = System.getProperty(STORAGE_DIR);
        if (StringUtils.isBlank(storagePath)) {
            LOG.debug("Propriété \"storage.dir\" non renseignée : utilisation de getWebInfPath.");
            storagePath = WebAppUtil.getWebInfPath();
        }
        File storageDir = new File(storagePath);
        if (!storageDir.exists()) {
            LOG.debug("Tentative de création du répertoire " + storagePath);
            if (!storageDir.mkdirs()) {
                return StringUtils.EMPTY;
            }
        }
        String finalPath = storageDir.getAbsolutePath();
        if (!finalPath.endsWith(File.separator)) {
            finalPath += File.separator;
        }
        LOG.debug("StorageDir construit : " + finalPath);
        return finalPath;
    }

    /**
     * Calcule le chemin absolu vers la racine de l'application
     *
     * @return par exemple : /home/ksup
     */
    public static String getAbsolutePath() {
        return System.getProperty(PROP_SERVER_HTML_PATH);
    }

    /**
     * Calcule le chemin vers le dossier WEB-INF de l'application
     *
     * @return par exemple /home/ksup/WEB-INF si l'application est déployée dans /home/ksup
     */
    public static String getWebInfPath() {
        return getAbsolutePath() + WEB_INF;
    }

    /**
     * Calcule le chemin vers le dossier WEB-INF/classes de l'application
     *
     * @return par exemple /home/ksup/WEB-INF/classes/ si l'application est déployée dans /home/ksup
     */
    public static String getClassPath() {
        return getAbsolutePath() + CLASS_PATH;
    }

    /**
     * Calcule le chemin vers le dossier WEB-INF/lib de l'application
     *
     * @return par exemple /home/ksup/WEB-INF/lib/ si l'application est déployée dans /home/ksup
     */
    public static String getLibPath() {
        return getAbsolutePath() + LIB_PATH;
    }

    /**
     * Calcule le chemin vers le dossier /WEB-INF/conf/sql/ de l'application
     *
     * @return par exemple /home/ksup/WEB-INF/conf/sql/ si l'application est déployée dans /home/ksup
     */
    public static String getSQLPath() {
        return getAbsolutePath() + SQL_PATH + PropertyHelper.getCoreProperty(PROP_DATASTORE_SERVER) + "/";
    }

    /**
     * Calcule le chemin absolu vers l'adminsite
     *
     * @return par exemple /home/ksup/adminsite si l'application est déployée dans /home/ksup
     */
    public static String getAbsoluteAdminsitePath() {
        return getAbsolutePath() + getRelativeAdminsitePath();
    }

    /**
     * Calcule le chemin relatif vers l'adminsite
     *
     * @return par défaut /adminsite
     */
    public static String getRelativeAdminsitePath() {
        return ADMIN_PATH;
    }

    /**
     * calcule le chemin relatif vers le dossier des extensions de l'applications
     *
     * @return par défaut /extensions
     */
    public static String getRelativeExtensionsPath() {
        return StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(PROP_EXTENSIONS_PATH), DEFAULT_EXTENSIONS_PATH);
    }

    /**
     * Calcule le chemin relatif vers le dossier d'une extension.
     *
     * @param dossierExtension le dossier/id de l'extension
     * @return le chemin relatif vers cette extension.par exemple /extensions/actualite
     */
    public static String getRelativeExtensionPath(final String dossierExtension) {
        return getRelativeExtensionsPath() + dossierExtension;
    }

    /**
     * Calcule le chemin absolu vers les extensions. Le chemin défini est forcément sous le chemin absolu de l'application défini par {@link WebAppUtil#getAbsolutePath()} Il peut
     * ensuite être surchargé via la propriété {@linkplain WebAppUtil#PROP_EXTENSIONS_PATH}. Par défaut, il est défini via la propriété
     * {@linkplain WebAppUtil#DEFAULT_EXTENSIONS_PATH}
     *
     * @return le chemin vers le dossier des extensions. Par exemple : /home/ksup/extensions/
     */
    public static String getAbsoluteExtensionsPath() {
        return getAbsolutePath() + getRelativeExtensionsPath();
    }

    /**
     * test si le répertoire externe de l'extension existe
     *
     * @param dossierExtension l'id ou le nom de dossier de l'extension
     * @return true si le dossier existe sous le répertoire /extensions
     */
    private static boolean isExtensionExterne(final String dossierExtension) {
        return new File(getAbsoluteExtensionsPath() + dossierExtension).exists();
    }

    /**
     * Retourne le dossier WEB-INF d'une extension. seul le chemin vers le dossier des extensions peut être surchargé.
     *
     * @param dossierExtension l'id ou le nom de dossier de l'extension
     * @return le chemin absolu vers WEB-INF/ de l'extension. Exemple : /home/ksup/extensions/monExtension/WEB-INF/
     */
    public static String getExtensionWebInfPath(final String dossierExtension) {
        if (!isExtensionExterne(dossierExtension)) {
            LOG.debug("Le dossier fournit n'es pas le conteneur d'une extension externe -> utilisation de getWebInfPath");
            return getWebInfPath();
        }
        return getAbsoluteExtensionsPath() + dossierExtension + WEB_INF;
    }

    /**
     * Retourne le dossier classpath d'une extension. seul le chemin vers le dossier des extensions peut être surchargé.
     *
     * @param dossierExtension l'id ou le nom de dossier de l'extension
     * @return le chemin absolu vers le classpath de l'extension. Exemple : /home/ksup/extensions/monExtension/WEB-INF/classes/
     */
    public static String getExtensionClassPath(final String dossierExtension) {
        if (!isExtensionExterne(dossierExtension)) {
            LOG.debug("Le dossier fournit n'es pas le conteneur d'une extension externe -> utilisation de getClassPath");
            return getClassPath();
        }
        return getAbsoluteExtensionsPath() + dossierExtension + CLASS_PATH;
    }

    /**
     * Retourne le dossier des librairies d'une extension. seul le chemin vers le dossier des extensions peut être surchargé. Il permet de charger les différents jar de
     * l'extension.
     *
     * @param dossierExtension l'id ou le nom de dossier de l'extension
     * @return le chemin absolu vers les libs de l'extension. Exemple : /home/ksup/extensions/monExtension/WEB-INF/lib/
     */
    public static String getExtensionLibPath(final String dossierExtension) {
        if (!isExtensionExterne(dossierExtension)) {
            LOG.debug("Le dossier fournit n'es pas le conteneur d'une extension externe -> utilisation de getLibPath");
            return getLibPath();
        }
        return getAbsoluteExtensionsPath() + dossierExtension + LIB_PATH;
    }

    /**
     * Retourne le dossier contenant la configuration SQL d'une extension. seul le chemin vers le dossier des extensions peut être surchargé. Cette méthode permet à
     * l'initialisation de l'application de créer les tables de l'extension si elles n'existent pas.
     *
     * @param dossierExtension l'id ou le nom de dossier de l'extension
     * @return le chemin absolu vers la conf SQL de l'extension. Exemple : /home/ksup/extensions/monExtension/WEB-INF/conf/sql/
     */
    public static String getExtensionSQLPath(final String dossierExtension) {
        if (!isExtensionExterne(dossierExtension)) {
            LOG.debug("Le dossier fournit n'es pas le conteneur d'une extension externe -> utilisation de getSQLPath");
            return getSQLPath();
        }
        return getAbsoluteExtensionsPath() + dossierExtension + SQL_PATH + PropertyHelper.getCoreProperty(PROP_DATASTORE_SERVER) + "/";
    }

    /**
     * Calcule le chemin d'upload des fichiers
     *
     * @return
     */
    public static String getUploadDefaultPath() {
        final String res = StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(PROP_UPLOAD_DEFAULTDIR), getStorageDir() + DEFAULT_WORK_DEFAULTDIR);
        if (StringUtils.isNotEmpty(res) && !new File(res).exists()) {
            LOG.debug("Tentative de création de création du répertoire et éventuels parents pour \"" + res + "\"");
            new File(res).mkdirs();
        }
        return res;
    }

    /**
     * Calcule le chemin de configuration des sites de l'application. Si le dossier de conf est spécifié en argument de la jvm, il est calculé à partir de celui ci. Sinon, il se
     * trouve sous WEB-INF/sites. Il n'y a pas de possibilité de le surchargé.
     *
     * @return
     */
    public static String getConfigurationSitesPath() {
        String cheminFichiersConf = getConfDir();
        if (StringUtils.isNotBlank(cheminFichiersConf)) {
            LOG.debug("Le conf.dir est renseigné -> concaténation avec le SITES_PATH");
            cheminFichiersConf += SITES_PATH;
        } else {
            LOG.debug("Le conf.dir n'est pas renseigné -> concaténation de getAbsolutePath avec le SITES_PATH");
            cheminFichiersConf = getAbsolutePath() + DEFAULT_CONF_PATH + SITES_PATH;
        }
        final File fichierConfSite = new File(cheminFichiersConf);
        if (!fichierConfSite.exists()) {
            LOG.debug("Tentative de création du répertoire \"" + fichierConfSite + "\"");
            fichierConfSite.mkdirs();
        }
        return cheminFichiersConf;
    }

    public static String getAbsoluteFichiersSitesPath() {
        final String res = getConfigurationSitesPath() + getRelatifFichiersSitesPath();
        if (StringUtils.isNotEmpty(res) && !new File(res).exists()) {
            LOG.debug("Tentative de création du répertoire \"" + res + "\"");
            new File(res).mkdirs();
        }
        return res;
    }

    public static String getRelatifFichiersSitesPath() {
        return DEFAULT_FICHIERS_UAS;
    }

    public static String getWorkDefaultPath() {
        final String res = StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(PROP_WORK_DEFAULTDIR), getAbsolutePath() + "/" + DEFAULT_WORK_DEFAULTDIR);
        if (StringUtils.isNotEmpty(res) && !new File(res).exists()) {
            LOG.debug("Tentative de création du répertoire \"" + res + "\"");
            new File(res).mkdirs();
        }
        return res;
    }

    public static String getLogsPath() {
        return StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(LogHelper.PROP_LOG_PATH), getStorageDir() + LogHelper.DEFAULT_LOG_PATH);
    }

    public static String getSessionsPath() {
        final String res = StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(PROP_CONNECTEURS_SESSION_PATH), getStorageDir() + DEFAULT_CONNECTEURS_SESSION_PATH);
        if (StringUtils.isNotEmpty(res) && !new File(res).exists()) {
            LOG.debug("Tentative de création du répertoire \"" + res + "\"");
            new File(res).mkdirs();
        }
        return res;
    }

    public static String getXslPath() {
        return StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(PROP_XSL_PATH), getAbsolutePath() + DEFAULT_XSL_PATH);
    }

    public static String getSauvegardePath() {
        final String res = StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(PROP_SAUVEGARDE_PATH), getStorageDir() + DEFAULT_SAUVEGARDE_PATH);
        if (StringUtils.isNotEmpty(res) && !new File(res).exists()) {
            LOG.debug("Tentative de création du répertoire \"" + res + "\"");
            new File(res).mkdirs();
        }
        return res;
    }

    public static String getImportSourcePath() {
        final String res = StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(PROP_IMPORT_BATCH_SOURCE), getStorageDir() + DEFAULT_IMPORT_BATCH_SOURCE);
        if (StringUtils.isNotEmpty(res) && !new File(res).exists()) {
            LOG.debug("Tentative de création du répertoire \"" + res + "\"");
            new File(res).mkdirs();
        }
        return res;
    }

    public static String getDownloadPrivatePath() {
        String idSession = "";
        if (ContexteUtil.getContexteUniv() != null) {
            LOG.debug("Récupération de la session depuis ContexteUtil.getContexteUniv().getKsession()");
            idSession = ContexteUtil.getContexteUniv().getKsession();
        }
        final String res = getWorkDefaultPath() + (StringUtils.isNotEmpty(idSession) ? File.separator + idSession : "");
        if (StringUtils.isNotEmpty(res) && !new File(res).exists()) {
            LOG.debug("Tentative de création du répertoire \"" + res + "\"");
            new File(res).mkdirs();
        }
        return res;
    }

    public static String getExternalHtmlPath() {
        if (StringUtils.isNotBlank(getStorageDir())) {
            LOG.debug("Le storage.dir est renseigné -> concaténation de getStorageDir et EXTERNAL_HTML_DIRECTORY");
            return String.format("%s/%s", getStorageDir(), EXTERNAL_HTML_DIRECTORY);
        } else {
            LOG.debug("Le storage.dir n'est pas renseigné -> concaténation de getAbsolutePath et EXTERNAL_HTML_DIRECTORY");
            return String.format("%s/%s/%s", getAbsolutePath(), "storage", EXTERNAL_HTML_DIRECTORY);
        }
    }
}
