package com.kportal.core.autorisation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.kportal.extension.ExtensionHelper;

/**
 * The Class Permission. simple bean pour stocker le code technique de la permission et une action correspondante c'est juste une sous partie de la classe PermissionBean
 */
public class Permission implements Serializable {

    private static final long serialVersionUID = 2078016352608152071L;

    /** The id. */
    public String id = "";

    /** The code. */
    public String code = "";

    /** The action. */
    public List<ActionPermission> actions = new ArrayList<>();

    /** The libelle. */
    public String libelle = "";

    /** type de module pour l'organisation de l'affichage dans l'écran des rôles */
    public Type type = Type.MODULE;

    public String idModule = "";

    public String idExtension = "";

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the actions.
     *
     * @return the actions
     */
    public List<ActionPermission> getActions() {
        return actions;
    }

    /**
     * Sets the actions.
     *
     * @param actions
     *            the new actions
     */
    public void setActions(List<ActionPermission> actions) {
        this.actions = actions;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets the libelle.
     *
     * @param libelle
     *            the new libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(String id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getIdExtension() {
        return idExtension;
    }

    public void setIdExtension(String idExtension) {
        this.idExtension = idExtension;
    }

    public String getLibelleAffichable() {
        return ExtensionHelper.getMessage(idExtension, getLibelle());
    }

    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    /**
     * Enum pour gérer les types de permission utiles pour construire l'écran de saisie d'un rôle
     */
    public enum Type {
        CONTENU("CTU"), CONTRIBUTION("CTN"), DIFFUSION("DSI"), COLLABORATIF("COL"), ADMINISTRATION("ADM"), SCRIPT("SCR"), MODULE("MOD");

        private String defaultIdPermission = "";

        Type(String id) {
            defaultIdPermission = id;
        }

        public String getDefaultIdPermission() {
            return defaultIdPermission;
        }
    }
}
