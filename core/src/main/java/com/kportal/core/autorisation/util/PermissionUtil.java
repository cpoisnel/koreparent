package com.kportal.core.autorisation.util;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.autorisation.ActionPermission;
import com.kportal.core.autorisation.Permission;
import com.kportal.extension.module.IModule;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;

public class PermissionUtil {

    /**
     * Retourne la permission {@link PermissionBean} à partir d'un module {@link IModule} d'un code de permission et d'un code d'action
     */
    public static PermissionBean getPermissionBean(final IModule module, final String codePermission, final String codeAction) {
        if (module != null) {
            for (final Permission permission : module.getPermissions()) {
                if (permission.getCode().equals(codePermission)) {
                    for (final ActionPermission action : permission.getActions()) {
                        if (action.getCode().equals(codeAction)) {
                            return getPermissionBean(permission, action);
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Construit un {@link PermissionBean} à partir d'un {@link Permission} et d'un {@link ActionPermission} Un {@link Permission} peut contenir N action, alors qu'un
     * {@link PermissionBean} en contient qu'une seule. Il faut donc préciser l'action pour pouvoir créer un {@link PermissionBean}
     *
     * @param permission
     * @param action
     * @return
     */
    public static PermissionBean getPermissionBean(final Permission permission, final ActionPermission action) {
        return new PermissionBean(StringUtils.upperCase(permission.getId()), permission.getCode(), action.getCode());
    }

    /**
     * Retourne les {@link PermissionBean} disponible a partir d'un objet Permission.
     *
     * @param permission
     * @return
     */
    public static Collection<PermissionBean> getPermissionsBeans(final Permission permission) {
        final Collection<PermissionBean> permissions = new ArrayList<>();
        for (final ActionPermission action : permission.getActions()) {
            permissions.add(getPermissionBean(permission, action));
        }
        return permissions;
    }

    /**
     * Méthode qui permet de tester le périmètre de modification d'un objet sur une rubrique : si le droit en modification renvoit faux on va regarder le droit en création.
     *
     * @param autorisations
     *            the autorisations
     * @param sPermission
     *            the s permission
     * @param codeRubrique
     *            the code rubrique
     *
     * @return true, if controler permission
     *
     */
    public static boolean controlerPermissionRubrique(final AutorisationBean autorisations, final String sPermission, final String codeRubrique) {
        PermissionBean permissionCourante = new PermissionBean(sPermission);
        final Perimetre perimetre = new Perimetre("", codeRubrique, "", "", "");
        boolean res = autorisations.possedePermissionPartielleSurPerimetre(permissionCourante, perimetre);
        if (!res && "FICHE".equals(permissionCourante.getType()) && "M".equals(permissionCourante.getAction())) {
            permissionCourante = new PermissionBean(permissionCourante.getType(), permissionCourante.getObjet(), "C");
            if (autorisations.possedePermissionPartielleSurPerimetre(permissionCourante, perimetre)) {
                res = true;
            }
        }
        return res;
    }
}
