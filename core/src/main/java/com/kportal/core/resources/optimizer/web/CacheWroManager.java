package com.kportal.core.resources.optimizer.web;

import com.kportal.extension.module.AbstractBeanManager;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

import ro.isdc.wro.http.support.ServletContextAttributeHelper;

public class CacheWroManager extends AbstractBeanManager {

    @Override
    public void refresh() {
        ContexteUniv ctx = ContexteUtil.getContexteUniv();
        if (ctx != null) {
            ServletContextAttributeHelper helper = new ServletContextAttributeHelper(ctx.getServletContext());
            helper.getWroConfiguration().reloadCache();
            helper.getWroConfiguration().reloadModel();
        }
    }
}
