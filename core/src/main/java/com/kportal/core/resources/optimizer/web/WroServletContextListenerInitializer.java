package com.kportal.core.resources.optimizer.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.web.context.ServletContextAware;

import com.kportal.core.resources.optimizer.manager.CustomWroManagerFactory;

import ro.isdc.wro.config.factory.PropertyWroConfigurationFactory;
import ro.isdc.wro.http.WroServletContextListener;

/**
 * Initialize {@link ro.isdc.wro.http.WroServletContextListener} with IoC.
 */
public class WroServletContextListenerInitializer implements ServletContextAware, DisposableBean {

    private ServletContext servletContext;

    private WroServletContextListener wroServletContextListener;

    private CustomWroManagerFactory wroManagerFactory;

    public void setWroManagerFactory(CustomWroManagerFactory wroManagerFactory) {
        this.wroManagerFactory = wroManagerFactory;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
        PropertyWroConfigurationFactory propertyWroConfigurationFactory = new PropertyWroConfigurationFactory(wroManagerFactory.getProperties());
        wroServletContextListener = new WroServletContextListener();
        wroServletContextListener.setConfiguration(propertyWroConfigurationFactory.create());
        wroServletContextListener.setManagerFactory(wroManagerFactory);
        wroServletContextListener.contextInitialized(new ServletContextEvent(servletContext));
    }

    @Override
    public void destroy() {
        wroServletContextListener.contextDestroyed(new ServletContextEvent(servletContext));
    }
}
