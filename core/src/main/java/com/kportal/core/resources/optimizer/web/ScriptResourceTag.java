package com.kportal.core.resources.optimizer.web;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.univ.utils.URLResolver;

/**
 * Custom tag JSP permettant de charger le code d'un groupe Javascript.
 */
public class ScriptResourceTag extends TagSupport {

    /**
     *
     */
    private static final long serialVersionUID = -3248471198390795965L;

    private static final String DEFAULT_TYPE = "text/javascript";

    /**
     * Nom du groupe.
     */
    private String group;

    /**
     *
     */
    private String locale;

    /**
     * Type de la ressource (par défaut text/javascript)
     */
    private String type = DEFAULT_TYPE;

    @Override
    public int doStartTag() throws JspException {
        try {
            final Writer writer = pageContext.getOut();
            for (final String url : ResourceUtils.getResourceUrl(pageContext.getServletContext(), (HttpServletRequest) pageContext.getRequest(), (HttpServletResponse) pageContext.getResponse(), group, type, locale)) {
                writer.write("<script type=\"");
                writer.write(type);
                // Calcule l'URL de la ressource.
                writer.write("\" src=\"");
                writer.write(URLResolver.getRequestBase((HttpServletRequest) pageContext.getRequest()));
                writer.write(url);
                writer.write("\"></script>");
            }
        } catch (final IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }

    @Override
    public void release() {
        group = null;
        type = DEFAULT_TYPE;
    }

    public void setGroup(final String group) {
        this.group = group;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(final String locale) {
        this.locale = locale;
    }
}
