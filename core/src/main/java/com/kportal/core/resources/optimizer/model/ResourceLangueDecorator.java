package com.kportal.core.resources.optimizer.model;

import java.util.Locale;

import com.kportal.extension.IExtension;

import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.ResourceType;

/**
 * Décorateur permettant de rajouter une locale à une ressource wro
 *
 * @author olivier.camon
 *
 */
public class ResourceLangueDecorator extends Resource {

    private final Resource resource;

    private String idExtension;

    private Locale locale;

    private ResourceLangueDecorator(final IExtension extension, final Locale locale, final String uri, final ResourceType type) {
        this.resource = Resource.create(uri, type);
        this.idExtension = extension.getId();
        this.locale = locale;
    }

    /**
     * Factory method for Resource creation. A factory method is preferred instead of public constructor, in order to avoid possibilities for clients to extend Resource class.
     *
     * @return an instance of {@link Resource} object.
     */
    public static ResourceLangueDecorator create(final IExtension extension, final Locale locale, final String uri, final ResourceType type) {
        return new ResourceLangueDecorator(extension, locale, uri, type);
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getIdExtension() {
        return idExtension;
    }

    public void setIdExtension(String idExtension) {
        this.idExtension = idExtension;
    }

    /**
     * @param type
     *            the type to set
     */
    @Override
    public void setType(final ResourceType type) {
        resource.setType(type);
    }

    /**
     * @param uri
     *            the uri to set
     */
    @Override
    public void setUri(final String uri) {
        resource.setUri(uri);
    }

    /**
     * @return the type
     */
    @Override
    public ResourceType getType() {
        return resource.getType();
    }

    /**
     * @return the uri associated with this resource.
     */
    @Override
    public String getUri() {
        return resource.getUri();
    }

    /**
     * @return the minimize
     */
    @Override
    public boolean isMinimize() {
        return resource.isMinimize();
    }

    /**
     * @param minimize
     *            the minimize to set
     */
    @Override
    public void setMinimize(final boolean minimize) {
        resource.isMinimize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        return resource.equals(obj);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return resource.hashCode();
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return resource.toString();
    }
}
