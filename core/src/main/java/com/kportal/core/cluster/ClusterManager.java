package com.kportal.core.cluster;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jsbsoft.jtf.core.ApplicationContextManager;

import fr.kosmos.cluster.api.Cluster;

/**
 * The Class ClusterManagerFactory. !!! CETTE CLASSE NE DOIT PAS INSTANCIER UN LOGGER STATIQUE CAR ELLE CHARGE LA JVM ROUTE UTILE POUR DETERMINER LE CHEMIN DU FICHIER DE LOG !!!
 */
@Component
public class ClusterManager {

    protected static final String ID_BEAN = "clusterManager";

    @Autowired
    private Cluster cluster;

    public static ClusterManager getInstance() {
        return ApplicationContextManager.getCoreContextBean(ID_BEAN, ClusterManager.class);
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster manager) {
        this.cluster = manager;
    }
}
