package com.kportal.core.cluster;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.PropertyConfigurer;

import fr.kosmos.cluster.api.MessageHandler;

public class ClusterHelper {

    /**
     * Parametre représentant le time to live du cluster. <br/>
     * Ce parametre permet de définir la réstriction réseau appliquée au clustering, à savoir: <br/>
     * <table>
     * <tr>
     * <td>TTL</td>
     * <td>Description</td>
     * </tr>
     * <tr>
     * <td>0</td>
     * <td>Restriction à la machine (ne sort pas de 127.0.0.1)</td>
     * </tr>
     * <tr>
     * <td>1</td>
     * <td>Restriction au même sous-réseau (transféré par le routeur)</td>
     * </tr>
     * <tr>
     * <td>32</td>
     * <td>Restriction au même site, organisation ou département</td>
     * </tr>
     * <tr>
     * <td>64</td>
     * <td>Restriction à la même région</td>
     * </tr>
     * <tr>
     * <td>128</td>
     * <td>Restriction au même continent</td>
     * </tr>
     * <tr>
     * <td>255</td>
     * <td>Pas de restriction</td>
     * </tr>
     * </table>
     */
    public static final String CLUSTER_TTL = "kdecole.cluster.multicast.ttl";

    /**
     * Retourne le singleton
     *
     * @return ClusterManager
     */
    public static ClusterManager getClusterManager() {
        return ClusterManager.getInstance();
    }

    /**
     * Retourne la propiété jvmRoute du cluster courant
     *
     * @return la jvmRoute courante
     */
    public static String getCurrentJvmRoute() {
        int ttl = 0;
        String ttlProperty = PropertyConfigurer.getProperty(CLUSTER_TTL);
        if (StringUtils.isNotBlank(ttlProperty) && StringUtils.isNumeric(ttlProperty)) {
            ttl = Integer.valueOf(ttlProperty);
        }
        if (ttl > 0 || getClusterManager().getCluster().getMembers().size() > 1) {
            return getClusterManager().getCluster().getName();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Execute un rafraichissement du singleton via la méthode handleMessage sur le noeud courant
     * puis envoie un message aux autres noeuds
     *
     * @param service le singleton
     * @param message le message
     */
    public static void refresh(MessageHandler<Serializable> service, Serializable message) {
        service.handleMessage(message);
        getClusterManager().getCluster().sendMessage(service.getClass(), message);
    }
}
