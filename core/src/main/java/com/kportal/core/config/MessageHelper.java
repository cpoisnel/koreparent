package com.kportal.core.config;

import java.util.Locale;
import java.util.Properties;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

public class MessageHelper {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(MessageHelper.class);

    public static String getMessage(String idCtx, final InfosSite infosSite, Locale locale, final String key, final boolean searchCore) {
        String res = "";
        if (StringUtils.isEmpty(idCtx)) {
            idCtx = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
        }
        if (locale == null || !LangueUtil.isActiveLocale(locale)) {
            locale = LangueUtil.getDefaultLocale();
        }
        try {
            final IExtension extension = ExtensionHelper.getExtension(idCtx);
            if (extension != null && extension.getId().equals(idCtx)) {
                // on va chercher le libellé associé au site alias ou template
                if (infosSite != null) {
                    // alias
                    if (StringUtils.isNotBlank(infosSite.getAlias())) {
                        res = extension.getMessage(locale, infosSite.getAlias() + "." + key);
                    }
                    // code template
                    if (StringUtils.isEmpty(res) && StringUtils.isNotBlank(infosSite.getCodeTemplate())) {
                        res = extension.getMessage(locale, infosSite.getCodeTemplate() + "." + key);
                    }
                }
                // sinon, le libellé par défaut
                if (StringUtils.isEmpty(res)) {
                    res = extension.getMessage(locale, key);
                }
                if (StringUtils.isEmpty(res)) {
                    if (ApplicationContextManager.DEFAULT_CORE_CONTEXT.equals(idCtx)) {
                        LOG.debug("Chaine " + key + " absente des messages " + locale.getCountry());
                    }
                    // appel recursif sur le core
                    if (!idCtx.equals(ApplicationContextManager.DEFAULT_CORE_CONTEXT) && searchCore) {
                        LOG.debug("Chaine " + key + " absente des messages " + locale.getCountry() + " pour l'extension id=" + idCtx + " recherche dans le core");
                        res = getMessage(ApplicationContextManager.DEFAULT_CORE_CONTEXT, infosSite, locale, key, searchCore);
                    }
                    // appel recursif sur la langue
                    if (StringUtils.isEmpty(res) && !locale.getCountry().equals(LangueUtil.getDefaultLocale().getCountry())) {
                        res = getMessage(idCtx, infosSite, LangueUtil.getDefaultLocale(), key, searchCore);
                    }
                }
            }
            // l'extension n'existe pas ou n'est pas encore chargée
            else {
                res = MessageLoaderUtil.getMessage(idCtx, LangueUtil.getLanguageCountry(locale), key);
                // appel recursif sur le core
                if (StringUtils.isEmpty(res) && !idCtx.equals(ApplicationContextManager.DEFAULT_CORE_CONTEXT) && searchCore) {
                    res = MessageLoaderUtil.getMessage(ApplicationContextManager.DEFAULT_CORE_CONTEXT, LangueUtil.getLanguageCountry(locale), key);
                }
            }
        } catch (final Exception e) {
            // je ne vois pas dans quel cas cette exception est levée...
            LOG.debug("an error occured getting the locale property", e);
        }
        return res;
    }

    public static String getMessage(final String idCtx, final String key) {
        return getMessage(idCtx, getInfosSite(), getLocale(), key, true);
    }

    public static String getMessage(final String idCtx, final Locale locale, final String key) {
        return getMessage(idCtx, getInfosSite(), locale, key, true);
    }

    public static String getExtensionMessage(final String idCtx, final String key) {
        return getMessage(idCtx, getInfosSite(), getLocale(), key, false);
    }

    public static String getExtensionMessage(final String idCtx, final Locale locale, final String key) {
        return getMessage(idCtx, getInfosSite(), locale, key, false);
    }

    public static String getCoreMessage(final String key) {
        return getMessage(ApplicationContextManager.DEFAULT_CORE_CONTEXT, getInfosSite(), getLocale(), key, false);
    }

    public static String getCoreMessage(final Locale locale, final String key) {
        return getMessage(ApplicationContextManager.DEFAULT_CORE_CONTEXT, getInfosSite(), locale, key, false);
    }

    public static Properties getMessages(final String idCtx, final Locale locale) {
        final IExtension extension = ExtensionHelper.getExtension(idCtx) == null ? ExtensionHelper.getCoreExtension() : ExtensionHelper.getExtension(idCtx);
        return extension.getMessages(locale);
    }

    private static InfosSite getInfosSite() {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        if (ctx != null) {
            return ctx.getInfosSite();
        } else {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            return serviceInfosSite.getPrincipalSite();
        }
    }

    private static Locale getLocale() {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        if (ctx != null) {
            return ctx.getLocale();
        } else {
            return LangueUtil.getDefaultLocale();
        }
    }

    public static Locale parseLocaleFromFileName(final String fileName) {
        String localeName = StringUtils.substringAfter(fileName, "_");
        while (localeName.contains("_") && localeName.split("_")[0].length() > 2) {
            localeName = StringUtils.substringAfter(localeName, "_");
        }
        localeName = StringUtils.substringBeforeLast(localeName, ".");
        Locale locale = null;
        try {
            locale = LocaleUtils.toLocale(localeName);
        } catch (final IllegalArgumentException e) {
            LOG.debug("unable to find the locale " + localeName, e);
        }
        return locale;
    }

    /**
     * Checks si la locale trouvée est bien valide
     *
     * @param value
     *            the value
     * @return true, if is valid locale
     */
    public static boolean isValidLocale(final String value) {
        final Locale[] locales = Locale.getAvailableLocales();
        for (final Locale l : locales) {
            if (value.equals(l.toString())) {
                return true;
            }
        }
        return false;
    }
}
