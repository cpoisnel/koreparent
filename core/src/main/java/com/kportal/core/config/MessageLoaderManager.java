package com.kportal.core.config;

import java.io.IOException;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.extension.ExtensionHelper;

public class MessageLoaderManager {

    public static synchronized void reload() throws IOException {
        // on boucle sur toutes les extensions chargees
        for (final String contextName : ExtensionHelper.getExtensionManager().getExtensions().keySet()) {
            for (final MessageLoader loader : ApplicationContextManager.getBeansOfType(contextName, MessageLoader.class).values()) {
                loader.reload();
            }
        }
    }

    public static synchronized void reloadByContext(final String contextName) throws IOException {
        // on verifie que l'extension est bien chargee
        if (ExtensionHelper.getExtensionManager().getExtension(contextName) != null) {
            for (final MessageLoader loader : ApplicationContextManager.getBeansOfType(contextName, MessageLoader.class).values()) {
                loader.reload();
            }
        }
    }
}
