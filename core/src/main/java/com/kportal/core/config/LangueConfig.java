package com.kportal.core.config;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class LangueConfig.
 */
public class LangueConfig {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "langueConfig";

    /** The Constant DEFAULT_PATH. */
    public static final String DEFAULT_PATH = "/adminsite/images/langues/";

    /** The Constant DEFAULT_IMAGE_DRAPEAU. */
    public static final String DEFAULT_IMAGE_DRAPEAU = "drapeau.png";

    /** Logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(LangueConfig.class);

    public int max_langue = 10;

    /** The locales. */
    private Map<Integer, Langue> langues = null;

    /** The locale bo. */
    private int indiceLangueBo = 0;

    /**
     * Initialisation à partir du fichier paramètre.
     *
     */
    public void init() {
        try {
            max_langue = Integer.parseInt(PropertyConfigurer.getProperty("application.max_langue"));
        } catch (final NumberFormatException e) {
            LOGGER.debug("unable to parse the given attribute", e);
        }
        langues = new HashMap<>();
        for (int i = 0; i < max_langue; i++) {
            final String language = PropertyConfigurer.getProperty("langue_" + i + ".language");
            if (StringUtils.isEmpty(language)) {
                continue;
            }
            final String country = StringUtils.defaultIfEmpty(PropertyConfigurer.getProperty("langue_" + i + ".country"), "");
            final String url = StringUtils.defaultIfEmpty(PropertyConfigurer.getProperty("langue_" + i + ".url"), DEFAULT_PATH + language);
            final Langue lb = new Langue(i, new Locale(language, country), url);
            langues.put(i, lb);
        }
        try {
            indiceLangueBo = Integer.parseInt(PropertyConfigurer.getProperty("application.langue_bo"));
        } catch (final NumberFormatException e) {
            LOGGER.debug("unable to parse the given attribute", e);
        }
    }

    /**
     * Gets the nb langues.
     *
     * @return the nb langues
     */
    public int getNbLangues() {
        return langues.size();
    }

    public Map<Integer, Langue> getLangues() {
        return langues;
    }

    public int getIndiceLangueBo() {
        return indiceLangueBo;
    }
}
