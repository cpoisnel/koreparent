package com.kportal.core.config;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.support.PropertiesLoaderSupport;

import com.jsbsoft.jtf.lang.CharEncoding;

public class PropertiesLoader extends PropertiesLoaderSupport {

    public Properties loadProperties() throws IOException {
        setFileEncoding(CharEncoding.DEFAULT);
        setIgnoreResourceNotFound(Boolean.TRUE);
        return mergeProperties();
    }
}
