package com.kportal.core.config;

import com.kportal.extension.ExtensionConfigurer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePropertySource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Pre-load context.
 * <p>Enable Environment variables (Spring)</p>
 *
 * @since CORE-1729
 * @author cpoisnel
 * @see ExtensionConfigurer#initAllApplicationContext(org.springframework.context.ApplicationContext)
 *
 *
 */
public class CoreContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final Logger LOG = LoggerFactory.getLogger(CoreContextInitializer.class);

    protected final String idContext;

    /**
     * Constructor with context ID.
     * @param idContext
     * ID context
     */
    public CoreContextInitializer(final String idContext) {
        super();
        this.idContext = idContext;
    }

    /**
     * Default constructor for 'core'.
     */
    public CoreContextInitializer() {
        this("core");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final ConfigurableApplicationContext applicationContext) {
        LOG.info("Load properties for context: '{}'", idContext);
        final ConfigurableEnvironment environment = applicationContext.getEnvironment();
        final List<Resource> resources = findProperties(applicationContext);
        final MutablePropertySources propertySources = environment.getPropertySources();
        for (final Resource resource : resources) {
            loadPropertyResource(propertySources, resource);
        }
        if (AbstractXmlApplicationContext.class.isAssignableFrom(applicationContext.getClass())) {
            activateXMLValidating((AbstractXmlApplicationContext) applicationContext);
        }
        LOG.debug("All properties loaded for context: '{}'", idContext);
    }

    /**
     * Activate XML Schema for validation (default: xml.validating=true).
     * <p>
     *     Be careful, moving property 'xml.validating' to false is at your peril.
     * </p>
     * @param applicationContext
     */
    protected void activateXMLValidating(final AbstractXmlApplicationContext applicationContext) {
        final boolean xmlValidating = applicationContext.getEnvironment().getProperty("xml.validating", boolean.class, true);
        if (!xmlValidating) {
            LOG.info("XML Schema for validation disabled for context '{}'", idContext);
        }
        applicationContext.setValidating(xmlValidating);
    }

    /**
     * Look-up properties by context.
     * @param applicationContext
     * Spring Application context
     * @return resources' list
     */
    protected List<Resource> findProperties(final ConfigurableApplicationContext applicationContext) {
        final List<Resource> resources = new ArrayList<>();
        resources.addAll(getResources("classpath*:core.properties"));
        resources.addAll(getResources("classpath*:application_*.properties"));
        resources.addAll(getResources("classpath*:jtf.properties"));
        final String pathProperties = applicationContext.getEnvironment().getProperty("path.properties");
        if (StringUtils.isNotEmpty(pathProperties)) {
            resources.addAll(getResources(String.format("%senv*.properties", pathProperties)));
        }
        return resources;
    }

    /**
     * Get resources by pattern.
     * @param pattern
     * Pattern wildcard for resources
     * @return resources' list
     */
    protected List<Resource> getResources(final String pattern) {
        List<Resource> resources = new ArrayList<>();
        try {
            resources = Arrays.asList(new PathMatchingResourcePatternResolver().getResources(pattern));
            LOG.debug("Loading resources in context '{}' from pattern '{}': '{}'", new Object[] {idContext, pattern, resources});
        } catch (final IOException e) {
            LOG.warn("Error with loading resources from pattern '{}' so not loading it in the context '{}'", new Object[] {pattern, idContext, e});
        }
        return resources;
    }

    /**
     * Load properties in property sources (added at last).
     * @param propertySources
     * property sources
     * @param resource
     * resource to add
     */
    protected void loadPropertyResource(final MutablePropertySources propertySources, final Resource resource) {
        try {
            propertySources.addLast(new ResourcePropertySource(resource));
            LOG.debug("Add resource in property sources' context: '{}'", resource);
        } catch (final IOException e) {
            LOG.debug("Didn't find '{}' so not loading it in the context '{}'", new Object[] {resource, idContext, e});
        }
    }
}