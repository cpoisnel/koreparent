package com.kportal.core.config;

import java.util.Locale;

public class Langue {

    public int indice;

    public Locale locale;

    public String url;

    public Langue(int i, Locale locale2, String url2) {
        indice = i;
        locale = locale2;
        url = url2;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int code) {
        this.indice = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
