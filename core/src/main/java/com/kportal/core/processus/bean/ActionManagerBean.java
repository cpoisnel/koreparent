package com.kportal.core.processus.bean;

import java.util.List;

import com.univ.objetspartages.om.PermissionBean;

public class ActionManagerBean {

    private String methodName;

    private List<PermissionBean> permissions;

    public ActionManagerBean() {}

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<PermissionBean> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionBean> permissions) {
        this.permissions = permissions;
    }
}
