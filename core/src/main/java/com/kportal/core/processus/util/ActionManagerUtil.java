package com.kportal.core.processus.util;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.core.processus.bean.ActionManagerBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;

public class ActionManagerUtil {

    private final Object obj;

    private final Class<?> clazz;

    private final List<Method> methods;

    private final HashMap<String, ActionManagerBean> actions = new HashMap<>();

    private final AutorisationBean autorisations;

    private String insufficientRightsMessage = "You are not allowed to do this.";

    private String invalidActionMessage = "This action is invalid.";

    private String inexistingMethodMessage = "This method does not exist.";

    public ActionManagerUtil(Object o, AutorisationBean autorisations) {
        this.obj = o;
        this.clazz = o.getClass();
        this.methods = Arrays.asList(this.clazz.getMethods());
        this.autorisations = autorisations;
    }

    public ActionManagerUtil register(String action, String methodName, List<PermissionBean> permissions) {
        ActionManagerBean amb = new ActionManagerBean();
        amb.setMethodName(methodName);
        amb.setPermissions(permissions);
        actions.put(action, amb);
        return this;
    }

    public void invoke(String action) throws Exception {
        if (this.actions.containsKey(action)) {
            ActionManagerBean amb = this.actions.get(action);
            if (containsMethod(amb.getMethodName())) {
                for (PermissionBean permission : amb.getPermissions()) {
                    if (!autorisations.possedePermission(permission)) {
                        throw new ErreurApplicative(this.insufficientRightsMessage);
                    }
                }
                this.clazz.getMethod(amb.getMethodName()).invoke(this.obj);
            } else {
                throw new Exception(this.inexistingMethodMessage);
            }
        } else {
            throw new Exception(this.invalidActionMessage);
        }
    }

    protected boolean containsMethod(String name) {
        boolean r = false;
        for (Method m : this.methods) {
            if (name.equals(m.getName())) {
                r = true;
            }
        }
        return r;
    }

    public String getInsufficientRightsMessage() {
        return insufficientRightsMessage;
    }

    public void setInsufficientRightsMessage(String insufficientRightsMessage) {
        this.insufficientRightsMessage = insufficientRightsMessage;
    }

    public String getInvalidActionMessage() {
        return invalidActionMessage;
    }

    public void setInvalidActionMessage(String invalidActionMessage) {
        this.invalidActionMessage = invalidActionMessage;
    }

    public String getInexistingMethodMessage() {
        return inexistingMethodMessage;
    }

    public void setInexistingMethodMessage(String inexistingMethodMessage) {
        this.inexistingMethodMessage = inexistingMethodMessage;
    }
}
