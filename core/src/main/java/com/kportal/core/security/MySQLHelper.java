package com.kportal.core.security;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.kportal.core.config.PropertyHelper;

/**
 * Classe utilitaire permettant d'encoder des valeurs dans les formats supportés par le produit à savoir :
 * - mysql (obsolète, reste pour la rétro compatibilité
 * - aes (par défaut, recommandé)
 */
public class MySQLHelper {

    private static final String PROP_PASSWORD_ENCODAGE = "password.encodage";

    private static final String ENCODAGE_AES = "aes";

    private static final String ENCODAGE_MYSQL = "mysql";

    /**
     * Encode la valeur fourni en paramètre en sha pour mysql (équivalent à la méthode PASSWORD() de mysql)
     * Cette méthode est déconseillée pour les nouveaux projet et reste ici pour des raisons de rétro-compatibilité
     * @param data la données à encoder : ne peut être null
     * @return la valeur encodée
     * @throws UnsupportedEncodingException lors de la récupérations des bytes de la chaine de caractères
     */
    public static String encrypt(final String data) throws UnsupportedEncodingException {
        final byte[] utf8 = data.getBytes(CharEncoding.DEFAULT);
        return "*" + DigestUtils.shaHex(DigestUtils.sha(utf8));
    }

    /**
     * Encodage du mot de passe MYSQL. On supporte 3 possibilités :
     * - pas d'encodage (pour les dev locals par exemple)
     * - encodage via PASSWORD() de mysql (pour des raisons de rétro-compatibilité)
     * - encodage AES : recommandé sur les nouveaux projets
     *
     * @param password
     *            le password à encoder
     * @return la valeur encoder du password
     *
     * @throws UnsupportedEncodingException
     *             lors de l'encodage en PASSWORD, l'encodage peut ne pas être supporté
     */
    public static String encodePassword(final String password) throws UnsupportedEncodingException {
        String res = password;
        final String encodage = StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(PROP_PASSWORD_ENCODAGE), "");
        // encodage mysql équivalent à la méthode PASSWORD()
        if (encodage.equals(ENCODAGE_MYSQL)) {
            res = encrypt(password.toUpperCase()).toUpperCase();
        }
        // encodage aes par defaut
        else if (encodage.equals(ENCODAGE_AES)) {
            res = AESHelper.encrypt(password, password);
        }
        return res;
    }
}
