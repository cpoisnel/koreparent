package com.kportal.pdf;

/*
 * PDFServlet.java
 *
 * Created on June 9, 2007, 10:25 PM
 */

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.univ.utils.ContexteUtil;

/**
 * The Class PDFServlet.
 * @deprecated cette servlet ne doit plus être appeler et sert juste pour les anciens front pour qu'ils puissent encore fonctionner.
 */
@Deprecated
public class PDFServlet extends HttpServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(PDFServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request
     *            servlet request
     * @param response
     *            servlet response
     */
    protected void processRequest(final HttpServletRequest request, final HttpServletResponse response) {
        // parse our markup into an xml Document
        try {
            String sUrl = request.getParameter("URL");
            // test que l'url est bien du serveur lui même
            if (isValidUrl(sUrl, request)) {
                if (StringUtils.contains(sUrl, "?")) {
                    sUrl += "&";
                } else {
                    sUrl += "?";
                }
                response.sendRedirect(sUrl + PDFUtils.TO_PDF_PARAMETER + "=true");
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                final String forward = ContexteUtil.getContexteUniv().getInfosSite().getJspFo() + "/error/404.jsp?URL_DEMANDEE=" + URLEncoder.encode(request.getRequestURI(), CharEncoding.DEFAULT);
                forwardError(request, response, forward);
            }
        } catch (final IOException ex) {
            LOG.error("An error occured while generating a pdf", ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            try {
                final String forward = ContexteUtil.getContexteUniv().getInfosSite().getJspFo() + "/error/500.jsp?URL_DEMANDEE=" + URLEncoder.encode(request.getRequestURI(), CharEncoding.DEFAULT);
                forwardError(request, response, forward);
            } catch (final UnsupportedEncodingException e) {
                LOG.error("unable to encode the parameter", e);
            }
        }
    }

    private void forwardError(final HttpServletRequest request, final HttpServletResponse response, String urlForward) {
        final String ref = request.getHeader("referer");
        if (StringUtils.isNotEmpty(ref)) {
            try {
                urlForward += "&REFERER=" + URLEncoder.encode(ref, CharEncoding.DEFAULT);
            } catch (UnsupportedEncodingException e) {
                LOG.debug("unable to encode the given string", e);
            }
        }
        final javax.servlet.ServletContext context = getServletConfig().getServletContext();
        final javax.servlet.RequestDispatcher rd = context.getRequestDispatcher(urlForward);
        try {
            rd.forward(request, response);
        } catch (ServletException | IOException e) {
            LOG.error("unable to forward to the error pzge", e);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request
     *            servlet request
     * @param response
     *            servlet response
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request
     *            servlet request
     * @param response
     *            servlet response
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Checks if is valid url.
     *
     * @param url
     *            the url
     * @param request
     *            the request
     * @return true, if is valid url
     */
    private boolean isValidUrl(final String url, final HttpServletRequest request) {
        Boolean isValid = Boolean.FALSE;
        if (StringUtils.isNotEmpty(url)) {
            isValid = url.contains(request.getServerName());
            if (isValid) {
                try {
                    final URL currentURL = new URL(url);
                } catch (final MalformedURLException e) {
                    isValid = false;
                }
            }
        }
        return isValid;
    }
}
