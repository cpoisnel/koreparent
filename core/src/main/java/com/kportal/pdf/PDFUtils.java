package com.kportal.pdf;

import org.apache.commons.lang3.StringUtils;

import com.univ.objetspartages.om.FicheUniv;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;

/**
 * Classe utilitaire pour la génération de pdf d'une fiche ou d'une url relative
 */
public class PDFUtils {

    public static final String TO_PDF_PARAMETER = "toPdf";

    /**
     * Retourne une url absolue permettant de gérer l'impression pdf de la fiche fourni en paramètre
     * @param fiche La fiche dont on souhaite avoir l'url d'impression pdf
     * @param ctx le contexte pour pouvoir calculer l'url absolue
     * @return l'url absolu de la fiche avec le paramètre nécessaire à l'impressio pdf
     */
    public static String getUrlFicheForPDF(FicheUniv fiche, ContexteUniv ctx) {
        String urlFiche = URLResolver.getAbsoluteUrlFiche(fiche, ctx);
        if (StringUtils.contains(urlFiche, "&") || StringUtils.contains(urlFiche, "?")) {
            urlFiche += "&amp;";
        } else {
            urlFiche += "?";
        }
        return urlFiche + TO_PDF_PARAMETER + "=true";
    }

    /**
     * Retourne une url absolue permettant de gérer l'impression pdf de l'url fourni en paramètre
     * @param relativeUrl l'url relative dont on souhaite avoir le pdf
     * @param ctx le contexte pour pouvoir calculer l'url absolue
     * @return l'url absolu de l'url avec le paramètre nécessaire à l'impressio pdf
     */
    public static String getUrlForPDF(String relativeUrl, ContexteUniv ctx) {
        if (StringUtils.contains(relativeUrl, "&") || StringUtils.contains(relativeUrl, "?")) {
            relativeUrl += "&amp;";
        } else {
            relativeUrl += "?";
        }
        return URLResolver.getAbsoluteUrl(relativeUrl, ctx) + TO_PDF_PARAMETER + "=true";
    }
}
