package com.kportal.pdf;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.allcolor.yahp.converter.CYaHPConverter;
import org.allcolor.yahp.converter.IHtmlToPdfTransformer;
import org.allcolor.yahp.converter.IHtmlToPdfTransformer.CConvertException;
import org.allcolor.yahp.converter.IHtmlToPdfTransformer.CHeaderFooter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.servlet.BufferedHttpResponseWrapper;
import com.univ.utils.Chaine;

/**
 * Filter permettant de gérer les impressions des pages en PDF.
 */
public class PDFFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(PDFFilter.class);

    private PDFproperties pdfProperties;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        pdfProperties = ApplicationContextManager.getEveryContextBean(PDFproperties.ID_BEAN, PDFproperties.class);
        if (pdfProperties == null) {
            pdfProperties = new PDFproperties();
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (isRequestForPdf(request)) {
            final HttpServletResponse response = (HttpServletResponse) servletResponse;
            final BufferedHttpResponseWrapper wrapper = new BufferedHttpResponseWrapper(response);
            filterChain.doFilter(servletRequest, wrapper);
            if (wrapper.getStatus() == HttpServletResponse.SC_OK) {
                final String urlWithoutParameter = request.getRequestURL().toString();
                final String title = getTitle(urlWithoutParameter);
                response.setHeader("Content-Disposition", getContentDispositionHeader(title));
                response.setContentType("application/pdf");
                final CYaHPConverter converter = new CYaHPConverter();
                final OutputStream os = response.getOutputStream();
                try {
                    convert(urlWithoutParameter, wrapper.getOutput(), title, converter, os);
                } catch (CConvertException e) {
                    LOG.error("impossible de générer le PDF", e);
                }
                os.flush();
                os.close();
            } else {
                final OutputStream os = response.getOutputStream();
                os.write(wrapper.getOutput().getBytes());
                os.flush();
                os.close();
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private boolean isRequestForPdf(final HttpServletRequest request) {
        return "true".equals(request.getParameter(PDFUtils.TO_PDF_PARAMETER));
    }

    @Override
    public void destroy() {
    }

    private void convert(final String sUrl, final String content, final String title, final CYaHPConverter converter, final OutputStream os) throws CConvertException, IOException {
        // contains configuration properties
        final Map<String, String> properties = new HashMap<>();
        properties.put(IHtmlToPdfTransformer.PDF_RENDERER_CLASS, IHtmlToPdfTransformer.FLYINGSAUCER_PDF_RENDERER);
        // ajout de fonts spécifiques
        if (pdfProperties.getFontsPath().length() > 0) {
            properties.put(IHtmlToPdfTransformer.FOP_TTF_FONT_PATH, pdfProperties.getFontsPath());
        }
        // add header/footer
        final List<CHeaderFooter> headerFooterList = new ArrayList<>();
        if (pdfProperties.getFooterHtml().length() > 0) {
            headerFooterList.add(new IHtmlToPdfTransformer.CHeaderFooter(format(pdfProperties.getFooterHtml(), title, sUrl), IHtmlToPdfTransformer.CHeaderFooter.FOOTER));
        }
        if (pdfProperties.getHeaderHtml().length() > 0) {
            headerFooterList.add(new IHtmlToPdfTransformer.CHeaderFooter(format(pdfProperties.getHeaderHtml(), title, sUrl), IHtmlToPdfTransformer.CHeaderFooter.HEADER));
        }
        converter.convertToPdf(Chaine.encodeSpecialEntities(content), pdfProperties.getPageSize(), headerFooterList, sUrl, os, properties);
    }

    private String format(String contenu, final String title, final String url) {
        final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        contenu = StringUtils.replace(contenu, PDFproperties.DATE_CONTENT, sdf.format(new Date(System.currentTimeMillis())));
        contenu = StringUtils.replace(contenu, PDFproperties.URL_CONTENT, url);
        contenu = StringUtils.replace(contenu, PDFproperties.TITLE_CONTENT, title);
        return contenu;
    }

    private String getTitle(final String url) {
        String title = "";
        if (StringUtils.endsWith(url, "/")) {
            title = StringUtils.substringAfterLast(url.substring(0, url.length() - 1), "/");
        } else {
            title = StringUtils.substringAfterLast(url, "/");
            title = StringUtils.substringBeforeLast(title, "-");
        }
        return title;
    }

    private String getContentDispositionHeader(final String title) {
        final String typeDisposition = PropertyHelper.getCoreProperty("telechargement.disposition");
        String contentDisposition = "attachment;filename=\"" + title + ".pdf" + "\"";
        if ("inline".equals(typeDisposition)) {
            contentDisposition = "inline;filename=\"" + title + ".pdf" + "\"";
        }
        return contentDisposition;
    }

    public String getUrlFromRequest() {
        return null;
    }
}
