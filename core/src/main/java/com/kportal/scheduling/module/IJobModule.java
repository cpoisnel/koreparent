package com.kportal.scheduling.module;

import java.util.List;

import org.quartz.JobDetail;

import com.kportal.extension.module.IModule;

public interface IJobModule extends IModule {

    public List<JobDetail> getJobDetails();
}
