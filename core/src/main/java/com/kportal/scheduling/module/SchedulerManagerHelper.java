package com.kportal.scheduling.module;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.scheduling.bean.JobParameter;

public class SchedulerManagerHelper {

    private static final String DEFAULT_NAME_TRIGGER = "Trigger";

    private static final String DEFAULT_NAME_JOB = "Sequence";

    public static SchedulerManager getSchedulerManager() {
        return (SchedulerManager) ApplicationContextManager.getCoreContextBean(SchedulerManager.ID_BEAN);
    }

    public static boolean isJobActif(JobKey jobKey) {
        return getSchedulerManager().isJobActif(jobKey);
    }

    public static String getJobNameFromTrigger(String triggerName) {
        return StringUtils.replace(triggerName, DEFAULT_NAME_TRIGGER, DEFAULT_NAME_JOB);
    }

    public static String getTriggerName(Long idSequenceTrigger) {
        return idSequenceTrigger + DEFAULT_NAME_TRIGGER;
    }

    public static IJobModule getModuleForJob(String jobName) {
        final Map<String, IJobModule> modules = getSchedulerManager().getJobModules();
        return modules.get(jobName);
    }

    /**
     * Récupère la liste de tous les jobs
     */
    public static Map<JobKey, JobDetail> getAllJobs() {
        return getSchedulerManager().getJobDetails();
    }

    /**
     * Récupère les détails d'un job donné
     */
    public static JobDetail getJob(final String idJob) {
        final JobKey job = new JobKey(idJob);
        return getAllJobs().get(job);
    }

    /**
     * Récupère la liste des paramètres pour un job donné
     */
    public static List<JobParameter> getParametersForJob(final String idJob) {
        final List<JobParameter> params = new ArrayList<>();
        final JobDetail details = getJob(idJob);
        final JobDataMap jdm = details.getJobDataMap();
        for (final String key : jdm.keySet()) {
            final JobParameter param = new JobParameter(key, jdm.get(key));
            params.add(param);
        }
        return params;
    }
}
