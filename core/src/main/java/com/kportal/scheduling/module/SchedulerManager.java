package com.kportal.scheduling.module;

import java.util.HashMap;
import java.util.Map;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.TriggerListener;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.AbstractBeanManager;
import com.kportal.extension.module.IModule;
import com.kportal.scheduling.monitoring.impl.quartz.BatchExecutionDateHelper;
import com.kportal.scheduling.monitoring.impl.quartz.BatchExecutionEndHelper;
import com.kportal.scheduling.service.SequenceTrigger;
import com.kportal.scheduling.service.SequenceTriggerFactory;
import com.kportal.scheduling.spring.quartz.SequenceJob;

public class SchedulerManager extends AbstractBeanManager {

    public static final String ID_BEAN = "schedulerManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerManager.class);

    private Scheduler scheduler;

    private SequenceTriggerFactory triggerFactory;

    private TriggerListener triggerListener;

    /** Classe helper permettant de connaitre la date de dernière éxécution des jobs. */
    private BatchExecutionDateHelper batchExecutionDateHelper;

    /** Classe helper permettant de savoir si un job a terminé son éxécution. */
    private BatchExecutionEndHelper batchExecutionEndHelper;

    public TriggerListener getTriggerListener() {
        return triggerListener;
    }

    public void setTriggerListener(TriggerListener triggerListener) {
        this.triggerListener = triggerListener;
    }

    public SequenceTriggerFactory getTriggerFactory() {
        return triggerFactory;
    }

    public void setTriggerFactory(SequenceTriggerFactory triggerFactory) {
        this.triggerFactory = triggerFactory;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public Map<JobKey, JobDetail> getJobDetails() {
        Map<JobKey, JobDetail> res = new HashMap<>();
        for (IModule jobModule : moduleManager.getModules(IJobModule.class)) {
            if (((IJobModule) jobModule).getJobDetails().size() > 0) {
                for (JobDetail jobDetail : ((IJobModule) jobModule).getJobDetails()) {
                    res.put(jobDetail.getKey(), jobDetail);
                }
            }
        }
        return res;
    }

    public Map<JobKey, String> getDiplayableJobDetails() {
        Map<JobKey, String> res = new HashMap<>();
        for (IModule jobModule : moduleManager.getModules(IJobModule.class)) {
            if (((IJobModule) jobModule).getJobDetails().size() > 0) {
                for (JobDetail jobDetail : ((IJobModule) jobModule).getJobDetails()) {
                    final String description = MessageHelper.getMessage(jobModule.getIdExtension(), jobDetail.getDescription());
                    res.put(jobDetail.getKey(), description);
                }
            }
        }
        return res;
    }

    public Map<String, IJobModule> getJobModules() {
        Map<String, IJobModule> res = new HashMap<>();
        for (IModule jobModule : moduleManager.getModules(IJobModule.class)) {
            if (((IJobModule) jobModule).getJobDetails().size() > 0) {
                for (JobDetail jobDetail : ((IJobModule) jobModule).getJobDetails()) {
                    res.put(jobDetail.getKey().getName(), (IJobModule) jobModule);
                }
            }
        }
        return res;
    }

    public Map<String, SequenceTrigger> getSequenceTriggers() {
        return triggerFactory.getSequenceTriggerService().getSequenceTriggers();
    }

    public boolean isJobActif(JobKey jobKey) {
        return getJobDetails().get(jobKey) != null;
    }

    @Override
    public void refresh() {
        try {
            if (!scheduler.isStarted()) {
                scheduler.start();
                try {
                    deleteAllTriggers();
                    //ajout du listener pour les sequences triggers
                    if (scheduler.getListenerManager().getTriggerListener(triggerListener.getName()) == null) {
                        scheduler.getListenerManager().addTriggerListener(triggerListener);
                    }
                    // ajout du listener pour les dates d'execution des jobs
                    if (scheduler.getListenerManager().getJobListener(batchExecutionDateHelper.getName()) == null) {
                        scheduler.getListenerManager().addJobListener(batchExecutionDateHelper);
                    }
                    // ajout du listener pour la fin d'execution des jobs
                    if (scheduler.getListenerManager().getJobListener(batchExecutionEndHelper.getName()) == null) {
                        scheduler.getListenerManager().addJobListener(batchExecutionEndHelper);
                    }
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            // on supprime les jobs obsolètes
            for (String groupName : scheduler.getJobGroupNames()) {
                for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupContains(groupName))) {
                    if (!isJobActif(jobKey)) {
                        scheduler.deleteJob(jobKey);
                    }
                }
            }
            // on ajoute les nouveaux jobs
            for (JobDetail jobDetail : getJobDetails().values()) {
                // on ajoute/update sytématiquement le job
                scheduler.addJob(jobDetail, true);
            }
            // on reinitialise le service
            getTriggerFactory().getSequenceTriggerService().init();
            // on synchronise les triggers
            reScheduleAllTrigger();
        } catch (SchedulerException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void reScheduleAllTrigger() throws SchedulerException {
        // suppression des triggers existants
        deleteAllTriggers();
        // on ajoute les nouveaux triggers et jobs associés
        scheduleAllTriggers();
    }

    private void scheduleAllTriggers() throws SchedulerException {
        // Appel du service SequenceTriggerService pour récupérer la liste de triggers configurée
        Map<String, SequenceTrigger> triggerMap = getSequenceTriggers();
        for (String key : triggerMap.keySet()) {
            SequenceTrigger sequenceTrigger = triggerMap.get(key);
            // on utilise la data map pour stocker la liste d'execution des jobs
            JobDetail job = JobBuilder.newJob(SequenceJob.class).usingJobData(new JobDataMap(sequenceTrigger.getSequence())).withIdentity(SchedulerManagerHelper.getJobNameFromTrigger(key), Scheduler.DEFAULT_GROUP).withDescription(sequenceTrigger.getDescription()).build();
            Trigger trigger = TriggerBuilder.newTrigger().withIdentity(key, Scheduler.DEFAULT_GROUP).withSchedule(CronScheduleBuilder.cronSchedule(sequenceTrigger.getCronExpression())).build();
            scheduler.scheduleJob(job, trigger);
        }
        scheduler.resumeAll();
    }

    private void deleteAllTriggers() throws SchedulerException {
        // on supprime tous les triggers
        for (String groupName : scheduler.getTriggerGroupNames()) {
            for (TriggerKey triggerKey : scheduler.getTriggerKeys(GroupMatcher.triggerGroupContains(groupName))) {
                scheduler.unscheduleJob(triggerKey);
            }
        }
    }

    public BatchExecutionDateHelper getBatchExecutionDateHelper() {
        return batchExecutionDateHelper;
    }

    public void setBatchExecutionDateHelper(BatchExecutionDateHelper batchExecutionDateHelper) {
        this.batchExecutionDateHelper = batchExecutionDateHelper;
    }

    public BatchExecutionEndHelper getBatchExecutionEndHelper() {
        return batchExecutionEndHelper;
    }

    public void setBatchExecutionEndHelper(BatchExecutionEndHelper batchExecutionEndHelper) {
        this.batchExecutionEndHelper = batchExecutionEndHelper;
    }
}
