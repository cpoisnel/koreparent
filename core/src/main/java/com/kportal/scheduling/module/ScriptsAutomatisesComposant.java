package com.kportal.scheduling.module;

import org.apache.commons.collections.CollectionUtils;

import com.kportal.extension.module.composant.Composant;
import com.kportal.scheduling.processus.AbstractScriptsAutomatises;
import com.kportal.scheduling.util.ScriptsAutomatisesUtil;
import com.univ.objetspartages.om.AutorisationBean;

public class ScriptsAutomatisesComposant extends Composant {

    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        return autorisation != null && (autorisation.possedePermission(ScriptsAutomatisesUtil.getPermissionParametrage()) || autorisation.possedePermission(ScriptsAutomatisesUtil.getPermissionLancement()) || CollectionUtils.isNotEmpty(ScriptsAutomatisesUtil.getLoadableJobs(autorisation)));
    }

    @Override
    public boolean isActionVisible(AutorisationBean autorisation, String code) {
        if (code.equals(AbstractScriptsAutomatises.ACTION_LISTER_TRIGGERS)) {
            return autorisation.possedePermission(ScriptsAutomatisesUtil.getPermissionParametrage());
        } else {
            return Boolean.TRUE;
        }
    }
}
