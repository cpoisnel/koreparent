package com.kportal.scheduling.spring.quartz;

import org.quartz.JobDataMap;
import org.quartz.impl.JobDetailImpl;

import com.kportal.scheduling.service.SequenceTrigger;

/**
 *
 * Sous classe de la classe JobDetail quartz destiné à etre executé au sein d'une sequence. Permet de spécifier si une erreur durant l'éxécution de ce job sera bloquant pour le
 * reste de la sequence via l'attribut blocking
 *
 * @author aga
 *
 */
public class SequenceJobDetail extends JobDetailImpl {

    /**
     *
     */
    private static final long serialVersionUID = -7462997939961903271L;

    /**
     * vrai si une erreur durant l'éxécution de ce JobDetail est bloquante pour la séquence dont il fait parti.
     */
    private Integer etat = SequenceTrigger.ETAT_ACTIF;

    private Integer type = SequenceTrigger.TYPE_NON_PARAMETRABLE_AFFICHABLE;

    private boolean blocking = false;

    private JobDataMap sequenceJobDataMap;

    public JobDataMap getSequenceJobDataMap() {
        return sequenceJobDataMap;
    }

    public void setSequenceJobDataMap(final JobDataMap sequenceJobData) {
        this.sequenceJobDataMap = sequenceJobData;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kportal.scheduling.spring.quartz.SequentialJob#isBlocking()
     */
    public boolean isBlocking() {
        return blocking;
    }

    /**
     * @param blocking
     *            vrai si une erreur durant l'éxécution de ce JobDetail est bloquante pour la séquence dont il fait parti.
     */
    public void setBlocking(final boolean blocking) {
        this.blocking = blocking;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(final Integer etat) {
        this.etat = etat;
    }

    public Integer getType() {
        return type;
    }

    public void setType(final Integer type) {
        this.type = type;
    }
}
