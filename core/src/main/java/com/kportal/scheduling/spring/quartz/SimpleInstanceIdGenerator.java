package com.kportal.scheduling.spring.quartz;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import org.quartz.SchedulerException;
import org.quartz.spi.InstanceIdGenerator;

public class SimpleInstanceIdGenerator implements InstanceIdGenerator {

    @Override
    public String generateInstanceId() throws SchedulerException {
        try {
            return InetAddress.getLocalHost().getHostName() + UUID.randomUUID();
        } catch (UnknownHostException e) {
            throw new SchedulerException("Couldn't get host name!", e);
        }
    }
}
