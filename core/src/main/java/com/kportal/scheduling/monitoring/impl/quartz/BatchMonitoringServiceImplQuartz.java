package com.kportal.scheduling.monitoring.impl.quartz;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import com.kportal.scheduling.module.SchedulerManager;
import com.kportal.scheduling.monitoring.BatchMonitoringService;
import com.kportal.scheduling.monitoring.JobInfos;
import com.kportal.scheduling.monitoring.exception.BatchMonitoringException;

/**
 * Implémentation du service BatchMonitoringService basée sur quartz. Attention : Les jobs lancés par la méthode startJob qui ne sont pas référencés directement par un trigger
 * doivent être déclarés dans la liste jobDetails du bean spring scheduler Les jobs qui sont déclarés dans une JobSequence ne sont PAS référencés directement par un trigger
 *
 *
 * @author aga
 */
public class BatchMonitoringServiceImplQuartz implements BatchMonitoringService {

    /** Scheduler quartz. */
    private SchedulerManager schedulerManager;

    /**
     * Job exists.
     *
     * @param jobName
     *            the job name
     * @return true, if successful
     * @throws SchedulerException
     *             the scheduler exception
     */
    @Override
    public boolean jobExists(final String jobName) throws SchedulerException {
        return jobExists(jobName, Scheduler.DEFAULT_GROUP);
    }

    /**
     * Job exists.
     *
     * @param jobName
     *            the job name
     * @param groupName
     *            the group name
     * @return true, if successful
     * @throws SchedulerException
     *             the scheduler exception
     */
    @Override
    public boolean jobExists(final String jobName, final String groupName) throws SchedulerException {
        return schedulerManager.getScheduler().getJobDetail(new JobKey(jobName, groupName)) != null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kportal.scheduling.monitoring.BatchMonitoringService#getJobInfos(
     * java.lang.String)
     */
    @Override
    public JobInfos getJobInfos(final String jobName) throws BatchMonitoringException {
        return getJobInfos(jobName, Scheduler.DEFAULT_GROUP);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kportal.scheduling.monitoring.BatchMonitoringService#getJobInfos(
     * java.lang.String, java.lang.String)
     */
    @Override
    public JobInfos getJobInfos(final String jobName, final String groupName) throws BatchMonitoringException {
        try {
            JobInfos jobInfos = new JobInfos();
            JobDetail jobDetail = schedulerManager.getScheduler().getJobDetail(new JobKey(jobName, groupName));
            JobExecutionContext jobExecutionContext = getJobExecutionContext(jobName, groupName);
            jobInfos.setGroupName(jobDetail.getKey().getGroup());
            jobInfos.setJobName(jobDetail.getKey().getName());
            jobInfos.setNextFireDate(getJobNextFireDate(jobName, groupName));
            jobInfos.setLastFireDate(getJobLastFireDate(jobName, groupName));
            if (jobExecutionContext != null) {
                jobInfos.setRunning(true);
                jobInfos.setCurrentFireTime(jobExecutionContext.getFireTime());
            } else {
                jobInfos.setRunning(false);
                jobInfos.setCurrentFireTime(null);
            }
            return jobInfos;
        } catch (SchedulerException e) {
            throw new BatchMonitoringException(e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kportal.scheduling.monitoring.BatchMonitoringService#startJob(com
     * .kportal.scheduling.monitoring.JobInfos, java.util.Map).
     *
     */
    @Override
    public JobInfos startJob(JobInfos jobInfos, Map<String, Object> additionalParameters) throws BatchMonitoringException {
        try {
            JobDataMap jobDataMap = new JobDataMap();
            if (additionalParameters != null) {
                jobDataMap.putAll(additionalParameters);
            }
            startJob(new JobKey(jobInfos.getJobName(), jobInfos.getGroupName()), jobDataMap, false);
        } catch (SchedulerException e) {
            throw new BatchMonitoringException(e);
        }
        return getJobInfos(jobInfos.getJobName(), jobInfos.getGroupName());
    }

    @Override
    public void startJob(JobKey jobKey, JobDataMap jobDataMap, boolean resumeAll) throws SchedulerException {
        synchronized (schedulerManager.getScheduler()) {
            if (!areJobsRunning()) {
                // on indique que le job va être executé
                schedulerManager.getBatchExecutionEndHelper().jobToBeExecuted(jobKey.getName(), jobKey.getGroup());
                // on active le trigger
                schedulerManager.getScheduler().triggerJob(jobKey, jobDataMap);
                if (resumeAll) {
                    // on le resume car tous les triggers sont en pause à ce moment là
                    //  @see #BatchTriggerListener
                    schedulerManager.getScheduler().resumeJob(jobKey);
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kportal.scheduling.monitoring.BatchMonitoringService#startJob(com
     * .kportal.scheduling.monitoring.JobInfos)
     */
    @Override
    public JobInfos startJob(JobInfos jobInfos) throws BatchMonitoringException {
        return startJob(jobInfos, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kportal.scheduling.monitoring.BatchMonitoringService#pauseJob(com
     * .kportal.scheduling.monitoring.JobInfos)
     */
    @Override
    public JobInfos pauseJob(JobInfos jobInfos) throws BatchMonitoringException {
        try {
            schedulerManager.getScheduler().pauseJob(new JobKey(jobInfos.getJobName(), jobInfos.getGroupName()));
        } catch (SchedulerException e) {
            throw new BatchMonitoringException(e);
        }
        return getJobInfos(jobInfos.getJobName(), jobInfos.getGroupName());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kportal.scheduling.monitoring.BatchMonitoringService#resumeJob(com
     * .kportal.scheduling.monitoring.JobInfos)
     */
    @Override
    public JobInfos resumeJob(JobInfos jobInfos) throws BatchMonitoringException {
        try {
            schedulerManager.getScheduler().resumeJob(new JobKey(jobInfos.getJobName(), jobInfos.getGroupName()));
        } catch (SchedulerException e) {
            throw new BatchMonitoringException(e);
        }
        return getJobInfos(jobInfos.getJobName(), jobInfos.getGroupName());
    }

    /**
     * retourne la date de prochaine éxécution du job en parcourant tous ses triggers et en prenant la date de prochaine éxécution la plus petite.
     *
     * @param jobName
     *            Le nom du job
     * @param groupName
     *            Le nom du groupe du job
     * @return La date de prochaine éxécution du job
     * @throws BatchMonitoringException
     *             the batch monitoring exception
     */
    private Date getJobNextFireDate(final String jobName, final String groupName) throws BatchMonitoringException {
        Date result = null;
        try {
            List<? extends Trigger> triggers = schedulerManager.getScheduler().getTriggersOfJob(new JobKey(jobName, groupName));
            for (Trigger trigger : triggers) {
                if (result == null || (trigger.getNextFireTime() != null && trigger.getNextFireTime().before(result))) {
                    result = trigger.getNextFireTime();
                }
            }
        } catch (SchedulerException e) {
            throw new BatchMonitoringException(e);
        }
        return result;
    }

    /**
     * retourne la date de dernière éxécution du job en parcourant tous ses triggers et en prenant la date de dernière éxécution la plus grande.
     *
     * @param jobName
     *            Le nom du job
     * @param groupName
     *            Le nom du groupe du job
     * @return La date de dernière éxécution du job
     * @throws BatchMonitoringException
     *             the batch monitoring exception
     */
    private Date getJobLastFireDate(final String jobName, final String groupName) throws BatchMonitoringException {
        return schedulerManager.getBatchExecutionDateHelper().getJobLastExecutionDate(jobName, groupName);
    }

    /**
     * Retourne le jobExecutionContext quartz d'un job en cours d'éxécution ou null si le job n'est pas démarré.
     *
     * @param jobName
     *            Le nom du job
     * @param groupName
     *            Le nom du groupe du job
     * @return le jobExecutionContext quartz d'un job en cours d'éxécution ou null si le job n'est pas démarré
     * @throws BatchMonitoringException
     *             the batch monitoring exception
     */
    private JobExecutionContext getJobExecutionContext(final String jobName, final String groupName) throws BatchMonitoringException {
        JobExecutionContext result;
        try {
            result = (JobExecutionContext) CollectionUtils.find(schedulerManager.getScheduler().getCurrentlyExecutingJobs(), new Predicate() {

                @Override
                public boolean evaluate(Object arg0) {
                    JobExecutionContext jobExecutionContext = (JobExecutionContext) arg0;
                    return StringUtils.equals(jobName, jobExecutionContext.getJobDetail().getKey().getName()) && StringUtils.equals(groupName, jobExecutionContext.getJobDetail().getKey().getGroup());
                }
            });
        } catch (SchedulerException e) {
            throw new BatchMonitoringException(e);
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.kportal.scheduling.monitoring.BatchMonitoringService#isJobEnded(java.lang.String, java.lang.String)
     */
    @Override
    public boolean isJobEnded(String jobName, String groupName) {
        return schedulerManager.getBatchExecutionEndHelper().isJobEnded(jobName, groupName);
    }

    /* (non-Javadoc)
     * @see com.kportal.scheduling.monitoring.BatchMonitoringService#isJobEnded(java.lang.String, java.lang.String)
     */
    @Override
    public boolean isJobStarted(String jobName, String groupName) {
        return schedulerManager.getBatchExecutionEndHelper().isJobStarted(jobName, groupName);
    }

    @Override
    public boolean areJobsRunning() throws SchedulerException {
        return schedulerManager.getBatchExecutionEndHelper().isJobRunning();
    }

    @Override
    public String getFirstRunningJobName() throws SchedulerException {
        return schedulerManager.getBatchExecutionEndHelper().getJobKeyRunning() != null ? schedulerManager.getScheduler().getJobDetail(schedulerManager.getBatchExecutionEndHelper().getJobKeyRunning()).getDescription() : StringUtils.EMPTY;
    }

    @Override
    public String getJobDescription(String jobName) throws SchedulerException {
        return schedulerManager.getScheduler().getJobDetail(new JobKey(jobName)).getDescription();
    }

    public SchedulerManager getSchedulerManager() {
        return schedulerManager;
    }

    public void setSchedulerManager(SchedulerManager schedulerManager) {
        this.schedulerManager = schedulerManager;
    }
}
