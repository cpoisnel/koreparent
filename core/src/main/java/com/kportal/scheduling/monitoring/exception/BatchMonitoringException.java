package com.kportal.scheduling.monitoring.exception;

/**
 * Exception mère du service de monitoring des batchs
 *
 * @author aga
 *
 */
public class BatchMonitoringException extends Exception {

    private static final long serialVersionUID = 1L;

    public BatchMonitoringException(Throwable arg0) {
        super(arg0);
    }
}
