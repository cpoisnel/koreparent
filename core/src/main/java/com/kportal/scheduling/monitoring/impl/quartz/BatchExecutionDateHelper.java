package com.kportal.scheduling.monitoring.impl.quartz;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.listeners.JobListenerSupport;

public class BatchExecutionDateHelper extends JobListenerSupport {

    private static final String NAME = "BatchExecutionDateHelper";

    private final Map<String, Date> jobNamesAndLastExecutionDate = Collections.synchronizedMap(new HashMap<String, Date>());

    private static String getJobLastExecutionDateKey(String jobName, String groupName) {
        StringBuilder buf = new StringBuilder();
        buf.append(jobName);
        if (groupName != null) {
            buf.append(groupName);
        }
        return buf.toString();
    }

    @Override
    public String getName() {
        return NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @seeorg.quartz.listeners.JobListenerSupport#jobWasExecuted(org.quartz.
     * JobExecutionContext, org.quartz.JobExecutionException)
     */
    @Override
    public void jobWasExecuted(final JobExecutionContext context, JobExecutionException jobException) {
        jobNamesAndLastExecutionDate.put(BatchExecutionDateHelper.getJobLastExecutionDateKey(context.getJobDetail().getKey().getName(), context.getJobDetail().getKey().getGroup()), new Date());
    }

    public Date getJobLastExecutionDate(final String jobName, final String groupName) {
        return jobNamesAndLastExecutionDate.get(BatchExecutionDateHelper.getJobLastExecutionDateKey(jobName, groupName));
    }
}
