package com.kportal.scheduling.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.ModuleHelper;
import com.kportal.scheduling.module.IJobModule;
import com.kportal.scheduling.module.SchedulerManagerHelper;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;

public class ScriptsAutomatisesUtil {

    public static final String ID_PERMISSION = "SUPV";

    public static final String ACTION_LANCEMENT = "L";

    public static final String CODE_PERMISSION = "scra";

    public static final String ACTION_PARAMETRAGE = "P";

    public static final String ID_BEAN_COMPOSANT = "scriptsAutomatises";

    /**
     * Retourne la permission de lancement.
     */
    public static PermissionBean getPermissionLancement() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, ACTION_LANCEMENT);
    }

    private static IModule getModule() {
        return ModuleHelper.getModule(ID_BEAN_COMPOSANT);
    }

    /**
     * Retourne la permission de lancement pour un jobModule
     */
    public static PermissionBean getPermissionLancement(IModule module, String code) {
        return PermissionUtil.getPermissionBean(module, code, ACTION_LANCEMENT);
    }

    /**
     * Retourne la permission de parametrage.
     */
    public static PermissionBean getPermissionParametrage() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, ACTION_PARAMETRAGE);
    }

    /**
     * Retourne la liste des jobs executables pour l'autorisation
     */
    public static List<String> getLoadableJobs(AutorisationBean autorisation) {
        List<String> jobloadable = new ArrayList<>();
        Map<String, IJobModule> map = SchedulerManagerHelper.getSchedulerManager().getJobModules();
        for (String key : map.keySet()) {
            IJobModule module = map.get(key);
            if (autorisation.possedePermission(getPermissionLancement(module, key))) {
                jobloadable.add(key);
            }
        }
        return jobloadable;
    }

    public static boolean possedePermissionComposant(AutorisationBean autorisation) {
        return autorisation != null && (autorisation.possedePermission(ScriptsAutomatisesUtil.getPermissionParametrage()) || autorisation.possedePermission(ScriptsAutomatisesUtil.getPermissionLancement()) || CollectionUtils.isNotEmpty(ScriptsAutomatisesUtil.getLoadableJobs(autorisation)));
    }
}
