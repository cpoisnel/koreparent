package com.kportal.scheduling.processus;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.processus.util.ActionManagerUtil;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.scheduling.service.SequenceTriggerServiceImpl;
import com.kportal.scheduling.util.ScriptsAutomatisesUtil;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;

public class ScriptsAutomatises extends AbstractScriptsAutomatises {

    private static final Logger LOG = LoggerFactory.getLogger(ScriptsAutomatises.class);

    public ScriptsAutomatises(final InfoBean infoBean) {
        super(infoBean);
    }

    @Override
    protected boolean traiterAction() throws Exception {
        autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique(ECRAN_LOGIN);
        } else if (!ScriptsAutomatisesUtil.possedePermissionComposant(autorisations)) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
        } else {
            try {
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                final ActionManagerUtil amu = new ActionManagerUtil(this, autorisations);
                amu.setInsufficientRightsMessage(MessageHelper.getCoreMessage(DROITS_INSUFFISANTS));
                final List<PermissionBean> consultation = new ArrayList<>();
                final List<PermissionBean> lancement = new ArrayList<>();
                final List<PermissionBean> parametrage = new ArrayList<>();
                parametrage.add(ScriptsAutomatisesUtil.getPermissionParametrage());
                amu.register(ACTION_LISTE, "traiterLISTE", consultation);
                amu.register(ACTION_LISTER_TRIGGERS, "traiterLISTERTRIGGERS", parametrage);
                amu.register(ACTION_EXECUTER, "traiterEXECUTER", lancement);
                amu.register(ACTION_CONFIGURER, "traiterCONFIGURER", lancement);
                amu.invoke(action);
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOG.error(e.getMessage(), e);
                infoBean.addMessageErreur(e.toString());
            }
        }
        return etat == FIN;
    }

    @Override
    public void recupererDroits() {
        // soit tout le droit de lancement
        final boolean all = autorisations.possedePermission(ScriptsAutomatisesUtil.getPermissionLancement());
        // soit un droit par script
        List<String> jobloadable = new ArrayList<>();
        if (!all) {
            jobloadable = ScriptsAutomatisesUtil.getLoadableJobs(autorisations);
        }
        infoBean.set(PEUT_LANCER_OUT, all);
        infoBean.set(LISTE_LOADABLE_JOBS, jobloadable);
        infoBean.set(PEUT_PARAMETRER_OUT, false);
    }

    @Override
    public void recupererTriggers() {
        final SequenceTriggerServiceImpl stsi = new SequenceTriggerServiceImpl();
        infoBean.set(LISTE_TRIGGERS_OUT, stsi.getSequenceTriggers());
    }
}
