package com.kportal.scheduling.processus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.kportal.scheduling.bean.JobParameter;
import com.kportal.scheduling.module.IJobModule;
import com.kportal.scheduling.module.SchedulerManagerHelper;
import com.kportal.scheduling.monitoring.BatchMonitoringService;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.utils.json.CodecJSon;

public abstract class AbstractScriptsAutomatises extends ProcessusBean {

    public static final String ACTION_LISTE = "PARAMETRER";

    public static final String ACTION_LISTER_TRIGGERS = "PLANIFIER";

    protected static final String ECRAN_LISTE = "LISTE";

    protected static final String ECRAN_LISTETRIGGERS = "LISTETRIGGERS";

    protected static final String ECRAN_LOGIN = "LOGIN";

    protected static final String ACTION_EXECUTER = "EXECUTER";

    protected static final String ACTION_CONFIGURER = "PARAMETRAGE";

    protected static final String DROITS_INSUFFISANTS = "BO_DROITS_INSUFFISANTS";

    protected static final String PEUT_LANCER_OUT = "PEUT_LANCER";

    protected static final String PEUT_PARAMETRER_OUT = "PEUT_PARAMETRER";

    protected static final String LISTE_TRIGGERS_OUT = "LISTE_TRIGGERS";

    protected static final String JOB_FIELD = "JOB";

    protected static final String PARAMS_FIELD = "PARAMS";

    protected static final String JOB_PARAMS_OUT = "JOB_PARAMS";

    protected static final String JOB_EXTENSION_ID = "JOB_EXTENSION_ID";

    protected static final String ECRAN_PARAMETRAGE = "PARAMETRAGE";

    protected static final String LISTE_LOADABLE_JOBS = "LISTE_LOADABLE_JOBS";

    /** The autorisations. */
    protected AutorisationBean autorisations = null;

    public AbstractScriptsAutomatises(final InfoBean infoBean) {
        super(infoBean);
    }

    @Override
    protected boolean traiterAction() throws Exception {
        return etat == FIN;
    }

    public void recupererDroits() {}

    public void recupererTriggers() {}

    public void traiterEXECUTER() throws Exception {
        final BatchMonitoringService monitor = (BatchMonitoringService) ApplicationContextManager.getCoreContextBean(BatchMonitoringService.ID_BEAN);
        final String jobName = infoBean.getString(JOB_FIELD);
        final Map<String, Object> jobDataMap = SchedulerManagerHelper.getParametersForJob(jobName).size() > 0 ? CodecJSon.decodeStringJSonToClass(infoBean.getString(PARAMS_FIELD), new TypeReference<HashMap<String, Object>>() {}) : new HashMap<String, Object>();
        if (!monitor.areJobsRunning()) {
            monitor.startJob(monitor.getJobInfos(jobName), jobDataMap);
        }
        traiterLISTE();
    }

    public void traiterCONFIGURER() {
        final String jobName = infoBean.getString(JOB_FIELD);
        final List<JobParameter> params = SchedulerManagerHelper.getParametersForJob(jobName);
        final IJobModule module = SchedulerManagerHelper.getModuleForJob(jobName);
        infoBean.set(JOB_EXTENSION_ID, module.getExtension().getId());
        infoBean.set(JOB_PARAMS_OUT, params);
        ecranLogique = ECRAN_PARAMETRAGE;
    }

    public void traiterLISTE() {
        recupererDroits();
        recupererTriggers();
        ecranLogique = ECRAN_LISTE;
    }

    public void traiterLISTERTRIGGERS() {
        recupererDroits();
        recupererTriggers();
        ecranLogique = ECRAN_LISTETRIGGERS;
    }
}
