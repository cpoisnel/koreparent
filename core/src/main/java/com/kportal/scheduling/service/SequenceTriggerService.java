package com.kportal.scheduling.service;

import java.util.Map;

public interface SequenceTriggerService {

    public Map<String, SequenceTrigger> getSequenceTriggers();

    public void init();
}
