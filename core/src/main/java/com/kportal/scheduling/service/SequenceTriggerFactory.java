package com.kportal.scheduling.service;

import com.jsbsoft.jtf.core.ApplicationContextManager;

public class SequenceTriggerFactory {

    private String serviceBeanName;

    public SequenceTriggerService getSequenceTriggerService() {
        return (SequenceTriggerService) ApplicationContextManager.getCoreContextBean(serviceBeanName);
    }

    public String getServiceBeanName() {
        return serviceBeanName;
    }

    public void setServiceBeanName(String serviceBeanName) {
        this.serviceBeanName = serviceBeanName;
    }
}
