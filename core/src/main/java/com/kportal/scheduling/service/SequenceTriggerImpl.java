package com.kportal.scheduling.service;

import java.util.Map;

import com.kportal.scheduling.spring.quartz.SequenceJobDetailDecorator;

public class SequenceTriggerImpl implements SequenceTrigger {

    public String cronExpression;

    public String description;

    public Integer etat = SequenceTrigger.ETAT_ACTIF;

    public Integer type = SequenceTrigger.TYPE_NON_PARAMETRABLE_AFFICHABLE;

    public Map<String, SequenceJobDetailDecorator> sequence;

    @Override
    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    @Override
    public Map<String, SequenceJobDetailDecorator> getSequence() {
        return sequence;
    }

    @Override
    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setSequence(Map<String, SequenceJobDetailDecorator> sequence) {
        this.sequence = sequence;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
