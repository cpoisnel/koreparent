package com.kportal.extension.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.service.impl.AbstractServiceBean;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.context.OverridedContextBean;
import com.kportal.extension.bean.ExtensionBean;
import com.kportal.extension.bean.ModuleBean;
import com.kportal.extension.dao.ModuleDAO;
import com.kportal.extension.module.IModule;

/**
 * Service permettant de gérer les modules de l'applications..
 */
public class ServiceModule extends AbstractServiceBean<ModuleBean, ModuleDAO> {

    private static final String NB_CE_MODULE_SURCHARGE_LE_MODULE = "BO_EXTENSION_MODULE_SURCHARGE_LE_MODULE";

    private static final String DE_L_EXTENSION = "BO_EXTENSION_DE_L_EXTENSION";

    private static final String NB_CE_MODULE_EST_SURCHARGE_PAR_LE_MODULE = "BO_EXTENSION_MODULE_SURCHARGE_PAR_LE_MODULE";

    private ServiceExtension serviceExtension;

    public void setServiceExtension(final ServiceExtension serviceExtension) {
        this.serviceExtension = serviceExtension;
    }

    public void save(ModuleBean moduleBean) {
        throw new UnsupportedOperationException("Cette opération n'est pas disponible sur ce service. Utilisez les méthodes \"add\" et \"update\"");
    }

    /**
     * Permet d'enregistrer un module en BDD.
     * @param moduleBean le module à sauvegarder
     */
    public void add(ModuleBean moduleBean) {
        dao.addWithForcedId(moduleBean);
    }

    /**
     * Permet d'updater un module en BDD.
     * @param moduleBean le module à sauvegarder
     */
    public void update(ModuleBean moduleBean) {
        dao.update(moduleBean);
    }
    
    /**
     * Supprime l'ensemble des modules dont l'état et {@link IModule#ETAT_A_RESTAURER}
     */
    public void deleteNotRestorable() {
        dao.deleteByState(IModule.ETAT_A_RESTAURER);
    }

    public void deleteByExtensionId(long extensionId) {
        dao.deleteByExtension(extensionId);
    }
    /**
     * Récupère l'ensemble des modules de l'extension dont l'id et passé en paramètre et dont le type matche la liste fourni
     * @param extensionId l'id de l'extension dont on souhaite récupérer les modules
     * @param types le type des modules que l'on souhaite récupérer.
     * @return l'ensemble des modules de l'extension et dont le type correspond à l'un de ceux fourni en paramètre. Si rien n'est trouvé une liste vide est retournée
     */
    public List<ModuleBean> getByExtensionAndTypes(long extensionId, List<Integer> types) {
        return dao.getByExtensionIdAndType(extensionId, types);
    }

    /**
     * Ancienne méthode contenu dans l'objet Module. C'est du code assez approximatif...
     * @param moduleBean Un module dont on mets à jour certains champs...
     * @param extension l'extension du module, si elle est null, on l'initialise à partir du champ idExtension du module fourni en paramètre
     * @return le module que l'on vient de mettre à jour
     */
    public ModuleBean setConfig(ModuleBean moduleBean, ExtensionBean extension) {
        if (extension == null) {
            extension = serviceExtension.getById(moduleBean.getIdExtension());
        }
        final IModule module = ApplicationContextManager.getBean(extension.getIdBean(), moduleBean.getIdBean(), IModule.class);
        if (module != null) {
            String description = StringUtils.EMPTY;
            // si c'est un bean qui surcharge
            if (module instanceof OverridedContextBean) {
                // on récupére le bean surchargé
                IModule module2 = ApplicationContextManager.getBean(extension.getIdBean(), moduleBean.getIdBean(), false, IModule.class);
                if (module2 != null) {
                    // si ce n'est pas le même
                    if (!module2.equals(module)) {
                        description = module2.getDescription();
                        description += MessageHelper.getCoreMessage(NB_CE_MODULE_EST_SURCHARGE_PAR_LE_MODULE) + MessageHelper.getMessage(module.getIdExtension(), module.getLibelle()) + MessageHelper.getCoreMessage(DE_L_EXTENSION) + MessageHelper.getMessage(module.getIdExtension(), module.getLibelleExtension()) + "\"";
                    } else {
                        // sinon on cherche le bean surchargé
                        description = module.getDescription();
                        module2 = ApplicationContextManager.getBean(((OverridedContextBean) module).getIdExtensionToOverride(), ((OverridedContextBean) module).getIdBeanToOverride(), false, IModule.class);
                        if (module2 != null) {
                            description += MessageHelper.getCoreMessage(NB_CE_MODULE_SURCHARGE_LE_MODULE) + MessageHelper.getMessage(((OverridedContextBean) module).getIdExtensionToOverride(), module2.getLibelle()) + MessageHelper.getCoreMessage(DE_L_EXTENSION) + MessageHelper.getMessage(((OverridedContextBean) module).getIdExtensionToOverride(), module2.getLibelleExtension()) + "\"";
                        }
                    }
                }
            }
            moduleBean.setIdBeanExtension(extension.getIdBean());
            moduleBean.setDescription(description);
            moduleBean.setLibelleExtension(extension.getLibelle());
        }
        return moduleBean;
    }

    /**
     * Update l'état de tout les modules correspondant à l'extension fourni en paramètre.
     * @param state l'état à mettre à jour
     * @param idExtension l'id de l'extension conserné. Si l'id est null, l'ensemble de la table est mis à jour.
     */
    public void updateStateByExtensionId(final int state, final Long idExtension) {
        dao.updateStateByExtensionId(state, idExtension);
    }
}
