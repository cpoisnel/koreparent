package com.kportal.extension.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.service.impl.AbstractServiceBean;
import com.kportal.extension.IExtension;
import com.kportal.extension.bean.ExtensionBean;
import com.kportal.extension.dao.ExtensionDAO;
import com.kportal.extension.exception.NoSuchExtensionException;

public class ServiceExtension extends AbstractServiceBean<ExtensionBean, ExtensionDAO> {

    public void save(ExtensionBean extensionBean) {
        throw new UnsupportedOperationException("Cette méthode n'est pas disponible sur ce service. Utilisez les méthodes \"add\" et \"update\"");
    }

    /**
     * Permet d'enregistrer une extension en BDD. Si le module à un Id, il fera un update, sinon il fera un add.
     * @param extensionBean l'extension à sauvegarder
     */
    public void add(ExtensionBean extensionBean) {
        dao.addWithForcedId(extensionBean);
    }

    /**
     * Permet d'enregistrer une extension en BDD. Si le module à un Id, il fera un update, sinon il fera un add.
     * @param extensionBean l'extension à sauvegarder
     */
    public void update(ExtensionBean extensionBean) {
        dao.update(extensionBean);
    }

    public List<ExtensionBean> getAll() {
        return dao.selectAll();
    }

    public List<ExtensionBean> getByTypeOrderByDateLabel(List<Integer> types) {
        return dao.selectByTypeOrderByDateLabel(types);
    }

    public ExtensionBean setConfig(ExtensionBean extensionBean) throws NoSuchExtensionException {
        final IExtension extension =  ApplicationContextManager.getBean(extensionBean.getIdBean(), extensionBean.getIdBean(), IExtension.class);
        if (extension == null) {
            throw new NoSuchExtensionException(extensionBean.getIdBean());
        }
        extensionBean.setAuteur(extension.getAuteur());
        extensionBean.setLibelle(extension.getLibelle());
        extensionBean. setDescription(extension.getDescription());
        if (StringUtils.isNotBlank(extension.getLogo())) {
            extensionBean.setLogo(extension.getRelativePath() + extension.getLogo());
        }
        extensionBean.setUrl(extension.getUrl());
        extensionBean.setCoreVersion(extension.getCoreVersion());
        return extensionBean;
    }

    public void changeAllState(Integer newState) {
        dao.updateAllState(newState);
    }

}
