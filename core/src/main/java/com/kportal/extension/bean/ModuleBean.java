package com.kportal.extension.bean;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.univ.objetspartages.bean.AbstractPersistenceBean;

/**
 * Classe contenant les données d'un module
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ModuleBean  extends AbstractPersistenceBean implements Serializable {

    private static final long serialVersionUID = 34901317833492943L;

    private String idBean = null;

    private Long idExtension = null;

    private String libelle = null;

    private Date dateCreation = null;

    private Date dateModification = null;

    private Integer etat = null;

    private Integer type = null;

    private String description = null;

    private String libelleExtension = null;

    private String idBeanExtension = null;

    public String getIdBeanExtension() {
        return idBeanExtension;
    }

    public void setIdBeanExtension(String idBeanExtension) {
        this.idBeanExtension = idBeanExtension;
    }

    public String getLibelleExtension() {
        return libelleExtension;
    }

    public void setLibelleExtension(String libelleExtension) {
        this.libelleExtension = libelleExtension;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setIdBean(String idBean) {
        this.idBean = idBean;
    }

    public void setIdExtension(Long idExtension) {
        this.idExtension = idExtension;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getIdBean() {
        return idBean;
    }

    public Long getIdExtension() {
        return idExtension;
    }

    public String getLibelle() {
        return libelle;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public Integer getEtat() {
        return etat;
    }
}
