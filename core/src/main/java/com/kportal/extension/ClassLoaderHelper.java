package com.kportal.extension;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;

public class ClassLoaderHelper {

    /**
     * Adds the class ressource.
     *
     * @param classFile
     *            the class file
     * @param ressources
     *            the ressources
     * @throws MalformedURLException
     *             the exception
     */
    public static void addRessource(File classFile, String extension, List<URL> ressources) throws MalformedURLException {
        for (File file : classFile.listFiles()) {
            if (file.isDirectory()) {
                addRessource(file, extension, ressources);
            } else if (file.getName().toLowerCase().endsWith("." + extension)) {
                ressources.add(file.toURI().toURL());
            }
        }
    }

    /**
     * Adds the url to current class loader.
     *
     * @param urlClassLoader
     *            the url class loader
     * @param url
     *            the u
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void addURLToClassLoader(URLClassLoader urlClassLoader, URL url) throws IOException {
        Class<URLClassLoader> classLoaderClass = URLClassLoader.class;
        try {
            Method method = classLoaderClass.getDeclaredMethod("addURL", URL.class);
            method.setAccessible(true);
            method.invoke(urlClassLoader, url);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new IOException("Impossible d'ajouter l'URL " + url.getPath() + " au ClassLoader  ", e);
        }
    }
}
