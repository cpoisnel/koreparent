package com.kportal.extension.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.UpdateToDataSourceException;
import com.jsbsoft.jtf.datasource.utils.DaoUtils;
import com.kportal.extension.bean.ExtensionBean;

/**
 * Le DAO permettant de stocker en BDD les extensions
 */
public class ExtensionDAO extends AbstractCommonDAO<ExtensionBean> {

    public ExtensionDAO() {
        tableName = "EXTENSION";
    }

    public ExtensionBean addWithForcedId(ExtensionBean extension) throws AddToDataSourceException {
        try {
            namedParameterJdbcTemplate.update(DaoUtils.getAddWithForcedIdQuery(tableName, getColumns(), columnNamingStrategy), getParameters(extension));
        } catch (DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"EXTENSION\"", extension.toString()), dae);
        }
        return extension;
    }

    public List<ExtensionBean> selectByTypeOrderByDateLabel(List<Integer> types) {
        final List<ExtensionBean> results = new ArrayList<>();
            try {
                final StringBuilder query = new StringBuilder("SELECT * FROM EXTENSION");
                final MapSqlParameterSource params = new MapSqlParameterSource();
                if(CollectionUtils.isNotEmpty(types)) {
                    params.addValue("types", types);
                    query.append(" WHERE TYPE IN (:types)");
                }
                query.append(" ORDER BY DATE_CREATION, LIBELLE");
                results.addAll(namedParameterJdbcTemplate.query(query.toString(), params, rowMapper));
            } catch (DataAccessException dae) {
                throw new DataSourceException("Unable to query on all datas in table \"EXTENSION\"", dae);
            }

        return results;
    }

    public List<ExtensionBean> selectAll() {
        List<ExtensionBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from EXTENSION", rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException("Unable to query on all datas in table \"EXTENSION\"", dae);
        }
        return results;
    }

    public void updateAllState(Integer newState) throws UpdateToDataSourceException {
        try {
            SqlParameterSource params = new MapSqlParameterSource("etat", newState);
            namedParameterJdbcTemplate.update("UPDATE EXTENSION SET ETAT = :etat", params);
        } catch (DataAccessException dae) {
            throw new UpdateToDataSourceException(String.format("Unable to update EXTENSION with state %d", newState), dae);
        }
    }

}
