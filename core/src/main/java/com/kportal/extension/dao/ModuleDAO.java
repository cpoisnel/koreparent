package com.kportal.extension.dao;

import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.jsbsoft.jtf.datasource.utils.DaoUtils;
import com.kportal.extension.bean.ModuleBean;

/**
 * Le DAO permettant de stocker en BDD les modules
 */
public class ModuleDAO extends AbstractCommonDAO<ModuleBean> {

    public ModuleDAO() {
        tableName = "MODULE";
    }

    public ModuleBean addWithForcedId(ModuleBean moduleBean) throws AddToDataSourceException {
        try {
            namedParameterJdbcTemplate.update(DaoUtils.getAddWithForcedIdQuery(tableName, getColumns(), columnNamingStrategy), getParameters(moduleBean));
        } catch (DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"MODULE\"", moduleBean.toString()), dae);
        }
        return moduleBean;
    }

    public void deleteByState(final int state) throws DeleteFromDataSourceException {
        try {
            SqlParameterSource namedParameters = new MapSqlParameterSource("state", state);
            namedParameterJdbcTemplate.update("delete from MODULE WHERE ETAT = :state", namedParameters);
        } catch (DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with state %d from table \"%s\"", state, tableName), dae);
        }
    }

    public void deleteByExtension(final long extensionId) throws DeleteFromDataSourceException {
        try {
            SqlParameterSource namedParameters = new MapSqlParameterSource("extensionId", extensionId);
            namedParameterJdbcTemplate.update("delete from MODULE WHERE ID_EXTENSION = :extensionId", namedParameters);
        } catch (DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with extension ID %d from table \"%s\"", extensionId, tableName), dae);
        }
    }

    public List<ModuleBean> getByExtensionIdAndType(Long extensionId, List<Integer> types) {
        final List<ModuleBean> results;
        try {
            final StringBuilder query = new StringBuilder("SELECT * from MODULE  WHERE ID_EXTENSION = :idExtension");
            final MapSqlParameterSource parameters = new MapSqlParameterSource("idExtension", extensionId);
            if(CollectionUtils.isNotEmpty(types)){
                parameters.addValue("types",types);
                query.append(" AND TYPE IN (:types)");
            }
            results = namedParameterJdbcTemplate.query(query.toString(), parameters, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }

    public void updateStateByExtensionId(final int state, final Long extensionId) {
        try {
            String query = "UPDATE MODULE SET ETAT = :state, DATE_MODIFICATION = :date ";
            MapSqlParameterSource parameters = new MapSqlParameterSource("state",state);
            parameters.addValue("date", new Timestamp(System.currentTimeMillis()));
            if (extensionId != null) {
                query += "WHERE ID_EXTENSION = :extensionId";
                parameters.addValue("extensionId", extensionId);
            }
            namedParameterJdbcTemplate.update(query, parameters);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during upate of rows from table \"%s\"", tableName), dae);
        }
    }
}
