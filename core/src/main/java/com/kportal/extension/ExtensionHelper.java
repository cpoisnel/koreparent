package com.kportal.extension;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

public class ExtensionHelper {

    public static final String EXTENSION_TEMPLATE = ".jsp";

    public static final String PATH_JSP = "/jsp";

    public static final String PATH_WEB_INF_JSP = "/WEB-INF/jsp";

    public static final String PATH_FO = "/fo";

    public static ExtensionConfigurer getExtensionConfigurer() {
        return (ExtensionConfigurer) ApplicationContextManager.getCoreContextBean(ExtensionConfigurer.ID_BEAN);
    }

    public static ExtensionManager getExtensionManager() {
        return (ExtensionManager) ApplicationContextManager.getCoreContextBean(ExtensionManager.ID_BEAN);
    }

    public static IExtension getExtension(String id) {
        return getExtensionManager().getExtension(id);
    }

    public static String getMessage(String id, String key) {
        if (StringUtils.isNotEmpty(key)) {
            String message = MessageHelper.getMessage(id, key);
            if (StringUtils.isNotEmpty(message)) {
                return message;
            }
        }
        return key;
    }

    public static CoreExtension getCoreExtension() {
        return (CoreExtension) getExtension(ApplicationContextManager.DEFAULT_CORE_CONTEXT);
    }

    public static boolean checkCoreVersion(String coreVersion) {
        boolean versionCompatible = Boolean.TRUE;
        Version v = new Version(coreVersion);
        Version core = new Version(getCoreExtension().getVersion());
        // si la version du core est bien renseignée et n'est pas égale au moins sur une version mineure
        if (v.isValid() && !core.equalsMineur(v)) {
            versionCompatible = Boolean.FALSE;
        }
        return versionCompatible;
    }

    public static String getTemplateExtension(String idExtension, String pathRelatif, boolean front) {
        if (!pathRelatif.endsWith(EXTENSION_TEMPLATE)) {
            pathRelatif += EXTENSION_TEMPLATE;
        }
        if (!pathRelatif.startsWith("/")) {
            pathRelatif = "/" + pathRelatif;
        }
        String path = pathRelatif;
        if (front) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            ContexteUniv ctx = ContexteUtil.getContexteUniv();
            String dossierJspFo = StringUtils.EMPTY;
            if (ctx != null && ctx.getInfosSite() != null) {
                dossierJspFo = ctx.getInfosSite().getJspFo();
                dossierJspFo = ctx.getInfosSite().getJspFo();
            }
            if (StringUtils.isEmpty(dossierJspFo)) {
                dossierJspFo = serviceInfosSite.getPrincipalSite().getJspFo();
            }
            // on a donc normalement une url du type /WEB-INF/jsp/../fo/*.jsp
            if (path.startsWith(PATH_WEB_INF_JSP + "/") && path.contains(PATH_FO + "/")) {
                // on le tranforme en /jsp/extensions/idExt/../*.jsp
                path = StringUtils.replace(path, PATH_FO + "/", "/");
                path = StringUtils.replace(path, PATH_WEB_INF_JSP + "/", WebAppUtil.getRelativeExtensionPath(StringUtils.defaultIfEmpty(idExtension, ApplicationContextManager.DEFAULT_CORE_CONTEXT)) + "/");
                path = dossierJspFo + path;
            } else if (path.startsWith(PATH_JSP + "/") && !dossierJspFo.equals(PATH_JSP)) {
                path = StringUtils.replace(path, PATH_JSP, dossierJspFo);
            }
        } else {
            if (path.startsWith(PATH_WEB_INF_JSP + "/")) {
                path = StringUtils.replace(path, PATH_WEB_INF_JSP + "/", PATH_WEB_INF_JSP + WebAppUtil.getRelativeExtensionPath(StringUtils.defaultIfEmpty(idExtension, ApplicationContextManager.DEFAULT_CORE_CONTEXT)) + "/");
            }
        }
        // si surcharge existe
        if (new File(WebAppUtil.getAbsolutePath() + path).exists()) {
            return path;
        }
        // on teste ensuite à la racine de l'extension
        if (StringUtils.isNotEmpty(idExtension) && getExtension(idExtension) != null) {
            path = getExtension(idExtension).getRelativePath() + pathRelatif;
            if (new File(WebAppUtil.getAbsolutePath() + path).exists()) {
                return path;
            }
        }
        // on test en dernier lieu le core
        if (new File(WebAppUtil.getAbsolutePath() + pathRelatif).exists()) {
            return pathRelatif;
        }
        return StringUtils.EMPTY;
    }

    public static String getTemplateExtension(String idExtension, String pathRelatif) {
        return getTemplateExtension(idExtension, pathRelatif, Boolean.TRUE);
    }

    /**
     * Vérifie si l'extension collaboratif est activée
     * @return True si l'extension est activée
     */
    public static boolean isExtensionActivated(String idExtension) {
        final IExtension extension = ExtensionHelper.getExtension(idExtension);
        if (extension != null) {
            return extension.getEtat() == IExtension.ETAT_ACTIF;
        }
        return false;
    }

}
