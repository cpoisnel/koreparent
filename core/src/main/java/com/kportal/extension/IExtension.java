package com.kportal.extension;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import com.kportal.extension.module.IModule;
import com.kportal.util.StatutsMessage;

/**
 * The Interface IExtension.
 */
public interface IExtension {

    int ETAT_NON_VALIDE = -1;

    int ETAT_NON_ACTIF = 0;

    int ETAT_ACTIF = 1;

    int ETAT_A_RESTAURER = -2;

    int TYPE_NON_PARAMETRABLE_NON_AFFICHABLE = -1;

    int TYPE_NON_PARAMETRABLE_AFFICHABLE = 0;

    int TYPE_PARAMETRABLE = 1;

    /**
     * Sets the id.
     *
     * @param name
     *            the new id
     */
    void setId(String name);

    /**
     * Gets the id.
     *
     * @return the id
     */
    String getId();

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    String getLibelle();

    void setLibelle(String libelle);

    /**
     * Gets the description.
     *
     * @return the description
     */
    String getDescription();

    /**
     * Gets the auteur.
     *
     * @return the auteur
     */
    String getAuteur();

    /**
     * Gets the copyright.
     *
     * @return the copyright
     */
    String getCopyright();

    /**
     * Gets the version.
     *
     * @return the version
     */
    String getVersion();

    /**
     * Gets the version.
     *
     * @return the version
     */
    String getCoreVersion();

    /**
     * Gets the modules.
     *
     * @return the modules
     */
    Collection<IModule> getModules();

    /**
     * Gets the message.
     *
     * @param locale
     *            the locale
     * @param key
     *            the key
     * @return the message
     */
    String getMessage(Locale locale, String key);

    /**
     * Gets all messages.
     *
     * @return the messages
     */
    Properties getMessages(Locale locale);

    /**
     * Gets the property.
     *
     * @param key
     *            the key
     * @return the property
     */
    String getProperty(String key);

    /**
     * Gets all properties.
     *
     * @return the properties
     */
    Properties getProperties();

    /**
     * Gets the relative path.
     *
     * @return the relative path
     */
    String getRelativePath();

    /**
     * sets the relative path.
     *
     */
    void setRelativePath(String path);

    /**
     * Gets the script sql.
     *
     * @return the script sql
     */
    String getScriptSQL();

    /**
     * Sets the script sql.
     *
     * @param scriptSQL
     *            the new script sql
     */
    void setScriptSQL(String scriptSQL);

    int getEtat();

    void setEtat(int etat);

    int getType();

    void setType(int type);

    List<StatutsMessage> getStatuts();

    String getLibelleAffichable(String key);

    String getUrl();

    String getLogo();

    boolean isExterne();

    void setExterne(boolean externe);

    boolean loadSQL();
}
