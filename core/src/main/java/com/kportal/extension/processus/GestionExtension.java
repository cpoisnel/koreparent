package com.kportal.extension.processus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurAsyncException;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.cluster.ClusterHelper;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;
import com.kportal.extension.bean.ExtensionBean;
import com.kportal.extension.bean.ModuleBean;
import com.kportal.extension.exception.NoSuchExtensionException;
import com.kportal.extension.module.IModule;
import com.kportal.extension.service.ServiceExtension;
import com.kportal.extension.service.ServiceModule;
import com.univ.objetspartages.om.AutorisationBean;

public class GestionExtension extends ProcessusBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(GestionExtension.class);

    /** The Constant ECRAN_LISTE. */
    private static final String ECRAN_LISTE = "LISTE";

    public GestionExtension(final InfoBean infoBean) {
        super(infoBean);
    }

    private ServiceExtension getServiceExtension(){
        return ServiceManager.getServiceForBean(ExtensionBean.class);
    }

    private ServiceModule getServiceModule(){
        return ServiceManager.getServiceForBean(ModuleBean.class);
    }

    @Override
    protected boolean traiterAction() throws Exception {
        AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else if (!autorisations.isWebMaster()) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
        } else {
            try {
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                if ("LISTE".equals(action)) {
                    preparerListe();
                } else if ("ACTIVER_EXTENSION".equals(action)) {
                    traiterActivation();
                } else if ("SUPPRIMER_EXTENSION".equals(action)) {
                    traiterSuppression();
                } else if ("ACTIVER_MODULE".equals(action)) {
                    traiterActivation();
                } else if ("RECHARGER".equals(action)) {
                    traiterRechargement(false);
                } else if ("RESTAURER".equals(action)) {
                    traiterRechargement(true);
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final ErreurAsyncException e) {
                throw e;
            } catch (final Exception e) {
                LOGGER.error(e.getMessage(), e);
                infoBean.addMessageErreur(e.toString());
            }
        }
        return etat == FIN;
    }

    /*
     * gere la supression directe d'une extension
     */
    private void traiterSuppression() throws Exception {
        try {
            final String idExtension = infoBean.getString("ID_EXTENSION");
            if (StringUtils.isNotEmpty(idExtension) && StringUtils.isNumeric(idExtension)) {
                ExtensionBean extensionBean = getServiceExtension().getById(Long.valueOf(idExtension));
                if (extensionBean == null || extensionBean.getType() != IExtension.TYPE_PARAMETRABLE) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
                }
                ExtensionHelper.getExtensionManager().removeExtension(extensionBean.getIdBean());
                reset();
            }
            preparerListe();
        } catch (final Exception e) {
            throw new ErreurAsyncException("Une erreur s'est produite", e);
        }
    }

    /*
     * gere l'activation ou la desactivation directe d'un module ou d'une extension
     */
    private void traiterActivation() throws Exception {
        try {
            final String idExtension = infoBean.getString("ID_EXTENSION");
            final String idModule = infoBean.getString("ID_MODULE");
            // traitement de l'extension
            if (StringUtils.isNotEmpty(idExtension)) {
                ExtensionBean extension = getServiceExtension().getById(Long.valueOf(idExtension));
                if (extension == null || extension.getType() != IExtension.TYPE_PARAMETRABLE) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
                }
                if (extension.getEtat() == IExtension.ETAT_ACTIF) {
                    extension.setEtat(IExtension.ETAT_NON_ACTIF);
                } else if (extension.getEtat() == IExtension.ETAT_NON_ACTIF) {
                    extension.setEtat(IExtension.ETAT_ACTIF);
                }
                getServiceExtension().update(extension);
                getServiceModule().updateStateByExtensionId(extension.getEtat(), extension.getIdExtension());
                refresh();
            } else if (StringUtils.isNotEmpty(idModule)) {
                // traitement du module
                final ModuleBean module = getServiceModule().getById(Long.valueOf(idModule));
                if (module == null || module.getType() != IModule.TYPE_PARAMETRABLE) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
                }
                if (module.getEtat() == IModule.ETAT_ACTIF) {
                    module.setEtat(IModule.ETAT_NON_ACTIF);
                } else if (module.getEtat() == IModule.ETAT_NON_ACTIF) {
                    module.setEtat(IModule.ETAT_ACTIF);
                }
                getServiceModule().update(module);
                refresh();
            }
            preparerListe();
        } catch (final Exception e) {
            throw new ErreurAsyncException("Une erreur s'est produite", e);
        }
    }

    private void traiterRechargement(final boolean reset) throws ErreurAsyncException {
        // mise à jour de l'état des modules et extensions
        if (reset) {
            getServiceExtension().changeAllState(IExtension.ETAT_A_RESTAURER);
            getServiceModule().updateStateByExtensionId(IModule.ETAT_A_RESTAURER, null);
        }
        reset();
        try {
            preparerListe();
        } catch (final Exception e) {
            throw new ErreurAsyncException("Une erreur s'est produite", e);
        }
    }

    private void preparerListe() throws Exception {
        final Map<ExtensionBean, List<ModuleBean>> lstExtension = new LinkedHashMap<>();
        List<Integer> types = Arrays.asList(IExtension.TYPE_NON_PARAMETRABLE_AFFICHABLE, IExtension.TYPE_PARAMETRABLE);
        List<ExtensionBean> extensionsByTypes = getServiceExtension().getByTypeOrderByDateLabel(types);
        for (ExtensionBean extensionBean : extensionsByTypes) {
            final List<ModuleBean> lstModule = new ArrayList<>();
            try {
                final ExtensionBean cloneExtention = getServiceExtension().setConfig(extensionBean);
                if (cloneExtention.getEtat() == IExtension.ETAT_ACTIF) {
                    List<ModuleBean> modulesByExtension = getServiceModule().getByExtensionAndTypes(cloneExtention.getIdExtension(), types);
                    for (ModuleBean moduleBean : modulesByExtension) {
                        try {
                            lstModule.add(getServiceModule().setConfig(moduleBean, cloneExtention));
                        } catch (final NoSuchBeanDefinitionException e) {
                            LOGGER.debug("no bean found", e);
                            // on supprime le bean si il n'existe plus
                            getServiceModule().delete(moduleBean.getId());
                        }
                    }
                }
                lstExtension.put(cloneExtention, lstModule);
            } catch (final NoSuchExtensionException e) {
                LOGGER.debug("no bean found", e);
                getServiceExtension().delete(extensionBean.getIdExtension());
            }
        }
        infoBean.set("LISTE_EXTENSIONS", lstExtension);
        ecranLogique = ECRAN_LISTE;
    }

    private void reset() {
        // rechargement du singleton ApplicationContextManager
        ClusterHelper.refresh(ApplicationContextManager.getInstance(), null);
    }

    private void refresh() {
        // rechargement du singleton ExtensionManager
        ClusterHelper.refresh(ExtensionHelper.getExtensionManager(), null);
    }
}
