package com.kportal.extension;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import com.kportal.core.config.ExtensionContextInitializer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.exception.NoSuchBeanException;

public class ExtensionConfigurer extends Observable {

    /** The Constant APP_CTX_FILE. */
    public static final String EXT_CTX_FILE = "ExtensionContext.xml";

    /** The Constant APP_CTX_FILE. */
    public static final String APP_CTX_FILE = "ApplicationContext.xml";

    /** The Constant ADMIN_PATH. */
    public static final String ADMIN_PATH = "/adminsite/";

    /** The Constant SAVE_PATH. */
    public static final String SAVE_PATH = "/extensions/";

    /** The Constant ID_BEAN. */
    protected static final String ID_BEAN = "extensionConfigurer";

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(ExtensionConfigurer.class);

    public Map<String, List<URL>> extensionNames;

    /**
     * Inits the instance.
     */
    public synchronized void refresh() {
        extensionNames = new LinkedHashMap<>();
        // boucle sur le répertoire /extensions
        final String cheminDossierDesExtension = WebAppUtil.getAbsoluteExtensionsPath();
        if (StringUtils.isNotEmpty(cheminDossierDesExtension)) {
            final File repExtensions = new File(cheminDossierDesExtension);
            // presence du repertoire de deploiement
            if (repExtensions.exists() && repExtensions.isDirectory()) {
                // listing des dossiers
                for (final File rep : repExtensions.listFiles()) {
                    if (rep.isDirectory()) {
                        // check de la configuration du dossier
                        final String extensionName = rep.getName();
                        final String pathConfig = WebAppUtil.getExtensionWebInfPath(extensionName) + EXT_CTX_FILE;
                        if (new File(pathConfig).exists()) {
                            if (!extensionNames.containsKey(extensionName)) {
                                extensionNames.put(extensionName, null);
                            } else {
                                LOG.warn("L'extension " + extensionName + " est déjà déclarée");
                            }
                        } else {
                            LOG.warn("Le fichier de configuration " + EXT_CTX_FILE + " de l'extension " + extensionName + " n'existe pas!");
                        }
                    }
                }
            } else {
                LOG.warn("Le répertoire de déploiement des modules applicatifs : " + cheminDossierDesExtension + " n'existe pas");
            }
        } else {
            LOG.warn("Le répertoire de déploiement des modules applicatifs n'est pas renseigné");
        }
        // boucle sur le répertoire /WEB-INF
        final String cheminWebInfApplicatif = WebAppUtil.getWebInfPath();
        if (StringUtils.isNotEmpty(cheminWebInfApplicatif)) {
            final File repWEBINF = new File(cheminWebInfApplicatif);
            // presence du repertoire de deploiement
            if (repWEBINF.exists() && repWEBINF.isDirectory()) {
                // listing des fichiers de type idExtensionContext.xml
                for (final File rep : repWEBINF.listFiles()) {
                    if (rep.isFile() && rep.getName().endsWith(EXT_CTX_FILE)) {
                        final String extensionName = StringUtils.substringBefore(rep.getName(), EXT_CTX_FILE);
                        extensionNames.put(extensionName, null);
                    }
                }
            }
        }
        setChanged();
        notifyObservers();
    }

    /**
     * Creation de la liste des applications contexte par extension. sauvegarde de la liste des urls chargés dans le classloader parent pour suppression
     *
     * @param contexteParent
     *            the contexte parent
     * @return the map
     */
    public Map<String, ApplicationContext> initAllApplicationContext(final ApplicationContext contexteParent) {
        updateClassLoader();
        return updateApplicationContext(contexteParent);
    }

    /**
     * On ne devrait pas manipuler le classLoader comme on le fait...
     */
    private void updateClassLoader() {
        for (final String extensionName : extensionNames.keySet()) {
            final List<URL> lstUrlModule = new ArrayList<>();
            try {
                if (isConfExterne(extensionName)) {
                    // chargement des classes
                    final File classFile = new File(WebAppUtil.getExtensionClassPath(extensionName));
                    if (classFile.exists() && classFile.isDirectory()) {
                        lstUrlModule.add(classFile.toURI().toURL());
                        ClassLoaderHelper.addRessource(classFile, "class", lstUrlModule);
                    }
                    // chargement des jars
                    final File libFile = new File(WebAppUtil.getExtensionLibPath(extensionName));
                    if (libFile.exists() && libFile.isDirectory()) {
                        ClassLoaderHelper.addRessource(libFile, "jar", lstUrlModule);
                    }
                    // ajout des ressources dans le class loader applicatif
                    final URLClassLoader currentClassLoader = (URLClassLoader) this.getClass().getClassLoader();
                    for (final URL url : lstUrlModule) {
                        ClassLoaderHelper.addURLToClassLoader(currentClassLoader, url);
                    }
                }
                extensionNames.put(extensionName, lstUrlModule);
            } catch (final FileNotFoundException e) {
                LOG.error("unable to find any configuration file for the given extension", e);
            } catch (final IOException e) {
                LOG.error("unable to add url to the classLoader", e);
            }
        }
    }

    private boolean isConfExterne(final String extensionName) throws FileNotFoundException {
        File extCtx = getExtensionContextFile(extensionName);
        return !extCtx.getPath().startsWith(WebAppUtil.getWebInfPath());
    }

    private File getExtensionContextFile(final String extensionName) throws FileNotFoundException {
        File extCtxFile = new File(WebAppUtil.getWebInfPath() + extensionName + EXT_CTX_FILE);
        final String cheminWebInfExtension = WebAppUtil.getExtensionWebInfPath(extensionName);
        if (!extCtxFile.exists()) {
            // soit dans /extensions/name/WEB-INF/ExtentionContext.xml
            extCtxFile = new File(cheminWebInfExtension + EXT_CTX_FILE);
            if (!extCtxFile.exists()) {
                throw new FileNotFoundException("Le fichier de configuration " + EXT_CTX_FILE + " de l'extension " + extensionName + " n'existe pas!");
            }
        }
        return extCtxFile;
    }

    private Map<String, ApplicationContext> updateApplicationContext(final ApplicationContext contexteParent) {
        final Map<String, ApplicationContext> extensionsCtx = new HashMap<>();
        final Collection<String> extensionsToRemove = new ArrayList<>();
        for (final String idExtension : extensionNames.keySet()) {
            try {
                // lecture du fichier de configuration Spring
                // soit dans /WEB-INF/nameExtentionContext.xml
                final File extCtxFile = getExtensionContextFile(idExtension);
                // creation et stockage du contexte Spring
                final ClassPathXmlApplicationContext moduleCtx = new ClassPathXmlApplicationContext();
                final ExtensionContextInitializer initializer = new ExtensionContextInitializer(idExtension);
                initializer.initialize(moduleCtx);
                moduleCtx.setConfigLocation("file:" + extCtxFile.getAbsolutePath());
                moduleCtx.setParent(contexteParent);
                moduleCtx.refresh();

                // controle la présence d'un bean de type extension du même nom
                if (moduleCtx.containsBean(idExtension)) {
                    if (!(moduleCtx.getBean(idExtension) instanceof IExtension)) {
                        throw new NoSuchBeanException("Le bean " + idExtension + " du fichier " + extCtxFile.getName() + " (ne contient aucun bean de type Extension du même nom");
                    }
                } else {
                    throw new NoSuchBeanException("Le fichier de configuration " + extCtxFile.getName() + " de l'extension " + idExtension + " ne contient aucun bean de type Extension du même nom");
                }
                extensionsCtx.put(idExtension, moduleCtx);
            } catch (final FileNotFoundException | NoSuchBeanException | BeansException e) {
                LOG.error("Erreur dans le chargement de l'extension " + idExtension, e);
                // on supprime l'extension non chargée
                extensionsToRemove.add(idExtension);
            }
        }
        for (String idExtension : extensionsToRemove) {
            extensionNames.remove(idExtension);
        }
        return extensionsCtx;
    }
}
