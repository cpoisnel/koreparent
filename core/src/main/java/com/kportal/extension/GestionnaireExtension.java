package com.kportal.extension;

import com.kportal.extension.module.composant.Composant;
import com.univ.objetspartages.om.AutorisationBean;

public class GestionnaireExtension extends Composant {

    @Override
    public boolean isVisible(AutorisationBean autorisation) {
        return autorisation != null && autorisation.isWebMaster();
    }
}
