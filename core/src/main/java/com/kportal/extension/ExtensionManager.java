package com.kportal.extension;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.database.MySQLDumpWriter;
import com.jsbsoft.jtf.database.MySQLScriptLoader;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.bean.ExtensionBean;
import com.kportal.extension.service.ServiceExtension;
import com.kportal.extension.service.ServiceModule;
import com.kportal.util.compress.Zip;
import com.univ.utils.ContexteDao;
import com.univ.utils.FileUtil;

import fr.kosmos.cluster.api.Cluster;
import fr.kosmos.cluster.api.MessageHandler;

/**
 * The Class ModuleManager.
 */
public class ExtensionManager extends Observable implements Observer, MessageHandler<Serializable> {

    protected static final String ID_BEAN = "extensionManager";

    private static final String POINT_SQL = ".sql";

    private static final String _DATA = "_data";

    private static final Logger LOGGER = LoggerFactory.getLogger(ExtensionManager.class);

    public ExtensionConfigurer extensionConfigurer;

    private Cluster cluster;

    private Map<String, IExtension> extensions = new HashMap<>();

    private Version core;

    private ServiceExtension serviceExtension;

    private ServiceModule serviceModule;

    public void init() {
        extensionConfigurer.addObserver(this);
        if (cluster != null) {
            cluster.registerService(this);
        }
    }

    public synchronized void refresh() {
        extensions = new HashMap<>();
        // chargement du core en premier
        loadExtension(ApplicationContextManager.DEFAULT_CORE_CONTEXT);
        // Si problème de chargement du core on arrete le tomcat
        if (extensions.get(ApplicationContextManager.DEFAULT_CORE_CONTEXT) == null) {
            throw new RuntimeException("Impossible de démarrer l'application sans extension core");
        }
        // boucle sur les autres contexes
        for (final String idAppCtx : ApplicationContextManager.getAllApplicationContext().keySet()) {
            if (!idAppCtx.equals(ApplicationContextManager.DEFAULT_CORE_CONTEXT)) {
                loadExtension(idAppCtx);
            }
        }
        // nettoyage de la base contenant les extensions et les modules
        cleanDB();
        // notification pour les managers
        setChanged();
        notifyObservers();
        LOGGER.info("Chargement des extensions OK");
    }

    private void loadExtension(final String idAppCtx) {
        // on ne charge qu'une extension par contexte
        final ApplicationContext appCtx = ApplicationContextManager.getAllApplicationContext().get(idAppCtx);
        final Map<String, IExtension> exts = appCtx.getBeansOfType(IExtension.class);
        for (final String name : exts.keySet()) {
            // l'extension à le même id que le contexte
            if (name.equals(idAppCtx)) {
                final IExtension extension = exts.get(name);
                extension.setId(name);
                extension.setExterne(isExterne(name));
                // calcul automatique du path pour les extensions "externes" (dans un répertoire à part ex /extensions)
                if (extension.isExterne()) {
                    extension.setRelativePath(WebAppUtil.getRelativeExtensionPath(name));
                } else {
                    // les extensions "internes" ne sont pas parametrables (desactivables et supprimables)
                    extension.setType(IExtension.TYPE_NON_PARAMETRABLE_AFFICHABLE);
                }
                try {
                    // chargement
                    initExtension(extension);
                    // référencement de l'extension dans la liste des extensions chargées
                    if (isCore(extension) || extension.getEtat() == IExtension.ETAT_ACTIF) {
                        extensions.put(name, extension);
                    }
                } catch (final Exception e) {
                    LOGGER.error("Erreur de chargement de l'extension id=" + extension.getId(), e);
                }
            }
        }
    }

    private boolean checkVersion(final IExtension extension) {
        boolean versionCompatible = Boolean.TRUE;
        final Version v = new Version(extension.getCoreVersion());
        // si la version du core est bien renseignée
        if (v.isValid()) {
            if (core.equalsMineur(v)) {
                // Différence de numéro de version de maintenance : une alerte indique qu'une version plus récente est disponible
                if (core.greaterMaintenance(v)) {
                    LOGGER.warn("Une version plus récente de l'extension " + extension.getId() + " est disponible");
                }
            }
            // Différence de numéro de version mineure / majeure : l'extension est désactivée
            else {
                LOGGER.error("La version de l'extension " + extension.getId() + " n'est plus compatible avec celle du socle, l'extension a été automatiquement désactivée");
                versionCompatible = Boolean.FALSE;
            }
        } else {
            LOGGER.warn("Impossible de valider la compatibilité de la version de l'extension " + extension.getId());
        }
        return versionCompatible;
    }

    private boolean isExterne(final String name) {
        final File repExtension = new File(WebAppUtil.getAbsoluteExtensionsPath() + name);
        return repExtension.exists();
    }

    private boolean isCore(final IExtension extension) {
        return extension.getId().equals(ApplicationContextManager.DEFAULT_CORE_CONTEXT);
    }

    private void cleanDB() {
        List<ExtensionBean> allExtensions = serviceExtension.getAll();
        for (ExtensionBean extensionBean : allExtensions) {
            if (!ApplicationContextManager.containsBean(extensionBean.getIdBean(), extensionBean.getIdBean())) {
                serviceModule.deleteByExtensionId(extensionBean.getIdExtension());
                serviceExtension.delete(extensionBean.getIdExtension());
            }
        }
    }

    private void initExtension(final IExtension extension) throws Exception {
        ExtensionBean extensionBean = null;
        // chargement en base
        try {
            extensionBean = serviceExtension.getById((long) extension.getId().hashCode());
        } catch (DataSourceException e) {
            LOGGER.debug(String.format("Une erreur est survenue lors de la récupération de l'extension %d", extension.getId().hashCode()), e);
        }
        if (extensionBean == null) {
            // si l'extension n'existe pas en base on crée un enregistrement en executant les scripts SQL d'initialisation
            extensionBean = new ExtensionBean();
            extensionBean.setIdExtension((long) extension.getId().hashCode());
            extensionBean.setTables(loadSQL(extension, "", ""));
            extensionBean.setEtat(extension.getEtat());
            extensionBean.setType(extension.getType());
            extensionBean.setIdBean(extension.getId());
            extensionBean.setLibelle(StringUtils.isNotEmpty(extension.getLibelle()) ? extension.getLibelle() : extension.getId());
            extensionBean.setVersion(extension.getVersion());
            final GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(System.currentTimeMillis());
            // la date de création du core est volontairement ramené à 0h pour être en premier dans la liste
            if (isCore(extension)) {
                calendar.set(Calendar.HOUR, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
            }
            extensionBean.setDateCreation(new Date(calendar.getTimeInMillis()));
            extensionBean.setDateModification(new Date(calendar.getTimeInMillis()));
            serviceExtension.add(extensionBean);
        }
        // sauvegarde de la version précedente
        final String oldVersion = extensionBean.getVersion();
        // check extension version
        boolean versionCompatible = Boolean.TRUE;
        if (isCore(extension)) {
            core = new Version(extension.getVersion());
        } else {
            versionCompatible = checkVersion(extension);
        }
        // restauration ou mise à jour de l'état, du type et/ou version
        if (!versionCompatible || extensionBean.getEtat() != extension.getEtat() || extensionBean.getType() != extension.getType() || !extensionBean.getVersion().equals(extension.getVersion())) {
            if (extensionBean.getEtat() == IExtension.ETAT_A_RESTAURER) {
                extensionBean.setEtat(extension.getEtat());
                extensionBean.setIdBean(extension.getId());
                extensionBean.setLibelle(StringUtils.isNotEmpty(extension.getLibelle()) ? extension.getLibelle() : extension.getId());
            }
            if (versionCompatible) {
                if (!extensionBean.getVersion().equals(extension.getVersion()) || extensionBean.getEtat() == IExtension.ETAT_NON_VALIDE) {
                    // mise à jour SQL
                    extensionBean.setTables(loadSQL(extension, extensionBean.getTables(), extensionBean.getVersion()));
                    extensionBean.setVersion(extension.getVersion());
                    if (extensionBean.getEtat() == IExtension.ETAT_NON_VALIDE) {
                        extensionBean.setEtat(extension.getEtat());
                    }
                }
                if (extensionBean.getType() != extension.getType()) {
                    extensionBean.setType(extension.getType());
                }
            } else {
                extensionBean.setEtat(IExtension.ETAT_NON_VALIDE);
            }
            extensionBean.setDateModification(new Date(System.currentTimeMillis()));
            serviceExtension.update(extensionBean);
        }
        // on met à jour l'état en mémoire
        extension.setEtat(extensionBean.getEtat());
        // point d'extension pour configuration automatique
        if (extension.getEtat() != IExtension.ETAT_NON_VALIDE) {
            for (final IExtensionConfig config : ApplicationContextManager.getBeansOfType(extension.getId(), IExtensionConfig.class).values()) {
                config.init(extension, new Version(oldVersion));
            }
        }
    }

    private String loadSQL(final IExtension extension, final String tablesExistantes, final String versionCourante) throws Exception {
        final Set<String> lTables = new HashSet<>();
        if (StringUtils.isNotBlank(tablesExistantes)) {
            lTables.addAll(Arrays.asList(tablesExistantes.split(";")));
        }
        File sqlFile = null;
        boolean doOnlyCreateTable = false;
        try (ContexteDao ctx = new ContexteDao()) {
            final MySQLScriptLoader scriptLoader = new MySQLScriptLoader(ctx.getConnection());
            // chargement du sql d'initialisation pour la structure des tables si pas de version courante
            if (StringUtils.isBlank(versionCourante)) {
                doOnlyCreateTable = true;
                sqlFile = new File(WebAppUtil.getExtensionSQLPath(extension.getId()) + extension.getId() + POINT_SQL);
            } else {
                sqlFile = new File(WebAppUtil.getExtensionSQLPath(extension.getId()) + extension.getId() + "_" + versionCourante + "_" + extension.getVersion() + POINT_SQL);
            }
            if (sqlFile.exists()) {
                try (FileReader fileReader = new FileReader(sqlFile)) {
                    scriptLoader.setStopOnError(true);
                    scriptLoader.setDoOnlyCreateTable(doOnlyCreateTable);
                    scriptLoader.runScript(fileReader);
                    lTables.addAll(scriptLoader.getCreatedTables());
                    lTables.addAll(scriptLoader.getExistingTables());
                    LOGGER.info("Chargement du fichier " + sqlFile.getName() + " OK");
                    if (scriptLoader.getCreatedTables().size() > 0) {
                        LOGGER.info("Table(s) créée(s) : " + scriptLoader.getCreatedTables().toString());
                    }
                    if (scriptLoader.getExistingTables().size() > 0) {
                        LOGGER.info("Table(s) existante(s) : " + scriptLoader.getExistingTables().toString());
                    }
                }
            }
            // Chargement des données d'initialisation si pas de version courante
            if (StringUtils.isBlank(versionCourante) && scriptLoader.getExistingTables().isEmpty()) {
                sqlFile = new File(WebAppUtil.getExtensionSQLPath(extension.getId()) + extension.getId() + _DATA + POINT_SQL);
                if (sqlFile.exists()) {
                    try (FileReader fileReader = new FileReader(sqlFile)) {
                        scriptLoader.setStopOnError(false);
                        scriptLoader.setDoOnlyCreateTable(false);
                        scriptLoader.runScript(fileReader);
                        LOGGER.info("Chargement du fichier " + sqlFile.getName() + " OK");
                    }
                }
            }
        }
        String tables = "";
        for (final String table : lTables) {
            if (tables.length() > 0) {
                tables += ";";
            }
            tables += table;
        }
        return tables;
    }

    /**
     * Gets the modules.
     *
     * @return the modules
     */
    public Map<String, IExtension> getExtensions() {
        return extensions;
    }

    /**
     * Gets the extension.
     *
     * @param name
     *            the name
     * @return the extension
     */
    public IExtension getExtension(final String name) {
        return extensions.get(StringUtils.defaultIfBlank(name, ApplicationContextManager.DEFAULT_CORE_CONTEXT));
    }

    @Override
    public void update(final Observable o, final Object arg) {
        ApplicationContextManager.refresh(extensionConfigurer);
        refresh();
    }

    public ExtensionConfigurer getExtensionConfigurer() {
        return extensionConfigurer;
    }

    public void setExtensionConfigurer(final ExtensionConfigurer extensionConfigurer) {
        this.extensionConfigurer = extensionConfigurer;
    }

    public void removeExtension(final String idBean) throws Exception {
        // on cible l'extention directement, ne pas utiliser la méthode getExtension
        IExtension extension = extensions.get(idBean);
        // l'extension n'est pas chargée (inactive), on va la lire directement dans le contexte
        if (extension == null) {
            extension = (IExtension) ApplicationContextManager.getBean(idBean, idBean);
        }
        final ExtensionBean extensionDB = serviceExtension.getById((long) extension.getId().hashCode());
        // garde fou pour éviter la suppression du core
        if (extensionDB == null) {
            throw new ErreurDonneeNonTrouve("impossible de supprimer une extension inexistante");
        }
        if (extensionDB.getIdBean().equals(ApplicationContextManager.DEFAULT_CORE_CONTEXT)) {
            throw new Exception("Impossible de supprimer l'extension core");
        }
        List<String> tables = new ArrayList<>();
        if (StringUtils.isNotEmpty(extensionDB.getTables())) {
            tables = Arrays.asList(extensionDB.getTables().split(";"));
        }
        // sauvegarde des tables de l'extension dans le fichier sql d'initialisation
        if (tables.size() > 0) {
            final String path = WebAppUtil.getExtensionSQLPath(extension.getId()) + extension.getId() + _DATA + POINT_SQL;
            dumpTableSQL(extension, tables, path);
        }
        // point d'extension pour configuration automatique
        for (final IExtensionConfig config : ApplicationContextManager.getBeansOfType(extension.getId(), IExtensionConfig.class).values()) {
            config.clean(extension);
        }
        // sauvegarde du dossier de l'extension
        saveFolder(extension);
        // suppression des tables de l'extension
        if (extension.loadSQL() && tables.size() > 0) {
            removeTablesSQL(tables);
        }
        // supression des entrees de module en base
        serviceModule.deleteByExtensionId(extensionDB.getIdExtension());
        serviceExtension.delete(extensionDB.getIdExtension());
    }

    private void saveFolder(final IExtension extension) throws IOException {
        try {
            final File rep = new File(WebAppUtil.getAbsolutePath() + extension.getRelativePath());
            final File archive = new File(FileUtil.mkdir(WebAppUtil.getSauvegardePath() + ExtensionConfigurer.SAVE_PATH) + extension.getId());
            Zip.compress(rep, archive);
            // suppression du repertoire de l'extension
            FileDeleteStrategy.FORCE.delete(rep);
        } catch (final IOException e) {
            throw new IOException("Impossible de sauvegarder le répertoire de l'extension id=" + extension.getId(), e);
        }
    }

    private void removeTablesSQL(final List<String> tables) {
        try (ContexteDao ctx = new ContexteDao()) {
            final MySQLScriptLoader scriptLoader = new MySQLScriptLoader(ctx.getConnection());
            for (final String table : tables) {
                scriptLoader.executeDropTable(table);
            }
        }
    }

    private void dumpTableSQL(final IExtension extension, final List<String> tables, final String path) throws IOException {
        try (ContexteDao ctx = new ContexteDao()) {
            final MySQLDumpWriter dumpWriter = new MySQLDumpWriter();
            dumpWriter.setExportStructure(false);
            dumpWriter.dumpDB(tables, path);
        } catch (final IOException e) {
            throw new IOException("Impossible de sauvegarder les tables SQL de l'extension id=" + extension.getId(), e);
        }
    }

    @Override
    public void handleMessage(final Serializable message) {
        refresh();
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(final Cluster cluster) {
        this.cluster = cluster;
    }

    public void setServiceExtension(ServiceExtension serviceExtension) {
        this.serviceExtension = serviceExtension;
    }

    public void setServiceModule(ServiceModule serviceModule) {
        this.serviceModule = serviceModule;
    }
}
