package com.kportal.extension.servlet;

import java.util.Collection;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.kportal.core.context.ContextLoaderListener;
import com.kportal.extension.module.AbstractBeanManager;
import com.kportal.servlet.ExtensionServlet;

public class ServletDeclarationManager extends AbstractBeanManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "servletDeclarationManager";

    @Override
    public void refresh() {
        final Collection<ExtensionServlet> servletDeclarations = ClassBeanManager.getInstance().getBeanOfType(ExtensionServlet.class);
        for (final ExtensionServlet servletDeclaration : servletDeclarations) {
            if (servletDeclaration.isActive()) {
                ContextLoaderListener.ajouterServlet(servletDeclaration);
            }
        }
    }
}
