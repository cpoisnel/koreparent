package com.kportal.extension.exception;

/**
 * Created on 27/12/2015.
 */
public class NoSuchBeanException extends Exception {

    private static final long serialVersionUID = 7664969446734168368L;

    public NoSuchBeanException() {
        super();
    }

    public NoSuchBeanException(String message) {
        super(message);
    }

    public NoSuchBeanException(String message, Throwable cause) {
        super(message, cause);
    }
}
