package com.kportal.extension.exception;

public class NoSuchExtensionException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1432058190890100451L;

    public NoSuchExtensionException() {
        super();
    }

    public NoSuchExtensionException(final String message) {
        super(message);
    }
}
