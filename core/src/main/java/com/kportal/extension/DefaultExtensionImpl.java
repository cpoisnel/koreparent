package com.kportal.extension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.MessageLoaderUtil;
import com.kportal.core.config.PropertyConfigurerUtil;
import com.kportal.core.context.BeanUtil;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.ModuleHelper;
import com.kportal.util.StatutsMessage;

/**
 * The Class Module.
 */
public class DefaultExtensionImpl implements IExtension {

    /** The id. */
    public String id = "";

    /** The libelle. */
    public String libelle = "";

    /** The description. */
    public String description = "";

    /** The auteur. */
    public String auteur = "";

    /** The copyright. */
    public String copyright = "";

    /** The version. */
    public String version = "";

    /** The version. */
    public String coreVersion = "";

    /** The version. */
    public String scriptSQL = "";

    public int etat = IExtension.ETAT_ACTIF;

    public int type = IExtension.TYPE_PARAMETRABLE;

    public String url = "";

    public String logo = "";

    public String relativePath = "";

    public boolean loadSQL = Boolean.TRUE;

    public boolean externe = Boolean.TRUE;

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    @Override
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    @Override
    public String getLibelle() {
        return libelle;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    @Override
    public String getLibelleAffichable(String key) {
        return ExtensionHelper.getMessage(getId(), key);
    }

    /**
     * Sets the libelle.
     *
     * @param libelle
     *            the new libelle
     */
    @Override
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the auteur.
     *
     * @return the auteur
     */
    @Override
    public String getAuteur() {
        return auteur;
    }

    /**
     * Sets the auteur.
     *
     * @param auteur
     *            the new auteur
     */
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    /**
     * Gets the copyright.
     *
     * @return the copyright
     */
    @Override
    public String getCopyright() {
        return copyright;
    }

    /**
     * Sets the copyright.
     *
     * @param copyright
     *            the new copyright
     */
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IExtension#getComposants()
     */
    @Override
    public Collection<IModule> getModules() {
        ArrayList<IModule> res = new ArrayList<>();
        for (String key : ModuleHelper.getModuleManager().getModules().keySet()) {
            if (id.equals(BeanUtil.getIdExtensionFromKey(key))) {
                res.add(ModuleHelper.getModuleManager().getModules().get(key));
            }
        }
        return res;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IExtension#getMessage(java.util.Locale, java.lang.String)
     */
    @Override
    public String getMessage(Locale locale, String key) {
        String res = locale.getLanguage();
        if (StringUtils.isNotEmpty(locale.getCountry())) {
            res += "_" + locale.getCountry();
        }
        return getMessage(res, key);
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IExtension#getMessage(java.lang.String, java.lang.String)
     */

    /**
     * Gets the message.
     *
     * @param langue
     *            the langue
     * @param key
     *            the key
     * @return the message
     */
    public String getMessage(String langue, String key) {
        return MessageLoaderUtil.getMessage(id, langue, key);
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IExtension#getProperty(java.lang.String)
     */
    @Override
    public String getProperty(String key) {
        return PropertyConfigurerUtil.getProperty(id, key);
    }

    /* (non-Javadoc)
     * @see com.kportal.extension.IExtension#getRelativePath()
     */
    @Override
    public String getRelativePath() {
        return relativePath;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    @Override
    public String getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the new version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Gets the script sql.
     *
     * @return the script sql
     */
    @Override
    public String getScriptSQL() {
        return scriptSQL;
    }

    /**
     * Sets the script sql.
     *
     * @param scriptSQL
     *            the new script sql
     */
    @Override
    public void setScriptSQL(String scriptSQL) {
        this.scriptSQL = scriptSQL;
    }

    @Override
    public int getEtat() {
        return etat;
    }

    @Override
    public void setEtat(int etat) {
        this.etat = etat;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public void setType(int type) {
        this.type = type;
    }

    @Override
    public Properties getMessages(Locale locale) {
        return MessageLoaderUtil.getMessages(id, locale);
    }

    @Override
    public Properties getProperties() {
        return PropertyConfigurerUtil.getProperties(id);
    }

    @Override
    public List<StatutsMessage> getStatuts() {
        List<StatutsMessage> statuts = new ArrayList<>();
        for (IModule module : getModules()) {
            statuts.addAll(module.getStatuts());
        }
        return statuts;
    }

    @Override
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public boolean loadSQL() {
        return loadSQL;
    }

    public void setLoadSQL(boolean loadSQL) {
        this.loadSQL = loadSQL;
    }

    @Override
    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    @Override
    public boolean isExterne() {
        return externe;
    }

    @Override
    public void setExterne(boolean externe) {
        this.externe = externe;
    }

    @Override
    public String getCoreVersion() {
        return coreVersion;
    }

    public void setCoreVersion(String coreVersion) {
        this.coreVersion = coreVersion;
    }
}
