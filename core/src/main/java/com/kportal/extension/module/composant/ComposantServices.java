package com.kportal.extension.module.composant;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.ModuleHelper;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;

public class ComposantServices extends ComposantAdministration {

    public static final String ID_BEAN = "composantServices";

    public static final String ID_PERMISSION = "SERVICE";

    /**
     * La permission permettant pour gérer les services
     */
    public static final String CODE_PERMISSION = "gestion";

    /**
     * L'action de gestion
     */
    public static final String CODE_ACTION_GESTION = "G";

    public static IModule getModule() {
        return ModuleHelper.getModule(ApplicationContextManager.DEFAULT_CORE_CONTEXT, ID_BEAN);
    }

    /**
     * Retourne la permission de gestion des services
     */
    public static PermissionBean getPermissionGestion() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, CODE_ACTION_GESTION);
    }

    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        boolean isVisible = Boolean.FALSE;
        if (isAccessibleBO() && autorisation != null) {
            isVisible = autorisation.possedePermission(getPermissionGestion());
        }
        return isVisible;
    }

    @Override
    public boolean isActionVisible(final AutorisationBean autorisation, final String code) {
        return isVisible(autorisation);
    }
}
