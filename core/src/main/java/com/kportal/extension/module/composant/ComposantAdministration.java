package com.kportal.extension.module.composant;

import com.univ.objetspartages.om.AutorisationBean;

public class ComposantAdministration extends Composant {

    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        return autorisation != null && autorisation.isWebMaster();
    }
}
