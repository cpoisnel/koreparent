package com.kportal.extension.module.plugin.toolbox;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.tag.interpreteur.InterpreteurTag;
import com.kportal.tag.util.ContexteTagUtil;

/**
 * Classe gérant l'interpretation des tags. Il récupère l'ensemble des beans implémentants l'interface {@link InterpreteurTag} et se charge de remplacer le code du tag par
 * l'interpretation.
 *
 * @author olivier.camon
 *
 */
public class PluginTagHelper {

    public static PluginTagManager getTagManager() {
        return (PluginTagManager) ApplicationContextManager.getCoreContextBean(PluginTagManager.ID_BEAN);
    }

    /**
     * Interprete l'ensemble des tags défini dans l'application dans le contexte par défaut.
     *
     * @param texte
     *            le texte devant être parser
     * @return le texte interpreté
     * @throws Exception
     *             il est possible que les tags lèvent des exceptions
     */
    public static String interpreterTags(String texte) throws Exception {
        return interpreterTags(texte, ContexteTagUtil.DEFAUT);
    }

    /**
     *
     * Parse le texte fourni en paramètre pour voir si il contient un tag Si un ou plusieurs tags sont présent, on remplace le code du tag par son interpertation exple :
     * "lorem ipsum [foo]bar[/foo]" tag -> foo present, devient "lorem ipsum foo bar"
     *
     * @param texte
     *            le texte à parser
     * @param typeInterpretation
     *            : le contexte d'interpretation : indexation, defaut, mobile...
     * @return le texte avec les tags interprété
     * @throws Exception
     *             lors de l'interpretation, on peut lever des exceptions
     */
    public static String interpreterTags(String texte, String typeInterpretation) throws Exception {
        final List<IPluginTag> tagDuType = getTagsList(typeInterpretation);
        if (!tagDuType.isEmpty()) {
            for (IPluginTag tagCourant : tagDuType) {
                String texteAInterpreter = tagCourant.extraitTagDuContenu(texte);
                while (StringUtils.isNotEmpty(texteAInterpreter)) {
                    String texteInterpreter = tagCourant.interpreteTag(texteAInterpreter);
                    texteInterpreter = interpreterTags(texteInterpreter, typeInterpretation);
                    texte = StringUtils.replace(texte, texteAInterpreter, texteInterpreter);
                    texteAInterpreter = tagCourant.extraitTagDuContenu(texte);
                }
            }
        }
        return texte;
    }

    private static List<IPluginTag> getTagsList(final String typeInterpretation) {
        final List<IPluginTag> tagDuType = new ArrayList<>();
        tagDuType.addAll(getTagManager().getTagsParType().get(typeInterpretation));
        Collections.sort(tagDuType, new Comparator<IPluginTag>() {
            @Override
            public int compare(final IPluginTag tag1, final IPluginTag tag2) {
                return StringUtils.length(tag2.getIdentifiantTag()) - StringUtils.length(tag1.getIdentifiantTag());
            }
        });
        return tagDuType;
    }

    public static String getReferencesTags(String texte) {
        Set<String> referencesParTag = new HashSet<>();
        Collection<IPluginTag> tagsReferences = getTagManager().getTagsParType().get(ContexteTagUtil.DEFAUT);
        for (IPluginTag tagRef : tagsReferences) {
            String texteAInterpreter = tagRef.extraitTagDuContenu(texte);
            while (StringUtils.isNotEmpty(texteAInterpreter)) {
                String texteInterpreter = tagRef.getReferenceTag(texteAInterpreter);
                texteInterpreter = getReferencesTags(texteInterpreter);
                referencesParTag.add(texteInterpreter);
                texte = StringUtils.replace(texte, texteAInterpreter, texteInterpreter);
                texteAInterpreter = tagRef.extraitTagDuContenu(texte);
            }
        }
        StringBuilder references = new StringBuilder("");
        if (!referencesParTag.isEmpty()) {
            for (String ref : referencesParTag) {
                references.append(ref);
            }
        }
        return references.toString();
    }
}
