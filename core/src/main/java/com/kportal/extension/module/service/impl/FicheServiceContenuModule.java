package com.kportal.extension.module.service.impl;

import java.util.Collection;

import org.apache.commons.lang3.ArrayUtils;

import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.extension.module.IModule;
import com.univ.objetspartages.om.ReferentielObjets;

public class FicheServiceContenuModule extends DefaultServiceContenuModule {

    @Override
    public void init() {
        modules = ReferentielObjets.getObjetsPartagesTries();
    }

    @Override
    public TypeContenu getTypeContenu() {
        return TypeContenu.FICHE;
    }

    @Override
    public String[] recupereParamsExport(final IModule moduleAExporter, final Collection<IModule> modules) {
        String[] paramsExport = null;
        if (modules.contains(moduleAExporter)) {
            paramsExport = ArrayUtils.add(paramsExport, ((Objetpartage) moduleAExporter).getCodeObjet());
        }
        return paramsExport;
    }
}
