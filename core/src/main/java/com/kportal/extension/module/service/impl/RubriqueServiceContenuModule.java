package com.kportal.extension.module.service.impl;

public class RubriqueServiceContenuModule extends DefaultServiceContenuModule {

    public static final String ID_BEAN = "serviceContenuRubrique";

    @Override
    public TypeContenu getTypeContenu() {
        return TypeContenu.TECHNIQUE;
    }
}
