package com.kportal.extension.module;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.cache.CacheLoaderManager;
import com.kportal.core.autorisation.Permission;
import com.kportal.core.context.BeanUtil;
import com.kportal.core.context.OverridedContextBean;
import com.kportal.extension.ExtensionManager;
import com.kportal.extension.IExtension;
import com.kportal.extension.bean.ModuleBean;
import com.kportal.extension.service.ServiceModule;

/**
 * The Class ModuleManager.
 */
public class ModuleManager extends Observable implements Observer {

    public static final String ID_BEAN = "moduleManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleManager.class);

    private ExtensionManager extensionManager;

    private CacheLoaderManager cacheLoaderManager;

    private ServiceModule serviceModule;

    /** The modules. */
    private Map<String, IModule> modules;

    public void setServiceModule(final ServiceModule serviceModule) {
        this.serviceModule = serviceModule;
    }

    public void init() {
        cacheLoaderManager.addObserver(this);
    }

    public void refresh() {
        modules = new HashMap<>();
        for (final IExtension extension : extensionManager.getExtensions().values()) {
            final ApplicationContext appCtx = ApplicationContextManager.getApplicationContext(extension.getId());
            // on récupère le nom du bean spring
            for (Map.Entry<String, IModule> beanByName : appCtx.getBeansOfType(IModule.class).entrySet()) {
                final IModule module = beanByName.getValue();
                module.setId(beanByName.getKey());
                module.setIdExtension(extension.getId());
                module.setLibelleExtension(extension.getLibelle());
                // set id module et extension sur les permissions
                for (final Permission permission : module.getPermissions()) {
                    if (StringUtils.isEmpty(permission.getId())) {
                        permission.setId(module.getId());
                    }
                    permission.setIdModule(module.getId());
                    permission.setIdExtension(module.getIdExtension());
                }
                try {
                    loadModuleDB(module);
                    // on charge temporairement l'ensemble des modules
                    modules.put(BeanUtil.getBeanKey(beanByName.getKey(), extension.getId()), module);
                } catch (final Exception e) {
                    LOGGER.debug("unable to load the module", e);
                    LOGGER.warn("Erreur de chargement du module id=" + module.getId() + ", extension=" + extension.getId() + " : " + e.getMessage());
                }
            }
        }
        // nettoyage des beans obsolètes
        cleanDB();
        // chargement définitif des modules
        loadModules();
        // notification des observers
        setChanged();
        notifyObservers();
        LOGGER.info("Chargement des modules OK");
    }

    private void loadModules() {
        // liste des modules surchargés à désactiver
        final List<String> aDecharger = new ArrayList<>();
        // liste des modules inactifs à désactiver
        final List<String> aDesactiver = new ArrayList<>();
        // boucle sur l'ensemble des modules chargés
        for (final IModule module : modules.values()) {
            // module actif
            if (module.getEtat() == IModule.ETAT_ACTIF) {
                // si c'est un bean qui surcharge un autre
                if (module instanceof OverridedContextBean) {
                    // cle du bean a surcharger
                    final String idBeanOver = BeanUtil.getBeanKey(((OverridedContextBean) module).getIdBeanToOverride(), ((OverridedContextBean) module).getIdExtensionToOverride());
                    // si le bean a surcharger existe
                    if (modules.get(idBeanOver) != null) {
                        // on garde une trace du module à décharger
                        aDecharger.add(idBeanOver);
                    }
                }
            }
            // non actif à désactiver
            else {
                aDesactiver.add(BeanUtil.getBeanKey(module.getId(), module.getIdExtension()));
            }
        }
        // mise à jour des modules surchargés en base : actif mais non paramètrable
        for (final String key : aDecharger) {
            ModuleBean moduleBean = serviceModule.getById((long) key.hashCode());
            if (moduleBean != null) {
                moduleBean.setEtat(IModule.ETAT_ACTIF);
                moduleBean.setType(IModule.TYPE_NON_PARAMETRABLE_AFFICHABLE);
                serviceModule.update(moduleBean);
            }
            // déchargement des modules surchargés
            modules.remove(key);
        }
        // désactivation des modules inactifs
        for (final String key : aDesactiver) {
            modules.remove(key);
        }
    }

    private void cleanDB() {
        serviceModule.deleteNotRestorable();
    }

    private void loadModuleDB(final IModule module) {
        // l'id en base est le hashcode de l'id du bean + extension
        ModuleBean moduleDB = serviceModule.getById((long) BeanUtil.getBeanKey(module.getId(), module.getIdExtension()).hashCode());
        if (moduleDB == null) {
            moduleDB = new ModuleBean();
            moduleDB.setId((long) BeanUtil.getBeanKey(module.getId(), module.getIdExtension()).hashCode());
            setModuleDB(module, moduleDB);
            moduleDB.setDateCreation(new Date(System.currentTimeMillis()));
            serviceModule.add(moduleDB);
        }
        if (moduleDB.getEtat() == IModule.ETAT_A_RESTAURER || moduleDB.getType() != module.getType()) {
            if (moduleDB.getEtat() == IModule.ETAT_A_RESTAURER) {
                setModuleDB(module, moduleDB);
            }
            moduleDB.setType(module.getType());
            serviceModule.update(moduleDB);
        }
        // mise à jour de l'état et du libellé par rapport à la conf en base
        module.setEtat(moduleDB.getEtat());
        module.setLibelle(moduleDB.getLibelle());
    }

    private void setModuleDB(final IModule module, final ModuleBean moduleDB) {
        moduleDB.setEtat(module.getEtat());
        moduleDB.setType(module.getType());
        moduleDB.setIdBean(module.getId());
        moduleDB.setLibelle(StringUtils.isNotEmpty(module.getLibelle()) ? module.getLibelle() : module.getId());
        moduleDB.setIdExtension((long) module.getIdExtension().hashCode());
        moduleDB.setDateModification(new Date(System.currentTimeMillis()));
    }

    /**
     * Retourne la liste des modules actifs d'un certain type
     *
     * @param type
     *            type de module souhaité
     * @return la liste des modules
     */
    @SuppressWarnings("unchecked")
    public <T extends IModule> Collection<T> getModules(final Class<T> type) {
        final ArrayList<T> res = new ArrayList<>();
        for (final IModule module : modules.values()) {
            if (type.isAssignableFrom(module.getClass())) {
                res.add((T) module);
            }
        }
        return res;
    }

    /**
     * Retourne la liste des modules actifs
     *
     * @return la liste
     */
    public Map<String, IModule> getModules() {
        return modules;
    }

    public ExtensionManager getExtensionManager() {
        return extensionManager;
    }

    public void setExtensionManager(final ExtensionManager extensionManager) {
        this.extensionManager = extensionManager;
    }

    public void setCacheLoaderManager(final CacheLoaderManager cacheLoaderManager) {
        this.cacheLoaderManager = cacheLoaderManager;
    }

    @Override
    public void update(final Observable o, final Object arg) {
        refresh();
    }
}
