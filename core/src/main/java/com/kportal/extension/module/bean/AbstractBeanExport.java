package com.kportal.extension.module.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public abstract class AbstractBeanExport<T extends Serializable> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4629107367655176375L;

    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "beanClass")
    private T bean;

    private String idModule;

    @JsonProperty("class")
    public Class<?> getActualClass() {
        return this.getClass();
    }

    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(final String idModule) {
        this.idModule = idModule;
    }

    public T getBean() {
        return bean;
    }

    public void setBean(T bean) {
        this.bean = bean;
    }
}
