package com.kportal.extension.module.plugin.rubrique;

import java.util.Map;

import com.kportal.extension.module.IModule;

public interface IPageAccueilRubrique<T extends BeanPageAccueil> extends IModule {

    /**
     * Renvoie la clé du type
     * @return le type
     */
    String getTypeRubrique();

    /**
     * Renvoie le path vers le template jsp de saisie spécifique au type
     * Utilisé en BO dans rubrique_saisie.jsp
     * @return le path
     */
    String getPathSaisieBO();

    /**
     * Renvoit le path vers la jsp de saisie en BO
     * Non utilisé pour le moment
     * @return le path
     */
    String getPathSaisieFO();

    /**
     * Traitement préalable à la saisie de rubrique en fonction du type
     * Appel dans le processus de saisie de rubrique
     * @param infoBean
     * @param bean
     * @throws Exception
     */
    void preparerPRINCIPAL(Map<String, Object> infoBean, T bean) throws Exception;

    /**
     * Traitement à l'enregistrement spécifique
     * Appel dans le processus de saisie de rubrique
     * @param infoBean
     * @param bean
     * @throws Exception
     */
    void traiterPRINCIPAL(Map<String, Object> infoBean, T bean) throws Exception;

    Class<T> getClasseBeanPageAccueil();

    boolean isInterne(BeanPageAccueil bean);
}
