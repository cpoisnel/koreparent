package com.kportal.extension.module.service.impl.beanexport;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.kosmos.usinesite.utils.UASServicesHelper;
import com.kportal.extension.module.bean.BeanExportMap;
import com.kportal.extension.module.bean.PluginFicheBeanExport;
import com.kportal.extension.module.plugin.objetspartages.om.ObjetPluginContenu;
import com.kportal.extension.module.plugin.objetspartages.om.SousObjet;
import com.kportal.extension.module.service.ServiceBeanExport;
import com.kportal.extension.module.service.impl.PluginFicheServiceContenuModule;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.utils.ContexteDao;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

public class ServiceBeanExportPluginFiche implements ServiceBeanExport<PluginFicheBeanExport<?>> {

    private ServiceMetatag serviceMetatag;

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    @SuppressWarnings("deprecation")
    @Override
    public BeanExportMap<PluginFicheBeanExport<?>> getBeansByRubrique(final List<String> codesRubrique, final String idModule, final String pathExport, final String... params) throws Exception {
        BeanExportMap<PluginFicheBeanExport<?>> pluginFichesBeans = new BeanExportMap<>();
        if (ArrayUtils.isNotEmpty(params)) {
            final List<String> codesObjet = new ArrayList<>();
            final List<String> classes = new ArrayList<>();
            for (final String param : params) {
                if (param.startsWith(PluginFicheServiceContenuModule.PARAM_META_CODE_OBJET)) {
                    codesObjet.add(StringUtils.substringAfter(param, PluginFicheServiceContenuModule.PARAM_META_CODE_OBJET));
                } else if (param.startsWith(PluginFicheServiceContenuModule.PARAM_CLASSE)) {
                    classes.add(StringUtils.substringAfter(param, PluginFicheServiceContenuModule.PARAM_CLASSE));
                }
            }
            if (CollectionUtils.isNotEmpty(classes)) {
                pluginFichesBeans = buildPluginBeans(codesRubrique, idModule, pathExport, codesObjet, classes);
            }
        }
        return pluginFichesBeans;
    }

    private BeanExportMap<PluginFicheBeanExport<?>> buildPluginBeans(final List<String> codesRubrique, final String idModule, final String pathExport, final List<String> codesObjet, final List<String> classes) throws Exception {
        BeanExportMap<PluginFicheBeanExport<?>> pluginFichesBeans = new BeanExportMap<>();
        try (ContexteDao ctx = new ContexteDao()) {
            final Set<String> idsMeta = new HashSet<>();
            final List<MetatagBean> metas = serviceMetatag.getMetasForRubriquesAndObjet(codesRubrique, codesObjet.toArray(new String[codesObjet.size()]));
            for (MetatagBean currentMeta : metas) {
                idsMeta.add(currentMeta.getId().toString());
            }
            if (CollectionUtils.isNotEmpty(idsMeta)) {
                for (final String classeObjet : classes) {
                    final Class<?> clazz = Class.forName(classeObjet);
                    final Object object = clazz.newInstance();
                    pluginFichesBeans = buildExportBeans(idModule, pathExport, ctx, idsMeta, classeObjet, object);
                }
            }
        }
        return pluginFichesBeans;
    }

    private BeanExportMap<PluginFicheBeanExport<?>> buildExportBeans(final String idModule, final String pathExport, final ContexteDao ctx, final Set<String> idsMeta, final String classeObjet, final Object object) throws Exception {
        final BeanExportMap<PluginFicheBeanExport<?>> pluginFichesBeans = new BeanExportMap<>();
        if (object instanceof ObjetPluginContenu) {
            final ObjetPluginContenu objetPluginContenu = (ObjetPluginContenu) object;
            objetPluginContenu.init();
            objetPluginContenu.setCtx(ctx);
            final RequeteSQL requete = new RequeteSQL();
            final ClauseWhere where = new ClauseWhere();
            where.setPremiereCondition(ConditionHelper.in("T1.ID_META", idsMeta));
            requete.where(where);
            objetPluginContenu.select(requete.formaterRequete());
            while (objetPluginContenu.nextItem()) {
                final PluginFicheBeanExport<?> beanExport = UASServicesHelper.getPluginFicheBeanExport(objetPluginContenu, UASServicesHelper.getBeanClass(objetPluginContenu));
                beanExport.setClasseObjet(classeObjet);
                beanExport.setIdModule(idModule);
                beanExport.setMedias(UASServicesHelper.getMedias(ctx, objetPluginContenu, pathExport));
                pluginFichesBeans.put(objetPluginContenu.getIdObjet().toString(), beanExport);
                if (CollectionUtils.isNotEmpty(objetPluginContenu.getSousObjets())) {
                    for (final SousObjet sousObjet : objetPluginContenu.getSousObjets()) {
                        final PluginFicheBeanExport<SousObjet> beanExportSousObjet = new PluginFicheBeanExport<>();
                        final String saveId = sousObjet.getIdObjet().toString();
                        sousObjet.setIdObjet((long) 0);
                        beanExportSousObjet.setBean(sousObjet);
                        beanExportSousObjet.setIdModule(idModule);
                        beanExportSousObjet.setClasseObjet(sousObjet.getClass().getName());
                        beanExportSousObjet.setMedias(UASServicesHelper.getMedias(ctx, sousObjet, pathExport));
                        // les plugins et leurs sous objets sont stockes dans la même map, on préfixe l'id par le nom de la classe
                        pluginFichesBeans.put(sousObjet.getClass().getSimpleName() + saveId, beanExportSousObjet);
                    }
                }
            }
            // TODO export des ressources et medias quand on aura la possibilité d'en ajouter sur tous les objets hors fiche
        }
        return pluginFichesBeans;
    }
}
