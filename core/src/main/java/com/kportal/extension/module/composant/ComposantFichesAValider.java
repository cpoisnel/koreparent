package com.kportal.extension.module.composant;

import com.univ.objetspartages.om.AutorisationBean;

public class ComposantFichesAValider extends Composant {

    @Override
    public boolean isVisible(AutorisationBean autorisation) {
        return autorisation != null && autorisation.isValidateur();
    }
}
