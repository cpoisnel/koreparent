package com.kportal.extension.module.plugin.objetspartages;

import java.util.List;
import java.util.Map;

import com.kportal.extension.module.plugin.objetspartages.om.ObjetPluginContenu;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;

/**
 * The Interface IPluginContenu.
 */
public interface IPluginFiche extends IPluginFicheLegacy {

    /**
     * Preparer principal.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    void preparerPrincipal(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception;

    /**
     * Traiter principal.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    void traiterPrincipal(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception;

    /**
     * Traiter pre principal pre processor au traitement principal.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     */
    void preTraiterPrincipal(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception;

    /**
     * Traiter post principal post processor au traitement principal.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     */
    void postTraiterPrincipal(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception;

    /**
     * Traiter suppression.
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    void supprimerObjets(FicheUniv ficheUniv, MetatagBean meta, String classeObjet) throws Exception;

    /**
     * Traiter suppression.
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     */
    void synchroniserObjets(FicheUniv ficheUniv, MetatagBean meta, String classeObjet) throws Exception;

    /**
     * Lire donnees.
     *
     * @param ficheUniv   the fiche univ
     * @param meta        the meta
     * @param classeObjet la classe de l'objet
     * @throws Exception the exception
     */
    List<ObjetPluginContenu> lireObjets(FicheUniv ficheUniv, MetatagBean meta, String classeObjet) throws Exception;

    /**
     * prepare les donnees dans le contexte pour affichage en front.
     *
     * @param ficheUniv   the fiche univ
     * @param meta        the meta
     * @param classeObjet la classe de l'objet
     * @throws Exception the exception
     */
    void setDataContexteUniv(FicheUniv ficheUniv, MetatagBean meta, String classeObjet) throws Exception;

    /**
     * prepare les donnees dans le contexte pour affichage en front.
     */
    Object getDataContexteUniv();

    /**
     * ajouter donnees.
     *
     * @param ficheUniv  the fiche univ
     * @param meta       the meta
     * @param listeObjet liste des sous objets
     * @throws Exception the exception
     */
    void ajouterObjets(FicheUniv ficheUniv, MetatagBean meta, List<ObjetPluginContenu> listeObjet, String classeObjet) throws Exception;

    /**
     * dupliquer donnees.
     *
     * @param ficheUniv   the fiche univ
     * @param meta        the meta
     * @param oldIdMeta   liste des sous objets
     * @param classeObjet La classe de l'objet
     * @throws Exception the exception
     */
    void dupliquerObjets(FicheUniv ficheUniv, MetatagBean meta, Long oldIdMeta, String classeObjet) throws Exception;


    /**
     * Après mise en ligne.
     *
     * @param ficheUniv   La fiche mise en ligne
     * @param idMeta      le nouvel id du meta
     * @param oldIdMeta   l'id meta référencé sur les plugins à mettre à jour
     * @param classeObjet La classe de l'objet
     */
    void apresMiseEnLigne(FicheUniv ficheUniv, Long idMeta, Long oldIdMeta, String classeObjet);

    List<String> getObjets();

    List<String> getClasses();

    /**
     * Check classe.
     *
     * @param name the name of the parent class
     * @return the boolean
     */
    boolean isActive(String name);

    /**
     * Check objet.
     *
     * @param objet the name of the parent class
     * @return the boolean
     */
    boolean hasObjet(String objet);

    /**
     * Gets the path saisie bo.
     *
     * @return the path saisie bo
     */
    String getPathSaisieBo();

    /**
     * Gets the path saisie fo.
     *
     * @return the path saisie fo
     */
    String getPathSaisieFo();

    /**
     * Gets the path saisie fo.
     *
     * @return the path saisie fo
     */
    String getPathTemplateFo();
}
