package com.kportal.extension.module.plugin.objetspartages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.extension.module.ModuleHelper;
import com.kportal.extension.module.plugin.objetspartages.om.ObjetPluginContenu;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Metatag;

/**
 * The Class PluginContenuManager.
 */
public class PluginFicheHelper {

    public static final int SAISIE_BO = 0;

    public static final int SAISIE_FO = 1;

    public static final int TEMPLATE_FO = 2;

    public static Collection<IPluginFiche> getPlugins() {
        return new ArrayList<>(ModuleHelper.getModuleManager().getModules(IPluginFiche.class));
    }

    /**
     * Preparer principal.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    public static void preparerPrincipal(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (plugin.isActive(ficheUniv.getClass().getName())) {
                final Map<String, Object> map = infoBean.getValues();
                plugin.preparerPrincipal(map, ficheUniv, meta);
                infoBean.setValues(map);
            }
        }
    }

    /**
     * Traitement principal dans le ControleurUniv.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    public static void traiterPrincipal(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (plugin.isActive(ficheUniv.getClass().getName())) {
                final Map<String, Object> map = infoBean.getValues();
                try {
                    plugin.traiterPrincipal(map, ficheUniv, meta);
                } finally {
                    infoBean.setValues(map);
                }
            }
        }
    }

    /**
     * Pre traitement principal dans le ControleurUniv.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the action
     * @throws Exception the exception
     */
    public static void preTraiterPrincipal(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (plugin.isActive(ficheUniv.getClass().getName())) {
                final Map<String, Object> map = infoBean.getValues();
                plugin.preTraiterPrincipal(map, ficheUniv, meta);
                infoBean.setValues(map);
            }
        }
    }

    /**
     * Post traitement principal dans le ControleurUniv.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the action
     * @throws Exception the exception
     */
    public static void postTraiterPrincipal(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (plugin.isActive(ficheUniv.getClass().getName())) {
                final Map<String, Object> map = infoBean.getValues();
                plugin.postTraiterPrincipal(map, ficheUniv, meta);
                infoBean.setValues(map);
            }
        }
    }

    /**
     * supprimer contenu.
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    public static void supprimerObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (process(plugin, ficheUniv, classeObjet)) {
                plugin.supprimerObjets(ficheUniv, meta, classeObjet);
            }
        }
    }

    /**
     * ajouter contenu.
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    public static void ajouterObjets(final FicheUniv ficheUniv, final MetatagBean meta, final List<ObjetPluginContenu> listeObjet, final String classeObjet) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (process(plugin, ficheUniv, classeObjet)) {
                plugin.ajouterObjets(ficheUniv, meta, listeObjet, classeObjet);
            }
        }
    }

    /**
     * synchroniser les objets. mise à jour des objets liés à une fiche
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    public static void synchroniserObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (process(plugin, ficheUniv, classeObjet)) {
                plugin.synchroniserObjets(ficheUniv, meta, classeObjet);
            }
        }
    }

    /**
     * synchroniser une liste d'objets mise à jour des objets liés à une fiche
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    public static void synchroniserListeObjets(final FicheUniv ficheUniv, final MetatagBean meta, final ArrayList<ObjetPluginContenu> lstObjets) throws Exception {
        final List<ObjetPluginContenu> lstObjetsOld = lireObjets(ficheUniv, meta, null);
        final List<ObjetPluginContenu> lstObjetsTemp = new ArrayList<>(lstObjets);
        for (final ObjetPluginContenu objetExistant : lstObjetsOld) {
            final Iterator<ObjetPluginContenu> it = lstObjetsTemp.iterator();
            while (it.hasNext()) {
                final ObjetPluginContenu nouvelObjet = it.next();
                if (objetExistant.getCle().equals(nouvelObjet.getCle())) {
                    nouvelObjet.setIdObjet(objetExistant.getIdObjet());
                    nouvelObjet.setIdMeta(objetExistant.getIdMeta());
                    nouvelObjet.update();
                    it.remove();
                    break;
                }
            }
        }
        // ajout des nouveaux objets
        ajouterObjets(ficheUniv, meta, lstObjetsTemp, null);
    }

    /**
     * dupliquer contenu.
     *
     * @param ficheUniv   the fiche univ
     * @param meta        the meta
     * @param oldIdMeta   the long
     * @param classeObjet the long
     * @throws Exception the exception
     */
    public static void dupliquerObjets(final FicheUniv ficheUniv, final MetatagBean meta, final Long oldIdMeta, final String classeObjet) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (process(plugin, ficheUniv, classeObjet)) {
                plugin.dupliquerObjets(ficheUniv, meta, oldIdMeta, classeObjet);
            }
        }
    }

    /**
     * Lire donnees.
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     */
    public static List<ObjetPluginContenu> lireObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        final ArrayList<ObjetPluginContenu> sousObjets = new ArrayList<>();
        for (final IPluginFiche plugin : getPlugins()) {
            if (process(plugin, ficheUniv, classeObjet)) {
                sousObjets.addAll(plugin.lireObjets(ficheUniv, meta, classeObjet));
            }
        }
        return sousObjets;
    }

    /**
     * possède un plugin de fiche
     *
     * @param classe la chaine de caractère contenant package.NomClasse
     * @param isBO   est ce qu'on est en BackOffice ou FrontOffice
     * @return vrai si la classe fourni en paramètre contient bien le plugin, faux sinon
     */
    public static Boolean hasPlugin(final String classe, final boolean isBO) {
        for (final IPluginFiche controleur : getPlugins()) {
            if (controleur.isActive(classe)) {
                final String path = isBO ? controleur.getPathSaisieBo() : controleur.getPathSaisieFo();
                if (StringUtils.isNotEmpty(path)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * test si la classe objet est un plugin de fiche sur la classe passée
     */
    public static Boolean isObjetPlugin(final String classeObjet, final String classe) throws Exception {
        if (!(Class.forName(classe).newInstance() instanceof FicheUniv) || !(Class.forName(classeObjet).newInstance() instanceof ObjetPluginContenu)) {
            return false;
        }
        for (final IPluginFiche controleur : getPlugins()) {
            if (process(controleur, (FicheUniv) Class.forName(classe).newInstance(), classeObjet)) {
                return true;
            }
        }
        return false;
    }

    private static boolean process(final IPluginFiche plugin, final FicheUniv ficheUniv, final String classeObjet) {
        return plugin.isActive(ficheUniv.getClass().getName()) && plugin.hasObjet(classeObjet);
    }

    public static void setDataContexteUniv(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (process(plugin, ficheUniv, classeObjet)) {
                plugin.setDataContexteUniv(ficheUniv, meta, classeObjet);
            }
        }
    }

    public static Collection<IPluginFiche> getPluginsByObjet(final String name) {
        final Collection<IPluginFiche> res = getPlugins();
        final Iterator<IPluginFiche> it = res.iterator();
        while (it.hasNext()) {
            final IPluginFiche next = it.next();
            if (CollectionUtils.isEmpty(next.getObjets()) || !next.getObjets().contains(name)) {
                it.remove();
            }
        }
        return res;
    }

    public static Collection<IPluginFiche> getPluginsFromClass(final String name) {
        final Collection<IPluginFiche> res = getPlugins();
        final Iterator<IPluginFiche> it = res.iterator();
        while (it.hasNext()) {
            final IPluginFiche next = it.next();
            if (CollectionUtils.isEmpty(next.getClasses()) || !next.getClasses().contains(name)) {
                it.remove();
            }
        }
        return res;
    }

    //FIXME : rétro-compatibilité

    /**
     * synchroniser les objets. mise à jour des objets liés à une fiche
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#synchroniserObjets(FicheUniv, MetatagBean, String)}
     */
    @Deprecated
    public static void synchroniserObjets(final FicheUniv ficheUniv, final Metatag meta, final String classeObjet) throws Exception {
        synchroniserObjets(ficheUniv, meta.getPersistenceBean(), classeObjet);
    }

    /**
     * ajouter contenu.
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#ajouterObjets(FicheUniv, MetatagBean, List, String)}
     */
    @Deprecated
    public static void ajouterObjets(final FicheUniv ficheUniv, final Metatag meta, final List<ObjetPluginContenu> listeObjet, final String classeObjet) throws Exception {
        ajouterObjets(ficheUniv, meta.getPersistenceBean(), listeObjet, classeObjet);
    }

    /**
     * supprimer contenu.
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#supprimerObjets(FicheUniv, MetatagBean, String)}
     */
    @Deprecated
    public static void supprimerObjets(final FicheUniv ficheUniv, final Metatag meta, final String classeObjet) throws Exception {
        supprimerObjets(ficheUniv, meta.getPersistenceBean(), classeObjet);
    }


    /**
     * Post traitement principal dans le ControleurUniv.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the action
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#postTraiterPrincipal(InfoBean, FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void postTraiterPrincipal(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        postTraiterPrincipal(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    /**
     * Pre traitement principal dans le ControleurUniv.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the action
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#preTraiterPrincipal(InfoBean, FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void preTraiterPrincipal(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        preTraiterPrincipal(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    /**
     * Traitement principal dans le ControleurUniv.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#traiterPrincipal(InfoBean, FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void traiterPrincipal(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        traiterPrincipal(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    /**
     * Preparer principal.
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#preparerPrincipal(InfoBean, FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void preparerPrincipal(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        preparerPrincipal(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    /**
     * synchroniser une liste d'objets mise à jour des objets liés à une fiche
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#synchroniserListeObjets(FicheUniv, MetatagBean, ArrayList)}
     */
    @Deprecated
    public static void synchroniserListeObjets(final FicheUniv ficheUniv, final Metatag meta, final ArrayList<ObjetPluginContenu> lstObjets) throws Exception {
        synchroniserListeObjets(ficheUniv, meta.getPersistenceBean(), lstObjets);
    }

    /**
     * dupliquer contenu.
     *
     * @param ficheUniv   the fiche univ
     * @param meta        the meta
     * @param oldIdMeta   the long
     * @param classeObjet the long
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#dupliquerObjets(FicheUniv, MetatagBean, Long, String)}
     */
    @Deprecated
    public static void dupliquerObjets(final FicheUniv ficheUniv, final Metatag meta, final Long oldIdMeta, final String classeObjet) throws Exception {
        dupliquerObjets(ficheUniv, meta.getPersistenceBean(), oldIdMeta, classeObjet);
    }

    /**
     * Lire donnees.
     *
     * @param ficheUniv the fiche univ
     * @param meta      the meta
     * @throws Exception the exception
     * @deprecated Utilisez {@link PluginFicheHelper#lireObjets(FicheUniv, MetatagBean, String)}
     */
    @Deprecated
    public static List<ObjetPluginContenu> lireObjets(final FicheUniv ficheUniv, final Metatag meta, final String classeObjet) throws Exception {
        return lireObjets(ficheUniv, meta.getPersistenceBean(), classeObjet);
    }

    /**
     * @deprecated Utilisez {@link PluginFicheHelper#setDataContexteUniv(FicheUniv, MetatagBean, String)}
     */
    @Deprecated
    public static void setDataContexteUniv(final FicheUniv ficheUniv, final Metatag meta, final String classeObjet) throws Exception {
        for (final IPluginFiche plugin : getPlugins()) {
            if (process(plugin, ficheUniv, classeObjet)) {
                plugin.setDataContexteUniv(ficheUniv, meta, classeObjet);
            }
        }
    }

    /**
     *
     * @param ficheUniv
     * @param idMeta
     * @param oldIdMeta
     * @param classeObjet
     */
    public static void apresMiseEnLigne(final FicheUniv ficheUniv, final Long idMeta, final Long oldIdMeta, final String classeObjet) {
        for (final IPluginFiche plugin : getPlugins()) {
            if (process(plugin, ficheUniv, classeObjet)) {
                plugin.apresMiseEnLigne(ficheUniv, idMeta, oldIdMeta, classeObjet);
            }
        }
    }
}
