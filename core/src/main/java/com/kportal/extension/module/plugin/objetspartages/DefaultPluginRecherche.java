package com.kportal.extension.module.plugin.objetspartages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.textsearch.ResultatRecherche;
import com.kportal.extension.module.DefaultModuleImpl;

public class DefaultPluginRecherche extends DefaultModuleImpl implements IPluginRecherche {

    private List<String> classes;

    private String pathRechercheBo = "";

    /**
     * Checks if is actif.
     *
     * @param requete
     *            the requete
     * @return true, if is actif
     */
    @Override
    public boolean isActif(String requete) {
        return Boolean.TRUE;
    }

    /**
     * Checks if is exclusif.
     *
     * @return true, if is exclusif
     */
    @Override
    public boolean isExclusif() {
        return Boolean.FALSE;
    }

    /**
     * Annule critere nombre.
     *
     * @param requete
     *            the requete
     * @return true, if successful
     */
    @Override
    public boolean annuleCritereNombre(String requete) {
        return Boolean.FALSE;
    }

    /**
     * Preparer requete.
     */
    @Override
    public String preparerRequete(OMContext ctx, String requete) throws Exception {
        return requete;
    }

    @Override
    public Map<String, Object> preparerRecherche(OMContext ctx, Map<String, Object> infoBean) throws Exception {
        return infoBean;
    }

    @Override
    public Map<String, Object> taiterRecherche(OMContext ctx, Map<String, Object> infoBean) throws Exception {
        return infoBean;
    }

    /**
     * Traiter requete.
     *
     * @param ctx
     *            the ctx
     * @param requete
     *            the requete
     * @param res
     *            the res
     * @return the list
     * @throws Exception
     *             the exception
     */
    @Override
    public List<ResultatRecherche> traiterRequete(OMContext ctx, String requete, List<ResultatRecherche> res) throws Exception {
        return res;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }

    @Override
    public boolean checkClasse(String classe) {
        if (classes == null) {
            return Boolean.TRUE;
        } else {
            if (classes.contains(classe)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Override
    public String getPathRechercheBo() {
        return pathRechercheBo;
    }

    @Override
    public List<String> getCriteresRequete() {
        return new ArrayList<>();
    }

    public void setPathRechercheBo(String templateRecherche) {
        this.pathRechercheBo = templateRecherche;
    }
}
