package com.kportal.extension.module.plugin.toolbox;

import java.util.List;
import java.util.Map;

import com.kportal.extension.module.IModule;

/**
 * The Interface IPluginToolbox.
 */
public interface IPluginToolbox extends IModule {

    /**
     * Gets the path.
     *
     * @return the path
     */
    String getPath();

    Map<String, String> getOptions();

    List<String> getProcessus();

    boolean isAccessible();
}
