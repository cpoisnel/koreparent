package com.kportal.extension.module.plugin.objetspartages;

import java.util.Map;

import com.kportal.extension.module.IModule;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Metatag;

/**
 * The Interface IPluginContenu.
 */
public interface IPluginSaisieFicheLegacy extends IModule {

    /**
     * Traiter pre action. Pre processor au traitement action par defaut
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     * @deprecated Utilisez {@link IPluginSaisieFiche#preTraiterAction(Map, FicheUniv, MetatagBean)}
     */
    @Deprecated
    void preTraiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, Metatag meta) throws Exception;

    /**
     * Traiter action. Pre processor au traitement action par defaut
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     * @deprecated Utilisez {@link IPluginSaisieFiche#traiterAction(Map, FicheUniv, MetatagBean)}
     */
    @Deprecated
    void traiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, Metatag meta) throws Exception;

    /**
     * Traiter post action. Post processor au traitement action par defaut
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     * @deprecated Utilisez {@link IPluginSaisieFiche#postTraiterAction(Map, FicheUniv, MetatagBean)}
     */
    @Deprecated
    void postTraiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, Metatag meta) throws Exception;
}
