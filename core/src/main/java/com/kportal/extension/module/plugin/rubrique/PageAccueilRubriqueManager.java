package com.kportal.extension.module.plugin.rubrique;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.extension.module.AbstractBeanManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.om.Rubrique;
import com.univ.utils.json.CodecJSon;

public class PageAccueilRubriqueManager extends AbstractBeanManager {

    public static final String ID_BEAN = "pageAccueilRubriqueManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(PageAccueilRubriqueManager.class);

    private Map<String, IPageAccueilRubrique> typesRubrique;

    public static PageAccueilRubriqueManager getInstance() {
        return (PageAccueilRubriqueManager) ApplicationContextManager.getCoreContextBean(PageAccueilRubriqueManager.ID_BEAN);
    }

    @Override
    public void refresh() {
        typesRubrique = new HashMap<>();
        final Collection<IPageAccueilRubrique> pagesAccueil = moduleManager.getModules(IPageAccueilRubrique.class);
        for (final IPageAccueilRubrique iModule : pagesAccueil) {
            if (typesRubrique.containsKey(iModule.getTypeRubrique())) {
                LOGGER.warn("Le type de rubrique " + iModule.getTypeRubrique() + " existe déjà");
            } else {
                typesRubrique.put(iModule.getTypeRubrique(), iModule);
            }
        }
    }

    public Hashtable<String, String> getListeTypesRubrique() {
        final Hashtable<String, String> h = new Hashtable<>();
        for (final String type : typesRubrique.keySet()) {
            h.put(type, getPageAccueilRubrique(type).getLibelleAffichable());
        }
        return h;
    }

    public Map<String, IPageAccueilRubrique> getTypesRubrique() {
        return typesRubrique;
    }

    public BeanPageAccueil getBeanPageAccueil(final InfosRubriques rubrique) {
        return getBeanPageAccueil(rubrique.getTypeRubrique(), rubrique.getPageAccueil());
    }

    public BeanPageAccueil getBeanPageAccueil(final RubriqueBean rubrique) {
        return getBeanPageAccueil(rubrique.getTypeRubrique(), rubrique.getPageAccueil());
    }

    public BeanPageAccueil getBeanPageAccueil(final String typeRubrique, final String contenuPageAccueil) {
        BeanPageAccueil bean = null;
        if (StringUtils.isNotEmpty(contenuPageAccueil)) {
            final IPageAccueilRubrique pageAccueil = getPageAccueilRubrique(typeRubrique);
            if (pageAccueil != null) {
                try {
                    bean = (BeanPageAccueil) CodecJSon.decodeStringJSonToClass(contenuPageAccueil, pageAccueil.getClasseBeanPageAccueil());
                } catch (IOException e) {
                    LOGGER.error("Erreur lors du traitement du JSON", e);
                }
            }
        }
        return bean;
    }

    public void setTypesRubrique(final Map<String, IPageAccueilRubrique> typesRubrique) {
        this.typesRubrique = typesRubrique;
    }

    /**
     * @deprecated cette méthode est là pour la rétrocompatibilité, utilisez {@link PageAccueilRubriqueManager#preparerPRINCIPAL(InfoBean, RubriqueBean)} à la place
     * @param infoBean
     * @param rubrique
     * @throws Exception
     */
    @Deprecated
    public void preparerPRINCIPAL(final InfoBean infoBean, final Rubrique rubrique) throws Exception {
        preparerPRINCIPAL(infoBean, rubrique.getPersistenceBean());
    }

    /**
     * @deprecated cette méthode est là pour la rétrocompatibilité, utilisez {@link PageAccueilRubriqueManager#traiterPRINCIPAL(InfoBean, RubriqueBean)} à la place
     * @param infoBean
     * @param rubrique
     * @throws Exception
     */
    @Deprecated
    public void traiterPRINCIPAL(final InfoBean infoBean, final Rubrique rubrique) throws Exception {
        traiterPRINCIPAL(infoBean, rubrique.getPersistenceBean());
    }

    public void preparerPRINCIPAL(final InfoBean infoBean, final RubriqueBean rubrique) throws Exception {
        for (final IPageAccueilRubrique pageAccueil : typesRubrique.values()) {
            BeanPageAccueil bean = (BeanPageAccueil) pageAccueil.getClasseBeanPageAccueil().newInstance();
            if (pageAccueil.getTypeRubrique().equals(rubrique.getTypeRubrique()) && StringUtils.isNotBlank(rubrique.getPageAccueil())) {
                bean = CodecJSon.decodeStringJSonToClass(rubrique.getPageAccueil(), bean.getClass());
                infoBean.set("URL_APERCU", bean.getUrlRubrique(rubrique.getCode(), rubrique.getLangue(), true));
            }
            final Map<String, Object> map = infoBean.getValues();
            pageAccueil.preparerPRINCIPAL(map, bean);
            infoBean.setValues(map);
            infoBean.set("TYPE_RUBRIQUE", rubrique.getTypeRubrique());
        }
    }

    public void traiterPRINCIPAL(final InfoBean infoBean, final RubriqueBean rubrique) throws Exception {
        final String typeRubrique = (String) infoBean.get("TYPE_RUBRIQUE");
        final IPageAccueilRubrique pageAccueil = getPageAccueilRubrique(typeRubrique);
        if (pageAccueil != null) {
            final Map<String, Object> map = infoBean.getValues();
            final BeanPageAccueil bean = (BeanPageAccueil) pageAccueil.getClasseBeanPageAccueil().newInstance();
            pageAccueil.traiterPRINCIPAL(map, bean);
            infoBean.setValues(map);
            rubrique.setPageAccueil(CodecJSon.encodeObjectToJSonInString(bean));
            rubrique.setTypeRubrique(typeRubrique);
        } else {
            rubrique.setPageAccueil(StringUtils.EMPTY);
            rubrique.setTypeRubrique(StringUtils.EMPTY);
        }
    }

    public boolean isInterne(final InfosRubriques rubrique) {
        return getPageAccueilRubrique(rubrique.getTypeRubrique()) == null || getPageAccueilRubrique(rubrique.getTypeRubrique()).isInterne(getBeanPageAccueil(rubrique));
    }

    public boolean isInterne(final RubriqueBean rubrique) {
        return getPageAccueilRubrique(rubrique.getTypeRubrique()) == null || getPageAccueilRubrique(rubrique.getTypeRubrique()).isInterne(getBeanPageAccueil(rubrique));
    }

    public IPageAccueilRubrique getPageAccueilRubrique(final String typeRubrique) {
        return typesRubrique.get(typeRubrique);
    }

    public String getTypePageAcceuilParBeanPageAccueil(final BeanPageAccueil pageAccueil) {
        String codeTypeRubrique = StringUtils.EMPTY;
        for (final Entry<String, IPageAccueilRubrique> typeRubrique : typesRubrique.entrySet()) {
            if (typeRubrique.getValue().getClasseBeanPageAccueil().equals(pageAccueil.getClass())) {
                codeTypeRubrique = typeRubrique.getKey();
            }
        }
        return codeTypeRubrique;
    }

}
