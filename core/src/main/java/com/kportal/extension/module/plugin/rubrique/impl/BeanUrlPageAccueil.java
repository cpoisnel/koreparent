package com.kportal.extension.module.plugin.rubrique.impl;

import org.apache.commons.lang3.StringUtils;

import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;

/**
 * Created on 30/10/15.
 */
public class BeanUrlPageAccueil implements BeanPageAccueil {

    private String url = StringUtils.EMPTY;

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    @Override
    public String getUrlRubrique(final String codeRubrique, final String langue, final boolean ampersands) {
        return url;
    }

    @Override
    public String getUrlModification(final String codeRubrique, final String langue, final boolean ampersands) {
        return StringUtils.EMPTY;
    }

    @Override
    public String getLibelleAffichable() {
        return url;
    }
}