package com.kportal.extension.module.composant;

import com.kosmos.usinesite.utils.UsineSiteComposantUtil;
import com.univ.objetspartages.om.AutorisationBean;

public class ComposantUsineASite extends ComposantAdministration {

    /**
     * En fonction des autorisations et de l'action fourni on retourne vrai si l'utilisateur a les droits en fonction de l'action qu'il entreprend.
     */
    public static boolean isAutoriseParActionProcessus(final AutorisationBean autorisations, final String action) {
        boolean isAutorise = autorisations != null && autorisations.isWebMaster();
        if (!isAutorise && autorisations != null) {
            isAutorise = autorisations.possedePermission(UsineSiteComposantUtil.getPermissionGestion());
            if (!isAutorise && UsineSiteComposantUtil.ACTIONS_MODIFICATION.contains(action)) {
                isAutorise = autorisations.possedePermission(UsineSiteComposantUtil.getPermissionModification());
            }
        }
        return isAutorise;
    }

    /*
     * Le processus est visible si on est webmaster ou si l'on a le droit de gestion ou de modification
     * (non-Javadoc)
     * @see com.kportal.extension.module.composant.ComposantAdministration#isVisible(com.univ.objetspartages.om.AutorisationBean)
     */
    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        boolean isVisible = Boolean.FALSE;
        if (isAccessibleBO() && autorisation != null) {
            isVisible = autorisation.possedePermission(UsineSiteComposantUtil.getPermissionGestion()) || autorisation.possedePermission(UsineSiteComposantUtil.getPermissionModification());
        }
        return isVisible;
    }

    /*
     * L'unique action présente sur ce composant est l'action de création, il faut donc les droits de gestion pour pouvoir y accéder
     * (non-Javadoc)
     * @see com.kportal.extension.module.composant.Composant#isActionVisible(com.univ.objetspartages.om.AutorisationBean, java.lang.String)
     */
    @Override
    public boolean isActionVisible(final AutorisationBean autorisation, final String code) {
        boolean isVisible = super.isVisible(autorisation);
        if (!isVisible && autorisation != null) {
            isVisible = autorisation.possedePermission(UsineSiteComposantUtil.getPermissionGestion());
        }
        return isVisible;
    }
}
