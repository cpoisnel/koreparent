package com.kportal.extension.module.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RessourceBean;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PluginFicheBeanExport<T extends Serializable> extends AbstractBeanExport<T> {

    /**
     *
     */
    private static final long serialVersionUID = -7624898944847722776L;

    public String classeObjet;

    private Map<String, MediaBean> medias;

    private List<RessourceBean> ressources;

    public Map<String, MediaBean> getMedias() {
        return medias;
    }

    public void setMedias(Map<String, MediaBean> medias) {
        this.medias = medias;
    }

    public List<RessourceBean> getRessources() {
        return ressources;
    }

    public void setRessources(List<RessourceBean> ressources) {
        this.ressources = ressources;
    }

    public String getClasseObjet() {
        return classeObjet;
    }

    public void setClasseObjet(String classeObjet) {
        this.classeObjet = classeObjet;
    }
}
