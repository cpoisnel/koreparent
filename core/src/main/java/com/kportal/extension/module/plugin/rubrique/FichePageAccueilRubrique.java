package com.kportal.extension.module.plugin.rubrique;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.plugin.rubrique.impl.BeanFichePageAccueil;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.util.MetatagUtils;

public class FichePageAccueilRubrique extends DefaultPageAccueilRubrique<BeanFichePageAccueil> {

    private static final long serialVersionUID = 6420883169765691077L;

    private transient ServiceMetatag serviceMetatag;

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    @Override
    public void preparerPRINCIPAL(final Map<String, Object> infoBean, final BeanFichePageAccueil bean) throws Exception {
        final FicheUniv ficheUniv = bean.getFichePageTete(true);
        final String typeFiche = ReferentielObjets.getLibelleObjet(bean.getObjet());
        if (ficheUniv != null) {
            infoBean.put("LIBELLE_CODE_PAGE_TETE", typeFiche + " : " + ficheUniv.getLibelleAffichable());
        } else {
            infoBean.put("LIBELLE_CODE_PAGE_TETE", "");
        }
        infoBean.put("CODE_PAGE_TETE", bean.getCodePageTete());
    }

    @Override
    public void traiterPRINCIPAL(final Map<String, Object> infoBean, final BeanFichePageAccueil bean) throws Exception {
        if (StringUtils.isEmpty((String) infoBean.get("CODE_PAGE_TETE"))) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("RUBRIQUE.PAGE_TETE.ERREUR.PAGE_TETE_OBLIGATOIRE"));
        }
        bean.parseCodePageTete((String) infoBean.get("CODE_PAGE_TETE"));
        //Le rattachement rubrique de la page de tête est mis à jour automatique dans la fiche si il est vide
        final FicheUniv ficheUniv = bean.getFichePageTete(true);
        if (ficheUniv != null) {
            // ticket 7493 : ficheUniv.code_rubrique et meta.code_rubrique doivent rester solidaires
            if (ficheUniv.getCodeRubrique().length() == 0) {
                ficheUniv.setCodeRubrique((String) infoBean.get("CODE"));
                ficheUniv.update();
                final MetatagBean meta = MetatagUtils.lireMeta(ficheUniv);
                meta.setMetaCodeRubrique((String) infoBean.get("CODE"));
                serviceMetatag.save(meta);
            }
        }
    }
}
