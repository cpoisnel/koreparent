package com.kportal.extension.module.plugin.rubrique;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.plugin.rubrique.impl.BeanServicePageAccueil;
import com.univ.objetspartages.om.ServiceBean;
import com.univ.utils.ServicesUtil;

public class ServicePageAccueilRubrique extends DefaultPageAccueilRubrique<BeanServicePageAccueil> {

    /**
     *
     */
    private static final long serialVersionUID = 7746055689577755034L;

    @Override
    public void preparerPRINCIPAL(final Map<String, Object> infoBean, final BeanServicePageAccueil bean) {
        if (StringUtils.isNotEmpty(bean.getCode())) {
            infoBean.put("CODE_SERVICE", bean.getCode());
        }
        final Hashtable<String, String> table = new Hashtable<>();
        table.put(" ", "");
        for (final ServiceBean service : ServicesUtil.getServices().values()) {
            table.put(service.getCode(), service.getIntitule());
        }
        infoBean.put("LISTE_SERVICES", table);
    }

    @Override
    public void traiterPRINCIPAL(final Map<String, Object> infoBean, final BeanServicePageAccueil bean) throws ErreurApplicative {
        final String codeService = (String) infoBean.get("CODE_SERVICE");
        if (StringUtils.isEmpty(codeService)) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("RUBRIQUE.PAGE_TETE.ERREUR.SERVICE_OBLIGATOIRE"));
        }
        bean.setCode((String)infoBean.get("CODE_SERVICE"));
    }

    @Override
    public boolean isInterne(final BeanPageAccueil bean) {
        return false;
    }

}
