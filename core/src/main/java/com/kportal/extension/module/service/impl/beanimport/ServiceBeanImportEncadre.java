package com.kportal.extension.module.service.impl.beanimport;

import com.kosmos.usinesite.utils.UASOmHelper;
import com.kportal.extension.module.bean.AbstractBeanExport;
import com.kportal.extension.module.bean.EncadreBeanExport;
import com.kportal.extension.module.service.ServiceBeanImport;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.services.ServiceEncadre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceBeanImportEncadre implements ServiceBeanImport<EncadreBeanExport> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceBeanImportEncadre.class);

    private ServiceEncadre serviceEncadre;

    public void setServiceEncadre(ServiceEncadre serviceEncadre) {
        this.serviceEncadre = serviceEncadre;
    }

    @Override
    public String importBeanExport(final AbstractBeanExport<?> beanExport, final String pathImport) {
        final EncadreBeanExport encadreBeanExport = (EncadreBeanExport) beanExport;
        //Si l'id = -1 alors on ne persisite pas le bean
        if(!((EncadreBeanExport) beanExport).getBean().getId().equals("-1")) {
            try {
                addEncadre(encadreBeanExport.getBean());
                UASOmHelper.addAllMediaBean(encadreBeanExport.getMedias(), pathImport);
            } catch (final Exception e) {
                LOGGER.error(String.format("Une erreur est survenue lors de l'import de l'encadre \"%s\"", encadreBeanExport), e);
            }
        }
        return null;
    }

    private void addEncadre(final EncadreBean encadreBean) throws Exception {
        final EncadreBean encadre = new EncadreBean();
        UASOmHelper.copyProperties(encadre, encadreBean);
        encadre.setId(0L);
        serviceEncadre.addWithForcedId(encadre);
    }
}
