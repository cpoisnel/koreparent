package com.kportal.extension.module.plugin.rubrique;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.webapp.WebAppUtil;

public final class PageAccueilRubriqueHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(PageAccueilRubriqueHelper.class);

    /**
     * Permet d'inclure la jsp de saisie d'une page d'acceuil de rubrique
     *
     * @param out
     * @param context
     * @param request
     * @param response
     * @param pageAccueil
     */
    public static void includeJspBO(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final IPageAccueilRubrique pageAccueil) {
        String relativePath = pageAccueil.getPathSaisieBO();
        if (StringUtils.isNotEmpty(relativePath)) {
            // on teste d'abord l'existence du fichier à la racine de la webapp
            if (!new File(WebAppUtil.getAbsolutePath() + relativePath).exists()) {
                // on teste ensuite à la racine de l'extension
                relativePath = pageAccueil.getExtension().getRelativePath() + relativePath;
                if (!new File(WebAppUtil.getAbsolutePath() + relativePath).exists()) {
                    relativePath = StringUtils.EMPTY;
                }
            }
            if (StringUtils.isNotEmpty(relativePath)) {
                try {
                    out.flush();
                    context.getRequestDispatcher(relativePath).include(request, response);
                    out.flush();
                } catch (final IOException | ServletException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
    }
}
