package com.kportal.extension.module.composant;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.ModuleHelper;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;

public class ComposantGroupe extends ComposantAdministration {

    public static final String ID_BEAN = "composantGroupe";

    /**
     * La permission permettant de lister les utilisateurs
     */
    public static final String CODE_PERMISSION = "groupe";

    /**
     * La permission permettant de gérer les utilisateur (Create Update Delete)
     */
    public static final String CODE_ACTION_GESTION = "G";

    public static IModule getModule() {
        return ModuleHelper.getModule(ApplicationContextManager.DEFAULT_CORE_CONTEXT, ID_BEAN);
    }

    /**
     * Retourne la permission de gestion des utilisateurs.
     */
    public static PermissionBean getPermissionGestion() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, CODE_ACTION_GESTION);
    }

    /**
     * En fonction des autorisations et de l'action fourni on retourne vrai si l'utilisateur a les droits en fonction de l'action qu'il entreprend.
     */
    public static boolean isAutoriseParActionProcessus(final AutorisationBean autorisations, final String action) {
        return autorisations != null && (autorisations.isWebMaster() || autorisations.possedePermission(getPermissionGestion()));
    }

    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        boolean isVisible = Boolean.FALSE;
        if (isAccessibleBO() && autorisation != null) {
            isVisible = autorisation.possedePermission(getPermissionGestion());
        }
        return isVisible;
    }

    @Override
    public boolean isActionVisible(final AutorisationBean autorisation, final String code) {
        return isVisible(autorisation);
    }
}
