package com.kportal.extension.module.plugin.toolbox;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.extension.module.ModuleHelper;

public class PluginToolboxHelper {

    public static String getRelativePath(final IPluginToolbox pluginToolbox) {
        return ModuleHelper.getRelativePathExtension(pluginToolbox) + pluginToolbox.getPath();
    }

    public static Collection<IPluginToolbox> getPlugins() {
        return ModuleHelper.getModuleManager().getModules(IPluginToolbox.class);
    }

    public static String getConfiguration(final InfoBean infoBean) {
        String res = "";
        final Collection<IPluginToolbox> plugins = getPlugins();
        if (plugins.size() > 0) {
            for (final IPluginToolbox iModule : plugins) {
                // controle des droits sur les plugins toolbox
                if (isVisible(iModule, infoBean)) {
                    if (res.length() > 0) {
                        res += "|";
                    }
                    final String path = getRelativePath(iModule);
                    res += StringUtils.substringAfterLast(path, "/") + "," + StringUtils.substringBeforeLast(path, "/") + "/";
                }
            }
        }
        return res;
    }

    public static List<Pair<String, String>> getOptions(final InfoBean infoBean) {
        final List<Pair<String, String>> res = new ArrayList<>();
        final Collection<IPluginToolbox> plugins = getPlugins();
        if (plugins.size() > 0) {
            for (final IPluginToolbox iModule : plugins) {
                // controle des droits sur les plugins toolbox
                if (isVisible(iModule, infoBean) && !iModule.getOptions().isEmpty()) {
                    for (final Entry<String, String> option : iModule.getOptions().entrySet()) {
                        res.add(new ImmutablePair<>(option.getKey(), option.getValue()));
                    }
                }
            }
        }
        return res;
    }

    private static boolean isVisible(final IPluginToolbox plugin, final InfoBean infoBean) {
        return plugin.isAccessible() && (plugin.getProcessus().isEmpty() || plugin.getProcessus().contains(infoBean.getNomProcessus()));
    }
}
