package com.kportal.extension.module.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class FicheBeanExport<T extends Serializable> extends AbstractBeanExport<T> {

    /**
     *
     */
    private static final long serialVersionUID = -3988622580137287500L;

    public boolean isPrivate;

    public String typeObjet;

    private Map<String, MediaBean> medias;

    private List<RessourceBean> ressources;

    private MetatagBean metatagBean;

    private List<RubriquepublicationBean> rubriquespublication;

    @JsonIgnore
    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Map<String, MediaBean> getMedias() {
        return medias;
    }

    public void setMedias(Map<String, MediaBean> medias) {
        this.medias = medias;
    }

    public List<RessourceBean> getRessources() {
        return ressources;
    }

    public void setRessources(List<RessourceBean> ressources) {
        this.ressources = ressources;
    }

    public MetatagBean getMetatag() {
        return metatagBean;
    }

    public void setMetatag(MetatagBean metatag) {
        this.metatagBean = metatag;
    }

    public List<RubriquepublicationBean> getRubriquespublication() {
        return rubriquespublication;
    }

    public void setRubriquespublication(List<RubriquepublicationBean> rubriquespublication) {
        this.rubriquespublication = rubriquespublication;
    }

    public String getTypeObjet() {
        return typeObjet;
    }

    public void setTypeObjet(String typeObjet) {
        this.typeObjet = typeObjet;
    }
}
