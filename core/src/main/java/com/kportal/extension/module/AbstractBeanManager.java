package com.kportal.extension.module;

import java.util.Observable;
import java.util.Observer;

public abstract class AbstractBeanManager extends Observable implements Observer {

    public ModuleManager moduleManager;

    public void init() {
        moduleManager.addObserver(this);
    }

    public ModuleManager getModuleManager() {
        return moduleManager;
    }

    public void setModuleManager(ModuleManager moduleManager) {
        this.moduleManager = moduleManager;
    }

    @Override
    public void update(Observable o, Object arg) {
        refresh();
        setChanged();
        notifyObservers();
    }

    public abstract void refresh();
}
