package com.kportal.extension.module.bean;

import java.util.Map;

import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.bean.MediaBean;

public class EncadreBeanExport extends AbstractBeanExport<EncadreBean> {

    /**
     *
     */
    private static final long serialVersionUID = 262931113670605500L;

    private Map<String, MediaBean> medias;

    public Map<String, MediaBean> getMedias() {
        return medias;
    }

    public void setMedias(Map<String, MediaBean> mediasEncadre) {
        this.medias = mediasEncadre;
    }
}
