package com.kportal.extension.module.composant;

import java.util.Collection;

import com.kportal.extension.module.ModuleHelper;

public class ComposantHelper {

    public static Collection<IComposant> getComposants() {
        return ModuleHelper.getModuleManager().getModules(IComposant.class);
    }
}
