package com.kportal.extension.module.composant;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.DefaultModuleImpl;
import com.univ.objetspartages.om.AutorisationBean;

/**
 * The Class Composant.
 */
public class Composant extends DefaultModuleImpl implements IComposant, Comparable<Composant> {

    /** l'id du bean parent si il existe. */
    public String idMenuBoParent;

    /** l'url d'accueil du composant. */
    public String urlAccueilBo;

    public ActionComposant actionDefaut;

    /** Son ordre dans la liste des composants. */
    public Integer ordre = 10000;

    /** Son processus. */
    public String parametreProcessus = StringUtils.EMPTY;

    /** si le composant est accessible dans le menu du back */
    public boolean accessibleBO = Boolean.TRUE;

    public List<ActionComposant> actions = new ArrayList<>();

    /**
     * Gets the id menu parent.
     *
     * @return the id menu parent
     */
    @Override
    public String getIdMenuBoParent() {
        return idMenuBoParent;
    }

    /**
     * Sets the id menu parent.
     *
     * @param idMenuParent
     *            the new id menu parent
     */
    public void setIdMenuBoParent(String idMenuParent) {
        this.idMenuBoParent = idMenuParent;
    }

    public ActionComposant getActionDefaut() {
        return actionDefaut;
    }

    public void setActionDefaut(ActionComposant actionDefaut) {
        this.actionDefaut = actionDefaut;
    }

    /**
     * Sets the url accueil bo.
     *
     * @param urlAccueilBo
     *            the new url accueil bo
     */
    public void setUrlAccueilBo(String urlAccueilBo) {
        this.urlAccueilBo = urlAccueilBo;
    }

    @Override
    public int getOrdre() {
        return ordre;
    }

    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

    public boolean isAccessibleBO() {
        return accessibleBO;
    }

    public void setAccessibleBO(boolean isAccessibleBO) {
        this.accessibleBO = isAccessibleBO;
    }

    /* (non-Javadoc)
     * @see com.kportal.extension.module.composant.IComposant#isVisible(com.univ.objetspartages.om.AutorisationBean)
     */
    @Override
    public boolean isVisible(AutorisationBean autorisation) {
        return accessibleBO && autorisation != null;
    }

    /**
     * Gets the parametre processus.
     *
     * @return the parametre processus
     */
    @Override
    public String getParametreProcessus() {
        return parametreProcessus;
    }

    /**
     * Sets the parametre processus.
     *
     * @param parametreProcessus
     *            the new parametre processus
     */
    public void setParametreProcessus(String parametreProcessus) {
        this.parametreProcessus = parametreProcessus;
    }

    @Override
    public int compareTo(Composant composant) {
        return this.ordre.compareTo(composant.ordre) != 0 ? this.ordre.compareTo(composant.ordre) : this.getLibelleAffichable().compareTo(composant.getLibelleAffichable());
    }

    @Override
    public List<ActionComposant> getActions() {
        return actions;
    }

    public void setActions(List<ActionComposant> actions) {
        this.actions = actions;
    }

    @Override
    public boolean isActionVisible(AutorisationBean autorisation, String code) {
        return accessibleBO && autorisation != null;
    }

    @Override
    public Menu getMenuAccueil(AutorisationBean autorisation) {
        if (autorisation != null) {
            Menu menuAccueil = new Menu();
            menuAccueil.setUrl(getUrlParAction(actionDefaut));
            menuAccueil.setLibelle(getLibelleAffichable());
            menuAccueil.setVisuel(actionDefaut.getCode().toLowerCase() + " " + getId());
            return menuAccueil;
        }
        return null;
    }

    @Override
    public Menu getMenuGeneral(AutorisationBean autorisation, InfoBean infoBean) {
        if (autorisation != null) {
            Menu menuActions = new Menu();
            for (ActionComposant action : actions) {
                if (isActionVisible(autorisation, action.getCode())) {
                    Menu menuActionCourante = new Menu();
                    menuActionCourante.setCode(action.getCode());
                    menuActionCourante.setLibelle(calculerLibelle(infoBean, action));
                    menuActionCourante.setUrl(getUrlParAction(action));
                    menuActionCourante.setVisuel(action.getCode().toLowerCase() + " " + getId());
                    menuActions.addSousMenu(menuActionCourante);
                }
            }
            return menuActions;
        }
        return null;
    }

    @Override
    public String getUrlAccueilBo() {
        if (StringUtils.isNotBlank(urlAccueilBo)) {
            return urlAccueilBo;
        } else if (actionDefaut != null) {
            return getUrlParAction(actionDefaut);
        }
        return StringUtils.EMPTY;
    }

    private String getUrlParAction(ActionComposant action) {
        StringBuilder urlAConstruire = new StringBuilder(WebAppUtil.SG_PATH + "?EXT=");
        urlAConstruire.append(getIdExtension()).append("&PROC=").append(getParametreProcessus()).append("&ACTION=");
        if (action != null) {
            urlAConstruire.append(action.getCode());
        } else {
            urlAConstruire.append("ACCUEIL");
        }
        return urlAConstruire.toString();
    }

    private String calculerLibelle(InfoBean infoBean, ActionComposant action) {
        StringBuilder codeLibelle = new StringBuilder();
        String key = "";
        if (StringUtils.isNotEmpty(infoBean.getNomProcessus())) {
            key = infoBean.getNomProcessus();
        } else {
            key = getId();
        }
        if (StringUtils.isNotBlank(action.getCode())) {
            codeLibelle.append(".").append(action.getCode());
        }
        codeLibelle.insert(0, key);
        String libelle = getMessage(codeLibelle.toString());
        if (StringUtils.isBlank(libelle)) {
            codeLibelle.replace(0, key.length(), "DEFAUT");
            libelle = getMessage(codeLibelle.toString());
        }
        return libelle;
    }

    @Override
    public String getLibelleMenu() {
        return getLibelleAffichable();
    }
}
