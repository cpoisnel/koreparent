package com.kportal.extension.module.service.impl.beanimport;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.usinesite.utils.UASOmHelper;
import com.kportal.extension.module.bean.AbstractBeanExport;
import com.kportal.extension.module.bean.FicheBeanExport;
import com.kportal.extension.module.service.ServiceBeanImport;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.om.AbstractFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.utils.ContexteDao;

public class ServiceBeanImportFiche implements ServiceBeanImport<FicheBeanExport<?>> {

    /** Logger available to subclasses. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceBeanImportFiche.class);

    private ServiceRubriquePublication serviceRubriquePublication;

    private ServiceMetatag serviceMetatag;

    public void setServiceRubriquePublication(final ServiceRubriquePublication serviceRubriquePublication) {
        this.serviceRubriquePublication = serviceRubriquePublication;
    }

    public void setServiceMetatag(ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    @Override
    public String importBeanExport(final AbstractBeanExport<?> beanExport, final String pathImport) {
        final FicheBeanExport<?> ficheBeanExport = (FicheBeanExport<?>) beanExport;
        if (ficheBeanExport.getMetatag() != null) {
            serviceMetatag.addWithForcedId(ficheBeanExport.getMetatag());
        }
        try (ContexteDao contexteDao = new ContexteDao()) {
            addFicheUniv(contexteDao, ficheBeanExport);
            UASOmHelper.addAllRessourceMediaBean(ficheBeanExport.getRessources(), ficheBeanExport.getMedias(), pathImport);
            addRubriquePublication(ficheBeanExport);
        } catch (final Exception e) {
            LOGGER.error(String.format("Une erreur est survenue lors de l'import de la fiche \"%s\"", ficheBeanExport), e);
        }
        return null;
    }

    private void addFicheUniv(final ContexteDao contexteDao, final FicheBeanExport<?> ficheBeanExport) throws Exception {
        final FicheUniv fiche = ReferentielObjets.instancierFiche(ficheBeanExport.getTypeObjet());
        fiche.init();
        fiche.setCtx(contexteDao);
        if (fiche instanceof AbstractFiche) {
            final AbstractFiche<?> finalFiche = (AbstractFiche<?>) fiche;
            UASOmHelper.copyProperties(finalFiche.getPersistenceBean(), ficheBeanExport.getBean());
        } else {
            UASOmHelper.copyProperties(fiche, ficheBeanExport.getBean());
        }
        fiche.addWithForcedId();
    }

    private void addRubriquePublication(final FicheBeanExport<?> ficheBeanExport) {
        if (CollectionUtils.isNotEmpty(ficheBeanExport.getRubriquespublication())) {
            for (final RubriquepublicationBean currentRubriquepublicationBean : ficheBeanExport.getRubriquespublication()) {
                serviceRubriquePublication.save(currentRubriquepublicationBean);
            }
        }
    }

}
