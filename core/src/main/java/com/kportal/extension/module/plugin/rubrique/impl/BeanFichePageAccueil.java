package com.kportal.extension.module.plugin.rubrique.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.ContexteUtil;
import com.univ.utils.UnivWebFmt;

/**
 * Created on 30/10/15.
 */
public class BeanFichePageAccueil implements BeanPageAccueil {

    private static final Logger LOG = LoggerFactory.getLogger(BeanFichePageAccueil.class);

    private String code = StringUtils.EMPTY;

    private String langue = StringUtils.EMPTY;

    private String objet = StringUtils.EMPTY;

    @Override
    public String getUrlRubrique(final String codeRubrique, final String codeLangue, final boolean ampersands) {
        return UnivWebFmt.determinerUrlFiche(ContexteUtil.getContexteUniv(), objet, code, langue, ampersands, codeRubrique);
    }

    @Override
    public String getUrlModification(final String codeRubrique, final String langue, final boolean ampersands) {
        if (ContexteUtil.getContexteUniv() != null) {
            final AutorisationBean autorisation = ContexteUtil.getContexteUniv().getAutorisation();
            FicheUniv fiche = null;
            try {
                fiche = getFichePageTete(true);
            } catch (final Exception e) {
                LOG.error("impossible de récupérer la fiche en page de tete",e);
            }
            if (fiche != null && autorisation.estAutoriseAModifierLaFiche(fiche)) {
                final String esperluette = ampersands ? "&amp;" : "&";
                return WebAppUtil.SG_PATH + "?EXT=" + ReferentielObjets.getExtension(fiche) + esperluette + "PROC=SAISIE_" + objet.toUpperCase() + esperluette + "ECRAN_LOGIQUE=LISTE" + esperluette + "ACTION=MODIFIER" + esperluette + "ID_FICHE=" + fiche.getIdFiche();
            }
        }
        return StringUtils.EMPTY;
    }

    @JsonIgnore
    public String getCodePageTete() {
        return StringUtils.isNotEmpty(code) ? code + ",LANGUE=" + langue + ",TYPE=" + objet : StringUtils.EMPTY;
    }

    /**
     * Retourne une instance de ficheUniv correspondant à la page de tete de la rubrique le parametre all permet de savoir si on doit retrouver juste la fiche si elle est en
     * ligne, ou bien si on peut rechercher dans tous les etats de fiche.
     * @param all
     *            the all
     *
     * @return the fiche page tete
     *
     * @throws Exception
     *             the exception
     */
    public FicheUniv getFichePageTete(final boolean all) throws Exception {
        FicheUniv ficheUniv = null;
        if (StringUtils.isNotEmpty(objet)) {
            ficheUniv = ReferentielObjets.instancierFiche(objet);
            if (ficheUniv != null) {
                ficheUniv.init();
                ficheUniv.setCtx(ContexteUtil.getContexteUniv());
                if (ficheUniv.selectCodeLangueEtat(code, langue, "0003") > 0) {
                    ficheUniv.nextItem();
                } else if (all) {
                    if (ficheUniv.selectCodeLangueEtat(code, langue, StringUtils.EMPTY) > 0) {
                        ficheUniv.nextItem();
                    }
                }
            }
        }
        return ficheUniv;
    }

    public void parseCodePageTete(final String codePageTete) {
        code = codePageTete;
        langue = "0";
        objet = "pagelibre";
        final int indiceLangue = codePageTete.indexOf(",LANGUE=");
        final int indiceType = codePageTete.indexOf(",TYPE=");
        if (indiceLangue != -1) {
            if (indiceType != -1) {
                langue = codePageTete.substring(indiceLangue + 8, indiceType);
            } else {
                langue = codePageTete.substring(indiceLangue + 8);
            }
            code = codePageTete.substring(0, indiceLangue);
        }
        if (indiceType != -1) {
            objet = codePageTete.substring(indiceType + 6);
        }
    }

    @Override
    public String getLibelleAffichable() {
        String res = ReferentielObjets.getLibelleObjet(objet);
        try {
            final FicheUniv fiche = getFichePageTete(false);
            if (fiche != null) {
                res += " - " + fiche.getLibelleAffichable();
            }
        } catch (final Exception e) {
            LOG.debug("unable to find the ficheUniv", e);
        }
        return res;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(final String langue) {
        this.langue = langue;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(final String objet) {
        this.objet = objet;
    }
}
