package com.kportal.extension.module.bean;

import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RubriqueBean;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class RubriqueBeanExport extends AbstractBeanExport<RubriqueBean> {

    /**
     *
     */
    private static final long serialVersionUID = -6781868813305653915L;

    private boolean rubriqueSite;

    private Map<String, MediaBean> medias;

    public boolean isRubriqueSite() {
        return rubriqueSite;
    }

    public void setRubriqueSite(final boolean rubriqueSite) {
        this.rubriqueSite = rubriqueSite;
    }

    public Map<String, MediaBean> getMedias() {
        return medias;
    }

    public void setMedias(final Map<String, MediaBean> medias) {
        this.medias = medias;
    }
}
