package com.kportal.extension.module.plugin.rubrique;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Map;

import com.kportal.extension.module.DefaultModuleImpl;

public abstract class DefaultPageAccueilRubrique<T extends BeanPageAccueil> extends DefaultModuleImpl implements IPageAccueilRubrique<T>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -9174663209559535271L;

    String typeRubrique = "";

    String pathSaisieBo = "";

    String pathSaisieFo = "";

    Class<T> classeBeanPageAccueil;

    boolean interne = true;

    public DefaultPageAccueilRubrique() {
        classeBeanPageAccueil = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public String getTypeRubrique() {
        return typeRubrique;
    }

    @Override
    public String getPathSaisieBO() {
        return pathSaisieBo;
    }

    @Override
    public String getPathSaisieFO() {
        return pathSaisieFo;
    }

    public void setPathSaisieBo(final String pathSaisieBo) {
        this.pathSaisieBo = pathSaisieBo;
    }

    public void setPathSaisieFo(final String pathSaisieFo) {
        this.pathSaisieFo = pathSaisieFo;
    }

    public void setTypeRubrique(final String typeRubrique) {
        this.typeRubrique = typeRubrique;
    }

    @Override
    public abstract void preparerPRINCIPAL(Map<String, Object> infoBean, T bean) throws Exception;

    @Override
    public abstract void traiterPRINCIPAL(Map<String, Object> infoBean, T bean) throws Exception;

    @Override
    public Class<T> getClasseBeanPageAccueil() {
        return classeBeanPageAccueil;
    }

    @Override
    public boolean isInterne(final BeanPageAccueil bean) {
        return interne;
    }

    public void setInterne(final boolean interne) {
        this.interne = interne;
    }
}
