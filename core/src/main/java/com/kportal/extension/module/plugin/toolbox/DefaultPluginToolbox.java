package com.kportal.extension.module.plugin.toolbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kportal.core.autorisation.ActionPermission;
import com.kportal.core.autorisation.Permission;
import com.kportal.extension.module.DefaultModuleImpl;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

public class DefaultPluginToolbox extends DefaultModuleImpl implements IPluginToolbox {

    public String path = "";

    /**
     * Utiliser plutôt les permissions dédiées à la toolbox qui contiennent ces informations.
     */
    @Deprecated
    public Map<String, String> options = new HashMap<>();

    public List<String> processus = new ArrayList<>();

    @Override
    public List<String> getProcessus() {
        return processus;
    }

    public void setProcessus(List<String> processus) {
        this.processus = processus;
    }

    @Override
    @Deprecated
    public Map<String, String> getOptions() {
        return options;
    }

    @Deprecated
    public void setOptions(Map<String, String> options) {
        this.options = options;
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRelativePath() {
        return PluginToolboxHelper.getRelativePath(this);
    }

    @Override
    public boolean isAccessible() {
        boolean res = Boolean.TRUE;
        bouclePermission:
        for (Permission permission : getPermissions()) {
            res = Boolean.FALSE;
            ContexteUniv ctx = ContexteUtil.getContexteUniv();
            if (ctx != null && ctx.getAutorisation() != null) {
                if (!permission.getActions().isEmpty()) {
                    for (ActionPermission action : permission.getActions()) {
                        PermissionBean permissionBean = new PermissionBean(permission.getId(), permission.getCode(), action.getCode());
                        if (ctx.getAutorisation().getListePermissions().get(permissionBean.getChaineSerialisee()) == null) {
                            break bouclePermission;
                        }
                    }
                } else {
                    PermissionBean permissionBean = new PermissionBean(permission.getId(), permission.getCode(), "");
                    if (ctx.getAutorisation().getListePermissions().get(permissionBean.getChaineSerialisee()) == null) {
                        break;
                    }
                }
                res = Boolean.TRUE;
            }
        }
        return res;
    }
}
