package com.kportal.extension.module.service.impl.beanimport;

import com.kosmos.usinesite.utils.UASOmHelper;
import com.kportal.extension.module.bean.AbstractBeanExport;
import com.kportal.extension.module.bean.RubriqueBeanExport;
import com.kportal.extension.module.service.ServiceBeanImport;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;

public class ServiceBeanImportRubrique implements ServiceBeanImport<RubriqueBeanExport> {

    private ServiceRubrique serviceRubrique;

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    @Override
    public String importBeanExport(final AbstractBeanExport<?> beanExport, final String pathImport) {
        final RubriqueBeanExport rubriqueBeanExport = (RubriqueBeanExport) beanExport;
        RubriqueBean rubriqueBean = rubriqueBeanExport.getBean();
        if (rubriqueBeanExport.isRubriqueSite()) {
            rubriqueBean.setIntitule(String.format("%s [Copie]", rubriqueBean.getIntitule()));
        }
        serviceRubrique.save(rubriqueBean);
        UASOmHelper.addAllMediaBean(rubriqueBeanExport.getMedias(), pathImport);
        if (rubriqueBeanExport.isRubriqueSite()) {
            return rubriqueBeanExport.getBean().getCode();
        } else {
            return null;
        }
    }
}
