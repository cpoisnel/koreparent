package com.kportal.extension.module.service.impl.beanexport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.usinesite.utils.UASOmHelper;
import com.kosmos.usinesite.utils.UASServicesHelper;
import com.kportal.extension.module.bean.BeanExportMap;
import com.kportal.extension.module.bean.FicheBeanExport;
import com.kportal.extension.module.service.ServiceBeanExport;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Metatag;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.utils.ContexteDao;

public class ServiceBeanExportFiche implements ServiceBeanExport<FicheBeanExport<?>> {

    private ServiceMetatag serviceMetatag;

    private ServiceRessource serviceRessource;

    private ServiceRubriquePublication serviceRubriquePublication;

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    public void setServiceRessource(final ServiceRessource serviceRessource) {
        this.serviceRessource = serviceRessource;
    }

    public void setServiceRubriquePublication(final ServiceRubriquePublication serviceRubriquePublication) {
        this.serviceRubriquePublication = serviceRubriquePublication;
    }

    @Override
    public BeanExportMap<FicheBeanExport<?>> getBeansByRubrique(final List<String> codesRubrique, final String idModule, final String pathExport, final String... params) throws Exception {
        final BeanExportMap<FicheBeanExport<?>> fichesBeans = new BeanExportMap<>();
        try (ContexteDao ctx = new ContexteDao()) {
            final String[] queryParams = new String[2];
            if (StringUtils.isNotEmpty(getParam(params, 0))) {
                queryParams[0] = params[0];
            }
            if (StringUtils.isNotEmpty(getParam(params, 1))) {
                queryParams[1] = params[1];
            }
            final List<MetatagBean> metas = serviceMetatag.getMetasForRubriquesAndObjet(codesRubrique, queryParams);
            for (MetatagBean currentMeta : metas) {
                processContent(ctx, idModule, currentMeta, codesRubrique, fichesBeans, pathExport);
            }
        }
        return fichesBeans;
    }

    private String getParam(final String[] params, final int index) {
        if (params.length > index) {
            return params[index];
        }
        return StringUtils.EMPTY;
    }

    /**
     * export du contenu et d es ressources liées
     * @param ctx le contexte pour l'accès au données...
     * @param idModule l'id du module à exporter
     * @param metatag le meta de la fiche à exporter
     * @param codesRubriquesExportees l'ensemble des codes de rubriques à exporter
     * @param fichesBeans les fiches déjà exporter
     * @param pathExport le chemin de l'export
     * @throws Exception
     * @deprecated Utilisez {@link #processContent(OMContext, String, MetatagBean, List, Map, String)}
     */
    @Deprecated
    public void processContent(final OMContext ctx, final String idModule, final Metatag metatag, final List<String> codesRubriquesExportees, final Map<String, FicheBeanExport<?>> fichesBeans, final String pathExport) throws Exception {
        processContent(ctx, idModule, metatag.getPersistenceBean(), codesRubriquesExportees, fichesBeans, pathExport);
    }

    /**
     * export du contenu et d es ressources liées
     * @param ctx le contexte pour l'accès au données...
     * @param idModule l'id du module à exporter
     * @param metatag le meta de la fiche à exporter
     * @param codesRubriquesExportees l'ensemble des codes de rubriques à exporter
     * @param fichesBeans les fiches déjà exporter
     * @param pathExport le chemin de l'export
     * @throws Exception
     */
    public void processContent(final OMContext ctx, final String idModule, final MetatagBean metatag, final List<String> codesRubriquesExportees, final Map<String, FicheBeanExport<?>> fichesBeans, final String pathExport) throws Exception {
        final String typeObjet = ReferentielObjets.getNomObjet(metatag.getMetaCodeObjet());
        final String codeFiche = metatag.getMetaCode();
        final String langue = metatag.getMetaLangue();
        if (!fichesBeans.containsKey(typeObjet + "," + codeFiche + "," + langue)) {
            final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(typeObjet);
            if (ficheUniv != null) {
                ficheUniv.setCtx(ctx);
                ficheUniv.init();
                if (ficheUniv.selectCodeLangueEtat(codeFiche, langue, metatag.getMetaEtatObjet()) > 0) {
                    ficheUniv.nextItem();
                    final FicheBeanExport<?> beanExport = UASServicesHelper.getFicheBeanExport(ficheUniv, UASServicesHelper.getBeanClass(ficheUniv));
                    beanExport.setTypeObjet(typeObjet);
                    beanExport.setIdModule(idModule);
                    //export du metatag pour ne pas perdre certaines propriétés de la fiche : structure visible, forum actif...
                    final MetatagBean metatagBean = new MetatagBean();
                    UASOmHelper.copyProperties(metatagBean, metatag);
                    beanExport.setMetatag(metatagBean);
                    // export des rubriques de publication
                    final List<RubriquepublicationBean> rubPubForFiche =  serviceRubriquePublication.getByFicheUniv(ficheUniv);
                    final List<RubriquepublicationBean> listeRubpub = new ArrayList<>();
                    for (RubriquepublicationBean rubpub : rubPubForFiche) {
                        if (codesRubriquesExportees.contains(rubpub.getRubriqueDest())) {
                            listeRubpub.add(rubpub);
                        }
                    }
                    beanExport.setRubriquespublication(listeRubpub);
                    // gestion des fichiers
                    final List<RessourceBean> vRessources = serviceRessource.getFullFileList(ficheUniv);
                    // gestion des médias
                    final Map<String, MediaBean> medias = UASServicesHelper.getMediasByRessources(vRessources, pathExport);
                    medias.putAll(UASServicesHelper.getMedias(ficheUniv, pathExport, Boolean.FALSE));
                    beanExport.setRessources(vRessources);
                    beanExport.setMedias(medias);
                    //ajout du contenu de la fiche dans le tableau des contenus à exporter
                    fichesBeans.put(typeObjet + "," + codeFiche + "," + langue, beanExport);
                }
            }
        }
    }
}
