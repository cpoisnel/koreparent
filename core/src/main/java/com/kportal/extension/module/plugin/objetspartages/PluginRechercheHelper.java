package com.kportal.extension.module.plugin.objetspartages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.kportal.extension.module.ModuleHelper;
import com.kportal.frontoffice.util.JSPIncludeHelper;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.ContexteUtil;
import com.univ.utils.RequeteUtil;

public class PluginRechercheHelper {

    public static Collection<IPluginRecherche> getPlugins() {
        return ModuleHelper.getModuleManager().getModules(IPluginRecherche.class);
    }

    public static IPluginRecherche getPluginActif(final String requete) {
        final String objet = RequeteUtil.renvoyerParametre(requete, "OBJET");
        final Object object = ReferentielObjets.instancierObjet(objet);
        if (object != null) {
            for (final IPluginRecherche plugin : getPlugins()) {
                if (plugin.checkClasse(object.getClass().getName())) {
                    if (plugin.isActif(requete)) {
                        return plugin;
                    }
                }
            }
        }
        return null;
    }

    public static void traiterRecherche(final OMContext ctx, final InfoBean infoBean) throws Exception {
        final String objet = infoBean.getString("CODE_OBJET");
        final Object object = ReferentielObjets.instancierObjet(objet);
        for (final IPluginRecherche plugin : getPlugins()) {
            if (plugin.checkClasse(object.getClass().getName())) {
                final Map<String, Object> map = infoBean.getValues();
                plugin.taiterRecherche(ctx, map);
                infoBean.setValues(map);
            }
        }
    }

    public static void preparerRecherche(final OMContext ctx, final InfoBean infoBean) throws Exception {
        final String objet = infoBean.getString("CODE_OBJET");
        final Object object = ReferentielObjets.instancierObjet(objet);
        for (final IPluginRecherche plugin : getPlugins()) {
            if (plugin.checkClasse(object.getClass().getName())) {
                final Map<String, Object> map = infoBean.getValues();
                plugin.preparerRecherche(ctx, map);
                infoBean.setValues(map);
            }
        }
    }

    public static List<String> getCriteresRequete(final String objet) {
        final List<String> criteres = new ArrayList<>();
        final Object object = ReferentielObjets.instancierObjet(objet);
        for (final IPluginRecherche plugin : getPlugins()) {
            if (plugin.checkClasse(object.getClass().getName())) {
                criteres.addAll(plugin.getCriteresRequete());
            }
        }
        return criteres;
    }

    /**
     * @deprecated Cette méthode était utilisé uniquement pour la recherche par fil qui n'est plus activé sur le BO.
     * @param out
     * @param context
     * @param request
     * @param response
     * @param objet
     * @param listeIncluse
     */
    @Deprecated
    public static void includePluginRechercheBo(final JspWriter out, final ServletContext context, final HttpServletRequest request, final HttpServletResponse response, final String objet, final boolean listeIncluse) {
        ContexteUtil.getContexteUniv().getDatas().put("listeIncluse", listeIncluse);
        final Object object = ReferentielObjets.instancierObjet(objet);
        for (final IPluginRecherche plugin : getPlugins()) {
            if (plugin.checkClasse(object.getClass().getName())) {
                JSPIncludeHelper.includeExtensionTemplate(out, context, request, response, plugin.getIdExtension(), plugin.getPathRechercheBo());
            }
        }
    }
}
