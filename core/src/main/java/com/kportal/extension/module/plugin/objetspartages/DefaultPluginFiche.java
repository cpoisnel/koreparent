package com.kportal.extension.module.plugin.objetspartages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.context.BeanUtil;
import com.kportal.extension.module.DefaultModuleImpl;
import com.kportal.extension.module.plugin.objetspartages.om.ObjetPluginContenu;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Metatag;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.utils.ContexteDao;
import com.univ.utils.ContexteUtil;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

public class DefaultPluginFiche extends DefaultModuleImpl implements IPluginFiche {

    /**
     * le path vers la jsp de saisie du contenu spécifique au plugin pour le BO
     */
    private String pathSaisieBo = "";
    /**
     * le path vers la jsp de saisie du contenu spécifique au plugin pour le FO
     */
    private String pathSaisieFo = "";
    /**
     * le path vers la jsp d'affichage des données du plugin contenu spécifique au plugin pour le FO
     */
    private String pathTemplateFo = "";
    /**
     * Liste des classes sur lesquelles le plugin est activé
     */
    private List<String> classes;
    /**
     * Liste des classes de type <ObjetPluginContenu> qui doivent être synchronisés par le plugin
     */
    private List<String> objets;

    protected ServiceMetatag serviceMetatag;

    @Override
    public List<String> getObjets() {
        return objets;
    }

    public void setObjets(final List<String> objets) {
        this.objets = objets;
    }

    @Override
    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(final List<String> classes) {
        this.classes = classes;
    }

    @Override
    public String getPathSaisieBo() {
        return pathSaisieBo;
    }

    public void setPathSaisieBo(final String pathSaisieB0) {
        this.pathSaisieBo = pathSaisieB0;
    }

    @Override
    public String getPathSaisieFo() {
        return pathSaisieFo;
    }

    public void setPathSaisieFo(final String pathSaisieF0) {
        this.pathSaisieFo = pathSaisieF0;
    }

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    @Override
    public boolean isActive(final String classe) {
        if (classes == null) {
            return Boolean.TRUE;
        } else {
            if (classes.contains(classe)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Override
    public boolean hasObjet(final String classe) {
        if (StringUtils.isEmpty(classe)) {
            return Boolean.TRUE;
        } else if (objets != null && objets.contains(classe)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public void postTraiterPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
    }

    @Override
    public void preparerPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
    }

    @Override
    public void traiterPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
    }

    @Override
    public void preTraiterPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
    }

    @Override
    public void supprimerObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjetCible) throws Exception {
        for (final String classeObjet : getListeObjets(classeObjetCible)) {
            final Object objet = Class.forName(classeObjet).newInstance();
            if (objet instanceof ObjetPluginContenu) {
                final ObjetPluginContenu sousObjet = (ObjetPluginContenu) objet;
                try (ContexteDao ctx = new ContexteDao()) {
                    sousObjet.setCtx(ctx);
                    ClauseWhere where = new ClauseWhere(ConditionHelper.egal("ID_META", meta.getId(), TypeOperande.LONG));
                    sousObjet.select(where.formaterSQL());
                    while (sousObjet.nextItem()) {
                        sousObjet.delete();
                    }
                }
            }
        }
    }

    @Override
    public List<ObjetPluginContenu> lireObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        return getObjets(meta.getId(), classeObjet);
    }

    @Override
    public void synchroniserObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
    }

    @Override
    public void dupliquerObjets(final FicheUniv ficheUniv, final MetatagBean meta, final Long oldIdMeta, final String classeObjet) throws Exception {
        ajouterObjets(ficheUniv, meta, getObjets(oldIdMeta, classeObjet), classeObjet);
    }

    @Override
    public void ajouterObjets(final FicheUniv ficheUniv, final MetatagBean meta, final List<ObjetPluginContenu> listeObjet, final String classeObjetCible) throws Exception {
        for (final String classeObjet : getListeObjets(classeObjetCible)) {
            for (final ObjetPluginContenu objetPluginContenu : listeObjet) {
                if (objetPluginContenu != null && objetPluginContenu.getClass().getName().equals(classeObjet)) {
                    objetPluginContenu.setIdMeta(meta.getId());
                    objetPluginContenu.add();
                }
            }
        }
    }

    public List<String> getListeObjets(final String classeObjetCible) {
        final List<String> lstObjets = new ArrayList<>();
        if (StringUtils.isNotEmpty(classeObjetCible)) {
            lstObjets.add(classeObjetCible);
        } else {
            if (objets != null) {
                lstObjets.addAll(objets);
            }
        }
        return lstObjets;
    }

    @SuppressWarnings("deprecation")
    private List<ObjetPluginContenu> getObjets(final Long idMeta, final String classeObjetCible) throws Exception {
        final ArrayList<ObjetPluginContenu> sousObjets = new ArrayList<>();
        for (final String classeObjet : getListeObjets(classeObjetCible)) {
            final Object objet = Class.forName(classeObjet).newInstance();
            if (objet instanceof ObjetPluginContenu) {
                final ObjetPluginContenu sousObjet = (ObjetPluginContenu) objet;
                try (ContexteDao ctxDao = new ContexteDao()) {
                    sousObjet.setCtx(ctxDao);
                    sousObjet.selectByMeta(idMeta);
                    while (sousObjet.nextItem()) {
                        sousObjets.add(sousObjet.cloneObjet());
                    }
                }
            }
        }
        return sousObjets;
    }

    @Override
    public void setDataContexteUniv(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        ContexteUtil.getContexteUniv().putData(BeanUtil.getBeanKey(getId(), getIdExtension()), lireObjets(ficheUniv, meta, classeObjet));
    }

    @Override
    public Object getDataContexteUniv() {
        return ContexteUtil.getContexteUniv().getData(BeanUtil.getBeanKey(getId(), getIdExtension()));
    }

    @Override
    public String getPathTemplateFo() {
        return pathTemplateFo;
    }

    public void setPathTemplateFo(final String pathTemplateF0) {
        this.pathTemplateFo = pathTemplateF0;
    }

    //FIXME : rétro-compatibilité
    @Override
    @Deprecated
    public void setDataContexteUniv(final FicheUniv ficheUniv, final Metatag meta, final String classeObjet) throws Exception {
        setDataContexteUniv(ficheUniv, meta.getPersistenceBean(), classeObjet);
    }

    @Override
    @Deprecated
    public void postTraiterPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        postTraiterPrincipal(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    @Override
    @Deprecated
    public void preparerPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        preparerPrincipal(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    @Override
    @Deprecated
    public void traiterPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        traiterPrincipal(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    @Override
    @Deprecated
    public void preTraiterPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        preTraiterPrincipal(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    @Override
    @Deprecated
    public void supprimerObjets(final FicheUniv ficheUniv, final Metatag meta, final String classeObjetCible) throws Exception {
        supprimerObjets(ficheUniv, meta.getPersistenceBean(), classeObjetCible);
    }

    @Override
    @Deprecated
    public List<ObjetPluginContenu> lireObjets(final FicheUniv ficheUniv, final Metatag meta, final String classeObjet) throws Exception {
        return lireObjets(ficheUniv, meta.getPersistenceBean(), classeObjet);
    }

    @Override
    @Deprecated
    public void synchroniserObjets(final FicheUniv ficheUniv, final Metatag meta, final String classeObjet) throws Exception {
        synchroniserObjets(ficheUniv, meta.getPersistenceBean(), classeObjet);
    }

    @Override
    @Deprecated
    public void dupliquerObjets(final FicheUniv ficheUniv, final Metatag meta, final Long oldIdMeta, final String classeObjet) throws Exception {
        dupliquerObjets(ficheUniv, meta.getPersistenceBean(), oldIdMeta, classeObjet);
    }

    @Override
    @Deprecated
    public void ajouterObjets(final FicheUniv ficheUniv, final Metatag meta, final List<ObjetPluginContenu> listeObjet, final String classeObjetCible) throws Exception {
        ajouterObjets(ficheUniv, meta.getPersistenceBean(), listeObjet, classeObjetCible);
    }

    @Override
    public void apresMiseEnLigne(final FicheUniv ficheUniv, final Long idMeta, final Long oldIdMeta, final String classeObjet) {}
}
