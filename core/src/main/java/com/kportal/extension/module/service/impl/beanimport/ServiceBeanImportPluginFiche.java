package com.kportal.extension.module.service.impl.beanimport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.usinesite.utils.UASOmHelper;
import com.kportal.extension.module.bean.AbstractBeanExport;
import com.kportal.extension.module.bean.PluginFicheBeanExport;
import com.kportal.extension.module.plugin.objetspartages.om.ObjetPluginContenu;
import com.kportal.extension.module.plugin.objetspartages.om.SousObjet;
import com.kportal.extension.module.service.ServiceBeanImport;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.utils.ContexteDao;

public class ServiceBeanImportPluginFiche implements ServiceBeanImport<PluginFicheBeanExport<?>> {

    /** Logger available to subclasses. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceBeanImportPluginFiche.class);

    @SuppressWarnings("deprecation")
    @Override
    public String importBeanExport(final AbstractBeanExport<?> beanExport, final String pathImport) {
        final PluginFicheBeanExport<?> pluginFicheBeanExport = (PluginFicheBeanExport<?>) beanExport;
        try (ContexteDao contexteDao = new ContexteDao()) {
            final Class<?> clazz = Class.forName(pluginFicheBeanExport.getClasseObjet());
            final Object object = clazz.newInstance();
            if (object instanceof ObjetPluginContenu) {
                final ObjetPluginContenu objetPluginContenu = (ObjetPluginContenu) object;
                objetPluginContenu.init();
                objetPluginContenu.setCtx(contexteDao);
                if (objetPluginContenu instanceof AbstractOm<?, ?>) {
                    final AbstractOm<?, ?> finalFiche = (AbstractOm<?, ?>) objetPluginContenu;
                    UASOmHelper.copyProperties(finalFiche.getPersistenceBean(), pluginFicheBeanExport.getBean());
                } else {
                    UASOmHelper.copyProperties(objetPluginContenu, pluginFicheBeanExport.getBean());
                }
                objetPluginContenu.add();
            }
            if (object instanceof SousObjet) {
                final SousObjet sousObjet = (SousObjet) object;
                sousObjet.init();
                sousObjet.setCtx(contexteDao);
                if (sousObjet instanceof AbstractOm<?, ?>) {
                    final AbstractOm<?, ?> finalFiche = (AbstractOm<?, ?>) sousObjet;
                    UASOmHelper.copyProperties(finalFiche.getPersistenceBean(), pluginFicheBeanExport.getBean());
                } else {
                    UASOmHelper.copyProperties(sousObjet, pluginFicheBeanExport.getBean());
                }
                sousObjet.add();
            }
            UASOmHelper.addAllRessourceMediaBean(pluginFicheBeanExport.getRessources(), pluginFicheBeanExport.getMedias(), pathImport);
        } catch (final Exception e) {
            LOGGER.error(String.format("Une erreur est survenue lors de l'import de la fiche \"%s\"", pluginFicheBeanExport), e);
        }
        return null;
    }
}
