package com.kportal.extension.module.plugin.objetspartages;

import java.util.List;
import java.util.Map;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.textsearch.ResultatRecherche;
import com.kportal.extension.module.IModule;

/**
 * The Interface IPluginRequete.
 */
public interface IPluginRecherche extends IModule {

    /**
     * Checks if is actif.
     *
     * @param requete
     *            the requete
     * @return true, if is actif
     */
    boolean isActif(String requete);

    /**
     * Checks if is exclusif.
     *
     * @return true, if is exclusif
     */
    boolean isExclusif();

    /**
     * Annule critere nombre.
     *
     * @param requete
     *            the requete
     * @return true, if successful
     */
    boolean annuleCritereNombre(String requete);

    /**
     * Preparer requete.
     */
    String preparerRequete(OMContext ctx, String requete) throws Exception;

    /**
     * Preparer recherche.
     */
    Map<String, Object> preparerRecherche(OMContext ctx, Map<String, Object> infoBean) throws Exception;

    /**
     * Preparer requete.
     */
    Map<String, Object> taiterRecherche(OMContext ctx, Map<String, Object> infoBean) throws Exception;

    /**
     * Traiter requete.
     *
     * @param ctx
     *            the ctx
     * @param requete
     *            the requete
     * @param res
     *            the res
     * @return the list
     * @throws Exception
     *             the exception
     */
    List<ResultatRecherche> traiterRequete(OMContext ctx, String requete, List<ResultatRecherche> res) throws Exception;

    /**
     * Check classes.
     *
     * @param name
     *            the name
     * @return the boolean
     */
    boolean checkClasse(String name);

    /**
     * Renvoie le path vers le template jsp incluant des champs de recherche
     * @return le path
     */
    String getPathRechercheBo();

    /**
     * Renvoie la liste des criteres de recherche
     * @return le path
     */
    List<String> getCriteresRequete();
}
