package com.kportal.extension.module.plugin.toolbox;

import java.util.ArrayList;
import java.util.Collection;

import com.kportal.extension.module.DefaultModuleImpl;
import com.kportal.tag.extracteur.ExtracteurTag;
import com.kportal.tag.interpreteur.InterpreteurTag;

public class DefaultPluginTag extends DefaultModuleImpl implements IPluginTag {

    private ExtracteurTag extracteur;

    private InterpreteurTag interpreteur;

    private Collection<String> contexteTag = new ArrayList<>();

    private String baliseOuvrante;

    private String baliseFermante;

    private String identifiantTag;

    @Override
    public String extraitTagDuContenu(String texte) {
        return extracteur.getContenuTagPresentDansTexte(texte, baliseOuvrante, baliseFermante);
    }

    @Override
    public String interpreteTag(String texteAInterpreter) throws Exception {
        return interpreteur.interpreterTag(texteAInterpreter, baliseOuvrante, baliseFermante);
    }

    @Override
    public String getReferenceTag(String texteAInterpreter) {
        return interpreteur.getReferenceTag(texteAInterpreter, baliseOuvrante, baliseFermante);
    }

    public void setBaliseOuvrante(String baliseOuvrante) {
        this.baliseOuvrante = baliseOuvrante;
    }

    public void setBaliseFermante(String baliseFermante) {
        this.baliseFermante = baliseFermante;
    }

    public void setExtracteur(ExtracteurTag extracteur) {
        this.extracteur = extracteur;
    }

    public void setInterpreteur(InterpreteurTag interpreteur) {
        this.interpreteur = interpreteur;
    }

    @Override
    public void setContexteTag(Collection<String> contexteTag) {
        this.contexteTag = contexteTag;
    }

    @Override
    public Collection<String> getContexte() {
        return contexteTag;
    }

    @Override
    public String getIdentifiantTag() {
        return identifiantTag;
    }

    public void setIdentifiantTag(String identifiantTag) {
        this.identifiantTag = identifiantTag;
    }
}
