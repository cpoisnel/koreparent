package com.kportal.extension.module.plugin.toolbox;

import java.util.Collection;
import java.util.Map;

import com.kportal.extension.module.AbstractBeanManager;
import com.kportal.tag.util.ContexteTagUtil;

public class PluginTagManager extends AbstractBeanManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "pluginTagToolboxManager";

    private Map<String, Collection<IPluginTag>> tagsParType;

    public Map<String, Collection<IPluginTag>> getTagsParType() {
        return tagsParType;
    }

    @Override
    public void refresh() {
        final Collection<IPluginTag> tagsDeclares = moduleManager.getModules(IPluginTag.class);
        tagsParType = ContexteTagUtil.getTagsParContexte(tagsDeclares);
    }
}
