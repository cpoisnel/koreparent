package com.kportal.extension.module.plugin.objetspartages;

import java.util.Map;

import com.kportal.extension.module.DefaultModuleImpl;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Metatag;

public class DefaultPluginSaisieFiche extends DefaultModuleImpl implements IPluginSaisieFiche {

    @Override
    public void preTraiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, Metatag meta) throws Exception {
        preTraiterAction(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    @Override
    public void traiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, Metatag meta) throws Exception {
        traiterAction(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    @Override
    public void postTraiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, Metatag meta) throws Exception {
        postTraiterAction(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    @Override
    public void preTraiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception {
    }

    @Override
    public void traiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception {
    }

    @Override
    public void postTraiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception {
    }
}
