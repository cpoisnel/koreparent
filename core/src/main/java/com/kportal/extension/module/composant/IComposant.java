package com.kportal.extension.module.composant;

import java.util.List;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.extension.module.IModule;
import com.univ.objetspartages.om.AutorisationBean;

public interface IComposant extends IModule {

    String getIdMenuBoParent();

    boolean isVisible(AutorisationBean autorisation);

    String getUrlAccueilBo();

    int getOrdre();

    String getParametreProcessus();

    List<ActionComposant> getActions();

    boolean isActionVisible(AutorisationBean autorisation, String code);

    Menu getMenuAccueil(AutorisationBean autorisation);

    String getLibelleMenu();

    Menu getMenuGeneral(AutorisationBean autorisation, InfoBean infoBean);
}
