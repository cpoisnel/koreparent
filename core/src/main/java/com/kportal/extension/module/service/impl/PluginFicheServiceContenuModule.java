package com.kportal.extension.module.service.impl;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.plugin.objetspartages.IPluginFiche;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheObjet;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.processus.ControleurFichiergw;

public class PluginFicheServiceContenuModule extends DefaultServiceContenuModule {

    public static final String PARAM_META_CODE_OBJET = "META_CODE_OBJET=";

    public static final String PARAM_CLASSE = "CLASSE=";

    @Override
    public void init() {
        modules = PluginFicheHelper.getPlugins();
        for (final IPluginFiche plugin : PluginFicheHelper.getPlugins()) {
            // on filtre les plugins non actifs (liste des classes d'application vide), les fiches objets (exportees avec les fiches) et en dur les fichiers
            if (plugin.getClass().isAssignableFrom(ControleurFichiergw.class) || plugin instanceof PluginFicheObjet || CollectionUtils.isEmpty(plugin.getObjets()) || plugin.getClasses() != null && plugin.getClasses().isEmpty()) {
                modules.remove(plugin);
            }
        }
    }

    @Override
    public TypeContenu getTypeContenu() {
        return TypeContenu.TECHNIQUE;
    }

    @Override
    /**
     * Renvoit la liste des objets parents et des plugins (nom des classe) à exporter
     */ public String[] recupereParamsExport(final IModule moduleAExporter, final Collection<IModule> modulesExportes) {
        String[] paramsExport = null;
        if (this.modules.contains(moduleAExporter)) {
            final IPluginFiche plugin = ((IPluginFiche) moduleAExporter);
            if (plugin.getClasses() != null) {
                for (final String nomClasse : plugin.getClasses()) {
                    final Objetpartage moduleObjet = ReferentielObjets.getObjetByCode(ReferentielObjets.getCodeObjetParClasse(nomClasse));
                    // si le plugin est actif pour un des objets exportes
                    if (modulesExportes.contains(moduleObjet)) {
                        paramsExport = ArrayUtils.add(paramsExport, PARAM_META_CODE_OBJET + moduleObjet.getCodeObjet());
                    }
                }
            }
            // on ajoute le plugin pour tous les objets exportes
            else {
                for (final IModule module : modulesExportes) {
                    if (module instanceof Objetpartage) {
                        paramsExport = ArrayUtils.add(paramsExport, PARAM_META_CODE_OBJET + ((Objetpartage) module).getCodeObjet());
                    }
                }
            }
            // si au moins une classe objet parente exportée
            if (ArrayUtils.isNotEmpty(paramsExport)) {
                for (final String objet : plugin.getObjets()) {
                    paramsExport = ArrayUtils.add(paramsExport, PARAM_CLASSE + objet);
                }
            }
        }
        return paramsExport;
    }
}
