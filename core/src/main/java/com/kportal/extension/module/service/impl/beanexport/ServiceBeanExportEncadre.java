package com.kportal.extension.module.service.impl.beanexport;

import java.util.List;

import com.kosmos.usinesite.utils.UASOmHelper;
import com.kosmos.usinesite.utils.UASServicesHelper;
import com.kportal.extension.module.bean.BeanExportMap;
import com.kportal.extension.module.bean.EncadreBeanExport;
import com.kportal.extension.module.service.ServiceBeanExport;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.services.ServiceEncadre;
import com.univ.utils.ContexteUtil;

public class ServiceBeanExportEncadre implements ServiceBeanExport<EncadreBeanExport> {

    private ServiceEncadre serviceEncadre;

    public void setServiceEncadre(ServiceEncadre serviceEncadre) {
        this.serviceEncadre = serviceEncadre;
    }

    @Override
    public BeanExportMap<EncadreBeanExport> getBeansByRubrique(final List<String> codesRubrique, final String idModule, final String pathExport, final String... params) throws Exception {
        final BeanExportMap<EncadreBeanExport> exportEncadres = new BeanExportMap<>();
        final List<EncadreBean> encadres = serviceEncadre.getByCodesRubrique(codesRubrique);
        for (EncadreBean currentEncadre : encadres) {
            final EncadreBeanExport encadreBeanExport = new EncadreBeanExport();
            // on préfixe le titre pour le retrouver facilement dans la liste
            final EncadreBean encadreBean = new EncadreBean();
            UASOmHelper.copyProperties(encadreBean, currentEncadre);
            encadreBeanExport.setIdModule(idModule);
            currentEncadre.setIntitule(currentEncadre.getIntitule());
            encadreBeanExport.setBean(encadreBean);
            encadreBeanExport.setMedias(UASServicesHelper.getMedias(ContexteUtil.getContexteUniv(), currentEncadre, pathExport));
            exportEncadres.put(currentEncadre.getCode(), encadreBeanExport);
        }
        return exportEncadres;
    }
}
