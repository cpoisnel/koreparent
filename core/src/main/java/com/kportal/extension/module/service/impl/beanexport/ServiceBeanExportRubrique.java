package com.kportal.extension.module.service.impl.beanexport;

import java.util.List;

import com.kosmos.usinesite.utils.UASServicesHelper;
import com.kportal.extension.module.bean.BeanExportMap;
import com.kportal.extension.module.bean.RubriqueBeanExport;
import com.kportal.extension.module.service.ServiceBeanExport;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;

public class ServiceBeanExportRubrique implements ServiceBeanExport<RubriqueBeanExport> {

    private ServiceRubrique serviceRubrique;

    private ServiceInfosSite serviceInfosSite;

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void setServiceInfosSite(final ServiceInfosSite serviceInfosSite) {
        this.serviceInfosSite = serviceInfosSite;
    }

    @Override
    public BeanExportMap<RubriqueBeanExport> getBeansByRubrique(final List<String> codesRubrique, final String idModule, final String pathExport, final String... params) {
        final BeanExportMap<RubriqueBeanExport> rubriqueBeans = new BeanExportMap<>();
        List<RubriqueBean> result = serviceRubrique.getRubriqueInCodes(codesRubrique);
        for (RubriqueBean rubrique : result) {
            final RubriqueBeanExport rubriqueBeanExport = new RubriqueBeanExport();
            final InfosSite infos = serviceInfosSite.getSiteBySection(rubrique.getCode());
            if (infos != null) {
                rubriqueBeanExport.setRubriqueSite(true);
            }
            rubriqueBeanExport.setIdModule(idModule);
            rubriqueBeanExport.setBean(rubrique);
            rubriqueBeanExport.setMedias(UASServicesHelper.getMedias(rubrique, pathExport, Boolean.TRUE));
            rubriqueBeans.put(rubrique.getCode(), rubriqueBeanExport);
        }
        return rubriqueBeans;
    }
}
