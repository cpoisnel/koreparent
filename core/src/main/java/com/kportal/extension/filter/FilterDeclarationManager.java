package com.kportal.extension.filter;

import java.util.Collection;

import javax.servlet.Filter;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.kportal.core.context.ContextLoaderListener;
import com.kportal.extension.module.AbstractBeanManager;
import com.kportal.filter.ExtensionFilter;

public class FilterDeclarationManager extends AbstractBeanManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "filterDeclarationManager";

    @Override
    public void refresh() {
        final Collection<ExtensionFilter> filterDeclarations = ClassBeanManager.getInstance().getBeanOfType(ExtensionFilter.class);
        for (final Filter filterDeclaration : filterDeclarations) {
            ContextLoaderListener.ajouterFilter(filterDeclaration);
        }
    }
}
