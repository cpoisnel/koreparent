package com.kportal.extension;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Version {

    private static final Logger LOGGER = LoggerFactory.getLogger(Version.class);
    int numeroMajeur = 0;
    int numeroMineur = 0;
    int numeroMaintenance = 0;

    public Version(String version) {
        if (StringUtils.isEmpty(version)){
            return;
        }
        String[] numeros = version.split("\\.");
        String numero = StringUtils.EMPTY;
        for (int i= 0;i<numeros.length;i++) {
            try{
                numero = StringUtils.substringBefore(numeros[i], "-");
                int temp = Integer.parseInt(numero);
                switch (i) {
                    case 0:
                        numeroMajeur = temp;
                        break;
                    case 1:
                        numeroMineur = temp;
                        break;
                    case 2:
                        numeroMaintenance = temp;
                        break;
                    default:
                        break;
                }
            }catch(Exception e){
                LOGGER.error("Erreur de parsing du numéro " + numero + " de la version " + version);
            }
        }
    }

    public Version(int numeroMajeur, int numeroMineur, int numeroMaintenance) {
        this.numeroMajeur = numeroMajeur;
        this.numeroMineur = numeroMineur;
        this.numeroMaintenance = numeroMaintenance;
    }

    public boolean equalsMajeur(Version v) {
        return numeroMajeur == v.numeroMajeur;
    }

    public boolean equalsMineur(Version v) {
        return equalsMajeur(v) && numeroMineur == v.numeroMineur;
    }

    public boolean equalsMaintenance(Version v) {
        return equalsMineur(v) && numeroMaintenance == v.numeroMaintenance;
    }

    public boolean greaterMajeur(Version v) {
        return numeroMajeur > v.numeroMajeur;
    }

    public boolean greaterMineur(Version v) {
        return greaterMajeur(v) || (equalsMajeur(v) && numeroMineur > v.numeroMineur);
    }

    public boolean greaterMaintenance(Version v) {
        return greaterMineur(v) || (equalsMineur(v)  && numeroMaintenance > v.numeroMaintenance) ;
    }

    public boolean equals(Version v){
        return equalsMaintenance(v);
    }

    public boolean greater(Version v){
        return greaterMaintenance(v);
    }

    public boolean isValid() {
        return numeroMajeur > 0;
    }

    @Override
    public String toString() {
        return numeroMajeur +"." + numeroMineur + "." + numeroMaintenance;
    }

}
