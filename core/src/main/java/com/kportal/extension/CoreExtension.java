package com.kportal.extension;

public class CoreExtension extends DefaultExtensionImpl {

    @Override
    public void setType(int type) {
        super.setType(IExtension.TYPE_NON_PARAMETRABLE_AFFICHABLE);
    }

    @Override
    public int getType() {
        return IExtension.TYPE_NON_PARAMETRABLE_AFFICHABLE;
    }

    public String getApplicationName() {
        return getLibelleAffichable(libelle);
    }

    public String getApplicationVersion() {
        return getLibelleAffichable(version);
    }

    public String getApplicationDescription() {
        return getLibelleAffichable(description);
    }

    public String getApplicationAuteur() {
        return getLibelleAffichable(auteur);
    }

    public String getApplicationCopyright() {
        return getLibelleAffichable(copyright);
    }
}
