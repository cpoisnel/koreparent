package com.kportal.cache;

import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.extension.ExtensionManager;
import com.univ.objetspartages.cache.CacheLibelleManager;
import com.univ.objetspartages.cache.CacheRoleManager;
import com.univ.objetspartages.cache.CacheServiceManager;

/*
 * Cette classe permet de charger les caches applicatifs au démarrage de tomcat une fois l'application initialisée
 * Tous les caches ne sont pas présents ici car d'autre Bean Manager charge également les caches. cf PageAccueilRubriqueManager qui gere les urls de rubrique
 */
public class CacheLoaderManager extends Observable implements Observer {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheLoaderManager.class);

    private CacheRoleManager cacheRoleManager;

    private CacheLibelleManager cacheLibelleManager;

    private CacheServiceManager cacheServiceManager;

    private ExtensionManager extensionManager;

    public void init() {
        extensionManager.addObserver(this);
    }

    public void refresh() {
        LOGGER.debug("rechargement des services");
        cacheServiceManager.refresh();
        LOGGER.debug("rechargement des libelles");
        cacheLibelleManager.flushAll();
        cacheLibelleManager.getListeInfosLibelles();
        LOGGER.debug("rechargement des rôles");
        cacheRoleManager.flush();
        cacheRoleManager.getListeRoles();
    }

    public void setCacheRoleManager(final CacheRoleManager cacheRoleManager) {
        this.cacheRoleManager = cacheRoleManager;
    }

    public void setCacheLibelleManager(final CacheLibelleManager cacheLibelleManager) {
        this.cacheLibelleManager = cacheLibelleManager;
    }

    public void setCacheServiceManager(CacheServiceManager cacheServiceManager) {
        this.cacheServiceManager = cacheServiceManager;
    }

    @Override
    public void update(Observable o, Object arg) {
        refresh();
        setChanged();
        notifyObservers();
    }

    public void setExtensionManager(ExtensionManager extensionManager) {
        this.extensionManager = extensionManager;
    }
}
