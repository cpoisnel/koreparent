package com.kportal.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

public abstract class AbstractCacheManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCacheManager.class);

    @Async
    public void asyncRefresh() {
        refresh();
    }

    public void refresh() {
        try {
            if (!CacheUtil.updateObjectValue(getCacheName(), getObjectKey(), getObjectToCache())) {
                throw new Exception();
            }
        } catch (final Exception e) {
            LOGGER.error("Erreur refresh du cache " + getCacheName(), e);
        }
    }

    public Object call() {
        return CacheUtil.getObjectValue(getCacheName(), getObjectKey());
    }

    public void flush() {
        if (!CacheUtil.flush(getCacheName(), getObjectKey())) {
            LOGGER.error("Erreur flush du cache " + getCacheName());
        }
    }

    public abstract Object getObjectToCache() throws Exception;

    public abstract String getCacheName();

    public abstract Object getObjectKey();
}
