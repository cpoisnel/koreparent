package com.kportal.cache;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;
import net.sf.ehcache.config.FactoryConfiguration;
import net.sf.ehcache.distribution.jgroups.JGroupsCacheManagerPeerProviderFactory;
import net.sf.ehcache.distribution.jgroups.JGroupsCacheReplicatorFactory;

/**
 * Created by olivier.camon on 29/10/15.
 */
public class CacheManagerFactory implements FactoryBean<CacheManager>, InitializingBean, DisposableBean {

    /**
     * Nom du cache par défaut.
     */
    private static final String DEFAULT_CACHE_NAME = "defaultCache";

    /**
     * Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CacheManagerFactory.class);

    /**
     * Chaine de configuration du peerProvider.
     */
    private String peerProviderProperties;

    /**
     * Chaine de configuration du peerListener.
     */
    private String peerListenerProperties;

    /**
     * Configuration générale des listeners de la réplication.
     */
    private Map<String, String> listenerProperties;

    /**
     * Nom du cache.
     */
    private String name;

    /**
     * Emplacement du fichier de configuration ehcache.xml alternatif.
     */
    private Resource configurationLocation;

    private CacheManager manager;

    @Override
    public CacheManager getObject() throws Exception {
        return manager;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Configuration configuration = null;
        if (configurationLocation != null) {
            // Charge une configuration alternative si demandé
            configuration = loadExternalConfiguration();
        } else {
            // Charge le fichier ehcache.xml par défaut.
            configuration = ConfigurationFactory.parseConfiguration();
        }
        LOG.info("Activation cluster JGroups");
        processClusterConfiguration(configuration, JGroupsCacheManagerPeerProviderFactory.class.getName(), null, JGroupsCacheReplicatorFactory.class.getName());
        // Donner une nom au cache pour JMX.
        if (name != null) {
            configuration.setName(name);
        }
        // Crée le cache manager.
        manager = CacheManager.newInstance(configuration);
        // Dump la configuration.
        LOG.debug("Configuration du cache : " + manager.getActiveConfigurationText());
    }

    private void processClusterConfiguration(final Configuration configuration, final String peerProviderFactoryClassName, final String peerListenerFactoryClassName, final String replicatorFactoryClassName) {
        // Configure le peerProvider.
        FactoryConfiguration factory = new FactoryConfiguration();
        factory.setClass(peerProviderFactoryClassName);
        factory.setProperties(peerProviderProperties);
        configuration.addCacheManagerPeerProviderFactory(factory);
        // Configurer le peerListener.
        if (peerListenerFactoryClassName != null) {
            factory = new FactoryConfiguration();
            factory.setClass(peerListenerFactoryClassName);
            if (!StringUtils.isEmpty(peerListenerProperties)) {
                factory.setProperties(peerListenerProperties);
            }
            configuration.addCacheManagerPeerListenerFactory(factory);
        }
        // Ajoute un listener pour les caches par défaut.
        final CacheConfiguration defaultCacheConfiguration = configuration.getDefaultCacheConfiguration();
        CacheConfiguration.CacheEventListenerFactoryConfiguration cacheListenerFactory = new CacheConfiguration.CacheEventListenerFactoryConfiguration();
        cacheListenerFactory.setClass(replicatorFactoryClassName);
        final String defaultListenerProperties = computeListenerProperties(DEFAULT_CACHE_NAME);
        if (!StringUtils.isEmpty(defaultListenerProperties)) {
            cacheListenerFactory.setProperties(defaultListenerProperties);
        }
        defaultCacheConfiguration.addCacheEventListenerFactory(cacheListenerFactory);
        // Ajoute un listener pour les caches nommés dans le fichier de configuration.
        for (final CacheConfiguration cacheConfiguration : configuration.getCacheConfigurations().values()) {
            cacheListenerFactory = new CacheConfiguration.CacheEventListenerFactoryConfiguration();
            cacheListenerFactory.setClass(replicatorFactoryClassName);
            cacheListenerFactory.setProperties(computeListenerProperties(cacheConfiguration.getName()));
            cacheConfiguration.addCacheEventListenerFactory(cacheListenerFactory);
        }
    }

    /**
     * Détermine la configuration a utilisé pour un cache.
     */
    public String computeListenerProperties(final String cacheName) {
        String result = listenerProperties.get(cacheName);
        if (result == null) {
            result = listenerProperties.get(DEFAULT_CACHE_NAME);
        }
        LOG.debug("RMI Listener configuration for cache " + cacheName + " : " + result);
        return result;
    }

    /**
     * Charge une configuration externe.
     */
    private Configuration loadExternalConfiguration() throws IOException {
        Configuration configuration;
        LOG.info("Configuration par défaut : " + configurationLocation.getFilename());
        try (final InputStream in = configurationLocation.getInputStream();) {
            configuration = ConfigurationFactory.parseConfiguration(in);
        }
        return configuration;
    }

    @Override
    public void destroy() throws Exception {
        manager.shutdown();
    }

    @Override
    public Class<CacheManager> getObjectType() {
        return CacheManager.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public void setListenerProperties(final String listenerProperties) {
        this.listenerProperties = new HashMap<String, String>();
        if (listenerProperties != null) {
            if (listenerProperties.indexOf('{') == -1) {
                // Ancien format, tous les caches ont la même configuration.
                this.listenerProperties.put(DEFAULT_CACHE_NAME, listenerProperties);
            } else {
                // Nouveau format, les configurations par cache sont de la forme nom_cache:{string},nom_autre_cache:{autre string}
                int current = 0;
                int posSep1 = listenerProperties.indexOf("{", current);
                int posSep2 = listenerProperties.indexOf('}', posSep1);
                int posSep3 = listenerProperties.indexOf(',', posSep2);
                while (current != -1) {
                    final String nomCache = listenerProperties.substring(current, posSep1).trim();
                    final String configurationCache = listenerProperties.substring(posSep1 + 1, posSep2);
                    this.listenerProperties.put(nomCache, configurationCache);
                    if (posSep3 == -1) {
                        // Fin.
                        current = -1;
                    } else {
                        current = posSep3 + 1;
                        posSep1 = listenerProperties.indexOf("{", current);
                        posSep2 = listenerProperties.indexOf('}', posSep1);
                        posSep3 = listenerProperties.indexOf(',', posSep2);
                    }
                }
            }
        }
    }

    public void setPeerListenerProperties(final String peerListenerProperties) {
        this.peerListenerProperties = peerListenerProperties;
    }

    public void setPeerProviderProperties(final String peerProviderProperties) {
        this.peerProviderProperties = peerProviderProperties;
    }

    public void setConfigurationLocation(final Resource configurationLocation) {
        this.configurationLocation = configurationLocation;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
