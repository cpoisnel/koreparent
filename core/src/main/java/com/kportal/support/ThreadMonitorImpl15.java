package com.kportal.support;

import java.io.IOException;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

import javax.management.MBeanServerConnection;

import static java.lang.management.ManagementFactory.THREAD_MXBEAN_NAME;
import static java.lang.management.ManagementFactory.getThreadMXBean;
import static java.lang.management.ManagementFactory.newPlatformMXBeanProxy;

public class ThreadMonitorImpl15 implements ThreadMonitor {

    private static String INDENT = "    ";

    private StringBuilder stringBuilder;

    private ThreadMXBean tmbean;

    public ThreadMonitorImpl15(final MBeanServerConnection server) throws IOException {
        this.tmbean = newPlatformMXBeanProxy(server, THREAD_MXBEAN_NAME, ThreadMXBean.class);
        this.stringBuilder = new StringBuilder();
    }

    public ThreadMonitorImpl15() {
        this.tmbean = getThreadMXBean();
        this.stringBuilder = new StringBuilder();
    }

    /**
     * Prints the thread dump information to System.out.
     */
    private void threadDump() {
        final Long start = System.currentTimeMillis();
        try {
            if (tmbean == null) {
                tmbean = getThreadMXBean();
            }
            dumpThreadInfoWithLocks();
        } catch (final Exception e) {
            throw new RuntimeException(e);
        } finally {
            final long end = System.currentTimeMillis();
            final double time = (end / 1000.0) - (start / 1000.0);
            println("Full thread dump : " + time + " seconds");
        }
    }

    /**
     * Prints the thread dump information with locks info to
     */
    private void dumpThreadInfoWithLocks() {
        println("Full Java thread dump with locks info");
        final long[] tids = tmbean.getAllThreadIds();
        println("");
        println("Threads: count =" + tmbean.getThreadCount());
        final ThreadInfo[] tinfos = tmbean.getThreadInfo(tids, Integer.MAX_VALUE);
        for (final ThreadInfo ti : tinfos) {
            printThreadInfo(ti);
        }
        findDeadlock();
    }

    private void printThreadInfo(final ThreadInfo ti) {
        // print thread information
        printThread(ti);
        // print stack trace with locks
        final StackTraceElement[] stacktrace = ti.getStackTrace();
        for (final StackTraceElement ste : stacktrace) {
            println(INDENT + "at " + ste.toString());
        }
        println("");
    }

    private void printThread(final ThreadInfo ti) {
        final StringBuilder sb = new StringBuilder("\"" + ti.getThreadName() + "\"" + " Id=" + ti.getThreadId() + " in " + ti.getThreadState());
        if (ti.getLockName() != null) {
            sb.append(" on lock=").append(ti.getLockName());
        }
        if (ti.isSuspended()) {
            sb.append(" (suspended)");
        }
        if (ti.isInNative()) {
            sb.append(" (running in native)");
        }
        println(sb.toString());
        if (ti.getLockOwnerName() != null) {
            println(INDENT + " owned by " + ti.getLockOwnerName() + " Id=" + ti.getLockOwnerId());
        }
    }

    /**
     * Checks if any threads are deadlocked. If any, print the thread dump information.
     */
    public boolean findDeadlock() {
        final long[] tids = tmbean.findMonitorDeadlockedThreads();
        if (tids == null) {
            println("No Deadlock found.");
            return false;
        }
        println("Deadlock found :-");
        final ThreadInfo[] infos = tmbean.getThreadInfo(tids, Integer.MAX_VALUE);
        for (final ThreadInfo ti : infos) {
            // print thread information
            printThreadInfo(ti);
        }
        return true;
    }

    public void println(final String s) {
        if (stringBuilder != null) {
            stringBuilder.append(s).append(System.getProperty("line.separator"));
        } else {
            System.out.println(s);
        }
    }

    @Override
    public String getFullThreadDump() {
        if (stringBuilder.length() > 0) {
            stringBuilder = new StringBuilder();
        }
        threadDump();
        return stringBuilder.toString();
    }
}
