package com.univ.tree.utils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.univ.tree.processus.GestionJsTree;

public class JsTreeManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "jsTreeManager";

    /**
     * Récupère le bean d'id passé en paramètre pour exécuter les actions de génération d'un JsTree
     *
     * @param idBean
     *            le bean à récuperer
     * @return le bean correspondant ou null si non trouvé
     */
    public static GestionJsTree<?> getGestionJsTree(final String idBean) {
        return (GestionJsTree<?>) ApplicationContextManager.getEveryContextBean(idBean);
    }
}
