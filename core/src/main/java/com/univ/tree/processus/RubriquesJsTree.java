package com.univ.tree.processus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurAsyncException;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.tree.bean.JsTreeDataModel;
import com.univ.tree.bean.JsTreeModel;
import com.univ.tree.bean.JsTreeNodeModel;
import com.univ.tree.bean.JsTreePath;
import com.univ.tree.utils.JsTreeUtils;
import com.univ.utils.Chaine;
import com.univ.utils.EscapeString;
import com.univ.utils.SessionUtil;

public class RubriquesJsTree extends GestionJsTree<List<RubriqueBean>> {

    /** l'id Spring du bean. */
    public static final String ID_BEAN = "rubriquesJsTree";

    private static final Logger LOG = LoggerFactory.getLogger(RubriquesJsTree.class);

    protected ServiceRubrique serviceRubrique;

    private ServiceInfosSite serviceInfosSite;

    private ServiceGroupeDsi serviceGroupeDsi;

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void setServiceInfosSite(final ServiceInfosSite serviceInfosSite) {
        this.serviceInfosSite = serviceInfosSite;
    }

    public void setServiceGroupeDsi(final ServiceGroupeDsi serviceGroupeDsi) {
        this.serviceGroupeDsi = serviceGroupeDsi;
    }

    protected static boolean hasVisibleChildren(final String permissions, final AutorisationBean autorisations, final RubriqueBean rubrique, final Set<String> ids) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        for (final RubriqueBean currentChild : serviceRubrique.getRubriqueByCodeParent(rubrique.getCode())) {
            final boolean selectable = JsTreeUtils.isRubriqueSelectable(permissions, autorisations, currentChild, ids);
            final boolean visible = JsTreeUtils.isRubriqueVisible(permissions, autorisations, currentChild, selectable, ids);
            if (visible) {
                return true;
            }
        }
        return false;
    }

    public static String getPath(final String root, final String code, final String separator) {
        final String realCode = StringUtils.remove(code, "#AUTO#");
        final List<String> path = new ArrayList<>();
        if (StringUtils.isBlank(realCode)) {
            return root;
        }
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(code);
        while (rubrique != null && !StringUtils.defaultString(root).equals(rubrique.getCode())) {
            path.add(rubrique.getIntitule());
            rubrique = serviceRubrique.getRubriqueByCode(rubrique.getCodeRubriqueMere());
        }
        Collections.reverse(path);
        return StringUtils.join(path, separator);
    }

    @Override
    public JsTreeModel traiterDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        final HashMap<String, String> parameters;
        final JsTreeModel jsTree = new JsTreeModel();
        parameters = JsTreeUtils.getParameters(req);
        assertParametersConsistency(parameters);
        final String permissions = parameters.get("PERMISSION");
        final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(parameters.get("RACINE"));
        final int niveau = Integer.parseInt(parameters.get("NIVEAU"));
        final String[] selection = StringUtils.defaultString(parameters.get("SELECTED")).split(";");
        final JsTreeNodeModel node = buildRubriqueNode(autorisations, permissions, rubrique, niveau, null);
        if (node != null) {
            jsTree.getNodes().add(node);
        }
        if (selection.length > 0) {
            loadToPath(autorisations, permissions, jsTree, selection);
        }
        return jsTree;
    }

    private void loadToPath(final AutorisationBean autorisations, final String permissions, final JsTreeModel jsTree, final String[] selection) {
        final Set<String> loadedNodes = new HashSet<>();
        for (final String code : selection) {
            final JsTreePath path = getPathToCode(code);
            final Iterator<String> codeIt = path.getChildPath().iterator();
            final Set<String> nodes = new HashSet<>();
            String topCode = codeIt.next();
            nodes.addAll(path.getChildPath());
            if (!loadedNodes.isEmpty()) {
                while (loadedNodes.contains(topCode) && codeIt.hasNext()) {
                    topCode = codeIt.next();
                }
            }
            if (StringUtils.isNotBlank(topCode)) {
                final JsTreeNodeModel parentNode = JsTreeUtils.getNodeWithCode(jsTree.getNodes().get(0), topCode);
                final RubriqueBean parent = serviceRubrique.getRubriqueByCode(topCode);
                if (parentNode != null && CollectionUtils.isEmpty(parentNode.getChildren())) {
                    final JsTreeNodeModel nodeModel = buildRubriqueNode(autorisations, permissions, parent, -2, nodes);
                    if (nodeModel != null && nodeModel.getChildren() != null) {
                        parentNode.getChildren().addAll(nodeModel.getChildren());
                    }
                }
                loadedNodes.addAll(nodes);
            }
        }
    }

    private JsTreePath getPathToCode(final String code) {
        final JsTreePath path = new JsTreePath();
        path.addChild(code);
        if (StringUtils.isNotBlank(code)) {
            final RubriqueBean child = serviceRubrique.getRubriqueByCode(code);
            if (child != null) {
                RubriqueBean parent = serviceRubrique.getRubriqueByCode(child.getCodeRubriqueMere());
                while (parent != null) {
                    path.addChild(parent.getCode());
                    parent = serviceRubrique.getRubriqueByCode(parent.getCodeRubriqueMere());
                }
            }
        }
        return path;
    }

    protected JsTreeNodeModel buildRubriqueNode(final AutorisationBean autorisations, final String permissions, final RubriqueBean rubrique, int niveau, final Set<String> ids) {
        JsTreeNodeModel node = null;
        // Ajoute les éléments à l'arbre
        String codeRubrique = StringUtils.EMPTY;
        if (rubrique != null) {
            codeRubrique = rubrique.getCode();
        }
        final List<RubriqueBean> directChilds = serviceRubrique.getRubriqueByCodeParent(codeRubrique);
        final Iterator<RubriqueBean> listSousRubriquesIt = directChilds.iterator();
        final boolean selectable = JsTreeUtils.isRubriqueSelectable(permissions, autorisations, rubrique, ids);
        if (JsTreeUtils.isRubriqueVisible(permissions, autorisations, rubrique, selectable, ids)) {
            node = new JsTreeNodeModel();
            // on ajoute l'élément
            final JsTreeDataModel datas = new JsTreeDataModel();
            if (rubrique == null) {
                datas.setTitle(" ");
                node.getAttr().put("rel", "root");
                node.getAttr().put("class", "rubrique_root");
                if(!directChilds.isEmpty()) {
                    node.setState("open");
                }
                node.getAttr().put("id", "0");
            } else {
                final List<String> groups = new ArrayList<>();
                for (final String groupCode : Chaine.getHashSetAccolades(rubrique.getGroupesDsi())) {
                    final GroupeDsiBean group = serviceGroupeDsi.getByCode(groupCode);
                    if (StringUtils.isNotBlank(group.getCode())) {
                        groups.add(group.getLibelle());
                    }
                }
                if (!selectable) {
                    node.getAttr().put("rel", "not_selectable");
                } else {
                    final InfosSite infos = serviceInfosSite.getSiteBySection(rubrique.getCode());
                    if (infos != null) {
                        node.getAttr().put("rel", "rubrique_site");
                    }
                }
                final String restrained = StringUtils.join(groups, " ,");
                datas.setTitle(rubrique.getIntitule());
                node.getAttr().put("class", EscapeString.escapeAttributHtml("rubrique_" + rubrique.getTypeRubrique()) + (StringUtils.isNotBlank(rubrique.getGroupesDsi()) ? " restrained" : ""));
                node.getAttr().put("title", rubrique.getIntitule() + (StringUtils.isNotBlank(restrained) ? " (diffusion restreinte : " + restrained + " )" : ""));
                node.getMetadata().put("libelle", rubrique.getIntitule());
                node.getMetadata().put("sCode", rubrique.getCode());
                node.getMetadata().put("idRubrique", rubrique.getIdRubrique().toString());
                node.getMetadata().put("sCodeMere", rubrique.getCodeRubriqueMere());
                node.getMetadata().put("libelleMere", serviceRubrique.getAscendantsLabelOnly(rubrique));
                node.getMetadata().put("langue", rubrique.getLangue());
                if (!directChilds.isEmpty() && hasVisibleChildren(permissions, autorisations, rubrique, ids)) {
                    node.setState("closed");
                }
                node.getAttr().put("id", Long.toString(rubrique.getIdRubrique()));
            }
            node.getMetadata().put("numchildren", Integer.toString(directChilds.size()));
            node.setData(datas);
            if (niveau > 0 || niveau == -1) {
                while (listSousRubriquesIt.hasNext()) {
                    final RubriqueBean sousRubrique = listSousRubriquesIt.next();
                    if (niveau != -1) {
                        niveau--;
                    }
                    final JsTreeNodeModel child = buildRubriqueNode(autorisations, permissions, sousRubrique, niveau, ids);
                    if (child != null) {
                        node.getChildren().add(child);
                    }
                    if (niveau != -1) {
                        niveau++;
                    }
                }
            } else if (niveau == -2) {
                while (listSousRubriquesIt.hasNext()) {
                    final JsTreeNodeModel child;
                    final RubriqueBean sousRubrique = listSousRubriquesIt.next();
                    if (!ids.contains(sousRubrique.getCode())) {
                        child = buildRubriqueNode(autorisations, permissions, sousRubrique, 0, null);
                    } else {
                        child = buildRubriqueNode(autorisations, permissions, sousRubrique, niveau, ids);
                    }
                    if (child != null) {
                        node.getChildren().add(child);
                    }
                }
            }
        }
        return node;
    }

    @Override
    public JsTreeModel traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        final HashMap<String, String> parameters = JsTreeUtils.getParameters(req);
        assertParametersConsistency(parameters);
        parameters.put("NIVEAU", "-1");
        final String codeForRequest = StringUtils.defaultIfEmpty(req.getParameter("CODE_RECHERCHE"), req.getParameter("CODE_SAISI"));
        final List<RubriqueBean> rubriques = serviceRubrique.getRubriqueByCodeLanguageLabelCategory(codeForRequest, parameters.get("LANGUE"), parameters.get("INTITULE"), parameters.get("CATEGORIE"));
        final JsTreeModel jsTree = filterTree(autorisations, rubriques, parameters);
        openAllNodes(jsTree.getNodes());
        return jsTree;
    }

    @Override
    protected JsTreeModel filterTree(final AutorisationBean autorisations, final List<RubriqueBean> rubriques, final HashMap<String, String> parameters) {
        final JsTreeModel jsTree = new JsTreeModel();
        final String permissions = parameters.get("PERMISSION");
        final RubriqueBean rubriqueRacine = serviceRubrique.getRubriqueByCode(parameters.get("RACINE"));
        final int niveau = Integer.parseInt(parameters.get("NIVEAU"));
        final Set<String> ids = new HashSet<>();
        for (final RubriqueBean rubrique : rubriques) {
            if (!rubrique.getCode().equals(parameters.get("CODE"))) {
                ids.add(rubrique.getCode());
                RubriqueBean rubriqueMere = serviceRubrique.getRubriqueByCode(rubrique.getCodeRubriqueMere());
                while (rubriqueMere != null && StringUtils.isNotBlank(rubriqueMere.getCode())) {
                    ids.add(rubriqueMere.getCode());
                    rubriqueMere = serviceRubrique.getRubriqueByCode(rubriqueMere.getCodeRubriqueMere());
                }
            }
        }
        if (ids.isEmpty()) {
            return jsTree;
        }
        ids.add("00");
        jsTree.getNodes().add(buildRubriqueNode(autorisations, permissions, rubriqueRacine, niveau, ids));
        return jsTree;
    }

    @Override
    public void assertParametersConsistency(final Map<String, String> parameters) {
        if (parameters.get("RACINE") == null || parameters.get("RACINE").length() == 0) {
            parameters.put("RACINE", StringUtils.EMPTY);
        }
        if (parameters.get("CODE") == null || parameters.get("CODE").length() == 0) {
            parameters.put("CODE", parameters.get("RACINE"));
        }
        // Test si la rubrique de sélection est une sous-rubrique de la rubrique racine
        final RubriqueBean rubriqueCode = serviceRubrique.getRubriqueByCode(parameters.get("CODE"));
        final RubriqueBean rubriqueRacine = serviceRubrique.getRubriqueByCode(parameters.get("RACINE"));
        if (!parameters.get("RACINE").equals(StringUtils.EMPTY) && !parameters.get("RACINE").equals(parameters.get("CODE")) && serviceRubrique.isParentSection(rubriqueCode, rubriqueRacine)) {
            parameters.put("RACINE", StringUtils.EMPTY);
        }
    }

    @Override
    public String traiterAction(final AutorisationBean autorisations, final Map<String, String[]> parametresDeLaRequete) {
        String action = StringUtils.EMPTY;
        String message = StringUtils.EMPTY;
        if (parametresDeLaRequete.get("ACTION") != null) {
            action = parametresDeLaRequete.get("ACTION")[0];
        }
        if ("SUPPRIMER".equals(action)) {
            final String[] codes = parametresDeLaRequete.get("CODES_RUBRIQUES");
            if (codes != null) {
                for (final String currentCode : codes) {
                    String libelleRubrique = currentCode;
                    final RubriqueBean infos = serviceRubrique.getRubriqueByCode(currentCode);
                    if (infos != null) {
                        libelleRubrique = infos.getIntitule();
                    }
                    message = String.format(MessageHelper.getCoreMessage("BO_SERVICES_ARBRE_SUPPRESSION_OK"), String.format(MessageHelper.getCoreMessage("BO_SERVICES_ARBRE_RUBRIQUE"), libelleRubrique));
                    try {
                        traiterSuppression(autorisations, currentCode);
                    } catch (final ErreurApplicative e) {
                        LOG.error("la suppression de la rubrique a echouee", e);
                        throw new ErreurAsyncException(e.getMessage());
                    }
                }
            }
        } else if ("DEPLACER".equals(action)) {
            final String[] codes = parametresDeLaRequete.get("CODES_RUBRIQUES[]");
            final String[] codesMere = parametresDeLaRequete.get("CODES_MERE[]") != null ? parametresDeLaRequete.get("CODES_MERE[]") : new String[codes.length];
            final String[] ordres = parametresDeLaRequete.get("ORDRES[]") != null ? parametresDeLaRequete.get("ORDRES[]") : new String[codes.length];
            if (codes != null) {
                for (int i = 0; i < codes.length; i++) {
                    String libelleRubrique = codes[i];
                    final RubriqueBean infos = serviceRubrique.getRubriqueByCode(codes[i]);
                    if (infos != null) {
                        libelleRubrique = infos.getIntitule();
                    }
                    message = String.format(MessageHelper.getCoreMessage("BO_SERVICES_ARBRE_DEPLACEMENT_OK"), String.format(MessageHelper.getCoreMessage("BO_SERVICES_ARBRE_RUBRIQUE"), libelleRubrique));
                    traiterDeplacement(autorisations, codes[i], codesMere[i], ordres[i]);
                }
            }
        }
        return message;
    }

    private void traiterSuppression(final AutorisationBean autorisations, final String code) throws ErreurApplicative {
        if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "M"), new Perimetre("*", code, "*", "*", ""))) {
            throw new ErreurAsyncException(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
        }
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(code);
        if (rubriqueBean == null) {
            throw new ErreurAsyncException(MessageHelper.getCoreMessage("BO_RUBRIQUE_INEXISTANTE"));
        }
        serviceRubrique.deletebyId(rubriqueBean.getIdRubrique());
    }

    private void traiterDeplacement(final AutorisationBean autorisations, final String code, final String codeMere, final String ordre) {
        if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "M"), new Perimetre("*", code, "*", "*", ""))) {
            throw new ErreurAsyncException(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
        }
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(code);
        if (rubriqueBean == null) {
            throw new ErreurAsyncException(MessageHelper.getCoreMessage("BO_RUBRIQUE_INEXISTANTE"));
        }
        final String codeRubriqueMere = StringUtils.defaultString(codeMere, StringUtils.EMPTY);
        rubriqueBean.setCodeRubriqueMere(codeRubriqueMere);
        if (StringUtils.isNotBlank(ordre)) {
            int order = 0;
            for (final RubriqueBean currentRubrique : serviceRubrique.getRubriqueByCodeParent(codeRubriqueMere)) {
                if (!currentRubrique.getIdRubrique().equals(rubriqueBean.getIdRubrique())) {
                    if (order == Integer.parseInt(ordre)) {
                        order++;
                    }
                    final RubriqueBean orderedRubrique = serviceRubrique.getById(currentRubrique.getIdRubrique());
                    if (orderedRubrique != null) {
                        orderedRubrique.setOrdre(Integer.toString(order));
                        serviceRubrique.save(orderedRubrique);
                    }
                    order++;
                }
            }
            rubriqueBean.setOrdre(ordre);
            serviceRubrique.save(rubriqueBean);
        }
    }

    @Override
    public JsTreeModel traiterFiltreDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        final String query = StringUtils.defaultString(req.getParameter("QUERY"), StringUtils.EMPTY);
        final HashMap<String, String> parameters = JsTreeUtils.getParameters(req);
        assertParametersConsistency(parameters);
        parameters.put("NIVEAU", "-1");
        final List<RubriqueBean> results = serviceRubrique.getRubriqueByLabel(query);
        final JsTreeModel jsTree = filterTree(autorisations, results, parameters);
        openAllNodes(jsTree.getNodes());
        return jsTree;
    }

    @Override
    public String getSelectedIds(final String string) {
        if (StringUtils.isEmpty(string)) {
            return StringUtils.EMPTY;
        }
        final List<String> selectedIds = new ArrayList<>();
        final String[] selections = string.split(";");
        for (final String currentSelection : selections) {
            final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(currentSelection);
            if (rubrique != null && StringUtils.isNotBlank(rubrique.getCode())) {
                selectedIds.add(Long.toString(rubrique.getIdRubrique()));
            }
        }
        return StringUtils.join(selectedIds, ";");
    }
}
