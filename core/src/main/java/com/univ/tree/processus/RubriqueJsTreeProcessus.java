package com.univ.tree.processus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.tree.bean.JsTreeDataModel;
import com.univ.tree.bean.JsTreeNodeModel;
import com.univ.tree.utils.JsTreeUtils;
import com.univ.utils.Chaine;
import com.univ.utils.EscapeString;

/**
 * Arbre spécifique pour le processus de saisie de rubrique
 *
 * @author olivier.camon
 *
 */
public class RubriqueJsTreeProcessus extends RubriquesJsTree {

    public static final String ID_BEAN = "rubriqueJsTreeProcessus";

    private static final Logger LOG = LoggerFactory.getLogger(RubriqueJsTreeProcessus.class);

    private static final String PERMISSION_MODIFICATION = "/TECH/rub/M";

    private static final String PERMISSION_SUPPRESSION = "/TECH/rub/S";

    private ServiceInfosSite serviceInfosSite;

    private ServiceGroupeDsi serviceGroupeDsi;

    public void setServiceInfosSite(final ServiceInfosSite serviceInfosSite) {
        this.serviceInfosSite = serviceInfosSite;
    }

    public void setServiceGroupeDsi(final ServiceGroupeDsi serviceGroupeDsi) {
        this.serviceGroupeDsi = serviceGroupeDsi;
    }

    private static boolean isRubriqueVisible(final AutorisationBean autorisations, final RubriqueBean rubrique, final Set<String> ids, final boolean selectable) {
        return JsTreeUtils.isRubriqueVisible(PERMISSION_MODIFICATION, autorisations, rubrique, selectable, ids) || JsTreeUtils.isRubriqueVisible(PERMISSION_SUPPRESSION, autorisations, rubrique, selectable, ids);
    }

    private static boolean isRubriqueSelectable(final AutorisationBean autorisations, final RubriqueBean rubrique, final Set<String> ids) {
        return JsTreeUtils.isRubriqueSelectable(PERMISSION_MODIFICATION, autorisations, rubrique, ids) || JsTreeUtils.isRubriqueSelectable(PERMISSION_SUPPRESSION, autorisations, rubrique, ids);
    }

    @Override
    protected JsTreeNodeModel buildRubriqueNode(final AutorisationBean autorisations, final String permissions, final RubriqueBean rubrique, int niveau, final Set<String> ids) {
        JsTreeNodeModel node = null;
        // Ajoute les éléments à l'arbre
        String codeRubrique = StringUtils.EMPTY;
        if (rubrique != null) {
            codeRubrique = rubrique.getCode();
        }
        final List<RubriqueBean> sousRubroques = serviceRubrique.getRubriqueByCodeParent(codeRubrique);
        final Iterator<RubriqueBean> listSousRubriquesIt = sousRubroques.iterator();
        final boolean selectable = isRubriqueSelectable(autorisations, rubrique, ids);
        if (isRubriqueVisible(autorisations, rubrique, ids, selectable)) {
            node = new JsTreeNodeModel();
            // on ajoute l'élément
            final JsTreeDataModel datas = new JsTreeDataModel();
            if (rubrique == null) {
                datas.setTitle(" ");
                node.getAttr().put("rel", "root");
                node.getAttr().put("class", "rubrique_root");
                node.setState("open");
                node.getAttr().put("id", "0");
            } else {
                final List<String> groups = new ArrayList<>();
                for (final String groupCode : Chaine.getHashSetAccolades(rubrique.getGroupesDsi())) {
                    final GroupeDsiBean group = serviceGroupeDsi.getByCode(groupCode);
                    if (group != null && StringUtils.isNotBlank(group.getCode())) {
                        groups.add(group.getLibelle());
                    }
                }
                if (!selectable) {
                    node.getAttr().put("rel", "not_selectable");
                } else {
                    final InfosSite infos = serviceInfosSite.getSiteBySection(rubrique.getCode());
                    if (infos != null) {
                        node.getAttr().put("rel", "rubrique_site");
                    }
                }
                final String restrained = StringUtils.join(groups, " ,");
                datas.setTitle(rubrique.getIntitule());
                node.getAttr().put("class", EscapeString.escapeAttributHtml("rubrique_" + rubrique.getTypeRubrique()) + (StringUtils.isNotBlank(rubrique.getGroupesDsi()) ? " restrained" : ""));
                node.getAttr().put("title", rubrique.getIntitule() + (StringUtils.isNotBlank(restrained) ? " (diffusion restreinte : " + restrained + " )" : ""));
                node.getMetadata().put("libelle", rubrique.getIntitule());
                node.getMetadata().put("sCode", rubrique.getCode());
                node.getMetadata().put("idRubrique", rubrique.getIdRubrique().toString());
                node.getMetadata().put("sCodeMere", rubrique.getCodeRubriqueMere());
                node.getMetadata().put("libelleMere", serviceRubrique.getAscendantsLabelOnly(rubrique));
                node.getMetadata().put("langue", rubrique.getLangue());
                if (!sousRubroques.isEmpty() && hasVisibleChildren(permissions, autorisations, rubrique, ids)) {
                    node.setState("closed");
                }
                node.getAttr().put("id", Long.toString(rubrique.getIdRubrique()));
            }
            node.getMetadata().put("numchildren", Integer.toString(sousRubroques.size()));
            node.setData(datas);
            if (niveau > 0 || niveau == -1) {
                while (listSousRubriquesIt.hasNext()) {
                    final RubriqueBean sousRubrique = listSousRubriquesIt.next();
                    if (niveau != -1) {
                        niveau--;
                    }
                    final JsTreeNodeModel child = buildRubriqueNode(autorisations, permissions, sousRubrique, niveau, ids);
                    if (child != null) {
                        node.getChildren().add(child);
                    }
                    if (niveau != -1) {
                        niveau++;
                    }
                }
            } else if (niveau == -2) {
                while (listSousRubriquesIt.hasNext()) {
                    final JsTreeNodeModel child;
                    final RubriqueBean sousRubrique = listSousRubriquesIt.next();
                    if (!ids.contains(sousRubrique.getCode())) {
                        child = buildRubriqueNode(autorisations, permissions, sousRubrique, 0, null);
                    } else {
                        child = buildRubriqueNode(autorisations, permissions, sousRubrique, niveau, ids);
                    }
                    if (child != null) {
                        node.getChildren().add(child);
                    }
                }
            }
        }
        return node;
    }
}
