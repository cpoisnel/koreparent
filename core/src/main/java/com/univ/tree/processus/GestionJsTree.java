package com.univ.tree.processus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.univ.objetspartages.om.AutorisationBean;
import com.univ.tree.bean.JsTreeModel;
import com.univ.tree.bean.JsTreeNodeModel;

/**
 * Classe abstraite à implementer pour réaliser les traitements d'un JsTree
 *
 * @author fabien.leconte
 *
 */
public abstract class GestionJsTree<T> {

    /**
     * Génère une arborescence JsTree en fonction des paramètres passés dans la requête <code>req</code>
     *
     * @param req
     * @return
     */
    public abstract JsTreeModel traiterDepuisRequete(HttpServletRequest req);

    /**
     * Permet de traiter les critères de filtre reçu depuis un JsTree
     *
     * @param req
     * @return
     */
    public abstract JsTreeModel traiterFiltreDepuisRequete(HttpServletRequest req);

    /**
     * Permet de traiter les critères de recherche reçu depuis un JsTree
     *
     * @param req
     * @return
     */
    public abstract JsTreeModel traiterRechercheDepuisRequete(HttpServletRequest req);

    /**
     * Assure la consistance des paramètres passés dans la requête
     *
     * @param parameters
     * @return
     */
    public abstract void assertParametersConsistency(final Map<String, String> parameters) throws Exception;

    /**
     * Permet de traiter des actions d'un JsTree (suppression de masse, archivage...) Par défaut il n'y a pas d'action de masse.
     *
     * @param parametresDeLaRequete
     */
    public String traiterAction(final AutorisationBean autorisations, final Map<String, String[]> parametresDeLaRequete) {
        return "";
    }

    /**
     * Permet d'ouvrir les branches de l'arbre avant même qu'il soit affiché
     *
     * @param nodes
     *            : liste noeuds à partir desquels les branches doivent être ouverte
     */
    protected void openAllNodes(final List<JsTreeNodeModel> nodes) {
        for (final JsTreeNodeModel currentNode : nodes) {
            if (currentNode != null && !currentNode.getChildren().isEmpty()) {
                currentNode.setState("open");
                openAllNodes(currentNode.getChildren());
            }
        }
    }

    protected abstract JsTreeModel filterTree(AutorisationBean autorisations, T object, HashMap<String, String> parameters);

    public abstract String getSelectedIds(String string);
}
