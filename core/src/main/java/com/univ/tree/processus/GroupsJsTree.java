package com.univ.tree.processus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurAsyncException;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.tree.bean.JsTreeDataModel;
import com.univ.tree.bean.JsTreeModel;
import com.univ.tree.bean.JsTreeNodeModel;
import com.univ.tree.bean.JsTreePath;
import com.univ.tree.utils.JsTreeUtils;
import com.univ.utils.SessionUtil;

public class GroupsJsTree extends GestionJsTree<List<GroupeDsiBean>> {

    /** l'id Spring du bean. */
    public static final String ID_BEAN = "groupsJsTree";

    private static final Logger LOG = LoggerFactory.getLogger(GroupsJsTree.class);

    private ServiceGroupeDsi serviceGroupeDsi;

    public void setServiceGroupeDsi(final ServiceGroupeDsi serviceGroupeDsi) {
        this.serviceGroupeDsi = serviceGroupeDsi;
    }

    private static JsTreeNodeModel buildGroupeNode(final AutorisationBean autorisations, final String permissions, final GroupeDsiBean groupe, int niveau, final Set<String> ids, final boolean locked_dyn) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        JsTreeNodeModel node = null;
        // Ajoute les éléments à l'arbre
        final List<GroupeDsiBean> subGroups = serviceGroupeDsi.getByParentGroup(groupe.getCode());
        final boolean selectable = JsTreeUtils.isGroupeSelectable(permissions, autorisations, groupe, ids);
        final boolean isDynamicGroup = StringUtils.isNotBlank(groupe.getRequeteGroupe());
        if (JsTreeUtils.isGroupeVisible(permissions, autorisations, groupe, selectable, ids)) {
            node = new JsTreeNodeModel();
            // on ajoute l'élément
            final JsTreeDataModel datas = new JsTreeDataModel();
            if (groupe.getCode().equals(JsTreeUtils.CODE_ROOT)) {
                datas.setTitle(" ");
                node.getAttr().put("rel", "root");
                node.getAttr().put("class", "groupe_root");
                if (CollectionUtils.isNotEmpty(subGroups)) {
                    node.setState("open");
                }
            } else {
                if (!selectable) {
                    node.getAttr().put("rel", "not_selectable");
                }
                if (isDynamicGroup && locked_dyn) {
                    node.getAttr().put("rel", "locked");
                }
                datas.setTitle(groupe.getLibelle());
                node.getAttr().put("class", "groupe_" + groupe.getType());
                node.getAttr().put("title", groupe.getLibelle());
                node.getMetadata().put("libelle", groupe.getLibelle());
                node.getMetadata().put("sCode", groupe.getCode());
                node.getMetadata().put("idGroupeDsi", groupe.getId().toString());
                node.getMetadata().put("type", LabelUtils.getLibelle("11", groupe.getType(), LangueUtil.getDefaultLocale()));
                node.getMetadata().put("structure", serviceStructure.getDisplayableLabel(groupe.getCodeStructure(), "0"));
                if (CollectionUtils.isNotEmpty(subGroups) && hasVisibleChildren(permissions, autorisations, groupe, ids)) {
                    node.setState("closed");
                }
            }
            node.getMetadata().put("numchildren", Integer.toString(subGroups.size()));
            node.getAttr().put("id", Long.toString(groupe.getId()));
            node.setData(datas);
            if (niveau > 0 || niveau == -1) {
                for (final GroupeDsiBean currentSubGroup : subGroups) {
                    if (niveau != -1) {
                        niveau--;
                    }
                    final JsTreeNodeModel child = buildGroupeNode(autorisations, permissions, currentSubGroup, niveau, ids, locked_dyn);
                    if (child != null) {
                        node.getChildren().add(child);
                    }
                    if (niveau != -1) {
                        niveau++;
                    }
                }
            } else if (niveau == -2) {
                for (final GroupeDsiBean currentSubGroup : subGroups) {
                    final JsTreeNodeModel child;
                    if (!ids.contains(currentSubGroup.getCode())) {
                        child = buildGroupeNode(autorisations, permissions, currentSubGroup, 0, null, locked_dyn);
                    } else {
                        child = buildGroupeNode(autorisations, permissions, currentSubGroup, niveau, ids, locked_dyn);
                    }
                    if (child != null) {
                        node.getChildren().add(child);
                    }
                }
            }
        }
        return node;
    }

    public static String getPath(String root, final String code, final String separator) {
        final List<String> path = new ArrayList<>();
        if (StringUtils.isBlank(root)) {
            root = JsTreeUtils.CODE_ROOT;
        }
        if (StringUtils.isBlank(code)) {
            return root;
        }
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        GroupeDsiBean group = serviceGroupeDsi.getByCode(code);
        while (group != null && !root.equals(group.getCode())) {
            path.add(group.getLibelle());
            group = serviceGroupeDsi.getByCode(group.getCodeGroupePere());
        }
        Collections.reverse(path);
        return StringUtils.join(path, separator);
    }

    private static boolean hasVisibleChildren(final String permissions, final AutorisationBean autorisations, final GroupeDsiBean groupe, final Set<String> ids) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        for (final GroupeDsiBean currentChild : serviceGroupeDsi.getByParentGroup(groupe.getCode())) {
            final boolean selectable = JsTreeUtils.isGroupeSelectable(permissions, autorisations, currentChild, ids);
            final boolean visible = JsTreeUtils.isGroupeVisible(permissions, autorisations, currentChild, selectable, ids);
            if (visible) {
                return true;
            }
        }
        return false;
    }

    @Override
    public JsTreeModel traiterDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        final JsTreeModel jsTree = new JsTreeModel();
        final Map<String, String> parameters = JsTreeUtils.getParameters(req);
        assertParametersConsistency(parameters);
        final String permissions = parameters.get("PERMISSION");
        final GroupeDsiBean group = serviceGroupeDsi.getByCode(parameters.get("RACINE"));
        if(group != null) {
            final String[] selection = StringUtils.defaultString(parameters.get("SELECTED")).split(";");
            final boolean locked_dyn = Boolean.parseBoolean(StringUtils.defaultString(parameters.get("LOCK_DYN"), "false"));
            final int niveau = Integer.parseInt(parameters.get("NIVEAU"));
            jsTree.getNodes().add(buildGroupeNode(autorisations, permissions, group, niveau, null, locked_dyn));
            if (selection.length > 0) {
                loadToPath(autorisations, permissions, jsTree, selection, locked_dyn);
            }
        }
        return jsTree;
    }

    private void loadToPath(final AutorisationBean autorisations, final String permissions, final JsTreeModel jsTree, final String[] selection, final boolean locked_dyn) {
        final List<String> loadedNodes = new ArrayList<>();
        for (final String code : selection) {
            final JsTreePath path = getPathToCode(code);
            final Iterator<String> codeIt = path.getChildPath().iterator();
            final Set<String> nodes = new HashSet<>();
            if(codeIt.hasNext()) {
                String topCode = codeIt.next();
                nodes.addAll(path.getChildPath());
                if (!loadedNodes.isEmpty()) {
                    while (loadedNodes.contains(topCode) && codeIt.hasNext()) {
                        topCode = codeIt.next();
                    }
                }
                if (StringUtils.isNotBlank(topCode)) {
                    final JsTreeNodeModel parentNode = JsTreeUtils.getNodeWithCode(jsTree.getNodes().get(0), topCode);
                    final GroupeDsiBean parent = serviceGroupeDsi.getByCode(topCode);
                    if (parentNode != null) {
                        parentNode.getChildren().addAll(buildGroupeNode(autorisations, permissions, parent, -2, nodes, locked_dyn).getChildren());
                        loadedNodes.addAll(nodes);
                    }
                }
            }
        }
    }

    private JsTreePath getPathToCode(final String code) {
        final JsTreePath path = new JsTreePath();
        final GroupeDsiBean child = serviceGroupeDsi.getByCode(code);
        if(child != null) {
            GroupeDsiBean parent = serviceGroupeDsi.getByCode(child.getCodeGroupePere());
            path.addChild(code);
            while (parent != null && !"00".equals(parent.getCode())) {
                path.addChild(parent.getCode());
                parent = serviceGroupeDsi.getByCode(parent.getCodeGroupePere());
            }
        }
        return path;
    }

    @Override
    public void assertParametersConsistency(final Map<String, String> parameters) {
        if (parameters.get("CODE") == null || parameters.get("CODE").length() == 0) {
            parameters.put("CODE", JsTreeUtils.CODE_ROOT);
        }
        if (parameters.get("RACINE") == null || parameters.get("RACINE").length() == 0) {
            parameters.put("RACINE", JsTreeUtils.CODE_ROOT);
        }
    }

    @Override
    public JsTreeModel traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        final HashMap<String, String> parameters;
        final JsTreeModel jsTree;
        parameters = JsTreeUtils.getParameters(req);
        assertParametersConsistency(parameters);
        parameters.put("NIVEAU", "-1");
        final List<GroupeDsiBean> results = serviceGroupeDsi.getByCodeTypeLabelStructureAndCache(req.getParameter("CODE_RECHERCHE"), req.getParameter("TYPE"), req.getParameter("LIBELLE"), req.getParameter("CODE_STRUCTURE"), null);
        jsTree = filterTree(autorisations, results, parameters);
        openAllNodes(jsTree.getNodes());
        return jsTree;
    }

    @Override
    public JsTreeModel traiterFiltreDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        final HashMap<String, String> parameters;
        JsTreeModel jsTree = new JsTreeModel();
        final String query = StringUtils.defaultString(req.getParameter("QUERY"), StringUtils.EMPTY);
        parameters = JsTreeUtils.getParameters(req);
        assertParametersConsistency(parameters);
        parameters.put("NIVEAU", "-1");
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final List<GroupeDsiBean> results = serviceGroupeDsi.getByLabel(query);
        jsTree = filterTree(autorisations, results, parameters);
        openAllNodes(jsTree.getNodes());
        return jsTree;
    }

    @Override
    public String traiterAction(final AutorisationBean autorisations, final Map<String, String[]> parametresDeLaRequete) {
        String action = StringUtils.EMPTY;
        String message = StringUtils.EMPTY;
        if (parametresDeLaRequete.get("ACTION") != null) {
            action = parametresDeLaRequete.get("ACTION")[0];
        }
        if ("SUPPRIMER".equals(action)) {
            final String[] ids = parametresDeLaRequete.get("IDS_GROUPE[]");
            if (ids != null) {
                final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
                for (final String currentId : ids) {
                    final GroupeDsiBean groupedsi = serviceGroupeDsi.getById(Long.valueOf(currentId));
                    if (groupedsi != null) {
                        final String libelleGroupe = groupedsi.getLibelle();
                        message = String.format(MessageHelper.getCoreMessage("BO_SERVICES_ARBRE_SUPPRESSION_OK"), String.format(MessageHelper.getCoreMessage("BO_SERVICES_ARBRE_GROUP"), libelleGroupe));
                        serviceGroupeDsi.delete(groupedsi.getIdGroupedsi());
                        serviceGroupeUtilisateur.deleteByGroup(groupedsi.getCode());
                    }
                }
            }
        }
        return message;
    }

    @Override
    protected JsTreeModel filterTree(final AutorisationBean autorisations, final List<GroupeDsiBean> allGroup, final HashMap<String, String> parameters) {
        final JsTreeModel jsTree = new JsTreeModel();
        final String permissions = parameters.get("PERMISSION");
        final GroupeDsiBean rootGroup = serviceGroupeDsi.getByCode(parameters.get("RACINE"));
        final int niveau = Integer.parseInt(parameters.get("NIVEAU"));
        final boolean locked_dyn = Boolean.parseBoolean(StringUtils.defaultString(parameters.get("LOCK_DYN"), "false"));
        final Set<String> ids = new HashSet<>();
        for (final GroupeDsiBean groupe : allGroup) {
            if (!groupe.getCode().equals(parameters.get("CODE"))) {
                ids.add(groupe.getCode());
                GroupeDsiBean parentGroup = serviceGroupeDsi.getByCode(groupe.getCodeGroupePere());
                while (parentGroup != null) {
                    ids.add(parentGroup.getCode());
                    parentGroup = serviceGroupeDsi.getByCode(parentGroup.getCodeGroupePere());
                }
            }
        }
        if (ids.isEmpty()) {
            return jsTree;
        }
        ids.add("00");
        jsTree.getNodes().add(buildGroupeNode(autorisations, permissions, rootGroup, niveau, ids, locked_dyn));
        return jsTree;
    }

    @Override
    public String getSelectedIds(final String string) {
        if (StringUtils.isEmpty(string)) {
            return StringUtils.EMPTY;
        }
        final List<String> selectedIds = new ArrayList<>();
        final String[] selections = string.split(";");
        for (final String currentSelection : selections) {
            final GroupeDsiBean group = serviceGroupeDsi.getByCode(currentSelection);
            if (group != null) {
                selectedIds.add(Long.toString(group.getId()));
            }
        }
        return StringUtils.join(selectedIds, ";");
    }
}
