package com.univ.tree.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class JsTreeAttributesModel {

    private String id;

    private String rel;

    private String mdata;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getMetadata() {
        return mdata;
    }

    public void setMetadata(String mdata) {
        this.mdata = mdata;
    }
}
