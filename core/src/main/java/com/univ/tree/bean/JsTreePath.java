package com.univ.tree.bean;

import java.util.ArrayDeque;
import java.util.Deque;

public class JsTreePath {

    private Deque<String> childPath;

    public JsTreePath() {
        childPath = new ArrayDeque<>();
    }

    public void addChild(String child) {
        this.childPath.addFirst(child);
    }

    public void removeChild(String child) {
        this.childPath.remove(child);
    }

    public Deque<String> getChildPath() {
        return childPath;
    }

    public String getStartPoint() {
        return childPath.getFirst();
    }
}
