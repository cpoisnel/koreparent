package com.univ.tree.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class JsTreeNodeModel {

    private JsTreeDataModel data;

    private String state;

    private HashMap<String, String> attr;

    private HashMap<String, String> metadata;

    private List<JsTreeNodeModel> children;

    public JsTreeNodeModel() {
        children = new ArrayList<>();
        metadata = new HashMap<>();
        attr = new HashMap<>();
    }

    public JsTreeDataModel getData() {
        return data;
    }

    public void setData(JsTreeDataModel data) {
        this.data = data;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<JsTreeNodeModel> getChildren() {
        return children;
    }

    public void setChildren(List<JsTreeNodeModel> children) {
        this.children = children;
    }

    public HashMap<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(HashMap<String, String> metadata) {
        this.metadata = metadata;
    }

    public HashMap<String, String> getAttr() {
        return attr;
    }

    public void setAttr(HashMap<String, String> attr) {
        this.attr = attr;
    }
}
