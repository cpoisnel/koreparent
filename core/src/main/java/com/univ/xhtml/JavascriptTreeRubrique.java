package com.univ.xhtml;

import java.util.Iterator;

import com.kportal.core.autorisation.util.PermissionUtil;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.InfosRubriques;
// TODO: Auto-generated Javadoc

/**
 * Classe représentant un arbre Javascript de rubriques.
 *
 * @author FBI
 */
public class JavascriptTreeRubrique {

    /** Arbre Javascript. */
    private JavascriptTree tree = null;

    /** Liste des autorisations de l'utilisateur. */
    private AutorisationBean autorisations = null;

    /** Permission demandée (ex : FICHE/0016/M). */
    private String permissions = null;

    /**
     * Constructeur.
     *
     * @param autorisations
     *            Liste des permissions de l'utilisateur
     * @param permissions
     *            Permission demandée (ex : FICHE/0016/M)
     */
    public JavascriptTreeRubrique(final AutorisationBean autorisations, final String permissions) {
        this.tree = new JavascriptTree();
        this.autorisations = autorisations;
        this.permissions = permissions;
    }

    /**
     * Chargement de l'arbre des rubriques pour la rubrique demandée.
     *
     * @param rubrique
     *            Rubrique à charger
     * @param rubriqueSelectionnee
     *            Rubrique sélectionnée
     *
     * @throws Exception
     *             the exception
     */
    public void load(final InfosRubriques rubrique, final InfosRubriques rubriqueSelectionnee) throws Exception {
        // Ajoute les éléments à l'arbre
        final Iterator<InfosRubriques> listSousRubriquesIt = rubrique.getListeSousRubriques().iterator();
        InfosRubriques sousRubrique = null;
        boolean selectable = true;
        while (listSousRubriquesIt.hasNext()) {
            sousRubrique = listSousRubriquesIt.next();
            selectable = isRubriqueSelectable(sousRubrique); // élément sélectionnable ou non
            if (isRubriqueVisible(sousRubrique, selectable)) {
                // on ajoute l'élément
                tree.addItem(new JavascriptTreeItem(sousRubrique.getCodeRubriqueMere(), sousRubrique.getCode(), sousRubrique.getIntitule(), sousRubrique.contains(rubriqueSelectionnee), // élément ouvert ou non
                    selectable));
                if (!sousRubrique.getListeSousRubriques().isEmpty()) {
                    load(sousRubrique, rubriqueSelectionnee);
                }
            }
        }
    }

    /**
     * Affiche l'arbre Javascript.
     *
     * @return Le code Javascript d'affichage de l'arbre
     */
    public String print() {
        return tree.print();
    }

    /**
     * Teste si la rubrique est sélectionnable.
     *
     * @param rubrique
     *            La rubrique à tester
     *
     * @return true si la rubrique est sélectionnable
     *
     * @throws Exception
     *             the exception
     */
    private boolean isRubriqueSelectable(final InfosRubriques rubrique) throws Exception {
        // Controle le périmètre de l'utilisateur sur la rubrique
        boolean selectableItem = true;
        if (permissions.length() > 0) {
            selectableItem = PermissionUtil.controlerPermissionRubrique(autorisations, permissions, rubrique.getCode());
        }
        return selectableItem;
    }

    /**
     * Teste si la rubrique est visible dans l'arbre (rubrique sélectionnable ou ayant un fils visible.
     *
     * @param rubrique
     *            La rubrique à tester
     * @param selectable
     *            true si la rubrique est sélectionnable
     *
     * @return true si la rubrique est visible dans l'arbre
     *
     * @throws Exception
     *             the exception
     */
    private boolean isRubriqueVisible(final InfosRubriques rubrique, final boolean selectable) throws Exception {
        boolean visible = selectable;
        if (!selectable) // rubrique non sélectionnable
        {
            // on vérifie ses rubriques filles
            final Iterator<InfosRubriques> listSousRubriquesIt = rubrique.getListeSousRubriques().iterator();
            InfosRubriques sousRubrique = null;
            while (listSousRubriquesIt.hasNext() && !visible) {
                sousRubrique = listSousRubriquesIt.next();
                visible = isRubriqueVisible(sousRubrique, isRubriqueSelectable(sousRubrique));
            }
        }
        return visible;
    }
}
