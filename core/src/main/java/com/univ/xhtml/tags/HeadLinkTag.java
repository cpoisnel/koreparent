package com.univ.xhtml.tags;

import org.htmlparser.tags.CompositeTag;

/**
 * A link tag.
 */
public class HeadLinkTag extends CompositeTag {

    /**
     *
     */
    private static final long serialVersionUID = -84511259414298318L;

    /**
     * The set of names handled by this tag.
     */
    private static final String[] mIds = new String[] {"LINK"};

    /**
     * The set of end tag names that indicate the end of this tag.
     */
    private static final String[] mEndTagEnders = new String[] {"HEAD",};

    /**
     * Create a new div tag.
     */
    public HeadLinkTag() {}

    /**
     * Return the set of names handled by this tag.
     *
     * @return The names to be matched that create tags of this type.
     */
    @Override
    public String[] getIds() {
        return mIds;
    }

    /**
     * Return the set of end tag names that cause this tag to finish.
     *
     * @return The names of following end tags that stop further scanning.
     */
    @Override
    public String[] getEndTagEnders() {
        return mEndTagEnders;
    }
}