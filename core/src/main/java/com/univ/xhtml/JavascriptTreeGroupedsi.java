package com.univ.xhtml;

import org.apache.commons.collections.CollectionUtils;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;

/**
 * Classe représentant un arbre Javascript de groupes DSI.
 *
 * @author FBI
 * @deprecated Utiliser le service d'arborescence disponible à travers la jsp "tree.jsp".
 */
@Deprecated
public class JavascriptTreeGroupedsi {

    private final ServiceGroupeDsi serviceGroupeDsi;

    /** Arbre Javascript. */
    private JavascriptTree tree = null;

    /** Liste des autorisations de l'utilisateur. */
    private AutorisationBean autorisations = null;

    /** Permission demandée (ex : FICHE/0016/M). */
    private String permissions = null;

    /**
     * Constructeur.
     *
     * @param autorisations
     *            Liste des permissions de l'utilisateur
     * @param permissions
     *            Permission demandée (ex : FICHE/0016/M)
     * @deprecated Cf. deprecated de la classe
     */
    @Deprecated
    public JavascriptTreeGroupedsi(final AutorisationBean autorisations, final String permissions) {
        this.tree = new JavascriptTree();
        this.autorisations = autorisations;
        this.permissions = permissions;
        serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
    }

    /**
     * Chargement de l'arbre des groupes DSI pour le groupe demandé.
     *
     * @param groupe
     *            Groupe à charger
     * @param groupeSelectionne
     *            Rubrique sélectionnée
     * @deprecated Cf. deprecated de la classe
     */
    @Deprecated
    public void load(final GroupeDsiBean groupe, final GroupeDsiBean groupeSelectionne) {
        // Ajoute les éléments à l'arbre
        for(GroupeDsiBean currentSubGroup : serviceGroupeDsi.getByParentGroup(groupe.getCode())) {
            final boolean selectable = isGroupeSelectable(currentSubGroup);
            if (isGroupeVisible(currentSubGroup, selectable)) {
                // on ajoute l'élément
                tree.addItem(new JavascriptTreeItem(currentSubGroup.getCodeGroupePere(), currentSubGroup.getCode(), currentSubGroup.getLibelle(), serviceGroupeDsi.contains(currentSubGroup, groupeSelectionne), selectable));
                if (CollectionUtils.isNotEmpty(serviceGroupeDsi.getByParentGroup(currentSubGroup.getCode()))) {
                    load(currentSubGroup, groupeSelectionne);
                }
            }
        }
    }

    /**
     * Affiche l'arbre Javascript.
     *
     * @return Le code Javascript d'affichage de l'arbre
     * @deprecated Cf. deprecated de la classe
     */
    @Deprecated
    public String print() {
        return tree.print();
    }

    /**
     * Teste si le groupe DSI est sélectionnable.
     *
     * @param groupeDsi
     *            Le groupe DSI à tester
     *
     * @return true si le groupe DSI est sélectionnable
     * @deprecated Cf. deprecated de la classe
     */
    @Deprecated
    private boolean isGroupeSelectable(final GroupeDsiBean groupeDsi) {
        // Controle le périmètre de l'utilisateur sur le groupe
        boolean selectableItem = "1".equals(groupeDsi.getSelectionnable());
        if (selectableItem && permissions.length() > 0) {
            // patch filtre sur les groupes dynamiques
            if (permissions.startsWith("DYN")) {
                selectableItem = !(groupeDsi.getRequeteGroupe().length() > 0);
            } else {
                selectableItem = serviceGroupeDsi.controlerPermission(autorisations, permissions, groupeDsi.getCode());
            }
        }
        return selectableItem;
    }

    /**
     * Teste si le groupe est visible dans l'arbre (groupe sélectionnable ou ayant un fils visible.
     *
     * @param groupe
     *            Le groupe à tester
     * @param selectable
     *            true si le groupe est sélectionnable
     *
     * @return true si le groupe est visible dans l'arbre
     * @deprecated Cf. deprecated de la classe
     */
    @Deprecated
    private boolean isGroupeVisible(final GroupeDsiBean groupe, final boolean selectable) {
        boolean visible = selectable;
        if (!selectable) {
            for(GroupeDsiBean currentSubGroup : serviceGroupeDsi.getByParentGroup(groupe.getCode())) {
                visible = isGroupeVisible(currentSubGroup, isGroupeSelectable(currentSubGroup));
            }
        }
        return visible;
    }
}
