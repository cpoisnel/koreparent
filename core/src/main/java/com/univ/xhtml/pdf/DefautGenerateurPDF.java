/*
 * 
 */
package com.univ.xhtml.pdf;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.lang.CharEncoding;
// TODO: Auto-generated Javadoc

public class DefautGenerateurPDF implements IGenerateurPDF {

    private static final String INDENT_YES = "yes";

    /** The _log. */
    private static final Logger LOG = LoggerFactory.getLogger(DefautGenerateurPDF.class);

    /* (non-Javadoc)
     * @see com.univ.xhtml.pdf.IGenerateurPDF#createPDF(java.io.InputStream, java.io.OutputStream)
     */
    @Override
    public void createPDF(InputStream is, OutputStream os) throws ErreurApplicative {
        // configure fopFactory as desired
        FopFactory fopFactory = FopFactory.newInstance();
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        // configure foUserAgent as desired
        try {
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, os);
            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            // Set the value of a <param> in the stylesheet
            transformer.setParameter("versionParam", "2.0");
            transformer.setOutputProperty(OutputKeys.ENCODING, CharEncoding.DEFAULT.toLowerCase());
            transformer.setOutputProperty(OutputKeys.INDENT, INDENT_YES);
            // Setup input for XSLT transformation
            Source src = new StreamSource(is);
            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());
            // Start XSLT transformation and FOP processing
            transformer.transform(src, res);
        } catch (FOPException e) {
            String messErr = "Erreur dans la génération du PDF via FOP";
            LOG.error(messErr, e);
            throw new ErreurApplicative(messErr);
        } catch (TransformerConfigurationException e) {
            String messErr = "Erreur dans la génération du PDF via FOP : impossible d'initialiser le transformer";
            LOG.error(messErr, e);
        } catch (TransformerException e) {
            String messErr = "Erreur dans la génération du PDF via FOP : impossible de convertir le flux en pdf";
            LOG.error(messErr, e);
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                LOG.error("Echec fermeture du flux PDF", e);
            }
        }
    }

    /* (non-Javadoc)
     * @see com.univ.xhtml.pdf.IGenerateurPDF#createPDF(java.io.File, java.io.File, java.io.OutputStream)
     */
    @Override
    public void createPDF(File fXml, File fXslt, OutputStream os) throws ErreurApplicative {
        // configure fopFactory as desired
        FopFactory fopFactory = FopFactory.newInstance();
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        // configure foUserAgent as desired
        try {
            // Construct fop with desired output format
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, os);
            // Setup XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(fXslt));
            // Set the value of a <param> in the stylesheet
            transformer.setParameter("versionParam", "2.0");
            transformer.setOutputProperty(OutputKeys.ENCODING, CharEncoding.DEFAULT.toLowerCase());
            transformer.setOutputProperty(OutputKeys.INDENT, INDENT_YES);
            // Setup input for XSLT transformation
            Source src = new StreamSource(fXml);
            // Resulting SAX events (the generated FO) must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());
            // Start XSLT transformation and FOP processing
            transformer.transform(src, res);
        } catch (FOPException e) {
            String messErr = "Erreur dans la génération du PDF via FOP";
            LOG.error(messErr, e);
            throw new ErreurApplicative(messErr);
        } catch (TransformerConfigurationException e) {
            String messErr = "Erreur dans la génération du PDF via FOP : impossible d'initialiser le transformer";
            LOG.error(messErr, e);
        } catch (TransformerException e) {
            String messErr = "Erreur dans la génération du PDF via FOP : impossible de convertir le flux en pdf";
            LOG.error(messErr, e);
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                LOG.error("Echec fermeture du flux PDF", e);
            }
        }
    }
}