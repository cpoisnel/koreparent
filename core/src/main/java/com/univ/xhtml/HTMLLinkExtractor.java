/*
 * Created on 6 avr. 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.xhtml;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.AndFilter;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.filters.NodeClassFilter;
import org.htmlparser.filters.OrFilter;
import org.htmlparser.nodes.TagNode;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * LinkExtractor extracts all the links from the given webpage and prints them on standard output.
 */
public class HTMLLinkExtractor {

    private static final Logger LOG = LoggerFactory.getLogger(HTMLLinkExtractor.class);
    //private NodeFilter filter = new NodeClassFilter (LinkTag.class);

    /** The _instance. */
    private static HTMLLinkExtractor _instance;

    /**
     * Instantiates a new hTML link extractor.
     */
    private HTMLLinkExtractor() {}

    /**
     * Gets the instance.
     *
     * @return the hTML link extractor
     */
    public static HTMLLinkExtractor GetInstance() {
        if (_instance == null) {
            synchronized (HTMLLinkExtractor.class) {
                // pour le mutlithread
                if (_instance == null) {
                    _instance = new HTMLLinkExtractor();
                }
            }
        }
        return _instance;
    }

    /**
     * Extrait les liens d'une page.
     *
     * @param url
     *            the url
     * @param nodeFilter
     *            the node filter
     *
     * @return la liste des liens de la page.
     *
     */
    private NodeList extractLinksFromUrl(final String url, final NodeFilter nodeFilter) {
        NodeList list = new NodeList();
        if (url.length() > 0 && nodeFilter != null) {
            try {
                list = new Parser(url).extractAllNodesThatMatch(nodeFilter);
            } catch (final ParserException e) {
                LOG.error("erreur de parsing de l'url", e);
            }
        }
        return list;
    }

    /**
     * Extract links from input.
     *
     * @param input
     *            the input
     * @param nodeFilter
     *            the node filter
     *
     * @return the node list
     */
    private NodeList extractLinksFromInput(final String input, final NodeFilter nodeFilter) {
        NodeList list = new NodeList();
        if (input.length() > 0 && nodeFilter != null) {
            try {
                final Parser parser = new Parser();
                parser.setInputHTML(input);
                list = parser.extractAllNodesThatMatch(nodeFilter);
            } catch (final ParserException e) {
                LOG.error("erreur de parsing de l'input", e);
            }
        }
        return list;
    }

    /**
     * Extrait tous les liens d'une page (src href action onclick).
     *
     * @param url
     *            the url
     *
     * @return la liste des liens de la page.
     *
     * @throws ParserException
     *             the parser exception
     *
     * @exception ParserException
     *                If a parse error occurs.
     */
    public List<String> extractAllLinksFromUrl(final String url) throws ParserException {
        final List<String> res = new ArrayList<>();
        final HasAttributeFilter srcFilter = new HasAttributeFilter("src");
        final HasAttributeFilter hrefFilter = new HasAttributeFilter("href");
        final HasAttributeFilter actionFilter = new HasAttributeFilter("action");
        final HasAttributeFilter onclickFilter = new HasAttributeFilter("onclick");
        final NodeFilter nf = new OrFilter(new OrFilter(srcFilter, hrefFilter), new OrFilter(actionFilter, onclickFilter));
        final NodeList list = extractLinksFromUrl(url, nf);
        String link = "";
        for (int i = 0; i < list.size(); i++) {
            Node node = list.elementAt(i);
            if (node instanceof LinkTag) {
                link = ((LinkTag) node).extractLink();
            } else if (node instanceof ImageTag) {
                link = ((ImageTag) node).extractImageLocn();
            } else if (node instanceof TagNode) {
                if (((TagNode) node).getAttribute("src") != null) {
                    link = ((TagNode) node).getAttribute("src");
                } else if (((TagNode) node).getAttribute("href") != null) {
                    link = ((TagNode) node).getAttribute("href");
                } else if (((TagNode) node).getAttribute("action") != null) {
                    link = ((TagNode) node).getAttribute("action");
                } else if (((TagNode) node).getAttribute("onclick") != null) {
                    link = ((TagNode) node).getAttribute("onclick");
                }
            }
            if (!link.startsWith("#")) {
                res.add(link);
            }
        }
        return res;
    }

    /**
     * Extrait tous les liens d'une page (src href action onclick).
     *
     * @param input
     *            the input
     *
     * @return la liste des liens de la page.
     *
     * @throws ParserException
     *             the parser exception
     *
     * @exception ParserException
     *                If a parse error occurs.
     */
    public List<String> extractAllLinksFromInput(final String input) throws ParserException {
        final List<String> res = new ArrayList<>();
        final HasAttributeFilter srcFilter = new HasAttributeFilter("src");
        final HasAttributeFilter hrefFilter = new HasAttributeFilter("href");
        final HasAttributeFilter actionFilter = new HasAttributeFilter("action");
        final HasAttributeFilter onclickFilter = new HasAttributeFilter("onclick");
        final NodeFilter nf = new OrFilter(new OrFilter(srcFilter, hrefFilter), new OrFilter(actionFilter, onclickFilter));
        final NodeList list = extractLinksFromInput(input, nf);
        String link = "";
        for (int i = 0; i < list.size(); i++) {
            Node node = list.elementAt(i);
            if (node instanceof LinkTag) {
                link = ((LinkTag) node).extractLink();
            } else if (node instanceof ImageTag) {
                link = ((ImageTag) node).extractImageLocn();
            } else if (node instanceof TagNode) {
                if (((TagNode) node).getAttribute("src") != null) {
                    link = ((TagNode) node).getAttribute("src");
                } else if (((TagNode) node).getAttribute("href") != null) {
                    link = ((TagNode) node).getAttribute("href");
                } else if (((TagNode) node).getAttribute("action") != null) {
                    link = ((TagNode) node).getAttribute("action");
                } else if (((TagNode) node).getAttribute("onclick") != null) {
                    link = ((TagNode) node).getAttribute("onclick");
                }
            }
            if (!link.startsWith("#")) {
                res.add(link);
            }
        }
        return res;
    }

    /**
     * Extrait les liens html d'une page.
     *
     * @param url
     *            de la page
     *
     * @return la liste des liens de la page.
     *
     */
    public List<String> extractLinksHtmlFromUrl(final String url) {
        final List<String> res = new ArrayList<>();
        final NodeFilter nf = new AndFilter(new NodeClassFilter(LinkTag.class), new NodeFilter() {

            /**
             *
             */
            private static final long serialVersionUID = -8219547060385109384L;

            @Override
            public boolean accept(final Node node) {
                boolean res = false;
                if (!((LinkTag) node).isMailLink() && !((LinkTag) node).isJavascriptLink()) {
                    res = true;
                }
                return res;
            }
        });
        final NodeList list = extractLinksFromUrl(url, nf);
        for (int i = 0; i < list.size(); i++) {
            res.add(((LinkTag) list.elementAt(i)).getLink());
        }
        return res;
    }

    /**
     * Extrait les liens html d'une page.
     *
     * @param urlBase
     *            url de base
     * @param input
     *            contenu de la page
     *
     * @return la liste des liens de la page.
     *
     */
    public List<String> extractLinksHtmlFromInput(final String urlBase, final String input) {
        final List<String> res = new ArrayList<>();
        // ajout ou non des liens de type mails
        final NodeFilter nf = new AndFilter(new NodeClassFilter(LinkTag.class), new NodeFilter() {

            /**
             *
             */
            private static final long serialVersionUID = 2915510831612396731L;

            @Override
            public boolean accept(final Node node) {
                boolean res = false;
                if (((LinkTag) node).isHTTPLikeLink() || ((LinkTag) node).isHTTPLink() || ((LinkTag) node).isHTTPSLink()) {
                    res = true;
                }
                return res;
            }
        });
        final NodeList list = extractLinksFromInput(input, nf);
        String link = "";
        int nIndexDiese = -1;
        for (int i = 0; i < list.size(); i++) {
            link = ((LinkTag) list.elementAt(i)).getLink();
            // suppression des ancres
            nIndexDiese = link.indexOf('#');
            if (nIndexDiese != -1) {
                link = link.substring(0, nIndexDiese);
            }
            // traitement des urls relatives
            link = HTMLParser.getAbosulteUrl(link, urlBase);
            res.add(link);
        }
        return res;
    }
}
