package com.univ.xhtml;

import org.apache.commons.lang3.StringUtils;
import org.htmlparser.Parser;
import org.htmlparser.beans.StringBean;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extract plaintext strings from a web page. Illustrative program to gather the textual contents of a web page. Uses a {@link org.htmlparser.beans.StringBean StringBean} to
 * accumulate the user visible text (what a browser would display) into a single string.
 */
public class HTMLStringExtractor {

    private static final Logger LOG = LoggerFactory.getLogger(HTMLStringExtractor.class);

    /** The _instance. */
    private static HTMLStringExtractor _instance;

    /**
     * Instantiates a new hTML string extractor.
     */
    private HTMLStringExtractor() {}

    /**
     * Gets the instance.
     *
     * @return the hTML string extractor
     */
    public static HTMLStringExtractor GetInstance() {
        if (_instance == null) {
            synchronized (HTMLStringExtractor.class) {
                // pour le mutlithread
                if (_instance == null) {
                    _instance = new HTMLStringExtractor();
                }
            }
        }
        return _instance;
    }

    /**
     * Extract the text from a page.
     *
     * @param links
     *            if <code>true</code> include hyperlinks in output.
     * @param url
     *            the url
     *
     * @return The textual contents of the page.
     *
     * @exception ParserException
     *                If a parse error occurs.
     */
    public String extractAllStrings(final String url, final boolean links) {
        final StringBean sb = new StringBean();
        sb.setLinks(links);
        sb.setURL(url);
        return sb.getStrings();
    }

    /**
     * Extract the text from a page.
     *
     * @param input
     *            the input
     * @param tagNameFilter
     *            the tag name filter
     *
     * @return The textual contents of the page.
     *
     * @exception ParserException
     *                If a parse error occurs.
     */
    public NodeList extractTagFromInput(final String input, final TagNameFilter tagNameFilter) {
        NodeList res = new NodeList();
        try {
            final Parser parser = new Parser();
            parser.setInputHTML(input);
            res = parser.extractAllNodesThatMatch(tagNameFilter);
        } catch (final ParserException e) {
            LOG.debug("Problème d'extraction du contenu", e);
        }
        return res;
    }

    /**
     * Extract all strings from input.
     *
     * @param input
     *            the input
     * @param links
     *            the links
     *
     * @return the string
     */
    public String extractAllStringsFromInput(final String input, final boolean links) {
        String res = StringUtils.EMPTY;
        if (input.length() > 0) {
            try {
                final StringBean sb = new StringBean();
                sb.setLinks(links);
                final Parser parser = new Parser();
                parser.setInputHTML(input);
                parser.visitAllNodesWith(sb);
                res = sb.getStrings();
            } catch (final ParserException e) {
                LOG.debug("Problème d'extraction du contenu", e);
            }
        }
        return StringUtils.defaultString(res);
    }
}
