package com.univ.xhtml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;
import org.xml.sax.SAXException;

import com.jsbsoft.jtf.lang.CharEncoding;

/**
 * The Class XHTMLFormater.
 */
public class XHTMLFormater extends Tidy {

    private static final Logger LOG = LoggerFactory.getLogger(XHTMLFormater.class);

    /**
     *
     */
    private static final long serialVersionUID = -3705440946973887283L;

    /**
     * Transformer noeud.
     *
     * @param node
     *            the node
     */
    public static void transformerNoeud(final Node node) {
        // Suppression des ancres (<a name="">)
        // Idéalement, il faudrait les transformer
        // mais ca permet deja de ne pas planter
        if ("A".equalsIgnoreCase(node.getNodeName())) {
            final Element elt = (Element) node;
            final String name = elt.getAttribute("name");
            if (name.length() > 0) {
                elt.removeAttribute("name");
            }
        }
        /* Ajout tag WIDTH à partir du style */
        if ("IMG".equalsIgnoreCase(node.getNodeName())) {
            final Element elt = (Element) node;
            final String width = elt.getAttribute("width");
            if (width.length() == 0) {
                final String style = elt.getAttribute("style");
                if (style.length() > 0) {
                    String value = style.trim();
                    final String WIDTH = "WIDTH:";
                    final int iWidth = value.indexOf(WIDTH);
                    if (iWidth != -1) {
                        value = value.substring(iWidth + WIDTH.length()).trim();
                        int vWidth = 0;
                        int i = 0;
                        while (Character.isDigit(value.charAt(i))) {
                            vWidth = (vWidth * 10) + (value.charAt(i) - '0');
                            i++;
                        }
                        elt.setAttribute("width", vWidth + "px");
                    }
                }
            }
        }
        final NodeList fils = node.getChildNodes();
        for (int i = 0; i < fils.getLength(); i++) {
            transformerNoeud(fils.item(i));
        }
    }

    /**
     * Transform toolbox.
     *
     * @param _texte
     *            the _texte
     *
     * @return the string
     */
    public static String transformToolbox(String _texte) {
        String res = "";
        //Log.debug("transformToolbox " + _texte);
        //<root> obligatoire pour parser
        _texte = "<root>" + _texte + "</root>";
        try {
            /* Construction du DOM */
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.parse(new ByteArrayInputStream(_texte.getBytes()));
            final Element rootElement = document.getDocumentElement();
            /* Transformation du DOM */
            transformerNoeud(rootElement);
            /* Sauvegarde du DOM */
            final DOMSource source = new DOMSource(rootElement);
            final TransformerFactory transFactory = TransformerFactory.newInstance();
            final Transformer transformer = transFactory.newTransformer();
            final Properties props = new Properties();
            props.setProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            props.setProperty(OutputKeys.INDENT, "no");
            props.setProperty(OutputKeys.ENCODING, "utf-8");
            transformer.setOutputProperties(props);
            final ByteArrayOutputStream bos = new ByteArrayOutputStream();
            final StreamResult result = new StreamResult(bos);
            transformer.transform(source, result);
            res = bos.toString();
            // Suupression du tag XML
            res = res.replaceAll("<root>", "");
            res = res.replaceAll("</root>", "");
        } catch (final TransformerException | SAXException | IOException | ParserConfigurationException e) {
            LOG.error("unable to transform the XML", e);
            res = _texte;
        }
        //Log.debug("apres transformToolbox " + res);
        return res;
    }

    /**
     * Transform toolbox old.
     *
     * @param _texte
     *            the _texte
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public static String transformToolboxOld(final String _texte) throws Exception {
        String res = "";
        final StringBuffer newTexte = new StringBuffer(1000);
        final String texte = _texte;
        int indexTexte = 0;
        int indexDebutMotCle = -1;
        /* Ajout tag WIDTH à partir du style*/
        final String token = "style=\"WIDTH:";
        while ((indexDebutMotCle = texte.indexOf(token, indexTexte)) != -1) {
            // Recopie portion avant le mot- clé
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Analyse largeur
            final String suite = texte.substring(indexDebutMotCle + token.length()).trim();
            int width = 0;
            int i = 0;
            while (Character.isDigit(suite.charAt(i))) {
                width = (width * 10) + (suite.charAt(i) - '0');
                i++;
            }
            //Détection fin de tag IMG
            final int indiceFinTag = suite.indexOf('>');
            final int indiceWidth = suite.indexOf("width=");
            // Width déjà précisé ?
            if ((indiceFinTag != -1) && (indiceWidth != -1) && (indiceWidth > indiceFinTag)) {
                // insertion de la largeur avant le style
                newTexte.append(" width=\"").append(width).append("px\" ");
            }
            newTexte.append(token);
            indexTexte = indexDebutMotCle + token.length();
        }
        // Recopie dernière portion de texte
        newTexte.append(texte.substring(indexTexte));
        res = new String(newTexte);
        return res;
    }

    /**
     * Parses the contenu toolbox.
     *
     * @param html
     *            the html
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public String parseContenuToolbox(final String html) throws Exception {
        setXHTML(true);
        setWord2000(true);
        setWraplen(0);
        setDocType("strict");
        setDropEmptyParas(true);
        setQuoteNbsp(false);
        setNumEntities(false);
        setOutputEncoding(CharEncoding.DEFAULT);
        setInputEncoding(CharEncoding.DEFAULT);
        setShowErrors(0);
        setShowWarnings(false);
        setQuoteMarks(false);
        // Aucune indetation
        setBreakBeforeBR(false);
        setIndentContent(false);
        setIndentAttributes(false);
        setLiteralAttribs(false);
        setSmartIndent(false);
        setTrimEmptyElements(false);
        setQuiet(true);
        final Properties prop = new Properties();
        prop.put("numeric-entities", "false");
        prop.put("show-body-only", "true");
        getConfiguration().addProps(prop);
        final ByteArrayInputStream is = new ByteArrayInputStream(html.getBytes());
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        parse(is, os);
        return os.toString();
    }

    /**
     * Transform htm lto xml.
     *
     * @param html
     *            the html
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public String transformHTMLtoXML(final String html) throws Exception {
        setXHTML(true);
        final Properties prop = new Properties();
        prop.put("show-body-only", "true");
        prop.put("numeric-entities", "true");
        getConfiguration().addProps(prop);
        final ByteArrayInputStream is = new ByteArrayInputStream(html.getBytes());
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        parse(is, os);
        //return os.toString();
        return transformToolbox(os.toString());
    }

    /* Ancienne version (avant de passer à un parser DOM) */

    /**
     * Transform html for newsletter.
     *
     * @param html
     *            the html
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public String transformHTMLForNewsletter(final String html) throws Exception {
        setXHTML(true);
        final Properties prop = new Properties();
        prop.put("show-body-only", "false");
        prop.put("doctype", "omit");
        getConfiguration().addProps(prop);
        final ByteArrayInputStream is = new ByteArrayInputStream(html.getBytes());
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        parse(is, os);
        //return os.toString();
        return transformToolbox(os.toString());
    }
}
