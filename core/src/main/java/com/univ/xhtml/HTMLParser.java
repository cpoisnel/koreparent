/*
 * Created on 6 avr. 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.xhtml;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.filters.OrFilter;
import org.htmlparser.filters.TagNameFilter;
import org.htmlparser.nodes.TagNode;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.MetaTag;
import org.htmlparser.tags.ScriptTag;
import org.htmlparser.tags.TitleTag;
import org.htmlparser.util.NodeIterator;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.xhtml.tags.HeadLinkTag;
// TODO: Auto-generated Javadoc

/**
 * The Class HTMLParser.
 */
public class HTMLParser extends Parser {

    /**
     *
     */
    private static final long serialVersionUID = 241127507423866036L;

    private static final Logger LOG = LoggerFactory.getLogger(HTMLParser.class);

    /** The input html. */
    private String inputHtml = "";

    /**
     * Instantiates a new hTML parser.
     *
     */
    public HTMLParser() {
        super();
    }

    /**
     * Instantiates a new hTML parser.
     *
     * @param url
     *            the url
     *
     * @throws ParserException
     *             the parser exception
     */
    public HTMLParser(final String url) throws ParserException {
        super(url);
    }

    /**
     * Instantiates a new hTML parser.
     *
     * @param urlConnection
     *            the url connection
     *
     * @throws ParserException
     *             the parser exception
     */
    public HTMLParser(final URLConnection urlConnection) throws ParserException {
        super(urlConnection);
    }

    /**
     * this parses the robots.txt file. It was taken from the PERL implementation Since this is only rarely called, it's not optimized for speed
     *
     * @param r
     *            the robots.txt file
     *
     * @return the disallows
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     *
     * @exception IOException
     *                any IOException
     */
    public static String[] parseRobots(final BufferedReader r) throws IOException {
        final Pattern disallowPattern = Pattern.compile("Disallow:(.*)");
        final Pattern agentPattern = Pattern.compile("User-agent:\\*");
        String line;
        boolean all = false;
        final List<String> disallowed = new ArrayList<>();
        while ((line = r.readLine()) != null) {
            line = StringUtils.remove(line, " ");
            if (line.startsWith("User-agent:")) {
                final Matcher matcher = agentPattern.matcher(line);
                if (matcher.find()) {
                    all = true;
                    continue;
                } else {
                    all = false;
                }
            } else if (all && line.startsWith("Disallow:")) {
                final Matcher matcher = disallowPattern.matcher(line);
                while (matcher.find()) {
                    disallowed.add(matcher.group(1));
                    continue;
                }
            }
        }
        final String[] disalloweds = new String[disallowed.size()];
        disallowed.toArray(disalloweds);
        return disalloweds;
    }

    /**
     * Sets the input html.
     *
     * @param input
     *            the new input html
     */
    public void setInputHtml(final String input) {
        this.inputHtml = input;
    }

    /**
     * Gets the input html.
     *
     * @return the input html
     */
    public String getInputHtml() {
        return inputHtml;
    }

    /**
     * Parses the.
     *
     * @throws ParserException
     *             the parser exception
     */
    public void parse() throws ParserException {
        inputHtml = parse(null).toHtml();
    }

    /**
     * Extract links.
     *
     * @param urlBase
     *            the url base
     *
     * @return the list
     */
    public List<String> extractLinks(final String urlBase) {
        final HTMLLinkExtractor htmlLinkExtractor = HTMLLinkExtractor.GetInstance();
        return htmlLinkExtractor.extractLinksHtmlFromInput(urlBase, inputHtml);
    }

    /**
     * Extract string.
     *
     * @param links
     *            the links
     *
     * @return the string
     */
    public String extractString(final boolean links) {
        final HTMLStringExtractor stringExtractor = HTMLStringExtractor.GetInstance();
        return stringExtractor.extractAllStringsFromInput(inputHtml, links);
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        String res = "";
        final HTMLStringExtractor stringExtractor = HTMLStringExtractor.GetInstance();
        final NodeList nodeList = stringExtractor.extractTagFromInput(inputHtml, new TagNameFilter(new TitleTag().getTagName()));
        if (nodeList.size() == 1) {
            res = ((TitleTag) nodeList.elementAt(0)).getStringText();
        }
        return res;
    }

    /**
     * Gets the url canonical.
     *
     * @param urlBase : Url de base pour calculée l'url canonique si celle-c est relative
     *
     * @return the meta tag
     */
    public String getCanonicalLink(String urlBase) {
        String res = "";
        final HTMLStringExtractor stringExtractor = HTMLStringExtractor.GetInstance();
        final NodeList nodeList = stringExtractor.extractTagFromInput(inputHtml, new TagNameFilter(new HeadLinkTag().getTagName()));
        TagNode linkTag = new HeadLinkTag();
        for (int i = 0; i < nodeList.size(); i++) {
            linkTag = (TagNode) nodeList.elementAt(i);
            if ("canonical".equals(linkTag.getAttribute("rel"))) {
                if (linkTag.getAttribute("href") != null) {
                    res = linkTag.getAttribute("href");
                    break;
                }
            }
        }
        if (StringUtils.isNotBlank(res)) {
            res = getAbosulteUrl(res, urlBase);
        }
        return res;
    }

    /**
     * Méthode permettant de vérifier si l'url est une url absolue. Dans le cas contraire, on la calcule.
     * @param url : url à vérifier
     * @param urlBase : url de base de la page
     * @return
     */
    public static String getAbosulteUrl(String url, String urlBase) {
        if (!url.toLowerCase().startsWith("http") && (urlBase != null)) {
            if (url.toLowerCase().startsWith("www")) {
                url = "http://" + url;
            } else {
                try {
                    url = new URL(new URL(urlBase), url).toString();
                } catch (final MalformedURLException e) {
                    LOG.debug(e.getMessage());
                }
            }
        }
        return url;
    }

    /**
     * Gets the meta tag.
     *
     * @param properties
     *            the properties
     *
     * @return the meta tag
     */
    public String getMetaTag(final String properties) {
        String res = "";
        final HTMLStringExtractor stringExtractor = HTMLStringExtractor.GetInstance();
        final NodeList nodeList = stringExtractor.extractTagFromInput(inputHtml, new TagNameFilter(new MetaTag().getTagName()));
        MetaTag metaTag = new MetaTag();
        for (int i = 0; i < nodeList.size(); i++) {
            metaTag = ((MetaTag) nodeList.elementAt(i));
            if (properties.equalsIgnoreCase(metaTag.getMetaTagName()) || properties.equalsIgnoreCase(metaTag.getHttpEquiv())) {
                res = metaTag.getMetaContent();
                break;
            }
        }
        return res;
    }

    /**
     * Process absolute url.
     *
     * @param urlBase
     *            the url base
     *
     * @throws ParserException
     *             the parser exception
     */
    public void processAbsoluteUrl(final String urlBase) throws ParserException {
        final StringBuilder outputHtml = new StringBuilder();
        try {
            final Parser parser = new Parser();
            //parser.setInputHTML(Chaine.remplacerChaine1parChaine2(Chaine.remplacerChaine1parChaine2(inputHtml,"\r",""),"\n",""));
            parser.setInputHTML(inputHtml);
            final NodeIterator e = parser.elements();
            while (e.hasMoreNodes()) {
                final Node node = e.nextNode();
                processNodeAbsoluteUrl(node, urlBase);
                outputHtml.append(node.toHtml());
            }
        } catch (final ParserException e) {
            LOG.error("erreur de parsing de l'html", e);
        }
        setInputHtml(outputHtml.toString());
    }

    /**
     * Process node absolute url.
     *
     * @param node
     *            the node
     * @param urlBase
     *            the url base
     *
     * @throws ParserException
     *             the parser exception
     */
    private void processNodeAbsoluteUrl(final Node node, final String urlBase) throws ParserException {
        if (node instanceof TagNode && !(node instanceof ScriptTag)) {
            final TagNode tag = (TagNode) node;
            // process recursively (nodes within nodes) via getChildren()
            final NodeList nl = tag.getChildren();
            if (null != nl) {
                for (final NodeIterator i = nl.elements(); i.hasMoreNodes(); ) {
                    processNodeAbsoluteUrl(i.nextNode(), urlBase);
                }
            }
            final HasAttributeFilter srcFilter = new HasAttributeFilter("src");
            final HasAttributeFilter hrefFilter = new HasAttributeFilter("href");
            final HasAttributeFilter actionFilter = new HasAttributeFilter("action");
            final HasAttributeFilter backgroundFilter = new HasAttributeFilter("background");
            final NodeFilter nf = new OrFilter(new OrFilter(srcFilter, hrefFilter), new OrFilter(actionFilter, backgroundFilter));
            if (nf.accept(node)) {
                String link = "";
                if (node instanceof LinkTag) {
                    link = ((LinkTag) node).extractLink();
                } else if (node instanceof ImageTag) {
                    link = ((ImageTag) node).extractImageLocn();
                } else if (node instanceof TagNode) {
                    if (((TagNode) node).getAttribute("src") != null) {
                        link = ((TagNode) node).getAttribute("src");
                    } else if (((TagNode) node).getAttribute("href") != null) {
                        link = ((TagNode) node).getAttribute("href");
                    } else if (((TagNode) node).getAttribute("action") != null) {
                        link = ((TagNode) node).getAttribute("action");
                    } else if (((TagNode) node).getAttribute("background") != null) {
                        link = ((TagNode) node).getAttribute("background");
                    }
                }
                if (!link.toLowerCase().startsWith("http") && (urlBase != null)) {
                    if (link.toLowerCase().startsWith("www")) {
                        link = "http://" + link;
                    } else if (!link.startsWith("javascript")) {
                        try {
                            link = new URL(new URL(urlBase), link).toString();
                        } catch (final MalformedURLException me) {
                        }
                    }
                }
                if (((TagNode) node).getAttribute("src") != null) {
                    ((TagNode) node).setAttribute("src", link);
                } else if (((TagNode) node).getAttribute("href") != null) {
                    ((TagNode) node).setAttribute("href", link);
                } else if (((TagNode) node).getAttribute("action") != null) {
                    ((TagNode) node).setAttribute("action", link);
                } else if (((TagNode) node).getAttribute("background") != null) {
                    ((TagNode) node).setAttribute("background", link);
                }
            }
        }
    }
}
