package com.univ.autocomplete.processus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.jsbsoft.jtf.textsearch.ResultatRecherche;
import com.jsbsoft.jtf.textsearch.util.RechercheUtilBO;
import com.univ.autocomplete.bean.AutoCompleteSuggestion;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.utils.ContexteUtil;
import com.univ.utils.SessionUtil;

public class MultiFicheAutoCompletion extends GestionAutoCompletion {

    public static final String ID_BEAN = "multiFicheAutoComplete";

    private static final Logger LOG = LoggerFactory.getLogger(MultiFicheAutoCompletion.class);

    /**
     * A partir de l'objet FicheUniv, on retrouve l'ensemble des fiches et on les map dans une liste de résultat exploitable
     *
     * @param fiche
     * @return
     */
    private static List<AutoCompleteSuggestion> getResults(final AutorisationBean autorisations, final List<ResultatRecherche> liste) {
        final List<AutoCompleteSuggestion> resultats = new ArrayList<>();
        for (final ResultatRecherche res : liste) {
            final AutoCompleteSuggestion auto = new AutoCompleteSuggestion();
            auto.setData(res);
            auto.setValue(res.getTitre());
            resultats.add(auto);
        }
        return resultats;
    }

    @Override
    public List<AutoCompleteSuggestion> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        List<AutoCompleteSuggestion> resultats = new ArrayList<>();
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            return resultats;
        }
        try {
            resultats = getResults(autorisations, RechercheUtilBO.traiterRecherche(autorisations, ""));
        } catch (final IOException e) {
            LOG.error("Impossible d'executer la recherche", e);
            resultats = Collections.emptyList();
        }
        return resultats;
    }

    @Override
    public String getKeyForCache() {
        String code = "anonyme";
        if (ContexteUtil.getContexteUniv() != null) {
            if (StringUtils.isNotEmpty(ContexteUtil.getContexteUniv().getCode())) {
                code = ContexteUtil.getContexteUniv().getCode();
            }
        }
        return String.format("%s_%s", ID_BEAN, code);
    }
}
