package com.univ.autocomplete.processus;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.univ.autocomplete.bean.AutoCompleteSuggestion;

public abstract class GestionAutoCompletion {

    /**
     * Permet de traiter les critères de recherche reçu depuis un input
     *
     * @param req
     * @return
     */
    public abstract List<AutoCompleteSuggestion> traiterRechercheDepuisRequete(HttpServletRequest req);

    public abstract String getKeyForCache();
}
