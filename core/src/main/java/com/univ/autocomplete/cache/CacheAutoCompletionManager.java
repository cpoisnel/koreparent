package com.univ.autocomplete.cache;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.cache.annotation.Cacheable;

import com.univ.autocomplete.bean.AutoCompleteSuggestion;
import com.univ.autocomplete.processus.GestionAutoCompletion;

public class CacheAutoCompletionManager {

    public static final String ID_BEAN = "cacheAutoCompletionManager";

    @Cacheable(value = "CacheAutoCompletionManager.traiterRecherche", key = "#key")
    public List<AutoCompleteSuggestion> traiterRecherche(final String key, final GestionAutoCompletion autoCompletion, final HttpServletRequest req) {
        return autoCompletion.traiterRechercheDepuisRequete(req);
    }
}
