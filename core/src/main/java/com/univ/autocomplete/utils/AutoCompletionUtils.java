package com.univ.autocomplete.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.univ.autocomplete.bean.AutoCompleteSuggestion;

public class AutoCompletionUtils {

    public static final String PARAM_BEAN_AUTOCOMPLETION = "BEAN_AUTO_COMPLETION";

    private static int MAX_AUTOCOMPLETION_RESULTS = 20;

    public static int getNbMaxResults() {
        return MAX_AUTOCOMPLETION_RESULTS;
    }

    public static List<AutoCompleteSuggestion> filtrerResultats(List<AutoCompleteSuggestion> list, String query) {
        List<AutoCompleteSuggestion> resultats = new ArrayList<>();
        Iterator<AutoCompleteSuggestion> it = list.iterator();
        while (it.hasNext() && resultats.size() <= MAX_AUTOCOMPLETION_RESULTS) {
            AutoCompleteSuggestion currentSuggestion = it.next();
            if (StringUtils.containsIgnoreCase(currentSuggestion.getValue(), query) || currentSuggestion.isForced()) {
                resultats.add(currentSuggestion);
            }
        }
        return resultats;
    }
}
