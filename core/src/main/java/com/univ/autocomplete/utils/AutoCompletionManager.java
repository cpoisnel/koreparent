package com.univ.autocomplete.utils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.univ.autocomplete.processus.GestionAutoCompletion;

public class AutoCompletionManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "autoCompletionManager";

    /**
     * Récupère le bean d'id passé en paramètre pour exécuter les actions à partir du datagrid
     *
     * @param idBean
     *            le bean à récuperer
     * @return le bean correspondant ou null si non trouvé
     */
    public static GestionAutoCompletion getGestionAutoCompletion(final String idBean) {
        return (GestionAutoCompletion) ApplicationContextManager.getEveryContextBean(idBean);
    }
}
