package com.univ.autocomplete.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.univ.autocomplete.bean.AutoCompleteSuggestion;
import com.univ.autocomplete.bean.AutoCompletionObject;
import com.univ.autocomplete.cache.CacheAutoCompletionManager;
import com.univ.autocomplete.processus.GestionAutoCompletion;
import com.univ.autocomplete.utils.AutoCompletionManager;
import com.univ.autocomplete.utils.AutoCompletionUtils;

public class AutoCompletionServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -7982996758134619952L;

    private static final Logger LOG = LoggerFactory.getLogger(AutoCompletionServlet.class);

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
        try {
            final String query = StringUtils.defaultString(req.getParameter("query"));
            final String autoCompletionBeanName = req.getParameter(AutoCompletionUtils.PARAM_BEAN_AUTOCOMPLETION);
            final GestionAutoCompletion gestionAutoCompletion = AutoCompletionManager.getGestionAutoCompletion(autoCompletionBeanName);
            final CacheAutoCompletionManager cacheAutoCompletionManager = (CacheAutoCompletionManager) ApplicationContextManager.getCoreContextBean(CacheAutoCompletionManager.ID_BEAN);
            final List<AutoCompleteSuggestion> resultats = cacheAutoCompletionManager.traiterRecherche(query, gestionAutoCompletion, req);
            final AutoCompletionObject autoComplete = new AutoCompletionObject();
            autoComplete.setQuery(query);
            autoComplete.setSuggestions(AutoCompletionUtils.filtrerResultats(resultats, query));
            resp.setContentType("application/json");
			resp.setCharacterEncoding("UTF-8");
            resp.getWriter().write(mapper.writeValueAsString(autoComplete));
        } catch (final IOException e) {
            LOG.error("Une erreur est survenue lors de la construction de la liste de propositions", e);
        }
    }
}
