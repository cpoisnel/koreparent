package com.univ.url;

import java.io.Serializable;
import java.util.Hashtable;

public class CacheUrlRubrique implements Serializable {

    private static final long serialVersionUID = -3899683651417635832L;

    private final Hashtable<String, String> lstUrlNavigation;

    private final Hashtable<String, String> lstNavigationUrl;

    public CacheUrlRubrique() {
        lstUrlNavigation = new Hashtable<>();
        lstNavigationUrl = new Hashtable<>();
    }

    public Hashtable<String, String> getLstUrlNavigation() {
        return lstUrlNavigation;
    }

    public Hashtable<String, String> getLstNavigationUrl() {
        return lstNavigationUrl;
    }
}