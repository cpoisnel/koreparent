package com.univ.url.bean;

import com.univ.objetspartages.bean.AbstractPersistenceBean;

/**
 * Created by olivier.camon on 30/09/15.
 */
public class SectionUrlLinkBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = -6127577672573431150L;

    private long idUrl;

    private String sectionCode;

    public long getIdUrl() {
        return idUrl;
    }

    public void setIdUrl(long idUrl) {
        this.idUrl = idUrl;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }
}
