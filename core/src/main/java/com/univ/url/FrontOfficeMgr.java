package com.univ.url;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.url.service.ServiceUrl;
import com.univ.utils.ContexteDao;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.ExceptionFicheArchivee;
import com.univ.utils.ExceptionFicheNonAccessible;
import com.univ.utils.ExceptionFicheNonTrouvee;
import com.univ.utils.ExceptionLogin;
import com.univ.utils.ExceptionSite;
import com.univ.utils.ExceptionUrlObsolete;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.RequeteUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * The Class FrontOfficeMgr.
 */
public class FrontOfficeMgr {

    private static final Logger LOG = LoggerFactory.getLogger(FrontOfficeMgr.class);

    /** The Constant ID_BEAN. */
    private static final String ID_BEAN = "frontOfficeMgr";

    public CacheUrlManager cacheUrlManager;

    private ServiceUrl serviceUrl;

    private static EspaceCollaboratifDAO getEspaceCollaboratifDao(){
        return ApplicationContextManager.getCoreContextBean(EspaceCollaboratifDAO.ID_BEAN, EspaceCollaboratifDAO.class);
    }

    public void setCacheUrlManager(final CacheUrlManager cacheUrlManager) {
        this.cacheUrlManager = cacheUrlManager;
    }

    public void setServiceUrl(final ServiceUrl serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    /**
     * Lire fiche.
     *
     * @param request
     *            the request
     *
     * @return the fiche univ
     *
     * @throws Exception
     *             the exception
     */
    public static FicheUniv lireFiche(final HttpServletRequest request) throws Exception {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final String uri = request.getRequestURI();
        final String queryString = request.getQueryString();
        String url = uri;
        if (queryString != null && queryString.length() > 0) {
            url += "?" + queryString;
        }
        return lireFiche(ctx, url, request.getParameterMap());
    }

    /**
     * Lire fiche.
     *
     * @param ctx
     *            the ctx
     * @param request
     *            the request
     *
     * @throws Exception
     *             the exception
     */
    public static void lireFiche(final ContexteUniv ctx, final HttpServletRequest request) throws Exception {
        final String uri = request.getRequestURI();
        if (isURIFicheUniv(ctx, uri)) {
            final String queryString = request.getQueryString();
            String url = uri;
            if (queryString != null && queryString.length() > 0) {
                url += "?" + queryString;
            }
            lireFiche(ctx, url, request.getParameterMap());
        }
    }

    private static boolean isURIFicheUniv(final ContexteUniv ctx, final String uri) {
        return uri.startsWith(ctx.getInfosSite().getJspFo() + "/fiche_");
    }

    /**
     * Lire fiche.
     *
     * @param ctx
     *            the ctx
     * @param url
     *            the url
     * @param params
     *            the params
     *
     * @return the fiche univ
     *
     * @throws Exception
     *             the exception
     */
    public static FicheUniv lireFiche(final ContexteUniv ctx, final String url, final Map<String, String[]> params) throws Exception {
        boolean oldUrl = false;
        // url friendly du type /rub1/sourub1/titre-de-ma-page-ID_META-.extension
        String uri = url;
        String param = "";
        if (url.contains("?")) {
            uri = url.substring(0, url.indexOf("?"));
            param = url.substring(url.indexOf("?") + 1);
        }
        Long idMeta;
        try {
            idMeta = Long.valueOf(uri.substring(uri.lastIndexOf("-") + 1, uri.lastIndexOf(".")));
        } catch (final NumberFormatException e) {
            LOG.debug("no valid id specified", e);
            idMeta = null;
        }
        // on ne lit que les fiches en ligne ou les aperçus
        ClauseWhere whereMetatag = null;
        if (idMeta != null) {
            whereMetatag = new ClauseWhere(ConditionHelper.egal("ID_METATAG", idMeta, TypeOperande.LONG));
            whereMetatag.and(ConditionHelper.in("META_ETAT_OBJET", Arrays.asList("0003", "0005")));
        } else if (param.length() > 0) {
            String objet = RequeteUtil.renvoyerParametre(param, "OBJET");
            if (objet.length() == 0) {
                /** Patch JRO 20091230 : possibilité de choisir une charte par site */
                final String s = "^\\" + ctx.getInfosSite().getJspFo() + "\\/fiche_([a-z]+).jsp.*$";
                /** Fin patch */
                final Pattern p = Pattern.compile(s);
                final Matcher m = p.matcher(url);
                if (m.find()) {
                    objet = m.group(1);
                } else {
                    objet = "pagelibre";
                }
            }
            final String code = RequeteUtil.renvoyerParametre(param, "CODE");
            final String langue = RequeteUtil.renvoyerParametre(param, "LANGUE");
            whereMetatag = new ClauseWhere(ConditionHelper.egalVarchar("META_CODE_OBJET", ReferentielObjets.getCodeObjet(objet)));
            whereMetatag.and(ConditionHelper.egalVarchar("META_CODE", code));
            whereMetatag.and(ConditionHelper.egalVarchar("META_LANGUE", langue));
            whereMetatag.and(ConditionHelper.egalVarchar("META_ETAT_OBJET", "0003"));
            oldUrl = (RequeteUtil.renvoyerParametre(param, "JSP").length() == 0);
            // SBI : Retraitement propagation des paramètres dans l'url de redirection
            final String[] tParam = param.split("&");
            param = "";
            for (final String p : tParam) {
                if (!(p.startsWith("OBJET=") || p.startsWith("CODE=") || p.startsWith("LANGUE="))) {
                    param += (param.length() > 0 ? "&" : "") + p;
                }
            }
        }
        if (whereMetatag == null) {
            throw new ExceptionFicheNonTrouvee(MessageHelper.getCoreMessage("ST_FICHE_INEXISTANTE"));
        }
        final List<MetatagBean> metas = MetatagUtils.getMetatagService().getFromWhereClause(whereMetatag);
        if (CollectionUtils.isEmpty(metas)) {
            throw new ExceptionFicheNonTrouvee(MessageHelper.getCoreMessage("ST_FICHE_INEXISTANTE"));
        }
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        final MetatagBean meta = metas.get(0);
        ctx.setMetaCourant(meta);
        final String langueObjet = meta.getMetaLangue();
        final String etatObjet = meta.getMetaEtatObjet();
        //on récupère désormais l'aperçu à partir des méta
        if ("0005".equals(etatObjet)) {
            ctx.setApercu(true);
        }
        if ("0004".equals(etatObjet) || "0006".equals(etatObjet)) {
            throw new ExceptionFicheArchivee(MessageHelper.getCoreMessage("ST_FICHE_INEXISTANTE"));
        }
        if ("0007".equals(etatObjet)) {
            throw new ExceptionFicheArchivee(MessageHelper.getCoreMessage("ST_FICHE_ARCHIVEE"));
        }
        ctx.setLocale(LangueUtil.getLocale(langueObjet));
        ctx.setLangue(langueObjet);
        FicheUniv ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(meta.getMetaCodeObjet()));
        ficheUniv.init();
        ficheUniv.setIdFiche(meta.getMetaIdFiche());
        try (ContexteDao ctxDao = new ContexteDao()) {
            ficheUniv.setCtx(ctxDao);
            ficheUniv.retrieve();
        } catch (final Exception e) {
            throw new ExceptionFicheNonTrouvee(MessageHelper.getCoreMessage("ST_FICHE_INEXISTANTE"), e);
        }
        if (oldUrl) {
            String newUrl = URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(ctx, ficheUniv), ctx);
            if (param.length() > 0) {
                newUrl += (newUrl.contains("?")) ? "&" : "?" + param;
            }
            throw new ExceptionUrlObsolete(newUrl);
        }
        String codeRubriqueForcee = null;
        if (FicheUnivMgr.isFicheCollaborative(ficheUniv)) {
            final DiffusionSelective dif = (DiffusionSelective) ficheUniv;
            // on initialise le contexte collaboratif à la lecture et non plus à la génération de l'url
            ctx.setEspaceCourant(dif.getDiffusionPublicViseRestriction());
            final EspaceCollaboratifBean infosEspace = getEspaceCollaboratifDao().getByCode(dif.getDiffusionPublicViseRestriction());
            ctx.setEspace(infosEspace);
            codeRubriqueForcee = infosEspace.getCodeRubrique();
        }
        // lecture des données du plugin pour s
        PluginFicheHelper.setDataContexteUniv(ficheUniv, meta, StringUtils.EMPTY);
        if (!ctx.isApercu()) {
            InfosSite site = null;
            String codeRubriqueAffichage = null;
            if (params.get("RF") != null) {
                codeRubriqueForcee = params.get("RF")[0];
            }
            boolean controleRubrique = false;
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            if (codeRubriqueForcee != null) {
                controleRubrique = true;
                if (ctx.getInfosSite().isRubriqueVisibleInSite(serviceRubrique.getRubriqueByCode(codeRubriqueForcee))) {
                    codeRubriqueAffichage = codeRubriqueForcee;
                } else {
                    // on affiche la page dans le site de la rubrique
                    site = serviceInfosSite.determineSite(codeRubriqueForcee, true);
                }
            } else {
                //on constitue les rubriques ou la fiche peut être publiée
                final List<String> lstCodeRubriquePubliable = new ArrayList<>();
                if (meta.getMetaCodeRubrique().length() > 0) {
                    lstCodeRubriquePubliable.add(meta.getMetaCodeRubrique());
                }
                final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
                List<RubriquepublicationBean> results = serviceRubriquePublication.getByTypeCodeLanguage(meta.getMetaCodeObjet(), meta.getMetaCode(), langueObjet);
                for (RubriquepublicationBean rubriquepublicationBean : results) {
                    lstCodeRubriquePubliable.add(rubriquepublicationBean.getRubriqueDest());
                }
                codeRubriqueAffichage = serviceRubrique.getRubriquePublication(ctx, serviceRubrique.getRubriqueByCode(ctx.getCodeRubriqueHistorique()), lstCodeRubriquePubliable, true);
                if (codeRubriqueAffichage == null) {
                    site = serviceInfosSite.displaySite(ficheUniv);
                }
                if (site != null && site.equals(ctx.getInfosSite())) {
                    site = null;
                }
            }
            if (site != null && !site.getAlias().equals(ctx.getInfosSite().getAlias())) {
                String urlFiche = UnivWebFmt.determinerUrlFiche(ctx, ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName())), ficheUniv.getCode(), ficheUniv.getLangue(), true, codeRubriqueForcee);
                if (codeRubriqueForcee != null) {
                    if (urlFiche.contains("?")) {
                        urlFiche += "&amp;RF=" + codeRubriqueForcee;
                    } else {
                        urlFiche += "?RF=" + codeRubriqueForcee;
                    }
                }
                throw new ExceptionSite(URLResolver.getAbsoluteUrl(urlFiche, ctx, site));
            }
            if (codeRubriqueAffichage == null) {
                if (ctx.getInfosSite().getRestriction() == 1) {
                    //si la rubrique d'affichage est nulle a cause d'une restriction sur la rubrique,
                    //on redirige d'abord vers le login (si on est pas loggué) avant de renvoyer ce message
                    if (ctx.getCode().length() == 0) {
                        throw new ExceptionLogin(url);
                    } else {
                        String errorMessage = MessageHelper.getCoreMessage(ctx.getLocale(), "FICHE.ERREUR.NON_VISIBLE_DANS_SITE");
                        errorMessage += String.format(" [ code : %s , nom : %s, type : %s]", ficheUniv.getCode(), ficheUniv.getLibelleAffichable(), ficheUniv.getClass().getName());
                        throw new ErreurApplicative(errorMessage);
                    }
                }
                codeRubriqueAffichage = ctx.getCodeRubriquePageCourante();
            }
            // on ajoute le code rubrique affichage dans le contexte
            ctx.setCodeRubriqueFicheCourante(codeRubriqueAffichage);
            ctx.calculerCodeRubriquePageCourante();
            if (!FicheUnivMgr.controlerRestriction(ficheUniv, ctx, controleRubrique)) {
                String msg = MessageHelper.getCoreMessage(ctx.getLocale(), "ST_FICHE_INACCESSIBLE");
                if (ctx.getCode().length() > 0) {
                    msg += "<br />" + MessageHelper.getCoreMessage(ctx.getLocale(), "ST_PAS_DROITS_ACCES");
                    throw new ExceptionFicheNonAccessible(msg);
                } else {
                    throw new ExceptionLogin(url);
                }
            }
        }
        if (ctx.isApercu()) {
            ctx.setCodeRubriqueFicheCourante(ficheUniv.getCodeRubrique());
            ctx.calculerCodeRubriquePageCourante();
            FicheUnivMgr.supprimerFiche(ficheUniv, true);
        }
        //Fix pour les méthodes getFormatedXXX on est obligé de remettre un ContexteUniv pour pouvoir appeler UnivWebFmt...
        ficheUniv.setCtx(ctx);
        ctx.setFicheCourante(ficheUniv);
        return ficheUniv;
    }

    public static FrontOfficeMgr getInstance() {
        return (FrontOfficeMgr) ApplicationContextManager.getCoreContextBean(ID_BEAN);
    }

    /**
     * Determiner url friendly.
     *
     * @param ctx
     *            the ctx
     * @param codeObjet
     *            the code objet
     * @param codeFiche
     *            the code fiche
     * @param langueFiche
     *            the langue fiche
     * @param etatFiche
     *            the etat fiche
     * @param codeRubriqueAffichage
     *            the code rubrique affichage
     * @param params
     *            the params
     *
     * @return the string
     *
     */
    public String getAbsoluteUrlFriendly(final OMContext ctx, final String codeObjet, final String codeFiche, final String langueFiche, final String etatFiche, final String codeRubriqueAffichage, final String params) {
        if (ctx.getInfosSite() == null) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            ctx.setInfosSite(serviceInfosSite.getPrincipalSite());
        }
        String codeRubriqueCourante = ctx.getInfosSite().getCodeRubrique();
        if (ctx instanceof ContexteUniv) {
            codeRubriqueCourante = EscapeString.escapeURL(((ContexteUniv) ctx).getCodeRubriquePageCourante());
        }
        final UrlCacheRequest ucr = new UrlCacheRequest(ctx.getInfosSite().getAlias(), codeRubriqueCourante, codeRubriqueAffichage, codeObjet, codeFiche, langueFiche, etatFiche);
        String urlFiche = cacheUrlManager.getAbsoluteUrlFriendly(ucr.getKey(), ctx, (MetatagBean) null, ucr);
        // on flushe le cache si url vide
        if (StringUtils.isEmpty(urlFiche)) {
            cacheUrlManager.refreshUrl(ucr.getKey());
        }
        //AGA 08042010 correction ticket mantis 0008051 : ne pas renvoyer les params si l'url est vide
        if (StringUtils.isNotBlank(urlFiche) && StringUtils.isNotBlank(params)) {
            urlFiche += (urlFiche.contains("?")) ? "&" : "?";
            urlFiche += params;
        }
        return urlFiche;
    }

    /**
     * Gets the url accueil rubrique.
     *
     * @param infoSite
     *            the info site
     * @param codeRubrique
     *            the code rubrique
     *
     * @return the url accueil rubrique
     */
    public String getUrlAccueilRubrique(final InfosSite infoSite, final String codeRubrique) {
        return serviceUrl.findUrlByCode(codeRubrique, infoSite);
    }

    /**
     * Gets the url accueil rubrique.
     *
     * @param infoSite
     *            the info site
     * @param rubrique
     *            the code rubrique
     *
     * @return the url accueil rubrique
     */
    public String getUrlAccueilRubrique(final InfosSite infoSite, final RubriqueBean rubrique) {
        String result = null;
        if (rubrique != null) {
            result = getUrlAccueilRubrique(infoSite, rubrique.getCode());
        }
        return result;
    }

    /**
     * Gets the infos rubrique by url.
     *
     * @param url
     *            the url
     *
     * @return the infos rubrique by url
     * @deprecated les {@link InfosRubriques} sont dépréciées. Utiliser {@link ServiceUrl#findSection(String)}
     */
    @Deprecated
    public InfosRubriques getInfosRubriqueByUrl(final String url) {
        final String codeRubrique = serviceUrl.findSection(url);
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
        InfosRubriques result = new InfosRubriques("");
        if (rubriqueBean != null) {
            result = new InfosRubriques(rubriqueBean);
        }
        return result;
    }

}
