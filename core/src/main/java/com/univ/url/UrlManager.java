package com.univ.url;

import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.PropertyHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.om.Metatag;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ServicesUtil;
import com.univ.utils.URLResolver;

public class UrlManager {

    /**
     * Calculer url fiche.
     *
     * @param idMeta
     *            the id meta
     *
     * @return the string
     */
    public static String calculerUrlApercu(final String idMeta) {
        return calculerUrl(idMeta, "apercu");
    }

    /**
     * Calculer url fiche.
     *
     * @param idMeta
     *            the id meta
     *
     * @return the string
     */
    public static String calculerUrlPage(final String idMeta) {
        return calculerUrl(idMeta, "page");
    }

    /**
     * Calculer url fiche.
     *
     * @param idMeta
     *            the id meta
     * @param libelle
     *            the libelle
     *
     * @return the string
     */
    public static String calculerUrl(final String idMeta, final String libelle) {
        String extension = PropertyHelper.getCoreProperty("application.content_extension");
        if (extension == null || extension.length() == 0) {
            extension = "htm";
        }
        String url = "/";
        url += libelle;
        url += "-";
        url += idMeta;
        url += ".";
        url += extension;
        return url;
    }

    /**
     * Calculer url fiche.
     *
     * @param ctx
     *            the ctx
     * @param codeObjet
     *            the code objet
     * @param idFiche
     *            the id fiche
     *
     * @return the string
     * @deprecated le contexte ne sert plus, utiliser {@link UrlManager#calculerUrlFiche(String, Long)}
     */
    @Deprecated
    public static String calculerUrlFiche(final OMContext ctx, final String codeObjet, final Long idFiche) {
        return calculerUrlFiche(codeObjet, idFiche);
    }

    /**
     * Calculer url fiche.
     *
     * @param codeObjet
     *            the code objet
     * @param idFiche
     *            the id fiche
     *
     * @return the string
     */
    public static String calculerUrlFiche(final String codeObjet, final Long idFiche) {
        if (StringUtils.isEmpty(codeObjet) || "0010".equals(codeObjet)) {
            return StringUtils.EMPTY;
        }
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        final MetatagBean metatag = serviceMetatag.getByCodeAndIdFiche(codeObjet, idFiche);
        if (metatag != null) {
            return calculerUrlFiche(serviceInfosSite.determineSite(metatag.getMetaCodeRubrique(), true), serviceRubrique.getRubriqueByCode(metatag.getMetaCodeRubrique()), metatag);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Calculer url fiche.
     *
     * @param site
     * @param rubrique
     * @param meta
     *
     * @return the string
     * @deprecated Utilisez {@link #calculerUrlFiche(InfosSite, RubriqueBean, MetatagBean)}
     */
    @Deprecated
    public static String calculerUrlFiche(final InfosSite site, final InfosRubriques rubrique, final Metatag meta) {
        return calculerUrlFiche(site, rubrique, meta.getPersistenceBean());
    }

    /**
     * Calculer url fiche.
     *
     * @param site
     * @param rubrique
     * @param meta
     * @return the string
     * @deprecated préférer {@link #calculerUrlFiche(InfosSite, RubriqueBean, MetatagBean)}
     */
    @Deprecated
    public static String calculerUrlFiche(final InfosSite site, final InfosRubriques rubrique, final MetatagBean meta) {
        String urlFiche = Chaine.formatString(meta.getMetaLibelleFiche());
        if (site != null && site.getModeReecritureRubrique() != 0) {
            final String urlRubrique = FrontOfficeMgr.getInstance().getUrlAccueilRubrique(site, rubrique.getCode());
            if (StringUtils.isNotEmpty(urlRubrique)) {
                urlFiche = String.format("%s%s", urlRubrique, urlFiche);
                urlFiche = StringUtils.substringAfter(urlFiche, URLResolver.getAbsoluteUrl("/", site, 0));
            }
        }
        return calculerUrl(meta.getId().toString(), urlFiche);
    }

    /**
     * Calculer url fiche.
     *
     * @param site
     * @param rubrique
     * @param meta
     * @return the string
     */
    public static String calculerUrlFiche(final InfosSite site, final RubriqueBean rubrique, final MetatagBean meta) {
        String urlFiche = Chaine.formatString(meta.getMetaLibelleFiche());
        if (site != null && site.getModeReecritureRubrique() != 0) {
            final String urlRubrique = FrontOfficeMgr.getInstance().getUrlAccueilRubrique(site, rubrique);
            if (StringUtils.isNotEmpty(urlRubrique)) {
                urlFiche = String.format("%s%s", urlRubrique, urlFiche);
                urlFiche = StringUtils.substringAfter(urlFiche, URLResolver.getAbsoluteUrl("/", site, 0));
            }
        }
        return calculerUrl(meta.getId().toString(), urlFiche);
    }

    /**
     * Calculer url fiche.
     *
     * @param idMeta
     *            the id meta
     * @param ctx
     *            the ctx
     *
     * @return the string
     * @deprecated le contexte n'est plus utile, utilisez {@link UrlManager#calculerUrlFiche(Long)}
     */
    @Deprecated
    public static String calculerUrlFiche(final OMContext ctx, final Long idMeta) {
        return calculerUrlFiche(idMeta);
    }

    /**
     * Calculer url fiche.
     *
     * @param idMeta
     *            the id meta
     *
     * @return the string
     */
    public static String calculerUrlFiche(final Long idMeta) {
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final MetatagBean meta = serviceMetatag.getById(idMeta);
        if (meta != null) {
            if ("0010".equals(meta.getMetaCodeObjet())) {
                return "";
            }
            return calculerUrlFiche(serviceInfosSite.determineSite(meta.getMetaCodeRubrique(), true), serviceRubrique.getRubriqueByCode(meta.getMetaCodeRubrique()), meta);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Calculer url fiche.
     *
     * @param metatag
     *            the metatag
     *
     * @return the string
     * @deprecated Utilisez {@link #calculerUrlFiche(MetatagBean)}
     */
    @Deprecated
    public static String calculerUrlFiche(final Metatag metatag) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        return calculerUrlFiche(serviceInfosSite.determineSite(metatag.getMetaCodeRubrique(), true), serviceRubrique.getRubriqueByCode(metatag.getMetaCodeRubrique()), metatag.getPersistenceBean());
    }

    public static String calculerUrlFiche(final MetatagBean metatag) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        return calculerUrlFiche(serviceInfosSite.determineSite(metatag.getMetaCodeRubrique(), true), serviceRubrique.getRubriqueByCode(metatag.getMetaCodeRubrique()), metatag);
    }

    /**
     * Calculer url forward sso.
     *
     * @param ctx
     *            the ctx
     * @param urlForward
     *            url cible une fois l'indentification propagée
     *
     * @return l'url
     *
     * @throws Exception
     *             the exception
     */
    public static String calculerUrlForward(final ContexteUniv ctx, final String urlForward) throws Exception {
        // forward.html?kticket=xxxxxx&FWD=urlForward
        final String redirect = URLEncoder.encode(urlForward, CharEncoding.DEFAULT);
        String extension = PropertyHelper.getCoreProperty("application.content_extension");
        if (extension == null || extension.length() == 0) {
            extension = "htm";
        }
        return "/forward." + extension + "?kticket=" + ServicesUtil.genererTicketService(ctx) + "&FWD=" + redirect;
    }
}
