package com.univ.url.service;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.univ.multisites.InfosSite;
import com.univ.url.bean.UrlBean;

/**
 * Created by olivier.camon on 21/12/15.
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class UrlUpdaterJob implements Job {

    private static final Logger LOG = LoggerFactory.getLogger(UrlUpdaterJob.class);

    protected static final String URL_PARAMETER = "URL_PARAMETER";

    public static final String URL_JOB_NAME = "urlJob";

    public static final String URL_JOB_GROUP = "urlJobGroup";

    public static final JobKey JOB_KEY = new JobKey(URL_JOB_NAME, URL_JOB_GROUP);

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        LOG.debug("start URL cache update Job");
        final Object parameter = context.getMergedJobDataMap().get(URL_PARAMETER);
        ServiceUrl serviceUrl = ServiceManager.getServiceForBean(UrlBean.class);
        if (parameter == null) {
            serviceUrl.saveMissingSectionUrl();
        } else if (parameter instanceof InfosSite) {
            serviceUrl.flushForSite((InfosSite) parameter);
        } else if (parameter instanceof String) {
            serviceUrl.flushForSection((String) parameter);
        }
        LOG.debug("end URL cache update Job");
    }
}
