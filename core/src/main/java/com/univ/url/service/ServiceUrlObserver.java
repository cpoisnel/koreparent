package com.univ.url.service;

import java.util.Observable;
import java.util.Observer;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.kportal.scheduling.module.SchedulerManager;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * Created by olivier.camon on 13/11/15.
 */
public class ServiceUrlObserver implements Observer {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceUrlObserver.class);

    private ServiceRubrique serviceRubrique;

    private ServiceInfosSite serviceInfosSite;

    private PageAccueilRubriqueManager pageAccueilRubriqueManager;

    private SchedulerManager schedulerManager;

    private Scheduler scheduler;

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void setServiceInfosSite(final ServiceInfosSite serviceInfosSite) {
        this.serviceInfosSite = serviceInfosSite;
    }

    public void setPageAccueilRubriqueManager(final PageAccueilRubriqueManager pageAccueilRubriqueManager) {
        this.pageAccueilRubriqueManager = pageAccueilRubriqueManager;
    }


    public void init() throws SchedulerException {
        pageAccueilRubriqueManager.addObserver(this);
        serviceRubrique.addObserver(this);
        if (serviceInfosSite instanceof Observable) {
            ((Observable)serviceInfosSite).addObserver(this);
        }
        final JobDetail urlJob = JobBuilder.newJob(UrlUpdaterJob.class).withIdentity(UrlUpdaterJob.JOB_KEY).storeDurably().build();
        scheduler = schedulerManager.getScheduler();
        scheduler.getListenerManager().addSchedulerListener(new SchedulerListenerForUrl(scheduler, urlJob));
    }

    @Override
    public void update(final Observable o, final Object parameter) {
        LOG.debug("an update is schedule on the URL");
        JobDataMap dataMap = new JobDataMap();
        dataMap.put(UrlUpdaterJob.URL_PARAMETER, parameter);
        try {
            if (scheduler.isStarted()) {
                scheduler.triggerJob(UrlUpdaterJob.JOB_KEY, dataMap);
            } else {
                LOG.info("scheduler not started, the job will be triggered with a listener");
            }
        } catch (SchedulerException e) {
            LOG.error("unable to schedule url job", e);
        }
    }

    public void setSchedulerManager(final SchedulerManager schedulerManager) {
        this.schedulerManager = schedulerManager;
    }
}
