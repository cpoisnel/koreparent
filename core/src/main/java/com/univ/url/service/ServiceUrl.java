package com.univ.url.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;

import com.kosmos.service.ServiceBean;
import com.kportal.cache.CacheUtil;
import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;
import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.kportal.extension.module.plugin.rubrique.impl.BeanFichePageAccueil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.url.bean.SectionUrlLinkBean;
import com.univ.url.bean.UrlBean;
import com.univ.url.dao.SectionUrlLinkDAO;
import com.univ.url.dao.UrlDAO;
import com.univ.utils.Chaine;
import com.univ.utils.URLResolver;

/**
 * Service gérant les URLs des rubriques de l'application.
 * Created on 30/09/15.
 */
public class ServiceUrl implements ServiceBean<UrlBean> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceUrl.class);

    private UrlDAO urlDAO;

    private SectionUrlLinkDAO sectionUrlLinkDAO;

    private ServiceRubrique serviceRubrique;

    private ServiceInfosSite serviceInfosSite;

    public void setUrlDAO(final UrlDAO urlDAO) {
        this.urlDAO = urlDAO;
    }

    public void setSectionUrlLinkDAO(final SectionUrlLinkDAO sectionUrlLinkDAO) {
        this.sectionUrlLinkDAO = sectionUrlLinkDAO;
    }

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void setServiceInfosSite(final ServiceInfosSite serviceInfosSite) {
        this.serviceInfosSite = serviceInfosSite;
    }

    public void saveMissingSectionUrl() {
        LOG.debug("starting url generation");
        final List<RubriqueBean> all = serviceRubrique.getAllWithoutUrl();
        for (final RubriqueBean rubriqueBean : all) {
            UrlBean currentResult = generateUrlBean(rubriqueBean);
            if (currentResult != null) {
                currentResult = urlDAO.add(currentResult);
                addSectionLink(currentResult);
            }
        }
        LOG.debug("end url generation");
    }

    /**
     * Permet de gérer d'enregistrer l'url d'une rubrique en base de données.
     * @param urlBean le bean contenant l'url, le code de site, la rubrique et ses rubriques parentes.
     */
    public void save(UrlBean urlBean) {
        if (urlBean.getId() == 0L) {
            urlBean = urlDAO.add(urlBean);
            addSectionLink(urlBean);
        } else {
            urlBean = urlDAO.update(urlBean);
            sectionUrlLinkDAO.deleteByUrlId(urlBean.getId());
            addSectionLink(urlBean);
        }
    }


    private void addSectionLink(final UrlBean urlBean) {
        final List<SectionUrlLinkBean> allSections = new ArrayList<>();
        for (final String sectionCode : urlBean.getParentsSectionCodes()) {
            final SectionUrlLinkBean link = new SectionUrlLinkBean();
            link.setIdUrl(urlBean.getId());
            link.setSectionCode(sectionCode);
            allSections.add(link);
        }
        sectionUrlLinkDAO.addAll(allSections);
    }

    /**
     * Supprime l'url associé à une rubrique et ses rubriques parentes.
     * @param id l'id de l'url à supprimer
     */
    @Override
    public void delete(final Long id) {
        urlDAO.delete(id);
        sectionUrlLinkDAO.deleteByUrlId(id);
    }

    /**
     * Retourne l'url depuis son id. Dans ce service, cette méthode n'a que peu d'utilité.
     * @param id l'id stocké en base
     * @return le bean correspondant ou null si non trouvé.
     */
    @Override
    public UrlBean getById(final Long id) {
        final UrlBean urlBean = urlDAO.getById(id);
        if (urlBean != null) {
            final List<SectionUrlLinkBean> sectionsUrls = sectionUrlLinkDAO.getByUrlId(id);
            final List<String> parentsSections = new ArrayList<>();
            for (final SectionUrlLinkBean sectionUrl : sectionsUrls) {
                parentsSections.add(sectionUrl.getSectionCode());
            }
            urlBean.setParentsSectionCodes(parentsSections);
        }
        return urlBean;
    }

    /**
     * Depuis le code d'une rubrique, on calcule son URL.
     * Pour cela, on vérifie d'abord que l'url n'est pas présente en base. Si c'est le cas, c'est cette valeur qui est retournée.
     * Sinon, on regénère l'url de la rubrique.
     * @param sectionCode le code de la rubrique dont on souhaite connaitre l'url. Il peut être vide ou null.
     * @return l'url de la rubrique. Soit celle de la base de données, soit une chaine vide si aucune rubrique n'est associé à cette valeur.
     */
    @Cacheable(value = "ServiceUrl.findUrlByCode")
    public String findUrlByCode(final String sectionCode, final InfosSite site) {
        String result = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(sectionCode)) {
            final UrlBean urlBean = urlDAO.getBySectionCode(sectionCode);
            if (urlBean != null) {
                result = urlBean.getUrl();
            } else {
                final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(sectionCode);
                final UrlBean url = generateUrlBean(rubriqueBean);
                if (url != null) {
                    result = url.getUrl();
                }
            }
        }
        if (StringUtils.isNotBlank(result)) {
            result = URLResolver.getAbsoluteUrl(result, site, 0);
        }
        return result;
    }

    /**
     * Methode permettant de flusher les caches associé au code de rubrique fourni en paramètre. Si cette rubrique possède des enfants, celles ci seront aussi flusher.
     * Attention, les urls en base seront supprimés.
     * @param sectionCode le code de la rubrique dont on souhaite flusher les urls
     */
    public void flushForSection(final String sectionCode) {
        dropOldUrlFromSection(sectionCode);
        saveMissingSectionUrl();
        flushAllCache();
    }

    /**
     * Comme sont équivalent pour les rubriques cette méthodes flush le cache de l'ensemble des urls d'un site.
     * Attention, les urls en base seront supprimés.
     * @param infosSite le site dont on souhaite flusher les urls.
     */
    public void flushForSite(final InfosSite infosSite) {
        dropOldUrlFromWebSite(infosSite.getAlias());
        saveMissingSectionUrl();
        flushAllCache();
    }

    private void flushAllCache() {
        final CacheManager cacheManager = CacheUtil.getCacheManager();
        final Cache cacheUrl = cacheManager.getCache("ServiceUrl.findUrlByCode");
        final Cache cacheSection = cacheManager.getCache("ServiceUrl.findSection");
        cacheUrl.clear();
        cacheSection.clear();
    }

    /**
     * Supprime toutes les entrées en base d'une rubrique donnée.
     * @param sectionCode le code de la rubrique.
     */
    public void dropOldUrlFromSection(final String sectionCode) {
        urlDAO.deleteAllForSection(sectionCode);
        sectionUrlLinkDAO.deleteAllForSection(sectionCode);
    }

    /**
     * Supprime toutes les entrées en base d'un site donné.
     * @param codeSite le code du site.
     */
    public void dropOldUrlFromWebSite(final String codeSite) {
        sectionUrlLinkDAO.deleteAllForSite(codeSite);
        urlDAO.deleteAllForSite(codeSite);
    }

    /**
     * Essaye de retrouver le code d'une rubrique depuis l'url fourni en paramètre.
     * @param url l'url dont on souhaite retrouver la rubrique. Peut être null.
     * @return le code de la rubrique si retrouvé ou une chaine vide sinon.
     */
    @Cacheable(value = "ServiceUrl.findSection", unless ="#result == null || #result.isEmpty()")
    public String findSection(final String url) {
        String result = StringUtils.EMPTY;
        final String host = getHostFromUrl(url);
        final InfosSite site = serviceInfosSite.getSiteByHost(host);
        if (StringUtils.isNotBlank(url)) {
            final String urlSite = URLResolver.getAbsoluteUrl(StringUtils.EMPTY, site, 0);
            final UrlBean urlBean = urlDAO.getByUrlAndSite(StringUtils.removeStart(url, urlSite), site.getAlias());
            if (urlBean != null) {
                result = urlBean.getSectionCode();
            }
        }
        return result;
    }

    private String getHostFromUrl(final String url) {
        String result = StringUtils.EMPTY;
        try {
            URI uri = new URI(url);
            result = uri.getHost();
        } catch (URISyntaxException e) {
            LOG.error("unable to parse the given URL", e);
        }
        return result;
    }

    /*
     * FIXME Si deux rubriques ont la même URL, on rajoute - ou + jusqu'a ce qu'elle soit unique. Le pbm c'est qu'on ne peux pas garantir l'ordre dans lequel on calcule les urls si elles sont flusher... Idem, on est obligé de cacher des valeurs à null pour avoir le meme comportement qu'avant sur les urls de fiches... wtf.
     */
    private UrlBean generateUrlBean(final RubriqueBean currentSection) {
        UrlBean result = null;
        final InfosSite sectionSite = serviceInfosSite.determineSite(currentSection, Boolean.FALSE);
        if (sectionSite != null) {
            result = new UrlBean();
            result.setSectionCode(currentSection.getCode());
            result.setCodeSite(sectionSite.getAlias());
            final BeanPageAccueil beanAccueil = PageAccueilRubriqueManager.getInstance().getBeanPageAccueil(currentSection);
            final StringBuilder sectionUrl = new StringBuilder();
            final List<String> parentsSectionsCodes = new ArrayList<>();
            if (sectionSite.getModeReecritureRubrique() != 0) {
                final List<String> titles = new ArrayList<>();
                titles.add(currentSection.getIntitule());
                parentsSectionsCodes.add(currentSection.getCode());
                RubriqueBean parentSection = serviceRubrique.getRubriqueByCode(currentSection.getCodeRubriqueMere());
                while (parentSection != null) {
                    titles.add(parentSection.getIntitule());
                    parentsSectionsCodes.add(parentSection.getCode());
                    parentSection = serviceRubrique.getRubriqueByCode(parentSection.getCodeRubriqueMere());
                }
                sectionUrl.append(findSectionUrlFromTitles(currentSection, sectionSite, titles));
            } else {
                sectionUrl.append(Chaine.formatString(currentSection.getIntitule())).append("/");
            }
            String url = StringUtils.EMPTY;
            if (beanAccueil != null) {
                if (sectionSite.getModeReecritureRubrique() == 0 && beanAccueil instanceof BeanFichePageAccueil) {
                    url = null;
                } else {
                    url = ensureUniqueUrl(sectionSite, sectionUrl.insert(0, "/").toString());
                }
            }
            result.setUrl(url);
            result.setParentsSectionCodes(parentsSectionsCodes);
        }
        return result;
    }

    private String findSectionUrlFromTitles(final RubriqueBean currentSection, final InfosSite sectionSite, final List<String> titles) {
        final StringBuilder sectionUrl = new StringBuilder();
        final int niveauMini = sectionSite.getNiveauMinReecritureRubrique();
        final int niveauMaxi = sectionSite.getNiveauMaxReecritureRubrique();
        if (titles.size() <= niveauMaxi) {
            Collections.reverse(titles);
            for (final String title : titles) {
                if (titles.indexOf(title) >= niveauMini - 1 && titles.indexOf(title) <= niveauMaxi - 1) {
                    sectionUrl.append(Chaine.formatString(title)).append("/");
                }
            }
        }
        if (sectionUrl.length() == 0) {
            sectionUrl.append(Chaine.formatString(currentSection.getIntitule())).append("/");
        }
        return sectionUrl.toString();
    }

    private String ensureUniqueUrl(final InfosSite sectionSite, final String sectionUrl) {
        String url = StringUtils.removeEnd(sectionUrl, "/");
        while (urlDAO.getByUrlAndSite(url + "/", sectionSite.getAlias()) != null) {
            if (url.endsWith("-")) {
                url += "+";
            } else {
                url += "-";
            }
        }
        return url + "/";
    }

}
