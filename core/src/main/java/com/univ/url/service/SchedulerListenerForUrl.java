package com.univ.url.service;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.listeners.SchedulerListenerSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Fistouille permettant de contourner le fait qu'on ne puisse pas se binder sur le SchedulerStarted event du fait du comportement du produit.
 *
 */
public class SchedulerListenerForUrl extends SchedulerListenerSupport {

    private static final Logger LOG = LoggerFactory.getLogger(SchedulerListenerForUrl.class);

    // Deuxième fistouille pour être sûr de lancer le job uniquement au démarrage du Scheduler et pas après chaque séquence.
    private boolean onStartup = Boolean.TRUE;

    private Scheduler scheduler;

    private JobDetail urlJob;

    public SchedulerListenerForUrl(Scheduler quartzScheduler, JobDetail job) {
        scheduler = quartzScheduler;
        urlJob = job;
    }


    @Override
    public void triggerResumed(final TriggerKey triggerKey) {
        if (triggerKey == null && onStartup) {
            try {
                LOG.debug("adding the URLJob to the scheduler");
                scheduler.addJob(urlJob, true);
                scheduler.triggerJob(UrlUpdaterJob.JOB_KEY, null);
            } catch (SchedulerException e) {
                LOG.error("unable to schedule url job", e);
            }
            onStartup = Boolean.FALSE;
        }
    }

    @Override
    public void schedulerShutdown() {
        onStartup = Boolean.TRUE;
    }
}
