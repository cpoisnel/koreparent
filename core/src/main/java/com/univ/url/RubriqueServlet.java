package com.univ.url;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.url.bean.UrlBean;
import com.univ.url.service.ServiceUrl;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.ExceptionFicheNonTrouvee;
import com.univ.utils.RequeteUtil;
import com.univ.utils.ServicesUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;

/**
 * The Class RubriqueServlet.
 */
public class RubriqueServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(RubriqueServlet.class);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest
     * , javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void service(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        String forward = "";
        String sendRedirect = "";
        ContexteUniv ctx = null;
        boolean isInterne = true;
        try {
            ctx = ContexteUtil.getContexteUniv();
            // accès front-office
            String urlFrontOffice = PropertyHelper.getCoreProperty("application.front_site");
            final String extension = PropertyHelper.getCoreProperty("application.content_extension");
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            if (urlFrontOffice == null || urlFrontOffice.length() == 0) {
                urlFrontOffice = "/contribution";
            }
            if (request.getRequestURI().startsWith(urlFrontOffice)) {
                forward = UnivWebFmt.getUrlLogin(ctx, false);
            } else if (request.getRequestURI().endsWith("/")) {
                final String url = URLResolver.getBasePath(request);
                // page d'accueil du site
                final InfosSite infosSite = serviceInfosSite.getSiteByHost(request.getServerName());
                final String codeRubriqueSite = infosSite.getCodeRubrique();
                if ("/".equals(request.getRequestURI())) {
                    if (StringUtils.isNotBlank(codeRubriqueSite)) {
                        if (!serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), codeRubriqueSite)) {
                            if (ctx.getCode().length() == 0) {
                                UnivWebFmt.redirigerVersLogin(ctx, response, url + request.getRequestURI());
                                LOG.debug("Redirection login");
                                return;
                            } else {
                                throw new ErreurApplicative(MessageHelper.getCoreMessage(ctx.getLocale(), "RUBRIQUE.ERREUR.NON_VISIBLE_DANS_SITE"));
                            }
                        }
                    }
                    forward = gestionUrlForward(request, ctx, infosSite, extension);
                } else {
                    // Page interieure
                    final ServiceUrl serviceUrl = ServiceManager.getServiceForBean(UrlBean.class);
                    final String codeRubrique = serviceUrl.findSection(url + request.getRequestURI());
                    if (StringUtils.isNotBlank(codeRubrique)) {
                        final RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(codeRubrique);
                        if (rubriqueCourante != null) {
                            forward = UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, rubriqueCourante, false, "", true, false);
                        }
                        if (forward.length() > 0) {
                            // si il ne s'agit pas d'un contenu, on controle l'accès à la rubrique
                            if (!forward.contains("." + extension)) {
                                if (!serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), rubriqueCourante)) {
                                    if (ctx.getCode().length() == 0) {
                                        UnivWebFmt.redirigerVersLogin(ctx, response, url + request.getRequestURI());
                                        LOG.debug("Redirection login");
                                        return;
                                    } else {
                                        throw new ErreurApplicative(MessageHelper.getCoreMessage(ctx.getLocale(), "RUBRIQUE.ERREUR.NON_VISIBLE_DANS_SITE"));
                                    }
                                }
                            }
                            if (rubriqueCourante != null) {
                                isInterne = PageAccueilRubriqueManager.getInstance().isInterne(rubriqueCourante);
                            }
                        } else {
                            ctx.setCodeRubriquePageCourante(codeRubrique);
                            ctx.setCodeRubriqueFicheCourante(codeRubrique);
                            if (rubriqueCourante != null) {
                                ctx.setLocale(LangueUtil.getLocale(rubriqueCourante.getLangue()));
                            }
                            throw new ExceptionFicheNonTrouvee(MessageHelper.getCoreMessage("ST_FICHE_INEXISTANTE"));
                        }
                    }
                }
                if (isInterne && forward.indexOf(url) == 0) {
                    forward = forward.substring(url.length());
                }
                if (forward.startsWith("http://") || forward.startsWith("https://")) {
                    sendRedirect = forward;
                    forward = "";
                }
            }
            if (forward.length() > 0) {
                getServletContext().getRequestDispatcher(forward).forward(request, response);
            } else if (sendRedirect.length() > 0) {
                // si connecté quand on bascule vers un site de confiance on
                // génère un kticket et on précise le host referer
                if (ctx.getKsession().length() > 0 && ServicesUtil.validerUrl(sendRedirect)) {
                    sendRedirect += ((sendRedirect.contains("?")) ? "&" : "?");
                    sendRedirect += "kticket=" + ServicesUtil.genererTicketService(ctx) + "&ksite=" + URLResolver.getAbsoluteUrl("/", ctx);
                }
                response.sendRedirect(sendRedirect);
            } else {
                getServletContext().getNamedDispatcher("default").forward(request, response);
            }
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
            // possibilité de choisir une charte par site
            forward = (ctx != null ? ctx.getInfosSite().getJspFo() : serviceInfosSite.getPrincipalSite().getJspFo()) + "/jsb_exception.jsp";
            request.setAttribute("Exception", e);
            getServletContext().getRequestDispatcher(forward).forward(request, response);
        }
    }

    /**
     * calcule l'url vers laquelle il faut forwarder la requete.
     *
     * @param request
     * @param ctx
     * @param infosSite
     * @param extension
     * @return
     * @throws Exception
     */
    private String gestionUrlForward(final HttpServletRequest request, final ContexteUniv ctx, final InfosSite infosSite, final String extension) throws Exception {
        final String codeRubrique = infosSite.getCodeRubrique();
        String forward = getUrlAccueilSite(request, infosSite, extension);
        if (StringUtils.isBlank(forward)) {
            forward = getUrlAccueilRubrique(ctx, infosSite, codeRubrique);
        }
        return forward;
    }

    /**
     * redirection vers la page d'accueil spécifique + gestion du multilingue
     *
     * @param request
     * @param infosSite
     * @param extension
     * @return
     * @deprecated le paramètre urlAccueil du site est déprécié. Il doit être remplacer par l'accueil d'une rubrique.
     */
    @Deprecated
    private String getUrlAccueilSite(final HttpServletRequest request, final InfosSite infosSite, String extension) {
        String forward = StringUtils.EMPTY;
        if (infosSite.getUrlAccueil().length() > 0) {
            String accueil = infosSite.getUrlAccueil();
            // la langue est bien demandée
            if (accueil.contains("LANGUE=")) {
                int indiceLocaleCourant = 0;
                final String lg = RequeteUtil.renvoyerParametre(accueil, "LANGUE");
                // la langue n'est pas renseignée
                if ("".equals(lg)) {
                    String langueNavigateur = request.getHeader("accept-language");
                    if (langueNavigateur != null && langueNavigateur.length() > 0) {
                        // on ne garde que les 2 premiers caractères de la locale, suivant la codification ISO 3166
                        if (langueNavigateur.length() > 2) {
                            langueNavigateur = langueNavigateur.substring(0, 2);
                        }
                        indiceLocaleCourant = LangueUtil.getIndiceFromCodeISO(langueNavigateur);
                    }
                } else {
                    if (LangueUtil.getLocale(lg) == null) {
                        indiceLocaleCourant = -1;
                    }
                }
                //si la locale du navigateur n'est pas une des locales déclarées dans K-Portal, on bascule en français
                if (indiceLocaleCourant == -1) {
                    indiceLocaleCourant = 0;
                }
                accueil = accueil.replaceAll("LANGUE=" + lg, "LOCALE=" + indiceLocaleCourant);
            }
            // si l'url est une jsp non fiche et non déjà forwardé, on ajoute le paramètre
            if (!accueil.contains("OBJET=") && !accueil.contains("FWD=")) {
                accueil = "FWD=" + accueil;
            }
            if (extension == null || extension.length() == 0) {
                extension = "htm";
            }
            forward = "/accueil." + extension + "?" + accueil;
        }
        return forward;
    }

    /**
     * Renvoi l'url de l'accueil de la rubrique de site.
     *
     * @param ctx
     * @param infosSite
     * @param codeRubrique
     * @return
     * @throws Exception
     */
    private String getUrlAccueilRubrique(final ContexteUniv ctx, final InfosSite infosSite, final String codeRubrique) throws Exception {
        String forward = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(codeRubrique)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (rubriqueCourante != null) {
                ctx.setLocale(LangueUtil.getLocale(rubriqueCourante.getLangue()));
                forward = UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, rubriqueCourante, false, "", false, false);

            }
            // si la rubrique n'a pas de page de tête on redirige vers le site principal(si il existe)
            if (StringUtils.isBlank(forward) && !isSitePrincipal(infosSite.getAlias())) {
                forward = renvoyerUrlAccueilSitePrincipal(ctx);
            }
        }
        return forward;
    }

    /**
     * Renvoie vrai si le site dont l'id est passé en paramètre est le site principal
     *
     * @param alias
     *            L'id du site à tester
     * @return vrai si le site dont l'id est passé en paramètre est le site principal OU s'il n'y a pas de site principal (PK ça !!! je ne sais pas)
     */
    private boolean isSitePrincipal(final String alias) throws Exception {
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        final InfosSite infosSitePrincipal = serviceInfosSite.getPrincipalSite();
        if (infosSitePrincipal == null) {
            LOG.debug("aucun site principal");
            return Boolean.TRUE;
        }
        final String aliasSitePrincipal = infosSitePrincipal.getAlias();
        return StringUtils.equals(alias, aliasSitePrincipal);
    }

    /**
     * Renvoie l'url d'accueil du site principal ou vide si elle n'est pas trouvée
     *
     * @param ctx
     *            Le contexte univ
     * @return l'url d'accueil du site principal OU vide si aucun site principal
     * @throws Exception
     */
    private String renvoyerUrlAccueilSitePrincipal(final ContexteUniv ctx) throws Exception {
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        final InfosSite infosSitePrincipal = serviceInfosSite.getPrincipalSite();
        if (infosSitePrincipal == null) {
            LOG.debug("Pas de site principal");
            return StringUtils.EMPTY;
        }
        final String codeRubriqueSite = infosSitePrincipal.getCodeRubrique();
        return UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, codeRubriqueSite, Boolean.FALSE);
    }
}
