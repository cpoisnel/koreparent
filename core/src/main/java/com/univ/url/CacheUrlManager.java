package com.univ.url;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.om.Metatag;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;

@Component
public class CacheUrlManager {

    public static final String ID_BEAN = "cacheUrlManager";

    public static final String KEY_CACHE = "CacheUrlManager.cacheUrlRubrique";

    private ServiceRubriquePublication serviceRubriquePublication;

    private ServiceRubrique serviceRubrique;

    private ServiceInfosSite serviceInfosSite;

    private ServiceMetatag serviceMetatag;

    public void setServiceRubrique(ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void setServiceRubriquePublication(final ServiceRubriquePublication serviceRubriquePublication) {
        this.serviceRubriquePublication = serviceRubriquePublication;
    }

    public void setServiceInfosSite(final ServiceInfosSiteProcessus serviceInfosSite) {
        this.serviceInfosSite = serviceInfosSite;
    }

    public void setServiceMetatag(ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }
    
    public static CacheUrlManager getInstance() {
        return (CacheUrlManager) ApplicationContextManager.getCoreContextBean(ID_BEAN);
    }

    @Deprecated
    public String getAbsoluteUrlFriendly(final String key, final OMContext ctx, final Metatag metatag, final UrlCacheRequest ucr) {
        return getAbsoluteUrlFriendly(key, ctx, metatag.getPersistenceBean(), ucr);
    }

    @Cacheable(value = "CacheUrlManager.getAbsoluteUrlFriendly", key = "#key")
    public String getAbsoluteUrlFriendly(final String key, final OMContext ctx, final MetatagBean metatag, final UrlCacheRequest ucr) {
        if (metatag == null) {
            return getAbsoluteUrlFriendly(ctx, ucr);
        } else {
            final InfosSite infosSite = serviceInfosSite.getInfosSite(ucr.getAlias());
            return URLResolver.getAbsoluteUrl(UrlManager.calculerUrlFiche(infosSite, serviceRubrique.getRubriqueByCode(ucr.getCodeRubrique()), metatag), ctx, infosSite);
        }
    }

    @CacheEvict(value = "CacheUrlManager.getAbsoluteUrlFriendly")
    public void refreshUrl(final String key) {
        // nothing
    }

    /**
     * Calcule les url friendly.
     *
     * @param ctx
     *            le contexte courant pour connaitre le site etc.
     * @param ucr
     *            le cache de la request
     *
     * @return l'url absolu de la fiche/rubrique
     *
     */
    private String getAbsoluteUrlFriendly(final OMContext ctx, final UrlCacheRequest ucr) {
        final String codeRubriqueCourante = ucr.getCodeRubrique();
        final String codeRubriqueAffichage = ucr.getCodeRubriqueAffichage();
        final String codeObjet = ucr.getCodeObjet();
        final String codeFiche = ucr.getCodeFiche();
        String langueFiche = ucr.getLangueFiche();
        String etatFiche = ucr.getEtatFiche();
        if (etatFiche == null || etatFiche.length() == 0) {
            etatFiche = "0003";
        }
        if (langueFiche == null || langueFiche.length() == 0) {
            if (ctx instanceof ContexteUniv) {
                langueFiche = ((ContexteUniv) ctx).getLangue();
            } else {
                langueFiche = "0";
            }
        }
        boolean chargementFiche = false;
        MetatagBean meta = serviceMetatag.getByMetaCodeObjetCodeLangueEtat(codeObjet, codeFiche, langueFiche, etatFiche);

        if (meta != null) {
                chargementFiche = true;
            } else {
                // Si la fiche n'est pas présente dans une langue étrangère,
                // on charge la fiche en francais
                if (!ctx.getLocale().equals(LangueUtil.getDefaultLocale())) {
                    meta = serviceMetatag.getByMetaCodeObjetCodeLangueEtat(codeObjet, codeFiche, LangueUtil.getLangueLocale(LangueUtil.getDefaultLocale()), etatFiche);
                    if (meta != null) {
                        chargementFiche = true;
                    }
                }
            }
            if (!chargementFiche) {
                // on cherche l'archive éventuelle
                meta = serviceMetatag.getByMetaCodeObjetCodeLangueEtat(codeObjet, codeFiche, langueFiche, "0007");
                if (meta != null) {
                    chargementFiche = true;
                }
            }
            if (!chargementFiche) {
                return StringUtils.EMPTY;
            }
        //on constitue les rubriques ou la fiche peut être publiée
        final List<String> lstCodeRubriquePubliable = new ArrayList<>();
        if (StringUtils.isNotBlank(meta.getMetaCodeRubrique())) {
            lstCodeRubriquePubliable.add(meta.getMetaCodeRubrique());
        }
        List<RubriquepublicationBean> results = serviceRubriquePublication.getByTypeCodeLanguage(codeObjet, codeFiche, langueFiche);
        for (RubriquepublicationBean rubriquepublicationBean : results) {
            lstCodeRubriquePubliable.add(rubriquepublicationBean.getRubriqueDest());
        }
        //si la fiche est publiable dans le site courant
        //si la fiche est publiable dans la rubrique courante
        //on calcule l'url avec le host courant et la rubrique courante
        //sinon
        //si la fiche est rattachée ou publiée dans le site courant
        //on calcule l'url avec le host courant et la rubrique de publication de la fiche dans le site courant
        //sinon
        //on calcule l'url avec le host courant et la rubrique courante
        //sinon
        //si la fiche est rattachée à une rubrique appartenant à un site
        //on calcule l'url avec la rubrique de la fiche et le host principal du site de la rubrique
        //sinon
        //si la fiche est publiée dans des rubriques appartenant à un site
        //on choisit la rubrique avec en priorité - 1 : site principal - 2 : sites non restreint - 3 : sites restreints
        //sinon
        //on calcule avec le host courant et la rubrique courante
        //si la fiche est publiable dans le site courant avec une priorité pour la rubrique incluse dans la rubrique courante
        String codeRubriqueAffichageTemp;
        if (StringUtils.isNotBlank(codeRubriqueAffichage)) {
            codeRubriqueAffichageTemp = codeRubriqueAffichage;
        } else {
            codeRubriqueAffichageTemp = serviceRubrique.getRubriquePublication(ctx, serviceRubrique.getRubriqueByCode(codeRubriqueCourante), lstCodeRubriquePubliable, true);
            if (codeRubriqueAffichageTemp == null || codeRubriqueAffichageTemp.equals(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE)) {
                // si la fiche est rattachée à une rubrique appartenant à un site
                for (String codeRubriqueTemp : lstCodeRubriquePubliable) {
                    if (serviceInfosSite.determineSite(codeRubriqueTemp, false) != null) {
                        codeRubriqueAffichageTemp = codeRubriqueTemp;
                        break;
                    }
                }
            }
            if (codeRubriqueAffichageTemp == null || codeRubriqueAffichageTemp.equals(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE)) {
                if (serviceInfosSite.getPrincipalSite() != null) {
                    codeRubriqueAffichageTemp = serviceInfosSite.getPrincipalSite().getCodeRubrique();
                } else {
                    codeRubriqueAffichageTemp = codeRubriqueCourante;
                }
            }
        }
        final InfosSite site = serviceInfosSite.determineSite(codeRubriqueAffichageTemp, true);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubriqueAffichageTemp);
        return URLResolver.getAbsoluteUrl(UrlManager.calculerUrlFiche(site, rubriqueBean, meta), ctx, site);
    }

}
