package com.univ.url;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kportal.extension.module.composant.Menu;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.SousParagrapheBean;

public class FrontOfficeBean implements Serializable {

    private static final long serialVersionUID = -49714174839342236L;

    private transient FicheUniv ficheUniv = null;

    private MetatagBean metatag = null;

    private boolean accueilSite = Boolean.FALSE;

    private boolean accueilRubrique = Boolean.FALSE;

    private boolean saisieFront = Boolean.FALSE;

    private boolean collaboratif = Boolean.FALSE;

    private boolean activationDSI = Boolean.FALSE;

    private boolean dsi = Boolean.FALSE;

    private boolean apercu = Boolean.FALSE;

    private boolean recherche = Boolean.FALSE;

    private boolean navigationSecondairePresente = Boolean.FALSE;

    private boolean encadrePresent = Boolean.FALSE;

    private String encadresAutoFiche;

    private Map<String, List<SousParagrapheBean>> encadresFiche;

    private List<String> encadresRecherche;

    private String redacteur = StringUtils.EMPTY;

    private String visuelRubrique;

    private String accrocheRubrique;

    private String contactRubrique;

    private String couleurTitreRubrique = "#fff";

    private String couleurFondRubrique = "#666";

    private String jspFo = "";

    private List<Menu> menuAccesDirect;

    private List<Menu> menuPrincipal;

    private List<Menu> menuSecondaire;

    private List<Menu> menuLangue;

    private List<Menu> menuPiedDePage;

    private List<Menu> menuReseauxSociaux;

    public List<Menu> getMenuPrincipal() {
        return menuPrincipal;
    }

    public void setMenuPrincipal(final List<Menu> menuPrincipal) {
        this.menuPrincipal = menuPrincipal;
    }

    public List<Menu> getMenuSecondaire() {
        return menuSecondaire;
    }

    public void setMenuSecondaire(final List<Menu> menuSecondaire) {
        this.menuSecondaire = menuSecondaire;
    }

    public List<Menu> getMenuLangue() {
        return menuLangue;
    }

    public void setMenuLangue(final List<Menu> menuLangue) {
        this.menuLangue = menuLangue;
    }

    public String getCouleurTitreRubrique() {
        return couleurTitreRubrique;
    }

    public void setCouleurTitreRubrique(final String couleurTitreRubrique) {
        this.couleurTitreRubrique = couleurTitreRubrique;
    }

    public String getCouleurFondRubrique() {
        return couleurFondRubrique;
    }

    public void setCouleurFondRubrique(final String couleurFondRubrique) {
        this.couleurFondRubrique = couleurFondRubrique;
    }

    public FicheUniv getFicheUniv() {
        return ficheUniv;
    }

    public void setFicheUniv(final FicheUniv ficheUniv) {
        this.ficheUniv = ficheUniv;
    }

    public MetatagBean getMetatag() {
        return metatag;
    }

    public void setMetatag(final MetatagBean metatag) {
        this.metatag = metatag;
    }

    public boolean isAccueilRubrique() {
        return accueilRubrique;
    }

    public void setAccueilRubrique(final boolean isAccueilRubrique) {
        this.accueilRubrique = isAccueilRubrique;
    }

    public boolean isAccueilSite() {
        return accueilSite;
    }

    public void setAccueilSite(final boolean isAccueilSite) {
        this.accueilSite = isAccueilSite;
    }

    public boolean isSaisieFront() {
        return saisieFront;
    }

    public void setSaisieFront(final boolean saisieFront) {
        this.saisieFront = saisieFront;
    }

    public boolean isCollaboratif() {
        return collaboratif;
    }

    public void setCollaboratif(final boolean collaboratif) {
        this.collaboratif = collaboratif;
    }

    public boolean isDsi() {
        return dsi;
    }

    public void setDsi(final boolean dsi) {
        this.dsi = dsi;
    }

    public boolean isApercu() {
        return apercu;
    }

    public void setApercu(final boolean apercu) {
        this.apercu = apercu;
    }

    public boolean isRecherche() {
        return recherche;
    }

    public void setRecherche(final boolean recherche) {
        this.recherche = recherche;
    }

    public boolean isNavigationSecondairePresente() {
        return navigationSecondairePresente;
    }

    public void setNavigationSecondairePresente(final boolean navigationSecondairePresente) {
        this.navigationSecondairePresente = navigationSecondairePresente;
    }

    public boolean isEncadrePresent() {
        return encadrePresent;
    }

    public void setEncadrePresent(final boolean isEncadrePresent) {
        this.encadrePresent = isEncadrePresent;
    }

    public String getEncadresAutoFiche() {
        return encadresAutoFiche;
    }

    public void setEncadresAutoFiche(final String encadresAutoFiche) {
        this.encadresAutoFiche = encadresAutoFiche;
    }

    public Map<String, List<SousParagrapheBean>> getEncadresFiche() {
        return encadresFiche;
    }

    public void setEncadresFiche(final Map<String, List<SousParagrapheBean>> encadresFiche) {
        this.encadresFiche = encadresFiche;
    }

    public List<String> getEncadresRecherche() {
        return encadresRecherche;
    }

    public void setEncadresRecherche(final List<String> encadresRecherche) {
        this.encadresRecherche = encadresRecherche;
    }

    public boolean isActivationDSI() {
        return activationDSI;
    }

    public void setActivationDSI(final boolean activationDSI) {
        this.activationDSI = activationDSI;
    }

    public String getAccrocheRubrique() {
        return accrocheRubrique;
    }

    public void setAccrocheRubrique(final String accrocheRubrique) {
        this.accrocheRubrique = accrocheRubrique;
    }

    public String getContactRubrique() {
        return contactRubrique;
    }

    public void setContactRubrique(final String contactRubrique) {
        this.contactRubrique = contactRubrique;
    }

    public String getVisuelRubrique() {
        return visuelRubrique;
    }

    public void setVisuelRubrique(final String visuelRubrique) {
        this.visuelRubrique = visuelRubrique;
    }

    public String getRedacteur() {
        return redacteur;
    }

    public void setRedacteur(final String redacteur) {
        this.redacteur = redacteur;
    }

    public List<Menu> getMenuPiedDePage() {
        return menuPiedDePage;
    }

    public void setMenuPiedDePage(final List<Menu> menuPiedDePage) {
        this.menuPiedDePage = menuPiedDePage;
    }

    public String getJspFo() {
        return jspFo;
    }

    public void setJspFo(final String jspFo) {
        this.jspFo = jspFo;
    }

    public List<Menu> getMenuAccesDirect() {
        return menuAccesDirect;
    }

    public void setMenuAccesDirect(final List<Menu> menuAccesDirect) {
        this.menuAccesDirect = menuAccesDirect;
    }

    public List<Menu> getMenuReseauxSociaux() {
        return menuReseauxSociaux;
    }

    public void setMenuReseauxSociaux(final List<Menu> menuReseauxSociaux) {
        this.menuReseauxSociaux = menuReseauxSociaux;
    }
}
