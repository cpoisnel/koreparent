package com.univ.url.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.univ.url.bean.SectionUrlLinkBean;

/**
 * Created by olivier.camon on 30/09/15.
 */
public class SectionUrlLinkDAO extends AbstractCommonDAO<SectionUrlLinkBean> {

    public SectionUrlLinkDAO() {
        tableName = "SECTION_URL_LINK";
    }

    public void addAll(List<SectionUrlLinkBean> sectionUrlLinks) {
        if (CollectionUtils.isNotEmpty(sectionUrlLinks)) {
            List<SqlParameterSource> allParameters = new ArrayList<>();
            for (SectionUrlLinkBean section : sectionUrlLinks) {
                allParameters.add(getParameters(section));
            }
            namedParameterJdbcTemplate.batchUpdate("INSERT INTO SECTION_URL_LINK (ID_URL, SECTION_CODE) VALUES (:idUrl, :sectionCode)", allParameters.toArray(new SqlParameterSource[allParameters.size()]));
        }
    }

    public void deleteByUrlId(final Long id) throws DeleteFromDataSourceException {
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
            namedParameterJdbcTemplate.update(String.format("delete from `%1$s` WHERE ID_URL = :id", tableName), namedParameters);
        } catch (DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with id %d from table \"%s\"", id, tableName), dae);
        }
    }

    public List<SectionUrlLinkBean> getByUrlId(final Long id) {
        List<SectionUrlLinkBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("urlId", id);
            results = namedParameterJdbcTemplate.query("select * from SECTION_URL_LINK WHERE ID_URL = :urlId", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on ID_URL field with %s value and on field ENCADRE and ACCROCHE with %s values in table \"SECTION_URL_LINK\"", id, id), dae);
        }
        return results;
    }

    public void deleteAllForSection(final String sectionCode) throws DataSourceException {
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("sectionCode", sectionCode);
            namedParameterJdbcTemplate.update(
                "DELETE FROM SECTION_URL_LINK WHERE SECTION_URL_LINK.SECTION_CODE = :sectionCode",
                namedParameters);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured deleting object with SECTION_CODE %s from table %s", sectionCode, tableName), dae);
        }
    }

    public void deleteAllForSite(final String codeSite) throws DataSourceException {
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("codeSite", codeSite);
            namedParameterJdbcTemplate.update(
                "DELETE FROM SECTION_URL_LINK WHERE EXISTS(SELECT 0 FROM URL WHERE URL.ID_URL = SECTION_URL_LINK.ID_URL AND URL.CODE_SITE = :codeSite)", namedParameters);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured deleting object with URL.CODE_SITE %s from table %s", codeSite, tableName), dae);
        }
    }

}
