package com.univ.url.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.url.bean.UrlBean;

/**
 * Created by olivier.camon on 30/09/15.
 */
public class UrlDAO extends AbstractCommonDAO<UrlBean> {

    private static final Logger LOG = LoggerFactory.getLogger(UrlDAO.class);

    public UrlDAO() {
        tableName = "URL";
    }

    @Override
    public UrlBean add(UrlBean bean) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            namedParameterJdbcTemplate.update("INSERT INTO URL (SECTION_CODE, CODE_SITE, URL) VALUES (:sectionCode, :codeSite, :url)", getParameters(bean), keyHolder);
            bean.setId(keyHolder.getKey().longValue());
        } catch (DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"%s\"", bean.toString(), tableName), dae);
        }
        return bean;
    }

    public void addAll(final List<UrlBean> urlBeans) throws DataSourceException {
        List<SqlParameterSource> allParameters = new ArrayList<>();
        for (UrlBean url : urlBeans) {
            allParameters.add(getParameters(url));
        }
        try {
            namedParameterJdbcTemplate.batchUpdate("INSERT INTO URL (SECTION_CODE, CODE_SITE, URL) VALUES (:sectionCode, :codeSite, :url)", allParameters.toArray(new SqlParameterSource[allParameters.size()]));
        } catch (final DataAccessException dae) {
            throw new DataSourceException("unable to execute the given batch insert", dae);
        }
    }

    public UrlBean getBySectionCode(final String sectionCode) throws DataSourceException {
        UrlBean result = null;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("sectionCode", sectionCode);
            result = namedParameterJdbcTemplate.queryForObject("select * from `URL` T1 WHERE T1.SECTION_CODE = :sectionCode", namedParameters, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for sectionCode %s on table %s", sectionCode, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size for sectionCode %s on table %s", sectionCode, tableName));
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with sectionCode %s from table %s", sectionCode, tableName), dae);
        }
        return result;
    }

    public UrlBean getByUrl(final String url) throws DataSourceException {
        UrlBean result = null;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("url", url);
            result = namedParameterJdbcTemplate.queryForObject("select * from `URL` T1 WHERE T1.URL = :url", namedParameters, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for url %s on table %s", url, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size for url %s on table %s", url, tableName));
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with url %s from table %s", url, tableName), dae);
        }
        return result;
    }

    public UrlBean getByUrlAndSite(final String url, final String site) throws DataSourceException {
        UrlBean result = null;
        try {
            final MapSqlParameterSource namedParameters = new MapSqlParameterSource("url", url);
            namedParameters.addValue("site", site);
            result = namedParameterJdbcTemplate.queryForObject("select * from `URL` T1 WHERE T1.URL = :url AND CODE_SITE = :site", namedParameters, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for url %s on table %s", url, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size for url %s on table %s", url, tableName));
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with url %s from table %s", url, tableName), dae);
        }
        return result;
    }

    public void deleteAllForSection(final String sectioncode) throws DataSourceException {
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("sectioncode", sectioncode);
            namedParameterJdbcTemplate.update(
                "DELETE FROM URL WHERE URL.SECTION_CODE = :sectioncode OR EXISTS( SELECT 0 FROM SECTION_URL_LINK WHERE URL.ID_URL = SECTION_URL_LINK.ID_URL AND SECTION_URL_LINK.SECTION_CODE = :sectioncode)",
                namedParameters);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured deleting object with SECTION_CODE %s from table %s", sectioncode, tableName), dae);
        }
    }


    public void deleteAllForSite(final String codeSite) throws DataSourceException {
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("codeSite", codeSite);
            namedParameterJdbcTemplate.update(" DELETE FROM URL WHERE URL.CODE_SITE = :codeSite", namedParameters);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured deleting object with CODE_SITE %s from table %s", codeSite, tableName), dae);
        }
    }
}
