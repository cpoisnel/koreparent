package com.univ.url;

import com.univ.objetspartages.bean.MetatagBean;
// TODO: Auto-generated Javadoc

/**
 * Classe représentant une requête de type lien de requêtes ou liste d'objets à laquelle on a associé un cache.
 */
public class UrlCacheRequest {

    private String alias = null;

    /** The code rubrique. */
    private String codeRubrique = null;

    /** The code rubrique affichage. */
    private String codeRubriqueAffichage = null;

    /** The code objet. */
    private String codeObjet = null;

    /** The code fiche. */
    private String codeFiche = null;

    /** The langue fiche. */
    private String langueFiche = null;

    /** The etat fiche. */
    private String etatFiche = null;

    /**
     * Constructeur.
     *
     * @param alias
     * @param codeRubrique
     *            the code rubrique
     * @param codeRubriqueAffichage
     *            the code rubrique affichage
     * @param codeObjet
     *            the code objet
     * @param codeFiche
     *            the code fiche
     * @param langueFiche
     *            the langue fiche
     * @param etatFiche
     *            the etat fiche
     */
    public UrlCacheRequest(String alias, String codeRubrique, String codeRubriqueAffichage, String codeObjet, String codeFiche, String langueFiche, String etatFiche) {
        this.alias = alias;
        this.codeRubrique = codeRubrique;
        this.codeRubriqueAffichage = codeRubriqueAffichage;
        this.codeObjet = codeObjet;
        this.codeFiche = codeFiche;
        this.langueFiche = langueFiche;
        this.etatFiche = etatFiche;
    }

    public UrlCacheRequest(String alias, MetatagBean metatag) {
        this.alias = alias;
        this.codeRubrique = metatag.getMetaCodeRubrique();
        this.codeRubriqueAffichage = "";
        this.codeObjet = metatag.getMetaCodeObjet();
        this.codeFiche = metatag.getMetaCode();
        this.langueFiche = metatag.getMetaLangue();
        this.etatFiche = metatag.getMetaEtatObjet();
    }

    public String getKey() {
        return alias + "@" + codeRubrique + "@" + codeRubriqueAffichage + "@" + codeObjet + "@" + codeFiche + "@" + langueFiche + "@" + etatFiche;
    }

    public String getAlias() {
        return alias;
    }

    public String getCodeRubrique() {
        return codeRubrique;
    }

    public String getCodeRubriqueAffichage() {
        return codeRubriqueAffichage;
    }

    public String getCodeObjet() {
        return codeObjet;
    }

    public String getCodeFiche() {
        return codeFiche;
    }

    public String getLangueFiche() {
        return langueFiche;
    }

    public String getEtatFiche() {
        return etatFiche;
    }
}
