/*
 * Created on 10 févr. 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.univ.xmlrpc;

import java.util.Hashtable;
import java.util.Vector;

import org.slf4j.LoggerFactory;
// TODO: Auto-generated Javadoc

/**
 * The Class RequeteXmlRpc.
 *
 * @author jean-sébastien steux
 *
 *
 *
 *         Modif le 10 févr. 2004
 */
public class RequeteXmlRpc {

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(RequeteXmlRpc.class);

    /** The o res. */
    Object oRes = new Object();

    /** The res. */
    Hashtable res = new Hashtable();

    /** The fin. */
    private boolean fin = false;

    /** The is object. */
    private boolean isObject = false;

    /**
     * Terminer.
     *
     * @param o
     *            the o
     */
    public void terminer(final Object o) {
        if (isObject) {
            oRes = o;
        } else if (o instanceof Hashtable) {
            res = (Hashtable) o;
        }
        fin = true;
    }

    /**
     * Lancer thread.
     *
     * @param url
     *            the url
     * @param user
     *            the user
     * @param password
     *            the password
     * @param methode
     *            the methode
     * @param param
     *            the param
     * @param timeOut
     *            the time out
     *
     * @return the hashtable
     */
    public Hashtable lancerThread(final String url, final String user, final String password, final String methode, final Vector param, final long timeOut) {
        try {
            final Thread thread = (new Thread(new ThreadXmlRpc(this, url, user, password, methode, param)));
            thread.start();
            final long debut = System.currentTimeMillis();
            long maintenant = System.currentTimeMillis();
            do {
                Thread.sleep(1000);
                maintenant = System.currentTimeMillis();
            } while ((!fin) && maintenant - debut < timeOut);
        } catch (final Exception e) {
            LOG.error("impossible de lancer le thread", e);
        }
        return res;
    }

    /**
     * Lancer thread return object.
     *
     * @param url
     *            the url
     * @param user
     *            the user
     * @param password
     *            the password
     * @param methode
     *            the methode
     * @param param
     *            the param
     * @param timeOut
     *            the time out
     *
     * @return the object
     */
    public Object lancerThreadReturnObject(final String url, final String user, final String password, final String methode, final Vector param, final long timeOut) {
        isObject = true;
        try {
            final Thread thread = (new Thread(new ThreadXmlRpc(this, url, user, password, methode, param)));
            thread.start();
            final long debut = System.currentTimeMillis();
            long maintenant = System.currentTimeMillis();
            do {
                Thread.sleep(1000);
                maintenant = System.currentTimeMillis();
            } while ((!fin) && maintenant - debut < timeOut);
        } catch (final Exception e) {
            LOG.error("impossible de lancer le thread", e);
        }
        return oRes;
    }
}
