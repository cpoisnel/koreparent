package com.univ.rss;

import java.lang.reflect.Constructor;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.core.config.PropertyHelper;
import com.univ.utils.RequeteUtil;

/**
 * Factory permettant d'obtenir la classe appropriée pour gérer l'export RSS d'un objet.
 *
 * @author fred
 */
public class ExportRssFactory {

    private static final Logger LOG = LoggerFactory.getLogger(ExportRssFactory.class);

    /** Singleton. */
    private static ExportRssFactory instance = null;

    /** Liste des constructeurs des gestionnaires d'export rss pour les différents objets pris en compte. */
    private HashMap<String, Constructor<?>> exportRssConstructors = null;

    /**
     * Constructeur privé.
     */
    private ExportRssFactory() {
        this.exportRssConstructors = new HashMap<>();
        final Properties allProperties = PropertyHelper.getAllProperties();
        final Enumeration<Object> en = allProperties.keys();
        //Class[] exportRssClassParams = new Class[4];
        final Class<?>[] exportRssClassParams = {String.class, String.class, String.class};
        while (en.hasMoreElements()) {
            final String jtfParam = (String) en.nextElement();
            if (jtfParam.startsWith("export_rss.") && jtfParam.contains(".class")) {
                // Récupération du nom de l'objet à exporter
                final int deb = jtfParam.indexOf("export_rss.") + 11;
                final int fin = jtfParam.indexOf(".class", deb);
                final String objectName = jtfParam.substring(deb, fin).toUpperCase();
                try {
                    // Récupération de la classe de l'export rss
                    Class<?> exportRssClass = Class.forName(allProperties.getProperty(jtfParam));
                    // Stockage du constructeur de l'export rss
                    exportRssConstructors.put(objectName, exportRssClass.getConstructor(exportRssClassParams));
                } catch (final ReflectiveOperationException e) {
                    LOG.error("Problème d'initialisation de l'export rss pour l'objet " + objectName, e);
                }
            }
        }
    }

    /**
     * Renvoie le singleton.
     *
     * @return L'unique instance de cette classe
     */
    public static ExportRssFactory getInstance() {
        if (instance == null) {
            syncGetInstance();
        }
        return instance;
    }

    /**
     * Sync get instance.
     */
    private static synchronized void syncGetInstance() {
        if (instance == null) {
            instance = new ExportRssFactory();
        }
    }

    /**
     * Renvoie l'instance appropriée du gestionnaire d'exports rss.
     *
     *
     * @return the abstract export rss
     *
     * @throws ErreurApplicative
     *             the erreur applicative
     *
     * @retun Une instance d'export rss
     */
    public AbstractExportRss create(String requete) throws ErreurApplicative {
        if (requete == null) {
            requete = "";
        }
        final String typeFlux = StringUtils.defaultIfEmpty(RequeteUtil.renvoyerParametre(requete, "TYPE_FLUX_FEED"), PropertyHelper.getCoreProperty("export.flux.typeDefaut"));
        final String objet = StringUtils.defaultIfEmpty(RequeteUtil.renvoyerParametre(requete, "OBJET").toUpperCase(), "ACTUALITE");
        final String selection = RequeteUtil.renvoyerParametre(requete, "SELECTION");
        if (StringUtils.isEmpty(selection)) {
            if (requete.length() > 0) {
                requete += "&";
            }
            requete += "SELECTION=0007";
        }
        final String nombre = RequeteUtil.renvoyerParametre(requete, "NOMBRE");
        int nb = 10;
        // limite du nombre de résultat
        if (StringUtils.isEmpty(nombre)) {
            if (requete.length() > 0) {
                requete += "&";
            }
            requete += "NOMBRE=" + nb;
        } else {
            // parsing du nombre si présent
            try {
                nb = Integer.parseInt(nombre);
                // limite à 100 pour ne pas surcharger le serveur sql
                if (nb > 100) {
                    nb = 100;
                }
            } catch (final NumberFormatException e) {
                LOG.debug("Impossible de récupérer le nombre d'élément à afficher " + nombre);
            }
            requete = StringUtils.replace(requete, "NOMBRE=" + nombre, "NOMBRE=" + nb);
        }
        // Récupération du constructeur associé à l'export rss de cet objet
        Constructor<?> constructor = exportRssConstructors.get(objet);
        if (constructor == null) {
            constructor = exportRssConstructors.get("DEFAUT");
        }
        final Object[] params = {requete, objet, typeFlux};
        AbstractExportRss rss;
        try {
            rss = (AbstractExportRss) constructor.newInstance(params);
        } catch (final ReflectiveOperationException | IllegalArgumentException e) {
            LOG.error("Problème lors de l'instanciation du gestionnaire d'export rss pour l'objet " + objet, e);
            throw new ErreurApplicative("Une erreur est survenue lors de l'export rss.");
        }
        return rss;
    }
}
