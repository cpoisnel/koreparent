package com.univ.rss;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

import com.kportal.extension.module.plugin.objetspartages.IPluginRecherche;
import com.kportal.extension.module.plugin.objetspartages.PluginRechercheHelper;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.ContexteUniv;

public class CacheFeedManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "cacheFeedManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheFeedManager.class);

    private static Map<String, Object> transformQueryToMap(String query) {
        final Map<String, Object> parametersMap = new HashMap<>();
        query = StringUtils.removeStart(query, "?");
        final List<String> parameters = Arrays.asList(StringUtils.split(query, "&"));
        for (final String parameterAndValues : parameters) {
            final String[] valuesByParamNames = StringUtils.split(parameterAndValues, "=");
            if(valuesByParamNames.length == 2) {
                parametersMap.put(valuesByParamNames[0], valuesByParamNames[1]);
            }
        }
        return parametersMap;
    }

    private static String transformMapToQuery(final Map<String, Object> queryToTransform) {
        final StringBuilder queryString = new StringBuilder();
        for (final Map.Entry<String, Object> valueByParameterName : queryToTransform.entrySet()) {
            if (queryString.length() > 0) {
                queryString.append('&');
            }
            queryString.append(valueByParameterName.getKey()).append('=').append(valueByParameterName.getValue());
        }
        return queryString.toString();
    }

    @Cacheable(value = "CacheFeedManager.getSyndFeed")
    public SyndFeed getSyndFeed(String query, final ContexteUniv ctx, final AbstractExportRss aer) {
        final SyndFeed feed = new SyndFeedImpl();
        try {
            aer.setFeedProperties(ctx, feed);
            final FicheUniv fiche = ReferentielObjets.instancierFiche(aer.getObjectName());
            if (fiche != null) {
                fiche.setCtx(ctx);
                fiche.init();
                mapFeedFromFicheUniv(fiche, query, ctx, feed, aer);
            }
        } catch (final Exception e) {
            LOGGER.error("Erreur de l'export RSS : requete = " + query, e);
        }
        return feed;
    }

    private void mapFeedFromFicheUniv(final FicheUniv fiche, final String query, final ContexteUniv ctx, final SyndFeed feed, final AbstractExportRss aer) throws Exception {
        String transformedQery = query;
        for (final IPluginRecherche plugin : PluginRechercheHelper.getPlugins()) {
            if (plugin.checkClasse(fiche.getClass().getName())) {
                Map<String, Object> queryParameters = transformQueryToMap(query);
                queryParameters = plugin.taiterRecherche(ctx, queryParameters);
                transformedQery = transformMapToQuery(queryParameters);
            }
        }
        // DSI
        ctx.setCalculListeResultatsFront(true);
        fiche.traiterRequete(transformedQery);
        ctx.setCalculListeResultatsFront(false);
        final List<SyndEntry> entries = new ArrayList<>();
        while (fiche.nextItem()) {
            try {
                final SyndEntry entry = aer.getSynEntry(ctx, feed, fiche);
                entries.add(entry);
            } catch (final Exception e) {
                LOGGER.error("Erreur sur une fiche dans l'export RSS : requete = " + transformedQery + " (id_fiche = " + fiche.getIdFiche(), e);
            }
        }
        feed.setModules(aer.getFeedModules(feed));
        feed.setEntries(entries);
    }
}
