package com.univ.rss;

import java.io.Writer;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jdom.Verifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.sun.syndication.feed.module.Module;
import com.sun.syndication.feed.module.mediarss.MediaEntryModuleImpl;
import com.sun.syndication.feed.module.mediarss.types.MediaContent;
import com.sun.syndication.feed.module.mediarss.types.Metadata;
import com.sun.syndication.feed.module.mediarss.types.UrlReference;
import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedOutput;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;
import com.univ.utils.RequeteUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;
// TODO: Auto-generated Javadoc

/**
 * Cette classe abstraite fournie le traitement de base pour transfromer des {@link FicheUniv} en un flux de données type RSS ou Atom. Pour cela, elle utilise la librairie java
 * ROME qui permet la transformation dans les formats suivant :
 * <ul>
 * <li>RSS 0.92 -> rss_0.92</li>
 * <li>RSS 0.93 -> rss_0.93</li>
 * <li>RSS 0.94 -> rss_0.94</li>
 * <li>RSS 1.0 -> rss_1.0</li>
 * <li>RSS 2.0 -> rss_2.0</li>
 * <li>Atom 0.3 -> atom_0.3</li>
 * <li>Atom 1.0 -> atom_1.0</li>
 * </ul>
 *
 * @author Pierre Cosson
 */
public abstract class AbstractExportRss {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractExportRss.class);

    private final ServiceUser serviceUser;

    /** Requête correspondant à l'export demandé. */
    private String query = null;

    /** Nom de l'objet à exporter. */
    private String objectName = null;

    /** Le type de flux par defaut : RSS ou atom. */
    private String feedType;

    private final ServiceRessource serviceRessource;

    /**
     * Constructeur qui permet de choisir le type de flux parmis ceux gérés par la librairie ROME :
     * <ul>
     * <li>RSS 0.92 -> rss_0.92</li>
     * <li>RSS 0.93 -> rss_0.93</li>
     * <li>RSS 0.94 -> rss_0.94</li>
     * <li>RSS 1.0 -> rss_1.0</li>
     * <li>RSS 2.0 -> rss_2.0</li>
     * <li>Atom 0.3 -> atom_0.3</li>
     * <li>Atom 1.0 -> atom_1.0</li>
     * </ul>
     *
     * @param query
     *            the query
     * @param objectName
     *            the object name
     * @param feedType
     *            the feed type
     */
    public AbstractExportRss(final String query, final String objectName, final String feedType) {
        serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
        this.query = query;
        this.objectName = objectName;
        this.feedType = feedType;
    }

    /**
     * Export des {@link FicheUniv} en flux d'information exploitable par un agrégateur.
     *
     * @throws Exception
     *             the exception
     */
    public void export(final ContexteUniv ctx, final Writer writer) throws Exception {
        // Creation du flux de données
        final CacheFeedManager cacheFeedManager = (CacheFeedManager) ApplicationContextManager.getCoreContextBean(CacheFeedManager.ID_BEAN);
        final SyndFeed feed = cacheFeedManager.getSyndFeed(query, ctx, this);
        final SyndFeedOutput output = new SyndFeedOutput();
        output.output(feed, writer);
    }

    /**
     *
     * @return
     */
    protected ArrayList<Module> getEntryModules(final SyndFeed feed, final FicheUniv fiche) {
        return new ArrayList<>();
    }

    /**
     *
     * @return
     */
    protected ArrayList<Module> getFeedModules(final SyndFeed feed) {
        return new ArrayList<>();
    }

    /**
     * Getter sur la requête.
     *
     * @return Returns the query.
     */
    public String getQuery() {
        return this.query;
    }

    /**
     * Renvoie la description du channel correspondant à cet export rss<br />
     * Cette description peut être transmise via la requête (query), en passant le paramètre "DESCRIPTION_CHANNEL".
     *
     * @return La description du channel
     *
     * @throws Exception
     *             the exception
     */
    protected String getDescription(final OMContext ctx) throws Exception {
        return URLDecoder.decode(RequeteUtil.renvoyerParametre(query, "DESCRIPTION"), CharEncoding.DEFAULT);
    }

    /**
     * Renvoie le titre de la fiche (sera exporté dans le noeud "title").
     *
     * @param fiche
     *            Les données à exporter
     *
     * @return La description du channel
     *
     */
    protected abstract String getItemTitle(FicheUniv fiche);

    /**
     * Renvoie la description de la fiche (sera exportée dans le noeud rss "description").
     *
     * @param fiche
     *            Les données à exporter
     *
     * @return La description du channel
     *
     * @throws Exception
     *             the exception
     */
    protected abstract String getItemDescription(ContexteUniv ctx, FicheUniv fiche) throws Exception;

    /**
     * Créer une {@link SyndEntry} en fonction des valeurs contenues dans une {@link FicheUniv}.
     *
     * @param fiche
     *            La {@link FicheUniv} servant de référence à la création du {@link SyndEntry}.
     *
     * @return Un {@link SyndEntry} inititialisé.
     *
     * @throws Exception
     *             Erreur durnat la récupération des valeurs.
     */
    protected SyndEntry getSynEntry(final ContexteUniv ctx, final SyndFeed feed, final FicheUniv fiche) throws Exception {
        final SyndEntry entry = new SyndEntryImpl();
        final MetatagBean meta = MetatagUtils.lireMeta(fiche);
        final UtilisateurBean redacteur = serviceUser.getByCode(fiche.getCodeRedacteur());
        // ajout des pièces jointes dans le FLUX
        final List<MediaContent> mediaContents = getMediaContents(ctx, fiche);
        // intégration de modules
        entry.setModules(getEntryModules(feed, fiche));
        final MediaEntryModuleImpl mediaModule = new MediaEntryModuleImpl();
        mediaModule.setMediaContents(mediaContents.toArray(new MediaContent[mediaContents.size()]));
        entry.getModules().add(mediaModule);
        entry.setTitle(StringEscapeUtils.unescapeHtml4(Chaine.encodeSpecialEntities(getItemTitle(fiche))));
        entry.setLink(URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(ctx, fiche, false), ctx));
        if (!("0").equals(PropertyHelper.getCoreProperty("export.rss." + objectName + ".description"))) {
            final SyndContent description = new SyndContentImpl();
            description.setType("text/html");
            String valeurDescription = StringEscapeUtils.unescapeHtml4(Chaine.encodeSpecialEntities(getItemDescription(ctx, fiche)));
            valeurDescription = removeIllegalsXMLCharacters(valeurDescription);
            description.setValue(valeurDescription);
            entry.setDescription(description);
        }
        if (!("0").equals(PropertyHelper.getCoreProperty("export.rss." + objectName + ".pubDate")) && meta != null) {
            entry.setPublishedDate(meta.getMetaDateMiseEnLigne());
        }
        if (!("0").equals(PropertyHelper.getCoreProperty("export.rss." + objectName + ".author")) && redacteur != null) {
            entry.setAuthor(StringEscapeUtils.unescapeHtml4(Chaine.encodeSpecialEntities(redacteur.getAdresseMail() + " (" + redacteur.getPrenom() + " " + redacteur.getNom() + ")")));
        }
        return entry;
    }

    /**
     * Renvoie la liste des ressources liées à une fiche dans les champs de type "liste de fichiers"
     *
     * @param fiche
     *            la fiche pour laquelle il faut retrouver les ressources
     *
     * @return la liste des ressources liées à une fiche dans les champs de type "liste de fichiers"
     *
     */
    protected List<MediaContent> getMediaContents(final OMContext ctx, final FicheUniv fiche) {
        final List<MediaContent> mediaContents = new ArrayList<>();
        MediaContent mediaContent;
        final List<RessourceBean> fileList = serviceRessource.getFiles(fiche);
        if (fileList.size() > 0) {
            for (final RessourceBean ressource : fileList) {
                // Pour chaque ressource on créé une balise media:content (mediarss de yahoo )
                try {
                    mediaContent = new MediaContent(new UrlReference(URLResolver.getAbsoluteUrl(RessourceUtils.getUrl(ressource), ctx)));
                    mediaContent.setFileSize(RessourceUtils.getPoids(ressource) * 1024L);
                    mediaContent.setType(RessourceUtils.getFormat(ressource));
                    final Metadata md = new Metadata();
                    md.setDescription(Chaine.encodeSpecialEntities(RessourceUtils.getDescription(ressource)));
                    md.setTitle(Chaine.encodeSpecialEntities(RessourceUtils.getTitre(ressource)));
                    mediaContent.setMetadata(md);
                    mediaContents.add(mediaContent);
                } catch (final URISyntaxException e) {
                    LOG.error("impossible de parser l'url de la ressource", e);
                }
            }
        }
        return mediaContents;
    }

    /**
     * Enleve tous les characteres d'une chaine qui ne sont pas des caractères XML valides, y compris les caractères unicodes spéciaux comme u0001 (contrairement à
     * StringEscapeUtils.escapeXml par exemple)
     *
     * @param text
     *            Le texte sur lequel on veut enlever les caractères spéciaux
     * @return Un nouveau String représentant la chaine sans caractère spéciaux
     */
    private String removeIllegalsXMLCharacters(final String text) {
        final StringBuilder result = new StringBuilder();
        final char[] chars = text.toCharArray();
        for (final char c : chars) {
            if (Verifier.isXMLCharacter(c)) {
                result.append(c);
            }
        }
        return result.toString();
    }

    public String getObjectName() {
        return objectName;
    }

    public String getFeedType() {
        return feedType;
    }

    public void setFeedType(final String type) {
        this.feedType = type;
    }

    /**
     * Positionne les propriétés du flux : title (ctx.getInfosSite().getIntitule()) link (urlSite) description language : fr encoding : utf-8
     */
    protected void setFeedProperties(final OMContext ctx, final SyndFeed feed) throws Exception {
        // Creation du flux
        feed.setFeedType(getFeedType());
        final String urlSite = URLResolver.getAbsoluteUrl("", ctx);
        feed.setTitle(StringEscapeUtils.unescapeHtml4(Chaine.encodeSpecialEntities(ctx.getInfosSite().getIntitule())));
        feed.setLink(urlSite);
        feed.setDescription(StringEscapeUtils.unescapeHtml4(Chaine.encodeSpecialEntities(getDescription(ctx))));
        feed.setLanguage("fr");
        feed.setEncoding(CharEncoding.DEFAULT);
    }
}
