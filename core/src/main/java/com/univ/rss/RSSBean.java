package com.univ.rss;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;

/**
 * Bean contenant les informations d'un élément d'un flux RSS.
 */
public class RSSBean {

    /**
     * liste de tag media:content (yahoo mediarss) du flux rss
     */
    private final List<RSSMediaContentBean> mediaContentList = new ArrayList<>();

    /** The title. */
    private String title = "";

    /** The description. */
    private String description = "";

    /** The link. */
    private String link = "";

    /** The date. */
    private Date date = null;

    /** The creator. */
    private String creator = "";

    /**
     * Gets the description.
     *
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the link.
     *
     * @return String
     */
    public String getLink() {
        return link;
    }

    /**
     * Gets the title.
     *
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            String
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the link.
     *
     * @param link
     *            String
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * Sets the title.
     *
     * @param title
     *            String
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the date.
     *
     * @return Returns the date.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date
     *            The date to set.
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets the creator.
     *
     * @return Returns the creator.
     */
    public String getCreator() {
        return creator;
    }

    /**
     * Sets the creator.
     *
     * @param creator
     *            The creator to set.
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return la liste de tag media:content (yahoo mediarss) du flux rss
     */
    public List<RSSMediaContentBean> getMediaContentList() {
        return mediaContentList;
    }

    public void addAllMedia(List<RSSMediaContentBean> list) {
        Collection<String> titles = getMediaTitleAlreadyAdded();
        for (RSSMediaContentBean mediaContentBean : list) {
            if (!titles.contains(mediaContentBean.getTitre())) {
                mediaContentList.add(mediaContentBean);
            }
        }
    }

    public void addMedia(RSSMediaContentBean media) {
        Collection<String> titles = getMediaTitleAlreadyAdded();
        if (!titles.contains(media.getTitre())) {
            mediaContentList.add(media);
        }
    }

    @SuppressWarnings("unchecked")
    private Collection<String> getMediaTitleAlreadyAdded() {
        return CollectionUtils.collect(mediaContentList, new Transformer() {

            @Override
            public String transform(final Object o) {
                return ((RSSMediaContentBean)o).getTitre();
            }
        });
    }
}
