package com.univ.rss.impl.rome;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.PropertyHelper;
import com.sun.syndication.feed.module.mediarss.MediaEntryModule;
import com.sun.syndication.feed.module.mediarss.MediaModule;
import com.sun.syndication.feed.module.mediarss.types.MediaContent;
import com.sun.syndication.feed.module.mediarss.types.MediaGroup;
import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.fetcher.FeedFetcher;
import com.sun.syndication.fetcher.FetcherException;
import com.sun.syndication.fetcher.impl.FeedFetcherCache;
import com.sun.syndication.fetcher.impl.LinkedHashMapFeedInfoCache;
import com.sun.syndication.io.FeedException;
import com.univ.rss.AbstractRequeteRSS;
import com.univ.rss.RSSBean;
import com.univ.rss.RSSMediaContentBean;
import com.univ.rss.RequeteMultiRSS;
import com.univ.utils.Chaine;

/**
 * Implémentation d'une requete RSS via rome-fetcher
 *
 * @author alexandre.garbe
 *
 */
public class RequeteRSSImplRome extends AbstractRequeteRSS {

    /**
     * Le logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RequeteRSSImplRome.class);

    /**
     * Le fetcher rome, chargé de rappatrié un flux rss.
     */
    private final FeedFetcher fetcher;

    /**
     * Un constructeur.
     *
     * @param requeteMulti
     *            Référence vers le gestionnaire de flux rss
     * @param maxReponses
     *            nombre de réponse max à renvoyer
     * @param url
     *            Url du flux rss a récupérer
     */
    public RequeteRSSImplRome(final RequeteMultiRSS requeteMulti, final int maxReponses, final String url) {
        super(requeteMulti, maxReponses, url);
        // cache rss
        final FeedFetcherCache feedInfoCache = LinkedHashMapFeedInfoCache.getInstance();
        // définition du nombre d'entrées maximum définies dans le cache
        // Pour chaque thread crée on spécifie un timeout 2s par defaut
        int nb = 10;
        if (PropertyHelper.getCoreProperty("import_rss.cache.maxentries") != null) {
            try {
                nb = Integer.valueOf(PropertyHelper.getCoreProperty("import_rss.cache.maxentries"));
            } catch (final NumberFormatException e) {
                LOGGER.debug("bad property values for \"import_rss.cache.maxentries\"", e);
            }
        }
        ((LinkedHashMapFeedInfoCache) feedInfoCache).setMaxEntries(nb);
        fetcher = new HttpURLFeedFetcherProxy(feedInfoCache);
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        try {
            final URL url = new URL(getUrl());
            testUrlConnection(url);
            final SyndFeed feed = fetcher.retrieveFeed(url);
            getRequeteMulti().setTitre(feed.getTitle());
            getRequeteMulti().setDescription(feed.getDescription());
            int cptReadEntries = 0;
            for (final Iterator<SyndEntry> itEntries = feed.getEntries().iterator(); itEntries.hasNext() && cptReadEntries < getMaxReponses();) {
                final RSSBean rssBean = getRSSBean(itEntries.next());
                getRequeteMulti().ajouterResultat(rssBean);
                cptReadEntries++;
            }
        } catch (final IllegalArgumentException | FetcherException | IOException | FeedException e) {
            LOGGER.debug("an error occured while querying for rss feed", e);
            LOGGER.error(e.getMessage() + " sur l'URL : " + getUrl());
        }
    }

    public void testUrlConnection(final URL url) throws IOException {
        HttpURLConnection urlConn = null;
        try {
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.connect();
        } finally {
            if (urlConn != null) {
                urlConn.disconnect();
            }
        }
    }

    /**
     * retourne une instance de com.univ.rss.RSSBean à partir d'une instance de com.sun.syndication.feed.synd.SyndEntry.
     *
     * @param syndEntry
     *            L'item rss d'entrée
     * @return Une instance de RSSBean
     */
    private RSSBean getRSSBean(final SyndEntry syndEntry) {
        final RSSBean result = new RSSBean();
        result.setCreator(syndEntry.getAuthor());
        result.setDate(syndEntry.getPublishedDate());
        if (syndEntry.getDescription() != null) {
            result.setDescription(Chaine.encodeSpecialEntities(syndEntry.getDescription().getValue()));
        }
        result.setLink(syndEntry.getLink());
        result.setTitle(Chaine.encodeSpecialEntities(syndEntry.getTitle()));
        // traitement des tags <enclosure>
        result.addAllMedia(getRssMediaContentFromEnclosures(syndEntry));
        // traitement des éventuels tags Media:entry
        final MediaEntryModule mediaEntryModule = (MediaEntryModule) syndEntry.getModule(MediaModule.URI);
        result.addAllMedia(getRssMediaContentFromMediaEntryModule(mediaEntryModule));
        return result;
    }

    /**
     * Retourne une liste d'instance de RSSMediaContentBean décrivant les média contenus dans le module rss mediaEntry en paramètre.
     *
     * @param mediaEntryModule
     *            le module contenant les tags media:entry du flux rss
     * @return une liste d'objets RSSMediaContentBean
     */
    private List<RSSMediaContentBean> getRssMediaContentFromMediaEntryModule(final MediaEntryModule mediaEntryModule) {
        final List<RSSMediaContentBean> results = new ArrayList<>();
        if (mediaEntryModule != null) {
            if (mediaEntryModule.getMediaContents() != null) {
                for (int i = 0; i < mediaEntryModule.getMediaContents().length; i++) {
                    final MediaContent content = mediaEntryModule.getMediaContents()[i];
                    try {
                        results.add(getRSSMediaContent(content));
                    } catch (final MalformedURLException e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                }
            }
            if (mediaEntryModule.getMediaGroups() != null) {
                for (int i = 0; i < mediaEntryModule.getMediaGroups().length; i++) {
                    final MediaGroup group = mediaEntryModule.getMediaGroups()[i];
                    if (group.getContents() != null) {
                        for (int j = 0; j < group.getContents().length; j++) {
                            final MediaContent content = group.getContents()[j];
                            try {
                                results.add(getRSSMediaContent(content));
                            } catch (final MalformedURLException e) {
                                LOGGER.error(e.getMessage(), e);
                            }
                        }
                    }
                }
            }
        }
        return results;
    }

    /**
     * Retourne une liste d'instance de RSSMediaContentBean décrivant les média contenus dans la liste de balises enclosures de l'entrée rss passée en paramètres.
     *
     * @param syndEntry
     *            L'entrée rss contenant éventuellement des enclosures
     * @return une liste d'objets RSSMediaContentBean *
     */
    private List<RSSMediaContentBean> getRssMediaContentFromEnclosures(final SyndEntry syndEntry) {
        final List<RSSMediaContentBean> results = new ArrayList<>();
        if (syndEntry.getEnclosures() != null) {
            for (final SyndEnclosure enclosure : (Iterable<SyndEnclosure>) syndEntry.getEnclosures()) {
                try {
                    String titre = "media";
                    if (enclosure.getUrl() != null) {
                        titre = StringUtils.substringAfterLast(enclosure.getUrl(), "/");
                        titre = StringUtils.substringBefore(titre, "?");
                    }
                    results.add(getRSSMediaContent(titre, null, null, null, enclosure.getType(), enclosure.getUrl()));
                } catch (final MalformedURLException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
        return results;
    }

    /**
     * Transforme une instance de com.sun.syndication.feed.module.mediarss.types.MediaContent en com.univ.rss.RSSMediaContentBean
     *
     * @param content
     *            l'objet de type MediaContent
     * @return L'instance de com.univ.rss.RSSMediaContentBean
     * @throws MalformedURLException
     *             Si l'url issue de MediaContent est invalide
     */
    private RSSMediaContentBean getRSSMediaContent(final MediaContent content) throws MalformedURLException {
        String titre = "media";
        String description = null;
        String url = null;
        if (content.getMetadata() != null) {
            description = content.getMetadata().getDescription();
        }
        if (content.getReference() != null) {
            url = content.getReference().toString();
        }
        if (url != null) {
            titre = StringUtils.substringAfterLast(url, "/");
            titre = StringUtils.substringBefore(titre, "?");
        }
        return getRSSMediaContent(titre, description, content.getWidth(), content.getHeight(), content.getType(), url);
    }

    /**
     * Retourne une instance de RSSMEdiaContentBean(Description d'un média d'un flux rss) a partir des informations en paramètre
     *
     * @param description
     *            La description du média (ou null)
     * @param width
     *            La largeur du média (ou null)
     * @param height
     *            La hauteur du média (ou null)
     * @param type
     *            Le type mime du média
     * @param url
     *            L'url du média
     * @return
     * @throws MalformedURLException
     */
    private RSSMediaContentBean getRSSMediaContent(final String titre, final String description, final Integer width, final Integer height, final String type, final String url) throws MalformedURLException {
        final RSSMediaContentBean rssMediaContentBean = new RSSMediaContentBean();
        rssMediaContentBean.setTitre(titre);
        rssMediaContentBean.setDescription(description);
        rssMediaContentBean.setHeight(height);
        rssMediaContentBean.setWidth(width);
        rssMediaContentBean.setMimeType(type);
        if (url != null) {
            rssMediaContentBean.setUrl(new URL(url));
        }
        return rssMediaContentBean;
    }
}
