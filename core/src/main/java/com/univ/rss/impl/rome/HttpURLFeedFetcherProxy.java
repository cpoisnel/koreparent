package com.univ.rss.impl.rome;

import java.net.URLConnection;

import com.sun.syndication.fetcher.impl.FeedFetcherCache;
import com.sun.syndication.fetcher.impl.HttpURLFeedFetcher;
import com.sun.syndication.fetcher.impl.SyndFeedInfo;

/**
 *
 * @author emmanuel.clisson
 *
 */
public class HttpURLFeedFetcherProxy extends HttpURLFeedFetcher {

    public HttpURLFeedFetcherProxy(FeedFetcherCache feedFetcherCache) {
        super(feedFetcherCache);
    }

    @Override
    protected void setRequestHeaders(URLConnection urlConnection, SyndFeedInfo syndFeedInfo) {
        super.setRequestHeaders(urlConnection, syndFeedInfo);
    }
}
