package com.univ.rss.impl.kosmos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.rss.AbstractRequeteRSS;
import com.univ.rss.RSSBean;
import com.univ.rss.RequeteMultiRSS;
// TODO: Auto-generated Javadoc

/**
 * Execute une requête et renvoie un flux rss.
 */
public class RequeteRSS extends AbstractRequeteRSS {

    private static final Logger LOG = LoggerFactory.getLogger(RequeteRSS.class);

    /** Message d'erreur sur parsing des dates. */
    private String errorMessage = "";

    /** Exception accompagnant l'erreur sur le parsing des dates. */
    private Exception eWithMessage = null;

    /**
     * Constructeur.
     *
     * @param requeteMulti
     *            the requete multi
     * @param maxReponses
     *            the max reponses
     * @param url
     *            the url
     */
    public RequeteRSS(final RequeteMultiRSS requeteMulti, final int maxReponses, final String url) {
        super(requeteMulti, maxReponses, url);
    }

    /**
     * Lance le thread.
     */
    @Override
    public void run() {
        LOG.debug("RequeteRSS : thread lancé, url = " + getUrl());
        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            final DocumentBuilder builder = factory.newDocumentBuilder();
            builder.setEntityResolver(new EntityResolver() {

                @Override
                public InputSource resolveEntity(final java.lang.String publicId, final java.lang.String systemId) throws SAXException, java.io.IOException {
                    // on desactive la validation de la DTD 0.91 qui n'est plus hebergée chez netscape
                    if ("-//Netscape Communications//DTD RSS 0.91//EN".equals(publicId)) {
                        return new InputSource(new ByteArrayInputStream(("<?xml version='1.0' encoding='" + CharEncoding.DEFAULT + "'?>").getBytes()));
                    } else {
                        return null;
                    }
                }
            });
            //patch alice
            final URL zurl = new URL(getUrl());
            final URLConnection urlConnection = zurl.openConnection();
            final String fileName = WebAppUtil.getWorkDefaultPath() + File.separator + "rss" + System.currentTimeMillis();
            final File fileRss = new File(fileName);
            try (final BufferedReader flux = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                final BufferedWriter out = new BufferedWriter(new FileWriter(fileName));) {
                String ligne;
                while ((ligne = flux.readLine()) != null) {
                    out.write(ligne.replaceAll("&#x2019;", "'"));
                }
            }
            final Document document = builder.parse(fileRss);
            if (!fileRss.delete()) {
                LOG.debug("unable to delete file " + fileName);
            }
            final Node nodeRss = document.getDocumentElement();
            if (nodeRss != null) {
                // Recherche channel
                final NodeList channels = nodeRss.getChildNodes();
                int nbResultats = 0;
                int i = 0;
                while (i < channels.getLength() && nbResultats < getMaxReponses()) {
                    final Node node = channels.item(i++);
                    if ("channel".equalsIgnoreCase(node.getNodeName())) {
                        final NodeList items = node.getChildNodes();
                        int j = 0;
                        while (j < items.getLength() && nbResultats < getMaxReponses()) {
                            final Node nodeItem = items.item(j++);
                            if ("item".equalsIgnoreCase(nodeItem.getNodeName()) && nodeItem.hasChildNodes()) {
                                // Analyse du résultat
                                getRequeteMulti().ajouterResultat(readItem(nodeItem));
                                nbResultats++;
                            }
                        }
                    } else if ("item".equalsIgnoreCase(node.getNodeName()) && node.hasChildNodes()) {
                        // Analyse du résultat
                        getRequeteMulti().ajouterResultat(readItem(node));
                        nbResultats++;
                    }
                }
                // AA : on ne loggue qu'une seule fois l'erreur sur le parsing de la date
                if (errorMessage.length() > 0 && eWithMessage != null) {
                    LOG.error(errorMessage, eWithMessage);
                } else if (errorMessage.length() > 0) {
                    LOG.error(errorMessage);
                }
                errorMessage = "";
                eWithMessage = null;
            }
        } catch (final IOException | SAXException | ParserConfigurationException e) {
            LOG.error("erreur sur la requete rss", e);
        }
        getRequeteMulti().decrementerCptRequetes();
        LOG.debug("RequeteRSS : fin d'exécution du thread");
    }

    /**
     * Lit le node et récupère un objet RSSBean contenant le contenu de l'élément.
     *
     * @param nodeItem
     *            Le node
     *
     * @return the RSS bean
     */
    private RSSBean readItem(final Node nodeItem) {
        final RSSBean rss = new RSSBean();
        Node nextFeuille = nodeItem.getFirstChild();
        while (nextFeuille != null) {
            if (nextFeuille.getFirstChild() != null) {
                if (nextFeuille.getNodeName().compareTo("title") == 0) {
                    rss.setTitle(StringEscapeUtils.escapeHtml4(nextFeuille.getFirstChild().getNodeValue().trim()));
                } else if (nextFeuille.getNodeName().compareTo("link") == 0) {
                    rss.setLink(nextFeuille.getFirstChild().getNodeValue().trim());
                } else if (nextFeuille.getNodeName().compareTo("description") == 0) {
                    rss.setDescription(nextFeuille.getFirstChild().getNodeValue().trim());
                } else if (nextFeuille.getNodeName().contains("creator")) {
                    rss.setCreator(nextFeuille.getFirstChild().getNodeValue().trim());
                } else if (nextFeuille.getNodeName().contains("date")) {
                    // Parsing de la date
                    String date = nextFeuille.getFirstChild().getNodeValue().trim();
                    try {
                        if (date.length() > 19) {
                            // exemple (EDF' : "2004-07-17T06:02:23+01:00"
                            // "yyyy-MM-ddTHH:mm:ss.S
                            // On supprime le time zone et on force à blanc le
                            // séparateur date heure (qui vaut 'T pour EDF)
                            date = date.substring(0, 10) + " " + date.substring(11, 19);
                            final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
                            rss.setDate(df.parse(date));
                        } else if (date.contains("/") && date.length() == 10) {
                            //format 29/04/2008 ou 2008/04/01
                            final SimpleDateFormat df;
                            if (date.indexOf("/") == 4) {
                                df = new SimpleDateFormat("yyyy/MM/dd");
                            } else {
                                df = new SimpleDateFormat("dd/MM/yyyy");
                            }
                            rss.setDate(df.parse(date));
                        } else if (date.length() > 0) {
                            errorMessage = "Format de date non interprété pour ce flux (" + date + ")";
                        }
                    } catch (final ParseException e) {
                        errorMessage = "Erreur lors de la lecture de la date pour ce flux : " + date;
                        eWithMessage = e;
                    }
                } else if (nextFeuille.getNodeName().contains("pubDate")) {
                    // Parsing de la date
                    final String date = nextFeuille.getFirstChild().getNodeValue().trim();
                    try {
                        if (date.length() > 19) {
                            rss.setDate(new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US).parse(date));
                        } else if (date.contains("/") && date.length() == 10) {
                            //format 29/04/2008 ou 2008/04/01
                            final SimpleDateFormat df;
                            if (date.indexOf("/") == 4) {
                                df = new SimpleDateFormat("yyyy/MM/dd");
                            } else {
                                df = new SimpleDateFormat("dd/MM/yyyy");
                            }
                            rss.setDate(df.parse(date));
                        } else if (date.length() > 0) {
                            errorMessage = "Format de date (pubDate) non interprété pour ce flux (" + date + ")";
                        }
                    } catch (final ParseException e) {
                        errorMessage = "Erreur lors de la lecture de la date (pubDate) pour ce flux : " + date;
                        eWithMessage = e;
                    }
                }
            }
            nextFeuille = nextFeuille.getNextSibling();
        }
        return rss;
    }
}
