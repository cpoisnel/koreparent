package com.univ.rss;

import java.lang.reflect.Method;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.om.Article;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.RequeteUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;
// TODO: Auto-generated Javadoc

/**
 * Gestionnaire d'export RSS sur des articles.
 *
 * @author fred
 */
public class ExportRssArticle extends AbstractExportRss {

    /**
     * Instantiates a new export rss article.
     *
     * @param query
     *            the query
     * @param objectName
     *            the object name
     * @param feedType
     *            the feed type
     */
    public ExportRssArticle(final String query, final String objectName, final String feedType) {
        super(query, objectName, feedType);
    }

    /**
     * Renvoie la description du channel correspondant à cet export rss.
     *
     * @return La description du channel
     *
     * @throws Exception
     *             the exception
     */
    @Override
    protected String getDescription(final OMContext ctx) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        // on recupere la description dans la requete
        String description = super.getDescription(ctx);
        if (description.length() == 0) // sinon
        {
            final String thematique = RequeteUtil.renvoyerParametre(getQuery(), "THEMATIQUE");
            if (thematique.length() > 0) {
                description += "\"" + LabelUtils.getLibelle("04", thematique, ctx.getLocale()) + "\" ";
            }
            final String codeRattachement = RequeteUtil.renvoyerParametre(getQuery(), "CODE_RATTACHEMENT");
            if (codeRattachement.length() > 0) {
                description += "[" + serviceStructure.getAttachementLabel(codeRattachement, "0", true).replaceAll("<br />", " / ") + "]";
            }
            if (description.length() == 0) {
                description = "Les 10 derniers articles";
            } else {
                description = "Sélection " + description;
            }
        }
        return description;
    }

    /**
     * Renvoie le titre de l'actu.
     *
     * @param fiche
     *            L'actu à exporter
     *
     * @return Le titre
     *
     */
    @Override
    protected String getItemTitle(final FicheUniv fiche) {
        return ((Article) fiche).getTitre();
    }

    /**
     * Renvoie la description de l'actu.
     *
     * @param fiche
     *            L'actu à exporter
     *
     * @return La description
     *
     * @throws Exception
     *             the exception
     */
    @Override
    protected String getItemDescription(final ContexteUniv ctx, final FicheUniv fiche) throws Exception {
        String description = "";
        final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
        final RessourceBean ressourceBean = serviceRessource.getFile(fiche);
        if (ressourceBean != null) {
            final String urlVignette = URLResolver.getAbsoluteUrl(RessourceUtils.getUrlVignette(ressourceBean), ctx);
            description = "<img src=\"" + urlVignette + "\" width=\"150px\" /><br />";
        }
        final String optionExport = PropertyHelper.getCoreProperty("export_rss.article.description");
        if (optionExport == null || optionExport.length() == 0) {
            description += ((Article) fiche).getContenu();
        } else {
            try {
                // Récupération de la liste de valeurs
                final String nomAccesseurGET = "get" + optionExport.substring(0, 1).toUpperCase() + optionExport.substring(1);
                // ex méthode getParcourss() sur l'objet Formation
                final Method accesseurGET = fiche.getClass().getMethod(nomAccesseurGET);
                final Object o[] = new Object[0];
                description = (String) accesseurGET.invoke(fiche, o);
            } catch (final ReflectiveOperationException | IllegalArgumentException e) {
                throw new ErreurApplicative("Attribut " + optionExport + " pour la fiche Article", e);
            }
        }
        // on a besoin de cette info pour le formatage HTML
        ctx.setFicheCourante(fiche);
        return UnivWebFmt.formaterEnHTML(ctx, description);
    }
}
