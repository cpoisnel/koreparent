package com.univ.multisites.helper;

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;
import com.univ.utils.URLResolver;

/**
 * Classe qui contient les codes des propriétés JTF liées aux {@link InfosSite} et qui permet de "parser" quelques valeurs de propriété.
 *
 * @author pierre.cosson
 *
 */
public class InfosSitePropertiesHelper {

    private static final Logger LOG = LoggerFactory.getLogger(InfosSitePropertiesHelper.class);

    public static final String PROPERTIES_SITE_PRINCIPAL = "site.principal";

    public static final String HOST = "host";

    public static final String DEBUT_PROPERTIES_SITE = "site.";

    public static final String FIN_PROPERTIES_SITE_SSO = ".sso";

    public static final String FIN_PROPERTIES_SITE_JSP_FO = ".jsp_fo";

    public static final String FIN_PROPERTIES_SITE_TEMPLATE = ".code_template";

    public static final String FIN_PROPERTIES_SITE_ALIAS = ".alias";

    public static final String FIN_PROPERTIES_SITE_URL_ACCUEIL = ".url_accueil";

    public static final String FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MODE = ".reecriture_rubrique_mode";

    public static final String FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MAX = ".reecriture_rubrique_max";

    public static final String FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MIN = ".reecriture_rubrique_min";

    public static final String FIN_PROPERTIES_SITE_RESTRICTION = ".restriction";

    public static final String FIN_PROPERTIES_SITE_RUBRIQUE = ".rubrique";

    public static final String FIN_PROPERTIES_SITE_BO_SSL_MODE = ".bo.ssl_mode";

    public static final String FIN_PROPERTIES_SITE_HTTP_ACTIONS = ".http_actions";

    public static final String FIN_PROPERTIES_SITE_HTTPS_ACTIONS = ".https_actions";

    public static final String FIN_PROPERTIES_SITE_SSL_MODE = ".ssl_mode";

    public static final String FIN_PROPERTIES_SITE_HTTPS_PORT = ".https_port";

    public static final String FIN_PROPERTIES_SITE_HTTPS_HOST = ".https_host";

    public static final String FIN_PROPERTIES_SITE_PORT = ".port";

    public static final String FIN_PROPERTIES_SITE_HOST = "." + HOST;

    public static final String FIN_PROPERTIES_SITE_INTITULE = ".intitule";

    public static final String FIN_PROPERTIES_SITE_PRINCIPAL = ".principal";

    public static final String FIN_PROPERTIES_SITE_ACTIF = ".actif";

    public static final String STRING_BOOLEAN_TRUE = "1";

    public static final String SLASH = "/";

    public static final char PROPERTIE_KEY_DELIMITEUR = '.';

    public static final char VALEUR_PROPERTIE_LISTE_DELIMITEUR = ';';

    public static final String ACTION_DECONNECTER = "DECONNECTER";

    public static final String ACTION_PRESENTER_MDP = "PRESENTER_MDP";

    public static final String ACTION_DEMANDER_MDP = "DEMANDER_MDP";

    public static final String ACTION_PERSONNALISER = "PERSONNALISER";

    public static final String ACTION_LOGIN = "LOGIN";

    /**
     * Transformer la liste d'actions en leur représentation framework, c'est à dire en "vraie" liste d'entiers.<br/>
     * <br/>
     * /!\ Cette méthode sert uniquement pour la conversion des configurations 5.0 / 5.1 sans UAS vers le nouveau modele. /!\
     * Pourquoi une {@link SortedSet}???? Je ne sais pas.
     *
     * @param actions
     *            liste des actions séparé par des ";"
     * @return La liste des actions.
     */
    public static SortedSet<Integer> transformerActionsStringEnListe(final String actions) {
        final String[] tableauActions = actions.split(String.valueOf(VALEUR_PROPERTIE_LISTE_DELIMITEUR));
        final TreeSet<Integer> listeActions = new TreeSet<>();
        for (final String action : tableauActions) {
            if (action.equals(ACTION_LOGIN)) {
                listeActions.add(URLResolver.LOGIN_FRONT);
            } else if (action.equals(ACTION_PERSONNALISER)) {
                listeActions.add(URLResolver.PERSONNALISER_FRONT);
            } else if (action.equals(ACTION_DEMANDER_MDP)) {
                listeActions.add(URLResolver.DEMANDER_MDP_FRONT);
            } else if (action.equals(ACTION_PRESENTER_MDP)) {
                listeActions.add(URLResolver.PRESENTER_MDP_FRONT);
            } else if (action.equals(ACTION_DECONNECTER)) {
                listeActions.add(URLResolver.DECONNECTER_FRONT);
            } else {
                listeActions.add(URLResolver.STANDARD_ACTION);
            }
        }
        return listeActions;
    }

    /**
     * Converti la liste d'actions (http ou https) en Set d'entier pour les ajouter au sites.
     * @param actions Une liste d'actions devant contenir des entiers
     * @return un set contenant les entiers (1/2/3/4/5) correspondant aux actions https possibles à cause de l'implémentation obsolète.
     * Si ce n'est pas des entiers, un Set vide est retourné.
     */
    public static SortedSet<Integer> transformListActionsToSet(final List<String> actions) {
        final SortedSet<Integer> result = new TreeSet<>();
        for (String currentAction : actions) {
            try {
                result.add(Integer.valueOf(currentAction));
            } catch (final NumberFormatException nfe) {
                LOG.error("unable to convert the given action to an integer", nfe);
            }
        }
        return result;
    }

    public static String getCheminDossiersProperties() {
        return WebAppUtil.getConfigurationSitesPath();
    }

    /**
     * Propriété du site et non du template
     *
     * @author olivier.camon
     *
     */
    public enum FinNomProprieteSite {
        SITE_SSO("sso"),
        JSP_FO("jsp_fo"),
        TEMPLATE("code_template"),
        ALIAS("alias"),
        URL_ACCUEIL("url_accueil"),
        REECRITURE_RUBRIQUE_MODE("reecriture_rubrique_mode"),
        REECRITURE_RUBRIQUE_MAX("reecriture_rubrique_max"),
        REECRITURE_RUBRIQUE_MIN("reecriture_rubrique_min"),
        RESTRICTION("restriction"),
        RUBRIQUE("rubrique"),
        BO_SSL_MODE("bo.ssl_mode"),
        HTTP_ACTIONS("http_actions"),
        HTTPS_ACTIONS("https_actions"),
        SSL_MODE("ssl_mode"),
        HTTPS_PORT("https_port"),
        HTTPS_HOST("https_host"),
        SITE_PORT("port"),
        HOST(InfosSitePropertiesHelper.HOST),
        INTITULE("intitule"),
        PRINCIPAL("principal"),
        ACTIF("actif");

        private String nom;

        FinNomProprieteSite(final String nom) {
            this.nom = nom;
        }

        public static boolean isProprieteSite(final String codePropriete) {
            if (StringUtils.isEmpty(codePropriete)) {
                return Boolean.FALSE;
            }
            for (final FinNomProprieteSite proprieteSite : FinNomProprieteSite.values()) {
                if (proprieteSite.getNom().equals(codePropriete)) {
                    return Boolean.TRUE;
                }
            }
            return Boolean.FALSE;
        }

        public String getNom() {
            return nom;
        }

        @Override
        public String toString() {
            return nom;
        }
    }
}
