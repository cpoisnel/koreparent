/*
 *
 */
package com.univ.multisites;

/**
 * Stockage pour tri des informations d'une fiche référencée.
 */
public class InfosFicheReferencee {

    /** The code. */
    private String code = "";

    /** The langue. */
    private String langue = "";

    /** The intitule. */
    private String intitule = "";

    /** The type. */
    private String type = "";

    /** The etat. */
    private String etat = "";

    /** The requete. */
    private String requete = "";

    /** The id. */
    private String id = "";

    /**
     * Gets the code.
     *
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            The code to set.
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the intitule.
     *
     * @return Returns the intitule.
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Sets the intitule.
     *
     * @param intitule
     *            The intitule to set.
     */
    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    /**
     * Gets the langue.
     *
     * @return Returns the langue.
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Sets the langue.
     *
     * @param langue
     *            The langue to set.
     */
    public void setLangue(final String langue) {
        this.langue = langue;
    }

    /**
     * Gets the type.
     *
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            The type to set.
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the requete.
     *
     * @return the requete
     */
    public String getRequete() {
        return requete;
    }

    /**
     * Sets the requete.
     *
     * @param requete
     *            the new requete
     */
    public void setRequete(final String requete) {
        this.requete = requete;
    }

    /**
     * Gets the etat.
     *
     * @return the etat
     */
    public String getEtat() {
        return etat;
    }

    /**
     * Sets the etat.
     *
     * @param etat
     *            the new etat
     */
    public void setEtat(final String etat) {
        this.etat = etat;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final String id) {
        this.id = id;
    }
}
