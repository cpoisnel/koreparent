package com.univ.multisites;

import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.multisites.service.ServiceInfosSite;
import com.univ.multisites.service.ServiceInfosSiteFactory;
import com.univ.objetspartages.om.FicheUniv;

/**
 * Façade pour accéder au site
 * @deprecated Utiliser le service {@link com.univ.multisites.service.ServiceInfosSite}
 */
@Deprecated
public class Site {

    private static final Logger LOG = LoggerFactory.getLogger(Site.class);

    private static ServiceInfosSite getServiceInfosSite() {
        return ServiceInfosSiteFactory.getServiceInfosSite();
    }

    /**
     * @deprecated Utiliser {@link com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus#getSitesList}
     * @return une {@link java.util.Map} <alias du site, {@link InfosSite}>.
     */
    @Deprecated
    public static Map<String, InfosSite> getListeInfosSites() {
        return getServiceInfosSite().getSitesList();
    }

    /**
     * @deprecated Utilisez {@link com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus#getPrincipalSite}
     * @return l'{@link InfosSite} dont l'attribut "isSitePrincipal" est à "true", le premier infosSite de la liste sinon.
     */
    @Deprecated
    public static InfosSite getSitePrincipal() {
        return getServiceInfosSite().getPrincipalSite();
    }

    /**
     * @deprecated Utilisez {@link com.univ.multisites.service.ServiceInfosSite#getSiteBySection}
     * @param rubrique le code de la rubrique à rechercher sous la forme d'une {@link String}
     * @return l'{@link InfosSite} correspondant s'il a été trouvé, null sinon.
     */
    @Deprecated
    public static InfosSite renvoyerItemSiteParRubrique(final String rubrique) {
        return getServiceInfosSite().getSiteBySection(rubrique);
    }

    /**
     * @deprecated Utiliser {@link com.univ.multisites.service.ServiceInfosSite#getInfosSite}
     * @param alias le code du site à récupérer.
     * @return l'{@link InfosSite} s'il a été trouvé, null sinon
     */
    @Deprecated
    public static InfosSite renvoyerItemSite(final String alias) {
        try {
            return getServiceInfosSite().getInfosSite(alias);
        }catch(Exception e) {
            LOG.error(String.format("Une erreur est survenue lors de la récupération du site correspondant au code %s", alias), e);
        }
        return null;
    }

    /**
     * Récupération d'un site stocké en mémoire.
     *
     * @param host
     *            the host
     *
     * @return the infos site
     * @deprecated Utiliser {@link com.univ.multisites.service.ServiceInfosSite#getSiteByHost}
     */
    @Deprecated
    public static InfosSite renvoyerItemSiteParHost(final String host) {
        return getServiceInfosSite().getSiteByHost(host);
    }

    /**
     * Récupère le site de la rubrique dont le code est fourni en paramètre.
     *
     *
     * @param codeRubrique
     *            Le code de la rubrique dont on souhaite connaitre le site
     * @param renvoyerSiteGlobal
     *            si aucun site n'est trouvé, doit on renvoyer le site principal?
     *
     * @return l'InfosSite de la rubrique ou null si le code est vide ou que renvoyerSiteGlobal est à false et que le site n'est pas trouvé ou le site principal si
     *         renvoyerSiteGlobal est à true
     * @deprecated Utiliser {@link com.univ.multisites.service.ServiceInfosSite#determineSite}
     */
    @Deprecated
    public static InfosSite determinerSiteRubrique(final String codeRubrique, final boolean renvoyerSiteGlobal) {
        return getServiceInfosSite().determineSite(codeRubrique, renvoyerSiteGlobal);
    }

    /**
     * Renvoie le site d'affichage de la fiche si elle ne s'affiche pas dans le site courant.
     *
     * @param fiche
     *            the fiche
     *
     * @return the site affichage
     * @deprecated Utiliser {@link com.univ.multisites.service.ServiceInfosSite#displaySite}
     */
    @Deprecated
    public static InfosSite getSiteAffichage(final FicheUniv fiche) {
        return getServiceInfosSite().displaySite(fiche);
    }

    /**
     * Determiner site rubrique.
     *
     * @param codeRubrique
     *            Le code de la rubrique dont on souhaite connaitre le site
     *
     * @return l'InfosSite de la rubrique ou null si le code est vide ou que renvoyerSiteGlobal est à false et que le site n'est pas trouvé ou le site principal si
     *         renvoyerSiteGlobal est à true
     * @deprecated Utiliser {@link com.univ.multisites.service.ServiceInfosSite#determineSite(String, boolean)}
     */
    @Deprecated
    public static InfosSite determinerSiteRubrique(final String codeRubrique) {
        return getServiceInfosSite().determineSite(codeRubrique, true);
    }

    /**
     * liste des hosts et alias des sites actifs
     *
     * @return la liste des hosts et alias
     * @deprecated Utiliser {@link com.univ.multisites.service.ServiceInfosSite#getFullAliasHostList}
     */
    @Deprecated
    public static ArrayList<String> getListeCompleteHostsEtAlias() {
        final ArrayList<String> result = new ArrayList<>();
        result.addAll(getServiceInfosSite().getFullAliasHostList());
        return result;
    }
}
