package com.univ.multisites.dao.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.PropertyConverter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.dao.InfosSiteDao;
import com.univ.multisites.helper.InfosSitePropertiesHelper;
import com.univ.multisites.helper.InfosSitePropertiesHelper.FinNomProprieteSite;

import static com.univ.multisites.InfosSite.MODEREECRITURERUBRIQUE_DEFAUT;
import static com.univ.multisites.InfosSite.NIVEAUMAXREECRITURERUBRIQUE_DEFAUT;
import static com.univ.multisites.InfosSite.NIVEAUMINREECRITURERUBRIQUE_DEFAUT;
import static com.univ.multisites.InfosSite.RESTRICTION_DEFAUT;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.DEBUT_PROPERTIES_SITE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_ACTIF;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_ALIAS;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_BO_SSL_MODE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HOST;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HTTPS_ACTIONS;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HTTPS_HOST;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HTTPS_PORT;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HTTP_ACTIONS;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_INTITULE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_JSP_FO;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_PORT;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_PRINCIPAL;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MAX;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MIN;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MODE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_RESTRICTION;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_RUBRIQUE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_SSL_MODE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_SSO;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_TEMPLATE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_URL_ACCUEIL;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.SLASH;

/**
 * DAO qui permet de LIRE (et uniquement) les {@link InfosSite} depuis les proerties chargée dans l'application.<br/>
 * <br/>
 *
 * <br/>
 * <strong>INFO sur le format des properties</strong><br/>
 * Déclaration du site principal :
 * <ul>
 * <li>site.principal=[ID]</li>
 * </ul>
 * Paramétrage pour chaque site dans le jtf :
 * <ul>
 * <li>site.[ID].intitule=[INTITULE]</li>
 * <li>
 * site.[ID].host=[HTTP_HOSTNAME]</li>
 * <li>
 * site.[ID].port=[HTTP_PORT]</li>
 * <li>
 * site.[ID].https_host=[HTTPS_HOSTNAME]</li>
 * <li>
 * site.[ID].https_port=[HTTPS_PORT]</li>
 * <li>
 * site.[ID].ssl_mode=[SSL_MODE]</li>
 * <li>
 * site.[ID].http_actions=[HTTP_ACTIONS]</li>
 * <li>
 * site.[ID].https_actions=[HTTPS_ACTIONS]</li>
 * <li>
 * site.[ID].bo.ssl_mode=[BO_SSL_MODE]</li>
 * <li>
 * site.[ID].rubrique=[CODE_RUBRIQUE]</li>
 * <li>
 * site.[ID].alias=[ALIAS]</li>
 * </ul>
 * où :
 * <ul>
 * <li>[ID] est l'identifiant du site</li>
 * <li>
 * [INTITULE] est le nom du site</li>
 * <li>
 * [HTTP_HOSTNAME] est le nom du virtual host pour l'appli en http</li>
 * <li>
 * [HTTP_PORT] est le numéro de port pour l'appli en http (paramètre optionnel, par défaut = 80)</li>
 * <li>
 * [HTTPS_HOSTNAME] est le nom du virtual host pour l'appli en https (paramètre optionnel, par défaut = [HTTP_HOSTNAME])</li>
 * <li>
 * [HTTPS_PORT] est le numéro de port pour l'appli en https (paramètre optionnel, par défaut = 443)</li>
 * <li>
 * [SSL_MODE] est le mode de fonctionnement pour la détermination des URLs (paramètre optionnel, par défaut = 0, valeurs possibles : 0 pour mode non contextuel, 1 pour mode
 * contextuel)</li>
 * <li>
 * [HTTP_ACTIONS] est la liste des actions que l'on force en http, utile uniquement en mode contextuel (paramètre optionnel, par défaut = "", valeurs possibles : "" | "DECONNECTER"
 * )</li>
 * <li>
 * [HTTPS_ACTIONS] est la liste des actions que l'on force en https, séparées par un ';' (paramètre optionnel, par défaut = "", valeurs possibles : "" | "LOGIN" | "PERSONNALISER" |
 * "DEMANDER_MDP" | "PRESENTER_MDP" )</li>
 * <li>
 * [BO_SSL_MODE] est le mode de fonctionnement SSL du back-office, utile uniquement pour CAS en mode proxy (paramètre optionnel, par défaut = 0, valeurs possibles : 0 pour http, 1
 * pour https)</li>
 * <li>
 * [CODE_RUBRIQUE] est le code de la rubrique mère du site (paramètre optionnel, par défaut = "")</li>
 * <li>
 * [ALIAS] est la liste des différents alias du host principal séparés par des points virgules</li>
 * </ul>
 *
 * @author pierre.cosson
 *
 */
public class InfosSiteDaoProperties implements InfosSiteDao {

    private static final Logger LOG = LoggerFactory.getLogger(InfosSiteDaoProperties.class);

    private static final Object LOCK = new Object();

    private static PropertiesConfiguration getPropertiesConfiguration(final String code) throws ConfigurationException {
        PropertiesConfiguration propsConfig = null;
        synchronized (LOCK) {
            AbstractConfiguration.setDefaultListDelimiter(InfosSitePropertiesHelper.VALEUR_PROPERTIE_LISTE_DELIMITEUR);
            propsConfig = new PropertiesConfiguration(new File(InfosSitePropertiesHelper.getCheminDossiersProperties() + code + ".properties"));
            propsConfig.setEncoding(CharEncoding.DEFAULT);
        }
        return propsConfig;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.multisites.dao.InfosSiteDao#getInfosSite(java.lang.String)
     */
    @SuppressWarnings("deprecation")
    @Override
    public InfosSite getInfosSite(final String code) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        final PropertiesConfiguration propsConfig = getPropertiesConfiguration(code);
        if (propsConfig.isEmpty()) {
            throw new ErreurDonneeNonTrouve("Aucun site ne correspond au code " + code);
        }
        final String proprieteSite = DEBUT_PROPERTIES_SITE + code;
        final String intitule = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_INTITULE);
        if (StringUtils.isNotEmpty(intitule)) {
            infosSite.setIntitule(intitule);
        }
        final String httpHost = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_HOST);
        if (StringUtils.isNotEmpty(httpHost)) {
            infosSite.setHttpHostname(httpHost);
        }
        final String httpsHost = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_HTTPS_HOST);
        if (StringUtils.isNotEmpty(httpsHost)) {
            infosSite.setHttpsHostname(httpsHost);
        }
        @SuppressWarnings("unchecked")
        final List<String> listeAliasHostsSite = propsConfig.getList(proprieteSite + FIN_PROPERTIES_SITE_ALIAS);
        if (CollectionUtils.isNotEmpty(listeAliasHostsSite)) {
            infosSite.setListeHostAlias(new HashSet<>(listeAliasHostsSite));
        }
        final List<String> httpAction = propsConfig.getList(proprieteSite + FIN_PROPERTIES_SITE_HTTP_ACTIONS);
        if (CollectionUtils.isNotEmpty(httpAction)) {
            final SortedSet<Integer> listeActions = InfosSitePropertiesHelper.transformListActionsToSet(httpAction);
            infosSite.setHttpActions(listeActions);
        }
        final List<String> httpsAction = propsConfig.getList(proprieteSite + FIN_PROPERTIES_SITE_HTTPS_ACTIONS);
        if (CollectionUtils.isNotEmpty(httpsAction)) {
            final SortedSet<Integer> listeActions = InfosSitePropertiesHelper.transformListActionsToSet(httpsAction);
            infosSite.setHttpsActions(listeActions);
        }
        final String siteRubrique = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_RUBRIQUE);
        if (StringUtils.isNotEmpty(siteRubrique)) {
            infosSite.setCodeRubrique(siteRubrique);
        } else {
            infosSite.setCodeRubrique(StringUtils.EMPTY);
        }
        final String siteURLAccueil = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_URL_ACCUEIL);
        if (StringUtils.isNotEmpty(siteURLAccueil)) {
            infosSite.setUrlAccueil(siteURLAccueil);
        }
        final String jspFO = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_JSP_FO);
        if (StringUtils.isNotEmpty(jspFO)) {
            infosSite.setJspFo(StringUtils.removeEnd(jspFO, SLASH));
        }
        final String codeTemplate = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_TEMPLATE);
        if (StringUtils.isNotEmpty(codeTemplate)) {
            infosSite.setCodeTemplate(codeTemplate);
        }
        infosSite.setAlias(code);
        infosSite.setHttpPort(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_PORT, InfosSite.HTTPPORT_DEFAUT));
        infosSite.setHttpsPort(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_HTTPS_PORT, InfosSite.HTTPSPORT_DEFAUT));
        infosSite.setSslMode(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_SSL_MODE, InfosSite.SSLMODE_DEFAUT));
        infosSite.setBoSslMode(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_BO_SSL_MODE, InfosSite.BOSSLMODE_DEFAUT));
        infosSite.setNiveauMinReecritureRubrique(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MIN, NIVEAUMINREECRITURERUBRIQUE_DEFAUT));
        infosSite.setNiveauMaxReecritureRubrique(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MAX, NIVEAUMAXREECRITURERUBRIQUE_DEFAUT));
        infosSite.setModeReecritureRubrique(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MODE, MODEREECRITURERUBRIQUE_DEFAUT));
        infosSite.setRestriction(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_RESTRICTION, RESTRICTION_DEFAUT));
        infosSite.setSso(propsConfig.getBoolean(proprieteSite + FIN_PROPERTIES_SITE_SSO));
        infosSite.setSitePrincipal(propsConfig.getBoolean(proprieteSite + FIN_PROPERTIES_SITE_PRINCIPAL));
        infosSite.setActif(propsConfig.getBoolean(proprieteSite + FIN_PROPERTIES_SITE_ACTIF));
        setListeProprietesComplementairesDansInfosSite(infosSite, propsConfig);
        return infosSite;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.multisites.dao.InfosSiteDao#getListeInfosSites()
     */
    @Override
    public Collection<InfosSite> getListeInfosSites() throws Exception {
        final ArrayList<InfosSite> infosSites = new ArrayList<>();
        final Iterator<File> tousLesSites = FileUtils.iterateFiles(new File(InfosSitePropertiesHelper.getCheminDossiersProperties()), new String[] {"properties"}, Boolean.FALSE);
        while (tousLesSites.hasNext()) {
            final File fichierCourant = tousLesSites.next();
            infosSites.add(getInfosSite(FilenameUtils.getBaseName(fichierCourant.getName())));
        }
        return infosSites;
    }

    @Override
    public void creer(final InfosSite infosSite) throws Exception {
        final File fichierUAS = new File(InfosSitePropertiesHelper.getCheminDossiersProperties() + infosSite.getAlias() + ".properties");
        if (fichierUAS.exists()) {
            throw new ErreursSaisieInfosSite("le site existe déjà");
        }
        sauvegardeProperties(infosSite);
    }

    @Override
    public void miseAJour(final InfosSite infosSite) throws Exception {
        sauvegardeProperties(infosSite);
    }

    @Override
    public void supprimer(final String code) throws Exception {
        final File fichierUAS = new File(InfosSitePropertiesHelper.getCheminDossiersProperties() + code + ".properties");
        synchronized (LOCK) {
            fichierUAS.delete();
        }
    }

    /**
     * Insérer dans l'infosSite toutes les propriétés en rapport avec le site.
     *
     * @param infosSite
     */
    private void setListeProprietesComplementairesDansInfosSite(final InfosSiteImpl infosSite, final PropertiesConfiguration propsConfig) {
        final String debutCleProprieteSite = InfosSitePropertiesHelper.DEBUT_PROPERTIES_SITE + infosSite.getAlias() + ".";
        @SuppressWarnings("unchecked")
        final Iterator<String> ensembleProprietes = propsConfig.getKeys();
        while (ensembleProprietes.hasNext()) {
            final String clePropriete = ensembleProprietes.next();
            if (!StringUtils.startsWithIgnoreCase(clePropriete, debutCleProprieteSite)) {
                continue;
            }
            // site.code_site.cleProprieteSite === DEVIENT ==> cleProprieteSite
            // (il s'agit de la proriete site)
            final String cleProprieteSite = StringUtils.removeStartIgnoreCase(clePropriete, debutCleProprieteSite);
            if (StringUtils.isNotEmpty(cleProprieteSite) && !FinNomProprieteSite.isProprieteSite(cleProprieteSite)) {
                infosSite.putProperty(cleProprieteSite, propsConfig.getProperty(clePropriete));
            }
        }
    }

    /**
     * On supprime le warning deprecation car on garde ce paramètre pour la retro compatibilité...
     *
     * @param infosSite
     * @throws ErreurApplicative
     */
    @SuppressWarnings("deprecation")
    private void sauvegardeProperties(final InfosSite infosSite) throws ErreurApplicative {
        PropertiesConfiguration propsConfig;
        try {
            propsConfig = getPropertiesConfiguration(infosSite.getAlias());
            final String proprieteSite = DEBUT_PROPERTIES_SITE + infosSite.getAlias();
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_INTITULE, infosSite.getIntitule());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_HOST, infosSite.getHttpHostname());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_HTTPS_HOST, infosSite.getHttpsHostname());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_ALIAS, infosSite.getListeHostAlias());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_PORT, infosSite.getHttpPort());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_HTTPS_PORT, infosSite.getHttpsPort());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_SSL_MODE, infosSite.getSslMode());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_BO_SSL_MODE, infosSite.getBoSslMode());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_HTTP_ACTIONS, infosSite.getHttpActions());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_HTTPS_ACTIONS, infosSite.getHttpsActions());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_RUBRIQUE, infosSite.getCodeRubrique());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MIN, infosSite.getNiveauMinReecritureRubrique());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MAX, infosSite.getNiveauMaxReecritureRubrique());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MODE, infosSite.getModeReecritureRubrique());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_URL_ACCUEIL, infosSite.getUrlAccueil());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_RESTRICTION, infosSite.getRestriction());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_SSO, infosSite.isSso());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_JSP_FO, infosSite.getJspFo());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_TEMPLATE, infosSite.getCodeTemplate());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_PRINCIPAL, infosSite.isSitePrincipal());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_ACTIF, infosSite.isActif());
            setListeProprietesComplementairesDansProperties(infosSite, propsConfig);
            synchronized (LOCK) {
                propsConfig.save();
            }
        } catch (final ConfigurationException e) {
            LOG.error("unable to save the given properties", e);
            throw new ErreursSaisieInfosSite("impossible de sauvegarder le site courant" + infosSite.getAlias());
        }
    }

    private void setListeProprietesComplementairesDansProperties(final InfosSite infosSite, final PropertiesConfiguration propsConfig) {
        final String prefixeProprieteSite = DEBUT_PROPERTIES_SITE + infosSite.getAlias() + ".";
        for (final Map.Entry<String, Object> propriete : infosSite.getProprietesComplementaires().entrySet()) {
            if(propriete.getValue() instanceof String){
                propsConfig.setProperty(prefixeProprieteSite + propriete.getKey(), PropertyConverter.escapeDelimiters((String) propriete.getValue(), InfosSitePropertiesHelper.VALEUR_PROPERTIE_LISTE_DELIMITEUR));
            }else {
                propsConfig.setProperty(prefixeProprieteSite + propriete.getKey(), propriete.getValue());
            }
        }
    }
}
