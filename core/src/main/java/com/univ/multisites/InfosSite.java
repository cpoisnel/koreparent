package com.univ.multisites;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * Classe permettant de contenir les données d'un site dans K-Portal.<br/>
 * De plus, cette classe est abstraite et ne contient que des GETTER pour que ces objets puissent être mis en cache sans risque.
 *
 * @author pierre.cosson
 *
 */
public abstract class InfosSite implements Serializable {

    public static final String HTTPHOSTNAME_DEFAUT = StringUtils.EMPTY;

    public static final String HTTPSHOSTNAME_DEFAUT = StringUtils.EMPTY;

    public static final String JSPFO_DEFAUT = "/jsp";

    public static final int HTTPPORT_DEFAUT = -1;

    public static final int HTTPSPORT_DEFAUT = -1;

    public static final int SSLMODE_DEFAUT = 0;

    public static final int BOSSLMODE_DEFAUT = 0;

    public static final int RESTRICTION_DEFAUT = 0;

    public static final int NIVEAUMINREECRITURERUBRIQUE_DEFAUT = 0;

    public static final int NIVEAUMAXREECRITURERUBRIQUE_DEFAUT = 100;

    public static final int MODEREECRITURERUBRIQUE_DEFAUT = 0;

    /**
     *
     */
    private static final long serialVersionUID = 748460367842507382L;

    public static final Set<Integer> HTTPACTIONS_SET_DEFAUT = Collections.emptySet();

    public static final Set<Integer> HTTPSACTIONS_SET_DEFAUT = Collections.emptySet();

    public static final boolean SSO_DEFAUT = Boolean.FALSE;

    protected long idInfosSite = 0;

    protected String alias = StringUtils.EMPTY;

    protected String intitule = StringUtils.EMPTY;

    protected String httpHostname = HTTPHOSTNAME_DEFAUT;

    protected String urlAccueil = StringUtils.EMPTY;

    protected int httpPort = HTTPPORT_DEFAUT;

    protected String httpsHostname = HTTPSHOSTNAME_DEFAUT;

    protected int httpsPort = HTTPSPORT_DEFAUT;

    protected int sslMode = SSLMODE_DEFAUT;

    protected Set<Integer> httpActionsSet = HTTPACTIONS_SET_DEFAUT;

    protected Set<Integer> httpsActionsSet = HTTPSACTIONS_SET_DEFAUT;

    protected int boSslMode = BOSSLMODE_DEFAUT;

    protected String codeRubrique = StringUtils.EMPTY;

    protected int restriction = RESTRICTION_DEFAUT;

    protected Set<String> listeHostAlias = new HashSet<>();

    protected int niveauMinReecritureRubrique = NIVEAUMINREECRITURERUBRIQUE_DEFAUT;

    protected int niveauMaxReecritureRubrique = NIVEAUMAXREECRITURERUBRIQUE_DEFAUT;

    protected int modeReecritureRubrique = MODEREECRITURERUBRIQUE_DEFAUT;

    protected String jspFo = JSPFO_DEFAUT;

    protected boolean sso = SSO_DEFAUT;

    protected boolean isSitePrincipal = Boolean.FALSE;

    protected Map<String, Object> proprietesComplementaires = new HashMap<>();

    protected boolean isActif = Boolean.TRUE;

    protected String codeTemplate = StringUtils.EMPTY;

    protected Date dateCreation = new Date();

    protected Date dateDerniereModification = new Date();

    protected String codeCreateur = StringUtils.EMPTY;

    protected String codeDernierModificateur = StringUtils.EMPTY;

    protected transient String historique = StringUtils.EMPTY;

    public long getIdInfosSite() {
        return idInfosSite;
    }

    public Set<Integer> getHttpActionsSet() {
        return httpActionsSet;
    }

    public Set<Integer> getHttpsActionsSet() {
        return httpsActionsSet;
    }

    public Map<String, Object> getProprietesComplementaires() {
        return proprietesComplementaires;
    }

    public boolean isActif() {
        return isActif;
    }

    public String getCodeTemplate() {
        return codeTemplate;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public Date getDateDerniereModification() {
        return dateDerniereModification;
    }

    public String getCodeCreateur() {
        return codeCreateur;
    }

    public String getCodeDernierModificateur() {
        return codeDernierModificateur;
    }

    public String getHistorique() {
        return historique;
    }

    public String getAlias() {
        return alias;
    }

    public String getIntitule() {
        return intitule;
    }

    public String getHttpHostname() {
        return httpHostname;
    }

    public int getHttpPort() {
        return httpPort;
    }

    public String getHttpsHostname() {
        return httpsHostname == null ? httpHostname : httpsHostname;
    }

    public int getHttpsPort() {
        return httpsPort;
    }

    public int getSslMode() {
        return sslMode;
    }

    public boolean isHttpAction(final int action) {
        return httpActionsSet.contains(Integer.valueOf(action));
    }

    public boolean isHttpsAction(final int action) {
        return httpsActionsSet.contains(Integer.valueOf(action));
    }

    public int getBoSslMode() {
        return boSslMode;
    }

    public String getCodeRubrique() {
        return codeRubrique;
    }

    public int getRestriction() {
        return restriction;
    }

    public Set<String> getListeHostAlias() {
        return listeHostAlias;
    }

    public int getNiveauMaxReecritureRubrique() {
        return niveauMaxReecritureRubrique;
    }

    public int getNiveauMinReecritureRubrique() {
        return niveauMinReecritureRubrique;
    }

    public int getModeReecritureRubrique() {
        return modeReecritureRubrique;
    }

    /**
     * @deprecated ce paramètre ne doit plus être utiliser, la page d'accueil est géré par les rubriques
     * @return
     */
    @Deprecated
    public String getUrlAccueil() {
        return urlAccueil;
    }

    public String getJspFo() {
        return jspFo;
    }

    public boolean isSso() {
        return sso;
    }

    public boolean isSitePrincipal() {
        return isSitePrincipal;
    }

    public Set<Integer> getHttpActions() {
        return httpActionsSet;
    }

    public Set<Integer> getHttpsActions() {
        return httpsActionsSet;
    }

    public Object getProprieteComplementaire(final String nomPropriete) {
        if (proprietesComplementaires == null) {
            return null;
        }
        return proprietesComplementaires.get(nomPropriete);
    }

    public String getProprieteComplementaireString(final String nomPropriete) {
        if (proprietesComplementaires == null) {
            return null;
        }
        final Object valeur = proprietesComplementaires.get(nomPropriete);
        if (valeur != null && valeur instanceof String) {
            return (String) valeur;
        } else {
            return null;
        }
    }

    public List<String> getProprieteComplementaireListString(final String nomPropriete) {
        if (proprietesComplementaires == null) {
            return null;
        }
        final Object valeur = proprietesComplementaires.get(nomPropriete);
        if (valeur != null && valeur instanceof List<?>) {
            return (List<String>) valeur;
        } else if (valeur != null && valeur instanceof String) {
            return Collections.singletonList(String.valueOf(valeur));
        } else {
            return null;
        }
    }

    /**
     * Vérifie si la rubrique est visible dans le site.
     *
     * @param rubrique
     *            la rubrique à tester.
     * @return <code>true</code> si la rubrique est visible dans le site.
     * @throws Exception
     *             Erreur durant la récupération des {@link InfosRubriques}.
     */
    public boolean isRubriqueVisibleInSite(final InfosRubriques rubrique) {
        final String codeRubrique = getCodeRubrique();
        if (StringUtils.isNotEmpty(codeRubrique)) {
            InfosRubriques rubriqueSite = new InfosRubriques("");
            if (StringUtils.isNotBlank(getCodeRubrique())) {
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(getCodeRubrique());
                if (rubriqueBean != null) {
                    rubriqueSite = new InfosRubriques(rubriqueBean);
                }
            }
            return rubriqueSite.contains(rubrique);
        }
        return Boolean.FALSE;
    }

    /**
     * Vérifie si la rubrique est visible dans le site.
     *
     * @param rubrique
     *            la rubrique à tester.
     * @return <code>true</code> si la rubrique est visible dans le site.
     */
    public boolean isRubriqueVisibleInSite(final RubriqueBean rubrique) {
        final String codeRubrique = getCodeRubrique();
        if (StringUtils.isNotEmpty(codeRubrique) && rubrique != null) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final List<RubriqueBean> allAscendant = serviceRubrique.getAllAscendant(rubrique.getCode());
            for (RubriqueBean ascendant : allAscendant) {
                if (ascendant.getCode().equals(codeRubrique)) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }
}