/*
 * JSS Created on 1 févr. 2005
 *
  */
package com.univ.multisites;

import java.util.Comparator;
// TODO: Auto-generated Javadoc

/**
 * The Class InfosFicheComparator.
 */
public class InfosFicheComparator implements Comparator<InfosFicheReferencee> {

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(final InfosFicheReferencee arg0, final InfosFicheReferencee arg1) {
        final InfosFicheReferencee infos0 = arg0;
        final InfosFicheReferencee infos1 = arg1;
        final String s0 = infos0.getType() + infos0.getCode() + infos0.getLangue() + infos0.getRequete();
        final String s1 = infos1.getType() + infos1.getCode() + infos1.getLangue() + infos1.getRequete();
        return s0.compareTo(s1);
    }
}
