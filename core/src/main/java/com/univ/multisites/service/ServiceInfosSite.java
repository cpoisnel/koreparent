package com.univ.multisites.service;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.jsbsoft.jtf.exception.ErreurUniciteNonRespectee;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;

/**
 * Service d'accés aux {@link InfosSite}.
 *
 *
 */
public interface ServiceInfosSite {

    /**
     * Récupérer une sauvegarde de {@link InfosSite} depuis la source de données. Il s'agit de récupérer un {@link InfosSite} sauvegardé géré ou non par l'application (actif ou
     * pas).
     *
     * @param code
     *            code du {@link InfosSite} à sauvegarder
     * @return le {@link InfosSite}
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurDonneeNonTrouve} : impossible de trouver le {@link InfosSite}</li>
     *             <li>autre : erreur durant les accés à la source de données</li>
     *             </ul>
     */
    InfosSite getInfosSite(final String code);

    /**
     * Récupère la liste complète de tous les {@link InfosSite} contenus dans la source de données.
     *
     * @return Liste de tous les {@link InfosSite} de la source de données. Cette liste peut être vide.
     * @throws Exception
     *             Erreur durant les accés à la source de données.
     */
    Collection<InfosSite> getListeTousInfosSites() throws Exception;

    /**
     * Supprime le site du code/alias fourni
     *
     * @param code
     *            le code du site à supprimer
     * @throws Exception
     *             Erreurs d'écriture dans la source de données
     */
    void supprimer(String code) throws Exception;

    /**
     * Sauvegarder un {@link InfosSite} en base de données.
     *
     * @param infosSite
     *            le {@link InfosSite} à sauvegarder
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurUniciteNonRespectee} : le code du site est déjà utilisé par un autre site</li>
     *             <li>autre : erreur durant les accés à la source de données</li>
     *             </ul>
     */
    void creer(InfosSiteImpl infosSite) throws Exception;

    /**
     * Sauvegarder un {@link InfosSite} en base de données.
     *
     * @param infosSite
     *            le {@link InfosSite} à sauvegarder
     * @param codeUtilisateur
     *            code de l'utilisateur à l'origine de la sauvegarde
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurUniciteNonRespectee} : le code du site est déjà utilisé par un autre site</li>
     *             <li>autre : erreur durant les accés à la source de données</li>
     *             </ul>
     */
    void creer(InfosSiteImpl infosSite, String codeUtilisateur) throws Exception;

    /**
     * Modifier un {@link InfosSite} dans la source de données.
     *
     * @param infosSite
     *            {@link InfosSite} à modifier.
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurUniciteNonRespectee} : le code du site est déjà utilisé par un autre site</li>
     *             <li> {@link ErreurDonneeNonTrouve} : impossible de trouver le {@link InfosSite} d'origine</li>
     *             <li>autre : erreur durant les accés à la source de données</li>
     *             </ul>
     */
    void modifier(InfosSiteImpl infosSite) throws Exception;

    /**
     * Modifier un {@link InfosSite} dans la source de données.
     *
     * @param nouvelleSauvegarde
     *            {@link InfosSite} à modifier.
     * @param codeUtilisateur
     *            code de l'utilisateur à l'origine de la modification
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurUniciteNonRespectee} : le code du site est déjà utilisé par un autre site</li>
     *             <li> {@link ErreurDonneeNonTrouve} : impossible de trouver le {@link InfosSite} d'origine</li>
     *             <li>autre : erreur durant les accés à la source de données</li>
     *             </ul>
     */
    void modifier(InfosSiteImpl nouvelleSauvegarde, String codeUtilisateur) throws Exception;

    /**
     * Enregistre le fichier temporaire dans le dossier spécifique à ce site.
     *
     * @param infosSite
     *            le site auquel appartient le fichier
     * @param fichierTemporaireSource
     *            le fichier à sauvegarder
     * @param nomPropertyTemplateSite
     *            le nom de la propriété du template au quel est rattaché le fichier
     * @throws Exception
     *             lors de l'écriture du fichier
     */
    void enregistrerFichier(InfosSite infosSite, File fichierTemporaireSource, String nomPropertyTemplateSite) throws Exception;

    /**
     * Supprimer le fichier du site fourni ne paramètre
     *
     * @param infosSite
     *            le site pour lequel on doit supprimer un fichier
     * @param nomPropertyTemplateSite
     *            la propriété correspondante au fichier
     * @throws Exception
     *             lors de l'écriture du fichier
     */
    void supprimerFichier(InfosSite infosSite, String nomPropertyTemplateSite) throws Exception;

    /**
     * Supprimer l'ensemble des fichiers liés au site.
     *
     * @param infosSite
     * @throws IOException
     *             Erreur de suppression du dossier.
     */
    void supprimerTousFichiers(InfosSite infosSite) throws IOException;

    /**
     * Mettre à niveau l'ensemble des fichiers
     *
     * @param infosSite
     * @throws IOException
     */
    void cleanFichiers(InfosSite infosSite) throws IOException;

    InfosSite getSiteByHost(String host);

    InfosSite getSiteBySection(String section);

    InfosSite getPrincipalSite();

    Map<String, InfosSite> getSitesList();

    Collection<String> getAliasHostList(String code);

    InfosSite determineSite(final String code, final boolean fallBack);

    InfosSite determineSite(final RubriqueBean rubrique, final boolean fallBack);

    InfosSite displaySite(FicheUniv fiche);

    Collection<String> getFullAliasHostList();
}
