package com.univ.multisites.bean.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import com.univ.multisites.InfosSite;

public class InfosSiteImpl extends InfosSite {

    /**
     *
     */
    private static final long serialVersionUID = -8566928599599335848L;

    public void setAlias(final String alias) {
        this.alias = alias;
    }

    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    public void setHttpHostname(final String httpHostname) {
        this.httpHostname = httpHostname;
    }

    public void setHttpPort(final int httpPort) {
        this.httpPort = httpPort;
    }

    public void setHttpsHostname(final String httpsHostname) {
        this.httpsHostname = httpsHostname;
    }

    public void setHttpsPort(final int httpsPort) {
        this.httpsPort = httpsPort;
    }

    public void setSslMode(final int sslMode) {
        this.sslMode = sslMode;
    }

    public void setHttpActions(final SortedSet<Integer> listeActions) {
        this.httpActionsSet = listeActions;
    }

    public void setHttpsActions(final SortedSet<Integer> listeActions) {
        this.httpsActionsSet = listeActions;
    }

    public void setBoSslMode(final int boSslMode) {
        this.boSslMode = boSslMode;
    }

    public void setCodeRubrique(final String codeRubrique) {
        this.codeRubrique = codeRubrique;
    }

    public void setRestriction(final int restriction) {
        this.restriction = restriction;
    }

    public void setListeHostAlias(final Set<String> listeHostAlias) {
        this.listeHostAlias = listeHostAlias;
    }

    public void setNiveauMaxReecritureRubrique(final int rubrickMaxLevel) {
        this.niveauMaxReecritureRubrique = rubrickMaxLevel;
    }

    public void setNiveauMinReecritureRubrique(final int rubrickMinLevel) {
        this.niveauMinReecritureRubrique = rubrickMinLevel;
    }

    public void setModeReecritureRubrique(final int modeReecritureRubrique) {
        this.modeReecritureRubrique = modeReecritureRubrique;
    }

    /**
     * @deprecated ce paramètre ne doit plus être utiliser, la page d'accueil est géré par les rubriques
     * @param urlAccueil ancien paramètre qui appelait une jsp en dur avec une syntaxe spécifique
     */
    @Deprecated
    public void setUrlAccueil(final String urlAccueil) {
        this.urlAccueil = urlAccueil;
    }

    public void setJspFo(final String jspFo) {
        this.jspFo = jspFo;
    }

    public void setSso(final boolean sso) {
        this.sso = sso;
    }

    public void setSitePrincipal(final boolean isSitePrincipal) {
        this.isSitePrincipal = isSitePrincipal;
    }

    public void putProperty(final String cle, final Object valeur) {
        if (this.proprietesComplementaires == null) {
            this.proprietesComplementaires = new HashMap<>();
        }
        this.proprietesComplementaires.put(cle, valeur);
    }

    public Object getProperty(final String cle) {
        if (this.proprietesComplementaires != null) {
            return proprietesComplementaires.get(cle);
        } else {
            return null;
        }
    }

    public void setIdInfosSite(final long idInfosSite) {
        this.idInfosSite = idInfosSite;
    }

    public void setHttpActionsSet(final SortedSet<Integer> httpActionsSet) {
        this.httpActionsSet = httpActionsSet;
    }

    public void setHttpsActionsSet(final SortedSet<Integer> httpsActionsSet) {
        this.httpsActionsSet = httpsActionsSet;
    }

    public void setProprietesComplementaires(final Map<String, Object> listeProprietesComplementaires) {
        this.proprietesComplementaires = listeProprietesComplementaires;
    }

    public void setActif(final boolean isActif) {
        this.isActif = isActif;
    }

    public void setCodeTemplate(final String codeTemplate) {
        this.codeTemplate = codeTemplate;
    }

    public void setDateCreation(final Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setDateDerniereModification(final Date dateDerniereModification) {
        this.dateDerniereModification = dateDerniereModification;
    }

    public void setCodeCreateur(final String codeCreateur) {
        this.codeCreateur = codeCreateur;
    }

    public void setCodeDernierModificateur(final String codeDernierModificateur) {
        this.codeDernierModificateur = codeDernierModificateur;
    }

    public void setHistorique(final String historique) {
        this.historique = historique;
    }
}