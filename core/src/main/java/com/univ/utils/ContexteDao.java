package com.univ.utils;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.datasource.pool.DataSourceListener;
import com.univ.multisites.InfosSite;

public class ContexteDao implements OMContext, Closeable {

    private static final Logger LOG = LoggerFactory.getLogger(ContexteDao.class);

    private final Map<String, Object> DATA_VIDE = new HashMap<>(0);

    private Connection connection = null;

    @Override
    public Connection getConnection() {
        if (connection == null) {
            try {
                DataSourceListener dataSourceListener = ApplicationContextManager.getCoreContextBean(DataSourceListener.ID_BEAN, DataSourceListener.class);
                DataSource datasource = dataSourceListener.getDataSource();
                connection = datasource.getConnection();
            } catch (SQLException e) {
                LOG.error("Récupération de connexion impossible", e);
                connection = null;
            }
        }
        return connection;
    }

    /**
     *
     * @deprecated utilisez {@link #close()}
     */
    @Deprecated
    public void release() {
        close();
    }

    @Override
    public Locale getLocale() {
        return null;
    }

    @Override
    public String getIdRequete() {
        return null;
    }

    @Override
    public Map<String, Object> getDatas() {
        return DATA_VIDE;
    }

    @Override
    public InfosSite getInfosSite() {
        return null;
    }

    @Override
    public void setInfosSite(final InfosSite infosSite) {
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public void setSecure(final boolean secure) {
    }

    @Override
    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                LOG.error("Fermeture de connexion impossible", e);
            }
        }
    }
}
