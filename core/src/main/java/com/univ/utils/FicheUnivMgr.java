package com.univ.utils;

import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.PersistenceBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Metatag;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.MetatagUtils;

/**
 * Gestionaire générique de fiches FicheUniv Date de création : (10/01/2003 14:23:22).
 *
 * @author :
 */
public class FicheUnivMgr {

    private static final Logger LOG = LoggerFactory.getLogger(FicheUnivMgr.class);

    private static EspaceCollaboratifDAO getEspaceCollaboratifDao(){
        return ApplicationContextManager.getCoreContextBean(EspaceCollaboratifDAO.ID_BEAN, EspaceCollaboratifDAO.class);
    }

    /**
     * Controle si l'utilisateur a les droits pour accéder à la fiche courante.
     *
     * @param ctx      pour avoir les accès à la base
     * @param ficheUniv the fiche univ
     * @return boolean accesOK
     */
    public static boolean controlerRestriction(final FicheUniv ficheUniv, final OMContext ctx)  {
        return controlerRestriction(ficheUniv, ctx, true);
    }

    /**
     * Controle si l'utilisateur a les droits pour accéder à la fiche courante.
     *
     * @param _ctx             pour avoir les accès à la base
     * @param controleRubrique controle
     * @param ficheUniv        the fiche univ
     * @return boolean accesOK
     */
    public static boolean controlerRestriction(final FicheUniv ficheUniv, final OMContext _ctx, final boolean controleRubrique) {
        /* Controle si DSI activée */
        String activationDsi = PropertyHelper.getCoreProperty("dsi.activation");
        if (activationDsi == null) {
            activationDsi = "0";
        }
        if ("0".equals(activationDsi)) {
            return true;
        }
        boolean accesOK = true;
        HttpServletRequest requeteHTTP = null;
        ContexteUniv ctx = null;
        // On controle que la requête est issue du Web
        boolean ajouterRestriction = false;
        if (_ctx instanceof ContexteUniv) {
            ctx = ((ContexteUniv) _ctx);
            requeteHTTP = ctx.getRequeteHTTP();
            if (requeteHTTP != null) {
                ajouterRestriction = true;
            }
        }
        if (ajouterRestriction) {
            if (ficheUniv instanceof DiffusionSelective) {
                accesOK = false;
                final DiffusionSelective ficheDiffusion = (DiffusionSelective) ficheUniv;
                final String modeRestriction = ficheDiffusion.getDiffusionModeRestriction();
                if ("0".equals(modeRestriction)) {
                    accesOK = true;
                } else if ("1".equals(modeRestriction)) {
                    // Restriction par rapport à l'addresse IP
                    final String adresseIP = requeteHTTP.getRemoteAddr();
                    String rechercheDomaine = PropertyHelper.getCoreProperty("dsi.recherche_domaine");
                    if (rechercheDomaine == null) {
                        rechercheDomaine = "0";
                    }
                    String hostClient = "zzzzz.zz";
                    if ("1".equals(rechercheDomaine)) {
                        hostClient = requeteHTTP.getRemoteHost();
                    }
                    String prefixeIP = PropertyHelper.getCoreProperty("dsi.prefixe_ip");
                    if (prefixeIP == null) {
                        prefixeIP = "";
                    }
                    final StringTokenizer st = new StringTokenizer(prefixeIP, ";");
                    // Parcours des différentes adresses IP
                    while (st.hasMoreTokens()) {
                        final String ipCourant = st.nextToken();
                        // Si uniquement des chiffres et des '.', c'est une adresse IP
                        // Sinon c'est un hostname
                        boolean estUneIP = true;
                        for (int i = 0; i < ipCourant.length(); i++) {
                            final char ch = ipCourant.charAt(i);
                            if ((!Character.isDigit(ch)) && (ch != '.')) {
                                estUneIP = false;
                            }
                        }
                        if ((estUneIP) && (adresseIP.indexOf(ipCourant) == 0)) {
                            accesOK = true;
                        }
                        if (!estUneIP) {
                            if (hostClient.endsWith(ipCourant)) {
                                accesOK = true;
                            }
                        }
                    }
                } else if ("2".equals(modeRestriction)) {
                    accesOK = controlerRestrictionGroupeDsi(ficheDiffusion.getDiffusionPublicVise(), ctx);
                } else if ("3".equals(modeRestriction)) {
                    accesOK = controlerRestrictionGroupeDsi(ficheDiffusion.getDiffusionPublicViseRestriction(), ctx);
                }
                // JSS 20040409 : collaboratif
                else if ("4".equals(modeRestriction)) {
                    // liste des espaces dont l'utilisateur est membre
                    EspaceCollaboratifBean espace = getEspaceCollaboratifDao().getByCode(ficheDiffusion.getDiffusionPublicViseRestriction());
                    if (Espacecollaboratif.estMembreEspace(ctx, espace)) {
                        accesOK = true;
                    } else {
                        // liste des espaces sur lesquels l'utilisateur a un droit de consultation
                        if (Espacecollaboratif.estVisiteurEspace(ctx, espace)) {
                            accesOK = true;
                        }
                    }
                }
            }
            if (accesOK && controleRubrique) {
                // RP20050905 ajout de la dsi sur les rubriques
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                accesOK = serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), ficheUniv.getCodeRubrique());
                // JSS 20051213 : test sur les rubriques de publication
                if (!accesOK) {
                    final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
                    final Collection<String> rubriquesPublications = serviceRubriquePublication.getRubriqueDestByFicheUnivAndRubrique(ficheUniv, ctx.getInfosSite().getCodeRubrique());
                    final Iterator<String> e = rubriquesPublications.iterator();
                    while (!accesOK && e.hasNext()) {
                        accesOK = serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), e.next());
                    }
                }
                if (accesOK && ctx.getCodeRubriqueFicheCourante().length() > 0) {
                    accesOK = serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), ctx.getCodeRubriqueFicheCourante());
                }
            }
            if (ctx.getCodeRubriqueFicheCourante().equals(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE)) {
                accesOK = false;
                ctx.setCodeRubriqueFicheCourante("");
            }
        }
        return accesOK;
    }

    /**
     * Controle si l'utilisateur a les droits pour accéder à la fiche courante
     * <p>
     * !!!! ATTENTION : CES CONTROLES SONT REDONDANTS AVEC CEUX EFFECTUES LORS D'UNE REQUETE (SQLUtil.controlerRestrictionGroupeDsi) : TOUTE MODIF DOIT DONC ETRE REPORTEE
     *
     * @param ctx                     contexte pour la base et les autorisations du user
     * @param groupesRestrictionFiche the groupes restriction fiche
     * @return boolean accesOK
     */
    private static boolean controlerRestrictionGroupeDsi(final String groupesRestrictionFiche, final ContexteUniv ctx) {
        boolean accesOK = false;
        // Restriction profil seulement
        /***************************************/
        /* On détermine l'ensemble des groupes */
        /***************************************/
        final Vector<String> listeGroupes = new Vector<>();
        for (String groupeCourant : ctx.getGroupesDsi()) {
            listeGroupes.add(groupeCourant);
            final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
            GroupeDsiBean group = serviceGroupeDsi.getByCode(groupeCourant);
            /* On recherche les items de niveau supérieur */
            final int niveauItemCourant = serviceGroupeDsi.getLevel(group);
            int niveau = niveauItemCourant - 1;
            while (niveau > 0) {
                listeGroupes.add(group.getCodeGroupePere());
                group = serviceGroupeDsi.getByCode(group.getCodeGroupePere());
                niveau--;
            }
        }
        // Restriction par rapport aux groupes
        final Enumeration<String> e = listeGroupes.elements();
        while (e.hasMoreElements()) {
            final String codeGroupeDsi = e.nextElement();
            if (groupesRestrictionFiche.contains("[/" + codeGroupeDsi + "]")) {
                accesOK = true;
            }
        }
        return accesOK;
    }

    /**
     * Initialisation d'un objet métier
     * <p>
     * Sont exclus du traitement: - l'identifiant - le code - la langue - les données métiers.
     *
     * @param ficheUniv : fiche dont les données techniques doivent etre initialisées
     */
    public static void init(final FicheUniv ficheUniv) {
        ficheUniv.setMetaKeywords("");
        ficheUniv.setMetaDescription("");
        ficheUniv.setTitreEncadre("");
        ficheUniv.setContenuEncadre("");
        ficheUniv.setEncadreRecherche("");
        ficheUniv.setEncadreRechercheBis("");
        ficheUniv.setMessageAlerte("");
        ficheUniv.setDateCreation(new Date(System.currentTimeMillis()));
        ficheUniv.setDateModification(new Date(System.currentTimeMillis()));
        ficheUniv.setCodeRedacteur("");
        ficheUniv.setCodeValidation("");
        ficheUniv.setCodeRattachement("");
        ficheUniv.setCodeRubrique("");
        if (ficheUniv instanceof DiffusionSelective) {
            final DiffusionSelective ficheDiffusion = (DiffusionSelective) ficheUniv;
            ficheDiffusion.setDiffusionModeRestriction("0");
            ficheDiffusion.setDiffusionPublicVise("");
            ficheDiffusion.setDiffusionPublicViseRestriction("");
        }
        ficheUniv.setEtatObjet("0001");
        ficheUniv.setNbHits((long) 0);
    }

    /**
     * Initialisation d'un objet métier
     * <p>
     * Sont exclus du traitement: - l'identifiant - le code - la langue - les données métiers.
     *
     * @param meta : fiche dont les données techniques doivent etre initialisées
     * @deprecated Utilisez {@link FicheUnivMgr#init(MetatagBean)}
     */
    @Deprecated
    public static FicheUniv init(final Metatag meta) {
        return init(meta.getPersistenceBean());
    }

    /**
     * Initialisation d'un objet métier
     * <p>
     * Sont exclus du traitement: - l'identifiant - le code - la langue - les données métiers.
     *
     * @param meta : fiche dont les données techniques doivent etre initialisées
     */
    public static FicheUniv init(final MetatagBean meta) {
        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(meta.getMetaCodeObjet()));
        if (ficheUniv != null) {
            ficheUniv.init();
            ficheUniv.setCodeRattachement(meta.getMetaCodeRattachement());
            ficheUniv.setCodeRubrique(meta.getMetaCodeRubrique());
            if (ficheUniv instanceof FicheRattachementsSecondaires) {
                ((FicheRattachementsSecondaires) ficheUniv).setCodeRattachementAutres(Chaine.convertirPointsVirgulesEnAccolades(meta.getMetaCodeRattachementAutres()));
            }
            ficheUniv.setCodeRedacteur(meta.getMetaCodeRedacteur());
            ficheUniv.setCodeValidation(meta.getMetaCodeValidation());
            ficheUniv.setCode(meta.getMetaCode());
            ficheUniv.setLangue(meta.getMetaLangue());
            ficheUniv.setEtatObjet(meta.getMetaEtatObjet());
            if (ficheUniv instanceof DiffusionSelective) {
                ((DiffusionSelective) ficheUniv).setDiffusionPublicVise(meta.getMetaDiffusionPublicVise());
                ((DiffusionSelective) ficheUniv).setDiffusionModeRestriction(meta.getMetaDiffusionModeRestriction());
                ((DiffusionSelective) ficheUniv).setDiffusionPublicViseRestriction(meta.getMetaDiffusionPublicViseRestriction());
            }
        }
        return ficheUniv;
    }

    /**
     * Création d'une version en brouillon
     * <p>
     * Attention, cette méthode détruit la version en brouillon en cours.
     *
     * @param ctx   pour accéder à la base
     * @param fiche : la fiche dont on veut créer un brouillon
     * @throws Exception the exception
     * @deprecated utiliser {@link FicheUnivMgr#creerVersionTravail(FicheUniv)}
     */
    @Deprecated
    public static void creerVersionTravail(final OMContext ctx, final FicheUniv fiche) throws Exception {
        creerVersionTravail(fiche);
    }

    /**
     * Création d'une version en brouillon
     * <p>
     * Attention, cette méthode détruit la version en brouillon en cours.
     *
     * @param fiche la fiche dont on veut créer un brouillon
     * @throws Exception lors des accès bdd
     */
    public static void creerVersionTravail(final FicheUniv fiche) throws Exception {
        try (ContexteDao ctx = new ContexteDao()) {
            if (existeVersionTravail(fiche)) {
                /* On vérifie qu'il n'y a pas déja de version de travail */
                final FicheUniv fiche2 = fiche.getClass().newInstance();
                fiche2.init();
                fiche2.setCtx(ctx);
                if (fiche2.selectCodeLangueEtat(fiche.getCode(), fiche.getLangue(), "0001") > 0) {
                    while (fiche2.nextItem()) {
                        FicheUnivMgr.supprimerFiche(fiche2, true);
                    }
                }
            }
            //création objet dans la langue
            fiche.setIdFiche((long) 0);
            fiche.setEtatObjet("0001");
            fiche.setCtx(ctx);
            fiche.add();
        }
    }

    /**
     * Enregistrement de l'objet
     * <p>
     * Cette fonction doit être appelée à la place de add() et update()
     * <p>
     * En effet, un enregistrement doît être répercuté dans les fiches des autres langues
     * <p>
     * Dans le cas d'une création, la relecture permet en particulier de pouvoir modifier l'objet par la suite.
     *
     * @param ctx       pour l'accès a la base
     * @param fCreation the f creation
     * @param fiche     the fiche
     * @throws Exception the exception
     * @deprecated utiliser {@link FicheUnivMgr#enregistrer(boolean, FicheUniv)}
     */
    @Deprecated
    public static void enregistrer(final boolean fCreation, final OMContext ctx, final FicheUniv fiche) throws Exception {
        enregistrer(fCreation, fiche);
    }

    /**
     * Enregistrement de l'objet
     * <p>
     * Cette fonction doit être appelée à la place de add() et update()
     * <p>
     * En effet, un enregistrement doît être répercuté dans les fiches des autres langues
     * <p>
     * Dans le cas d'une création, la relecture permet en particulier de pouvoir modifier l'objet par la suite.
     *
     * @param creation doit on creer ou updater la fiche?
     * @param fiche    la fiche à enregistrer
     * @throws Exception lors des accès en base de données
     */
    public static void enregistrer(final boolean creation, final FicheUniv fiche) throws Exception {
        //on ne reporte plus les modifs dans les autres fiches
        try (ContexteDao ctx = new ContexteDao()) {
            if (creation) {
                /* On vérifie qu'il n'y a pas une version */
                boolean doublon = Boolean.FALSE;
                final FicheUniv fiche2 = fiche.getClass().newInstance();
                fiche2.init();
                fiche2.setCtx(ctx);
                if (fiche2.selectCodeLangueEtat(fiche.getCode(), fiche.getLangue(), "") > 0) {
                    doublon = Boolean.TRUE;
                }
                if (!doublon) {
                    fiche.setCtx(ctx);
                    fiche.add();
                } else {
                    throw new ErreurApplicative("Cette fiche existe déjà");
                }
            } else {
                fiche.setCtx(ctx);
                fiche.update();
            }
        }
    }

    /**
     * Enregistrement du métatag : ajout ou màj, en fonction de son existance ou non.
     *
     * @param ctx  pour l'accès à la base
     * @param meta the meta
     * @deprecated utiliser {@link FicheUnivMgr#enregistrerMeta(Metatag)}
     */
    @Deprecated
    public static void enregistrerMeta(final OMContext ctx, final Metatag meta) {
        enregistrerMeta(meta);
    }

    /**
     * Enregistrement du métatag : ajout ou màj, en fonction de son existance ou non.
     *
     * @param meta le meta a enregistrer
     * @deprecated Utilisez {@link ServiceMetatag#save(PersistenceBean)}
     */
    @Deprecated
    public static void enregistrerMeta(final Metatag meta) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        serviceMetatag.save(meta.getPersistenceBean());
    }

    /**
     * Permet de savoir s'il existe déjà une version en cours de chantier pour cet objet.
     *
     * @param ctx   contexte pour accéder à la base
     * @param fiche the fiche
     * @return boolean existe
     * @throws Exception the exception
     * @deprecated utiliser {@link FicheUnivMgr#existeVersionTravail(FicheUniv)}
     */
    @Deprecated
    public static boolean existeVersionTravail(final OMContext ctx, final FicheUniv fiche) throws Exception {
        return existeVersionTravail(fiche);
    }

    /**
     * Permet de savoir s'il existe déjà une version en cours de chantier pour cet objet.
     *
     * @param fiche la fiche à vérifier
     * @return boolean est ce que la fiche existe en version de travail?
     * @throws Exception lors des accès en bdd
     */
    public static boolean existeVersionTravail(final FicheUniv fiche) throws Exception {
        // On vérifie qu'il n'y a pas déja de version de travail
        boolean result;
        try (ContexteDao ctx = new ContexteDao()) {
            final FicheUniv fiche2 = fiche.getClass().newInstance();
            fiche2.init();
            fiche2.setCtx(ctx);
            result = (fiche2.selectCodeLangueEtat(fiche.getCode(), fiche.getLangue(), "0001") > 0);
        }
        return result;
    }

    /**
     * Lecture du métatag.
     *
     * @param ctx       pour accéder à la base
     * @param ficheUniv fiche dont on veut récupérer le méta
     * @return une instance de Metatag
     * @deprecated utiliser {@link FicheUnivMgr#lireMeta(FicheUniv)}
     */
    @Deprecated
    public static Metatag lireMeta(final OMContext ctx, final FicheUniv ficheUniv) {
        return lireMeta(ficheUniv);
    }

    /**
     * Lecture du métatag.
     *
     * @param ficheUniv fiche dont on veut récupérer le méta
     * @return une instance de Metatag
     * @deprecated Utilisez {@link com.univ.objetspartages.util.MetatagUtils#lireMeta(FicheUniv)}
     */
    @Deprecated
    public static Metatag lireMeta(final FicheUniv ficheUniv)  {
        // Lecture métatag
        final Metatag meta = new Metatag();
        meta.setPersistenceBean(MetatagUtils.getMetatagService().getByCodeAndIdFiche(ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName()), ficheUniv.getIdFiche()));
        return meta;
    }

    /**
     * creation d'un métatag.
     *
     * @param ctx       pour accéder à la base
     * @param ficheUniv fiche dont on veut créer le méta Cette méthode est appelée uniquement dans le scansite en cas de perte de meta données ce qui est normalement impossible sauf en
     *                  cas de crash de la bd !!!
     * @deprecated utiliser {@link FicheUnivMgr#creerMeta(FicheUniv)}
     */
    @Deprecated
    public static Metatag creerMeta(final OMContext ctx, final FicheUniv ficheUniv) {
        return creerMeta(ficheUniv);
    }

    /**
     * creation d'un métatag.
     *
     * @param ficheUniv fiche dont on veut créer le méta Cette méthode est appelée uniquement dans le scansite en cas de perte de meta données ce qui est normalement impossible sauf en
     *                  cas de crash de la bd !!!
     * @deprecated Utilisez {@link MetatagUtils#creerMeta(FicheUniv)}
     */
    @Deprecated
    public static Metatag creerMeta(final FicheUniv ficheUniv) {
        final Metatag meta = new Metatag();
        meta.setPersistenceBean(MetatagUtils.creerMeta(ficheUniv));
        return meta;
    }

    /**
     * Mise en ligne de la fiche
     * <p>
     * La mise en ligne est précédée de la destruction de la version déjà en ligne.
     *
     * @param ctx           pour accéder à la base
     * @param fiche         the fiche
     * @param ancienneFiche the ancienne fiche
     * @return the long
     * @throws Exception the exception
     * @deprecated utiliser {@link FicheUnivMgr#mettreEnLigne(FicheUniv, FicheUniv)}
     */
    @Deprecated
    public static Long mettreEnLigne(final OMContext ctx, final FicheUniv fiche, final FicheUniv ancienneFiche) throws Exception {
        return mettreEnLigne(fiche, ancienneFiche);
    }

    /**
     * Mise en ligne de la fiche
     * <p>
     * La mise en ligne est précédée de la destruction de la version déjà en ligne.
     *
     * @param fiche         la fiche a mettre en ligne
     * @param ancienneFiche la fiche pouvant être à supprimer lors du passage de la nouvelle en version en ligne
     * @return l'id du meta de la fiche en ligne
     * @throws Exception lors des accès en bdd
     */
    public static Long mettreEnLigne(final FicheUniv fiche, final FicheUniv ancienneFiche) throws Exception {
        Long idMetaFicheEnLigne = (long) 0;
        try (ContexteDao ctx = new ContexteDao()) {
            if (!"0003".equals(fiche.getEtatObjet())) {
                fiche.setEtatObjet("0003");
                fiche.setCtx(ctx);
                fiche.update();
                // Suppression de l'ancienne fiche
                if (ancienneFiche != null && !ancienneFiche.getIdFiche().equals(fiche.getIdFiche())) {
                    idMetaFicheEnLigne = supprimerFiche(ancienneFiche, false);
                }
            }
        }
        return idMetaFicheEnLigne;
    }

    /**
     * Suppression du métatag.
     *
     * @param ctx       le contexte pour accéder à la base
     * @param ficheUniv la fiche dont on veut supprimer le méta
     * @return the long
     * @deprecated utiliser {@link FicheUnivMgr#supprimerMeta(FicheUniv)}
     */
    @Deprecated
    public static Metatag supprimerMeta(final OMContext ctx, final FicheUniv ficheUniv) {
        return supprimerMeta(ficheUniv);
    }

    /**
     * Suppression du métatag.
     *
     * @param ficheUniv la fiche dont on veut supprimer le méta
     * @return le meta venant d'être supprimer
     * @deprecated Utilisez {@link MetatagUtils#supprimerMeta(FicheUniv)}
     */
    @Deprecated
    public static Metatag supprimerMeta(final FicheUniv ficheUniv) {
        final Metatag meta = new Metatag();
        meta.setPersistenceBean(MetatagUtils.supprimerMeta(ficheUniv));
        return meta;
    }


    /**
     * Transforme une jointure par code en texte de type
     * <p>
     * [id-fiche]actualite;1063965371735[/id-fiche]
     * <p>
     * (destiné aux références croisées).
     *
     * @param objet the objet
     * @param code  the code
     * @return String la jointure au format tel qu'il apparait dans la toolbox
     */
    public static String getReferenceParJointure(final String objet, final String code) {
        final StringBuilder res = new StringBuilder();
        //AM 200501 : correction pour gérer les jointures multiples
        final StringTokenizer st = new StringTokenizer(code, ";");
        while (st.hasMoreTokens()) {
            res.append("[id-fiche]").append(objet).append(";").append(st.nextToken()).append("[/id-fiche]");
        }
        return res.toString();
    }

    /**
     * Transforme une jointure par code en texte de type
     * <p>
     * "[id-image]16[/id-image]" (destiné aux références croisées).
     *
     * @param idPhoto the id photo
     * @return String la jointure au format tel qu'il apparait dans la toolbox
     */
    public static String getReferenceParPhoto(final Long idPhoto) {
        String res = "";
        if (!idPhoto.equals(Long.valueOf(0))) {
            res = "[id-image]" + idPhoto + "[/id-image]";
        }
        return res;
    }

    /**
     * Transforme une liste de Fichiergw en texte de type "[id-image]16[/id-image]" répété autant de fois qu'il y a de photos dans la liste (destiné aux références croisées).
     *
     * @param listePhotos the liste photos
     * @return String la jointure au format tel qu'il apparait dans la toolbox
     */
    public static String getReferenceParListePhoto(final List<RessourceBean> listePhotos) {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        String res = "";
        for (final RessourceBean photo : listePhotos) {
            final MediaBean mediaBean = serviceMedia.getById(photo.getIdMedia());
            if (MediaUtils.isPhoto(mediaBean)) {
                res = "[id-image]" + photo.getIdMedia() + "[/id-image]";
            }
        }
        return res;
    }

    /**
     * Renvoie le contenu d'un champ texte pour étude des références
     * <p>
     * (destiné aux références croisées).
     *
     * @param texte the texte
     * @return String le meme texte... sauf s'il est null, renvoie ""
     * @deprecated merci d'utiliser {@link StringUtils#defaultString(String)} ...
     */
    @Deprecated
    public static String getReferenceParTexte(final String texte) {
        String res = "";
        if (texte != null) {
            res = texte;
        }
        return res;
    }

    /**
     * Lecture du métatag.
     *
     * @param ctx        le contexte pour l'accès à la base
     * @param _codeObjet the _code objet
     * @param idFiche    the id fiche
     * @return le méta correspondant à l'identifiant et au type d'objet
     * @deprecated utilser {@link FicheUnivMgr#lireMeta(String, Long)}
     */
    @Deprecated
    public static Metatag lireMeta(final OMContext ctx, final String _codeObjet, final Long idFiche) {
        return lireMeta(_codeObjet, idFiche);
    }

    /**
     * Lecture du métatag.
     *
     * @param codeObjet le code objet de la fiche à supprimer
     * @param idFiche   l'id de la fiche à supprimer
     * @return le méta correspondant à l'identifiant et au type d'objet
     * @deprecated Utilisez {@link MetatagUtils#lireMeta(String, Long)} )}
     */
    @Deprecated
    public static Metatag lireMeta(final String codeObjet, final Long idFiche) {
        // Lecture métatag
        final Metatag meta = new Metatag();
        meta.setPersistenceBean(MetatagUtils.getMetatagService().getByCodeAndIdFiche(codeObjet, idFiche));
        return meta;
    }

    /**
     * Initialisation d'un objet métier dupliqué seules les données techniques sont réinitialisées, sauf le rédacteur Sont exclus du traitement: - l'identifiant - le code - la
     * langue - les données métiers.
     *
     * @param ficheUniv the fiche univ
     */
    public static void dupliquer(final FicheUniv ficheUniv) {
        ficheUniv.setMessageAlerte("");
        ficheUniv.setDateCreation(new Date(System.currentTimeMillis()));
        ficheUniv.setDateModification(new Date(System.currentTimeMillis()));
        ficheUniv.setCodeValidation("");
        ficheUniv.setEtatObjet("0001");
        ficheUniv.setNbHits((long) 0);
    }

    /**
     * Checks if is fiche collaborative.
     *
     * @param ficheUniv the _fiche univ
     * @return true, if is fiche collaborative
     */
    public static boolean isFicheCollaborative(final FicheUniv ficheUniv) {
        return ficheUniv instanceof DiffusionSelective && "4".equals(((DiffusionSelective) ficheUniv).getDiffusionModeRestriction()) && ((DiffusionSelective) ficheUniv).getDiffusionPublicViseRestriction().length() > 0;
    }

    /**
     * Synchroniser meta tag.
     *
     * @param ctx           the ctx
     * @param ficheUniv     the fiche univ
     * @param majReferences the maj references
     * @return true, if successful
     * @throws Exception the exception
     * @deprecated utiliser {@link FicheUnivMgr#synchroniserMetaTag(FicheUniv, String)}
     */
    @Deprecated
    public static boolean synchroniserMetaTag(final OMContext ctx, final FicheUniv ficheUniv, final String majReferences) throws Exception {
        return synchroniserMetaTag(ficheUniv, majReferences);
    }

    /**
     * Synchronisation des méta-tags Traitement sur les fiches.
     *
     * @param ctx                      the ctx
     * @param ficheUniv                the fiche univ
     * @param majReferences            the maj references
     * @param effectuerTraitementFiche the effectuer traitement fiche
     * @return true, if synchroniser meta tag
     * @deprecated utiliser {@link FicheUnivMgr#synchroniserMetaTag(FicheUniv, String, boolean)}
     */
    @Deprecated
    public static boolean synchroniserMetaTag(final OMContext ctx, final FicheUniv ficheUniv, final String majReferences, final boolean effectuerTraitementFiche) {
        return synchroniserMetaTag(ficheUniv, majReferences, effectuerTraitementFiche);
    }

    /**
     * Synchroniser meta tag.
     *
     * @param ficheUniv     the fiche univ
     * @param majReferences the maj references
     * @return true, if successful
     */
    public static boolean synchroniserMetaTag(final FicheUniv ficheUniv, final String majReferences) {
        return synchroniserMetaTag(ficheUniv, majReferences, false);
    }

    /**
     * Synchronisation des méta-tags Traitement sur les fiches.
     *
     * @param ficheUniv                the fiche univ
     * @param majReferences            the maj references
     * @param effectuerTraitementFiche the effectuer traitement fiche
     * @return true, if synchroniser meta tag
     */
    public static boolean synchroniserMetaTag(final FicheUniv ficheUniv, final String majReferences, final boolean effectuerTraitementFiche) {
        final MetatagBean meta = MetatagUtils.synchroniserMetaTag(ficheUniv, majReferences);
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        String codeObjet = "";
        String code = "";
        String langue = "0";
        boolean synchro = false;
        try (ContexteDao ctx = new ContexteDao()) {
            codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
            code = ficheUniv.getCode();
            langue = ficheUniv.getLangue();
            final long today = System.currentTimeMillis();
            // Lecture métatag
            if (meta != null) {
                if (effectuerTraitementFiche) {
                    // Traitement des méta-informations
                    // Suppresion de la fiche
                    if (meta.getMetaDateSuppression() != null) {
                        if (Formateur.estSaisie(meta.getMetaDateSuppression())) {
                            if (today > meta.getMetaDateSuppression().getTime()) {
                                supprimerFiche(ficheUniv, true);
                            }
                        }
                    } else {
                        meta.setMetaDateSuppression(null);
                    }
                    if ("0003".equals(ficheUniv.getEtatObjet())) {
                        if (meta.getMetaDateRubriquage() != null) {
                            // Rubriquage de la fiche
                            if (Formateur.estSaisie(meta.getMetaDateRubriquage())) {
                                if (today > meta.getMetaDateRubriquage().getTime()) {
                                    // on vérifie que la rubrique existe bien
                                    if (meta.getMetaCodeRubriquage().length() > 0) {
                                        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                                        if (serviceRubrique.codeAlreadyExist(meta.getMetaCodeRubriquage())) {
                                            ficheUniv.setCodeRubrique(meta.getMetaCodeRubriquage());
                                            ficheUniv.setCtx(ctx);
                                            ficheUniv.update();
                                            serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_RUBRIQUAGE_AUTO, "nobody", ficheUniv.getEtatObjet());
                                        }
                                        meta.setMetaCodeRubriquage("");
                                        meta.setMetaDateRubriquage(null);
                                    }
                                }
                            }
                        } else {
                            meta.setMetaDateRubriquage(null);
                        }
                        // Archivage (fiches en ligne)
                        if (meta.getMetaDateArchivage() != null) {
                            if (Formateur.estSaisie(meta.getMetaDateArchivage()) && today > meta.getMetaDateArchivage().getTime()) {
                                ficheUniv.setEtatObjet("0007");
                                ficheUniv.setCtx(ctx);
                                ficheUniv.update();
                                meta.setMetaDateArchivage(null);
                                serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_ARCHIVAGE_AUTO, "nobody", "0007");
                            }
                        } else {
                            meta.setMetaDateArchivage(null);
                        }
                    }
                }
                boolean update = false;
                // La mise à jour des références croisées est en option
                if ("1".equals(majReferences)) {
                    final String references = meta.getMetaListeReferences();
                    serviceMetatag.calculerReferences(meta, ficheUniv.getReferences(), ficheUniv.getContenuEncadre(), ficheUniv.getLangue());
                    if (!references.equals(meta.getMetaListeReferences())) {
                        update = true;
                    }
                }
                // Synchronisation du méta avec sa fiche
                final List<String> differences = serviceMetatag.controlerCoherenceAvecFiche(meta, ficheUniv);
                if (!differences.isEmpty()) {
                    serviceMetatag.synchroniser(meta, ficheUniv, false);
                    update = true;
                }
                // Hors traitement spécifique, on note la modification de la fiche
                if (!effectuerTraitementFiche) {
                    meta.setMetaDateOperation(new Date(System.currentTimeMillis()));
                    update = true;
                }
                if (update) {
                    serviceMetatag.save(meta);
                }
            }
            synchro = true;
        } catch (final Exception e) {
            LOG.error("Exception pendant synchronisation metatag fiche  objet : " + ReferentielObjets.getNomObjet(codeObjet) + " code : " + code + " langue : " + langue, e);
        }
        return synchro;
    }

    /**
     * Suppression des fiches obsoletes.
     *
     * @param ctx             the ctx
     * @param ficheUniv       the fiche univ
     * @param suppressionMeta the suppression meta
     * @return the long
     * @deprecated utiliser {@link FicheUnivMgr#supprimerFiche(FicheUniv, boolean)}
     */
    @Deprecated
    public static Long supprimerFiche(final OMContext ctx, final FicheUniv ficheUniv, final boolean suppressionMeta) {
        return supprimerFiche(ficheUniv, suppressionMeta);
    }

    public static Long supprimerFiche(final FicheUniv ficheUniv, final boolean suppressionMeta) {
        String nomObjet = StringUtils.EMPTY;
        Long idMeta = 0L;
        try (ContexteDao ctx = new ContexteDao()) {
            nomObjet = ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjet(ficheUniv));
            // suppression de la fiche
            ficheUniv.setCtx(ctx);
            ficheUniv.delete();
            MetatagBean meta;
            // suppression des metas
            if (suppressionMeta) {
                meta = MetatagUtils.supprimerMeta(ficheUniv);
                if (meta != null) {
                    idMeta = meta.getId();
                }
            } else {
                meta = MetatagUtils.lireMeta(ficheUniv);
                if (meta != null) {
                    idMeta = meta.getId();
                }
            }
            // RP20060301 suppression des rubriques de publication
            // On supprime les rubriques de publication uniquement s'il n'y a plus de fiche ayant le meme code
            final FicheUniv fiche2 = ficheUniv.getClass().newInstance();
            fiche2.init();
            fiche2.setCtx(ctx);
            if (fiche2.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "") == 0) {
                final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
                serviceRubriquePublication.deleteByFiche(ficheUniv);
            }
            // suppression des données liés au plugins de contenu via la fiche et/ou le meta
            PluginFicheHelper.supprimerObjets(ficheUniv, meta, null);
            LOG.debug("Suppression fiche " + "objet : " + nomObjet + " code : " + ficheUniv.getCode() + " langue : " + ficheUniv.getLangue() + "\n");
        } catch (final Exception e) {
            LOG.error("Exception pendant suppression fiche " + "objet : " + nomObjet + " code : " + ficheUniv.getCode() + " langue : " + ficheUniv.getLangue(), e);
        }
        return idMeta;
    }

    /**
     * Suppression des fiches obsoletes.
     *
     * @param ficheUniv la fiche à supprimer
     * @param meta      le meta associé à la fiche à supprimer
     *                  <p>
     *                  =
     * @deprecated Utilisez {@link #supprimerFiche(FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void supprimerFiche(final FicheUniv ficheUniv, final Metatag meta) {
        supprimerFiche(ficheUniv, meta.getPersistenceBean());
    }

    /**
     * Suppression des fiches obsoletes.
     *
     * @param ficheUniv la fiche à supprimer
     * @param meta      le meta associé à la fiche à supprimer
     *                  <p>
     *                  =
     */
    public static void supprimerFiche(final FicheUniv ficheUniv, final MetatagBean meta) {
        String nomObjet = "";
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        try (ContexteDao ctx = new ContexteDao()) {
            nomObjet = ReferentielObjets.getNomObjet(meta.getMetaCodeObjet());
            // suppression de la fiche
            ficheUniv.setCtx(ctx);
            ficheUniv.delete();
            // suppression des metas
            serviceMetatag.delete(meta.getId());
            // suppression des rubriques de publication
            // On supprime les rubriques de publication uniquement s'il n'y a plus de fiche ayant le meme code
            final FicheUniv fiche2 = ficheUniv.getClass().newInstance();
            fiche2.init();
            fiche2.setCtx(ctx);
            if (fiche2.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), StringUtils.EMPTY) == 0) {
                final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
                serviceRubriquePublication.deleteByFiche(ficheUniv);
            }
            // suppression des données liés au plugins de contenu via la fiche et/ou le meta
            PluginFicheHelper.supprimerObjets(ficheUniv, meta, null);
            LOG.debug("Suppression fiche objet : " + nomObjet + " code : " + ficheUniv.getCode() + " langue : " + ficheUniv.getLangue());
        } catch (final Exception e) {
            LOG.error("Exception pendant suppression fiche objet : " + nomObjet + " code : " + ficheUniv.getCode() + " langue : " + ficheUniv.getLangue(), e);
        }
    }

    /**
     * Méthode à ne pas utiliser. Elle ne calcule pas la chaine à sérialiser par lucene mais renvoie uniquement le libellé de sa structure dans sa lange plus un espace si il est trouvé.
     *
     * @param ctx   le contexte n'est pas utilisé
     * @param fiche la fiche dont on souhaite avoir la structure (ne peut pas être null)
     * @return le libellé de la structure + un espace si il est trouvé ou une chaine vide sinon,
     * @throws Exception lors de la récupération du libellé de la structure
     * @deprecated Méthode à ne pas utiliser. Elle ne calcule pas la chaine à sérialiser par lucene mais renvoie uniquement le libellé de sa structure dans sa lange plus un espace si il est trouvé.
     * le contexte ne sert pas...
     */
    @Deprecated
    public static String getFullTextString(final OMContext ctx, final FicheUniv fiche) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        String res = serviceStructure.getDisplayableLabel(fiche.getCodeRattachement(), fiche.getLangue());
        if (res.length() > 0) {
            res += " ";
        }
        return res;
    }
}
