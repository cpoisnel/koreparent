/*
 * Created on 26 sept. 2005
 *
 * Gestion de cache
 */
package com.univ.utils;
// TODO: Auto-generated Javadoc

/**
 * The Class CacheUrlBean.
 *
 * @author jeanseb Stockage des informations associées à un cache d'url
 */
public class CacheUrlBean {

    /** The timestamp. */
    long timestamp = 0;

    /** The contenu. */
    String contenu = "";

    /**
     * The Constructor.
     *
     * @param timestamp
     *            the timestamp
     * @param contenu
     *            the contenu
     */
    public CacheUrlBean(long timestamp, String contenu) {
        super();
        this.timestamp = timestamp;
        this.contenu = contenu;
    }

    /**
     * Gets the contenu.
     *
     * @return the contenu
     */
    public String getContenu() {
        return contenu;
    }

    /**
     * Gets the timestamp.
     *
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }
}
