package com.univ.utils;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.MessageHelper;

public class ParametrageHelper {

    private static String DEFAULT_PREFIX = "JOB_PARAM";

    public static String getMessage(String extensionId, String param) {
        return getMessage(DEFAULT_PREFIX, extensionId, param);
    }

    public static String getMessage(String prefix, String extensionId, String param) {
        final String key = String.format("BO.%s.%s.%s", StringUtils.upperCase(extensionId), prefix, StringUtils.upperCase(param));
        return StringUtils.defaultIfEmpty(MessageHelper.getExtensionMessage(extensionId, key), param);
    }
}
