package com.univ.utils;

import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.textsearch.ResultatRecherche;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;

public class RechercheUtil {

    private static final Logger LOG = LoggerFactory.getLogger(RechercheUtil.class);

    private static EspaceCollaboratifDAO getEspaceCollaboratifDao(){
        return ApplicationContextManager.getCoreContextBean(EspaceCollaboratifDAO.ID_BEAN, EspaceCollaboratifDAO.class);
    }

    /**
     * Traiter recherche.
     *
     * @param ctx     the ctx
     * @param request the request
     * @return the vector
     * @throws Exception the exception
     */
    public static Vector<ResultatRecherche> traiterRecherche(final OMContext ctx, final String request) throws Exception {
      return new Vector<>();
    }

    /**
     *
     * @param ctx
     * @param request
     * @return
     * @throws Exception
     */
    public static int getTotalResultats(final OMContext ctx, final String request) throws Exception {
        return 0;
    }




    /**
     * Test.
     *
     * @param word
     *            the word
     *
     * @return the string
     */
    private static String escape(final String word) {
        // + - && || ! ( ) { } [ ] ^ " ~ * ? : \ %
        final int lg = word.length();
        final StringBuilder sb = new StringBuilder(lg);
        for (int i = 0; i < lg; i++) {
            final char c = word.charAt(i);
            if (c == '+' || c == '-' || c == '&' || c == '|' || c == '!' || c == '\\' || c == '(' || c == ')' || c == '{' || c == '}' || c == '[' || c == ']' || c == '^' || c == '~' || c == '*' || c == '?' || c == ':' || c == '%') {
                sb.append('\\');
            }
            sb.append(c);
        }
        return sb.toString();
    }
}
