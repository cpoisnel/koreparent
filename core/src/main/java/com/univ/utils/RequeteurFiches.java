package com.univ.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.Objetpartage;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseJoin;
import com.univ.utils.sql.clause.ClauseJoin.TypeJointure;
import com.univ.utils.sql.clause.ClauseLimit;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ClauseJoinHelper;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.OrderByHelper;
import com.univ.utils.sql.criterespecifique.RequeteSQLHelper;
import com.univ.utils.sql.operande.TypeOperande;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * The Class RequeteurFiches.
 *
 * @author Rom1
 */
public class RequeteurFiches extends AbstractRequeteur {

    private static final Logger LOG = LoggerFactory.getLogger(RequeteurFiches.class);

    /**
     * Constructeur vide.
     */
    public RequeteurFiches() {
        super();
    }

    /**
     * Constructeur à partir de l'infoBean.
     *
     * @param infoBean the info bean
     */
    public RequeteurFiches(final InfoBean infoBean) {
        super(infoBean);
    }

    /**
     * Constructeur à partir d'une chaine de parametre.
     *
     * @param mode       the mode
     * @param selection  the selection
     * @param pagination the pagination
     * @param lien       the lien
     * @param requete    the requete
     */
    public RequeteurFiches(final String mode, final String selection, final String pagination, final String lien, final String requete) {
        super(mode, selection, pagination, lien, "0", requete);
    }

    /**
     * Constructeur à partir d'une chaine de parametre.
     *
     * @param mode       the mode
     * @param selection  the selection
     * @param pagination the pagination
     * @param lien       the lien
     * @param dossier    the dossier
     * @param requete    the requete
     */
    public RequeteurFiches(final String mode, final String selection, final String pagination, final String lien, final String dossier, final String requete) {
        super(mode, selection, pagination, lien, dossier, requete);
    }

    /**
     * Gets the nombre fiches collaboratives.
     *
     * @param _ctx        the _ctx
     * @param _codeObjet  the _code objet
     * @param _etatObjet  the _etat objet
     * @param _codeEspace the _code espace
     * @return the nombre fiches collaboratives
     * @throws Exception the exception
     * @deprecated Plus utilisé, à supprimer
     */
    @Deprecated
    public static int getNombreFichesCollaboratives(final OMContext _ctx, final String _codeObjet, final String _etatObjet, final String _codeEspace) throws Exception {
        final RequeteurFiches requeteur = new RequeteurFiches();
        final TreeMap<String, Object> listeMeta = requeteur.select(_codeObjet, _etatObjet, "", "", "", "", "", "", null, null, "", "4", _codeEspace, "", "", 0, 0, "", REQUETE_COUNT);
        return (Integer) listeMeta.get("" + NB_RESULTAT);
    }

    /**
     * Gets the nombre fiches.
     *
     * @param _ctx       the _ctx
     * @param _codeObjet the _code objet
     * @param _etatObjet the _etat objet
     * @return the nombre fiches
     * @throws Exception the exception
     */
    public static int getNombreFiches(final OMContext _ctx, final String _codeObjet, final String _etatObjet) throws Exception {
        final RequeteurFiches requeteur = new RequeteurFiches();
        final TreeMap<String, Object> listeMeta = requeteur.select(_codeObjet, _etatObjet, "", "", "", "", "", "", null, null, "", "", "", "", "", 0, 0, "", REQUETE_COUNT);
        return (Integer) listeMeta.get("" + NB_RESULTAT);
    }

    /**
     * Gets the liste fiches utilisateur.
     *
     * @param _ctx           the _ctx
     * @param _codeObjet     the _code objet
     * @param _codeRedacteur the _code redacteur
     * @return the liste fiches utilisateur
     * @throws Exception the exception
     */
    public static TreeMap<String, FicheUniv> getListeFichesUtilisateur(final OMContext _ctx, final String _codeObjet, final String _codeRedacteur) throws Exception {
        final RequeteurFiches requeteur = new RequeteurFiches();
        /* FG 20060223: Gestion du cache */
        final TreeMap<String, FicheUniv> result = new TreeMap<>();
        int compteur = 0;
        final TreeMap<String, Object> listeMeta = requeteur.select(_codeObjet, "0003", "", _codeRedacteur, "", "", "", "", null, null, "", "", "", "", "META_DATE_MODIFICATION DESC, META_CODE_OBJET, META_LIBELLE_FICHE", 0, 0, "", REQUETE_SANS_COUNT);
        FicheUniv ficheUniv;
        Long idFiche;
        String codeObjet;
        MetatagBean bean;
        for (String s : listeMeta.keySet()) {
            bean = (MetatagBean) listeMeta.get(s);
            idFiche = bean.getMetaIdFiche();
            codeObjet = bean.getMetaCodeObjet();
            ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(codeObjet));
            if (ficheUniv != null) {
                ficheUniv.init();
                ficheUniv.setCtx(_ctx);
                ficheUniv.setIdFiche(idFiche);
                try {
                    ficheUniv.retrieve();
                    result.put("" + (compteur++), ficheUniv);
                } catch (final Exception e) {
                    LOG.info("impossible de récupérer la fiche d'id " + idFiche, e);
                }
            }
        }
        return result;
    }

    public static ClauseWhere construireClauseWhere(final ContexteUniv ctxUniv, final String typeObjet, final String etatObjet, final String libelle, final String codeRedacteur, final String codeValidation, final String codeRattachement, final String codeRubrique, final String typeDate, final Date dateDebut, final Date dateFin, final String diffusionPublicVise, final String diffusionModeRestriction, final String diffusionPublicViseRestriction, final String langue, final String mode) {
        final ClauseWhere where = new ClauseWhere();
        if (isNotEmpty(etatObjet) && !"0000".equals(etatObjet)) {
            where.setPremiereCondition(ConditionHelper.egalVarchar("T1.META_ETAT_OBJET", etatObjet));
        }
        if (isNotEmpty(libelle)) {
            where.and(ConditionHelper.like("T1.META_LIBELLE_FICHE", libelle, "%", "%"));
        }
        if (isNotEmpty(codeRedacteur)) {
            where.and(traiterCodeRedacteur(codeRedacteur));
        }
        if (isNotEmpty(codeValidation)) {
            where.and(ConditionHelper.egalVarchar("T1.META_CODE_VALIDATION", codeValidation));
        }
        if (isNotEmpty(langue) && !"0000".equals(langue)) {
            where.and(ConditionHelper.egalVarchar("T1.META_LANGUE", langue));
        }
        if (isNotEmpty(codeRattachement)) {
            final ConditionList conditionStructure = new ConditionList(ConditionHelper.getConditionStructure("T1.META_CODE_RATTACHEMENT", codeRattachement));
            conditionStructure.or(ConditionHelper.getConditionStructureMultiple("T1.META_CODE_RATTACHEMENT", codeRattachement));
            where.and(conditionStructure);
        }
        if (isNotEmpty(diffusionPublicVise)) {
            final Set<GroupeDsiBean> listeGroupes = determinerListeGroupeDiffusion(diffusionPublicVise);
            where.and(traiterDiffusionPublicVise(listeGroupes));
        }
        if (isDiffusionModeRestriction(diffusionModeRestriction)) {
            where.and(ConditionHelper.egalVarchar("T1.META_DIFFUSION_MODE_RESTRICTION", diffusionModeRestriction));
        } else if (isEmpty(diffusionModeRestriction)) {
            // par défaut on exclut les objets du collaboratif
            where.and(ConditionHelper.notEgal("T1.META_DIFFUSION_MODE_RESTRICTION", "4", TypeOperande.VARCHAR));
        }
        if (isNotEmpty(diffusionPublicViseRestriction)) {
            if ("4".equals(diffusionModeRestriction)) {
                where.and(ConditionHelper.egalVarchar("T1.META_DIFFUSION_PUBLIC_VISE_RESTRICTION", diffusionPublicViseRestriction));
            } else {
                final Set<GroupeDsiBean> listeGroupes = determinerListeGroupeDiffusion(diffusionPublicViseRestriction);
                where.and(traiterDiffusionPublicViseRestriction(listeGroupes));
            }
        }
        if (isNotEmpty(typeDate)) {
            where.and(traiterTypeDate(typeDate, dateDebut, dateFin));
        }
        where.and(traiterCritereGeneriques(ctxUniv, mode, typeObjet, codeRattachement, codeRubrique, diffusionPublicVise, diffusionModeRestriction, diffusionPublicViseRestriction));
        return where;
    }

    /**
     * Le coderedacteur contient plusieurs codes rédacteurs séparé par des ":". Une fois les codes récupérés, ils peuvent commencer par SANS_CODE_DYNAMIQUE ce qui fait que le code
     * du rédacteur ne doit pas être égal à la valeur fourni.
     *
     * @param codesRedacteurs
     * @return
     */
    private static ConditionList traiterCodeRedacteur(String codesRedacteurs) {
        final ConditionList conditionSurredacteur = new ConditionList();
        codesRedacteurs = codesRedacteurs.toUpperCase();
        final String[] codesSplites = codesRedacteurs.split(":");
        for (String codeRedacteur : codesSplites) {
            if (codeRedacteur.startsWith(SANS_CODE_DYNAMIQUE)) {
                codeRedacteur = codeRedacteur.substring((SANS_CODE_DYNAMIQUE).length());
                conditionSurredacteur.or(ConditionHelper.notEgal("T1.META_CODE_REDACTEUR", codeRedacteur, TypeOperande.VARCHAR));
            } else {
                conditionSurredacteur.or(ConditionHelper.egalVarchar("T1.META_CODE_REDACTEUR", codeRedacteur));
            }
        }
        return conditionSurredacteur;
    }

    /**
     * diffusionPublicVise peut contenir plusieurs code de groupeDSI séparé par des ";". Une fois splité, on récupére l'ensemble des sous groupeDSI de la liste fourni.
     *
     * @param diffusionPublicVise
     * @return
     */
    private static Set<GroupeDsiBean> determinerListeGroupeDiffusion(final String diffusionPublicVise) {
        final Set<GroupeDsiBean> listeDesGroupes = new HashSet<>();
        final String[] groupes = diffusionPublicVise.split(";");
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        for (final String codeGroupe : groupes) {
            listeDesGroupes.add(serviceGroupeDsi.getByCode(codeGroupe));
            listeDesGroupes.addAll(serviceGroupeDsi.getAllSubGroups(codeGroupe));
        }
        return listeDesGroupes;
    }

    /**
     * traite le champ META_DIFFUSION_PUBLIC_VISE pour la liste de groupes fournis en paramètre.
     *
     * @param listeGroupes
     * @return
     */
    private static Condition traiterDiffusionPublicVise(final Set<GroupeDsiBean> listeGroupes) {
        return construitConditionDiffusion(listeGroupes, "T1.META_DIFFUSION_PUBLIC_VISE");
    }

    /**
     * Les conditions sur les champs META_DIFFUSION_PUBLIC_VISE_RESTRICTION et META_DIFFUSION_PUBLIC_VISE sont identiques. Elles consistent à un ensemble de condition OR like %[/
     * valeur ]% .
     *
     * @param listeGroupes
     * @param nomColonne
     * @return
     */
    private static Condition construitConditionDiffusion(final Set<GroupeDsiBean> listeGroupes, final String nomColonne) {
        final ConditionList conditionCodeDiffusion = new ConditionList();
        for (final GroupeDsiBean group : listeGroupes) {
            conditionCodeDiffusion.or(ConditionHelper.like(nomColonne, group.getCode(), "%[/", "]%"));
        }
        return conditionCodeDiffusion;
    }

    /**
     * traite le champ META_DIFFUSION_PUBLIC_VISE_RESTRICTION pour la liste de groupes fournis en paramètre.
     *
     * @param listeGroupes
     * @return
     */
    private static Condition traiterDiffusionPublicViseRestriction(final Set<GroupeDsiBean> listeGroupes) {
        return construitConditionDiffusion(listeGroupes, "T1.META_DIFFUSION_PUBLIC_VISE_RESTRICTION");
    }

    /**
     * Verifie si l'on gère le champ DIFFUSION_MODE_RESTRICTION ou non
     *
     * @param diffusionModeRestriction
     * @return
     */
    private static boolean isDiffusionModeRestriction(final String diffusionModeRestriction) {
        return "0".equals(diffusionModeRestriction) || "1".equals(diffusionModeRestriction) || "2".equals(diffusionModeRestriction) || "3".equals(diffusionModeRestriction) || "4".equals(diffusionModeRestriction);
    }

    public static TreeMap<String, Object> executerRequete(final RequeteSQL requeteSelect, final int from, final int increment) {
        final TreeMap<String, Object> resultatsMeta = new TreeMap<>();
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        int inc = -1;
        RequeteSQL requeteAExecuter;
        try {
            requeteAExecuter = requeteSelect.clone();
        } catch (CloneNotSupportedException e) {
            LOG.error("unable to clone the request", e);
            requeteAExecuter = requeteSelect;
        }
        if (increment != 0) {
            if (ctx.getDatas().get("optimizedLimit") != null) {
                ctx.getDatas().put("optimizedLimit", String.valueOf(increment));
            } else {
                requeteAExecuter.limit(new ClauseLimit(from, increment));
            }
            inc = increment;
        }
        ctx.getDatas().put("optimizedSelect", "1");
        int compteur = NB_RESULTAT + 1;
        final ServiceMetatag  serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final List<MetatagBean> metas = serviceMetatag.getFromRequest(requeteAExecuter);
        final Iterator<MetatagBean> itMeta = metas.iterator();
        final Collection<Long> metaAlreadyAdded = new ArrayList<>();
        while (itMeta.hasNext() && (inc == -1 || resultatsMeta.size() < inc)) {
            final MetatagBean currentMeta = itMeta.next();
            if (!metaAlreadyAdded.contains(currentMeta.getId())) {
                metaAlreadyAdded.add(currentMeta.getId());
                resultatsMeta.put(String.valueOf(compteur), currentMeta);
                compteur++;
            }
        }
        ctx.getDatas().remove("optimizedSelect");
        return resultatsMeta;
    }

    /**
     * dsl...
     *
     * @param where
     * @param clauseOrderBy
     * @return
     */
    public static Collection<ClauseJoin> traiterJointureRequeteGenerique(final ClauseWhere where, final ClauseOrderBy clauseOrderBy) {
        final Collection<ClauseJoin> listeJointures = new ArrayList<>();
        if (where.formaterSQL().contains("RUB_PUB.")) {
            final ClauseJoin joinRubPub = new ClauseJoin(TypeJointure.LEFT_JOIN, "RUBRIQUEPUBLICATION RUB_PUB");
            joinRubPub.on(ConditionHelper.critereJointureSimple("T1.META_CODE", "RUB_PUB.CODE_FICHE_ORIG"));
            joinRubPub.and(ConditionHelper.critereJointureSimple("T1.META_LANGUE", "RUB_PUB.LANGUE_FICHE_ORIG"));
            joinRubPub.and(ConditionHelper.critereJointureSimple("T1.META_CODE_OBJET", "RUB_PUB.TYPE_FICHE_ORIG"));
            listeJointures.add(joinRubPub);
        }
        final String orderByFormater = clauseOrderBy.formaterSQL();
        if (orderByFormater.contains("RUB.")) {
            final ClauseJoin joinRub = ClauseJoinHelper.creerJointure(TypeJointure.LEFT_JOIN, "RUBRIQUE RUB", ConditionHelper.critereJointureSimple("RUB.CODE", "T1.META_CODE_RUBRIQUE"));
            listeJointures.add(joinRub);
        }
        if (orderByFormater.contains("S.")) {
            final Collection<String> codesTypeStructures = ReferentielObjets.getReferentiel().getCodesObjetsStructure();
            if (CollectionUtils.isNotEmpty(codesTypeStructures)) {
                final ClauseJoin joinStructure = new ClauseJoin();
                final String premiereStructureDefini = codesTypeStructures.iterator().next();
                String nomClasse = ReferentielObjets.getClasseObjet(premiereStructureDefini);
                joinStructure.setTypeJointure(TypeJointure.LEFT_JOIN);
                nomClasse = nomClasse.substring(nomClasse.lastIndexOf(".") + 1);
                joinStructure.setNomTable(nomClasse.toUpperCase() + " S");
                joinStructure.on(ConditionHelper.critereJointureSimple("S.CODE", "T1.META_CODE_RATTACHEMENT"));
                listeJointures.add(joinStructure);
            }
        }
        if (orderByFormater.contains("U.")) {
            final ClauseJoin joinUtilisateur = new ClauseJoin(TypeJointure.LEFT_JOIN, "UTILISATEUR U");
            joinUtilisateur.on(ConditionHelper.critereJointureSimple("U.CODE", "T1.META_CODE_REDACTEUR"));
            joinUtilisateur.or(ConditionHelper.egalVarchar("T1.META_CODE_REDACTEUR", ""));
            listeJointures.add(joinUtilisateur);
        }
        return listeJointures;
    }

    /**
     * Le nom du champ date est fourni en paramètre dans typeDate. Il faut que la date soit comprise entre la date de début et la date de fin(exclus). Si la date de début n'est pas
     * saisie, c'est 01/01/1970 et si la date de fin n'est pas saisie, c'est la date du jour.
     *
     * @param typeDate
     * @param dateDebut
     * @param dateFin
     * @return
     */
    private static Condition traiterTypeDate(final String typeDate, Date dateDebut, Date dateFin) {
        final ConditionList conditionSurDate = new ConditionList();
        if (Formateur.estSaisie(dateDebut) || Formateur.estSaisie(dateFin)) {
            if (!Formateur.estSaisie(dateDebut)) {
                dateDebut = new Date(0);
            }
            if (!Formateur.estSaisie(dateFin)) {
                dateFin = new Date(System.currentTimeMillis());
            }
            conditionSurDate.setPremiereCondtion(ConditionHelper.greaterEquals("T1.META_" + typeDate, dateDebut, TypeOperande.DATE_ET_HEURE));
            conditionSurDate.and(ConditionHelper.lessEquals("T1.META_" + typeDate, dateFin, TypeOperande.DATE_ET_HEURE));
        }
        return conditionSurDate;
    }

    /**
     * Gère un ensemble conséquent de critère équivalent de l'ancienne méthode AbstractRequeteur.ajouterCriteresGeneriques(...)
     *
     * @param ctxUniv
     * @param mode
     * @param typeObjet
     * @param codeStructure
     * @param codeRubrique
     * @param diffusionPublicVise
     * @param diffusionModeRestriction
     * @param diffusionPublicViseRestriction
     * @return
     */
    public static Condition traiterCritereGeneriques(final ContexteUniv ctxUniv, final String mode, final String typeObjet, final String codeStructure, final String codeRubrique, final String diffusionPublicVise, String diffusionModeRestriction, final String diffusionPublicViseRestriction) {
        final ConditionList conditionGenerique = new ConditionList();
        final AutorisationBean autorisations = getAutorisationBean(ContexteUtil.getContexteUniv());
        if (autorisations == null && (mode.equals(MODE_MODIFICATION) || mode.equals(MODE_VALIDATION))) {
            return ConditionHelper.egalVarchar("T1.META_CODE_OBJET", "0000");
        }
        final Collection<String> listeDesObjets = recupererTypesObjets(typeObjet, diffusionModeRestriction);
        diffusionModeRestriction = despecialiserModeRestriction(diffusionModeRestriction);
        conditionGenerique.setPremiereCondtion(traiterListeDesObjets(listeDesObjets, autorisations, mode, codeStructure, codeRubrique, diffusionPublicVise, diffusionPublicViseRestriction, diffusionModeRestriction));
        // DSI en front - On gère la DSI en parcourant les résultats
        if (mode.equals(MODE_CONSULTATION)) {
            final Condition conditiionDSI = RequeteSQLHelper.construireConditionDSI(ctxUniv, diffusionPublicVise, diffusionModeRestriction, diffusionPublicViseRestriction, null);
            conditionGenerique.and(conditiionDSI);
        }
        // rubrique de publication
        if (!"4".equals(diffusionModeRestriction)) {
            final Condition conditionRubriquePublication = ConditionHelper.getConditionRubPubSuivantAction(ctxUniv, mode, "T1.META_CODE_RUBRIQUE", codeRubrique);
            conditionGenerique.and(conditionRubriquePublication);
        }
        if (mode.equals(MODE_CONSULTATION)) {
            final Date aujourdhui = new Date(System.currentTimeMillis());
            conditionGenerique.and(ConditionHelper.lessEquals("T1.META_DATE_MISE_EN_LIGNE", aujourdhui, TypeOperande.DATE));
        }
        if (mode.equals(MODE_MODIFICATION)) {
            final ConditionList conditionEtatObjet = new ConditionList(ConditionHelper.egalVarchar("T1.META_ETAT_OBJET", "0001"));
            conditionEtatObjet.or(ConditionHelper.egalVarchar("T1.META_ETAT_OBJET", "0002"));
            conditionEtatObjet.or(ConditionHelper.egalVarchar("T1.META_ETAT_OBJET", "0003"));
            conditionGenerique.and(conditionEtatObjet);
        }
        return conditionGenerique;
    }

    /**
     * Retourne le bean des autorisation à partir du contexte fourni.
     *
     * @param ctx
     * @return
     */
    private static AutorisationBean getAutorisationBean(final OMContext ctx) {
        AutorisationBean autorisations = null;
        if (ctx.getDatas() != null) {
            autorisations = (AutorisationBean) ctx.getDatas().get("AUTORISATIONS");
        }
        if (autorisations == null && ctx instanceof ContexteUniv) {
            autorisations = ((ContexteUniv) ctx).getAutorisation();
        }
        return autorisations;
    }

    /**
     * Récupère les objets à traiter
     *
     * @param typeObjet
     * @param diffusionModeRestriction
     * @return
     */
    private static Collection<String> recupererTypesObjets(final String typeObjet, final String diffusionModeRestriction) {
        Collection<String> typesDesObjetsChercher = new ArrayList<>();
        if (isEmpty(typeObjet) || "0000".equals(typeObjet)) {
            for (final Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
                if ((isEmpty(diffusionModeRestriction) && objet.isStrictlyCollaboratif()) || ("4".equals(diffusionModeRestriction) && !objet.isCollaboratif())) {
                    continue;
                }
                typesDesObjetsChercher.add(objet.getNomObjet());
            }
        } else {
            typesDesObjetsChercher = Arrays.asList(typeObjet.split(","));
        }
        return typesDesObjetsChercher;
    }

    /**
     * FIXME : pourquoi?
     *
     * @param diffusionModeRestriction
     * @return
     */
    private static String despecialiserModeRestriction(final String diffusionModeRestriction) {
        if ("+4".equals(diffusionModeRestriction)) {
            return "";
        }
        return diffusionModeRestriction;
    }

    /**
     * @see com.univ.utils.AbstractRequeteur#select(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String, Date, Date, java.lang.String, java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String, int, int, java.lang.String, java.lang.String)
     */
    @Override
    public TreeMap<String, Object> select(final String typeObjet, final String etatObjet, final String libelle, final String codeRedacteur, final String codeValidation, final String codeRattachement, final String codeRubrique, final String typeDate, final Date dateDebut, final Date dateFin, final String diffusionPublicVise, final String diffusionModeRestriction, final String diffusionPublicViseRestriction, final String langue, final String orderBy, final int from, final int increment, final String mode, final String count) {
        final ContexteUniv ctxUniv = ContexteUtil.getContexteUniv();
        TreeMap<String, Object> hashResultat = new TreeMap<>();
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseWhere where = construireClauseWhere(ctxUniv, typeObjet, etatObjet, libelle, codeRedacteur, codeValidation, codeRattachement, codeRubrique, typeDate, dateDebut, dateFin, diffusionPublicVise, diffusionModeRestriction, diffusionPublicViseRestriction, langue, mode);
        requeteSelect.where(where);
        final ClauseOrderBy clauseOrderBy = OrderByHelper.reconstruireClauseOrderBy(orderBy);
        requeteSelect.orderBy(clauseOrderBy);
        requeteSelect.setJointures(traiterJointureRequeteGenerique(where, clauseOrderBy));
        if (!count.equals(REQUETE_COUNT)) {
            hashResultat = executerRequete(requeteSelect, from, increment);
        }
        if (count.equals(REQUETE_COUNT) || count.equals(REQUETE_AVEC_COUNT)) {
            ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
            final int total = serviceMetatag.getFromRequest(requeteSelect).size();
            hashResultat.put(String.valueOf(NB_RESULTAT), total);
        }
        return hashResultat;
    }
}
