package com.univ.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.kportal.core.webapp.WebAppUtil;
// TODO: Auto-generated Javadoc
/**
 * The Class FileReaderServlet.
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */

/**
 * Accès à un fichier de reporting
 */
public class FileReaderServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -2328660109979013684L;

    private static final Logger LOG = LoggerFactory.getLogger(FileReaderServlet.class);

    /**
     * Process incoming HTTP GET requests.
     *
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void doGet(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
        performTask(request, response);
    }

    /**
     * Process incoming HTTP POST requests.
     *
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void doPost(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
        performTask(request, response);
    }

    /**
     * Process incoming requests for information.
     *
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     */
    public void performTask(final HttpServletRequest request, final HttpServletResponse response) {
        int status = 0;
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        String file = request.getParameter("FILE") != null ? request.getParameter("FILE") : "";
        String fileName = request.getParameter("NAME") != null ? request.getParameter("NAME") : "";
        String contentType = request.getParameter("TYPE") != null ? request.getParameter("TYPE") : "";
        String contentDisposition = "";
        String filePath = "";
        if (!file.contains("../")) {
            final String serverPath = WebAppUtil.getDownloadPrivatePath();
            if (StringUtils.isEmpty(file) && StringUtils.isNotEmpty(fileName)) {
                file = fileName;
            }
            if (StringUtils.isEmpty(fileName) && StringUtils.isNotEmpty(file)) {
                fileName = (file.contains("/") ? StringUtils.substringAfterLast(file, "/") : file);
            }
            contentDisposition = "attachment;filename=\"" + fileName + "\"";
            filePath = serverPath + File.separator + file;
            if (StringUtils.isEmpty(contentType)) {
                contentType = "text/plain";
            }
        }
        final File f = new File(filePath);
        if (f.exists()) {
            try (final ServletOutputStream writer = response.getOutputStream()) {
                response.setContentType(contentType);
                response.setCharacterEncoding(CharEncoding.DEFAULT.toLowerCase());
                if (contentDisposition.length() > 0) {
                    response.setHeader("Content-Disposition", contentDisposition);
                }
                ecrireContenuDuFichier(writer, f);
            } catch (final IOException e) {
                LOG.error("Erreur lors de la lecture/ecriture du fichier", e);
            }
        } else if (filePath.length() > 0) {
            status = HttpServletResponse.SC_NOT_FOUND;
        }
        if (status != 0) {
            response.setStatus(status);
            String pageErreur = "";
            if (status == HttpServletResponse.SC_NOT_FOUND) {
                pageErreur = ctx.getInfosSite().getJspFo() + "/error/404.jsp";
            } else {
                pageErreur = ctx.getInfosSite().getJspFo() + "/error/403.jsp";
            }
            try {
                final javax.servlet.RequestDispatcher rd = getServletContext().getRequestDispatcher(pageErreur);
                rd.forward(request, response);
            } catch (final ServletException | IOException e) {
                LOG.error("erreur lors du forward de la request", e);
            }
        }
    }

    private void ecrireContenuDuFichier(final ServletOutputStream writer, final File f) throws IOException {
        try (FileInputStream fis = new FileInputStream(f); BufferedInputStream bis = new BufferedInputStream(fis)) {
            int nbBytes;
            final byte[] buf = new byte[1024 * 4];
            while ((nbBytes = bis.read(buf)) > 0) {
                writer.write(buf, 0, nbBytes);
            }
        } catch (final FileNotFoundException e) {
            LOG.error("Erreur lors de la lecture du fichier", e);
            writer.write("Erreur lors de la lecture du fichier".getBytes());
        }
    }
}
