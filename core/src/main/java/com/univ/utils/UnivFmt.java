package com.univ.utils;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.CodeLibelle;
import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.jsbsoft.jtf.textsearch.sitesdistants.CibleRecherche;
import com.jsbsoft.jtf.textsearch.sitesdistants.RechercheSitesDistants;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.toolbox.config.CkeditorConfigurerUtils;
import com.kosmos.toolbox.service.CkeditorConfigurationService;
import com.kosmos.toolbox.utils.ToolboxHelper;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantUtilisateur;
import com.kportal.extension.module.plugin.toolbox.PluginToolboxHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.tree.processus.GroupsJsTree;
import com.univ.tree.processus.RubriquesJsTree;
import com.univ.tree.processus.StructuresJsTree;

/**
 * Fonction de formatage des JSP pour l'administration.
 */
public class UnivFmt {

    /**
     * Constante représentant un type de toolbar backoffice
     */
    public static final String TYPE_TOOLBAR_BACKOFFICE = "BackOffice";
    // Types de liste

    /** The Constant TYPE_LISTE_INPUT. */
    public static final int TYPE_LISTE_INPUT = 0;

    /** The Constant TYPE_LISTE_COMBO. */
    public static final int TYPE_LISTE_COMBO = 1;

    /** The Constant TYPE_LISTE_RECHERCHE. */
    public static final int TYPE_LISTE_RECHERCHE = 2;

    public static final int CONTEXT_DEFAULT = 0;

    public static final int CONTEXT_STRUCTURE = 1;

    public static final int CONTEXT_ZONE = 2;

    public static final int CONTEXT_GROUPEDSI_RESTRICTION = 3;

    public static final int CONTEXT_GROUPEDSI_PUBLIC_VISE = 4;

    public static final int CONTEXT_STRUCTURES_ENSEIGNEMENT = 6;

    private static final Pattern selectPattern = Pattern.compile("([/]*[a-zA-Z0-9\\-]*)/([\\S]*)");

    //Mapping des urls particulirères
    private static final Map<String, String> URLS = new HashMap<>();

    /** The Constant GROUPEDSI_RESTRICTION. */
    private static final int GROUPEDSI_RESTRICTION = 0;

    /** The Constant GROUPEDSI_PUBLIC_VISE. */
    private static final int GROUPEDSI_PUBLIC_VISE = 1;

    private static CkeditorConfigurationService getCkeditorConfigurationService(){
        return ApplicationContextManager.getCoreContextBean(CkeditorConfigurationService.ID_BEAN, CkeditorConfigurationService.class);
    }

    static {
        URLS.put("utilisateur", WebAppUtil.SG_PATH + "?PROC=SAISIE_UTILISATEUR&ACTION=RECHERCHER&MODE=RECHERCHE&TOOLBOX=TRUE");
        URLS.put("pagelibre", WebAppUtil.SG_PATH + "?PROC=SAISIE_PAGELIBRE&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE_JOINTURE&LANGUE_FICHE=0");
        URLS.put("pagelibre_creation", WebAppUtil.SG_PATH + "?PROC=TRAITEMENT_PAGELIBRE&ACTION=AJOUTER&CODE_RUBRIQUE=LIBELLE_CODE_PAGE_TETE");
    }

    /**
     * Renvoie true si une zone est saisissable en fonction du - du mode de saisie (MODIFICATION, CREATION) - du paramétrage dans jtf.properties - de la langue
     *
     * @param infoBean
     *            the info bean
     * @param nomZone
     *            the nom zone
     *
     * @return true, if est saisissable
     */
    public static boolean estSaisissable(final InfoBean infoBean, final String nomZone) {
        final int mode = UnivFmt.getModeSaisieZone(infoBean, nomZone);
        return mode == FormateurJSP.SAISIE_FACULTATIF || mode == FormateurJSP.SAISIE_OBLIGATOIRE;
    }

    /**
     * AM 200501 : criteres de recherche backoffice parametrables pour Formation et Ue renvoie facultatif ou invisible pour une zone dans les formulaires de recherche.
     *
     * @param infoBean
     *            the infobean
     * @param nomZone
     *            the nom zone
     *
     * @return the mode recherche zone
     */
    public static int getModeRechercheZone(final InfoBean infoBean, final String nomZone) {
        int mode = FormateurJSP.SAISIE_FACULTATIF;
        String s = PropertyHelper.getProperty(infoBean.getNomExtension(), "recherche." + nomZone + ".INVISIBLE");
        if ("1".equals(s)) {
            mode = -1;
        } else {
            // si la zone est désactivée en saisie, on renvoie -1 également sur
            // la page de recherche
            s = PropertyHelper.getProperty(infoBean.getNomExtension(), "saisie." + nomZone + ".INVISIBLE");
            if ("1".equals(s)) {
                mode = -1;
            }
        }
        return mode;
    }

    /**
     * Renvoie le mode de saisie pour une zone en fonction de: - du mode de saisie (MODIFICATION, CREATION) - du paramétrage dans jtf.properties - de la langue
     *
     * @param infoBean
     *            the info bean
     * @param nomZone
     *            the nom zone
     *
     * @return the mode saisie zone
     */
    public static int getModeSaisieZone(final InfoBean infoBean, final String nomZone) {
        int modeSaisieDonneesImportes = -1;
        boolean visible = !"1".equals(PropertyHelper.getProperty(infoBean.getNomExtension(), "saisie." + nomZone + ".INVISIBLE"));
        boolean modifiable = !"0".equals(PropertyHelper.getProperty(infoBean.getNomExtension(), "saisie." + nomZone + ".MODIFIABLE"));
        boolean obligatoire = "0".equals(PropertyHelper.getProperty(infoBean.getNomExtension(), "saisie." + nomZone + ".FACULTATIF"));
        if (!visible) {
            return -1;
        }
        // si la fiche vient d'être dupliquée ou traduite, on est comme en mode
        // CREATION
        if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet()) || InfoBean.ACTION_DUPLIQUER.equals(infoBean.getActionUtilisateur()) || InfoBean.ACTION_TRADUIRE.equals(infoBean.getActionUtilisateur()) || infoBean.get("PREMIERE_ACTION") != null) {
            modeSaisieDonneesImportes = FormateurJSP.SAISIE_OBLIGATOIRE;
            if (!obligatoire) {
                modeSaisieDonneesImportes = FormateurJSP.SAISIE_FACULTATIF;
            }
        } else if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {
            modeSaisieDonneesImportes = FormateurJSP.SAISIE_AFFICHAGE;
            if (modifiable) {
                if (!obligatoire) {
                    modeSaisieDonneesImportes = FormateurJSP.SAISIE_FACULTATIF;
                } else {
                    modeSaisieDonneesImportes = FormateurJSP.SAISIE_OBLIGATOIRE;
                }
            }
        }
        return modeSaisieDonneesImportes;
    }

    /**
     * Renvoie le mode de saisie pour un code ou une langue (obligatoire en création, affiché en modif).
     *
     * @param infoBean
     *            the info bean
     *
     * @return the mode saisie code
     */
    public static int getModeSaisieCode(final InfoBean infoBean) {
        int mode = FormateurJSP.SAISIE_OBLIGATOIRE;
        // si la fiche est en état MODIF et qu'elle ne vient pas d'être dupliquée
        // le troisième test sert sur changement d'onglet lorsqu'on vient de dupliquer :
        // on doit pouvoir revenir sur l'onglet principal et modifier le code
        if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF) && !infoBean.getActionUtilisateur().equals(InfoBean.ACTION_DUPLIQUER) && !InfoBean.ACTION_DUPLIQUER.equals(infoBean.get("PREMIERE_ACTION"))) {
            mode = FormateurJSP.SAISIE_AFFICHAGE;
        }
        return mode;
    }

    /**
     * Checks if is style zone.
     *
     * @param infoBean
     *            the infobean
     * @param nomZone
     *            the nom zone
     *
     * @return true, if is style zone
     */
    public static boolean isStyleZone(final InfoBean infoBean, final String nomZone) {
        if ("1".equals(PropertyHelper.getProperty(infoBean.getNomExtension(), "saisie.STYLE"))) {
            return true;
        } else if ("1".equals(PropertyHelper.getProperty(infoBean.getNomExtension(), "saisie." + nomZone + ".STYLE"))) {
            return true;
        }
        return false;
    }

    /**
     * Inserer combo sites.
     *
     * @param ctx
     *            the ctx
     * @param out
     *            the out
     * @param sitesLocaux
     *            the sites locaux
     * @param sitesDistants
     *            the sites distants
     * @param selectedOption
     *            the selected option
     *
     * @throws IOException
     *             the exception
     */
    public static void insererComboSites(final ContexteUniv ctx, final Writer out, final boolean sitesLocaux, final boolean sitesDistants, String selectedOption) throws IOException {
        if (sitesDistants || sitesLocaux) {
            if ("".equals(selectedOption) && ctx.getInfosSite() != null) {
                selectedOption = ctx.getInfosSite().getCodeRubrique();
            }
            out.write("<option value=\"TOUS\">Tous</option>");
            if (sitesLocaux) {
                out.write("<option value=\"\">--------------------</option>");
                final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
                for (InfosSite infosSite : serviceInfosSite.getSitesList().values()) {
                    String selection = "";
                    if (infosSite.getCodeRubrique().equals(selectedOption)) {
                        selection = "selected=\"selected\"";
                    }
                    out.write("<option value=\"" + infosSite.getAlias() + "\" " + selection + ">" + infosSite.getIntitule() + "</option>");
                }
            }
            if (sitesDistants) {
                final RechercheSitesDistants recherche = new RechercheSitesDistants();
                final Collection<CibleRecherche> lstCible = recherche.getLstCible();
                out.write("<option value=\"\">--------------------</option>");
                for (CibleRecherche cible : lstCible) {
                    String selection = "";
                    if (cible.getCode().equals(selectedOption)) {
                        selection = "selected=\"selected\"";
                    }
                    out.write("<option value=\"" + cible.getCode() + "\" " + selection + ">" + cible.getLibelle() + "</option>\n");
                }
            }
        }
    }

    /**
     * Inserer combo objets.
     *
     * @param ctx
     *            the ctx
     * @param out
     *            the out
     *
     * @throws IOException
     *             the exception
     */
    public static void insererComboObjets(final ContexteUniv ctx, final Writer out) throws IOException {
        out.write("<option value=\"TOUS\">Dans :</option>");
        out.write("<option value=\"TOUS\">Toutes les fiches</option>");
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            out.write("<option value=\"" + ReferentielObjets.getNomObjet(codeObjet) + "\">" + ReferentielObjets.getLibelleObjet(codeObjet) + "</option>");
        }
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param size
     *            the size
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererChampSaisie(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final int size, final String nomZone) throws IOException {
        insererChampSaisie(fmt, out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, size, nomZone, "", false, "");
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererChampSaisie(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final String nomZone) throws IOException {
        insererChampSaisie(fmt, out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, nomZone, "");
    }

    /**
     * Insère le code HTML d'une combo dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomTable
     *            the nom table
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererCombo(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomTable, final String nomZone) throws IOException {
        insererCombo(fmt, out, infoBean, nomDonnee, optionModification, nomTable, nomZone, "");
    }

    /**
     * Insère le code HTML d'une combo dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomTable
     *            the nom table
     * @param nomZone
     *            the nom zone
     * @param tricle
     *            the tricle
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererCombo(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomTable, final String nomZone, final String tricle) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        fmt.insererCombo(out, infoBean, nomDonnee, optionModification, nomTable, "8pt,LIB=" + nomZone + tricle);
        insererFinChamp(out);
    }

    /**
     * Insère le code HTML d'une combo dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param map
     *            the map
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererComboHashtable(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final Map<String, String> map, final String nomZone) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        fmt.insererCombo(out, infoBean, nomDonnee, optionModification, map, "8pt,LIB=" + nomZone);
        insererFinChamp(out);
    }

    /**
     * Insère le code HTML d'une combo dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param map
     *            the map
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuComboHashtable(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final Map<String, String> map, final String nomZone) throws IOException {
        fmt.insererCombo(out, infoBean, nomDonnee, optionModification, map, "8pt,LIB=" + nomZone);
    }

    /**
     * Insère le code HTML d'une combo dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomHashtable
     *            the nom hashtable
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererComboHashtable(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomHashtable, final String nomZone) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        fmt.insererComboHashtable(out, infoBean, nomDonnee, optionModification, nomHashtable, "8pt,LIB=" + nomZone);
        insererFinChamp(out);
    }

    /**
     * Insertion de radio-boutons (non limité maintenant!).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @deprecated ne pas utiliser car le code html généré n'est pas valide (utilisation de balise p englobant une liste ul li)
     */
    @Deprecated
    public void insererRadiosBoutons(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        insererContenuRadiosBoutons(fmt, out, infoBean, nomDonnee, optionModification);
        insererFinChamp(out);
    }

    /**
     * Insertion de radio-boutons (non limité maintenant!).
     *
     * @param ordreElement
     *            : FormateurJSP.ORDRE_NORMAL affiche d'abord le libellé puis le bouton radio / FormateurJSP.ORDRE_INVERSE
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @deprecated ne pas utiliser car le code html généré n'est pas valide (utilisation de balise p englobant une liste ul li)
     */
    @Deprecated
    public void insererRadiosBoutons(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final int ordreElement) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone);
        insererContenuRadiosBoutons(fmt, out, infoBean, nomDonnee, optionModification, ordreElement);
        insererFinChamp(out);
    }

    /**
     * Inserer contenu radios boutons.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuRadiosBoutons(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification) throws IOException {
        insererContenuRadiosBoutons(fmt, out, infoBean, nomDonnee, optionModification, FormateurJSP.ORDRE_INVERSE);
    }

    /**
     * Inserer contenu radios boutons.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param ordreElements
     *            the ordre elements
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuRadiosBoutons(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int ordreElements) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        int i = 0;
        // on ordonne les codes puisqu'ils commencent tous par un chiffre
        final Set<String> tree = new TreeSet<>();
        while (infoBean.get("RADIO" + i) != null) {
            tree.add(i + "#" + infoBean.get("RADIO" + i));
            infoBean.set("RADIO" + i, null);
            i++;
        }
        i = 0;
        out.println("<ul class=\"en_ligne sans_puce\">");
        for (String libelle : tree) {
            String value = "";
            String checked = "";
            libelle = libelle.substring(libelle.indexOf("#") + 1);
            if (libelle.contains("#")) {
                value = libelle.substring(libelle.indexOf("#") + 1);
                libelle = libelle.substring(0, libelle.indexOf("#"));
            } else {
                value = Integer.toString(i);
            }
            if (value.equals(infoBean.getString(nomDonnee))) {
                checked = "checked=\"checked\"";
            }
            out.println("<li>");
            if (FormateurJSP.SAISIE_AFFICHAGE != optionModification) {
                out.println("<input id=\"" + nomDonnee + i + "\" type=\"radio\" name=\"" + nomDonnee + "\" value=\"" + value + "\" " + checked + " />");
            } else {
                out.println("<input id=\"" + nomDonnee + i + "\" type=\"radio\" disabled=\"disabled\" name=\"" + nomDonnee + "\" value=\"" + value + "\" " + checked + " />");
            }
            out.println("<label for=\"" + nomDonnee + i + "\">" + libelle + "</label>");
            out.println("</li>");
            i++;
        }
        out.println("</ul>");
    }

    /**
     * Insère le code HTML d'une saisie de structure la page JSP
     *
     * Le libellé, si le code est prévalorisé doit être dans la donnée LIBELLE_[NOMCODE).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             the exception
     */
    public void insererContenuSaisieStructure(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone) throws IOException {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        String res = "";
        String libelle = "";
        if (infoBean.get("LIBELLE_" + nomDonnee) != null) {
            libelle = EscapeString.escapeHtml(infoBean.getString("LIBELLE_" + nomDonnee));
        }
        String value = "";
        if (infoBean.get(nomDonnee) != null) {
            value = EscapeString.escapeHtml(infoBean.getString(nomDonnee));
        }
        /*
         * AM 2003 09 : si front office, la langue doit être prise en compte
         * pour l'affichage des boutons et des structures
         */
        boolean frontOffice = false;
        String lg = String.valueOf(LangueUtil.getIndiceLocale(LangueUtil.getDefaultLocale()));
        final String langueInfoBean = infoBean.getString("LANGUE");
        final String langueFicheInfoBean = infoBean.getString("LANGUE_FICHE");
        if (StringUtils.isNotEmpty(langueFicheInfoBean) && StringUtils.isNumeric(langueFicheInfoBean)) {
            lg = langueFicheInfoBean;
        } else if (StringUtils.isNotEmpty(langueInfoBean) && StringUtils.isNumeric(langueInfoBean)) {
            lg = langueInfoBean;
        }
        if (infoBean.get("SAISIE_FRONT") != null) {
            frontOffice = true;
        }
        // AM 200501 : sur les sélections multiples, la zone qui doit être
        // controlée n'est pas TMP_
        // le controle sur obligatoire ou non ne marchait pas.
        String szTmp = nomDonnee;
        if (szTmp.startsWith("TMP_")) {
            szTmp = szTmp.substring(4);
        }
        if (optionModification != FormateurJSP.SAISIE_AFFICHAGE) {
            if ("-".equals(libelle) || "".equals(libelle)) {
                libelle = MessageHelper.getMessage(infoBean.getNomExtension(), "JTF_CLIQUER_PARCOURIR");
            }
            res += "<input type=\"text\" class=\"readonly\" readonly=\"readonly\" id=\"LIBELLE_" + nomDonnee + "\" name=\"LIBELLE_";
            res += nomDonnee;
            res += "\" size=\"30\" value=\"";
            res += libelle;
            if (value.length() > 0) {
                res += "\" title=\"" + serviceStructure.getBreadCrumbs(value, LangueUtil.getIndiceLocaleDefaut());
            }
            res += "\"/>";
            int indiceChamps = fmt.getIndiceChamp();
            res += "<input type=\"hidden\" name=\"#FORMAT_" + szTmp + "\" value=\"" + optionModification + ";" + FormateurJSP.FORMAT_TEXTE + ";0;0;" + "LIB=" + nomZone + ";" + indiceChamps++ + "\"/>";
            fmt.setIndiceChamp(indiceChamps);
            res += "<input type=\"hidden\" name=\"" + nomDonnee;
            res += "\" value=\"" + value;
            res += "\" />";
            // AM 200412 : zones cachées
            if (optionModification == -1 || ReferentielObjets.getNombreObjetsTypeStructure() == 0) {
                out.println(res);
                return;
            }
            if (frontOffice) {
                String filtre = "";
                if (infoBean.get("FILTRE_" + nomDonnee) != null) {
                    filtre = infoBean.getString("FILTRE_" + nomDonnee);
                    filtre = EscapeString.escapeJavaScript(filtre);
                }
                res += "<input id=\"parcourir_" + nomDonnee + "\" type=\"button\" class=\"button submit\" value=\"" + MessageHelper.getCoreMessage("JTF_BOUTON_PARCOURIR") + "\" onclick=\"showMessageField('strfo/" + lg + "/" + filtre + "','" + nomDonnee + "','LIBELLE_" + nomDonnee + "');\"/> ";
            } else {
                // création, l'arbre des structures contiendra toutes les
                // langues
                String typeAide = "structure/";
                if (infoBean.getEtatObjet() != null && !InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                    typeAide += lg;
                }
                // Filtrage de l'arbre des structures en fonction des
                // permissions
                // JSS 20051031 : rattachement secondaires
                String nomDonneeFiltreArbre = nomDonnee;
                if (nomDonnee.startsWith("TMP")) {
                    // On enlève le TMP_ (pour les listes)
                    nomDonneeFiltreArbre = nomDonnee.substring(4);
                }
                // Controle si la donnée correspond bien à la structure de
                // l'objet métier
                if (infoBean.get("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT") != null && infoBean.getString("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT").equals(nomDonneeFiltreArbre) || (infoBean.get("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT_AUTRES") != null && infoBean.getString("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT_AUTRES").equals(nomDonneeFiltreArbre))) {
                    // Controle si affichage ecran PRINCIPAL ou RECHERCHE
                    // Valorisé dans ControleurUniv
                    final String type = infoBean.getString("GRS_PERMISSION_TYPE");
                    final String objet = infoBean.getString("GRS_PERMISSION_OBJET");
                    final String action = infoBean.getString("GRS_PERMISSION_ACTION");
                    if (type != null && type.length() > 0 && objet != null && objet.length() > 0 && action != null && action.length() > 0) {
                        typeAide = "strbo/" + type + "/" + objet + "/" + action + "/";
                        if (!InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                            typeAide += lg;
                        }
                    }
                }
                typeAide += "/";
                if (infoBean.get("FILTRE_" + nomDonnee) != null) {
                    typeAide += EscapeString.escapeJavaScript(infoBean.getString("FILTRE_" + nomDonnee));
                }
                res += "<input id=\"parcourir_" + nomDonnee + "\" type=\"button\" class=\"button\" value=\"" + MessageHelper.getCoreMessage("JTF_BOUTON_PARCOURIR") + "\" onclick=\"showMessageField('" + typeAide + "','" + nomDonnee + "','LIBELLE_" + nomDonnee + "');\" /> ";
            }
            if (optionModification == FormateurJSP.SAISIE_FACULTATIF) {
                res += "<input type=\"button\" class=\"button reset\" value=\"" + MessageHelper.getCoreMessage("JTF_BOUTON_EFFACER") + "\" onclick=\"effacerTextField('" + nomDonnee + "','LIBELLE_" + nomDonnee + "','','" + MessageHelper.getCoreMessage("JTF_CLIQUER_PARCOURIR") + "');\"/> ";
            }
        } else {
            fmt.insererChampSaisie(out, infoBean, "LIBELLE_" + szTmp, FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 0);
        }
        out.println(res);
    }

    /**
     * Recupere le picto de la langue en allant le chercher dans le dossier de la langue.
     *
     * @param out
     *            the out
     * @param codeLangue
     *            the code langue
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererDrapeauLangue(final JspWriter out, final String codeLangue) throws IOException {
        final String urlDrapeau = LangueUtil.getPathImageDrapeau(codeLangue);
        final String res = "<img src = \"" + urlDrapeau + "\" alt=\"" + LangueUtil.getDisplayName(codeLangue) + "\" />";
        out.print(res);
    }

    /**
     * Insère une toolbox FCK.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param width
     *            the width
     * @param height
     *            the height
     * @param toolbar
     *            the toolbar
     * @param autorisations
     *            the autorisations
     * @param pageLibre
     *            the page libre
     * @param encadreAvance
     *            the encadre avance
     * @param pageStyle
     *            the page style
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void insererFckToolbox(final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int width, final int height, final String toolbar, final AutorisationBean autorisations, final boolean pageLibre, // liste des
        // titres
        final boolean encadreAvance, // titre
        final boolean pageStyle) // titre
        throws IOException {
        String valeur = "";
        if (infoBean.getString(nomDonnee) != null) {
            // on echappe les caracteres pouvant poser des problèmes de
            // fonctionnement avec fckeditor
            valeur = escapeFckValue(infoBean.getString(nomDonnee), infoBean.isControled(nomDonnee));
            // on effectue certains traitements particulier sur le contenu
            valeur = formaterFckValue(infoBean, valeur);
        }
        out.write("<script type=\"text/javascript\">\r\n");
        out.write(" var oFCKeditor" + nomDonnee + " = new FCKeditor( '" + nomDonnee + "', " + width + ", " + height + ", '', '' ) ;\r\n");
        // CustomConfigurationsPath et CustomPlugins sont les deux seules
        // variables dont la valeur est chargée le fichier kportalconfig.js
        out.write(" oFCKeditor" + nomDonnee + ".Config.CustomConfigurationsPath = '/adminsite/fcktoolbox/kosmos/kportalconfig.js' ;\r\n");
        // plugins specifiques
        out.write("oFCKeditor" + nomDonnee + ".Config.ExtensionsPath = '" + WebAppUtil.getRelativeExtensionsPath() + "';\r\n");
        final String conf = PluginToolboxHelper.getConfiguration(infoBean);
        if (conf.length() > 0) {
            out.write("oFCKeditor" + nomDonnee + ".Config.CustomPlugins = '" + conf + "';\r\n");
        }
        out.write(" oFCKeditor" + nomDonnee + ".BasePath = '/adminsite/fcktoolbox/fckeditor/' ;\r\n");
        out.write(" oFCKeditor" + nomDonnee + ".Value = '" + EscapeString.escapeJavaScript(valeur) + "';\r\n");
        // styles de paragraphes
        if (StringUtils.isNotEmpty(PropertyHelper.getCoreProperty("fckeditor.kportalestyles"))) {
            out.write("oFCKeditor" + nomDonnee + ".Config['KStylesXmlPath'] = '" + PropertyHelper.getCoreProperty("fckeditor.kportalestyles") + "';\r\n");
        }
        // styles personnalisés
        if (StringUtils.isNotEmpty(PropertyHelper.getCoreProperty("fckeditor.fckstyles"))) {
            out.write("oFCKeditor" + nomDonnee + ".Config['StylesXmlPath'] = '" + PropertyHelper.getCoreProperty("fckeditor.fckstyles") + "';\r\n");
        }
        // templates personnalisés
        if (StringUtils.isNotEmpty(PropertyHelper.getCoreProperty("fckeditor.fcktemplates"))) {
            out.write("oFCKeditor" + nomDonnee + ".Config['TemplatesXmlPath'] = '" + PropertyHelper.getCoreProperty("fckeditor.fcktemplates") + "';\r\n");
        }
        String sToolbar = "";
        out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] = \"\";\r\n");
        // dans le cas d'une saisie back
        if (StringUtils.equals(toolbar, TYPE_TOOLBAR_BACKOFFICE)) {
            out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \"" + PropertyHelper.getCoreProperty("fckeditor.toolbar.standard") + "\";\r\n");
            // titre d'encadré
            if (encadreAvance) {
                if (PropertyHelper.getCoreProperty("fckeditor.toolbar.encadre") != null && PropertyHelper.getCoreProperty("fckeditor.toolbar.encadre").length() > 0) {
                    out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \"+" + PropertyHelper.getCoreProperty("fckeditor.toolbar.encadre") + "\";\r\n");
                }
            }
            // boutons specifiques aux pages libres et newsletters
            else if (pageLibre) {
                if (PropertyHelper.getCoreProperty("fckeditor.toolbar.styles_pagelibre") != null && PropertyHelper.getCoreProperty("fckeditor.toolbar.styles_pagelibre").length() > 0) {
                    out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \"+" + PropertyHelper.getCoreProperty("fckeditor.toolbar.styles_pagelibre") + "\";\r\n");
                }
            } else if (pageStyle) {
                if (PropertyHelper.getCoreProperty("fckeditor.toolbar.styles_standard") != null && PropertyHelper.getCoreProperty("fckeditor.toolbar.styles_standard").length() > 0) {
                    out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \"+" + PropertyHelper.getCoreProperty("fckeditor.toolbar.styles_standard") + "\";\r\n");
                }
            }
            // couleur de texte et surlignage
            if (autorisations != null && autorisations.possedePermissionTech("fpg") && (pageLibre || pageStyle)) {
                if (PropertyHelper.getCoreProperty("fckeditor.toolbar.colors") != null && PropertyHelper.getCoreProperty("fckeditor.toolbar.colors").length() > 0) {
                    out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \"+" + PropertyHelper.getCoreProperty("fckeditor.toolbar.colors") + "\";\r\n");
                }
            }
            sToolbar = PropertyHelper.getCoreProperty("fckeditor.toolbar.bo");
        } else if ("Collaboratif".equals(toolbar)) { // dans le cas d'une saisie
            // front ou création espace
            // collab.
            if (StringUtils.isNotBlank(infoBean.getString("ESPACE"))) {
                sToolbar = PropertyHelper.getCoreProperty("fckeditor.toolbar.collaboratif");
            } else {
                out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \"" + PropertyHelper.getCoreProperty("fckeditor.toolbar.standard") + "\";\r\n");
                sToolbar = PropertyHelper.getCoreProperty("fckeditor.toolbar.fo");
            }
        } else {
            sToolbar = PropertyHelper.getCoreProperty("fckeditor.toolbar." + toolbar);
        }
        int nbOptions = 0;
        if (sToolbar.length() > 0) {
            final String[] tOptions = sToolbar.split(",", -2);
            boolean active = true;
            for (final String option : tOptions) {
                // liste d'objets kportal
                if ("KPortalList".equals(option)) {
                    if (autorisations == null || !autorisations.possedePermissionTech("flf")) {
                        active = false;
                    }
                }
                // tags
                if ("KPortalTag".equals(option)) {
                    if (autorisations == null || !autorisations.possedePermissionTech("fat")) {
                        active = false;
                    }
                }
                // rss
                if ("KPortalRss".equals(option)) {
                    if (autorisations == null || !autorisations.possedePermissionTech("fra")) {
                        active = false;
                    }
                }
                // galerie
                if ("KPortalGalerie".equals(option)) {
                    if (autorisations == null || !autorisations.possedePermissionTech("fgm")) {
                        active = false;
                    }
                }
                if (active) {
                    if (nbOptions > 0) {
                        out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \",\";");
                    }
                    out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \"" + option + "\";\r\n");
                }
                active = true;
                nbOptions++;
            }
        }
        // insertion des options pour les plugins actifs
        final List<Pair<String, String>> options = PluginToolboxHelper.getOptions(infoBean);
        for (final Pair<String, String> pair : options) {
            if ("DynamicToolbar_buttons".equals(pair.getKey())) {
                if (nbOptions > 0) {
                    out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \",\";");
                }
                out.write(" oFCKeditor" + nomDonnee + ".Config['" + pair.getKey() + "'] += \"" + pair.getValue() + "\";\r\n");
            } else {
                out.write(" oFCKeditor" + nomDonnee + ".Config['" + pair.getKey() + "'] = \"" + pair.getValue() + "\";\r\n");
            }
        }
        // option voir le source html
        if (autorisations != null && autorisations.possedePermissionTech("mdh")) {
            out.write(" oFCKeditor" + nomDonnee + ".Config['DynamicToolbar_buttons'] += \"|" + PropertyHelper.getCoreProperty("fckeditor.toolbar.source") + "\";\r\n");
        }
        out.write(" oFCKeditor" + nomDonnee + ".Config['MediathequeMaxWidth']=" + PropertyHelper.getCoreProperty("fckeditor.mediatheque.max_width") + ";\r\n");
        out.write(" oFCKeditor" + nomDonnee + ".ToolbarSet = 'DynamicToolbar' ;\r\n");
        out.write(" oFCKeditor" + nomDonnee + ".Create() ;\r\n");
        out.write(" if( document.getElementsByName('FCK_EDITORS_NAMES')[0].value.indexOf('" + nomDonnee + "_FCK')==-1 ){ \r\n");
        out.write(" if( document.getElementsByName('FCK_EDITORS_NAMES')[0].value.length>0)document.getElementsByName('FCK_EDITORS_NAMES')[0].value += \";\";\r\n");
        out.write("document.getElementsByName('FCK_EDITORS_NAMES')[0].value +=\"" + nomDonnee + "_FCK\";}\r\n");
        out.write("</script>\r\n");
    }

    /**
     * Supprime les caractères posant problème pour l'intégration dans la toolbox.
     *
     * @param chaine
     *            the chaine
     * @param controlled
     *            the controlled
     *
     * @return the string
     */
    private String escapeFckValue(String chaine, final boolean controlled) {
        if (chaine == null) {
            return "";
        }
        String sautDeLigne = "";
        if (controlled) {
            sautDeLigne = "<br />";
        }
        chaine = StringUtils.replace(chaine, "\r\n", sautDeLigne);
        chaine = StringUtils.replace(chaine, "\r", sautDeLigne);
        chaine = StringUtils.replace(chaine, "\n", sautDeLigne);
        return chaine;
    }

    /**
     * Formater fck value.
     *
     * @param chaine
     *            the chaine
     *
     * @return the string
     */
    private String formaterFckValue(final InfoBean infoBean, String chaine) {
        // Securisation des images toolbox
        final HttpSession session = infoBean.getSessionHttp();
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
        final String sTag = "(\\[id-image\\][0-9A-Z]+\\[/id-image\\])";
        final Pattern p = Pattern.compile(sTag);
        final Matcher m = p.matcher(chaine);
        while (m.find()) {
            infosSession.put(m.group(1), -1);
        }
        // on transforme les tags [id-image] en
        // /servlet/com.univ.utils.LectureImageToolbox?TAG=[id-image]
        final String replacePattern = "src=\"(http://)*([a-zA-Z0-9:]+)*(/servlet/com.univ.utils.LectureImageToolbox\\?TAG=)*\\[id-image\\]";
        chaine = StringUtils.replacePattern(chaine, replacePattern, "src=\"/servlet/com.univ.utils.LectureImageToolbox?TAG=[id-image]");
        return chaine;
    }

    /**
     * Insère le code HTML d'un champ toolbox dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     * @param stylePageLibre
     *            the page libre
     * @param styleEncadreAvance
     *            the encadre avance
     * @param styleStandart
     *            the newsletter
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @deprecated Utiliser le tag
     */
    @Deprecated
    public void insererEditeurHTML(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final String nomZone, final boolean stylePageLibre, final boolean styleEncadreAvance, final boolean styleStandart) throws IOException {
        insererEditeurHTML(fmt, out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, nomZone, stylePageLibre, styleEncadreAvance, styleStandart, TYPE_TOOLBAR_BACKOFFICE);
    }

    /**
     * Insère le code HTML d'un champ toolbox dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     * @param stylePageLibre
     *            the page libre
     * @param styleEncadreAvance
     *            the encadre avance
     * @param styleStandart
     *            the newsletter
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @deprecated Use the component tag instead ({@link com.kosmos.toolbox.component.tag.ToolboxTag})
     */
    @Deprecated
    public void insererEditeurHTML(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final String nomZone, final boolean stylePageLibre, final boolean styleEncadreAvance, final boolean styleStandart, final String typeToolbar) throws IOException {
        if (optionModification != -1) {
            insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
            if (optionModification == FormateurJSP.SAISIE_AFFICHAGE) {
                fmt.insererChampSaisie(out, infoBean, nomDonnee, FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 0);
            } else {
                final String rawConfKey = CkeditorConfigurerUtils.buildConfKey(infoBean, nomDonnee);
                final String userCode = ContexteUtil.getContexteUniv().getCode();
                final UUID weakConfKey = getCkeditorConfigurationService().getWeakConfigurationKey(userCode, infoBean.getNomExtension(), rawConfKey);
                final String rawValue = infoBean.get(nomDonnee, String.class);
                String formattedValue = StringUtils.EMPTY;
                if (StringUtils.isNotBlank(rawValue)) {
                    formattedValue = ToolboxHelper.prepareForBo(infoBean, rawValue);
                }
                int currentIndex = fmt.getIndiceChamp();
                out.write(String.format("<input type=\"hidden\" name=\"#FORMAT_%s\" value=\"%d;%d;%d;%d;APPLET_HTML=1,CODE_HTML=1,LIB=%s;6\">", nomDonnee, optionModification, FormateurJSP.FORMAT_TEXTE_CACHE, nbCarMin, nbCarMax, nomZone, currentIndex++));
                fmt.setIndiceChamp(currentIndex);
                out.write("<textarea class=\"js-ckeditor\" id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "\" rows=\"8\" data-conf=\"" + weakConfKey.toString() + "\" maxlength=\"" + nbCarMax + "\"" + (nbCarMin > 0 ? " required" : StringUtils.EMPTY) + ">");
                out.write(EscapeString.escapeHtml(StringUtils.defaultString(formattedValue)));
                out.write("</textarea>");
            }
        }
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererToolbox(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int nbCarMax, final String nomZone) throws IOException {
        insererEditeurHTML(fmt, out, infoBean, nomDonnee, optionModification, FormateurJSP.FORMAT_MULTI_LIGNE, 0, nbCarMax, nomZone, false, false, false);
    }

    /**
     * Insère le code HTML de l'entete d'un champ.
     *
     * @param out
     *            the out
     * @param nomDonnee
     *            the nom donnee
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void insererEnteteChamp(final JspWriter out, final String nomDonnee, final String nomZone) throws IOException {
        insererEnteteChamp(out, nomDonnee, nomZone, 1);
    }

    /**
     * Insère le code HTML de l'entete d'un champ.
     *
     * @param out
     *            the out
     * @param nomDonnee
     *            the nom donnee
     * @param nomZone
     *            the nom zone
     * @param optionModification
     *            the option modification
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void insererEnteteChamp(final JspWriter out, final String nomDonnee, String nomZone, final int optionModification) throws IOException {
        if (optionModification == FormateurJSP.SAISIE_OBLIGATOIRE) {
            nomZone += " (*)";
        }
        if (nomZone.contains("TRI_")) {
            nomZone = nomZone.substring(0, nomZone.indexOf("TRI_"));
        }
        out.println("<p>");
        if (nomZone.length() > 0) {
            // EL 20051228 Refonte ergo back-office: décentralisation dans la
            // CSS de l'affichage avec mise en place d'une classe :
            if (FormateurJSP.SAISIE_AFFICHAGE == optionModification) {
                out.println("<span class=\"label colonne\">" + nomZone + "</span>");
            } else {
                out.println("<label for=\"" + nomDonnee + "\" class=\"colonne\"> " + nomZone + "</label>");
            }
        }
    }

    /**
     * Insère le code HTML de la fin d'un champ.
     *
     * @param out
     *            the out
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @deprecated New layout without any tables, this method is no longer necessary
     */
    @Deprecated
    private void insererFinChamp(final JspWriter out) throws IOException {
        out.println("</p>");
    }

    /**
     * Insère le code HTML d'une image à uploader
     *
     * Dans le composant d'information, les données doivent s'appeler - NOM_DONNEE[_URL] - NOM_DONNEE[_FICHIER].
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererImage(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone) throws IOException {
        out.println("   <div id=\"layerphoto\"><img src=\"" + EscapeString.escapeHtml(infoBean.getString(nomDonnee + "_URL")) + "\" width=\"150\" ></div> ");
        fmt.insererChampSaisie(out, infoBean, nomDonnee + "_FICHIER", optionModification, FormateurJSP.FORMAT_FICHIER, 10, 30);
    }

    /**
     * Insère le code HTML d'une saisie de structure la page JSP
     *
     * Le libellé, si le code est prévalorisé doit être dans la donnée LIBELLE_[NOMCODE).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             the exception
     */
    public void insererSaisieStructure(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone) throws IOException {
        // AM 200412 : zones cachées
        if (!(optionModification == -1 || ReferentielObjets.getNombreObjetsTypeStructure() == 0)) {
            insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        }
        insererContenuSaisieStructure(fmt, out, infoBean, nomDonnee, optionModification, nomZone);
        if (!(optionModification == -1 || ReferentielObjets.getNombreObjetsTypeStructure() == 0)) {
            insererFinChamp(out);
        }
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     * @param _format
     *            the _format
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererChampSaisie(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final String nomZone, final String _format) throws IOException {
        insererChampSaisie(fmt, out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, nomZone, _format, true);
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     * @param _format
     *            the _format
     * @param afficheCal
     *            the affiche cal
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererChampSaisie(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final String nomZone, final String _format, final boolean afficheCal) throws IOException {
        insererChampSaisie(fmt, out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, nbCarMax, nomZone, _format, afficheCal, "");
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     * @param _format
     *            the _format
     * @param afficheCal
     *            the affiche cal
     * @param nomImage
     *            the nom image
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererChampSaisie(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final String nomZone, final String _format, final boolean afficheCal, final String nomImage) throws IOException {
        insererChampSaisie(fmt, out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, nbCarMax, nomZone, _format, afficheCal, nomImage);
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param size
     *            the size
     * @param nomZone
     *            the nom zone
     * @param _format
     *            the _format
     * @param afficheCal
     *            the affiche cal
     * @param nomImage
     *            the nom image
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererChampSaisie(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final int size, final String nomZone, final String _format, final boolean afficheCal, final String nomImage) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        insererContenuChampSaisie(fmt, out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, size, nomZone, _format, afficheCal, nomImage);
        insererFinChamp(out);
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     * @param _format
     *            the _format
     * @param afficheCal
     *            the affiche cal
     * @param nomImage
     *            the nom image
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuChampSaisie(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final String nomZone, final String _format, final boolean afficheCal, final String nomImage) throws IOException {
        insererContenuChampSaisie(fmt, out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, nbCarMax, nomZone, _format, afficheCal, nomImage);
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param size
     *            the size
     * @param nomZone
     *            the nom zone
     * @param _format
     *            the _format
     * @param afficheCal
     *            the affiche cal
     * @param nomImage
     *            the nom image
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuChampSaisie(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final int size, final String nomZone, final String _format, final boolean afficheCal, final String nomImage) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        String format = "LIB=" + nomZone;
        if (_format.length() > 0) {
            format = _format + "," + format;
        }
        fmt.insererChampSaisie(out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, size, format);
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuChampSaisie(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final String nomZone) throws IOException {
        insererContenuChampSaisie(fmt, out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, nomZone, "", true, "");
    }

    /**
     * Insère le code HTML d'une saisie de liste
     *
     * Le libellé, si le code est prévalorisé doit être dans la donnée LIBELLE_[NOMCODE).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     * @param typeListe
     *            the type liste
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuSaisieListeDebut(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final int typeListe) throws IOException {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        if (optionModification != -1 && optionModification != FormateurJSP.SAISIE_AFFICHAGE) {
            String res = "";
            String value = "";
            if (infoBean.get(nomDonnee) != null) {
                value = infoBean.getString(nomDonnee);
            }
            String libelle = "";
            if (infoBean.get("LIBELLE_" + nomDonnee) != null) {
                libelle = infoBean.getString("LIBELLE_" + nomDonnee);
            }
            // permet de definir l'infobulle du champ dans le processus
            String infoBulle = "";
            if (infoBean.get("INFOBULLE_" + nomDonnee) != null) {
                infoBulle = infoBean.getString("INFOBULLE_" + nomDonnee);
            }
            if (infoBulle.length() == 0) // infobulle standard
            {
                // gestion du fil d'ariane pour les rubriques, structures,
                // groupes
                if ("RUBRIQUE_PUBLICATION".equals(nomDonnee) || "CODE_RATTACHEMENT_AUTRES".equals(nomDonnee) || "PUBLIC_VISE_DSI".equals(nomDonnee) || "PUBLIC_VISE_DSI_RESTRICTION".equals(nomDonnee) || "SERVICE_PUBLIC_VISE_DSI".equals(nomDonnee) || // saisie service
                    "SERVICE_PUBLIC_VISE_DSI_RESTRICTION".equals(nomDonnee) || // saisie service
                    "GROUPES_DSI".equals(nomDonnee) || // saisie rubrique
                    "GROUPE_DSI".equals(nomDonnee) || // saisie utilisateur
                    "GROUPES".equals(nomDonnee)) // saisie profil
                {
                    final StringTokenizer st = new StringTokenizer(value, ";");
                    final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                    while (st.hasMoreElements()) {
                        if (infoBulle.length() > 0) {
                            infoBulle += ";";
                        }
                        if ("RUBRIQUE_PUBLICATION".equals(nomDonnee)) {
                            infoBulle += serviceRubrique.getLabelWithAscendantsLabels(st.nextToken());
                        } else if ("CODE_RATTACHEMENT_AUTRES".equals(nomDonnee)) {
                            infoBulle += serviceStructure.getBreadCrumbs(st.nextToken(), " > ");
                        } else {
                            infoBulle += serviceGroupeDsi.getBreadCrumbs(st.nextToken(), StringUtils.EMPTY);
                        }
                    }
                } else {
                    infoBulle = libelle;
                }
            }
            res += "<!-- DEBUT GESTION LISTE " + nomDonnee + " -->\n";
            res += "<input type=\"hidden\" name=\"" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(value) + "\"/>\n";
            res += "<input type=\"hidden\" name=\"LIBELLE_" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(libelle) + "\"/>\n";
            res += "<input type=\"hidden\" name=\"INFOBULLE_" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(infoBulle) + "\"/>\n";
            out.println(res);
        }
    }

    /**
     * Insère le code HTML d'une fin de saisie multiple.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     * @param typeListe
     *            the type liste
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuSaisieListeFin(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final int typeListe) throws IOException {
        if (optionModification != -1 && optionModification != FormateurJSP.SAISIE_AFFICHAGE) {
            String res = "";
            res += "</td>\n";
            // boutons Ajouter / (Modifier) / Supprimer /
            res += "<td>";
            res += "<a href=\"#\" title=\"Ajouter l'élément sélectionné\" onclick=\"oMultivalueField" + nomDonnee + ".Add(); return false;\"><img src=\"/adminsite/images/add_icon.gif\" alt=\"Ajouter l'élément sélectionné\"  /></a>\n";
            res += "<br />\n";
            if (infoBean.get(nomDonnee + "_MODIFICATION") != null) {
                res += "<a href=\"#\" title=\"Modifier l'élément sélectionné\" onclick=\"oMultivalueField" + nomDonnee + ".Modify(); return false;\"><img src=\"/adminsite/images/modify_icon.gif\" alt=\"Modifier l'élément sélectionné\"  /></a>\n";
                res += "<br />\n";
            }
            res += "<a href=\"#\" title=\"Supprimer l'élément sélectionné\" onclick=\"oMultivalueField" + nomDonnee + ".Remove(); return false;\"><img src=\"/adminsite/images/remove_icon.gif\" alt=\"Supprimer l'élément sélectionné\"  /></a>\n";
            res += "</td>\n";
            // select contenant la liste des valeurs
            if ("1".equals(infoBean.get("ZONE_LARGE"))) {
                res += "<td>\n";
            } else {
                res += "<td>\n";
            }
            if (nomZone.length() > 0) {
                res += "<label for=\"SELECT_" + nomDonnee + "\" value=\"" + nomZone + "\">\n";
            }
            res += "<select " + /* id=\"SELECT_" + nomDonnee + "\" */"name=\"SELECT_" + nomDonnee + "\" size=\"5\" class=\"multivalue\" style=\"width:100%;min-width:100px;\" onchange=\"oMultivalueField" + nomDonnee + ".SelectItem();\">\n";
            res += "</select>\n";
            res += "</td>\n";
            // boutons Remonter / Descendre
            res += "<td >";
            res += "<a href=\"#\" title=\"Remonter l'élément sélectionné\" onclick=\"oMultivalueField" + nomDonnee + ".MoveUp(); return false;\"><img src=\"/adminsite/images/fhaut.gif\" alt=\"Remonter\"  /></a>\n";
            res += "<br />\n";
            res += "<a href=\"#\" title=\"Descendre l'élément sélectionné\" onclick=\"oMultivalueField" + nomDonnee + ".MoveDown(); return false;\"><img src=\"/adminsite/images/fbas.gif\" alt=\"Descendre\"  /></a>\n";
            res += "</td>\n";
            res += "</tr>\n";
            res += "</table>\n";
            res += "<script type=\"text/javascript\">\n";
            res += "    var oMultivalueField" + nomDonnee + " = new MultivalueField(window.document.forms[0], '" + nomDonnee + "', " + typeListe + ");\n";
            res += "    oMultivalueField" + nomDonnee + ".Init();\n";
            res += "</script>\n";
            res += "<!-- FIN GESTION LISTE " + nomDonnee + " -->\n";
            out.println(res);
        }
    }

    /**
     * Insère le code HTML d'une liste de fichiers collaboratifs.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomZone
     *            the nom zone
     * @param bouton1
     *            the bouton1
     * @param modeFichier
     *            the mode fichier
     * @param nomForm
     *            the nom form
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererListeFichiersGw(final JspWriter out, final InfoBean infoBean, final String nomZone, final String bouton1, final String modeFichier, final String nomForm) throws IOException {
        insererEnteteChamp(out, "", nomZone);
        insererContenuListeFichiersGw(out, infoBean, bouton1, modeFichier, nomForm, "1/1", FormateurJSP.SAISIE_FACULTATIF, "Liste de fichier");
        insererFinChamp(out);
    }

    /**
     * Insère le code HTML d'une liste de fichiers collaboratifs.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param bouton1
     *            the bouton1
     * @param modeFichier
     *            the mode fichier
     * @param nomForm
     *            the nom form
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuListeFichiersGw(final JspWriter out, final InfoBean infoBean, final String bouton1, final String modeFichier, final String nomForm) throws IOException {
        insererContenuListeFichiersGw(out, infoBean, bouton1, modeFichier, nomForm, "1/1", FormateurJSP.SAISIE_FACULTATIF, "Liste de fichier");
    }

    /**
     * Insère le code HTML d'une liste de fichiers collaboratifs.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param bouton1
     *            the bouton1
     * @param modeFichier
     *            the mode fichier
     * @param nomForm
     *            the nom form
     * @param indice
     *            the indice
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuListeFichiersGw(final JspWriter out, final InfoBean infoBean, final String bouton1, final String modeFichier, final String nomForm, String indice, final int optionModification, final String nomZone) throws IOException {
        final String nomDonnee = "FICHIER_MULTIPLE";
        final String[] itemIndice = indice.split("/", -2);
        if (itemIndice.length == 2) {
            indice = itemIndice[0];
            final String nombreFichier = itemIndice[1];
            String valeur = "";
            int nbFichiers = 0;
            if (infoBean.getString("TOTAL_" + nomDonnee + "_" + indice) != null) {
                valeur = infoBean.getString("TOTAL_" + nomDonnee + "_" + indice);
            }
            // génération du javascript
            out.println("<div id=\"liste-fichier-" + indice + "\" class=\"liste-fichier\">");
            // TODO REVOIR CETTE TABLE QUI SERT D'INNER DANS toolbox.js l714
            out.println("<table id=\"inner-fichier-" + indice + "\">");
            out.println("<script type=\"text/javascript\">");
            if (StringUtils.isNotEmpty(infoBean.getString(UploadedFile.KEY_MAX_FILE_SIZE))) {
                out.println("maxFileSize = '" + infoBean.getString(UploadedFile.KEY_MAX_FILE_SIZE) + "';");
            }
            out.println("indiceForm = '" + nomForm.replaceAll("'", "\\\\'") + "';");
            out.println("libelleFichierMulti=\"" + nomDonnee + "\"");
            out.println("arrayFichierMulti[" + indice + "]= new Array();");
            if (valeur.length() > 0) {
                int j = 0;
                for (String fichierTab : StringUtils.split(valeur, '|')) {
                    final String[] fichierItem = fichierTab.split(";", -2);
                    out.println("arrayFichierMulti[" + indice + "][" + j + "]= new Fichier('" + fichierItem[0] + "','" + EscapeString.escapeJavaScript(fichierItem[1]) + "','" + EscapeString.escapeJavaScript(fichierItem[2]) + "','" + EscapeString.escapeJavaScript(fichierItem[3]) + "','" + EscapeString.escapeJavaScript(fichierItem[4]) + "');");
                    nbFichiers++;
                    j++;
                }
            }
            out.println("</script>");
            out.println("</table>");
            String saisie = StringUtils.EMPTY;
            if (infoBean.get("SAISIE_FRONT") != null) {
                saisie = "&amp;SAISIE_FRONT=true";
            }
            out.println(String.format("<input type=\"button\" class=\"submit\" value=\"%s\" onclick=\"ajouterFichier('%s','multi','%s');\" />", bouton1, indice, saisie));
            out.println("</div>");
            out.println("<input type=\"hidden\" name=\"#FORMAT_TOTAL_" + nomDonnee + "_" + indice + "\" value=\"" + optionModification + ";;;;LIB=" + nomZone + ";" + "\" />");
            out.println("<input type=\"hidden\" name=\"TOTAL_" + nomDonnee + "_" + indice + "\" value=\"" + EscapeString.escapeHtml(valeur) + "\" />");
            out.println("<input type=\"hidden\" name=\"NB_" + nomDonnee + "_" + indice + "\" value=\"" + nbFichiers + "\" />");
            out.println("<input type=\"hidden\" name=\"NO_" + nomDonnee + "_" + indice + "\" value=\"\" />");
            out.println("<input type=\"hidden\" name=\"MODE_" + nomDonnee + "_" + indice + "\" value=\"" + modeFichier + "\" />");
            out.println("<input type=\"hidden\" name=\"NB_" + nomDonnee + "\" value=\"" + nombreFichier + "\"/>");
            out.println("<script type=\"text/javascript\">affichageFichier('" + indice + "');</script>");
        }
    }

    /**
     * Insère le code HTML d'une saisie unique de fichier.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomZone
     *            the nom zone
     * @param modeFichier
     *            the mode fichier
     * @param nomForm
     *            the nom form
     * @param indice
     *            the indice
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererSaisieFichierGw(final JspWriter out, final InfoBean infoBean, final String nomZone, final String modeFichier, final String nomForm, final String indice) throws IOException {
        insererEnteteChamp(out, "", nomZone);
        insererContenuSaisieFichierGw(out, infoBean, modeFichier, nomForm, indice);
        insererFinChamp(out);
    }

    /**
     * Insère le code HTML d'une saisie unique de fichier.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param nomZone
     *            the nom zone
     * @param modeFichier
     *            the mode fichier
     * @param nomForm
     *            the nom form
     * @param indice
     *            the indice
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererSaisieFichierGw(final JspWriter out, final InfoBean infoBean, final String nomDonnee, final String nomZone, final String modeFichier, final String nomForm, final String indice) throws IOException {
        insererEnteteChamp(out, nomDonnee, nomZone);
        insererContenuSaisieFichierGw(out, infoBean, nomDonnee, modeFichier, nomForm, indice, FormateurJSP.SAISIE_FACULTATIF, "Fichier");
        insererFinChamp(out);
    }

    /**
     * Insère le code HTML d'une saisie unique de fichier.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomZone
     *            the nom zone
     * @param modeFichier
     *            the mode fichier
     * @param nomForm
     *            the nom form
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererSaisieFichierGw(final JspWriter out, final InfoBean infoBean, final String nomZone, final String modeFichier, final String nomForm) throws IOException {
        insererEnteteChamp(out, "", nomZone);
        insererContenuSaisieFichierGw(out, infoBean, modeFichier, nomForm, "1/1");
        insererFinChamp(out);
    }

    /**
     * Insère le code HTML d'une saisie unique de fichier.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param modeFichier
     *            the mode fichier
     * @param nomForm
     *            the nom form
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuSaisieFichierGw(final JspWriter out, final InfoBean infoBean, final String modeFichier, final String nomForm) throws IOException {
        insererContenuSaisieFichierGw(out, infoBean, modeFichier, nomForm, "1/1");
    }

    /**
     * Insère le code HTML d'une saisie unique de fichier.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param modeFichier
     *            the mode fichier
     * @param nomForm
     *            the nom form
     * @param indice
     *            the indice
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuSaisieFichierGw(final JspWriter out, final InfoBean infoBean, final String modeFichier, final String nomForm, final String indice) throws IOException {
        insererContenuSaisieFichierGw(out, infoBean, "", modeFichier, nomForm, indice, FormateurJSP.SAISIE_FACULTATIF, "Fichier");
    }

    /**
     * Insère le code HTML d'une saisie unique de fichier.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param modeFichier
     *            the mode fichier
     * @param nomForm
     *            the nom form
     * @param indice
     *            the indice
     * @param optionModification
     *            (facultatif ou obligatoire)
     * @param nomZone
     *            nom de la zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuSaisieFichierGw(final JspWriter out, final InfoBean infoBean, String nomDonnee, final String modeFichier, final String nomForm, String indice, final int optionModification, final String nomZone) throws IOException {
        final String[] itemIndice = indice.split("/", -2);
        if (nomDonnee.length() == 0) {
            nomDonnee = "FICHIER_UNIQUE";
        }
        if (itemIndice.length == 2) {
            indice = itemIndice[0];
            final String nombreFichier = itemIndice[1];
            String fichier = "";
            String libelle = MessageHelper.getCoreMessage("JTF_CLIQUER_PARCOURIR");
            String saisie = "";
            if (infoBean.get("SAISIE_FRONT") != null) {
                saisie = "&amp;SAISIE_FRONT=true";
            }
            String valeur = "";
            if (infoBean.getString(nomDonnee + "_" + indice) != null) {
                valeur = infoBean.getString(nomDonnee + "_" + indice);
            }
            // génération du javascript
            out.println("<script type=\"text/javascript\">");
            if (infoBean.get("ESPACE") != null) {
                out.println("espace = '" + ((String) infoBean.get("ESPACE")).replaceAll("'", "\\\\'") + "';");
            }
            if (StringUtils.isNotEmpty(infoBean.getString(UploadedFile.KEY_MAX_FILE_SIZE))) {
                out.println("maxFileSize = '" + infoBean.getString(UploadedFile.KEY_MAX_FILE_SIZE) + "';");
            }
            out.println("indiceForm = '" + nomForm.replaceAll("'", "\\\\'") + "';");
            out.println("libelleFichierUnique=\"" + nomDonnee + "\"");
            // retour en modification
            if (valeur.length() > 0) {
                final String[] fichierItem = valeur.split(";", -2);
                out.println("arrayFichierUnique[" + indice + "] = new Fichier('" + fichierItem[0] + "','" + Chaine.escapeJavaScript(fichierItem[1]) + "','" + Chaine.escapeJavaScript(fichierItem[2]) + "', '" + Chaine.escapeJavaScript(fichierItem[3]) + "','" + fichierItem[4] + "');");
                fichier = EscapeString.escapeHtml(valeur);
                libelle = EscapeString.escapeHtml(infoBean.getString("LIBELLE_" + nomDonnee + "_" + indice));
            }
            out.println("</script>");
            out.println("<input type=\"hidden\" name=\"#FORMAT_" + nomDonnee + "_" + indice + "\" value=\"" + optionModification + ";;;;LIB=" + nomZone + ";" + "\" />");
            out.println("<input type=\"hidden\" name=\"" + nomDonnee + "_" + indice + "\" value=\"" + fichier + "\" />");
            out.println("<input type=\"hidden\" name=\"MODE_" + nomDonnee + "_" + indice + "\" value=\"" + modeFichier + "\" />");
            out.println("<input type=\"hidden\" name=\"NB_" + nomDonnee + "\" value=\"" + nombreFichier + "\"/>");
            out.println("<input type=\"text\" readonly=\"readonly\" name=\"LIBELLE_" + nomDonnee + "_" + indice + "\" value=\"" + libelle + "\"/>");
            out.println("<input type=\"button\" class=\"submit\" value=\"" + MessageHelper.getCoreMessage("JTF_BOUTON_PARCOURIR") + "\" onclick=\"ajouterFichier('" + indice + "','unique','" + saisie + "');\"/>");
            out.println("<input type=\"button\" class=\"reset\" value=\"" + MessageHelper.getCoreMessage("JTF_BOUTON_EFFACER") + "\" onclick=\"effacerFichier(" + indice + ");\"/>");
        }
    }

    /**
     * Insère le code HTML d'un lien vers une popup de choix de couleur.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param champ1
     *            the champ1
     * @param champ2
     *            the champ2
     * @param nomForm
     *            the nom form
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @deprecated ne oas utiliser la majorité des paramètres ne servent plus... inserez directement l'input type color dans la jsp
     */
    @Deprecated
    public void insererContenuSaisieCouleur(final JspWriter out, final InfoBean infoBean, final String champ1, final String champ2, final String nomForm) throws IOException {
        if (infoBean.get(champ1) != null) {
            String codeCouleur = EscapeString.escapeHtml((String) infoBean.get(champ1));
            out.println("<input type=\"color\" name=\"" + champ1 + "\" value=\"" + codeCouleur + "\" />");
        }
    }

    /**
     * Inserer toolbox gw.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     * @param width
     *            the width
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @deprecated Utiliser le tag d'instertion
     */
    @Deprecated
    public void insererToolboxGw(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int nbCarMax, final String nomZone, final int width) throws IOException {
        insererToolboxGw(fmt, out, infoBean, nomDonnee, optionModification, nbCarMax, nomZone, width, "Collaboratif");
    }

    /**
     * Inserer toolbox gw.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nbCarMax
     *            the nb car max
     * @param nomZone
     *            the nom zone
     * @param width
     *            the width
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @deprecated Utiliser le tag
     */
    @Deprecated
    public void insererToolboxGw(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int nbCarMax, final String nomZone, final int width, final String toolbar) throws IOException {
        final HttpSession session = infoBean.getSessionHttp();
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
        final AutorisationBean autorisations = (AutorisationBean) infosSession.get(SessionUtilisateur.AUTORISATIONS);
        fmt.insererChampSaisie(out, infoBean, nomDonnee, optionModification, FormateurJSP.FORMAT_TEXTE_CACHE, 0, nbCarMax, "APPLET_HTML=1,CODE_HTML=1,LIB=" + nomZone);
        insererFckToolbox(out, infoBean, nomDonnee, width, 250, toolbar, autorisations, false, false, false);
    }

    /**
     * Inserer combo hashtable.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomHashtable
     *            the nom hashtable
     * @param nomZone
     *            the nom zone
     * @param tri
     *            the tri
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererComboHashtable(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomHashtable, final String nomZone, final String tri) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        insererContenuComboHashtable(fmt, out, infoBean, nomDonnee, optionModification, nomHashtable, nomZone, tri);
        insererFinChamp(out);
    }

    /**
     * Inserer contenu combo hashtable.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomHashtable
     *            the nom hashtable
     * @param nomZone
     *            the nom zone
     * @param tri
     *            the tri
     */
    public void insererContenuComboHashtable(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomHashtable, final String nomZone, final String tri) {
        fmt.insererComboHashtable(out, infoBean, nomDonnee, optionModification, nomHashtable, "8pt,LIB=" + nomZone + ",TRI=" + tri);
    }

    /**
     * Insère le code HTML d'une saisie dans une POP-UP
     *
     * Le libellé, si le code est prévalorisé doit être dans la donnée LIBELLE_[NOMCODE).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     * @param nomFonctionPopUp
     *            the nom fonction pop up
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuSaisieZone(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final String nomFonctionPopUp) throws IOException {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        String res = "";
        String libelle = "";
        if (infoBean.get("LIBELLE_" + nomDonnee) != null) {
            libelle = infoBean.getString("LIBELLE_" + nomDonnee);
        }
        // permet de definir l'infobulle du champ dans le processus
        String infoBulle = "";
        if (infoBean.get("INFOBULLE_" + nomDonnee) != null) {
            infoBulle = infoBean.getString("INFOBULLE_" + nomDonnee);
        }
        String value = "";
        if (infoBean.get(nomDonnee) != null) {
            value = infoBean.getString(nomDonnee);
        }
        // AAR 200908 : sur les sélections multiples, la zone qui doit être
        // controlée n'est pas TMP_
        // le controle sur obligatoire ou non ne marchait pas.
        String szTmp = nomDonnee;
        if (szTmp.startsWith("TMP_")) {
            szTmp = szTmp.substring(4);
        }
        if (optionModification != FormateurJSP.SAISIE_AFFICHAGE) {
            int indice = fmt.getIndiceChamp();
            res += "<input type=\"hidden\" name=\"#FORMAT_" + szTmp + "\" value=\"" + optionModification + ";" + FormateurJSP.FORMAT_TEXTE + ";" + 0 + ";" + 0 + ";" + "LIB=" + nomZone + ";" + indice++ + "\" />";
            fmt.setIndiceChamp(indice);
            String libelleBoutonPopup = MessageHelper.getCoreMessage("JTF_BOUTON_CHERCHER");
            String libelleParDefaut = MessageHelper.getCoreMessage("JTF_CLIQUER_CHERCHER");
            final boolean arbre = nomFonctionPopUp.startsWith("rub") || nomFonctionPopUp.startsWith("groupe") || nomFonctionPopUp.startsWith("public");
            if (arbre) {
                // on affiche une popup contenant un arbre, le libellé devient
                // "Parcourir"
                libelleBoutonPopup = MessageHelper.getMessage(infoBean.getNomExtension(), "JTF_BOUTON_PARCOURIR");
                libelleParDefaut = MessageHelper.getCoreMessage("JTF_CLIQUER_PARCOURIR");
                if (value.length() > 0) {
                    if (nomFonctionPopUp.startsWith("rub")) {
                        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                        infoBulle = serviceRubrique.getLabelWithAscendantsLabels(value);
                    } else if (nomFonctionPopUp.startsWith("groupe") || nomFonctionPopUp.startsWith("public")) {
                        infoBulle = serviceGroupeDsi.getBreadCrumbs(value, StringUtils.EMPTY);
                    }
                }
            }
            if (infoBulle.length() == 0) {
                infoBulle = libelle;
            }
            res += "<input type=\"hidden\" name=\"" + nomDonnee;
            res += "\" value=\"" + value;
            res += "\" />";
            if ("-".equals(libelle) || "".equals(libelle)) {
                libelle = libelleParDefaut;
            }
            String size = "30";
            // ajout taille specifique a la demande : pour les parcours de la
            // fiche formation par exemple
            if ("1".equals(infoBean.get("ZONE_LARGE"))) {
                size = "25";
            }
            res += "<input type=\"text\" readonly=\"readonly\" name=\"LIBELLE_" + nomDonnee + "\" size=\"" + size + "\" value=\"" + EscapeString.escapeAttributHtml(libelle) + "\" title=\"" + EscapeString.escapeAttributHtml(infoBulle) + "\" />";
            if (nomFonctionPopUp.toLowerCase().startsWith("saisie_")) {
                res += "<input type=\"button\" class=\"button\" value=\"" + libelleBoutonPopup + "\" onclick=\"ouvrirFenetreRechercheParProcessus('" + infoBean.getNomExtension() + "','" + nomFonctionPopUp.toUpperCase() + "','" + nomDonnee + "','LIBELLE_" + nomDonnee + "');\"/> ";
            } else {
                String typeAide = nomFonctionPopUp.toLowerCase();
                // JSS20050126 : du fait des permissions, rubbo doit etre
                // sensible à la casse
                if (typeAide.startsWith("rubbo")) {
                    typeAide = nomFonctionPopUp;
                }
                // JSS 20040419 : Controle si l'arbre des rubriques doit etre
                // restreint
                // au périmètre
                else if ("rubrique".equals(typeAide)) {
                    // Controle si la donnée correspond bien à la rubrique de
                    // l'objet métier
                    if (infoBean.get("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE") != null) {
                        if (infoBean.getString("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE").equals(nomDonnee)) {
                            // Controle si affichage ecran PRINCIPAL ou
                            // RECHERCHE
                            // Valorisé dans ControleurUniv
                            String type = StringUtils.defaultString(infoBean.getString("GRS_PERMISSION_TYPE"));
                            String objet = StringUtils.defaultString(infoBean.getString("GRS_PERMISSION_OBJET"));
                            String action = StringUtils.defaultString(infoBean.getString("GRS_PERMISSION_ACTION"));
                            if (!"".equals(type) && !"".equals(objet) && (!"".equals(action) || "acp".equals(objet))) {
                                typeAide = "rubbo/" + type + "/" + objet + "/" + action + "/";
                            }
                        }
                    }
                }
                res += "<input type=\"button\" class=\"button\" value=\"" + libelleBoutonPopup + "\" onclick=\"showMessageField('" + typeAide + "','" + nomDonnee + "','LIBELLE_" + nomDonnee + "');\" /> ";
            }
            if (optionModification == FormateurJSP.SAISIE_FACULTATIF) {
                res += "<input type=\"button\" class=\"button\" value=\"Effacer\" onclick=\"effacerTextField('" + nomDonnee + "','LIBELLE_" + nomDonnee + "','','" + libelleParDefaut + "');\"/> ";
            }
        } else {
            fmt.insererChampSaisie(out, infoBean, "LIBELLE_" + szTmp, FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 0);
        }
        out.println(res);
    }

    /**
     * Début d'un bloc de saisie multivaluée.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     * @param typeListe
     *            the type liste
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererSaisieListeDebut(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final int typeListe) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        insererContenuSaisieListeDebut(fmt, out, infoBean, nomDonnee, optionModification, nomZone, typeListe);
    }

    /**
     * Fin d'un bloc de saisie multivaluée.
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     * @param typeListe
     *            the type liste
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererSaisieListeFin(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final int typeListe) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererContenuSaisieListeFin(fmt, out, infoBean, nomDonnee, optionModification, nomZone, typeListe);
        insererFinChamp(out);
    }

    /**
     * Insère le code HTML d'une saisie via une pop-up
     *
     * Le libellé, si le code est prévalorisé doit être dans la donnée LIBELLE_[NOMCODE).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     * @param nomFonctionPopUp
     *            the nom fonction pop up
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererSaisieZone(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final String nomFonctionPopUp) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        insererContenuSaisieZone(fmt, out, infoBean, nomDonnee, optionModification, nomZone, nomFonctionPopUp);
        insererFinChamp(out);
    }

    /**
     * Insère le code HTML d'une saisie d'un groupe DSI
     *
     * Le libellé, si le code est prévalorisé doit être dans la donnée LIBELLE_[NOMCODE).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuSaisieGroupeDsi(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone) throws IOException {
        insererContenuSaisieGroupedsi(fmt, out, infoBean, nomDonnee, optionModification, nomZone, GROUPEDSI_RESTRICTION);
    }

    /**
     * Insère le code HTML d'une saisie d'un groupe DSI
     *
     * Le libellé, si le code est prévalorisé doit être dans la donnée LIBELLE_[NOMCODE).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererContenuSaisiePublicViseDsi(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone) throws IOException {
        insererContenuSaisieGroupedsi(fmt, out, infoBean, nomDonnee, optionModification, nomZone, GROUPEDSI_PUBLIC_VISE);
    }

    /**
     * Insère le code HTML d'une saisie d'un groupe DSI
     *
     * Le libellé, si le code est prévalorisé doit être dans la donnée LIBELLE_[NOMCODE).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     * @param typeGroupedsi
     *            the type groupedsi
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void insererContenuSaisieGroupedsi(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final int typeGroupedsi) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        if (optionModification == -1) {
            return;
        }
        if (optionModification != FormateurJSP.SAISIE_AFFICHAGE) {
            String libelle = "";
            if (infoBean.get("LIBELLE_" + nomDonnee) != null) {
                libelle = infoBean.getString("LIBELLE_" + nomDonnee);
            }
            String value = "";
            if (infoBean.get(nomDonnee) != null) {
                value = infoBean.getString(nomDonnee);
            }
            String res = "<input type=\"hidden\" name=\"" + nomDonnee;
            res += "\" value=\"" + value;
            res += "\" />";
            // JSS 20040419 : filtrage de l'arbre des groupes
            String typeAide = "groupe_dsi";
            if (typeGroupedsi == GROUPEDSI_PUBLIC_VISE) {
                typeAide = "public_vise_dsi";
            }
            // Controle si le filtre est activé
            if ("1".equals(infoBean.get("GRS_FILTRE_ARBRE_GROUPE")) || "1".equals(infoBean.get("GRS_FILTRE_ARBRE_GROUPE_" + nomDonnee.toUpperCase()))) {
                // Controle si affichage ecran PRINCIPAL ou RECHERCHE
                // Valorisé dans ControleurUniv
                final String type = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_TYPE");
                final String objet = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_OBJET");
                final String action = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_ACTION");
                if (type != null && type.length() > 0 && objet != null && objet.length() > 0) {
                    typeAide = "groupebo/";
                    if (typeGroupedsi == GROUPEDSI_PUBLIC_VISE) {
                        typeAide = "publicbo/";
                    }
                    typeAide += type + "/" + objet + "/" + action + "/";
                }
            }
            res += "<input type=\"button\" class=\"button\" value=\"Parcourir\" onclick=\"showMessageField('" + typeAide + "','" + nomDonnee + "','LIBELLE_" + nomDonnee + "');\" /> ";
            if (optionModification == FormateurJSP.SAISIE_FACULTATIF) {
                res += "<input type=\"button\" class=\"button\" value=\"Effacer\" onclick=\"effacerTextField('" + nomDonnee + "','LIBELLE_" + nomDonnee + "','','Cliquer sur parcourir');\"/> ";
            }
            if ("-".equals(libelle) || "".equals(libelle)) {
                libelle = "Cliquer sur parcourir";
            }
            res += "<input type=\"text\" readonly=\"readonly\" name=\"LIBELLE_";
            res += nomDonnee;
            res += "\" size=\"30\" value=\"";
            res += libelle;
            if (value.length() > 0) {
                res += "\" title=\"" + serviceGroupeDsi.getBreadCrumbs(value, StringUtils.EMPTY);
            }
            res += "\"/> ";
            out.println(res);
        } else {
            fmt.insererChampSaisie(out, infoBean, "LIBELLE_" + nomDonnee, FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 0);
        }
    }

    /**
     * Insère le code HTML d'une saisie d'un groupe DSI
     *
     * Le libellé, si le code est prévalorisé doit être dans la donnée LIBELLE_[NOMCODE).
     *
     * @param fmt
     *            the fmt
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomZone
     *            the nom zone
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void insererSaisieGroupeDsi(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        insererContenuSaisieGroupeDsi(fmt, out, infoBean, nomDonnee, optionModification, nomZone);
        insererFinChamp(out);
    }

    /**
     * Insérer le composant de saisie multi-valuée
     *
     */
    public void insererKmultiSelectLtl(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final String nomHashtable, final String format, final int context) throws IOException {
        insererKmultiSelect(fmt, out, infoBean, nomDonnee, optionModification, nomZone, nomHashtable, format, "ltl", context, "", false);
    }

    public void insererKmultiSelectLtl(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final String nomHashtable, final String format, final int context, final boolean editable) throws IOException {
        insererKmultiSelect(fmt, out, infoBean, nomDonnee, optionModification, nomZone, nomHashtable, format, "ltl", context, "", editable);
    }

    public void insererKmultiSelectTtl(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final String nomHashtable, final String format, final int context, final String nomFonctionPopUp) throws IOException {
        insererKmultiSelect(fmt, out, infoBean, nomDonnee, optionModification, nomZone, nomHashtable, format, "ttl", context, nomFonctionPopUp, false);
    }

    public void insererKmultiSelectTtl(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final String nomHashtable, final String format, final int context, final String nomFonctionPopUp, final boolean editable) throws IOException {
        insererKmultiSelect(fmt, out, infoBean, nomDonnee, optionModification, nomZone, nomHashtable, format, "ttl", context, nomFonctionPopUp, editable);
    }

    private void insererKmultiSelect(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, String nomHashtable, final String format, final String type, final int context, final String nomFonctionPopUp, final boolean editable) throws IOException {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        if (optionModification == FormateurJSP.SAISIE_AFFICHAGE) {
            out.write("<p><span class=\"label colonne\">" + nomZone + "</span>");
            fmt.insererChampSaisie(out, infoBean, "LIBELLE_" + nomDonnee, FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 0);
            out.write("</p>");
            return;
        }
        final StringBuffer kMultiSelect = new StringBuffer("<div>");
        final Map<String, String> actionMap = genererAction(context, infoBean, nomDonnee, nomFonctionPopUp, false);
        if (StringUtils.isNotBlank(nomZone)) {
            kMultiSelect.append("<strong class=\"label colonne ").append(optionModification == 2 ? "obligatoire" : "").append("\">").append(nomZone).append(optionModification == 2 ? " (*)" : "").append("</strong>");
        }
        kMultiSelect.append("<div id=\"kmultiselect").append(nomDonnee).append("\" class=\"kmultiselect-").append(type).append(optionModification == 0 ? " locked" : "").append(editable ? " editable\" " : "\" ").append("data-addAction=\"").append(actionMap.get("action")).append("\" ").append("data-popintitle=\"LOCALE_BO.popin.title.").append(actionMap.get("title")).append("\" ").append("data-popinwidth=\"").append(actionMap.get("width")).append("\" ").append("data-popinvalidate=\"").append(actionMap.get("validate")).append("\">");
        // Hidden input fields
        kMultiSelect.append(fmt.genererHiddenInputs(!"ltl".equals(type), nomDonnee, optionModification, format, infoBean, context));
        // Parent list (only in LtL mode)
        if ("ltl".equals(type)) {
            Map<String, String> table = new TreeMap<>();
            final Locale localeCourante = ContexteUtil.getContexteUniv().getLocale();
            switch (context) {
                case CONTEXT_STRUCTURES_ENSEIGNEMENT:
                    final Collection<StructureModele> listeStructures = serviceStructure.getAllSubStructures(StringUtils.EMPTY, LangueUtil.getIndiceLocaleDefaut(), true);
                    final String indiceLocale = String.valueOf(LangueUtil.getIndiceLocale(localeCourante));
                    for (StructureModele infosSt : listeStructures) {
                        String libelleFiche = infosSt.getLibelleLong();
                        if (!indiceLocale.equals(infosSt.getLangue())) {
                            final StructureModele structure = serviceStructure.getByCodeLanguage(infosSt.getCode(), indiceLocale, true);
                            if(structure != null) {
                                libelleFiche = structure.getLibelleLong();
                            }
                        }
                        if ("0".equals(infosSt.getAttributSpecifique1())) {
                            table.put(infosSt.getCode(), libelleFiche);
                        }
                    }
                    infoBean.set(nomZone, table);
                    nomHashtable = nomZone;
                    break;
                default:
                    infoBean.set(nomZone, table);
                    break;
            }
            Map<String, String> tempTable = infoBean.getMap(nomHashtable);
            if (tempTable == null) {
                tempTable = CodeLibelle.lireTable(infoBean.getNomExtension(), nomHashtable, localeCourante);
                if (tempTable == null) {
                    table = new TreeMap<>();
                } else {
                    table = new TreeMap<>(tempTable);
                }
            } else {
                table = new TreeMap<>(tempTable);
            }
            kMultiSelect.append("<div class=\"kscrollable\">");
            kMultiSelect.append("<ul class=\"ui-sortable kmultiselect-list kmultiselect-parent-list\">");
            kMultiSelect.append(fmt.genererListe(out, infoBean, nomDonnee, optionModification, table, format, context));
            kMultiSelect.append("</ul>");
            kMultiSelect.append("</div>");
        }
        // Composition list
        kMultiSelect.append("<div class=\"kscrollable\">");
        kMultiSelect.append("<ul class=\"ui-sortable kmultiselect-list kmultiselect-composition-list\">");
        kMultiSelect.append("</ul>");
        kMultiSelect.append("</div>");
        // Close tags
        kMultiSelect.append("</div>");
        kMultiSelect.append("</div>");
        out.println(kMultiSelect);
    }

    /**
     * Permet de générer un champ de recherche de rédacteur si l'utilisateur à les droits nécessaires.
     * @param fmt c'est historique
     * @param out c'est pour écrire directement la réponse dedans...
     * @param infoBean pour les données déjà saisies
     * @param nomDonnee le nom de la donnée à récup dans l'infobean
     * @param nomZone le label de la zone.
     * @throws IOException
     */
    public void insererRechercheUtilisateur(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final String nomZone) throws IOException {
        ContexteUniv ctx = ContexteUtil.getContexteUniv();
        AutorisationBean autorisations = ctx.getAutorisation();
        boolean isAutorise = autorisations != null && (autorisations.possedePermission(ComposantUtilisateur.getPermissionConsultation()) || autorisations.isWebMaster());
        if (isAutorise) {
            insererkMonoSelect(fmt, out, infoBean, nomDonnee, FormateurJSP.SAISIE_FACULTATIF, nomZone, "utilisateur", UnivFmt.CONTEXT_ZONE);
        }
    }

    /**
     * Insérer le composant de saisie mono-valuée
     * @throws IOException
     *
     */
    public void insererkMonoSelect(final FormateurJSP fmt, final JspWriter out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone, final String nomFonctionPopUp, final int context) throws IOException {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        if (optionModification == FormateurJSP.SAISIE_AFFICHAGE) {
            out.write("<p><span class=\"label colonne\">" + nomZone + "</span>");
            fmt.insererChampSaisie(out, infoBean, "LIBELLE_" + nomDonnee, FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 0);
            out.write("</p>");
            return;
        }
        String libelle = "";
        if (StringUtils.isNotEmpty(infoBean.getString("LIBELLE_" + nomDonnee))) {
            libelle = infoBean.getString("LIBELLE_" + nomDonnee);
        } else if (StringUtils.isNotEmpty(infoBean.getString(nomDonnee))) {
            libelle = MessageHelper.getCoreMessage("BO_LIBELLE_VIDE");
        }
        final String clearable = (optionModification == 2 || optionModification == 3) ? " not-clearable" : "";
        final StringBuffer kMonoSelect = new StringBuffer("");
        if (!StringUtils.isBlank(nomZone)) {
            kMonoSelect.append("<div>");
            kMonoSelect.append("<span class=\"label colonne").append(optionModification == 2 ? " obligatoire" : " ").append("\">").append(nomZone).append((optionModification == 2 || optionModification == 3) ? " (*)" : "").append("</span>");
        }
        final String placeHolder = context == CONTEXT_GROUPEDSI_PUBLIC_VISE || context == CONTEXT_GROUPEDSI_RESTRICTION ? "m" : "f";
        final Map<String, String> actionMap = genererAction(context, infoBean, nomDonnee, nomFonctionPopUp, true);
        kMonoSelect.append("<div id=\"kMonoSelect").append(nomDonnee).append("\" class=\"kmonoselect").append(clearable).append("\" ").append("data-value=\"").append(libelle).append("\" ").append("data-placeholder=\"").append(placeHolder).append("\" ").append("data-editAction=\"").append(actionMap.get("action")).append("\" ").append("data-popintitle=\"LOCALE_BO.popin.title.").append(actionMap.get("title")).append("\" ").append("data-popinwidth=\"").append(actionMap.get("width")).append("\" ").append("data-popinvalidate=\"").append(actionMap.get("validate")).append("\" data-title=\"").append(actionMap.get("tip")).append("\">");
        kMonoSelect.append(fmt.genererHiddenInputs(false, nomDonnee, optionModification, "LIB=" + nomZone, infoBean, context));
        kMonoSelect.append("</div>");
        if (!StringUtils.isBlank(nomZone)) {
            kMonoSelect.append("</div>");
        }
        out.println(kMonoSelect);
    }

    private Map<String, String> genererAction(final int context, final InfoBean infoBean, final String nomDonnee, final String params, final boolean mono) {
        String finalAction = "";
        String titleAction = "";
        String widthAction = "530";
        String validateAction = "true";
        String tip = "";
        boolean frontOffice = false;
        String lg = String.valueOf(LangueUtil.getIndiceLocale(LangueUtil.getDefaultLocale()));
        final String langueInfoBean = infoBean.getString("LANGUE");
        final String langueFicheInfoBean = infoBean.getString("LANGUE_FICHE");
        if (StringUtils.isNotEmpty(langueFicheInfoBean) && StringUtils.isNumeric(langueFicheInfoBean)) {
            lg = langueFicheInfoBean;
        } else if (StringUtils.isNotEmpty(langueInfoBean) && StringUtils.isNumeric(langueInfoBean)) {
            lg = langueInfoBean;
        }
        if (infoBean.get("SAISIE_FRONT") != null) {
            frontOffice = true;
        }
        switch (context) {
            case CONTEXT_ZONE:
                final Matcher matcher = selectPattern.matcher(params);
                final String realParams = matcher.matches() ? matcher.group(2) : params;
                // formulaire de recherche par fiche pour associer une fiche
                if (realParams.startsWith("saisie_")) {
                    widthAction = "700";
                    titleAction = "objet";
                    validateAction = "false";
                    String param = "";
                    if (realParams.contains("&")) {
                        param = StringUtils.substring(realParams, realParams.indexOf("&"));
                    }
                    final String nomObjet = StringUtils.substring(realParams, 7, realParams.length() - param.length());
                    final String codeObjet = ReferentielObjets.getCodeObjet(nomObjet);
                    final String extension = ReferentielObjets.getExtension(codeObjet);
                    final String proc = ReferentielObjets.getProcessus(codeObjet);
                    finalAction = WebAppUtil.SG_PATH + "?EXT=" + extension + "&PROC=" + EscapeString.escapeJavaScript(proc) + "&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE_JOINTURE&LANGUE_FICHE=-1" + param;
                } else if (realParams.startsWith("objet_")) {
                    // recherche d'une fiche parmi plusieurs types (page de tête, fiche parente  commentaire etc)
                    final String toolbox = realParams.substring(6).toUpperCase(); // PAGE_TETE, COMMENTAIRE, ...
                    validateAction = "false";
                    widthAction = "700";
                    titleAction = "objet";
                    finalAction = "/adminsite/toolbox/choix_objet.jsp?TOOLBOX=" + toolbox;
                } else {
                    titleAction = mono ? "rubrique.mono" : "rubrique.multi";
                    validateAction = mono ? "false" : "true";
                    if (!StringUtils.isBlank(URLS.get(realParams))) {
                        widthAction = "700";
                        titleAction = realParams;
                        finalAction = URLS.get(realParams);
                    } else {
                        tip = RubriquesJsTree.getPath("", infoBean.getString(nomDonnee), " > ");
                        String typeAide = realParams;
                        if ("rubrique".equals(typeAide)) {
                            // Controle si la donnée correspond bien à la rubrique de
                            // l'objet métier
                            // pas de filtrage par défaut
                            finalAction = "/adminsite/tree/tree.jsp?JSTREEBEAN=rubriquesJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&UNBINDED_SELECT=true";
                            // filtrage activé dans la page courante
                            if (infoBean.get("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE") != null && infoBean.getString("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE").equals(nomDonnee)) {
                                // Controle si affichage ecran PRINCIPAL ou
                                // RECHERCHE
                                // Valorisé dans ControleurUniv
                                final String type = infoBean.getString("GRS_PERMISSION_TYPE");
                                final String objet = infoBean.getString("GRS_PERMISSION_OBJET");
                                final String action = infoBean.getString("GRS_PERMISSION_ACTION");
                                if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(objet) && (StringUtils.isNotBlank(action) || "acp".equals(objet))) {
                                    typeAide = "/" + type + "/" + objet + "/" + action + "/";
                                    finalAction = "/adminsite/tree/tree.jsp?JSTREEBEAN=rubriquesJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&UNBINDED_SELECT=true&PERMISSION=" + typeAide;
                                }
                            }
                        } else {
                            typeAide = "/" + realParams;
                            finalAction = "/adminsite/tree/tree.jsp?JSTREEBEAN=rubriquesJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&UNBINDED_SELECT=true&PERMISSION=" + typeAide;
                        }
                    }
                }
                break;
            case CONTEXT_STRUCTURE:
                finalAction = "/adminsite/tree/tree.jsp?JSTREEBEAN=structuresJsTree&DISPLAY=full&SELECTED={0}&CODE={1}";
                titleAction = mono ? "structure.mono" : "structure.multi";
                validateAction = mono ? "false" : "true";
                tip = StructuresJsTree.getPath("", infoBean.getString(nomDonnee), " > ");
                if (frontOffice) {
                    finalAction += "&LANGUE=" + lg;
                    finalAction += "&RACINE=" + nomDonnee;
                    finalAction += "&FRONT=true";
                    if (infoBean.get("FILTRE_" + nomDonnee) != null) {
                        finalAction += "&FILTRE=" + EscapeString.escapeJavaScript(infoBean.getString("FILTRE_" + nomDonnee));
                    }
                } else {
                    // création, l'arbre des structures contiendra toutes les
                    // langues
                    if (infoBean.getEtatObjet() != null && !InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                        finalAction += "&LANGUE=" + lg;
                    }
                    // Filtrage de l'arbre des structures en fonction des
                    // permissions
                    // JSS 20051031 : rattachement secondaires
                    String nomDonneeFiltreArbre = nomDonnee;
                    if (nomDonnee.startsWith("TMP")) {
                        // On enlève le TMP_ (pour les listes)
                        nomDonneeFiltreArbre = nomDonnee.substring(4);
                    }
                    // Controle si la donnée correspond bien à la structure de
                    // l'objet métier
                    if (infoBean.get("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT") != null && infoBean.getString("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT").equals(nomDonneeFiltreArbre) || infoBean.get("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT_AUTRES") != null && infoBean.getString("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT_AUTRES").equals(nomDonneeFiltreArbre)) {
                        // Controle si affichage ecran PRINCIPAL ou RECHERCHE
                        // Valorisé dans ControleurUniv
                        final String type = infoBean.getString("GRS_PERMISSION_TYPE");
                        final String objet = infoBean.getString("GRS_PERMISSION_OBJET");
                        final String action = infoBean.getString("GRS_PERMISSION_ACTION");
                        if (!StringUtils.isBlank(type) && !StringUtils.isBlank(objet) && !StringUtils.isBlank(action)) {
                            finalAction += "&PERMISSION=/" + type + "/" + objet + "/" + action + "/";
                        }
                    }
                    if (infoBean.get("FILTRE_" + nomDonnee) != null) {
                        finalAction += "&FILTRE=" + EscapeString.escapeJavaScript(infoBean.getString("FILTRE_" + nomDonnee));
                    }
                    if (infoBean.get("RACINE_" + nomDonnee) != null) {
                        finalAction += "&RACINE=" + EscapeString.escapeJavaScript(infoBean.getString("RACINE_" + nomDonnee));
                    }
                }
                break;
            case CONTEXT_GROUPEDSI_RESTRICTION:
                // JSS 20040419 : filtrage de l'arbre des groupes
                finalAction = "/adminsite/tree/tree.jsp?JSTREEBEAN=groupsJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&UNBINDED_SELECT=true" + ("lock_dyn".equals(params) ? "&LOCK_DYN=true" : StringUtils.EMPTY);
                titleAction = mono ? "groupe.mono" : "groupe.multi";
                validateAction = mono ? "false" : "true";
                tip = GroupsJsTree.getPath("", infoBean.getString(nomDonnee), " > ");
                // Controle si le filtre est activé
                if ("1".equals(infoBean.get("GRS_FILTRE_ARBRE_GROUPE")) || "1".equals(infoBean.get("GRS_FILTRE_ARBRE_GROUPE_" + nomDonnee.toUpperCase()))) {
                    // Controle si affichage ecran PRINCIPAL ou RECHERCHE
                    // Valorisé dans ControleurUniv
                    final String type = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_TYPE");
                    final String objet = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_OBJET");
                    final String action = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_ACTION");
                    if (!StringUtils.isBlank(type) && !StringUtils.isBlank(objet)) {
                        finalAction += "&PERMISSION=/" + type + "/" + objet + "/" + action + "/";
                    }
                }
                break;
            case CONTEXT_GROUPEDSI_PUBLIC_VISE:
                // JSS 20040419 : filtrage de l'arbre des groupes
                finalAction = "/adminsite/tree/tree.jsp?JSTREEBEAN=groupsJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&PUBLIC_VISE=1";
                titleAction = mono ? "groupe.mono" : "groupe.multi";
                validateAction = mono ? "false" : "true";
                tip = GroupsJsTree.getPath("", infoBean.getString(nomDonnee), " > ");
                // Controle si le filtre est activé
                if ("1".equals(infoBean.get("GRS_FILTRE_ARBRE_GROUPE")) || "1".equals(infoBean.get("GRS_FILTRE_ARBRE_GROUPE_" + nomDonnee.toUpperCase()))) {
                    // Controle si affichage ecran PRINCIPAL ou RECHERCHE
                    // Valorisé dans ControleurUniv
                    final String type = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_TYPE");
                    final String objet = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_OBJET");
                    final String action = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_ACTION");
                    if (!StringUtils.isBlank(type) && !StringUtils.isBlank(objet)) {
                        finalAction += "&PERMISSION=/" + type + "/" + objet + "/" + action + "/";
                    }
                }
                break;
        }
        final HashMap<String, String> map = new HashMap<>();
        map.put("action", EscapeString.escapeAttributHtml(finalAction));
        map.put("title", EscapeString.escapeAttributHtml(titleAction));
        map.put("width", EscapeString.escapeAttributHtml(widthAction));
        map.put("validate", EscapeString.escapeAttributHtml(validateAction));
        map.put("tip", EscapeString.escapeAttributHtml(tip));
        return map;
    }
}
