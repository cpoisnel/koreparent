package com.univ.utils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.htmlparser.util.ParserException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Document.OutputSettings;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.HTMLUtil;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.identification.GestionnaireIdentification;
import com.jsbsoft.jtf.identification.SourceAuthClearTrust;
import com.jsbsoft.jtf.identification.ValidateurCAS;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.jsbsoft.jtf.textsearch.ResultatRecherche;
import com.jsbsoft.jtf.webutils.FormateurHTML;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;
import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.kportal.extension.module.plugin.rubrique.impl.BeanFichePageAccueil;
import com.kportal.extension.module.plugin.toolbox.PluginTagHelper;
import com.univ.mediatheque.Mediatheque;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.om.MediaPhoto;
import com.univ.objetspartages.om.PageLibre;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.ServiceBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.url.FrontOfficeMgr;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.xhtml.HTMLParser;

/**
 * Fonction de formatage des JSP pour le front-office.
 */
public class UnivWebFmt extends FormateurHTML {

    public static final String OLD_TAG_URL_LOGIN = "/servlet/[href_logindsi]";

    public static final String TAG_URL_LOGIN = "[href_logindsi]";

    public static final String OLD_TAG_URL_PREF = "/servlet/[href_prefdsi]";

    public static final String TAG_URL_PREF = "[href_prefdsi]";

    public static final String OLD_TAG_URL_LOGOUT = "/servlet/[href_logoutdsi]";

    public static final String TAG_URL_LOGOUT = "[href_logoutdsi]";

    public static final String TAG_URL_ACCUEIL = "[href_home]";

    private static final Logger LOGGER = LoggerFactory.getLogger(UnivWebFmt.class);

    /**
     * Gets the url fichier joint.
     *
     * @param ctx       the ctx
     * @param ficheUniv the ficheUniv
     * @return the url fichier joint
     * @deprecated garder pour la compatibilité avec les fronts 5.1. Ne pas utiliser. Si vous devez migrer, il suffit de mettre l'url
     * de la fiche pour que le traitement soit fait :
     * Si la fiche est en "document à télécharger uniquement" et qu'elle ne possède qu'un seul fichier joint, la requête renverra la piece jointe
     * si elle posséde plusieurs fichiers, uniquement les pièces jointes seront affichées mais la JSP d'affichage de la fiche sera bien
     * identique à celle de d'habitude.
     */
    @Deprecated
    public static String getUrlFichierJoint(final ContexteUniv ctx, final FicheUniv ficheUniv) {
        if (ficheUniv == null) {
            return StringUtils.EMPTY;
        }
        return ctx.getInfosSite().getJspFo() + "/saisie/liste_fichiergw.jsp?OBJET=DOCUMENT&amp;CODE=" + ficheUniv.getCode() + "&amp;LANGUE=" + ficheUniv.getLangue();
    }

    /**
     * Retourne l'url d'acces a l'accueil de la dsi, avec ssl ou pas.
     *
     * @param ctx the ctx
     * @return the url accueil dsi
     */
    public static String getUrlAccueilDsi(final ContexteUniv ctx) {
        return getUrlDsi("REVENIR_ACCUEIL", true);
    }

    /**
     * Renvoie l'url relative de deconnection.
     *
     * @param ctx the ctx
     * @return the url deconnexion
     */
    public static String getUrlDeconnexion(final ContexteUniv ctx) {
        return getUrlDeconnexion(ctx, true, true);
    }

    public static String getUrlDeconnexion(final ContexteUniv ctx, final boolean ampersands) {
        return getUrlDeconnexion(ctx, ampersands, true);
    }

    public static String getUrlDeconnexion(final ContexteUniv ctx, final boolean ampersands, final boolean front) {
        String url = getUrlDsi("DECONNECTER", false);
        if (!front) {
            url = StringUtils.remove(url, "_FRONT");
        }
        // logout de CAS
        final GestionnaireIdentification gI = GestionnaireIdentification.getInstance();
        if (gI.estSourceAuth(ValidateurCAS.SOURCE_LIBELLE_CAS) && StringUtils.isNotEmpty(gI.getValidateurCAS().getUrlCasLogoutFront(ctx))) {
            if (front) {
                return gI.getValidateurCAS().getUrlCasLogoutFront(ctx);
            } else {
                return gI.getValidateurCAS().getUrlCasLogout(ctx);
            }
        } else if (gI.estSourceAuth(SourceAuthClearTrust.SOURCE_LIBELLE_CT)) {
            final SourceAuthClearTrust authCT = (SourceAuthClearTrust) gI.getSourceAuth();
            if (front) {
                url = URLResolver.getAbsoluteUrl(url, ctx, URLResolver.DECONNECTER_FRONT);
            } else {
                url = URLResolver.getAbsoluteBoUrl(url, ctx.getInfosSite());
            }
            return authCT.getUrlCtrustLogout(ctx, url);
        }
        if (ctx.getCodeRubriquePageCourante().length() > 0) {
            url += "&RH=" + EscapeString.escapeURL(ctx.getCodeRubriquePageCourante());
        }
        if (ampersands) {
            url = StringUtils.replace(url, "&", "&amp;");
        }
        return url;
    }

    /**
     * Retourne l'url d'acces aux preferences dsi, avec ssl ou pas.
     *
     * @param ctx the ctx
     * @return the url preferences
     */
    public static String getUrlPreferences(final ContexteUniv ctx) {
        String urlPreferences = getUrlDsi("PERSONNALISER", true);
        if (StringUtils.isNotBlank(ctx.getCodeRubriquePageCourante())) {
            urlPreferences += "&amp;RH=" + EscapeString.escapeURL(ctx.getCodeRubriquePageCourante());
        }
        return urlPreferences;
    }

    /**
     * Retourne l'url d'accès à la demande de mot de passe, avec ssl ou pas.
     *
     * @param ctx the ctx
     * @return the url demande de mote passe
     */
    public static String getUrlDemandeMotDePasse(final ContexteUniv ctx) {
        String urlDemandeMotDePasse = getUrlDsi("DEMANDER_MDP", true);
        if (StringUtils.isNotBlank(ctx.getCodeRubriquePageCourante())) {
            urlDemandeMotDePasse += "&amp;RH=" + EscapeString.escapeURL(ctx.getCodeRubriquePageCourante());
        }
        return urlDemandeMotDePasse;
    }

    /**
     * Retourne l'url d'acces au login dsi, avec ssl ou pas.
     *
     * @param ctx        the ctx
     * @param ampersands the ampersands
     * @return the url login
     */
    public static String getUrlLogin(final ContexteUniv ctx, final boolean ampersands) {
        final String esperluette = ampersands ? "&amp;" : "&";
        // JB AM 200501 : login de CAS
        final GestionnaireIdentification gI = GestionnaireIdentification.getInstance();
        String url;
        if (gI.estSourceAuth(ValidateurCAS.SOURCE_LIBELLE_CAS)) {
            url = gI.getValidateurCAS().getUrlCasLoginFront(ctx);
        } else {
            url = getUrlDsi("CONNECTER", ampersands);
            if (ctx.getCodeRubriquePageCourante().length() > 0) {
                url += esperluette + "RH=" + ctx.getCodeRubriquePageCourante();
            }
        }
        return url;
    }

    /**
     * Retourne l'url de connexion.
     *
     * @param ctx the ctx
     * @return the url connexion
     */
    public static String getUrlConnexion(final ContexteUniv ctx) {
        return getUrlLogin(ctx, true);
    }

    /**
     * Retourne l'url d'acces a la deconnexion, avec ssl ou pas.
     *
     * @param req the _req
     * @param ctx the ctx
     * @return the url connexion externe
     */
    public static String getUrlConnexionExterne(final HttpServletRequest req, final ContexteUniv ctx) {
        // JB AM 200501 : logout de CAS
        // JB 20050705 : la synchro du logout est maintenant optionnelle
        final GestionnaireIdentification gI = GestionnaireIdentification.getInstance();
        String url = StringUtils.EMPTY;
        if (gI.estSourceAuth(ValidateurCAS.SOURCE_LIBELLE_CAS)) {
            url = gI.getValidateurCAS().getUrlCasLoginFront(ctx);
        } else if (gI.estSourceAuth(SourceAuthClearTrust.SOURCE_LIBELLE_CT)) {
            final SourceAuthClearTrust authCT = (SourceAuthClearTrust) gI.getSourceAuth();
            url = URLResolver.getAbsoluteUrl(authCT.getUrlCtrustLogin(ctx), ctx);
        }
        return url;
    }

    /**
     * Gets the url dsi.
     *
     * @param action     the action
     * @param ampersands the ampersands
     * @return the url dsi
     */
    private static String getUrlDsi(final String action, final boolean ampersands) {
        final String esperluette = ampersands ? "&amp;" : "&";
        return WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION_FRONT" + esperluette + "ACTION=" + action;
    }

    /**
     * Ajoute un lien vers une page fille.
     *
     * @param ctx     the ctx
     * @param onglets the _onglets
     * @param libelle the libelle
     * @param objet   the objet
     * @param code    the code
     */
    private static void ajouterLienEncadre(final ContexteUniv ctx, final Map<String, String> onglets, final String libelle, final String objet, final String code) {
        onglets.put(libelle.toUpperCase(), "<a href=\"" + URLResolver.getAbsoluteUrl(determinerUrlFiche(ctx, objet, code), ctx) + "\">" + libelle + "</a>");
    }

    /**
     * Ajoute l'onglet courant au vecteur d'onglets.
     *
     * @param ctx               the ctx
     * @param collectionOnglets the _collection onglets
     * @param rubrique          the rubrique
     * @param stNav             the st nav
     * @throws Exception the exception
     */
    @SuppressWarnings("unchecked")
    private static void ajouterOngletEnteteRubrique(final ContexteUniv ctx, final Object collectionOnglets, final RubriqueBean rubrique, final int stNav) throws Exception {
        String codeHTML;
        final String url = renvoyerUrlAccueilRubrique(ctx, rubrique.getCode());
        if (StringUtils.isEmpty(url)) {
            codeHTML = rubrique.getNomOnglet();
        } else {
            codeHTML = "<a href=\"" + URLResolver.getAbsoluteUrl(url, ctx) + "\">" + rubrique.getNomOnglet() + "</a>";
        }
        if (collectionOnglets instanceof Vector) {
            ((Vector<String>) collectionOnglets).add(0, codeHTML);
        }
        if (collectionOnglets instanceof TreeMap) {
            ((TreeMap<String, String>) collectionOnglets).put(rubrique.getNomOnglet(), codeHTML);
        }
    }

    /**
     * Determine l'url d'une fiche a partir de l'objet.
     *
     * @param ctx   the ctx
     * @param fiche the fiche
     * @return the string
     */
    public static String determinerUrlFiche(final ContexteUniv ctx, final FicheUniv fiche) {
        return determinerUrlFiche(ctx, fiche, true);
    }

    /**
     * Determiner url fiche.
     *
     * @param ctx       the ctx
     * @param fiche     the fiche
     * @param ampersand the ampersand
     * @return String url de la fiche, en francais par defaut ou de la langue de la rubrique de la fiche si on ne force pas la langue si la langue est specifiee, String vide ou url
     * de la fiche dans cette langue (utile pour le multilingue)
     */
    public static String determinerUrlFiche(final ContexteUniv ctx, final FicheUniv fiche, final boolean ampersand) {
        if (fiche == null) {
            return StringUtils.EMPTY;
        }
        final String code = ReferentielObjets.getCodeObjetParClasse(fiche.getClass().getName());
        final String nomObjet = ReferentielObjets.getNomObjet(code);
        String langue = StringUtils.EMPTY;
        if (!fiche.getLangue().equals(LangueUtil.getLangueLocale(ctx.getLocale()))) {
            langue = ",LANGUE=" + fiche.getLangue();
        }
        return determinerUrlFiche(ctx, nomObjet, fiche.getCode() + langue, ampersand);
    }

    /**
     * Determine l'identifiant d'une fiche a partir du nom de l'objet et du code AM 072003 : stNav permet de definir si l'on ramene la structure de navigation ou non : utilise pour
     * les urls du menu de gauche, qui est generique.
     *
     * @param ctx   the ctx
     * @param objet the objet
     * @param code  the code
     * @return the string
     */
    public static String determinerUrlFiche(final ContexteUniv ctx, final String objet, final String code) {
        return determinerUrlFiche(ctx, objet, code, true);
    }

    /**
     * Renvoie l'url absolue de la fiche.
     *
     * @param ctx        the ctx
     * @param objet      the objet
     * @param code       the code
     * @param ampersands the ampersands
     * @return String url permettant d'acceder e une fiche AM 072003 : stNav permet de definir si l'on ramene la structure de navigation ou non : utilise pour les urls du menu de
     * gauche, qui est generique
     */
    public static String determinerUrlFiche(final ContexteUniv ctx, final String objet, final String code, final boolean ampersands) {
        final String esperluette = ampersands ? "&amp;" : "&";
        String urlFiche = StringUtils.EMPTY;
        if (StringUtils.isEmpty(code) || StringUtils.contains(code, "=") && !StringUtils.contains(code, ",LANGUE=")) {
            /**
             * Patch JRO 20091230 : possibilite de choisir une charte par site
             */
            urlFiche = RechercheFicheHelper.getUrlResults("OBJET=" + objet + "&" + code, "0", ctx.getCodeRubriquePageCourante());
            urlFiche = StringUtils.replace(urlFiche, "&", "&amp;");
            /** Fin patch */
            if (ctx.getRubriqueExterneConnecteur().length() > 0) {
                urlFiche += esperluette + "RUBNAV=" + EscapeString.escapeURL(ctx.getRubriqueExterneConnecteur());
            }
        } else {
            String codeFiche = StringUtils.EMPTY;
            String langueFiche = StringUtils.EMPTY;
            if (!StringUtils.contains(code, ",LANGUE=")) {
                codeFiche = code;
                langueFiche = LangueUtil.getLangueLocale(ctx.getLocale());
            } else {
                codeFiche = code.substring(0, code.indexOf(",LANGUE="));
                langueFiche = code.substring(code.indexOf(",LANGUE=") + 8);
            }
            urlFiche = determinerUrlFiche(ctx, objet, codeFiche, langueFiche, ampersands);
        }
        return urlFiche;
    }

    /**
     * Renvoie l'url absolue de la fiche.
     *
     * @param ctx         the ctx
     * @param objet       the objet
     * @param codeFiche   the code fiche
     * @param langueFiche the langue fiche
     * @param ampersands  the ampersands
     * @return String url permettant d'acceder e une fiche AM 072003 : stNav permet de definir si l'on ramene la structure de navigation ou non : utilise pour les urls du menu de
     * gauche, qui est generique
     */
    public static String determinerUrlFiche(final OMContext ctx, final String objet, final String codeFiche, final String langueFiche, final boolean ampersands) {
        return determinerUrlFiche(ctx, objet, codeFiche, langueFiche, ampersands, StringUtils.EMPTY);
    }

    /**
     * Renvoie l'url absolue de la fiche.
     *
     * @param ctx                the ctx
     * @param objet              the objet
     * @param codeFiche          the code fiche
     * @param langueFiche        the langue fiche
     * @param ampersands         the ampersands
     * @param codeRubriqueForcee the code rubrique forcee
     * @return String url permettant d'acceder a une fiche AM 072003 : stNav permet de definir si l'on ramene la structure de navigation ou non : utilise pour les urls du menu de
     * gauche, qui est generique
     */
    public static String determinerUrlFiche(final OMContext ctx, final String objet, final String codeFiche, final String langueFiche, final boolean ampersands, final String codeRubriqueForcee) {
        final String esperluette = ampersands ? "&amp;" : "&";
        String urlFiche = StringUtils.EMPTY;
        String rubNav = StringUtils.EMPTY;
        String rubHistorique = StringUtils.EMPTY;
        if (ctx instanceof ContexteUniv) {
            if (((ContexteUniv) ctx).isGenererLiensInternes()) {
                if (((ContexteUniv) ctx).getCodesLiensInternes().contains(objet.toLowerCase() + "/" + codeFiche + "/" + langueFiche)) {
                    return "#" + objet.toLowerCase() + "/" + codeFiche + "/" + langueFiche;
                }
            }
            rubNav = ((ContexteUniv) ctx).getRubriqueExterneConnecteur();
            rubHistorique = ((ContexteUniv) ctx).getCodeRubriquePageCourante();
        }
        final String codeObjet = ReferentielObjets.getCodeObjet(objet.toLowerCase());
        String params = StringUtils.EMPTY;
        if (rubNav.length() > 0) {
            if (params.length() > 0) {
                params += esperluette;
            }
            params += "RUBNAV=" + EscapeString.escapeURL(rubNav);
        }
        if (rubHistorique.length() > 0) {
            if (params.length() > 0) {
                params += esperluette;
            }
            params += "RH=" + EscapeString.escapeURL(rubHistorique);
        }
        // URL FRIENDLY
        urlFiche = FrontOfficeMgr.getInstance().getAbsoluteUrlFriendly(ctx, codeObjet, codeFiche, langueFiche, "0003", codeRubriqueForcee, params);
        return urlFiche;
    }

    /**
     * /** Formatage d'une chaine en HTML - conversion des "\n" en <BR>
     * - interpretation des liens hypertexte.
     *
     * @param ctx   the ctx
     * @param texte the _texte
     * @return the string
     * @throws Exception the exception
     */
    public static String formaterEnHTML(final ContexteUniv ctx, final String texte) throws Exception {
        return formaterEnHTML(ctx, texte, true);
    }

    public static String formaterEnHTML(final ContexteUniv ctx, final String texte, final boolean formaterEnHtml) throws Exception {
        return formaterEnHTML(ctx, texte, formaterEnHtml, true);
    }

    /**
     * Formatage d'une chaine en HTML (avec des urls absolues) - conversion des "\n" en <BR>
     * - interpretation des liens hypertexte.
     *
     * @param ctx            the ctx
     * @param _texte         the _texte
     * @param formaterEnHtml the formater en html
     * @return the string
     * @throws Exception the exception
     */
    public static String formaterEnHTML(final ContexteUniv ctx, final String _texte, final boolean formaterEnHtml, final boolean handleLinks) throws Exception {
        if (StringUtils.isBlank(_texte)) {
            return _texte;
        }
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        final int lgBuffer = _texte.length() + 100;
        // nettoyage des attributs non valides inseres par la toolbox
        String texte = ToolboxUtil.nettoyerAttributsNonValides(_texte);
        texte = PluginTagHelper.interpreterTags(texte);
        // transformation des tags
        texte = transformerTagsPersonnalisation(ctx, texte);
        int indexTexte = 0;
        int indexDebutMotCle = -1;
        // JSS 20020611-003
        /** ****************************** */
        /* Gestion des methode d'exit */
        /** ****************************** */
        StringBuffer newTexte = new StringBuffer(lgBuffer);
        /* Boucle sur chaque mot cle */
        while ((indexDebutMotCle = texte.indexOf("[traitement", indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            int indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            final String contenuTag = Chaine.remplacerPointsVirgules(texte.substring(indexDebutMotCle + 1, indexFinMotCle));
            String action = StringUtils.EMPTY;
            String paramExit = StringUtils.EMPTY;
            String resultatExit = StringUtils.EMPTY;
            final StringTokenizer st = new StringTokenizer(contenuTag, "*");
            int indiceToken = 0;
            while (st.hasMoreTokens()) {
                final String itemTag = st.nextToken();
                if (indiceToken == 1) {
                    action = itemTag;
                }
                if (indiceToken == 2) {
                    paramExit = EscapeString.unescapeHtml(itemTag);
                    paramExit = StringUtils.replace(paramExit, "#", "&");
                }
                indiceToken++;
            }
            /* OLD TRAITEMENT REQUETE */
            if (ctx.getJsp() != null) {
                // appel de la méthode dans la JSP
                final Class<?>[] params = {Class.forName("com.univ.utils.ContexteUniv"), Class.forName("java.lang.String"), Class.forName("java.lang.String")};
                final Class<?> classJSP = ctx.getJsp().getClass();
                final Method methodeExit = MethodUtils.getAccessibleMethod(classJSP, "methodeExit", params);
                if (methodeExit != null) {
                    final Object[] args = {ctx, action, paramExit};
                    resultatExit = (String) methodeExit.invoke(ctx.getJsp(), args);
                }
            } else {
                // pas de page d'exit : traitement par defaut utile notamment pour les mailings
                ctx.setCalculListeResultatsFront(true);
                final List<ResultatRecherche> listeFiches = RequeteUtil.traiterRequete(ctx, paramExit);
                ctx.setCalculListeResultatsFront(false);
                for (ResultatRecherche res : listeFiches) {
                    final FicheUniv fiche = RequeteUtil.lireFiche(res);
                    final String urlFiche = UnivWebFmt.determinerUrlFiche(ctx, fiche);
                    resultatExit = resultatExit + "<a href=\"" + URLResolver.getAbsoluteUrl(urlFiche, ctx) + "\" class=\"txtcourant\">" + fiche.getLibelleAffichable() + "</a><br />";
                }
            }
            newTexte.append(resultatExit);
            indexTexte = indexFinMotCle + 1;
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        /** ************************** */
        /* Transformation des images */
        /** ************************** */
        /* Boucle sur chaque mot clé */
        final List<MediaBean> lstMedia = new ArrayList<>();
        while ((indexDebutMotCle = texte.indexOf("[id-image]", indexTexte)) != -1) {
            // Recopie portion avant le mot- clé
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Recherche de l'ID de l'image
            int indexFinMotCle = texte.indexOf("[/id-image]", indexDebutMotCle);
            // JSS 20020910-001 : Fin du traitement si tag incomplet
            if (indexFinMotCle == -1) {
                break;
            }
            final int indexDebutMotCleSuivant = texte.indexOf("[id-image]", indexDebutMotCle + 1);
            if (indexDebutMotCleSuivant != -1 && indexDebutMotCleSuivant < indexFinMotCle) {
                break;
            }
            String image = texte.substring(indexDebutMotCle + 10, indexFinMotCle);
            if (image.startsWith("F")) {
                image = image.substring(1);
            }
            final MediaBean media = serviceMedia.getById(Long.valueOf(image));
            if (media != null) {
                String url = MediaUtils.getUrlAbsolue(media);
                if (!MediaUtils.isPublic(media) && ctx.getFicheCourante() != null) {
                    url += (url.contains("?") ? "&amp;" : "?") + "ID_FICHE=" + ctx.getFicheCourante().getIdFiche();
                }
                // insertion de l'utl de l'image
                if (MediaUtils.isLocal(media)) {
                    newTexte.append(URLResolver.getAbsoluteUrl(url, ctx));
                } else {
                    newTexte.append(url);
                }
                lstMedia.add(media);
            }
            indexTexte = indexFinMotCle + 11;
        }
        // Recopie dernière portion de texte
        newTexte.append(texte.substring(indexTexte));
        texte = new String(newTexte);
        // traitement des alt et title des images traitées
        for (final MediaBean media : lstMedia) {
            texte = texte.replaceAll("\\[legende-image\\]" + media.getId() + "\\[/legende-image\\]", StringUtils.replace(media.getLegende(), "\"", "&#34;"));
            texte = texte.replaceAll("\\[title-image\\]" + media.getId() + "\\[/title-image\\]", StringUtils.replace(media.getTitre(), "\"", "&#34;"));
            String urlVignette = StringUtils.EMPTY;
            if (StringUtils.isNotBlank(MediaUtils.getUrlVignetteAbsolue(media))) {
                urlVignette = MediaUtils.getUrlVignetteAbsolue(media);
            } else if (Mediatheque.getInstance().getRessource(media.getTypeRessource()) != null) {
                urlVignette = Mediatheque.getInstance().getRessource(media.getTypeRessource()).getUrlVignette();
            }
            texte = texte.replaceAll("\\[vignette-image\\]" + media.getId() + "\\[/vignette-image\\]", urlVignette);
        }
        /** ************************** */
        /* Transformation des fiches */
        /** ************************** */
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        /* Boucle sur chaque mot clé */
        while ((indexDebutMotCle = texte.indexOf("[id-fiche]", indexTexte)) != -1) {
            // Recopie portion avant le mot- clé
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            int indexFinMotCle = texte.indexOf("[/id-fiche]", indexDebutMotCle);
            // JSS 20020910-001 : Fin du traitement si tag incomplet
            if (indexFinMotCle == -1) {
                break;
            }
            final int indexDebutMotCleSuivant = texte.indexOf("[id-fiche]", indexDebutMotCle + 1);
            if (indexDebutMotCleSuivant != -1 && indexDebutMotCleSuivant < indexFinMotCle) {
                break;
            }
            final int linkTypeStart = texte.indexOf("_linktype=\"", indexFinMotCle) != -1 ? texte.indexOf("_linktype=\"", indexFinMotCle) + "_linktype=\"".length() : -1;
            final int linkTypeEnd = linkTypeStart != -1 ? texte.indexOf("\"", linkTypeStart) : -1;
            final String linkType = linkTypeStart != -1 && linkTypeEnd != -1 ? texte.substring(linkTypeStart, linkTypeEnd) : StringUtils.EMPTY;
            final String chaine = texte.substring(indexDebutMotCle + 10, indexFinMotCle);
            // Analyse de la chaine
            String codeFiche = "", objet = "", langueFiche = "";
            int indiceToken = 0;
            final StringTokenizer st = new StringTokenizer(chaine, ";");
            // JSS20040226 : evite les boucles
            while (st.hasMoreTokens() && indiceToken < 3) {
                if (indiceToken == 0) {
                    objet = st.nextToken();
                }
                if (indiceToken == 1) {
                    codeFiche = st.nextToken();
                }
                if (indiceToken == 2) {
                    langueFiche = st.nextToken();
                }
                indiceToken++;
            }
            String url;
            if("requete".equals(linkType)) {
                url = handleRequestLink(ctx, objet, codeFiche);
            } else {
                if (StringUtils.isNotBlank(langueFiche)) {
                    url = determinerUrlFiche(ctx, objet, codeFiche, langueFiche, false);
                } else {
                    url = determinerUrlFiche(ctx, objet, codeFiche);
                }
            }
            // Generation d'une url absolue (sauf pour les liens internes des
            // pdf)
            if (!(ctx.isGenererLiensInternes() && url.startsWith("#"))) {
                url = URLResolver.getAbsoluteUrl(url, ctx);
            }
            newTexte.append(url);
            // gestion de parametres supplementaires comme ?RF=XXXX
            indexTexte = indexFinMotCle + 11;
            if (texte.charAt(indexTexte) == '?' || texte.charAt(indexTexte) == '&') {
                if (url.contains("?")) {
                    newTexte.append("&");
                } else {
                    newTexte.append("?");
                }
                indexTexte++;
            }
        }
        // Recopie derniere portion de texte
        newTexte.append(texte.substring(indexTexte));
        /* ******************************************** */
        /* Transformation des liens vers une rubrique */
        /* ******************************************** */
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        /* Boucle sur chaque mot clé */
        while ((indexDebutMotCle = texte.indexOf("[id-rubrique]", indexTexte)) != -1) {
            // Recopie portion avant le mot- clé
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            int indexFinMotCle = texte.indexOf("[/id-rubrique]", indexDebutMotCle);
            // JSS 20020910-001 : Fin du traitement si tag incomplet
            if (indexFinMotCle == -1) {
                break;
            }
            final int indexDebutMotCleSuivant = texte.indexOf("[id-rubrique]", indexDebutMotCle + 1);
            if (indexDebutMotCleSuivant != -1 && indexDebutMotCleSuivant < indexFinMotCle) {
                break;
            }
            String codeRubrique = texte.substring(indexDebutMotCle + 13, indexFinMotCle);
            // on conserve ce test pour les contenus issus d'une version antérieure
            if (codeRubrique.endsWith("TARGET=BLANK")) {
                codeRubrique = codeRubrique.substring(0, codeRubrique.indexOf("TARGET=BLANK"));
            }
            // insertion de l'url de la fiche et de sa class css pour les rubriques externes
            final RubriqueBean rubriqueCourant = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (rubriqueCourant != null) {
                final String urlRubrique = URLResolver.getAbsoluteUrl(renvoyerUrlAccueilRubrique(ctx, codeRubrique), ctx);
                newTexte.append(urlRubrique).append("\" class=\"type_rubrique_").append(rubriqueCourant.getTypeRubrique());
            }
            indexTexte = indexFinMotCle + 14;
        }
        // Recopie dernière portion de texte
        newTexte.append(texte.substring(indexTexte));
        /* ****************************************************************** */
        /* Transformation des liens vers un formulaire de recherche avancee */
        /* ****************************************************************** */
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        /* Boucle sur chaque mot clé */
        while ((indexDebutMotCle = texte.indexOf("[form-recherche]", indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            int indexFinMotCle = texte.indexOf("[/form-recherche]", indexDebutMotCle);
            // JSS 20020910-001 : Fin du traitement si tag incomplet
            if (indexFinMotCle == -1) {
                break;
            }
            final int indexDebutMotCleSuivant = texte.indexOf("[form-recherche]", indexDebutMotCle + 1);
            if (indexDebutMotCleSuivant != -1 && indexDebutMotCleSuivant < indexFinMotCle) {
                break;
            }
            final String codeObjet = texte.substring(indexDebutMotCle + 16, indexFinMotCle);
            // insertion de l'url de la fiche
            newTexte.append(URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFormulaire(ctx, codeObjet, ctx.getLangue(), true, ctx.getCodeRubriquePageCourante()), ctx));
            indexTexte = indexFinMotCle + 17;
        }
        // Recopie dernière portion de texte
        newTexte.append(texte.substring(indexTexte));
        /** ****************************** */
        /* Insertion des appels http */
        /** ****************************** */
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        /* Boucle sur chaque mot cle */
        while ((indexDebutMotCle = texte.indexOf("[url", indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            int indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            final String contenuTag = Chaine.remplacerPointsVirgules(texte.substring(indexDebutMotCle + 1, indexFinMotCle));
            String sUrl = StringUtils.EMPTY;
            final StringTokenizer st = new StringTokenizer(contenuTag, "*");
            int indiceToken = 0;
            while (st.hasMoreTokens()) {
                final String itemTag = st.nextToken();
                if (indiceToken == 1) {
                    sUrl = itemTag;
                }
                indiceToken++;
            }
            if (sUrl.length() > 0) {
                // Si l'url commence par /, on rajoute le host
                if (sUrl.charAt(0) == '/') {
                    sUrl = URLResolver.getAbsoluteUrl(sUrl, ctx);
                }
                sUrl = sUrl.replaceAll("&amp;", "&");
                try {
                    final URL urlSiteAInclure = new URL(StringUtils.prependIfMissing(sUrl, "http://", "https://"));
                    final URLConnection urlConnection = urlSiteAInclure.openConnection();
                    // Recuperation des timeout de connection et de premiere
                    // lecture (si ils sont definis)
                    final String connectionReadTimeout = PropertyHelper.getCoreProperty("fckeditor.tag.insertionpageexterne.readtimeout");
                    final String connectionConnectTimeout = PropertyHelper.getCoreProperty("fckeditor.tag.insertionpageexterne.connecttimeout");
                    if (connectionReadTimeout != null) {
                        urlConnection.setReadTimeout(Integer.valueOf(connectionReadTimeout));
                    }
                    if (connectionConnectTimeout != null) {
                        urlConnection.setConnectTimeout(Integer.valueOf(connectionConnectTimeout));
                    }
                    final HTMLParser htmlParser = new HTMLParser(urlConnection);
                    htmlParser.parse();
                    int iDeb = 0;
                    if (sUrl.startsWith("http://")) {
                        iDeb = 7;
                    }
                    /*
                     * patch AM : on inclue le dernier / sinon ca ne marche que
                     * pour les urls sans sous-repertoires, mais pas pour les
                     * urls de type http://www.unicaen.fr/ufr/iupbanque/
                     */
                    int iFin = iDeb + sUrl.substring(iDeb).lastIndexOf('/') + 1;
                    if (iFin < iDeb) {
                        iFin = sUrl.length();
                    }
                    String urlBase = StringUtils.EMPTY;
                    if (iFin > 0) {
                        try {
                            urlBase = new URL(sUrl.substring(0, iFin)).toString();
                        } catch (final MalformedURLException e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                    }
                    htmlParser.processAbsoluteUrl(urlBase);
                    newTexte.append(htmlParser.getInputHtml());
                } catch (final ParserException | IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            indexTexte = indexFinMotCle + 1;
        }
        // Recopie derniere portion de texte
        newTexte.append(texte.substring(indexTexte));
        /** ****************************** */
        /* Insertion des photos */
        /** ****************************** */
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        /* Boucle sur chaque mot cle */
        while ((indexDebutMotCle = texte.indexOf("[ouvrir_photo", indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            int indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            final String idPhoto = texte.substring(indexDebutMotCle + 14, indexFinMotCle);
            final MediaBean media = serviceMedia.getById(Long.valueOf(idPhoto));
            int largeur = MediaPhoto.getCritereLimite().getLargeur();
            int hauteur = MediaPhoto.getCritereLimite().getHauteur();
            try {
                largeur = Integer.parseInt(serviceMedia.getSpecificData(media, MediaPhoto.ATTRIBUT_LARGEUR));
                hauteur = Integer.parseInt(serviceMedia.getSpecificData(media, MediaPhoto.ATTRIBUT_HAUTEUR));
            } catch (final NumberFormatException e) {
                LOGGER.info("unable to parse the property", e);
            }
            // exemple : '/jsp/photo.jsp?ID_PHOTO='+idPhoto
            final String data = "javascript:window.open(" + PropertyHelper.getCoreProperty("phototheque.ecran_physique") + "?ID_MEDIA=" + idPhoto  + ");return false;";
            newTexte.append(data);
            indexTexte = indexFinMotCle + 1;
        }
        // Recopie derniere portion de texte
        newTexte.append(texte.substring(indexTexte));
        texte = new String(newTexte);
        /** ****************************** */
        /* Insertion des pages statiques */
        /** ****************************** */
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        /* Boucle sur chaque mot cle */
        while ((indexDebutMotCle = texte.indexOf("[page", indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            int indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            final String contenuTag = Chaine.remplacerPointsVirgules(texte.substring(indexDebutMotCle + 1, indexFinMotCle));
            String nomPage = StringUtils.EMPTY;
            final StringTokenizer st = new StringTokenizer(contenuTag, "*");
            int indiceToken = 0;
            while (st.hasMoreTokens()) {
                final String itemTag = st.nextToken();
                if (indiceToken == 1) {
                    nomPage = itemTag;
                }
                indiceToken++;
            }
            /* lecture du fichier */
            final File pageFile = new File(String.format("%s%s%s", WebAppUtil.getExternalHtmlPath(), File.separator, StringUtils.replace(nomPage, "/", File.separator)));
            if (pageFile.exists() && pageFile.isFile()) {
                newTexte.append(FileUtils.readFileToString(pageFile, StandardCharsets.UTF_8));
            }
            indexTexte = indexFinMotCle + 1;
            // Suppression de la fin de la ligne (eviter de rajouter des <br />
            final int indexBR = texte.indexOf("<BR>", indexTexte);
            if (indexBR != -1) {
                indexTexte = indexBR + 4;
            }
        }
        // Recopie derniere portion de texte
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        newTexte = genererLienIntranet(ctx, lgBuffer, newTexte);
        String res = new String(newTexte);
        // interpretation des services
        if (ctx.getFicheCourante() != null) {
            res = ServicesUtil.transformerTagsServices(ctx, res);
        }
        if (handleLinks) {
            res = gererLiens(res);
        }
        return res;
    }

    private static String handleRequestLink(final ContexteUniv ctx, final String objet, String code) {
        String urlRequest;
        urlRequest = RechercheFicheHelper.getUrlResults("OBJET=" + objet + "&" + code.replaceAll(",LANGUE=\"(.)*\"", StringUtils.EMPTY), "0", ctx.getCodeRubriquePageCourante());
        urlRequest = StringUtils.replace(urlRequest, "&", "&amp;");
        if (ctx.getRubriqueExterneConnecteur().length() > 0) {
            urlRequest += "&amp;RUBNAV=" + EscapeString.escapeURL(ctx.getRubriqueExterneConnecteur());
        }
        return urlRequest;
    }

    /**
     * méthode d'interpretation des tags de l'intranet. Il y avait un bug dans la toolbox qui rajoutait http://host/servlet/
     *
     * @param ctx
     * @param lgBuffer
     * @param newTexte
     * @return
     */
    private static StringBuffer genererLienIntranet(ContexteUniv ctx, int lgBuffer, StringBuffer newTexte) {
        int indexTexte;
        int indexDebutMotCle;
        String texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        // Insertion lien vers page d'accueil dsi
        while ((indexDebutMotCle = texte.indexOf(OLD_TAG_URL_LOGIN, indexTexte)) != -1) {
            // Recopie portion avant le mot-cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            newTexte.append(getUrlLogin(ctx, true));
            indexTexte = indexDebutMotCle + OLD_TAG_URL_LOGIN.length();
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        texte = new String(newTexte);
        /* preferences */
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        // Insertion lien vers page d'accueil dsi
        while ((indexDebutMotCle = texte.indexOf(TAG_URL_LOGIN, indexTexte)) != -1) {
            // Recopie portion avant le mot-cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            newTexte.append(URLResolver.getAbsoluteUrl(getUrlLogin(ctx, true), ctx));
            indexTexte = indexDebutMotCle + TAG_URL_LOGIN.length();
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        texte = new String(newTexte);
        /* preferences */
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        // Insertion lien vers page d'accueil dsi
        while ((indexDebutMotCle = texte.indexOf(OLD_TAG_URL_PREF, indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            newTexte.append(getUrlPreferences(ctx));
            indexTexte = indexDebutMotCle + OLD_TAG_URL_PREF.length();
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        texte = new String(newTexte);
        /* deconnexion */
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        while ((indexDebutMotCle = texte.indexOf(TAG_URL_PREF, indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            newTexte.append(URLResolver.getAbsoluteUrl(getUrlPreferences(ctx), ctx));
            indexTexte = indexDebutMotCle + TAG_URL_PREF.length();
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        texte = new String(newTexte);
        /* deconnexion */
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        // Insertion lien vers page d'accueil dsi
        while ((indexDebutMotCle = texte.indexOf(OLD_TAG_URL_LOGOUT, indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            newTexte.append(getUrlDeconnexion(ctx));
            indexTexte = indexDebutMotCle + OLD_TAG_URL_LOGOUT.length();
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        texte = new String(newTexte);
        /* deconnexion */
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        // Insertion lien vers page d'accueil dsi
        while ((indexDebutMotCle = texte.indexOf(TAG_URL_LOGOUT, indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            newTexte.append(URLResolver.getAbsoluteUrl(getUrlDeconnexion(ctx), ctx));
            indexTexte = indexDebutMotCle + TAG_URL_LOGOUT.length();
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        texte = new String(newTexte);
        // Page d'accueil du profil
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        // Insertion lien vers page d'accueil dsi
         while ((indexDebutMotCle = texte.indexOf(TAG_URL_ACCUEIL, indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            newTexte.append(URLResolver.getAbsoluteUrl(getUrlAccueilDsi(ctx), ctx));
            indexTexte = indexDebutMotCle + TAG_URL_ACCUEIL.length();
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        return newTexte;
    }

    // Utilisation de Jsoup pour détecter les liens externes et internes
    private static String gererLiens(final String texte) {
        final Document doc = Jsoup.parseBodyFragment(texte);
        OutputSettings settings = doc.outputSettings();
        settings.prettyPrint(Boolean.FALSE);
        doc.outputSettings(settings);
        final Elements links = doc.getElementsByTag("a");
        if (CollectionUtils.isNotEmpty(links)) {
            final ServiceInfosSite serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final Collection<String> hosts = serviceInfosSite.getFullAliasHostList();
            String[] hostsArray = hosts.toArray(new String[hosts.size()]);
            String[] prefixes = new String[]{"#", "/"};
            for (final Element currentLink : links) {
                if (currentLink.hasAttr("href") && !isIdentifiedLink(currentLink)) {
                    if (StringUtils.startsWithAny(currentLink.attr("href"), hostsArray) || StringUtils.startsWithAny(currentLink.attr("href"), prefixes)) {
                        currentLink.addClass("lien_interne");
                    } else if (currentLink.attr("href").startsWith("mailto:")) {
                        currentLink.attr("href", HTMLUtil.obfuscateEmailLink(currentLink.attr("href")));
                        currentLink.addClass("mailto");
                    } else {
                        currentLink.addClass("lien_externe");
                    }
                } else if (currentLink.hasAttr("name")) {
                    currentLink.addClass("anchor");
                }
                currentLink.removeAttr("_linkType");
            }
        }
        return doc.body().html();
    }

    private static boolean isIdentifiedLink(Element link) {
        return link.hasClass("lien_externe") || link.hasClass("lien_interne");
    }

    private boolean isLinkInternal(String link, String[] prefixes) {
        return StringUtils.startsWithAny(link, prefixes);
    }

    /**
     * Renvoie la liste des onglets sous forme de code HTML.
     *
     * @param ctx       the ctx
     * @param ficheUniv the fiche univ
     * @return the onglets navigation rubrique
     * @throws Exception the exception
     */
    public static Vector<String> getOngletsNavigationRubrique(final ContexteUniv ctx, final FicheUniv ficheUniv) throws Exception {
        return getOngletsNavigationRubrique(ctx, ficheUniv, true, 1);
    }

    /**
     * Gets the onglets navigation rubrique.
     *
     * @param ctx       the ctx
     * @param ficheUniv the fiche univ
     * @param affiche   the affiche
     * @return the onglets navigation rubrique
     * @throws Exception the exception
     */
    public static Vector<String> getOngletsNavigationRubrique(final ContexteUniv ctx, final FicheUniv ficheUniv, final boolean affiche) throws Exception {
        return getOngletsNavigationRubrique(ctx, ficheUniv, affiche, 1);
    }

    /**
     * Renvoie la liste des onglets sous forme de code HTML.
     *
     * @param affiche   Definit si l'on doit renvoyer le nom de la rubrique lorsque la fiche courante est la page de tete de celle-ci (par defaut, oui)
     * @param ctx       the ctx
     * @param ficheUniv the fiche univ
     * @param stNav     the st nav
     * @return the onglets navigation rubrique
     * @throws Exception the exception
     */
    public static Vector<String> getOngletsNavigationRubrique(final ContexteUniv ctx, final FicheUniv ficheUniv, final boolean affiche, final int stNav) throws Exception {
        final Vector<String> v = new Vector<>();
        final String codeRubrique = ctx.getCodeRubriquePageCourante();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(codeRubrique);
        // On lit les rubriques dans le sens ascendant
        while (rubrique != null && serviceRubrique.getLevel(rubrique) > 0) {
            // si l'option affiche = false, alors on affiche pas l'onglet
            // lorsque la fiche courante est la page de tete.
            final BeanPageAccueil beanAccueil = PageAccueilRubriqueManager.getInstance().getBeanPageAccueil(rubrique);
            BeanFichePageAccueil ficheAcceuil = null;
            if (beanAccueil != null && beanAccueil instanceof BeanFichePageAccueil) {
                ficheAcceuil = (BeanFichePageAccueil) beanAccueil;
            }
            if (affiche || ficheUniv == null || (ficheAcceuil != null && !ficheUniv.getCode().equals(ficheAcceuil.getCode()))) {
                ajouterOngletEnteteRubrique(ctx, v, rubrique, stNav);
            }
            rubrique = serviceRubrique.getRubriqueByCode(rubrique.getCodeRubriqueMere());
        }
        return v;
    }

    /**
     * Renvoie la liste des onglets sous forme de code HTML.
     *
     * @param ctx  the ctx
     * @param code Le code de la rubrique dont on souhaite avoir la liste des onglets
     * @return the onglets navigation rubrique
     * @throws Exception the exception
     */
    public static Vector<String> getOngletsNavigationRubrique(final ContexteUniv ctx, final String code) throws Exception {
        final Vector<String> v = new Vector<>();
        if (StringUtils.isEmpty(code)) {
            return v;
        }
        /* On lit les rubriques dans le sens ascendant */
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(code);
        while (rubrique != null && serviceRubrique.getLevel(rubrique) > 0) {
            ajouterOngletEnteteRubrique(ctx, v, rubrique, 1);
            rubrique = serviceRubrique.getRubriqueByCode(rubrique.getCodeRubriqueMere());
        }
        return v;
    }

    /**
     * Encadre de navigation automatique: Renvoie la liste des pages filles d'une rubrique sous forme de code HTML Les pages filles peuvent etre : - les pages libres de la rubrique
     * - les pages de tete des sous-rubriques - les articles de la rubrique.
     *
     * @param ctx            the ctx
     * @param ficheUniv      the fiche univ
     * @param titre          the titre
     * @param codeHTMLEntete the _code html entete
     * @param codeHTMLItem   the ² html item
     * @return the encadre contenu rubrique courante
     * @throws Exception the exception
     */
    public static String getEncadreContenuRubriqueCourante(final ContexteUniv ctx, final FicheUniv ficheUniv, final String titre, final String codeHTMLEntete, final String codeHTMLItem) throws Exception {
        return getEncadreContenuRubriqueCourante(ctx, ficheUniv, titre, codeHTMLEntete, codeHTMLItem, StringUtils.EMPTY, StringUtils.EMPTY, false, true, true);
    }

    /**
     * Encadre de navigation automatique: Renvoie la liste des pages filles d'une rubrique sous forme de code HTML Les pages filles peuvent etre : - les pages libres de la rubrique
     * - les pages de tete des sous-rubriques - les articles de la rubrique
     * <p/>
     * !!!! CETTE FONCTION REMPLACE getEncadreRubrique() qui n'a pas ete supprimee pour des problemes de compatibilite.
     *
     * @param ctx                            the ctx
     * @param ficheUniv                      the fiche univ
     * @param titre                          the titre
     * @param codeHTMLEntete                 the _code html entete
     * @param codeHTMLItem                   the _code html item
     * @param codeHTMLFinItem                the _code html fin item
     * @param codeHTMLFinEntete              the _code html fin entete
     * @param enlever2Points                 the _enlever2 points
     * @param affichePageTeteDansEncadre     the _affiche page tete dans encadre
     * @param affichePageCouranteDansEncadre the _affiche page courante dans encadre
     * @return the encadre contenu rubrique courante
     * @throws Exception the exception
     */
    public static String getEncadreContenuRubriqueCourante(final ContexteUniv ctx, final FicheUniv ficheUniv, final String titre, final String codeHTMLEntete, final String codeHTMLItem, String codeHTMLFinItem, final String codeHTMLFinEntete, final boolean enlever2Points, final boolean affichePageTeteDansEncadre, final boolean affichePageCouranteDansEncadre) throws Exception {
        final String codeFicheCourante = StringUtils.EMPTY;
        // DSI : appliquer les restrictions
        ctx.setCalculListeResultatsFront(true);
        final Map<String, String> m = new TreeMap<>();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final String codeRubriqueEncadre = ctx.getCodeRubriquePageCourante();
        final RubriqueBean infos = serviceRubrique.getRubriqueByCode(codeRubriqueEncadre);
        if (infos == null || !"1".equals(infos.getGestionEncadre())) {
            return StringUtils.EMPTY;
        }
        final BeanPageAccueil beanAccueil = PageAccueilRubriqueManager.getInstance().getBeanPageAccueil(infos);
        BeanFichePageAccueil ficheAcceuil = null;
        if (beanAccueil != null && beanAccueil instanceof BeanFichePageAccueil) {
            ficheAcceuil = (BeanFichePageAccueil) beanAccueil;
        }
        /* Selection des sous-rubriques */
        final Collection<RubriqueBean> sousRubriques = serviceRubrique.getRubriqueByCodeParent(infos.getCode());
        for (RubriqueBean sousRubrique : sousRubriques) {
            ajouterOngletEnteteRubrique(ctx, m, sousRubrique, 1);
        }
        /* Selection des objets API implementant l'interface RubriqueExterne */
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            String nomObjet = ReferentielObjets.getNomObjet(codeObjet);
            FicheUniv maFicheUniv = ReferentielObjets.instancierFiche(nomObjet);
            if (maFicheUniv == null) {
                continue;
            }
            maFicheUniv.init();
            maFicheUniv.setCtx(ctx);
            final String nomPage = ReferentielObjets.getNomPage(codeObjet);
            if (maFicheUniv.selectParCodeRubrique(codeRubriqueEncadre, StringUtils.EMPTY) > 0) {
                while (maFicheUniv.nextItem()) {
                    // si la fiche n'est pas la page de tete ou que
                    // l'affichage de la page de tete dans l'encadre est
                    // demande
                    // pour les anciennes rubriques, codepagetete = lecode,
                    // pour les nouvelles rubriques, codepagetete =
                    // lecode,LANGUE=lalangue,TYPE=letypeobjet
                    if (affichePageTeteDansEncadre || (ficheAcceuil != null && !maFicheUniv.getCode().equals(ficheAcceuil.getCode()) && !maFicheUniv.getLangue().equals(ficheAcceuil.getLangue()) && !ReferentielObjets.getNomObjet(maFicheUniv).equals(ficheAcceuil.getObjet()))) {
                        // si la fiche n'est pas la page courante ou que
                        // l'affichage de cette page n'a pas ete desactive
                        if (affichePageCouranteDansEncadre || !maFicheUniv.getCode().equals(codeFicheCourante)) {
                            // si le titre de la fiche commence par ':' et
                            // que le booleen enlever2Points est active, on
                            // enleve
                            String titreFiche = maFicheUniv.getLibelleAffichable();
                            if (titreFiche.startsWith(":") && enlever2Points) {
                                titreFiche = titreFiche.substring(1, titreFiche.length());
                            }
                            ajouterLienEncadre(ctx, m, titreFiche, nomPage, maFicheUniv.getCode() + ",LANGUE=" + maFicheUniv.getLangue());
                        }
                    }
                }
            }
            /*
             * si la rubrique est une rubrique d'objet (qui s'affiche si l'objet
             * n'a pas de rubrique rattachee)
             */
            if (nomObjet.equalsIgnoreCase(codeRubriqueEncadre)) {
                /*
                 * alors on renvoit toutes les fiches de cet objet, sauf si
                 * elles sont rattachees a une autre rubrique
                 */
                final ClauseWhere where = new ClauseWhere(ConditionHelper.egalVarchar("LANGUE", infos.getLangue()));
                where.and(ConditionHelper.egalVarchar("ETAT_OBJET", "0003"));
                final ConditionList conditionCodeRubrique = new ConditionList(ConditionHelper.egalVarchar("CODE_RUBRIQUE", StringUtils.EMPTY));
                conditionCodeRubrique.or(ConditionHelper.egalVarchar("CODE_RUBRIQUE", codeRubriqueEncadre));
                where.and(conditionCodeRubrique);
                maFicheUniv.select(where.formaterSQL());
                while (maFicheUniv.nextItem()) {
                    // si la fiche n'est pas la page de tete ou que l'affichage
                    // de la page de tete dans l'encadre est demande
                    // pour les anciennes rubriques, codepagetete = lecode, pour
                    // les nouvelles rubriques, codepagetete =
                    // lecode,LANGUE=lalangue,TYPE=letypeobjet
                    if (affichePageTeteDansEncadre || (!maFicheUniv.getCode().equals(ficheAcceuil.getCode()))) {
                        // si la fiche n'est pas la page courante ou que
                        // l'affichage de cette page n'a pas ete desactive
                        if (affichePageCouranteDansEncadre || !maFicheUniv.getCode().equals(codeFicheCourante)) {
                            // si le titre de la fiche commence par ':' et que
                            // le booleen enlever2Points est active, on enleve
                            String titreFiche = maFicheUniv.getLibelleAffichable();
                            if (titreFiche.startsWith(":") && enlever2Points) {
                                titreFiche = titreFiche.substring(1, titreFiche.length());
                            }
                            ajouterLienEncadre(ctx, m, titreFiche, nomPage, maFicheUniv.getCode() + ",LANGUE=" + maFicheUniv.getLangue());
                        }
                    }
                }
            }
        }
        /* construction de la chaine resultat */
        String chaineEncadre = StringUtils.EMPTY;
        final Iterator<String> iter = m.values().iterator();
        codeHTMLFinItem = StringUtils.defaultIfEmpty(codeHTMLFinItem, "<br />");
        while (iter.hasNext()) {
            final String nextIter = iter.next();
            chaineEncadre = chaineEncadre + codeHTMLItem + nextIter + codeHTMLFinItem;
        }
        // Ajout du titre
        if (chaineEncadre.length() > 0) {
            if (titre.length() > 0) {
                chaineEncadre = "[titre;" + titre + "]" + codeHTMLEntete + chaineEncadre + codeHTMLFinEntete;
            } else {
                final RubriqueBean rubriqueEncadre = serviceRubrique.getRubriqueByCode(codeRubriqueEncadre);
                if (rubriqueEncadre != null) {
                    chaineEncadre = "[titre;" + rubriqueEncadre.getIntitule() + "]" + codeHTMLEntete + chaineEncadre + codeHTMLFinEntete;
                } else {
                    chaineEncadre = "[titre;]" + codeHTMLEntete + chaineEncadre + codeHTMLFinEntete;
                }
            }
        }
        // DSI : appliquer les restrictions
        ctx.setCalculListeResultatsFront(false);
        return chaineEncadre;
    }

    /**
     * Tags de personnalisation : - [debutgroupe,fingroupe,nom,prenom ...]
     *
     * @param ctx    the ctx
     * @param _texte the _texte
     * @return the string
     * @throws Exception the exception
     */
    public static String transformerTagsPersonnalisation(final ContexteUniv ctx, final String _texte) throws Exception {
        final ServiceUser serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final int lgBuffer = _texte.length() + 100;
        StringBuffer newTexte = new StringBuffer(lgBuffer);
        String texte = _texte;
        int indexTexte = 0;
        int indexDebutMotCle = -1;
        int indexFinMotCle = 0;
        /** ******************************** */
        /* Gestion des groupes */
        /** ******************************** */
        indexTexte = 0;
        indexDebutMotCle = -1;
        indexFinMotCle = 0;
        /* Boucle sur chaque mot cle */
        while ((indexDebutMotCle = texte.indexOf("[traitement;debutgroupe", indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            final String contenuTag = Chaine.remplacerPointsVirgules(texte.substring(indexDebutMotCle + 1, indexFinMotCle));
            String action = StringUtils.EMPTY;
            String paramExit = StringUtils.EMPTY;
            final StringTokenizer st = new StringTokenizer(contenuTag, "*");
            int indiceToken = 0;
            while (st.hasMoreTokens()) {
                final String itemTag = st.nextToken();
                if (indiceToken == 1) {
                    action = itemTag;
                }
                if (indiceToken == 2) {
                    paramExit = itemTag;
                    paramExit = StringUtils.replace(paramExit, "#", "&");
                }
                indiceToken++;
            }
            if (action.compareToIgnoreCase("debutgroupe") == 0) {
                final String tagFinGroupe = "[traitement;fingroupe]";
                final int indexFinGroupe = texte.indexOf(tagFinGroupe, indexDebutMotCle);
                if (indexFinGroupe != -1) {
                    Enumeration<String> e = null;
                    final Vector<String> listeGroupes = new Vector<>();
                    if (paramExit.length() == 0) {
                        // Tous les groupes
                        for (String codeGroupe : ctx.getGroupesDsi()) {
                            final GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupe);
                            if (group != null) {
                                listeGroupes.add(codeGroupe);
                            }
                        }
                    } else {
                        // Types de groupe separe par un ','
                        final StringTokenizer typesGroupes = new StringTokenizer(paramExit, ",");
                        while (typesGroupes.hasMoreTokens()) {
                            // On recupere tous les groupes du type de groupe
                            for (GroupeDsiBean group : serviceGroupeDsi.getByType(typesGroupes.nextToken())) {
                                if (ctx.getGroupesDsi().contains(group.getCode())) {
                                    listeGroupes.add(group.getCode());
                                }
                            }
                        }
                    }
                    e = listeGroupes.elements();
                    while (e.hasMoreElements()) {
                        ctx.setGroupePersonnalisationCourant(e.nextElement());
                        newTexte.append(transformerTagsPersonnalisation(ctx, texte.substring(indexFinMotCle + 1, indexFinGroupe)));
                        ctx.setGroupePersonnalisationCourant(StringUtils.EMPTY);
                    } // Repositionnement apres les groupes
                    indexFinMotCle = texte.indexOf("]", indexFinGroupe);
                }
            }
            indexTexte = indexFinMotCle + 1;
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuffer(lgBuffer);
        /** ************************** */
        /* Transformation des tags */
        /** ************************** */
        final GroupeDsiBean groupeDsiBean = serviceGroupeDsi.getByCode(ctx.getGroupePersonnalisationCourant());
        while ((indexDebutMotCle = texte.indexOf("[", indexTexte)) != -1) {
            // Recopie portion avant le mot- cle
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Recherche de mot-cle
            indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            String valeurRemplacement = StringUtils.EMPTY;
            boolean estUnMotcle = false;
            if (indexFinMotCle != -1) {
                String motCle = texte.substring(indexDebutMotCle + 1, indexFinMotCle);
                if ("groupe".equals(motCle)) {
                    valeurRemplacement = groupeDsiBean.getCode();
                    estUnMotcle = true;
                } else if ("intitulegroupe".equals(motCle)) {
                    valeurRemplacement = groupeDsiBean.getLibelle();
                    estUnMotcle = true;
                } else if ("pagegroupe".equals(motCle)) {
                    // Affichage de la page de tete
                    if (StringUtils.isNotBlank(groupeDsiBean.getCodePageTete())) {
                        String langueFiche = "0";
                        String codeFiche = groupeDsiBean.getCodePageTete();
                        final int indiceLangue = codeFiche.indexOf(",LANGUE=");
                        if (indiceLangue != -1) {
                            langueFiche = codeFiche.substring(indiceLangue + 8);
                            codeFiche = codeFiche.substring(0, indiceLangue);
                        }
                        valeurRemplacement = "<a href=\"" + URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(ctx, "pagelibre", groupeDsiBean.getCodePageTete()), ctx) + "\">" + PageLibre.getLibelleAffichable(ctx, codeFiche, langueFiche) + "</a>";
                    }
                    estUnMotcle = true;
                } else if ("code".equals(motCle)) {
                    valeurRemplacement = ctx.getCode();
                    estUnMotcle = true;
                } else if ("nom".equals(motCle)) {
                    valeurRemplacement = ctx.getNom();
                    estUnMotcle = true;
                } else if ("prenom".equals(motCle)) {
                    valeurRemplacement = ctx.getPrenom();
                    estUnMotcle = true;
                } else if ("profil".equals(motCle)) {
                    if (StringUtils.isEmpty(ctx.getProfilDsi())) {
                        valeurRemplacement = IRequeteurConstantes.CODE_DYNAMIQUE;
                    } else {
                        valeurRemplacement = ctx.getProfilDsi();
                    }
                    estUnMotcle = true;
                } else if ("intituleprofil".equals(motCle)) {
                    valeurRemplacement = serviceProfildsi.getDisplayableLabel(ctx.getProfilDsi());
                    estUnMotcle = true;
                } else if ("liendesabonews".equals(motCle)) {
                    valeurRemplacement = URLResolver.getAbsoluteUrl(WebAppUtil.SG_PATH + "?PROC=TRAITEMENT_DEMANDE_ABONNEMENT&amp;ACTION=DEMANDER_DESABONNEMENT&amp;EMAIL=" + ctx.getAdresseMail() + "&amp;CODE=" + ctx.getCode(), ctx);
                    estUnMotcle = true;
                } else if ("centresinteret".equals(motCle)) {
                    if (ctx.getCentresInteret().isEmpty()) {
                        valeurRemplacement = "???";
                    } else {
                        for (String current : ctx.getCentresInteret()) {
                            if (valeurRemplacement.length() > 0) {
                                valeurRemplacement += "+";
                            }
                            valeurRemplacement += current;
                        }
                    }
                    estUnMotcle = true;
                } else if ("intitulestructure".equals(motCle)) {
                    valeurRemplacement = serviceStructure.getDisplayableLabel(ctx.getCodeStructure(), ctx.getLangue());
                    estUnMotcle = true;
                } else if ("date".equals(motCle)) {
                    valeurRemplacement = DateFormat.getDateInstance(DateFormat.LONG, ctx.getLocale()).format(new Date(System.currentTimeMillis()));
                    estUnMotcle = true;
                } else if ("ksession".equals(motCle)) {
                    valeurRemplacement = ctx.getKsession();
                    estUnMotcle = true;
                } else if ("modifierfiches".equals(motCle)) {
                    valeurRemplacement = "[traitement;liste_fiches;CODE_REDACTEUR=" + IRequeteurConstantes.CODE_DYNAMIQUE + "#MODE=" + IRequeteurConstantes.MODE_MODIFICATION + "#TRI=5#INCREMENT=3#TAB=0]";
                    estUnMotcle = true;
                } else if ("creerfiches".equals(motCle)) {
                    valeurRemplacement = "[traitement;lien_fiches;CODE_REDACTEUR=" + IRequeteurConstantes.CODE_DYNAMIQUE + "#ACTION=AJOUTER#SEPARATEUR=br#LIBELLE=OBJET]";
                    estUnMotcle = true;
                } else if ("validerfiches".equals(motCle)) {
                    valeurRemplacement = "[traitement;lien_fiches;CODE_REDACTEUR=" + IRequeteurConstantes.CODE_DYNAMIQUE + "#ACTION=VALIDER#SEPARATEUR=br#LIBELLE=Fiches à valider]";
                    estUnMotcle = true;
                }
                if (motCle.startsWith("soumettreactu")) {
                    final String[] listeMots = motCle.split(";", -2);
                    String libelleLien = "Saisir une actualité";
                    if (listeMots.length > 1) {
                        libelleLien = listeMots[1];
                    }
                    String codeRubrique = StringUtils.EMPTY;
                    if (listeMots.length > 2) {
                        codeRubrique = listeMots[2];
                    }
                    estUnMotcle = true;
                    if ("RB".equalsIgnoreCase(codeRubrique)) {
                        codeRubrique = IRequeteurConstantes.CODE_DYNAMIQUE;
                    }
                    valeurRemplacement = "[traitement;lien_fiches;OBJET=ACTUALITE#ACTION=AJOUTER#LIBELLE=" + libelleLien + "#CODE_RUBRIQUE=" + codeRubrique + "]";
                } else if (motCle.startsWith("soumettredoc")) {
                    final String[] listeMots = motCle.split(";", -2);
                    String libelleLien = "Saisir un document";
                    if (listeMots.length > 1) {
                        libelleLien = listeMots[1];
                    }
                    String codeRubrique = StringUtils.EMPTY;
                    if (listeMots.length > 2) {
                        codeRubrique = listeMots[2];
                    }
                    estUnMotcle = true;
                    valeurRemplacement = "[traitement;lien_fiches;OBJET=DOCUMENT#ACTION=AJOUTER#LIBELLE=" + libelleLien + "#CODE_RUBRIQUE=" + codeRubrique + "]";
                } else if (motCle.startsWith("soumettrearticle")) {
                    final String[] listeMots = motCle.split(";", -2);
                    String libelleLien = "Saisir un article";
                    if (listeMots.length > 1) {
                        libelleLien = listeMots[1];
                    }
                    String codeRubrique = StringUtils.EMPTY;
                    if (listeMots.length > 2) {
                        codeRubrique = listeMots[2];
                    }
                    estUnMotcle = true;
                    valeurRemplacement = "[traitement;lien_fiches;OBJET=ARTICLE#ACTION=AJOUTER#LIBELLE=" + libelleLien + "#CODE_RUBRIQUE=" + codeRubrique + "]";
                } else if (motCle.startsWith("envoi_mail_dsi")) {
                    final String[] listeMots = motCle.split(";", -2);
                    String codeGroupe = StringUtils.EMPTY;
                    String libelleLien = "Envoyer un mail à tout le groupe";
                    if (listeMots.length > 1) {
                        if (listeMots[1].length() > 0) {
                            libelleLien = listeMots[1];
                        }
                    }
                    if (listeMots.length > 2) {
                        codeGroupe = listeMots[2];
                    }
                    // on va lister les menbres des groupes dsi de l'utilisateur
                    if (StringUtils.isEmpty(codeGroupe)) {
                        if (ctx.getAutorisation() != null) {
                            Collection<String> vCodeGroupe = ctx.getAutorisation().getListeGroupes();
                            valeurRemplacement += "<a href=\"mailto:";
                            for (final String string : vCodeGroupe) {
                                final List<UtilisateurBean> users = serviceUser.select("", "", "", "", "", string, "", "");
                                if (CollectionUtils.isNotEmpty(users)) {
                                    for (UtilisateurBean currentUser : users) {
                                        valeurRemplacement += String.format("%s,", currentUser.getAdresseMail());
                                    }
                                }
                            }
                            valeurRemplacement += "\">" + libelleLien + "</a>";
                        }
                    } else {
                        final List<UtilisateurBean> users = serviceUser.select("", "", "", "", "", codeGroupe, "", "");
                        if (CollectionUtils.isNotEmpty(users)) {
                            valeurRemplacement += "<a href=\"mailto:";
                            for (UtilisateurBean currentUser : users) {
                                valeurRemplacement += currentUser.getAdresseMail() + ",";
                            }
                            valeurRemplacement += "\">" + libelleLien + "</a>";
                        }
                    }
                    estUnMotcle = true;
                } else if (motCle.startsWith("liste_membres_dsi")) {
                    final String[] listeMots = motCle.split(";", -2);
                    String codeGroupe = StringUtils.EMPTY;
                    if (listeMots.length > 1) {
                        codeGroupe = listeMots[1];
                    }
                    // on va lister les membres des groupes dsi de l'utilisateur
                    if (StringUtils.isEmpty(codeGroupe)) {
                        if (ctx.getAutorisation() != null) {
                            final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
                            final Collection<String> groupsCodes = new Vector<>(serviceGroupeUtilisateur.getAllGroupsCodesByUserCode(ctx.getCode()));
                            for (final String codeGroupeUtilisateur : groupsCodes) {
                                final List<UtilisateurBean> users = serviceUser.select("", "", "", "", "", codeGroupeUtilisateur, "", "");
                                if (CollectionUtils.isNotEmpty(users)) {
                                    for (UtilisateurBean currentUser : users) {
                                        if (StringUtils.isNotEmpty(valeurRemplacement)) {
                                            valeurRemplacement += "<br />";
                                        }
                                        valeurRemplacement += "<a href=\"mailto:" + currentUser.getAdresseMail() + "\">" + String.format("%s %s", currentUser.getPrenom(), currentUser.getNom()) + "</a>";
                                    }
                                }
                            }
                        }
                    } else {
                        final List<UtilisateurBean> users = serviceUser.select("", "", "", "", "", codeGroupe, "", "");
                        if (CollectionUtils.isNotEmpty(users)) {
                            for (UtilisateurBean currentUser : users) {
                                if (StringUtils.isNotEmpty(valeurRemplacement)) {
                                    valeurRemplacement += "<br />";
                                }
                                valeurRemplacement += "<a href=\"mailto:" + currentUser.getAdresseMail() + "\">" + String.format("%s %s", currentUser.getPrenom(), currentUser.getNom()) + "</a>";
                            }
                        }
                    }
                    estUnMotcle = true;
                }
            }
            if (estUnMotcle) {
                newTexte.append(valeurRemplacement);
                indexTexte = indexFinMotCle + 1;
            } else {
                newTexte.append("[");
                indexTexte = indexDebutMotCle + 1;
            }
        } // Recopie derniere portion de texte
        newTexte.append(texte.substring(indexTexte));
        return newTexte.toString();
    }

    /**
     * Renvoie la liste des rubriques sous forme d'une liste de tags option, ce qui donne avec le separateur "-" par exemple :
     * <p/>
     * Rubrique 1 -Sous rubrique 11 -sous rubrique 12 --sousous rubrique 121 Rubrique 2 ...
     *
     * @param ctx        the ctx
     * @param separateur the separateur
     * @return the string
     * @throws Exception the exception
     */
    public static String insererComboRubriques(final ContexteUniv ctx, final String separateur) throws Exception {
        /*
         * par defaut : pas de longueur max, on renvoit le code dans "value" et
         * on met en majuscules le premier niveau de rubriques
         */
        return insererComboRubriques(ctx, separateur, -1, 0, 1);
    }

    /**
     * Renvoie la liste des rubriques sous forme d'une liste de tags option, ce qui donne avec le separateur "-" par exemple :
     * <p/>
     * Rubrique 1 -Sous rubrique 11 -sous rubrique 12 --sousous rubrique 121 Rubrique 2 ...
     * <p/>
     * separateur est un caractere qui servira pour distinguer les niveaux de rubriques
     * <p/>
     * longueurMax definit si l'intitule est limite en nb de caracteres. Si longueurMax vaut -1, on affiche tous les caracteres
     * <p/>
     * typeValue vaut 0 ou 1. Il permet de savoir ce que l'on met dans l'attribut Value du tag "option". Si 0, on met le code de la rubrique, si 1, on met l'url de la page
     * d'accueil de la rubrique.
     * <p/>
     * uppercase vaut 0 ou 1. Il permet de mettre en majuscule le premier niveau de rubrique ou non. Si 0, on ne met pas en majuscules, si 1, on passe en majuscule.
     *
     * @param ctx         the ctx
     * @param separateur  the separateur
     * @param longueurMax the longueur max
     * @param typeValue   the type value
     * @param uppercase   the uppercase
     * @return the string
     * @throws Exception the exception
     */
    public static String insererComboRubriques(final ContexteUniv ctx, final String separateur, final int longueurMax, final int typeValue, final int uppercase) throws Exception {
        String res = StringUtils.EMPTY;
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final Collection<RubriqueBean> listeMeres = serviceRubrique.getRubriqueByCodeParentFront(StringUtils.EMPTY, ctx.getGroupesDsiAvecAscendants());
        for (RubriqueBean ir : listeMeres) {
            String attValue = null;
            String intitule = ir.getIntitule();
            if (uppercase == 1) {
                intitule = intitule.toUpperCase();
            }
            if (intitule.length() > longueurMax && longueurMax != -1) {
                intitule = intitule.substring(0, longueurMax - 1);
            }
            if (typeValue == 0) {
                attValue = ir.getCode();
            } else {
                attValue = URLResolver.getAbsoluteUrl(renvoyerUrlAccueilRubrique(ctx, ir.getCode()), ctx);
            }
            res += "<option value=\"" + attValue + "\">" + intitule + "</option>\n";
            res += insererComboRubriquesFilles(ctx, ir, 1, separateur, longueurMax, typeValue);
        }
        return res;
    }

    /**
     * Renvoie les intitules des filles d'une rubrique en fonction de son code. la liste est une suite de tags "option". cette methode s'appelle de maniere recursive pour parcourir
     * tout l'arbre.
     *
     * @param niveau      : niveau 1 a x : un tiret de plus par niveau devant l'intitule de la rubrique.
     * @param ctx         the ctx
     * @param irMere      the ir mere
     * @param separateur  the separateur
     * @param longueurMax the longueur max
     * @param typeValue   the type value
     * @return the string
     * @throws Exception the exception
     */
    private static String insererComboRubriquesFilles(final ContexteUniv ctx, final RubriqueBean irMere, int niveau, final String separateur, final int longueurMax, final int typeValue) throws Exception {
        String res = StringUtils.EMPTY;
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final Collection<RubriqueBean> listeFilles = serviceRubrique.getRubriqueByCodeParent(irMere.getCode());
        for (RubriqueBean ir : listeFilles) {
            // affichage rubriques filles
            if (ir != null && ir.getCodeRubriqueMere().equals(irMere.getCode())) {
                String espaces = StringUtils.EMPTY;
                for (int niv = 0; niv < niveau; niv++) {
                    espaces += separateur;
                }
                String intitule = espaces + ir.getIntitule();
                if (intitule.length() > longueurMax && longueurMax != -1) {
                    intitule = intitule.substring(0, longueurMax - 1);
                }
                String attValue = StringUtils.EMPTY;
                if (typeValue == 0) {
                    attValue = ir.getCode();
                } else {
                    attValue = URLResolver.getAbsoluteUrl(renvoyerUrlAccueilRubrique(ctx, ir.getCode()), ctx);
                }
                res += "<option value=\"" + attValue + "\">" + intitule + "</option>\n";
            }
            niveau++;
            res += insererComboRubriquesFilles(ctx, ir, niveau, separateur, longueurMax, typeValue);
            niveau--;
        }
        return res;
    }

    /**
     * Renvoie l'url de la page d'accueil d'une rubrique.
     *
     * @param ctx          the ctx
     * @param codeRubrique the _code rubrique
     * @return String url de la page d'accueil de la rubrique (peut etre une fiche, un formulaire de recherche)
     * @throws Exception the exception
     */
    public static String renvoyerUrlAccueilRubrique(final ContexteUniv ctx, final String codeRubrique) throws Exception {
        return renvoyerUrlAccueilRubrique(ctx, codeRubrique, true);
    }

    /**
     * Renvoie l'url de la page d'accueil d'une rubrique.
     *
     * @param ctx          the ctx
     * @param codeRubrique the _code rubrique
     * @param ampersands   the ampersands
     * @return String url de la page d'accueil de la rubrique (peut etre une fiche, un formulaire de recherche)
     * @throws Exception the exception
     */
    public static String renvoyerUrlAccueilRubrique(final ContexteUniv ctx, final String codeRubrique, final boolean ampersands) throws Exception {
        return renvoyerUrlAccueilRubrique(ctx, codeRubrique, ampersands, StringUtils.EMPTY);
    }

    /**
     * Renvoie l'url de la page d'accueil d'une rubrique.
     *
     * @param ctx          the ctx
     * @param codeRubrique the _code rubrique
     * @param ampersands   the ampersands
     * @param langue       the langue
     * @return String url de la page d'accueil de la rubrique (peut etre une fiche, un formulaire de recherche) ! attention si la langue est specifiee peut renvoyer String vide !
     * @throws Exception the exception
     */
    public static String renvoyerUrlAccueilRubrique(final ContexteUniv ctx, final String codeRubrique, final boolean ampersands, final String langue) throws Exception {
        return renvoyerUrlAccueilRubrique(ctx, codeRubrique, ampersands, langue, true);
    }

    /**
     * Renvoyer url accueil rubrique.
     *
     * @param ctx             the ctx
     * @param codeRubrique    the _code rubrique
     * @param ampersands      the ampersands
     * @param langue          the langue
     * @param forcageRubrique the forcage rubrique
     * @return the string
     * @throws Exception the exception
     */
    public static String renvoyerUrlAccueilRubrique(final ContexteUniv ctx, final String codeRubrique, final boolean ampersands, final String langue, final boolean forcageRubrique) throws Exception {
        return renvoyerUrlAccueilRubrique(ctx, codeRubrique, ampersands, langue, forcageRubrique, true);
    }

    /**
     * Renvoie l'url de la page d'accueil d'une rubrique.
     *
     * @param ctx             the ctx
     * @param codeRubrique    the _code rubrique
     * @param ampersands      the ampersands
     * @param langue          the langue
     * @param forcageRubrique the forcage rubrique
     * @param urlFriendly     the url friendly
     * @return String url de la page d'accueil de la rubrique (peut etre une fiche, un formulaire de recherche)
     * @throws Exception the exception
     */
    public static String renvoyerUrlAccueilRubrique(final ContexteUniv ctx, final String codeRubrique, final boolean ampersands, final String langue, final boolean forcageRubrique, final boolean urlFriendly) throws Exception {
        String res = StringUtils.EMPTY;
        // de la forme http://monSite/maRubrique/
        if (urlFriendly) {
            final FrontOfficeMgr frontOfficeMgr = FrontOfficeMgr.getInstance();
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final InfosSite siteDeLaRubrique = serviceInfosSite.determineSite(codeRubrique, true);
            res = frontOfficeMgr.getUrlAccueilRubrique(siteDeLaRubrique, codeRubrique);
            if (res != null) {
                return res;
            }
            res = StringUtils.EMPTY;
        }
        final String esperluette = ampersands ? "&amp;" : "&";
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean info = serviceRubrique.getRubriqueByCode(codeRubrique);
        final PageAccueilRubriqueManager accueilRubriqueManager = PageAccueilRubriqueManager.getInstance();
        if (info != null) {
            final BeanPageAccueil  beanAccueil = accueilRubriqueManager.getBeanPageAccueil(info);
            if (beanAccueil != null) {
                res = beanAccueil.getUrlRubrique(codeRubrique, langue, ampersands);
            }
            if (forcageRubrique && StringUtils.isNotEmpty(res) && accueilRubriqueManager.isInterne(info)) {
                res += res.indexOf('?') == -1 ? "?" : esperluette;
                res += "RF=" + URLEncoder.encode(codeRubrique, CharEncoding.DEFAULT);
            }
            if (!ampersands) {
                res = StringUtils.replace(res, "&amp;", "&");
            }
        }
        return res;
    }
    /**
     * Renvoie l'url de la page d'accueil d'une rubrique.
     *
     * @param ctx             the ctx
     * @param rubrique    the _code rubrique
     * @param ampersands      the ampersands
     * @param langue          the langue
     * @param forcageRubrique the forcage rubrique
     * @param urlFriendly     the url friendly
     * @return String url de la page d'accueil de la rubrique (peut etre une fiche, un formulaire de recherche)
     * @throws Exception the exception
     */
    public static String renvoyerUrlAccueilRubrique(final ContexteUniv ctx, final RubriqueBean rubrique, final boolean ampersands, final String langue, final boolean forcageRubrique, final boolean urlFriendly) throws Exception {
        String res = StringUtils.EMPTY;
        // de la forme http://monSite/maRubrique/
        if (urlFriendly) {
            FrontOfficeMgr frontOfficeMgr = FrontOfficeMgr.getInstance();
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final InfosSite siteDeLaRubrique = serviceInfosSite.determineSite(rubrique.getCode(), true);
            res = frontOfficeMgr.getUrlAccueilRubrique(siteDeLaRubrique, rubrique.getCode());
            if (res != null) {
                return res;
            }
            res = StringUtils.EMPTY;
        }
        final String esperluette = ampersands ? "&amp;" : "&";
        PageAccueilRubriqueManager accueilRubriqueManager = PageAccueilRubriqueManager.getInstance();
        final BeanPageAccueil beanAccueil = accueilRubriqueManager.getBeanPageAccueil(rubrique);
        if (beanAccueil != null) {
            res = beanAccueil.getUrlRubrique(rubrique.getCode(), langue, ampersands);
        }
        if (forcageRubrique && StringUtils.isNotEmpty(res) && accueilRubriqueManager.isInterne(rubrique)) {
            res += res.indexOf('?') == -1 ? "?" : esperluette;
            res += "RF=" + URLEncoder.encode(rubrique.getCode(), CharEncoding.DEFAULT);
        }
        if (!ampersands) {
            res = StringUtils.replace(res, "&amp;", "&");
        }
        return res;
    }

    /**
     * Determine l'url absolue du formulaire de recherche avancee, a partir du type d'objet, et de la langue courante.
     *
     * @param ctx          the ctx
     * @param objet        the _objet
     * @param langue       the _langue
     * @param ampersands   the _ampersands
     * @param codeRubrique the _code rubrique
     * @return String url vers le formulaire de recherche avancee de l'objet
     */
    public static String determinerUrlFormulaire(final ContexteUniv ctx, final String objet, final String langue, final boolean ampersands, final String codeRubrique) {
        final String esperluette = ampersands ? "&amp;" : "&";
        String urlFiche = WebAppUtil.SG_PATH + "?PROC=RECHERCHE" + esperluette + "ACTION=RECHERCHER" + esperluette + "OBJET=" + EscapeString.escapeURL(objet.toUpperCase());
        if (codeRubrique.length() > 0) {
            urlFiche += esperluette + "RH=" + EscapeString.escapeURL(codeRubrique);
        }
        if (langue != null && langue.length() > 0) {
            urlFiche += esperluette + "LANGUE=" + EscapeString.escapeURL(langue);
        }
        return urlFiche;
    }

    /**
     * Ajoute un parametre a l'url.
     *
     * @param urlToTransform the _url
     * @param name           the name
     * @param value          the value
     * @return the string
     */
    private static String ajouterParametreUrl(final String urlToTransform, final String name, final String value) {
        String url = urlToTransform;
        if (!url.contains("?")) {
            url += "?";
        } else {
            url += "&amp;";
        }
        url += name + "=" + EscapeString.escapeURL(value);
        return url;
    }

    /**
     * Determine l'url à partir d'une url relative (ajout des variables de navigation) Les variables ajoutees sont STNAV, RUBNAV et ESPACE.
     *
     * @param ctx         the ctx
     * @param urlRelative the _url relative
     * @return the string
     */
    public static String determinerUrlRelative(final ContexteUniv ctx, final String urlRelative) {
        return determinerUrlRelative(ctx, urlRelative, false);
    }

    /**
     * Determine l'url à partir d'une url relative (ajout des variables de navigation) Les variables ajoutees sont STNAV, RUBNAV et ESPACE.
     *
     * @param ctx           the ctx
     * @param urlRelative   the _url relative
     * @param critereEspace the _critere espace
     * @return the string
     */
    public static String determinerUrlRelative(final ContexteUniv ctx, final String urlRelative, final boolean critereEspace) {
        String urlFiche = urlRelative;
        if (critereEspace && ctx.getEspaceCourant().length() > 0) {
            urlFiche = ajouterParametreUrl(urlFiche, "ESPACE", ctx.getEspaceCourant());
        }
        if (ctx.getCodeRubriquePageCourante().length() > 0) {
            urlFiche = ajouterParametreUrl(urlFiche, "RH", ctx.getCodeRubriquePageCourante());
        }
        if (ctx.getRubriqueExterneConnecteur().length() > 0) {
            urlFiche = ajouterParametreUrl(urlFiche, "RUBNAV", ctx.getRubriqueExterneConnecteur());
        }
        if (ctx.getLangue().length() > 0) {
            urlFiche = ajouterParametreUrl(urlFiche, "LANGUE", ctx.getLangue());
        }
        return urlFiche;
    }

    /**
     * Determine pour un service si il doit etre affiche dans une popup.
     *
     * @param ctx     the ctx
     * @param service the _service
     * @return the string
     * @deprecated utiliser {@link ServicesUtil#determinerTargetService(ContexteUniv, ServiceBean)}mais
     */
    @Deprecated
    public static String determinerTargetService(final ContexteUniv ctx, final ServiceBean service) {
        return ServicesUtil.determinerTargetService(ctx, service);
    }

    /**
     * Redirection vers la page de login.
     *
     * @param ctx         : contexte de la page
     * @param response    the response
     * @param callbackUrl the callback url
     * @throws IOException lors du send redirect
     */
    public static void redirigerVersLogin(final ContexteUniv ctx, HttpServletResponse response, String callbackUrl) throws IOException {
        final HttpServletRequest request = ctx.getRequeteHTTP();
        if (callbackUrl == null || callbackUrl.length() == 0) {
            callbackUrl = request.getRequestURI();
            if (request.getQueryString() != null && request.getQueryString().length() > 0) {
                callbackUrl += "?" + request.getQueryString();
            }
        }
        if (!callbackUrl.contains("RH=")) {
            if (!callbackUrl.contains("?")) {
                callbackUrl += "?RH=" + ctx.getCodeRubriquePageCourante();
            } else {
                callbackUrl += "&RH=" + ctx.getCodeRubriquePageCourante();
            }
        }
        initialiserUrlRedirection(request.getSession(Boolean.FALSE), callbackUrl);
        if (response instanceof CacheUrlWrappedResponse) {
            response = ((CacheUrlWrappedResponse) response).getHttpServletResponse();
        }
        response.sendRedirect(URLResolver.getAbsoluteUrl(getUrlLogin(ctx, false), ctx, URLResolver.LOGIN_FRONT));
    }

    /**
     * Initialise la redirection dans la variable URL_DEMANDEE de la session http.
     *
     * @param session the session
     * @return the string
     */
    public static String renvoyerUrlRedirection(final HttpSession session) {
        String res = null;
        final String urlDemandee = (String) session.getAttribute("URL_DEMANDEE");
        final Long timeUrlDemandee = (Long) session.getAttribute("TIME_URL_DEMANDEE");
        // on teste un time out au niveau de la redirection
        if (urlDemandee != null) {
            if (timeUrlDemandee != null) {
                if ((System.currentTimeMillis() - timeUrlDemandee) < 60000) {
                    res = urlDemandee;
                }
                session.removeAttribute("TIME_URL_DEMANDEE");
            }
            session.removeAttribute("URL_DEMANDEE");
        }
        return res;
    }

    /**
     * Initialise la redirection dans la variable URL_DEMANDEE de la session http.
     *
     * @param session     the session
     * @param callbackUrl the callback url
     */
    public static void initialiserUrlRedirection(final HttpSession session, String callbackUrl) {
        if (session != null && callbackUrl != null) {
            if (callbackUrl.contains("?") || !callbackUrl.contains("%")) {
                try {
                    callbackUrl = URLEncoder.encode(callbackUrl, CharEncoding.DEFAULT);
                } catch (UnsupportedEncodingException e) {
                    LOGGER.error("erreur lors de l'encodage de l'url de callback", e);
                }
            }
            // on alimente la session http avec les elements de redirection
            session.setAttribute("URL_DEMANDEE", callbackUrl);
            session.setAttribute("TIME_URL_DEMANDEE", System.currentTimeMillis());
        }
    }

    /**
     * Redirection vers la page de login.
     *
     * @param infoBean the info bean
     * @param request  the request
     * @param response the response
     * @throws IOException the exception
     */
    public static void redirigerVersLogin(final InfoBean infoBean, final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final ContexteUniv ctx = new ContexteUniv(request);
        if (infoBean.get("RH") != null) {
            ctx.setCodeRubriquePageCourante(infoBean.getString("RH"));
        }
        redirigerVersLogin(ctx, response, StringUtils.EMPTY);
    }

    /**
     * Redirection vers un site.
     *
     * @param response the response
     * @param ctx      the ctx
     * @param e        the e
     * @throws Exception the exception
     */
    public static void redirigerVersUrl(final ContexteUniv ctx, HttpServletResponse response, final ExceptionSite e) throws IOException {
        String url = URLDecoder.decode(e.getUrlRedirect(), CharEncoding.DEFAULT);
        url = url.replaceAll("&amp;", "&");
        if (response instanceof CacheUrlWrappedResponse) {
            response = ((CacheUrlWrappedResponse) response).getHttpServletResponse();
        }
        response.sendRedirect(url);
    }

    /**
     * Renvoie les encadres de la rubrique courante et eventuellement de ses rubriques mere.
     *
     * @param rubrique the rubrique
     * @return la liste des encadres pour la rubrique demandee
     * @deprecated les encadrés ne sont plus récupérer de cette façon.
     */
    @Deprecated
    public static ArrayList<String> getListeEncadresRubriqueCourante(final InfosRubriques rubrique) {
        final ArrayList<String> listEncadreRubrique = new ArrayList<>();
        InfosRubriques tmpRubrique = rubrique;
        while (tmpRubrique.getNiveau() > 0) {
            if (tmpRubrique.getEncadre().length() > 0) {
                // si rubrique courante ou si encadre affichable dans les
                // sous-rubriques
                if (tmpRubrique.equals(rubrique) || tmpRubrique.isEncadreSousRubrique()) {
                    listEncadreRubrique.add(0, tmpRubrique.getEncadre());
                }
            }
            tmpRubrique = tmpRubrique.getRubriqueMere();
        }
        return listEncadreRubrique;
    }

    /**
     * Inserer combo libelle.
     *
     * @param ctx           the ctx
     * @param nomDonnee     the nom donnee
     * @param nomCss        the nom css
     * @param typeLibelle   the type libelle
     * @param libelleDefaut the libelle defaut
     * @return the string
     */
    public static String insererComboLibelle(final ContexteUniv ctx, final String nomDonnee, final String nomCss, final String typeLibelle, final String libelleDefaut) {
        String res = StringUtils.EMPTY;
        res += "<select id=\"" + nomCss + "\" name=\"" + nomDonnee + "\" size=\"1\" onchange=\"window.location.href=this.options[this.selectedIndex].value;\">";
        res += "<option value=\"#\" selected >" + libelleDefaut + "</option>";
        try {
            final Map<String, String> hLibelle = LabelUtils.getLabelCombo(typeLibelle, ctx.getLocale());
            if (!(hLibelle.isEmpty())) {
                // on ordonne les codes
                final TreeSet<String> tree = new TreeSet<>(hLibelle.keySet());
                // chaineLibelle : " nomObjet#code ou lien externe#libelle "
                for (String key : tree) {
                    final String[] chaine = hLibelle.get(key).split("#", -2);
                    String code = StringUtils.EMPTY;
                    String libelle = StringUtils.EMPTY;
                    if (chaine.length > 0) {
                        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(chaine[0]);
                        if (chaine.length > 1) {
                            code = chaine[1];
                        }
                        if (chaine.length > 2) {
                            libelle = chaine[2];
                        }
                        if (ficheUniv != null) {
                            ficheUniv.setCtx(ctx);
                            ficheUniv.init();
                            if (ficheUniv.selectCodeLangueEtat(code, LangueUtil.getLangueLocale(ctx.getLocale()), "0003") > 0) {
                                ficheUniv.nextItem();
                                if (StringUtils.isEmpty(libelle)) {
                                    libelle = ficheUniv.getLibelleAffichable();
                                }
                                res += "<option value=\"" + URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(ctx, ficheUniv), ctx) + "\">" + libelle + "</option>";
                            }
                        } else {
                            if (StringUtils.isEmpty(code)) {
                                code = "#";
                            }
                            if (StringUtils.isEmpty(libelle)) {
                                libelle = "Option";
                            }
                            res += "<option value=\"" + UnivWebFmt.formaterEnHTML(ctx, code) + "\">" + libelle + "</option>";
                        }
                    }
                }
            }
        } catch (final Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        res += "</select>";
        return res;
    }

    /**
     * Renvoie la liste des structures sous forme d'une liste de tags option, ce qui donne avec le separateur "-" par exemple :
     * <p/>
     * Structure 1 -Sous Structure 11 -sous Structure 12 --sousous Structure 121 Structure 2 ...
     *
     * @param ctx                 the ctx
     * @param codeStructureRoot   the separateur
     * @param codeStructureSelect the separateur
     * @param separateur          the separateur
     * @return the string
     * @throws Exception the exception
     */
    public static String insererComboStructures(final ContexteUniv ctx, final String codeStructureRoot, final String codeStructureSelect, final String separateur) throws Exception {
        /*
         * par defaut : pas de longueur max, on renvoit le code dans "value" et
         * on met en majuscules le premier niveau de rubriques
         */
        return insererComboStructures(ctx, codeStructureRoot, codeStructureSelect, separateur, -1, 0, 0);
    }

    /**
     * Renvoie la liste des structures sous forme d'une liste de tags option, ce qui donne avec le separateur "-" par exemple :
     * <p/>
     * Structure 1 -Sous Structure 11 -sous Structure 12 --sousous Structure 121 Structure 2 ...
     * <p/>
     * separateur est un caractere qui servira pour distinguer les niveaux de structure
     * <p/>
     * longueurMax definit si l'intitule est limite en nb de caracteres. Si longueurMax vaut -1, on affiche tous les caracteres
     * <p/>
     * typeValue vaut 0 ou 1. Il permet de savoir ce que l'on met dans l'attribut Value du tag "option". Si 0, on met le code de la rubrique, si 1, on met l'url de la page
     * d'accueil de la rubrique.
     * <p/>
     * uppercase vaut 0 ou 1. Il permet de mettre en majuscule le premier niveau de rubrique ou non. Si 0, on ne met pas en majuscules, si 1, on passe en majuscule.
     *
     * @param ctx         the ctx
     * @param separateur  the separateur
     * @param longueurMax the longueur max
     * @param typeValue   the type value
     * @param uppercase   the uppercase
     * @return the string
     * @throws Exception the exception
     */
    public static String insererComboStructures(final ContexteUniv ctx, String codeRoot, String codeSelect, final String separateur, final int longueurMax, final int typeValue, final int uppercase) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final StringBuilder res = new StringBuilder();
        final String langue = ctx.getLangue();
        final StructureModele infosStructureSelect = serviceStructure.getByCodeLanguage(codeSelect, langue);
        for (final StructureModele sousStructure : serviceStructure.getSubStructures(StringUtils.EMPTY, langue, true)) {
            if (serviceStructure.isVisibleInFront(sousStructure) && FicheUnivMgr.controlerRestriction(sousStructure, ctx, Boolean.TRUE)) {
                String attValue = StringUtils.EMPTY;
                String selected = StringUtils.EMPTY;
                String intitule = sousStructure.getLibelleAffichable();
                if (uppercase == 1) {
                    intitule = intitule.toUpperCase();
                }
                if (intitule.length() > longueurMax && longueurMax != -1) {
                    intitule = intitule.substring(0, longueurMax - 1);
                }
                if (typeValue == 0) {
                    attValue = sousStructure.getCode();
                } else {
                    attValue = URLResolver.getAbsoluteUrl(determinerUrlFiche(ctx, serviceStructure.getByCodeLanguage(sousStructure.getCode(), sousStructure.getLangue())), ctx);
                }
                if(infosStructureSelect != null) {
                    if (sousStructure.getCode().equals(infosStructureSelect.getCode())) {
                        selected = "selected=\"selected\"";
                    }
                    res.append("<option value=\"").append(attValue).append("\" ").append(selected).append(">").append(intitule).append("</option>\n");
                    res.append(insererComboStructuresFilles(ctx, sousStructure, 1, separateur, longueurMax, typeValue, infosStructureSelect.getCode()));
                }
            }
        }
        return res.toString();
    }

    /**
     * Renvoie les intitules des filles d'une structure en fonction de son code. la liste est une suite de tags "option". cette methode s'appelle de maniere recursive pour
     * parcourir tout l'arbre.
     *
     * @param niveau      : niveau 1 a x : un tiret de plus par niveau devant l'intitule de la rubrique.
     * @param ctx         the ctx
     * @param isMere      the is mere
     * @param separateur  the separateur
     * @param longueurMax the longueur max
     * @param typeValue   the type value
     * @return the string
     * @throws Exception the exception
     */
    private static String insererComboStructuresFilles(final ContexteUniv ctx, final StructureModele isMere, int niveau, final String separateur, final int longueurMax, final int typeValue, final String codeStructureSelect) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final StringBuilder res = new StringBuilder();
        for (final StructureModele is : serviceStructure.getSubStructures(isMere.getCode(), isMere.getLangue(), true)) {
            if (serviceStructure.isVisibleInFront(is) && FicheUnivMgr.controlerRestriction(is, ctx, Boolean.TRUE)) {
                String espaces = StringUtils.EMPTY;
                String selected = StringUtils.EMPTY;
                for (int niv = 0; niv < niveau; niv++) {
                    espaces += separateur;
                }
                String intitule = espaces + " " + is.getLibelleAffichable();
                if (intitule.length() > longueurMax && longueurMax != -1) {
                    intitule = intitule.substring(0, longueurMax - 1);
                }
                String attValue;
                if (typeValue == 0) {
                    attValue = is.getCode();
                } else {
                    attValue = URLResolver.getAbsoluteUrl(determinerUrlFiche(ctx, serviceStructure.getByCodeLanguage(is.getCode(), is.getLangue())), ctx);
                }
                if (is.getCode().equals(codeStructureSelect)) {
                    selected = "selected=\"selected\"";
                }
                res.append("<option value=\"").append(attValue).append("\" ").append(selected).append(">").append(intitule).append("</option>\n");
                niveau++;
                res.append(insererComboStructuresFilles(ctx, is, niveau, separateur, longueurMax, typeValue, codeStructureSelect));
                niveau--;
            }
        }
        return res.toString();
    }
}
