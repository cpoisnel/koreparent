package com.univ.utils;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.email.JSBMailbox;

/**
 * The Class ThreadMail.
 */
public class ThreadMail implements Runnable {

    /** The _log. */
    private static final Logger LOG = LoggerFactory.getLogger(ThreadMail.class);

    /** The _lst to (liste de String[]{mailTo,nomTo,codeTo}). */
    private final List<String> _lstTo;

    /** The _subject. */
    private final String _subject;

    /** The _message. */
    private final String _message;

    @Deprecated
    public ThreadMail(final String[] _from, final List<String[]> _lstTo, final String _subject, final String _message, final String[] _pathFichier, final String[] _nomFichier, final String _typeMime) {
        ArrayList<String> listTo = new ArrayList<>();
        for (String[] to : _lstTo) {
            listTo.add(to[0]);
        }
        this._lstTo = listTo;
        this._subject = _subject;
        this._message = _message;
    }

    /**
     * constructeur.
     *
     * @param _lstTo
     *            the _lst to
     * @param _subject
     *            the _subject
     * @param _message
     *            the _message
     */
    public ThreadMail(final List<String> _lstTo, final String _subject, final String _message) {
        this._lstTo = _lstTo;
        this._subject = _subject;
        this._message = _message;
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        final JSBMailbox mailbox = new JSBMailbox(false);
        // envoi d'un mail par destinataire
        for (String to : _lstTo) {
            try {
                mailbox.sendSystemMsg(to, _subject, _message);
            } catch (final MessagingException | EmailException e) {
                LOG.error("Echec de l'envoi de mail : " + e.getMessage(), e);
            }
        }
    }
}
