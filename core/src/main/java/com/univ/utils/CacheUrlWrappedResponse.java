/*
 * Created on 26 sept. 2005
 *
 * Gestion de caches d'url
 */
package com.univ.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
// TODO: Auto-generated Javadoc

/**
 * The Class CacheUrlWrappedResponse.
 *
 * @author jeanseb
 *
 *         Pseudo-reponse http pour faire des appels de cache
 */
public class CacheUrlWrappedResponse extends HttpServletResponseWrapper {

    /** The writer. */
    private PrintWriter writer;

    /** The response. */
    private HttpServletResponse response;

    /**
     * Instantiates a new cache url wrapped response.
     *
     * @param _response
     *            the _response
     * @param _writer
     *            the _writer
     */
    public CacheUrlWrappedResponse(HttpServletResponse _response, PrintWriter _writer) {
        super(_response);
        writer = _writer;
        response = _response;
    }

    /* (non-Javadoc)
     * @see javax.servlet.ServletResponseWrapper#getWriter()
     */
    @Override
    public PrintWriter getWriter() throws IOException {
        return writer;
    }

    /* (non-Javadoc)
     * @see javax.servlet.ServletResponseWrapper#getContentType()
     */
    @Override
    public String getContentType() {
        return "text/html";
    }

    /**
     * Gets the http servlet response.
     *
     * @return the http servlet response
     */
    public HttpServletResponse getHttpServletResponse() {
        return response;
    }
}
