package com.univ.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.om.SSOBean;
import com.univ.objetspartages.om.SousParagrapheBean;
import com.univ.xml.NodeUtil;

/**
 * The Class RequeteConnecteur.
 */
public class RequeteConnecteur {

    /** Logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(RequeteConnecteur.class);

    /**
     * Parsing du flux XML envoyé par le programme externe et initialisation du contexte.
     *
     * @param ctx
     *            com.univ.utils.ContexteUniv ctx pour accès à la base
     *
     */
    public static void parseFlux(final ContexteUniv ctx) {
        /* Données à mettre dans le contexte */
        String rubriqueExterneConnecteur = "";
        final List<SousParagrapheBean> encadresExternes = new ArrayList<>();
        final List<String> encadresRechercheExternes = new ArrayList<>();
        final Map<String, String> donneesSpecifiques = new HashMap<>();
        String langue = "";
        /* Parsing du fichier de requête XML */
        //String ksession = ctx.getKsession();
        final String graphBean = ctx.getRequeteHTTP().getParameter("graphbean");
        LOGGER.debug("graphbean = " + graphBean);
        if (graphBean == null) {
            return;
        }
        final InputStream is = new ByteArrayInputStream(graphBean.getBytes());
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document document = null;
        Element rootElement = null;
        try {
            builder = factory.newDocumentBuilder();
            document = builder.parse(is);
            rootElement = document.getDocumentElement();
        } catch (final ParserConfigurationException | SAXException | IOException e) {
            // Erreur lors du parsing
            // On continue normalement (pas de traitement)
            LOGGER.info("erreur de parsing", e);
        }
        Node donnees = rootElement;
        while (donnees != null) {
            if (donnees.hasChildNodes()) {
                Node requete = donnees.getFirstChild();
                while (requete != null) {
                    if (requete.getNodeName().compareTo("RUBRIQUE") == 0) {
                        if (requete.getFirstChild() != null) {
                            rubriqueExterneConnecteur = requete.getFirstChild().getNodeValue().trim();
                        }
                    }
                    if (requete.getNodeName().compareTo("LANGUE") == 0) {
                        if (requete.getFirstChild() != null) {
                            langue = requete.getFirstChild().getNodeValue().trim();
                        }
                    }
                    if ("SECURE".equals(requete.getNodeName())) {
                        if (requete.getFirstChild() != null) {
                            ctx.setSecure("1".equals(requete.getFirstChild().getNodeValue().trim()));
                        }
                    }
                    if (requete.getNodeName().compareTo("LISTE_ENCADRES") == 0) {
                        Node listeEncadres = requete.getFirstChild();
                        while (listeEncadres != null) {
                            if (listeEncadres.getNodeName().compareTo("ENCADRE") == 0) {
                                final SousParagrapheBean encadre = new SousParagrapheBean();
                                Node nodeEncadre = listeEncadres.getFirstChild();
                                while (nodeEncadre != null) {
                                    if (nodeEncadre.getNodeName().compareTo("TITRE") == 0) {
                                        encadre.setTitre(nodeEncadre.getFirstChild().getNodeValue().trim());
                                    }
                                    if (nodeEncadre.getNodeName().compareTo("CONTENU") == 0) {
                                        encadre.setContenu(nodeEncadre.getFirstChild().getNodeValue().trim());
                                    }
                                    nodeEncadre = nodeEncadre.getNextSibling();
                                }
                                encadresExternes.add(encadre);
                            }
                            listeEncadres = listeEncadres.getNextSibling();
                        }
                    }
                    if (requete.getNodeName().compareTo("DONNEES_SPECIFIQUES") == 0) {
                        Node listeDonnees = requete.getFirstChild();
                        while (listeDonnees != null) {
                            if (listeDonnees.getNodeName().compareTo("DONNEE") == 0) {
                                Node nodeDonnees = listeDonnees.getFirstChild();
                                String nom = "";
                                String valeur = "";
                                while (nodeDonnees != null) {
                                    if (nodeDonnees.getNodeName().compareTo("NOM") == 0) {
                                        nom = nodeDonnees.getFirstChild().getNodeValue().trim();
                                    }
                                    if (nodeDonnees.getNodeName().compareTo("VALEUR") == 0) {
                                        valeur = nodeDonnees.getFirstChild().getNodeValue().trim();
                                    }
                                    nodeDonnees = nodeDonnees.getNextSibling();
                                }
                                /* Ajout de la donnée */
                                if (nom.length() > 0) {
                                    donneesSpecifiques.put(nom, valeur);
                                }
                            }
                            listeDonnees = listeDonnees.getNextSibling();
                        }
                    }
                    if (requete.getNodeName().compareTo("LISTE_ENCADRES_RECHERCHE") == 0) {
                        Node listeEncadresRecherche = requete.getFirstChild();
                        while (listeEncadresRecherche != null) {
                            if (listeEncadresRecherche.getFirstChild() != null) {
                                encadresRechercheExternes.add(listeEncadresRecherche.getFirstChild().getNodeValue().trim());
                            }
                            listeEncadresRecherche = listeEncadresRecherche.getNextSibling();
                        }
                    }
                    requete = requete.getNextSibling();
                }
            }
            donnees = donnees.getNextSibling();
        }
        /* Ajout des information dans le contexte */
        ctx.setRubriqueExterneConnecteur(rubriqueExterneConnecteur);
        ctx.setEncadresExternes(encadresExternes);
        ctx.setEncadresRechercheExternes(encadresRechercheExternes);
        ctx.setDonneesSpecifiques(donneesSpecifiques);
        // JSS 20050912 : ajout de la langue pour les connecteurs
        if (langue.length() > 0) {
            ctx.setLangue(langue);
        }
    }

    /**
     * Lecture du Bean SSO à partir du fichier session.xml
     *
     * @param _ksession
     *            the _ksession
     *
     * @return the SSO bean
     *
     * @throws Exception
     *             the exception
     */
    public static SSOBean lireBeanSSO(final String _ksession) throws Exception {
        final SSOBean ssoBean = new SSOBean();
        /** *********************************************** */
        /* Lecture du fichier XML DE SESSION */
        /** ********************************************** */
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = null;
        final File f = new File(WebAppUtil.getSessionsPath() + File.separator + "session_" + _ksession + ".xml");
        //le fichier n'existe pas
        if (!f.exists()) {
            return ssoBean;
        }
        document = builder.parse(f);
        final Element rootElement = document.getDocumentElement();
        final Node donnees = rootElement;
        if (donnees.hasChildNodes()) {
            Node nextFeuille = donnees.getFirstChild();
            while (nextFeuille != null) {
                if ("ID".equals(nextFeuille.getNodeName())) {
                    ssoBean.setId(NodeUtil.extraireValeurNode(nextFeuille));
                }
                if ("CODE".equals(nextFeuille.getNodeName())) {
                    ssoBean.setCodeKportal(NodeUtil.extraireValeurNode(nextFeuille));
                }
                if ("CIVILITE".equals(nextFeuille.getNodeName())) {
                    ssoBean.setCivilite(NodeUtil.extraireValeurNode(nextFeuille));
                }
                if ("NOM".equals(nextFeuille.getNodeName())) {
                    ssoBean.setNom(NodeUtil.extraireValeurNode(nextFeuille));
                }
                if ("PRENOM".equals(nextFeuille.getNodeName())) {
                    ssoBean.setPrenom(NodeUtil.extraireValeurNode(nextFeuille));
                }
                if ("EMAIL".equals(nextFeuille.getNodeName())) {
                    ssoBean.setEmail(NodeUtil.extraireValeurNode(nextFeuille));
                }
                if ("STRUCTURE".equals(nextFeuille.getNodeName())) {
                    ssoBean.setStructure(NodeUtil.extraireValeurNode(nextFeuille));
                }
                if ("CODE_LDAP".equals(nextFeuille.getNodeName())) {
                    ssoBean.setCodeGestion(NodeUtil.extraireValeurNode(nextFeuille));
                }
                if ("PROFIL".equals(nextFeuille.getNodeName())) {
                    ssoBean.setProfil(NodeUtil.extraireValeurNode(nextFeuille));
                }
                if ("GROUPES".equals(nextFeuille.getNodeName())) {
                    ssoBean.setGroupes(NodeUtil.extraireValeurNode(nextFeuille));
                }
                nextFeuille = nextFeuille.getNextSibling();
            }
        }
        return ssoBean;
    }
}
