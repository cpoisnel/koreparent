package com.univ.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.RequeteSQLHelper;

/**
 * The Class AbstractRequeteur.
 */
public abstract class AbstractRequeteur implements IRequeteurConstantes {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRequeteur.class);

    /** The mode. */
    private String mode = MODE_CONSULTATION;

    /** The liste actions. */
    private TreeSet<String> listeActions = null;

    /** The pagination. */
    private boolean pagination = true;

    /** The lien_ajout. */
    private boolean lien_ajout = true;

    /** The liste attributs. */
    private Hashtable<String, AttributRequeteur> listeAttributs = null;

    /**
     * Constructeur à partir de l'infoBean.
     */
    public AbstractRequeteur() {
    }

    /**
     * Constructeur à partir de l'infoBean.
     *
     * @param infoBean
     *            the info bean
     */
    public AbstractRequeteur(final InfoBean infoBean) {
        String mode = "";
        String selection = "";
        String pagination = "";
        String lien = "";
        String dossier = "";
        String requete = "";
        if (infoBean.get(MODE) != null) {
            mode = infoBean.getString(MODE);
        }
        if (infoBean.get(ACTIONS) != null) {
            selection = infoBean.getString(ACTIONS);
        }
        if (infoBean.get(AVEC_PAGINATION) != null) {
            pagination = infoBean.getString(AVEC_PAGINATION);
        }
        if (infoBean.get(AVEC_AJOUT) != null) {
            lien = infoBean.getString(AVEC_AJOUT);
        }
        if (infoBean.get(AVEC_DOSSIER) != null) {
            dossier = infoBean.getString(AVEC_DOSSIER);
        }
        if (infoBean.get(REQUETE) != null) {
            requete = infoBean.getString(REQUETE);
        }
        initialiserRequeteur(mode, selection, pagination, lien, dossier, requete);
    }

    /**
     * Constructeur à partir d'une chaine de parametre.
     *
     * @param mode
     *            the mode
     * @param selection
     *            the selection
     * @param pagination
     *            the pagination
     * @param lien
     *            the lien
     * @param requete
     *            the requete
     */
    public AbstractRequeteur(final String mode, final String selection, final String pagination, final String lien, final String requete) {
        initialiserRequeteur(mode, selection, pagination, lien, "0", requete);
    }

    /**
     * Constructeur à partir d'une chaine de parametre.
     *
     * @param mode
     *            the mode
     * @param selection
     *            the selection
     * @param pagination
     *            the pagination
     * @param lien
     *            the lien
     * @param dossier
     *            the dossier
     * @param requete
     *            the requete
     */
    public AbstractRequeteur(final String mode, final String selection, final String pagination, final String lien, final String dossier, final String requete) {
        initialiserRequeteur(mode, selection, pagination, lien, dossier, requete);
    }

    /**
     * Gets the boolean value.
     *
     * @param valeur
     *            the valeur
     * @param defaut
     *            the defaut
     *
     * @return the boolean value
     */
    public static boolean getBooleanValue(final String valeur, final boolean defaut) {
        boolean res = defaut;
        if ("1".equals(valeur)) {
            res = true;
        } else if ("0".equals(valeur)) {
            res = false;
        }
        return res;
    }

    /**
     * Gets the boolean value.
     *
     * @param valeur
     *            the valeur
     *
     * @return the boolean value
     */
    public static boolean getBooleanValue(final String valeur) {
        boolean res = true;
        if ("1".equals(valeur)) {
            res = true;
        } else if ("0".equals(valeur)) {
            res = false;
        }
        return res;
    }

    /**
     * Gets the triable boolean value.
     *
     * @param valeur
     *            the valeur
     *
     * @return the triable boolean value
     */
    public static boolean getTriableBooleanValue(final String valeur) {
        boolean res = false;
        if ("0000".equals(valeur) || "".equals(valeur) || "TOUS".equals(valeur)) {
            res = true;
        }
        return res;
    }

    /**
     * Renvoyer date selection.
     *
     * @param _code
     *            the _code
     *
     * @return the java.sql. date
     */
    public static Date renvoyerDateSelection(final String _code) {
        Date dateSelection = null;
        if (_code.length() > 0) {
            final Date dateAujourdhui = new Date(System.currentTimeMillis());
            final java.util.GregorianCalendar cal = new java.util.GregorianCalendar();
            cal.setTime(dateAujourdhui);
            // jour
            if ("0001".equals(_code)) {
                dateSelection = dateAujourdhui;
            } else if ("0002".equals(_code)) {
                final int offset = (cal.get(Calendar.DAY_OF_WEEK) - Calendar.MONDAY + 7) % 7;
                cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - offset);
                dateSelection = new Date(cal.getTimeInMillis());
            }
            // mois
            else if ("0003".equals(_code)) {
                final int offset = cal.get(Calendar.DAY_OF_MONTH) - 1;
                cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - offset);
                dateSelection = new Date(cal.getTimeInMillis());
            }
        }
        return dateSelection;
    }

    /**
     * Parcours l'ensemble des objets à traiter et rajoute pleins de conditions
     *
     * @param listeDesObjets
     * @param autorisations
     * @param mode
     * @param codeStructure
     * @param codeRubrique
     * @param diffusionPublicVise
     * @param diffusionPublicViseRestriction
     * @param diffusionModeRestriction
     * @return
     */
    protected static Condition traiterListeDesObjets(final Collection<String> listeDesObjets, final AutorisationBean autorisations, final String mode, final String codeStructure, final String codeRubrique, final String diffusionPublicVise, final String diffusionPublicViseRestriction, final String diffusionModeRestriction) {
        final ConditionList conditionSurObjet = new ConditionList();
        final Set<String> codesDesObjets = new HashSet<>();
        for (final String nomObjet : listeDesObjets) {
            FicheUniv ficheUniv = ReferentielObjets.instancierFiche(nomObjet);
            if (isModeModificationOuValidation(ficheUniv, mode)) {
                ficheUniv = initialiserFicheUniv(ficheUniv, codeStructure, codeRubrique, diffusionPublicVise, diffusionModeRestriction, diffusionPublicViseRestriction);
                conditionSurObjet.or(RequeteSQLHelper.traiterConditionDsiMeta(autorisations, ficheUniv, mode, codeRubrique, null));
            }
            if (isModeConsultationOuModification(ficheUniv, mode)) {
                codesDesObjets.add(ReferentielObjets.getCodeObjet(ficheUniv));
            }
        }
        if (conditionSurObjet.isEmpty()) {
            if (codesDesObjets.isEmpty()) {
                return ConditionHelper.egalVarchar("T1.META_CODE_OBJET", "0000");
            }
            conditionSurObjet.setPremiereCondtion(ConditionHelper.in("T1.META_CODE_OBJET", codesDesObjets));
            if (MODE_MODIFICATION.equals(mode)) {
                conditionSurObjet.and(ConditionHelper.egalVarchar("T1.META_CODE_REDACTEUR", autorisations.getCode()));
            }
        } else if (!codesDesObjets.isEmpty()) {
            final ConditionList conditionCodeObjetEtRedacteur = new ConditionList(ConditionHelper.in("T1.META_CODE_OBJET", codesDesObjets));
            conditionCodeObjetEtRedacteur.and(ConditionHelper.egalVarchar("T1.META_CODE_REDACTEUR", autorisations.getCode()));
            conditionSurObjet.or(conditionCodeObjetEtRedacteur);
        }
        return conditionSurObjet;
    }

    private static boolean isModeModificationOuValidation(final FicheUniv ficheUniv, final String mode) {
        return ficheUniv != null && (MODE_MODIFICATION.equals(mode) || MODE_VALIDATION.equals(mode));
    }

    private static boolean isModeConsultationOuModification(final FicheUniv ficheUniv, final String mode) {
        return ficheUniv != null && (StringUtils.isEmpty(mode) || MODE_CONSULTATION.equals(mode) || MODE_MODIFICATION.equals(mode));
    }

    /**
     * initialise un objet FicheUniv en fonction du type de la ficheUniv fourni en paramètre.
     *
     * @param ficheUniv
     * @param codeStructure
     * @param codeRubrique
     * @param diffusionPublicVise
     * @param diffusionModeRestriction
     * @param diffusionPublicViseRestriction
     * @return
     */
    private static FicheUniv initialiserFicheUniv(final FicheUniv ficheUniv, final String codeStructure, final String codeRubrique, final String diffusionPublicVise, final String diffusionModeRestriction, final String diffusionPublicViseRestriction) {
        ficheUniv.init();
        ficheUniv.setCodeRattachement(codeStructure);
        ficheUniv.setCodeRubrique(codeRubrique);
        if (ficheUniv instanceof FicheRattachementsSecondaires) {
            ((FicheRattachementsSecondaires) ficheUniv).setCodeRattachementAutres(codeStructure);
        }
        if (ficheUniv instanceof DiffusionSelective) {
            ((DiffusionSelective) ficheUniv).setDiffusionPublicVise(diffusionPublicVise);
            ((DiffusionSelective) ficheUniv).setDiffusionModeRestriction(diffusionModeRestriction);
            ((DiffusionSelective) ficheUniv).setDiffusionPublicViseRestriction(diffusionPublicViseRestriction);
        }
        return ficheUniv;
    }

    /**
     * Recherche d'une fiche par rapport à sa structure de rattachement (zone multivaluée).
     *
     * @param _nomDonnee
     *            the _nom donnee
     * @param _codeStructures
     *            the _code structures
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public static String formaterRechercheParStructureMultiple(final String _nomDonnee, final String _codeStructures) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        String resultatRequete = "";
        //JSS 20021104-001
        //Les '+' sont à convertir en ';' (; interdits en saisie WYSIWYG)
        final String newStructures = StringUtils.replace(_codeStructures, "+", ";");
        final StringTokenizer stStructures = new StringTokenizer(newStructures, ";");
        String codeStructure;
        while (stStructures.hasMoreTokens()) {
            codeStructure = stStructures.nextToken();
            if (!"".equals(codeStructure)) {
                final List<StructureModele> subStructures = serviceStructure.getAllSubStructures(codeStructure, LangueUtil.getIndiceLocaleDefaut(), true);
                if (resultatRequete.length() > 0) {
                    resultatRequete += " OR ";
                }
                //resultatSousRequete += _nomDonnee +" REGEXP '(^|.*;)"+ codeStructure +"($|;.*)'";
                resultatRequete += _nomDonnee.toUpperCase() + " LIKE '%[" + codeStructure + "]%'";
                for (StructureModele currentStructure : subStructures) {
                    resultatRequete += " OR ";
                    resultatRequete += _nomDonnee.toUpperCase() + " LIKE '%[" + currentStructure.getCode() + "]%'";
                }
            }
        }
        return resultatRequete;
    }

    /**
     * Intialiser requeteur.
     *
     * @param _mode
     *            the _mode
     * @param _selection
     *            the _selection
     * @param _pagination
     *            the _pagination
     * @param _lien
     *            the _lien
     * @param dossier
     *            the dossier
     * @param _requete
     *            the _requete
     */
    private void initialiserRequeteur(final String _mode, final String _selection, final String _pagination, final String _lien, final String dossier, final String _requete) {
        this.mode = _mode;
        this.listeActions = initialiserActions(_selection);
        this.pagination = getBooleanValue(_pagination);
        this.lien_ajout = getBooleanValue(_lien);
        this.listeAttributs = initialiserAttributs(_requete);
        finaliser();
    }

    /**
     * Initialiser actions.
     *
     * @param _actions
     *            the _actions
     *
     * @return the tree set
     */
    private TreeSet<String> initialiserActions(String _actions) {
        final TreeSet<String> treeTemp = new TreeSet<>();
        if (_actions == null) {
            _actions = "";
        }
        if ("".equals(_actions)) {
            if (!mode.equals(MODE_CONSULTATION)) {
                treeTemp.add(ACTION_VOIR);
                treeTemp.add(ACTION_MODIFIER);
            }
        } else {
            final StringTokenizer st = new StringTokenizer(_actions, "|");
            while (st.hasMoreElements()) {
                if (st.hasMoreTokens()) {
                    final String action = st.nextToken();
                    if (action.equals(ACTION_MODIFIER) || action.equals(ACTION_SUPPRIMER) || action.equals(ACTION_VOIR)) {
                        treeTemp.add(action);
                    }
                }
            }
        }
        return treeTemp;
    }

    /**
     * Initialiser attributs.
     *
     * @param _requete
     *            the _requete
     *
     * @return the hashtable
     */
    private Hashtable<String, AttributRequeteur> initialiserAttributs(final String _requete) {
        String nom = "";
        String valeur = "";
        boolean requetable = true;
        boolean triable = true;
        String val = "";
        final Hashtable<String, AttributRequeteur> hTemp = new Hashtable<>();
        //StringTokenizer st = new StringTokenizer(_requete, "@");
        //while (st.hasMoreTokens()) {
        final String[] tempTab = _requete.split("@@", -2);
        for (final String element : tempTab) {
            //val = st.nextToken();
            val = element;
            final int indexNom = val.indexOf("=");
            if (indexNom != -1) {
                nom = val.substring(0, indexNom).toUpperCase();
                val = val.substring(indexNom + 1);
                final StringTokenizer st2 = new StringTokenizer(val, "|");
                if (st2.countTokens() == 3 || st2.countTokens() == 2) {
                    if (st2.countTokens() == 3 && st2.hasMoreTokens()) {
                        valeur = st2.nextToken();
                    } else {
                        valeur = "";
                    }
                    if (st2.hasMoreTokens()) {
                        requetable = getBooleanValue(st2.nextToken());
                    }
                    if (st2.hasMoreTokens()) {
                        triable = getBooleanValue(st2.nextToken());
                    }
                    hTemp.put(nom, new AttributRequeteur(nom, valeur, requetable, triable));
                } else if (getBooleanValue(val)) {
                    hTemp.put(nom, new AttributRequeteur(nom, "", true, true));
                } else if (!getBooleanValue(val)) {
                    hTemp.put(nom, new AttributRequeteur(nom, "", false, false));
                } else if (val.length() > 0) {
                    hTemp.put(nom, new AttributRequeteur(nom, val, true, true));
                }
            }
        }
        return hTemp;
    }

    /**
     * Finaliser.
     */
    private void finaliser() {
        if (this.mode.equals(MODE_CONSULTATION)) {
            setAttribut(ATTRIBUT_ETAT_OBJET, "0003", false, false);
        }
        if (this.mode.equals(MODE_VALIDATION)) {
            setAttribut(ATTRIBUT_ETAT_OBJET, "0002", false, false);
            String message = MessageHelper.getCoreMessage(ContexteUtil.getContexteUniv().getLocale(), TITRE_MES_FICHES_A_VALIDER);
            if (message.length() == 0) {
                message = "Mes fiches à valider";
            }
            setAttribut("TITRE", message, false, false);
        }
        if (listeAttributs.containsKey(ATTRIBUT_CODE_REDACTEUR)) {
            final String code = getAttribut(ATTRIBUT_CODE_REDACTEUR).getValeur();
            if (code.equals(CODE_DYNAMIQUE)) {
                String message = MessageHelper.getCoreMessage(ContexteUtil.getContexteUniv().getLocale(), TITRE_MES_FICHES);
                if (message.length() == 0) {
                    message = "Mes fiches";
                }
                setAttribut("TITRE", message, false, false);
            }
        }
    }

    /**
     * Sets the attribut.
     *
     * @param _nom
     *            the _nom
     * @param _valeur
     *            the _valeur
     * @param _requetable
     *            the _requetable
     * @param _triable
     *            the _triable
     */
    public final void setAttribut(final String _nom, final String _valeur, final boolean _requetable, final boolean _triable) {
        if (listeAttributs.containsKey(_nom)) {
            listeAttributs.remove(_nom);
        }
        listeAttributs.put(_nom, new AttributRequeteur(_nom, _valeur, _requetable, _triable));
    }

    /**
     * Sets the attribut.
     *
     * @param _nom
     *            the _nom
     * @param _chaine
     *            the _chaine
     */
    public final void setAttribut(final String _nom, final String _chaine) {
        String valeur = _chaine;
        boolean requetable = true;
        boolean triable = true;
        final StringTokenizer st2 = new StringTokenizer(_chaine, "|");
        if (st2.countTokens() == 3 || st2.countTokens() == 2) {
            if (st2.countTokens() == 3 && st2.hasMoreTokens()) {
                valeur = st2.nextToken();
            } else {
                valeur = "";
            }
            if (st2.hasMoreTokens()) {
                requetable = getBooleanValue(st2.nextToken());
            }
            if (st2.hasMoreTokens()) {
                triable = getBooleanValue(st2.nextToken());
            }
        } else if ("1".equals(valeur) && !ATTRIBUT_LANGUE.equals(_nom)) {
            valeur = "";
            requetable = true;
            triable = true;
        } else if ("0".equals(valeur) && !ATTRIBUT_LANGUE.equals(_nom)) {
            valeur = "";
            requetable = false;
            triable = false;
        } else if (valeur.length() > 0) {
            requetable = true;
            triable = true;
        }
        if (listeAttributs.containsKey(_nom)) {
            listeAttributs.remove(_nom);
        }
        listeAttributs.put(_nom, new AttributRequeteur(_nom, valeur, requetable, triable));
    }

    /**
     * Gets the attribut.
     *
     * @param _nom
     *            the _nom
     *
     * @return the attribut
     */
    public final AttributRequeteur getAttribut(final String _nom) {
        AttributRequeteur attribut = null;
        if (listeAttributs.containsKey(_nom)) {
            attribut = listeAttributs.get(_nom);
        } else {
            attribut = new AttributRequeteur();
        }
        return attribut;
    }

    /**
     * Gets the liste actions.
     *
     * @return the liste actions
     */
    public TreeSet<String> getListeActions() {
        return listeActions;
    }

    /**
     * Gets the liste attributs.
     *
     * @return the liste attributs
     */
    public Hashtable<String, AttributRequeteur> getListeAttributs() {
        return listeAttributs;
    }

    /**
     * Checks if is lien_ajout.
     *
     * @return true, if is lien_ajout
     */
    public boolean isLien_ajout() {
        return lien_ajout;
    }

    /**
     * Gets the mode.
     *
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Checks if is pagination.
     *
     * @return true, if is pagination
     */
    public boolean isPagination() {
        return pagination;
    }

    /**
     * Generer url requete.
     *
     * @return the string
     */
    public final String genererUrlRequete() {
        String res = WebAppUtil.SG_PATH + "?EXT=" + ApplicationContextManager.DEFAULT_CORE_CONTEXT + "&amp;PROC=GESTION_FICHES&amp;ACTION=LISTE&amp;";
        String temp = "";
        res += MODE + "=" + mode;
        temp = getSelection();
        if (temp.length() > 0) {
            res += "&amp;" + ACTIONS + "=" + temp;
        }
        if (!pagination) {
            res += "&amp;" + AVEC_PAGINATION + "=0";
        }
        if (!lien_ajout) {
            res += "&amp;" + AVEC_AJOUT + "=0";
        }
        temp = getAttributRequete();
        if (temp.length() > 0) {
            res += "&amp;" + REQUETE + "=" + temp;
        }
        return res;
    }

    /**
     * Gets the pagination.
     *
     * @return the pagination
     */
    public String getPagination() {
        return pagination ? "1" : "0";
    }

    /**
     * Gets the ajout lien.
     *
     * @return the ajout lien
     */
    public String getAjoutLien() {
        return lien_ajout ? "1" : "0";
    }

    /**
     * Gets the selection.
     *
     * @return the selection
     */
    public String getSelection() {
        String res = "";
        if (listeActions.size() > 0) {
            String temp = "";
            for (String listeAction : listeActions) {
                if (temp.length() > 0) {
                    temp += "|";
                }
                temp += listeAction;
            }
            res += temp;
        }
        return res;
    }

    /**
     * Gets the attribut requete.
     *
     * @return the attribut requete
     */
    public final String getAttributRequete() {
        String res = "";
        String temp = "";
        if (listeAttributs.size() > 0) {
            final Enumeration<String> e = listeAttributs.keys();
            while (e.hasMoreElements()) {
                temp = "";
                final AttributRequeteur att = getAttribut(e.nextElement());
                if (att.getValeur().length() > 0 || !getBooleanValue(att.getRequetable()) || !getBooleanValue(att.getTriable())) {
                    temp += att.getNom().toUpperCase() + "=" + att.getValeur() + "|" + att.getRequetable() + "|" + att.getTriable();
                }
                if (res.length() > 0 && temp.length() > 0) {
                    res += "@@";
                }
                res += temp;
            }
        }
        return res;
    }

    /**
     * Gets the liste fiches.
     *
     * @param _codeTri
     *            the _code tri
     * @param _ordreTri
     *            the _ordre tri
     * @param _from
     *            the _from
     * @param _increment
     *            the _increment
     * @param _count
     *            the _count
     *
     * @return the liste fiches
     *
     */
    public TreeMap<String, Object> getListeFiches(final String _codeTri, final String _ordreTri, final int _from, final int _increment, final String _count) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dateDebut = null;
        Date dateFin = null;
        try {
            dateDebut = new Date(sdf.parse(getAttribut(ATTRIBUT_DATE_DEBUT).getValeur()).getTime());
            dateFin = new Date(sdf.parse(getAttribut(ATTRIBUT_DATE_FIN).getValeur()).getTime());
        } catch (final ParseException e) {
            LOG.debug("unable to parse the dates attributes", e);
        }
        return getListeFiches(getAttribut(ATTRIBUT_CODE_OBJET).getValeur(), getAttribut(ATTRIBUT_ETAT_OBJET).getValeur(), getAttribut(ATTRIBUT_LIBELLE_FICHE).getValeur(), getAttribut(ATTRIBUT_CODE_REDACTEUR).getValeur(), getAttribut(ATTRIBUT_CODE_RUBRIQUE).getValeur(), getAttribut(ATTRIBUT_CODE_RATTACHEMENT).getValeur(), getAttribut(ATTRIBUT_TYPE_DATE).getValeur(), dateDebut, dateFin, getAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE).getValeur(), getAttribut(ATTRIBUT_DIFFUSION_MODE_RESTRICTION).getValeur(), getAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION).getValeur(), getAttribut(ATTRIBUT_LANGUE).getValeur(), _codeTri, _ordreTri, _from, _increment, getMode(), _count);
    }

    /**
     * Gets the liste fiches.
     *
     * @param _codeObjet
     *            the _code objet
     * @param _etatObjet
     *            the _etat objet
     * @param _libelle
     *            the _libelle
     * @param _codeRedacteur
     *            the _code redacteur
     * @param _codeRubrique
     *            the _code rubrique
     * @param _codeStructure
     *            the _code structure
     * @param _typeDate
     *            the _type date
     * @param _dateDebut
     *            the _date debut
     * @param _dateFin
     *            the _date fin
     * @param _diffusionPublicVise
     *            the _diffusion public vise
     * @param _diffusionModeRestriction
     *            the _diffusion mode restriction
     * @param _diffusionPublicViseRestriction
     *            the _diffusion public vise restriction
     * @param _langue
     *            the _langue
     * @param _codeTri
     *            the _code tri
     * @param _ordreTri
     *            the _ordre tri
     * @param _from
     *            the _from
     * @param _increment
     *            the _increment
     * @param _mode
     *            the _mode
     * @param _count
     *            the _count
     *
     * @return the liste fiches
     *
     */
    public TreeMap<String, Object> getListeFiches(final String _codeObjet, final String _etatObjet, final String _libelle, final String _codeRedacteur, final String _codeRubrique, final String _codeStructure, final String _typeDate, final Date _dateDebut, final Date _dateFin, final String _diffusionPublicVise, final String _diffusionModeRestriction, final String _diffusionPublicViseRestriction, final String _langue, final String _codeTri, final String _ordreTri, final int _from, final int _increment, final String _mode, final String _count) {
        String orderBy = "";
        String desc = "";
        if (_ordreTri.equals(ORDRE_TRI_DESCENDANT)) {
            desc = "DESC";
        }
        // traitement du mode de tri
        if ("".equals(_codeTri)) {
            orderBy = "META_DATE_MODIFICATION DESC, META_LIBELLE_FICHE";
        } else if (TRI_DATE_ASC.equals(_codeTri)) {
            orderBy = "META_DATE_MODIFICATION ASC, META_LIBELLE_FICHE";
        } else if (TRI_DATE_DESC.equals(_codeTri)) {
            orderBy = "META_DATE_MODIFICATION DESC, META_LIBELLE_FICHE";
        } else if (TRI_LIBELLE.equals(_codeTri)) {
            orderBy = "META_LIBELLE_FICHE " + desc + ", META_DATE_MODIFICATION DESC";
        } else if (TRI_AUTEUR.equals(_codeTri)) {
            orderBy = "META_CODE_REDACTEUR " + desc + " , META_DATE_MODIFICATION DESC , META_LIBELLE_FICHE";
        } else if (TRI_ETAT.equals(_codeTri)) {
            orderBy = "META_ETAT_OBJET " + desc + ", META_DATE_MODIFICATION DESC, META_LIBELLE_FICHE";
        } else if (TRI_OBJET.equals(_codeTri)) {
            orderBy = "META_LIBELLE_OBJET " + desc + ", META_DATE_MODIFICATION DESC, META_LIBELLE_FICHE";
        } else if (TRI_RUBRIQUE.equals(_codeTri)) {
            orderBy = "RUB.INTITULE  " + desc + ", META_DATE_MODIFICATION DESC, META_LIBELLE_FICHE";
        } else if (TRI_STRUCTURE.equals(_codeTri)) {
            orderBy = "S.LIBELLE_COURT  " + desc + ", META_DATE_MODIFICATION DESC, META_LIBELLE_FICHE";
        } else if (TRI_DSI.equals(_codeTri)) {
            orderBy = "META_DIFFUSION_PUBLIC_VISE " + desc + ", META_DATE_MODIFICATION DESC, META_LIBELLE_FICHE";
        } else if (TRI_LANGUE.equals(_codeTri)) {
            orderBy = "META_LANGUE " + desc + ", META_DATE_MODIFICATION DESC, META_LIBELLE_FICHE";
        } else {
            orderBy = _codeTri;
        }
        // choix d'une action obligatoire
        TreeMap<String, Object> liste = null;
        if (_mode.equals(MODE_CONSULTATION) || _mode.equals(MODE_MODIFICATION) || _mode.equals(MODE_VALIDATION)) {
            liste = select(_codeObjet, _etatObjet, _libelle, _codeRedacteur, "", _codeStructure, _codeRubrique, _typeDate, _dateDebut, _dateFin, _diffusionPublicVise, _diffusionModeRestriction, _diffusionPublicViseRestriction, _langue, orderBy, _from, _increment, _mode, _count);
        } else {
            liste = new TreeMap<>();
        }
        return liste;
    }

    /**
     * Gets the count.
     *
     * @return the count
     *
     */
    public int getCount() {
        int count = 0;
        try {
            count = (Integer) getListeFiches("", "", 0, 0, REQUETE_COUNT).get("" + NB_RESULTAT);
        } catch (final Exception e) {
            LOG.error("unable to query on meta", e);
        }
        return count;
    }

    /**
     * Select.
     *
     * @param typeObjet
     *            the _type objet
     * @param etatObjet
     *            the _etat objet
     * @param libelle
     *            the _libelle
     * @param codeRedacteur
     *            the _code redacteur
     * @param codeValidation
     *            the _code validation
     * @param codeRattachement
     *            the _code rattachement
     * @param codeRubrique
     *            the _code rubrique
     * @param typeDate
     *            the _type date
     * @param dateDebut
     *            the _date debut
     * @param dateFin
     *            the _date fin
     * @param diffusionPublicVise
     *            the _diffusion public vise
     * @param diffusionModeRestriction
     *            the _diffusion mode restriction
     * @param diffusionPublicViseRestriction
     *            the _diffusion public vise restriction
     * @param langue
     *            the _langue
     * @param orderBy
     *            the _order by
     * @param from
     *            the _from
     * @param increment
     *            the _increment
     * @param mode
     *            the _mode
     * @param count
     *            the _count
     *
     * @return the tree map
     *
     */
    protected abstract TreeMap<String, Object> select(String typeObjet, String etatObjet, String libelle, String codeRedacteur, String codeValidation, String codeRattachement, String codeRubrique, String typeDate, Date dateDebut, Date dateFin, String diffusionPublicVise, String diffusionModeRestriction, String diffusionPublicViseRestriction, String langue, String orderBy, int from, int increment, String mode, String count);
}
