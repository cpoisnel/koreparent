package com.univ.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;

/**
 * @author romain
 *
 */
public class ExceptionUrlObsolete extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = -1425973990878387689L;

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionUrlObsolete.class);

    /** URL absolue de redirection */
    private String urlRedirect = "";

    public ExceptionUrlObsolete(final String urlRedirect) {
        super("redirection permanente");
        LOG.debug("redirection vers " + urlRedirect);
        this.urlRedirect = urlRedirect;
    }

    /**
     * @return Returns the urlRedirect.
     */
    public String getUrlRedirect() {
        return urlRedirect;
    }
}
