package com.univ.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.RequeteMgr;
import com.jsbsoft.jtf.identification.GestionnaireIdentification;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.jsbsoft.jtf.webutils.ContextePage;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.SSOBean;
import com.univ.objetspartages.om.ServiceBean;
import com.univ.objetspartages.om.SousParagrapheBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * Contexte utilisé dans les jsp.
 */
public class ContexteUniv extends ContextePage {
    // clef de la map des infos de session permettant de récupérer la map des infos des applicatifs metier

    /** The Constant CLEF_INFOS_SESSION_APPLICATIFS_METIER. */
    public static final String CLEF_INFOS_SESSION_APPLICATIFS_METIER = "INFO_SESSION_APPLICATIFS_METIER";

    /** Logger */
    private static final Logger LOGGER = LoggerFactory.getLogger(ContexteUniv.class);

    /** nom de la jsp. */
    private Object jsp = null;

    /** nom de la jsp. */
    private JspWriter jspWriter = null;

    /** nom de la jsp. */
    private boolean apercu = false;

    /** requete. */
    private String requete = StringUtils.EMPTY;

    /** rubrique de navigation. */
    private String rubriqueExterneConnecteur = StringUtils.EMPTY;

    /** encadrés provenant d'applications externes, via connecteurs. */
    private List<SousParagrapheBean> encadresExternes = new ArrayList<>();

    /** données spécifiques provenant d'applications externes, via connecteurs. */
    private Map<String, String> donneesSpecifiques = new HashMap<>();

    /** encadrés de recherche provenant d'applications externes, via connecteurs. */
    private List<String> encadresRechercheExternes = new ArrayList<>();

    /** identifiant de session. */
    private String ksession = StringUtils.EMPTY;

    /** date de la derniere session. */
    private Date dateDerniereSession;

    /** template provenant d'applications externes, via connecteurs. */
    private String templateExterne = StringUtils.EMPTY;

    /** adresse mail de l'utilisateur. */
    private String adresseMail = StringUtils.EMPTY;

    /** Génération XHTML pour FOP. */
    private boolean genererLiensInternes = false;

    /** Vecteur des liens qui doivent pointer vers le document lui-meme (liens internes au pdf). */
    private Vector<String> codesLiensInternes = new Vector<>();

    /** Code de la rubrique courante. */
    private String codeRubriquePageCourante = StringUtils.EMPTY;

    /** Code de la rubrique courante. */
    private String codeRubriqueFicheCourante = StringUtils.EMPTY;

    /** Code de la rubrique précédente. */
    private String codeRubriqueHistorique = StringUtils.EMPTY;

    /** fiche courante. */
    private FicheUniv ficheCourante = null;

    /** meta courant. */
    private MetatagBean metaCourant = null;

    /** liste des autorisations de l'utilisateur loggué. */
    private AutorisationBean autorisation = null;

    /** Affiche-t-on une liste d'objets (utilisé par la DSI). */
    private boolean calculListeResultatsFront = false;

    /** liste des centres d'interets de l'utilisateur loggué. */
    private Collection<String> centresInteret = new Vector<>();

    /** code de l'utilisateur loggué. */
    private String code = StringUtils.EMPTY;

    /** code gestion de l'utilisateur loggué. */
    private String codeGestion = StringUtils.EMPTY;

    /** code structure de l'utilisateur loggué. */
    private String codeStructure = StringUtils.EMPTY;

    /** The groupe personnalisation courant. */
    private String groupePersonnalisationCourant = StringUtils.EMPTY;

    /** liste des groupes dsi de l'utilisateur loggué. */
    private TreeSet<String> groupesDsi = new TreeSet<>();

    /** The groupes dsi avec ascendants. */
    private TreeSet<String> groupesDsiAvecAscendants = new TreeSet<>();

    /** Liste des services. */
    private TreeSet<String> listeServicesAutorises = new TreeSet<>();

    /** The service courant. */
    private String serviceCourant = StringUtils.EMPTY;

    /** nom de l'utilisateur loggué. */
    private String nom = StringUtils.EMPTY;

    /** prenom de l'utilisateur loggué. */
    private String prenom = StringUtils.EMPTY;

    /** profil courant de l'utilisateur loggué. */
    private String profilDsi = StringUtils.EMPTY;

    /** The infos profil dsi. */
    private ProfildsiBean infosProfilDsi;

    /** liste des profils dsi de l'utilisateur loggué. */
    private TreeSet<String> listeProfilsDsi = new TreeSet<>();

    /** requete http. */
    private HttpServletRequest requeteHTTP = null;

    /** reponse http. */
    private HttpServletResponse reponseHTTP = null;

    /** servlet context. */
    private ServletContext servletContext = null;

    /** The espace courant. */
    @Deprecated
    private String espaceCourant = StringUtils.EMPTY;

    /** The espace historique. */
    @Deprecated
    private String espaceHistorique = StringUtils.EMPTY;

    /** The espace personnalisation courant : utilisé pour simuler des requêtes sur les espaces collaboratifs. */
    @Deprecated
    private String espacePersonnalisationCourant = StringUtils.EMPTY;

    /** The infos espace. */
    @Deprecated
    private EspaceCollaboratifBean espace;

    /** The liste espaces. */
    @Deprecated
    private TreeSet<String> listeEspaces = new TreeSet<>();

    /** The liste espaces dont visiteur. */
    @Deprecated
    private Vector<String> listeEspacesDontVisiteur = new Vector<>();

    /** l'url appelée par l'utilisateur **/
    private String urlPageCourante = StringUtils.EMPTY;

    private final EspaceCollaboratifDAO espaceCollaboratifDAO = ApplicationContextManager.getCoreContextBean(EspaceCollaboratifDAO.ID_BEAN, EspaceCollaboratifDAO.class);

    /**
     * constructeur ContexteUniv.
     *
     * @param _uri
     *            java.lang.String
     */
    public ContexteUniv(final String _uri) {
        super(_uri);
    }

    /**
     * constructeur ContexteUniv.
     *
     * @param _uri
     *            java.lang.String
     * @param _langue
     *            java.lang.String
     */
    public ContexteUniv(final String _uri, final String _langue) {
        super(_uri, _langue);
    }

    /**
     * Instantiates a new contexte univ.
     *
     * @param _contextePage
     *            the _contexte page
     *
     */
    public ContexteUniv(final PageContext _contextePage) {
        this((HttpServletRequest) _contextePage.getRequest());
        setReponseHTTP((HttpServletResponse) _contextePage.getResponse());
        setServletContext(_contextePage.getServletContext());
        initialiserInfosUtilisateur();
        initialiserInfosConnecteurs();
        setEspaceCourant(getEspaceHistorique());
    }

    /**
     * Constructeur.
     *
     * @param request
     *            the request
     */
    public ContexteUniv(final HttpServletRequest request) {
        super(request.getRequestURI());
        setRequeteHTTP(request);
        final String codeRH = request.getParameter("RH");
        if (codeRH != null) {
            this.codeRubriqueHistorique = codeRH;
        }
        // JSS20050126 : multi-sites
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        setInfosSite(serviceInfosSite.getSiteByHost(request.getServerName()));
        setSecure("https".equals(URLResolver.getRequestScheme(request)));
        // JSS 20040222 - Gestion des identifiants de requete
        String dump = StringUtils.EMPTY;
        if (request.getParameterNames() != null) {
            final Enumeration<String> e = request.getParameterNames();
            while (e.hasMoreElements()) {
                final String name = e.nextElement();
                dump += "param[" + name + "]=" + request.getParameter(name);
                dump += "\n";
            }
        }
        if (request.getCookies() != null) {
            for (int i = 0; i < request.getCookies().length; i++) {
                dump += "Cookie[" + request.getCookies()[i].getName() + "] = " + request.getCookies()[i].getValue();
                dump += "\n";
            }
        }
        if (request.getRemoteAddr() != null) {
            dump += "IP :" + request.getRemoteAddr();
            dump += "\n";
        }
        initialiserUrlPageCourante(request);
        RequeteMgr.ajouterEvenement(getIdRequete(), dump);
    }

    /**
     * Gets the contexte univ.
     *
     * @param contextePage
     *            the _contexte page
     *
     * @return the contexte univ
     *
     * @throws Exception
     *             the exception
     */
    public static ContexteUniv getContexteUniv(final PageContext contextePage) throws Exception {
        // URL FRIENDLY : récupère le contexte du controleur ou l'instancie
        final HttpServletRequest request = (HttpServletRequest) contextePage.getRequest();
        ContexteUniv ctx = ContexteUtil.getContexteUniv();
        if (ctx == null) {
            ctx = initContexteUniv(request, (HttpServletResponse) contextePage.getResponse(), contextePage.getServletContext(), contextePage.getServletConfig());
            ctx.setApercu(request.getParameter("MODE") != null && "APERCU".equals(request.getParameter("MODE")));
        }
        ctx.setContextePage(contextePage);
        return ctx;
    }

    /**
     * Inits the contexte univ.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @param servletContext
     *            the servlet context
     * @param servletConfig
     *            the servlet config
     *
     * @return the contexte univ
     *
     */
    public static ContexteUniv initContexteUniv(final HttpServletRequest request, final HttpServletResponse response, final ServletContext servletContext, final ServletConfig servletConfig) {
        final ContexteUniv ctx = new ContexteUniv(request);
        ctx.setReponseHTTP(response);
        ctx.setServletContext(servletContext);
        ctx.initialiserInfosUtilisateur();
        ctx.initialiserInfosConnecteurs();
        ctx.setEspaceCourant(ctx.getEspaceHistorique());
        return ctx;
    }

    public void initialiserUrlPageCourante(final HttpServletRequest request) {
        String urlPageCourante = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(request.getRequestURI())) {
            urlPageCourante = request.getRequestURI();
        }
        if (StringUtils.isNotBlank(request.getQueryString())) {
            urlPageCourante += "?" + request.getQueryString();
        }
        setUrlPageCourante(urlPageCourante);
    }

    /**
     * getter jsp.
     *
     * @return java.lang.Object
     */
    public Object getJsp() {
        return jsp;
    }

    /**
     * setter jsp.
     *
     * @param newJsp
     *            java.lang.Object
     */
    public void setJsp(final Object newJsp) {
        jsp = newJsp;
    }

    /**
     * getter requete.
     *
     * @return java.lang.String
     */
    public String getRequete() {
        return requete;
    }

    /**
     * setter requete.
     *
     * @param newRequete
     *            java.lang.String
     */
    public void setRequete(final String newRequete) {
        requete = newRequete;
    }

    /**
     * Gets the rubrique externe connecteur.
     *
     * @return java.lang.String
     */
    public String getRubriqueExterneConnecteur() {
        return rubriqueExterneConnecteur;
    }

    /**
     * setter rubriqueNavigation.
     *
     * @param newRubriqueNavigation
     *            java.lang.String
     */
    public void setRubriqueExterneConnecteur(final String newRubriqueNavigation) {
        rubriqueExterneConnecteur = newRubriqueNavigation;
    }

    /**
     * getter autorisations.
     *
     * @return com.univ.objetspartages.om.AutorisationBean
     */
    public AutorisationBean getAutorisation() {
        return autorisation;
    }

    /**
     * setter autorisations.
     *
     * @param newAutorisation
     *            com.univ.objetspartages.om.AutorisationBean
     */
    public void setAutorisation(final AutorisationBean newAutorisation) {
        autorisation = newAutorisation;
    }

    /**
     * getter centres d'interets.
     *
     * @return java.util.Vector
     */
    public Collection<String> getCentresInteret() {
        return centresInteret;
    }

    /**
     * setter centres d'interets.
     *
     * @param newCentresInteret
     *            java.util.Vector
     */
    public void setCentresInteret(final Collection<String> newCentresInteret) {
        centresInteret = newCentresInteret;
    }

    /**
     * getter code.
     *
     * @return java.lang.String
     */
    public String getCode() {
        return code;
    }

    /**
     * setter code.
     *
     * @param newCode
     *            java.lang.String
     */
    public void setCode(final String newCode) {
        code = newCode;
    }

    /**
     * getter groupePersonnalisationCourant.
     *
     * @return java.lang.String
     */
    public String getGroupePersonnalisationCourant() {
        return groupePersonnalisationCourant;
    }

    /**
     * setter groupePersonnalisationCourant.
     *
     * @param newGroupePersonnalisationCourant
     *            java.lang.String
     */
    public void setGroupePersonnalisationCourant(final String newGroupePersonnalisationCourant) {
        groupePersonnalisationCourant = newGroupePersonnalisationCourant;
    }

    /**
     * getter groupes dsi.
     *
     * @return java.util.Vector
     */
    public TreeSet<String> getGroupesDsi() {
        return groupesDsi;
    }

    /**
     * setter groupesDsi.
     *
     * @param newGroupesDsi
     *            TreeSet
     */
    public void setGroupesDsi(final TreeSet<String> newGroupesDsi) {
        groupesDsi = newGroupesDsi;
    }

    /**
     * getter groupes dsi.
     *
     * @return java.util.Vector
     */
    public TreeSet<String> getGroupesDsiAvecAscendants() {
        return groupesDsiAvecAscendants;
    }

    /**
     * setter groupesDsi.
     *
     * @param newGroupesDsi
     *            TreeSet
     */
    public void setGroupesDsiAvecAscendants(final TreeSet<String> newGroupesDsi) {
        groupesDsiAvecAscendants = newGroupesDsi;
    }

    /**
     * Gets the liste services autorises.
     *
     * @return the liste services autorises
     */
    public TreeSet<String> getListeServicesAutorises() {
        return listeServicesAutorises;
    }

    /**
     * setter groupesDsi.
     *
     * @param newServices
     *            the new services
     */
    public void setListeServicesAutorises(final TreeSet<String> newServices) {
        listeServicesAutorises = newServices;
    }

    /**
     * getter groupes dsi.
     *
     * @return java.util.Vector
     */
    public TreeSet<String> getListeProfilsDsi() {
        return listeProfilsDsi;
    }

    /**
     * setter newProfilsDsi.
     *
     * @param newProfilsDsi
     *            the new profils dsi
     */
    public void setListeProfilsDsi(final TreeSet<String> newProfilsDsi) {
        listeProfilsDsi = newProfilsDsi;
    }

    /**
     * getter nom.
     *
     * @return java.lang.String
     */
    public String getNom() {
        return nom;
    }

    /**
     * setter nom.
     *
     * @param newNom
     *            java.lang.String
     */
    public void setNom(final String newNom) {
        nom = newNom;
    }

    /**
     * getter prenom.
     *
     * @return java.lang.String
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * setter prenom.
     *
     * @param newPrenom
     *            java.lang.String
     */
    public void setPrenom(final String newPrenom) {
        prenom = newPrenom;
    }

    /**
     * getter profil.
     *
     * @return java.lang.String
     */
    public String getProfilDsi() {
        return profilDsi;
    }

    /**
     * setter profil dsi.
     *
     * @param newProfilDsi
     *            java.lang.String
     */
    public void setProfilDsi(final String newProfilDsi) {
        profilDsi = newProfilDsi;
    }

    /**
     * getter profil.
     *
     * @return {@link com.univ.objetspartages.bean.ProfildsiBean}
     */
    public ProfildsiBean getProfilDsiBean() {
        return infosProfilDsi;
    }

    /**
     * setter profil dsi.
     *
     * @param newInfosProfilDsi
     *            the new infos profil dsi
     */
    public void setProfilDsiBean(final ProfildsiBean newInfosProfilDsi) {
        infosProfilDsi = newInfosProfilDsi;
    }

    /**
     * getter requeteHttp.
     *
     * @return javax.servlet.http.HttpServletRequest
     */
    public HttpServletRequest getRequeteHTTP() {
        return requeteHTTP;
    }

    /**
     * setter requete http.
     *
     * @param newRequeteHTTP
     *            javax.servlet.http.HttpServletRequest
     */
    public void setRequeteHTTP(final HttpServletRequest newRequeteHTTP) {
        requeteHTTP = newRequeteHTTP;
    }

    /**
     * getter requeteHttp Date de création : (15/01/2003 16:48:45).
     *
     * @return javax.servlet.http.HttpServletRequest
     */
    public HttpServletResponse getReponseHTTP() {
        return reponseHTTP;
    }

    /**
     * Sets the reponse http.
     *
     * @param reponseHTTP
     *            the new reponse http
     */
    public void setReponseHTTP(final HttpServletResponse reponseHTTP) {
        this.reponseHTTP = reponseHTTP;
    }

    /**
     * getter requeteHttp Date de création : (15/01/2003 16:48:45).
     *
     * @return javax.servlet.http.HttpServletRequest
     */
    public ServletContext getServletContext() {
        return servletContext;
    }

    /**
     * Sets the servlet context.
     *
     * @param servletContext
     *            the new servlet context
     */
    public void setServletContext(final ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /**
     * Intialisation des infos utilisateurs.
     *
     */
    public void initialiserInfosUtilisateur() {
        final HttpServletRequest newRequeteHTTP = getRequeteHTTP();
        if (newRequeteHTTP == null) {
            return;
        }
        final HttpSession _session = newRequeteHTTP.getSession(Boolean.TRUE);
        if (_session == null) {
            return;
        }
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) _session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        if (sessionUtilisateur != null) {
            final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
            if (infosSession.get("CODE") != null) {
                final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
                setCode((String) infosSession.get(SessionUtilisateur.CODE));
                setCodeStructure((String) infosSession.get(SessionUtilisateur.CODE_STRUCTURE));
                setCodeGestion((String) infosSession.get(SessionUtilisateur.CODE_GESTION));
                setNom((String) infosSession.get(SessionUtilisateur.NOM));
                setPrenom((String) infosSession.get(SessionUtilisateur.PRENOM));
                // JSS 20050510 : profil dynamique
                setProfilDsi((String) infosSession.get(SessionUtilisateur.PROFIL_DSI));
                setProfilDsiBean(serviceProfildsi.getByCode((String) infosSession.get(SessionUtilisateur.PROFIL_DSI)));
                setListeProfilsDsi(new TreeSet<>((Collection<String>) infosSession.get(SessionUtilisateur.PROFILS_DSI)));
                // JB 20050609 : recopie des infos de session des applicatifs metier
                final Map infosSessionApplicatifsMetier = (Map) infosSession.get(CLEF_INFOS_SESSION_APPLICATIFS_METIER);
                if (infosSessionApplicatifsMetier != null) {
                    getDatas().put(CLEF_INFOS_SESSION_APPLICATIFS_METIER, infosSessionApplicatifsMetier);
                }
                setCentresInteret((Collection<String>) infosSession.get(SessionUtilisateur.CENTRES_INTERET));
                setAdresseMail((String) infosSession.get(SessionUtilisateur.ADRESSE_MAIL));
                final TreeSet<String> set = new TreeSet<>();
                final Enumeration<String> e = ((Vector<String>) infosSession.get(SessionUtilisateur.GROUPES_DSI)).elements();
                while (e.hasMoreElements()) {
                    set.add(e.nextElement());
                }
                setGroupesDsi(set);
                // JSS 20050722 : gestion services
                calculerGroupesDsiAvecAscendants();
                // JSS 20040409 : ajout espaces
                setAutorisation((AutorisationBean) infosSession.get(SessionUtilisateur.AUTORISATIONS));
                /* Connecteurs Java/Php: ksession */
                setKsession((String) infosSession.get(SessionUtilisateur.KSESSION));
                //setDateDerniereSession((Date) infosSession.get(SessionUtilisateur.DATE_DERNIERE_SESSION));
                initialiserEspace();
            }
        }
        // on charge les services même si non connecté
        initialiserListeServices();
    }

    public void initialiserEspace() {
        if (requeteHTTP != null) {
            String codeEspace = requeteHTTP.getParameter("ESPACE");
            if (requeteHTTP.getAttribute("EXCLUSION_ESPACE") != null) {
                codeEspace = StringUtils.EMPTY;
            }
            final InfoBean infoBean = (InfoBean) requeteHTTP.getAttribute("infoBean");
            // initialisation de l'espace à partir d'un processus
            if (codeEspace == null && infoBean != null) {
                codeEspace = infoBean.getString("ESPACE");
            }
            if (StringUtils.isNotBlank(codeEspace) && ((infoBean != null && infoBean.get("EXCLUSION_ESPACE") == null) || (infoBean == null))) {
                setEspaceHistorique(codeEspace);
                setEspace(espaceCollaboratifDAO.getByCode(codeEspace));
                setEspaceCourant(getEspaceHistorique());
                setLocale(LangueUtil.getLocale(getEspace().getLangue()));
            }
        }
    }

    /**
     * Suppresssion des données liés à l'utilisateur courant.
     */
    public void supprimerInfosUtilisateurs() {
        setCode(StringUtils.EMPTY);
        setCodeStructure(StringUtils.EMPTY);
        setCodeGestion(StringUtils.EMPTY);
        setNom(StringUtils.EMPTY);
        setPrenom(StringUtils.EMPTY);
        setProfilDsi(StringUtils.EMPTY);
        setProfilDsiBean(null);
        setListeProfilsDsi(new TreeSet<String>());
        setListeServicesAutorises(new TreeSet<String>());
        setCentresInteret(new Vector<String>());
        setAdresseMail(StringUtils.EMPTY);
        setGroupesDsi(new TreeSet<String>());
        setGroupesDsiAvecAscendants(new TreeSet<String>());
        setKsession(StringUtils.EMPTY);
        setEspaceHistorique(StringUtils.EMPTY);
        setEspace(null);
        setAutorisation(null);
    }

    /**
     * calcul des groupes ascendants pour optimiser les controles de diffusion par la suite.
     *
     */
    public void calculerGroupesDsiAvecAscendants() {
        final TreeSet<String> listeGroupesAvecAscendants = new TreeSet<>();
        for (String groupeCourant : getGroupesDsi()) {
            listeGroupesAvecAscendants.add(groupeCourant);
            final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
            GroupeDsiBean group = serviceGroupeDsi.getByCode(groupeCourant);
            /* On recherche les items de niveau supérieur */
            final int niveauItemCourant = serviceGroupeDsi.getLevel(group);
            int niveau = niveauItemCourant - 1;
            while (niveau > 0) {
                listeGroupesAvecAscendants.add(group.getCodeGroupePere());
                group = serviceGroupeDsi.getByCode(group.getCodeGroupePere());
                niveau--;
            }
        }
        setGroupesDsiAvecAscendants(listeGroupesAvecAscendants);
    }

    /**
     * calcul des services autorises.
     *
     */
    public void initialiserListeServices() {
        setListeServicesAutorises(ServicesUtil.calculerCodesServicesAutorises(this));
    }

    /**
     * Calculer liste services push.
     *
     * @return the vector
     *
     */
    public Vector<ServiceBean> calculerListeServicesPush() {
        return ServicesUtil.calculerListeServicesPush(this);
    }

    /**
     * Permet de savoir si la dsi doit être appliquée.
     *
     * @return boolean calculListeResultatsFront
     */
    public boolean isCalculListeResultatsFront() {
        return calculListeResultatsFront;
    }

    /**
     * setter calculListeResultatsFront Permet de savoir si la dsi doit être appliquée.
     *
     * @param newAffichageListeResultats
     *            the new affichage liste resultats
     */
    public void setCalculListeResultatsFront(final boolean newAffichageListeResultats) {
        calculListeResultatsFront = newAffichageListeResultats;
    }

    /**
     * Gets the encadres externes.
     *
     * @return java.util.Vector
     */
    public List<SousParagrapheBean> getEncadresExternes() {
        return encadresExternes;
    }

    /**
     * setter encadrés externes.
     *
     * @param newEncadresExternes
     *            java.util.Vector
     */
    public void setEncadresExternes(final List<SousParagrapheBean> newEncadresExternes) {
        encadresExternes = newEncadresExternes;
    }

    /**
     * Gets the encadres recherche externes.
     *
     * @return java.util.Vector
     */
    public List<String> getEncadresRechercheExternes() {
        return encadresRechercheExternes;
    }

    /**
     * setter encadrés de recherche externes.
     *
     * @param newEncadresRechercheExternes
     *            java.util.Vector
     */
    public void setEncadresRechercheExternes(final List<String> newEncadresRechercheExternes) {
        encadresRechercheExternes = newEncadresRechercheExternes;
    }

    /**
     * Gets the ksession.
     *
     * @return java.lang.String
     */
    public String getKsession() {
        return ksession;
    }

    /**
     * setter ksession.
     *
     * @param newKsession
     *            java.lang.String
     */
    public void setKsession(final String newKsession) {
        ksession = newKsession;
    }

    /**
     * Gets the template externe.
     *
     * @return java.lang.String
     */
    public String getTemplateExterne() {
        return templateExterne;
    }

    /**
     * setter template externe.
     *
     * @param newTemplateExterne
     *            java.lang.String
     */
    public void setTemplateExterne(final String newTemplateExterne) {
        templateExterne = newTemplateExterne;
    }

    /**
     * Intialisation des infos connecteurs : ktemplate : doit toujours être renseigné à partir de l'application externe krequete : optionnel (permet de retourver les infos.
     * graphiques à intégrer ksession : doit être précisé si utilisateur connecté
     *
     */
    public void initialiserInfosConnecteurs() {
        final HttpServletRequest request = getRequeteHTTP();
        if (request != null) {
            final String ktemplate = request.getParameter("ktemplate");
            if (StringUtils.isNotEmpty(ktemplate)) {
                setTemplateExterne(ktemplate);
                final String kSession = request.getParameter("ksession");
                if (StringUtils.isNotEmpty(kSession)) {
                    setKsession(kSession);
                }
                // L'appel vient d'un programme externe: on lit le fichier XML posté.
                RequeteConnecteur.parseFlux(this);
                // JSS 20050607 : ajout des infos utilisateurs dans le contexte
                // pour cela il est nécessaire de lire le code
                if (kSession.length() > 0) {
                    try {
                        final SSOBean ssoBean = RequeteConnecteur.lireBeanSSO(ksession);
                        if (ssoBean.getCodeKportal().length() > 0) {
                            GestionnaireIdentification.getInstance().chargerContexteProxyClient(this, ssoBean.getCodeKportal());
                        }
                    } catch (final Exception e) {
                        LOGGER.error("Erreur à la lecture des informations du bean SSO", e);
                    }
                }
            }
        }
    }

    /**
     * getter adresse mail.
     *
     * @return java.lang.String
     */
    public String getAdresseMail() {
        return adresseMail;
    }

    /**
     * setter adresse mail.
     *
     * @param newAdresseMail
     *            java.lang.String
     */
    public void setAdresseMail(final String newAdresseMail) {
        adresseMail = newAdresseMail;
    }

    /**
     * Checks if is generer liens internes.
     *
     * @return Returns the genererLiensInternes.
     */
    public boolean isGenererLiensInternes() {
        return genererLiensInternes;
    }

    /**
     * Sets the generer liens internes.
     *
     * @param genererLiensInternes
     *            The genererLiensInternes to set.
     */
    public void setGenererLiensInternes(final boolean genererLiensInternes) {
        this.genererLiensInternes = genererLiensInternes;
    }

    /**
     * Gets the codes liens internes.
     *
     * @return Returns the codesLiensInternes.
     */
    public Vector<String> getCodesLiensInternes() {
        return codesLiensInternes;
    }

    /**
     * Sets the codes liens internes.
     *
     * @param codesLiensInternes
     *            The codesLiensInternes to set.
     */
    public void setCodesLiensInternes(final Vector<String> codesLiensInternes) {
        this.codesLiensInternes = codesLiensInternes;
    }

    /**
     * Gets the code gestion.
     *
     * @return Returns the codeGestion.
     */
    public String getCodeGestion() {
        return codeGestion;
    }

    /**
     * Sets the code gestion.
     *
     * @param codeGestion
     *            The codeGestion to set.
     */
    public void setCodeGestion(final String codeGestion) {
        this.codeGestion = codeGestion;
    }

    /**
     * Gets the code structure.
     *
     * @return Returns the codeStructure.
     */
    public String getCodeStructure() {
        return codeStructure;
    }

    /**
     * Sets the code structure.
     *
     * @param codeStructure
     *            The codeStructure to set.
     */
    public void setCodeStructure(final String codeStructure) {
        this.codeStructure = codeStructure;
    }

    /**
     * Gets the espace courant.
     *
     * @return Returns the espaceCourant.
     */
    //TODO FBI : @Deprecated
    public String getEspaceCourant() {
        return espaceCourant;
    }

    /**
     * Sets the espace courant.
     *
     * @param espaceCourant
     *            The espaceCourant to set.
     */
    //TODO FBI : @Deprecated
    public void setEspaceCourant(final String espaceCourant) {
        this.espaceCourant = espaceCourant;
    }

    /**
     * Gets the liste espaces.
     *
     * @return Returns the listeEspaces.
     * @deprecated Méthode plus utilisée, à supprimer
     */
    @Deprecated
    public TreeSet<String> getListeEspaces() {
        return listeEspaces;
    }

    /**
     * Sets the liste espaces.
     *
     * @param listeEspaces
     *            The listeEspaces to set.
     * @deprecated Méthode plus utilisée, à supprimer
     */
    @Deprecated
    public void setListeEspaces(final TreeSet<String> listeEspaces) {
        this.listeEspaces = listeEspaces;
    }

    /**
     * Gets the espace personnalisation courant.
     *
     * @return Returns the espacePersonnalisationCourant.
     */
    // TODO FBI : @Deprecated
    public String getEspacePersonnalisationCourant() {
        return espacePersonnalisationCourant;
    }

    /**
     * Sets the espace personnalisation courant.
     *
     * @param espacePersonnalisationCourant
     *            The espacePersonnalisationCourant to set.
     */
    // TODO FBI : @Deprecated
    public void setEspacePersonnalisationCourant(final String espacePersonnalisationCourant) {
        this.espacePersonnalisationCourant = espacePersonnalisationCourant;
    }

    /**
     * Gets the infos espace.
     *
     * @return Returns the infosEspace.
     */
    // TODO FBI : @Deprecated
    public EspaceCollaboratifBean getEspace() {
        return espace;
    }

    /**
     * Sets the infos espace.
     *
     * @param espace
     *            The infosEspace to set.
     */
    // TODO FBI : @Deprecated
    public void setEspace(final EspaceCollaboratifBean espace) {
        this.espace = espace;
    }

    /**
     * Gets the code rubrique page courante.
     *
     * @return Returns the codeRubriquePageCourante.
     */
    public String getCodeRubriquePageCourante() {
        return codeRubriquePageCourante;
    }

    /**
     * Sets the code rubrique page courante.
     *
     * @param codeRubriquePageCourante
     *            The codeRubriquePageCourante to set.
     */
    public void setCodeRubriquePageCourante(final String codeRubriquePageCourante) {
        this.codeRubriquePageCourante = codeRubriquePageCourante;
    }

    /**
     * Calculer code rubrique page courante.
     *
     */
    public void calculerCodeRubriquePageCourante() {
        String code = getCodeRubriquePageCourante();
        // on peut être dans un cas où la rubrique courante a été mise à inexistante lors de la génération
        // de liens url friendly (par ex. via UnivWebFmt.formaterEnHtml) dans la page (par FrontOfficeMgr.getRubriquePublication),
        // et qui sont appelés avant l'appel à cette méthode calculerCodeRubriquePageCourante
        // cette méthode ne gère pas les contrôles d'accès mais juste de l'affichage (les contrôles sont faits dans lireFiche),
        // on peut donc se permettre de supprimer ce test pour recalculer la bonne rubrique correspondant vraiment à la fiche courante
        if (!(getCodeRubriqueFicheCourante().equals(StringUtils.EMPTY))) {
            code = getCodeRubriqueFicheCourante();
        } else if (getRequeteHTTP() != null && getRequeteHTTP().getParameter("RF") != null) {
            code = getRequeteHTTP().getParameter("RF");
        } else if (getRequete() != null) {
            if (getRequete().contains("OBJET=")) {
                final String objet = RequeteUtil.renvoyerParametre(getRequete(), "OBJET");
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                final RubriqueBean infosRubriques = serviceRubrique.getRubriqueByCode(objet);
                if (infosRubriques != null && infosRubriques.getCode().length() > 0) {
                    code = infosRubriques.getCode();
                }
            }
        }
        // Rubrique des Connecteurs
        if (getTemplateExterne() != null) {
            if (getTemplateExterne().length() > 0) {
                code = getRubriqueExterneConnecteur();
            }
        }
        //}
        // rubrique historique
        if (StringUtils.isEmpty(code)) {
            code = getCodeRubriqueHistorique();
        }
        // test pour le site courant
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        if (StringUtils.EMPTY.equals(code) || !getInfosSite().isRubriqueVisibleInSite(serviceRubrique.getRubriqueByCode(code))) {
            code = getInfosSite().getCodeRubrique();
        }
        setCodeRubriquePageCourante(code);
    }

    /**
     * Gets the code rubrique historique.
     *
     * @return Returns the codeRubriqueHistorique.
     */
    public String getCodeRubriqueHistorique() {
        return codeRubriqueHistorique;
    }

    /**
     * Gets the code rubrique historique.
     *
     */
    public void setCodeRubriqueHistorique(final String codeRubriqueHistorique) {
        this.codeRubriqueHistorique = codeRubriqueHistorique;
    }

    /**
     * Gets the espace historique.
     *
     * @return Returns the espaceHistorique.
     */
    // TODO FBI : @Deprecated
    public String getEspaceHistorique() {
        return espaceHistorique;
    }

    /**
     * Sets the espace historique.
     *
     * @param espaceHistorique
     *            The espaceHistorique to set.
     */
    // TODO FBI : @Deprecated
    public void setEspaceHistorique(final String espaceHistorique) {
        this.espaceHistorique = espaceHistorique;
    }

    /**
     * Gets the service courant.
     *
     * @return the service courant
     */
    public String getServiceCourant() {
        return serviceCourant;
    }

    /**
     * Sets the service courant.
     *
     * @param serviceCourant
     *            the new service courant
     */
    public void setServiceCourant(final String serviceCourant) {
        this.serviceCourant = serviceCourant;
    }

    /**
     * Gets the fiche courante.
     *
     * @return Returns the ficheCourante.
     */
    public FicheUniv getFicheCourante() {
        return ficheCourante;
    }

    /**
     * Sets the fiche courante.
     *
     * @param ficheCourante
     *            The ficheCourante to set.
     */
    public void setFicheCourante(final FicheUniv ficheCourante) {
        this.ficheCourante = ficheCourante;
    }

    /**
     * Gets the date derniere session.
     *
     * @return Returns the dateDerniereSession.
     */
    public Date getDateDerniereSession() {
        return dateDerniereSession;
    }

    /**
     * Sets the date derniere session.
     *
     * @param dateDerniereSession
     *            The dateDerniereSession to set.
     */
    public void setDateDerniereSession(final Date dateDerniereSession) {
        this.dateDerniereSession = dateDerniereSession;
    }

    /**
     * Gets the liste espaces dont visiteur.
     *
     * @return the liste espaces dont visiteur
     */
    @Deprecated
    public Vector<String> getListeEspacesDontVisiteur() {
        return listeEspacesDontVisiteur;
    }

    /**
     * Sets the liste espaces dont visiteur.
     *
     * @param listeEspacesDontVisiteur
     *            the new liste espaces dont visiteur
     * @deprecated Méthode plus utilisée, à supprimer
     */
    @Deprecated
    public void setListeEspacesDontVisiteur(final Vector<String> listeEspacesDontVisiteur) {
        this.listeEspacesDontVisiteur = listeEspacesDontVisiteur;
    }

    /**
     * Gets the code rubrique fiche courante.
     *
     * @return the code rubrique fiche courante
     */
    public String getCodeRubriqueFicheCourante() {
        return codeRubriqueFicheCourante;
    }

    /**
     * Sets the code rubrique fiche courante.
     *
     * @param codeRubriqueFicheCourante
     *            the new code rubrique fiche courante
     */
    public void setCodeRubriqueFicheCourante(final String codeRubriqueFicheCourante) {
        this.codeRubriqueFicheCourante = codeRubriqueFicheCourante;
    }

    /**
     * Gets the donnees specifiques.
     *
     * @return the donnees specifiques
     */
    public Map<String, String> getDonneesSpecifiques() {
        return donneesSpecifiques;
    }

    /**
     * Sets the donnees specifiques.
     *
     * @param donneesSpecifiques
     *            the new donnees specifiques
     */
    public void setDonneesSpecifiques(final Map<String, String> donneesSpecifiques) {
        this.donneesSpecifiques = donneesSpecifiques;
    }

    /**
     * Checks if is apercu.
     *
     * @return true, if is apercu
     */
    public boolean isApercu() {
        return apercu;
    }

    /**
     * Sets the apercu.
     *
     * @param apercu
     *            the new apercu
     */
    public void setApercu(final boolean apercu) {
        this.apercu = apercu;
    }

    /**
     * Sets the contexte page.
     *
     * @param contextePage
     *            the new contexte page
     */
    public void setContextePage(final PageContext contextePage) {}

    /**
     * Gets the meta courant.
     *
     * @return the meta courant
     */
    public MetatagBean getMetaCourant() {
        return metaCourant;
    }

    /**
     * Sets the meta courant.
     *
     * @param metaCourant
     *            the new meta courant
     */
    public void setMetaCourant(final MetatagBean metaCourant) {
        this.metaCourant = metaCourant;
    }

    public JspWriter getJspWriter() {
        return jspWriter;
    }

    public void setJspWriter(final JspWriter jspWriter) {
        this.jspWriter = jspWriter;
    }

    public String getUrlPageCourante() {
        return urlPageCourante;
    }

    public void setUrlPageCourante(final String urlPageCourante) {
        this.urlPageCourante = urlPageCourante;
    }
}
