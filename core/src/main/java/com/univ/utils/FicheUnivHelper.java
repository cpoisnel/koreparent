package com.univ.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.CodeLibelle;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.util.MetatagUtils;

public class FicheUnivHelper {

    private static final Logger LOG = LoggerFactory.getLogger(FicheUnivHelper.class);

    /**
     * Histoire de centraliser l'instanciation de fiche, permet d'instancier une fiche en fonction de son nom ou de son code
     *
     * @param codeOrNom le code du type de fiche ou son nom (déclaré dans Spring)
     * @return la ficheUniv à instancier ou null si non trouvée
     */
    public static FicheUniv instancierFiche(final String codeOrNom) {
        return ReferentielObjets.instancierFiche(codeOrNom);
    }

    /**
     * Renvoie la fiche demandee (methode statique utilisee pour les jointures entre fiches).
     * @param codeOrNom le code du type de fiche ou son nom (déclaré dans Spring) exemple pour article code = 0015 et nom = article
     * @param codeFiche le code de la fiche à récupérer (à ne pas confondre avec le code du type)
     * @param langue la langue dans laquelle on souhaite récupérer la fiche
     * @return si le type de fiche n'est pas trouvé cela renverra null sinon, il fera une requête SQL et il faudra faire
     * la vérification après que la fiche a bien été trouvé. Deplus, cette méthode est historique et après avoir regarder sur
     * les fiches en lignes, elle regarde sans spécifier d'état. Attention donc à son utilisation!!
     * @throws Exception lors des requêtes SQL
     */
    public static FicheUniv getFiche(final String codeOrNom, final String codeFiche, String langue) throws Exception {
        FicheUniv fiche = null;
        if (StringUtils.isNotEmpty(codeOrNom) && StringUtils.isNotEmpty(codeFiche)) {
            fiche = ReferentielObjets.instancierFiche(codeOrNom);
            if (fiche != null) {
                if (StringUtils.isEmpty(langue)) {
                    langue = "0"; // francais par defaut
                }
                try (final ContexteDao ctx = new ContexteDao()) {
                    fiche.init();
                    fiche.setCtx(ctx);
                    int count = fiche.selectCodeLangueEtat(codeFiche, langue, "0003");
                    if (count == 0) // si pas de version en ligne
                    {
                        count = fiche.selectCodeLangueEtat(codeFiche, langue, "");
                    }
                    if (count == 0) // si pas de version dans la langue demandee
                    {
                        count = fiche.selectCodeLangueEtat(codeFiche, "", "0003");
                    }
                    if (count > 0) {
                        fiche.nextItem();
                    }
                }
            }
        }
        return fiche;
    }

    /**
     * Méthode retournant une ficheUniv pour le front office en réalisant les contrôles d'accès avec les données du contexte fourni en paramètre
     * @param ctx le contexte de la requête pour gérer les droits
     * @param codeOrNom le code du type de fiche ou son nom (déclaré dans Spring) exemple pour article code = 0015 et nom = article
     * @param codeFiche le code de la fiche à récupérer (à ne pas confondre avec le code du type)
     * @param langue la langue dans laquelle on souhaite récupérer la fiche
     * @param controleRubrique doit on contrôler les droits sur les rubriques de la fiche
     * @return la fiche récupérer ou null si le code du type d'objet n'est pas correct. Attention même problème que la pluspart du temps sur les fiches
     * si la fiche n'est pas trouvé par la requête SQL (vu qu'on passe par nextItem... l'objet est retourné même si il n'est pas remplis...
     * @throws ExceptionFicheNonTrouvee si la fiche n'est pas trouvée
     * @throws ExceptionFicheNonAccessible si la fiche n'est pas accessible pour l'utilisateur courant
     */
    public static FicheUniv getFicheFrontOffice(ContexteUniv ctx, final String codeOrNom, final String codeFiche, String langue, final boolean controleRubrique) throws ExceptionFicheNonTrouvee, ExceptionFicheNonAccessible {
        FicheUniv fiche = null;
        if (StringUtils.isNotEmpty(codeOrNom) && StringUtils.isNotEmpty(codeFiche)) {
            fiche = ReferentielObjets.instancierFiche(codeOrNom);
            if (fiche != null) {
                langue = StringUtils.defaultString(langue, "0");
                try (final ContexteDao ctxDao = new ContexteDao()) {
                    fiche.init();
                    fiche.setCtx(ctxDao);
                    boolean isVisible = Boolean.TRUE;
                    try {
                        int count = fiche.selectCodeLangueEtat(codeFiche, langue, EtatFiche.EN_LIGNE.getEtat());
                        if (count > 0) {
                            fiche.nextItem();
                        }
                        isVisible = FicheUnivMgr.controlerRestriction(fiche, ctx, controleRubrique);
                    } catch (Exception e) {
                        throw new ExceptionFicheNonTrouvee(MessageHelper.getCoreMessage("ST_FICHE_INEXISTANTE"), e);
                    }
                    if (!isVisible) {
                        throw new ExceptionFicheNonAccessible(MessageHelper.getCoreMessage("ST_FICHE_INACCESSIBLE"));
                    }
                }
            }
        }
        return fiche;
    }

    /**
     * A la différence de {@link FicheUnivHelper#getFicheFrontOffice(ContexteUniv, String, String, String, boolean)} cette méthode catch les exceptions et renvoie null
     * pour éviter de lever une exception si elle est appelé dans une JSP.
     * @param ctx le contexte de la requête pour gérer les droits
     * @param codeOrNom le code du type de fiche ou son nom (déclaré dans Spring) exemple pour article code = 0015 et nom = article
     * @param codeFiche le code de la fiche à récupérer (à ne pas confondre avec le code du type)
     * @param langue la langue dans laquelle on souhaite récupérer la fiche
     * @param controleRubrique doit on contrôler les droits sur les rubriques de la fiche
     * @return la fiche récupérer ou null si le code du type d'objet n'est pas correct. Attention même problème que la pluspart du temps sur les fiches
     * si la fiche n'est pas trouvé par la requête SQL (vu qu'on passe par nextItem... l'objet est retourné même si il n'est pas remplis...
     */
    public static FicheUniv getFicheFrontOfficePourJSP(ContexteUniv ctx, final String codeOrNom, final String codeFiche, String langue, final boolean controleRubrique) {
        FicheUniv fiche;
        try {
            fiche = getFicheFrontOffice(ctx, codeOrNom, codeFiche, langue, controleRubrique);
        } catch (ExceptionFicheNonTrouvee | ExceptionFicheNonAccessible e) {
            LOG.debug("unable to find the content", e);
            fiche = null;
        }
        return fiche;
    }

    /**
     * Retourne une fiche à partir de son id de metatag.
     *
     * @param idMeta l'id du metatag de la fiche à récupérer
     * @return Si il n'est pas possible de retrouver le type de fiche à partir du meta, cela renvoi null. Sinon
     * cela renvoie la fiche
     * @throws Exception lors des requêtes SQL : sur la connexion à la bdd ou si le meta ou la fiche ne sont pas retrouvés depuis leurs ids
     */
    public static FicheUniv getFicheParIdMeta(final Long idMeta) throws Exception {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final MetatagBean meta = serviceMetatag.getById(idMeta);
        if (meta != null) {
            try (final ContexteDao ctxConnection = new ContexteDao()) {
                final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(meta.getMetaCodeObjet()));
                if (ficheUniv != null) {
                    ficheUniv.setCtx(ctxConnection);
                    ficheUniv.init();
                    ficheUniv.setIdFiche(meta.getMetaIdFiche());
                    ficheUniv.retrieve();
                }
                return ficheUniv;
            }
        }
        return null;
    }

    /**
     * Méthode historique renvoyant une map contenant le code des fiches et leurs libellés pour les éléments selects
     * @param codeOrNom le code du type de fiche ou son nom (déclaré dans Spring) exemple pour article code = 0015 et nom = article
     * @param etat l'état dans lequel on souhaite récupérer les fiches (en ligne...)
     * @param langue la langue dans laquelle on souhaite récupérer les fiches (0 = lange par défaut...)
     * @return Une map vide si l'état n'est pas précisé ou si aucune fiche n'est retrouvé
     * @throws Exception lors des requetes SQL...
     */
    public static Map<String, String> getListeFiche(final String codeOrNom, final String etat, String langue) throws Exception {
        final HashMap<String, String> res = new HashMap<>();
        if (etat == null) {
            return res;
        }
        if (StringUtils.isNotEmpty(codeOrNom) && StringUtils.isNotEmpty(codeOrNom)) {
            FicheUniv fiche = ReferentielObjets.instancierFiche(codeOrNom);
            if (fiche != null) {
                if (StringUtils.isEmpty(langue)) {
                    langue = "0"; // francais par defaut
                }
                // On cherche d'abord la version en ligne puis les autres versions
                final int count = fiche.selectCodeLangueEtat("", langue, etat);
                if (count > 0) {
                    while (fiche.nextItem()) {
                        res.put(fiche.getCode(), fiche.getLibelleAffichable());
                    }
                }
            }
        }
        return res;
    }

    /**
     * Permet de retrouver la valeur d'un libellé à partir de sa fiche, de l'id du .dat et de son code
     * @param fiche la fiche permet de récupérer l'extension concerné par le libellé en question et de savoir dans quel fichier aller chercher
     * @param nomTable le nom du fichier dat surlequel on doit aller chercher le label
     * @param codeLibelle le code du libellé à aller chercher
     * @return Le libellé associé ou une chaine vide si non trouvé.
     */
    public static String getLibelle(final FicheUniv fiche, final String nomTable, final String codeLibelle) {
        return CodeLibelle.lireLibelle(ReferentielObjets.getExtension(fiche), nomTable, LangueUtil.getLocale(Integer.parseInt(fiche.getLangue())), codeLibelle);
    }

    /**
     * Permet de supprimer un lot de fiche à partir de leur id meta.
     * @param idsMetas l'ensemble des fiches à supprimer
     * @param autorisations les autorisations de l'utilisateur souhaitant réaliser l'opération
     * @throws Exception Lors de l'accés à la BDD ou si les meta/fiches ne sont pas retrouvés à partir de leur id...
     */
    public static void supprimerListeFichesParIdsMeta(final Collection<Long> idsMetas, final AutorisationBean autorisations) throws Exception {
        if (idsMetas != null && !idsMetas.isEmpty() && autorisations != null) {
            final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            for (final Long idMeta : idsMetas) {
                final MetatagBean meta = serviceMetatag.getById(idMeta);
                if (meta != null) {
                    final FicheUniv fiche = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(meta.getMetaCodeObjet()));
                    fiche.setCtx(ctx);
                    fiche.setIdFiche(meta.getMetaIdFiche());
                    fiche.retrieve();
                    if (autorisations.estAutoriseASupprimerLaFiche(fiche)) {
                        fiche.setEtatObjet(EtatFiche.A_SUPPRIMER.getEtat());
                        fiche.update();
                        serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_SUPPRESSION, ctx.getCode(), EtatFiche.A_SUPPRIMER.getEtat());
                        serviceMetatag.synchroniser(meta, fiche, true);
                        serviceMetatag.save(meta);
                        PluginFicheHelper.synchroniserObjets(fiche, meta, null);
                    }
                }
            }
        }
    }

    /**
     * Permet d'archiver un lot de fiche à partir de leur id meta.
     * @param idsMetas l'ensemble des fiches à archiver
     * @param autorisations les autorisations de l'utilisateur souhaitant réaliser l'opération
     * @throws Exception Lors de l'accés à la BDD ou si les meta/fiches ne sont pas retrouvés à partir de leur id...
     */
    public static void archiverListeFichesParIdsMeta(final Collection<Long> idsMetas, final AutorisationBean autorisations) throws Exception {
        if (idsMetas != null && !idsMetas.isEmpty() && autorisations != null) {
            final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            for (final Long idMeta : idsMetas) {
                final MetatagBean meta = serviceMetatag.getById(idMeta);
                if (meta != null) {
                    final FicheUniv fiche = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(meta.getMetaCodeObjet()));
                    fiche.setCtx(ctx);
                    fiche.setIdFiche(meta.getMetaIdFiche());
                    fiche.retrieve();
                    if (autorisations.estAutoriseAModifierLaFiche(fiche)) {
                        fiche.setEtatObjet(EtatFiche.ARCHIVE.getEtat());
                        fiche.update();
                        meta.setMetaDateArchivage(null);
                        serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_ARCHIVAGE, ctx.getCode(), EtatFiche.ARCHIVE.getEtat());
                        serviceMetatag.synchroniser(meta, fiche, true);
                        serviceMetatag.save(meta);
                        PluginFicheHelper.synchroniserObjets(fiche, meta, null);
                    }
                }
            }
        }
    }

    /**
     * TODO : a revoir car il y a trop de traitement spécifique lors de la mise en ligne
     *
     * Permet de mettre en ligne un lot de fiche à partir de leur id meta.
     * @param idsMetas l'ensemble des fiches à mettre en ligne
     * @param autorisations les autorisations de l'utilisateur souhaitant réaliser l'opération
     * @throws Exception Lors de l'accés à la BDD ou si les meta/fiches ne sont pas retrouvés à partir de leur id...
     */
    public static void mettreEnLigneListeFichesParIdsMeta(final Collection<Long> idsMetas, final AutorisationBean autorisations) throws Exception {
        if (idsMetas != null && !idsMetas.isEmpty() && autorisations != null) {
            final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            for (final Long idMeta : idsMetas) {
                final MetatagBean meta = serviceMetatag.getById(idMeta);
                if (meta != null) {
                    final FicheUniv fiche = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(meta.getMetaCodeObjet()));
                    fiche.setCtx(ctx);
                    fiche.setIdFiche(meta.getMetaIdFiche());
                    fiche.retrieve();
                    if (autorisations.estAutoriseAModifierLaFiche(fiche)) {
                        fiche.setEtatObjet(EtatFiche.EN_LIGNE.getEtat());
                        fiche.update();
                        meta.setMetaDateMiseEnLigne(null);
                        serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_VALIDATION, ctx.getCode(), EtatFiche.EN_LIGNE.getEtat());
                        serviceMetatag.synchroniser(meta, fiche, true);
                        serviceMetatag.save(meta);
                        PluginFicheHelper.synchroniserObjets(fiche, meta, null);
                    }
                }
            }
        }
    }

    /**
     * Renvoit la liste des états objet avec le libellé normalisé pour être utilisé facilement dans l'ihm. CAD : pas d'accents, pas d'espace, pas de majuscule.
     *
     * @return la liste des états
     */
    public static Map<String, String> getClassEtatsObjet() {
        final Map<String, String> libellesParEtat = ReferentielObjets.getEtatsObjet();
        final Map<String, String> classParEtat = new HashMap<>();
        for (final String codeLibelle : libellesParEtat.keySet()) {
            classParEtat.put(codeLibelle, getClassEtat(codeLibelle));
        }
        return classParEtat;
    }

    /**
     * Renvoie la class css de l'état depuis son code
     *
     * @param etat l'état dont on souhaite connaitre la classe css
     * @return la classe CSS correspondante ou null si non trouvé
     */
    public static String getClassEtat(final String etat) {
        return PropertyHelper.getCoreProperty("ETATFICHE_" + etat);
    }
}
