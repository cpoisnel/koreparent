/*
 *
 */
package com.univ.utils;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.regex.Matcher;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class HTMLtoTexteTransformer.
 *
 * @author malice
 */
public class HTMLtoTexteTransformer {
    // tag ouvert

    /** The Constant REGEXP_HTML_TAG. */
    private static final String REGEXP_HTML_TAG = "<([a-zA-Z0-9]+)[^>]*>";
    // tag fermé

    /** The Constant REGEXP_HTML_TAG. */
    private static final String REGEXP_HTML_END_TAG = "</([a-zA-Z0-9]+)>";
    //lien (on le recupere dans le texte)

    /** The Constant REGEXP_HTML_LINK. */
    private static final String REGEXP_HTML_LINK = "<[aA].*?[Hh][Rr][Ee][Ff]=\"?([a-zA-Z0-9\\:\\.\\/_\\-@,&;=\\?%$]*)\"?.*?>(.*?)</[aA]>";

    /**
     * Instantiates a new hTM lto texte transformer.
     */
    private HTMLtoTexteTransformer() {}

    /**
     * Transformer html.
     *
     * @param _texteHtml
     *            the _texte html
     * @param apercu
     *            the apercu
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public static String transformerHtml(final String _texteHtml, final boolean apercu) throws Exception {
        if ("".equals(_texteHtml.trim())) {
            return _texteHtml;
        }
        String newTexte = _texteHtml;
        //on annule le remplacement du br car les \n ne sont pas interprétés par les clients mails
        final String cr = "<br />";
        StringBuffer sb = new StringBuffer();
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(REGEXP_HTML_LINK);
        Matcher m = p.matcher(newTexte);
        while (m.find()) {
            final String text = m.group(2) + " : " + m.group(1);
            m.appendReplacement(sb, Matcher.quoteReplacement(text));
        }
        m.appendTail(sb);
        newTexte = sb.toString();
        p = java.util.regex.Pattern.compile(REGEXP_HTML_TAG);
        m = p.matcher(newTexte);
        sb = new StringBuffer();
        while (m.find()) {
            final String tag = m.group(1);
            if ("br".equalsIgnoreCase(tag)) {
                m.appendReplacement(sb, "\n");
            } else {
                m.appendReplacement(sb, "");
            }
        }
        m.appendTail(sb);
        newTexte = sb.toString();
        p = java.util.regex.Pattern.compile(REGEXP_HTML_END_TAG);
        m = p.matcher(newTexte);
        sb = new StringBuffer();
        while (m.find()) {
            final String tag = m.group(1);
            if ("li".equalsIgnoreCase(tag) || "div".equalsIgnoreCase(tag) || "p".equalsIgnoreCase(tag) || "h".equalsIgnoreCase(tag)) {
                m.appendReplacement(sb, "\n");
            } else {
                m.appendReplacement(sb, "");
            }
        }
        m.appendTail(sb);
        newTexte = sb.toString();
        final BufferedReader br = new BufferedReader(new StringReader(newTexte));
        String line = null;
        sb = new StringBuffer();
        while ((line = br.readLine()) != null) {
            if (!StringUtils.isEmpty(line)) {
                sb.append(line).append(cr);
            }
        }
        newTexte = sb.toString();
        newTexte = newTexte.replaceAll("&rsquo;", "&#39;");
        newTexte = newTexte.replaceAll("&oelig;", "oe");
        newTexte = newTexte.replaceAll("&euro;", "e");
        return EscapeString.unescapeHtml(newTexte);
    }
}
