package com.univ.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;

/**
 * Classe utile pour le traitement sur les chaînes de caractere Date de création : (05/02/2002 17:47:26).
 */
public class Chaine {

    private static final Logger LOG = LoggerFactory.getLogger(Chaine.class);

    final static Pattern linkPattern = Pattern.compile("(?i)((?:(http[s]?)?://|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:\'\".,<>?«»“”‘’]))");

    final static Pattern emailPattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*)@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

    /**
     * Décompose une chaine en vecteur en se basant sur un séparateur
     *
     * @param separateur
     *            the separateur
     * @param chaine
     *            the chaine
     *
     * @return the vecteur separateur
     */
    public static Vector<String> getVecteurSeparateur(final String separateur, final String chaine) {
        final Vector<String> v = new Vector<>();
        if (chaine != null) {
            final StringTokenizer st = new StringTokenizer(chaine, separateur);
            while (st.hasMoreTokens()) {
                v.add(st.nextToken());
            }
        }
        return v;
    }

    /**
     * Décompose une chaine en vecteur en se basant sur le séparateur ; (Point Virgule)
     *
     * @param chaine
     *            the chaine
     *
     * @return the vecteur points virgules
     */
    public static Vector<String> getVecteurPointsVirgules(final String chaine) {
        final Vector<String> v = new Vector<>();
        if (chaine != null) {
            final StringTokenizer st = new StringTokenizer(chaine, ";");
            while (st.hasMoreTokens()) {
                v.add(st.nextToken());
            }
        }
        return v;
    }

    /**
     * Convertir virgules en accolades.
     *
     * @param chaine
     *            the chaine
     *
     * @return the string
     */
    public static String convertirVirgulesEnAccolades(final String chaine) {
        final StringBuilder res = new StringBuilder();
        if (StringUtils.isNotEmpty(chaine)) {
            final String[] temp = chaine.split(",");
            for (final String element : temp) {
                res.append("[").append(element).append("]");
            }
        }
        return res.toString();
    }

    /**
     * Convertir points virgules en accolades.
     *
     * @param chaine
     *            the chaine
     *
     * @return the string
     */
    public static String convertirPointsVirgulesEnAccolades(final String chaine) {
        final StringBuilder res = new StringBuilder();
        if (StringUtils.isNotEmpty(chaine)) {
            final String[] temp = chaine.split(";");
            for (final String element : temp) {
                res.append("[").append(element).append("]");
            }
        }
        return res.toString();
    }

    /**
     * Gets the hash set points virgules.
     *
     * @param chaine
     *            the chaine
     *
     * @return the hash set points virgules
     */
    public static Set<String> getHashSetPointsVirgules(final String chaine) {
        final Set<String> v = new HashSet<>();
        final StringTokenizer st = new StringTokenizer(chaine, ";");
        while (st.hasMoreTokens()) {
            final String val = st.nextToken();
            v.add(val);
        }
        return v;
    }

    /**
     * Gets the hash set accolades.
     *
     * @param chaine
     *            the chaine
     *
     * @return the hash set accolades
     */
    public static Set<String> getHashSetAccolades(final String chaine) {
        final Set<String> v = new HashSet<>();
        if (StringUtils.isNotEmpty(chaine)) {
            final StringTokenizer st = new StringTokenizer(chaine, "[]");
            while (st.hasMoreTokens()) {
                final String val = st.nextToken();
                v.add(val);
            }
        }
        return v;
    }

    /**
     * Construit une chaine à partir d'un vecteur en concaténant les éléments
     *
     * @param v
     *            the v
     *
     * @return the string
     */
    public static String convertirPointsVirgules(final Collection<String> v) {
        return StringUtils.join(v, ";");
    }

    /**
     * Construit une chaine à partir d'un vecteur en concaténant les éléments
     *
     * @param v
     *            the v
     *
     * @return the string
     */
    public static String convertirPointsVirgules(final Set<String> v) {
        return StringUtils.join(v, ";");
    }

    /**
     * Décompose une chaine en vecteur en se basant sur le séparateur ; (Point Virgule)
     *
     * @param chaine
     *            the chaine
     *
     * @return the vecteur accolades
     */
    public static Vector<String> getVecteurAccolades(final String chaine) {
        final Vector<String> v = new Vector<>();
        if (StringUtils.isNotEmpty(chaine)) {
            final StringTokenizer st = new StringTokenizer(chaine, "[]");
            while (st.hasMoreTokens()) {
                final String val = st.nextToken();
                v.add(val);
            }
        }
        return v;
    }

    /**
     * Convertir accolades.
     *
     * @param v
     *            the v
     *
     * @return the string
     */
    public static String convertirAccolades(final Collection<String> v) {
        String res = StringUtils.EMPTY;
        for (String aV : v) {
            res += "[" + aV + "]";
        }
        return res;
    }

    /**
     * Transformation des mails et liens en code html.
     *
     * @param _chaine
     *            : la chaine à transformer
     *
     * @return String : la chaine transformée
     */
    public static String transformeEnHtml(String _chaine) {
        final Matcher matcherEmail = emailPattern.matcher(_chaine);
        _chaine = matcherEmail.replaceAll("<a href=\"mailto:$0\">$0</a>");
        final Matcher matcherLink = linkPattern.matcher(_chaine);
        _chaine = matcherLink.replaceAll("<a href=\"$1\" onclick=\"javascript:window.open(this.href);return false;\">$1</a>");
        return _chaine;
    }

    /**
     * @deprecated {@link StringUtils#replace} Remplacement d'une chaine par une autre.
     * @param _tag
     *            la chaine sur laquelle on veut appliquer les transformations
     * @param _chaine1
     *            la chaine recherchée
     * @param _chaine2
     *            la chaine qui doit remplacer l'autre
     *
     * @return String la chaine après traitement
     */
    @Deprecated
    public static String remplacerChaine1parChaine2(final String _tag, final String _chaine1, final String _chaine2) {
        String tmp = "";
        int indexDebutTag = -1;
        int indexFinTag = 0;
        while ((indexDebutTag = _tag.indexOf(_chaine1, indexFinTag)) != -1) {
            tmp = tmp + _tag.substring(indexFinTag, indexDebutTag) + _chaine2;
            indexFinTag = indexDebutTag + _chaine1.length();
        }
        tmp = tmp + _tag.substring(indexFinTag);
        return tmp;
    }

    /**
     * Remplacement dans l'adresse mail des espaces, majuscules et accents. Est utilisé pour créer des adresses mails pour contactoffice a partir du nom et prenom
     *
     * @param _adresse
     *            the _adresse
     *
     * @return String l'adresse prete
     */
    public static String traiterEmail(final String _adresse) {
        if (_adresse == null) {
            return "";
        }
        String adresse = _adresse.trim().toLowerCase();
        adresse = StringUtils.replace(adresse, "à", "a");
        adresse = StringUtils.replace(adresse, "â", "a");
        adresse = StringUtils.replace(adresse, "é", "e");
        adresse = StringUtils.replace(adresse, "è", "e");
        adresse = StringUtils.replace(adresse, "ê", "e");
        adresse = StringUtils.replace(adresse, "ë", "e");
        adresse = StringUtils.replace(adresse, "î", "i");
        adresse = StringUtils.replace(adresse, "ï", "i");
        adresse = StringUtils.replace(adresse, "ô", "o");
        adresse = StringUtils.replace(adresse, "ö", "o");
        adresse = StringUtils.replace(adresse, "û", "u");
        adresse = StringUtils.replace(adresse, "ü", "u");
        adresse = StringUtils.replace(adresse, "ù", "u");
        adresse = StringUtils.replace(adresse, "ç", "c");
        adresse = StringUtils.replace(adresse, "'", "");
        adresse = StringUtils.replace(adresse, " ", "");
        return adresse;
    }

    /**
     * Remplacement des espaces, majuscules et caractères accentués
     *
     * @param chaine
     *            le string à traiter
     *
     * @return String la chaine apres traitement
     */
    public static String formatString(final String chaine) {
        if (chaine == null) {
            return "";
        }
        final int lg = chaine.length();
        final StringBuilder sb = new StringBuilder(lg);
        char lastChar = '-';
        for (int i = 0; i < lg; i++) {
            final char c = chaine.charAt(i);
            char c2 = '?';
            char c3 = '?';
            if (c >= 'a' && c <= 'z') {
                c2 = c;
            } else if (c >= 'A' && c <= 'Z') {
                c2 = Character.toLowerCase(c);
            } else if (c >= '0' && c <= '9') {
                c2 = c;
            } else if (c == 'À' || c == 'Á' || c == 'Â' || c == 'à' || c == 'á' || c == 'â') {
                c2 = 'a';
            } else if (c == 'È' || c == 'É' || c == 'Ê' || c == 'Ë' || c == 'è' || c == 'é' || c == 'ê' || c == 'ë') {
                c2 = 'e';
            } else if (c == 'Ì' || c == 'Í' || c == 'Î' || c == 'Ï' || c == 'ì' || c == 'í' || c == 'î' || c == 'ï') {
                c2 = 'i';
            } else if (c == 'Ò' || c == 'Ó' || c == 'Ô' || c == 'ò' || c == 'ó' || c == 'ô') {
                c2 = 'o';
            } else if (c == 'Ù' || c == 'Ú' || c == 'Û' || c == 'Ü' || c == 'ù' || c == 'ú' || c == 'û' || c == 'ü') {
                c2 = 'u';
            } else if (c == 'Ç' || c == 'ç') {
                c2 = 'c';
            } else if (c == 'Ñ' || c == 'ñ') {
                c2 = 'n';
            } else if (c == 'Æ' || c == 'æ') {
                c2 = 'a';
                c3 = 'e';
            } else if (c == '' || c == '') {
                c2 = 'o';
                c3 = 'e';
            } else {
                c2 = '-';
            }
            if (c2 != '-' || lastChar != '-') {
                sb.append(c2);
                lastChar = c2;
                if (c3 != '?') {
                    sb.append(c3);
                }
            }
        }
        return sb.toString();
    }

    /**
     * Mise des villes au format INSEE. Remplacement des espaces par des '-'
     *
     * @param _ville
     *            le nom de la ville
     *
     * @return String la ville après traitement
     */
    public static String formaterVilleInsee(final String _ville) {
        return _ville.trim().replace(' ', '-');
    }

    /**
     * Teste si la chaine est de la forme hh:mm.
     *
     * @param _chaineHoraire
     *            the _chaine horaire
     *
     * @return boolean vrai ou faux
     */
    public static boolean controlerHoraire(final String _chaineHoraire) {
        boolean ok = false;
        if (StringUtils.isNotEmpty(_chaineHoraire) && _chaineHoraire.length() == 5) {
            if (_chaineHoraire.indexOf(":") == 2) {
                final int heure = Integer.parseInt(_chaineHoraire.substring(0, 2));
                final int minute = Integer.parseInt(_chaineHoraire.substring(3));
                if (heure >= 0 && heure <= 23 && minute >= 0 && minute <= 59) {
                    ok = true;
                }
            }
        }
        return ok;
    }

    /**
     * takes converts an e-mail address to a hex encoded href link in order to thwart spambots.
     *
     * @param email
     *            the _email
     *
     * @return a standard href link with mailto: and the e-mail address converted to ASCII Hex
     * @deprecated cette méthode ne fait rien... A ne pas utiliser.
     */
    @Deprecated
    public static String encodeMail(final String email) {
        return email;
    }

    /**
     * Auteur : ANDIL/EL basé sur code de malice  * Remplacement dans l'adresse mail des espaces (par tiret), majuscules et  * accents. Est utilisé pour créer des adresses mails
     * pour contactoffice a partir du nom  * @param String _adresse la chaine qui sera le prefixe du mail  * @return String l'adresse prete  
     *
     * @param _adresse
     *            the _adresse
     *
     * @return the string
     */
    public static String traiterEmailPrenom(final String _adresse) {
        if (_adresse == null) {
            return "";
        }
        String adresse = _adresse.trim().toLowerCase();
        adresse = StringUtils.replace(adresse, "à", "a");
        adresse = StringUtils.replace(adresse, "â", "a");
        adresse = StringUtils.replace(adresse, "é", "e");
        adresse = StringUtils.replace(adresse, "è", "e");
        adresse = StringUtils.replace(adresse, "ê", "e");
        adresse = StringUtils.replace(adresse, "ë", "e");
        adresse = StringUtils.replace(adresse, "î", "i");
        adresse = StringUtils.replace(adresse, "ï", "i");
        adresse = StringUtils.replace(adresse, "ô", "o");
        adresse = StringUtils.replace(adresse, "ö", "o");
        adresse = StringUtils.replace(adresse, "û", "u");
        adresse = StringUtils.replace(adresse, "ü", "u");
        adresse = StringUtils.replace(adresse, "ù", "u");
        adresse = StringUtils.replace(adresse, "ç", "c");
        adresse = StringUtils.replace(adresse, "'", "");
        adresse = StringUtils.replace(adresse, " ", "-");
        return adresse;
    }

    /**
     * Auteur : ANDIL/EL basé sur code de malice  Remplacement dans l'adresse mail des espaces (par rien), majuscules et  * accents. Est utilisé pour créer des adresses mails pour
     * contactoffice a partir du nom et prenom  * @param String _adresse la chaine qui sera le prefixe du mail  * @return String l'adresse prete  
     *
     * @param _adresse
     *            the _adresse
     *
     * @return the string
     */
    public static String traiterEmailNom(final String _adresse) {
        if (_adresse == null) {
            return "";
        }
        String adresse = _adresse.trim().toLowerCase();
        adresse = StringUtils.replace(adresse, "à", "a");
        adresse = StringUtils.replace(adresse, "â", "a");
        adresse = StringUtils.replace(adresse, "é", "e");
        adresse = StringUtils.replace(adresse, "è", "e");
        adresse = StringUtils.replace(adresse, "ê", "e");
        adresse = StringUtils.replace(adresse, "ë", "e");
        adresse = StringUtils.replace(adresse, "î", "i");
        adresse = StringUtils.replace(adresse, "ï", "i");
        adresse = StringUtils.replace(adresse, "ô", "o");
        adresse = StringUtils.replace(adresse, "ö", "o");
        adresse = StringUtils.replace(adresse, "û", "u");
        adresse = StringUtils.replace(adresse, "ü", "u");
        adresse = StringUtils.replace(adresse, "ù", "u");
        adresse = StringUtils.replace(adresse, "ç", "c");
        adresse = StringUtils.replace(adresse, "'", "");
        adresse = StringUtils.replace(adresse, " ", "");
        return adresse;
    }

    /**
     * Controler code metier.
     *
     * @param code
     *            the code
     *
     * @throws ErreurApplicative
     *             the erreur applicative
     */
    public static void controlerCodeMetier(final String code) throws ErreurApplicative {
        // Vérifie que le code ne contient que des caractères alphanumériques ou '-' ou '_'
        if (StringUtils.isNotEmpty(code) && code.length() == 1) {
            throw new ErreurApplicative("Le code doit contenir plus d'un caractère.");
        }
        char c;
        for (int i = 0; i < code.length(); i++) {
            c = code.charAt(i);
            if ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9') && c != '-' && c != '_' && c != '@' && c != '.') {
                throw new ErreurApplicative("Seuls les caractères alphanumériques et les caractères '.','-','_', et '@' sont autorisés pour le code.");
            }
        }
    }

    /**
     * RP2005 optimisation de la méthode (et appel en plus directement sur les fichier textes) Formate le texte recherché pour lucene et pour la recherche avancée.
     *
     * @param contenu
     *            the contenu
     *
     * @return the string
     */
    public static String supprimerISOLatin1Accent(final String contenu) {
        final int lg = contenu.length();
        final StringBuilder sb = new StringBuilder(lg);
        for (int i = 0; i < lg; i++) {
            final char c = contenu.charAt(i);
            char c2 = '?';
            char c3 = '?';
            if (c == 'À' || c == 'Á' || c == 'Â' || c == 'à' || c == 'á' || c == 'â') {
                c2 = 'a';
            } else if (c == 'È' || c == 'É' || c == 'Ê' || c == 'Ë' || c == 'è' || c == 'é' || c == 'ê' || c == 'ë') {
                c2 = 'e';
            } else if (c == 'Ì' || c == 'Í' || c == 'Î' || c == 'Ï' || c == 'ì' || c == 'í' || c == 'î' || c == 'ï') {
                c2 = 'i';
            } else if (c == 'Ò' || c == 'Ó' || c == 'Ô' || c == 'ò' || c == 'ó' || c == 'ô') {
                c2 = 'o';
            } else if (c == 'Ù' || c == 'Ú' || c == 'Û' || c == 'Ü' || c == 'ù' || c == 'ú' || c == 'û' || c == 'ü') {
                c2 = 'u';
            } else if (c == 'Ç' || c == 'ç') {
                c2 = 'c';
            } else if (c == 'Ñ' || c == 'ñ') {
                c2 = 'n';
            } else if (c == 'Æ' || c == 'æ') {
                c2 = 'a';
                c3 = 'e';
            } else if (c == '' || c == '') {
                c2 = 'o';
                c3 = 'e';
            } else {
                c2 = c;
            }
            sb.append(c2);
            if (c3 != '?') {
                sb.append(c3);
            }
        }
        return sb.toString();
    }

    /**
     * Remplacement des : - points-virgules : ; et des (&#59;) par des * - dièses :(&#35;) par des # (couleurs).
     *
     * @param tag
     *            the tag
     *
     * @return the string
     */
    public static String remplacerPointsVirgules(final String tag) {
        return remplacerPointsVirgules(tag, false);
    }

    public static String remplacerPointsVirgules(final String tag, final boolean isParagraphe) {
        String res = "";
        if (isParagraphe) {
            res = StringUtils.replace(tag, "&#59;", "*");
            res = StringUtils.replace(res, "&#61;", "=");
            res = StringUtils.replace(res, "&#38;", "&");
            res = StringUtils.replace(res, "&#63;", "?");
            res = StringUtils.replace(res, "&#35;", "#");
        } else {
            res = EscapeString.unescapeHtml(tag);
        }
        /* 2 ème partie : remplacement des ; par des  '*'
           NE PAS REMPLACER LES &000;
         */
        final int n = res.length();
        boolean insideTag = false;
        final StringBuilder sb = new StringBuilder(50);
        for (int i = 0; i < n; i++) {
            final char c = res.charAt(i);
            // Détection tags
            if (c == '&') {
                insideTag = true;
            }
            // Insertion caractère
            if (c == ';') {
                if (!insideTag) {
                    sb.append('*');
                } else {
                    sb.append(';');
                    insideTag = false;
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String formatToHtml(String chaine) {
        final String replacement = "<br />";
        chaine = chaine.replaceAll("\r\n", replacement);
        chaine = chaine.replaceAll("\r", replacement);
        chaine = chaine.replaceAll("\n", replacement);
        return chaine;
    }

    /*
     * Encodage des caractères spéciaux ANSI en entité HTML décimal
     * http://htmlhelp.com/reference/html40/entities/special.html
     */
    public static String encodeSpecialEntities(final String chaine) {
        final ANSICodec codec = ANSICodec.getInstance();
        return codec.encodeSpecialEntities(chaine);
    }

    public static String escapeJavaScript(final String chaine) {
        return  StringUtils.defaultString(chaine).replaceAll("'", "&apos;").replaceAll("\"", "&quot;");
    }

    /**
     * Decompose une chaine en liste en se basant sur le separateur [] (crochets)
     */
    public static List<String> getListCrochets(final String chaine) {
        final List<String> l = new ArrayList<>();
        if (chaine != null) {
            final StringTokenizer st = new StringTokenizer(chaine, "[]");
            while (st.hasMoreTokens()) {
                l.add(st.nextToken());
            }
        }
        return l;
    }

    @Deprecated
    public static String encodeUTF8(final String valeur) {
        return encode(valeur, org.apache.commons.lang3.CharEncoding.UTF_8);
    }

    @Deprecated
    public static String encodeISO88591(final String valeur) {
        return encode(valeur, org.apache.commons.lang3.CharEncoding.ISO_8859_1);
    }

    @Deprecated
    public static String encode(final String valeur, final String charset) {
        String res = valeur;
        if (StringUtils.isNotEmpty(valeur)) {
            try {
                // TODO !!! Plus de vérification de Charset. Trouver autre chose que Tika
                res = new String(valeur.getBytes(charset), charset);
            } catch (final UnsupportedEncodingException e) {
                LOG.debug("unable to encode the given string", e);
            }
        }
        return res;
    }
}
