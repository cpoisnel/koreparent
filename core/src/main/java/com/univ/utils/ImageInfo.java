package com.univ.utils;

import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Get file format, image resolution and number of bits per pixel from JPEG, GIF, BMP, PCX and PNG input streams.
 * <p>
 * Use the class like this:
 *
 * <pre>
 * ImageInfo ii = new ImageInfo();
 * ii.setInput(in); // in can be InputStream or RandomAccessFile
 * if (!ii.check()) {
 *     Log.error(&quot;Not a JPEG, GIF, BMP, PCX or PNG stream.&quot;);
 * } else {
 *     System.out.println(ii.getFormatName() + &quot;, &quot; + ii.getWidth() + &quot; x &quot; + ii.getHeight() + &quot; pixels, &quot; + ii.getBitsPerPixel() + &quot; bits per pixel.&quot;);
 * }
 * </pre>
 *
 * You can also use this class as a command line program. Call it with a number of image file names as parameters:
 *
 * <pre>
 * java ImageInfo *.jpg *.png *.gif *.bmp *.pcx
 * </pre>
 *
 * or call it without parameters and pipe data to it:
 *
 * <pre>
 * cat image.jpg | java ImageInfo
 * </pre>
 * <p>
 * Known limitations:
 * <ul>
 * <li>GIF bits per pixel are only read from the global header. For some GIFs, local palettes change this to a typically larger value. To fix this, one would have to scan the
 * entire GIF file.</li>
 * <li>Transparency information is not included in the bits per pixel count. Actually, it was my decision not to include those bits, so it's a feature! ;-)</li>
 * </ul>
 * <p>
 * Requirements:
 * <ul>
 * <li>Java 1.1 or higher</li>
 * </ul>
 * <p>
 * The latest version can be found at <a
 * href="http://www.geocities.com/marcoschmidt.geo/projects.html#imageinfo">http://www.geocities.com/marcoschmidt.geo/projects.html#imageinfo</a>.
 * <p>
 * Written by <a href="mailto:marcoschmidt@users.sourceforge.net">Marco Schmidt</a>.
 * <p>
 * This class is contributed to the Public Domain. Use it at your own risk.
 * <p>
 * Last modification 2001-10-16.
 * <p>
 * History:
 * <ul>
 * <li><strong>2001-08-24</strong> Initial version.</li>
 * <li><strong>2001-10-13</strong> Added support for the file formats BMP and PCX.</li>
 * <li><strong>2001-10-16</strong> Fixed bug in read(int[], int, int) that returned the array length, not the number of bytes read (problem only occurred in combination with
 * DataInput). Thanks to Bob Kimble for sending in the bug report!</li>
 * </ul>
 */
public class ImageInfo {

    private static final Logger LOG = LoggerFactory.getLogger(ImageInfo.class);

    /** Return value of {@link #getFormat()} for JPEG streams. */
    public static final int FORMAT_JPEG = 0;

    /** Return value of {@link #getFormat()} for GIF streams. */
    public static final int FORMAT_GIF = 1;

    /** Return value of {@link #getFormat()} for PNG streams. */
    public static final int FORMAT_PNG = 2;

    /** The Constant FORMAT_NAMES. */
    private static final String[] FORMAT_NAMES = {"JPEG", "GIF", "PNG", "BMP", "PCX"};

    /** The Constant EXTENSION_NAMES. */
    private static final String[] EXTENSION_NAMES = {"jpg", "gif", "png", "bmp", "pcx"};

    /** The width. */
    private int width;

    /** The height. */
    private int height;

    /** The bits per pixel. */
    private int bitsPerPixel;

    /** The format. */
    private int format;

    /** The in. */
    private InputStream in;

    /** The din. */
    private DataInput din;

    /**
     * Call this method after you have provided an input stream or file using {@link #setInput(InputStream)} or {@link #setInput(DataInput)}. If true is returned, the file format
     * was known and you information about its content can be retrieved using the various getXyz methods.
     *
     * @return if information could be retrieved from input
     */
    public boolean check() {
        try {
            final int b1 = read() & 0xff;
            final int b2 = read() & 0xff;
            if (b1 == 0x47 && b2 == 0x49) {
                return checkGif();
            } else if (b1 == 0x89 && b2 == 0x50) {
                return checkPng();
            } else if (b1 == 0xff && b2 == 0xd8) {
                return checkJpeg();
            } else {
                return false;
            }
        } catch (final IOException ioe) {
            LOG.debug("unable to check the given file", ioe);
            return false;
        }
    }

    /**
     * Check gif.
     *
     * @return true, if successful
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private boolean checkGif() throws IOException {
        final byte[] GIF_MAGIC_87A = {0x46, 0x38, 0x37, 0x61};
        final byte[] GIF_MAGIC_89A = {0x46, 0x38, 0x39, 0x61};
        final byte[] a = new byte[9];
        if (read(a) != 9) {
            return false;
        }
        if ((!equals(a, 0, GIF_MAGIC_89A, 0, 4)) && (!equals(a, 0, GIF_MAGIC_87A, 0, 4))) {
            return false;
        }
        format = FORMAT_GIF;
        width = getShortLittleEndian(a, 4);
        height = getShortLittleEndian(a, 6);
        bitsPerPixel = (((a[8] & 0xff) >> 4) & 0x07) + 1;
        return true;
    }

    /**
     * Check jpeg.
     *
     * @return true, if successful
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private boolean checkJpeg() throws IOException {
        final byte[] data = new byte[6];
        while (true) {
            if (read(data, 0, 4) != 4) {
                return false;
            }
            final int marker = getShortBigEndian(data, 0);
            final int size = getShortBigEndian(data, 2);
            if ((marker & 0xff00) != 0xff00) {
                return false; // not a valid marker
            }
            if (marker >= 0xffc0 && marker <= 0xffcf && marker != 0xffc4 && marker != 0xffc8) {
                if (read(data) != 6) {
                    return false;
                }
                format = FORMAT_JPEG;
                bitsPerPixel = (data[0] & 0xff) * (data[5] & 0xff);
                width = getShortBigEndian(data, 3);
                height = getShortBigEndian(data, 1);
                return true;
            } else {
                skip(size - 2);
            }
        }
    }

    /**
     * Check png.
     *
     * @return true, if successful
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private boolean checkPng() throws IOException {
        final byte[] PNG_MAGIC = {0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a};
        final byte[] a = new byte[24];
        if (read(a) != 24) {
            return false;
        }
        if (!equals(a, 0, PNG_MAGIC, 0, 6)) {
            return false;
        }
        format = FORMAT_PNG;
        width = getIntBigEndian(a, 14);
        height = getIntBigEndian(a, 18);
        bitsPerPixel = a[22] & 0xff;
        final int colorType = a[23] & 0xff;
        if (colorType == 2 || colorType == 6) {
            bitsPerPixel *= 3;
        }
        return true;
    }

    /**
     * Equals.
     *
     * @param a1
     *            the a1
     * @param offs1
     *            the offs1
     * @param a2
     *            the a2
     * @param offs2
     *            the offs2
     * @param num
     *            the num
     *
     * @return true, if successful
     */
    private boolean equals(final byte[] a1, int offs1, final byte[] a2, int offs2, int num) {
        while (num-- > 0) {
            if (a1[offs1++] != a2[offs2++]) {
                return false;
            }
        }
        return true;
    }

    /**
     * If {@link #check()} was successful, returns the image's number of bits per pixel. Does not include transparency information like the alpha channel.
     *
     * @return number of bits per image pixel
     */
    public int getBitsPerPixel() {
        return bitsPerPixel;
    }

    /**
     * If {@link #check()} was successful, returns the image format as one of the FORMAT_xyz constants from this class. Use {@link #getFormatName()} to get a textual description of
     * the file format.
     *
     * @return file format as a FORMAT_xyz constant
     */
    public int getFormat() {
        return format;
    }

    /**
     * If {@link #check()} was successful, returns the image format's name. Use {@link #getFormat()} to get a unique number.
     *
     * @return file format name
     */
    public String getFormatName() {
        if (format >= 0 && format < FORMAT_NAMES.length) {
            return FORMAT_NAMES[format];
        }
        return "?";
    }

    /**
     * If {@link #check()} was successful, returns the image extension's name. Use {@link #getFormat()} to get a unique number.
     *
     * @return file format name
     */
    public String getExtensionName() {
        if (format >= 0 && format < EXTENSION_NAMES.length) {
            return EXTENSION_NAMES[format];
        }
        return "?";
    }

    /**
     * If {@link #check()} was successful, returns one the image's vertical resolution in pixels.
     *
     * @return image height in pixels
     */
    public int getHeight() {
        return height;
    }

    /**
     * Gets the int big endian.
     *
     * @param a
     *            the a
     * @param offs
     *            the offs
     *
     * @return the int big endian
     */
    private int getIntBigEndian(final byte[] a, final int offs) {
        return (a[offs] & 0xff) << 24 | (a[offs + 1] & 0xff) << 16 | (a[offs + 2] & 0xff) << 8 | a[offs + 3] & 0xff;
    }

    /**
     * Gets the short big endian.
     *
     * @param a
     *            the a
     * @param offs
     *            the offs
     *
     * @return the short big endian
     */
    private int getShortBigEndian(final byte[] a, final int offs) {
        return (a[offs] & 0xff) << 8 | (a[offs + 1] & 0xff);
    }

    /**
     * Gets the short little endian.
     *
     * @param a
     *            the a
     * @param offs
     *            the offs
     *
     * @return the short little endian
     */
    private int getShortLittleEndian(final byte[] a, final int offs) {
        return (a[offs] & 0xff) | (a[offs + 1] & 0xff) << 8;
    }

    /**
     * If {@link #check()} was successful, returns one the image's horizontal resolution in pixels.
     *
     * @return image width in pixels
     */
    public int getWidth() {
        return width;
    }

    /**
     * Read.
     *
     * @return the int
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private int read() throws IOException {
        if (in != null) {
            return in.read();
        }
        return din.readByte();
    }

    /**
     * Read.
     *
     * @param a
     *            the a
     *
     * @return the int
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private int read(final byte[] a) throws IOException {
        if (in != null) {
            return in.read(a);
        }
        din.readFully(a);
        return a.length;
    }

    /**
     * Read.
     *
     * @param a
     *            the a
     * @param offset
     *            the offset
     * @param num
     *            the num
     *
     * @return the int
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private int read(final byte[] a, final int offset, final int num) throws IOException {
        if (in != null) {
            return in.read(a, offset, num);
        }
        din.readFully(a, offset, num);
        return num;
    }

    /**
     * Set the input stream to the argument stream (or file). Note that {@link java.io.RandomAccessFile} implements {@link java.io.DataInput}.
     *
     * @param dataInput
     *            the input stream to read from
     */
    public void setInput(final DataInput dataInput) {
        din = dataInput;
        in = null;
    }

    /**
     * Set the input stream to the argument stream (or file).
     *
     * @param inputStream
     *            the input stream to read from
     */
    public void setInput(final InputStream inputStream) {
        in = inputStream;
        din = null;
    }

    /**
     * Skip.
     *
     * @param num
     *            the num
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void skip(final int num) throws IOException {
        if (in != null) {
            in.skip(num);
        } else {
            din.skipBytes(num);
        }
    }

    /**
     * Close.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void close() throws IOException {
        in.close();
    }
}