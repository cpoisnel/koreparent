package com.univ.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class ToolboxUtil {

    public static String tranformerLienRelatifEnAbsolu(String texte, String host) {
        String exp = "href=\"([^\"]*)\"";
        Pattern p = Pattern.compile(exp);
        Matcher m = p.matcher(texte);
        StringBuffer sb = new StringBuffer(texte.length());
        while (m.find()) {
            String href = m.group(1);
            if (href.startsWith("/")) {
                href = "href=\"" + host + href + "\"";
                m.appendReplacement(sb, Matcher.quoteReplacement(href));
            }
        }
        m.appendTail(sb);
        return sb.toString();
    }

    public static String nettoyerAttributsNonValides(String texte) {
        // suppression des ancres parasites #KLINK
        texte = StringUtils.replace(texte, "#KLINK", "");
        // suppression des attributs parasites identifies
        // <span ktag_news_span="true">[(*)]</span>
        // <span kgalerie_span="true">[(*)]</span>
        // _linktype="[^"]*"
        // <span kliste_span="true">[(*)]</span>
        // <span krss_span="true">(*)</span>
        // <span ksite_span="true">[(*)]</span>
        // <span ktag_span="true">[(*)]</span>
        String exp = "<span k[^>]*>(\\[[^\\]]*\\])</span>";
        Pattern p = Pattern.compile(exp);
        Matcher m = p.matcher(texte);
        StringBuffer sb = new StringBuffer(texte.length());
        while (m.find()) {
            String text = m.group(1);
            m.appendReplacement(sb, Matcher.quoteReplacement(text));
        }
        m.appendTail(sb);
        texte = sb.toString();
        exp = "<span krss_span=\"true\">(<a[^>]*>[^<]*</a>)</span>";
        p = Pattern.compile(exp);
        m = p.matcher(texte);
        sb = new StringBuffer(texte.length());
        while (m.find()) {
            String text = m.group(1);
            m.appendReplacement(sb, Matcher.quoteReplacement(text));
        }
        m.appendTail(sb);
        exp = "<span class=\"[^>]*ktag\">(\\[?[^\\]]*\\]?)</span>";
        p = Pattern.compile(exp);
        m = p.matcher(texte);
        sb = new StringBuffer(texte.length());
        while (m.find()) {
            String text = m.group(1);
            m.appendReplacement(sb, Matcher.quoteReplacement(text));
        }
        m.appendTail(sb);
        return sb.toString();
    }
}
