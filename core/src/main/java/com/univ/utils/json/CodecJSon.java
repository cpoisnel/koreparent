package com.univ.utils.json;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Classe permettant de manipuler des flux en JSON
 *
 */
public class CodecJSon {

    public static File encodeObjectToJsonInFile(final String path, final Object obj, final DateFormat dateFormat, final boolean autoDetectGetters) throws JsonGenerationException, JsonMappingException, IOException {
        final File file = new File(path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        final ObjectMapper mapper = new ObjectMapper();
        if (dateFormat != null) {
            mapper.setDateFormat(dateFormat);
        }
        if (!autoDetectGetters) {
            mapper.configure(MapperFeature.AUTO_DETECT_GETTERS, false);
            mapper.configure(MapperFeature.AUTO_DETECT_IS_GETTERS, false);
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        }
        mapper.writeValue(file, obj);
        return file;
    }

    /**
     * Retourne un {@link Object} encodé en json dans un {@link File}
     *
     * @param obj
     *            Object à encoder
     * @param dateFormat
     *            format de la date à utiliser
     * @return object encodé
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static File encodeObjectToJsonInFile(final String path, final Object obj, final DateFormat dateFormat) throws IOException {
        return encodeObjectToJsonInFile(path, obj, dateFormat, true);
    }

    /**
     * Retourne un {@link Object} encodé en json dans un {@link File}
     *
     * @param obj
     *            Object à encoder
     * @param autoDetectGetters
     *            desactive le parsing automatique des getters
     * @return object encodé
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static File encodeObjectToJsonInFile(final String path, final Object obj, final boolean autoDetectGetters) throws IOException {
        return encodeObjectToJsonInFile(path, obj, null, autoDetectGetters);
    }

    /**
     * Retourne un {@link Object} encodé en json dans un {@link File}
     *
     * @param obj
     *            Object à encoder
     * @return object encodé
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static File encodeObjectToJsonInFile(final String path, final Object obj) throws IOException {
        return encodeObjectToJsonInFile(path, obj, null, true);
    }

    /**
     * Retourne un {@link Object} encodé en json dans un {@link String}
     *
     * @param obj
     *            Object à encoder
     * @param dateFormat
     *            format de la date à utiliser
     * @param autoDetectGetters
     *            desactive le parsing automatique des getters
     * @return object encodé
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static String encodeObjectToJSonInString(final Object obj, final DateFormat dateFormat, final boolean autoDetectGetters) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        if (dateFormat != null) {
            mapper.setDateFormat(dateFormat);
        }
        if (!autoDetectGetters) {
            mapper.configure(MapperFeature.AUTO_DETECT_GETTERS, false);
            mapper.configure(MapperFeature.AUTO_DETECT_IS_GETTERS, false);
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        }
        return mapper.writeValueAsString(obj);
    }

    /**
     * Retourne un {@link Object} encodé en json dans un {@link String}
     *
     * @param obj
     *            Object à encoder
     * @param dateFormat
     *            format de la date à utiliser
     * @return object encodé
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static String encodeObjectToJSonInString(final Object obj, final DateFormat dateFormat) throws IOException {
        return encodeObjectToJSonInString(obj, dateFormat, true);
    }

    /**
     * Retourne un {@link Object} encodé en json dans un {@link String}
     *
     * @param obj
     *            Object à encoder
     * @param autoDetectGetters
     *            desactive le parsing automatique des getters
     * @return object encodé
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static String encodeObjectToJSonInString(final Object obj, final boolean autoDetectGetters) throws IOException {
        return encodeObjectToJSonInString(obj, null, autoDetectGetters);
    }

    /**
     * Retourne un {@link Object} encodé en json dans un {@link String}
     *
     * @param obj
     *            Object à encoder
     * @return object encodé
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static String encodeObjectToJSonInString(final Object obj) throws IOException {
        return encodeObjectToJSonInString(obj, null, true);
    }

    /**
     * Retourne la valeur dans la class passée en paramètre (dans le typeReference)
     *
     * <br/>
     * (Utile pour les types complexe)
     *
     * @param valeur
     *            valeur JSon
     * @param typeReference
     *            référence le type de retour (exemples: new TypeReference<MaClass>(){} / new TypeReference<List<MaClass>>(){})
     * @return L'object valeur décodé dans la classe
     * @throws IOException
     */
    public static <T> T decodeStringJSonToClass(final String valeur, final TypeReference<T> typeReference) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(valeur, typeReference);
    }

    public static <T> T decodeStringJSonToClass(final String valeur, final Class<T> typeClass) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(valeur, typeClass);
    }

    public static <T> T decodeFileJSonToClass(final File valeur, final TypeReference<T> typeReference) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(valeur, typeReference);
    }

    public static <T> T decodeFileJSonToClass(final File valeur, final Class<T> typeClass) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(valeur, typeClass);
    }
}
