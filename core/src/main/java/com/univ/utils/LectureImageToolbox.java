package com.univ.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.RequeteMgr;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.MediaUtils;

/**
 * Accès front-office au fichier joint d'un objet document.
 * @author Rom
 */
public class LectureImageToolbox extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -3215356115631894058L;

    private static final Logger LOG = LoggerFactory.getLogger(LectureImageToolbox.class);

    private final transient ServiceMedia serviceMedia;

    public LectureImageToolbox() {
        serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
    }

    /**
     * Process incoming HTTP GET requests.
     *
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        performTask(request, response);
    }

    /**
     * Process incoming HTTP POST requests.
     *
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        performTask(request, response);
    }

    /**
     * Process incoming requests for information.
     *
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     */
    public void performTask(final HttpServletRequest request, final HttpServletResponse response) {
        final ContexteUniv ctx = ContexteUniv.initContexteUniv(request, response, getServletContext(), getServletConfig());
        String pathImage = "";
        final String tag = request.getParameter("TAG");
        if (tag != null && tag.length() > 0) {
            // test mise en session
            MediaBean mediaBean = null;
            final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) request.getSession(Boolean.FALSE).getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
            if (sessionUtilisateur != null) {
                final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
                if (infosSession.get(tag) != null) {
                    // récupération de l'id de l'image
                    mediaBean = getMediaFromTag(tag);
                    pathImage = MediaUtils.getPathAbsolu(mediaBean);
                    final boolean lecture = (boolean) infosSession.get(tag);
                    if (!lecture) {
                        infosSession.remove(tag);
                    }
                }
            }
            if (pathImage.length() > 0 && mediaBean != null) {
                try (final InputStream is = getInputStream(pathImage, mediaBean)) {
                    writeStreamInResponse(is, response, mediaBean);
                } catch (final IOException fnfe) {
                    LOG.error("io exception while sending image data", fnfe);
                }
                // JSS 20040222 : Gestion des identifiants de requete
                // On n'attend pas que le  document soit téléchargé
                // (détection de boucles intempestives)
                RequeteMgr.terminerRequete(ctx.getIdRequete());
            }
        }
    }

    private MediaBean getMediaFromTag(final String tag) {
        // récupération de l'id de l'image
        MediaBean result = null;
        final int idxId = tag.indexOf("[id-image]");
        final int idxFinId = tag.indexOf("[/id-image]");
        if (idxId != -1 && idxFinId != -1) {
            String idImg = tag.substring(idxId + 10, idxFinId);
            if (idImg.length() > 0) {
                if (idImg.startsWith("F")) {
                    idImg = idImg.substring(1);
                }
                try {
                    result = serviceMedia.getById(Long.valueOf(idImg));
                } catch (final NumberFormatException nfe) {
                    LOG.debug("unable to convert " + idImg + " to a long value", nfe);
                }
            }
        }
        return result;
    }

    private InputStream getInputStream(final String pathImage, final MediaBean mediaBean) throws IOException {
        final InputStream is;
        if (MediaUtils.isLocal(mediaBean)) {
            final File f = new File(pathImage);
            is = new FileInputStream(f);
        } else {
            final URL url = new URL(pathImage);
            final URLConnection urlConnection = url.openConnection();
            is = urlConnection.getInputStream();
        }
        return is;
    }
    private void writeStreamInResponse(final InputStream is, final HttpServletResponse response, final MediaBean mediaBean) throws IOException {
        try (ServletOutputStream writer = response.getOutputStream();
             final BufferedInputStream bis = new BufferedInputStream(is);) {
            final byte[] buf = new byte[1024 * 4];
            response.setHeader("Content-Disposition", "inline;filename=\"" + mediaBean.getSource() + "\"");
            response.setContentType(mediaBean.getFormat());
            int nbBytes;
            while ((nbBytes = bis.read(buf)) > 0) {
                writer.write(buf, 0, nbBytes);
            }
        }
    }

}
