package com.univ.utils;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.InfosRequeteGroupe;
import com.univ.objetspartages.om.RequeteGroupeDynamique;
import com.univ.objetspartages.services.ServiceGroupeDsi;

/**
 * The Class RequeteGroupeUtil.
 *
 * @author jeanseb
 *
 *         Fonctions utilitaires
 */
public class RequeteGroupeUtil {

    private static final Logger LOG = LoggerFactory.getLogger(RequeteGroupeUtil.class);

    /**
     * Instancie un objet {@link RequeteGroupeDynamique} contenant les informations de requête pour le groupe dynamique dont le code est fourni en paramètre
     * @param ctx le contexte n'est plus utilisé
     * @param codeRequete le code du groupe
     * @return un bean contenant les infos pour la requête de groupe dynamique
     * @deprecated utiliser la méthode {@link RequeteGroupeUtil#instancierRequete(String)}
     */
    @Deprecated
    public static RequeteGroupeDynamique instancierRequete(final OMContext ctx, final String codeRequete) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return instancierRequete(serviceGroupeDsi.getGroupRequestList().get(codeRequete));
    }

    /**
     * Formatage SQL de la Recherche d'une fiche par rapport à sa rubrique principale ou une de ses rubriques de publication.
     *
     * @param codeRequete le code du groupe
     * @return un bean contenant les infos pour la requête de groupe dynamique
     */
    public static RequeteGroupeDynamique instancierRequete(final String codeRequete) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        return instancierRequete(serviceGroupeDsi.getGroupRequestList().get(codeRequete));
    }

    /**
     * Instancie un objet {@link RequeteGroupeDynamique} contenant les informations de requête pour le groupe dynamique dont le code est fourni en paramètre
     * @param ctx le contexte n'est plus utilisé
     * @param infosRequete le groupe dont on souhaite avoir les infos de requêtes
     * @return un bean contenant les infos pour la requête de groupe dynamique
     * @deprecated utiliser la méthode {@link RequeteGroupeUtil#instancierRequete(InfosRequeteGroupe)}
     */
    @Deprecated
    public static RequeteGroupeDynamique instancierRequete(final OMContext ctx, final InfosRequeteGroupe infosRequete) {
        return instancierRequete(infosRequete);
    }

    /**
     * Instancie un objet {@link RequeteGroupeDynamique} contenant les informations de requête pour le groupe dynamique dont le code est fourni en paramètre
     * @param infosRequete le groupe dont on souhaite avoir les infos de requêtes
     * @return un bean contenant les infos pour la requête de groupe dynamique
     */
    public static RequeteGroupeDynamique instancierRequete(final InfosRequeteGroupe infosRequete) {
        RequeteGroupeDynamique requeteDyn = null;
        try {
            final Class<?> classeObjet = Class.forName(infosRequete.getNomClasse());
            final Class<?> classesParam[] = new Class[0];
            final Constructor<?> constructeur = classeObjet.getConstructor(classesParam);
            requeteDyn = (RequeteGroupeDynamique) constructeur.newInstance();
            requeteDyn.setDelaiRechargementResolutionGroupesUtilisateur(infosRequete.getTsCacheGroupesUtilisateur());
            requeteDyn.setNomRequete(infosRequete.getAlias());
        } catch (final ReflectiveOperationException | IllegalArgumentException e) {
            LOG.warn("Impossible de charger la classe paramétré pour ce groupe dynamique : ", e);
        }
        return requeteDyn;
    }

    /**
     * Calcule la liste des utilisateur apartenant au groupe donnée
     * @param ctx le contexte n'est plus utilisé
     * @param codeGroupe le code de groupe dont on souhaite calculer la liste des utilisateurs
     * @param setCodesGroupesCache ???
     * @return la liste des utilisateurs ou une liste vide si non trouvé
     * @throws Exception
     * @deprecated utiliser la méthode {@link RequeteGroupeUtil#getVecteurUtilisateurs(String, Set)}
     */
    @Deprecated
    public static Collection<String> getVecteurUtilisateurs(final OMContext ctx, final String codeGroupe, final Set<String> setCodesGroupesCache) throws Exception {
        return getVecteurUtilisateurs(codeGroupe, setCodesGroupesCache);
    }

    /**
     * Calcule la liste des utilisateur apartenant au groupe donnée
     * @param codeGroupe le code de groupe dont on souhaite calculer la liste des utilisateurs
     * @param setCodesGroupesCache ???
     * @return la liste des utilisateurs ou une liste vide si non trouvé
     * @throws Exception lors des requêtes en bdd
     */
    public static Collection<String> getVecteurUtilisateurs(final String codeGroupe, final Set<String> setCodesGroupesCache) throws Exception {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupe);
        if(group != null) {
            final String codeRequete = group.getRequeteGroupe();
            final InfosRequeteGroupe infosRequete = serviceGroupeDsi.getGroupRequestList().get(codeRequete);
            final RequeteGroupeDynamique requeteDyn = instancierRequete(infosRequete);
            if (requeteDyn != null) {
                return requeteDyn.getVecteurUtilisateursCache(codeGroupe, setCodesGroupesCache);
            }
        }
        return Collections.emptyList();
    }

    /**
     * Retourne la liste des codes des groupes utilisateur de l'utilisateur fourni en paramètre
     * @param ctx le contexte n'est plus utilisé
     * @param infosRequete les données de la requête dynamique à traiter
     * @param codeUtilisateur le code de l'utilisateur dont on souhaite avoir la liste des codes de groupes
     * @param timeStamp un pseudo timer pour voir si on requête ou non?!?
     * @return la liste des groupes ou une liste vide si non trouvé
     * @throws Exception lors des requêtes en BDD principalement
     * @deprecated utiliser la méthode {@link com.univ.utils.RequeteGroupeUtil#getGroupesUtilisateur(InfosRequeteGroupe, String, long)}
     */
    @Deprecated
    public static Collection<String> getGroupesUtilisateur(final OMContext ctx, final InfosRequeteGroupe infosRequete, final String codeUtilisateur, final long timeStamp) throws Exception {
        return getGroupesUtilisateur(infosRequete, codeUtilisateur, timeStamp);
    }

    /**
     * Retourne la liste des codes des groupes utilisateur de l'utilisateur fourni en paramètre
     * @param infosRequete les données de la requête dynamique à traiter
     * @param codeUtilisateur le code de l'utilisateur dont on souhaite avoir la liste des codes de groupes
     * @param timeStamp un pseudo timer pour voir si on requête ou non?!?
     * @return la liste des groupes ou une liste vide si non trouvé
     * @throws Exception lors des requêtes en BDD principalement
     */
    public static Collection<String> getGroupesUtilisateur(final InfosRequeteGroupe infosRequete, final String codeUtilisateur, final long timeStamp) throws Exception {
        final RequeteGroupeDynamique requeteDyn = instancierRequete(infosRequete);
        if (requeteDyn != null) {
            return requeteDyn.getGroupesUtilisateurCache(codeUtilisateur, timeStamp);
        }
        return Collections.emptyList();
    }
}
