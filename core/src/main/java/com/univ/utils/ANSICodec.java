package com.univ.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts Unicode characters to their ANSI entities
 */
public class ANSICodec {

    private static final Logger LOG = LoggerFactory.getLogger(ANSICodec.class);

    /** Singleton instance. */
    private static ANSICodec codec;

    /** The entity lookup. */
    private final Map<Integer, Integer> unicodeToANSI;

    private final Map<String, String> unicodeToHex;

    /**
     * Creates a new instance of HTMLCodec.
     */
    public ANSICodec() {
        /** set up the HashMaps */
        unicodeToANSI = new HashMap<>();
        unicodeToHex = new HashMap<>();
        initialize();
    }

    /**
     * Returns an instance of the specific codec to be used. This is so they can be singletons and reused.
     *
     * @return The Codec object to use.
     */
    public static ANSICodec getInstance() {
        if (codec == null) {
            codec = new ANSICodec();
        }
        return codec;
    }

    /**
     * Set up the HashMap that contains the convertion Data.
     */
    private void initialize() {
        unicodeToANSI.put(376, 159);
        unicodeToHex.put("\u009F", "&#376;");
        unicodeToHex.put("\u0178", "&#376;");
        unicodeToANSI.put(382, 158);
        unicodeToHex.put("\u009E", "&#382;");
        unicodeToHex.put("\u017E", "&#382;");
        unicodeToANSI.put(339, 156);
        unicodeToHex.put("\u009C", "&#339;");
        unicodeToHex.put("\u0153", "&#339;");
        unicodeToANSI.put(8250, 155);
        unicodeToHex.put("\u009B", "&#8250;");
        unicodeToHex.put("\u203A", "&#8250;");
        unicodeToANSI.put(353, 154);
        unicodeToHex.put("\u009A", "&#353;");
        unicodeToHex.put("\u0161", "&#353;");
        unicodeToANSI.put(8482, 153);
        unicodeToHex.put("\u0099", "&#8482;");
        unicodeToHex.put("\u2122", "&#8482;");
        unicodeToANSI.put(732, 152);
        unicodeToHex.put("\u0098", "&#732;");
        unicodeToHex.put("\u02DC", "&#732;");
        unicodeToANSI.put(8212, 151);
        unicodeToHex.put("\u0097", "&#8212;");
        unicodeToHex.put("\u2014", "&#8212;");
        unicodeToANSI.put(8211, 150);
        unicodeToHex.put("\u0096", "&#8211;");
        unicodeToHex.put("\u2013", "&#8211;");
        unicodeToANSI.put(8226, 149);
        unicodeToHex.put("\u0095", "&#8226;");
        unicodeToHex.put("\u0095", "&#8226;");
        unicodeToANSI.put(8221, 148);
        unicodeToHex.put("\u0094", "&#8221;");
        unicodeToHex.put("\u201D", "&#8221;");
        unicodeToANSI.put(8220, 147);
        unicodeToHex.put("\u0093", "&#8220;");
        unicodeToHex.put("\u201C", "&#8220;");
        unicodeToANSI.put(8217, 146);
        unicodeToHex.put("\u0092", "&#8217;");
        unicodeToHex.put("\u2019", "&#8217;");
        unicodeToANSI.put(8216, 145);
        unicodeToHex.put("\u0091", "&#8216;");
        unicodeToHex.put("\u2018", "&#8216;");
        unicodeToANSI.put(381, 142);
        unicodeToHex.put("\u008E", "&#381;");
        unicodeToHex.put("\u017D", "&#381;");
        unicodeToANSI.put(338, 140);
        unicodeToHex.put("\u008C", "&#338;");
        unicodeToHex.put("\u0152", "&#338;");
        unicodeToANSI.put(8249, 139);
        unicodeToHex.put("\u008B", "&#8249;");
        unicodeToHex.put("\u2039", "&#8249;");
        unicodeToANSI.put(352, 138);
        unicodeToHex.put("\u008A", "&#352;");
        unicodeToHex.put("\u0160", "&#352;");
        unicodeToANSI.put(8240, 137);
        unicodeToHex.put("\u0089", "&#8240;");
        unicodeToHex.put("\u2030", "&#8240;");
        unicodeToANSI.put(710, 136);
        unicodeToHex.put("\u0088", "&#710;");
        unicodeToHex.put("\u02C6", "&#710;");
        unicodeToANSI.put(8225, 135);
        unicodeToHex.put("\u0087", "&#8225;");
        unicodeToHex.put("\u2021", "&#8225;");
        unicodeToANSI.put(8224, 134);
        unicodeToHex.put("\u0086", "&#8224;");
        unicodeToHex.put("\u2020", "&#8224;");
        unicodeToANSI.put(8230, 133);
        unicodeToHex.put("\u0085", "&#8230;");
        unicodeToHex.put("\u2026", "&#8230;");
        unicodeToANSI.put(8222, 132);
        unicodeToHex.put("\u0084", "&#8222;");
        unicodeToHex.put("\u201E", "&#8222;");
        unicodeToANSI.put(402, 131);
        unicodeToHex.put("\u0083", "&#402;");
        unicodeToHex.put("\u0192", "&#402;");
        unicodeToANSI.put(8218, 130);
        unicodeToHex.put("\u0082", "&#8218;");
        unicodeToHex.put("\u201A", "&#8218;");
        unicodeToANSI.put(8364, 128);
        unicodeToHex.put("\u0080", "&#8364;");
        unicodeToHex.put("\u20AC", "&#8364;");
    }

    /**
     * This method decodes the string passed in and returns a decoded version of the string.
     *
     * @param chars
     *            The encoded text string that should be decoded
     *
     * @return The encoded version of the string passed in.
     */
    public final String decode(String chars) {
        BufferedReader br = new BufferedReader(new StringReader(chars));
        Writer sw = new StringWriter();
        int c;
        try {
            while ((c = br.read()) != -1) {
                if (unicodeToANSI.get(c) != null) {
                    c = unicodeToANSI.get(c);
                }
                sw.write(c);
            }
        } catch (IOException e) {
            LOG.debug("unable to decode the given string", e);
            return chars;
        } finally {
            try {
                sw.close();
                br.close();
            } catch (IOException e) {
                LOG.debug("unable to close the reader", e);
            }
        }
        return sw.toString();
    }

    /**
     * This method encodes the string passed in and returns a encoded version of the string.
     *
     * @param chars
     *            The encoded text string that should be decoded
     *
     * @return The encoded version of the string passed in.
     */
    public final String encodeSpecialEntities(String chars) {
        for (String s : unicodeToHex.keySet()) {
            chars = chars.replace(s, unicodeToHex.get(s));
        }
        return chars;
    }
}
