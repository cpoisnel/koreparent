package com.univ.utils.media;

import java.util.HashMap;
import java.util.Map;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.univ.mediatheque.Mediatheque;
import com.univ.objetspartages.om.SpecificMedia;
import com.univ.objetspartages.util.LabelUtils;

/**
 * Helper permettant de générer des couples de codes et libellé sur les type de ressources / type de médias des médias.
 *
 * @author aga
 *
 */
public class TypeRessourceTypeMediaHelper {

    /**
     * Nom du bean tel que défini dans le contexte Spring
     */
    public final static String ID_BEAN = "typeRessourceTypeMediaHelper";

    /**
     * Une instance de l'objet mediatheque
     */
    private Mediatheque mediatheque;

    /**
     * Pour tous les types de media du type de ressource dont le code est en paramètre retourne une map<String,String> dont la clé est la concaténation du code du type de ressource
     * et du code du type de media (suivant {@link CodeTypeRessourceCodeTypeMedia#toString()} et la valeur est la concaténation du libellé du type de ressource et du libellé du
     * type de media (suivant {@link LibelleTypeRessourceLibelleTypeMedia#toString()} Une entrée est également ajoutés pour chaque type de ressource pour représenter le choix
     * "tous les médias du type de ressource".
     *
     * La principale utilité de cette méthode est de faciliter la génération d'un filtre unique sur les types de médias de tous les types de ressources (pour la gallerie notament).
     *
     * @param ctx
     *            Le contexte kportal
     * @param codeTypeRessource
     *            Le code du type de la ressource pour lequel on veut générer la map
     * @return Une map<String,String>
     */
    public Map<String, String> getCodeEtLibelleTypeRessourceEtTypeMedia(OMContext ctx, String codeTypeRessource) {
        Map<String, String> result = new HashMap<>();
        SpecificMedia specificMedia = getMediatheque().getRessource(codeTypeRessource);
        Map<String, String> codesEtLibellesMedia = LabelUtils.getLabelCombo(specificMedia.getCodeTypeLibelle(), LangueUtil.getDefaultLocale());
        //ajout d'une entrée pour debut optgroupe de ce type de média
        //result.put(StringUtils.chop(codeTypeRessource), "#GROUPE#" + specificMedia.getLibelleAffichable());
        //ajout d'une entrée pour "toutes les ressources de ce type de média"
        result.put(new CodeTypeRessourceCodeTypeMedia(codeTypeRessource, null).toString(), new LibelleTypeRessourceLibelleTypeMedia(specificMedia.getLibelleAffichable(), null).toString());
        if (codesEtLibellesMedia != null) {
            for (Map.Entry<String, String> codeEtLibelleMediaEntry : codesEtLibellesMedia.entrySet()) {
                result.put(new CodeTypeRessourceCodeTypeMedia(codeTypeRessource, codeEtLibelleMediaEntry.getKey()).toString(), new LibelleTypeRessourceLibelleTypeMedia(specificMedia.getLibelleAffichable(), codeEtLibelleMediaEntry.getValue()).toString());
            }
        }
        //ajout d'une entrée pour fin optgroupe de ce type de média
        //result.put(codeTypeRessource + "|z", "#/GROUPE#" + specificMedia.getLibelleAffichable());
        return result;
    }

    /**
     * @return the mediatheque
     */
    public Mediatheque getMediatheque() {
        return mediatheque;
    }

    /**
     * @param mediatheque
     *            the mediatheque to set
     */
    public void setMediatheque(Mediatheque mediatheque) {
        this.mediatheque = mediatheque;
    }
}
