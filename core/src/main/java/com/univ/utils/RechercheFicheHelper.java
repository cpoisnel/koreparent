package com.univ.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.textsearch.RechercheFmt;
import com.univ.objetspartages.util.CritereRecherche;

public class RechercheFicheHelper {

    /**
     * @deprecated utiliser pour la retrocompatibilité de la recherche back
     */
    @Deprecated
    public static final String ATTRIBUT_INFOBEAN_REQUETE = "REQUETE";

    public static final String ATTRIBUT_INFOBEAN_CRITERES = "CRITERE_REQUETE";

    private static final String BASE_URL_ADVANCED_SEARCH = "/servlet/com.jsbsoft.jtf.core.SG?PROC=RECHERCHE&ACTION=RECHERCHER&OBJET=";

    private static final String BASE_URL_RESULTS = "/servlet/com.jsbsoft.jtf.core.SG?PROC=RECHERCHE&ACTION=VALIDER&%23ECRAN_LOGIQUE%23=RECHERCHE&";

    /**
     * Ajout d'un critere dans la requete.
     *
     * @param infoBean
     *            la map de données
     * @param requete
     *            la requete
     * @param donnee
     *            la donnee
     * @param valeurForcee
     *            la valeur forcée
     * @param recherche
     *            the recherche
     *
     * @return the string
     * @deprecated utiliser pour la retrocompatibilité de la recherche back
     */
    @Deprecated
    public static String ajouterCritereRequete(final Map<String, Object> infoBean, final String requete, final String donnee, final boolean valeurForcee, final boolean recherche) {
        String res = requete;
        final Object param = infoBean.get(donnee);
        if (param != null) {
            if (param instanceof String) {
                final String sParam = (String) param;
                if (valeurForcee || sParam.length() > 0 && !"0000".equals(sParam)) {
                    res += "&" + donnee + "=" + RechercheFmt.formaterTexteRecherche(sParam, recherche, false);
                }
            } else if (param instanceof Date) {
                final Date dateParam = (Date) param;
                if (Formateur.estSaisie(dateParam)) {
                    res += "&" + donnee + "=" + Formateur.formater(dateParam);
                }
            }
        }
        return res;
    }

    /**
     * Ajout d'un critere dans la requete.
     *
     * @param infoBean
     *            la map de donnees
     * @param requete
     *            the requete
     * @param donnee
     *            the donnee
     *
     * @return the string
     * @deprecated utiliser pour la retrocompatibilité de la recherche back
     */
    @Deprecated
    public static String ajouterCritereRequete(final Map<String, Object> infoBean, final String requete, final String donnee) {
        return ajouterCritereRequete(infoBean, requete, donnee, false, false);
    }

    @Deprecated
    public static Map<String, String> getCriteresRecherche(final String nomDonnee, final Object valeur) {
        final Map<String, String> cleValeurCritere = new HashMap<>();
        if (valeur != null) {
            if (valeur instanceof String) {
                final String sParam = (String) valeur;
                if (StringUtils.isNotBlank(sParam) && !"0000".equals(sParam)) {
                    cleValeurCritere.put(nomDonnee, sParam);
                }
            } else if (valeur instanceof Date) {
                final Date dateParam = (Date) valeur;
                if (Formateur.estSaisie(dateParam)) {
                    cleValeurCritere.put(nomDonnee, Formateur.formater(dateParam));
                }
            }
        }
        return cleValeurCritere;
    }

    @Deprecated
    public static CritereRecherche getCritereRecherche(final InfoBean infobean, final String nomDonnee) {
        CritereRecherche critere = null;
        final Object valeur = infobean.get(nomDonnee);
        if (valeur != null) {
            if (valeur instanceof String) {
                final String sParam = (String) valeur;
                if (StringUtils.isNotBlank(sParam) && !"0000".equals(sParam)) {
                    critere = new CritereRecherche(nomDonnee, sParam, sParam);
                }
            } else if (valeur instanceof Date) {
                final Date dateParam = (Date) valeur;
                if (Formateur.estSaisie(dateParam)) {
                    critere = new CritereRecherche(nomDonnee, dateParam.toString(), Formateur.formater(dateParam));
                }
            }
        }
        return critere;
    }

    public static boolean isFullTextSearch(final InfoBean infoBean) {
        final String unobjet = infoBean.getString("UNOBJET");
        final String query = infoBean.getString(RechercheFiche.QUERY);
        return StringUtils.isNotBlank(query) || "TOUS".equalsIgnoreCase(infoBean.getString("OBJET")) || ("TRUE".equals(unobjet));
    }

    public static String getUrlAdvancedSearch(final String nomObjetCherche, final String rubriqueHistorique) {
        return BASE_URL_ADVANCED_SEARCH + EscapeString.escapeURL(nomObjetCherche) + "&RH=" + EscapeString.escapeURL(rubriqueHistorique);
    }

    public static String getUrlResults(final String request, final String from, final String rubriqueHistorique) {
        final StringBuilder url = new StringBuilder(BASE_URL_RESULTS);
        if (StringUtils.contains(request, "&FROM=")) {
            String oldValue = StringUtils.substringBetween(request, "&FROM=", "&");
            if (oldValue == null) {
                oldValue = StringUtils.substringAfter(request, "&FROM=");
            }
            url.append(StringUtils.replace(request, "&FROM=" + oldValue, "&FROM=" + from));
        } else {
            url.append(request);
            url.append("&FROM=").append(EscapeString.escapeURL(from));
        }
        url.append("&RH=").append(EscapeString.escapeURL(rubriqueHistorique));
        return url.toString();
    }
}
