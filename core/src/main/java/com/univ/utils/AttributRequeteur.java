/*
 * Created on 27 oct. 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.utils;
// TODO: Auto-generated Javadoc

/**
 * The Class AttributRequeteur.
 */
public class AttributRequeteur {

    /** The nom. */
    private String nom = "";

    /** The valeur. */
    private String valeur = "";

    /** The is requetable. */
    private boolean isRequetable = true;

    /** The is triable. */
    private boolean isTriable = true;

    /**
     * Instantiates a new attribut requeteur.
     */
    public AttributRequeteur() {}

    /**
     * Instantiates a new attribut requeteur.
     *
     * @param _nom
     *            the _nom
     * @param _valeur
     *            the _valeur
     * @param _requetable
     *            the _requetable
     * @param _triable
     *            the _triable
     */
    public AttributRequeteur(String _nom, String _valeur, boolean _requetable, boolean _triable) {
        this.nom = _nom;
        this.valeur = _valeur;
        this.isRequetable = _requetable;
        this.isTriable = _triable;
    }

    /**
     * Checks if is requetable.
     *
     * @return true, if is requetable
     */
    public boolean isRequetable() {
        return isRequetable;
    }

    /**
     * Gets the requetable.
     *
     * @return the requetable
     */
    public String getRequetable() {
        return isRequetable ? "1" : "0";
    }

    /**
     * Gets the triable.
     *
     * @return the triable
     */
    public String getTriable() {
        return isTriable ? "1" : "0";
    }

    /**
     * Sets the requetable.
     *
     * @param isRequetable
     *            the new requetable
     */
    public void setRequetable(boolean isRequetable) {
        this.isRequetable = isRequetable;
    }

    /**
     * Checks if is triable.
     *
     * @return true, if is triable
     */
    public boolean isTriable() {
        return isTriable;
    }

    /**
     * Sets the triable.
     *
     * @param isTriable
     *            the new triable
     */
    public void setTriable(boolean isTriable) {
        this.isTriable = isTriable;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets the nom.
     *
     * @param nom
     *            the new nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets the valeur.
     *
     * @return the valeur
     */
    public String getValeur() {
        return valeur;
    }

    /**
     * Sets the valeur.
     *
     * @param valeur
     *            the new valeur
     */
    public void setValeur(String valeur) {
        this.valeur = valeur;
    }
}
