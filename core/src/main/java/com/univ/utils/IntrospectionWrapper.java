package com.univ.utils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * The Class IntrospectionWrapper.
 */
public class IntrospectionWrapper {

    /** The lst wrapper. */
    private static Hashtable<String, IntrospectionWrapper> lstWrapper = new Hashtable<>();

    /** The lst getter. */
    protected Map<String, Method> lstGetter = new HashMap<>();

    /** The lst setter. */
    protected Map<String, Method> lstSetter = new HashMap<>();

    /**
     * Instantiates a new introspection wrapper.
     *
     * @param classe
     *            the classe
     */
    public IntrospectionWrapper(final Class<?> classe) {
        final Hashtable<String, Method> lstNom = new Hashtable<>();
        final Method[] lstMethod = classe.getMethods();
        //on conserve les noms des propriétés avec getters
        for (final Method element : lstMethod) {
            if (isGetter(element)) {
                lstNom.put(element.getName().substring(3), element);
            }
        }
        //si un setter approprié existe, la méthode est enregistrée
        for (final Method element : lstMethod) {
            if (isSetter(element)) {
                final String nomParametre = element.getName().substring(3);
                if (lstNom.get(nomParametre) != null) {
                    lstGetter.put(nomParametre, lstNom.get(nomParametre));
                    lstSetter.put(nomParametre, element);
                }
            }
        }
    }

    /**
     * Gets the introspection wrapper.
     *
     * @param classe
     *            the classe
     *
     * @return the introspection wrapper
     */
    public static IntrospectionWrapper getIntrospectionWrapper(final Class<?> classe) {
        //instancie le wrapper de la classe s'il n'existe pas
        final String nom = classe.getName();
        IntrospectionWrapper out = lstWrapper.get(nom);
        if (out == null) {
            out = new IntrospectionWrapper(classe);
            lstWrapper.put(nom, out);
        }
        return out;
    }

    /**
     * Checks if is getter.
     *
     * @param methode
     *            the methode
     *
     * @return true, if is getter
     */
    private boolean isGetter(final Method methode) {
        //getter avec 0 paramètre en entrée, qui commence par get
        final String nomMethode = methode.getName();
        if ((nomMethode.startsWith("get")) && (methode.getParameterTypes().length == 0)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if is setter.
     *
     * @param methode
     *            the methode
     *
     * @return true, if is setter
     */
    private boolean isSetter(final Method methode) {
        //methode avec un paramètre en entrée, sans paramètres en sortie
        final String nomMethode = methode.getName();
        if ((nomMethode.startsWith("set")) && (methode.getParameterTypes().length == 1)) {
            return true;
        }
        return false;
    }

    /**
     * Gets the getter.
     *
     * @param nom
     *            the nom
     *
     * @return the getter
     */
    public Method getGetter(final String nom) {
        return lstGetter.get(nom);
    }

    /**
     * Gets the setter.
     *
     * @param nom
     *            the nom
     *
     * @return the setter
     */
    public Method getSetter(final String nom) {
        return lstSetter.get(nom);
    }

    /**
     * Gets the lst getter.
     *
     * @return the lst getter
     */
    public Map<String, Method> getLstGetter() {
        return lstGetter;
    }

    /**
     * Gets the lst setter.
     *
     * @return the lst setter
     */
    public Map<String, Method> getLstSetter() {
        return lstSetter;
    }
}