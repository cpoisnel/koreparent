package com.univ.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.mail.MailFormaterManager;
import com.kportal.cms.mail.MailNotificationFormater;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Metatag;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.processus.SaisieFiche;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.MetatagUtils;

/**
 * The Class MailUtil.
 *
 * @author Romain
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 */
public class MailUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailUtil.class);

    private static MailFormaterManager getFormaterManager() {
        return (MailFormaterManager) ApplicationContextManager.getCoreContextBean(MailFormaterManager.ID_BEAN);
    }

    private static List<MailNotificationFormater> getFormaters(final String name) {
        if (getFormaterManager().getFormaters().get(name) != null) {
            return getFormaterManager().getFormaters().get(name);
        }
        return getFormaterManager().getDefaultFormaters();
    }

    private static ServiceUser getServiceUtilisateur() {
        return ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Envoyer mail retour validation.
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @param meta
     *            the meta
     * @param miseEnLigne
     *            the mise en ligne
     *
     * @throws Exception
     *             the exception
     *             @deprecated Utilisez {@link MailUtil#envoyerMailRetourValidation(InfoBean, FicheUniv, MetatagBean, boolean)}
     */
    @Deprecated
    public static void envoyerMailRetourValidation(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta, final boolean miseEnLigne) throws Exception {
        envoyerMailRetourValidation(infoBean, ficheUniv, meta.getPersistenceBean(), miseEnLigne);
    }

    /**
     * Envoyer mail retour validation.
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @param meta
     *            the meta
     * @param miseEnLigne
     *            the mise en ligne
     *
     * @throws Exception
     *             the exception
     */
    public static void envoyerMailRetourValidation(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta, final boolean miseEnLigne) throws Exception {
        for (final MailNotificationFormater formater : getFormaters(ficheUniv.getClass().getName())) {
            // AA : on utilise l'historique pour savoir qui a fait la demande de validation
            // pour l'envoyer a cet utilisateur en destinataire principal et
            // on met le redacteur de la fiche en copie.
            LOGGER.debug("Parcours de l'historique pour retrouver le demandeur de validation : " + meta.getMetaHistorique());
            final List<String> vHistorique = MetatagUtils.getMetatagService().getHistory(meta);
            String auteurModification = StringUtils.EMPTY;
            // chaque item est forme ainsi : [evenement/dateModif/code_auteur/etat_objet]";
            for (String item : vHistorique) {
                if (item.length() > 0) {
                    item = item.substring(1);
                    final StringTokenizer st = new StringTokenizer(item, "/");
                    if (st.hasMoreTokens()) {
                        //l'action est le premier élément
                        String evenement = st.nextToken();
                        //si l'action est bien une demande de validation, on récupère l'auteur
                        if (evenement.equals(MetatagUtils.HISTORIQUE_DEMANDE_VALIDATION) && st.hasMoreTokens()) {
                            st.nextToken();
                            if (st.hasMoreTokens()) {
                                auteurModification = st.nextToken();
                                break;
                            }
                        }
                    }
                }
            }
            //Si on a trouvé le demandeur, il est destinataire sinon on met le redacteur de la fiche
            final UtilisateurBean validateur = getServiceUtilisateur().getByCode(ContexteUtil.getContexteUniv().getCode());
            String message;
            if (miseEnLigne) {
                message = formater.getMessageRetourMiseEnLigne(infoBean.getValues(), ficheUniv, meta, validateur);
            } else {
                message = formater.getMessageRetourNonValide(infoBean.getValues(), ficheUniv, meta, validateur);
            }
            if (StringUtils.isNotEmpty(message)) {
                //Envoi du mail
                final List<String> lstDest = new ArrayList<>();
                if (StringUtils.isNotBlank(auteurModification)) {
                    LOGGER.debug("Envoi mail retour validation au demandeur : " + auteurModification);
                    final UtilisateurBean ut = getServiceUtilisateur().getByCode(auteurModification);
                    if (ut != null && StringUtils.isNotBlank(ut.getCode())) {
                        lstDest.add(ut.getAdresseMail());
                    }
                }
                //Ajout a la liste des destinataires l'utilisateur anonyme
                if (ficheUniv.getCodeRedacteur().equalsIgnoreCase(ServiceUser.UTILISATEUR_ANONYME) && StringUtils.isNotEmpty(meta.getMetaMailAnonyme())) {
                    lstDest.add(meta.getMetaMailAnonyme());
                }
                if (lstDest.size() > 0) {
                    final String objet = StringUtils.defaultIfEmpty(MessageHelper.getCoreMessage("ST_MAIL_OBJET_RE_DEMANDE_VALIDATION"), "Re: Demande de validation");
                    final ThreadMail tm = new ThreadMail(lstDest, objet, message);
                    final Thread th = new Thread(tm);
                    th.start();
                }
            }
        }
    }

    /**
     * Envoyer mail demande validation.
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @param meta
     *            the meta
     *
     * @throws Exception
     *             the exception
     * @deprecated {@link #envoyerMailDemandeValidation(InfoBean, FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void envoyerMailDemandeValidation(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        envoyerMailDemandeValidation(infoBean, ficheUniv, meta.getPersistenceBean());
    }


    /**
     * Envoyer mail demande validation.
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @param meta
     *            the meta
     *
     * @throws Exception
     *             the exception
     */
    public static void envoyerMailDemandeValidation(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        for (final MailNotificationFormater formater : getFormaters(ficheUniv.getClass().getName())) {
            String chaineValidateur = "";
            if (infoBean.get("LISTE_VALIDATEURS") != null) {
                chaineValidateur = infoBean.getString("LISTE_VALIDATEURS");
            }
            if (!"[AUCUNS]".equalsIgnoreCase(chaineValidateur)) {
                final String codeRubriqueValidation = ficheUniv.getCodeRubrique();
                final String publicsVisesValidation = "";
                String espaceValidation = "";
                // suppression périmètres groupes sur les fiches
                if (ficheUniv instanceof DiffusionSelective && "4".equals(((DiffusionSelective) ficheUniv).getDiffusionModeRestriction())) {
                    espaceValidation = ((DiffusionSelective) ficheUniv).getDiffusionPublicViseRestriction();
                }
                // affinage de l'envoi de mail sur le niveau d'approbation
                String codeAction = "V";
                if (StringUtils.isNotEmpty(meta.getMetaNiveauApprobation())) {
                    codeAction = meta.getMetaNiveauApprobation();
                }
                // structures secondaires
                final Collection<String> mails = getServiceUtilisateur().getListeUtilisateursPossedantPermission(new PermissionBean("FICHE", infoBean.getString("CODE_OBJET"), codeAction), AutorisationBean.getStructuresPerimetreFiche(ficheUniv), codeRubriqueValidation, publicsVisesValidation, espaceValidation, true);
                //recuperation de l'utilisateur demandeur de la validation
                final UtilisateurBean utilisateurDemandeur = getServiceUtilisateur().getByCode(ContexteUtil.getContexteUniv().getCode());
                final String message = formater.getMessageDemandeValidation(infoBean.getValues(), ficheUniv, meta, utilisateurDemandeur);
                if (StringUtils.isNotEmpty(message)) {
                    final List<String> lDestMail = new ArrayList<>();
                    final List<String> vValidateur = Chaine.getVecteurPointsVirgules(chaineValidateur);
                    for (final String validateur : mails) {
                        // filtrage des validateurs si liste spécifiée
                        final String[] tValidateur = validateur.split(";", -2);
                        if (vValidateur.contains(tValidateur[2])) {
                            lDestMail.add(tValidateur[0]);
                        }
                    }
                    // envoi des mails dans un thread parallele
                    final String objet = StringUtils.defaultIfEmpty(MessageHelper.getCoreMessage("ST_MAIL_OBJET_DEMANDE_VALIDATION"), "Demande de validation");
                    final ThreadMail tm = new ThreadMail(lDestMail, objet, message);
                    final Thread th = new Thread(tm);
                    th.start();
                }
            }
        }
    }

    /**
     * Envoyer mail notification.
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @param meta
     *            the meta
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link MailUtil#envoyerMailNotification(InfoBean, FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void envoyerMailNotification(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        envoyerMailNotification(infoBean, ficheUniv, meta.getPersistenceBean());
    }

    /**
     * Envoyer mail notification.
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @param meta
     *            the meta
     *
     * @throws Exception
     *             the exception
     */
    public static void envoyerMailNotification(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        // TODO : supprimer cette boucle et revoir la notion de MailNotificationFormater
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        for (final MailNotificationFormater formater : getFormaters(ficheUniv.getClass().getName())) {
            if (! FicheUnivMgr.isFicheCollaborative(ficheUniv)) {
                final String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
                final String codeRubriqueValidation = ficheUniv.getCodeRubrique();
                final String publicsVisesValidation = "";
                String espaceValidation = "";
                if (ficheUniv instanceof DiffusionSelective && "4".equals(((DiffusionSelective) ficheUniv).getDiffusionModeRestriction())) {
                    espaceValidation = ((DiffusionSelective) ficheUniv).getDiffusionPublicViseRestriction();
                }
                /* JSS 20041031: structures secondaires */
                final Collection<String> mails = getServiceUtilisateur().getListeUtilisateursPossedantPermission(new PermissionBean("FICHE", codeObjet, "N"), AutorisationBean.getStructuresPerimetreFiche(ficheUniv), codeRubriqueValidation, publicsVisesValidation, espaceValidation, true);
                /* rubriques de publication */
                final Collection<String> rubPubs = serviceRubriquePublication.getRubriqueDestByFicheUniv(ficheUniv);
                for (String rubPub : rubPubs) {
                    mails.addAll(getServiceUtilisateur().getListeUtilisateursPossedantPermission(new PermissionBean("FICHE", codeObjet, "N"), AutorisationBean.getStructuresPerimetreFiche(ficheUniv), rubPub, publicsVisesValidation, espaceValidation, true));
                }
                /* recuperation de l'utilisateur à notifier */
                final UtilisateurBean utilisateurDemandeur = getServiceUtilisateur().getByCode(ContexteUtil.getContexteUniv().getCode());
                final String message = formater.getMessageNotification(infoBean.getValues(), ficheUniv, meta, utilisateurDemandeur);
                if (StringUtils.isNotEmpty(message)) {
                    final List<String> lDestMail = new ArrayList<>();
                    for (final String validateur : mails) {
                        // RP20050811 filtrage du code redacteur
                        final String[] tValidateur = validateur.split(";", -2);
                        if (!tValidateur[2].equals(utilisateurDemandeur.getCode())) {
                            lDestMail.add(tValidateur[0]);
                        }
                    }
                    // envoi des mails dans un thread parallele
                    final String objet = StringUtils.defaultIfEmpty(MessageHelper.getCoreMessage("ST_MAIL_OBJET_MISE_EN_LIGNE_FICHE"), "Mise en ligne d'une fiche");
                    final ThreadMail tm = new ThreadMail(lDestMail, objet, message);
                    final Thread th = new Thread(tm);
                    th.start();
                }
            }
        }
    }

    /**
     * Controle si les validateurs possèdent le droit d'approbation choisi.
     *
     * @param processus
     *            the processus
     * @param fiche
     *            the fiche
     * @param chaineUtilisateur
     *            the _chaine utilisateur
     * @param codeApprobation
     *            the _code approbation
     *
     * @return true, if controler liste validateur
     *
     * @throws Exception
     *             the exception
     */
    public static boolean controlerListeValidateur(final SaisieFiche processus, final FicheUniv fiche, final String chaineUtilisateur, final String codeApprobation) throws Exception {
        boolean res = true;
        if (chaineUtilisateur != null && chaineUtilisateur.length() > 0 && !"[AUCUNS]".equals(chaineUtilisateur)) {
            final String codeObjet = ReferentielObjets.getCodeObjet(fiche);
            final String codeRubriqueValidation = fiche.getCodeRubrique();
            final String publicsVisesValidation = "";
            String espaceValidation = "";
            // JSS 20051201 : suppression périmètres groupes sur les fiches
            if (fiche instanceof DiffusionSelective && "4".equals(((DiffusionSelective) fiche).getDiffusionModeRestriction())) {
                espaceValidation = ((DiffusionSelective) fiche).getDiffusionPublicViseRestriction();
            }
            final Collection<String> mails = getServiceUtilisateur().getListeUtilisateursPossedantPermission(new PermissionBean("FICHE", codeObjet, codeApprobation), AutorisationBean.getStructuresPerimetreFiche(fiche), codeRubriqueValidation, publicsVisesValidation, espaceValidation, false);
            final String[] utilisateur = chaineUtilisateur.split(";", -2);
            for (String anUtilisateur : utilisateur) {
                if (!mails.contains(anUtilisateur)) {
                    res = false;
                    break;
                }
            }
        }
        return res;
    }

    /**
     * Check an email format against a pattern. This method is null safe.
     *
     * @param email : the email to test as a {@link String}
     *
     * @return true if the given email matches the pattern, false otherwise.
     */
    public static boolean verifMail(final String email) {
        return StringUtils.isNotBlank(email) && Pattern.matches("[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)" + "*(\\.[_A-Za-z0-9-]+)", email);
    }
}
