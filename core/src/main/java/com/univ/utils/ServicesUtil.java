package com.univ.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.identification.GestionnaireIdentification;
import com.jsbsoft.jtf.identification.ValidateurCAS;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.cache.CacheServiceManager;
import com.univ.objetspartages.om.SSOBean;
import com.univ.objetspartages.om.ServiceBean;
import com.univ.objetspartages.services.ServiceServiceExterne;
import com.univ.services.ReponseService;
import com.univ.services.RequeteService;
import com.univ.services.ServicesInvoker;
import com.univ.xml.NodeUtil;

/**
 * The Class ServicesUtil. Classe utilitaire de gestion des services - fontions de formatage - gestion des caches
 */
public class ServicesUtil {

    /** Url d'appel de la vue maxi d'un service */
    private final static String URL_ACTIVATION_SERVICE = "/adminsite/sso/activation_service.jsp?service=";

    private static final Logger LOG = LoggerFactory.getLogger(ServicesUtil.class);

    public static CacheServiceManager getCacheServiceManager() {
        return (CacheServiceManager) ApplicationContextManager.getCoreContextBean(CacheServiceManager.ID_BEAN);
    }

    public static Map<String, ServiceBean> getServices() {
        Map<String, ServiceBean> res = getCacheServiceManager().call();
        if (res == null) {
            res = new HashMap<>();
        }
        return res;
    }

    /**
     * calcule la liste des codes des services autorisés pour l'utilisateur courant
     *
     * appelée automatiquement à la construction du contexteUniv et disponible dans le contexte.
     *
     * @param ctx
     *            the ctx
     *
     * @return the tree set
     *
     */
    public static TreeSet<String> calculerCodesServicesAutorises(final ContexteUniv ctx) {
        final TreeSet<String> listeServices = new TreeSet<>();
        /** ************************************ */
        /* Parcours de tous les services */
        /** ************************************ */
        for (final ServiceBean service : getServices().values()) {
            boolean serviceAutorise = false;
            /* Contrôle sur les restrictions */
            if ("3".equals(service.getDiffusionModeRestriction())) {
                serviceAutorise = true;
            } else if ("0".equals(service.getDiffusionModeRestriction()) && ctx.getCode().length() > 0) {
                serviceAutorise = true;
            } else {
                Collection<String> groupesRestriction = new ArrayList<>();
                if ("1".equals(service.getDiffusionModeRestriction())) {
                    groupesRestriction = service.getDiffusionPublicVise();
                }
                if ("2".equals(service.getDiffusionModeRestriction())) {
                    groupesRestriction = service.getDiffusionPublicViseRestriction();
                }
                /* Test sur chaque groupe */
                for (String codeGroupeRestriction : groupesRestriction) {
                    if (ctx.getGroupesDsiAvecAscendants().contains(codeGroupeRestriction)) {
                        serviceAutorise = true;
                    }
                }
            }
            if (serviceAutorise) {
                listeServices.add(service.getCode());
            }
        }
        return listeServices;
    }

    /**
     * calcule la liste des services en mode "PUSH" pour l'utilisateur courant.
     *
     * @param ctx
     *            the ctx
     *
     * @return the vector
     *
     */
    public static Vector<ServiceBean> calculerListeServicesPush(final ContexteUniv ctx) {
        // Pour éviter les doublons
        final TreeSet<String> listeCodesServices = new TreeSet<>();
        // Pour le résultat
        final Vector<ServiceBean> vListeServices = new Vector<>();
        /* Parcours de tous les services */
        for (final ServiceBean service : getServices().values()) {
            // Test autorisation
            if (ctx.getListeServicesAutorises().contains(service.getCode())) {
                boolean pushOK = false;
                if ("0".equals(service.getDiffusionMode())) {
                    pushOK = true;
                }
                if ("1".equals(service.getDiffusionMode())) {
                    // Test "push" sur chaque groupe
                    final Collection<String> groupesDiffusion = service.getDiffusionPublicVise();
                    for (String codeGroupeDiffusion : groupesDiffusion) {
                        if (ctx.getGroupesDsiAvecAscendants().contains(codeGroupeDiffusion)) {
                            pushOK = true;
                        }
                    }
                }
                if (pushOK && (!listeCodesServices.contains(service.getCode()))) {
                    listeCodesServices.add(service.getCode());
                    vListeServices.add(service);
                }
            }
        }
        return vListeServices;
    }

    /**
     * Traitement des tags spécifiques aux services.
     *
     * @param ctx
     *            the _ctx
     * @param texte
     *            the _texte
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public static String transformerTagsServices(final ContexteUniv ctx, final String texte) throws Exception {
        // Liste des services à exécuter
        final Map<String, RequeteService> requetesServices = new Hashtable<>();
        String res = transformerTagsServicesHorsContenu(ctx, texte, requetesServices);
        if (requetesServices.size() > 0) {
            final Map<String, ReponseService> reponsesServices = new ServicesInvoker().executer(ctx, requetesServices);
            // Remplacement des requêtes par leur réponse
            for (final String cleRequete : reponsesServices.keySet()) {
                final ReponseService reponse = reponsesServices.get(cleRequete);
                String contenu = "";
                if (reponse != null) {
                    if (reponse.isTerminated()) {
                        contenu = reponse.getReponse();
                    } else {
                        contenu = MessageHelper.getCoreMessage("ST_SERVICES_DELAI_DEPASSE");
                    }
                }
                res = StringUtils.replace(res, cleRequete, contenu);
            }
        }
        return res;
    }

    /**
     * Traitement des tags spécifiques aux services (sauf les contenus qui seront interprétés dans des threads).
     *
     * @param ctx
     *            the _ctx
     * @param values
     *            the _texte
     * @param requetesServices
     *            the _requetes services
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     */
    public static String transformerTagsServicesHorsContenu(final ContexteUniv ctx, final String values, final Map<String, RequeteService> requetesServices) throws Exception {
        int indexDebutMotCle = -1;
        int indexFinMotCle = 0;
        /** ********************************************* */
        /* Condition d'insertion du service */
        /** ********************************************* */
        int indexTexte = 0;
        String texte = values;
        StringBuffer newTexte = new StringBuffer(texte.length() + 100);
        /* Boucle sur chaque mot clé */
        while ((indexDebutMotCle = texte.indexOf("[service_debut", indexTexte)) != -1) {
            // Recopie portion avant le mot- clé
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            final String contenuTag = Chaine.remplacerPointsVirgules(texte.substring(indexDebutMotCle + 1, indexFinMotCle));
            // Analyse du code service
            String codeService = "";
            final StringTokenizer st = new StringTokenizer(contenuTag, "*");
            int indiceToken = 0;
            while (st.hasMoreTokens()) {
                final String itemTag = st.nextToken();
                if (indiceToken == 1) {
                    codeService = itemTag;
                }
                indiceToken++;
            }
            // Recherche du tag de fin
            final String tagFinGroupe = "[service_fin]";
            final int indexFinGroupe = texte.indexOf(tagFinGroupe, indexDebutMotCle);
            // Fin du traitement si tag incomplet
            if (indexFinGroupe == -1) {
                indexTexte = indexDebutMotCle;
                break;
            }
            final int indexDebutMotCleSuivant = texte.indexOf("[service_debut", indexDebutMotCle + 1);
            if ((indexDebutMotCleSuivant != -1) && (indexDebutMotCleSuivant < indexFinGroupe)) {
                indexTexte = indexDebutMotCle;
                break;
            }
            if (StringUtils.isNotEmpty(codeService) && ctx.getListeServicesAutorises().contains(codeService)) {
                ctx.setServiceCourant(codeService);
                newTexte.append(transformerTagsServicesHorsContenu(ctx, texte.substring(indexFinMotCle + 1, indexFinGroupe), requetesServices));
                ctx.setServiceCourant("");
            }
            // Repositionnement apres les services
            indexFinMotCle = texte.indexOf("]", indexFinGroupe);
            indexTexte = indexFinMotCle + 1;
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        /** ************************** */
        /* Transformation des tags */
        /* spécifique à un service */
        /** ************************** */
        if (ctx.getServiceCourant().length() > 0) {
            texte = newTexte.toString();
            indexTexte = 0;
            newTexte = new StringBuffer(texte.length() + 100);
            final ServiceBean service = getService(ctx.getServiceCourant());
            while ((indexDebutMotCle = texte.indexOf("[", indexTexte)) != -1) {
                // Recopie portion avant le mot- clé
                newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
                // Recherche de mot-cle
                indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
                String valeurRemplacement = "";
                boolean estUnMotcle = false;
                String motCle = "";
                if (indexFinMotCle != -1) {
                    motCle = texte.substring(indexDebutMotCle + 1, indexFinMotCle);
                    if (motCle.startsWith("service_contenu")) {
                        String nomVue = "";
                        final int idxPtV = motCle.indexOf(";");
                        if (idxPtV != -1) {
                            nomVue = motCle.substring(idxPtV + 1);
                        }
                        final int iRequete = requetesServices.size();
                        final String cleRequete = "#requete#" + iRequete + "#";
                        requetesServices.put(cleRequete, new RequeteService(service, nomVue));
                        valeurRemplacement = cleRequete;
                        estUnMotcle = true;
                    } else if (motCle.startsWith("service_link")) {
                        String nomLink = "";
                        final int idx = motCle.indexOf(";");
                        if (idx != -1) {
                            nomLink = motCle.substring(idx + 1);
                        } else {
                            // nom du lien non renseigné ==> on prend le nom du service
                            nomLink = service.getIntitule();
                        }
                        final String target = determinerTargetService(ctx, service);
                        final String serviceUrl = determinerUrlAccueilService(service);
                        if (StringUtils.isNotBlank(serviceUrl)) {
                            valeurRemplacement = "<a href=\"" + serviceUrl + "\"";
                            if (target.length() > 0) {
                                valeurRemplacement += " onclick=\"window.open(this.href, '" + target + "'); return false;\"";
                            }
                            valeurRemplacement += ">" + nomLink + "</a>";
                        }
                        estUnMotcle = true;
                    } else if ("service_url".equals(motCle)) {
                        valeurRemplacement = determinerUrlAccueilService(service);
                        estUnMotcle = true;
                    } else if ("service_target".equals(motCle)) {
                        valeurRemplacement = determinerTargetService(ctx, service);
                        estUnMotcle = true;
                    }
                }
                if (estUnMotcle) {
                    newTexte.append(valeurRemplacement);
                    indexTexte = indexFinMotCle + 1;
                } else {
                    newTexte.append("[");
                    indexTexte = indexDebutMotCle + 1;
                }
            } // Recopie dernière portion de texte
            newTexte.append(texte.substring(indexTexte));
        }
        return newTexte.toString();
    }

    /**
     * Ajout paramètres standard pour un service.
     *
     * @param ctx
     *            the _ctx
     * @param service
     *            the _service
     * @param url
     *            the url
     * @param ticket
     *            the ticket
     *
     * @return the string
     *
     */
    public static String ajouterParametresService(final ContexteUniv ctx, final ServiceBean service, final String url, final String ticket) {
        String urlComplete = url;
        urlComplete += !url.contains("?") ? "?" : "&";
        urlComplete += "service=" + service.getCode();
        urlComplete += "&langue=" + ctx.getLangue();
        urlComplete += "&kportal_host=" + URLResolver.getAbsoluteUrl("", false, ctx.getInfosSite());
        String secure = "0";
        if (URLResolver.getAbsoluteUrl("", ctx).startsWith("https")) {
            secure = "1";
        }
        urlComplete += "&secure=" + secure;
        if (ticket.length() > 0) {
            urlComplete += "&kticket=" + ticket;
        }
        return urlComplete;
    }

    /**
     * Détermine pour un service l'url permettant d'accéder à ce service (redirige le cas échéant sur la page de génération de ticket).
     *
     * @param service
     *            the _service
     *
     * @return @throws Exception
     *
     */
    public static String determinerUrlAccueilService(final ServiceBean service) {
        String url = "";
        if (StringUtils.isNotEmpty(service.getUrl())) {
            url = URL_ACTIVATION_SERVICE + service.getCode();
        }
        return url;
    }

    /**
     * Détermine l'url de base d'un service de type url.
     *
     * @param ctx
     *            the _ctx
     * @param service
     *            the _service
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public static String determinerUrlServiceTypeUrl(final ContexteUniv ctx, final ServiceBean service) throws Exception {
        String url = service.getUrl();
        if (url.length() > 0) {
            String kticket = "";
            if ("1".equals(service.getJetonKportal()) || ctx.getKsession().length() > 0) {
                kticket = ServicesUtil.genererTicketService(ctx);
            }
            url = ajouterParametresService(ctx, service, url, kticket);
            // 20051220 JB : mode proxy specifie
            if ("1".equals(service.getProxyCas())) {
                final ValidateurCAS validateurCAS = GestionnaireIdentification.getInstance().getValidateurCAS();
                if (validateurCAS != null) {
                    url = validateurCAS.getUrlCasServiceLogin(url);
                }
            }
        }
        LOG.debug("*** url service appelee : " + url);
        return url;
    }

    /**
     * Valider ticket.
     *
     * @param kticket
     *            the kticket
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public static String validerTicket(final String kticket) throws Exception {
        String ksession = "";
        // Lecture du ticket pour en déduire le ksession
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder builder = factory.newDocumentBuilder();
        final File f = new File(WebAppUtil.getSessionsPath() + File.separator + "kticket_" + kticket + ".xml");
        if (f.exists()) {
            final Document document = builder.parse(f);
            final Node donnees = document.getDocumentElement();
            if (donnees.hasChildNodes()) {
                Node nextFeuille = donnees.getFirstChild();
                while (nextFeuille != null) {
                    if ("KSESSION".equals(nextFeuille.getNodeName())) {
                        ksession = NodeUtil.extraireValeurNode(nextFeuille);
                    }
                    nextFeuille = nextFeuille.getNextSibling();
                }
            }
            // On supprime le ticket
            f.delete();
        } else {
            LOG.error("Le fichier " + f.getName() + " est introuvable.");
        }
        return ksession;
    }

    /**
     * Supprime le ticket.
     *
     * @param kticket
     *            the kticket
     *
     */
    public static void supprimerTicket(final String kticket) {
        final File f = new File(WebAppUtil.getSessionsPath() + File.separator + "kticket_" + kticket + ".xml");
        if (f.exists()) {
            f.delete();
        }
    }

    /**
     * Gets the sSO bean.
     *
     * @param request
     *            the request
     *
     * @return the sSO bean
     *
     * @throws Exception
     *             the exception
     */
    public static SSOBean getSSOBean(final HttpServletRequest request) throws Exception {
        SSOBean ssoBean = null;
        // appel du serveur distant
        if ((request.getParameter("ksite") != null && request.getParameter("ksite").length() > 0) || (request.getParameter("kportal_host") != null && request.getParameter("kportal_host").length() > 0)) {
            ssoBean = getSSOBeanFromRequest(request);
        }
        // validation local
        else {
            final String ksession = validerTicket(request.getParameter("kticket"));
            // Lecture du Bean SSO
            ssoBean = RequeteConnecteur.lireBeanSSO(ksession);
        }
        return ssoBean;
    }

    /**
     * Détermine pour un service la target du service permettant d'accéder à ce service.
     *
     * @param ctx
     *            the _ctx
     * @param service
     *            the _service
     *
     * @return le code du service si url popup = 1 ?!?
     */
    public static String determinerTargetService(final ContexteUniv ctx, final ServiceBean service) {
        String target = "";
        if ("1".equals(service.getUrlPopup())) {
            target = service.getCode();
        }
        return target;
    }

    /**
     * Génération d'un ticket temporaire à partir duquel on retrouvera le ksession.
     *
     * @param ctx
     *            the ctx
     *
     * @return the string
     *
     * @throws ErreurApplicative
     *             the exception
     */
    public static String genererTicketService(final ContexteUniv ctx) throws ErreurApplicative {
        final long random = (long) (Math.random() * 10000.);
        final String ticket = Long.toString(System.currentTimeMillis()) + "_" + random;
        if (StringUtils.isEmpty(ctx.getKsession())) {
            throw new ErreurApplicative("Veuillez vous connecter");
        }
        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.newDocument();
            final Node nodeFiche = document.createElement("KTICKET");
            NodeUtil.addNode(nodeFiche, "KSESSION", ctx.getKsession());
            document.appendChild(nodeFiche);
            final TransformerFactory transFactory = TransformerFactory.newInstance();
            final Transformer transformer = transFactory.newTransformer();
            final DOMSource source = new DOMSource(document);
            final File fichierSortie = new File(WebAppUtil.getSessionsPath() + File.separator + "kticket_" + ticket + ".xml");
            fichierSortie.createNewFile();
            try (FileWriter fw = new FileWriter(fichierSortie)) {
                final StreamResult sr = new StreamResult(fw);
                transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
                transformer.transform(source, sr);
            }
        } catch (TransformerException | ParserConfigurationException | IOException e) {
            throw new ErreurApplicative("Recupération du fichier de session impossible", e);
        }
        return ticket;
    }

    /**
     * appel du SSO kportal.
     *
     * @param request
     *            the request
     *
     * @return le bean SSO (code retour = '0' si OK)
     *
     * @throws Exception
     *             the exception
     */
    private static SSOBean getSSOBeanFromRequest(final HttpServletRequest request) throws Exception {
        final SSOBean bean = new SSOBean();
        final String kticket = request.getParameter("kticket");
        String urlServeur = request.getParameter("ksite");
        if (urlServeur == null || urlServeur.length() == 0) {
            urlServeur = request.getParameter("kportal_host");
        }
        // controle si site de confiance
        if (!validerUrl(urlServeur)) {
            bean.setCodeRetour("600");
            return bean;
        }
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        final InfosSite infosSite = serviceInfosSite.getSiteByHost(request.getServerName());
        final String uri = "/adminsite/sso/validation_ticket.jsp?kticket=" + kticket + "&kreferer=" + URLResolver.getAbsoluteBoUrl("/", infosSite);
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder builder = factory.newDocumentBuilder();
        final String url = urlServeur + uri;
        final Document document = builder.parse(url);
        final Node arbreXML = document.getDocumentElement();
        final NodeList nodeList = arbreXML.getChildNodes();
        if (nodeList != null) {
            /* Analyse du résultat */
            for (int i = 0; i < nodeList.getLength(); i++) {
                final Node donnees = nodeList.item(i);
                if (donnees != null) {
                    if (donnees.getFirstChild() != null) {
                        if (donnees.getNodeName().compareTo("CIVILITE") == 0) {
                            bean.setCivilite(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("CODE_UTILISATEUR_KPORTAL") == 0) {
                            bean.setCodeKportal(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("CODE_GESTION_KPORTAL") == 0) {
                            bean.setCodeGestion(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("EMAIL") == 0) {
                            bean.setEmail(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("GROUPES") == 0) {
                            bean.setGroupes(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("NOM") == 0) {
                            bean.setNom(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("PRENOM") == 0) {
                            bean.setPrenom(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("PROFIL") == 0) {
                            bean.setProfil(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("STRUCTURE") == 0) {
                            bean.setStructure(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("KSESSION") == 0) {
                            bean.setKsession(donnees.getFirstChild().getNodeValue().trim());
                        }
                        if (donnees.getNodeName().compareTo("CODE_RETOUR") == 0) {
                            bean.setCodeRetour(donnees.getFirstChild().getNodeValue().trim());
                        }
                    }
                }
            }
        }
        return bean;
    }

    /**
     * Valider url.
     *
     * @param sUrl
     *            the s url
     *
     * @return true, if successful
     */
    public static boolean validerUrl(final String sUrl) {
        final boolean res = false;
        URL url;
        try {
            url = new URL(sUrl);
        } catch (final MalformedURLException e) {
            return false;
        }
        final String host = url.getHost();
        final String param = PropertyHelper.getCoreProperty("sso.sites_confiance");
        if (param != null) {
            final String aliasHost[] = param.split(";");
            for (final String element : aliasHost) {
                if (host.matches(element)) {
                    return true;
                }
            }
        }
        return res;
    }

    /**
     * Valider site.
     *
     * @param sUrl
     *            the s url
     * @return true, if successful
     */
    public static boolean validerSite(final String sUrl) {
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        try {
            final URL url = new URL(sUrl);
            final InfosSite site = serviceInfosSite.getSiteByHost(url.getHost());
            return site != null && site.getAlias().length() > 0;
        } catch (final MalformedURLException e) {
            return false;
        }
    }

    /**
     * Gets the service.
     *
     * @param codeService
     *            the code service
     *
     * @return the service
     */
    public static ServiceBean getService(final String codeService) {
        if (getServices().containsKey(codeService)) {
            return getServices().get(codeService);
        }
        return new ServiceBean();
    }

    /**
     * Suppression des paramètres d'un service.
     *
     * @param ctx
     *            the ctx
     * @param codeService
     *            the code service
     *
     * @throws Exception
     *             the exception
     */
    public static synchronized void removeInstanceService(final OMContext ctx, final String codeService) throws Exception {
        if (codeService != null) {
            final ServiceServiceExterne serviceServiceExterne = ServiceManager.getServiceForBean(ServiceBean.class);
            ServiceBean serviceBean = serviceServiceExterne.getByCode(codeService);
            if (serviceBean != null) {
                serviceServiceExterne.delete(serviceBean.getId());
            }
        }
    }

    /**
     * Sauvegarde des paramètres d'un service.
     *
     * @param ctx
     *            the ctx
     * @param serviceBean
     *            the service bean
     *
     * @throws Exception
     *             the exception
     */
    public static synchronized void setInstanceService(final OMContext ctx, final ServiceBean serviceBean) throws Exception {
        final ServiceServiceExterne serviceServiceExterne = ServiceManager.getServiceForBean(ServiceBean.class);
        ServiceBean service = serviceServiceExterne.getByCode(serviceBean.getCode());
        if (service == null) {
            service = new ServiceBean();
        }
        /* insertion services */
        service.setCode(serviceBean.getCode());
        service.setIntitule(serviceBean.getIntitule());
        service.setExpirationCache(serviceBean.getExpirationCache());
        service.setJetonKportal(serviceBean.getJetonKportal());
        service.setProxyCas(serviceBean.getProxyCas());
        service.setUrl(serviceBean.getUrl());
        service.setUrlPopup(serviceBean.getUrlPopup());
        service.setVueReduiteUrl(serviceBean.getVueReduiteUrl());
        service.setDiffusionMode(serviceBean.getDiffusionMode());
        service.setDiffusionPublicVise(serviceBean.getDiffusionPublicVise());
        service.setDiffusionModeRestriction(serviceBean.getDiffusionModeRestriction());
        service.setDiffusionPublicViseRestriction(serviceBean.getDiffusionPublicViseRestriction());
        serviceServiceExterne.save(service);
    }
}
