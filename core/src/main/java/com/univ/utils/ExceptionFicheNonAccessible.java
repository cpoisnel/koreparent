package com.univ.utils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
// TODO: Auto-generated Javadoc

/**
 * The Class ExceptionFicheNonAccessible.
 *
 * @author romain
 */
public class ExceptionFicheNonAccessible extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = -8371325345177434619L;

    /**
     * Instantiates a new exception fiche non accessible.
     *
     * @param _message
     *            the _message
     */
    public ExceptionFicheNonAccessible(final String _message) {
        super(_message);
    }
}
