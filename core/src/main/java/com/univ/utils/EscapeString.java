package com.univ.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.codecs.MySQLCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.lang.CharEncoding;

/**
 * Classe centralisant les échappements de caractères pour éviter les XSS, SQLInjection...
 *
 * @author olivier.camon
 *
 */
public final class EscapeString {

    private static final Logger LOG = LoggerFactory.getLogger(EscapeString.class);

    /** The Constant EVENTS_JS. */
    private static final String[] EVENTS_JS = {"onblur", "onchange", "onclick", "ondblclick", "onerror", "onfocus", "onkeydown", "onkeypress", "onkeyup", "onload", "onmousedown", "onmousemove", "onmouseout", "onmouseover", "onmouseup", "onresize", "onselect", "onunload"};

    /** The Constant BALISE_JS. */
    private static final String BALISE_JS = "script";

    /** The Constant CODEC_MYSQL. */
    private static final MySQLCodec CODEC_MYSQL = new MySQLCodec(MySQLCodec.Mode.STANDARD);

    /** The Constant CODEC_URL. */
    private static final URLCodec CODEC_URL = new URLCodec(CharEncoding.DEFAULT);

    private EscapeString() {
        //on instancie pas cette classe utilitaire.
    }

    /**
     * Evite les XSS lorsque la chaine est inserée entre deux balises html.
     *
     * @param chaineAEchaper
     *            the chaine a echaper
     * @return the string
     */
    public static String escapeHtml(String chaineAEchaper) {
        return ESAPI.encoder().encodeForHTML(chaineAEchaper);
    }

    /**
     * Evite les XSS lorsque la chaine est inserée dans un attribut html.
     *
     * @param chaineAEchaper
     *            the chaine a echaper
     * @return the string
     */
    public static String escapeAttributHtml(String chaineAEchaper) {
        return ESAPI.encoder().encodeForHTMLAttribute(chaineAEchaper);
    }

    /**
     * Evite les XSS lorsque la chaine est inserée dans du javascript.
     *
     * @param chaineAEchaper
     *            the chaine a echaper
     * @return the string
     */
    public static String escapeJavaScript(String chaineAEchaper) {
        return ESAPI.encoder().encodeForJavaScript(chaineAEchaper);
    }

    /**
     * Evite les XSS lorsque la valeur este insérée dans du code CSS.
     *
     * @param chaineAEchaper
     *            the chaine a echaper
     * @return the string
     */
    public static String escapeCSS(String chaineAEchaper) {
        return ESAPI.encoder().encodeForCSS(chaineAEchaper);
    }

    /**
     * Evite les XSS lorsque la valeur est insérée dans une URL.
     *
     * @param chaineAEchaper
     *            the chaine a echaper
     * @return the string
     */
    public static String escapeURL(String chaineAEchaper) {
        String valeurEchapper = null;
        try {
            valeurEchapper = CODEC_URL.encode(chaineAEchaper);
        } catch (EncoderException e) {
            LOG.debug("unable to encode " + chaineAEchaper, e);
            valeurEchapper = chaineAEchaper;
        }
        return valeurEchapper;
    }

    /**
     * Echape tout les caractères spécifiques d'une chaine a inserer dans une requete sqL exemple : ' -> '', _ -> \_, - -> \- ...
     *
     * @param chaineAEchaper
     *            the chaine a echaper
     * @return the string
     */
    public static String escapeSqlValue(String chaineAEchaper) {
        return ESAPI.encoder().encodeForSQL(CODEC_MYSQL, chaineAEchaper);
    }

    /**
     * N'échappe que les ' et les \ des requêtes SQL.
     *
     * @param chaineAEchaper
     *            the chaine a echaper
     * @return the string
     */
    public static String escapeSql(String chaineAEchaper) {
        chaineAEchaper = StringUtils.replace(chaineAEchaper, "\\", "\\\\");
        return StringUtils.replace(chaineAEchaper, "'", "''");
    }

    /**
     * Unescape html.
     *
     * @param chaineADecoder
     *            the chaine a decoder
     * @return the string
     */
    public static String unescapeHtml(String chaineADecoder) {
        return ESAPI.encoder().decodeForHTML(chaineADecoder);
    }

    /**
     * Unescape url.
     *
     * @param chaineAEDecoder
     *            the chaine ae decoder
     * @return the string
     */
    public static String unescapeURL(String chaineAEDecoder) {
        String valeurEchapper = null;
        try {
            valeurEchapper = new String(CODEC_URL.decode(chaineAEDecoder.getBytes(CharEncoding.DEFAULT_CHARSET)));
        } catch (DecoderException e) {
            LOG.debug("unable to decode " + chaineAEDecoder, e);
            valeurEchapper = chaineAEDecoder;
        }
        return valeurEchapper;
    }

    /**
     * Echappe la chaine si elle possède au moins une balise <script> ou un élément html avec une action onEvent.
     *
     * @param valeur
     *            the valeur
     * @return the string
     */
    public static String escapeScriptAndEvent(String valeur) {
        String chaine = valeur.toLowerCase();
        // recherche de toutes les balises html <balise> ... </balise>
        Pattern p = Pattern.compile("<([a-z]+)(.*?)>", Pattern.DOTALL);
        Matcher m = p.matcher(chaine);
        boolean escape = false;
        while (m.find()) {
            String tag1 = m.group(1);
            if (tag1.equals(BALISE_JS)) {
                escape = true;
                break;
            }
            String tag2 = m.group(2);
            if (StringUtils.isNotEmpty(tag2)) {
                for (String event : EVENTS_JS) {
                    if (tag2.contains(event)) {
                        escape = true;
                        break;
                    }
                }
            }
        }
        if (escape) {
            valeur = valeur.replaceAll("<", "&lt;");
            valeur = valeur.replaceAll(">", "&gt;");
        }
        return valeur;
    }
}
