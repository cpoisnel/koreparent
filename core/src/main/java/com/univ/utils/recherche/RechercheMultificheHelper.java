package com.univ.utils.recherche;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;
import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.kportal.extension.module.plugin.rubrique.impl.BeanFichePageAccueil;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.url.bean.UrlBean;
import com.univ.url.service.ServiceUrl;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseJoin;
import com.univ.utils.sql.clause.ClauseJoin.TypeJointure;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.LimitHelper;
import com.univ.utils.sql.criterespecifique.RequeteSQLHelper;
import com.univ.utils.sql.operande.TypeOperande;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Classe helper de recherche de fiche univ avec critères multiples (recherche directe)
 *
 */
public class RechercheMultificheHelper {

    private static final Logger LOG = LoggerFactory.getLogger(RechercheMultificheHelper.class);

    /** On récupère l'id métatag qui est précédé de n'importe quel caratère puis un - . Il est suivi d'un . et de 0 ou plusieurs caratères non numériques et optionnel un "?" suivi de tous caractères.**/
    public static final String REGEXP_RECHERCHE_META = ".*-(\\d{1,})\\.(?:\\D+)?(?:\\?.*)?$";

    public static ResultatRechercheMultifiche rerchercherParmisToutesLesFiches(final OMContext ctx, final AutorisationBean autorisations, final RequeteMultifiche requete) throws ErreurApplicative {
        return rerchercherParmisToutesLesFiches(ctx, autorisations, requete.getsLibelle(), requete.getsCodeObjet(), requete.getsCodeFiche(), requete.getsCodeRubrique(), requete.getsCodeRattachement(), requete.getsCodeRedacteur(), requete.getsIdMeta(), requete.getUrlFiche(), requete.getDateDebutCreation(), requete.getDateFinCreation(), requete.getDateDebutModification(), requete.getDateFinModification(), requete.getDateDebutMiseEnLigne(), requete.getDateFinMiseEnLigne());
    }

    public static ResultatRechercheMultifiche rerchercherParmisToutesLesFiches(final OMContext ctx, final AutorisationBean autorisations, final String sLibelle, final String sCodeObjet, final String sCodeFiche, final String sCodeRubrique, final String sCodeRattachement, final String sCodeRedacteur, final String urlFiche, final Date dateDebutCreation, final Date dateFinCreation, final Date dateDebutModification, final Date dateFinModification, final Date dateDebutMiseEnLigne, final Date dateFinMiseEnLigne) throws ErreurApplicative {
        return rerchercherParmisToutesLesFiches(ctx, autorisations, sLibelle, sCodeObjet, sCodeFiche, sCodeRubrique, sCodeRattachement, sCodeRedacteur, "", urlFiche, dateDebutCreation, dateFinCreation, dateDebutModification, dateFinModification, dateDebutMiseEnLigne, dateFinMiseEnLigne);
    }

    public static ResultatRechercheMultifiche rerchercherParmisToutesLesFiches(final OMContext ctx, final AutorisationBean autorisations, final String sLibelle, final String sCodeObjet, final String sCodeFiche, final String sCodeRubrique, final String sCodeRattachement, final String sCodeRedacteur, final String sIdMeta, final String urlFiche, final Date dateDebutCreation, final Date dateFinCreation, final Date dateDebutModification, final Date dateFinModification, final Date dateDebutMiseEnLigne, final Date dateFinMiseEnLigne) throws ErreurApplicative {
        return rerchercherParmisToutesLesFiches(ctx, autorisations, sLibelle, sCodeObjet, sCodeFiche, sCodeRubrique, sCodeRattachement, sCodeRedacteur, sIdMeta, urlFiche, dateDebutCreation, dateFinCreation, dateDebutModification, dateFinModification, dateDebutMiseEnLigne, dateFinMiseEnLigne, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
    }

    /**
     * Execute une recherche de fiches univ multicritères. Execute en priorité une recherche sur l'idMeta si il est présent, sur urlFiche si elle est présente et que c'est une url
     * de fiche (pas une url de rubrique) et sur les autres critères sinon
     *
     * @param ctx
     *            Le contexte KPortal
     * @param autorisations
     *            Une instance de AutorisationBean permettant de ne ramener que les fiches autorisées
     * @param sLibelle
     *            Le libelle de la fiche univ recherchée
     * @param sCodeObjet
     *            Le code objet de la fiche univ recherchée
     * @param sCodeFiche
     *            Le code fiche de la fiche univ recherchée
     * @param sCodeRubrique
     *            Le code de la rubrique de la fiche univ recherchée
     * @param sCodeRattachement
     *            Le code rattachement de la fiche univ recherchée
     * @param sCodeRedacteur
     *            Le code rédacteur de la fiche univ recherchée
     * @param sIdMeta
     *            L'id du meta de la fiche univ recherchée
     * @param urlFiche
     *            L'url de la fiche univ recherchée
     * @param dateDebutCreation
     *            La borne inférieure de la date de création de la fiche univ recherchée
     * @param dateFinCreation
     *            La borne supérieure de la date de création de la fiche univ recherchée
     * @param dateDebutModification
     *            La borne inférieure de la date de modification de la fiche univ recherchée
     * @param dateFinModification
     *            La borne supérieure de la date de modification de la fiche univ recherchée
     * @param dateDebutMiseEnLigne
     *            La borne inférieure de la date de mise en ligne de la fiche univ recherchée
     * @param dateFinMiseEnLigne
     *            La borne supérieure de la date de mise en ligne de la fiche univ recherchée
     * @return Un objet ResultatRechercheMultifiche contenant les résultats de la recherche
     * @throws ErreurApplicative
     */
    public static ResultatRechercheMultifiche rerchercherParmisToutesLesFiches(final OMContext ctx, final AutorisationBean autorisations, final String sLibelle, String sCodeObjet, String sCodeFiche, final String sCodeRubrique, final String sCodeRattachement, final String sCodeRedacteur, final String sIdMeta, final String urlFiche, final Date dateDebutCreation, final Date dateFinCreation, final Date dateDebutModification, final Date dateFinModification, final Date dateDebutMiseEnLigne, final Date dateFinMiseEnLigne, final String langue, final String etatObjet, final String nombre) throws ErreurApplicative {
        final Collection<MetatagBean> lesMetas = new ArrayList<>();
        // On enleve les paramètres de requete de l'url
        final String urlFicheSansParametres = StringUtils.substringBefore(urlFiche, "?");
        // l'url est renseignee, on trouve la fiche à partir de l'id meta
        // present
        // dans l'url (si c'est une url de fiche)
        if (StringUtils.isNotBlank(urlFicheSansParametres) && !urlFicheSansParametres.endsWith("/")) {
            try {
                final MetatagBean metatagRecherche = rechercheMetaParUrlFiche(autorisations, urlFicheSansParametres);
                if (metatagRecherche != null) {
                    lesMetas.add(metatagRecherche);
                }
            } catch (final ErreurApplicative e) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_URL_INVALIDE"), e);
            }
        } else {
            // Sinon formulaire de recherche transversale
            // Dans le cas ou une url de rubrique est saisie on récupere le
            // codeFiche et le codeObjet en fonction de l'url
            URI uri = null;
            if (urlFicheSansParametres != null) {
                try {
                    uri = new URI(urlFicheSansParametres);
                } catch (final URISyntaxException e) {
                    LOG.error("unable to parse the given URL", e);
                }
            }
            if (uri != null && (StringUtils.isEmpty(uri.getPath()) || uri.getPath().endsWith("/"))) {
                // url de rubrique
                final String codeRubrique = getCodeRubrique(urlFicheSansParametres, uri);
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                final RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(codeRubrique);
                BeanPageAccueil beanAccueil = null;
                if (rubriqueCourante != null) {
                    beanAccueil = PageAccueilRubriqueManager.getInstance().getBeanPageAccueil(rubriqueCourante);
                }
                if (beanAccueil != null && beanAccueil instanceof BeanFichePageAccueil) {
                    sCodeFiche = ((BeanFichePageAccueil) beanAccueil).getCode();
                    sCodeObjet = ReferentielObjets.getCodeObjet(((BeanFichePageAccueil) beanAccueil).getObjet());
                    lesMetas.addAll(rechercherParCriteresMultiples(ctx, autorisations, sLibelle, sCodeObjet, sCodeFiche, sCodeRubrique, sCodeRattachement, sCodeRedacteur, dateDebutCreation, dateFinCreation, dateDebutModification, dateFinModification, dateDebutMiseEnLigne, dateFinMiseEnLigne, langue, etatObjet, nombre));
                }
            } else {
                // execution de la recherche multicriteres
                lesMetas.addAll(rechercherParCriteresMultiples(ctx, autorisations, sLibelle, sCodeObjet, sCodeFiche, sCodeRubrique, sCodeRattachement, sCodeRedacteur, dateDebutCreation, dateFinCreation, dateDebutModification, dateFinModification, dateDebutMiseEnLigne, dateFinMiseEnLigne, langue, etatObjet, nombre));
            }
        }
        final ResultatRechercheMultifiche result = new ResultatRechercheMultifiche();
        result.getResultats().addAll(lesMetas);
        result.setCodeObjet(sCodeObjet);
        return result;
    }

    private static String getCodeRubrique(final String urlRecherche, final URI uri) {
        final ServiceUrl serviceUrl = ServiceManager.getServiceForBean(UrlBean.class);
        String codeRubrique = serviceUrl.findSection(urlRecherche);
        if (StringUtils.isEmpty(codeRubrique)) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final String host = uri.getHost();
            codeRubrique = serviceInfosSite.getSiteByHost(host).getCodeRubrique();
        }
        return codeRubrique;
    }

    /**
     * Recherche un Metatag en fonction de l'url d'une fiche univ et des autorisations sur la fiche correspondante.
     *
     * @param autorisations
     *            L'objet AutorisationBean qui permettra de vérifier les droits sur la fiche liée au méta.
     * @param urlFiche
     *            L'url de la fiche dont on recherche le metatag.
     * @return Le Metatag correspondant à la recherche ou null.
     * @throws ErreurApplicative
     */
    private static MetatagBean rechercheMetaParUrlFiche(final AutorisationBean autorisations, final String urlFiche) throws ErreurApplicative {
        final Pattern pattern = Pattern.compile(REGEXP_RECHERCHE_META);
        final Matcher m = pattern.matcher(urlFiche);
        if (m.matches()) {
            final Long idMeta = Long.valueOf(m.group(1));
            return rechercheMetaParIdMeta(autorisations, idMeta);
        } else {
            return null;
        }
    }

    /**
     * Recherche un Metatag en fonction de son id et des autorisations sur la fiche correspondante.
     *
     * @param autorisations
     *            L'objet AutorisationBean qui permettra de vérifier les droits sur la fiche liée au méta
     * @param idMeta
     *            L'id du Metatag recherché
     * @return Le Metatag correspondant à la recherche ou null
     * @throws ErreurApplicative
     */
    private static MetatagBean rechercheMetaParIdMeta(final AutorisationBean autorisations, final Long idMeta) throws ErreurApplicative {
        final MetatagBean meta = MetatagUtils.getMetatagService().getById(idMeta);
        if (meta != null) {
            final FicheUniv ficheUniv = FicheUnivMgr.init(meta);
            if (!autorisations.estAutoriseAModifierLaFiche(ficheUniv)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_OPERATION_INTERDITE"));
            }
        }
        return meta;
    }

    /**
     * Ajoute un critere limitant la recherche sur colonne de type date bornée par les deux dates en paramètre
     *
     * @param nomColonne
     *            Le nom de la colonne date que l'on veut bornée
     * @param dateDebut
     *            La borne inferieure (exclusive)
     * @param dateFin
     *            La borne superieure(exclusive)
     * @return La condition créé.
     */
    private static Condition traiterDate(final String nomColonne, final Date dateDebut, final Date dateFin) {
        final ConditionList conditionSurDate = new ConditionList();
        if (dateDebut != null) {
            conditionSurDate.setPremiereCondtion(ConditionHelper.greaterThan(nomColonne, dateDebut, TypeOperande.DATE));
        }
        if (dateFin != null) {
            conditionSurDate.and(ConditionHelper.lessThan(nomColonne, dateFin, TypeOperande.DATE));
        }
        return conditionSurDate;
    }

    /**
     * Execute une recherche de fiches univ multicritères.
     *
     * @param ctx
     *            Le contexte KPortal
     * @param autorisations
     *            Une instance de AutorisationBean permettant de ne ramener que les fiches autorisées
     * @param libelle
     *            Le libelle de la fiche univ recherchée
     * @param codeObjet
     *            Le code objet de la fiche univ recherchée
     * @param codeFiche
     *            Le code fiche de la fiche univ recherchée
     * @param codeRubrique
     *            Le code de la rubrique de la fiche univ recherchée
     * @param codeRattachement
     *            Le code rattachement de la fiche univ recherchée
     * @param codeRedacteur
     *            Le code rédacteur de la fiche univ recherchée
     * @param debutCreation
     *            La borne inférieure de la date de création de la fiche univ recherchée
     * @param finCreation
     *            La borne supérieure de la date de création de la fiche univ recherchée
     * @param debutModification
     *            La borne inférieure de la date de modification de la fiche univ recherchée
     * @param finModification
     *            La borne supérieure de la date de modification de la fiche univ recherchée
     * @param debutMiseEnLigne
     *            La borne inférieure de la date de mise en ligne de la fiche univ recherchée
     * @param finMiseEnLigne
     *            La borne supérieure de la date de mise en ligne de la fiche univ recherchée
     * @return Un objet ResultatRechercheMultifiche contenant les résultats de la recherche
     */
    private static Collection<MetatagBean> rechercherParCriteresMultiples(final OMContext ctx, final AutorisationBean autorisations, final String libelle, final String codeObjet, final String codeFiche, final String codeRubrique, final String codeRattachement, final String codeRedacteur, final Date debutCreation, final Date finCreation, final Date debutModification, final Date finModification, final Date debutMiseEnLigne, final Date finMiseEnLigne, final String langue, final String etatObjet, final String nombre) {
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseWhere where = new ClauseWhere();
        if (isNotEmpty(libelle)) {
            where.setPremiereCondition(ConditionHelper.rechercheMots("T1.META_LIBELLE_FICHE", libelle));
        }
        if (isNotEmpty(codeRedacteur)) {
            where.and(ConditionHelper.egalVarchar("T1.META_CODE_REDACTEUR", codeRedacteur));
        }
        if (isNotEmpty(codeFiche)) {
            where.and(ConditionHelper.egalVarchar("T1.META_CODE", codeFiche));
        }
        if (isNotEmpty(codeRattachement)) {
            where.and(ConditionHelper.getConditionStructure("T1.META_CODE_RATTACHEMENT", codeRattachement));
        }
        if (isNotEmpty(langue)) {
            if (!"0000".equals(langue)) {
                where.and(ConditionHelper.egalVarchar("T1.META_LANGUE", langue));
            }
        }
        if (isNotEmpty(etatObjet)) {
            if (!"0000".equals(etatObjet)) {
                where.and(ConditionHelper.egalVarchar("T1.META_ETAT_OBJET", etatObjet));
            }
        }
        where.and(traiterDate("T1.META_DATE_CREATION", debutCreation, finCreation));
        where.and(traiterDate("T1.META_DATE_MODIFICATION", debutModification, finModification));
        where.and(traiterDate("T1.META_DATE_MISE_EN_LIGNE", debutMiseEnLigne, finMiseEnLigne));
        if (isNotEmpty(codeObjet) && !"0000".equals(codeObjet)) {
            final FicheUniv fiche = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(codeObjet));
            where.and(RequeteSQLHelper.traiterConditionDsiMeta(autorisations, fiche, "", codeRubrique, null));
        } else {
            final ConditionList conditionSurObjets = new ConditionList();
            for (final String nomObjet : ReferentielObjets.getListeNomsObjet()) {
                final FicheUniv fiche = ReferentielObjets.instancierFiche(nomObjet);
                if (fiche != null && FicheAnnotationHelper.isAccessibleBo(fiche)) {
                    fiche.setCodeRubrique(codeRubrique);
                    conditionSurObjets.or(RequeteSQLHelper.traiterConditionDsiMeta(autorisations, fiche, "", codeRubrique, null));
                }
            }
            where.and(conditionSurObjets);
        }
        where.and(ConditionHelper.getConditionRubPubSuivantAction(ctx, "", "T1.META_CODE_RUBRIQUE", codeRubrique));
        requeteSelect.where(where);
        requeteSelect.join(jointurePourClauseWhere(where));
        requeteSelect.limit(LimitHelper.ajouterCriteresLimitesEtOptimisation(ctx, nombre));
        return MetatagUtils.getMetatagService().getFromRequest(requeteSelect);
    }

    private static ClauseJoin jointurePourClauseWhere(final ClauseWhere where) {
        final ClauseJoin joinRubPub = new ClauseJoin();
        if (where.formaterSQL().contains("RUB_PUB.")) {
            joinRubPub.setTypeJointure(TypeJointure.LEFT_JOIN);
            joinRubPub.setNomTable("RUBRIQUEPUBLICATION RUB_PUB");
            joinRubPub.on(ConditionHelper.critereJointureSimple("T1.META_CODE", "RUB_PUB.CODE_FICHE_ORIG"));
            joinRubPub.and(ConditionHelper.critereJointureSimple("T1.META_LANGUE", "RUB_PUB.LANGUE_FICHE_ORIG"));
            joinRubPub.and(ConditionHelper.critereJointureSimple("T1.META_CODE_OBJET", "RUB_PUB.TYPE_FICHE_ORIG"));
        }
        return joinRubPub;
    }
}
