package com.univ.utils.cache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import com.univ.utils.ContexteUtil;
import com.univ.utils.RequeteUtil;

/**
 * Classe représentant une requête de type lien de requêtes ou liste d'objets à laquelle on a associé un cache.
 */
public class ObjetsCacheRequest implements ICacheRequest {

    public static final String ID_BEAN = "objetsCacheRequest";

    /**
     * Exécute la requête.
     *
     * @return the object
     *
     * @throws Exception
     *             On fait suivre l'exception de RequeteUtil (pas de mise en cache dans ce cas)
     */
    @Override
    @Cacheable(value= "ObjetsCacheRequest.cache", key = "#kSession + #key")
    public Object call(String kSession, String key, Object... objects) throws Exception {
        return perform(objects);
    }

    @Override
    @CacheEvict(value = "ObjetsCacheRequest.cache")
    public void flush() throws Exception {}

    @Override
    public Object perform(Object... objects) throws Exception {
        return RequeteUtil.traiterRequete(ContexteUtil.getContexteUniv(), (String) objects[0]);
    }

    @Override
    public String getKey(Object... objects) {
        return (String) objects[0];
    }
}
