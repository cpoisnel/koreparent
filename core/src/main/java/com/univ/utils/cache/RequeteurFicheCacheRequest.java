package com.univ.utils.cache;

import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import com.univ.utils.AbstractRequeteur;
import com.univ.utils.IRequeteurConstantes;

/**
 * The Class RequeteurFicheCacheRequest.
 */
public class RequeteurFicheCacheRequest implements ICacheRequest {

    private static final Logger LOG = LoggerFactory.getLogger(RequeteurFicheCacheRequest.class);

    public static final String ID_BEAN = "requeteurFicheCacheRequest";

    @Override
    @Cacheable(value = "RequeteurFicheCacheRequest.cache", key = "#ksession + #key")
    public Object call(final String kSession, final String key, final Object... objects) {
        return perform(objects);
    }

    /**
     * Gets the liste fiches.
     *
     * @return the liste fiches
     *
     */
    public TreeMap<String, Object> getListeFiches(final AbstractRequeteur requeteur, final String codeTri, final String ordreTri, final int from, final int increment, final String count) {
        TreeMap<String, Object> listeFiches = new TreeMap<>();
        final TreeMap<String, Object> resultCache = requeteur.getListeFiches(codeTri, ordreTri, from, increment, count);
        /*
         * Tableaux de bords au comportement standard cad qu'on retrieve
         * tous les fiches renvoyés par le select
         */
        if (count.equals(IRequeteurConstantes.REQUETE_COUNT)) {
            // en mode count, le resultat ne renvoie qu'un élément, on
            // renvoie la liste telle quelle
            // !il peut y avoir un diff avec le nb de fiches réelles
            listeFiches = resultCache;
        } else {
            int compteur = IRequeteurConstantes.NB_RESULTAT + 1;
            for (final Map.Entry<String, Object> result : resultCache.entrySet()) {
                final String idMeta = result.getKey();
                // on traite les éléments (sf si c'est le nombre de resultat)
                if (!idMeta.equals(Integer.toString(IRequeteurConstantes.NB_RESULTAT))) {
                    // Récupération ds infos fiches
                    listeFiches.put(Integer.toString(compteur), result.getValue());
                    compteur++;
                }
            }
            if (count.equals(IRequeteurConstantes.REQUETE_AVEC_COUNT)) {
                listeFiches.put(Integer.toString(IRequeteurConstantes.NB_RESULTAT), resultCache.get(Integer.toString(IRequeteurConstantes.NB_RESULTAT)));
            }
        }
        return listeFiches;
    }

    @Override
    @CacheEvict(value = "RequeteurFicheCacheRequest.cache")
    public void flush() throws Exception {}

    @Override
    public Object perform(final Object... objects) {
        return getListeFiches((AbstractRequeteur) objects[0], (String) objects[1], (String) objects[2], (int) objects[3], (int) objects[4], (String) objects[5]);
    }

    @Override
    public String getKey(final Object... objects) {
        return ((AbstractRequeteur) objects[0]).genererUrlRequete();
    }
}
