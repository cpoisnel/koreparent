package com.univ.utils;

import java.util.Map;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.IUtilisateurMapping;
import com.univ.objetspartages.om.Utilisateur;
// TODO: Auto-generated Javadoc

/**
 * Interface permettant de synchroniser les utilisateurs metier avec l'utilisateur K. (et inversement), et de créer une map des informations de session (chargées dans le contexte
 * en front).
 *
 * Note : un utilisateur peut avoir plusieurs utilisateurs metier rattaches.
 *
 * Important : ne prend pas en charge la suppression d'utilisateur(s) metier lors de la suppression d'un utilisateur K, ni l'ajout car on ne peut connaitre les donnees des
 * utilisateurs metier.
 *
 * @author jbiard
 */
public interface ISynchroniseurUtilisateurMetier {

    /** The Constant SESSION_UTILISATEUR_CHARGEMENT_CLASSE_LIBELLE_JTF. */
    String SESSION_UTILISATEUR_CHARGEMENT_CLASSE_LIBELLE_JTF = "utilisateur_metier";

    /**
     * Appelee lors de la mise a jour d'un utilisateur metier.
     *
     * Cree / met à jour l'utilisateur K. et si besoin synchronise les elements communs avec les autres utilisateurs metier rattaches.
     *
     * @param utilisateurMapping
     *            objet mappe a partir duquel on met a jour l'utilisateur.
     * @param codeUtilisateur
     *            code de l'utilisateur
     * @param mdp
     *            mot de passe de l'utilisateur
     * @param ctx
     *            the ctx
     *
     * @return vrai si un utilisateur a été créé ou bien mis à jour.
     *
     * @throws Exception
     *             the exception
     */
    boolean majUtilisateur(IUtilisateurMapping utilisateurMapping, String codeUtilisateur, String mdp, OMContext ctx) throws Exception;

    /**
     * Appelee lors de la mise a jour d'un utilisateur K.
     *
     * @param utilisateur
     *            utilisateur a partir duquel on met a jour l'objet.
     * @param ctx
     *            the ctx
     *
     * @return vrai si un objet a été mis à jour.
     *
     * @throws Exception
     *             the exception
     */
    boolean majUtilisateurMetier(UtilisateurBean utilisateur, OMContext ctx) throws Exception;
    // parametre du jtf précisant quelle classe prend en charge la synchro
    // le format est utilisateur_metier.nom_applicatif.classe (ex utilisateur_metier.kdecole.classe)

    /**
     * Appelée pour la suppression d'un utilisateur métier.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur à supprimer
     * @param utilisateurMetier
     *            the utilisateur metier
     * @param ctx
     *            the ctx
     *
     * @return vrai si la suppression a été effectuée.
     *
     * @throws Exception
     *             the exception
     */
    boolean suppressionUtilisateur(IUtilisateurMapping utilisateurMetier, String codeUtilisateur, OMContext ctx) throws Exception;

    /**
     * Alimente une map avec les informations propres à l'utilisateur metier, Celle-ci est chargée au login de l'utilisateur K. dans la session.
     *
     * @param utilisateur
     *            utilisateur charge
     * @param mapInfosUser
     *            map des infos de l'utilisateur au chargement de session
     * @param ctx
     *            the ctx
     * @param bIdentificationFront
     *            indique s'il s'agit ou non d'une ident front
     *
     * @return map des infos chargees
     *
     * @throws Exception
     *             the exception
     *
     * @see com.jsbsoft.jtf.identification.GestionnaireIdentification
     * @deprecated Utilisez {@link ISynchroniseurUtilisateurMetier#getInfosSessionUtilisateurMetier(UtilisateurBean, Map, OMContext, boolean)}
     */
    @Deprecated
    Map getInfosSessionUtilisateurMetier(Utilisateur utilisateur, Map<String, Object> mapInfosUser, OMContext ctx, boolean bIdentificationFront) throws Exception;

    /**
     * Alimente une map avec les informations propres à l'utilisateur metier, Celle-ci est chargée au login de l'utilisateur K. dans la session.
     *
     * @param utilisateur
     *            utilisateur charge
     * @param mapInfosUser
     *            map des infos de l'utilisateur au chargement de session
     * @param ctx
     *            the ctx
     * @param bIdentificationFront
     *            indique s'il s'agit ou non d'une ident front
     *
     * @return map des infos chargees
     *
     * @throws Exception
     *             the exception
     *
     * @see com.jsbsoft.jtf.identification.GestionnaireIdentification
     */
    Map getInfosSessionUtilisateurMetier(UtilisateurBean utilisateur, Map<String, Object> mapInfosUser, OMContext ctx, boolean bIdentificationFront) throws Exception;
}