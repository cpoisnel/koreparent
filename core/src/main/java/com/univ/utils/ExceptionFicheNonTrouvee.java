package com.univ.utils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
// TODO: Auto-generated Javadoc

/**
 * The Class ExceptionFicheNonTrouvee.
 *
 * @author romain
 */
public class ExceptionFicheNonTrouvee extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = 982577047128934854L;

    /**
     * Instantiates a new exception fiche non trouvee.
     *
     * @param _message
     *            the _message
     */
    public ExceptionFicheNonTrouvee(final String _message) {
        super(_message);
    }

    /**
     * Instantiates a new exception fiche non trouvee.
     *
     * @param _message
     *            Le message de l'exception
     * @param t l'origine de l'exception
     */
    public ExceptionFicheNonTrouvee(final String _message, Throwable t) {
        super(_message, t);
    }
}
