package com.univ.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe permettant de manipuler des dates.
 *
 * @author fred
 */
public class DateUtil {

    /** Formatteur de date : format dd/MM/yyyy. */
    private static final DateFormat DD_MM_YYYY = new SimpleDateFormat("dd/MM/yyyy");

    private static final DateFormat DATE_AND_TIME = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private static final DateFormat HOUR_MIN = new SimpleDateFormat("HH:mm");

    private static final DateFormat TIME = new SimpleDateFormat("HH:mm:ss");

    private static final DateFormat DATE_FOR_SQL = new SimpleDateFormat("yyyy-MM-dd");

    private static final DateFormat DATETIME_FOR_SQL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final Logger LOG = LoggerFactory.getLogger(DateUtil.class);

    /**
     * Gets the simple date format.
     *
     * @return the simple date format
     */
    public static SimpleDateFormat getSimpleDateFormat() {
        return (SimpleDateFormat) DD_MM_YYYY.clone();
    }

    public static DateFormat getDateAndTime() {
        return (DateFormat) DATE_AND_TIME.clone();
    }

    public static DateFormat getTimeWithoutSecond() {
        return (DateFormat) HOUR_MIN.clone();
    }

    public static DateFormat getTime() {
        return (DateFormat) TIME.clone();
    }

    public static DateFormat getDateForSql() {
        return (DateFormat) DATE_FOR_SQL.clone();
    }

    public static DateFormat getDatetimeForSql() {
        return (DateFormat) DATETIME_FOR_SQL.clone();
    }

    /**
     * Parse une date au format dd/MM/yyyy.
     *
     * @param sDate
     *            La chaine à parser
     *
     * @return La date demandée
     */
    public static Date parseDate(final String sDate) {
        Date date = null;
        if (StringUtils.isNotBlank(sDate)) {
            try {
                date = new Date(getSimpleDateFormat().parse(sDate).getTime());
            } catch (final ParseException e) {
                LOG.info("date incorrecte : " + sDate);
            }
        }
        return date;
    }

    /**
     * Parse une date au format dd/MM/yyyy.
     *
     * @param sDate
     *            La chaine à parser
     *
     * @return La date demandée
     */
    public static Date parseDate(final String sDate, final Locale currentLocale) {
        Date date = null;
        if (StringUtils.isNotBlank(sDate)) {
            try {
                date = new Date(DateFormat.getDateInstance(DateFormat.SHORT, currentLocale).parse(sDate).getTime());
            } catch (final ParseException e) {
                LOG.info("date incorrecte : " + sDate);
            }
        }
        return date;
    }

    /**
     * Formate une date au format dd/MM/yyyy.
     *
     * @param date
     *            La date à formater
     *
     * @return La chaine formatée
     */
    public static String formatDate(final Date date) {
        return date == null ? "" : getSimpleDateFormat().format(date);
    }

    public static String formatDateAndTime(final Date date) {
        String parsedDate = StringUtils.EMPTY;
        if (date != null) {
            parsedDate = getDateAndTime().format(date);
        }
        return parsedDate;
    }

    public static String formatTimeWithoutSecond(final Date date) {
        String parsedDate = StringUtils.EMPTY;
        if (date != null) {
            parsedDate = getTimeWithoutSecond().format(date);
        }
        return parsedDate;
    }

    public static String formatTime(final Date date) {
        String parsedDate = StringUtils.EMPTY;
        if (date != null) {
            parsedDate = getTime().format(date);
        }
        return parsedDate;
    }

    /**
     * Formate une date au format dd/MM/yyyy.
     *
     * @param date
     *            La date à formater
     *
     * @return La chaine formatée
     */
    public static String formatDate(final Date date, final Locale currentLocale) {
        String parsedDate = StringUtils.EMPTY;
        if (date != null) {
            parsedDate = DateFormat.getDateInstance(DateFormat.SHORT, currentLocale).format(date);
        }
        return parsedDate;
    }

    public static String formatDateAndTime(final Date date, final Locale currentLocale) {
        String parsedDate = StringUtils.EMPTY;
        if (date != null) {
            parsedDate = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.DEFAULT, currentLocale).format(date);
        }
        return parsedDate;
    }

    public static String formatTimeWithoutSecond(final Date date, final Locale currentLocale) {
        String parsedDate = StringUtils.EMPTY;
        if (date != null) {
            parsedDate = DateFormat.getTimeInstance(DateFormat.SHORT, currentLocale).format(date);
        }
        return parsedDate;
    }

    public static String formatTime(final Date date, final Locale currentLocale) {
        String parsedDate = StringUtils.EMPTY;
        if (date != null) {
            parsedDate = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocale).format(date);
        }
        return parsedDate;
    }

    /**
     * Renvoie le premier jour de la semaine pour la date passée en paramètre.
     *
     * @param date
     *            La date de départ
     *
     * @return La date demandée
     */
    public static Date getFirstDayOfWeek(final Date date) {
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY); // lundi
        return new Date(cal.getTimeInMillis());
    }

    /**
     * Renvoie le dernier jour de la semaine pour la date passée en paramètre.
     *
     * @param date
     *            La date de départ
     *
     * @return La date demandée
     */
    public static Date getLastDayOfWeek(final Date date) {
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY); // dimanche
        return new Date(cal.getTimeInMillis());
    }

    /**
     * Renvoie le premier jour du mois pour la date passée en paramètre.
     *
     * @param date
     *            La date de départ
     *
     * @return La date demandée
     */
    public static Date getFirstDayOfMonth(final Date date) {
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return new Date(cal.getTimeInMillis());
    }

    /**
     * Renvoie le dernier jour du mois pour la date passée en paramètre.
     *
     * @param date
     *            La date de départ
     *
     * @return La date demandée
     */
    public static Date getLastDayOfMonth(final Date date) {
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return new Date(cal.getTimeInMillis());
    }

    /**
     * Renvoie la date correspondant à la date plus le nombre de jours demandés.
     *
     * @param date
     *            La date de départ
     * @param nbDays
     *            Le nombre de jours à ajouter (positif) ou retirer (négatif)
     *
     * @return La date demandée
     */
    public static Date addDays(final Date date, final int nbDays) {
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, nbDays);
        return new Date(cal.getTimeInMillis());
    }

    /**
     * Renvoie la date avec 23;59;59 en heure.
     * @param date Date mise à jour
     * @return Date avec en heure 23;59:59
     */
    public static Date getEndOfDay(final Date date) {
        return LocalDateTime.fromDateFields(date).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate();
    }
}
