package com.univ.utils;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.InfosRubriques;
// TODO: Auto-generated Javadoc

/**
 * Classe contenant des méthodes utilitaires pour déterminer des URL (absolues, https, multisites, ...) en fonction du contexte.
 *
 * @author FRED
 */
public class URLResolver {

    public static final String FORWARDED_PROTOCOL = "X-Forwarded-Proto";

    /** The Constant STANDARD_ACTION. */
    public static final int STANDARD_ACTION = 0;

    /** The Constant LOGIN_FRONT. */
    public static final int LOGIN_FRONT = 1;

    /** The Constant PERSONNALISER_FRONT. */
    public static final int PERSONNALISER_FRONT = 2;

    /** The Constant DEMANDER_MDP_FRONT. */
    public static final int DEMANDER_MDP_FRONT = 3;

    /** The Constant PRESENTER_MDP_FRONT. */
    public static final int PRESENTER_MDP_FRONT = 4;

    /** The Constant DECONNECTER_FRONT. */
    public static final int DECONNECTER_FRONT = 5;

    private static final Logger LOG = LoggerFactory.getLogger(URLResolver.class);

    /**
     * Construit un path absolu à partir des différents éléments de l'URL.
     *
     * @param scheme
     *            le schéma de la requette (http/https)
     * @param hostName
     *            le host courant
     * @param port
     *            le port pour la requete (en prod pas très utile)
     *
     * @return l'url de base du site en mode scheme://host:port
     */
    public static String getBasePath(final String scheme, final String hostName, int port) {
        final StringBuilder basePath = new StringBuilder();
        if (scheme != null && hostName != null) {
            basePath.append(scheme.toLowerCase());
            basePath.append("://");
            basePath.append(hostName);
            int defaultPort = -1;
            if ("http".equalsIgnoreCase(scheme)) {
                defaultPort = 80;
            } else if ("https".equalsIgnoreCase(scheme)) {
                defaultPort = 443;
            }
            if (port < 0) {
                port = defaultPort;
            }
            if (port != defaultPort) {
                basePath.append(':');
                basePath.append(port);
            }
        }
        return basePath.toString();
    }

    /**
     * Renvoie une url absolue pour la webapp demandée (en http ou https).
     *
     * @param site
     *            le site dont on souhaite connaitre l'url de base
     * @param secure
     *            est ce que on est en https ou non?
     *
     * @return l'url de base du site en mode scheme://host:port
     */
    public static String getBasePath(final InfosSite site, final boolean secure) {
        return secure ? getBasePath("https", site.getHttpsHostname(), site.getHttpsPort()) : getBasePath("http", site.getHttpHostname(), site.getHttpPort());
    }

    /**
     * Détermine si une URL front-office doit être sécurisée ou non.
     *
     * @param ctx
     *            le contexte pour savoir si la requête était sécurisé ou non
     * @param site
     *            le site dont on souhaite savoir si il est ne https
     *
     * @return vrai si le site est en https en front
     */
    private static boolean isSecureFoUrl(final OMContext ctx, InfosSite site) {
        boolean secure = false;
        if (site == null) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            site = serviceInfosSite.getPrincipalSite();
        }
        final int ssl_mode = site.getSslMode();
        switch (ssl_mode) // mode de resolution des urls
        {
            case 0: // http
                break;
            case 1: // context relative
                secure = ctx.isSecure(); // (true si requete https)
                break;
            case 2: // context forcé
                secure = true;
                break;
        }
        return secure;
    }

    /**
     * Détermine si une URL front-office doit être sécurisée ou non.
     *
     * @param site
     *            le contexte pour savoir si la requête était sécurisé ou non
     * @param action
     *            l'action effectué pour savoir si l'https est activé dessus
     *
     * @return vrai si le site est en https sur l'action fourni en parametre
     */
    private static boolean isSecureFoUrl(InfosSite site, final int action) {
        boolean secure = false;
        if (site == null) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            site = serviceInfosSite.getPrincipalSite();
        }
        if (site.getSslMode() == 2) {
            secure = true;
        } else if (action == LOGIN_FRONT || action == PERSONNALISER_FRONT || action == DEMANDER_MDP_FRONT || action == PRESENTER_MDP_FRONT) {
            secure = site.isHttpsAction(action);
        } else if (site.getSslMode() == 1 && action == DECONNECTER_FRONT) {
            secure = !site.isHttpAction(action);
        }
        return secure;
    }

    /**
     * Détermine si une URL back-office doit être sécurisée ou non.
     *
     * @param site
     *            le site que l'on souhaite tester
     *
     * @return vrai si le site est en https en back
     */
    private static boolean isSecureBoUrl(InfosSite site) {
        if (site == null) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            site = serviceInfosSite.getPrincipalSite();
        }
        return site.getBoSslMode() == 1;
    }

    /**
     * Renvoie le path absolu à partir de la requête courante.
     *
     * @param request
     *            La requête courante
     *
     * @return l'url de base de la requête fourni en parametre
     */
    public static String getBasePath(final HttpServletRequest request) {
        final String scheme = URLResolver.getRequestScheme(request);
        final String hostName = request.getServerName();
        final int port = request.getServerPort();
        return getBasePath(scheme, hostName, port);
    }

    /**
     * Calcule l'url absolue de la fiche fourni en paramètre
     *
     * @param fiche
     *            la fiche dont on souhaite connaitre l'url.
     * @param ctx
     *            le contexte pour connaitre le site courant...
     * @return l'url de la fiche ou l'url de base du site si une erreur est survenu.
     */
    public static String getAbsoluteUrlFiche(final FicheUniv fiche, final ContexteUniv ctx) {
        return getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(ctx, fiche), ctx, ctx.getInfosSite());
    }

    /**
     * Récupère une url absolue pour le site courant en fonction du contexte.
     *
     * @param relativeUrl
     *            l'url relative à mettre en absolu
     * @param ctx
     *            le contexte (pour connaitre le site etc.
     *
     * @return l'url absolu de l'url relative fourni pour le site courant
     */
    public static String getAbsoluteUrl(final String relativeUrl, final OMContext ctx) {
        return getAbsoluteUrl(relativeUrl, ctx, ctx.getInfosSite());
    }

    /**
     * Récupère une url absolue pour le site courant en fonction du contexte.
     *
     * @param relativeUrl
     *            l'url relative à mettre en absolu
     * @param ctx
     *            le contexte (pour connaitre le site etc.
     * @param specificAction
     *            l'action concerné par cette url
     *
     * @return l'url absolu de l'url relative fourni pour le site courant
     */
    public static String getAbsoluteUrl(final String relativeUrl, final OMContext ctx, final int specificAction) {
        return getAbsoluteUrl(relativeUrl, ctx.getInfosSite(), specificAction);
    }

    /**
     * Récupère une url absolue pour le site demandé en fonction du contexte.
     *
     * @param relativeUrl
     *            l'url relative à mettre en absolu
     * @param ctx
     *            le contexte (pour connaitre le site etc.
     * @param site
     *            le site pour lequel on veut l'url en absolu
     *
     * @return l'url absolu de l'url relative fourni pour le site courant
     */
    public static String getAbsoluteUrl(final String relativeUrl, final OMContext ctx, InfosSite site) {
        if (site == null) {
            site = ctx.getInfosSite();
        }
        return getAbsoluteUrl(relativeUrl, isSecureFoUrl(ctx, site), site);
    }

    /**
     * Récupère une url absolue pour le site demandé en fonction du contexte.
     *
     * @param relativeUrl
     *            l'url relative à mettre en absolu
     * @param site
     *            le site pour lequel on veut l'url en absolu
     * @param specificAction
     *            l'action concerné par cette url
     *
     * @return l'url absolu de l'url relative fourni pour le site courant
     */
    public static String getAbsoluteUrl(final String relativeUrl, final InfosSite site, final int specificAction) {
        return getAbsoluteUrl(relativeUrl, isSecureFoUrl(site, specificAction), site);
    }

    /**
     * Récupère une url absolue sécurisée ou non, pour le site demandé.
     *
     * @param relativeUrl
     *            l'url relative à mettre en absolu
     * @param secure
     *            vrai si la reuqette doit être en https
     * @param site
     *            le site pour lequel on veut l'url en absolu
     *
     * @return l'url absolu de l'url relative fourni pour le site courant
     */
    public static String getAbsoluteUrl(final String relativeUrl, final boolean secure, final InfosSite site) {
        String url;
        if (relativeUrl.startsWith("http") || relativeUrl.startsWith("https")) {
            url = relativeUrl;
        } else {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            url = getBasePath(site == null ? serviceInfosSite.getPrincipalSite() : site, secure) + relativeUrl;
        }
        return url;
    }

    /**
     * Récupère une url absolue back-office sécurisée ou non, pour le site demandé. (utilisé pour l'initialisation de cas)
     *
     * @param relativeUrl
     *            l'url relative à mettre en absolu
     * @param site
     *            le site pour lequel on veut l'url en absolu
     *
     * @return l'url absolu de l'url relative fourni pour le site courant
     */
    public static String getAbsoluteBoUrl(final String relativeUrl, final InfosSite site) {
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        return getBasePath(site == null ? serviceInfosSite.getPrincipalSite() : site, isSecureBoUrl(site)) + relativeUrl;
    }

    /**
     * Récupère une url absolue sécurisée ou non, pour le site demandé.
     *
     * @param relativeUrl
     *            l'url relative à mettre en absolu
     * @param ctx
     *            le contexte pour connaitre le site etc.
     *
     * @return l'url absolu de la ressource fourni pour le site courant
     */
    public static String getRessourceUrl(final String relativeUrl, final OMContext ctx) {
        return getAbsoluteUrl(relativeUrl, isSecureFoUrl(ctx, ctx.getInfosSite()), ctx.getInfosSite());
    }

    /**
     * Récupère une url absolue sécurisée ou non, pour le site demandé.
     *
     * @param relativeUrl
     *            l'url relative à mettre en absolu
     * @param secure
     *            vrai si la reuqette doit être en https
     * @param ctx
     *            le contexte pour connaitre le site etc.
     *
     * @return l'url absolu de la ressource fourni pour le site courant
     */
    public static String getRessourceUrl(final String relativeUrl, final OMContext ctx, final boolean secure) {
        return getAbsoluteUrl(relativeUrl, secure, ctx.getInfosSite());
    }

    /**
     * Récupère l'url de la page demandée pour un aperçu Si le BO est sécurisé, on repasse en HTTP, si le FO est sécurisé, on reste en HTTPS.
     *
     * @param request
     *            : la requête http provenant du back-office
     * @param relativeUrl
     *            : l'url relative de la page souhaitée en aperçu
     *
     * @return l'url d'aperçu
     */
    public static String getApercuUrl(final HttpServletRequest request, final String relativeUrl) {
        final String hostName = request.getServerName();
        int port = request.getServerPort();
        String scheme = getRequestScheme(request);
        // si on est en mode contextuel SSL dans le front (ssl_mode=1), cela veut dire que le front est en https
        // sinon, on force le passage en http
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        if ("https".equals(scheme) && serviceInfosSite.getPrincipalSite().getSslMode() < 1) {
            scheme = "http";
            port = serviceInfosSite.getPrincipalSite().getHttpPort();
        }
        return getBasePath(scheme, hostName, port) + relativeUrl;
    }

    /**
     * Renvoie l'url absolue d'une page d'accueil de rubrique.
     *
     * @param ctx
     *            le contexte pour connaitre le site etc.
     * @param infosRubrique
     *            la rubrique dont on souhaite avoir la page d'accueil
     *
     * @return l'url absolu de la page d'accueil de rubrique
     *
     * @throws Exception
     *             lors du calcul de l'url d'accueil de la rubrique
     */
    public static String getAbsoluteUrlAcceuil(final ContexteUniv ctx, final InfosRubriques infosRubrique) throws Exception {
        return getAbsoluteUrl(UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, infosRubrique.getCode()), ctx);
    }

    /**
     * Renvoie le protocol de la requête passé en paramètre. La méthode cherche d'abord à récupérer la valeur du header "X-Forwarded-Proto". Si ce dernier n'est pas présent, la
     * méthode renvoit le résultat de la méthode "getScheme()" de la requête.
     *
     * @param request la requête dont on souhaite connaitre le schema
     * @return le schema de la requete http
     */
    public static String getRequestScheme(final HttpServletRequest request) {
        String scheme;
        scheme = request.getHeader(FORWARDED_PROTOCOL);
        if (StringUtils.isBlank(scheme)) {
            scheme = request.getScheme();
        }
        return scheme;
    }

    /**
     * Renvoie la base de la requête http en prenant en compte le protocol réel (http ou https) à partir de la méthode {@link #getRequestScheme(HttpServletRequest)}
     *
     * @param request la requête dont on souhaite connaitre le base
     * @return la base sous la forme "http(s)://blabla.truc.com"
     * @see {@link #getRequestScheme(HttpServletRequest)}
     */
    public static String getRequestBase(final HttpServletRequest request) {
        final String realScheme = getRequestScheme(request);
        String fullUrl = request.getRequestURL().toString();
        fullUrl = StringUtils.removeEnd(fullUrl, request.getRequestURI());
        fullUrl = StringUtils.removeStart(fullUrl, request.getScheme());
        fullUrl = realScheme + fullUrl;
        return fullUrl;
    }
}
