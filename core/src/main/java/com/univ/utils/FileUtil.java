package com.univ.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
// TODO: Auto-generated Javadoc

/**
 * The Class FileUtil.
 */
public class FileUtil {

    /** Logger available to subclasses. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    /**
     * Copie de fichiers.
     *
     * @param in
     *            the in
     * @param out
     *            the out
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void copyInputStream(final InputStream in, final OutputStream out) throws IOException {
        final byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) >= 0) {
            out.write(buffer, 0, len);
        }
        in.close();
        out.close();
    }

    /**
     * Vidage d'un répertoire.
     *
     * @param directory
     *            the directory
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void emptyDirectory(final File directory) throws IOException {
        final String[] files = directory.list();
        File file;
        for (int i = files.length - 1; i >= 0; i--) {
            file = new File(directory.getAbsolutePath() + File.separator + files[i]);
            if (file.isDirectory()) {
                emptyDirectory(file);
            }
            file.delete();
        }
    }

    /**
     * Vidage & suppression d'un répertoire.
     *
     * @param directory
     *            the directory
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void emptyAndDeleteDirectory(final File directory) throws IOException {
        emptyDirectory(directory);
        directory.delete();
    }

    /**
     * copie le fichier source dans le fichier resultat retourne vrai si cela réussit.
     *
     * @param source
     *            the source
     * @param destination
     *            the destination
     *
     * @return la taille du fichier copié
     */
    public static int copierFichier(final File source, final File destination) {
        return copierFichier(source, destination, false);
    }

    /**
     * copie le fichier source dans le fichier resultat retourne vrai si cela réussit.
     *
     * @param source
     *            the source
     * @param destination
     *            the destination
     * @param deleteSource
     *            de la source après copie
     *
     * @return la taille du fichier copié
     */
    public static int copierFichier(final File source, final File destination, final boolean deleteSource) {
        final int poids = 0;
        try {
            primitiveCopierFichier(source, destination, deleteSource);
        } catch (final IOException e) {
            LOGGER.error(String.format("Erreur lors de l'ecriture du fichier %s", destination.getAbsolutePath()), e);
        }
        return poids;
    }

    public static int primitiveCopierFichier(final File source, final File destination, final boolean deleteSource) throws IOException {
        int poids = 0;
        int temp = 0;
        // Declaration des flux
        java.io.FileInputStream sourceFile = null;
        java.io.FileOutputStream destinationFile = null;
        try {
            if (source.isDirectory()) {
                FileUtils.copyDirectory(source, destination);
            } else {
                // Création du fichier
                destination.createNewFile();
                // Ouverture des flux
                sourceFile = new java.io.FileInputStream(source);
                destinationFile = new java.io.FileOutputStream(destination);
                // Lecture
                final byte buffer[] = new byte[1024 * 4];
                int nbLecture;
                while ((nbLecture = sourceFile.read(buffer)) != -1) {
                    destinationFile.write(buffer, 0, nbLecture);
                    temp = temp + nbLecture;
                }
                // Copie réussie
                poids = temp;
            }
            if (deleteSource) {
                source.delete();
            }
        } finally {
            // Quoi qu'il arrive, on ferme les flux
            try {
                if (sourceFile != null) {
                    sourceFile.close();
                }
            } catch (final IOException e) {
                LOGGER.error("Une erreur est survenue lors de la tentative de fermeture du fichier source", e);
            }
            try {
                if (destinationFile != null) {
                    destinationFile.close();
                }
            } catch (final IOException e) {
                LOGGER.error("Une erreur est survenue lors de la tentative de fermeture du fichier de destination", e);
            }
        }
        return poids;
    }
    // calcul du poids du fichier en Ko

    /**
     * Calculer poids.
     *
     * @param f
     *            the f
     *
     * @return the int
     * @deprecated cette méthode ne calcule pas le poid correctement. Utilisez {@link File#length()} pour ça
     */
    @Deprecated
    public static int calculerPoids(final File f) {
        int poids = 0;
        try (final FileInputStream sourceFile = new FileInputStream(f);){
            // Lecture
            final byte buffer[] = new byte[1024];
            while (sourceFile.read(buffer) != -1) {
                poids++;
            }
            return poids;
        } catch (final IOException e) {
            LOGGER.error(String.format("Fichier %s introuvable.", f.getName()), e);
        }
        return poids;
    }

    /**
     * Gets the extension.
     *
     * @param filename
     *            the filename
     *
     * @return the extension
     */
    public static String getExtension(final String filename) {
        return FilenameUtils.getExtension(filename);
    }

    public static String mkdir(final String string) {
        final File file = new File(string);
        if (!file.exists()) {
            file.mkdirs();
        }
        return string;
    }
}
