package com.univ.utils;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.PropertyHelper;
import com.univ.mediatheque.utils.MediathequeHelper;

/**
 * The Class PhotoUtil.
 * Created on 19 nov. 07
 */
public class PhotoUtil {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(PhotoUtil.class);

    /**
     * Resize.
     *
     * @param path
     *            the path
     * @param format
     *            the format
     * @param newLargeur
     *            the new largeur
     * @param newHauteur
     *            the new hauteur
     * @param replace
     *            the replace
     *
     * @return the file
     */
    public static File resize(final String path, final String format, final int newLargeur, final int newHauteur, final boolean replace) {
        final File fOld = new File(path);
        File fNew = new File(fOld.getParent(), "tmp_" + System.currentTimeMillis());
        try {
            final String scaleType = PropertyHelper.getCoreProperty(MediathequeHelper.CRITERES_PROPORTIONS_PROPERTIES_KEY);
            final Iterator<?> writers = ImageIO.getImageWritersByFormatName(format);
            final ImageWriter writer = (ImageWriter) writers.next();
            final BufferedImage image = ImageIO.read(fOld);
            // Redimensionnement de l'image
            final BufferedImage bImage = scale(image, newLargeur, newHauteur, scaleType);
            if (!image.equals(bImage)) {
                final IIOImage ioImage = new IIOImage(bImage, null, null);
                final ImageOutputStream ios = ImageIO.createImageOutputStream(fNew);
                writer.setOutput(ios);
                if ("jpg".equalsIgnoreCase(format) || "jpeg".equalsIgnoreCase(format)) {
                    // Paramètrage de la compression du writer
                    final ImageWriteParam iwp = writer.getDefaultWriteParam();
                    iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                    // 90% de compression
                    iwp.setCompressionQuality(0.9f);
                    writer.write(null, ioImage, iwp);
                } else {
                    writer.write(ioImage);
                }
                ios.close();
                writer.dispose();
            } else {
                fNew = fOld;
            }
            if (replace) {
                FileUtil.copierFichier(fNew, fOld, true);
                return fOld;
            }
        } catch (final IOException e) {
            LOG.error(String.format("Impossible de redimensionner l'image \"%s\"", fOld.getAbsolutePath()), e);
            return fOld;
        }
        return fNew;
    }

    /**
     * Redimensionne une image.
     *
     * @param image
     *            Image à redimensionner.
     * @param newWidth
     *            the new width
     * @param newHeight
     *            the new height
     *
     * @return Image redimensionnée.
     * @throws IOException
     */
    public static BufferedImage scale(final BufferedImage image, final int newWidth, final int newHeight, final String scaleType) throws IOException {
        final Dimension optimalValues = computeOptimalValues(image, newWidth, newHeight, scaleType);
        BufferedImage buf = Scalr.resize(image, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, optimalValues.width, optimalValues.height);
        if ("crop".equals(scaleType) && (newHeight < optimalValues.height || newWidth < optimalValues.width)) {
            // Calcul d'un cropping centré
            final Dimension cropPos = new Dimension(0, 0);
            cropPos.height = (optimalValues.height / 2) - (newHeight / 2);
            cropPos.width = (optimalValues.width / 2) - (newWidth / 2);
            if (buf.getHeight() != newHeight || buf.getWidth() != newWidth) {
                buf = Scalr.crop(buf, cropPos.width, cropPos.height, newWidth, newHeight);
            }
        }
        return buf;
    }

    /**
     * Calcul les valeurs optimales pour le redimensionnement de l'image en fonction du paramétrage effectué pour l'application (la méthode "ratio" est utilisée par défaut).
     * <ul>
     * <li>Crop : cette méthode vise à respecter les dimensions fournies quoi qu'il arrive sans pour autant négliger le ratio original de l'image. L'image est donc redimensionnée
     * en respectant le ratio, et un cropping est effectué si besoin est pour revenir au dimension souhaitées.</li>
     * <li>Ratio : ancienne méthode de redimensionnement -> le ratio est privilégié au détriment des dimensions cibles.</li>
     * </ul>
     *
     * @param image
     *            : l'image originale de type {@link java.awt.image.BufferedImage BufferedImage}
     * @param requestedWidth
     *            : la largeur souhaitée
     * @param requestedHeight
     *            : la hauteur souhaitée
     * @param scaleType
     *            : le type de méthode utilisée (la méthode "ratio" est utilisée par défaut)
     * @return le couple hauteur/largeur optimal sous la forme d'une {@link java.awt.Dimension Dimension}
     */
    private static Dimension computeOptimalValues(final BufferedImage image, int requestedWidth, int requestedHeight, final String scaleType) {
        final Dimension result = new Dimension();
        final double height = image.getHeight();
        final double width = image.getWidth();
        final double yScale = requestedHeight / height;
        final double xScale = requestedWidth / width;
        if (yScale > 1.0f || xScale > 1.0f) {
            result.setSize(width, height);
        } else {
            if ("ratio".equals(scaleType)) {
                if (xScale < yScale) {
                    requestedHeight = (int) (height * xScale);
                } else {
                    requestedWidth = (int) (width * yScale);
                }
            } else {
                if (xScale < yScale) {
                    requestedWidth = (int) (width * yScale);
                } else if (xScale > yScale) {
                    requestedHeight = (int) (height * xScale);
                } else {
                    requestedHeight = (int) (height * xScale);
                    requestedWidth = (int) (width * xScale);
                }
            }
            result.setSize(requestedWidth, requestedHeight);
        }
        return result;
    }
}
