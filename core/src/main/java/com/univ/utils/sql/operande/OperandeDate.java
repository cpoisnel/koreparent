package com.univ.utils.sql.operande;

import java.util.Date;

import com.univ.utils.DateUtil;
import com.univ.utils.EscapeString;

/**
 * Permet de formatter un champ contenant une date de type java.util.Date la date est formatter soit en "yyyy-MM-dd" soit en "yyyy-MM-dd HH:mm:ss" en fonction des besoins
 *
 * @author olivier.camon
 *
 */
public class OperandeDate implements Operande {

    private Date valeur;

    private boolean isDateEtHeure;

    public OperandeDate(Date valeur, boolean isDateEtHeure) {
        this.valeur = valeur;
        this.isDateEtHeure = isDateEtHeure;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.operande.Operande#formaterOperande()
     */
    @Override
    public String formaterOperande() {
        if (valeur == null) {
            return "";
        }
        String dateFormate = "";
        if (isDateEtHeure) {
            dateFormate = DateUtil.getDatetimeForSql().format(valeur);
        } else {
            dateFormate = DateUtil.getDateForSql().format(valeur);
        }
        return "'" + EscapeString.escapeSql(dateFormate) + "'";
    }
}
