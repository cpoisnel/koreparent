package com.univ.utils.sql;

import com.kportal.core.config.PropertyHelper;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * Classe utilitaire contenant un ensemble de constante utilisé dans tout le package SQL.
 *
 * @author olivier.camon
 *
 */
public final class ConstanteSQL {

    public static final String PARENTHESE_OUVRANTE = " ( ";

    public static final String PARENTHESE_FERMANTE = " ) ";

    public static final String WHERE = " WHERE ";

    public static final String HAVING = " HAVING ";

    public static final String GROUP_BY = " GROUP BY ";

    public static final String ORDER_BY = " ORDER BY ";

    public static final String LIMIT = " LIMIT ";

    public static final String NULL = " NULL ";

    public static final String NO_ARBO = "_NOARBO";

    public static final String JOCKER_CODE = "-";

    /**
     * si la dsi est activé, on réalise les contrôles
     */
    public static final String ACTIVATION_DSI = PropertyHelper.getCoreProperty("dsi.activation");

    /**
     * spécifique filtre IP, on recherche sur le host si = 1.
     */
    public static final String RECHERCHE_DOMAINE = PropertyHelper.getCoreProperty("dsi.recherche_domaine");

    /**
     * Code des groupes dynamiques
     */
    public static final String DYNAMIK = "DYNAMIK";

    /**
     * Suffixe pour dire qu'on filtre sur les types de groupes et non sur le code
     */
    public static final String TYPE_GROUPE = "_TYPEGROUPE";

    /**
     * Condition impossible à réalisé 1=2,
     */
    public static final Condition CONDITION_IMPOSSIBLE = ConditionHelper.genericCondition("1", TypeOperande.VARCHAR, "2", TypeOperande.VARCHAR, Operateur.EQUALS);

    /**
     * souvent utilisé dans le produit, pourquoi CODE = ZZZZ ?
     */
    public static final ConditionList CONDITION_DEFAUT_CODE = new ConditionList(ConditionHelper.egalVarchar("T1.CODE", "ZZZZZZZZZZZZZZZZZZZZ"));

    /**
     * Condition par défaut lorsque l'on filtre sur les groupes.. @see {@link SQLUtil.calculerPersonnalisationGroupeDsi()}
     */
    public static final Condition CONDITION_DEFAUT_POUR_GROUPE = ConditionHelper.egalVarchar("T1.DIFFUSION_PUBLIC_VISE", "ZZZZZZZZZZZZZZZZZZZZZZZZ");
}
