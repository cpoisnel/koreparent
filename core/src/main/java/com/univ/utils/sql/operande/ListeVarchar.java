package com.univ.utils.sql.operande;

import java.util.Collection;

import com.univ.utils.EscapeString;
import com.univ.utils.sql.ConstanteSQL;

/**
 * Opérande permettant d'inserer une liste de valeur tel que lors de l'utilisation d'une condition IN
 *
 * @author olivier.camon
 *
 */
public class ListeVarchar implements Operande {

    private Collection<String> valeurs;

    public ListeVarchar(Collection<String> valeurs) {
        this.valeurs = valeurs;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.operande.Operande#formaterOperande()
     */
    @Override
    public String formaterOperande() {
        if (valeurs == null || valeurs.isEmpty()) {
            return "";
        }
        StringBuilder listeDeValeursFormatter = new StringBuilder(ConstanteSQL.PARENTHESE_OUVRANTE);
        Boolean isFirst = Boolean.TRUE;
        for (String valeur : valeurs) {
            if (!isFirst) {
                listeDeValeursFormatter.append(", ");
            }
            listeDeValeursFormatter.append("'").append(EscapeString.escapeSql(valeur)).append("'");
            isFirst = Boolean.FALSE;
        }
        listeDeValeursFormatter.append(ConstanteSQL.PARENTHESE_FERMANTE);
        return listeDeValeursFormatter.toString();
    }
}
