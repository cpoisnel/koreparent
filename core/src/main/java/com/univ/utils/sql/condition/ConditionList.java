package com.univ.utils.sql.condition;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.sql.ConstanteSQL;
import com.univ.utils.sql.OperateurConditionnel;

/**
 * Représente un ensemble de condition SQL. Exemple : (FOO = 'BAR' AND BAR = 'FOO') Cette liste de condition est entouré de parenthèse lorsqu'on la formatte
 *
 * @author olivier.camon
 *
 */
public class ConditionList implements Condition {

    private Condition premiereCondition;

    private List<Condition> conditions;

    private List<OperateurConditionnel> operateurDeCondition;

    /**
     * Constructeur par défaut
     */
    public ConditionList() {
        conditions = new ArrayList<>();
        operateurDeCondition = new ArrayList<>();
    }

    /**
     * Instancie et initialise la première condition de la listes
     *
     * @param conditionAAjouter
     *            la première condition de la liste
     */
    public ConditionList(Condition conditionAAjouter) {
        conditions = new ArrayList<>();
        operateurDeCondition = new ArrayList<>();
        premiereCondition = conditionAAjouter;
    }

    /**
     * Ajoute un critère AND CONDITIONAAJOUTER à la liste
     *
     * @param conditionAAjouter
     *            condition à ajouter sous la forme AND CONDITION
     */
    public void and(Condition conditionAAjouter) {
        if (premiereCondition == null || premiereCondition.isEmpty()) {
            premiereCondition = conditionAAjouter;
        } else {
            operateurDeCondition.add(OperateurConditionnel.AND);
            conditions.add(conditionAAjouter);
        }
    }

    /**
     * Ajoute un critère OR CONDITIONAAJOUTER à la liste
     *
     * @param conditionAAjouter
     *            condition à ajouter sous la forme OR CONDITION
     */
    public void or(Condition conditionAAjouter) {
        if (premiereCondition == null || premiereCondition.isEmpty()) {
            premiereCondition = conditionAAjouter;
        } else {
            operateurDeCondition.add(OperateurConditionnel.OR);
            conditions.add(conditionAAjouter);
        }
    }

    /**
     * initialise la première condition de la liste
     */
    public void setPremiereCondtion(Condition condition) {
        this.premiereCondition = condition;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.condition.Condition#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return this.premiereCondition == null || StringUtils.isEmpty(this.premiereCondition.formaterCondition());
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.condition.Condition#formaterCondition()
     */
    @Override
    public String formaterCondition() {
        StringBuilder requeteSQLFormater = new StringBuilder();
        if (premiereCondition == null || premiereCondition.isEmpty()) {
            return requeteSQLFormater.toString();
        }
        requeteSQLFormater.append(premiereCondition.formaterCondition());
        for (int i = 0; i < operateurDeCondition.size(); ++i) {
            Condition sousCondition = conditions.get(i);
            if (!sousCondition.isEmpty() && operateurDeCondition.get(i) != null) {
                requeteSQLFormater.append(operateurDeCondition.get(i).getOperateur()).append(sousCondition.formaterCondition());
            }
        }
        requeteSQLFormater = ajouterParenthesesSiNonVide(requeteSQLFormater);
        return requeteSQLFormater.toString();
    }

    private StringBuilder ajouterParenthesesSiNonVide(StringBuilder requeteSQLFormater) {
        if (StringUtils.isNotEmpty(requeteSQLFormater.toString())) {
            requeteSQLFormater.insert(0, ConstanteSQL.PARENTHESE_OUVRANTE);
            requeteSQLFormater.append(ConstanteSQL.PARENTHESE_FERMANTE);
        }
        return requeteSQLFormater;
    }
}
