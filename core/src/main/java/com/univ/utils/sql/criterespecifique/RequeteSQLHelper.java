package com.univ.utils.sql.criterespecifique;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.ContexteUniv;
import com.univ.utils.DateUtil;
import com.univ.utils.IRequeteurConstantes;
import com.univ.utils.sql.ConstanteSQL;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseJoin;
import com.univ.utils.sql.clause.ClauseJoin.TypeJointure;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.condition.ConditionSimple;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * Classe utilitaire permettant de calculer les conditions génériques lors d'une requête SQL.
 *
 * @author olivier.camon
 *
 */
public class RequeteSQLHelper {

    private static EspaceCollaboratifDAO getEspaceCollaboratifDao() {
        return ApplicationContextManager.getCoreContextBean(EspaceCollaboratifDAO.ID_BEAN, EspaceCollaboratifDAO.class);
    }

    /**
     * Migration de SQLUtil.ajouterCriteresGeneriques(String, OMContext, FicheUniv, String, String, String). Elle permet de calculer les condtions sur les metatags, la DSI, le BO,
     * les rubriques de publications,
     *
     * @param where
     *            la clause where déjà construite.
     * @param ctx
     *            le contexet courant
     * @param fiche
     *            la fiche sur laquelle on veut vérifier les contrôles
     * @param codeRubrique
     *            le code de la rubrique courante.
     * @return La requête SQL contenant les conditions génériques.
     */
    public static RequeteSQL getRequeteGenerique(final ClauseWhere where, final OMContext ctx, final FicheUniv fiche, final String codeRubrique) {
        final RequeteSQL requeteCritererGenerique = new RequeteSQL();
        final Condition conditionMeta = traiterConditionMeta(ctx, fiche);
        final Condition conditionDSI = ConditionHelper.getConditionDSI(ctx, fiche);
        final Condition conditionBO = ConditionHelper.getConditionBO(ctx, fiche);
        final Condition conditionRubPub = ConditionHelper.getConditionRubPubDSIFiche(ctx, codeRubrique);
        final Condition conditionIdRubriqueAndFicheOrig = ConditionHelper.getConditionRubPubFiche(fiche);
        boolean isJoinRubPub = Boolean.FALSE;
        if (conditionIdRubriqueAndFicheOrig != null && !conditionIdRubriqueAndFicheOrig.isEmpty()) {
            where.and(conditionIdRubriqueAndFicheOrig);
            requeteCritererGenerique.join(ClauseJoinHelper.creerJointureRubPub(fiche));
            isJoinRubPub = Boolean.TRUE;
        }
        if (conditionRubPub != null && !conditionRubPub.isEmpty()) {
            where.and(conditionRubPub);
            isJoinRubPub = Boolean.TRUE;
        }
        if (!isJoinRubPub) {
            requeteCritererGenerique.join(ClauseJoinHelper.creerJointureRubPub(fiche));
        }
        if (conditionMeta != null && !conditionMeta.isEmpty()) {
            where.and(conditionMeta);
            requeteCritererGenerique.join(ClauseJoinHelper.creerJointureMetaTag(fiche));
        }
        if (conditionBO != null && !conditionBO.isEmpty()) {
            where.and(conditionBO);
        }
        if (conditionDSI != null && !conditionDSI.isEmpty()) {
            where.and(conditionDSI);
        }
        requeteCritererGenerique.where(where);
        return requeteCritererGenerique;
    }

    private static Condition traiterConditionMeta(final OMContext ctx, final FicheUniv fiche) {
        ConditionList metaDateMiseEnLigne = null;
        if (ctx instanceof ContexteUniv && ((ContexteUniv) ctx).isCalculListeResultatsFront()) {
            final Date aujourdhui = DateUtil.getEndOfDay(new Date());
            metaDateMiseEnLigne = new ConditionList(ConditionHelper.lessEquals("META.META_DATE_MISE_EN_LIGNE", aujourdhui, TypeOperande.DATE_ET_HEURE));
            if (isFicheStructureInTree(fiche)) {
                metaDateMiseEnLigne.and(ConditionHelper.egalVarchar("META.META_IN_TREE", "1"));
            }
            final String codeObjet = ReferentielObjets.getCodeObjet(fiche);
            metaDateMiseEnLigne.and(ConditionHelper.egalVarchar("META.META_CODE_OBJET", codeObjet));
        }
        return metaDateMiseEnLigne;
    }

    private static boolean isFicheStructureInTree(final FicheUniv fiche) {
        return fiche instanceof StructureModele && !"1".equals(PropertyHelper.getCoreProperty("structure.inTree"));
    }

    /**
     * Creer les conditions sur la ficheUniv fourni en paramètre.
     *
     * @param autorisations
     * @param ficheUniv
     * @param action
     * @param rubriqueRecherche
     * @param prefixeTableMeta
     * @return
     */
    public static Condition traiterConditionDsiMeta(final AutorisationBean autorisations, final FicheUniv ficheUniv, final String action, final String rubriqueRecherche, String prefixeTableMeta) {
        if (StringUtils.isEmpty(prefixeTableMeta)) {
            prefixeTableMeta = "T1";
        }
        ConditionList conditionComplete = null;
        final ConditionList conditionSurFiche = new ConditionList();
        boolean restriction = Boolean.FALSE;
        boolean aucuneRestriction = Boolean.FALSE;
        boolean aucunePermission = Boolean.TRUE;
        // Controle des droits sur les actions de modification
        final String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
        final Map<String, String> actionsPossibles = recupererActionsPossibles(action);
        final Set<String> codesEspacesCollab = new HashSet<>();
        if (autorisations != null) {
            for (final Entry<String, String> actionPossible : actionsPossibles.entrySet()) {
                final String lettreAction = actionPossible.getKey();
                for (final Entry<String, Vector<Perimetre>> permission : autorisations.getListePermissions().entrySet()) {
                    if (isPermissionValide(permission.getKey(), codeObjet, lettreAction)) {
                        aucunePermission = Boolean.FALSE;
                        for (final Perimetre perimetre : permission.getValue()) {
                            final ConditionList conditionPerimetre = new ConditionList();
                            if (isAcuneRestriction(perimetre)) {
                                aucuneRestriction = Boolean.TRUE;
                            } else {
                                final Condition conditionRubrique = traiterCodeRubriquePerimetre(perimetre.getCodeRubrique(), rubriqueRecherche, prefixeTableMeta);
                                final Condition conditionStructure = traiterStructurePerimetre(perimetre.getCodeStructure(), ficheUniv, prefixeTableMeta);
                                if (StringUtils.isNotEmpty(perimetre.getCodeEspaceCollaboratif())) {
                                    codesEspacesCollab.add(perimetre.getCodeEspaceCollaboratif());
                                }
                                if (conditionRubrique != null) {
                                    restriction = Boolean.TRUE;
                                    conditionPerimetre.setPremiereCondtion(conditionRubrique);
                                }
                                if (conditionStructure != null) {
                                    restriction = Boolean.TRUE;
                                    conditionPerimetre.and(conditionStructure);
                                }
                            }
                            conditionSurFiche.or(conditionPerimetre);
                        }
                    }
                }
            }
        }
        if (!codesEspacesCollab.isEmpty()) {
            final ConditionList conditionEspaceCollab = new ConditionList(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_DIFFUSION_MODE_RESTRICTION", "4"));
            conditionEspaceCollab.and(ConditionHelper.in(prefixeTableMeta + ".META_DIFFUSION_PUBLIC_VISE_RESTRICTION", codesEspacesCollab));
            conditionSurFiche.or(conditionEspaceCollab);
        }
        if (aucunePermission) {
            if (autorisations == null) {
                return ConstanteSQL.CONDITION_IMPOSSIBLE;
            }
            final ConditionList ficheDeLUtilisateur = new ConditionList(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_CODE_REDACTEUR", autorisations.getCode()));
            if (StringUtils.isNotBlank(codeObjet)) {
                ficheDeLUtilisateur.and(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_CODE_OBJET", codeObjet));
            }
            return ficheDeLUtilisateur;
        }
        if (conditionSurFiche.isEmpty() && !aucuneRestriction) {
            return new ConditionSimple();
        }
        if (aucuneRestriction && !restriction && !codesEspacesCollab.isEmpty()) {
            conditionSurFiche.or(ConditionHelper.notEgal(prefixeTableMeta + ".META_DIFFUSION_MODE_RESTRICTION", "4", TypeOperande.VARCHAR));
        }
        conditionComplete = new ConditionList(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_CODE_OBJET", codeObjet));
        conditionComplete.and(conditionSurFiche);
        return conditionComplete;
    }

    /*
     * requete générique sur la table meta en consultation et avec plusieurs types d'objet
     */
    public static RequeteSQL getRequeteGeneriqueMeta(final RequeteSQL requeteCritere, final OMContext ctx, final List<FicheUniv> lstFiche, final String codeRubrique) {
        final ClauseWhere where = requeteCritere.getWhere();
        if (lstFiche.size() > 0) {
            // controle de la date de mise en ligne sur la table méta
            final Date aujourdhui = DateUtil.getEndOfDay(new Date());
            final ConditionList metaDateMiseEnLigne = new ConditionList(ConditionHelper.lessEquals("META.META_DATE_MISE_EN_LIGNE", aujourdhui, TypeOperande.DATE_ET_HEURE));
            where.and(metaDateMiseEnLigne);
            // controle des droits sur chaque objet
            final ConditionList conditionSurObjets = new ConditionList();
            for (final FicheUniv ficheUniv : lstFiche) {
                if (isDiffusionSelective(ficheUniv, ctx)) {
                    final String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
                    final ConditionList conditionFiche = new ConditionList(ConditionHelper.egalVarchar("META.META_CODE_OBJET", codeObjet));
                    if (isFicheStructureInTree(ficheUniv)) {
                        conditionFiche.and(ConditionHelper.egalVarchar("META.META_IN_TREE", "1"));
                    }
                    conditionSurObjets.or(conditionFiche);
                }
            }
            where.and(conditionSurObjets);
            // controle de la DSI sur les groupes
            where.and(construireConditionDSI((ContexteUniv) ctx, "", "", "", "META"));
            // controle de la DSI sur la rubrique
            where.and(ConditionHelper.getConditionRubPubSuivantAction(ctx, IRequeteurConstantes.MODE_CONSULTATION, "META.META_CODE_RUBRIQUE", codeRubrique));
            // ajout de la jointure sur la table rubrique publication
            if (where.formaterSQL().contains("RUB_PUB.")) {
                final ClauseJoin joinRubPub = new ClauseJoin();
                joinRubPub.setTypeJointure(TypeJointure.LEFT_JOIN);
                joinRubPub.setNomTable("RUBRIQUEPUBLICATION RUB_PUB");
                joinRubPub.on(ConditionHelper.critereJointureSimple("META.META_CODE", "RUB_PUB.CODE_FICHE_ORIG"));
                joinRubPub.and(ConditionHelper.critereJointureSimple("META.META_LANGUE", "RUB_PUB.LANGUE_FICHE_ORIG"));
                joinRubPub.and(ConditionHelper.critereJointureSimple("META.META_CODE_OBJET", "RUB_PUB.TYPE_FICHE_ORIG"));
                requeteCritere.join(joinRubPub);
            }
            // Ajout des criteres generiques
            requeteCritere.where(where);
        }
        return requeteCritere;
    }

    private static boolean isDiffusionSelective(final FicheUniv fiche, final OMContext ctx) {
        return fiche instanceof DiffusionSelective && "1".equals(ConstanteSQL.ACTIVATION_DSI) && ctx instanceof ContexteUniv && ((ContexteUniv) ctx).isCalculListeResultatsFront();
    }

    /**
     * Permet de savoir si la permission est valide ou non pour le type d'objet et l'action choisie.
     *
     * @param key
     * @param codeObjet
     * @param lettreAction
     * @return
     */
    public static boolean isPermissionValide(final String key, final String codeObjet, final String lettreAction) {
        final PermissionBean permissionBean = new PermissionBean(key);
        return "FICHE".equals(permissionBean.getType()) && permissionBean.getObjet().equals(codeObjet) && permissionBean.getAction().equals(lettreAction);
    }

    /**
     * Vérifie si le perimtre ne possède aucune restriction.
     *
     * @param perimetre
     * @return
     */
    public static boolean isAcuneRestriction(final Perimetre perimetre) {
        return perimetre.getCodeRubrique().length() == 0 && perimetre.getCodeStructure().length() == 0 && perimetre.getCodeProfil().length() == 0 && perimetre.getCodeGroupe().length() == 0 && perimetre.getCodeEspaceCollaboratif().length() == 0;
    }

    /**
     * Construit une liste d'action possible MODIFICATION, VALIDATION...
     *
     * @param action
     * @return
     */
    public static Hashtable<String, String> recupererActionsPossibles(final String action) {
        final Hashtable<String, String> actionsPossibles = new Hashtable<>();
        String lettreAction = "M";
        if (action.equals(IRequeteurConstantes.MODE_MODIFICATION)) {
            lettreAction = "M";
        } else if (action.equals(IRequeteurConstantes.MODE_VALIDATION)) {
            lettreAction = "V";
            actionsPossibles.putAll(AutorisationBean.getListeNiveauxApprobation());
        }
        actionsPossibles.put(lettreAction, action);
        return actionsPossibles;
    }

    /**
     * Creer les conditions sur le codeRubrique, Si il est égal à "-" ca veut dire vide.
     *
     * @param codeRubrique
     * @param rubriqueRecherche
     * @param prefixeTableMeta
     * @return
     */
    public static Condition traiterCodeRubriquePerimetre(final String codeRubrique, final String rubriqueRecherche, final String prefixeTableMeta) {
        Condition conditionRubrique = null;
        if (StringUtils.isNotEmpty(codeRubrique)) {
            if ("-".equals(codeRubrique)) {
                conditionRubrique = ConditionHelper.egalVarchar(prefixeTableMeta + ".META_CODE_RUBRIQUE", "");
            } else {
                final Set<String> codesRubriquesValides = recupererListeCodeRubriqueParRubriqueRecherche(codeRubrique, rubriqueRecherche);
                conditionRubrique = ConditionHelper.in(prefixeTableMeta + ".META_CODE_RUBRIQUE", codesRubriquesValides);
            }
        }
        return conditionRubrique;
    }

    /**
     * Construit la condition sur le code de rattachement en fonction du codeStructure fourni. Si le codeStructure vaut "-", ca veut dire vide.
     *
     * @param codeStructure
     * @param ficheUniv
     * @param prefixeTableMeta
     * @return
     */
    public static Condition traiterStructurePerimetre(final String codeStructure, final FicheUniv ficheUniv, final String prefixeTableMeta) {
        ConditionList conditionStructure = null;
        if (StringUtils.isNotEmpty(codeStructure)) {
            if ("-".equals(codeStructure)) {
                conditionStructure = new ConditionList(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_CODE_RATTACHEMENT", ""));
                if (ficheUniv instanceof StructureModele) {
                    conditionStructure.and(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_CODE_RATTACHEMENT_AUTRES", ""));
                }
            } else {
                conditionStructure = new ConditionList(ConditionHelper.getConditionStructure(prefixeTableMeta + ".META_CODE_RATTACHEMENT", codeStructure));
                if (ficheUniv instanceof StructureModele) {
                    conditionStructure.or(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_CODE", codeStructure));
                }
                if (ficheUniv instanceof FicheRattachementsSecondaires) {
                    conditionStructure.or(traiterStructureMultiplePerimetre(codeStructure));
                }
            }
        }
        return conditionStructure;
    }

    /**
     * Cherche les codes de l'ensemble des rubriques
     *
     * @param codeRubrique
     * @param codeRubriqueRecherche
     * @return
     */
    public static Set<String> recupererListeCodeRubriqueParRubriqueRecherche(final String codeRubrique, final String codeRubriqueRecherche) {
        final Set<String> codeDesRurbiquesValides = new HashSet<>();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueRecherche = serviceRubrique.getRubriqueByCode(codeRubriqueRecherche);
        final RubriqueBean infosRub = serviceRubrique.getRubriqueByCode(codeRubrique);
        if (infosRub != null) {
            final Collection<RubriqueBean> infosRubriquesToutNiveau = serviceRubrique.getAllChilds(infosRub.getCode());
            for (final RubriqueBean rub : infosRubriquesToutNiveau) {
                if (StringUtils.isEmpty(codeRubriqueRecherche) || serviceRubrique.isParentSection(rubriqueRecherche, rub)) {
                    codeDesRurbiquesValides.add(rub.getCode());
                }
            }
        }
        if (StringUtils.isEmpty(codeRubriqueRecherche) || serviceRubrique.isParentSection(rubriqueRecherche, infosRub)) {
            codeDesRurbiquesValides.add(codeRubrique);
        }
        return codeDesRurbiquesValides;
    }

    /**
     * Construit la condition sur les structure de rattachements secondaire. Le codeStructure peut contenir plusieurs code séparé par des "+";
     *
     * @param codesStructures
     * @return
     */
    public static Condition traiterStructureMultiplePerimetre(final String codesStructures) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final ConditionList conditionStructureMultiple = new ConditionList();
        final String[] toutLesCodes = codesStructures.split("\\+");
        for (final String code : toutLesCodes) {
            if (StringUtils.isNotEmpty(code)) {
                final Collection<StructureModele> sousInfosStructures = serviceStructure.getAllSubStructures(code, LangueUtil.getIndiceLocaleDefaut(), true);
                conditionStructureMultiple.or(ConditionHelper.like("T1.META_CODE_RATTACHEMENT_AUTRES", code, "%[", "]%"));
                for (final StructureModele infoStructure : sousInfosStructures) {
                    conditionStructureMultiple.or(ConditionHelper.like("T1.META_CODE_RATTACHEMENT_AUTRES", infoStructure.getCode(), "%[", "]%"));
                }
            }
        }
        return conditionStructureMultiple;
    }

    /**
     *
     * @param ctx
     * @param diffusionPublicVise
     * @param diffusionModeRestriction
     * @param diffusionPublicViseRestriction
     * @param prefixeTableMeta
     * @return
     */
    public static Condition construireConditionDSI(final ContexteUniv ctx, final String diffusionPublicVise, final String diffusionModeRestriction, final String diffusionPublicViseRestriction, String prefixeTableMeta) {
        if (StringUtils.isEmpty(prefixeTableMeta)) {
            prefixeTableMeta = "T1";
        }
        final ConditionList conditionDSI = new ConditionList();
        if (isSousDSI(ctx)) {
            if (StringUtils.isEmpty(diffusionModeRestriction)) {
                conditionDSI.setPremiereCondtion(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_DIFFUSION_MODE_RESTRICTION", "0"));
            }
            conditionDSI.or(traiterProfilEtGroupe(ctx, diffusionModeRestriction, diffusionPublicVise, prefixeTableMeta));
            conditionDSI.or(traiterProfilDynamique(ctx, diffusionModeRestriction, diffusionPublicViseRestriction, prefixeTableMeta));
            if (Espacecollaboratif.isExtensionActivated()) {
                conditionDSI.or(traiterEspaceCollab(ctx, diffusionModeRestriction, diffusionPublicViseRestriction, prefixeTableMeta));
            }
        }
        return conditionDSI;
    }

    /**
     * Méthode de remplacement de  AbstractRequeteur#calculerRestrictionGroupeDsi(ContexteUniv, String, String)
     *
     * @param ctx
     * @param nomColonne
     * @param valeur
     * @return
     */
    private static Condition traiterRestrictionGroupeDSI(final ContexteUniv ctx, final String nomColonne, final String valeur) {
        final Collection<String> codeDesGroupesDSI = getListeCodesRestrictionGroupeDSI(ctx, valeur);
        final ConditionList conditionRestrictionDSI = new ConditionList();
        for (final String codeGroupeDSI : codeDesGroupesDSI) {
            conditionRestrictionDSI.or(ConditionHelper.like(nomColonne, codeGroupeDSI, "%[/", "]%"));
        }
        return conditionRestrictionDSI;
    }

    /**
     * Construit la condition sur les profils et les groupes lorsque le mode de diffusion le nécessite
     *
     * @param ctx
     * @param diffusionModeRestriction
     * @param diffusionPublicVise
     * @return
     */
    private static Condition traiterProfilEtGroupe(final ContexteUniv ctx, final String diffusionModeRestriction, final String diffusionPublicVise, final String prefixeTableMeta) {
        final ConditionList restrictionProfilGroupe = new ConditionList();
        if ("".equals(diffusionModeRestriction) || "2".equals(diffusionModeRestriction)) {
            restrictionProfilGroupe.setPremiereCondtion(traiterRestrictionGroupeDSI(ctx, prefixeTableMeta + ".META_DIFFUSION_PUBLIC_VISE", diffusionPublicVise));
            restrictionProfilGroupe.and(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_DIFFUSION_MODE_RESTRICTION", "2"));
        }
        return restrictionProfilGroupe;
    }

    /**
     * Construit la condition sur les profils dynamique lorsque le mode de diffusion le nécessite
     *
     * @param ctx
     * @param diffusionModeRestriction
     * @param diffusionPublicViseRestriction
     * @return
     */
    private static Condition traiterProfilDynamique(final ContexteUniv ctx, final String diffusionModeRestriction, final String diffusionPublicViseRestriction, final String prefixeTableMeta) {
        ConditionList restrictionProfilDynamique = new ConditionList();
        if ("".equals(diffusionModeRestriction) || "3".equals(diffusionModeRestriction)) {
            restrictionProfilDynamique = new ConditionList(traiterRestrictionGroupeDSI(ctx, prefixeTableMeta + ".META_DIFFUSION_PUBLIC_VISE_RESTRICTION", diffusionPublicViseRestriction));
            restrictionProfilDynamique.and(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_DIFFUSION_MODE_RESTRICTION", "3"));
        }
        return restrictionProfilDynamique;
    }

    private static boolean isSousDSI(final ContexteUniv ctx) {
        return "1".equals(ConstanteSQL.ACTIVATION_DSI) && ctx.isCalculListeResultatsFront();
    }

    /**
     * Construit la condition sur les espace collab lorsque le mode de diffusion le nécessite
     *
     * @param ctx
     * @param diffusionModeRestriction
     * @param diffusionPublicViseRestriction
     * @return
     */
    private static Condition traiterEspaceCollab(final ContexteUniv ctx, final String diffusionModeRestriction, final String diffusionPublicViseRestriction, final String prefixeTableMeta) {
        ConditionList conditionEspace = new ConditionList();
        if (("".equals(diffusionModeRestriction) || "4".equals(diffusionModeRestriction)) && StringUtils.isEmpty(diffusionPublicViseRestriction)) {
            final List<EspaceCollaboratifBean> espaces = getEspaceCollaboratifDao().getAll();
            final Set<String> espacesUtilisateur = new HashSet<>();
            for (final EspaceCollaboratifBean espace : espaces) {
                if (Espacecollaboratif.estMembreEspace(ctx, espace) || Espacecollaboratif.estVisiteurEspace(ctx, espace)) {
                    espacesUtilisateur.add(espace.getCode());
                }
            }
            if (espacesUtilisateur.isEmpty()) {
                espacesUtilisateur.add("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"); // c'est historique... désolé...
            }
            conditionEspace = new ConditionList(ConditionHelper.in(prefixeTableMeta + ".META_DIFFUSION_PUBLIC_VISE_RESTRICTION", espacesUtilisateur));
            conditionEspace.and(ConditionHelper.egalVarchar(prefixeTableMeta + ".META_DIFFUSION_MODE_RESTRICTION", "4"));
        }
        return conditionEspace;
    }

    /**
     * Migration de la méthode calculerRestrictionGroupeDSI. Retourne la liste des groupesDSI à traiter.
     *
     * @param ctx
     * @param valeur
     * @return
     */
    private static Collection<String> getListeCodesRestrictionGroupeDSI(final ContexteUniv ctx, final String valeur) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final Collection<String> listeGroupes = new HashSet<>();
        final Set<String> groupesDSIDuContexte = ctx.getGroupesDsi();
        boolean requete = true;
        if ((StringUtils.isNotEmpty(valeur) && !groupesDSIDuContexte.contains(valeur)) || groupesDSIDuContexte.isEmpty()) {
            requete = false;
        }
        if (requete) {
            for (final String codeGroupeDSIDuContexte : groupesDSIDuContexte) {
                listeGroupes.add(codeGroupeDSIDuContexte);
                GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupeDSIDuContexte);
                int niveau = serviceGroupeDsi.getLevel(group) - 1;
                while (niveau > 0) {
                    listeGroupes.add(group.getCodeGroupePere());
                    group = serviceGroupeDsi.getByCode(group.getCodeGroupePere());
                    niveau--;
                }
            }
        } else {
            listeGroupes.add("ZZZZZZZZZZZZZZZZZZZZ");
        }
        return listeGroupes;
    }
}
