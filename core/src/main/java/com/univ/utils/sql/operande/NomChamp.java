package com.univ.utils.sql.operande;

import com.univ.utils.EscapeString;

/**
 * Permet d'inserer un nom de champ dans la requête tel que T1.CODE = ...
 *
 * @author olivier.camon
 *
 */
public class NomChamp implements Operande {

    private String valeurnomChamp;

    public NomChamp(String nomChamp) {
        this.valeurnomChamp = nomChamp;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.operande.Operande#formaterOperande()
     */
    @Override
    public String formaterOperande() {
        if (valeurnomChamp == null) {
            return "";
        }
        return EscapeString.escapeSql(valeurnomChamp);
    }
}
