package com.univ.utils.sql.criterespecifique;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.utils.ContexteUniv;
import com.univ.utils.sql.ConstanteSQL;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;

/**
 * Classe utilitaire permettant de calculer les conditions pour la DSI lors d'une requête SQL. C'est une migration de SQLUtil.ajouterCriteresDsi(String, OMContext).
 *
 * @author olivier.camon
 *
 */
public final class CritereDSI {

    private static EspaceCollaboratifDAO getEspaceCollaboratifDao(){
        return ApplicationContextManager.getCoreContextBean(EspaceCollaboratifDAO.ID_BEAN, EspaceCollaboratifDAO.class);
    }

    /**
     * Migration de SQLUtil.ajouterCriteresDsi(String, OMContext) Calcule l'ensemble des conditions sur les critères dsi de la fiche courante. Si la diffusion est limitée
     * (diffusion selective), on ajoute des critères sur les ip et les hosts si renseigné dans le JTF (ancien comportement) les groupes et profils les espaces collab Les groupes
     * perso Les espaces perso
     *
     * @param ctx
     *            Le contexte si c'est selectif le contexte est de type ContexteUniv
     * @param fiche
     *            la fiche courante
     * @return les conditions DSI, null si il n'y en a pas.
     */
    public static Condition traiterCritereDSI(final OMContext ctx, final FicheUniv fiche) {
        ConditionList critereDSI = null;
        if (isDiffusionSelective(fiche, ctx)) {
            final ContexteUniv ctxUniv = (ContexteUniv) ctx;
            critereDSI = new ConditionList(traiterRestrictionDSI(ctxUniv));
            if (isGroupePersonnalisation(ctxUniv)) {
                final Condition persoGroupe = traiterPersonnalisationGroupe(ctxUniv);
                if (persoGroupe != null) {
                    critereDSI.and(persoGroupe);
                }
            }
            if (isEspacePersonnalisation(ctxUniv)) {
                critereDSI.and(traiterPersonnalisationEspace(ctxUniv));
            }
        }
        return critereDSI;
    }

    private static boolean isDiffusionSelective(final FicheUniv fiche, final OMContext ctx) {
        return fiche instanceof DiffusionSelective && "1".equals(ConstanteSQL.ACTIVATION_DSI) && ctx instanceof ContexteUniv && ((ContexteUniv) ctx).isCalculListeResultatsFront();
    }

    private static ConditionList traiterRestrictionDSI(final ContexteUniv ctx) {
        final ConditionList conditionRestrictionDSI = new ConditionList(ConditionHelper.egalVarchar("T1.DIFFUSION_MODE_RESTRICTION", "0"));
        final Collection<String> groupesAVerifier = getAllGroupesDSIDuCtx(ctx.getGroupesDsi());
        final ConditionList conditionPublicVise = traiterGroupeDSI(groupesAVerifier, "T1.DIFFUSION_PUBLIC_VISE", "2");
        if (conditionPublicVise != null) {
            conditionRestrictionDSI.or(conditionPublicVise);
        }
        final ConditionList conditionPublicViseRestriction = traiterGroupeDSI(groupesAVerifier, "T1.DIFFUSION_PUBLIC_VISE_RESTRICTION", "3");
        if (conditionPublicViseRestriction != null) {
            conditionRestrictionDSI.or(conditionPublicViseRestriction);
        }
        if(Espacecollaboratif.isExtensionActivated()) {
            final ConditionList conditionEspaceCollab = traiterEspaceCollab(ctx);
            if (conditionEspaceCollab != null) {
                conditionRestrictionDSI.or(conditionEspaceCollab);
            }
        }
        return conditionRestrictionDSI;
    }

    private static Collection<String> getAllGroupesDSIDuCtx(final Set<String> codesGroupesCourant) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final Collection<String> groupesAVerifier = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(codesGroupesCourant)) {
            groupesAVerifier.addAll(codesGroupesCourant);
            for (final String codeGroupeCourant : codesGroupesCourant) {
                GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupeCourant);
                int niveau = serviceGroupeDsi.getLevel(group) - 1;
                while (niveau > 0) {
                    group = serviceGroupeDsi.getByCode(group.getCodeGroupePere());
                    groupesAVerifier.add(group.getCode());
                    niveau--;
                }
            }
        }
        return groupesAVerifier;
    }

    private static ConditionList traiterGroupeDSI(final Collection<String> groupesAVerifier, final String nomColonnePublicVise, final String valeurDiffusionModeRestriction) {
        final ConditionList restrictionGroupe = new ConditionList();
        final ConditionList conditionGroupeDSI = creerConditionProfilEtGroupe(nomColonnePublicVise, groupesAVerifier);
        if (conditionGroupeDSI != null) {
            restrictionGroupe.setPremiereCondtion(ConditionHelper.egalVarchar("T1.DIFFUSION_MODE_RESTRICTION", valeurDiffusionModeRestriction));
            restrictionGroupe.and(conditionGroupeDSI);
        }
        return restrictionGroupe;
    }

    private static ConditionList creerConditionProfilEtGroupe(final String nomColonne, final Collection<String> listeDesGroupes) {
        ConditionList conditionProfilGroupe = new ConditionList();
        boolean isFirst = Boolean.TRUE;
        for (final String codeGroupe : listeDesGroupes) {
            if (isFirst) {
                conditionProfilGroupe.setPremiereCondtion(ConditionHelper.like(nomColonne, codeGroupe, "%[/", "]%"));
            } else {
                conditionProfilGroupe.or(ConditionHelper.like(nomColonne, codeGroupe, "%[/", "]%"));
            }
            isFirst = Boolean.FALSE;
        }
        return conditionProfilGroupe;
    }

    private static ConditionList traiterEspaceCollab(final ContexteUniv ctx) {
        ConditionList conditionEspaceCollab = null;
        final Set<String> espacesAccessibles = new TreeSet<>();
        final Collection<EspaceCollaboratifBean> listeEspaces = getEspaceCollaboratifDao().getAll();
        for (EspaceCollaboratifBean espace : listeEspaces) {
            if (Espacecollaboratif.estMembreEspace(ctx, espace) || Espacecollaboratif.estVisiteurEspace(ctx, espace)) {
                espacesAccessibles.add(espace.getCode());
            }
        }
        if (!espacesAccessibles.isEmpty()) {
            conditionEspaceCollab = new ConditionList(ConditionHelper.egalVarchar("T1.DIFFUSION_MODE_RESTRICTION", "4"));
            conditionEspaceCollab.and(ConditionHelper.in("T1.DIFFUSION_PUBLIC_VISE_RESTRICTION", espacesAccessibles));
        }
        return conditionEspaceCollab;
    }

    private static boolean isGroupePersonnalisation(final ContexteUniv ctx) {
        return StringUtils.isNotEmpty(ctx.getGroupePersonnalisationCourant());
    }

    private static boolean isEspacePersonnalisation(final ContexteUniv ctx) {
        return StringUtils.isNotEmpty(ctx.getEspacePersonnalisationCourant());
    }

    private static Condition traiterPersonnalisationGroupe(final ContexteUniv ctx) {
        if (ctx.getAutorisation() == null) {
            return ConstanteSQL.CONDITION_DEFAUT_POUR_GROUPE;
        }
        Collection<String> codesDesGroupes = null;
        final String groupePersoCourant = ctx.getGroupePersonnalisationCourant();
        if (ConstanteSQL.DYNAMIK.equals(groupePersoCourant)) {
            codesDesGroupes = getAllGroupesDSIDuCtx(ctx.getGroupesDsi());
        } else if (groupePersoCourant.endsWith(ConstanteSQL.TYPE_GROUPE)) {
            final String type = StringUtils.substringBefore(groupePersoCourant, ConstanteSQL.TYPE_GROUPE);
            codesDesGroupes = getGroupesDSIDuCtxParType(ctx.getGroupesDsi(), type);
            if (codesDesGroupes.isEmpty()) {
                return ConstanteSQL.CONDITION_DEFAUT_POUR_GROUPE;
            }
        } else {
            codesDesGroupes = Arrays.asList(ctx.getGroupePersonnalisationCourant().split(";"));
        }
        return creerConditionProfilEtGroupe("T1.DIFFUSION_PUBLIC_VISE", codesDesGroupes);
    }

    private static Collection<String> getGroupesDSIDuCtxParType(final Set<String> codesGroupesCourant, final String type) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final Collection<String> groupesAVerifier = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(codesGroupesCourant)) {
            for (final String codeGroupeCourant : codesGroupesCourant) {
                GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupeCourant);
                if(group != null) {
                    if (type.equals(group.getType())) {
                        groupesAVerifier.add(group.getCode());
                    }
                    int niveau = serviceGroupeDsi.getLevel(group) - 1;
                    while (niveau > 0 && group != null) {
                        group = serviceGroupeDsi.getByCode(group.getCodeGroupePere());
                        if (group != null && type.equals(group.getType())) {
                            groupesAVerifier.add(group.getCode());
                        }
                        niveau--;
                    }
                }
            }
        }
        return groupesAVerifier;
    }

    private static Condition traiterPersonnalisationEspace(final ContexteUniv ctx) {
        final String espace = ctx.getEspacePersonnalisationCourant();
        return ConditionHelper.egalVarchar("T1.DIFFUSION_PUBLIC_VISE_RESTRICTION", espace);
    }
}
