package com.univ.utils.sql.clause;

import com.univ.utils.sql.ConstanteSQL;

/**
 * Permet de construire une clause limit d'une requête SQL exemple : LIMIT FOO 1, 5
 *
 * @author olivier.camon
 *
 */
public class ClauseLimit implements ClauseSQL {

    private int offset = -1;

    private int nbLignes = -1;

    /**
     * Instancie une nouvelle clause LIMIT et initialise le nombre de ligne maximum de la requête
     *
     * @param nbLignes
     */
    public ClauseLimit(int nbLignes) {
        this.nbLignes = nbLignes;
    }

    /**
     * Instancie une nouvelle clause LIMIT et initialise le premier tuble à prendre en compte et le nombre de ligne maximum
     *
     * @param offset
     *            le premier tuple à prendre en compte
     * @param nbLignes
     *            le nombre de tuple maximum de la requête
     */
    public ClauseLimit(int offset, int nbLignes) {
        this.nbLignes = nbLignes;
        this.offset = offset;
    }

    /**
     * Change le numéro du premier tuple à prendre en compte
     *
     * @param offset
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * Change le nombre de tuple à retourner
     *
     * @param nbLignes
     */
    public void setNbLignes(int nbLignes) {
        this.nbLignes = nbLignes;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.clause.ClauseSQL#formaterSQL()
     */
    @Override
    public String formaterSQL() {
        StringBuilder clauseLimit = new StringBuilder();
        if (nbLignes > 0) {
            clauseLimit.append(ConstanteSQL.LIMIT);
            if (offset >= 0) {
                clauseLimit.append(offset).append(" ,");
            }
            clauseLimit.append(nbLignes);
        }
        return clauseLimit.toString();
    }
}
