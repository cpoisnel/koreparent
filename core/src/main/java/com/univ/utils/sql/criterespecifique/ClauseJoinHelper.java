package com.univ.utils.sql.criterespecifique;

import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.sql.clause.ClauseJoin;
import com.univ.utils.sql.clause.ClauseJoin.TypeJointure;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.operande.TypeOperande;

public class ClauseJoinHelper {

    /**
     * Permet de créer une jointure de type LEFT JOIN sur la table METATAG à partir d'une fiche passé en paramètre.
     *
     * @param fiche
     *            permet de connaitre le nom de la classe de l'objet ficheUniv, ne doit pas être null
     * @return
     */
    public static ClauseJoin creerJointureMetaTag(final FicheUniv fiche) {
        final ClauseJoin joinMetaDonnee = new ClauseJoin(TypeJointure.LEFT_JOIN, "METATAG META");
        final String codeObjet = ReferentielObjets.getCodeObjet(fiche);
        joinMetaDonnee.on(ConditionHelper.critereJointureSimple("T1.ID_" + ReferentielObjets.getNomTableSql(codeObjet), "META.META_ID_FICHE"));
        joinMetaDonnee.and(ConditionHelper.egalVarchar("META.META_CODE_OBJET", codeObjet));
        return joinMetaDonnee;
    }

    /**
     * Permet de créer une jointure de type LEFT JOIN sur la table RUBRIQUEPUBLICATION à partir d'une fiche passé en paramètre.
     *
     * @param fiche
     *            permet de connaitre le code de l'objet, ne doit pas être null
     * @return
     */
    public static ClauseJoin creerJointureRubPub(final FicheUniv fiche) {
        final ClauseJoin joinRubPub = new ClauseJoin(TypeJointure.LEFT_JOIN, "RUBRIQUEPUBLICATION RUB_PUB");
        final String codeObjet = ReferentielObjets.getCodeObjet(fiche);
        joinRubPub.on(ConditionHelper.critereJointureSimple("T1.CODE", "RUB_PUB.CODE_FICHE_ORIG"));
        joinRubPub.and(ConditionHelper.egal("T1.LANGUE", "RUB_PUB.LANGUE_FICHE_ORIG", TypeOperande.NOM_CHAMP));
        joinRubPub.and(ConditionHelper.egalVarchar("RUB_PUB.TYPE_FICHE_ORIG", codeObjet));
        return joinRubPub;
    }

    /**
     * Creer une jointure basique en fonction du type de jointure fourni du nom de la table et de la condition fourni.
     *
     * @param type
     * @param nomTableEtAlias
     * @param conditionDeJointure
     * @return
     */
    public static ClauseJoin creerJointure(final TypeJointure type, final String nomTableEtAlias, final Condition conditionDeJointure) {
        final ClauseJoin jointure = new ClauseJoin(type, nomTableEtAlias);
        jointure.on(conditionDeJointure);
        return jointure;
    }
}
