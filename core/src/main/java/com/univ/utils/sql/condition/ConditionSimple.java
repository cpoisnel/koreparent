package com.univ.utils.sql.condition;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.sql.Operateur;
import com.univ.utils.sql.operande.Operande;

public class ConditionSimple implements Condition {

    private static final String VALEUR_RETOUR_SI_VIDE = "";

    private Operande premiereOperande;

    private Operateur operateur;

    private Operande deuxiemeOperande;

    public ConditionSimple() {
        super();
    }

    public ConditionSimple(Operande premiereOperande, Operateur operateur, Operande deuxiemeOperande) {
        setPremiereOperande(premiereOperande);
        setOperateur(operateur);
        setDeuxiemeOperande(deuxiemeOperande);
    }

    public Operande getPremiereOperande() {
        return premiereOperande;
    }

    public void setPremiereOperande(Operande premiereOperande) {
        this.premiereOperande = premiereOperande;
    }

    public Operateur getOperateur() {
        return operateur;
    }

    public void setOperateur(Operateur operateur) {
        this.operateur = operateur;
    }

    public Operande getDeuxiemeOperande() {
        return deuxiemeOperande;
    }

    public void setDeuxiemeOperande(Operande deuxiemeOperande) {
        this.deuxiemeOperande = deuxiemeOperande;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.condition.Condition#formaterCondition()
     */
    @Override
    public String formaterCondition() {
        if (isAttributNull()) {
            return VALEUR_RETOUR_SI_VIDE;
        }
        String formatPremiereOperande = premiereOperande.formaterOperande();
        String formatDeuxiemeOperande = deuxiemeOperande.formaterOperande();
        if (StringUtils.isEmpty(formatPremiereOperande) || StringUtils.isEmpty(formatDeuxiemeOperande)) {
            return VALEUR_RETOUR_SI_VIDE;
        }
        return premiereOperande.formaterOperande() + operateur.getOperateur() + deuxiemeOperande.formaterOperande();
    }

    private Boolean isAttributNull() {
        return premiereOperande == null || deuxiemeOperande == null || operateur == null;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.condition.Condition#isEmpty()
     */
    @Override
    public boolean isEmpty() {
        return StringUtils.isEmpty(formaterCondition());
    }
}
