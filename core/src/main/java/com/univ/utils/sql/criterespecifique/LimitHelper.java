package com.univ.utils.sql.criterespecifique;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.utils.sql.clause.ClauseLimit;

/**
 * Classe utilitaire calculant les critères limit d'une requête SQL
 *
 * @author olivier.camon
 *
 */
public class LimitHelper {

    /**
     * Migration de la méthode SQLUtil.ajouterCriteresLimitesEtOptimisation(String, OMContext, String)}
     *
     * @param ctx
     * @param limit
     * @return
     */
    public static ClauseLimit ajouterCriteresLimitesEtOptimisation(OMContext ctx, final String limit) {
        ClauseLimit limite = new ClauseLimit(0);
        if (StringUtils.isNotEmpty(limit) && StringUtils.isNumeric(limit)) {
            int nbLignes = Integer.parseInt(limit);
            if (ctx.getDatas().get("optimizedSelect") != null) {
                ctx.getDatas().put("optimizedLimit", limit);
            } else {
                limite.setNbLignes(nbLignes);
            }
        }
        ctx.getDatas().put("optimizedObject", "true");
        return limite;
    }

    public static ClauseLimit ajouterCriteresLimitesEtOptimisation(OMContext ctx, final int offset, final int limit) {
        ClauseLimit limite = new ClauseLimit(0);
        if (limit > 0) {
            if (ctx.getDatas().get("optimizedSelect") != null) {
                ctx.getDatas().put("optimizedLimit", String.valueOf(limit));
            } else {
                limite.setNbLignes(limit);
                limite.setOffset(offset);
            }
        }
        return limite;
    }
}
