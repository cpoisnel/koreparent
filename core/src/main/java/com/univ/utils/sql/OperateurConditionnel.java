package com.univ.utils.sql;

/**
 * Les deux opéranteurs pouvant séparé deux conditions entre elles.
 *
 * @author olivier.camon
 *
 */
public enum OperateurConditionnel {
    OR(" OR "), AND(" AND ");

    private String operateur;

    private OperateurConditionnel(String valeur) {
        operateur = valeur;
    }

    public String getOperateur() {
        return operateur;
    }
}
