package com.univ.utils.sql.operande;

/**
 * Liste les types d'opérandes possible.
 *
 * @author olivier.camon
 *
 */
public enum TypeOperande {
    NOM_CHAMP, VARCHAR, LISTE_VARCHAR, LONG, LISTE_NOMBRE, TEXTE, DATE, DATE_ET_HEURE, NON_ECHAPABLE, INTEGER;
}
