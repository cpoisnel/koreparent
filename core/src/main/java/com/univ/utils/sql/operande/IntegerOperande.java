package com.univ.utils.sql.operande;

public class IntegerOperande implements Operande {

    private Object valeurDuInt;

    public IntegerOperande(Integer valeur) {
        this.valeurDuInt = valeur;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.operande.Operande#formaterOperande()
     */
    @Override
    public String formaterOperande() {
        if (valeurDuInt == null) {
            return "";
        }
        return String.valueOf(valeurDuInt);
    }
}
