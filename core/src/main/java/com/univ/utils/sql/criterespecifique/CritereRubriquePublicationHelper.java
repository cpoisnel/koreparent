package com.univ.utils.sql.criterespecifique;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.ContexteUniv;
import com.univ.utils.IRequeteurConstantes;
import com.univ.utils.sql.ConstanteSQL;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;

/**
 * Classe utilitaire calculant les conditions sur la table RUBRIQUEPUBLICATION.
 *
 * @author olivier.camon
 *
 */
public final class CritereRubriquePublicationHelper {

    protected static Condition traiterConditionRubPubIdRubriqueAndFicheOrig(final FicheUniv fiche) {
        final String codeObjet = ReferentielObjets.getCodeObjet(fiche);
        final ConditionList critereRubPub = new ConditionList(ConditionHelper.isNull("RUB_PUB.ID_RUBRIQUEPUBLICATION"));
        critereRubPub.or(ConditionHelper.egalVarchar("RUB_PUB.TYPE_FICHE_ORIG", codeObjet));
        return critereRubPub;
    }

    protected static Condition traiterCritereRubPubDSIFiche(final OMContext ctx, final String codeRubrique) {
        final Set<String> listeCodeRubriques = getCodesRubriquesSuivantCritereDSIDeLaFiche(ctx, codeRubrique);
        final ConditionList conditionRubPub = new ConditionList(ConditionHelper.in("T1.CODE_RUBRIQUE", listeCodeRubriques));
        conditionRubPub.or(ConditionHelper.in("RUB_PUB.RUBRIQUE_DEST", listeCodeRubriques));
        return conditionRubPub;
    }

    private static Set<String> getCodesRubriquesSuivantCritereDSIDeLaFiche(final OMContext ctx, String codeRubrique) {
        final Set<String> listeCodesRubriques = new HashSet<>();
        String codeRubriqueSite = null;
        boolean rechercheArborescente = Boolean.TRUE;
        InfosSite infosSite = null;
        ContexteUniv ctxUniv = null;
        if (ctx instanceof ContexteUniv && ((ContexteUniv) ctx).isCalculListeResultatsFront()) {
            infosSite = ctx.getInfosSite();
            ctxUniv = (ContexteUniv) ctx;
            if (isSiteCloisonne(infosSite)) {
                codeRubriqueSite = infosSite.getCodeRubrique();
            }
        }
        if (StringUtils.isNotEmpty(codeRubrique) && codeRubrique.endsWith("_NOARBO")) {
            codeRubrique = StringUtils.substringBefore(codeRubrique, "_NOARBO");
            rechercheArborescente = Boolean.FALSE;
        }
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(codeRubrique);
        codeRubrique = getCodeRubriqueCloisonnee(infosSite, rubrique, codeRubrique, codeRubriqueSite);
        rubrique = serviceRubrique.getRubriqueByCode(codeRubrique);
        if (isCodeExisteMaisPasRubrique(codeRubrique, rubrique)) {
            codeRubrique = ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE;
        }
        final Collection<RubriqueBean> listeRubriques = getListeRubriques(ctxUniv, rubrique, codeRubrique, IRequeteurConstantes.MODE_CONSULTATION, rechercheArborescente);
        for (final RubriqueBean rubCourante : listeRubriques) {
            listeCodesRubriques.add(rubCourante.getCode());
        }
        return listeCodesRubriques;
    }

    protected static Condition traiterCritereRubriquePublicationSuivantAction(final OMContext ctx, final String action, final String nomColonne, final String codeRubrique) {
        final Set<String> listeCodeRubriques = getCodesRubriquesSuivantAction(ctx, action, codeRubrique);
        final ConditionList conditionRubPub = new ConditionList(ConditionHelper.in(nomColonne, listeCodeRubriques));
        conditionRubPub.or(ConditionHelper.in("RUB_PUB.RUBRIQUE_DEST", listeCodeRubriques));
        return conditionRubPub;
    }

    public static Set<String> getCodesRubriquesSuivantAction(final OMContext ctx, final String action, String codeRubrique) {
        final Set<String> listeCodesRubriques = new HashSet<>();
        String codeRubriqueSite = null;
        InfosSite infosSite = null;
        final boolean rechercheArborescente = !StringUtils.endsWith(codeRubrique, "_NOARBO");
        codeRubrique = StringUtils.removeEnd(codeRubrique, "_NOARBO");
        if (IRequeteurConstantes.MODE_CONSULTATION.equals(action)) {
            infosSite = ctx.getInfosSite();
            if (isSiteCloisonne(infosSite)) {
                codeRubriqueSite = infosSite.getCodeRubrique();
            }
        }
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(codeRubrique);
        codeRubrique = getCodeRubriqueCloisonnee(infosSite, rubrique, codeRubrique, codeRubriqueSite);
        rubrique = serviceRubrique.getRubriqueByCode(codeRubrique);
        if (isCodeExisteMaisPasRubrique(codeRubrique, rubrique)) {
            codeRubrique = ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE;
        }
        final Collection<RubriqueBean> listeRubriques = getListeRubriques(ctx, rubrique, codeRubrique, action, rechercheArborescente);
        for (final RubriqueBean rubCourante : listeRubriques) {
            listeCodesRubriques.add(rubCourante.getCode());
        }
        return listeCodesRubriques;
    }

    private static Collection<RubriqueBean> getListeRubriques(final OMContext ctxUniv, RubriqueBean rubrique, final String codeRubrique, final String action, final boolean rechercheArborescente) {
        Collection<RubriqueBean> listeRubriques = new HashSet<>();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        if (isCodeRubriqueExistante(codeRubrique)) {
            if (IRequeteurConstantes.MODE_CONSULTATION.equals(action) && ctxUniv != null && ctxUniv instanceof ContexteUniv && "1".equals(ConstanteSQL.ACTIVATION_DSI)) {
                if (serviceRubrique.controlerRestrictionRubrique(((ContexteUniv) ctxUniv).getGroupesDsiAvecAscendants(), codeRubrique)) {
                    if (rechercheArborescente) {
                        listeRubriques = serviceRubrique.determinerListeSousRubriquesAutorisees(((ContexteUniv) ctxUniv).getGroupesDsiAvecAscendants(), rubrique);
                    }
                } else {
                    rubrique = new RubriqueBean();
                    rubrique.setCode(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE);
                }
                if (rubrique != null) {
                    listeRubriques.add(rubrique);
                }
            } else if (StringUtils.isNotEmpty(codeRubrique)) {
                if (rechercheArborescente) {
                    listeRubriques = serviceRubrique.getAllChilds(rubrique.getCode());
                }
                if (rubrique != null) {
                    listeRubriques.add(rubrique);
                }
            }
        } else {
            rubrique = new RubriqueBean();
            rubrique.setCode(ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE);
            listeRubriques.add(rubrique);
        }
        return listeRubriques;
    }

    private static boolean isSiteCloisonne(final InfosSite infosSite) {
        return infosSite != null && infosSite.getRestriction() == 1;
    }

    private static String getCodeRubriqueCloisonnee(final InfosSite infosSite, final RubriqueBean rubrique, final String codeRubrique, final String codeRubriqueSite) {
        String codeRubriqueRetour = codeRubrique;
        if (StringUtils.isNotEmpty(codeRubriqueSite)) {
            if (isCodeExisteMaisPasRubrique(codeRubrique, rubrique)) {
                codeRubriqueRetour = ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE;
            } else if (StringUtils.isEmpty(codeRubrique)) {
                codeRubriqueRetour = codeRubriqueSite;
            } else if (!infosSite.isRubriqueVisibleInSite(rubrique)) {
                codeRubriqueRetour = ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE;
            }
        }
        return codeRubriqueRetour;
    }

    private static boolean isCodeExisteMaisPasRubrique(final String codeRubrique, final RubriqueBean infoRub) {
        return StringUtils.isNotEmpty(codeRubrique) && infoRub == null;
    }

    private static boolean isCodeRubriqueExistante(final String codeRubrique) {
        return !ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE.equals(codeRubrique);
    }
}
