package com.univ.utils.sql.clause;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.sql.Operateur;
import com.univ.utils.sql.OperateurConditionnel;
import com.univ.utils.sql.condition.Condition;

/**
 * Représente une clause JOIN en SQL. Elle contient le nom de la table et 1 ou + critère de jointure. Les Jointures peuvent être de type {@link TypeJointure}
 *
 * @author olivier.camon
 *
 */
public class ClauseJoin implements ClauseSQL {

    private TypeJointure typeJointure;

    private String nomTable;

    private Condition premierCritere;

    private List<Condition> critereJointure;

    private List<OperateurConditionnel> operateurDeCondition;

    /**
     * Constructeur par défaut
     */
    public ClauseJoin() {
        critereJointure = new ArrayList<>();
        operateurDeCondition = new ArrayList<>();
    }

    /**
     * Constructeur initialisant directement une jointure
     */
    public ClauseJoin(TypeJointure typeJointure, String nomTable) {
        this.typeJointure = typeJointure;
        this.nomTable = nomTable;
        critereJointure = new ArrayList<>();
        operateurDeCondition = new ArrayList<>();
    }

    /**
     * Défini le nom de la table sur laquelle on souhaite faire une jointure
     *
     * @param nomTable
     *            : le nom de la table à joindre
     */
    public void setNomTable(String nomTable) {
        this.nomTable = nomTable;
    }

    public String getNomTable() {
        return nomTable;
    }

    /**
     * Défini le type de jointure (join, inner join, outer join...) de la jointure
     *
     * @param typeJointure
     */
    public void setTypeJointure(TypeJointure typeJointure) {
        this.typeJointure = typeJointure;
    }

    /**
     * ajoute la première condition de la jointure
     *
     * @param condition
     */
    public void on(Condition condition) {
        premierCritere = condition;
    }

    /**
     * rajoute une condition OR sur la jointure courante
     *
     * @param condition
     *            la condition à ajouter
     * @return l'instance de clause join courante
     */
    public ClauseJoin or(Condition condition) {
        operateurDeCondition.add(OperateurConditionnel.OR);
        critereJointure.add(condition);
        return this;
    }

    /**
     * rajoute une condition AND sur la jointure courante
     *
     * @param condition
     *            la condition à ajouter
     * @return l'instance de clause join courante
     */
    public ClauseJoin and(Condition condition) {
        operateurDeCondition.add(OperateurConditionnel.AND);
        critereJointure.add(condition);
        return this;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.clause.ClauseSQL#formaterSQL()
     */
    @Override
    public String formaterSQL() {
        StringBuilder requeteJoin = new StringBuilder();
        if (isAttributValide()) {
            return requeteJoin.toString();
        }
        requeteJoin.append(premierCritere.formaterCondition());
        for (int i = 0; i < operateurDeCondition.size(); ++i) {
            String sousCondition = critereJointure.get(i).formaterCondition();
            if (StringUtils.isNotEmpty(sousCondition)) {
                requeteJoin.append(operateurDeCondition.get(i).getOperateur()).append(sousCondition);
            }
        }
        requeteJoin = ajouterClauseJoinSiNonVide(requeteJoin);
        return requeteJoin.toString();
    }

    private boolean isAttributValide() {
        return premierCritere == null || typeJointure == null || StringUtils.isEmpty(nomTable);
    }

    private StringBuilder ajouterClauseJoinSiNonVide(StringBuilder requeteSQLFormater) {
        StringBuilder enteteClauseJoin = new StringBuilder();
        if (StringUtils.isNotEmpty(requeteSQLFormater.toString())) {
            enteteClauseJoin.append(typeJointure.getType());
            enteteClauseJoin.append(nomTable);
            enteteClauseJoin.append(Operateur.ON.getOperateur());
        }
        enteteClauseJoin.append(requeteSQLFormater);
        return enteteClauseJoin;
    }

    /**
     * Contient l'ensemble des types de jointures autorisé
     *
     * @author olivier.camon
     *
     */
    public enum TypeJointure {
        JOINT(" JOIN "),
        INNER_JOIN(" INNER JOIN "),
        LEFT_JOIN(" LEFT JOIN "),
        NATURAL_JOIN(" NATURAL JOIN "),
        RIGHT_JOIN(" RIGHT JOIN "),
        CROSS_JOIN(" CROSS JOIN "),
        RIGHT_OUTER_JOIN(" RIGHT OUTER JOIN "),
        LEFT_OUTER_JOIN(" LEFT OUTER JOIN "),
        FULL_OUTER_JOIN(" FULL OUTER JOIN ");

        private String typeJointure;

        private TypeJointure(String valeur) {
            typeJointure = valeur;
        }

        public String getType() {
            return typeJointure;
        }
    }
}
