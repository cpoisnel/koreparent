package com.univ.utils.sql.clause;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.EscapeString;
import com.univ.utils.sql.ConstanteSQL;

/**
 * Représente une clause ORDER BY d'une requête SQL. une clause Order by contient un ensemble de nom de champ et d'opérateur (ASC / DESC / rien) Exemple : ORDER BY FOO, BAR DESC
 *
 * @author olivier.camon
 *
 */
public class ClauseOrderBy implements ClauseSQL {

    private List<String> colonnesOrdersBy;

    private List<SensDeTri> ordres;

    /**
     * Constructeur par défaut
     */
    public ClauseOrderBy() {
        colonnesOrdersBy = new ArrayList<>();
        ordres = new ArrayList<>();
    }

    /**
     * Constructeur initialisant une première clause Order by
     *
     * @param nomChamp
     *            le champ à trié
     * @param sens
     *            le sens du tri
     */
    public ClauseOrderBy(String nomChamp, SensDeTri sens) {
        colonnesOrdersBy = new ArrayList<>();
        ordres = new ArrayList<>();
        colonnesOrdersBy.add(nomChamp);
        ordres.add(sens);
    }

    /**
     * Rajoute un ordre à la clause Order by
     *
     * @param nomChamp
     *            le champ à trié
     * @param sens
     *            le sens du tri
     * @return l'instance de la clause Order by courante
     */
    public ClauseOrderBy orderBy(String nomChamp, SensDeTri sens) {
        colonnesOrdersBy.add(nomChamp);
        ordres.add(sens);
        return this;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.clause.ClauseSQL#formaterSQL()
     */
    @Override
    public String formaterSQL() {
        StringBuilder requeteSQLOrder = new StringBuilder();
        boolean isFirst = Boolean.TRUE;
        for (int i = 0; i < colonnesOrdersBy.size(); ++i) {
            String nomColonne = colonnesOrdersBy.get(i);
            if (StringUtils.isNotEmpty(nomColonne)) {
                if (!isFirst) {
                    requeteSQLOrder.append(", ");
                }
                requeteSQLOrder.append(EscapeString.escapeSql(nomColonne));
                if (ordres.get(i) != null && StringUtils.isNotEmpty(ordres.get(i).getSens())) {
                    requeteSQLOrder.append(ordres.get(i).getSens());
                }
                isFirst = Boolean.FALSE;
            }
        }
        requeteSQLOrder = ajouterClauseOrderSiNonVide(requeteSQLOrder);
        return requeteSQLOrder.toString();
    }

    private StringBuilder ajouterClauseOrderSiNonVide(StringBuilder requeteSQLFormater) {
        if (StringUtils.isNotEmpty(requeteSQLFormater.toString())) {
            requeteSQLFormater.insert(0, ConstanteSQL.ORDER_BY);
        }
        return requeteSQLFormater;
    }

    /**
     * énumère les sens de tri possible pour la clause Order BY
     *
     * @author olivier.camon
     *
     */
    public enum SensDeTri {
        ASC(" ASC "), DESC(" DESC ");

        private String sens;

        private SensDeTri(String valeur) {
            sens = valeur;
        }

        public String getSens() {
            return sens;
        }
    }
}
