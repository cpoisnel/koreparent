package com.univ.utils.sql.operande;

/**
 * Opérande non échappable utile pour utilier des fonctions tel que REGEXP ou DATE(...) cet opérande n'est pas échappé, il faut que l'utilisateur ÉCHAPE ABSOLUMENT SA VALEUR LUI
 * MÊME lors de l'utilisation de cet opérande
 *
 * @author olivier.camon
 *
 */
public class NonEchapable implements Operande {

    private Object valeurNonEchapable;

    public NonEchapable(String valeur) {
        this.valeurNonEchapable = valeur;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.operande.Operande#formaterOperande()
     */
    @Override
    public String formaterOperande() {
        if (valeurNonEchapable == null) {
            return "";
        }
        return valeurNonEchapable.toString();
    }
}
