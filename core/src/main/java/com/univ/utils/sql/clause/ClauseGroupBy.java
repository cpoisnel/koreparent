package com.univ.utils.sql.clause;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.EscapeString;
import com.univ.utils.sql.ConstanteSQL;

/**
 * Représente une clause GROUP BY d'une requête SQL. une clause Group By contient un ensemble de nom de champ. Exemple : GROUP BY FOO, BAR
 *
 * @author olivier.camon
 *
 */
public class ClauseGroupBy implements ClauseSQL {

    public List<String> groupBy;

    public ClauseGroupBy() {
        groupBy = new ArrayList<>();
    }

    /**
     * Ajoute une règle de regroupement à la clause.
     *
     * @param nomChamp
     *            le champ que l'on souhaite ajouter au group by
     * @return l'instance courante de la clause group by
     */
    public ClauseGroupBy groupBy(String nomChamp) {
        groupBy.add(nomChamp);
        return this;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.utils.sql.clause.ClauseSQL#formaterSQL()
     */
    @Override
    public String formaterSQL() {
        StringBuilder groupByFormater = new StringBuilder();
        boolean isFirst = Boolean.TRUE;
        for (String valeurGroupBy : groupBy) {
            if (!isFirst) {
                groupByFormater.append(" , ");
            }
            groupByFormater.append(EscapeString.escapeSql(valeurGroupBy));
            isFirst = Boolean.FALSE;
        }
        groupByFormater = ajouterClauseGroupBySiNonVide(groupByFormater);
        return groupByFormater.toString();
    }

    private StringBuilder ajouterClauseGroupBySiNonVide(StringBuilder requeteSQLFormater) {
        if (StringUtils.isNotEmpty(requeteSQLFormater.toString())) {
            requeteSQLFormater.insert(0, ConstanteSQL.GROUP_BY);
        }
        return requeteSQLFormater;
    }
}
