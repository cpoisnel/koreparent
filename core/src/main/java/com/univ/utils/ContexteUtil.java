package com.univ.utils;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * classe stockant un Contexte dans un ThreadLocal afin de ne pas à avoir à en instancier un à chaque fois ou de le passer dans toute les entêtes de méthodes
 *
 * @author olivier.camon
 *
 */
public abstract class ContexteUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContexteUtil.class);

    private static final ThreadLocal<ContexteUniv> threadLocal = new ThreadLocal<>();

    /**
     * Initialise un contexte a partir de la request et de la réponse A la différence des autres méthodes, le contexte est stocké dans un thread local permettant de le récupérer
     * sans en instancié de nouveau. Cette méthode doit être appelé une seule fois dans un filtre qui se chargera aussi de le releaser afin de centraliser la gestion des contextes.
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    public static ContexteUniv setContexteUniv(final HttpServletRequest request, final HttpServletResponse response, final ServletContext servletContext) {
        final ContexteUniv ctx = new ContexteUniv(request);
        ctx.setReponseHTTP(response);
        ctx.initialiserInfosUtilisateur();
        ctx.initialiserInfosConnecteurs();
        ctx.setEspaceCourant(ctx.getEspaceHistorique());
        ctx.setServletContext(servletContext);
        //Initialisation de la langue à partir de la variable LOCALE de l'URL
        initLangueUtilisateur(request, ctx);
        ajoutDansThreadLocal(ctx);
        return ctx;
    }

    private static void initLangueUtilisateur(final HttpServletRequest request, final ContexteUniv ctx) {
        String codeRubrique = StringUtils.EMPTY;
        if (ctx.getRequeteHTTP() != null) {
            codeRubrique = StringUtils.defaultString(ctx.getRequeteHTTP().getParameter("RF"), ctx.getCodeRubriqueHistorique());
        }
        boolean isLocaleSet = Boolean.FALSE;
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        if (StringUtils.isNotBlank(codeRubrique)) {
            final RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(StringUtils.defaultString(codeRubrique, ctx.getCodeRubriqueHistorique()));
            if (rubriqueCourante != null) {
                isLocaleSet = Boolean.TRUE;
                ctx.setLangue(rubriqueCourante.getLangue());
                ctx.setLocale(LangueUtil.getLocale(rubriqueCourante.getLangue()));
            }
        }
        if (!isLocaleSet) {
            final Locale localeNavigateur = request.getLocale();
            final Locale effectiveLocale = LangueUtil.getEffectiveLocale(localeNavigateur);
            ctx.setLocale(effectiveLocale);
            ctx.setLangue(LangueUtil.getLangueLocale(effectiveLocale));
        }
    }

    /**
     * Initialise un contexteuniv lorsque l'on a pas de requête.
     *
     * @return
     */
    public static ContexteUniv setContexteSansRequete() {
        final ContexteUniv ctx = new ContexteUniv("");
        ajoutDansThreadLocal(ctx);
        return ctx;
    }

    public static void ajoutDansThreadLocal(final ContexteUniv ctx) {
        final ContexteUniv oldCtx = getContexteUniv();
        if (oldCtx != null) {
            oldCtx.release();
            LOGGER.warn("Initialisation d'un contexte univ alors qu'il en existe déjà un");
        }
        threadLocal.set(ctx);
    }

    /**
     * se charge de récupere le contexte provenant du THreadLocal,
     *
     * @return le contexte initialisé dans le filtre
     */
    public static ContexteUniv getContexteUniv() {
        return threadLocal.get();
    }

    /**
     * Release le contexte et le supprime de ThreadLocal, Cette méthode ne doit être appelé que dans le filtre afin de ne pas avoir plusieurs release du même contexte.
     */
    public static void releaseContexteUniv() {
        final ContexteUniv ctx = getContexteUniv();
        if (ctx != null) {
            ctx.release();
            threadLocal.remove();
        } else {
            LOGGER.warn("contexte déjà libéré");
        }
    }
}
