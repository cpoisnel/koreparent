package com.univ.utils;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.textsearch.ResultatRecherche;
import com.jsbsoft.jtf.textsearch.sitesdistants.CibleRecherche;
import com.jsbsoft.jtf.textsearch.sitesdistants.RechercheSitesDistants;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.PropertyHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.om.RechercheExterne;

/**
 * processus recherche front-office.
 *
 */
public class RechercheFiche extends ProcessusBean {

    public static final String REQUETE = "REQUETE";

    public static final String QUERY = "QUERY";

    public static final String SEARCH_RESULTS = "SEARCH_RESULTS";

    public static final String NB_TOTAL_PAGES = "NB_TOTAL_PAGES";

    public static final String INDICE_DERNIERE_PAGE = "INDICE_DERNIERE_PAGE";

    public static final String INDICE_PAGE_PRECEDENTE = "INDICE_PAGE_PRECEDENTE";

    public static final String FIN_FENETRE = "FIN_FENETRE";

    public static final String DEBUT_FENETRE = "DEBUT_FENETRE";

    public static final String NB_RESULTATS = "NB_RESULTATS";

    public static final String NB_RESULTATS_PAR_PAGE = "NB_RESULTATS_PAR_PAGE";

    /** The Constant ECRAN_RECHERCHE. */
    protected static final String ECRAN_RECHERCHE = "RECHERCHE";

    /** The Constant ECRAN_RECHERCHE_AVANCEE. */
    protected static final String ECRAN_RECHERCHE_AVANCEE = "RECHERCHE_AVANCEE";

    /** The Constant ECRAN_LISTE. */
    protected static final String ECRAN_LISTE = "LISTE";

    private static final String FROM_PARAMETER = "FROM";

    /**
     * processus recherche WEB d'une fiche.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public RechercheFiche(final InfoBean ciu) {
        super(ciu);
    }

    /**
     * Affichage de la liste des fiches sélectionnées.
     */
    protected void preparerLISTE() {
        ecranLogique = ECRAN_LISTE;
    }

    /**
     * Affichage de l'écran des critères de recherche.
     *
     * @throws Exception
     *             the exception
     */
    protected void preparerRECHERCHE() throws Exception {
        infoBean.set("SAISIE_FRONT", "TRUE");
        for (final RechercheExterne rechercheExterne : ClassBeanManager.getInstance().getBeanOfType(RechercheExterne.class)) {
            if (rechercheExterne.preparerRECHERCHE(infoBean, this)) {
                break;
            }
        }
        ecranLogique = ECRAN_RECHERCHE;
    }

    /**
     * Affichage de l'écran des critères de recherche.
     *
     * @throws Exception
     *             the exception
     */
    protected void preparerRECHERCHE_AVANCEE() throws Exception {
        ecranLogique = ECRAN_RECHERCHE_AVANCEE;
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        ecranLogique = infoBean.getEcranLogique();
        fixObjetParameter();
        action = infoBean.getActionUtilisateur();
        etat = EN_COURS;
        /* Entrée par lien hyper-texte */
        if (ecranLogique == null) {
            if ("RECHERCHER".equals(action)) {
                preparerRECHERCHE();
            }
            if ("RECHERCHE_AVANCEE".equals(action)) {
                preparerRECHERCHE_AVANCEE();
            }
        } else
        /* Entrée par formulaire */ {
            if (ecranLogique.equals(ECRAN_RECHERCHE)) {
                traiterRECHERCHE();
            }
        }
        //placer l'état dans le composant d'infoBean
        infoBean.setEcranLogique(ecranLogique);
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    private void fixObjetParameter() {
        if (infoBean.get("OBJET") == null && infoBean.get("objet") != null) {
            infoBean.set("OBJET", infoBean.get("objet"));
        }
    }

    /**
     * Affichage de l'écran des critères de recherche.
     *
     * @throws Exception
     *             the exception
     */
    @SuppressWarnings("deprecation")
    protected void traiterRECHERCHE() throws Exception {
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            String requestParams = StringUtils.EMPTY;
            if (RechercheFicheHelper.isFullTextSearch(infoBean)) {
                requestParams = buildRequest();
                infoBean.set(REQUETE, requestParams);
            } else {
                for (final RechercheExterne rechercheExterne : ClassBeanManager.getInstance().getBeanOfType(RechercheExterne.class)) {
                    if (rechercheExterne.traiterRECHERCHE(infoBean, this)) {
                        requestParams = infoBean.getString(RechercheFicheHelper.ATTRIBUT_INFOBEAN_REQUETE);
                        break;
                    }
                }
            }
            final List<ResultatRecherche> results = search(requestParams);
            processPagination(results);
            preparerLISTE();
        }
    }

    private void processPagination(final List<ResultatRecherche> results) {
        int nbResultats = 0;
        int nbResultatsParPage = 10;
        final String nbResultPerPage = PropertyHelper.getCoreProperty("recherche.nbrespage");
        if (StringUtils.isNotBlank(nbResultPerPage) && StringUtils.isNumeric(nbResultPerPage)) {
            nbResultatsParPage = Integer.valueOf(nbResultPerPage);
        }
        /* Variables permettant de gérer l'affichage de la fenêtre de résultats : on affiche les résultats qui vont de debutFenetre à finFenetre,
        en respectant finFenetre - debutFenetre = nbResultatsParPage */
        int debutFenetre = 0;
        int finFenetre = 0;
        int indicePagePrecedente = 0;
        int indiceDernierePage = 0;
        final String fromParameter = infoBean.getString(FROM_PARAMETER);
        if (StringUtils.isNotBlank(fromParameter) && StringUtils.isNumeric(fromParameter)) {
            debutFenetre = Integer.valueOf(fromParameter);
        }
        if (results.size() > 0) {
            nbResultats = results.get(0).getTotal();
        }
        /* On détermine le nombre total de pages à afficher */
        int nbTotalPages = nbResultats / nbResultatsParPage;
        if (nbResultats % nbResultatsParPage > 0) {
            nbTotalPages++;
        }
        finFenetre = debutFenetre + nbResultatsParPage;
        if (nbResultats < finFenetre) {
            finFenetre = nbResultats;
        }
        if (nbResultats > finFenetre) {
            indiceDernierePage = (nbTotalPages * nbResultatsParPage) - nbResultatsParPage;
        }
        if (debutFenetre > 0) {
            indicePagePrecedente = debutFenetre - nbResultatsParPage;
        }
        infoBean.set(DEBUT_FENETRE, debutFenetre);
        infoBean.set(FIN_FENETRE, finFenetre);
        infoBean.set(INDICE_PAGE_PRECEDENTE, indicePagePrecedente);
        infoBean.set(INDICE_DERNIERE_PAGE, indiceDernierePage);
        infoBean.set(NB_TOTAL_PAGES, nbTotalPages);
        infoBean.set(NB_RESULTATS, nbResultats);
        infoBean.set(NB_RESULTATS_PAR_PAGE, nbResultatsParPage);
    }

    private List<ResultatRecherche> search(final String requete) throws Exception {
        /* Variables pour gérer les résultats */
        List<ResultatRecherche> listeFiches = Collections.emptyList();
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        ctx.putData("requete", requete);
        ctx.putData("criteresRechAvancee", "");
        ctx.putData("modifRecherche", "&MODIFRECHERCHE=TRUE");
        ctx.setCalculListeResultatsFront(Boolean.TRUE);
        if (RechercheFicheHelper.isFullTextSearch(infoBean)) {
            listeFiches = RechercheUtil.traiterRecherche(ctx, requete);
        } else {
            listeFiches = RequeteUtil.traiterRequete(ctx, requete);
        }
        ctx.setCalculListeResultatsFront(Boolean.FALSE);
        ctx.setRequete(requete.toUpperCase());
        infoBean.set(SEARCH_RESULTS, listeFiches);
        return listeFiches;
    }

    @SuppressWarnings("deprecation")
    private String buildRequest() throws ErreurApplicative {
        // Si un site est spécifié, on le force dans le code rubrique
        if (StringUtils.isNotEmpty(infoBean.getString("SITE_RECHERCHE"))) {
            if ("TOUS".equalsIgnoreCase(infoBean.getString("SITE_RECHERCHE"))) {
                infoBean.set("CODE_SITE_DISTANT", infoBean.getString("SITE_RECHERCHE"));
                infoBean.set("CODE_RUBRIQUE", "");
            } else {
                final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
                final InfosSite infosSite = serviceInfosSite.getInfosSite(infoBean.getString("SITE_RECHERCHE"));
                final CibleRecherche cible = RechercheSitesDistants.getCible(infoBean.getString("SITE_RECHERCHE"));
                // soit il s'agit d'un site distant
                if (cible != null) {
                    infoBean.set("CODE_SITE_DISTANT", cible.getCode());
                } else if (infosSite != null) {
                    infoBean.set("CODE_RUBRIQUE", infosSite.getCodeRubrique());
                }
            }
        }
        if ("0".equals(infoBean.getString("SITE_CLOISONNE"))) {
            infoBean.set("CODE_RUBRIQUE", StringUtils.EMPTY);
        } else if (StringUtils.isEmpty(infoBean.getString("CODE_RUBRIQUE"))) {
            // si aucune rubrique n'est spécifié, on force au site courant
            final InfosSite infosSite = getInfosSite();
            if (infosSite != null && (infosSite.getRestriction() == 1 || "1".equals(infoBean.getString("SITE_CLOISONNE")))) {
                infoBean.set("CODE_RUBRIQUE", infosSite.getCodeRubrique());
            }
        }
        try {
            if (infoBean.get("DATE_DEBUT") != null && infoBean.get("DATE_DEBUT") instanceof String) {
                Formateur.parserDate(infoBean.getString("DATE_DEBUT"));
            }
            if (infoBean.get("DATE_FIN") != null && infoBean.get("DATE_FIN") instanceof String) {
                Formateur.parserDate(infoBean.getString("DATE_FIN"));
            }
        } catch (final ParseException e) {
            throw new ErreurApplicative("Format de date incorrect", e);
        }
        String requete = "OBJET=" + infoBean.getString("OBJET");
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "QUERY", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "CODE_SITE_DISTANT", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "SITES_DISTANTS", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "SITES_DISTANTS_EXCLUS", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "CODE_RUBRIQUE", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "ESPACE", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "DATE_DEBUT", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "DATE_FIN", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "DOCUMENT_JOINT", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "SEARCH_SOUSRUBRIQUES", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "SEARCH_EXCLUSIONOBJET", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "LANGUE_SEARCH", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, FROM_PARAMETER, false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "LANGUE", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "UNOBJET", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "PAGE", false, true);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "TRI", false, true);
        return requete;
    }
}
