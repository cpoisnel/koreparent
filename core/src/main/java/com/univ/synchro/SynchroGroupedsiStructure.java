package com.univ.synchro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;

/**
 * Création automatique de groupes DSI et synchronisation des rattachements en fonction de la structure.
 */
public class SynchroGroupedsiStructure {

    /** The Constant PARAM_JTF_SYNCHRO_GROUPESDSI. */
    private final static String PARAM_JTF_SYNCHRO_GROUPESDSI = "static.synchro_struct_groupesdsi";

    /** The Constant PARAM_JTF_DSI. */
    private final static String PARAM_JTF_DSI = "dsi.activation";

    private static final Logger LOG = LoggerFactory.getLogger(SynchroGroupedsiStructure.class);

    /**
     * Synchroniser groupe dsi.
     *
     * @param ctx
     *            the ctx
     * @param structure
     *            the structure
     * @deprecated le contexte n'est plus utilisé
     */
    @Deprecated
    public static void synchroniserGroupeDsi(final OMContext ctx, final StructureModele structure) {
        synchroniserGroupeDsi(structure, false);
    }

    /**
     * Synchroniser groupe dsi.
     *
     * @param ctx
     *            the ctx
     * @param structure
     *            the structure
     * @param isImport
     *            the is import
     * @deprecated le contexte n'est plus utilisé
     */
    @Deprecated
    public static void synchroniserGroupeDsi(final OMContext ctx, final StructureModele structure, final boolean isImport) {
        synchroniserGroupeDsi(structure,isImport);
    }
    /**
     * Synchroniser groupe dsi.
     *
     * @param structure
     *            the structure
     */
    public static void synchroniserGroupeDsi(final StructureModele structure) {
        synchroniserGroupeDsi(structure, false);
    }

    /**
     * Synchroniser groupe dsi.
     *
     * @param structure
     *            the structure
     * @param isImport
     *            the is import
     */
    public static void synchroniserGroupeDsi(final StructureModele structure, final boolean isImport) {
        // JB 20050822 : la création des groupes DSI devient optionnelle, mais par défaut elle est activée (cf jtf)
        boolean bSynchroCreationGroupesDSI = true;
        final String paramSynchro = PropertyHelper.getCoreProperty(PARAM_JTF_SYNCHRO_GROUPESDSI);
        if (paramSynchro != null) {
            bSynchroCreationGroupesDSI = paramSynchro.startsWith("1");
        }
        //AM 20051027 : si la dsi est désactivée, on ne génère pas non plus ces groupes
        final String paramDsi = PropertyHelper.getCoreProperty(PARAM_JTF_DSI);
        if (paramDsi == null || !"1".equals(paramDsi)) {
            bSynchroCreationGroupesDSI = false;
        }
        if (bSynchroCreationGroupesDSI) {
            final String typeGroupeStructure = "STRUCT";
            final String codeGroupeStructure = typeGroupeStructure + "_" + structure.getCode();
            // JSS 20040419 : On synchronise le rattachement du groupe
            String codeRattachement = structure.getCodeRattachement();
            if ("00".equals(codeRattachement)) {
                codeRattachement = "";
            }
            // Ajout STRUCT_ pour le code du groupe
            if (codeRattachement.length() > 0) {
                codeRattachement = typeGroupeStructure + "_" + codeRattachement;
            }
            final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
            GroupeDsiBean groupeDsiBean = serviceGroupeDsi.getByCode(codeGroupeStructure);
            if (groupeDsiBean == null) {
                groupeDsiBean = new GroupeDsiBean();
                groupeDsiBean.setCode(codeGroupeStructure);
                groupeDsiBean.setCodeGroupePere(codeRattachement);
                String libelle = (structure).getLibelleCourt();
                if (libelle.length() == 0) {
                    libelle = (structure).getLibelleLong();
                }
                groupeDsiBean.setLibelle(libelle);
                groupeDsiBean.setType(typeGroupeStructure);
                groupeDsiBean.setCodeStructure(structure.getCode());
                serviceGroupeDsi.save(groupeDsiBean);
            } else {
                groupeDsiBean.setCodeGroupePere(codeRattachement);
                serviceGroupeDsi.save(groupeDsiBean);
            }
        }
    }
}
