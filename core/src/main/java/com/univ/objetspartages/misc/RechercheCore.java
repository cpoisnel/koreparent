package com.univ.objetspartages.misc;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.cms.objetspartages.Objetpartage;
import com.univ.objetspartages.om.AbstractRechercheExterne;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.utils.RechercheFicheHelper;

import static com.univ.objetspartages.util.CritereRechercheUtil.getCritereTexteNonVideFormater;

public class RechercheCore extends AbstractRechercheExterne {

    @Override
    public boolean preparerRECHERCHE(final InfoBean infoBean, final OMContext ctx) throws Exception {
        boolean rechercheEffectuee = false;
        final String objet = StringUtils.defaultString(infoBean.getString("OBJET"), "").toUpperCase();
        if ("PAGELIBRE".equals(objet)) {
            infoBean.set("LISTE_LANGUES", LangueUtil.getListeLangues(ctx.getLocale()));
            rechercheEffectuee = true;
        }
        return rechercheEffectuee;
    }

    @Override
    public boolean traiterRECHERCHE(final InfoBean infoBean, final OMContext ctx) throws Exception {
        boolean rechercheEffectuee = false;
        final String objet = infoBean.getString("OBJET").toUpperCase();
        if ("PAGELIBRE".equals(objet)) {
            final List<CritereRecherche> criteres = new ArrayList<>();
            final Objetpartage module = ReferentielObjets.getObjetByNom(objet);
            criteres.add(new CritereRecherche("OBJET", infoBean.getString("OBJET"), StringUtils.EMPTY));
            CollectionUtils.addIgnoreNull(criteres, getCritereTexteNonVideFormater(infoBean, "TITRE"));
            CollectionUtils.addIgnoreNull(criteres, getCritereTexteNonVideFormater(infoBean, "MOTS_CLES"));
            if ("0".equals(module.getProperty("fiche.PAGELIBRE.recherche_vide"))) {
                if (criteres.size() <= 1) {
                    throw new ErreurApplicative(module.getMessage(ctx.getLocale(), "ST_RECHERCHE_SAISIR_UN_CRITERE"));
                }
            }
            rechercheEffectuee = true;
            // attribut commun à toute les fiches
            criteres.addAll(getCritereDefaut(infoBean));
            infoBean.set(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES, criteres);
            gestionAncienFront(infoBean, criteres);
        }
        return rechercheEffectuee;
    }

    @Override
    public List<String> getCriteresRequete(final String objet, final boolean listeIncluse) {
        return new ArrayList<>();
    }
}
