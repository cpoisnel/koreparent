package com.univ.objetspartages.misc;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.cms.objetspartages.Objetpartage;
import com.univ.objetspartages.om.AbstractRechercheExterne;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.utils.RechercheFicheHelper;

import static com.univ.objetspartages.util.CritereRechercheUtil.getCritereDate;
import static com.univ.objetspartages.util.CritereRechercheUtil.getCritereTexteNonVideFormater;
import static com.univ.objetspartages.util.CritereRechercheUtil.getCritereThematique;

public class RechercheArticle extends AbstractRechercheExterne {

    /**
     * Prepare le formulaire de recherche.
     *
     * @param infoBean
     *            the info bean
     * @param ctx
     *            the ctx
     *
     * @return true, if preparer recherche
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean preparerRECHERCHE(final InfoBean infoBean, final OMContext ctx) throws Exception {
        boolean rechercheEffectuee = false;
        final String objet = StringUtils.defaultString(infoBean.getString("OBJET"), "").toUpperCase();
        final Locale locale = ctx.getLocale();
        if ("ARTICLE".equals(objet)) {
            infoBean.set("LISTE_THEMATIQUES", LabelUtils.getLabelCombo("04", locale));
            rechercheEffectuee = true;
        }
        return rechercheEffectuee;
    }

    /**
     * Traite la recherche.
     *
     * @param infoBean
     *            the info bean
     * @param ctx
     *            the ctx
     *
     * @return true, if traiter recherche
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterRECHERCHE(final InfoBean infoBean, final OMContext ctx) throws Exception {
        boolean rechercheEffectuee = false;
        final String objet = infoBean.getString("OBJET").toUpperCase();
        if ("ARTICLE".equals(objet)) {
            final List<CritereRecherche> criteres = new ArrayList<>();
            final Objetpartage module = ReferentielObjets.getObjetByNom(objet);
            criteres.add(new CritereRecherche("OBJET", objet, StringUtils.EMPTY));
            CollectionUtils.addIgnoreNull(criteres, getCritereTexteNonVideFormater(infoBean, "TITRE"));
            CollectionUtils.addIgnoreNull(criteres, getCritereThematique(infoBean));
            CollectionUtils.addIgnoreNull(criteres, getCritereDate(infoBean, "DATE_DEBUT"));
            CollectionUtils.addIgnoreNull(criteres, getCritereDate(infoBean, "DATE_FIN"));
            if ("0".equals(module.getProperty("fiche.ARTICLE.recherche_vide")) && criteres.size() <= 1) {
                throw new ErreurApplicative(module.getMessage(ctx.getLocale(), "ST_RECHERCHE_SAISIR_UN_CRITERE"));
            }
            rechercheEffectuee = true;
            // attribut commun à toute les fiches
            criteres.addAll(getCritereDefaut(infoBean));
            infoBean.set(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES, criteres);
            gestionAncienFront(infoBean, criteres);
        }
        return rechercheEffectuee;
    }

    /**
     * Renvoie la liste des criteres selectionnables pour les requetes insérées dans les toolbox
     *
     * @param objet
     *            courant
     * @param listeIncluse liste incluse
     */
    @Override
    public List<String> getCriteresRequete(final String objet, final boolean listeIncluse) {
        final List<String> criteres = new ArrayList<>();
        if ("ARTICLE".equals(objet)) {
            criteres.add("THEMATIQUE");
            criteres.add("DATE_DEBUT");
            criteres.add("DATE_FIN");
            if (listeIncluse) {
                criteres.add("SELECTION");
                criteres.add("JOUR");
                criteres.add("TRI_DATE");
            }
        }
        return criteres;
    }
}
