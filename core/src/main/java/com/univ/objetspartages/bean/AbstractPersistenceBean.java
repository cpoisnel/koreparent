package com.univ.objetspartages.bean;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonView;
import com.univ.utils.json.CustomJacksonId;
import com.univ.utils.json.Views;

public class AbstractPersistenceBean implements PersistenceBean, Serializable {

    private static final long serialVersionUID = 2876757237198604281L;

    @CustomJacksonId
    @JsonView(Views.BackOfficeView.class)
    protected Long id = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
