package com.univ.objetspartages.bean;

public class LabelBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = -6136789441442217337L;

    /** The type. */
    private String type;

    /** The code. */
    private String code;

    /** The libelle. */
    private String libelle;

    /** The langue. */
    private String langue;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }
}
