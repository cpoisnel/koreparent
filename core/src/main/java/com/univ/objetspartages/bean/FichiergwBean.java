package com.univ.objetspartages.bean;
// TODO: Auto-generated Javadoc

import java.util.Date;

/**
 * The Class FichiergwBean.
 */
public class FichiergwBean {

    /** The id fichiergw. */
    protected Long idFichiergw = null;

    /** The code. */
    protected String code = null;

    /** The code parent. */
    protected String codeParent = null;

    /** The type fichier. */
    protected String typeFichier = null;

    /** The id photo. */
    protected String idPhoto = null;

    /** The id vignette. */
    protected String idVignette = null;

    /** The path fichier joint. */
    protected String pathFichierJoint = null;

    /** The format fichier joint. */
    protected String formatFichierJoint = null;

    /** The poids fichier joint. */
    protected String poidsFichierJoint = null;

    /** The nom fichier joint. */
    protected String nomFichierJoint = null;

    /** The libelle fichier joint. */
    protected String libelleFichierJoint = null;

    /** The numero version. */
    protected String numeroVersion = null;

    /** The date version. */
    protected Date dateVersion = null;

    /** The commentaire version. */
    protected String commentaireVersion = null;

    /** The auteur version. */
    protected String auteurVersion = null;

    /** The etat. */
    protected String etat = null;

    /**
     * Instantiates a new fichiergw bean.
     */
    public FichiergwBean() {
        super();
    }

    /**
     * Inits the.
     *
     * @param bean
     *            the bean
     */
    public void init(FichiergwBean bean) {
        this.idFichiergw = bean.idFichiergw;
        this.code = bean.code;
        this.codeParent = bean.codeParent;
        this.typeFichier = bean.typeFichier;
        this.idPhoto = bean.idPhoto;
        this.idVignette = bean.idVignette;
        this.pathFichierJoint = bean.pathFichierJoint;
        this.formatFichierJoint = bean.formatFichierJoint;
        this.poidsFichierJoint = bean.poidsFichierJoint;
        this.nomFichierJoint = bean.nomFichierJoint;
        this.libelleFichierJoint = bean.libelleFichierJoint;
        this.numeroVersion = bean.numeroVersion;
        this.dateVersion = bean.dateVersion;
        this.commentaireVersion = bean.commentaireVersion;
        this.auteurVersion = bean.auteurVersion;
        this.etat = bean.etat;
    }

    /**
     * Gets the auteur version.
     *
     * @return the auteurVersion
     */
    public String getAuteurVersion() {
        return auteurVersion;
    }

    /**
     * Sets the auteur version.
     *
     * @param auteurVersion
     *            the auteurVersion to set
     */
    public void setAuteurVersion(String auteurVersion) {
        this.auteurVersion = auteurVersion;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the code parent.
     *
     * @return the codeParent
     */
    public String getCodeParent() {
        return codeParent;
    }

    /**
     * Sets the code parent.
     *
     * @param codeParent
     *            the codeParent to set
     */
    public void setCodeParent(String codeParent) {
        this.codeParent = codeParent;
    }

    /**
     * Gets the commentaire version.
     *
     * @return the commentaireVersion
     */
    public String getCommentaireVersion() {
        return commentaireVersion;
    }

    /**
     * Sets the commentaire version.
     *
     * @param commentaireVersion
     *            the commentaireVersion to set
     */
    public void setCommentaireVersion(String commentaireVersion) {
        this.commentaireVersion = commentaireVersion;
    }

    /**
     * Gets the date version.
     *
     * @return the dateVersion
     */
    public Date getDateVersion() {
        return dateVersion;
    }

    /**
     * Sets the date version.
     *
     * @param dateVersion
     *            the dateVersion to set
     */
    public void setDateVersion(Date dateVersion) {
        this.dateVersion = dateVersion;
    }

    /**
     * Gets the etat.
     *
     * @return the etat
     */
    public String getEtat() {
        return etat;
    }

    /**
     * Sets the etat.
     *
     * @param etat
     *            the etat to set
     */
    public void setEtat(String etat) {
        this.etat = etat;
    }

    /**
     * Gets the format fichier joint.
     *
     * @return the formatFichierJoint
     */
    public String getFormatFichierJoint() {
        return formatFichierJoint;
    }

    /**
     * Sets the format fichier joint.
     *
     * @param formatFichierJoint
     *            the formatFichierJoint to set
     */
    public void setFormatFichierJoint(String formatFichierJoint) {
        this.formatFichierJoint = formatFichierJoint;
    }

    /**
     * Gets the id fichiergw.
     *
     * @return the idFichiergw
     */
    public Long getIdFichiergw() {
        return idFichiergw;
    }

    /**
     * Sets the id fichiergw.
     *
     * @param idFichiergw
     *            the idFichiergw to set
     */
    public void setIdFichiergw(Long idFichiergw) {
        this.idFichiergw = idFichiergw;
    }

    /**
     * Gets the id photo.
     *
     * @return the idPhoto
     */
    public String getIdPhoto() {
        return idPhoto;
    }

    /**
     * Sets the id photo.
     *
     * @param idPhoto
     *            the idPhoto to set
     */
    public void setIdPhoto(String idPhoto) {
        this.idPhoto = idPhoto;
    }

    /**
     * Gets the id vignette.
     *
     * @return the idVignette
     */
    public String getIdVignette() {
        return idVignette;
    }

    /**
     * Sets the id vignette.
     *
     * @param idVignette
     *            the idVignette to set
     */
    public void setIdVignette(String idVignette) {
        this.idVignette = idVignette;
    }

    /**
     * Gets the libelle fichier joint.
     *
     * @return the libelleFichierJoint
     */
    public String getLibelleFichierJoint() {
        return libelleFichierJoint;
    }

    /**
     * Sets the libelle fichier joint.
     *
     * @param libelleFichierJoint
     *            the libelleFichierJoint to set
     */
    public void setLibelleFichierJoint(String libelleFichierJoint) {
        this.libelleFichierJoint = libelleFichierJoint;
    }

    /**
     * Gets the nom fichier joint.
     *
     * @return the nomFichierJoint
     */
    public String getNomFichierJoint() {
        return nomFichierJoint;
    }

    /**
     * Sets the nom fichier joint.
     *
     * @param nomFichierJoint
     *            the nomFichierJoint to set
     */
    public void setNomFichierJoint(String nomFichierJoint) {
        this.nomFichierJoint = nomFichierJoint;
    }

    /**
     * Gets the numero version.
     *
     * @return the numeroVersion
     */
    public String getNumeroVersion() {
        return numeroVersion;
    }

    /**
     * Sets the numero version.
     *
     * @param numeroVersion
     *            the numeroVersion to set
     */
    public void setNumeroVersion(String numeroVersion) {
        this.numeroVersion = numeroVersion;
    }

    /**
     * Gets the path fichier joint.
     *
     * @return the pathFichierJoint
     */
    public String getPathFichierJoint() {
        return pathFichierJoint;
    }

    /**
     * Sets the path fichier joint.
     *
     * @param pathFichierJoint
     *            the pathFichierJoint to set
     */
    public void setPathFichierJoint(String pathFichierJoint) {
        this.pathFichierJoint = pathFichierJoint;
    }

    /**
     * Gets the poids fichier joint.
     *
     * @return the poidsFichierJoint
     */
    public String getPoidsFichierJoint() {
        return poidsFichierJoint;
    }

    /**
     * Sets the poids fichier joint.
     *
     * @param poidsFichierJoint
     *            the poidsFichierJoint to set
     */
    public void setPoidsFichierJoint(String poidsFichierJoint) {
        this.poidsFichierJoint = poidsFichierJoint;
    }

    /**
     * Gets the type fichier.
     *
     * @return the typeFichier
     */
    public String getTypeFichier() {
        return typeFichier;
    }

    /**
     * Sets the type fichier.
     *
     * @param typeFichier
     *            the typeFichier to set
     */
    public void setTypeFichier(String typeFichier) {
        this.typeFichier = typeFichier;
    }
}
