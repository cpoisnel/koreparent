package com.univ.objetspartages.bean;

import java.util.Date;

import com.univ.objetspartages.om.EtatFiche;

public class HistoriqueBean implements Comparable<HistoriqueBean> {

    private Long idFiche;

    private Date dateAction;

    private String action;

    private String utilisateur;

    private EtatFiche etat;

    public Long getIdFiche() {
        return idFiche;
    }

    public void setIdFiche(Long idFiche) {
        this.idFiche = idFiche;
    }

    public Date getDateAction() {
        return dateAction;
    }

    public void setDateAction(Date dateAction) {
        this.dateAction = dateAction;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }

    public EtatFiche getEtat() {
        return etat;
    }

    public void setEtat(EtatFiche etat) {
        this.etat = etat;
    }

    @Override
    public int compareTo(HistoriqueBean historiqueBean) {
        int result = 0;
        if (historiqueBean != null && historiqueBean.getDateAction() != null && this.getDateAction() != null) {
            result = getDateAction().compareTo(historiqueBean.getDateAction());
        } else {
            result = (historiqueBean == null || historiqueBean.getDateAction() == null) ? -1 : 1;
        }
        return result;
    }
}
