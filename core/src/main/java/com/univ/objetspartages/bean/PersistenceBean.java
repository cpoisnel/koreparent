package com.univ.objetspartages.bean;

import java.io.Serializable;

/**
 * Created on 22/09/15.
 */
public interface PersistenceBean extends Serializable {
    Long getId();

    void setId(Long id);
}
