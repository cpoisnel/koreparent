package com.univ.objetspartages.bean;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonView;
import com.kportal.cms.objetspartages.annotation.Fiche;
import com.kportal.cms.objetspartages.annotation.GetterAnnotation;
import com.kportal.cms.objetspartages.annotation.Rubrique;
import com.kportal.cms.objetspartages.annotation.SetterAnnotation;
import com.kportal.cms.objetspartages.annotation.User;
import com.univ.utils.json.Views;

public class AbstractFicheBean extends AbstractPersistenceBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2078943141200588647L;

    /** The code. */
    @JsonView({Views.BackOfficeView.class, Views.FrontOfficeView.class, Views.PublicView.class})
    protected String code = StringUtils.EMPTY;

    /** The titre. */
    @JsonView({Views.BackOfficeView.class, Views.FrontOfficeView.class, Views.PublicView.class})
    protected String titre = StringUtils.EMPTY;

    /** The code rubrique. */
    @JsonView(Views.BackOfficeView.class)
    @Rubrique
    protected String codeRubrique = StringUtils.EMPTY;

	/** The code rattachement. */
	@JsonView(Views.BackOfficeView.class)
	@Fiche(nomObjet = "defaultstructureksup")
	protected String codeRattachement = StringUtils.EMPTY;

    /** The meta keywords. */
    @JsonView(Views.BackOfficeView.class)
    protected String metaKeywords = StringUtils.EMPTY;

    /** The meta description. */
    @JsonView(Views.BackOfficeView.class)
    protected String metaDescription = StringUtils.EMPTY;

    /** The titre encadre. */
    @JsonView(Views.BackOfficeView.class)
    protected String titreEncadre = StringUtils.EMPTY;

    /** The contenu encadre. */
    @JsonView(Views.BackOfficeView.class)
    protected String contenuEncadre = StringUtils.EMPTY;

    /** The encadre recherche. */
    @JsonView(Views.BackOfficeView.class)
    protected String encadreRecherche = StringUtils.EMPTY;

    /** The encadre recherche bis. */
    @JsonView(Views.BackOfficeView.class)
    protected String encadreRechercheBis = StringUtils.EMPTY;

    /** The date alerte. */
    @JsonView(Views.BackOfficeView.class)
    protected Date dateAlerte = null;

    /** The message alerte. */
    @JsonView(Views.BackOfficeView.class)
    protected String messageAlerte = StringUtils.EMPTY;

    /** The date creation. */
    @JsonView(Views.BackOfficeView.class)
    protected Date dateCreation = null;

    /** The date proposition. */
    @JsonView(Views.BackOfficeView.class)
    protected Date dateProposition = null;

    /** The date validation. */
    @JsonView(Views.BackOfficeView.class)
    protected Date dateValidation = null;

    /** The date modification. */
    @JsonView(Views.BackOfficeView.class)
    protected Date dateModification = null;

    /** The code redacteur. */
    @JsonView(Views.BackOfficeView.class)
    @User
    protected String codeRedacteur = StringUtils.EMPTY;

    /** The code validation. */
    @JsonView(Views.BackOfficeView.class)
    @User
    protected String codeValidation = StringUtils.EMPTY;

    /** The langue. */
    @JsonView(Views.BackOfficeView.class)
    protected String langue = StringUtils.EMPTY;

    /** The etat objet. */
    @JsonView(Views.BackOfficeView.class)
    protected String etatObjet = StringUtils.EMPTY;

    /** The nb hits. */
    @JsonView(Views.BackOfficeView.class)
    protected Long nbHits = null;

    public void init(final AbstractFicheBean bean) {
        this.id = bean.id;
        this.code = bean.code;
        this.titre = bean.titre;
        this.codeRubrique = bean.codeRubrique;
        this.codeRattachement = bean.codeRattachement;
        this.metaKeywords = bean.metaKeywords;
        this.metaDescription = bean.metaDescription;
        this.titreEncadre = bean.titreEncadre;
        this.contenuEncadre = bean.contenuEncadre;
        this.encadreRecherche = bean.encadreRecherche;
        this.encadreRechercheBis = bean.encadreRechercheBis;
        this.dateAlerte = bean.dateAlerte;
        this.messageAlerte = bean.messageAlerte;
        this.dateCreation = bean.dateCreation;
        this.dateProposition = bean.dateProposition;
        this.dateValidation = bean.dateValidation;
        this.dateModification = bean.dateModification;
        this.codeRedacteur = bean.codeRedacteur;
        this.codeValidation = bean.codeValidation;
        this.langue = bean.langue;
        this.etatObjet = bean.etatObjet;
        this.nbHits = bean.nbHits;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(final String titre) {
        this.titre = titre;
    }

    public String getCodeRubrique() {
        return codeRubrique;
    }

    public void setCodeRubrique(final String codeRubrique) {
        this.codeRubrique = codeRubrique;
    }

    public String getCodeRattachement() {
        return codeRattachement;
    }

    public void setCodeRattachement(final String codeRattachement) {
        this.codeRattachement = codeRattachement;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(final String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(final String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getTitreEncadre() {
        return titreEncadre;
    }

    public void setTitreEncadre(final String titreEncadre) {
        this.titreEncadre = titreEncadre;
    }

    @GetterAnnotation(isToolbox = true)
    public String getContenuEncadre() {
        return contenuEncadre;
    }

    @SetterAnnotation(isToolbox = true)
    public void setContenuEncadre(final String contenuEncadre) {
        this.contenuEncadre = contenuEncadre;
    }

    public String getEncadreRecherche() {
        return encadreRecherche;
    }

    public void setEncadreRecherche(final String encadreRecherche) {
        this.encadreRecherche = encadreRecherche;
    }

    public String getEncadreRechercheBis() {
        return encadreRechercheBis;
    }

    public void setEncadreRechercheBis(final String encadreRechercheBis) {
        this.encadreRechercheBis = encadreRechercheBis;
    }

    public Date getDateAlerte() {
        return dateAlerte;
    }

    public void setDateAlerte(final Date dateAlerte) {
        this.dateAlerte = dateAlerte;
    }

    public String getMessageAlerte() {
        return messageAlerte;
    }

    public void setMessageAlerte(final String messageAlerte) {
        this.messageAlerte = messageAlerte;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(final Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateProposition() {
        return dateProposition;
    }

    public void setDateProposition(final Date dateProposition) {
        this.dateProposition = dateProposition;
    }

    public Date getDateValidation() {
        return dateValidation;
    }

    public void setDateValidation(final Date dateValidation) {
        this.dateValidation = dateValidation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(final Date dateModification) {
        this.dateModification = dateModification;
    }

    public String getCodeRedacteur() {
        return codeRedacteur;
    }

    public void setCodeRedacteur(final String codeRedacteur) {
        this.codeRedacteur = codeRedacteur;
    }

    public String getCodeValidation() {
        return codeValidation;
    }

    public void setCodeValidation(final String codeValidation) {
        this.codeValidation = codeValidation;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(final String langue) {
        this.langue = langue;
    }

    public String getEtatObjet() {
        return etatObjet;
    }

    public void setEtatObjet(final String etatObjet) {
        this.etatObjet = etatObjet;
    }

    public Long getNbHits() {
        return nbHits;
    }

    public void setNbHits(final Long nbHits) {
        this.nbHits = nbHits;
    }

}
