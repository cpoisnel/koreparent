package com.univ.objetspartages.bean;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.kportal.cms.objetspartages.annotation.Fiche;
import com.kportal.cms.objetspartages.annotation.GetterAnnotation;
import com.kportal.cms.objetspartages.annotation.Rubrique;
import com.kportal.cms.objetspartages.annotation.SetterAnnotation;
import com.kportal.cms.objetspartages.annotation.User;
// TODO: Auto-generated Javadoc

/**
 * Classe d'acces aux donnees pour pagelibre.
 */
public class PagelibreBean extends AbstractRestrictionFicheBean implements Serializable {

    private static final long serialVersionUID = -2834597145328529412L;

    /** The contenu. */
    protected String contenu = StringUtils.EMPTY;

    /** The complements. */
    protected String complements = StringUtils.EMPTY;

    /** The rattachement bandeau. */
    private String rattachementBandeau = StringUtils.EMPTY;

    /**
     * Instantiates a new pagelibre bean.
     */
    public PagelibreBean() {
        super();
    }

    /**
     * Inits the.
     *
     * @param bean
     *            the bean
     */
    public void init(PagelibreBean bean) {
        this.id = bean.id;
        this.titre = bean.titre;
        this.rattachementBandeau = bean.rattachementBandeau;
        this.contenu = bean.contenu;
        this.complements = bean.complements;
        this.code = bean.code;
        this.codeRubrique = bean.codeRubrique;
        this.codeRattachement = bean.codeRattachement;
        this.metaKeywords = bean.metaKeywords;
        this.metaDescription = bean.metaDescription;
        this.titreEncadre = bean.titreEncadre;
        this.contenuEncadre = bean.contenuEncadre;
        this.encadreRecherche = bean.encadreRecherche;
        this.encadreRechercheBis = bean.encadreRechercheBis;
        this.dateAlerte = bean.dateAlerte;
        this.messageAlerte = bean.messageAlerte;
        this.dateCreation = bean.dateCreation;
        this.dateProposition = bean.dateProposition;
        this.dateValidation = bean.dateValidation;
        this.dateModification = bean.dateModification;
        this.codeRedacteur = bean.codeRedacteur;
        this.codeValidation = bean.codeValidation;
        this.langue = bean.langue;
        this.etatObjet = bean.etatObjet;
        this.nbHits = bean.nbHits;
        this.diffusionPublicVise = bean.diffusionPublicVise;
        this.diffusionModeRestriction = bean.diffusionModeRestriction;
        this.diffusionPublicViseRestriction = bean.diffusionPublicViseRestriction;
    }

    /**
     * Gets the complements.
     *
     * @return the complements
     */
    @GetterAnnotation(isToolbox = true)
    public String getComplements() {
        return complements;
    }

    /**
     * Sets the complements.
     *
     * @param complements
     *            the complements to set
     */
    @SetterAnnotation(isToolbox = true)
    public void setComplements(String complements) {
        this.complements = complements;
    }

    /**
     * Gets the contenu.
     *
     * @return the contenu
     */
    @GetterAnnotation(isToolbox = true)
    public String getContenu() {
        return contenu;
    }

    /**
     * Sets the contenu.
     *
     * @param contenu
     *            the contenu to set
     */
    @SetterAnnotation(isToolbox = true)
    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    /**
     * Gets the id pagelibre.
     *
     * @return the idPagelibre
     * @deprecated Utilisez {@link PagelibreBean#getId()}
     */
    @Deprecated
    public Long getIdPagelibre() {
        return id;
    }

    /**
     * Sets the id pagelibre.
     *
     * @param idPagelibre
     *            the idPagelibre to set
     * @deprecated {@link PagelibreBean#setId(Long)}
     */
    @Deprecated
    public void setIdPagelibre(Long idPagelibre) {
        this.id = idPagelibre;
    }

    /**
     * Gets the rattachement bandeau.
     *
     * @return the rattachementBandeau
     */
    public String getRattachementBandeau() {
        return rattachementBandeau;
    }

    /**
     * Sets the rattachement bandeau.
     *
     * @param rattachementBandeau
     *            the rattachementBandeau to set
     */
    public void setRattachementBandeau(String rattachementBandeau) {
        this.rattachementBandeau = rattachementBandeau;
    }
}
