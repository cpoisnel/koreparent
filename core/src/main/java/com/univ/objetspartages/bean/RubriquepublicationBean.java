package com.univ.objetspartages.bean;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Classe contenant les données de la table de liaison entre les rubriques et les fiches (multi rubriquage)
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class RubriquepublicationBean extends AbstractPersistenceBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -959463976509889583L;

    /** The type fiche orig. */
    private String typeFicheOrig = StringUtils.EMPTY;

    /** The code fiche orig. */
    private String codeFicheOrig = StringUtils.EMPTY;

    /** The langue fiche orig. */
    private String langueFicheOrig = StringUtils.EMPTY;

    /** The rubrique dest. */
    private String rubriqueDest = StringUtils.EMPTY;

    /** The source requete. */
    private String sourceRequete = StringUtils.EMPTY;

    public Long getIdRubriquepublication() {
        return getId();
    }

    public void setIdRubriquepublication(Long idRubriquepublication) {
        setId(idRubriquepublication);
    }

    public String getTypeFicheOrig() {
        return typeFicheOrig;
    }

    public void setTypeFicheOrig(String typeFicheOrig) {
        this.typeFicheOrig = typeFicheOrig;
    }

    public String getCodeFicheOrig() {
        return codeFicheOrig;
    }

    public void setCodeFicheOrig(String codeFicheOrig) {
        this.codeFicheOrig = codeFicheOrig;
    }

    public String getLangueFicheOrig() {
        return langueFicheOrig;
    }

    public void setLangueFicheOrig(String langueFicheOrig) {
        this.langueFicheOrig = langueFicheOrig;
    }

    public String getRubriqueDest() {
        return rubriqueDest;
    }

    public void setRubriqueDest(String rubriqueDest) {
        this.rubriqueDest = rubriqueDest;
    }

    public String getSourceRequete() {
        return sourceRequete;
    }

    public void setSourceRequete(String sourceRequete) {
        this.sourceRequete = sourceRequete;
    }
}
