package com.univ.objetspartages.bean;

import java.io.Serializable;

import com.kportal.cms.objetspartages.annotation.GetterAnnotation;
import com.kportal.cms.objetspartages.annotation.SetterAnnotation;

public class EncadreBean extends AbstractPersistenceBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6769252814254802051L;

    /** The intitule. */
    protected String intitule = null;

    /** The actif. */
    protected String actif = null;

    /** The langue. */
    protected String langue = null;

    /** The poids. */
    protected Integer poids = null;

    /** The contenu. */
    protected String contenu = null;

    /** The objets. */
    protected String objets = null;

    /** The code. */
    protected String code = null;

    /** The code rattachement. */
    protected String codeRattachement = null;

    /** The code rubrique. */
    protected String codeRubrique = null;

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getActif() {
        return actif;
    }

    public void setActif(String actif) {
        this.actif = actif;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public Integer getPoids() {
        return poids;
    }

    public void setPoids(Integer poids) {
        this.poids = poids;
    }

    @GetterAnnotation(isToolbox = true)
    public String getContenu() {
        return contenu;
    }

    @SetterAnnotation(isToolbox = true)
    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getObjets() {
        return objets;
    }

    public void setObjets(String objets) {
        this.objets = objets;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeRattachement() {
        return codeRattachement;
    }

    public void setCodeRattachement(String codeRattachement) {
        this.codeRattachement = codeRattachement;
    }

    public String getCodeRubrique() {
        return codeRubrique;
    }

    public void setCodeRubrique(String codeRubrique) {
        this.codeRubrique = codeRubrique;
    }
}
