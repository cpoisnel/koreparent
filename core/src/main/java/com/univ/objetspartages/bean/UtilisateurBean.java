package com.univ.objetspartages.bean;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * Created on 10/04/15.
 */
public class UtilisateurBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = -3659955420987100388L;

    private String code = StringUtils.EMPTY;

    private String motDePasse = StringUtils.EMPTY;

    private Date dateNaissance;

    private String civilite = StringUtils.EMPTY;

    private String nom = StringUtils.EMPTY;

    private String prenom = StringUtils.EMPTY;

    private String codeRattachement = StringUtils.EMPTY;

    private String groupes = StringUtils.EMPTY;

    private String adresseMail = StringUtils.EMPTY;

    private String restrictionValidation = StringUtils.EMPTY;

    private String extensionModification = StringUtils.EMPTY;

    private String centresInteret = StringUtils.EMPTY;

    private String profilDsi = StringUtils.EMPTY;

    private String groupesDsi = StringUtils.EMPTY;

    private String codeLdap = StringUtils.EMPTY;

    private String groupesDsiImport = StringUtils.EMPTY;

    private String roles = StringUtils.EMPTY;

    private Date dateDerniereSession;

    private String profilDefaut = StringUtils.EMPTY;

    private String sourceImport = StringUtils.EMPTY;

    private String formatEnvoi = StringUtils.EMPTY;

    private String modeSaisieExpert = "1";

    private Long tsCacheGroupes = 0L;

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(final String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(final Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(final String civilite) {
        this.civilite = civilite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(final String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(final String prenom) {
        this.prenom = prenom;
    }

    public String getCodeRattachement() {
        return codeRattachement;
    }

    public void setCodeRattachement(final String codeRattachement) {
        this.codeRattachement = codeRattachement;
    }

    public String getGroupes() {
        return groupes;
    }

    public void setGroupes(final String groupes) {
        this.groupes = groupes;
    }

    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(final String adresseMail) {
        this.adresseMail = adresseMail;
    }

    public String getRestrictionValidation() {
        return restrictionValidation;
    }

    public void setRestrictionValidation(final String restrictionValidation) {
        this.restrictionValidation = restrictionValidation;
    }

    public String getExtensionModification() {
        return extensionModification;
    }

    public void setExtensionModification(final String extensionModification) {
        this.extensionModification = extensionModification;
    }

    public String getCentresInteret() {
        return centresInteret;
    }

    public void setCentresInteret(final String centresInteret) {
        this.centresInteret = centresInteret;
    }

    public String getProfilDsi() {
        return profilDsi;
    }

    public void setProfilDsi(final String profilDsi) {
        this.profilDsi = profilDsi;
    }

    public String getGroupesDsi() {
        return groupesDsi;
    }

    public void setGroupesDsi(final String groupesDsi) {
        this.groupesDsi = groupesDsi;
    }

    public String getCodeLdap() {
        return codeLdap;
    }

    public void setCodeLdap(final String codeLdap) {
        this.codeLdap = codeLdap;
    }

    public String getGroupesDsiImport() {
        return groupesDsiImport;
    }

    public void setGroupesDsiImport(final String groupesDsiImport) {
        this.groupesDsiImport = groupesDsiImport;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(final String roles) {
        this.roles = roles;
    }

    public Date getDateDerniereSession() {
        return dateDerniereSession;
    }

    public void setDateDerniereSession(final Date dateDerniereSession) {
        this.dateDerniereSession = dateDerniereSession;
    }

    public String getProfilDefaut() {
        return profilDefaut;
    }

    public void setProfilDefaut(final String profilDefaut) {
        this.profilDefaut = profilDefaut;
    }

    public String getSourceImport() {
        return sourceImport;
    }

    public void setSourceImport(final String sourceImport) {
        this.sourceImport = sourceImport;
    }

    public String getFormatEnvoi() {
        return formatEnvoi;
    }

    public void setFormatEnvoi(final String formatEnvoi) {
        this.formatEnvoi = formatEnvoi;
    }

    public String getModeSaisieExpert() {
        return modeSaisieExpert;
    }

    public void setModeSaisieExpert(final String modeSaisieExpert) {
        this.modeSaisieExpert = modeSaisieExpert;
    }

    public Long getTsCacheGroupes() {
        return tsCacheGroupes;
    }

    public void setTsCacheGroupes(final Long tsCacheGroupes) {
        this.tsCacheGroupes = tsCacheGroupes;
    }
}
