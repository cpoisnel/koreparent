package com.univ.objetspartages.bean;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Bean contenant les informations de liaison entre les groupes et les utilisateurs.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class GroupeUtilisateurBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = -7832666640404894932L;

    private String codeGroupe = StringUtils.EMPTY;

    private String codeUtilisateur = StringUtils.EMPTY;

    private String sourceImport = StringUtils.EMPTY;

    public Long getIdGroupeutilisateur() {
        return getId();
    }

    public void setIdGroupeutilisateur(Long idGroupeutilisateur) {
        setId(idGroupeutilisateur);
    }

    public String getCodeGroupe() {
        return codeGroupe;
    }

    public void setCodeGroupe(String codeGroupe) {
        this.codeGroupe = codeGroupe;
    }

    public String getCodeUtilisateur() {
        return codeUtilisateur;
    }

    public void setCodeUtilisateur(String codeUtilisateur) {
        this.codeUtilisateur = codeUtilisateur;
    }

    public String getSourceImport() {
        return sourceImport;
    }

    public void setSourceImport(String sourceImport) {
        this.sourceImport = sourceImport;
    }
}
