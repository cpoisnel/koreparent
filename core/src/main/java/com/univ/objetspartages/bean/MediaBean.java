package com.univ.objetspartages.bean;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.kportal.cms.objetspartages.annotation.Fiche;
import com.kportal.cms.objetspartages.annotation.Rubrique;
import com.kportal.cms.objetspartages.annotation.User;

public class MediaBean extends AbstractPersistenceBean implements Serializable {

    private static final long serialVersionUID = -7936209136254349467L;

    /** The titre. */
    protected String titre = StringUtils.EMPTY;

    /** The legende. */
    protected String legende = StringUtils.EMPTY;

    /** The description. */
    protected String description = StringUtils.EMPTY;

    /** The auteur. */
    protected String auteur = StringUtils.EMPTY;

    /** The copyright. */
    protected String copyright = StringUtils.EMPTY;

    /** The type ressource. */
    protected String typeRessource = StringUtils.EMPTY;

    /** The type media. */
    protected String typeMedia = StringUtils.EMPTY;

    /** The source. */
    protected String source = StringUtils.EMPTY;

    /** The format. */
    protected String format = StringUtils.EMPTY;

    /** The code rubrique. */
    @Rubrique
    protected String codeRubrique = StringUtils.EMPTY;

    /** The code rattachement. */
    @Fiche(nomObjet = "defaultstructureksup")
    protected String codeRattachement = StringUtils.EMPTY;

    /** The url. */
    protected String url = StringUtils.EMPTY;

    /** The url vignette. */
    protected String urlVignette = StringUtils.EMPTY;

    /** The poids. */
    protected Long poids = null;

    /** The code redacteur. */
    @User
    protected String codeRedacteur = StringUtils.EMPTY;

    /** The date creation. */
    protected Date dateCreation = null;

    /** The thematique. */
    protected String thematique = StringUtils.EMPTY;

    /** The meta keywords. */
    protected String metaKeywords = StringUtils.EMPTY;

    /** The specific data. */
    protected String specificData = StringUtils.EMPTY;

    /** The traduction data. */
    protected String traductionData = StringUtils.EMPTY;

    /** The accessibility data. */
    protected String accessibilityData = StringUtils.EMPTY;

    /** The is mutualise. */
    protected String isMutualise = StringUtils.EMPTY;

    /**
     * Sets the titre.
     *
     * @param titre
     *            the new titre
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * Sets the legende.
     *
     * @param legende
     *            the new legende
     */
    public void setLegende(String legende) {
        this.legende = legende;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the auteur.
     *
     * @param auteur
     *            the new auteur
     */
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    /**
     * Sets the copyright.
     *
     * @param copyright
     *            the new copyright
     */
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    /**
     * Sets the type ressource.
     *
     * @param typeRessource
     *            the new type ressource
     */
    public void setTypeRessource(String typeRessource) {
        this.typeRessource = typeRessource;
    }

    /**
     * Sets the type media.
     *
     * @param typeMedia
     *            the new type media
     */
    public void setTypeMedia(String typeMedia) {
        this.typeMedia = typeMedia;
    }

    /**
     * Sets the source.
     *
     * @param source
     *            the new source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Sets the format.
     *
     * @param format
     *            the new format
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * Sets the code rubrique.
     *
     * @param codeRubrique
     *            the new code rubrique
     */
    public void setCodeRubrique(String codeRubrique) {
        this.codeRubrique = codeRubrique;
    }

    /**
     * Sets the code rattachement.
     *
     * @param codeRattachement
     *            the new code rattachement
     */
    public void setCodeRattachement(String codeRattachement) {
        this.codeRattachement = codeRattachement;
    }

    /**
     * Sets the url.
     *
     * @param url
     *            the new url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Sets the url vignette.
     *
     * @param urlVignette
     *            the new url vignette
     */
    public void setUrlVignette(String urlVignette) {
        this.urlVignette = urlVignette;
    }

    /**
     * Sets the poids.
     *
     * @param poids
     *            the new poids
     */
    public void setPoids(Long poids) {
        this.poids = poids;
    }

    /**
     * Sets the code redacteur.
     *
     * @param codeRedacteur
     *            the new code redacteur
     */
    public void setCodeRedacteur(String codeRedacteur) {
        this.codeRedacteur = codeRedacteur;
    }

    /**
     * Sets the date creation.
     *
     * @param dateCreation
     *            the new date creation
     */
    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * Sets the meta keywords.
     *
     * @param metaKeywords
     *            the new meta keywords
     */
    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    /**
     * Sets the specific data.
     *
     * @param specificData
     *            the new specific data
     */
    public void setSpecificData(String specificData) {
        this.specificData = specificData;
    }

    /**
     * Sets the traduction data.
     *
     * @param traductionData
     *            the new traduction data
     */
    public void setTraductionData(String traductionData) {
        this.traductionData = traductionData;
    }

    /**
     * Sets the accessibility data.
     *
     * @param accessibilityData
     *            the new accessibility data
     */
    public void setAccessibilityData(String accessibilityData) {
        this.accessibilityData = accessibilityData;
    }

    /**
     * Sets the checks if is mutualise.
     *
     * @param isMutualise
     *            the new checks if is mutualise
     */
    public void setIsMutualise(String isMutualise) {
        this.isMutualise = isMutualise;
    }

    /**
     * Gets the titre.
     *
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Gets the legende.
     *
     * @return the legende
     */
    public String getLegende() {
        return legende;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the auteur.
     *
     * @return the auteur
     */
    public String getAuteur() {
        return auteur;
    }

    /**
     * Gets the copyright.
     *
     * @return the copyright
     */
    public String getCopyright() {
        return copyright;
    }

    /**
     * Gets the type ressource.
     *
     * @return the type ressource
     */
    public String getTypeRessource() {
        return typeRessource;
    }

    /**
     * Gets the type media.
     *
     * @return the type media
     */
    public String getTypeMedia() {
        return typeMedia;
    }

    /**
     * Gets the source.
     *
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * Gets the format.
     *
     * @return the format
     */
    public String getFormat() {
        return format;
    }

    /**
     * Gets the code rubrique.
     *
     * @return the code rubrique
     */
    public String getCodeRubrique() {
        return codeRubrique;
    }

    /**
     * Gets the code rattachement.
     *
     * @return the code rattachement
     */
    public String getCodeRattachement() {
        return codeRattachement;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets the url vignette.
     *
     * @return the url vignette
     */
    public String getUrlVignette() {
        return urlVignette;
    }

    /**
     * Gets the poids.
     *
     * @return the poids
     */
    public Long getPoids() {
        return poids;
    }

    /**
     * Gets the code redacteur.
     *
     * @return the code redacteur
     */
    public String getCodeRedacteur() {
        return codeRedacteur;
    }

    /**
     * Gets the date creation.
     *
     * @return the date creation
     */
    public Date getDateCreation() {
        return dateCreation;
    }

    /**
     * Gets the meta keywords.
     *
     * @return the meta keywords
     */
    public String getMetaKeywords() {
        return metaKeywords;
    }

    /**
     * Gets the specific data.
     *
     * @return the specific data
     */
    public String getSpecificData() {
        return specificData;
    }

    /**
     * Gets the traduction data.
     *
     * @return the traduction data
     */
    public String getTraductionData() {
        return traductionData;
    }

    /**
     * Gets the accessibility data.
     *
     * @return the accessibility data
     */
    public String getAccessibilityData() {
        return accessibilityData;
    }

    /**
     * Gets the checks if is mutualise.
     *
     * @return the checks if is mutualise
     */
    public String getIsMutualise() {
        return isMutualise;
    }

    public String getThematique() {
        return thematique;
    }

    public void setThematique(String thematique) {
        this.thematique = thematique;
    }
}
