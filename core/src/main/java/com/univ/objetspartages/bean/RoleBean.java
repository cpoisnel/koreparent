package com.univ.objetspartages.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.univ.objetspartages.om.PermissionBean;

/**
 * Classe contenant les infos des rôles d'un utilisateur
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class RoleBean extends AbstractPersistenceBean implements Serializable {

    private static final long serialVersionUID = -4074232758132951643L;

    /** The code. */
    private String code = String.valueOf(System.currentTimeMillis());

    /** The libelle. */
    private String libelle = StringUtils.EMPTY;

    /** The perimetre. */
    private String perimetre = StringUtils.EMPTY;

    /** The permissions. */
    private Collection<PermissionBean> permissions = new ArrayList<>();

    public Long getIdRole() {
        return getId();
    }

    public void setIdRole(Long idRole) {
        setId(idRole);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getPerimetre() {
        return perimetre;
    }

    public void setPerimetre(String perimetre) {
        this.perimetre = perimetre;
    }

    public Collection<PermissionBean> getPermissions() {
        return permissions;
    }

    public void setPermissions(Collection<PermissionBean> permissions) {
        this.permissions = permissions;
    }
}
