package com.univ.objetspartages.bean;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bean contenant les informations d'un site à indexer
 */
public class SiteExterneBean extends AbstractPersistenceBean implements Cloneable {

    private static final Logger LOG = LoggerFactory.getLogger(SiteExterneBean.class);

    private static final long serialVersionUID = 8715719554411221076L;

    /** The code. */
    private String code = String.valueOf(System.currentTimeMillis());

    /** The libelle. */
    private String libelle = StringUtils.EMPTY;

    /** The url. */
    private String url = StringUtils.EMPTY;

    /** The reg exp accepte. */
    private String regExpAccepte = StringUtils.EMPTY;

    /** The niveau profondeur. */
    private Integer niveauProfondeur = -1;

    /** The reg exp refuse. */
    private String regExpRefuse = StringUtils.EMPTY;

    /** The langue. */
    private String langue = "0";

    public Long getIdSite() {
        return getId();
    }

    public void setIdSite(Long idSite) {
        setId(idSite);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRegExpAccepte() {
        return regExpAccepte;
    }

    public void setRegExpAccepte(String regExpAccepte) {
        this.regExpAccepte = regExpAccepte;
    }

    public Integer getNiveauProfondeur() {
        return niveauProfondeur;
    }

    public void setNiveauProfondeur(Integer niveauProfondeur) {
        this.niveauProfondeur = niveauProfondeur;
    }

    public String getRegExpRefuse() {
        return regExpRefuse;
    }

    public void setRegExpRefuse(String regExpRefuse) {
        this.regExpRefuse = regExpRefuse;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    @Override
    public Object clone() {
        SiteExterneBean result = null;
        try {
            result = (SiteExterneBean) super.clone();
        } catch (CloneNotSupportedException e) {
            LOG.error("unable to clone SiteExterneBean", e);
        }
        return result;
    }
}
