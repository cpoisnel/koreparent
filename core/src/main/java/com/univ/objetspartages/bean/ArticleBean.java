package com.univ.objetspartages.bean;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.kportal.cms.objetspartages.annotation.GetterAnnotation;
import com.kportal.cms.objetspartages.annotation.Label;
import com.kportal.cms.objetspartages.annotation.SetterAnnotation;
// TODO: Auto-generated Javadoc

/**
 * Classe d'acces aux donnees pour article.
 */
public class ArticleBean extends AbstractRestrictionFicheBean {

    private static final long serialVersionUID = 5378745888019802640L;

    /** The sous titre. */
    protected String sousTitre = StringUtils.EMPTY;

    /** The date article. */
    protected Date dateArticle = null;

    /** The id vignette. */
    protected Long idVignette = 0L;

    /** The chapeau. */
    protected String chapeau = StringUtils.EMPTY;

    /** The corps. */
    protected String corps = StringUtils.EMPTY;

    /** The thematique. */
    @Label(type = "04")
    protected String thematique = StringUtils.EMPTY;

    /** The ordre. */
    protected Integer ordre = 0;

    /**
     * Instantiates a new article bean.
     */
    public ArticleBean() {
        super();
    }

    /**
     * initialise un ArticleBean à partir de celui fourni en paramètre
     *
     * @param bean
     *            le bean servant à initialiser cette instance
     */
    public void init(final ArticleBean bean) {
        super.init(bean);
        this.sousTitre = bean.sousTitre;
        this.dateArticle = bean.dateArticle;
        this.idVignette = bean.idVignette;
        this.chapeau = bean.chapeau;
        this.corps = bean.corps;
        this.thematique = bean.thematique;
        this.ordre = bean.ordre;
    }

    /**
     * Gets the chapeau.
     *
     * @return the chapeau
     */
    public String getChapeau() {
        return chapeau;
    }

    /**
     * Sets the chapeau.
     *
     * @param chapeau
     *            the chapeau to set
     */
    public void setChapeau(final String chapeau) {
        this.chapeau = chapeau;
    }

    /**
     * Gets the corps.
     *
     * @return the corps
     */
    @GetterAnnotation(isToolbox = true)
    public String getCorps() {
        return corps;
    }

    /**
     * Sets the corps.
     *
     * @param corps
     *            the corps to set
     */
    @SetterAnnotation(isToolbox = true)
    public void setCorps(final String corps) {
        this.corps = corps;
    }

    /**
     * Gets the date article.
     *
     * @return the dateArticle
     */
    public Date getDateArticle() {
        return dateArticle;
    }

    /**
     * Sets the date article.
     *
     * @param dateArticle
     *            the dateArticle to set
     */
    public void setDateArticle(final Date dateArticle) {
        this.dateArticle = dateArticle;
    }

    /**
     * Gets the id article.
     *
     * @return the idArticle
     */
    public Long getIdArticle() {
        return id;
    }

    /**
     * Sets the id article.
     *
     * @param idArticle
     *            the idArticle to set
     */
    public void setIdArticle(final Long idArticle) {
        this.id = idArticle;
    }

    /**
     * Gets the id vignette.
     *
     * @return the idVignette
     */
    public Long getIdVignette() {
        return idVignette;
    }

    /**
     * Sets the id vignette.
     *
     * @param idVignette
     *            the idVignette to set
     */
    public void setIdVignette(final Long idVignette) {
        this.idVignette = idVignette;
    }

    /**
     * Gets the ordre.
     *
     * @return the ordre
     */
    public Integer getOrdre() {
        return ordre;
    }

    /**
     * Sets the ordre.
     *
     * @param ordre
     *            the ordre to set
     */
    public void setOrdre(final Integer ordre) {
        this.ordre = ordre;
    }

    /**
     * Gets the sous titre.
     *
     * @return the sousTitre
     */
    public String getSousTitre() {
        return sousTitre;
    }

    /**
     * Sets the sous titre.
     *
     * @param sousTitre
     *            the sousTitre to set
     */
    public void setSousTitre(final String sousTitre) {
        this.sousTitre = sousTitre;
    }

    /**
     * Gets the thematique.
     *
     * @return the thematique
     */
    public String getThematique() {
        return thematique;
    }

    /**
     * Sets the thematique.
     *
     * @param thematique
     *            the thematique to set
     */
    public void setThematique(final String thematique) {
        this.thematique = thematique;
    }
}
