package com.univ.objetspartages.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Classe d'acces aux donnees pour rubrique.
 */
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class RubriqueBean extends AbstractPersistenceBean {

    /**
     *
     */
    private static final long serialVersionUID = -3940774569254922021L;

    /** The code. */
    protected String code = "";

    /** The langue. */
    protected String langue = "";

    /** The intitule. */
    protected String intitule = "";

    /** The nom onglet. */
    protected String nomOnglet = "";

    /** The accroche. */
    protected String accroche = "";

    /** The id bandeau. */
    protected Long idBandeau = (long) 0;

    /** The couleur fond. */
    protected String couleurFond = "";

    /** The couleur titre. */
    protected String couleurTitre = "";

    /** The code rubrique mere. */
    protected String codeRubriqueMere = "";

    /** The type rubrique. */
    protected String typeRubrique = "";

    /** The page accueil. */
    protected String pageAccueil = "";

    /** The gestion encadre. */
    protected String gestionEncadre = "";

    /** The encadre. */
    protected String encadre = "";

    /** The encadre sous rubrique. */
    protected String encadreSousRubrique = "";

    /** The ordre. */
    protected String ordre = "";

    /** The contact. */
    protected String contact = "";

    /** The groupes dsi. */
    protected String groupesDsi = "";

    /** The requetes rubrique publication. */
    protected String requetesRubriquePublication = "";

    /** The categorie. */
    protected String categorie = "";

    /** The id picto. */
    protected Long idPicto = (long) 0;

    /**
     * Instantiates a new rubrique bean.
     */
    public RubriqueBean() {
        super();
    }

    /**
     * Inits the.
     *
     * @param bean
     *            the bean
     */
    public void init(RubriqueBean bean) {
        this.id = bean.id;
        this.code = bean.code;
        this.langue = bean.langue;
        this.intitule = bean.intitule;
        this.nomOnglet = bean.nomOnglet;
        this.accroche = bean.accroche;
        this.idBandeau = bean.idBandeau;
        this.couleurFond = bean.couleurFond;
        this.couleurTitre = bean.couleurTitre;
        this.codeRubriqueMere = bean.codeRubriqueMere;
        this.typeRubrique = bean.typeRubrique;
        this.pageAccueil = bean.pageAccueil;
        this.gestionEncadre = bean.gestionEncadre;
        this.encadre = bean.encadre;
        this.encadreSousRubrique = bean.encadreSousRubrique;
        this.ordre = bean.ordre;
        this.contact = bean.contact;
        this.groupesDsi = bean.groupesDsi;
        this.requetesRubriquePublication = bean.requetesRubriquePublication;
        this.categorie = bean.categorie;
        this.idPicto = bean.idPicto;
    }

    /**
     * Gets the accroche.
     *
     * @return the accroche
     */
    public String getAccroche() {
        return accroche;
    }

    /**
     * Sets the accroche.
     *
     * @param accroche
     *            the accroche to set
     */
    public void setAccroche(String accroche) {
        this.accroche = accroche;
    }

    /**
     * Gets the categorie.
     *
     * @return the categorie
     */
    public String getCategorie() {
        return categorie;
    }

    /**
     * Sets the categorie.
     *
     * @param categorie
     *            the categorie to set
     */
    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the code rubrique mere.
     *
     * @return the codeRubriqueMere
     */
    public String getCodeRubriqueMere() {
        return codeRubriqueMere;
    }

    /**
     * Sets the code rubrique mere.
     *
     * @param codeRubriqueMere
     *            the codeRubriqueMere to set
     */
    public void setCodeRubriqueMere(String codeRubriqueMere) {
        this.codeRubriqueMere = codeRubriqueMere;
    }

    /**
     * Gets the contact.
     *
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets the contact.
     *
     * @param contact
     *            the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * Gets the couleur fond.
     *
     * @return the couleurFond
     */
    public String getCouleurFond() {
        return couleurFond;
    }

    /**
     * Sets the couleur fond.
     *
     * @param couleurFond
     *            the couleurFond to set
     */
    public void setCouleurFond(String couleurFond) {
        this.couleurFond = couleurFond;
    }

    /**
     * Gets the couleur titre.
     *
     * @return the couleurTitre
     */
    public String getCouleurTitre() {
        return couleurTitre;
    }

    /**
     * Sets the couleur titre.
     *
     * @param couleurTitre
     *            the couleurTitre to set
     */
    public void setCouleurTitre(String couleurTitre) {
        this.couleurTitre = couleurTitre;
    }

    /**
     * Gets the encadre.
     *
     * @return the encadre
     */
    public String getEncadre() {
        return encadre;
    }

    /**
     * Sets the encadre.
     *
     * @param encadre
     *            the encadre to set
     */
    public void setEncadre(String encadre) {
        this.encadre = encadre;
    }

    /**
     * Gets the encadre sous rubrique.
     *
     * @return the encadreSousRubrique
     */
    public String getEncadreSousRubrique() {
        return encadreSousRubrique;
    }

    /**
     * Sets the encadre sous rubrique.
     *
     * @param encadreSousRubrique
     *            the encadreSousRubrique to set
     */
    public void setEncadreSousRubrique(String encadreSousRubrique) {
        this.encadreSousRubrique = encadreSousRubrique;
    }

    /**
     * Gets the gestion encadre.
     *
     * @return the gestionEncadre
     */
    public String getGestionEncadre() {
        return gestionEncadre;
    }

    /**
     * Sets the gestion encadre.
     *
     * @param gestionEncadre
     *            the gestionEncadre to set
     */
    public void setGestionEncadre(String gestionEncadre) {
        this.gestionEncadre = gestionEncadre;
    }

    /**
     * Gets the groupes dsi.
     *
     * @return the groupesDsi
     */
    public String getGroupesDsi() {
        return groupesDsi;
    }

    /**
     * Sets the groupes dsi.
     *
     * @param groupesDsi
     *            the groupesDsi to set
     */
    public void setGroupesDsi(String groupesDsi) {
        this.groupesDsi = groupesDsi;
    }

    /**
     * Gets the id bandeau.
     *
     * @return the idBandeau
     */
    public Long getIdBandeau() {
        return idBandeau;
    }

    /**
     * Sets the id bandeau.
     *
     * @param idBandeau
     *            the idBandeau to set
     */
    public void setIdBandeau(Long idBandeau) {
        this.idBandeau = idBandeau;
    }

    /**
     * Gets the id rubrique.
     *
     * @return the idRubrique
     */
    public Long getIdRubrique() {
        return getId();
    }

    /**
     * Sets the id rubrique.
     *
     * @param idRubrique
     *            the idRubrique to set
     */
    public void setIdRubrique(Long idRubrique) {
        setId(idRubrique);
    }

    /**
     * Gets the intitule.
     *
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Sets the intitule.
     *
     * @param intitule
     *            the intitule to set
     */
    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    /**
     * Gets the langue.
     *
     * @return the langue
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Sets the langue.
     *
     * @param langue
     *            the langue to set
     */
    public void setLangue(String langue) {
        this.langue = langue;
    }

    /**
     * Gets the nom onglet.
     *
     * @return the nomOnglet
     */
    public String getNomOnglet() {
        return nomOnglet;
    }

    /**
     * Sets the nom onglet.
     *
     * @param nomOnglet
     *            the nomOnglet to set
     */
    public void setNomOnglet(String nomOnglet) {
        this.nomOnglet = nomOnglet;
    }

    /**
     * Gets the ordre.
     *
     * @return the ordre
     */
    public String getOrdre() {
        return ordre;
    }

    /**
     * Sets the ordre.
     *
     * @param ordre
     *            the ordre to set
     */
    public void setOrdre(String ordre) {
        this.ordre = ordre;
    }

    /**
     * Gets the requetes rubrique publication.
     *
     * @return the requetesRubriquePublication
     */
    public String getRequetesRubriquePublication() {
        return requetesRubriquePublication;
    }

    /**
     * Sets the requetes rubrique publication.
     *
     * @param requetesRubriquePublication
     *            the requetesRubriquePublication to set
     */
    public void setRequetesRubriquePublication(String requetesRubriquePublication) {
        this.requetesRubriquePublication = requetesRubriquePublication;
    }

    /**
     * Gets the type rubrique.
     *
     * @return the typeRubrique
     */
    public String getTypeRubrique() {
        return typeRubrique;
    }

    /**
     * Sets the type rubrique.
     *
     * @param typeRubrique
     *            the typeRubrique to set
     */
    public void setTypeRubrique(String typeRubrique) {
        this.typeRubrique = typeRubrique;
    }

    public String getPageAccueil() {
        return pageAccueil;
    }

    public void setPageAccueil(String pageAccueil) {
        this.pageAccueil = pageAccueil;
    }

    public Long getIdPicto() {
        return idPicto;
    }

    public void setIdPicto(Long idPicto) {
        this.idPicto = idPicto;
    }
}
