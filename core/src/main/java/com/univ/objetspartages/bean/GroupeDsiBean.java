package com.univ.objetspartages.bean;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class GroupeDsiBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = 9202190749623135708L;

    /** The code. */
    private String code = String.valueOf(System.currentTimeMillis());

    /** The libelle. */
    private String libelle = StringUtils.EMPTY;

    /** The type. */
    private String type = StringUtils.EMPTY;

    /** The code structure. */
    private String codeStructure = StringUtils.EMPTY;

    /** The code page tete. */
    private String codePageTete = StringUtils.EMPTY;

    /** The roles. */
    private String roles = StringUtils.EMPTY;

    /** The code groupe pere. */
    private String codeGroupePere = StringUtils.EMPTY;

    /** The requete groupe. */
    private String requeteGroupe = StringUtils.EMPTY;

    /** The requete ldap. */
    private String requeteLdap = StringUtils.EMPTY;

    /** The source import. */
    private String sourceImport = StringUtils.EMPTY;

    /** The gestion cache. */
    private String gestionCache = "0";

    /** The delai expiration cache. */
    private Long delaiExpirationCache = 0l;

    /** The derniere maj cache. */
    private Long derniereMajCache = 0l;

    /** The selectionnable. */
    private String selectionnable = "1";

    public Long getIdGroupedsi() {
        return getId();
    }

    public void setIdGroupedsi(Long idGroupedsi) {
        setId(idGroupedsi);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCodeStructure() {
        return codeStructure;
    }

    public void setCodeStructure(String codeStructure) {
        this.codeStructure = codeStructure;
    }

    public String getCodePageTete() {
        return codePageTete;
    }

    public void setCodePageTete(String codePageTete) {
        this.codePageTete = codePageTete;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getCodeGroupePere() {
        return codeGroupePere;
    }

    public void setCodeGroupePere(String codeGroupePere) {
        this.codeGroupePere = codeGroupePere;
    }

    public String getRequeteGroupe() {
        return requeteGroupe;
    }

    public void setRequeteGroupe(String requeteGroupe) {
        this.requeteGroupe = requeteGroupe;
    }

    public String getRequeteLdap() {
        return requeteLdap;
    }

    public void setRequeteLdap(String requeteLdap) {
        this.requeteLdap = requeteLdap;
    }

    public String getSourceImport() {
        return sourceImport;
    }

    public void setSourceImport(String sourceImport) {
        this.sourceImport = sourceImport;
    }

    public String getGestionCache() {
        return gestionCache;
    }

    public void setGestionCache(String gestionCache) {
        this.gestionCache = gestionCache;
    }

    public Long getDelaiExpirationCache() {
        return delaiExpirationCache;
    }

    public void setDelaiExpirationCache(Long delaiExpirationCache) {
        this.delaiExpirationCache = delaiExpirationCache;
    }

    public Long getDerniereMajCache() {
        return derniereMajCache;
    }

    public void setDerniereMajCache(Long derniereMajCache) {
        this.derniereMajCache = derniereMajCache;
    }

    public String getSelectionnable() {
        return selectionnable;
    }

    public void setSelectionnable(String selectionnable) {
        this.selectionnable = selectionnable;
    }
}
