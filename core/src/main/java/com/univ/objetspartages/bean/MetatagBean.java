package com.univ.objetspartages.bean;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * Bean contenant les infos complémentaires d'une fiches.
 */
public class MetatagBean extends AbstractPersistenceBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6796792575799157194L;

    /** The meta id fiche. */
    protected Long metaIdFiche = 0L;

    /** The meta code objet. */
    protected String metaCodeObjet = StringUtils.EMPTY;

    /** The meta libelle objet. */
    protected String metaLibelleObjet = StringUtils.EMPTY;

    /** The meta historique. */
    protected String metaHistorique = StringUtils.EMPTY;

    /** The meta date archivage. */
    protected Date metaDateArchivage;

    /** The meta liste references. */
    protected String metaListeReferences = StringUtils.EMPTY;

    /** The meta forum. */
    protected String metaForum = StringUtils.EMPTY;

    /** The meta forum ano. */
    protected String metaForumAno = StringUtils.EMPTY;

    /** The meta saisie front. */
    protected String metaSaisieFront = StringUtils.EMPTY;

    /** The meta mail anonyme. */
    protected String metaMailAnonyme = StringUtils.EMPTY;

    /** The meta notification mail. */
    protected String metaNotificationMail = StringUtils.EMPTY;

    /** The meta in tree. */
    protected String metaInTree = StringUtils.EMPTY;

    /** The meta document fichiergw. */
    protected String metaDocumentFichiergw = StringUtils.EMPTY;

    /** The meta rubriques publication. */
    protected String metaRubriquesPublication = StringUtils.EMPTY;

    /** The meta niveau approbation. */
    protected String metaNiveauApprobation = StringUtils.EMPTY;

    /** The meta libelle fiche. */
    protected String metaLibelleFiche = StringUtils.EMPTY;

    /** The meta code. */
    protected String metaCode = StringUtils.EMPTY;

    /** The meta code rattachement. */
    protected String metaCodeRattachement = StringUtils.EMPTY;

    /** The meta code rubrique. */
    protected String metaCodeRubrique = StringUtils.EMPTY;

    /** The meta meta keywords. */
    protected String metaKeywords = StringUtils.EMPTY;

    /** The meta meta description. */
    protected String metaDescription = StringUtils.EMPTY;

    /** The meta date creation. */
    protected Date metaDateCreation;

    /** The meta date proposition. */
    protected Date metaDateProposition;

    /** The meta date validation. */
    protected Date metaDateValidation;

    /** The meta date modification. */
    protected Date metaDateModification;

    /** The meta date modification. */
    protected Date metaDateOperation;

    /** The meta code redacteur. */
    protected String metaCodeRedacteur = StringUtils.EMPTY;

    /** The meta code validation. */
    protected String metaCodeValidation = StringUtils.EMPTY;

    /** The meta langue. */
    protected String metaLangue = StringUtils.EMPTY;

    /** The meta etat objet. */
    protected String metaEtatObjet = StringUtils.EMPTY;

    /** The meta nb hits. */
    protected Long metaNbHits;

    /** The meta source import. */
    protected String metaSourceImport = StringUtils.EMPTY;

    /** The meta code rattachement autres. */
    protected String metaCodeRattachementAutres = StringUtils.EMPTY;

    /** The meta diffusion public vise. */
    protected String metaDiffusionPublicVise = StringUtils.EMPTY;

    /** The meta diffusion mode restriction. */
    protected String metaDiffusionModeRestriction = StringUtils.EMPTY;

    /** The meta diffusion public vise restriction. */
    protected String metaDiffusionPublicViseRestriction = StringUtils.EMPTY;

    /** The meta date mise en ligne. */
    protected Date metaDateMiseEnLigne;

    /** The meta date suppression. */
    protected Date metaDateSuppression;

    /** The meta date rubriquage. */
    protected Date metaDateRubriquage;

    /** The meta code rubriquage. */
    protected String metaCodeRubriquage = StringUtils.EMPTY;

    /**
     * Gets the meta diffusion mode restriction.
     *
     * @return the metaDiffusionModeRestriction
     */
    public String getMetaDiffusionModeRestriction() {
        return metaDiffusionModeRestriction;
    }

    /**
     * Sets the meta diffusion mode restriction.
     *
     * @param metaDiffusionModeRestriction
     *            the metaDiffusionModeRestriction to set
     */
    public void setMetaDiffusionModeRestriction(final String metaDiffusionModeRestriction) {
        this.metaDiffusionModeRestriction = metaDiffusionModeRestriction;
    }

    /**
     * Gets the meta code.
     *
     * @return the metaCode
     */
    public String getMetaCode() {
        return metaCode;
    }

    /**
     * Sets the meta code.
     *
     * @param metaCode
     *            the metaCode to set
     */
    public void setMetaCode(final String metaCode) {
        this.metaCode = metaCode;
    }

    /**
     * Gets the meta code objet.
     *
     * @return the metaCodeObjet
     */
    public String getMetaCodeObjet() {
        return metaCodeObjet;
    }

    /**
     * Sets the meta code objet.
     *
     * @param metaCodeObjet
     *            the metaCodeObjet to set
     */
    public void setMetaCodeObjet(final String metaCodeObjet) {
        this.metaCodeObjet = metaCodeObjet;
    }

    /**
     * Gets the meta code rattachement.
     *
     * @return the metaCodeRattachement
     */
    public String getMetaCodeRattachement() {
        return metaCodeRattachement;
    }

    /**
     * Sets the meta code rattachement.
     *
     * @param metaCodeRattachement
     *            the metaCodeRattachement to set
     */
    public void setMetaCodeRattachement(final String metaCodeRattachement) {
        this.metaCodeRattachement = metaCodeRattachement;
    }

    /**
     * Gets the meta code rattachement autres.
     *
     * @return the metaCodeRattachementAutres
     */
    public String getMetaCodeRattachementAutres() {
        return metaCodeRattachementAutres;
    }

    /**
     * Sets the meta code rattachement autres.
     *
     * @param metaCodeRattachementAutres
     *            the metaCodeRattachementAutres to set
     */
    public void setMetaCodeRattachementAutres(final String metaCodeRattachementAutres) {
        this.metaCodeRattachementAutres = metaCodeRattachementAutres;
    }

    /**
     * Gets the meta code redacteur.
     *
     * @return the metaCodeRedacteur
     */
    public String getMetaCodeRedacteur() {
        return metaCodeRedacteur;
    }

    /**
     * Sets the meta code redacteur.
     *
     * @param metaCodeRedacteur
     *            the metaCodeRedacteur to set
     */
    public void setMetaCodeRedacteur(final String metaCodeRedacteur) {
        this.metaCodeRedacteur = metaCodeRedacteur;
    }

    /**
     * Gets the meta code rubriquage.
     *
     * @return the metaCodeRubriquage
     */
    public String getMetaCodeRubriquage() {
        return metaCodeRubriquage;
    }

    /**
     * Sets the meta code rubriquage.
     *
     * @param metaCodeRubriquage
     *            the metaCodeRubriquage to set
     */
    public void setMetaCodeRubriquage(final String metaCodeRubriquage) {
        this.metaCodeRubriquage = metaCodeRubriquage;
    }

    /**
     * Gets the meta code rubrique.
     *
     * @return the metaCodeRubrique
     */
    public String getMetaCodeRubrique() {
        return metaCodeRubrique;
    }

    /**
     * Sets the meta code rubrique.
     *
     * @param metaCodeRubrique
     *            the metaCodeRubrique to set
     */
    public void setMetaCodeRubrique(final String metaCodeRubrique) {
        this.metaCodeRubrique = metaCodeRubrique;
    }

    /**
     * Gets the meta code validation.
     *
     * @return the metaCodeValidation
     */
    public String getMetaCodeValidation() {
        return metaCodeValidation;
    }

    /**
     * Sets the meta code validation.
     *
     * @param metaCodeValidation
     *            the metaCodeValidation to set
     */
    public void setMetaCodeValidation(final String metaCodeValidation) {
        this.metaCodeValidation = metaCodeValidation;
    }

    /**
     * Gets the meta date archivage.
     *
     * @return the metaDateArchivage
     */
    public Date getMetaDateArchivage() {
        return metaDateArchivage;
    }

    /**
     * Sets the meta date archivage.
     *
     * @param metaDateArchivage
     *            the metaDateArchivage to set
     */
    public void setMetaDateArchivage(final Date metaDateArchivage) {
        this.metaDateArchivage = metaDateArchivage;
    }

    /**
     * Gets the meta date creation.
     *
     * @return the metaDateCreation
     */
    public Date getMetaDateCreation() {
        return metaDateCreation;
    }

    /**
     * Sets the meta date creation.
     *
     * @param metaDateCreation
     *            the metaDateCreation to set
     */
    public void setMetaDateCreation(final Date metaDateCreation) {
        this.metaDateCreation = metaDateCreation;
    }

    /**
     * Gets the meta date mise en ligne.
     *
     * @return the metaDateMiseEnLigne
     */
    public Date getMetaDateMiseEnLigne() {
        return metaDateMiseEnLigne;
    }

    /**
     * Sets the meta date mise en ligne.
     *
     * @param metaDateMiseEnLigne
     *            the metaDateMiseEnLigne to set
     */
    public void setMetaDateMiseEnLigne(final Date metaDateMiseEnLigne) {
        this.metaDateMiseEnLigne = metaDateMiseEnLigne;
    }

    /**
     * Gets the meta date modification.
     *
     * @return the metaDateModification
     */
    public Date getMetaDateModification() {
        return metaDateModification;
    }

    /**
     * Sets the meta date modification.
     *
     * @param metaDateModification
     *            the metaDateModification to set
     */
    public void setMetaDateModification(final Date metaDateModification) {
        this.metaDateModification = metaDateModification;
    }

    /**
     * Gets the meta date proposition.
     *
     * @return the metaDateProposition
     */
    public Date getMetaDateProposition() {
        return metaDateProposition;
    }

    /**
     * Sets the meta date proposition.
     *
     * @param metaDateProposition
     *            the metaDateProposition to set
     */
    public void setMetaDateProposition(final Date metaDateProposition) {
        this.metaDateProposition = metaDateProposition;
    }

    /**
     * Gets the meta date rubriquage.
     *
     * @return the metaDateRubriquage
     */
    public Date getMetaDateRubriquage() {
        return metaDateRubriquage;
    }

    /**
     * Sets the meta date rubriquage.
     *
     * @param metaDateRubriquage
     *            the metaDateRubriquage to set
     */
    public void setMetaDateRubriquage(final Date metaDateRubriquage) {
        this.metaDateRubriquage = metaDateRubriquage;
    }

    /**
     * Gets the meta date suppression.
     *
     * @return the metaDateSuppression
     */
    public Date getMetaDateSuppression() {
        return metaDateSuppression;
    }

    /**
     * Sets the meta date suppression.
     *
     * @param metaDateSuppression
     *            the metaDateSuppression to set
     */
    public void setMetaDateSuppression(final Date metaDateSuppression) {
        this.metaDateSuppression = metaDateSuppression;
    }

    /**
     * Gets the meta date validation.
     *
     * @return the metaDateValidation
     */
    public Date getMetaDateValidation() {
        return metaDateValidation;
    }

    /**
     * Sets the meta date validation.
     *
     * @param metaDateValidation
     *            the metaDateValidation to set
     */
    public void setMetaDateValidation(final Date metaDateValidation) {
        this.metaDateValidation = metaDateValidation;
    }

    /**
     * Gets the meta diffusion public vise.
     *
     * @return the metaDiffusionPublicVise
     */
    public String getMetaDiffusionPublicVise() {
        return metaDiffusionPublicVise;
    }

    /**
     * Sets the meta diffusion public vise.
     *
     * @param metaDiffusionPublicVise
     *            the metaDiffusionPublicVise to set
     */
    public void setMetaDiffusionPublicVise(final String metaDiffusionPublicVise) {
        this.metaDiffusionPublicVise = metaDiffusionPublicVise;
    }

    /**
     * Gets the meta diffusion public vise restriction.
     *
     * @return the metaDiffusionPublicViseRestriction
     */
    public String getMetaDiffusionPublicViseRestriction() {
        return metaDiffusionPublicViseRestriction;
    }

    /**
     * Sets the meta diffusion public vise restriction.
     *
     * @param metaDiffusionPublicViseRestriction
     *            the metaDiffusionPublicViseRestriction to set
     */
    public void setMetaDiffusionPublicViseRestriction(final String metaDiffusionPublicViseRestriction) {
        this.metaDiffusionPublicViseRestriction = metaDiffusionPublicViseRestriction;
    }

    /**
     * Gets the meta document fichiergw.
     *
     * @return the metaDocumentFichiergw
     */
    public String getMetaDocumentFichiergw() {
        return metaDocumentFichiergw;
    }

    /**
     * Sets the meta document fichiergw.
     *
     * @param metaDocumentFichiergw
     *            the metaDocumentFichiergw to set
     */
    public void setMetaDocumentFichiergw(final String metaDocumentFichiergw) {
        this.metaDocumentFichiergw = metaDocumentFichiergw;
    }

    /**
     * Gets the meta etat objet.
     *
     * @return the metaEtatObjet
     */
    public String getMetaEtatObjet() {
        return metaEtatObjet;
    }

    /**
     * Sets the meta etat objet.
     *
     * @param metaEtatObjet
     *            the metaEtatObjet to set
     */
    public void setMetaEtatObjet(final String metaEtatObjet) {
        this.metaEtatObjet = metaEtatObjet;
    }

    /**
     * Gets the meta forum.
     *
     * @return the metaForum
     */
    public String getMetaForum() {
        return metaForum;
    }

    /**
     * Sets the meta forum.
     *
     * @param metaForum
     *            the metaForum to set
     */
    public void setMetaForum(final String metaForum) {
        this.metaForum = metaForum;
    }

    /**
     * Gets the meta forum ano.
     *
     * @return the metaForumAno
     */
    public String getMetaForumAno() {
        return metaForumAno;
    }

    /**
     * Sets the meta forum ano.
     *
     * @param metaForumAno
     *            the metaForumAno to set
     */
    public void setMetaForumAno(final String metaForumAno) {
        this.metaForumAno = metaForumAno;
    }

    /**
     * Gets the meta historique.
     *
     * @return the metaHistorique
     */
    public String getMetaHistorique() {
        return metaHistorique;
    }

    /**
     * Sets the meta historique.
     *
     * @param metaHistorique
     *            the metaHistorique to set
     */
    public void setMetaHistorique(final String metaHistorique) {
        this.metaHistorique = metaHistorique;
    }

    /**
     * Gets the meta id fiche.
     *
     * @return the metaIdFiche
     */
    public Long getMetaIdFiche() {
        return metaIdFiche;
    }

    /**
     * Sets the meta id fiche.
     *
     * @param metaIdFiche
     *            the metaIdFiche to set
     */
    public void setMetaIdFiche(final Long metaIdFiche) {
        this.metaIdFiche = metaIdFiche;
    }

    /**
     * Gets the meta in tree.
     *
     * @return the metaInTree
     */
    public String getMetaInTree() {
        return metaInTree;
    }

    /**
     * Sets the meta in tree.
     *
     * @param metaInTree
     *            the metaInTree to set
     */
    public void setMetaInTree(final String metaInTree) {
        this.metaInTree = metaInTree;
    }

    /**
     * Gets the meta langue.
     *
     * @return the metaLangue
     */
    public String getMetaLangue() {
        return metaLangue;
    }

    /**
     * Sets the meta langue.
     *
     * @param metaLangue
     *            the metaLangue to set
     */
    public void setMetaLangue(final String metaLangue) {
        this.metaLangue = metaLangue;
    }

    /**
     * Gets the meta libelle fiche.
     *
     * @return the metaLibelleFiche
     */
    public String getMetaLibelleFiche() {
        return metaLibelleFiche;
    }

    /**
     * Sets the meta libelle fiche.
     *
     * @param metaLibelleFiche
     *            the metaLibelleFiche to set
     */
    public void setMetaLibelleFiche(final String metaLibelleFiche) {
        this.metaLibelleFiche = metaLibelleFiche;
    }

    /**
     * Gets the meta libelle objet.
     *
     * @return the metaLibelleObjet
     */
    public String getMetaLibelleObjet() {
        return metaLibelleObjet;
    }

    /**
     * Sets the meta libelle objet.
     *
     * @param metaLibelleObjet
     *            the metaLibelleObjet to set
     */
    public void setMetaLibelleObjet(final String metaLibelleObjet) {
        this.metaLibelleObjet = metaLibelleObjet;
    }

    /**
     * Gets the meta liste references.
     *
     * @return the metaListeReferences
     */
    public String getMetaListeReferences() {
        return metaListeReferences;
    }

    /**
     * Sets the meta liste references.
     *
     * @param metaListeReferences
     *            the metaListeReferences to set
     */
    public void setMetaListeReferences(final String metaListeReferences) {
        this.metaListeReferences = metaListeReferences;
    }

    /**
     * Gets the meta mail anonyme.
     *
     * @return the metaMailAnonyme
     */
    public String getMetaMailAnonyme() {
        return metaMailAnonyme;
    }

    /**
     * Sets the meta mail anonyme.
     *
     * @param metaMailAnonyme
     *            the metaMailAnonyme to set
     */
    public void setMetaMailAnonyme(final String metaMailAnonyme) {
        this.metaMailAnonyme = metaMailAnonyme;
    }

    /**
     * Gets the meta meta description.
     *
     * @return the metaDescription
     */
    public String getMetaDescription() {
        return metaDescription;
    }

    /**
     * Sets the meta meta description.
     *
     * @param metaDescription
     *            the metaDescription to set
     */
    public void setMetaDescription(final String metaDescription) {
        this.metaDescription = metaDescription;
    }

    /**
     * Gets the meta meta keywords.
     *
     * @return the metaKeywords
     */
    public String getMetaKeywords() {
        return metaKeywords;
    }

    /**
     * Sets the meta meta keywords.
     *
     * @param metaKeywords
     *            the metaKeywords to set
     */
    public void setMetaKeywords(final String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    /**
     * Gets the meta nb hits.
     *
     * @return the metaNbHits
     */
    public Long getMetaNbHits() {
        return metaNbHits;
    }

    /**
     * Sets the meta nb hits.
     *
     * @param metaNbHits
     *            the metaNbHits to set
     */
    public void setMetaNbHits(final Long metaNbHits) {
        this.metaNbHits = metaNbHits;
    }

    /**
     * Gets the meta niveau approbation.
     *
     * @return the metaNiveauApprobation
     */
    public String getMetaNiveauApprobation() {
        return metaNiveauApprobation;
    }

    /**
     * Sets the meta niveau approbation.
     *
     * @param metaNiveauApprobation
     *            the metaNiveauApprobation to set
     */
    public void setMetaNiveauApprobation(final String metaNiveauApprobation) {
        this.metaNiveauApprobation = metaNiveauApprobation;
    }

    /**
     * Gets the meta notification mail.
     *
     * @return the metaNotificationMail
     */
    public String getMetaNotificationMail() {
        return metaNotificationMail;
    }

    /**
     * Sets the meta notification mail.
     *
     * @param metaNotificationMail
     *            the metaNotificationMail to set
     */
    public void setMetaNotificationMail(final String metaNotificationMail) {
        this.metaNotificationMail = metaNotificationMail;
    }

    /**
     * Gets the meta rubriques publication.
     *
     * @return the metaRubriquesPublication
     */
    public String getMetaRubriquesPublication() {
        return metaRubriquesPublication;
    }

    /**
     * Sets the meta rubriques publication.
     *
     * @param metaRubriquesPublication
     *            the metaRubriquesPublication to set
     */
    public void setMetaRubriquesPublication(final String metaRubriquesPublication) {
        this.metaRubriquesPublication = metaRubriquesPublication;
    }

    /**
     * Gets the meta saisie front.
     *
     * @return the metaSaisieFront
     */
    public String getMetaSaisieFront() {
        return metaSaisieFront;
    }

    /**
     * Sets the meta saisie front.
     *
     * @param metaSaisieFront
     *            the metaSaisieFront to set
     */
    public void setMetaSaisieFront(final String metaSaisieFront) {
        this.metaSaisieFront = metaSaisieFront;
    }

    /**
     * Gets the meta source import.
     *
     * @return the metaSourceImport
     */
    public String getMetaSourceImport() {
        return metaSourceImport;
    }

    /**
     * Sets the meta source import.
     *
     * @param metaSourceImport
     *            the metaSourceImport to set
     */
    public void setMetaSourceImport(final String metaSourceImport) {
        this.metaSourceImport = metaSourceImport;
    }

    /**
     * Gets the meta date operation.
     *
     * @return the meta date operation
     */
    public Date getMetaDateOperation() {
        return metaDateOperation;
    }

    /**
     * Sets the meta date operation.
     *
     * @param metaDateOperation
     *            the new meta date operation
     */
    public void setMetaDateOperation(final Date metaDateOperation) {
        this.metaDateOperation = metaDateOperation;
    }

}
