package com.univ.objetspartages.bean;

import java.io.Serializable;

public class RessourceBean extends AbstractPersistenceBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6613619618476993523L;

    /** The id media. */
    protected Long idMedia = null;

    /** The code parent. */
    protected String codeParent = null;

    /** The etat. */
    protected String etat = null;

    /** The ordre. */
    protected Integer ordre = null;

    public Long getIdMedia() {
        return idMedia;
    }

    public void setIdMedia(final Long idMedia) {
        this.idMedia = idMedia;
    }

    public String getCodeParent() {
        return codeParent;
    }

    public void setCodeParent(final String codeParent) {
        this.codeParent = codeParent;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(final String etat) {
        this.etat = etat;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(final Integer ordre) {
        this.ordre = ordre;
    }
}
