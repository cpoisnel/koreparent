package com.univ.objetspartages.processus;

import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.AbstractProcessusBean;
import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.core.ProcessusHelper;
import com.jsbsoft.jtf.core.TypeMessage;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.multisites.InfosFicheReferencee;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheObjet;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceEncadre;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.synchro.SynchroGroupedsiStructure;
import com.univ.url.UrlManager;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteDao;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.MailUtil;
import com.univ.utils.URLResolver;

/**
 * Creation date: (19/10/2001 18:05:21).
 *
 * @author AM 02.2003 : Modification cf traiterPrincipal
 */
public class ControleurUniv {

    private static final Logger LOG = LoggerFactory.getLogger(ControleurUniv.class);

    /**
     * Format de date pour les heures minutes (HH:mm)
     */
    private final static DateFormat FORMAT_HEURE_MINUTE = new SimpleDateFormat("HH:mm");

    /**
     * Lire flag.
     *
     * @param value
     *            the value
     *
     * @return true, if successful
     */
    private static boolean lireFlag(final String value) {
        return "1".equals(value);
    }

    /**
     * Preparer principal.
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @param processus
     *            the processus
     *
     * @throws Exception
     *             the exception
     */
    public static void preparerPRINCIPAL(final InfoBean infoBean, final FicheUniv ficheUniv, final SaisieFiche processus) throws Exception {
        // JSS 2004-04-19 : délégation
        // controle droit modif et initialisation des droits pour la JSP
        final ServiceUser serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final ServiceEncadre serviceEncadre = ServiceManager.getServiceForBean(EncadreBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final HttpSession session = infoBean.getSessionHttp();
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        final AutorisationBean autorisations = (AutorisationBean) sessionUtilisateur.getInfos().get(SessionUtilisateur.AUTORISATIONS);
        final MetatagBean meta = MetatagUtils.lireMeta(ficheUniv);
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        // Controle des métas
        if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) {
            final List<String> differences = serviceMetatag.controlerCoherenceAvecFiche(meta, ficheUniv);
            if (!differences.isEmpty()) {
                String chaineDifferences = "";
                for (String difference : differences) {
                    chaineDifferences += "\n" + difference;
                }
                infoBean.set("GRS_CONTROLE_META", chaineDifferences);
            }
        }
        infoBean.set("GRS_AUTORISATION_SUPPRESSION", "0");
        infoBean.set("GRS_AUTORISATION_DUPLICATION", "0");
        infoBean.set("GRS_AUTORISATION_TRADUCTION", "0");
        infoBean.set("GRS_AUTORISATION_VALIDATION", "0");
        if (autorisations != null) {
            if (autorisations.estAutoriseASupprimerLaFiche(ficheUniv)) {
                infoBean.set("GRS_AUTORISATION_SUPPRESSION", "1");
            }
            if (autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_CREATION)) {
                infoBean.set("GRS_AUTORISATION_DUPLICATION", "1");
            }
            if (autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_TRADUCTION)) {
                infoBean.set("GRS_AUTORISATION_TRADUCTION", "1");
            }
            // RP20050811 on test maintenant le droit d'approbation sur le niveau courant
            // revient à tester le droit en validation classique si pas de niveaux intermédiaires
            if (autorisations.possedeAutorisationValidation(ficheUniv, meta.getMetaNiveauApprobation())) {
                infoBean.set("GRS_AUTORISATION_VALIDATION", "1");
            }
        }
        //sur l'action de changement d'onglet, on mémorise l'action d'origine s'il s'agit de duplication ou traduction
        if (infoBean.getActionUtilisateur() != null && (infoBean.getActionUtilisateur().equals(InfoBean.ACTION_TRADUIRE) || infoBean.getActionUtilisateur().equals(InfoBean.ACTION_DUPLIQUER))) {
            //on mémorise l'action de duplication ou de traduction pour restituer le bon mode d'affichage des champs : code modifiable ou non, champ modifiable ou non
            infoBean.set("PREMIERE_ACTION", infoBean.getActionUtilisateur());
        } else if (!InfoBean.ACTION_ONGLET.equals(infoBean.getActionUtilisateur())) {
            infoBean.remove("PREMIERE_ACTION");
        }
        // JSS 2004-04-19 : délégation (fin)
        //JSS20050301 : sous_onglet à vide pour éviter les plantages
        // dans les JSP
        if (infoBean.get("SOUS_ONGLET") == null) {
            infoBean.set("SOUS_ONGLET", "");
        }
        if (infoBean.get("GRS_SAISIE_REFERENCEMENT") == null) {
            infoBean.set("GRS_SAISIE_REFERENCEMENT", "1");
        }
        if (infoBean.get("GRS_SAISIE_MODE_BROUILLON") == null) {
            infoBean.set("GRS_SAISIE_MODE_BROUILLON", "1");
        }
        if (infoBean.get("GRS_SAISIE_ENCADRES") == null) {
            infoBean.set("GRS_SAISIE_ENCADRES", "1");
        }
        // BSE KSupXML
        if (infoBean.get("GRS_APERCU") == null) {
            infoBean.set("GRS_APERCU", "1");
        }
        infoBean.set("DATE_ALERTE", ficheUniv.getDateAlerte());
        infoBean.set("MESSAGE_ALERTE", ficheUniv.getMessageAlerte());
        infoBean.set("META_KEYWORDS", ficheUniv.getMetaKeywords());
        infoBean.set("META_DESCRIPTION", ficheUniv.getMetaDescription());
        infoBean.set("TITRE_ENCADRE", ficheUniv.getTitreEncadre());
        infoBean.set("CONTENU_ENCADRE", ficheUniv.getContenuEncadre());
        infoBean.set("LISTE_ENCADRES_RECHERCHE", serviceEncadre.getEncadreSearchList());
        infoBean.set("ENCADRE_RECHERCHE", ficheUniv.getEncadreRecherche());
        infoBean.set("ENCADRE_RECHERCHE_BIS", ficheUniv.getEncadreRechercheBis());
        infoBean.set("DATE_CREATION", ficheUniv.getDateCreation());
        infoBean.set("DATE_PROPOSITION", ficheUniv.getDateProposition());
        infoBean.set("DATE_VALIDATION", ficheUniv.getDateValidation());
        infoBean.set("CODE_REDACTEUR", ficheUniv.getCodeRedacteur());
        infoBean.set("LIBELLE_CODE_REDACTEUR", serviceUser.getLibelle(ficheUniv.getCodeRedacteur()));
        if (ServiceUser.UTILISATEUR_ANONYME.equals(ficheUniv.getCodeRedacteur())) {
            infoBean.set("LIBELLE_CODE_REDACTEUR", "(anonyme) " + meta.getMetaMailAnonyme());
        }
        if (autorisations != null) {
            autorisations.setRedacteurFicheCourante(false);
            if (!"".equals(ficheUniv.getCodeRedacteur()) && !ServiceUser.UTILISATEUR_ANONYME.equals(ficheUniv.getCodeRedacteur()) && ficheUniv.getCodeRedacteur().equals(sessionUtilisateur.getInfos().get(SessionUtilisateur.CODE))) {
                autorisations.setRedacteurFicheCourante(true);
            }
        }
        infoBean.set("CODE_VALIDATION", ficheUniv.getCodeValidation());
        infoBean.set("ETAT_OBJET", ficheUniv.getEtatObjet());
        infoBean.set("AUTRES_ETATS", null);
        final FicheUniv ficheEtats = ficheUniv.getClass().newInstance();
        ficheEtats.init();
        ficheEtats.setCtx(processus);
        if (ficheEtats.select(" WHERE CODE='" + ficheUniv.getCode() + "' AND ETAT_OBJET NOT IN ('" + ficheUniv.getEtatObjet() + "') ORDER BY LANGUE, ETAT_OBJET DESC") > 0) {
            final Hashtable<String, String> etats = new Hashtable<>();
            while (ficheEtats.nextItem()) {
                etats.put(ficheEtats.getEtatObjet(), ficheEtats.getIdFiche().toString());
            }
            if (etats.size() > 0) {
                infoBean.set("AUTRES_ETATS", etats);
            }
        }
        // Préparation de la liste des langues pour la traduction
        String code = ficheUniv.getCode();
        infoBean.set("CODE", code);
        // RP 20060223 ajout d'un code temporaire si le code de la fiche est vide ( création )
        if ("".equals(code)) {
            code = "TS" + System.currentTimeMillis();
        }
        infoBean.set("TS_CODE", code);
        infoBean.set("LANGUE", ficheUniv.getLangue());
        /* initialisation langues disponibles pour la traduction (uniquement en BO) */
        final Hashtable<String, String> listeAutresLangues = new Hashtable<>();
        final Hashtable<String, String> listeLangues = LangueUtil.getListeLangues(processus.getLocale());
        infoBean.set("LISTE_LANGUES", listeLangues);
        if (infoBean.get("SAISIE_FRONT") == null && !InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
            final FicheUniv ficheUniv2 = ficheUniv.getClass().newInstance();
            ficheUniv2.setCtx(processus);
            ficheUniv2.init();
            for (final Enumeration<String> v = listeLangues.keys(); v.hasMoreElements(); ) {
                String key = v.nextElement();
                if (ficheUniv2.selectCodeLangueEtat(ficheUniv.getCode(), key, "") == 0) {
                    // l'objet n'est pas défini dans la langue
                    listeAutresLangues.put(key, listeLangues.get(key));
                }
            }
        }
        infoBean.set("LISTE_AUTRES_LANGUES", listeAutresLangues);
        infoBean.set("DEMANDE_VALIDATION", "0");
        infoBean.set("MISE_EN_LIGNE", "0");
        infoBean.set("REMPLACER_VERSION", "0");
        infoBean.set("RETOUR_AUTEUR", "0");
        infoBean.set("AFFICHER_REMPLACER_VERSION", "0");
        infoBean.set("MODE_BROUILLON", "0");
        // Mode brouillon prévalorisé si modif sur brouillon seulement pour le back office (pas de persistance du mode brouillon en front)
        if ((infoBean.get("SAISIE_FRONT") == null && (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) || (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet()) && "1".equals(PropertyHelper.getCoreProperty("saisie.etat_creation.brouillon"))))) && "0001".equals(ficheUniv.getEtatObjet())) {
            infoBean.set("MODE_BROUILLON", "1");
        }
        /*
        Dans le cas d'une fiche en ligne pour laquelle
        il existe une version de travail, on affiche une
        combo pour confirmer l'écrasement de la version de
        travail
         */
        final EtatFiche etatObjetInfoBean = EtatFiche.getEtatParCode(infoBean.getString("ETAT_OBJET"));
        if (EtatFiche.EN_LIGNE.getEtat().equals(ficheUniv.getEtatObjet()) && FicheUnivMgr.existeVersionTravail(ficheUniv)) {
            infoBean.set("AFFICHER_REMPLACER_VERSION", "1");
        }
        /*
         * Dans le cas d'une fiche restaurable on affiche une popup s'il existe
         * deja une fiche a l'etat de brouillon
         */
        else if ((EtatFiche.A_SUPPRIMER.equals(etatObjetInfoBean) || EtatFiche.SAUVEGARDE_AUTO.equals(etatObjetInfoBean) || EtatFiche.ARCHIVE.equals(etatObjetInfoBean)) && FicheUnivMgr.existeVersionTravail(ficheUniv)) {
            infoBean.set("AFFICHER_POPUP_RESTAURATION", "1");
        }
        /*
         * RP 20050303
         * Pour les liens directs vers les fiches à valider, on vérifie si la validation
         * n'a pas été annulée entre temps sinon on en informe le validateur.
         */
        if (infoBean.get("LIEN_DIRECT") != null && !"0002".equals(ficheUniv.getEtatObjet())) {
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_PLUS_ETAT_VALIDATION"));
        }
        /* Version en ligne et il existe une fiche à valider
        Si je n'ai pas le droit de valider,
        je ne peux rien faire -> affichage d'une erreur
         */
        if ("0003".equals(ficheUniv.getEtatObjet()) && ficheUniv.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "0002") > 0) {
            final boolean estValidateur = autorisations != null && autorisations.getAutorisation(infoBean.getString("CODE_OBJET"), AutorisationBean.INDICE_VALIDATION);
            if (!estValidateur) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_VALIDATION_DOUBLON"));
            }
        }
        // RP20050820 pour une fiche à valider on renseigne :
        // le niveau d'approbation courant et son libelle
        infoBean.set("AFFICHER_CHOISIR_VALIDATEUR", "1");
        infoBean.set("GRS_SAISIE_WORKFLOW", "0");
        String niveauApprobation = "";
        String libelleApprobation = "";
        if ("1".equals(PropertyHelper.getCoreProperty("validation_multi_niveaux.activation"))) {
            infoBean.set("GRS_SAISIE_WORKFLOW", "1");
            if ("0002".equals(ficheUniv.getEtatObjet())) {
                niveauApprobation = meta.getMetaNiveauApprobation();
                // si pas de niveau d'approbation on est en validation classique
                if (niveauApprobation.length() > 0) {
                    libelleApprobation = AutorisationBean.getListeNiveauxApprobation().get(niveauApprobation);
                } else {
                    infoBean.set("AFFICHER_CHOISIR_VALIDATEUR", "0");
                }
            }
        }
        infoBean.set("LIBELLE_APPROBATION", libelleApprobation);
        infoBean.set("NIVEAU_APPROBATION", niveauApprobation);
        final String activationDsi = StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty("dsi.activation"), "0");
        infoBean.set("GRS_SAISIE_COLLABORATIF", "0");
        infoBean.set("GRS_SAISIE_DIFFUSION", "0");
        if (("1".equals(activationDsi)) && (ficheUniv instanceof DiffusionSelective)) {
            // JSS 20060103 : on ajoute le cas de l'espace
            if (autorisations != null && (autorisations.isDiffuseurDSI() || infoBean.get("SAISIE_FRONT") != null || infoBean.get("ESPACE") != null)) {
                infoBean.set("GRS_SAISIE_DIFFUSION", "1");
                final DiffusionSelective ficheDiffusion = (DiffusionSelective) ficheUniv;
                formaterPublicViseDsiDansInfoBean(infoBean, processus, ficheDiffusion.getDiffusionPublicVise(), "PUBLIC_VISE_DSI");
                infoBean.set("DIFFUSION_MODE_RESTRICTION", ficheDiffusion.getDiffusionModeRestriction());
                // JSS 20040409 : la valeur '4' correspond à un espace collaboratif
                if (!"4".equals(infoBean.get("DIFFUSION_MODE_RESTRICTION"))) {
                    formaterPublicViseDsiDansInfoBean(infoBean, processus, ficheDiffusion.getDiffusionPublicViseRestriction(), "PUBLIC_VISE_DSI_RESTRICTION");
                } else {
                    infoBean.set("PUBLIC_VISE_ESPACE", ficheDiffusion.getDiffusionPublicViseRestriction());
                    infoBean.set("PUBLIC_VISE_DSI_RESTRICTION", "");
                }
                if (Espacecollaboratif.isExtensionActivated()) {
                    if (FicheUnivMgr.isFicheCollaborative(ficheUniv)) {
                        infoBean.set("GRS_SAISIE_COLLABORATIF", "1");
                    }
                    // avec la mémorisation du composant d'information il faut prévaloriser la saisie dans un espace collaboratif
                    if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet()) && infoBean.get("ESPACE") != null) {
                        infoBean.set("DIFFUSION_MODE_RESTRICTION", "4");
                        infoBean.set("PUBLIC_VISE_ESPACE", infoBean.get("ESPACE"));
                    }
                }
            }
        }
        /* Date dernière modif */
        infoBean.set("DATE_MODIFICATION", ficheUniv.getDateModification());
        /* Message d'avertissement si il existe une autre version
        plus récente */
        infoBean.set("EXISTE_VERSION_PLUS_RECENTE", "0");
        final String objet = ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET"));
        final FicheUniv ficheUniv2 = ReferentielObjets.instancierFiche(objet);
        ficheUniv2.setCtx(processus);
        ficheUniv2.init();
        // Date de la fiche courante
        // RP20051123 Ajout des dates de gestion de la fiche dans metatag
        if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
            infoBean.set("DATE_MISE_EN_LIGNE", new Date(System.currentTimeMillis()));
            infoBean.set("HEURE_MISE_EN_LIGNE", "00:00");
            infoBean.set("GRS_SAUVEGARDE_DATE_MISE_EN_LIGNE", new Date(System.currentTimeMillis()));
        } else {
            //En base la date et l'heure sont stockés dans la meme colonne, à l'affichage on sépare en 2 champs
            final Date dateHeureMiseEnLigne = meta.getMetaDateMiseEnLigne();
            infoBean.set("DATE_MISE_EN_LIGNE", dateHeureMiseEnLigne);
            infoBean.set("HEURE_MISE_EN_LIGNE", ((DateFormat) FORMAT_HEURE_MINUTE.clone()).format(dateHeureMiseEnLigne));
            infoBean.set("GRS_SAUVEGARDE_DATE_MISE_EN_LIGNE", meta.getMetaDateMiseEnLigne());
        }
        // JSS 20031021-001 ARCHIVAGE
        infoBean.set("DATE_ARCHIVAGE", meta.getMetaDateArchivage());
        infoBean.set("GRS_SAUVEGARDE_DATE_ARCHIVAGE", meta.getMetaDateArchivage());
        // Prévalorisation date d'archivage pour tous les objets
        if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet()) || "DUPLIQUER".equals(infoBean.get("PREMIERE_ACTION"))) {
            //par défaut, on désactive l'archivage automatique
            int archivage = -1;
            final String nomobjet = ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName()));
            if (PropertyHelper.getExtensionProperty(infoBean.getNomExtension(), "fiche." + nomobjet.toUpperCase() + ".archivage") != null) {
                try {
                    archivage = Integer.parseInt(PropertyHelper.getExtensionProperty(infoBean.getNomExtension(), "fiche." + nomobjet.toUpperCase() + ".archivage"));
                } catch (final NumberFormatException e) {
                    LOG.debug("unable to parse the given value", e);
                }
            }
            // AA : si on met -1, on désactive ce paramètre
            if (archivage != -1) {
                // on ajoute le nombre de mois souhaités à la date du jour
                final GregorianCalendar aujourdhui = new GregorianCalendar();
                aujourdhui.add(Calendar.MONTH, archivage);
                final Date dateArchivage = new Date(aujourdhui.getTime().getTime());
                infoBean.set("DATE_ARCHIVAGE", dateArchivage);
                // La zone sera considérée comme non modifiée donc pas de controle sur TRAITER_PRINCIPAL
                infoBean.set("GRS_SAUVEGARDE_DATE_ARCHIVAGE", dateArchivage);
            }
        }
        infoBean.set("DATE_SUPPRESSION", meta.getMetaDateSuppression());
        infoBean.set("GRS_SAUVEGARDE_DATE_SUPPRESSION", meta.getMetaDateSuppression());
        // Prévalorisation date suppression pour tous les objets
        if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet()) || "DUPLIQUER".equals(infoBean.get("PREMIERE_ACTION"))) {
            //par défaut, on désactive la suppression automatique
            int suppression = -1;
            final String nomobjet = ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName()));
            if (PropertyHelper.getExtensionProperty(infoBean.getNomExtension(), "fiche." + nomobjet.toUpperCase() + ".suppression") != null) {
                try {
                    suppression = Integer.parseInt(PropertyHelper.getExtensionProperty(infoBean.getNomExtension(), "fiche." + nomobjet.toUpperCase() + ".suppression"));
                } catch (final NumberFormatException e) {
                    LOG.debug("unable to parse the given value", e);
                }
            }
            // AA : si on met -1, on désactive ce paramètre
            if (suppression != -1) {
                // on ajoute le nombre de mois souhaités à la date du jour
                final GregorianCalendar aujourdhui = new GregorianCalendar();
                aujourdhui.add(Calendar.MONTH, suppression);
                final Date dateSuppression = new Date(aujourdhui.getTime().getTime());
                infoBean.set("DATE_SUPPRESSION", dateSuppression);
                // La zone sera considérée comme non modifiée donc pas de controle sur TRAITER_PRINCIPAL
                infoBean.set("GRS_SAUVEGARDE_DATE_SUPPRESSION", dateSuppression);
            }
        }
        infoBean.set("DATE_RUBRIQUAGE", meta.getMetaDateRubriquage());
        infoBean.set("CODE_RUBRIQUAGE", meta.getMetaCodeRubriquage());
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(meta.getMetaCodeRubriquage());
        if (rubriqueBean != null) {
            infoBean.set("LIBELLE_CODE_RUBRIQUAGE", rubriqueBean.getIntitule());
        }
        infoBean.set("GRS_SAUVEGARDE_DATE_RUBRIQUAGE", meta.getMetaDateRubriquage());
        infoBean.set("GRS_SAUVEGARDE_CODE_RUBRIQUAGE", meta.getMetaCodeRubriquage());
        // RP20060130 fil d'ariane sur les fiches
        final String codeRubrique = ficheUniv.getCodeRubrique();
        final String filAriane = serviceRubrique.getLabelWithAscendantsLabels(codeRubrique);
        infoBean.set("FIL_ARIANE", filAriane);
        final Date dateFiche = serviceMetatag.getLastModificationDate(meta);
        if (dateFiche != null && ficheUniv.getCode().length() > 0 && ficheUniv2.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "") > 0) {
            while (ficheUniv2.nextItem()) {
                // On vérifie que ce n'est pas la fiche courante
                if (!ficheUniv2.getIdFiche().equals(ficheUniv.getIdFiche())) {
                    // On conserve les fiches en brouillon, attente validation et en ligne
                    if ("0001".equals(ficheUniv.getEtatObjet()) || "0002".equals(ficheUniv.getEtatObjet()) || "0003".equals(ficheUniv.getEtatObjet())) {
                        // Lecture métatag autre fiche
                        // JSS20050126 : correction > il faut utiliser meta2 et pas écraser meta
                        final MetatagBean meta2 = MetatagUtils.lireMeta(infoBean.getString("CODE_OBJET"), ficheUniv2.getIdFiche());
                        final Date dateAutreFiche = serviceMetatag.getLastModificationDate(meta2);
                        if ((dateAutreFiche != null) && (dateAutreFiche.after(dateFiche))) {
                            infoBean.set("EXISTE_VERSION_PLUS_RECENTE", "1");
                        }
                    }
                }
            }
        }
        /* VIN 041206
         *  Refonte des structures
         * pour les fiches implementant StructureModele preparation de la case a cocher
         * "visible dans l'arbre"
         */
        if (ficheUniv instanceof StructureModele) {
            //on dit si la case a cocher doit etre affichee ou non
            infoBean.set("IN_TREE_ACTIF", true);
            //on donne la valeur initial de la case a cocher
            if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                infoBean.set("IN_TREE", "1");
            } else {
                infoBean.set("IN_TREE", meta.getMetaInTree());
            }
        }
        // Nom des attributs pour rubrique et structure
        final String nomColonneStructure = "CODE_RATTACHEMENT";
        final String nomColonneRubrique = "CODE_RUBRIQUE";
        String urlFicheModification = "";
        // structure prévalorisée
        if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
            if (infoBean.get("PREV_CODE_RATTACHEMENT") != null) {
                infoBean.set(nomColonneStructure, infoBean.getString("PREV_CODE_RATTACHEMENT"));
                infoBean.set("LIBELLE_" + nomColonneStructure, serviceStructure.getDisplayableLabel(infoBean.getString("PREV_CODE_RATTACHEMENT"), ficheUniv.getLangue()));
                urlFicheModification += "&PREV_CODE_RATTACHEMENT=" + infoBean.getString("PREV_CODE_RATTACHEMENT");
            } else if (infoBean.get("SAISIE_FRONT") != null) {
                infoBean.set(nomColonneStructure, "");
                infoBean.set("LIBELLE_" + nomColonneStructure, "");
            }
        } else if (infoBean.get("SAISIE_FRONT") != null) {
            infoBean.set(nomColonneStructure, ficheUniv.getCodeRattachement());
            infoBean.set("LIBELLE_" + nomColonneStructure, serviceStructure.getDisplayableLabel(ficheUniv.getCodeRattachement(), ficheUniv.getLangue()));
        }
        // rubrique prévalorisée
        if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
            if (infoBean.get("PREV_CODE_RUBRIQUE") != null) {
                final RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(infoBean.get("PREV_CODE_RUBRIQUE", String.class));
                if (rubriqueCourante != null) {
                    infoBean.set(nomColonneRubrique, rubriqueCourante.getCode());
                    String titre = rubriqueCourante.getIntitule();
                    if (titre.contains("#")) {
                        titre = titre.substring(titre.indexOf("#") + 1);
                    }
                    infoBean.set("LIBELLE_" + nomColonneRubrique, titre);
                }
                urlFicheModification += "&PREV_CODE_RUBRIQUE=" + infoBean.getString("PREV_CODE_RUBRIQUE");
            } else if (infoBean.get("SAISIE_FRONT") != null) {
                infoBean.set(nomColonneRubrique, "");
                infoBean.set("LIBELLE_" + nomColonneRubrique, "");
            }
        } else if (infoBean.get("SAISIE_FRONT") != null) {
            infoBean.set(nomColonneRubrique, ficheUniv.getCodeRubrique());
            final RubriqueBean rubriqueFiche = serviceRubrique.getRubriqueByCode(ficheUniv.getCodeRubrique());
            if (rubriqueFiche != null) {
                infoBean.set("LIBELLE_" + nomColonneRubrique, rubriqueFiche.getIntitule());
            }
        }
        //public vise prévalorisé
        if (ficheUniv instanceof DiffusionSelective && InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet()) && infoBean.get("ESPACE") == null) {
            if (infoBean.get("PREV_DIFFUSION_PUBLIC_VISE") != null) {
                formaterPublicViseDsiDansInfoBean(infoBean, processus, infoBean.getString("PREV_DIFFUSION_PUBLIC_VISE"), "PUBLIC_VISE_DSI");
                infoBean.set("DIFFUSION_MODE_RESTRICTION", "0");
                urlFicheModification += "&PREV_DIFFUSION_PUBLIC_VISE=" + infoBean.getString("PREV_DIFFUSION_PUBLIC_VISE");
            } else if (infoBean.get("SAISIE_FRONT") != null) {
                infoBean.set("PUBLIC_VISE_DSI", "");
                infoBean.set("LIBELLE_PUBLIC_VISE_DSI", "");
                infoBean.set("DIFFUSION_MODE_RESTRICTION", "0");
            }
        }
        // prévalorisation du périmètre de la fiche en saisie front hors collaboratif
        if (infoBean.get("SAISIE_FRONT") != null) {
            if (autorisations != null && autorisations.possedeModeExpert()) {
                if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                    if (!(ficheUniv instanceof FicheObjet)) {
                        urlFicheModification = WebAppUtil.SG_PATH + "?ACTION=AJOUTER" + urlFicheModification;
                    }
                } else {
                    urlFicheModification = WebAppUtil.SG_PATH + "?ACTION=MODIFIER&amp;ID_FICHE=" + ficheUniv.getIdFiche();
                }
                if (StringUtils.isNotBlank(urlFicheModification)) {
                    urlFicheModification += "&amp;EXT=" + ReferentielObjets.getExtension(infoBean.getString("CODE_OBJET")) + "&amp;PROC=SAISIE_" + ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")).toUpperCase();
                    urlFicheModification = "href=\"#\" onclick=\"window.open('" + URLResolver.getAbsoluteUrl(urlFicheModification, processus) + "','_blank','');";
                    if (infoBean.getString("URL_REDIRECT") != null && infoBean.getString("URL_REDIRECT").length() > 0) {
                        urlFicheModification += "window.location.href='" + infoBean.getString("URL_REDIRECT") + "';\"";
                    } else {
                        urlFicheModification += "window.history.back();\"";
                    }
                    infoBean.set("GRS_URL_MODIFICATION_EXPERT", urlFicheModification);
                }
            }
            if ("1".equals(infoBean.getString("GRS_AUTORISATION_SUPPRESSION")) && InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {
                String urlFicheSuppression = WebAppUtil.SG_PATH + "?EXT=" + ReferentielObjets.getExtension(infoBean.getString("CODE_OBJET")) + "&amp;ACTION=SUPPRIMER&amp;SAISIE_FRONT=TRUE&amp;TYPE_FICHE=" + ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")).toUpperCase();
                urlFicheSuppression += "&amp;ID_FICHE=" + ficheUniv.getIdFiche() + "&amp;PROC=SAISIE_" + ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")).toUpperCase() + "_FRONT";
                if (infoBean.get("ESPACE") != null) {
                    urlFicheSuppression += "&amp;ESPACE=" + infoBean.getString("ESPACE");
                }
                if (infoBean.get("RH") != null) {
                    urlFicheSuppression += "&amp;RH=" + infoBean.getString("RH");
                }
                if (infoBean.get("LANGUE") != null) {
                    urlFicheSuppression += "&amp;LANGUE=" + infoBean.getString("LANGUE");
                }
                infoBean.set("GRS_URL_SUPPRESSION", urlFicheSuppression);
            }
            if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                infoBean.set("MAIL_ANONYME", "");
            } else {
                infoBean.set("MAIL_ANONYME", meta.getMetaMailAnonyme());
            }
        }
        // point d'extension pour des traitements préparatoires sur plugin de contenu
        PluginFicheHelper.preparerPrincipal(infoBean, ficheUniv, meta);
        // valorisation de l'utilisation du document normal ou uniquement pour fichiers techniques
        if (StringUtils.isNotEmpty(meta.getMetaDocumentFichiergw())) {
            if ("1".equals(meta.getMetaDocumentFichiergw())) {
                infoBean.set("DOCUMENT_FICHIERGW", "1");
            } else {
                infoBean.set("DOCUMENT_FICHIERGW", "0");
            }
        }
        // avant d'affecter des valeurs par défaut, on regarde si des valeurs ne sont pas déjà valorisées dans le processus
        boolean initProcessus = false;
        if ((infoBean.get(nomColonneRubrique) != null) && (infoBean.getString(nomColonneRubrique).length()) > 0) {
            initProcessus = true;
        }
        if ((infoBean.getString(nomColonneStructure) != null) && (infoBean.getString(nomColonneStructure).length() > 0)) {
            initProcessus = true;
        }
        String structureParDefaut = "";
        String rubriqueParDefaut = "";
        if (!initProcessus && (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) && (autorisations != null)) {
            // modification sur la prévalorisation de la structure et la rubrique en fonction du périmètre de l'utilisateur
            // (elle est prioritaire par rapport aux rubriques)
            // On sélectionne la rubrique de + haut niveau
            // Pour cela, on balaie tous les périmètres
            // On sélectionne la structure de plus haut niveau
            // On sélectionne par défaut la structure de l'utilisateur
            int niveauRubriqueCourante = 10;
            final Vector<Perimetre> listePerimetres = autorisations.getListePerimetres(new PermissionBean("FICHE", ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName()), "C"));
            final Enumeration<Perimetre> e = listePerimetres.elements();
            while (e.hasMoreElements()) {
                final Perimetre perimetreCourant = e.nextElement();
                // Rubrique de plus haut niveau
                if ("".equals(perimetreCourant.getCodeEspaceCollaboratif()) && perimetreCourant.getCodeRubrique().length() > 0) {
                    final RubriqueBean rubriquePerimetre = serviceRubrique.getRubriqueByCode(perimetreCourant.getCodeRubrique());
                    if (rubriquePerimetre != null) {
                        final int niveauRubrique = serviceRubrique.getLevel(rubriquePerimetre);
                        if (niveauRubrique < niveauRubriqueCourante) {
                            niveauRubriqueCourante = niveauRubrique;
                            rubriqueParDefaut = perimetreCourant.getCodeRubrique();
                            structureParDefaut = perimetreCourant.getCodeStructure();
                        }
                    }
                }
            }
            if (rubriqueParDefaut.length() > 0 && !"-".equals(rubriqueParDefaut)) {
                infoBean.set(nomColonneRubrique, rubriqueParDefaut);
                final RubriqueBean rubriqueBeanParDefaut = serviceRubrique.getRubriqueByCode(rubriqueParDefaut);
                if (rubriqueBeanParDefaut != null) {
                    infoBean.set("LIBELLE_" + nomColonneRubrique, rubriqueBeanParDefaut.getIntitule());
                }
            }
            if ("".equals(structureParDefaut) || "-".equals(structureParDefaut)) {
                // On sélectionne la structure de l'utilisateur
                final String structureUtilisateur = autorisations.getCodeStructure();
                if (structureUtilisateur.length() > 0) {
                    // On regarde si l'autorisation est compatible avec la rubrique de plus haut niveau
                    if (autorisations.possedePermission(new PermissionBean("FICHE", ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName()), "C"), new Perimetre(structureUtilisateur, rubriqueParDefaut, "*", "*", ""))) {
                        structureParDefaut = structureUtilisateur;
                    }
                }
            }
            if (structureParDefaut.length() > 0 && !"-".equals(structureParDefaut)) {
                infoBean.set(nomColonneStructure, structureParDefaut);
                infoBean.set("LIBELLE_" + nomColonneStructure, serviceStructure.getDisplayableLabel(structureParDefaut, "0"));
            }
        }
        // Affichage des restrictions dans l'arbre des rubriques et structures
        // Utilisé dans les fonctions UnivFmt.insererSaisieZone rubrique et structure
        infoBean.set("GRS_PERMISSION_TYPE", "FICHE");
        infoBean.set("GRS_PERMISSION_OBJET", infoBean.getString("GRS_CODE_OBJET"));
        infoBean.set("GRS_PERMISSION_ACTION", "C");
        if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
            infoBean.set("GRS_PERMISSION_ACTION", "C");
        }
        if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) {
            infoBean.set("GRS_PERMISSION_ACTION", "M");
        }
        // sauvegarde des valeurs avant modification
        infoBean.set("GRS_SAUVEGARDE_CODE_RUBRIQUE", infoBean.get(nomColonneRubrique));
        infoBean.set("NOM_CODE_RUBRIQUE", nomColonneRubrique);
        infoBean.set("GRS_SAUVEGARDE_CODE_RATTACHEMENT", infoBean.get(nomColonneStructure));
        infoBean.set("NOM_CODE_RATTACHEMENT", nomColonneStructure);
        // structures secondaires
        final String nomColonneRattachementAutres = "CODE_RATTACHEMENT_AUTRES";
        if (infoBean.get(nomColonneRattachementAutres) != null) {
            infoBean.set("GRS_SAUVEGARDE_CODE_RATTACHEMENT_AUTRES", infoBean.get(nomColonneRattachementAutres));
            infoBean.set("NOM_CODE_RATTACHEMENT_AUTRES", nomColonneRattachementAutres);
        }
        if (infoBean.get("PUBLIC_VISE_DSI") != null) {
            infoBean.set("GRS_SAUVEGARDE_PUBLIC_VISE_ESPACE", infoBean.get("PUBLIC_VISE_ESPACE"));
        } else {
            infoBean.set("GRS_SAUVEGARDE_PUBLIC_VISE_ESPACE", "");
        }
        // JSS20050126 : push multi-sites
        // On regarde si l'utilisateur a un droit de publication
        infoBean.set("GRS_SAISIE_PUBLICATION", "0");
        if (autorisations != null) {
            final Vector<Perimetre> listePerimetresPublication = autorisations.getListePerimetres(new PermissionBean("TECH", "pub", ""));
            if (listePerimetresPublication.size() > 0) {
                infoBean.set("GRS_SAISIE_PUBLICATION", "1");
                // On va systématiquement chercher les rubriques de publication
                // ( elles constituent la référence)
                Collection<String> rubPub = new ArrayList<>();
                //la table RUBRIQUEPUBLICATION n'est valable que pour les fiches en état EN LIGNE (cf ticket 4105)
                if ("0003".equals(ficheUniv.getEtatObjet())) {
                    final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
                    rubPub = serviceRubriquePublication.getRubriqueDestByFicheWithSourceControl(ficheUniv);
                }
                // Et on rajoute les rubriques saisies au niveau des méta-tags
                final Vector<String> rubriquesMeta = Chaine.getVecteurPointsVirgules(meta.getMetaRubriquesPublication());
                final Enumeration<String> eMeta = rubriquesMeta.elements();
                while (eMeta.hasMoreElements()) {
                    final String codeMeta = eMeta.nextElement();
                    if (!rubPub.contains(codeMeta)) {
                        rubPub.add(codeMeta);
                    }
                }
                // Recopie des codes et des libelles dans le composant d'information
                String codesRubriquesPublication = "";
                String libellesRubriquesPublication = "";
                String codesRubriquesPublicationAutomatiques = "";
                for (String codeRubPub : rubPub) {
                    String libelleAuto = "";
                    String codeRubAuto = codeRubPub;
                    if (codesRubriquesPublication.length() > 0) {
                        codesRubriquesPublication += ";";
                        libellesRubriquesPublication += ";";
                    }
                    // sauvegarde des rubriques automatiques de publication
                    if (codeRubAuto.startsWith("#AUTO#")) {
                        if (codesRubriquesPublicationAutomatiques.length() > 0) {
                            codesRubriquesPublicationAutomatiques += ";";
                        }
                        codeRubPub = codeRubAuto.substring(6);
                        codesRubriquesPublicationAutomatiques += codeRubAuto;
                        libelleAuto = " (automatique)";
                    }
                    final RubriqueBean rubriquePub = serviceRubrique.getRubriqueByCode(codeRubPub);

                    String libelleRubPub = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
                    if (rubriquePub != null) {
                        libelleRubPub = rubriquePub.getIntitule();
                    }
                    codesRubriquesPublication += codeRubAuto;
                    libellesRubriquesPublication += libelleRubPub + libelleAuto;
                }
                // sauvegarde des rubriques de publication
                infoBean.set("RUBRIQUE_PUBLICATION_AUTOMATIQUE", codesRubriquesPublicationAutomatiques);
                infoBean.set("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION", codesRubriquesPublication);
                infoBean.set("GRS_SAUVEGARDE_LIBELLE_RUBRIQUE_PUBLICATION", libellesRubriquesPublication);
                infoBean.set("RUBRIQUE_PUBLICATION", codesRubriquesPublication);
                infoBean.set("LIBELLE_RUBRIQUE_PUBLICATION", libellesRubriquesPublication);
            }
        }
        // RP20060515 Gestion des autres langues
        infoBean.set("FICHES_AUTRE_LANGUE", null);
        if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {
            final Hashtable<String, String> hLangue = LangueUtil.getListeLangues(processus.getLocale());
            hLangue.remove(ficheUniv.getLangue());
            if (hLangue.size() > 0) {
                final FicheUniv fiche2 = ficheUniv.getClass().newInstance();
                fiche2.init();
                fiche2.setCtx(processus);
                if (fiche2.select(" WHERE CODE='" + ficheUniv.getCode() + "' AND ETAT_OBJET NOT IN ('0005','0006') ORDER BY LANGUE, ETAT_OBJET DESC") > 1) {
                    final TreeMap<String, String> autresLangues = new TreeMap<>();
                    String etatObjetFicheEnregistree = "";
                    while (fiche2.nextItem()) {
                        if (hLangue.containsKey(fiche2.getLangue()) && autorisations != null && autorisations.estAutoriseAModifierLaFiche(fiche2) && (autresLangues.get(fiche2.getLangue()) == null || (autresLangues.get(fiche2.getLangue()) != null && "0004".equals(etatObjetFicheEnregistree)))) {
                            final String extension = ReferentielObjets.getExtension(ReferentielObjets.getCodeObjet(fiche2));
                            final String nomProcessus = "SAISIE_" + ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")).toUpperCase();
                            final String[][] parametresURL = new String[][] {{"ID_FICHE", fiche2.getIdFiche().toString()}};
                            final String urlFiche = ProcessusHelper.getUrlProcessAction(infoBean, extension, nomProcessus, "MODIFIER", parametresURL);
                            autresLangues.put(fiche2.getLangue(), urlFiche);
                            etatObjetFicheEnregistree = fiche2.getEtatObjet();
                        }
                    }
                    if (autresLangues.size() > 0) {
                        infoBean.set("FICHES_AUTRE_LANGUE", autresLangues);
                    }
                }
            }
        }
        setMessagesAlerte(infoBean);
    }

    /**
     * Initialise les messages d'alertes à afficher sur la saisie de fiche
     *
     * @param infoBean
     *            l'infoBean contient les infos necessaires pour savoir si oui ou non on a un message d'alerte et lequel
     */
    private static void setMessagesAlerte(final InfoBean infoBean) {
        if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && EtatFiche.SAUVEGARDE_AUTO.getEtat().equals(infoBean.get("ETAT_OBJET"))) {
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_SAUVEGARDE_AUTO_APERCU"));
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_RESTAURATION"));
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_SUPPRESSION_POSSIBLE"));
        } else if ("1".equals(infoBean.getString("GRS_AUTORISATION_SUPPRESSION")) && EtatFiche.A_SUPPRIMER.getEtat().equals(infoBean.get("ETAT_OBJET"))) {
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_A_SUPPRIMER"));
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_RESTAURATION"));
        } else if ("1".equals(infoBean.getString("GRS_AUTORISATION_SUPPRESSION")) && EtatFiche.ARCHIVE.getEtat().equals(infoBean.get("ETAT_OBJET"))) {
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_ARCHIVE"));
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_RESTAURATION"));
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_SUPPRESSION_POSSIBLE"));
        }
        if (infoBean.get("GRS_CONTROLE_META") != null) {
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_META_DONNEES"));
        }
        if ("1".equals(infoBean.getString("EXISTE_VERSION_PLUS_RECENTE"))) {
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_ENCOURS_MODIFICATION"));
            infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_FICHE_VERSION_RECENTE"));
        }
    }

    /**
     * AM 02.2003 Modification : cf traitement "demande de validation"
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @param processus
     *            the processus
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public static String traiterPRINCIPAL(final InfoBean infoBean, final FicheUniv ficheUniv, final SaisieFiche processus) throws Exception {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        String ecranLogique = "CONFIRMATION";
        final String action = infoBean.getActionUtilisateur();
        if (infoBean.get("GRS_LIBELLE_COPIE") != null) {
            infoBean.remove("GRS_LIBELLE_COPIE");
        }
        final HttpSession session = infoBean.getSessionHttp();
        AutorisationBean autorisations = null;
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        if (sessionUtilisateur != null) {
            autorisations = (AutorisationBean) sessionUtilisateur.getInfos().get(SessionUtilisateur.AUTORISATIONS);
        }
        // Uniformisation de la saisie collaborative
        String codeEspaceCollaboratif = "";
        if (infoBean.get("ESPACE") != null && infoBean.getString("ESPACE").length() > 0) {
            codeEspaceCollaboratif = infoBean.getString("ESPACE");
        }

        MetatagBean meta = MetatagUtils.initMeta(ficheUniv);
        MetatagBean metaOld = MetatagUtils.lireMeta(ficheUniv);
        meta.setId(metaOld.getId());
        meta.setMetaHistorique(metaOld.getMetaHistorique());

        // point d'extension pour des traiter les actions prealables par les plugins de contenu
        // saisie notamment (agenda, parcours par ex)
        PluginFicheHelper.preTraiterPrincipal(infoBean, ficheUniv, meta);
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        // on ne fait rien si l'action ne correspond pas à une action K-PORTAL
        // car il s'agit d'une navigation à l'intérieur de la page
        if (!InfoBean.ACTION_SUPPRIMER.equals(action) && !"ARCHIVER".equals(action) && !"RESTAURER_ARCHIVE".equals(action) && !"RESTAURER_SAUVEGARDE".equals(action) && !InfoBean.ACTION_DUPLIQUER.equals(action) && !InfoBean.ACTION_TRADUIRE.equals(action) && !InfoBean.ACTION_ENREGISTRER.equals(action) && !"ANNULER_SUPPRESSION".equals(action)) {
            return "PRINCIPAL";
        }
        // Variable de test pour perimetre
        final String nomColonneStructure = "CODE_RATTACHEMENT";
        final String nomColonneRattachementAutres = "CODE_RATTACHEMENT_AUTRES";
        final String nomColonneRubrique = "CODE_RUBRIQUE";
        boolean modificationPerimetre = false;
        FicheUniv ficheEnLigneAvantTraitement = ficheUniv.getClass().newInstance();
        ficheEnLigneAvantTraitement.init();
        ficheEnLigneAvantTraitement.setCtx(processus);
        if (ficheEnLigneAvantTraitement.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "0003") > 0) {
            ficheEnLigneAvantTraitement.nextItem();
        } else {
            ficheEnLigneAvantTraitement = null;
        }
        boolean miseEnLigne = false;
        if (!InfoBean.ACTION_SUPPRIMER.equals(action)) {
            if (infoBean.get(nomColonneStructure) != null) {
                if (!infoBean.getString("GRS_SAUVEGARDE_CODE_RATTACHEMENT").equals(infoBean.get(nomColonneStructure))) {
                    modificationPerimetre = true;
                }
            }
            if (infoBean.get(nomColonneRattachementAutres) != null) {
                if (!infoBean.getString("GRS_SAUVEGARDE_CODE_RATTACHEMENT_AUTRES").equals(infoBean.get(nomColonneRattachementAutres))) {
                    modificationPerimetre = true;
                }
            }
            if (infoBean.get(nomColonneRubrique) != null) {
                if (!infoBean.getString("GRS_SAUVEGARDE_CODE_RUBRIQUE").equals(infoBean.get(nomColonneRubrique))) {
                    modificationPerimetre = true;
                }
            }
            if (infoBean.get("PUBLIC_VISE_DSI") != null) {
                if (infoBean.get("PUBLIC_VISE_ESPACE") != null) {
                    if (!infoBean.getString("GRS_SAUVEGARDE_PUBLIC_VISE_ESPACE").equals(infoBean.getString("PUBLIC_VISE_ESPACE"))) {
                        modificationPerimetre = true;
                    }
                }
            }
            // VIN 20041206 Refonte des structures
            if (ficheUniv instanceof StructureModele && infoBean.get("IN_TREE") != null) {
                meta.setMetaInTree((String) infoBean.get("IN_TREE"));
            }
            /*
             * Date de mise en ligne
             */
            // Uniquement si date à vide
            if (!(Formateur.estSaisie(infoBean.get("DATE_MISE_EN_LIGNE", Date.class)))) {
                if (Formateur.estSaisie(infoBean.get("GRS_SAUVEGARDE_DATE_MISE_EN_LIGNE", Date.class))) {
                    meta.setMetaDateMiseEnLigne(infoBean.get("GRS_SAUVEGARDE_DATE_MISE_EN_LIGNE", Date.class));
                } else {
                    meta.setMetaDateMiseEnLigne(infoBean.get("DATE_CREATION", Date.class));
                }
            } else {
                Date dateCreation = infoBean.get("DATE_MISE_EN_LIGNE", Date.class);
                if (StringUtils.isNotBlank((String) infoBean.get("HEURE_MISE_EN_LIGNE"))) {
                    final Date heureMiseEnLigne = ((DateFormat) FORMAT_HEURE_MINUTE.clone()).parse((String) infoBean.get("HEURE_MISE_EN_LIGNE"));
                    final long heureEnMinutes = DateUtils.getFragmentInMinutes(heureMiseEnLigne, Calendar.DATE);
                    final Date dateMiseEnLigneSansHeure = DateUtils.truncate(dateCreation, Calendar.DATE);
                    dateCreation = new Date(DateUtils.addMinutes(dateMiseEnLigneSansHeure, (int) heureEnMinutes).getTime());
                }
                meta.setMetaDateMiseEnLigne(dateCreation);
            }
            if (infoBean.get("FORUM") != null) {
                meta.setMetaForum((String) infoBean.get("FORUM"));
            }
            if (infoBean.get("FORUM_ANO") != null) {
                meta.setMetaForumAno((String) infoBean.get("FORUM_ANO"));
            }
            if (infoBean.get("DOCUMENT_FICHIERGW") != null) {
                meta.setMetaDocumentFichiergw((String) infoBean.get("DOCUMENT_FICHIERGW"));
            }
            if (codeEspaceCollaboratif.length() > 0 && infoBean.get("ENVOI_MAIL") != null) {
                meta.setMetaNotificationMail((String) infoBean.get("ENVOI_MAIL"));
            }
            if (infoBean.get("MAIL_ANONYME") != null) {
                meta.setMetaMailAnonyme((String) infoBean.get("MAIL_ANONYME"));
            }
            if (infoBean.get("SAISIE_FRONT") != null) {
                meta.setMetaSaisieFront("1");
            }
        }
        // Type d'enregistrement (demande de validation, refus ..)
        // pour aliment le métatag de l'historique
        String typeEnregistrementMeta = "";
        final Date date = new Date(System.currentTimeMillis());
        ficheUniv.setDateModification(date);
        /*************************************/
        /* Suppression                       */
        /*************************************/
        if (InfoBean.ACTION_SUPPRIMER.equals(action)) {
            // ajout suppression unitaire
            if (autorisations == null || !autorisations.estAutoriseASupprimerLaFiche(ficheUniv)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_SUPPRESSION_PERIMETRE"));
            }
            ficheUniv.setEtatObjet("0004");
            ficheUniv.update();
            serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_SUPPRESSION, getCodeAuteur(processus), "0004");
            serviceMetatag.synchroniser(meta, ficheUniv, true);
            serviceMetatag.save(meta);
            // mise à jour des objets lies à une fiche
            PluginFicheHelper.synchroniserObjets(ficheUniv, meta, null);
            final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_FICHE"), ficheUniv.getLibelleAffichable());
            infoBean.addMessageConfirmation(confirmation);
        }
        /*************************************/
        /* Archivage                         */
        /*************************************/
        else if ("ARCHIVER".equals(action)) {
            ficheUniv.setEtatObjet("0007");
            ficheUniv.update();
            serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_ARCHIVAGE, getCodeAuteur(processus), "0007");
            serviceMetatag.synchroniser(meta, ficheUniv, true);
            if (infoBean.get("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION") != null) {
                meta.setMetaRubriquesPublication(infoBean.getString("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION"));
            }
            serviceMetatag.save(meta);
            // mise à jour des objets lies à une fiche
            PluginFicheHelper.synchroniserObjets(ficheUniv, meta, null);
            final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_ARCHIVER_FICHE"), ficheUniv.getLibelleAffichable());
            infoBean.addMessageConfirmation(confirmation);
        }
        /*************************************/
        /* Restauration                      */
        /*************************************/
        else if ("ANNULER_SUPPRESSION".equals(action) || "RESTAURER_SAUVEGARDE".equals(action) || "RESTAURER_ARCHIVE".equals(action)) {
            final String restauration = (String) infoBean.get("RESTAURATION");
            if ("NOUVELLE_FICHE".equals(restauration)) {
                ficheUniv.setCode(Long.toString(System.currentTimeMillis()));
            } else if ("REMPLACER_BROUILLON".equals(restauration)) {
                // on supprime le brouillon existant
                FicheUniv ficheUniv2 = ficheUniv.getClass().newInstance();
                ficheUniv2.setCtx(processus);
                if (ficheUniv2.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "0001") > 0) {
                    ficheUniv2.nextItem();
                    FicheUnivMgr.supprimerFiche(ficheUniv2, true);
                }
            }
            ficheUniv.setEtatObjet("0001");
            ficheUniv.update();
            // ajout de l'historique
            serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_RESTAURATION_ARCHIVAGE, getCodeAuteur(processus), "0001");
            // réinitialisation du niveau d'approbation courant
            meta.setMetaNiveauApprobation("");
            serviceMetatag.synchroniser(meta, ficheUniv, true);
            serviceMetatag.save(meta);
            // mise à jour des objets lies à une fiche
            PluginFicheHelper.synchroniserObjets(ficheUniv, meta, null);
            final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_RESTAURER_FICHE"), ficheUniv.getLibelleAffichable());
            infoBean.addMessageConfirmation(confirmation);
        }
        /* **************************** */
        /*   Duplication de la fiche    */
        /* **************************** */
        else if ("DUPLIQUER".equals(action)) {
            if (autorisations == null || !autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_CREATION)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_AJOUT_PERIMETRE"));
            }
            // sauvegarde de la fiche pour duplication des fichiers
            final FicheUniv ficheUnivOrigine = ficheUniv.getClass().newInstance();
            ficheUnivOrigine.setCtx(processus);
            ficheUnivOrigine.init();
            ficheUnivOrigine.setIdFiche(ficheUniv.getIdFiche());
            ficheUnivOrigine.retrieve();
            /* Données à réinitialiser dans le nouvel objet */
            FicheUnivMgr.dupliquer(ficheUniv);
            //création objet dans la langue
            ficheUniv.setIdFiche((long) 0);
            ficheUniv.setCode(Long.toString(System.currentTimeMillis()));
            ficheUniv.setCodeRedacteur(autorisations.getCode());
            ficheUniv.add();
            if (infoBean.get("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION") != null) {
                // duplication des rubriques de publication (non automatiques)
                final Vector<String> listeRubPub = new Vector<>();
                final String[] rubPub = infoBean.getString("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION").split(";", -2);
                for (String aRubPub : rubPub) {
                    if (!aRubPub.startsWith("#AUTO#")) {
                        listeRubPub.add(aRubPub);
                    }
                }
                final InfosFicheReferencee infosFiche = new InfosFicheReferencee();
                infosFiche.setCode(ficheUniv.getCode());
                infosFiche.setLangue(ficheUniv.getLangue());
                infosFiche.setType(ReferentielObjets.getCodeObjet(ficheUniv));
                serviceRubriquePublication.saveAllRubPubForFiche(infosFiche, listeRubPub);
            }
            final String saveMetaForum = meta.getMetaForum();
            final String saveMetaForumAno = meta.getMetaForumAno();
            final String saveMetaInTree = meta.getMetaInTree();
            // mise à jour du méta de la fiche dupliquée
            meta = MetatagUtils.creerMeta(ficheUniv);
            if (infoBean.get("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION") != null) {
                meta.setMetaRubriquesPublication(infoBean.getString("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION"));
            }
            serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_DUPLICATION, getCodeAuteur(processus), "0001");
            meta.setMetaForum(saveMetaForum);
            meta.setMetaForumAno(saveMetaForumAno);
            meta.setMetaInTree(saveMetaInTree);
            if (StringUtils.isNotEmpty(infoBean.getString("DOCUMENT_FICHIERGW"))) {
                meta.setMetaDocumentFichiergw((String) infoBean.get("DOCUMENT_FICHIERGW"));
            }
            serviceMetatag.save(meta);
            // traitement principal des plugins
            PluginFicheHelper.traiterPrincipal(infoBean, ficheUniv, meta);
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            infoBean.addMessageConfirmation("Fiche dupliquée");
            infoBean.set("GRS_LIBELLE_COPIE", InfoBean.LIBELLE_DUPLICATION);
            ecranLogique = "PRINCIPAL";
            processus.preparerPRINCIPAL();
        }
        /* ************************************ */
        /*   Traduction dans une autre langue   */
        /* ************************************ */
        else if ("TRADUIRE".equals(action)) {
            if (autorisations == null || !autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_TRADUCTION)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_TRADUCTION_PERIMETRE"));
            }
            if (StringUtils.isEmpty(infoBean.getString("AUTRE_LANGUE")) || "0000".equals(infoBean.getString("AUTRE_LANGUE"))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_SAISIR_LANGUE"));
            }
            // sauvegarde de la fiche pour duplication des fichiers
            final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
            final FicheUniv ficheUnivOrigine = ficheUniv.getClass().newInstance();
            ficheUnivOrigine.setCtx(processus);
            ficheUnivOrigine.init();
            ficheUnivOrigine.setIdFiche(ficheUniv.getIdFiche());
            ficheUnivOrigine.retrieve();
            ficheUniv.setCodeRedacteur(autorisations.getCode());
            ficheUniv.dupliquer(infoBean.getString("AUTRE_LANGUE"));
            serviceRessource.duplicateFiles(ficheUnivOrigine, ficheUniv);
            if (infoBean.get("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION") != null) {
                // duplication des rubriques de publication (non automatiques)
                final Vector<String> listeRubPub = new Vector<>();
                final String[] rubPub = infoBean.getString("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION").split(";", -2);
                for (String aRubPub : rubPub) {
                    if (!aRubPub.startsWith("#AUTO#")) {
                        listeRubPub.add(aRubPub);
                    }
                }
                final InfosFicheReferencee infosFiche = new InfosFicheReferencee();
                infosFiche.setCode(ficheUniv.getCode());
                infosFiche.setLangue(ficheUniv.getLangue());
                infosFiche.setType(ReferentielObjets.getCodeObjet(ficheUniv));
                serviceRubriquePublication.saveAllRubPubForFiche(infosFiche, listeRubPub);
            }
            final String saveMetaForum = meta.getMetaForum();
            final String saveMetaForumAno = meta.getMetaForumAno();
            final String saveMetaInTree = meta.getMetaInTree();
            meta.getId();
            meta = MetatagUtils.creerMeta(ficheUniv);
            serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_TRADUCTION, getCodeAuteur(processus), "0001");
            meta.setMetaForum(saveMetaForum);
            meta.setMetaForumAno(saveMetaForumAno);
            meta.setMetaInTree(saveMetaInTree);
            serviceMetatag.save(meta);
            // traitement principal des plugins
            PluginFicheHelper.traiterPrincipal(infoBean, ficheUniv, meta);
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            ecranLogique = "PRINCIPAL";
            infoBean.addMessageConfirmation("Fiche dupliquée en " + LangueUtil.getDisplayName(infoBean.getString("AUTRE_LANGUE")));
            infoBean.set("GRS_LIBELLE_COPIE", InfoBean.LIBELLE_TRADUCTION);
            processus.preparerPRINCIPAL();
        }
        /*************************************/
        /* Validation de l'écran             */
        /*************************************/
        else if (action.equals(InfoBean.ACTION_ENREGISTRER)) {
            //JSS 20051117 : on a besoin du rédacteur pour la mise en ligne unitaire
            final String codeRedacteurPrecedent = ficheUniv.getCodeRedacteur();
            if (infoBean.get("CODE_REDACTEUR") != null) {
                ficheUniv.setCodeRedacteur(infoBean.getString("CODE_REDACTEUR"));
            }
            if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                ficheUniv.setCodeRedacteur((String) processus.getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE));
                //saisie anonyme dans le front, on met l'utilisateur anonyme
                if ("1".equals(meta.getMetaSaisieFront()) && ficheUniv.getCodeRedacteur() == null) {
                    ficheUniv.setCodeRedacteur(ServiceUser.UTILISATEUR_ANONYME);
                }
            }
            if (ficheUniv.getCodeRedacteur() == null) {
                ficheUniv.setCodeRedacteur("");
            }

            Date infoBeanDateAlerte = infoBean.getDate("DATE_ALERTE");
            if (infoBeanDateAlerte != null && isDateApresAujourdhui(infoBeanDateAlerte)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_INVALIDE_DATE_ALERTE"));
            }
            ficheUniv.setDateAlerte(infoBean.get("DATE_ALERTE", Date.class));

            if (infoBean.get("MESSAGE_ALERTE") != null) {
                ficheUniv.setMessageAlerte((String) infoBean.get("MESSAGE_ALERTE"));
            }
            if (infoBean.get("META_KEYWORDS") != null) {
                ficheUniv.setMetaKeywords((String) infoBean.get("META_KEYWORDS"));
            }
            if (infoBean.get("META_DESCRIPTION") != null) {
                ficheUniv.setMetaDescription((String) infoBean.get("META_DESCRIPTION"));
            }
            if (infoBean.get("CONTENU_ENCADRE") != null) {
                ficheUniv.setContenuEncadre((String) infoBean.get("CONTENU_ENCADRE"));
            }
            if (infoBean.get("ENCADRE_RECHERCHE") != null) {
                ficheUniv.setEncadreRecherche((String) infoBean.get("ENCADRE_RECHERCHE"));
                ficheUniv.setEncadreRechercheBis((String) infoBean.get("ENCADRE_RECHERCHE_BIS"));
            } else {
                ficheUniv.setEncadreRecherche("");
                ficheUniv.setEncadreRechercheBis("");
            }
            /* Diffusion */
            String activationDsi = StringUtils.defaultString(PropertyHelper.getCoreProperty("dsi.activation"), "0");
            if (("1".equals(activationDsi)) && (ficheUniv instanceof DiffusionSelective)) {
                //  l'utilisateur a-t-il les droits de diffusion ?
                // JSS 20060103 : on ajoute le cas de l'espace
                if (autorisations != null && (autorisations.isDiffuseurDSI() || (("4").equals(infoBean.get("DIFFUSION_MODE_RESTRICTION"))))) {
                    final DiffusionSelective ficheDiffusion = (DiffusionSelective) ficheUniv;
                    if (infoBean.getString("PUBLIC_VISE_DSI") != null) {
                        ficheDiffusion.setDiffusionPublicVise(formaterPublicViseDsiPourEnregistrement(infoBean.getString("PUBLIC_VISE_DSI")));
                        // RP 20050410 controle de la saisie de publics vises
                        if (infoBean.getString("DIFFUSION_MODE_RESTRICTION") != null && infoBean.getString("DIFFUSION_MODE_RESTRICTION").length() > 0) {
                            if ("2".equals(infoBean.getString("DIFFUSION_MODE_RESTRICTION")) && "".equals(infoBean.getString("PUBLIC_VISE_DSI"))) {
                                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_SAISIR_PUBLICS_VISES"));
                            } else if ("3".equals(infoBean.getString("DIFFUSION_MODE_RESTRICTION")) && "".equals(infoBean.getString("PUBLIC_VISE_DSI_RESTRICTION"))) {
                                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_SAISIR_PUBLICS_AUTORISES"));
                            } else {
                                ficheDiffusion.setDiffusionModeRestriction(infoBean.getString("DIFFUSION_MODE_RESTRICTION"));
                            }
                        }
                    }
                    if (infoBean.getString("PUBLIC_VISE_DSI_RESTRICTION") != null) {
                        // JSS 20040409 : la valeur '4' correspond à un espace collaboratif
                        if ("4".equals(infoBean.get("DIFFUSION_MODE_RESTRICTION"))) {
                            // La notion de public visé n'a pas de sens pour des espaces
                            ficheDiffusion.setDiffusionPublicVise("");
                            ficheDiffusion.setDiffusionPublicViseRestriction(infoBean.getString("PUBLIC_VISE_ESPACE"));
                        } else {
                            ficheDiffusion.setDiffusionPublicViseRestriction(formaterPublicViseDsiPourEnregistrement(infoBean.getString("PUBLIC_VISE_DSI_RESTRICTION")));
                        }
                    }
                }
            }
            if (infoBean.get("CODE_RUBRIQUAGE") != null) {
                final String dateRubriquage = Formateur.formater(infoBean.get("DATE_RUBRIQUAGE"));
                final String codeRubriquage = Formateur.formater(infoBean.get("CODE_RUBRIQUAGE"));
                final String dateSauvegardeRubriquage = Formateur.formater(infoBean.get("GRS_SAUVEGARDE_DATE_RUBRIQUAGE"));
                final String codeSauvegardeRubriquage = infoBean.getString("GRS_SAUVEGARDE_CODE_RUBRIQUAGE");
                Date infoBeanDateRubriquage = infoBean.getDate("DATE_RUBRIQUAGE");
                // Controles des valeurs uniquement si modifié
                if (!dateSauvegardeRubriquage.equals(dateRubriquage) || !codeSauvegardeRubriquage.equals(codeRubriquage)) {
                    if (Formateur.estSaisie(infoBean.get("DATE_RUBRIQUAGE", Date.class))) {
                        if (infoBean.getString("CODE_RUBRIQUAGE").length() == 0) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_SAISIR_RUBRIQUE_DESTINATION"));
                        }else if(isDateApresAujourdhui(infoBeanDateRubriquage)) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_INVALIDE_DATE_RUBRIQUAGE"));
                        }
                    } else if (infoBean.getString("CODE_RUBRIQUAGE").length() > 0) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_SAISIR_DATE_RUBRIQUAGE"));
                    }
                    final String codeRubriqueInitial = ficheUniv.getCodeRubrique();
                    // on teste le droit de modif sur le futur code de rubriquage
                    ficheUniv.setCodeRubrique(infoBean.getString("CODE_RUBRIQUAGE"));
                    if (autorisations == null || !autorisations.estAutoriseAModifierLaFiche(ficheUniv, false)) {
                        ficheUniv.setCodeRubrique(codeRubriqueInitial);
                        // On restaure les anciennes valeurs
                        infoBean.set("DATE_RUBRIQUAGE", infoBean.get("GRS_SAUVEGARDE_DATE_RUBRIQUAGE"));
                        infoBean.set("CODE_RUBRIQUAGE", infoBean.get("GRS_SAUVEGARDE_CODE_RUBRIQUAGE"));
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_RUBRIQUAGE_PERIMETRE"));
                    }
                    // on restaure l'ancien code de rubrique
                    ficheUniv.setCodeRubrique(codeRubriqueInitial);
                }
                // Controles OK, on sauvegarde
                meta.setMetaDateRubriquage(infoBeanDateRubriquage);
                meta.setMetaCodeRubriquage(infoBean.getString("CODE_RUBRIQUAGE"));
            }
            /*
             * Date de suppression
             */
            // Uniquement si date modifiée
            final String dateSuppression = Formateur.formater(infoBean.get("DATE_SUPPRESSION"));
            final String dateSauvegardeSuppression = Formateur.formater(infoBean.get("GRS_SAUVEGARDE_DATE_SUPPRESSION"));
            Date infoBeanDateSuppression = infoBean.getDate("DATE_SUPPRESSION");
            if (!dateSuppression.equals(dateSauvegardeSuppression)) {
                if (autorisations == null || !autorisations.estAutoriseASupprimerLaFiche(ficheUniv, false)) {
                    // On restaure l'ancienne valeur
                    infoBean.set("DATE_SUPPRESSION", infoBean.get("GRS_SAUVEGARDE_DATE_SUPPRESSION"));
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_DATE_SUPPESSION_PERIMETRE"));
                }

                if ( infoBeanDateSuppression!= null && isDateApresAujourdhui(infoBeanDateSuppression)) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_INVALIDE_DATE_SUPPRESSION"));
                }
            }
            meta.setMetaDateSuppression(infoBeanDateSuppression);
            /*
             * Date d'archivage
             */
            // Uniquement si date modifiée
            final String dateArchivage = Formateur.formater(infoBean.get("DATE_ARCHIVAGE"));
            final String dateSauvegardeArchivage = Formateur.formater(infoBean.get("GRS_SAUVEGARDE_DATE_ARCHIVAGE"));
            final Date infoBeanDateArchivage = infoBean.getDate("DATE_ARCHIVAGE");
            if (!dateArchivage.equals(dateSauvegardeArchivage)) {
                if (autorisations == null || !autorisations.estAutoriseASupprimerLaFiche(ficheUniv, false)) {
                    // On restaure l'ancienne valeur
                    infoBean.set("DATE_ARCHIVAGE", infoBean.get("GRS_SAUVEGARDE_DATE_ARCHIVAGE"));
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_DATE_ARCHIVAGE_PERIMETRE"));
                }
                if (infoBeanDateArchivage != null && isDateApresAujourdhui(infoBean.get("DATE_ARCHIVAGE", Date.class))) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_INVALIDE_DATE_ARCHIVAGE"));
                }
            }
            meta.setMetaDateArchivage(infoBeanDateArchivage);
            /***********************/
            /* Gestion de l'apercu */
            /***********************/
            Long idMetaFicheEnLigne = 0L;
            if ("1".equals(infoBean.getString("APERCU"))) {
                /* SAUVEGARDE AUTOMATIQUE (sauf si il s'agit dèjà d'une sauvegarde) */
                if (!infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                    // sauvegarde de l'id du metatag de la fiche en ligne
                    idMetaFicheEnLigne = meta.getId();
                    // pas de sauvegarde automatique en création, car le code n'est pas définitif
                    if (!"0006".equals(ficheUniv.getEtatObjet())) {
                        /* Destruction de la sauvegarde courante */
                        FicheUniv ficheUniv2 = ficheUniv.getClass().newInstance();
                        ficheUniv2.setCtx(processus);
                        if (ficheUniv2.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "0006") > 0) {
                            while (ficheUniv2.nextItem()) {
                                // on utilise le champ alerte pour les fichiers
                                ficheUniv2.setMessageAlerte(ServiceRessource.ETAT_SAUVEGARDE_STRUCTURE_COURANTE);
                                FicheUnivMgr.supprimerFiche(ficheUniv2, true);
                            }
                        }
                        /* création d'une nouvelle sauvegarde */
                        ficheUniv.setIdFiche((long) 0);
                        ficheUniv.setEtatObjet("0006");
                        ficheUniv.add();
                        // création nouveau méta
                        final MetatagBean metaSave = new MetatagBean();
                        metaSave.setMetaEtatObjet(EtatFiche.SAUVEGARDE_AUTO.getEtat());
                        metaSave.setMetaIdFiche(ficheUniv.getIdFiche());
                        metaSave.setMetaLangue(ficheUniv.getLangue());
                        metaSave.setMetaCodeObjet(ReferentielObjets.getCodeObjet(ficheUniv));
                        serviceMetatag.synchroniser(metaSave, ficheUniv, true);

                        if (infoBean.get("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION") != null) {
                            meta.setMetaRubriquesPublication(infoBean.getString("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION"));
                        }
                        serviceMetatag.save(metaSave);
                        // traitement principal des plugins : fichier, sous objet etc
                        PluginFicheHelper.traiterPrincipal(infoBean, ficheUniv, metaSave);
                    }
                }
                /* insertion du nouvel apercu */
                ficheUniv.setIdFiche((long) 0);
                ficheUniv.setEtatObjet("0005");
                ficheUniv.add();
                // création nouveau méta
                final MetatagBean metaPreview = new MetatagBean();
                metaPreview.setMetaEtatObjet(EtatFiche.APERCU.getEtat());
                metaPreview.setMetaIdFiche(ficheUniv.getIdFiche());
                metaPreview.setMetaLangue(ficheUniv.getLangue());
                metaPreview.setMetaCodeObjet(ReferentielObjets.getCodeObjet(ficheUniv));
                serviceMetatag.synchroniser(metaPreview, ficheUniv, true);
                if (ficheEnLigneAvantTraitement != null) {
                    metaPreview.setMetaNbHits(idMetaFicheEnLigne);
                }
                infoBean.set("CODE", ficheUniv.getCode());
                infoBean.set("LANGUE", ficheUniv.getLangue());
                infoBean.setEcranLogique("APERCU");
                serviceMetatag.save(metaPreview);
                infoBean.set("ID_METATAG", metaPreview.getId().toString());
                PluginFicheHelper.traiterPrincipal(infoBean, ficheUniv, metaPreview);
                gestionRedirectionPourApercu(infoBean);
                return "APERCU";
            }
            /*****************************/
            /* Suite de l'enregistrement */
            /*****************************/
            /*
             * Les variables prises en compte sont :
             *
             *  - TYPE_ENREGISTREMENT [2,3]
             *  - MODE_BROUILLON [1]
             */
            String typeEnregistrement = "";
            if (infoBean.get("TYPE_ENREGISTREMENT") != null) {
                typeEnregistrement = infoBean.getString("TYPE_ENREGISTREMENT");
            }
            boolean retourValidation = false;
            if ("3".equals(typeEnregistrement)) {
                retourValidation = true;
            }
            boolean demandeMiseEnLigne = false;
            boolean validationApprobation = false;
            final String niveauApprobationCourant = meta.getMetaNiveauApprobation();
            if ("2".equals(typeEnregistrement)) {
                // RP20050811 on ne met véritablement en ligne que si plus de niveau d'approbation en cours
                if ("".equals(niveauApprobationCourant)) {
                    miseEnLigne = true;
                } else {
                    validationApprobation = true;
                    demandeMiseEnLigne = true;
                }
            }
            final boolean annulerDemandeValidation = lireFlag(infoBean.getString("ANNULER_DEMANDE_VALIDATION"));
            boolean modeBrouillon = false;
            if (infoBean.get("MODE_BROUILLON") != null && "1".equals(infoBean.getString("MODE_BROUILLON"))) {
                modeBrouillon = true;
                //RP 20050910 on réinitialise le niveau d'approbation courant
                meta.setMetaNiveauApprobation("");
            }
            // Par défaut, action enregistrer = demande mise en ligne
            // (sauf pour les fiches à valider sans niveau d'approbation)
            // RP20050811 en plus demande de mise en ligne si présence d'un niveau d'approbation
            demandeMiseEnLigne = !modeBrouillon && !retourValidation && ("0001".equals(ficheUniv.getEtatObjet()) || ("0003".equals(ficheUniv.getEtatObjet())) || ("0002".equals(ficheUniv.getEtatObjet()) && demandeMiseEnLigne));
            // L'utilisateur a-t-il les droits pour mettre en ligne ?
            if (demandeMiseEnLigne) {
                //Peut-etre la personne a les droits pour mettre en ligne
                if (autorisations != null) {
                    // RP20050819
                    String niveauApprobationDemande = "";
                    if (infoBean.get("NIVEAU_APPROBATION_DEMANDE") != null) {
                        niveauApprobationDemande = infoBean.getString("NIVEAU_APPROBATION_DEMANDE");
                    }
                    // pas de niveau d'approbation specifie on teste les droits de l'utilisateur
                    if (niveauApprobationDemande.length() == 0) {
                        int indice = AutorisationBean.INDICE_VALIDATION;
                        // Dans le cas d'un objet collaboratif on ignore les niveaux d'approbation intermédiaires
                        if ("4".equals(infoBean.get("DIFFUSION_MODE_RESTRICTION"))) {
                            indice = AutorisationBean.INDICE_APPROBATION;
                        }
                        if (autorisations.getAutorisationParFiche(ficheUniv, indice, meta.getMetaNiveauApprobation())) {
                            miseEnLigne = true;
                            demandeMiseEnLigne = false;
                        } else if (autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_MISE_EN_LIGNE_UNITAIRE) && (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION) || codeRedacteurPrecedent.equals(autorisations.getCode()))) {
                            // JSS 20051115 : nouveau droit de 'mise en ligne unitaire'
                            // AAR 20100426 - ticket 7381 - le controle des autorisations se fait sur le redacteur avant qu'il ne soit modifié par le contributeur (sinon, il peut par ex. s'attribuer la fiche d'un autre rédacteur et la mettre en ligne)
                            miseEnLigne = true;
                            demandeMiseEnLigne = false;
                        }
                    } else {
                        // on reteste que le niveau choisi correspond bien au périmètre saisi
                        if (!autorisations.estAutoriseADemanderApprobation(ficheUniv, meta.getMetaNiveauApprobation(), niveauApprobationDemande)) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_APPROBATION_PERIMETRE"));
                        }
                        // on resteste que les validateurs choisis sont bien dans la liste
                        if (!MailUtil.controlerListeValidateur(processus, ficheUniv, infoBean.getString("LISTE_VALIDATEURS"), niveauApprobationDemande)) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_VALIDATEUR_PERIMETRE"));
                        }
                    }
                    // on renseigne le niveau d'approbation suivant de la fiche
                    if (!miseEnLigne) {
                        meta.setMetaNiveauApprobation(autorisations.getNiveauApprobation());
                    }
                }
            }
            /****************************************/
            /* Controle des droits d'enregistrement */
            /****************************************/
            if (autorisations != null) {
                /* Création */
                if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                    // RP20051003 patch en saisie anonyme en front on ne bloque pas la création
                    boolean ano = false;
                    if (infoBean.get("SAISIE_FRONT") != null) {
                        final String anonyme = PropertyHelper.getExtensionProperty(infoBean.getNomExtension(), ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjet(ficheUniv)) + ".anonyme");
                        if ("1".equals(anonyme)) {
                            ano = true;
                        }
                    }
                    if (!autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_CREATION) && !ano) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_CREATION_PERIMETRE"));
                    }
                }
                /* Modification */
                if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && !(retourValidation || miseEnLigne || validationApprobation)) {
                    // On exclut les validations de ce test
                    // Elles seront traitées lors des controles de validation
                    // Si l'utilisateur est rédacteur de la fiche et qu'il
                    // ne change pas son périmère
                    // on ne fait pas de controle
                    boolean effectuerControle = true;
                    if (autorisations.getCode().equals(ficheUniv.getCodeRedacteur()) && ficheUniv.getCodeRedacteur().equals(codeRedacteurPrecedent)) {
                        effectuerControle = modificationPerimetre;
                    }
                    if ((effectuerControle) && !autorisations.estAutoriseAModifierLaFiche(ficheUniv, false)) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_MODIFICATION_RUBRIQUE_STRUCTURE"));
                    }
                }
                /* Validation */
                if (retourValidation || miseEnLigne || validationApprobation) {
                    boolean droitsMiseEnLigne = false;
                    // JSS 20051115 : ajout role 'mise en ligne unitaire'
                    if (autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_MISE_EN_LIGNE_UNITAIRE)) {
                        if (codeRedacteurPrecedent.equals(autorisations.getCode()) || infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                            droitsMiseEnLigne = true;
                        }
                    }
                    // On controle que la personne a encore les droits de validation
                    // RP20050811 on teste uniquement le droit d'approbation courant
                    // si plus niveau intermédiaire c'est le droit de validation classique
                    if (autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_APPROBATION, niveauApprobationCourant)) {
                        droitsMiseEnLigne = true;
                    }
                    if (!droitsMiseEnLigne) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_VALIDATION_RUBRIQUE_STRUCTURE"));
                    }
                }
            }
            if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                ficheUniv.setDateCreation(date);
                ficheUniv.setDateProposition(date);
            }
            // Création d'un fiche (brouilllon ou a valider) à partir d'une fiche en ligne
            if ("0003".equals(ficheUniv.getEtatObjet()) && !miseEnLigne) {
                /* il existe un brouillon */
                if (ficheUniv.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "0001") > 0) {
                    if (!modeBrouillon) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_BROUILLON_DOUBLON"));
                    }
                }
                FicheUnivMgr.creerVersionTravail(ficheUniv);
                // création nouveau méta
                meta.setId((long) 0);
                meta.setMetaEtatObjet(ficheUniv.getEtatObjet());
                typeEnregistrementMeta = MetatagUtils.HISTORIQUE_CREATION;
            }
            /* Contrôles pour le validateur */
            if (retourValidation && miseEnLigne) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_ENLIGNE_RENVOYEE"));
            }
            /* Demande de validation */
            if (demandeMiseEnLigne) {
                /* il existe une version à valider */
                try (ContexteDao ctx = new ContexteDao()) {
                    ficheUniv.setCtx(ctx);
                    if (ficheUniv.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "0002") > 0) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_A_VALIDER_DOUBLON"));
                    }
                }
                typeEnregistrementMeta = MetatagUtils.HISTORIQUE_DEMANDE_VALIDATION;
                ficheUniv.setEtatObjet("0002");
                ficheUniv.setDateProposition(date);
                infoBean.addMessageConfirmation(MessageHelper.getCoreMessage("ST_FRONT_AVALIDER"));
            }
            /* Annulation demande de validation */
            if (annulerDemandeValidation) {
                // on supprime le brouillon existant
                FicheUniv ficheUniv2 = ficheUniv.getClass().newInstance();
                ficheUniv2.setCtx(processus);
                if (ficheUniv2.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "0001") > 0) {
                    ficheUniv2.nextItem();
                    FicheUnivMgr.supprimerFiche(ficheUniv2, true);
                }
                typeEnregistrementMeta = MetatagUtils.HISTORIQUE_ANNULATION_DEMANDE_VALIDATION;
                //RP 20050910 on réinitialise le niveau d'approbation courant
                meta.setMetaNiveauApprobation("");
                ficheUniv.setEtatObjet("0001");
            }
            /* Retour à l'auteur validation négative */
            if (retourValidation) {
                MailUtil.envoyerMailRetourValidation(infoBean, ficheUniv, meta, false);
                typeEnregistrementMeta = MetatagUtils.HISTORIQUE_RETOUR_AUTEUR;
                ficheUniv.setEtatObjet("0001");
                // RP20050812 on supprime l'état d'approbation en cours
                meta.setMetaNiveauApprobation("");
            }
            /* Mise en ligne */
            if (miseEnLigne) {
                /* s'il s'agit d'une mise en ligne suite à une validation, avec test sur les commentaires en saisie front*/
                if ("0002".equals(ficheUniv.getEtatObjet())) {
                    typeEnregistrementMeta = MetatagUtils.HISTORIQUE_VALIDATION;
                    retourValidation = true;
                }
                ficheUniv.setCodeValidation((String) processus.getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE));
                ficheUniv.setDateValidation(date);
            }
            /* Enregistrement de la fiche en création : enregistrer = true*/
            if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                if (typeEnregistrementMeta.length() == 0) {
                    typeEnregistrementMeta = MetatagUtils.HISTORIQUE_CREATION;
                }
                FicheUnivMgr.enregistrer(true, ficheUniv);
            }
            /* Enregistrement de la fiche en modification : enregistrer = false*/
            if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {
                if (typeEnregistrementMeta.length() == 0) {
                    typeEnregistrementMeta = MetatagUtils.HISTORIQUE_MODIFICATION;
                }
                // enregistrement de la fiche
                FicheUnivMgr.enregistrer(false, ficheUniv);
                // 5.1 MacroObjetUtil.synchroniserSousObjets( processus, (MacroObjet) ficheUniv);
            }
            /*
             * JSS 20050301 : push multi-sites
             */
            if (infoBean.get("GRS_SAISIE_PUBLICATION") != null && "1".equals(infoBean.get("GRS_SAISIE_PUBLICATION"))) {
                if (miseEnLigne) {
                    // dans la cas d'une mise en ligne, on recopie les rubriques dans la table
                    // RUBRIQUE_PUBLICATION
                    final Vector<String> listeRubriques = Chaine.getVecteurPointsVirgules(infoBean.getString("RUBRIQUE_PUBLICATION"));
                    final Vector<String> listeRubriquesAutomatiques = Chaine.getVecteurPointsVirgules(infoBean.getString("RUBRIQUE_PUBLICATION_AUTOMATIQUE"));
                    // on vérifie que les rubriques automatiques n'ont pas été supprimées
                    if (!listeRubriques.containsAll(listeRubriquesAutomatiques)) {
                        infoBean.set("RUBRIQUE_PUBLICATION", infoBean.get("GRS_SAUVEGARDE_RUBRIQUE_PUBLICATION"));
                        infoBean.set("LIBELLE_RUBRIQUE_PUBLICATION", infoBean.get("GRS_SAUVEGARDE_LIBELLE_RUBRIQUE_PUBLICATION"));
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_IMPOSSIBLE_SUPPRIMER_RUBPUB"));
                    }
                    listeRubriques.removeAll(listeRubriquesAutomatiques);
                    final InfosFicheReferencee infosFiche = new InfosFicheReferencee();
                    infosFiche.setCode(ficheUniv.getCode());
                    infosFiche.setLangue(ficheUniv.getLangue());
                    infosFiche.setType(ReferentielObjets.getCodeObjet(ficheUniv));
                    serviceRubriquePublication.saveAllRubPubForFiche(infosFiche, listeRubriques);
                    // les méta ne servent plus à rien
                    meta.setMetaRubriquesPublication("");
                } else {
                    // Sinon, on sauvegarde dans les méta-tags
                    meta.setMetaRubriquesPublication(infoBean.getString("RUBRIQUE_PUBLICATION"));
                }
            }
            /* Mise en ligne (traitement spécifique car suppression des versions déjà en ligne) */
            if (miseEnLigne) {
                final Long idMetaEnLigne = FicheUnivMgr.mettreEnLigne(ficheUniv, ficheEnLigneAvantTraitement);
                PluginFicheHelper.apresMiseEnLigne(ficheUniv, idMetaEnLigne, meta.getId(), null);
                if (idMetaEnLigne != 0 && !meta.getId().equals(idMetaEnLigne)) {
                    // !!! Attention ici il ne faut pas supprimer les plugins (cf PluginFicheHelper.supprimerObjets(ficheUniv, meta, null);)
                    // car il s'agit de la fiche mise en ligne
                    // on ne supprime QUE le méta vu qu'on a repris l'ancien
                    serviceMetatag.delete(meta.getId());
                    meta.setId(idMetaEnLigne);
                }
                /* Enregistrement métatag */
                if (typeEnregistrementMeta.length() == 0) {
                    typeEnregistrementMeta = MetatagUtils.HISTORIQUE_MODIFICATION;
                }
                serviceMetatag.addHistory(meta, typeEnregistrementMeta, getCodeAuteur(processus), ficheUniv.getEtatObjet());
                serviceMetatag.synchroniser(meta, ficheUniv, true);
                serviceMetatag.save(meta);
                // traitement principal des plugins
                PluginFicheHelper.traiterPrincipal(infoBean, ficheUniv, meta);
                /* Retour à l'auteur validation positive */
                if (retourValidation) {
                    MailUtil.envoyerMailRetourValidation(infoBean, ficheUniv, meta, true);
                }
                // Envoi d'un mail de notification suivant le droit "N"
                MailUtil.envoyerMailNotification(infoBean, ficheUniv, meta);
            } else {
                /* Enregistrement métatag */
                if (typeEnregistrementMeta.length() == 0) {
                    typeEnregistrementMeta = MetatagUtils.HISTORIQUE_MODIFICATION;
                }
                serviceMetatag.addHistory(meta, typeEnregistrementMeta, getCodeAuteur(processus), ficheUniv.getEtatObjet());
                serviceMetatag.synchroniser(meta, ficheUniv, true);
                serviceMetatag.save(meta);
                // traitement principal des plugins
                PluginFicheHelper.traiterPrincipal(infoBean, ficheUniv, meta);
            }
            /* Envoi d'un mail de validation */
            if (demandeMiseEnLigne) {
                MailUtil.envoyerMailDemandeValidation(infoBean, ficheUniv, meta);
            }
            /* Suppression de la sauvegarde automatique */
            final FicheUniv ficheUniv3 = ficheUniv.getClass().newInstance();
            ficheUniv3.setCtx(processus);
            if (ficheUniv3.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "0006") > 0) {
                while (ficheUniv3.nextItem()) {
                    FicheUnivMgr.supprimerFiche(ficheUniv3, true);
                }
            }
            ajouterConfirmationApresEnregistrement(infoBean, ficheUniv, miseEnLigne);
        }
        FicheUniv ficheEnLigneApresTraitement = ficheUniv.getClass().newInstance();
        ficheEnLigneApresTraitement.init();
        ficheEnLigneApresTraitement.setCtx(processus);
        if (ficheEnLigneApresTraitement.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "0003") > 0) {
            ficheEnLigneApresTraitement.nextItem();
        } else {
            ficheEnLigneApresTraitement = null;
        }
        // Synchronisation des fiches
        if (ficheEnLigneAvantTraitement == null && ficheEnLigneApresTraitement != null) {
            PluginFicheHelper.synchroniserObjets(ficheUniv, meta, null);
        } else if (ficheEnLigneAvantTraitement != null && ficheEnLigneApresTraitement != null && perimetreModifie(ficheEnLigneAvantTraitement, ficheEnLigneApresTraitement)) {
            PluginFicheHelper.synchroniserObjets(ficheUniv, meta, null);
        } else if (ficheEnLigneAvantTraitement != null && ficheEnLigneApresTraitement == null) {
            PluginFicheHelper.synchroniserObjets(ficheUniv, meta, null);
        }
        if (ficheUniv instanceof StructureModele && action.equals(InfoBean.ACTION_ENREGISTRER)) {
            // Mise à jour des groupes
            SynchroGroupedsiStructure.synchroniserGroupeDsi((StructureModele) ficheUniv);
        }
        // post traitement principal des plugins
        PluginFicheHelper.postTraiterPrincipal(infoBean, ficheUniv, meta);
        // RP 20050203 retour sur l'onglet de l'objet
        infoBean.set("ETAT_FICHE_ENREGISTREE", ficheUniv.getEtatObjet());
        LOG.debug("ETAT FICHE : " + infoBean.get("ETAT_FICHE_ENREGISTREE"));
        if (!FicheUnivMgr.isFicheCollaborative(ficheUniv)) {
            gestionRedirectionApresEnregistrement(infoBean);
        }
        return ecranLogique;
    }

    /**
     * Vérifie que la date est supérieur ou égale à la date du jour sauf si {@code dateAVerifier = new Date(0);} ...
     *
     * @param dateAVerifier
     *            la date à vérifier ne doit pas être égale à null.
     * @return vrai si la date est au 01/01/1970 ou si la date est >= à la date du jour.
     */
    private static boolean isDateApresAujourdhui(final Date dateAVerifier) {
        final Calendar calendrierSuppression = new GregorianCalendar();
        calendrierSuppression.setTime(dateAVerifier);
        final Calendar maintenant = DateUtils.truncate(new GregorianCalendar(), Calendar.DAY_OF_MONTH);
        return Formateur.estSaisie(dateAVerifier) && maintenant.after(calendrierSuppression);
    }

    /**
     * On ajoute le message en fonction de l'action de l'utilisateur. FIXME je ne sais pas pourquoi on supprime les messages de confirmation avant..
     *
     * @param infoBean les données du formulaires
     * @param ficheUniv la fiche en cours de CRUD
     * @param isMiseEnLigne est ce qu'on est en cours de mise en ligne?
     */
    private static void ajouterConfirmationApresEnregistrement(final InfoBean infoBean, final FicheUniv ficheUniv, final boolean isMiseEnLigne) {
        infoBean.removeMessage(TypeMessage.CONFIRMATION.getCode());
        String messageConfirmation = StringUtils.EMPTY;
        if (isMiseEnLigne) {
            messageConfirmation = MessageHelper.getCoreMessage("CONFIRMATION_MISE_EN_LIGNE_FICHE");
        } else if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
            messageConfirmation = MessageHelper.getCoreMessage("CONFIRMATION_CREATION_FICHE");
        } else if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {
            messageConfirmation = MessageHelper.getCoreMessage("CONFIRMATION_MODIFICATION_FICHE");
        }
        if (StringUtils.isNotBlank(messageConfirmation)) {
            final String confirmation = String.format(messageConfirmation, ficheUniv.getLibelleAffichable());
            infoBean.addMessageConfirmation(confirmation);
        }
    }

    /**
     * Suppression de l'écran de confirmation qui ne servait qu'a faire des sendRedirect FIXME ca n'a rien à foutre là...
     *
     * @param infoBean les données du formulaires
     * @throws Exception
     */
    private static void gestionRedirectionApresEnregistrement(final InfoBean infoBean) throws Exception {
        StringBuilder urlRedirect = new StringBuilder(WebAppUtil.SG_PATH);
        urlRedirect.append("?PROC=");
        urlRedirect.append(infoBean.getNomProcessus());
        final String action = infoBean.getActionUtilisateur();
        if (infoBean.getSessionHttp().getAttribute("CURRENT_SEARCH_PARAMS") != null) {
            urlRedirect.append("&").append(URLEncoder.encode("#ECRAN_LOGIQUE#", CharEncoding.DEFAULT)).append("=RECHERCHE&").append(URLEncoder.encode("#ETAT#", CharEncoding.DEFAULT)).append("=RECHERCHE&RECHERCHE_ETENDUE=OUI&SEARCH_PARAMS=IN_CACHE");
            if (infoBean.get("PAGE") != null) {
                urlRedirect.append("&PAGE=").append(infoBean.getString("PAGE"));
            }
        } else if ("ANNULER_SUPPRESSION".equals(action) || "RESTAURER_SAUVEGARDE".equals(action) || "RESTAURER_ARCHIVE".equals(action)) {
            urlRedirect.append("&ACTION=MODIFIER&ID_FICHE=").append(infoBean.getString("ID_FICHE"));
        } else {
            // liste des fiches du redacteur
            urlRedirect.append("&ACTION=ACCUEIL");
        }
        //AA : modif pour être redirigé vers l'écran "Fiches à valider" quand on vient de valider une fiche.
        if (infoBean.get("ECRANVALIDER") != null) {
            urlRedirect = new StringBuilder(WebAppUtil.SG_PATH);
            urlRedirect.append("?PROC=VALIDATION&ACTION=LISTE");
        }
        if (infoBean.get("ETAT_FICHE_ENREGISTREE") != null) {
            urlRedirect.append("&ETAT_FICHE_ENREGISTREE=").append(EscapeString.escapeURL(infoBean.getString("ETAT_FICHE_ENREGISTREE")));
        }
        infoBean.setEcranRedirection(urlRedirect.toString());
    }

    private static void gestionRedirectionPourApercu(final InfoBean infoBean) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        String urlApercu = UrlManager.calculerUrlApercu(infoBean.getString("ID_METATAG"));
        String rubrique = infoBean.getString("CODE_RUBRIQUE");
        if (StringUtils.isEmpty(rubrique)) {
            rubrique = infoBean.getString("RH");
        }
        urlApercu = URLResolver.getAbsoluteUrl(urlApercu, ctx, serviceInfosSite.determineSite(rubrique, true));
        infoBean.setEcranRedirection(urlApercu);
    }

    /**
     * Formate le public visé pour l'affichage (on enleve les crochets et on calcule les libellés).
     *
     * @param infoBean
     *            the info bean
     * @param ctx
     *            the ctx
     * @param publicViseDsiEnregistre
     *            the public vise dsi enregistre
     * @param nomDonnee
     *            the nom donnee
     *
     */
    public static void formaterPublicViseDsiDansInfoBean(final InfoBean infoBean, final OMContext ctx, final String publicViseDsiEnregistre, final String nomDonnee) {
        String publicViseDsiSaisi = StringUtils.EMPTY, libellePublicViseDsi = StringUtils.EMPTY;
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        final StringTokenizer st = new StringTokenizer(publicViseDsiEnregistre, ";");
        while (st.hasMoreTokens()) {
            String item = st.nextToken();
            if (item.indexOf("[") == 0) {
                // On enleve les crochets
                item = item.substring(1, item.length() - 1);
            }
            // On enleve le / (historique par rapport au profil)
            if (item.startsWith("/")) {
                item = item.substring(1);
            }
            if (publicViseDsiSaisi.length() > 0) {
                publicViseDsiSaisi += ";";
            }
            // On enleve les crochets
            publicViseDsiSaisi += item;
            //Construction du libelle
            String libelleTmp = StringUtils.EMPTY;
            final int iSlash = item.indexOf("/");
            if (iSlash > 0) {
                libelleTmp += serviceProfildsi.getDisplayableLabel(item.substring(0, iSlash));
            }
            if (iSlash < item.length() - 1) {
                if (libelleTmp.length() > 0) {
                    libelleTmp += "/";
                }
                libelleTmp += serviceGroupeDsi.getIntitules(item.substring(iSlash + 1));
            }
            if (libellePublicViseDsi.length() > 0) {
                libellePublicViseDsi += ";";
            }
            libellePublicViseDsi += libelleTmp;
        }
        infoBean.set(nomDonnee, publicViseDsiSaisi);
        infoBean.set("LIBELLE_" + nomDonnee, libellePublicViseDsi);
    }

    /**
     * Formate le public visé pour l'enregistrement.
     *
     * @param publicViseDsiSaisi
     *            the public vise dsi saisi
     *
     * @return the string
     *
     */
    public static String formaterPublicViseDsiPourEnregistrement(final String publicViseDsiSaisi) {
        String publicViseDsiEnregistre = "";
        final StringTokenizer st = new StringTokenizer(publicViseDsiSaisi, ";");
        while (st.hasMoreTokens()) {
            final String item = st.nextToken();
            if (publicViseDsiEnregistre.length() > 0) {
                publicViseDsiEnregistre += ";";
            }
            publicViseDsiEnregistre += "[/" + item + "]";
        }
        return publicViseDsiEnregistre;
    }

    /**
     * Affichage d'un pop-up contenant la liste des fiches sélectionnées dans une requête.
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @param processus
     *            the processus
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     * @deprecated n'est plus appelé dans les popups
     */
    @Deprecated
    public static int preparerLISTE_POPUP(final InfoBean infoBean, FicheUniv ficheUniv, final SaisieFiche processus) throws Exception {
        final TreeMap<String, FicheUniv> listeFiches = new TreeMap<>();
        // La hashtable permet de gérer l'unicité (fiches ayant plusieurs états)
        final Hashtable<String, String> listeCodesLangues = new Hashtable<>();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        /*
        Stockage des fiches dans TreeMap trié sous la forme

            {0/langue/intitule/code; ficheUniv}
                si la langue est celle demandée
                (permet un tri par intitulé)
            ou
            {1/langue/intitule/code; ficheUniv}
                sinon (permet un tri par langue, puis par intitulé)

        (le code dans la cle permet de distinguer 2 fiches ayant le même libellé)

        --------------------------------------------

        La hashtable ListeCodesLangues permet
            - d'assurer l'unicité des combinaisons (CODE, LANGUE)
            - de stocker l'état le + proche de la mise en ligne
         */
        while (ficheUniv.nextItem()) {
            if ("0001".equals(ficheUniv.getEtatObjet()) || "0002".equals(ficheUniv.getEtatObjet()) || "0003".equals(ficheUniv.getEtatObjet())) {
                // On vérifie que la combinaison CODE/LANGUE n'a pas déjà été stockée
                // Calcul l'état le + proche de la mise en ligne
                // BROUILLON < A VALIDER < EN LIGNE
                boolean aStocker = true;
                String codeLangue = ficheUniv.getCode() + "/" + ficheUniv.getLangue();
                String intitule = listeCodesLangues.get(codeLangue);
                if (intitule != null) // élément déjà présent dans la liste
                {
                    // on teste si il existe un état plus proche de la mise en ligne
                    String etat = intitule.substring(intitule.lastIndexOf("/") + 1);
                    if (ficheUniv.getEtatObjet().compareTo(etat) > 0) {
                        // on supprime l'ancien état de la fiche
                        listeFiches.remove(intitule);
                        listeCodesLangues.remove(codeLangue);
                    } else {
                        aStocker = false;
                    }
                }
                if (aStocker) {
                    // On met 0 devant l'intitulé pour mettre les fiches de la langue de l'article en premier
                    if (ficheUniv.getLangue().equals(infoBean.getString("LANGUE_FICHE"))) {
                        intitule = "0/";
                    } else {
                        intitule = "1/";
                    }
                    intitule += ficheUniv.getLangue() + "/";
                    final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(ficheUniv.getCodeRubrique());
                    String intituleRubrique = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
                    if (rubriqueBean != null) {
                        intituleRubrique = rubriqueBean.getIntitule();
                    }
                    intitule += ficheUniv.getLibelleAffichable() + " [" + intituleRubrique + "]";
                    intitule += "/" + ficheUniv.getCode();
                    intitule += "/" + ficheUniv.getEtatObjet();
                    FicheUniv cloneFiche = ficheUniv.getClass().newInstance();
                    cloneFiche.setIdFiche(ficheUniv.getIdFiche());
                    cloneFiche.setCtx(processus);
                    cloneFiche.retrieve();
                    listeFiches.put(intitule.toUpperCase(), cloneFiche);
                    listeCodesLangues.put(codeLangue, intitule.toUpperCase());
                }
            }
        }
        // Parcours du tableau trié
        int i = 0;
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        for (FicheUniv currentFiche : listeFiches.values()) {
            RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(currentFiche.getCodeRubrique());
            String intituleRubrique = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
            if (rubriqueBean != null) {
                intituleRubrique = rubriqueBean.getIntitule();
            }
            String intitule = currentFiche.getLibelleAffichable() + " [" + intituleRubrique + "]";
            List<String> listeRubriques = null;
            if (infoBean.get("FCK_PLUGIN") != null) {
                listeRubriques = new ArrayList<>(serviceRubriquePublication.getRubriqueDestByFicheUniv(currentFiche));
                if (currentFiche.getCodeRubrique().length() > 0) {
                    listeRubriques.add(0, currentFiche.getCodeRubrique());
                }
            }
            String sListeCodesRubrique = "";
            String sListeLibellesRubrique = "";
            if (listeRubriques != null && listeRubriques.size() > 1) {
                for (String listeRubrique : listeRubriques) {
                    if (sListeCodesRubrique.length() > 0) {
                        sListeCodesRubrique += ";;";
                        sListeLibellesRubrique += ";;";
                    }
                    final String libelle = serviceRubrique.getLabelWithAscendantsLabels(listeRubrique);
                    sListeCodesRubrique += listeRubrique;
                    sListeLibellesRubrique += libelle.length() > 0 ? libelle : "--";
                }
                sListeLibellesRubrique = StringUtils.replace(sListeLibellesRubrique, "'", "\\'");
                sListeLibellesRubrique = StringUtils.replace(sListeLibellesRubrique, "\"", "&#34;");
            }
            infoBean.set("CODE#" + i, currentFiche.getCode());
            infoBean.set("LANGUE#" + i, currentFiche.getLangue());
            infoBean.set("LIBELLE#" + i, intitule);
            infoBean.set("ETAT_OBJET#" + i, currentFiche.getEtatObjet());
            infoBean.set("LISTE_CODES_RUBRIQUE#" + i, sListeCodesRubrique);
            infoBean.set("LISTE_LIBELLES_RUBRIQUE#" + i, sListeLibellesRubrique);
            infoBean.set("DOCUMENT#" + i, "0");
            // Variable qui force le telechargement de fichiers sur les documents
            if (infoBean.get("FCK_PLUGIN") != null) {
                final MetatagBean metatag = MetatagUtils.lireMeta(currentFiche);
                if ("1".equals(metatag.getMetaDocumentFichiergw())) {
                    infoBean.set("DOCUMENT#" + i, "2");
                } else {
                    infoBean.set("DOCUMENT#" + i, "1");
                }
            }
            try {
                final Method m = currentFiche.getClass().getMethod("getUrl");
                infoBean.set("URL#" + i, m.invoke(currentFiche));
            } catch (final IllegalArgumentException | ReflectiveOperationException | SecurityException e) {
                LOG.debug("unable to invoque the given method", e);
            }
            i++;
        }
        return listeFiches.size();
    }

    /**
     * Vérifie entre les deux fiches fourni en paramètre si elles ont :
     * les mêmes structures/ structure 2ndaire,
     * la même rubrique
     * le même public visé
     *
     * @param ficheUnivAvant
     *            the fiche univ avant
     * @param ficheUnivApres
     *            the fiche univ apres
     *
     * @return true, if perimetre modifie
     *
     */
    private static boolean perimetreModifie(final FicheUniv ficheUnivAvant, final FicheUniv ficheUnivApres) {
        boolean res = false;
        if (!ficheUnivAvant.getCodeRattachement().equals(ficheUnivApres.getCodeRattachement())) {
            res = true;
        }
        if (!ficheUnivAvant.getCodeRubrique().equals(ficheUnivApres.getCodeRubrique())) {
            res = true;
        }
        if (ficheUnivAvant instanceof FicheRattachementsSecondaires && (!((FicheRattachementsSecondaires) ficheUnivAvant).getCodeRattachementAutres().equals(((FicheRattachementsSecondaires) ficheUnivApres).getCodeRattachementAutres()))) {
            res = true;
        }
        if (ficheUnivAvant instanceof DiffusionSelective && ((!((DiffusionSelective) ficheUnivAvant).getDiffusionPublicVise().equals(((DiffusionSelective) ficheUnivApres).getDiffusionPublicVise())) || !(((DiffusionSelective) ficheUnivAvant).getDiffusionModeRestriction().equals(((DiffusionSelective) ficheUnivApres).getDiffusionModeRestriction())) || !(((DiffusionSelective) ficheUnivAvant).getDiffusionPublicViseRestriction().equals(((DiffusionSelective) ficheUnivApres).getDiffusionPublicViseRestriction())))) {
            res = true;
        }
        return res;
    }

    private static String getCodeAuteur(final AbstractProcessusBean processus) {
        final Object codeAuteur_tmp = processus.getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE);
        String codeAuteur = ServiceUser.UTILISATEUR_ANONYME;
        if (codeAuteur_tmp != null) {
            codeAuteur = codeAuteur_tmp.toString();
        }
        return codeAuteur;
    }
}
