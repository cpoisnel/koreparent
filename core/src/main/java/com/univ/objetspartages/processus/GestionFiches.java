package com.univ.objetspartages.processus;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.AbstractRequeteur;
import com.univ.utils.Chaine;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.IRequeteurConstantes;
import com.univ.utils.RequeteurFiches;
import com.univ.utils.URLResolver;

/**
 * processus d'affichage des contributions dans les espaces collaboratifs.
 */
public class GestionFiches extends ProcessusBean implements IRequeteurConstantes {

    private static final Logger LOG = LoggerFactory.getLogger(GestionFiches.class);

    /** The Constant ECRAN_LISTE. */
    private static final String ECRAN_LISTE = "LISTE";

    private final ServiceUser serviceUser;

    private final EspaceCollaboratifDAO espaceCollaboratifDAO = ApplicationContextManager.getCoreContextBean(EspaceCollaboratifDAO.ID_BEAN, EspaceCollaboratifDAO.class);

    /** The autorisations. */
    private AutorisationBean autorisations;

    /**
     * processus recherche utilisateur.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public GestionFiches(final InfoBean ciu) {
        super(ciu);
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Affichage d'une liste de contributions.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerLISTE() throws Exception {
        final AbstractRequeteur requeteur = new RequeteurFiches(infoBean);
        final String codeUtilisateur = (String) getGp().getSessionUtilisateur().getInfos().get("CODE");
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        // Pour chaque variable on va regarder si elle est stockée dans l'infoBean ou dans la requete (note FBI : pourquoi ??)
        // LIBELLE
        String libelle = "";
        if (infoBean.get(ATTRIBUT_LIBELLE_FICHE) != null) {
            libelle = infoBean.getString(ATTRIBUT_LIBELLE_FICHE);
            requeteur.setAttribut(ATTRIBUT_LIBELLE_FICHE, libelle, true, true);
        } else {
            libelle = requeteur.getAttribut(ATTRIBUT_LIBELLE_FICHE).getValeur();
        }
        infoBean.set(ATTRIBUT_LIBELLE_FICHE, libelle);
        infoBean.set(ATTRIBUT_LIBELLE_FICHE + "_REQ", requeteur.getAttribut(ATTRIBUT_LIBELLE_FICHE).getRequetable());
        infoBean.set(ATTRIBUT_LIBELLE_FICHE + "_TRI", requeteur.getAttribut(ATTRIBUT_LIBELLE_FICHE).getTriable());
        // LANGUE
        String langue = "";
        if (isInfoBeanAttributeValued(ATTRIBUT_LANGUE)) {
            langue = infoBean.getString(ATTRIBUT_LANGUE);
            if (CODE_DYNAMIQUE.equals(langue)) {
                langue = LangueUtil.getLangueLocale(this.getLocale());
            }
            requeteur.setAttribut(ATTRIBUT_LANGUE, langue, true, true);
        } else {
            langue = requeteur.getAttribut(ATTRIBUT_LANGUE).getValeur();
        }
        infoBean.set(ATTRIBUT_LANGUE, langue);
        final Hashtable<String, String> hash = LangueUtil.getListeLangues();
        infoBean.set("LISTE_LANGUES", hash);
        // on n'affiche pas l'option des langues si une seule langue est configurée pour le site
        if (hash.size() > 1) {
            infoBean.set(ATTRIBUT_LANGUE + "_REQ", requeteur.getAttribut(ATTRIBUT_LANGUE).getRequetable());
            infoBean.set(ATTRIBUT_LANGUE + "_TRI", requeteur.getAttribut(ATTRIBUT_LANGUE).getTriable());
        } else {
            infoBean.set(ATTRIBUT_LANGUE + "_REQ", "0");
            infoBean.set(ATTRIBUT_LANGUE + "_TRI", "0");
        }
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String typeDate = requeteur.getAttribut(ATTRIBUT_TYPE_DATE).getValeur();
        if (typeDate == null || "".equals(typeDate)) {
            typeDate = TYPE_DATE_MODIFICATION; //valeur par défaut
        }
        infoBean.set(ATTRIBUT_TYPE_DATE, typeDate);
        infoBean.set(ATTRIBUT_TYPE_DATE + "_REQ", requeteur.getAttribut(ATTRIBUT_TYPE_DATE).getRequetable());
        infoBean.set(ATTRIBUT_TYPE_DATE + "_TRI", requeteur.getAttribut(ATTRIBUT_TYPE_DATE).getTriable());
        // DATE_DEBUT
        Date dateDebut = null;
        if (infoBean.get(ATTRIBUT_DATE_DEBUT) != null) {
            dateDebut = (Date) infoBean.get(ATTRIBUT_DATE_DEBUT);
            requeteur.setAttribut(ATTRIBUT_DATE_DEBUT, sdf.format(dateDebut), true, true);
        } else {
            try {
                //si on est dans l'écran requeteur et que la date de fin a été saisie, on initialise la date de début
                if (!ECRAN_LISTE.equals(ecranLogique) || infoBean.get(ATTRIBUT_DATE_FIN) == null) {
                    dateDebut = new Date(sdf.parse(requeteur.getAttribut(ATTRIBUT_DATE_DEBUT).getValeur()).getTime());
                }
            } catch (final ParseException e) {
                LOG.debug("unable to parse the start date", e);
            }
        }
        infoBean.set(ATTRIBUT_DATE_DEBUT, dateDebut);
        infoBean.set(ATTRIBUT_DATE_DEBUT + "_REQ", requeteur.getAttribut(ATTRIBUT_DATE_DEBUT).getRequetable());
        // DATE_FIN
        Date dateFin = null;
        if (infoBean.get(ATTRIBUT_DATE_FIN) != null) {
            dateFin = (Date) infoBean.get(ATTRIBUT_DATE_FIN);
            requeteur.setAttribut(ATTRIBUT_DATE_FIN, sdf.format(dateFin), true, true);
        } else {
            try {
                // si on est dans l'écran requeteur et que la date de début a été saisie, on initialise la date de fin
                if (!ECRAN_LISTE.equals(ecranLogique) || infoBean.get(ATTRIBUT_DATE_DEBUT) == null) {
                    dateFin = new Date(sdf.parse(requeteur.getAttribut(ATTRIBUT_DATE_FIN).getValeur()).getTime());
                }
            } catch (final ParseException e) {
                LOG.debug("unable to parse the end date", e);
            }
        }
        infoBean.set(ATTRIBUT_DATE_FIN, dateFin);
        infoBean.set(ATTRIBUT_DATE_FIN + "_REQ", requeteur.getAttribut(ATTRIBUT_DATE_FIN).getRequetable());
        // REDACTEUR
        String redacteur = "";
        String codeRedacteur = "";
        if (infoBean.get(ATTRIBUT_CODE_REDACTEUR) != null) {
            codeRedacteur = infoBean.getString(ATTRIBUT_CODE_REDACTEUR);
            requeteur.setAttribut(ATTRIBUT_CODE_REDACTEUR, codeRedacteur, false, false);
        } else if (infoBean.get(ATTRIBUT_REDACTEUR) != null) {
            redacteur = infoBean.getString(ATTRIBUT_REDACTEUR);
            if (redacteur.length() > 0 && requeteur.getAttribut(ATTRIBUT_REDACTEUR).getValeur().equals(redacteur)) {
                codeRedacteur = requeteur.getAttribut(ATTRIBUT_CODE_REDACTEUR).getValeur();
            } else {
                requeteur.setAttribut(ATTRIBUT_REDACTEUR, redacteur, false, false);
            }
        } else {
            codeRedacteur = requeteur.getAttribut(ATTRIBUT_CODE_REDACTEUR).getValeur();
            if (codeRedacteur.equals(CODE_DYNAMIQUE) && codeUtilisateur != null) {
                codeRedacteur = codeUtilisateur;
            } else if (codeRedacteur.equals(SANS_CODE_DYNAMIQUE) && codeUtilisateur != null) {
                codeRedacteur = SANS_CODE_DYNAMIQUE + codeUtilisateur;
            }
            redacteur = requeteur.getAttribut(ATTRIBUT_REDACTEUR).getValeur();
            if ("".equals(redacteur)) {
                requeteur.setAttribut(ATTRIBUT_CODE_REDACTEUR, codeRedacteur, AbstractRequeteur.getBooleanValue(requeteur.getAttribut(ATTRIBUT_CODE_REDACTEUR).getRequetable()), AbstractRequeteur.getBooleanValue(requeteur.getAttribut(ATTRIBUT_CODE_REDACTEUR).getTriable()));
            }
        }
        infoBean.set(ATTRIBUT_CODE_REDACTEUR, codeRedacteur);
        infoBean.set(ATTRIBUT_REDACTEUR, redacteur);
        infoBean.set(ATTRIBUT_CODE_REDACTEUR + "_REQ", requeteur.getAttribut(ATTRIBUT_CODE_REDACTEUR).getRequetable());
        infoBean.set(ATTRIBUT_CODE_REDACTEUR + "_TRI", requeteur.getAttribut(ATTRIBUT_CODE_REDACTEUR).getTriable());
        // DSI
        String diffusionPublicVise = "";
        if (isInfoBeanAttributeValued(ATTRIBUT_DIFFUSION_PUBLIC_VISE)) {
            diffusionPublicVise = infoBean.getString(ATTRIBUT_DIFFUSION_PUBLIC_VISE);
            requeteur.setAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE, diffusionPublicVise, true, true);
        } else {
            diffusionPublicVise = requeteur.getAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE).getValeur();
        }
        infoBean.set(ATTRIBUT_DIFFUSION_PUBLIC_VISE, diffusionPublicVise);
        infoBean.set(ATTRIBUT_DIFFUSION_PUBLIC_VISE + "_REQ", requeteur.getAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE).getRequetable());
        infoBean.set(ATTRIBUT_DIFFUSION_PUBLIC_VISE + "_TRI", requeteur.getAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE).getTriable());
        String diffusionModeRestriction = ""; // valeurs possibles : "", "2+4", "4"
        if (infoBean.get(ATTRIBUT_DIFFUSION_MODE_RESTRICTION) != null) {
            diffusionModeRestriction = infoBean.getString(ATTRIBUT_DIFFUSION_MODE_RESTRICTION);
            requeteur.setAttribut(ATTRIBUT_DIFFUSION_MODE_RESTRICTION, diffusionModeRestriction, false, false);
        } else {
            diffusionModeRestriction = requeteur.getAttribut(ATTRIBUT_DIFFUSION_MODE_RESTRICTION).getValeur();
        }
        infoBean.set(ATTRIBUT_DIFFUSION_MODE_RESTRICTION, diffusionModeRestriction);
        String diffusionPublicViseRestriction = "";
        if (infoBean.get(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION) != null) {
            diffusionPublicViseRestriction = infoBean.getString(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION);
            requeteur.setAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION, diffusionPublicViseRestriction, false, false);
        } else {
            diffusionPublicViseRestriction = requeteur.getAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION).getValeur();
        }
        // cas particulier des espaces collaboratifs
        String codeEspaceCollaboratif = "";
        if (infoBean.get("ESPACE") != null && infoBean.getString("ESPACE").length() > 0) {
            codeEspaceCollaboratif = infoBean.getString("ESPACE");
        }
        if (StringUtils.isNotEmpty(codeEspaceCollaboratif)) {
            diffusionModeRestriction = "4";
            requeteur.setAttribut(ATTRIBUT_DIFFUSION_MODE_RESTRICTION, diffusionModeRestriction, false, false);
            diffusionPublicViseRestriction = codeEspaceCollaboratif;
            requeteur.setAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION, codeEspaceCollaboratif, false, false);
        } else if ("4".equals(diffusionModeRestriction) && diffusionPublicViseRestriction.length() > 0) {
            infoBean.set("ESPACE", diffusionPublicViseRestriction);
            codeEspaceCollaboratif = diffusionPublicViseRestriction;
        }
        infoBean.set(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION, diffusionPublicViseRestriction);
        infoBean.set(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION + "_REQ", requeteur.getAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION).getRequetable());
        infoBean.set(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION + "_TRI", requeteur.getAttribut(ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION).getTriable());
        // RUBRIQUE
        String rubrique = "";
        if (isInfoBeanAttributeValued(ATTRIBUT_CODE_RUBRIQUE)) {
            rubrique = infoBean.getString(ATTRIBUT_CODE_RUBRIQUE);
            requeteur.setAttribut(ATTRIBUT_CODE_RUBRIQUE, rubrique, true, true);
        } else {
            rubrique = requeteur.getAttribut(ATTRIBUT_CODE_RUBRIQUE).getValeur();
        }
        infoBean.set(ATTRIBUT_CODE_RUBRIQUE, rubrique);
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(rubrique);
        String libelleRubrique = StringUtils.EMPTY;
        if (rubriqueCourante != null) {
            libelleRubrique = rubriqueCourante.getIntitule();
            if (libelleRubrique.contains("#")) {
                libelleRubrique = libelleRubrique.substring(libelleRubrique.indexOf("#") + 1);
            }
        }
        infoBean.set("LIBELLE_" + ATTRIBUT_CODE_RUBRIQUE, libelleRubrique);
        infoBean.set(ATTRIBUT_CODE_RUBRIQUE + "_REQ", requeteur.getAttribut(ATTRIBUT_CODE_RUBRIQUE).getRequetable());
        infoBean.set(ATTRIBUT_CODE_RUBRIQUE + "_TRI", requeteur.getAttribut(ATTRIBUT_CODE_RUBRIQUE).getTriable());
        // RATTACHEMENT
        String rattachement = "";
        if (isInfoBeanAttributeValued(ATTRIBUT_CODE_RATTACHEMENT)) {
            rattachement = infoBean.getString(ATTRIBUT_CODE_RATTACHEMENT);
            requeteur.setAttribut(ATTRIBUT_CODE_RATTACHEMENT, rattachement, true, true);
        } else {
            rattachement = requeteur.getAttribut(ATTRIBUT_CODE_RATTACHEMENT).getValeur();
        }
        infoBean.set(ATTRIBUT_CODE_RATTACHEMENT, rattachement);
        infoBean.set("LIBELLE_" + ATTRIBUT_CODE_RATTACHEMENT, serviceStructure.getDisplayableLabel(rattachement, langue));
        infoBean.set(ATTRIBUT_CODE_RATTACHEMENT + "_REQ", requeteur.getAttribut(ATTRIBUT_CODE_RATTACHEMENT).getRequetable());
        infoBean.set(ATTRIBUT_CODE_RATTACHEMENT + "_TRI", requeteur.getAttribut(ATTRIBUT_CODE_RATTACHEMENT).getTriable());
        String code0bjet = "";
        if (infoBean.get(ATTRIBUT_CODE_OBJET) != null) {
            code0bjet = infoBean.getString(ATTRIBUT_CODE_OBJET);
            requeteur.setAttribut(ATTRIBUT_CODE_OBJET, code0bjet, true, true);
        } else {
            code0bjet = requeteur.getAttribut(ATTRIBUT_CODE_OBJET).getValeur();
        }
        final Hashtable<String, String> hashObjets = new Hashtable<>();
        String listeObjets = requeteur.getAttribut(LISTE_OBJETS).getValeur().toUpperCase();
        String nomObjet = "";
        if (listeObjets.length() == 0) {
            listeObjets = code0bjet.toUpperCase();
        }
        if (listeObjets.length() > 0) {
            final String[] listeObjetsTab = listeObjets.split(",");
            for (final String element : listeObjetsTab) {
                nomObjet = element.toUpperCase();
                hashObjets.put(nomObjet, ReferentielObjets.getLibelleObjet(ReferentielObjets.getCodeObjet(nomObjet)));
            }
        } else {
            // pas de liste d'objets prévalorisée, on ajoute tout
            String libelleObjet = "";
            if (StringUtils.isNotEmpty(codeEspaceCollaboratif)) {
                final EspaceCollaboratifBean espace = espaceCollaboratifDAO.getByCode(codeEspaceCollaboratif);
                final Collection<String> services = new ArrayList<>(Chaine.getVecteurPointsVirgules(espace.getServices()));
                for (final String service : services) {
                    if (listeObjets.length() > 0) {
                        listeObjets += ",";
                    }
                    listeObjets += service;
                    hashObjets.put(service, ReferentielObjets.getLibelleObjet(service));
                }
            } else {
                for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
                    nomObjet = ReferentielObjets.getNomObjet(codeObjet);
                    libelleObjet = ReferentielObjets.getLibelleObjet(codeObjet);
                    if (listeObjets.length() > 0) {
                        listeObjets += ",";
                    }
                    listeObjets += nomObjet;
                    hashObjets.put(nomObjet, libelleObjet);
                }
            }
        }
        infoBean.set("LISTE_CODE_OBJET", hashObjets);
        infoBean.set(ATTRIBUT_CODE_OBJET, code0bjet.toUpperCase());
        infoBean.set(ATTRIBUT_CODE_OBJET + "_REQ", requeteur.getAttribut(ATTRIBUT_CODE_OBJET).getRequetable());
        infoBean.set(ATTRIBUT_CODE_OBJET + "_TRI", requeteur.getAttribut(ATTRIBUT_CODE_OBJET).getTriable());
        if (code0bjet.length() == 0 || "TOUS".equals(code0bjet) || "0000".equals(code0bjet)) {
            code0bjet = listeObjets;
            requeteur.setAttribut(ATTRIBUT_CODE_OBJET, code0bjet, AbstractRequeteur.getBooleanValue(requeteur.getAttribut(ATTRIBUT_CODE_OBJET).getRequetable()), AbstractRequeteur.getBooleanValue(requeteur.getAttribut(ATTRIBUT_CODE_OBJET).getTriable()));
        }
        requeteur.setAttribut(LISTE_OBJETS, listeObjets, false, false);
        infoBean.set(LISTE_OBJETS, code0bjet);
        // ETAT
        String etatObjet = "";
        if (infoBean.get(ATTRIBUT_ETAT_OBJET) != null) {
            etatObjet = infoBean.getString(ATTRIBUT_ETAT_OBJET);
            requeteur.setAttribut(ATTRIBUT_ETAT_OBJET, etatObjet, true, true);
        } else {
            etatObjet = requeteur.getAttribut(ATTRIBUT_ETAT_OBJET).getValeur();
        }
        infoBean.set(ATTRIBUT_ETAT_OBJET, etatObjet);
        infoBean.set(ATTRIBUT_ETAT_OBJET + "_REQ", requeteur.getAttribut(ATTRIBUT_ETAT_OBJET).getRequetable());
        infoBean.set(ATTRIBUT_ETAT_OBJET + "_TRI", requeteur.getAttribut(ATTRIBUT_ETAT_OBJET).getTriable());
        // TITRE
        String titre = "";
        if (infoBean.get("TITRE") != null) {
            titre = infoBean.getString("TITRE");
            requeteur.setAttribut("TITRE", titre, false, false);
        } else {
            titre = requeteur.getAttribut("TITRE").getValeur();
        }
        String urlCourante = StringUtils.replace(requeteur.genererUrlRequete(), "\"", "&#34;");
        if (infoBean.getString("RF") != null) {
            urlCourante += "&amp;RF=" + infoBean.getString("RF");
        } else if (infoBean.getString("RH") != null) {
            urlCourante += "&amp;RF=" + infoBean.getString("RH");
        }
        infoBean.set(TITRE, titre);
        infoBean.setTitreEcran(titre);
        infoBean.set(MODE, requeteur.getMode());
        infoBean.set(ACTIONS, requeteur.getSelection());
        infoBean.set(AVEC_PAGINATION, requeteur.getPagination());
        infoBean.set(AVEC_AJOUT, requeteur.getAjoutLien());
        infoBean.set(REQUETE, StringUtils.replace(requeteur.getAttributRequete(), "\"", "&#34;"));
        infoBean.set("URL_REDIRECT", urlCourante);
        // PAGINATION
        int indexDebut = 0;
        if (infoBean.getString("FROM") != null) {
            indexDebut = Integer.parseInt(infoBean.getString("FROM"));
        }
        infoBean.setInt("FROM", indexDebut);
        int increment = 10;
        if (infoBean.getString("INCREMENT") != null) {
            try {
                increment = Integer.parseInt(infoBean.getString("INCREMENT"));
                if (increment == 0) {
                    increment = 10;
                }
            } catch (final NumberFormatException e) {
                LOG.debug("unable to parse the INCREMENT attribute", e);
            }
        }
        infoBean.setInt("INCREMENT", increment);
        String tri = "";
        if (infoBean.get("TRI") != null && infoBean.getString("TRI").length() > 0) {
            tri = infoBean.getString("TRI");
        }
        String ordretri = "";
        if (infoBean.get("ORDRE_TRI") != null && infoBean.getString("ORDRE_TRI").length() > 0) {
            ordretri = infoBean.getString("ORDRE_TRI");
        }
        infoBean.set("ORDRE_TRI", ordretri);
        infoBean.set("TRI", tri);
        final Map<String, Object> tMap = requeteur.getListeFiches(code0bjet, etatObjet, libelle, codeRedacteur, rubrique, rattachement, typeDate, dateDebut, dateFin, diffusionPublicVise, diffusionModeRestriction, diffusionPublicViseRestriction, langue, tri, ordretri, indexDebut, increment, requeteur.getMode(), REQUETE_AVEC_COUNT);
        int nbItemsTotal = 0;
        try {
            nbItemsTotal = (Integer) tMap.get(String.valueOf(NB_RESULTAT));
        } catch (final Exception e) {
            LOG.debug("unable to get the total number of item", e);
        }
        tMap.remove(String.valueOf(NB_RESULTAT));
        int i = 0;
        String libelleService = "";
        String libelleGroupes = "";
        boolean afficheAction = false;
        final TreeMap<Integer, FicheUniv> tMapInfoBean = new TreeMap<>();
        for (final Map.Entry<String, Object> donneeFiche : tMap.entrySet()) {
            final MetatagBean bean = (MetatagBean) donneeFiche.getValue();
            final Long idFiche = bean.getMetaIdFiche();
            final String codeObjet = bean.getMetaCodeObjet();
            final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(codeObjet));
            if (ficheUniv != null) {
                ficheUniv.init();
                ficheUniv.setCtx(this);
                ficheUniv.setIdFiche(idFiche);
                try {
                    ficheUniv.retrieve();
                } catch (final Exception e) {
                    LOG.debug("the content was not found", e);
                }
            }
            nomObjet = ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjet(ficheUniv));
            libelleService = ReferentielObjets.getLibelleObjet(ReferentielObjets.getCodeObjet(ficheUniv));
            if (ficheUniv instanceof DiffusionSelective) {
                libelleGroupes = "";
                if (FicheUnivMgr.isFicheCollaborative(ficheUniv)) {
                    final EspaceCollaboratifBean infos = espaceCollaboratifDAO.getByCode(((DiffusionSelective) ficheUniv).getDiffusionPublicViseRestriction());
                    if (infos.getIntitule().length() > 0) {
                        libelleGroupes = infos.getIntitule();
                    }
                }
                if (libelleGroupes.length() == 0) {
                    final List<String> vTemp = Chaine.getVecteurPointsVirgules(((DiffusionSelective) ficheUniv).getDiffusionPublicVise());
                    for (String item : vTemp) {
                        item = item.substring(1, item.length() - 1);
                        final String[] temp = item.split("/", -2);
                        final String libelleGroupe = serviceGroupeDsi.getIntitule(temp[1]);
                        if (libelleGroupe.length() > 0) {
                            if (libelleGroupes.length() > 0) {
                                libelleGroupes += " / ";
                            }
                            libelleGroupes += libelleGroupe;
                        }
                    }
                }
                infoBean.set("DIFFUSION_PUBLIC_VISE#" + i, libelleGroupes);
            }
            /* Données à afficher */
            infoBean.set("INTITULE_FICHE#" + i, ficheUniv.getLibelleAffichable());
            infoBean.set("TYPE_FICHE#" + i, nomObjet.toUpperCase());
            infoBean.set("LIBELLE_FICHE#" + i, libelleService);
            infoBean.set("ID_FICHE#" + i, ficheUniv.getIdFiche().toString());
            infoBean.set("ETAT#" + i, ficheUniv.getEtatObjet());
            infoBean.set("CODE_RATTACHEMENT#" + i, serviceStructure.getDisplayableLabel(ficheUniv.getCodeRattachement(), langue));
            infoBean.set("LANGUE#" + i, "");
            infoBean.set("CODE_RUBRIQUE#" + i, "");
            infoBean.set("RUBRIQUE#" + i, "");
            infoBean.set("LANGUE#" + i, ficheUniv.getLangue());
            final String codeRubrique = ficheUniv.getCodeRubrique();
            infoBean.set("RUBRIQUE#" + i, codeRubrique);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
            libelleRubrique = StringUtils.EMPTY;
            if (rubriqueBean != null) {
                libelleRubrique = rubriqueBean.getIntitule();
                if (libelleRubrique.contains("#")) {
                    libelleRubrique = libelleRubrique.substring(libelleRubrique.indexOf("#") + 1);
                }
            }
            infoBean.set("CODE_RUBRIQUE#" + i, libelleRubrique);
            String urlFicheModification = "";
            String urlFicheSuppression = "";
            String parametres = "";
            final String ext = StringUtils.isNotEmpty(ReferentielObjets.getExtension(ficheUniv)) ? "EXT=" + ReferentielObjets.getExtension(ficheUniv) + "&amp;" : "";
            boolean actionPossible = false;
            if (FicheAnnotationHelper.isSaisieFrontOffice(ficheUniv)) {
                actionPossible = true;
                urlFicheModification = WebAppUtil.SG_PATH + "?" + ext + "ACTION=MODIFIER&amp;SAISIE_FRONT=TRUE";
                urlFicheSuppression = WebAppUtil.SG_PATH + "?" + ext + "ACTION=SUPPRIMER&amp;SAISIE_FRONT=TRUE&amp;TYPE_FICHE=" + nomObjet.toUpperCase();
                parametres = "&amp;ID_FICHE=" + ficheUniv.getIdFiche() + "&amp;PROC=SAISIE_" + nomObjet.toUpperCase() + "_FRONT" + "&amp;URL_REDIRECT=" + URLEncoder.encode(urlCourante, CharEncoding.DEFAULT);
                urlFicheSuppression += parametres;
                urlFicheModification += parametres;
                if (StringUtils.isNotEmpty(codeEspaceCollaboratif)) {
                    urlFicheModification += "&amp;ESPACE=" + codeEspaceCollaboratif;
                    urlFicheSuppression += "&amp;ESPACE=" + codeEspaceCollaboratif;
                }
                if (infoBean.get("RH") != null) {
                    urlFicheModification += "&amp;RH=" + infoBean.getString("RH");
                    urlFicheSuppression += "&amp;RH=" + infoBean.getString("RH");
                } else {
                    urlFicheModification += "&amp;RH=" + ficheUniv.getCodeRubrique();
                    urlFicheSuppression += "&amp;RH=" + ficheUniv.getCodeRubrique();
                }
                if (infoBean.get("LANGUE") != null) {
                    urlFicheModification += "&amp;LANGUE=" + infoBean.getString("LANGUE");
                    urlFicheSuppression += "&amp;LANGUE=" + infoBean.getString("LANGUE");
                }
            } else {
                urlFicheModification = "";
                urlFicheSuppression = "";
                if (autorisations != null && autorisations.possedeModeExpert()) {
                    actionPossible = true;
                    urlFicheModification = WebAppUtil.SG_PATH + "?" + ext + "ACTION=MODIFIER";
                    urlFicheSuppression = WebAppUtil.SG_PATH + "?" + ext + "ACTION=SUPPRIMER";
                    parametres = "&amp;ID_FICHE=" + ficheUniv.getIdFiche() + "&amp;PROC=SAISIE_" + nomObjet.toUpperCase();
                    urlFicheSuppression = "href=\"#\" onclick=\"window.open('" + URLResolver.getAbsoluteBoUrl(urlFicheSuppression + parametres, this.getInfosSite()) + "','_blank','');return false\"";
                    urlFicheModification = "href=\"#\" onclick=\"window.open('" + URLResolver.getAbsoluteBoUrl(urlFicheModification + parametres, this.getInfosSite()) + "','_blank','');return false\"";
                }
            }
            infoBean.set("URL_FICHE#" + i, urlFicheModification);
            infoBean.set("URL_MODIF_FICHE#" + i, urlFicheModification);
            infoBean.set("URL_SUPPR_FICHE#" + i, urlFicheSuppression);
            infoBean.set("DATE#" + i, ficheUniv.getDateModification());
            if (autorisations != null) {
                // action de suppression
                boolean autorisation = false;
                if (actionPossible && requeteur.getListeActions().contains(ACTION_SUPPRIMER)) {
                    autorisation = autorisations.estAutoriseASupprimerLaFiche(ficheUniv);
                }
                if (autorisation) {
                    infoBean.set("SUPPRIMER_FICHE#" + i, "1");
                    afficheAction = true;
                } else {
                    infoBean.set("SUPPRIMER_FICHE#" + i, "0");
                }
                // action de modification
                autorisation = false;
                if (actionPossible && requeteur.getListeActions().contains(ACTION_MODIFIER)) {
                    autorisation = autorisations.estAutoriseAModifierLaFiche(ficheUniv, true);
                }
                if (autorisation) {
                    infoBean.set("MODIFIER_FICHE#" + i, "1");
                    afficheAction = true;
                } else {
                    infoBean.set("MODIFIER_FICHE#" + i, "0");
                }
                // action de consultation
                autorisation = false;
                if ("0003".equals(ficheUniv.getEtatObjet()) && requeteur.getListeActions().contains(ACTION_VOIR)) {
                    autorisation = FicheUnivMgr.controlerRestriction(ficheUniv, this);
                }
                if (autorisation) {
                    infoBean.set("VOIR_FICHE#" + i, "1");
                    afficheAction = true;
                } else {
                    infoBean.set("VOIR_FICHE#" + i, "0");
                }
            }
            infoBean.set("REDACTEUR#" + i, serviceUser.getLibelle(ficheUniv.getCodeRedacteur()));
            tMapInfoBean.put(i, ficheUniv);
            i++;
        }
        if (afficheAction) {
            infoBean.set("ACTION_FICHE", "1");
        } else {
            infoBean.set("ACTION_FICHE", "0");
        }
        infoBean.set("TREE_FICHES", tMapInfoBean);
        infoBean.set("LISTE_NB_ITEMS", i);
        infoBean.setInt("NB_TOTAL", nbItemsTotal);
        if (increment != 0) {
            infoBean.setInt("NUM_PAGE", (indexDebut / increment) + 1);
            infoBean.setInt("NB_PAGES", ((nbItemsTotal - 1) / increment) + 1);
        }
        ecranLogique = ECRAN_LISTE;
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() {
        final Object o = getGp().getSessionUtilisateur().getInfos().get("AUTORISATIONS");
        if ((o == null) && (!MODE_CONSULTATION.equals(infoBean.getString("MODE")))) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            try {
                autorisations = (AutorisationBean) o;
                if (autorisations != null) {
                    this.getDatas().put("AUTORISATIONS", autorisations);
                }
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                etat = EN_COURS;
                /* Entrée par lien hyper-texte */
                if ("LISTE".equals(action) || "VALIDER".equals(action)) {
                    preparerLISTE();
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOG.debug("an error occured while processing", e);
                LOG.error("erreur lors du traitement du processus", e.getMessage());
                infoBean.addMessageErreur(e.toString());
            }
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Gets the processus.
     *
     * @return the processus
     */
    public String getProcessus() {
        return infoBean.getString("PROC");
    }

    private boolean isInfoBeanAttributeValued(final String attributeName) {
        return StringUtils.isNotBlank((String) infoBean.get(attributeName)) && !StringUtils.equals((String) infoBean.get(attributeName), "0000");
    }
}
