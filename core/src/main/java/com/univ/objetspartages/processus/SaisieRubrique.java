package com.univ.objetspartages.processus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.core.ProcessusHelper;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;
import com.kportal.extension.module.plugin.rubrique.IPageAccueilRubrique;
import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.univ.datagrid.processus.RubriqueDatagrid;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.multisites.InfosFicheComparator;
import com.univ.multisites.InfosFicheReferencee;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheObjet;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.objetspartages.util.CritereRechercheUtil;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.RechercheFicheHelper;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;
import com.univ.utils.recherche.RechercheMultificheHelper;
import com.univ.utils.recherche.ResultatRechercheMultifiche;

/**
 * Processus de saisie de rubrique.
 */
public class SaisieRubrique extends ProcessusBean {

    /**
     * The Constant MAX_FICHE_AFFICHAGE.
     */
    public static final int MAX_FICHE_AFFICHAGE = 100;

    private static final Logger LOGGER = LoggerFactory.getLogger(SaisieRubrique.class);

    /**
     * The Constant MODE_AJOUT.
     */
    private static final int MODE_AJOUT = 0;

    /**
     * The Constant MODE_MODIFICATION.
     */
    private static final int MODE_MODIFICATION = 1;

    /**
     * The Constant MODE_SUPPRESSION.
     */
    private static final int MODE_SUPPRESSION = 2;

    /**
     * The Constant MODE_RECHERCHE.
     */
    private static final int MODE_RECHERCHE = 3;

    /**
     * The Constant ECRAN_RECHERCHE.
     */
    private static final String ECRAN_RECHERCHE = "RECHERCHE";

    /**
     * The Constant ECRAN_PRINCIPAL.
     */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    private static final String ECRAN_ACCUEIL = "ACCUEIL";

    /**
     * The Constant ECRAN_LISTE.
     */
    private static final String ECRAN_LISTE = "LISTE";

    /**
     * The Constant ECRAN_RECHERCHE_FICHES_REFERENCEES.
     */
    private static final String ECRAN_RECHERCHE_FICHES_REFERENCEES = "RECHERCHE_FICHES_REFERENCEES";

    /**
     * The Constant ECRAN_AJOUT_REQUETE.
     */
    private static final String ECRAN_AJOUT_REQUETE = "AJOUT_REQUETE";

    /**
     * The Constant ECRAN_LISTE_FICHES_REFERENCEES.
     */
    private static final String ECRAN_LISTE_FICHES_REFERENCEES = "LISTE_FICHES_REFERENCEES";

    private final ServiceRubrique serviceRubrique;

    private final ServiceMetatag serviceMetatag;

    private final ServiceRubriquePublication serviceRubriquePublication;

    private final ServiceMedia serviceMedia;

    /**
     * The rubrique.
     */
    RubriqueBean rubriqueBean = null;

    /**
     * The liste identifiant.
     */
    Long listeIdentifiant[] = null;
    //JSS 20040419 : délégation

    /**
     * The autorisations.
     */
    AutorisationBean autorisations = null;

    /**
     * The liste fiches referencees.
     */
    TreeSet<InfosFicheReferencee> listeFichesReferencees = new TreeSet<>(new InfosFicheComparator());

    /**
     * The liste rubrique fille.
     */
    Collection<RubriqueBean> listeRubriqueFille = null;

    /**
     * The liste requete rubrique publication.
     */
    Vector<String> listeRequeteRubriquePublication = new Vector<>();

    /**
     * The mode.
     */
    private int mode = -1;

    /**
     * processus saisie Rubrique.
     *
     * @param ciu com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieRubrique(final InfoBean ciu) {
        super(ciu);
        serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
    }

    /**
     * Initialisation du processus.
     *
     * @throws Exception the exception
     */
    private void initialisation() throws Exception {
        // initialisation objets métiers
        rubriqueBean = new RubriqueBean();
        rubriqueBean.setCodeRubriqueMere(StringUtils.defaultString(infoBean.getString("CODE_RUBRIQUE_MERE")));
        etat = EN_COURS;
        this.getDatas().put("AUTORISATIONS", autorisations);
        // JSS 20040419 : Vérifications accès
        if ("AJOUTER".equals(action)) {
            if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "C"))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            mode = MODE_AJOUT;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
            preparerPRINCIPAL();
        }
        /* AM 200309 : modification d'une rubrique sans passer par la recherche */
        else if ("MODIFIERRUBRIQUE".equals(action)) {
            mode = MODE_MODIFICATION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            final Long idRub = Long.valueOf((String) infoBean.get("ID_RUBRIQUE"));
            rubriqueBean = serviceRubrique.getById(idRub);
            if (rubriqueBean == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_RUBRIQUE_INEXISTANTE"));
            }
            if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "M"), new Perimetre("*", rubriqueBean.getCode(), "*", "*", ""))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            preparerPRINCIPAL();
        }
        /* JSS 20040419 : idem MODIFIERRUBRIQUE mais par le code */
        else if ("MODIFIERRUBRIQUEPARCODE".equals(action)) {
            mode = MODE_MODIFICATION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            final String code = infoBean.getString("CODE_RUBRIQUE");
            if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "M"), new Perimetre("*", code, "*", "*", ""))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            rubriqueBean = serviceRubrique.getRubriqueByCode(code);
            if (rubriqueBean == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_RUBRIQUE_INEXISTANTE"));
            }
            preparerPRINCIPAL();
        } else if ("SUPPRIMERRUBRIQUEPARCODE".equals(action)) {
            final String code = infoBean.getString("CODE_RUBRIQUE");
            if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "S"), new Perimetre("*", code, "*", "*", ""))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            rubriqueBean = serviceRubrique.getRubriqueByCode(code);
            if (rubriqueBean == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_RUBRIQUE_INEXISTANTE"));
            }
            traiterSuppression(rubriqueBean.getIdRubrique());
        } else if ("MODIFIER".equals(action)) {
            if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "M")) && !autorisations.possedePermission(new PermissionBean("TECH", "rub", "S"))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            mode = MODE_MODIFICATION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            preparerRECHERCHE();
        } else if ("SUPPRIMER".equals(action)) {
            if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "S"))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            mode = MODE_SUPPRESSION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_SUPPRESSION);
            preparerRECHERCHE();
        } else if ("RECHERCHER".equals(action)) {
            if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "M"))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            mode = MODE_RECHERCHE;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
            preparerRECHERCHE();
        } else if ("ACCUEIL".equals(action)) {
            insererRechercheDansInfoBean(infoBean, new ArrayList<CritereRecherche>());
            ecranLogique = ECRAN_ACCUEIL;
        }
    }

    /**
     * Affichage de la liste des Rubriques.
     *
     * @throws Exception the exception
     */
    private void preparerLISTE() throws Exception {
        ecranLogique = ECRAN_LISTE;
    }

    /**
     * Affichage de l'écran de saisie d'un Rubrique.
     *
     * @throws Exception the exception
     */
    private void preparerPRINCIPAL() throws Exception {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        ecranLogique = ECRAN_PRINCIPAL;
        String code = rubriqueBean.getCode();
        if (StringUtils.isEmpty(code)) {
            code = String.valueOf(System.currentTimeMillis());
        }
        infoBean.set("CODE", code);
        infoBean.set("LISTE_TYPE_RUBRIQUE", PageAccueilRubriqueManager.getInstance().getListeTypesRubrique());
        infoBean.set("LANGUE", rubriqueBean.getLangue());
        infoBean.set("LISTE_LANGUES", LangueUtil.getListeLangues());
        infoBean.set("INTITULE", rubriqueBean.getIntitule());
        infoBean.set("ACCROCHE", rubriqueBean.getAccroche());
        Long idBandeau = rubriqueBean.getIdBandeau();
        if (idBandeau == null) {
            idBandeau = 0L;
        }
        infoBean.set("ID_BANDEAU", String.valueOf(idBandeau));
        infoBean.set("LISTE_BANDEAUX", MediaUtils.getMediasAsMap(serviceMedia.getByTypeAndTypeMedia("photo", "0100")));
        Long idPicto = rubriqueBean.getIdPicto();
        if (idPicto == null) {
            idPicto = 0L;
        }
        infoBean.set("ID_PICTO", String.valueOf(idPicto));
        infoBean.set("LISTE_PICTOS", MediaUtils.getMediasAsMap(serviceMedia.getByTypeAndTypeMedia("photo", "0104")));
        infoBean.set("LISTE_CATEGORIES", LabelUtils.getLabelCombo("80", LangueUtil.getLocale(rubriqueBean.getLangue())));
        infoBean.set("CATEGORIE", rubriqueBean.getCategorie());
        infoBean.set("COULEUR_TITRE", rubriqueBean.getCouleurTitre());
        infoBean.set("COULEUR_FOND", rubriqueBean.getCouleurFond());
        infoBean.set("NOM_ONGLET", rubriqueBean.getNomOnglet());
        infoBean.set("CONTACT", rubriqueBean.getContact());
        final RubriqueBean rubriqueMere = serviceRubrique.getRubriqueByCode(rubriqueBean.getCodeRubriqueMere());
        if (rubriqueMere != null) {
            infoBean.set("CODE_RUBRIQUE_MERE", rubriqueMere.getCode());
            infoBean.set("LIBELLE_CODE_RUBRIQUE_MERE", rubriqueMere.getIntitule());
        }
        infoBean.set("SAUVEGARDE_CODE_RUBRIQUE_MERE", rubriqueBean.getCodeRubriqueMere());
        // gestion délégation (filtre de l'arbre)
        infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE", "CODE_RUBRIQUE_MERE");
        infoBean.set("GRS_PERMISSION_TYPE", "TECH");
        infoBean.set("GRS_PERMISSION_OBJET", "rub");
        if (mode == MODE_AJOUT) {
            infoBean.set("GRS_PERMISSION_ACTION", "C");
        } else {
            infoBean.set("GRS_PERMISSION_ACTION", "M");
        }
        // 20060130 : FBI : filtre l'arbre des groupes en fonction des autorisations
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE", "1");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_TYPE", "TECH");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_OBJET", "dsi");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_ACTION", "");
        PageAccueilRubriqueManager.getInstance().preparerPRINCIPAL(infoBean, rubriqueBean);
        infoBean.set("GESTION_ENCADRE", rubriqueBean.getGestionEncadre());
        infoBean.set("ENCADRE", rubriqueBean.getEncadre());
        infoBean.set("ENCADRE_SOUS_RUBRIQUE", rubriqueBean.getEncadreSousRubrique());
        final String sTmp = Chaine.convertirPointsVirgules(Chaine.getVecteurAccolades(rubriqueBean.getGroupesDsi()));
        infoBean.set("GROUPES_DSI", sTmp);
        infoBean.set("LIBELLE_GROUPES_DSI", serviceGroupeDsi.getIntitules(sTmp));
        infoBean.set("GRS_SAISIE_DIFFUSION", autorisations.isDiffuseurDSI() ? "1" : "0");
        // multi-sites
        if (mode == MODE_MODIFICATION) {
            // On récupère la liste des fiches
            listeFichesReferencees.addAll(serviceRubriquePublication.getListeFichesReferences(rubriqueBean.getCode(), true));
        }
        listeRubriqueFille = serviceRubrique.getRubriqueByCodeParent(rubriqueBean.getCode());
        final Map<String, Collection<String>> rubriqueParRestriction = new HashMap<>();
        if (StringUtils.isNotBlank(rubriqueBean.getCodeRubriqueMere())) {
            String codeRubriqueCourante = rubriqueBean.getCodeRubriqueMere();
            while (codeRubriqueCourante.length() > 0) {
                final RubriqueBean infoRubriqueCourante = serviceRubrique.getRubriqueByCode(codeRubriqueCourante);
                @SuppressWarnings("unchecked")
                final Collection<String> intitulesGroupes = CollectionUtils.collect(Chaine.getHashSetAccolades(infoRubriqueCourante.getGroupesDsi()), new Transformer() {

                    @Override
                    public String transform(final Object input) {
                        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
                        return serviceGroupeDsi.getIntitules((String) input);
                    }
                });
                if (CollectionUtils.isNotEmpty(intitulesGroupes)) {
                    rubriqueParRestriction.put(infoRubriqueCourante.getIntitule(), intitulesGroupes);
                }
                codeRubriqueCourante = infoRubriqueCourante.getCodeRubriqueMere();
            }
        }
        infoBean.set("GROUPES_DSI_RESTRICTION", rubriqueParRestriction);
        // requete de rubriquage automatique
        listeRequeteRubriquePublication = Chaine.getVecteurAccolades(rubriqueBean.getRequetesRubriquePublication());
        final Hashtable<String, String> hashOjbets = new Hashtable<>();
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            final FicheUniv fiche = ReferentielObjets.instancierFiche(codeObjet);
            if (fiche != null && !(fiche instanceof FicheObjet) && FicheAnnotationHelper.isAccessibleBo(fiche)) {
                hashOjbets.put(ReferentielObjets.getNomObjet(codeObjet), ReferentielObjets.getLibelleObjet(codeObjet));
            }
        }
        infoBean.set("LISTE_OBJETS_REQUETE", hashOjbets);
        // droit de publication dans la rubrique
        if (PermissionUtil.controlerPermissionRubrique(autorisations, "TECH/pub/", rubriqueBean.getCode())) {
            infoBean.set("GRS_SAISIE_PUBLICATION", "1");
        }
        rafraichirPRINCIPAL();
        if (StringUtils.isNotBlank(rubriqueBean.getIntitule())) {
            infoBean.setTitreEcran(rubriqueBean.getIntitule());
        }
    }

    /**
     * Rafraichir principal.
     *
     * @throws Exception the exception
     */
    private void rafraichirPRINCIPAL() throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        // Copie des fiches référencées dans InfoBean
        infoBean.setInt("FICHES_REFERENCEES_NB_ITEMS", Math.min(listeFichesReferencees.size(), MAX_FICHE_AFFICHAGE));
        int i = 0;
        for (final InfosFicheReferencee infos : listeFichesReferencees) {
            // on n'affiche que les 100 premières fiches
            if (i == MAX_FICHE_AFFICHAGE) {
                break;
            }
            // Toutes les variables sont nécessaire pour traiter l'action
            // 'SUPPRIMER'
            infoBean.set("FICHE_REFERENCEE_INTITULE#" + i, infos.getIntitule());
            infoBean.set("FICHE_REFERENCEE_CODE#" + i, infos.getCode());
            infoBean.set("FICHE_REFERENCEE_LANGUE#" + i, infos.getLangue());
            infoBean.set("FICHE_REFERENCEE_TYPE#" + i, infos.getType());
            infoBean.set("FICHE_REFERENCEE_TYPE_LIBELLE#" + i, ReferentielObjets.getLibelleObjet(infos.getType()));
            infoBean.set("FICHE_REFERENCEE_ETAT_LIBELLE#" + i, ReferentielObjets.getLibelleEtatObjet(infos.getEtat()));
            final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(infos.getType()));
            ficheUniv.init();
            ficheUniv.setCtx(this);
            ficheUniv.setIdFiche(Long.valueOf(infos.getId()));
            String urlModification = StringUtils.EMPTY;
            try {
                ficheUniv.retrieve();
                if (autorisations.estAutoriseAModifierLaFiche(ficheUniv)) {
                    final String urlModif = ProcessusHelper.getUrlProcessAction(infoBean, ReferentielObjets.getExtension(ficheUniv), "SAISIE_" + ReferentielObjets.getNomObjet(infos.getType()).toUpperCase(), "MODIFIER", new String[][]{{"ID_FICHE", String.valueOf(ficheUniv.getIdFiche())}});
                    urlModification = "<a class=\"action modifier\" href=\"" + URLResolver.getAbsoluteUrl(urlModif, this) + "\" title=\"Modifier la fiche\">Modifier</a>";
                }
            } catch (final Exception e) {
                LOGGER.debug("unable to retrieve the given page", e);
            }
            final MetatagBean meta = MetatagUtils.lireMeta(ficheUniv);
            infoBean.set("FICHE_REFERENCEE_ID_META#" + i, meta.getId());
            infoBean.set("FICHE_REFERENCEE_URL_MODIF#" + i, urlModification);
            i++;
        }
        i = 0;
        /* sous rubriques*/
        infoBean.setInt("RUBRIQUES_FILLES_NB_ITEMS", listeRubriqueFille.size());
        for (final RubriqueBean infos : listeRubriqueFille) {
            String intitule = infos.getIntitule();
            if (intitule.contains("<BR>")) {
                intitule = StringUtils.replace(intitule, "<BR>", "");
            } else if (intitule.contains("<br>")) {
                intitule = StringUtils.replace(intitule, "<br>", "");
            }
            infoBean.set("RUBRIQUE_FILLE_CODE#" + i, infos.getCode());
            // lien de modification de la rubrique
            if (autorisations.possedePermission(new PermissionBean("TECH", "rub", "M"), new Perimetre("*", infos.getCode(), "*", "*", ""))) {
                infoBean.set("RUBRIQUE_FILLE_INTITULE#" + i, "<a href=\"" + WebAppUtil.SG_PATH + "?PROC=SAISIE_RUBRIQUE&amp;ACTION=MODIFIERRUBRIQUE&amp;ID_RUBRIQUE=" + infos.getIdRubrique() + "\" target=\"_blank\" title=\"Modifier la rubrique\"><span class=\"coco\">" + intitule + "</span></a>");
            } else {
                infoBean.set("RUBRIQUE_FILLE_INTITULE#" + i, intitule);
            }
            // lien vers la page de tête de la rubrique
            final BeanPageAccueil beanAccueil = PageAccueilRubriqueManager.getInstance().getBeanPageAccueil(infos);
            final IPageAccueilRubrique pageAccueil = PageAccueilRubriqueManager.getInstance().getPageAccueilRubrique(infos.getTypeRubrique());
            String actions = StringUtils.EMPTY;
            if (pageAccueil != null && beanAccueil != null) {
                final String libelle = beanAccueil.getLibelleAffichable();
                String url = beanAccueil.getUrlModification(infos.getCode(), infos.getLangue(), true);
                if (StringUtils.isNotEmpty(url)) {
                    actions += "<a href=\"#\" onclick=\"openRub('" + url + "');\" title=\"Modifier la page\">" + libelle + "</a>";
                } else {
                    actions += libelle;
                }
                if (StringUtils.isNotBlank(actions)) {
                    actions += " - ";
                }
                url = beanAccueil.getUrlRubrique(infos.getCode(), infos.getLangue(), true);
                if (StringUtils.isNotEmpty(url)) {
                    actions += "<a href=\"#\" onclick=\"openRub('" + url + "');\" title=\"Voir la page\">Aperçu</a>";
                }
                if (StringUtils.isNotBlank(actions)) {
                    actions = pageAccueil.getLibelleAffichable() + " : " + actions;
                }
            }
            infoBean.set("RUBRIQUE_FILLE_PAGE#" + i, actions);
            i++;
        }
        /* Copie des requetes automatiques */
        infoBean.setInt("REQUETE_NB_ITEMS", listeRequeteRubriquePublication.size());
        i = 0;
        for (final String requete : listeRequeteRubriquePublication) {
            final String[] item = requete.split("/", -2);
            final String codeObjet = ReferentielObjets.getCodeObjet(item[0]);
            String libelleObjet = "-";
            if (StringUtils.isNotBlank(codeObjet)) {
                libelleObjet = ReferentielObjets.getLibelleObjet(codeObjet);
            }
            String structure = item[1];
            if (structure.length() > 0) {
                final String[] temp = structure.split(";", -2);
                final StructureModele infosStructure = serviceStructure.getByCodeLanguage(temp[0], LangueUtil.getIndiceLocaleDefaut());
                if(infosStructure != null) {
                    if (infosStructure.getLibelleCourt().length() > 0) {
                        structure = infosStructure.getLibelleCourt();
                    } else {
                        structure = infosStructure.getLibelleLong();
                    }
                }
                if ("".equals(structure)) {
                    structure = "-( inexistante)";
                } else {
                    if ("1".equals(temp[1])) {
                        structure += " (sous structures incluses)";
                    }
                }
            } else {
                structure = "-";
            }
            final String codeRubrique = item[2];
            String libelleRubrique = StringUtils.EMPTY;
            if (codeRubrique.length() > 0) {
                final String[] temp = codeRubrique.split(";", -2);
                final RubriqueBean rubriquetemp = serviceRubrique.getRubriqueByCode(temp[0]);
                if (rubriquetemp != null) {
                    libelleRubrique = rubriquetemp.getIntitule();
                }
                if (codeRubrique.length() == 0) {
                    libelleRubrique = "- (inexistante)";
                } else {
                    if ("1".equals(temp[1])) {
                        libelleRubrique += " (sous rubriques incluses)";
                    }
                }
            } else {
                libelleRubrique = "-";
            }
            String dernierItem = item[3];
            String langueRequete = StringUtils.EMPTY;
            if (item.length > 4) {
                langueRequete = item[3];
                dernierItem = item[4];
            }
            infoBean.set("REQUETE_OBJET#" + i, libelleObjet);
            infoBean.set("REQUETE_STRUCTURE#" + i, structure);
            infoBean.set("REQUETE_RUBRIQUE#" + i, libelleRubrique);
            infoBean.set("REQUETE_LANGUE#" + i, langueRequete);
            if ("1".equals(dernierItem)) {
                infoBean.set("REQUETE_MODE#" + i, "Synchronisé");
            }
            infoBean.set("REQUETE#" + i, requete);
            i++;
        }
        ecranLogique = ECRAN_PRINCIPAL;
    }

    /**
     * Affichage de l'écran de recherche d'une fiche.
     *
     * @throws Exception the exception
     */
    private void preparerRECHERCHE_FICHES_REFERENCEES() throws Exception {
        // restriction de l'affichage des structures et des rubriques
        infoBean.set("GRS_PERMISSION_TYPE", "FICHE");
        infoBean.set("GRS_PERMISSION_OBJET", "");
        infoBean.set("GRS_PERMISSION_ACTION", "M");
        ecranLogique = ECRAN_RECHERCHE_FICHES_REFERENCEES;
    }

    /**
     * Affichage de l'écran de recherche d'une fiche.
     *
     * @throws Exception the exception
     */
    private void traiterRECHERCHE_FICHES_REFERENCEES() throws Exception {
        if ("REVENIR".equals(action)) {
            rafraichirPRINCIPAL();
        }
        if ("VALIDER".equals(action)) {
            preparerLISTE_FICHES_REFERENCEES();
        }
        infoBean.set("ANCRE", "contenu-fiches-ref");
    }

    /**
     * Affichage de l'écran de liste de fiches référencées.
     *
     * @throws Exception the exception
     */
    private void preparerLISTE_FICHES_REFERENCEES() throws Exception {
        final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        final String sCodeRubriqueCourante = infoBean.getString("CODE_RUBRIQUE");
        final String sLibelle = infoBean.getString("TITRE");
        final String sCodeObjet = infoBean.getString("CODE_OBJET");
        final String sCodeRubrique = infoBean.getString("CODE_RUBRIQUE_RECHERCHE");
        final String sCodeRattachement = infoBean.getString("CODE_RATTACHEMENT");
        final String sCodeRedacteur = infoBean.getString("CODE_REDACTEUR");
        final String sIdMeta = infoBean.getString("ID_META");
        final String urlFiche = infoBean.getString("URL_FICHE");
        // Récupération des critère de recherche de date
        Date dateDebutCreation = infoBean.getDate("DATE_CREATION_DEBUT");
        if (!Formateur.estSaisie(dateDebutCreation)) {
            dateDebutCreation = null;
        }
        Date dateFinCreation = infoBean.getDate("DATE_CREATION_FIN");
        if (!Formateur.estSaisie(dateFinCreation)) {
            dateFinCreation = null;
        }
        Date dateDebutModification = infoBean.getDate("DATE_MODIFICATION_DEBUT");
        if (!Formateur.estSaisie(dateDebutModification)) {
            dateDebutModification = null;
        }
        Date dateFinModification = infoBean.getDate("DATE_MODIFICATION_FIN");
        if (!Formateur.estSaisie(dateFinModification)) {
            dateFinModification = null;
        }
        Date dateDebutMiseEnLigne = infoBean.getDate("DATE_MISE_EN_LIGNE_DEBUT");
        if (!Formateur.estSaisie(dateDebutMiseEnLigne)) {
            dateDebutMiseEnLigne = null;
        }
        Date dateFinMiseEnLigne = infoBean.getDate("DATE_MISE_EN_LIGNE_FIN");
        if (!Formateur.estSaisie(dateFinMiseEnLigne)) {
            dateFinMiseEnLigne = null;
        }
        final ResultatRechercheMultifiche resultatRechercheMultifiche = RechercheMultificheHelper.rerchercherParmisToutesLesFiches(this, autorisations, sLibelle, sCodeObjet, StringUtils.EMPTY, sCodeRubrique, sCodeRattachement, sCodeRedacteur, sIdMeta, urlFiche, dateDebutCreation, dateFinCreation, dateDebutModification, dateFinModification, dateDebutMiseEnLigne, dateFinMiseEnLigne, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        infoBean.setInt("LISTE_NB_ITEMS", 0);
        int cptFiche = 0;
        if (resultatRechercheMultifiche.getResultats().size() > 0) {
            for (final MetatagBean leMeta : resultatRechercheMultifiche.getResultats()) {
                final FicheUniv fiche = FicheUnivMgr.init(leMeta);
                fiche.setCtx(this);
                fiche.setIdFiche(leMeta.getMetaIdFiche());
                fiche.retrieve();
                boolean insertion = true;
                // on filtre les fiches de la rubrique courante
                if (fiche.getCodeRubrique().equals(sCodeRubriqueCourante)) {
                    insertion = false;
                }
                // on exclut les fiches rattachées à des espaces
                if (insertion && fiche instanceof DiffusionSelective) {
                    if ("4".equals(((DiffusionSelective) fiche).getDiffusionModeRestriction())) {
                        insertion = false;
                    }
                }
                if (insertion) {
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_INTITULE#" + cptFiche, fiche.getLibelleAffichable());
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_CODE#" + cptFiche, fiche.getCode());
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_LANGUE#" + cptFiche, fiche.getLangue());
                    final String typeObjet = ReferentielObjets.getCodeObjet(fiche);
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_TYPE#" + cptFiche, typeObjet);
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_ID#" + cptFiche, fiche.getIdFiche().toString());
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_ETAT#" + cptFiche, fiche.getEtatObjet());
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_LIBELLE_TYPE#" + cptFiche, ReferentielObjets.getLibelleObjet(typeObjet));
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_DATE#" + cptFiche, Formateur.formaterDate(fiche.getDateModification(), ""));
                    final String url = UnivWebFmt.determinerUrlFiche(this, ReferentielObjets.getNomObjet(typeObjet), fiche.getCode(), fiche.getLangue(), false);
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_URL#" + cptFiche, url);
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_INTITULE_RUBRIQUE#" + cptFiche, "");
                    final RubriqueBean rubriqueFiche = serviceRubrique.getRubriqueByCode(fiche.getCodeRubrique());
                    if (rubriqueFiche != null) {
                        infoBean.set("RECHERCHE_FICHES_REFERENCEES_INTITULE_RUBRIQUE#" + cptFiche, rubriqueFiche.getIntitule());
                    } else {
                        infoBean.set("RECHERCHE_FICHES_REFERENCEES_INTITULE_RUBRIQUE#" + cptFiche, MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE"));
                    }
                    infoBean.set("RECHERCHE_FICHES_REFERENCEES_SELECTION#" + cptFiche, "0");
                    cptFiche++;
                }
            }
        }
        // aucun des champs n'est renseigné, on balance une erreur
        // applicative
        else {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_AUCUN_RESULTAT"));
        }
        infoBean.setInt("RECHERCHE_FICHES_REFERENCEES_NB_ITEMS", cptFiche);
        ecranLogique = ECRAN_LISTE_FICHES_REFERENCEES;
    }

    /**
     * Affichage de l'écran de recherche d'une fiche.
     *
     * @throws Exception the exception
     */
    private void traiterLISTE_FICHES_REFERENCEES() throws Exception {
        if ("REVENIR".equals(action)) {
            ecranLogique = ECRAN_RECHERCHE_FICHES_REFERENCEES;
        }
        if ("VALIDER".equals(action)) {
            for (int i = 0; i < infoBean.getInt("RECHERCHE_FICHES_REFERENCEES_NB_ITEMS"); i++) {
                if ("1".equals(infoBean.getString("RECHERCHE_FICHES_REFERENCEES_SELECTION#" + i))) {
                    /* Sauvegarde fiche dans TreeSet */
                    final InfosFicheReferencee infosFiche = new InfosFicheReferencee();
                    infosFiche.setCode(infoBean.getString("RECHERCHE_FICHES_REFERENCEES_CODE#" + i));
                    infosFiche.setType(infoBean.getString("RECHERCHE_FICHES_REFERENCEES_TYPE#" + i));
                    infosFiche.setLangue(infoBean.getString("RECHERCHE_FICHES_REFERENCEES_LANGUE#" + i));
                    infosFiche.setIntitule(infoBean.getString("RECHERCHE_FICHES_REFERENCEES_INTITULE#" + i));
                    infosFiche.setEtat(infoBean.getString("RECHERCHE_FICHES_REFERENCEES_ETAT#" + i));
                    infosFiche.setId(infoBean.getString("RECHERCHE_FICHES_REFERENCEES_ID#" + i));
                    listeFichesReferencees.add(infosFiche);
                }
            }
            rafraichirPRINCIPAL();
        }
    }

    /**
     * Affichage de l'écran de saisie d'une requete de référencement.
     *
     * @throws Exception the exception
     */
    private void traiterAJOUT_REQUETE() throws Exception {
        if ("REVENIR".equals(action)) {
            infoBean.setTitreEcran(rubriqueBean.getIntitule());
            ecranLogique = ECRAN_PRINCIPAL;
        }
        if (action.contains("AJOUTER_REQUETE#")) {
            final int indiceDiese = action.indexOf("#");
            final int i = Integer.parseInt(action.substring(indiceDiese + 1));
            String objet = infoBean.getString("REQUETE_OBJET_" + i);
            String structure = infoBean.getString("REQUETE_STRUCTURE_" + i);
            String codeRubrique = infoBean.getString("REQUETE_RUBRIQUE_" + i);
            String langue = infoBean.getString("REQUETE_LANGUE_" + i);
            final String mode = infoBean.getString("REQUETE_MODE_" + i);
            if ("0000".equals(objet) && "".equals(structure) && "".equals(codeRubrique) && "0000".equals(langue)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "RUBRIQUE.REQUETE.ERREUR.CRITERE_REQUETE"));
            }
            if ("annuaire".equals(objet) && codeRubrique.length() > 0) {
                codeRubrique = "";
            }
            if (structure.length() > 0) {
                structure += ";" + infoBean.getString("REQUETE_STRUCTURE_ARBO_" + i);
            }
            if (codeRubrique.length() > 0) {
                codeRubrique += ";" + infoBean.getString("REQUETE_RUBRIQUE_ARBO_" + i);
            }
            if ("0000".equals(objet)) {
                objet = "";
            }
            if ("0000".equals(langue)) {
                langue = "";
            }
            final String requete = objet + "/" + structure + "/" + codeRubrique + "/" + langue + "/" + mode;
            if (listeRequeteRubriquePublication.contains(requete)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "RUBRIQUE.REQUETE.ERREUR.REQUETE_EXISTANTE"));
            }
            listeRequeteRubriquePublication.add(requete);
            // nettoyage des données saisies
            infoBean.set("REQUETE_OBJET_" + i, "");
            infoBean.set("REQUETE_STRUCTURE_" + i, "");
            infoBean.set("REQUETE_RUBRIQUE_" + i, "");
            infoBean.set("REQUETE_LANGUE_" + i, "");
            infoBean.set("REQUETE_STRUCTURE_ARBO_" + i, "");
            infoBean.set("REQUETE_RUBRIQUE_ARBO_" + i, "");
            infoBean.set("LIBELLE_REQUETE_STRUCTURE_" + i, "");
            infoBean.set("LIBELLE_REQUETE_RUBRIQUE_" + i, "");
            infoBean.set("LIBELLE_REQUETE_LANGUE_" + i, "");
            infoBean.set("REQUETE_MODE_" + i, "");
            rafraichirPRINCIPAL();
            infoBean.setTitreEcran(rubriqueBean.getIntitule());
            infoBean.setActionUtilisateur("VALIDER");
        }
        infoBean.set("ANCRE", "contenu-requete");
    }

    /**
     * Affichage de l'écran des critères de recherche d'un Rubrique.
     *
     * @throws Exception the exception
     */
    private void preparerRECHERCHE() throws Exception {
        ecranLogique = ECRAN_RECHERCHE;
        /* JSS 20040419 : Filtre de l'arbre */
        infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE", "CODE");
        infoBean.set("GRS_PERMISSION_TYPE", "TECH");
        infoBean.set("GRS_PERMISSION_OBJET", "rub");
        if (mode == MODE_SUPPRESSION) {
            infoBean.set("GRS_PERMISSION_ACTION", "C");
        }
        if (mode == MODE_MODIFICATION) {
            infoBean.set("GRS_PERMISSION_ACTION", "M");
        }
        infoBean.set("LISTE_LANGUES", LangueUtil.getListeLangues());
        infoBean.set("CATEGORIES", LabelUtils.getLabelCombo("80", ContexteUtil.getContexteUniv().getLocale()));
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     * @throws Exception the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        // JSS 20040419 : délégation
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            try {
                infoBean.getMessages().clear();
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                /* Entrée par lien hyper-texte */
                // JSS 20040419 : les tests d'autorisation sont reportés dans la methode initialisation
                if (etat == DEBUT) {
                    initialisation();
                } else {
                    if (ECRAN_RECHERCHE.equals(ecranLogique)) {
                        traiterRECHERCHE();
                    } else if (ECRAN_LISTE.equals(ecranLogique)) {
                        traiterLISTE();
                    } else if (ECRAN_PRINCIPAL.equals(ecranLogique)) {
                        traiterPRINCIPAL();
                    } else if (ECRAN_RECHERCHE_FICHES_REFERENCEES.equals(ecranLogique)) {
                        traiterRECHERCHE_FICHES_REFERENCEES();
                    } else if (ECRAN_LISTE_FICHES_REFERENCEES.equals(ecranLogique)) {
                        traiterLISTE_FICHES_REFERENCEES();
                    } else if (ECRAN_AJOUT_REQUETE.equals(ecranLogique)) {
                        traiterAJOUT_REQUETE();
                    }
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOGGER.error(e.getMessage(), e);
                infoBean.addMessageErreur(e.toString());
            }
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Traitement de l'écran de sélection d'un Rubrique.
     *
     * @throws Exception the exception
     */
    private void traiterLISTE() throws Exception {
        if (InfoBean.ACTION_MODIFIER.equals(action)) {
            final int indice = Integer.parseInt((String) infoBean.get("LISTE_INDICE"));
            rubriqueBean = serviceRubrique.getById(listeIdentifiant[indice]);
            preparerPRINCIPAL();
        } else if (InfoBean.ACTION_VALIDER.equals(action)) {
            final int indice = Integer.parseInt((String) infoBean.get("LISTE_INDICE"));
            rubriqueBean = serviceRubrique.getById(listeIdentifiant[indice]);
            remplirDonneesRecherche();
            etat = FIN;
        } else if (InfoBean.ACTION_SUPPRIMER.equals(action)) {
            final int indice = Integer.parseInt((String) infoBean.get("LISTE_INDICE"));
            traiterSuppression(listeIdentifiant[indice]);
        } else if (InfoBean.ACTION_ANNULER.equals(action)) {
            infoBean.set("ID_RUBRIQUE", null);
            etat = FIN;
        }
    }

    private void traiterSuppression(final Long idRubrique) throws Exception {
        serviceRubrique.deletebyId(idRubrique);
        infoBean.addMessageConfirmation(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_RUBRIQUE"));
        etat = FIN;
    }

    /**
     * Gestion de l'écran de saisie d'un Rubrique.
     *
     * @throws Exception the exception
     */
    private void traiterPRINCIPAL() throws Exception {
        if ("AJOUTER_REQUETE".equals(action)) {
            infoBean.setTitreEcran(MessageHelper.getCoreMessage("SAISIE_RUBRIQUE.PRINCIPAL.AJOUTER_REQUETE"));
            ecranLogique = ECRAN_AJOUT_REQUETE;
        } else if ("AJOUTER_FICHE_REFERENCEE".equals(action)) {
            preparerRECHERCHE_FICHES_REFERENCEES();
        } else if (action.contains("SUPPRIMER_FICHE_REFERENCEE")) {
            infoBean.set("ANCRE", "contenu-fiches-ref");
            /* Suppression de l'élément courant */
            final int indiceDiese = action.indexOf("#");
            final int i = Integer.parseInt(action.substring(indiceDiese + 1));
            final InfosFicheReferencee infosFiche = new InfosFicheReferencee();
            infosFiche.setCode(infoBean.getString("FICHE_REFERENCEE_CODE#" + i));
            infosFiche.setType(infoBean.getString("FICHE_REFERENCEE_TYPE#" + i));
            infosFiche.setLangue(infoBean.getString("FICHE_REFERENCEE_LANGUE#" + i));
            infosFiche.setIntitule(infoBean.getString("FICHE_REFERENCEE_INTITULE#" + i));
            listeFichesReferencees.remove(infosFiche);
            rafraichirPRINCIPAL();
            final String confirmation = MessageHelper.getCoreMessage("BO_RUBRIQUE_CONFIRMATION_SUPPRESSION_REFERENCE_FICHE");
            infoBean.addMessageConfirmation(String.format(confirmation, infosFiche.getIntitule()));
        } else if (action.contains("SUPPRIMER_REQUETE")) {
            infoBean.set("ANCRE", "contenu-requete");
            /* Suppression de l'élément courant */
            final int indiceDiese = action.indexOf("#");
            final int i = Integer.parseInt(action.substring(indiceDiese + 1));
            listeRequeteRubriquePublication.remove(infoBean.getString("REQUETE#" + i));
            rafraichirPRINCIPAL();
            infoBean.addMessageConfirmation(MessageHelper.getCoreMessage("BO_RUBRIQUE_CONFIRMATION_SUPPRESSION_REQUETE"));
        } else if (action.contains("REMONTER_RUBRIQUE_FILLE#")) {
            /* Suppression de l'élément courant */
            final int indiceDiese = action.indexOf("#");
            final int indice = Integer.parseInt(action.substring(indiceDiese + 1));
            final String tmpCode = infoBean.getString("RUBRIQUE_FILLE_CODE#" + indice);
            final String tmpIntitule = infoBean.getString("RUBRIQUE_FILLE_INTITULE#" + indice);
            final String tmpPage = infoBean.getString("RUBRIQUE_FILLE_PAGE#" + indice);
            infoBean.set("RUBRIQUE_FILLE_CODE#" + indice, infoBean.getString("RUBRIQUE_FILLE_CODE#" + (indice - 1)));
            infoBean.set("RUBRIQUE_FILLE_INTITULE#" + indice, infoBean.getString("RUBRIQUE_FILLE_INTITULE#" + (indice - 1)));
            infoBean.set("RUBRIQUE_FILLE_PAGE#" + indice, infoBean.getString("RUBRIQUE_FILLE_PAGE#" + (indice - 1)));
            infoBean.set("RUBRIQUE_FILLE_CODE#" + (indice - 1), tmpCode);
            infoBean.set("RUBRIQUE_FILLE_INTITULE#" + (indice - 1), tmpIntitule);
            infoBean.set("RUBRIQUE_FILLE_PAGE#" + (indice - 1), tmpPage);
            infoBean.set("ANCRE", "sousrubrique");
        } else if (action.contains("BAISSER_RUBRIQUE_FILLE#")) {
            /* Suppression de l'élément courant */
            final int indiceDiese = action.indexOf("#");
            final int indice = Integer.parseInt(action.substring(indiceDiese + 1));
            final String tmpCode = infoBean.getString("RUBRIQUE_FILLE_CODE#" + indice);
            final String tmpIntitule = infoBean.getString("RUBRIQUE_FILLE_INTITULE#" + indice);
            final String tmpPage = infoBean.getString("RUBRIQUE_FILLE_PAGE#" + indice);
            infoBean.set("RUBRIQUE_FILLE_CODE#" + indice, infoBean.getString("RUBRIQUE_FILLE_CODE#" + (indice + 1)));
            infoBean.set("RUBRIQUE_FILLE_INTITULE#" + indice, infoBean.getString("RUBRIQUE_FILLE_INTITULE#" + (indice + 1)));
            infoBean.set("RUBRIQUE_FILLE_PAGE#" + indice, infoBean.getString("RUBRIQUE_FILLE_PAGE#" + (indice + 1)));
            infoBean.set("RUBRIQUE_FILLE_CODE#" + (indice + 1), tmpCode);
            infoBean.set("RUBRIQUE_FILLE_INTITULE#" + (indice + 1), tmpIntitule);
            infoBean.set("RUBRIQUE_FILLE_PAGE#" + (indice + 1), tmpPage);
            infoBean.set("ANCRE", "sousrubrique");
        } else if (action.equals(InfoBean.ACTION_VALIDER)) {
            //JSS 20040419 : test permissions
            if (mode == MODE_AJOUT) {
                if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "C"), new Perimetre("*", infoBean.getString("CODE_RUBRIQUE_MERE"), "*", "*", ""))) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + " : " + MessageHelper.getCoreMessage("AUTORISATION_CREATION_HORS_PERIMETRE"));
                }
                // Vérifie que le code ne contient que des caractères alphanumériques ou '-' ou '_'
                Chaine.controlerCodeMetier((String) infoBean.get("CODE"));
            } else if (mode == MODE_MODIFICATION) {
                // Test des cas de déplacement, il faut un droit de création dans la nouvelle rubrique mère
                if (!infoBean.getString("SAUVEGARDE_CODE_RUBRIQUE_MERE").equals(infoBean.getString("CODE_RUBRIQUE_MERE"))) {
                    if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "C"), new Perimetre("*", infoBean.getString("CODE_RUBRIQUE_MERE"), "*", "*", ""))) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + " : " + MessageHelper.getCoreMessage("AUTORISATION_DEPLACEMENT_HORS_PERIMETRE"));
                    }
                } else if (!autorisations.possedePermission(new PermissionBean("TECH", "rub", "M"), new Perimetre("*", infoBean.getString("CODE"), "*", "*", ""))) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + " : " + MessageHelper.getCoreMessage("AUTORISATION_MODIFICATION_HORS_PERIMETRE"));
                }
            }
            //AM 20030207-001 : Empecher la rubrique de pointer sur elle-meme
            if (infoBean.getString("CODE_RUBRIQUE_MERE").equals(infoBean.get("CODE"))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "RUBRIQUE.RUBRIQUE_MERE.ERREUR.RUBRIQUE_MERE_ELLE_MEME"));
            }
            //Controle boucle sur rubriques filles
            final RubriqueBean newRubrique = serviceRubrique.getRubriqueByCode(infoBean.getString("CODE"));
            final RubriqueBean rubriqueMere = serviceRubrique.getRubriqueByCode(infoBean.getString("CODE_RUBRIQUE_MERE"));
            if (serviceRubrique.isParentSection(newRubrique, rubriqueMere)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "RUBRIQUE.RUBRIQUE_MERE.ERREUR.RUBRIQUE_MERE_FILLE"));
            }
            rubriqueBean.setCode((String) infoBean.get("CODE"));
            rubriqueBean.setLangue((String) infoBean.get("LANGUE"));
            rubriqueBean.setIntitule((String) infoBean.get("INTITULE"));
            rubriqueBean.setAccroche((String) infoBean.get("ACCROCHE"));
            // 0004047
            rubriqueBean.setCategorie((String) infoBean.get("CATEGORIE"));
            // Le bandeau et les onglets ne sont saisissables que par le webmaster
            if (infoBean.get("ID_BANDEAU") != null) {
                rubriqueBean.setIdBandeau(Long.valueOf(infoBean.getString("ID_BANDEAU")));
                rubriqueBean.setCouleurFond((String) infoBean.get("COULEUR_FOND"));
                rubriqueBean.setCouleurTitre((String) infoBean.get("COULEUR_TITRE"));
            }
            rubriqueBean.setIdPicto(Long.valueOf(infoBean.getString("ID_PICTO")));
            rubriqueBean.setNomOnglet((String) infoBean.get("NOM_ONGLET"));
            rubriqueBean.setContact((String) infoBean.get("CONTACT"));
            final String oldParentSectionCode = rubriqueBean.getCodeRubriqueMere();
            if (!"0000".equals(infoBean.getString("CODE_RUBRIQUE_MERE"))) {
                rubriqueBean.setCodeRubriqueMere((String) infoBean.get("CODE_RUBRIQUE_MERE"));
            } else {
                rubriqueBean.setCodeRubriqueMere(StringUtils.EMPTY);
            }
            if (autorisations.isDiffuseurDSI()) {
                rubriqueBean.setGroupesDsi(Chaine.convertirAccolades(Chaine.getVecteurPointsVirgules(infoBean.getString("GROUPES_DSI"))));
            } else if (rubriqueBean.getGroupesDsi() == null) {
                rubriqueBean.setGroupesDsi(StringUtils.EMPTY);
            }
            // traitement spécifique du type de rubrique
            PageAccueilRubriqueManager.getInstance().traiterPRINCIPAL(infoBean, rubriqueBean);
            rubriqueBean.setGestionEncadre(StringUtils.defaultString(infoBean.getString("GESTION_ENCADRE"), "0"));
            rubriqueBean.setEncadre((String) infoBean.get("ENCADRE"));
            rubriqueBean.setEncadreSousRubrique(StringUtils.defaultString(infoBean.getString("ENCADRE_SOUS_RUBRIQUE"), "0"));
            // RP20051211 sauvegarde des requetes de rubriquage automatique
            final Vector<String> requeteASupprimer = Chaine.getVecteurAccolades(rubriqueBean.getRequetesRubriquePublication());
            final int nbRequete = infoBean.getInt("REQUETE_NB_ITEMS");
            for (int i = 0; i < nbRequete; i++) {
                final String requete = infoBean.getString("REQUETE#" + i);
                if (requeteASupprimer.contains(requete)) {
                    requeteASupprimer.remove(requete);
                }
            }
            // suppression directs des liens de requetes
            final TreeSet<InfosFicheReferencee> listeFichesReferencementSupprimees = new TreeSet<>(new InfosFicheComparator());
            for (final String aRequeteASupprimer : requeteASupprimer) {
                listeFichesReferencementSupprimees.addAll(serviceRubriquePublication.deleteRubPubAuto(rubriqueBean.getCode(), aRequeteASupprimer));
            }
            // enregistrement
            rubriqueBean.setRequetesRubriquePublication(Chaine.convertirAccolades(listeRequeteRubriquePublication));
            if (mode == MODE_AJOUT) {
                /* On teste si le code existe déjà */
                if (serviceRubrique.codeAlreadyExist(rubriqueBean.getCode())) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "RUBRIQUE.CODE_RUBRIQUE.ERREUR.CODE_EXISTANT"));
                }
                final int ordre = serviceRubrique.getMaxOrderAvailable(rubriqueBean.getCodeRubriqueMere());
                rubriqueBean.setOrdre(String.valueOf(ordre));
                serviceRubrique.save(rubriqueBean);
                infoBean.addMessageConfirmation(String.format(MessageHelper.getCoreMessage(this.getLocale(), "CONFIRMATION_CREATION_RUBRIQUE"), rubriqueBean.getIntitule()));
            } else if (mode == MODE_MODIFICATION) {
                // RP 20050405
                // mise à jour de l'ordre des rubriques
                final int nbRubrique = infoBean.getInt("RUBRIQUES_FILLES_NB_ITEMS");
                for (int i = 0; i < nbRubrique; i++) {
                    final String codeRubrique = infoBean.getString("RUBRIQUE_FILLE_CODE#" + i);
                    final RubriqueBean rubriqueFille = serviceRubrique.getRubriqueByCode(codeRubrique);
                    if (rubriqueFille != null && !String.valueOf(i + 1).equals(rubriqueFille.getOrdre())) {
                        rubriqueFille.setOrdre(String.valueOf(i + 1));
                        serviceRubrique.save(rubriqueFille);
                    }
                }
                if (oldParentSectionCode != null && !oldParentSectionCode.equals(rubriqueBean.getCodeRubriqueMere())) {
                    int ordre = serviceRubrique.getMaxOrderAvailable(rubriqueBean.getCodeRubriqueMere());
                    rubriqueBean.setOrdre(String.valueOf(ordre));
                }
                serviceRubrique.save(rubriqueBean);
                infoBean.addMessageConfirmation(String.format(MessageHelper.getCoreMessage(this.getLocale(), "CONFIRMATION_MODIFICATION_RUBRIQUE"), rubriqueBean.getIntitule()));
            }
            /* JSS 20050126 : Sauvegarde des fiches référencées */
            // Après la sauvegarde des rubriques de publication,
            // il faut réindexer dans Lucene toutes les fiches (y compris les fiches supprimées)

            final List<InfosFicheReferencee> listeAvantValidation = serviceRubriquePublication.getListeFichesReferences(rubriqueBean.getCode(), false);
            serviceRubriquePublication.saveAllFicheForRubPub(rubriqueBean.getCode(), listeFichesReferencees);
            final TreeSet<InfosFicheReferencee> listeFichesAIndexer = new TreeSet<>(new InfosFicheComparator());
            listeFichesAIndexer.addAll(listeAvantValidation);
            listeFichesAIndexer.addAll(listeFichesReferencees);
            listeFichesAIndexer.addAll(listeFichesReferencementSupprimees);
            // Itération sur chaque fiche à réindexer
            for (final InfosFicheReferencee infos : listeFichesAIndexer) {
                // Lecture de la fiche
                final FicheUniv ficheAreindexee = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(infos.getType()));
                if (ficheAreindexee != null) {
                    ficheAreindexee.init();
                    ficheAreindexee.setCtx(this);
                    if (ficheAreindexee.selectCodeLangueEtat(infos.getCode(), infos.getLangue(), "0003") > 0) {
                        ficheAreindexee.nextItem();
                        ficheAreindexee.setDateProposition(new Date(System.currentTimeMillis()));
                        ficheAreindexee.update();
                        final MetatagBean meta = MetatagUtils.lireMeta(ficheAreindexee);
                        meta.setMetaDateProposition(ficheAreindexee.getDateProposition());
                        serviceMetatag.save(meta);
                    }
                }
            }
            etat = FIN;
        } else if (action.equals(InfoBean.ACTION_SUPPRIMER)) {
            traiterSuppression(rubriqueBean.getIdRubrique());
        } else if ("RAFRAICHIR".equals(action)) {
            infoBean.set("ANCRE", "contenu-fiches-rat");
        }
    }

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     * @throws Exception the exception
     */
    private void traiterRECHERCHE() throws Exception {
        insererRechercheDansInfoBean(infoBean, getCriteresRecherche(infoBean));
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            final List<RubriqueBean> results = serviceRubrique.getRubriqueByCodeLanguageLabelCategory(StringUtils.defaultIfEmpty(infoBean.getString("CODE"), infoBean.getString("CODE_SAISI")), infoBean.getString("LANGUE"), infoBean.getString("INTITULE"), infoBean.get("CATEGORIE", String.class));
            if (results.size() == 1) {
                rubriqueBean = results.get(0);
                if (mode == MODE_RECHERCHE) {
                    remplirDonneesRecherche();
                    etat = FIN;
                }
                if (mode == MODE_MODIFICATION) {
                    preparerPRINCIPAL();
                }
                if (mode == MODE_SUPPRESSION) {
                    preparerLISTE();
                }
            } else {
                preparerLISTE();
            }
        } else if (action.equals(InfoBean.ACTION_ANNULER)) {
            if (mode == MODE_RECHERCHE) {
                infoBean.set("ID_RUBRIQUE", null);
                etat = FIN;
            }
        }
    }

    private List<CritereRecherche> getCriteresRecherche(final InfoBean infoBean) {
        final List<CritereRecherche> criteres = new ArrayList<>();
        CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVideFormater(infoBean, "INTITULE"));
        CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVide(infoBean, "CODE_SAISI"));
        final String codeRubrique = infoBean.getString("CODE");
        if (StringUtils.isNotBlank(codeRubrique)) {
            String libelleRubrique = codeRubrique;
            final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (rubrique != null) {
                libelleRubrique = rubrique.getIntitule();
            } else {
                LOGGER.info("pas de rubrique de code : " + codeRubrique);
            }
            criteres.add(new CritereRecherche("CODE", codeRubrique, libelleRubrique));
            criteres.add(new CritereRecherche("LIBELLE_CODE", libelleRubrique));
        }
        final String langue = infoBean.getString("LANGUE");
        if (StringUtils.isNotBlank(langue) && !"0000".equals(langue)) {
            criteres.add(new CritereRecherche("LANGUE", langue, LangueUtil.getLocale(langue).getDisplayLanguage(ContexteUtil.getContexteUniv().getLocale())));
        }
        final String category = infoBean.getString("CATEGORIE");
        if (StringUtils.isNotBlank(category) && !"0000".equals(category)) {
            criteres.add(new CritereRecherche("CATEGORIE", category));
        }
        return criteres;
    }

    private void insererRechercheDansInfoBean(final InfoBean infoBean, final List<CritereRecherche> criteres) throws ErreurApplicative {
        criteres.add(new CritereRecherche(DatagridUtils.PARAM_BEAN_DATAGRID, RubriqueDatagrid.ID_BEAN));
        infoBean.set(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES, criteres);
    }

    /**
     * Préparation des données à renvoyer pour une rechercher Rubrique.
     *
     * @throws Exception the exception
     */
    private void remplirDonneesRecherche() throws Exception {
        infoBean.set("ID_RUBRIQUE", rubriqueBean.getIdRubrique());
        infoBean.set("CODE", rubriqueBean.getCode());
        infoBean.set("INTITULE", rubriqueBean.getIntitule());
        infoBean.set("NOM_ONGLET", rubriqueBean.getNomOnglet());
        infoBean.set("CODE_RUBRIQUE_MERE", rubriqueBean.getCodeRubriqueMere());
    }
}
