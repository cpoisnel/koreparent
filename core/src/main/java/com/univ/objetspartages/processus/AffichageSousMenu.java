package com.univ.objetspartages.processus;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantHelper;
import com.kportal.extension.module.composant.IComposant;
import com.kportal.extension.module.composant.Menu;
import com.kportal.ihm.service.ServiceMenuBack;
import com.univ.objetspartages.om.AutorisationBean;

public class AffichageSousMenu extends ProcessusBean {

    public AffichageSousMenu(final InfoBean infoBean) {
        super(infoBean);
    }

    @Override
    protected boolean traiterAction() {
        infoBean.getNomProcessus();
        final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            Menu menuEcranCourant = null;
            final IComposant composantCourant = retrouverComposant(infoBean);
            final Menu menuBackComplet = ServiceMenuBack.getMenuBackOfficeParUtilisateur(autorisations);
            if (menuBackComplet != null && CollectionUtils.isNotEmpty(menuBackComplet.getSousMenu())) {
                int index = 0;
                while (menuEcranCourant == null && index < menuBackComplet.getSousMenu().size()) {
                    final Menu menuCourant = menuBackComplet.getSousMenu().get(index);
                    if (StringUtils.equals(menuCourant.getIdMenu(), composantCourant.getId())) {
                        menuEcranCourant = menuCourant;
                    }
                    index++;
                }
            }
            infoBean.setEcranLogique("ACCUEIL");
            infoBean.setTitreEcran(composantCourant.getLibelleAffichable());
            infoBean.set("MENU_ECRAN_COURANT", menuEcranCourant);
        }
        return Boolean.FALSE;
    }

    private IComposant retrouverComposant(final InfoBean infoBean) {
        final String nomProcessus = infoBean.getNomProcessus();
        final Collection<IComposant> composantsDefinis = ComposantHelper.getComposants();
        for (final IComposant composantCourant : composantsDefinis) {
            if (composantCourant.getParametreProcessus().equals(nomProcessus)) {
                return composantCourant;
            }
        }
        return null;
    }
}
