package com.univ.objetspartages.processus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.datagrid.processus.MultiFicheDatagrid;
import com.univ.datagrid.processus.MultiFicheToolboxDatagrid;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.objetspartages.util.CritereRechercheUtil;
import com.univ.utils.ContexteUtil;
import com.univ.utils.RechercheFiche;
import com.univ.utils.RechercheFicheHelper;

/**
 * Processus de recherche de l'accueil du BO
 */
public class RechercheDirecte extends RechercheFiche {

    /** The Constant ECRAN_RECHERCHE_DIRECTE. */
    protected static final String ECRAN_RECHERCHE_DIRECTE = "RECHERCHE";

    protected static final String ACTION_RECHERCHE_AVANCEE = "RECHERCHE_AVANCEE";

    protected static final String ACTION_RECHERCHER = "RECHERCHER";

    private static final Logger LOG = LoggerFactory.getLogger(RechercheDirecte.class);

    private final ServiceUser serviceUser;

    /**
     * Commentaire relatif au constructeur RechercheGRS.
     *
     * @param ciu
     *            the ciu
     */
    public RechercheDirecte(final InfoBean ciu) {
        super(ciu);
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.utils.RechercheFiche#traiterAction()
     */
    @Override
    public boolean traiterAction() throws Exception {
        ecranLogique = infoBean.getEcranLogique() == null ? "" : infoBean.getEcranLogique();
        action = infoBean.getActionUtilisateur();
        etat = EN_COURS;
        /* Entrée par formulaire */
        if (ECRAN_RECHERCHE_DIRECTE.equals(ecranLogique)) {
            traiterRECHERCHE();
        } else if (ACTION_RECHERCHE_AVANCEE.equals(action) || ACTION_RECHERCHER.equals(action)) {
            traiterRECHERCHEAVANCEE();
        }
        // placer l'état dans le composant d'infoBean
        infoBean.setEcranLogique(ecranLogique);
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    protected void traiterRECHERCHEAVANCEE() throws Exception {
        final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            ecranLogique = "LOGIN";
        } else {
            infoBean.set("LISTE_LANGUES", LangueUtil.getListeLangues());
            ecranLogique = ECRAN_RECHERCHE_DIRECTE;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.utils.RechercheFiche#traiterRECHERCHE()
     */
    @Override
    protected void traiterRECHERCHE() throws Exception {
        if (InfoBean.ACTION_VALIDER.equals(action)) {
            final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
            if (autorisations == null) {
                infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
                ecranLogique = "LOGIN";
            } else {
                insererRechercheDansInfoBean(infoBean);
                preparerLISTE();
            }
        }
    }

    /**
     * Si l'url d'une fiche est renseigné, les autres paramètes sont ignorés...
     *
     * @param infoBean
     */
    private void insererRechercheDansInfoBean(final InfoBean infoBean) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final List<CritereRecherche> criteres = new ArrayList<>();
        final String urlFiche = infoBean.getString("URL_FICHE");
        if (StringUtils.isNotBlank(urlFiche)) {
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVide(infoBean, "URL_FICHE"));
        } else {
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVideFormater(infoBean, "TITRE"));
            final String codeObjet = infoBean.getString("CODE_OBJET");
            if (StringUtils.isNotBlank(codeObjet) && !"0000".equals(codeObjet)) {
                final String libelleObjet = ReferentielObjets.getLibelleObjet(codeObjet);
                criteres.add(new CritereRecherche("CODE_OBJET", codeObjet, StringUtils.defaultIfBlank(libelleObjet, codeObjet)));
            }
            if (StringUtils.isNotBlank(infoBean.getString("CODE_RUBRIQUE_RECHERCHE"))) {
                final String codeRubrique = infoBean.getString("CODE_RUBRIQUE_RECHERCHE");
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                String libelleRubrique = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
                final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
                if (rubriqueBean != null) {
                    libelleRubrique = rubriqueBean.getIntitule();
                }
                criteres.add(new CritereRecherche("CODE_RUBRIQUE_RECHERCHE", codeRubrique, libelleRubrique));
                criteres.add(new CritereRecherche("LIBELLE_CODE_RUBRIQUE_RECHERCHE", libelleRubrique, StringUtils.EMPTY));
            }
            if (StringUtils.isNotBlank(infoBean.getString("CODE_RATTACHEMENT"))) {
                final String codeRattachement = infoBean.getString("CODE_RATTACHEMENT");
                String libelleStructure = StringUtils.defaultString(serviceStructure.getDisplayableLabel(codeRattachement, LangueUtil.getLangueLocale(this.getLocale())), codeRattachement);
                criteres.add(new CritereRecherche("CODE_RATTACHEMENT", codeRattachement, StringUtils.defaultIfBlank(libelleStructure, codeRattachement)));
                criteres.add(new CritereRecherche("LIBELLE_CODE_RATTACHEMENT", libelleStructure, StringUtils.EMPTY));
            }
            if (StringUtils.isNotBlank(infoBean.getString("CODE_REDACTEUR")) || StringUtils.isNotBlank(infoBean.getString("DE_MOI"))) {
                String codeRedacteur = infoBean.getString("CODE_REDACTEUR");
                codeRedacteur = (StringUtils.isEmpty(infoBean.getString("DE_MOI")) ? codeRedacteur : (String) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE));
                final String libelleUtilisateur = serviceUser.getLibelle(codeRedacteur);
                criteres.add(new CritereRecherche("CODE_REDACTEUR", codeRedacteur, StringUtils.defaultIfBlank(libelleUtilisateur, codeRedacteur)));
                criteres.add(new CritereRecherche("LIBELLE_CODE_REDACTEUR", libelleUtilisateur, StringUtils.EMPTY));
            }
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVide(infoBean, "ID_META"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVide(infoBean, "CODE_FICHE"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereDate(infoBean, "DATE_CREATION_DEBUT"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereDate(infoBean, "DATE_CREATION_FIN"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereDate(infoBean, "DATE_MODIFICATION_DEBUT"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereDate(infoBean, "DATE_MODIFICATION_FIN"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereDate(infoBean, "DATE_MISE_EN_LIGNE_DEBUT"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereDate(infoBean, "DATE_MISE_EN_LIGNE_FIN"));
            final String langue = infoBean.getString("LANGUE");
            if (StringUtils.isNotBlank(langue) && !"0000".equals(langue)) {
                final Locale localeRechercher = LangueUtil.getLocale(langue);
                String libelleLangue = langue;
                if (localeRechercher != null) {
                    libelleLangue = localeRechercher.getDisplayLanguage(ContexteUtil.getContexteUniv().getLocale());
                }
                criteres.add(new CritereRecherche("LANGUE", langue, libelleLangue));
            }
            final String etatObjet = infoBean.getString("ETAT_OBJET");
            if (StringUtils.isNotBlank(etatObjet) && !"0000".equals(etatObjet)) {
                final String libelleEtatObjet = MessageHelper.getCoreMessage("ETATFICHE_" + etatObjet);
                criteres.add(new CritereRecherche("ETAT_OBJET", etatObjet, StringUtils.defaultIfBlank(libelleEtatObjet, etatObjet)));
            }
        }
        String idBean = MultiFicheDatagrid.ID_BEAN;
        if (StringUtils.isNotBlank(infoBean.getString("TOOLBOX"))) {
            idBean = MultiFicheToolboxDatagrid.ID_BEAN;
        }
        criteres.add(new CritereRecherche(DatagridUtils.PARAM_BEAN_DATAGRID, idBean, StringUtils.EMPTY));
        infoBean.set(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES, criteres);
    }
}
