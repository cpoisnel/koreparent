package com.univ.objetspartages.processus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.plugin.objetspartages.PluginRechercheHelper;
import com.kportal.extension.module.plugin.objetspartages.PluginSaisieFicheHelper;
import com.univ.datagrid.processus.FicheUnivDatagrid;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheAnnuaire;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.RechercheExterne;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.AffichageHistorique;
import com.univ.objetspartages.util.AffichageReferences;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.RechercheFicheHelper;
import com.univ.utils.recherche.RechercheMultificheHelper;
import com.univ.utils.recherche.ResultatRechercheMultifiche;

/**
 * Super-processus saisie fiche.
 */
public abstract class SaisieFiche extends ProcessusBean {

    /** The Constant ECRAN_RECHERCHE. */
    protected static final String ECRAN_RECHERCHE = "RECHERCHE";

    /** The Constant ECRAN_PRINCIPAL. */
    protected static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    /** The Constant ECRAN_LISTE. */
    protected static final String ECRAN_LISTE = "LISTE";

    /** The Constant ECRAN_SUPPRESSION_FRONT. */
    protected static final String ECRAN_SUPPRESSION = "SUPPRESSION";

    private static final Logger LOG = LoggerFactory.getLogger(SaisieFiche.class);

    /**
     * Constructeur.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieFiche(final InfoBean ciu) {
        super(ciu);
    }

    /**
     * controle et restriction du périmètre de modification.
     *
     * @throws Exception
     *             the exception
     */
    public void controlerPerimetreModification() throws Exception {
        final Object o = getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        final AutorisationBean autorisations = (AutorisationBean) o;
        // A FAIRE : test de controle des zones saisie
        this.getDatas().put("CONTROLE_PERIMETRE_BO", "1");
        this.getDatas().put("AUTORISATIONS", autorisations);
    }

    /**
     * Affichage de la liste des ésultats.
     *
     * @throws Exception
     *             the exception
     */
    protected void preparerLISTE() throws Exception {
        ecranLogique = ECRAN_LISTE;
    }

    /**
     * Affichage de l'écran de saisie d'une fiche.
     *
     * @throws Exception
     *             the exception
     */
    protected abstract void preparerPRINCIPAL() throws Exception;

    /**
     * Affichage de l'écran des critères de recherche d'une fiche.
     *
     * @throws Exception
     *             the exception
     */
    protected abstract void preparerRECHERCHE() throws Exception;
    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     * @throws Exception
     *             the exception
     */
    //protected abstract void traiterLISTE() throws Exception ;

    /**
     * Gestion de l'écran de saisie d'une fiche
     */
    protected abstract void traiterPRINCIPAL() throws Exception;

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     * @throws Exception
     *             the exception
     */
    protected void traiterRECHERCHE() throws Exception {
        if (action.equals(InfoBean.ACTION_VALIDER) || action.equals(InfoBean.ACTION_ENREGISTRER)) {
            for (final Object bean : ClassBeanManager.getInstance().getBeanOfType(RechercheExterne.class)) {
                final RechercheExterne rechercheExterne = (RechercheExterne) bean;
                if (rechercheExterne.traiterRECHERCHE(infoBean, this)) {
                    break;
                }
            }
            preparerLISTE();
        } else if (action.equals(InfoBean.ACTION_AJOUTER)) {
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
            preparerPRINCIPAL();
        } else if (action.equals(InfoBean.ACTION_ANNULER)) {
            ecranLogique = "FIN_TOOLBOX";
        }
    }

    /**
     * Point d'entrée du processus.
     *
     * @param ficheUniv
     *            the fiche univ
     *
     * @throws Exception
     *             the exception
     */
    public void traiterActionParDefaut(final FicheUniv ficheUniv) throws Exception {
        final AutorisationBean autorisations;
        final Object o = getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        /*
         Les noms des colonnes permettent de mettre les filtres d'arbres
         rubriques et structures au niveau des JSP d'administration
         (exploité au niveau de UnivFmt)
         */
        infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT", "CODE_RATTACHEMENT");
        // JSS 20051031 : rattachement secondaires
        // RP 20110117, nouveau parametre pour ne pas controler les structures de rattachement secondaires
        if (!"0".equals(PropertyHelper.getCoreProperty("fiche." + ReferentielObjets.getNomObjet(ficheUniv).toUpperCase() + ".filtre_rattachements_secondaires"))) {
            infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT_AUTRES", "CODE_RATTACHEMENT_AUTRES");
        }
        infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE", "CODE_RUBRIQUE");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE", "1");
        // JSS 20051201 : diffusion indépendante des fiches
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_TYPE", "TECH");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_OBJET", "dsi");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_ACTION", "");
        final String code = ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName());
        infoBean.set("GRS_CODE_OBJET", code);
        infoBean.set("GRS_INTITULE_OBJET", ReferentielObjets.getLibelleObjet(code));
        final String nomobjet = ReferentielObjets.getNomObjet(code);
        final String anonyme = PropertyHelper.getCoreProperty(nomobjet + ".anonyme");
        boolean saisieAnonyme = "1".equals(anonyme);
        boolean isRechercheAnonyme = false, isAllRechercheAnonyme = false;
        final String sRechercheAnonyme = PropertyHelper.getCoreProperty("fiche." + nomobjet.toUpperCase() + ".recherche_anonyme");
        // recherche anonyme=0 pas de recherche anonyme autorisée
        // recherche anonyme=1 recherche anonyme sur les processus de recherche BO comme liens internes, de jointure (fils par ex)...
        // recherche anonyme=2 recherche anonyme sur TOUS les processus de recherche en BO
        if (sRechercheAnonyme != null && ("1".equals(sRechercheAnonyme) || "2".equals(sRechercheAnonyme))) {
            isRechercheAnonyme = true;
            if ("2".equals(sRechercheAnonyme)) {
                isAllRechercheAnonyme = true;
            }
        }
        PluginSaisieFicheHelper.preTraiterAction(infoBean, ficheUniv, (MetatagBean) null);
        if (infoBean.get("SAISIE_ANONYME") != null) {
            saisieAnonyme = (Boolean) infoBean.get("SAISIE_ANONYME");
        }
        // si saisie front anonyme et que l'objet courant peut etre saisi en anonyme et que ce n'est pas un lien direct vers la validation
        // on ne renvoit PAS sur la page de login
        boolean envoilogin = true;
        if (o == null && infoBean.get("SAISIE_FRONT") != null && saisieAnonyme && infoBean.get("LIEN_DIRECT") == null) {
            envoilogin = false;
        }
        // la saisie via le front doit renvoyer l'ecran de login du front, si anonyme desactive OU saisie back office
        if (o == null && ((infoBean.get("SAISIE_FRONT") != null && !saisieAnonyme) || envoilogin)) {
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            String callbackUrl = StringUtils.EMPTY;
            if (StringUtils.isNotBlank(ctx.getUrlPageCourante())) {
                callbackUrl = ctx.getUrlPageCourante();
            }
            infoBean.set("URL_DEMANDEE", callbackUrl);
            if (infoBean.get("SAISIE_FRONT") != null) {
                infoBean.setEcranRedirection(WebAppUtil.CONNEXION_FO);
            } else {
                infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            }
            infoBean.setEcranLogique("LOGIN");
        } else {
            try {
                autorisations = (AutorisationBean) o;
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                etat = EN_COURS;
                PluginSaisieFicheHelper.traiterAction(infoBean, ficheUniv, (MetatagBean) null);
                /* Entrée par lien hyper-texte */
                if (ecranLogique == null) {
                    //pas de contrôle sur les permissions en création car le contributeur
                    //ne pourra de toute façon pas enregistrer sa fiche s'il n'a pas de droit de création
                    if ("AJOUTER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
                        preparerPRINCIPAL();
                    } else if ("MODIFIER".equals(action)) {
                        if (ficheUniv.getIdFiche().equals(0L)) {
                            try {
                                ficheUniv.setIdFiche(Long.valueOf(infoBean.getString("ID_FICHE")));
                                ficheUniv.retrieve();
                            } catch (final Exception e) {
                                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_INEXISTANTE"), e);
                            }
                        }
                        //contrôle avant affichage car certains utilisateurs changaient l'id de la fiche dans l'url
                        //pour pouvoir modifier des fiches hors de leur périmètre
                        //le contrôle fait à l'enregistrement de la fiche ne suffira pas car le contributeur
                        //peut avoir changé la rubrique de la fiche pour qu'elle se retrouve dans son périmètre autorisé
                        final MetatagBean meta = MetatagUtils.lireMeta(ficheUniv);
                        if (autorisations == null || !autorisations.estAutoriseAModifierLaFiche(ficheUniv) && !autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_APPROBATION, meta.getMetaNiveauApprobation())) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_DROIT_MODIFICATION"));
                        }
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                        preparerPRINCIPAL();
                        if (infoBean.get("LIBELLE_AFFICHABLE") != null) {
                            infoBean.setTitreEcran(infoBean.getString("LIBELLE_AFFICHABLE"));
                        }
                    } else if (action.equals(InfoBean.ACTION_SUPPRIMER)) {
                        if (infoBean.get("SAISIE_FRONT") != null) {
                            preparerSUPPRESSION();
                        } else {
                            infoBean.set("TYPE_FICHE", ReferentielObjets.getNomObjet(ficheUniv));
                            traiterSUPPRESSION(ficheUniv);
                        }
                    } else if ("ACCUEIL".equals(action)) {
                        // on reinitialise le cache
                        infoBean.getSessionHttp().removeAttribute("CURRENT_SEARCH_PARAMS");
                        final List<CritereRecherche> paramRecherche = new ArrayList<>();
                        paramRecherche.add(new CritereRecherche("OBJET", ReferentielObjets.getNomObjet(ficheUniv), StringUtils.EMPTY));
                        paramRecherche.add(new CritereRecherche("ETAT_OBJET", "0000", StringUtils.EMPTY));
                        paramRecherche.add(new CritereRecherche(DatagridUtils.PARAM_BEAN_DATAGRID, FicheUnivDatagrid.ID_BEAN, StringUtils.EMPTY));
                        infoBean.set(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES, paramRecherche);
                        preparerLISTE();
                    } else if ("RECHERCHER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
                        // recuperation des criteres de recherche en cache
                        if (infoBean.get("SEARCH_PARAMS") != null) {
                            restoreSearchParams();
                        }
                        PluginRechercheHelper.preparerRecherche(this, infoBean);
                        preparerRECHERCHE();
                        // restriction de l'affichage des structures et des rubriques
                        if (infoBean.get("TOOLBOX") == null) {
                            infoBean.set("GRS_PERMISSION_TYPE", "FICHE");
                            infoBean.set("GRS_PERMISSION_OBJET", infoBean.getString("GRS_CODE_OBJET"));
                            infoBean.set("GRS_PERMISSION_ACTION", "M");
                        }
                    }
                } else // Entrée par formulaire
                {
                    if (ecranLogique.equals(ECRAN_RECHERCHE)) {
                        // pour la recherche on applique ou non les critères de restriction sur le périmetre BO
                        // si recherche anonyme activée, on ne controle pas les processus toolbox de type liens internes, de jointure...
                        // si all recherche anonyme, on ne controle AUCUN processus de recherche BO
                        if (autorisations != null && infoBean.get("SAISIE_FRONT") == null && (!isRechercheAnonyme || (isRechercheAnonyme && infoBean.get("TOOLBOX") == null && !isAllRechercheAnonyme))) {
                            // permet d'appliquer les restrictions back-office à la requete SQL de recherche
                            controlerPerimetreModification();
                            // gestion d'un cache pour les criteres de recherche de l'utilisateur
                            if (infoBean.get("TOOLBOX") == null) {
                                if (infoBean.get("SEARCH_PARAMS") != null) {
                                    restoreSearchParams();
                                } else {
                                    saveSearchParams();
                                }
                            }
                        }
                        // on regarde le critère url (id-meta)
                        if (!StringUtils.isEmpty(infoBean.getString("URL_META"))) {
                            final String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
                            final ResultatRechercheMultifiche resultatRechercheMultifiche = RechercheMultificheHelper.rerchercherParmisToutesLesFiches(this, autorisations, "", codeObjet, "", "", "", "", "", infoBean.getString("URL_META"), null, null, null, null, null, null, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
                            if (resultatRechercheMultifiche.getResultats().size() > 0) {
                                for (final MetatagBean leMeta : resultatRechercheMultifiche.getResultats()) {
                                    if (ficheUniv.selectCodeLangueEtat(leMeta.getMetaCode(), "", "") > 0) {
                                        preparerLISTE();
                                        break;
                                    }
                                }
                            } else {
                                traiterRECHERCHE();
                            }
                        } else {
                            PluginRechercheHelper.traiterRecherche(this, infoBean);
                            traiterRECHERCHE();
                        }
                    } else if (ecranLogique.equals(ECRAN_SUPPRESSION)) {
                        traiterSUPPRESSION(ficheUniv);
                    } else if (ecranLogique.indexOf(ECRAN_PRINCIPAL) == 0) {
                        //anti spam sur la saisie anonyme
                        if (isEnregistremeentModeAnonyme(infoBean, o, saisieAnonyme)) {
                            final String honeyPot = infoBean.getString("VALIDATION_ANONYME");
                            if (StringUtils.isNotEmpty(honeyPot)) {
                                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "JTF_ERR_VALIDATION_ANONYME"));
                            }
                        }
                        if (action.equals(InfoBean.ACTION_ONGLET)) {
                            infoBean.set("SOUS_ONGLET", infoBean.getString("SOUS_ONGLET_DEMANDE"));
                            infoBean.set("HISTORIQUE", AffichageHistorique.getListeHistorique(this, infoBean));
                            infoBean.set("REFERENCE", AffichageReferences.getListeReferences(this, infoBean));
                        }
                        traiterPRINCIPAL();
                    }
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
                PluginSaisieFicheHelper.postTraiterAction(infoBean, ficheUniv, (MetatagBean) null);
            } catch (final Exception e) {
                LOG.error("erreur dans le processus de saisie de fiche", e);
                infoBean.addMessageErreur(e.toString());
            }
        }
    }

    /**
     * On vérifie que l'on est en saisie anonyme en front et que l'action n'est pas un apercu
     *
     * @param infoBean
     * @param autorisations
     * @param saisieAnonyme
     * @return
     */
    private boolean isEnregistremeentModeAnonyme(final InfoBean infoBean, final Object autorisations, final boolean saisieAnonyme) {
        return saisieAnonyme && infoBean.get("SAISIE_FRONT") != null && autorisations == null && !"1".equals(infoBean.getString("APERCU"));
    }

    /**
     * Preparer suppressionfront.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerSUPPRESSION() throws Exception {
        /* Lecture de l'objet */
        final String nomObjet = infoBean.getString("TYPE_FICHE").toUpperCase();
        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(nomObjet);
        ficheUniv.setCtx(this);
        ficheUniv.init();
        ficheUniv.setIdFiche(Long.valueOf(infoBean.getString("ID_FICHE")));
        /* On controle si la fiche existe */
        try {
            ficheUniv.retrieve();
        } catch (final Exception e) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_INEXISTANTE"), e);
        }
        infoBean.set("INTITULE", ficheUniv.getLibelleAffichable());
        ecranLogique = ECRAN_SUPPRESSION;
    }

    /**
     * Traiter suppressionfront.
     *
     * @param ficheUniv
     *            the fiche univ
     *
     * @throws Exception
     *             the exception
     */
    private void traiterSUPPRESSION(final FicheUniv ficheUniv) throws Exception {
        final String nomObjet = infoBean.getString("TYPE_FICHE").toUpperCase();
        infoBean.set("ID_" + nomObjet, infoBean.getString("ID_FICHE"));
        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_SUPPRESSION);
        traiterPRINCIPAL();
        // pour la redirection automatique
        if (infoBean.get("ESPACE") == null) {
            final String rub = ficheUniv.getCodeRubrique();
            if (rub.length() > 0) {
                infoBean.set("RUBRIQUE", ficheUniv.getCodeRubrique());
            }
            infoBean.set("ACCUEIL", "true");
        }
    }

    /**
     * Sauvegarde les paramètres de recherche de l'utilisateur dans la session.
     */
    private void saveSearchParams() {
        final HashMap<String, Object> currentSearchParams = new HashMap<>();
        final Iterator<String> dataKeysIt = infoBean.getDataKeys().iterator();
        String key = null;
        while (dataKeysIt.hasNext()) {
            key = dataKeysIt.next();
            if (!"ETAT_FICHE_ENREGISTREE".equals(key) && !"NB_RESULTATS".equals(key)) {
                currentSearchParams.put(key, infoBean.get(key));
            }
        }
        infoBean.getSessionHttp().setAttribute("CURRENT_SEARCH_PARAMS", currentSearchParams);
    }

    /**
     * Restaure les paramètres de la recherche de la session vers l'infoBean.
     */
    private void restoreSearchParams() {
        final HashMap<String, Object> searchParams = (HashMap<String, Object>) infoBean.getSessionHttp().getAttribute("CURRENT_SEARCH_PARAMS");
        if (searchParams != null) {
            final Iterator<String> searchParamsIt = searchParams.keySet().iterator();
            String key = null;
            while (searchParamsIt.hasNext()) {
                key = searchParamsIt.next();
                if (infoBean.get(key) == null) {
                    // on n'ecrase pas une valeur existante
                    infoBean.set(key, searchParams.get(key));
                }
            }
        }
    }

    /**
     * alimente les zones Code et Langue de chaque fiche lors de l'enregistrement. si la fiche est en création ou vient d'être dupliquée et que le code est saisissable, on le
     * vérifie
     *
     * @param ficheUniv
     *            : la fiche en cours d'enregistrement
     * @param bCodeModifiable
     *            : le code de la fiche est-il modifiable par l'utilisateur ?
     *
     * @throws Exception
     *             the exception
     */
    public void alimenteDonneesCreation(final FicheUniv ficheUniv, final boolean bCodeModifiable) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION) || InfoBean.ACTION_DUPLIQUER.equals(infoBean.get("PREMIERE_ACTION"))) {
            //si le code a pu être saisi par l'utilisateur, on le vérifie
            if (bCodeModifiable) {
                // en mode modification, on doit verifier que le code saisi ne l'a pas deja ete pour une autre fiche
                // ce mode n'est accessible que suite a une duplication. En dehors de ce cas, on ne peut jamais modifier
                // le code d'une fiche. C'est pourquoi le code de cette fiche ne peut pas exister dans un autre etat
                // dans la base, puisqu'on vient de la créer suite a une duplication
                if (!infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                    final FicheUniv fiche2 = ficheUniv.getClass().newInstance();
                    fiche2.init();
                    fiche2.setCtx(this);
                    if (fiche2.selectCodeLangueEtat(infoBean.getString("CODE"), infoBean.getString("LANGUE"), "") > 0) {
                        while (fiche2.nextItem()) {
                            //si la fiche dans la base n'est pas elle-meme, et s'il ne s'agit pas de l'aperçu ou de la sauvegarde automatique de cette fiche
                            if (!fiche2.getIdFiche().equals(ficheUniv.getIdFiche()) && !"0005".equals(fiche2.getEtatObjet()) && !"0006".equals(fiche2.getEtatObjet())) {
                                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_ERREUR_CODE_FICHE_DOUBLON"));
                            }
                        }
                    }
                }
                if (ficheUniv instanceof com.univ.objetspartages.om.StructureModele && infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                    // verifie que le code ne contient que des caractères alphanumériques ou '-' ou '_'
                    // verifie l'unicité du code sur tous les types structures
                    serviceStructure.checkCode(infoBean.get("CODE", String.class), infoBean.get("LANGUE", String.class));
                } else if (ficheUniv instanceof com.univ.objetspartages.om.AnnuaireModele && infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                    // verifie l'unicité du code sur tous les types annuaire
                    FicheAnnuaire.checkCode((String) infoBean.get("CODE"), infoBean.getString("LANGUE"));
                } else {
                    Chaine.controlerCodeMetier((String) infoBean.get("CODE"));
                }
                ficheUniv.setCode(infoBean.getString("CODE"));
            } else {
                //si le code n'est pas saisi par l'utilisateur, on le génère en création
                if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                    ficheUniv.setCode(Long.toString(System.currentTimeMillis()));
                }
            }
            // donnees saisissables uniquement en creation
            if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                ficheUniv.setLangue(infoBean.getString("LANGUE"));
            }
        }
        if (ficheUniv instanceof com.univ.objetspartages.om.StructureModele) {
            //En cas de fiche structure on vérifie qu'on n'est pas en train de créer un cycle
            final String nomColonneStructure = "CODE_RATTACHEMENT";
            serviceStructure.checkCycle(infoBean.get("CODE", String.class), infoBean.get("LANGUE", String.class), infoBean.get(nomColonneStructure, String.class));
        }
    }
}
