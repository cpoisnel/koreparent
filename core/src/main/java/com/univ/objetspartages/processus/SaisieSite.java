package com.univ.objetspartages.processus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.jsbsoft.jtf.textsearch.sitesdistants.IndexeurSitesDistants;
import com.jsbsoft.jtf.textsearch.sitesdistants.RechercheSitesDistants;
import com.jsbsoft.jtf.textsearch.sitesdistants.SiteIndexStates;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantSitesDistants;
import com.univ.objetspartages.bean.SiteExterneBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.services.ServiceSiteExterne;

/**
 * Processus de gestion des sites distants.
 */
public class SaisieSite extends ProcessusBean {

    /** The Constant ECRAN_PRINCIPAL. */
    private final static String ECRAN_PRINCIPAL = "PRINCIPAL";

    /** The Constant ECRAN_LISTE. */
    private final static String ECRAN_LISTE = "LISTE";

    private static final Logger LOG = LoggerFactory.getLogger(SaisieSite.class);

    /** The site. */
    private SiteExterneBean site = null;

    private ServiceSiteExterne serviceSiteExterne;
    /**
     * Constructeur.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieSite(final InfoBean ciu) {
        super(ciu);
        serviceSiteExterne = ServiceManager.getServiceForBean(SiteExterneBean.class);
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        /* The autorisations. */
        final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            try {
                if (!autorisations.possedePermission(ComposantSitesDistants.getPermissionGestion())) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
                }
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                if (ecranLogique == null) {
                    site = new SiteExterneBean();
                    etat = EN_COURS;
                    if ("AJOUTER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
                        preparerPRINCIPAL();
                    } else if ("MODIFIER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                        preparerLISTE();
                    } else if ("MODIFIERPARID".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                        if (infoBean.get("ID_SITE") == null) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_SITE.ERREUR.SITE_A_MODIFIER_VIDE"));
                        }
                        site = serviceSiteExterne.getById(Long.valueOf(infoBean.getString("ID_SITE")));
                        preparerPRINCIPAL();
                    } else if ("SUPPRIMERPARID".equals(action)) {
                        if (infoBean.get("ID_SITE") == null) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_SITE.ERREUR.SITE_A_SUPPRIMER_VIDE"));
                        }
                        site = serviceSiteExterne.getById(Long.valueOf(infoBean.getString("ID_SITE")));
                        if (site != null) {
                            //TODO : Chantier Elastic : Suppression de l'index des sites externes.
                            serviceSiteExterne.delete(site.getIdSite());
                            RechercheSitesDistants.init();
                       }
                        final String confirmation = String.format(MessageHelper.getCoreMessage(this.getLocale(), "CONFIRMATION_SUPPRESSION_SITEEXTERNE"), site.getLibelle());
                        infoBean.addMessageConfirmation(confirmation);
                        etat = FIN;
                    } else if ("INDEXERPARID".equals(action)) {
                        if (infoBean.get("ID_SITE") == null) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_SITE.ERREUR.SITE_A_SUPPRIMER_VIDE"));
                        }
                        site = serviceSiteExterne.getById(Long.valueOf(infoBean.getString("ID_SITE")));
                        IndexeurSitesDistants.getInstance().indexeSite(site, false);
                        preparerLISTE();
                        etat = FIN;
                    }
                } else {
                    if (ecranLogique.equals(ECRAN_LISTE)) {
                        traiterLISTE();
                    } else if (ecranLogique.equals(ECRAN_PRINCIPAL)) {
                        traiterPRINCIPAL();
                    }
                }
                // placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOG.error("erreur de traitement sur le processus", e);
                infoBean.addMessageErreur(e.getMessage());
            }
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Affichage de l'écran des critères de recherche d'un Site.
     *
     */
    private void preparerLISTE() {
        ecranLogique = ECRAN_LISTE;
        final Map<Long, SiteIndexStates> mapEtatSites = IndexeurSitesDistants.getInstance().getEtatsSites();
        final Map<Long, SiteIndexStates> mapEtatSiteFront = new HashMap<>(mapEtatSites);
        final List<SiteExterneBean> toutLesSites = serviceSiteExterne.getAllExernalWebSite();
        final List<String> sitesEnCoursDIndexation = new ArrayList<>();
        for (final SiteExterneBean siteCourant : toutLesSites) {
            if (mapEtatSiteFront.containsKey(siteCourant.getIdSite())) {
                if (SiteIndexStates.RUNNING.equals(mapEtatSiteFront.get(siteCourant.getIdSite()))) {
                    sitesEnCoursDIndexation.add(siteCourant.getLibelle());
                }
            } else {
                mapEtatSiteFront.put(siteCourant.getIdSite(), SiteIndexStates.NOT_DONE);
            }
        }
        infoBean.set("LISTE_SITES", toutLesSites);
        infoBean.set("SITES_EN_COURS_INDEXATION", sitesEnCoursDIndexation);
        infoBean.set("ETAT_SITES", mapEtatSiteFront);
    }

    /**
     * Affichage de l'écran de saisie d'un Site.
     *
     */
    private void preparerPRINCIPAL()  {
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.set("CODE", site.getCode());
        infoBean.set("LIBELLE", site.getLibelle());
        infoBean.set("URL", "".equals(site.getUrl()) ? "http://" : site.getUrl());
        infoBean.set("REG_EXP_ACCEPTE", site.getRegExpAccepte());
        infoBean.set("NIVEAU_PROFONDEUR", site.getNiveauProfondeur());
        infoBean.set("REG_EXP_REFUSE", site.getRegExpRefuse());
        if (StringUtils.isNotBlank(site.getLibelle())) {
            infoBean.setTitreEcran(site.getLibelle());
        }
    }

    /**
     * Traitement associé à la sélection d'un site.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterLISTE() throws Exception {
        if (action.equals(InfoBean.ACTION_MODIFIER)) {
            if (infoBean.get("ID_SITE") == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_SITE.ERREUR.SITE_A_MODIFIER_VIDE"));
            }
            site = serviceSiteExterne.getById(Long.valueOf(infoBean.getString("ID_SITE")));
            preparerPRINCIPAL();
        } else if (action.equals(InfoBean.ACTION_SUPPRIMER)) {
            if (infoBean.get("ID_SITE") == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_SITE.ERREUR.SITE_A_SUPPRIMER_VIDE"));
            }
            site = serviceSiteExterne.getById(Long.valueOf(infoBean.getString("ID_SITE")));
            if (site != null) {
                //final File repertoireIndex = IndexationHelper.getRepertoireIndexation(site);
                serviceSiteExterne.delete(site.getIdSite());
                /*if (repertoireIndex.exists()) {
                    repertoireIndex.delete();
                }*/
                RechercheSitesDistants.init();
            }
            etat = FIN;
        } else if (action.equals(InfoBean.ACTION_ANNULER) || action.equals(InfoBean.ACTION_ABANDON)) {
            etat = FIN;
        }
    }

    /**
     * Gestion de l'écran de saisie d'un site distant.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterPRINCIPAL() throws Exception {
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            site.setLibelle((String) infoBean.get("LIBELLE"));
            String szUrl = (String) infoBean.get("URL");
            if (!szUrl.contains("http://") || "http://".equals(szUrl)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_SITE.ERREUR.URL_INVALIDE"));
            }
            // on rajoute un / si besoin
            if (szUrl.charAt(szUrl.length() - 1) != '/') {
                // cas http://serveur
                if (szUrl.lastIndexOf('/') == "http://".lastIndexOf('/')) {
                    szUrl += '/';
                }
            }
            site.setUrl(szUrl);
            // expression reguliere limitant le scope au site
            // On prend comme repère le dernier slash car si on demande d'indexer http://www.site.com/rubrique,
            // ce n'est pas pour que tout www.site.com soit indexé
            final String szRegExp = szUrl.substring(0, szUrl.lastIndexOf('/') + 1) + ".*";
            try {
                // verifications des expressions regulieres
                final Pattern pattern = Pattern.compile(szRegExp);
                site.setRegExpAccepte(szRegExp);
                site.setRegExpRefuse("");
            } catch (final PatternSyntaxException e) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_SITE.ERREUR.REGEXP_INVALIDE"), e);
            }
            site.setNiveauProfondeur(Integer.valueOf(infoBean.getString("NIVEAU_PROFONDEUR")));
            if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                final String code = infoBean.getString("CODE");
                if (code.indexOf(';') != -1) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_SITE.ERREUR.CARACTERE_INTERDIT"));
                }
                site.setCode(code);
                serviceSiteExterne.save(site);
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_CREATION_SITEEXTERNE"), site.getLibelle());
                infoBean.addMessageConfirmation(confirmation);
            } else if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) {
                serviceSiteExterne.save(site);
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_MODIFICATION_SITEEXTERNE"), site.getLibelle());
                infoBean.addMessageConfirmation(confirmation);
            }
            etat = FIN;
            RechercheSitesDistants.init();
        } else if (action.equals(InfoBean.ACTION_SUPPRIMER)) {
            if (infoBean.get("ID_SITE") == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_SITE.ERREUR.SITE_A_SUPPRIMER_VIDE"));
            }
            site = serviceSiteExterne.getById(Long.valueOf(infoBean.getString("ID_SITE")));
            if (site != null) {
                // TODO INDEXATION SITES DISTANTS
                //final File repertoireIndex = IndexationHelper.getRepertoireIndexation(site);
                serviceSiteExterne.delete(site.getIdSite());
                /*if (repertoireIndex.exists()) {
                    if (!repertoireIndex.delete()) {
                        LOG.info("unable to delete the current directory" + repertoireIndex.getAbsolutePath());
                    }
                }*/
                RechercheSitesDistants.init();
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_SITEEXTERNE"), site.getLibelle());
                infoBean.addMessageConfirmation(confirmation);
            }
            etat = FIN;
        }
    }
}
