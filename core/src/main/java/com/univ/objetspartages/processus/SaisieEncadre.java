package com.univ.objetspartages.processus;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceEncadre;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;

/**
 * processus saisie Encadre.
 */
public class SaisieEncadre extends ProcessusBean {

    private static final Logger LOG = LoggerFactory.getLogger(SaisieEncadre.class);

    /** The Constant MODE_AJOUT. */
    private static final int MODE_AJOUT = 0;

    /** The Constant MODE_MODIFICATION. */
    private static final int MODE_MODIFICATION = 1;

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    /** The Constant ECRAN_ACCUEIL. */
    private static final String ECRAN_ACCUEIL = "ACCUEIL";

    private final ServiceEncadre serviceEncadre;

    /** The encadre. */
    EncadreBean encadre = null;

    /** The liste identifiant. */
    Map<Integer, Long> listeIdentifiant = null;

    /** The autorisations. */
    AutorisationBean autorisations = null;
    // en mode délégation , filtre sur les arbres
    // et pas de modification des périmètres
    // boolean modeDelegation = false;

    /** The mode. */
    private int mode = -1;

    /**
     * processus saisie Encadre.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieEncadre(final InfoBean ciu) {
        super(ciu);
        serviceEncadre = ServiceManager.getServiceForBean(EncadreBean.class);
    }

    /**
     * Initialisation du processus.
     *
     * @throws Exception
     *             the exception
     */
    private void initialisation() throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        // initalisation objets métiers
        encadre = new EncadreBean();
        // Fait ici car demande des ressources
        infoBean.set("LISTE_STRUCTURES", serviceStructure.getStructureListWithFullLabels());
        etat = EN_COURS;
        if ("AJOUTER".equals(action)) {
            //l'action AJOUTER est aussi accessible a partir de l'onglet gestion éditoriale, on contrôle donc la permission et pas si c'est un webmaster
            if (!autorisations.possedePermission(new PermissionBean("TECH", "enc", "M"))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            mode = MODE_AJOUT;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
            preparerPRINCIPAL();
        }
        if ("MODIFIERPARID".equals(action)) {
            final Long idEncadre;
            try {
                idEncadre = Long.valueOf(infoBean.getString("ID_ENCADRE"));
            } catch (final NumberFormatException nfe) {
                LOG.debug("unable to parse the id", nfe);
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "ENCADRE.ERREUR.ENCADRE_INEXISTANT"));
            }
            encadre = serviceEncadre.getById(idEncadre);
            if (encadre == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "ENCADRE.ERREUR.ENCADRE_INEXISTANT"));
            }
            if (!autorisations.possedePermission(new PermissionBean("TECH", "enc", "M"), new Perimetre(encadre.getCodeRattachement(), encadre.getCodeRubrique(), "*", "*", ""))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            mode = MODE_MODIFICATION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            preparerPRINCIPAL();
        }
        //le webmestre peut supprimer à partir de l'onglet gestion éditoriale, c'est plus rapide, on lui ajoute donc cette action
        if ("SUPPRIMERPARID".equals(action)) {
            final Long idEncadre = Long.valueOf(infoBean.getString("ID_ENCADRE"));
            encadre = serviceEncadre.getById(idEncadre);
            if (encadre == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "ENCADRE.ERREUR.ENCADRE_INEXISTANT"));
            }
            supprimerEncadre();
        } else if ("ACCUEIL".equals(action)) {
            ecranLogique = ECRAN_ACCUEIL;
        }
    }

    /**
     * Affichage de l'écran de saisie d'un Encadre.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerPRINCIPAL() throws Exception {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        ecranLogique = ECRAN_PRINCIPAL;
        // JSS 20050713 : ajout code pour e-sup
        final String timeStamp = Long.toString(System.currentTimeMillis());
        infoBean.set("CODE", StringUtils.isEmpty(encadre.getCode()) ? timeStamp.substring(timeStamp.length() - 8, timeStamp.length()) : encadre.getCode());
        infoBean.set("CODE_RATTACHEMENT", encadre.getCodeRattachement());
        infoBean.set("LIBELLE_CODE_RATTACHEMENT", serviceStructure.getDisplayableLabel(encadre.getCodeRattachement(), encadre.getLangue()));
        // JSS 20040409 : Ajout rubrique
        infoBean.set("CODE_RUBRIQUE", encadre.getCodeRubrique());
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(encadre.getCodeRubrique());
        String rubriqueLabel = StringUtils.EMPTY;
        if (rubriqueBean != null) {
            rubriqueLabel = rubriqueBean.getIntitule();
        }
        infoBean.set("LIBELLE_CODE_RUBRIQUE", rubriqueLabel);
        if (!autorisations.isWebMaster()) {
            // filtre des arbres (en mode délégation)
            final List<Perimetre> perimetresEncadre = autorisations.getListePerimetres(new PermissionBean("TECH", "enc", "M"));
            if (perimetresEncadre != null && mode == MODE_AJOUT) {
                for (final Perimetre perimetreEncadre : perimetresEncadre) {
                    if (StringUtils.isNotBlank(perimetreEncadre.getCodeRubrique())) {
                        final RubriqueBean rubriqueEncadre = serviceRubrique.getRubriqueByCode(perimetreEncadre.getCodeRubrique());
                        String rubriqueEncadreLabel = StringUtils.EMPTY;
                        if (rubriqueEncadre != null) {
                            rubriqueEncadreLabel = rubriqueEncadre.getIntitule();
                        }
                        infoBean.set("LIBELLE_CODE_RUBRIQUE", rubriqueEncadreLabel);
                    }
                    if (StringUtils.isNotBlank(perimetreEncadre.getCodeStructure())) {
                        infoBean.set("CODE_RATTACHEMENT", perimetreEncadre.getCodeStructure());
                        infoBean.set("LIBELLE_CODE_RATTACHEMENT", serviceStructure.getDisplayableLabel(perimetreEncadre.getCodeStructure(), encadre.getLangue()));
                    }
                }
            }
            infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE", "CODE_RUBRIQUE");
            infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT", "CODE_RATTACHEMENT");
            infoBean.set("GRS_PERMISSION_TYPE", "TECH");
            infoBean.set("GRS_PERMISSION_OBJET", "enc");
            infoBean.set("GRS_PERMISSION_ACTION", "M");
        }
        infoBean.set("INTITULE", encadre.getIntitule());
        infoBean.set("ACTIF", encadre.getActif());
        infoBean.set("LISTE_LANGUES", LangueUtil.getListeLangues());
        infoBean.set("LANGUE", encadre.getLangue());
        infoBean.set("POIDS", encadre.getPoids());
        infoBean.set("CONTENU", encadre.getContenu());
        final Hashtable<String, String> hObjets = new Hashtable<>();
        for (final Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
            hObjets.put(objet.getCodeObjet(), objet.getLibelleObjet());
        }
        infoBean.set("LISTE_OBJETS", hObjets);
        /* intialisation liste objets */
        final List<String> listeObjets = serviceEncadre.getListObjets(encadre);
        infoBean.setInt("CODE_OBJETS_NB_ITEMS", listeObjets.size());
        int i = 0;
        for (final String currentObject : listeObjets) {
            infoBean.set("CODE_OBJET#" + i, currentObject);
            if (StringUtils.startsWith(currentObject, "RUB_")) {
                infoBean.set("INTITULE_OBJET#" + i, serviceRubrique.getLabelWithAscendantsLabels(StringUtils.substringAfter(currentObject, "RUB_")));
            }
            i++;
        }
        if (StringUtils.isNotBlank(encadre.getIntitule())) {
            infoBean.setTitreEcran(encadre.getIntitule());
        }
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        try {
            ecranLogique = infoBean.getEcranLogique();
            action = infoBean.getActionUtilisateur();
            autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
            if (autorisations == null) {
                infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
                infoBean.setEcranLogique("LOGIN");
            } else {
                if (etat == DEBUT) {
                    initialisation();
                } else if (ecranLogique.equals(ECRAN_PRINCIPAL)) {
                    traiterPRINCIPAL();
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            }
        } catch (final Exception e) {
            LOG.error("Erreur lors du traitement sur les encadres", e);
            infoBean.addMessageErreur(e.toString());
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Gestion de l'écran de saisie d'un Encadre.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterPRINCIPAL() throws Exception {
        if ("AJOUTER_CODE_RUBRIQUE".equals(action)) {
            if ("".equals(infoBean.get("CODE_RUBRIQUE_APPLICATION"))) {
                infoBean.addMessageErreur("La rubrique doit être spécifiée");
            } else {
                final int nbItems = infoBean.getInt("CODE_OBJETS_NB_ITEMS");
                ajoutePerimetreDansInfoBean(nbItems, "RUB_" + infoBean.get("CODE_RUBRIQUE_APPLICATION"));
            }
            infoBean.set("ACTION_PERIMETRE", Boolean.TRUE);
        }
        if ("AJOUTER_CODE_STRUCTURE".equals(action)) {
            if ("".equals(infoBean.get("CODE_STRUCTURE")) && "0000".equals(infoBean.get("CODE_OBJET"))) {
                infoBean.addMessageErreur("La structure et/ou l'objet doit être spécifiée");
            } else {
                //si l'utilisateur n'associe pas de structure au périmètre sur un objet, il doit en avoir les droits
                if ("".equals(infoBean.get("CODE_STRUCTURE")) && !autorisations.isWebMaster() && !autorisations.possedePermission(new PermissionBean("TECH", "enc", "M"), new Perimetre("-", "*", "*", "*", ""))) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + MessageHelper.getCoreMessage("SAISIE_ENCADRE_PAS_DROIT_PERIMETRE_STRUCTURE"));
                }
                final int nbItems = infoBean.getInt("CODE_OBJETS_NB_ITEMS");
                String code = "";
                if (!"0000".equals(infoBean.get("CODE_OBJET"))) {
                    code = infoBean.getString("CODE_OBJET");
                }
                code += "/";
                code += infoBean.getString("CODE_STRUCTURE");
                ajoutePerimetreDansInfoBean(nbItems, code);
            }
            infoBean.set("ACTION_PERIMETRE", Boolean.TRUE);
        }
        if ("AJOUTER_CODE_STRUCTURE_SEULE".equals(action)) {
            if ("".equals(infoBean.get("CODE_STRUCTURE_SEULE"))) {
                infoBean.addMessageErreur("La structure doit être spécifiée.");
            } else {
                final int nbItems = infoBean.getInt("CODE_OBJETS_NB_ITEMS");
                String code = "";
                code += "/";
                code += infoBean.getString("CODE_STRUCTURE_SEULE");
                ajoutePerimetreDansInfoBean(nbItems, code);
            }
            infoBean.set("ACTION_PERIMETRE", Boolean.TRUE);
        }
        if (action.contains("SUPPRIMER_CODE_OBJET")) {
            /* Suppression de l'élément courant */
            final int indiceDiese = action.indexOf("#");
            final int indice = Integer.parseInt(action.substring(indiceDiese + 1));
            for (int j = indice; j < infoBean.getInt("CODE_OBJETS_NB_ITEMS") - 1; j++) {
                infoBean.set("CODE_OBJET#" + j, infoBean.get("CODE_OBJET#" + (j + 1)));
            }
            infoBean.setInt("CODE_OBJETS_NB_ITEMS", infoBean.getInt("CODE_OBJETS_NB_ITEMS") - 1);
            infoBean.set("ACTION_PERIMETRE", Boolean.TRUE);
        }
        if (InfoBean.ACTION_VALIDER.equals(action)) {
            //on contrôle le périmètre dans lequel a été saisi l'encadré
            if (!autorisations.isWebMaster() && !autorisations.possedePermission(new PermissionBean("TECH", "enc", "M"), new Perimetre(infoBean.getString("CODE_RATTACHEMENT"), infoBean.getString("CODE_RUBRIQUE"), "*", "*", ""))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + " : " + MessageHelper.getCoreMessage("AUTORISATION_ACTION_HORS_PERIMETRE"));
            }
            encadre.setIntitule((String) infoBean.get("INTITULE"));
            encadre.setActif((String) infoBean.get("ACTIF"));
            encadre.setLangue((String) infoBean.get("LANGUE"));
            encadre.setPoids((Integer) infoBean.get("POIDS"));
            encadre.setContenu((String) infoBean.get("CONTENU"));
            // JSS 20050713 : ajout code pour e-sup
            encadre.setCode((String) infoBean.get("CODE"));
            encadre.setCodeRattachement((String) infoBean.get("CODE_RATTACHEMENT"));
            encadre.setCodeRubrique((String) infoBean.get("CODE_RUBRIQUE"));
            /* Vecteur des objets */
            // on parcourt les périmètres choisis pour savoir si l'ensemble de ces périmètres entrent dans ceux autorisés pour cet utilisateur
            final List<String> objets = new ArrayList<>();
            String sObjet;
            for (int j = 0; j < infoBean.getInt("CODE_OBJETS_NB_ITEMS"); j++) {
                sObjet = infoBean.getString("CODE_OBJET#" + j);
                if (sObjet.startsWith("RUB_")) {
                    //test sur perimetre rubrique
                    if (!autorisations.isWebMaster() && !autorisations.possedePermission(new PermissionBean("TECH", "enc", "M"), new Perimetre("-", sObjet.substring(4), "*", "*", ""))) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + MessageHelper.getCoreMessage("SAISIE_ENCADRE_PAS_DROIT_PERIMETRE"));
                    }
                } else if (sObjet.contains("/")) {
                    //test sur structure
                    if (!autorisations.isWebMaster() && !autorisations.possedePermission(new PermissionBean("TECH", "enc", "M"), new Perimetre(sObjet.substring(sObjet.indexOf("/") + 1), "-", "*", "*", ""))) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + MessageHelper.getCoreMessage("SAISIE_ENCADRE_PAS_DROIT_PERIMETRE"));
                    }
                }
                objets.add(sObjet);
            }
            //si aucun périmètre n'est défini, l'encadré s'applique à tous les contenus, et on doit donc contrôler les permissions
            if (objets.size() == 0) {
                //test sur tout périmetre
                if (!autorisations.isWebMaster() && !autorisations.possedePermission(new PermissionBean("TECH", "enc", "M"), new Perimetre("-", "-", "*", "*", ""))) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + MessageHelper.getCoreMessage("SAISIE_ENCADRE_PAS_DROIT_PERIMETRE"));
                }
            }
            serviceEncadre.setListObjet(encadre, objets);
            if (mode == MODE_AJOUT) {
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_CREATION_ENCADRE"), encadre.getIntitule());
                infoBean.addMessageConfirmation(confirmation);
            }
            if (mode == MODE_MODIFICATION) {
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_MODIFICATION_ENCADRE"), encadre.getIntitule());
                infoBean.addMessageConfirmation(confirmation);
            }
            serviceEncadre.save(encadre);
            etat = FIN;
        } else if (InfoBean.ACTION_SUPPRIMER.equals(action)) {
            supprimerEncadre();
        }
    }

    /**
     * Ajoute la valeur dans le périmètre ssi elle n'est pas déjà présente
     *
     * @param nbItems
     * @param valeurPerimetre
     */
    private void ajoutePerimetreDansInfoBean(final int nbItems, final String valeurPerimetre) {
        if (isPerimetreUnique(valeurPerimetre)) {
            infoBean.set("CODE_OBJET#" + nbItems, valeurPerimetre);
            infoBean.setInt("CODE_OBJETS_NB_ITEMS", nbItems + 1);
        }
    }

    /**
     * Vérifie si le périmètre n'est pas déjà présent dans l'infobean
     *
     * @param valeurPerimetre
     * @return
     */
    private boolean isPerimetreUnique(final String valeurPerimetre) {
        boolean isUnique = Boolean.TRUE;
        for (int i = 0; i < infoBean.getInt("CODE_OBJETS_NB_ITEMS"); i++) {
            if (valeurPerimetre.equals(infoBean.get("CODE_OBJET#" + i))) {
                isUnique = Boolean.FALSE;
                break;
            }
        }
        return isUnique;
    }

    private void supprimerEncadre() throws ErreurApplicative {
        if (!autorisations.isWebMaster() && !autorisations.possedePermission(new PermissionBean("TECH", "enc", "M"), new Perimetre(infoBean.getString("CODE_RATTACHEMENT"), infoBean.getString("CODE_RUBRIQUE"), "*", "*", ""))) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
        }
        serviceEncadre.delete(encadre.getId());
        final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_ENCADRE"), encadre.getIntitule());
        infoBean.addMessageConfirmation(confirmation);
        etat = FIN;
    }
}
