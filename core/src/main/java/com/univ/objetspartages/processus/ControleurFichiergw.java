/*
 * Created on 3 déc. 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.objetspartages.processus;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.kportal.extension.module.plugin.objetspartages.DefaultPluginFiche;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.ContexteUtil;
// TODO: Auto-generated Javadoc

/**
 * The Class ControleurFichiergw.
 */
public class ControleurFichiergw extends DefaultPluginFiche {

    private static final Logger LOG = LoggerFactory.getLogger(ControleurFichiergw.class);

    private ServiceRessource serviceRessource;

    public void setServiceRessource(final ServiceRessource serviceRessource) {
        this.serviceRessource = serviceRessource;
    }

    private String getFormattedCreationDate(RessourceBean ressource, Locale locale) {
        final Date dateCreation = RessourceUtils.getDateCreation(ressource);
        if(dateCreation != null) {
            return DateFormat.getDateInstance(DateFormat.SHORT, locale).format(dateCreation);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Preparer principal.
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     *
     */
    @Override
    public void preparerPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final MetatagBean meta) {
        if (ficheUniv.getIdFiche().equals(0L)) {
            return;
        }
        // traitement uniquement pour les fiches avec fichier joints
        if (super.isActive(ficheUniv.getClass().getName())) {
            final OMContext ctx = ContexteUtil.getContexteUniv();
            List<RessourceBean> listeFichier = serviceRessource.getFiles(ficheUniv);
            String totalFichierJoint = "";
            String mode = "";
            String libelleFichierJoint = "";
            String codeParent = "";
            String indice = "";
            int nbTotal = 1;
            String nomDonnee = "FICHIER_MULTIPLE";
            if (CollectionUtils.isNotEmpty(listeFichier)) {
                for (final RessourceBean currentRessource : listeFichier) {
                    codeParent = currentRessource.getCodeParent();
                    mode = RessourceUtils.getTypeRessource(currentRessource);
                    final String indiceCourant = codeParent.substring(codeParent.indexOf("NO=") + 3, codeParent.length());
                    if (!indice.equals(indiceCourant)) {
                        if (totalFichierJoint.length() > 0) {
                            infoBean.put("TOTAL_" + nomDonnee + "_" + indice, totalFichierJoint);
                            infoBean.put("MODE_" + nomDonnee + "_" + indice, mode);
                            nbTotal++;
                        }
                        totalFichierJoint = "";
                        indice = indiceCourant;
                    }
                    if (totalFichierJoint.length() > 0) {
                        totalFichierJoint += "|";
                    }
                    libelleFichierJoint = "";
                    if (StringUtils.isNotBlank(RessourceUtils.getTitre(currentRessource))) {
                        libelleFichierJoint = RessourceUtils.getTitre(currentRessource) + " - ";
                    }
                    libelleFichierJoint += RessourceUtils.getSource(currentRessource);
                    totalFichierJoint += currentRessource.getId().toString() + ";" + libelleFichierJoint.replaceAll(";", ",").replaceAll("\\|", " ") + ";" + RessourceUtils.getFormat(currentRessource).replaceAll(";", ",") + ";" + RessourceUtils.getLegende(currentRessource).replaceAll(";", ",") + ";" + getFormattedCreationDate(currentRessource, ctx.getLocale());
                }
                infoBean.put("TOTAL_" + nomDonnee + "_" + indice, totalFichierJoint);
                infoBean.put("MODE_" + nomDonnee + "_" + indice, mode);
                infoBean.put("NB_" + nomDonnee, Integer.toString(nbTotal));
            }
            listeFichier = serviceRessource.getFilesIndex(ficheUniv);
            String fichierUniqueIndice;
            nomDonnee = "FICHIER_UNIQUE";
            if (CollectionUtils.isNotEmpty(listeFichier)) {
                int i;
                for (i = 0; i < listeFichier.size(); i++) {
                    final RessourceBean currentRessource = listeFichier.get(i);
                    codeParent = currentRessource.getCodeParent();
                    indice = codeParent.substring(codeParent.indexOf("NO=") + 3, codeParent.length());
                    libelleFichierJoint = "";
                    if (StringUtils.isNotBlank(RessourceUtils.getTitre(currentRessource))) {
                        libelleFichierJoint = RessourceUtils.getTitre(currentRessource) + " - ";
                    }
                    libelleFichierJoint += RessourceUtils.getSource(currentRessource);
                    fichierUniqueIndice = currentRessource.getId().toString() + ";" + libelleFichierJoint.replaceAll(";", ",").replaceAll("\\|", " ") + ";" + RessourceUtils.getFormat(currentRessource).replaceAll(";", ",") + ";" + RessourceUtils.getLegende(currentRessource).replaceAll(";", ",") + ";" + getFormattedCreationDate(currentRessource, ctx.getLocale());
                    infoBean.put(nomDonnee + "_" + indice, fichierUniqueIndice);
                    mode = RessourceUtils.getTypeRessource(currentRessource);
                    infoBean.put("LIBELLE_" + nomDonnee + "_" + indice, libelleFichierJoint);
                    infoBean.put("MODE_" + nomDonnee + "_" + indice, mode);
                }
                infoBean.put("NB_" + nomDonnee, Integer.toString(i));
            }
        }
    }

    /**
     * Traiter principal.
     *
     * @param infoBean
     *            the _info bean
     * @param ficheUniv
     *            the _fiche univ
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public void traiterPrincipal(final Map<String, Object> infoBean, final FicheUniv ficheUniv, final MetatagBean metatag) {
        // traitement générique sur la gestion des ressources dans les contenus de type toolbox
        serviceRessource.saveContentResource(infoBean, ficheUniv);
        // traitement uniquement pour les fiches déclarées sur le plugin
        if (super.isActive(ficheUniv.getClass().getName())) {
            String nomDonnee = "FICHIER_MULTIPLE";
            if (infoBean.get("NB_" + nomDonnee) != null) {
                int nombreFichier = 0;
                try {
                    nombreFichier = Integer.parseInt((String) infoBean.get("NB_" + nomDonnee));
                } catch (final NumberFormatException e) {
                    LOG.debug("unable to parse the files number", e);
                }
                if (nombreFichier > 0) {
                    for (int i = 1; i <= nombreFichier; i++) {
                        if (infoBean.get("TOTAL_" + nomDonnee + "_" + i) != null) {
                            serviceRessource.syncFiles(ficheUniv, (String) infoBean.get("TOTAL_" + nomDonnee + "_" + i), i);
                        }
                    }
                }
            }
            nomDonnee = "FICHIER_UNIQUE";
            if (infoBean.get("NB_" + nomDonnee) != null) {
                int nombreFichier = 0;
                try {
                    nombreFichier = Integer.parseInt((String) infoBean.get("NB_" + nomDonnee));
                } catch (final NumberFormatException e) {
                    LOG.debug("unable to parse the files number", e);
                }
                if (nombreFichier > 0) {
                    for (int i = 1; i <= nombreFichier; i++) {
                        if (infoBean.get(nomDonnee + "_" + i) != null) {
                             serviceRessource.syncFile(ficheUniv, (String) infoBean.get(nomDonnee + "_" + i), i);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void supprimerObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjetCible) {
        serviceRessource.deleteFiles(ficheUniv);
    }

    @Override
    public boolean isActive(final String classe) {
        // ce plugin est actif pour toutes les fiches car il doit a minima nettoyer les ressources du contenu (toolbox)
        return Boolean.TRUE;
    }
}
