package com.univ.objetspartages.processus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.CodeLibelle;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.core.autorisation.ActionPermission;
import com.kportal.core.autorisation.Permission;
import com.kportal.core.autorisation.Permission.Type;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.ExtensionManager;
import com.kportal.extension.IExtension;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.ModuleHelper;
import com.kportal.extension.module.ModuleManager;
import com.kportal.extension.module.composant.ComposantRole;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RoleBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.cache.CacheRoleManager;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRole;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.Chaine;

/**
 * Processus saisie Role.
 */
public class SaisieRole extends ProcessusBean {

    /** The Constant MODE_AJOUT. */
    private static final int MODE_AJOUT = 0;

    /** The Constant MODE_MODIFICATION. */
    private static final int MODE_MODIFICATION = 1;

    /** The Constant ECRAN_ACCUEIL. */
    private static final String ECRAN_ACCUEIL = "ACCUEIL";

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    private static final Logger LOG = LoggerFactory.getLogger(SaisieRole.class);

    private final ServiceRole serviceRole;

    /** The role. */
    private RoleBean role;

    /** The mode. */
    private int mode = -1;

    /**
     * processus saisie Role.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieRole(final InfoBean ciu) {
        super(ciu);
        serviceRole = ServiceManager.getServiceForBean(RoleBean.class);
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            if (!ComposantRole.isAutoriseParActionProcessus(autorisations, null)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            try {
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                etat = EN_COURS;
                if (ecranLogique == null) {
                    if ("ACCUEIL".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                        infoBean.set("LISTE_ROLES", serviceRole.getAll());
                        ecranLogique = ECRAN_ACCUEIL;
                    } else if ("AJOUTER".equals(action)) {
                        mode = MODE_AJOUT;
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
                        role = new RoleBean();
                        preparerPRINCIPAL();
                    } else if ("MODIFIER".equals(action)) {
                        final String idRole = infoBean.getString("ID_ROLE");
                        if (StringUtils.isNotBlank(idRole) && StringUtils.isNumeric(idRole)) {
                            role = serviceRole.getById(Long.valueOf(idRole));
                        }
                        if (role == null) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "ROLE.ERREUR.ROLE_INEXISTANT"));
                        }
                        mode = MODE_MODIFICATION;
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                        preparerPRINCIPAL();
                    } else if ("SUPPRIMER".equals(action)) {
                        traiterSUPPRIMER();
                    }
                } else if (ecranLogique.equals(ECRAN_PRINCIPAL)) {
                    if (action.equals(InfoBean.ACTION_VALIDER)) {
                        traiterPRINCIPAL();
                    } else if (action.equals(InfoBean.ACTION_SUPPRIMER)) {
                        traiterSUPPRIMER();
                    }
                }
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOG.error("Erreur de traitement sur le processus", e);
                infoBean.addMessageErreur(e.toString());
            }
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Affichage de l'écran de saisie d'un Role.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerPRINCIPAL() throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.set("CODE", role.getCode());
        infoBean.set("LIBELLE", role.getLibelle());
        final Perimetre perimetre = new Perimetre(role.getPerimetre());
        // Structure
        infoBean.set("STRUCTURE_TYPE_PERIMETRE", "");
        infoBean.set("STRUCTURE_PERIMETRE", "");
        infoBean.set("LIBELLE_STRUCTURE_PERIMETRE", "");
        if ("".equals(perimetre.getCodeStructure())) {
            infoBean.set("STRUCTURE_TYPE_PERIMETRE", "0");
        } else if ("-".equals(perimetre.getCodeStructure())) {
            infoBean.set("STRUCTURE_TYPE_PERIMETRE", "2");
        } else {
            infoBean.set("STRUCTURE_TYPE_PERIMETRE", "1");
            infoBean.set("STRUCTURE_PERIMETRE", perimetre.getCodeStructure());
            infoBean.set("LIBELLE_STRUCTURE_PERIMETRE", serviceStructure.getDisplayableLabel(perimetre.getCodeStructure(), ""));
        }
        // Rubrique
        infoBean.set("RUBRIQUE_TYPE_PERIMETRE", "");
        infoBean.set("RUBRIQUE_PERIMETRE", "");
        infoBean.set("LIBELLE_RUBRIQUE_PERIMETRE", "");
        if ("".equals(perimetre.getCodeRubrique())) {
            infoBean.set("RUBRIQUE_TYPE_PERIMETRE", "0");
        } else if ("-".equals(perimetre.getCodeRubrique())) {
            infoBean.set("RUBRIQUE_TYPE_PERIMETRE", "2");
        } else {
            infoBean.set("RUBRIQUE_TYPE_PERIMETRE", "1");
            infoBean.set("RUBRIQUE_PERIMETRE", perimetre.getCodeRubrique());
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(perimetre.getCodeRubrique());
            if (rubriqueBean != null) {
                infoBean.set("LIBELLE_RUBRIQUE_PERIMETRE", rubriqueBean.getIntitule());
            } else {
                infoBean.set("LIBELLE_RUBRIQUE_PERIMETRE", StringUtils.EMPTY);
            }
        }
        // Groupes
        infoBean.set("GROUPE_TYPE_PERIMETRE", "");
        infoBean.set("GROUPE_PERIMETRE", "");
        infoBean.set("LIBELLE_GROUPE_PERIMETRE", "");
        if ("".equals(perimetre.getCodeGroupe())) {
            infoBean.set("GROUPE_TYPE_PERIMETRE", "0");
        } else if ("-".equals(perimetre.getCodeGroupe())) {
            infoBean.set("GROUPE_TYPE_PERIMETRE", "2");
        } else {
            infoBean.set("GROUPE_TYPE_PERIMETRE", "1");
            infoBean.set("GROUPE_PERIMETRE", perimetre.getCodeGroupe());
            infoBean.set("LIBELLE_GROUPE_PERIMETRE", serviceGroupeDsi.getIntitule(perimetre.getCodeGroupe()));
        }
        if (Espacecollaboratif.isExtensionActivated()) {
            if ("*".equals(perimetre.getCodeEspaceCollaboratif())) {
                infoBean.set("ESPACE_COLLABORATIF_COURANT_PERIMETRE", "1");
            } else {
                infoBean.set("ESPACE_COLLABORATIF_COURANT_PERIMETRE", "0");
            }
        }
        infoBean.set("PERMISSIONS", role.getPermissions());
        infoBean.set("LISTE_TECHS", CodeLibelle.lireTable(infoBean.getNomExtension(), "permission_objet_tech", null));
        // Initialisation des permissions par extension par type
        final ModuleManager moduleManager = ModuleHelper.getModuleManager();
        final ExtensionManager extensionManager = ExtensionHelper.getExtensionManager();
        final EnumMap<Type, List<Permission>> permissionsByType = new EnumMap<>(Type.class);
        if (moduleManager.getModules().size() > 0) {
            // boucle par extension
            for (final IExtension extension : extensionManager.getExtensions().values()) {
                final List<IModule> modules = new ArrayList<>();
                modules.addAll(extension.getModules());
                Collections.sort(modules, new Comparator<IModule>() {

                    @Override
                    public int compare(final IModule o1, final IModule o2) {
                        int nbActionsO1 = 0;
                        int nbActionsO2 = 0;
                        for (final Permission permission : o1.getPermissions()) {
                            nbActionsO1 += permission.getActions().size();
                        }
                        for (final Permission permission : o2.getPermissions()) {
                            nbActionsO2 += permission.getActions().size();
                        }
                        return nbActionsO2 - nbActionsO1;
                    }
                });
                final Map<String, Permission> permissionMap = ApplicationContextManager.getBeansOfType(extension.getId(), Permission.class);
                for (final Permission permission : permissionMap.values()) {
                    if (permissionsByType.get(permission.getType()) == null) {
                        permissionsByType.put(permission.getType(), new ArrayList<Permission>());
                    }
                    permissionsByType.get(permission.getType()).add(permission);
                }
                for (final IModule module : modules) {
                    if (module.getEtat() == IModule.ETAT_ACTIF) {
                        for (final Permission permission : module.getPermissions()) {
                            if (permissionsByType.get(permission.getType()) == null) {
                                permissionsByType.put(permission.getType(), new ArrayList<Permission>());
                            }
                            permissionsByType.get(permission.getType()).add(permission);
                        }
                    }
                }
            }
        }
        /* 0 - ONGLET 0 CONTENU : fiches et gestion éditoriale */
        infoBean.setInt("DEBUT_ONGLET_0", 0);
        int i = 0;
        int j;
        int k = 0;
        int indiceOnglet = 0;
        final Map<String, String> hPermissionsValidation = AutorisationBean.getListeNiveauxApprobation();
        final Set<String> tmPermissionsValidation = AutorisationBean.getListePermissionsValidation();
        final String[] ordre = ReferentielObjets.getOrdreOnglets();
        // Boucle sur les fiches
        while (k < ordre.length) {
            final String codeObjet = ordre[k];
            final Objetpartage objet = ReferentielObjets.getObjetByCode(codeObjet);
            if (ReferentielObjets.instancierFiche(codeObjet) == null || objet.isStrictlyCollaboratif()) {
                k++;
                continue;
            }
            if (i == 0) {
                infoBean.set("PERMISSION_ONGLET_" + i, "contenu");
                infoBean.set("PERMISSION_PERIMETRE_" + i + "_0", "rubrique");
                infoBean.set("PERMISSION_PERIMETRE_" + i + "_1", "structure");
                infoBean.set("PERMISSION_TITRE_" + i, "Fiches");
                indiceOnglet++;
            }
            infoBean.set("PERMISSION_TYPE_" + i, "FICHE");
            infoBean.set("PERMISSION_CODE_OBJET_" + i, codeObjet);
            infoBean.set("PERMISSION_INTITULE_" + i, ReferentielObjets.getLibelleObjet(codeObjet));
            infoBean.set("PERMISSION_HORS_PERIMETRE_" + i, "0");
            infoBean.set("PERMISSION_ACTION_" + i + "_0", "C");
            if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "C")) {
                infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_0", "1");
            }
            infoBean.set("PERMISSION_ACTION_" + i + "_1", "M");
            if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "M")) {
                infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_1", "1");
            }
            infoBean.set("PERMISSION_ACTION_" + i + "_2", "D");
            if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "D")) {
                infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_2", "1");
            }
            infoBean.set("PERMISSION_ACTION_" + i + "_3", "S");
            if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "S")) {
                infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_3", "1");
            }
            // RP20050809 ajout des permissions d'approbation
            j = 4;
            for (final String code : tmPermissionsValidation) {
                infoBean.set("PERMISSION_ACTION_" + i + "_" + j, code);
                if (i == 0) {
                    infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_" + j, hPermissionsValidation.get(code));
                }
                if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), code)) {
                    infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_" + j, "1");
                }
                j++;
            }
            // validation
            infoBean.set("PERMISSION_ACTION_" + i + "_" + j, "V");
            if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "V")) {
                infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_" + j, "1");
            }
            // notification
            infoBean.set("PERMISSION_ACTION_" + i + "_" + (j + 1), "N");
            if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "N")) {
                infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_" + (j + 1), "1");
            }
            // JSS 20051115 : ajout droit mise en ligne
            infoBean.set("PERMISSION_ACTION_" + i + "_" + (j + 2), "U");
            if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "U")) {
                infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_" + (j + 2), "1");
            }
            infoBean.set("PERMISSION_ACTION_" + i + "_" + (j + 3), "R");
            if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "R")) {
                infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_" + (j + 3), "1");
            }
            if (i == 0) {
                infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_0", "Création");
                infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_1", "Modification");
                infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_2", "Traduction");
                infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_3", "Suppression");
                infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_" + j, "Validation");
                infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_" + (j + 1), "Notification");
                infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_" + (j + 2), "Mise en ligne");
                infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_" + (j + 3), "Suppression");
            }
            // Boucle sur les permissions
            for (final PermissionBean permission : role.getPermissions()) {
                if ("FICHE".equals(permission.getType()) && permission.getObjet().equals(codeObjet)) {
                    if ("C".equals(permission.getAction())) {
                        infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_0", "1");
                    }
                    if ("M".equals(permission.getAction())) {
                        infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_1", "1");
                    }
                    if ("D".equals(permission.getAction())) {
                        infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_2", "1");
                    }
                    if ("S".equals(permission.getAction())) {
                        infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_3", "1");
                    }
                    // RP20050809 ajout des permissions d'approbation
                    j = 4;
                    for (final String code : tmPermissionsValidation) {
                        if (permission.getAction().equals(code)) {
                            infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_" + j, "1");
                        }
                        j++;
                    }
                    if ("V".equals(permission.getAction())) {
                        infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_" + j, "1");
                    }
                    if ("N".equals(permission.getAction())) {
                        infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_" + (j + 1), "1");
                    }
                    if ("U".equals(permission.getAction())) {
                        infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_" + (j + 2), "1");
                    }
                    if ("R".equals(permission.getAction())) {
                        infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_" + (j + 3), "1");
                    }
                }
            }
            infoBean.setInt("PERMISSIONS_NB_ACTIONS_" + i, j + 4);
            i++;
            k++;
        }// fin de boucle sur les objets
        final Map<String, String> hDejaAffiche = new HashMap<>();
        final Map<String, String> hPermissionsTechniques = CodeLibelle.lireTable(infoBean.getNomExtension(), "permission_objet_tech", null);
        i = ajouterPermissionTechnique(i, "rub", hPermissionsTechniques.get("rub"), "Gestion éditoriale", hDejaAffiche, new String[] {"C", "M", "S", "P"}, null);
        // RP 20060217 !!!!!!!! le droit de publication P/"pub" est ajouté dans la liste des droits "rub"
        // donc traité en spécifique dans la méthode ajouterPermissionTechnique et le traiter principal
        // i = ajouterPermissionTechnique( i, "pub", (String) hPermissionsTechniques.get( "pub"),  null, hDejaAffiche, null, "" );
        i = ajouterPermissionTechnique(i, "pho", hPermissionsTechniques.get("pho"), null, hDejaAffiche, new String[] {"U", "C", "M", "S"}, null);
        // RP 20060217 !!!!!!!! le droit d'utilisation U à la photothèque "acp" est ajouté dans la liste des droits "pho"
        // donc traité en spécifique dans la méthode ajouterPermissionTechnique et le traiter principal
        //i = ajouterPermissionTechnique( i, "acp", (String) hPermissionsTechniques.get( "acp"),  null, hDejaAffiche, null, "" );
        i = ajouterPermissionTechnique(i, "enc", hPermissionsTechniques.get("enc"), null, hDejaAffiche, new String[] {"M"}, null);
        // on ajoute les permissions du socle de type contenu
        if (CollectionUtils.isNotEmpty(permissionsByType.get(Type.CONTENU))) {
            i = ajouterPermissionExtension(i, permissionsByType.get(Type.CONTENU));
        }
        infoBean.setInt("FIN_ONGLET_0", i - 1);
        /* 1 - ONGLET 1 CONTRIBUTION AVANCEE : mode et fonctionnalités */
        infoBean.setInt("DEBUT_ONGLET_" + indiceOnglet, i);
        infoBean.set("PERMISSION_ONGLET_" + i, "contribution-avancee");
        i = ajouterPermissionTechnique(i, "mda", hPermissionsTechniques.get("mda"), "Mode de contribution", hDejaAffiche, null, null);
        // on ajoute les permissions de type contribution
        if (CollectionUtils.isNotEmpty(permissionsByType.get(Type.CONTRIBUTION))) {
            i = ajouterPermissionExtension(i, permissionsByType.get(Type.CONTRIBUTION));
        }
        infoBean.setInt("FIN_ONGLET_" + indiceOnglet, i - 1);
        indiceOnglet++;
        // RP 20060104 : activation du collaboratif
        final String activationDsi = PropertyHelper.getCoreProperty("dsi.activation");
        infoBean.set("GRS_SAISIE_DSI", "0");
        if ("1".equals(activationDsi)) {
            infoBean.set("GRS_SAISIE_DSI", "1");
            /* 2 - ONGLET 2 DIFFUSION : gestion de la dsi */
            infoBean.setInt("DEBUT_ONGLET_" + indiceOnglet, i);
            infoBean.set("PERMISSION_PERIMETRE_" + i + "_0", "groupe");
            infoBean.set("PERMISSION_ONGLET_" + i, "diffusion");
            // on ajoute les permissions de type diffusion
            if (CollectionUtils.isNotEmpty(permissionsByType.get(Type.DIFFUSION))) {
                i = ajouterPermissionExtension(i, permissionsByType.get(Type.DIFFUSION));
            }
            i = ajouterPermissionTechnique(i, "dsi", hPermissionsTechniques.get("dsi"), "Groupes", hDejaAffiche, null, null);
            infoBean.setInt("FIN_ONGLET_" + indiceOnglet, i - 1);
            indiceOnglet++;
        } else {
            hPermissionsTechniques.remove("dsi");
        }
        /* 3 - ONGLET 3 COLLABORATIF */
        if (Espacecollaboratif.isExtensionActivated()) {
            infoBean.setInt("DEBUT_ONGLET_" + indiceOnglet, i);
            k = 0;
            j = i;
            infoBean.set("PERMISSION_ONGLET_" + i, "collaboratif");
            // Boucle sur les fiches
            while (k < ordre.length) {
                final String codeObjet = ordre[k];
                if (!ReferentielObjets.getObjetByCode(codeObjet).isCollaboratif()) {
                    k++;
                    continue;
                }
                if (j == i) {
                    infoBean.set("PERMISSION_TITRE_" + i, "Fiches");
                }
                infoBean.set("PERMISSION_TYPE_" + i, "FICHE");
                infoBean.set("PERMISSION_CODE_OBJET_" + i, codeObjet);
                infoBean.set("PERMISSION_INTITULE_" + i, ReferentielObjets.getLibelleObjet(codeObjet));
                infoBean.set("PERMISSION_HORS_PERIMETRE_" + i, "0");
                infoBean.set("PERMISSION_ACTION_" + i + "_0", "C");
                if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "C")) {
                    infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_0", "1");
                }
                infoBean.set("PERMISSION_ACTION_" + i + "_1", "M");
                if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "M")) {
                    infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_1", "1");
                }
                infoBean.set("PERMISSION_ACTION_" + i + "_2", "S");
                if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "S")) {
                    infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_2", "1");
                }
                infoBean.set("PERMISSION_ACTION_" + i + "_3", "V");
                if (!ReferentielObjets.isActionParametrableDansRole(ReferentielObjets.getNomObjet(codeObjet), "V")) {
                    infoBean.set("PERMISSION_ACTION_DISABLED_" + i + "_3", "1");
                }
                if (j == i) {
                    infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_0", "Création");
                    infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_1", "Modification");
                    infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_2", "Suppression");
                    infoBean.set("PERMISSION_ACTION_LIBELLE_" + i + "_3", "Validation");
                }
                // Boucle sur les permissions
                for (final PermissionBean permission : role.getPermissions()) {
                    if ("FICHE".equals(permission.getType()) && permission.getObjet().equals(codeObjet)) {
                        if ("C".equals(permission.getAction())) {
                            infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_0", "1");
                        }
                        if ("M".equals(permission.getAction())) {
                            infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_1", "1");
                        }
                        if ("S".equals(permission.getAction())) {
                            infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_2", "1");
                        }
                        if ("V".equals(permission.getAction())) {
                            infoBean.set("PERMISSION_ACTION_SELECT_" + i + "_3", "1");
                        }
                    }
                }
                infoBean.setInt("PERMISSIONS_NB_ACTIONS_" + i, 4);
                k++;
                i++;
            }
            // on ajoute les permissions de type administration
            if (CollectionUtils.isNotEmpty(permissionsByType.get(Type.COLLABORATIF))) {
                i = ajouterPermissionExtension(i, permissionsByType.get(Type.COLLABORATIF));
            }
            i = ajouterPermissionTechnique(i, "ges", hPermissionsTechniques.get("ges"), "Gestion des espaces", hDejaAffiche, null, null);
            infoBean.setInt("FIN_ONGLET_" + indiceOnglet, i - 1);
            indiceOnglet++;
        } else {
            hPermissionsTechniques.remove("ges");
        }
        /* 4 - ONGLET 4 Administration  */
        infoBean.setInt("DEBUT_ONGLET_" + indiceOnglet, i);
        infoBean.set("PERMISSION_ONGLET_" + i, "administration");
        // on ajoute les permissions de type administration
        if (CollectionUtils.isNotEmpty(permissionsByType.get(Type.ADMINISTRATION))) {
            i = ajouterPermissionExtension(i, permissionsByType.get(Type.ADMINISTRATION));
        }
        i = ajouterPermissionTechnique(i, "wmg", hPermissionsTechniques.get("wmg"), "Super administrateur", hDejaAffiche, null, null);
        infoBean.setInt("FIN_ONGLET_" + indiceOnglet, i - 1);
        indiceOnglet++;
        /* 5 - ONGLET 5 Modules */
        if (CollectionUtils.isNotEmpty(permissionsByType.get(Type.MODULE))) {
            infoBean.setInt("DEBUT_ONGLET_" + indiceOnglet, i);
            infoBean.set("PERMISSION_ONGLET_" + i, "modules");
            // on ajoute les permissions de type module
            i = ajouterPermissionExtension(i, permissionsByType.get(Type.MODULE));
            infoBean.setInt("FIN_ONGLET_" + indiceOnglet, i - 1);
            indiceOnglet++;
            infoBean.set("GRS_SAISIE_MODULES", "1");
        }
        /* 6 - ONGLET 6 Scripts */
        if (CollectionUtils.isNotEmpty(permissionsByType.get(Type.SCRIPT))) {
            infoBean.setInt("DEBUT_ONGLET_" + indiceOnglet, i);
            infoBean.set("PERMISSION_ONGLET_" + i, "scripts");
            // on ajoute les permissions de type module
            i = ajouterPermissionExtension(i, permissionsByType.get(Type.SCRIPT));
            infoBean.setInt("FIN_ONGLET_" + indiceOnglet, i - 1);
            indiceOnglet++;
            infoBean.set("GRS_SAISIE_MODULES", "1");
        }
        infoBean.setInt("PERMISSIONS_NB_ITEMS", i);
        infoBean.setInt("ONGLETS_NB_ITEMS", indiceOnglet);
        if (StringUtils.isNotBlank(role.getLibelle())) {
            infoBean.setTitreEcran(role.getLibelle());
        }
    }

    private int ajouterPermissionExtension(int i, final List<Permission> permissions) {
        String idExtension = StringUtils.EMPTY;
        String section = null;
        for (final Permission permission : permissions) {
            final IExtension extension = ExtensionHelper.getExtension(permission.getIdExtension());
            if (extension != null && !permission.getIdExtension().equals(idExtension)) {
                idExtension = permission.getIdExtension();
                // creation d'une nouvelle seection pour chaque extension
                section = extension.getLibelleAffichable(extension.getLibelle());
            }
            i = ajouterPermissionModule(i, permission.getId(), permission.getCode(), section, permission.getLibelleAffichable(), permission.getActions());
            section = null;
        }
        return i;
    }

    /**
     * Ajouter permission technique.
     *
     * @param _nbItems
     *            the _nb items
     * @param objet
     *            the objet
     * @param intituleObjet
     *            the intitule objet
     * @param titreSection
     *            the titre rupture
     * @param hDejaAffiche
     *            the h deja affiche
     * @param codesAction
     *            the codes action
     * @param titreActionParDefaut
     *            the titre action par defaut
     *
     * @return the int
     *
     */
    private int ajouterPermissionTechnique(final int _nbItems, String objet, final String intituleObjet, final String titreSection, final Map<String, String> hDejaAffiche, final String codesAction[], final String titreActionParDefaut) {
        int nbItems = _nbItems;
        if (hDejaAffiche.get(objet) == null) {
            infoBean.set("PERMISSION_TITRE_" + nbItems, titreSection);
            infoBean.set("PERMISSION_TYPE_" + nbItems, PermissionBean.TYPE_TECHNIQUE);
            infoBean.set("PERMISSION_CODE_OBJET_" + nbItems, objet);
            infoBean.set("PERMISSION_INTITULE_" + nbItems, intituleObjet);
            infoBean.set("PERMISSION_HORS_PERIMETRE_" + nbItems, "0");
            // Parcours du fichier contenant les actions
            int nbActions = 0;
            final Map<String, String> listeActions = CodeLibelle.lireTable(infoBean.getNomExtension(), "permission_action_tech_" + objet, null);
            // codesAction permet de forcer l'ordre des actions
            if (codesAction == null) {
                if (listeActions.size() == 0) {
                    infoBean.set("PERMISSION_ACTION_" + nbItems + "_0", "");
                    nbActions = 1;
                } else {
                    for (final String action : listeActions.keySet()) {
                        infoBean.set("PERMISSION_ACTION_" + nbItems + "_" + nbActions, action);
                        nbActions++;
                    }
                }
            } else {
                for (final String element : codesAction) {
                    infoBean.set("PERMISSION_ACTION_" + nbItems + "_" + nbActions, element);
                    nbActions++;
                }
            }
            final String objetSave = objet;
            // Recherche de la permission pour chaque action
            int j;
            for (j = 0; j < nbActions; j++) {
                String codeAction = infoBean.getString("PERMISSION_ACTION_" + nbItems + "_" + j);
                final String codeActionPourLibelle = codeAction;
                objet = objetSave;
                if ("rub".equals(objet) && "P".equals(codeAction)) {
                    objet = "pub";
                    codeAction = "";
                }
                if ("pho".equals(objet) && "U".equals(codeAction)) {
                    objet = "acp";
                    codeAction = "";
                }
                // Recherche libellé associé pour afficher l'entete
                final String libelleAction = listeActions.get(codeActionPourLibelle);
                if (libelleAction == null) {
                    if (titreActionParDefaut != null) {
                        infoBean.set("PERMISSION_ACTION_LIBELLE_" + nbItems + "_" + j, titreActionParDefaut);
                    }
                } else {
                    infoBean.set("PERMISSION_ACTION_LIBELLE_" + nbItems + "_" + j, libelleAction);
                }
                for (final PermissionBean permission : role.getPermissions()) {
                    // Recherche de la permission
                    if ("TECH".equals(permission.getType()) && permission.getObjet().equals(objet) && permission.getAction().equals(codeAction)) {
                        infoBean.set("PERMISSION_ACTION_SELECT_" + nbItems + "_" + j, "1");
                    }
                }
            }
            infoBean.setInt("PERMISSIONS_NB_ACTIONS_" + nbItems, j);
            hDejaAffiche.put(objet, "");
            nbItems++;
        }
        return nbItems;
    }

    /**
     * Ajouter permission technique.
     *
     * @param _nbItems
     *            the _nb items
     * @param type
     *            the objet
     * @param objet
     *            the objet
     * @param section
     *            the objet
     * @param intituleObjet
     *            the intitule objet
     * @param actions
     *            the titre rupture
     * @return the int
     *
     */
    private int ajouterPermissionModule(final int _nbItems, final String type, final String objet, final String section, final String intituleObjet, final List<ActionPermission> actions) {
        int nbItems = _nbItems;
        infoBean.set("PERMISSION_TYPE_" + nbItems, type.toUpperCase());
        infoBean.set("PERMISSION_CODE_OBJET_" + nbItems, objet);
        infoBean.set("PERMISSION_INTITULE_" + nbItems, intituleObjet);
        infoBean.set("PERMISSION_HORS_PERIMETRE_" + nbItems, "0");
        infoBean.set("PERMISSION_TITRE_" + nbItems, section);
        if (actions.isEmpty()) {
            actions.add(new ActionPermission());
        }
        int nbActions = 0;
        for (final ActionPermission action : actions) {
            infoBean.set("PERMISSION_ACTION_" + nbItems + "_" + nbActions, action.getCode());
            if (action.getLibelle().length() > 0) {
                infoBean.set("PERMISSION_ACTION_LIBELLE_" + nbItems + "_" + nbActions, action.getLibelle());
            }
            for (final PermissionBean permission : role.getPermissions()) {
                if ((permission.getType().equals(type.toUpperCase())) && (permission.getObjet().equals(objet)) && (permission.getAction().equals(action.getCode()))) {
                    infoBean.set("PERMISSION_ACTION_SELECT_" + nbItems + "_" + nbActions, "1");
                }
            }
            nbActions++;
        }
        infoBean.setInt("PERMISSIONS_NB_ACTIONS_" + nbItems, nbActions);
        nbItems++;
        return nbItems;
    }

    /**
     * Gestion de l'écran de saisie d'un Role.
     *
     * @throws ErreurApplicative
     *             the exception
     */
    private void traiterPRINCIPAL() throws ErreurApplicative {
        // Vérifie que le code ne contient que des caractères alphanumériques ou '-' ou '_'
        Chaine.controlerCodeMetier((String) infoBean.get("CODE"));
        role.setCode(infoBean.getString("CODE"));
        role.setLibelle(infoBean.getString("LIBELLE"));
        String perimetreCodeEspaceCollaboratif = "";
        if ("1".equals(infoBean.getString("ESPACE_COLLABORATIF_COURANT_PERIMETRE"))) {
            perimetreCodeEspaceCollaboratif = "*";
        }
        String structurePerimetre = "";
        if (StringUtils.isNotBlank(infoBean.getString("STRUCTURE_PERIMETRE"))) {
            structurePerimetre = infoBean.getString("STRUCTURE_PERIMETRE");
        }
        String rubriquePerimetre = "";
        if (StringUtils.isNotBlank(infoBean.getString("RUBRIQUE_PERIMETRE"))) {
            rubriquePerimetre = infoBean.getString("RUBRIQUE_PERIMETRE");
        }
        String groupePerimetre = "";
        if (StringUtils.isNotBlank(infoBean.getString("GROUPE_PERIMETRE"))) {
            groupePerimetre = infoBean.getString("GROUPE_PERIMETRE");
        }
        role.setPerimetre(new Perimetre(structurePerimetre, rubriquePerimetre, "", groupePerimetre, perimetreCodeEspaceCollaboratif).getChaineSerialisee());
        /* Vecteur des permissions */
        final Collection<PermissionBean> v = new ArrayList<>();
        for (int j = 0; j < infoBean.getInt("PERMISSIONS_NB_ITEMS"); j++) {
            for (int k = 0; k < infoBean.getInt("PERMISSIONS_NB_ACTIONS_" + j); k++) {
                final String permissionAction = infoBean.getString("PERMISSION_ACTION_" + j + "_" + k);
                if (permissionAction != null) {
                    final String select = infoBean.getString("PERMISSION_ACTION_SELECT_" + j + "_" + k);
                    if ((select != null) && ("1".equals(select))) {
                        if ("rub".equals(infoBean.getString("PERMISSION_CODE_OBJET_" + j)) && "P".equals(permissionAction)) {
                            v.add(new PermissionBean(infoBean.getString("PERMISSION_TYPE_" + j), "pub", ""));
                        } else if ("pho".equals(infoBean.getString("PERMISSION_CODE_OBJET_" + j)) && "U".equals(permissionAction)) {
                            v.add(new PermissionBean(infoBean.getString("PERMISSION_TYPE_" + j), "acp", ""));
                        } else {
                            v.add(new PermissionBean(infoBean.getString("PERMISSION_TYPE_" + j), infoBean.getString("PERMISSION_CODE_OBJET_" + j), permissionAction));
                        }
                    }
                }
            }
        }
        role.setPermissions(v);
        if (mode == MODE_AJOUT) {
            if (serviceRole.getByCode(role.getCode()) != null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "ROLE.ERREUR.CODE_EXISTANT"));
            }
            serviceRole.save(role);
            final CacheRoleManager cacheRoleManager = ApplicationContextManager.getCoreContextBean(CacheRoleManager.ID_BEAN, CacheRoleManager.class);
            cacheRoleManager.flush();
            final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_CREATION_ROLE"), role.getLibelle());
            infoBean.addMessageConfirmation(confirmation);
        } else if (mode == MODE_MODIFICATION) {
            serviceRole.save(role);
            final CacheRoleManager cacheRoleManager = ApplicationContextManager.getCoreContextBean(CacheRoleManager.ID_BEAN, CacheRoleManager.class);
            cacheRoleManager.flush();
            final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_MODIFICATION_ROLE"), role.getLibelle());
            infoBean.addMessageConfirmation(confirmation);
        }
        etat = FIN;
    }

    /**
     * Gestion de la suppression d'un rôle.
     *
     * @throws Exception exception lors de la suppression
     */
    private void traiterSUPPRIMER() {
        final RoleBean role = serviceRole.getById(Long.valueOf(infoBean.getString("ID_ROLE")));
        if(role != null) {
            final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_ROLE"), role.getLibelle());
            infoBean.addMessageConfirmation(confirmation);
            serviceRole.delete(role.getId());
            final CacheRoleManager cacheRoleManager = ApplicationContextManager.getCoreContextBean(CacheRoleManager.ID_BEAN, CacheRoleManager.class);
            cacheRoleManager.flush();
        } else {
            infoBean.addMessageErreur(MessageHelper.getCoreMessage("ERREUR_SUPPRESSION_ROLE_INCONNU"));
        }
        etat = FIN;
    }
}
