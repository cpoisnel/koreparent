package com.univ.objetspartages.processus;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.PageLibre;
import com.univ.objetspartages.om.ParagrapheBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;

/**
 * Processus de gestion des pagelibres.
 */
public class SaisiePageLibre extends SaisieFiche {

    /** The pagelibre. */
    private PageLibre pagelibre = null;

    /**
     * Constructeur.
     *
     * @param infoBean
     *            the info bean
     */
    public SaisiePageLibre(final InfoBean infoBean) {
        super(infoBean);
    }

    /**
     * Point d'entree du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        infoBean.set("CODE_OBJET", "0016");
        pagelibre = new PageLibre();
        pagelibre.init();
        traiterActionParDefaut(pagelibre);
        infoBean.set("NOM_ONGLET", "pagelibre");
        // on continue si on n'est pas a la FIN !!!
        return etat == FIN;
    }

    /**
     * Affichage de l'ecran des criteres de recherche d'un(e) pagelibre.
     *
     */
    @Override
    protected void preparerRECHERCHE() {
        ecranLogique = ECRAN_RECHERCHE;
        infoBean.set("LISTE_LANGUES", LangueUtil.getListeLangues());
    }

    /**
     * Affichage de l'ecran de saisie d'une pagelibre.
     *
     * @throws Exception
     *             the exception
     */
    @Override
    protected void preparerPRINCIPAL() throws Exception {
        infoBean.set("TITRE", pagelibre.getTitre());
        infoBean.set("RATTACHEMENT_BANDEAU", pagelibre.getRattachementBandeau());
        infoBean.set("COMPLEMENTS", pagelibre.getComplements());
        // Récupération des paragraphes
        final Vector<ParagrapheBean> listeParagraphes = pagelibre.getParagraphes();
        final Enumeration<ParagrapheBean> e = listeParagraphes.elements();
        ParagrapheBean paragraphe = null;
        // en mode création, on crée un premier paragraphe, ou quand aucun paragraphe n'a été saisi.
        if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet()) || pagelibre.getContenu().length() == 0) {
            final int nbItems = 0;
            infoBean.set("CONTENU_" + nbItems, "");
            infoBean.setInt("LIGNE_" + nbItems, nbItems + 1);
            infoBean.setInt("COLONNE_" + nbItems, 1);
            infoBean.setInt("LARGEUR_" + nbItems, 100);
            infoBean.set("PARAGRAPHES_NB_ITEMS", "" + (nbItems + 1));
        } else {
            int i = 0;
            while (e.hasMoreElements()) {
                paragraphe = e.nextElement();
                infoBean.set("CONTENU_" + i, paragraphe.getContenu());
                infoBean.setInt("LIGNE_" + i, paragraphe.getLigne());
                infoBean.setInt("COLONNE_" + i, paragraphe.getColonne());
                infoBean.setInt("LARGEUR_" + i, paragraphe.getLargeur());
                i++;
            }
            infoBean.set("PARAGRAPHES_NB_ITEMS", "" + i);
        }
        infoBean.set("CODE_RUBRIQUE", pagelibre.getCodeRubrique());
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(pagelibre.getCodeRubrique());
        String rubriqueLabel = StringUtils.EMPTY;
        if (rubriqueBean != null) {
            rubriqueLabel = rubriqueBean.getIntitule();
        }
        infoBean.set("LIBELLE_CODE_RUBRIQUE", rubriqueLabel);
        infoBean.set("CODE_RATTACHEMENT", pagelibre.getCodeRattachement());
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        infoBean.set("LIBELLE_CODE_RATTACHEMENT", serviceStructure.getDisplayableLabel(pagelibre.getCodeRattachement(), pagelibre.getLangue()));
        infoBean.set("SOUS_ONGLET", "PRINCIPAL"); // onglet par défaut
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.set("ID_PAGELIBRE", pagelibre.getIdFiche().toString());
        infoBean.set("LIBELLE_AFFICHABLE", pagelibre.getTitre());
        // Appel traitement par défaut
        ControleurUniv.preparerPRINCIPAL(infoBean, pagelibre, this);
    }

    /**
     * Traite l'ecran de saisie d'une fiche.
     *
     * @throws Exception
     *             the exception
     */
    @Override
    protected void traiterPRINCIPAL() throws Exception {
        // chargement de l'objet
        if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
            pagelibre.init();
            // La langue doit être valorisée pour lr traitement générique ControleurUniv.preparerPrincipal
            if (infoBean.get("LANGUE") != null) {
                pagelibre.setLangue((String) infoBean.get("LANGUE"));
            }
        } else {
            pagelibre.setIdPagelibre(Long.valueOf(infoBean.getString("ID_PAGELIBRE")));
            pagelibre.retrieve();
        }
        if (action.indexOf("SUPPRIMER_PARAGRAPHE") == 0) {
            // Suppression de l'élément courant
            final int indiceDiese = action.indexOf("#");
            final int indice = Integer.parseInt(action.substring(indiceDiese + 1));
            final int nbItems = Integer.parseInt(infoBean.getString("PARAGRAPHES_NB_ITEMS"));
            for (int j = indice; j < nbItems - 1; j++) {
                infoBean.set("CONTENU_" + j, infoBean.getString("CONTENU_" + (j + 1)));
                infoBean.setInt("LIGNE_" + j, infoBean.getInt("LIGNE_" + (j + 1)));
                infoBean.setInt("COLONNE_" + j, infoBean.getInt("COLONNE_" + (j + 1)));
                infoBean.setInt("LARGEUR_" + j, infoBean.getInt("LARGEUR_" + (j + 1)));
            }
            infoBean.set("PARAGRAPHES_NB_ITEMS", "" + (nbItems - 1));
            ecranLogique = ECRAN_PRINCIPAL;
        } else if ("AJOUTER_PARAGRAPHE".equals(action)) {
            final int nbItems = Integer.parseInt(infoBean.getString("PARAGRAPHES_NB_ITEMS"));
            int positionLigne = 0;
            for (int i = 0; i < nbItems; i++) {
                final int positionParagraphe = infoBean.getInt("LIGNE_" + i);
                if (positionLigne <= positionParagraphe) {
                    positionLigne = positionParagraphe + 1;
                }
            }
            infoBean.set("CONTENU_" + nbItems, "");
            infoBean.setInt("LIGNE_" + nbItems, nbItems + 1);
            infoBean.setInt("COLONNE_" + nbItems, 1);
            infoBean.setInt("LARGEUR_" + nbItems, 100);
            infoBean.set("PARAGRAPHES_NB_ITEMS", "" + (nbItems + 1));
            infoBean.set("FOCUS", String.valueOf(nbItems));
            ecranLogique = ECRAN_PRINCIPAL;
        } else {
            // changement d'onglet
            if (InfoBean.ACTION_ONGLET.equals(action)) {
                infoBean.set("SOUS_ONGLET", infoBean.getString("SOUS_ONGLET_DEMANDE"));
            }
            // validation de l'ecran
            else if (InfoBean.ACTION_ENREGISTRER.equals(action)) {
                alimenteDonneesCreation(pagelibre, true);
                pagelibre.setTitre((String) infoBean.get("TITRE"));
                pagelibre.setRattachementBandeau((String) infoBean.get("RATTACHEMENT_BANDEAU"));
                // Stockage des paragraphes dans un vecteur
                final int nbItems = Integer.parseInt(infoBean.getString("PARAGRAPHES_NB_ITEMS"));
                final ArrayList<ParagrapheBean> listeParagraphes = new ArrayList<>();
                ParagrapheBean paragraphe = null;
                for (int i = 0; i < nbItems; i++) {
                    paragraphe = new ParagrapheBean();
                    if (infoBean.get("LIGNE_" + i) != null) {
                        paragraphe.setLigne(infoBean.getInt("LIGNE_" + i));
                    } else {
                        paragraphe.setLigne(i + 1);
                    }
                    if (infoBean.get("COLONNE_" + i) != null) {
                        paragraphe.setColonne(infoBean.getInt("COLONNE_" + i));
                    } else {
                        paragraphe.setColonne(1);
                    }
                    if (infoBean.get("LARGEUR_" + i) != null) {
                        paragraphe.setLargeur(infoBean.getInt("LARGEUR_" + i));
                    } else {
                        paragraphe.setLargeur(100);
                    }
                    paragraphe.setContenu(infoBean.getString("CONTENU_" + i));
                    listeParagraphes.add(paragraphe);
                }
                pagelibre.setParagraphes(listeParagraphes);
                pagelibre.setComplements((String) infoBean.get("COMPLEMENTS"));
                if (!"0000".equals(infoBean.getString("CODE_RUBRIQUE"))) {
                    pagelibre.setCodeRubrique(infoBean.getString("CODE_RUBRIQUE"));
                } else {
                    pagelibre.setCodeRubrique("");
                }
                pagelibre.setCodeRattachement(infoBean.getString("CODE_RATTACHEMENT"));
            }
            // appel au traitement general
            ecranLogique = ControleurUniv.traiterPRINCIPAL(infoBean, pagelibre, this);
            if (ecranLogique.length() == 0) {
                etat = FIN;
            }
        }
    }
}
