package com.univ.objetspartages.processus;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.core.MyRequestWrapper;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.identification.GestionnaireIdentification;
import com.jsbsoft.jtf.identification.SourceAuthClearTrust;
import com.jsbsoft.jtf.identification.SourceAuthHelper;
import com.jsbsoft.jtf.identification.ValidateurCAS;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.security.MySQLHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AnnuaireModele;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;
import com.univ.objetspartages.services.ServiceLabel;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.MailUtil;
import com.univ.utils.SessionUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;

/**
 * Processus prenant en charge l'identification.
 */
public class Identification extends ProcessusBean {

    private static final String PROC_IDENTIFICATION_FRONT = "IDENTIFICATION_FRONT";

    /**
     * The Constant ECRAN_PRINCIPAL.
     */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    /**
     * The Constant ECRAN_CONFIRMATION.
     */
    private static final String ECRAN_CONFIRMATION = "CONFIRMATION";

    /**
     * The Constant ECRAN_DEMANDE_MDP.
     */
    private static final String ECRAN_DEMANDE_MDP = "DEMANDE_MDP";

    /**
     * The Constant ECRAN_PRESENTATION_MDP.
     */
    private static final String ECRAN_PRESENTATION_MDP = "PRESENTATION_MDP";

    /**
     * The Constant ECRAN_CONFIRMATION_MDP.
     */
    private static final String ECRAN_CONFIRMATION_MDP = "CONFIRMATION_MDP";

    /**
     * The Constant ECRAN_PERSONNALISATION.
     */
    private static final String ECRAN_PERSONNALISATION = "PERSONNALISATION";

    /**
     * The Constant ECRAN_COOKIE.
     */
    private static final String ECRAN_COOKIE = "COOKIE";

    /**
     * The Constant ECRAN_DECONNEXION.
     */
    private static final String ECRAN_DECONNEXION = "DECONNEXION";

    /**
     * The Constant ECRAN_MENU.
     */
    private static final String ECRAN_MENU = "MENU";

    /**
     * The Constant ECRAN_REDIRECTION.
     */
    private static final String ECRAN_REDIRECTION = "REDIRECTION";

    private static final Logger LOG = LoggerFactory.getLogger(Identification.class);

    private final ServiceUser serviceUser;

    private final ServiceLabel serviceLabel;

    private final ServiceMetatag serviceMetatag;

    /**
     * The g i.
     */
    private final GestionnaireIdentification gI;

    /**
     * processus saisie Session.
     *
     * @param ciu com.jsbsoft.jtf.core.InfoBean
     */
    public Identification(final InfoBean ciu) {
        super(ciu);
        gI = GestionnaireIdentification.getInstance();
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
        serviceLabel = ServiceManager.getServiceForBean(LabelBean.class);
        serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     * @throws Exception the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        try {
            ecranLogique = infoBean.getEcranLogique();
            action = infoBean.getActionUtilisateur();
            // recuperation de la hashtable pour utilisation ci-dessous
            final Map<String, Object> mapInfosSessionsUser = getGp().getSessionUtilisateur().getInfos();
            if (ecranLogique == null) {
                if ("DECONNECTER".equals(action)) {
                    gI.deconnecte((String) mapInfosSessionsUser.get(SessionUtilisateur.KSESSION), mapInfosSessionsUser, this);
                    // PATCH Double identification (CAS + KDB)
                    // PCO 23/04/2009
                    // On écrase le cookie afin de dire que la personne n'est
                    // plus connecté en temps que KUSER.
                    // Il faudra que l'utilisateur fasse un choix de mode de
                    // connexion à nouveau (le mode par defaut est CAS)
                    this.setKUserConnectorCookieValue("no");
                    final ContexteUniv ctx = ContexteUtil.getContexteUniv();
                    if (ctx != null) {
                        ctx.supprimerInfosUtilisateurs();
                    } else {
                        LOG.error("impossible de recuperer le contexte");
                    }
                    getGp().getSessionUtilisateur().getHttpSession().invalidate();
                    final String redirectionLogin = PropertyHelper.getCoreProperty("sso.redirection_login");
                    // redirection après déconnexion sauf si on vient d'une deconnexion propagée en back-office (cf AdminsiteUtils.getLiensPropagationDeconnexionFormatter)
                    final boolean redirection = !"false".equalsIgnoreCase(infoBean.getString("REDIRECT"));
                    if (redirection) {
                        // redirection specifique activee
                        if ("1".equals(redirectionLogin)) {
                            setEcranRedirection(true);
                        }
                        // redirection vers la mire de login par defaut
                        else {
                            setEcranLogin();
                        }
                    }
                    // retour vers un écran vide
                    else {
                        ecranLogique = ECRAN_CONFIRMATION;
                    }
                } else if ("CONNECTER".equals(action)) {
                    // PATCH Double identification (CAS + KDB)
                    // PCO 23/04/2009
                    // Permet d'éviter de repasser par la session de connexion
                    // quand on est déjà connecté.
                    final String codeUtilisateur = (String) mapInfosSessionsUser.get(SessionUtilisateur.CODE);
                    if (StringUtils.isEmpty(codeUtilisateur)) {
                        setEcranLogin();
                    } else {
                        setEcranRedirection(false);
                    }
                } else if ("REVENIR_ACCUEIL".equals(action)) {
                    initialiserACCUEIL();
                } else if ("PERSONNALISER".equals(action)) {
                    initialiserPERSONNALISATION();
                } else if ("DEMANDER_MDP".equals(action)) {
                    initialiserDEMANDE_MDP();
                } else if ("PRESENTER_MDP".equals(action)) {
                    traiterGENERATION_MDP();
                } else if ("VALIDER".equals(action)) {
                    if ("1".equals(infoBean.get(SourceAuthHelper.KUSER_CONNECTOR))) {
                        // possibilité de s'authentifier explicitement sur la bse de donnée, pour les batches (ONISEP)
                        final UtilisateurBean utilisateur = gI.connecteUserKbd(infoBean.getString("LOGIN"), infoBean.getString("PASSWORD"), this, getGp().getSessionUtilisateur().getInfos(), infoBean.getString("#user-agent"));
                        if (utilisateur == null) {
                            infoBean.addMessageErreur(MessageHelper.getCoreMessage(getLocale(), "ST_LOGIN_PASSWORD_INCORRECT"));
                            ecranLogique = ECRAN_PRINCIPAL;
                        } else {
                            setEcranRedirection(false);
                        }
                    } else {
                        final boolean bIdentificationFront = infoBean.getNomProcessus().equals(PROC_IDENTIFICATION_FRONT);
                        // CAS
                        if (gI.estSourceAuth(ValidateurCAS.SOURCE_LIBELLE_CAS)) {
                            LOG.debug("Connexion CAS");
                            final UtilisateurBean utilisateur = gI.getValidateurCAS().validation(this, infoBean);
                            if (utilisateur != null) {
                                gI.chargeInfoUser(utilisateur, this, getGp().getSessionUtilisateur().getInfos(), infoBean.getString("#user-agent"), bIdentificationFront);
                                setEcranRedirection(false);
                            } else {
                                setEcranLogin();
                            }
                            // CTrust
                        } else if (gI.estSourceAuth(SourceAuthClearTrust.SOURCE_LIBELLE_CT)) {
                            LOG.debug("Connexion Ctrust");
                            final HttpServletRequest request = (HttpServletRequest) getDatas().get(OMContext.CLE_SERVLET_SG_REQUETE_HTTP);
                            final String loginCtrust = request.getHeader(SourceAuthClearTrust.PARAM_LOGIN_HTTP_HEADER);
                            if (loginCtrust != null && StringUtils.isEmpty((String) mapInfosSessionsUser.get(SessionUtilisateur.CODE))) {
                                if (gI.chargeInfoUser(loginCtrust, this, request.getSession(Boolean.FALSE), request.getHeader("user-agent"))) {
                                    setEcranLogin();
                                } else {
                                    throw new ErreurApplicative(MessageFormat.format(MessageHelper.getCoreMessage("ST_DSI_IDENTIFICATION_ECHEC"), loginCtrust));
                                }
                            } else {
                                infoBean.setEcranRedirection(URLResolver.getAbsoluteUrl("/", ContexteUtil.getContexteUniv()));
                                ecranLogique = ECRAN_REDIRECTION;
                            }
                        }
                    }
                } else if ("CREER_COOKIE".equals(action)) {
                    final String remoteAddr = ((HttpServletRequest) getDatas().get(OMContext.CLE_SERVLET_SG_REQUETE_HTTP)).getRemoteAddr();
                    if (GestionnaireIdentification.isAuthorizedAddr(remoteAddr)) {
                        final String user = StringUtils.defaultString(infoBean.getString("USER"), PropertyHelper.getCoreProperty("identification.user"));
                        final UtilisateurBean userBatch = serviceUser.getByCode(user);
                        gI.chargeInfoUser(userBatch, this, getGp().getSessionUtilisateur().getInfos(), infoBean.getString("#user-agent"), true);
                    }
                    ecranLogique = ECRAN_COOKIE;
                }
            } else {
                if (ECRAN_PRINCIPAL.equals(ecranLogique)) {
                    traiterPRINCIPAL();
                } else if (ECRAN_PERSONNALISATION.equals(ecranLogique)) {
                    traiterPERSONNALISATION();
                } else if (ECRAN_DEMANDE_MDP.equals(ecranLogique)) {
                    traiterDEMANDE_MDP();
                }
            }
            infoBean.setEcranLogique(ecranLogique);
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
            infoBean.addMessageErreur(e.toString());
        }
        return etat == FIN;
    }

    private void setEcranRedirection(final boolean ecranDeconnexion) {
        final boolean bIdentificationFront = infoBean.getNomProcessus().equals(PROC_IDENTIFICATION_FRONT);
        if (!bIdentificationFront) {
            if (ecranDeconnexion) {
                ecranLogique = ECRAN_DECONNEXION;
            } else {
                ecranLogique = ECRAN_MENU;
            }
            return;
        }
        ecranLogique = ECRAN_REDIRECTION;
        final String ksession = (String) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.KSESSION);
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        String action = StringUtils.defaultString(infoBean.getActionUtilisateur());
        // creation d'un cookie sso en fin d'authentification, pour déclencher la propagation de sessions sur chaque site en front
        if ("VALIDER".equals(action) && !ecranDeconnexion) {
            final Cookie cookie = new Cookie("sso", "true");
            cookie.setPath("/");
            ctx.getReponseHTTP().addCookie(cookie);
        }
        final String redirection = PropertyHelper.getCoreProperty("sso.redirection_login");
        //redirection via la jsp index_redirect, pour les redirections vers des sites externes. Ceci après login dsi
        if ("1".equals(redirection) && !"REVENIR_ACCUEIL".equals(action) && !"RETOUR_PARENT".equals(action)) {
            final String urlRedirect = StringUtils.defaultString((String) infoBean.get("URL_REDIRECT"));
            LOG.debug("Identification set redirection : " + urlRedirect);
            if (urlRedirect.length() > 0) {
                infoBean.setEcranRedirection(urlRedirect);
                return;
            }
        }
        // redirection sur une url
        String urlDemandee = "";
        if (ctx.getRequeteHTTP().getSession(false) != null) {
            urlDemandee = UnivWebFmt.renvoyerUrlRedirection(ctx.getRequeteHTTP().getSession(false));
        }
        if (StringUtils.isNotEmpty(urlDemandee)) {
            try {
                urlDemandee = URLDecoder.decode(urlDemandee, "UTF-8");
            } catch (final UnsupportedEncodingException e) {
                LOG.info("unable to decode the given url", e);
            }
            // on ajoute ksession=[ksession] à la fin de l'url
            if (StringUtils.isNotEmpty(ksession)) {
                if (urlDemandee.indexOf('?') == -1) {
                    urlDemandee += "?";
                } else {
                    urlDemandee += "&";
                }
                urlDemandee += "ksession=" + ksession;
            }
            LOG.debug("Redirection url demandee : " + urlDemandee);
            infoBean.setEcranRedirection(URLResolver.getAbsoluteUrl(urlDemandee, ctx));
        } else {
            // redirection page d'accueil DSI après login ou deconnexion
            String urlRedirect = URLResolver.getAbsoluteUrl("/", ctx);
            try {
                final Map<String, Object> infosSession = SessionUtil.getInfosSession(ctx.getRequeteHTTP());
                final RubriqueBean accueil = (RubriqueBean) infosSession.get(SessionUtilisateur.RUBRIQUE_ACCUEIL);
                if (accueil != null) {
                    urlRedirect = URLResolver.getAbsoluteUrl(ctx.getReponseHTTP().encodeRedirectURL(UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, accueil.getCode(), true, accueil.getLangue())), ctx);
                } else {
                    // FIXME : Possible portion inutile -> à voir plus en détail si encore utilisée
                    final String codeFiche = (String) infosSession.get(SessionUtilisateur.CODE_PAGE_ACCUEIL);
                    final String langueFiche = (String) infosSession.get(SessionUtilisateur.LANGUE_PAGE_ACCUEIL);
                    if (StringUtils.isNotEmpty(codeFiche)) {
                        urlRedirect = URLResolver.getAbsoluteUrl(ctx.getReponseHTTP().encodeRedirectURL(UnivWebFmt.determinerUrlFiche(ctx, "pagelibre", codeFiche, langueFiche, false)), ctx);
                    }
                }
            } catch (final Exception e) {
                LOG.debug("an error occured processing the url", e);
            }
            if (StringUtils.isNotEmpty(ksession)) {
                if (urlRedirect.indexOf('?') == -1) {
                    urlRedirect += "?";
                } else {
                    urlRedirect += "&";
                }
                urlRedirect += "ksession=" + ksession;
            }
            infoBean.setEcranRedirection(urlRedirect);
        }
    }

    /**
     * Gestion de l'écran de saisie d'un Session.
     * <p>
     * Gere la connexion vers la source selectionnee dans le jtf.
     *
     * @throws Exception the exception
     */
    private void traiterPRINCIPAL() throws Exception {
        final UtilisateurBean utilisateur;
        if ("1".equals(infoBean.get(SourceAuthHelper.KUSER_CONNECTOR))) {
            // PATCH Double identification (CAS + KDB)
            // PCO 23/04/2009
            // Permet de forcer le mode de connexion à celui de la BD K-Portal
            // quand le paramétre KUSER_CONNECTOR est à 1
            utilisateur = gI.connecteUserKbd(infoBean.getString("LOGIN"), infoBean.getString("PASSWORD"), this, getGp().getSessionUtilisateur().getInfos(), infoBean.getString("#user-agent"));
        } else {
            utilisateur = gI.connecte(infoBean.getString("LOGIN"), infoBean.getString("PASSWORD"), this, getGp().getSessionUtilisateur().getInfos(), infoBean.getString("#user-agent"));
        }
        if (utilisateur == null) {
            infoBean.addMessageErreur(MessageHelper.getCoreMessage(getLocale(), "ST_LOGIN_PASSWORD_INCORRECT"));
            ecranLogique = ECRAN_PRINCIPAL;
        } else {
            // PATCH Double identification (CAS + KDB)
            // PCO 23/04/2009
            // Ajouter le cookie qui permettra lorsque la session sera invalidé
            // de connaitre le mode de connexion de l'utilisateur.
            // Lors de la déconexion ce cookie est passé à NO, cela permettra à
            // l'utilisateur le choix de connexion.
            if ("1".equals(infoBean.get("KUSER_CONNECTOR"))) {
                this.setKUserConnectorCookieValue("ok");
            } else {
                this.setKUserConnectorCookieValue("no");
            }
            setEcranRedirection(false);
        }
    }

    /**
     * Affichage de l'écran de saisie d'un Session.
     */
    private void initialiserPRINCIPAL() {
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.set("LOGIN", "");
        infoBean.set("PASSWORD", "");
    }

    /**
     * init du formulaire d'Affichage demande de nouveau de mot de passe.
     */
    private void initialiserDEMANDE_MDP() {
        ecranLogique = ECRAN_DEMANDE_MDP;
        infoBean.set("LOGIN", "");
        infoBean.set("EMAIL", "");
        infoBean.setTitreEcran(MessageHelper.getCoreMessage("ST_DSI_OUBLI_MDP_TITRE"));
    }

    /**
     * Affichage de l'écran de saisie d'un Session.
     *
     * @throws Exception the exception
     */
    private void initialiserPERSONNALISATION() throws Exception {
        // JSS 20030317-002
        if (!SessionUtil.estConnecte(getGp().getSessionUtilisateur())) {
            setEcranLogin();
        } else {
            ecranLogique = ECRAN_PERSONNALISATION;
            // JB 20050906 : le login est en fait le code ldap si le mapping est appliqué
            if (!gI.getMappingLogin()) {
                infoBean.set("LOGIN", getGp().getSessionUtilisateur().getInfos().get("CODE"));
            } else {
                infoBean.set("LOGIN", getGp().getSessionUtilisateur().getInfos().get("CODE_GESTION"));
            }
            /* JSS 20030610-003 */
            final String synchronisationUtilisateurEtAnnuaire = StringUtils.defaultString(PropertyHelper.getCoreProperty("utilisateur.synchronisation_code_avec_annuaire"), "1");
            infoBean.set("SYNCHRONISATION", synchronisationUtilisateurEtAnnuaire);
            infoBean.set("PASSWORD_1", "");
            infoBean.set("PASSWORD_2", "");
            final UtilisateurBean utilisateur = serviceUser.getByCode((String) getGp().getSessionUtilisateur().getInfos().get("CODE"));
            if (utilisateur != null) {
                infoBean.set("NOM", utilisateur.getNom());
                infoBean.set("PRENOM", utilisateur.getPrenom());
                infoBean.set("ADRESSE_MAIL", utilisateur.getAdresseMail());
                String droitModificationPassword = StringUtils.defaultString(PropertyHelper.getCoreProperty("dsi.droits.modification_password"), "1");
                if (serviceUser.isAlterationForbiddenBySource(utilisateur)) {
                    droitModificationPassword = "0";
                }
                infoBean.set("DROIT_MODIFICATION_PASSWORD", droitModificationPassword);
                String droitModificationEmail = PropertyHelper.getCoreProperty("dsi.droits.modification_email");
                if (droitModificationEmail == null && !serviceUser.fromLdap(utilisateur)) {
                    droitModificationEmail = "1";
                } else {
                    droitModificationEmail = "0";
                }
                infoBean.set("DROIT_MODIFICATION_EMAIL", droitModificationEmail);
                // gestion de la modification de la structure en front
                initialiserPERSONNALISATIONStructure(utilisateur.getCodeRattachement());
                //AM 20051026 : format d'envoi des newsletters
                infoBean.set("FORMAT_ENVOI", utilisateur.getFormatEnvoi());
                infoBean.set("RADIO0", "HTML");
                infoBean.set("RADIO1", "Texte");
                /* JSS 20050510 : ajout profil / défaut */
                /* On propose le choix à l'utilisateur de modifier son profil / défaut
                 * uniquement si il en a +eurs
                 */
                infoBean.set("MODIFICATION_PROFIL_DEFAUT", "0");
                final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
                final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
                final Vector<String> groupesDsi = new Vector<>(serviceGroupeUtilisateur.getAllGroupsCodesByUserCode(utilisateur.getCode()));
                final Map<String, String> listeProfils = serviceProfildsi.getListeProfilsDSIParGroupes(groupesDsi);
                infoBean.set("PROFIL_DEFAUT", utilisateur.getProfilDefaut());
                infoBean.set("LISTE_PROFILS", listeProfils);
                if (listeProfils.size() > 1) {
                    infoBean.set("MODIFICATION_PROFIL_DEFAUT", "1");
                }
                /* ********************* */
                /*      Thématiques      */
                /* ********************* */
                final Collection<String> vThemesUtilisateurs = serviceUser.getCentresInteret(utilisateur);
                int iTheme = 0;
                /* tri des thèmes par libellés */
                final List<LabelBean> labels = serviceLabel.getByTypeLanguage("04", LangueUtil.getLangueLocale(this.getLocale()));
                for (final LabelBean currentLabel : labels) {
                    /* Pour chaque theme, on regarde s'il est sélectionné */
                    String valeurTheme = "0";
                    if (vThemesUtilisateurs.contains(currentLabel.getCode())) {
                        valeurTheme = "1";
                    }
                    infoBean.set("CODE_THEME#" + iTheme, currentLabel.getCode());
                    infoBean.set("LIBELLE_THEME#" + iTheme, currentLabel.getLibelle());
                    infoBean.set("VALEUR_THEME#" + iTheme, valeurTheme);
                    iTheme++;
                }
                infoBean.set("THEMES_NB_ITEMS", iTheme);
            }
        }
    }

    private void setEcranLogin() {
        final HttpServletRequest request = (HttpServletRequest) getDatas().get(OMContext.CLE_SERVLET_SG_REQUETE_HTTP);
        final boolean kUserConnector = SourceAuthHelper.isDoubleSourceAuthActif(request, infoBean);
        if (!kUserConnector && gI.estSourceAuth(ValidateurCAS.SOURCE_LIBELLE_CAS)) {
            final boolean isFront = infoBean.getNomProcessus().equals(PROC_IDENTIFICATION_FRONT);
            if (isFront) {
                infoBean.setEcranRedirection(URLResolver.getAbsoluteUrl(gI.getValidateurCAS().getUrlCasLoginFront(this), ContexteUtil.getContexteUniv()));
            } else {
                infoBean.setEcranRedirection(URLResolver.getAbsoluteUrl(gI.getValidateurCAS().getUrlCasLogin(this), ContexteUtil.getContexteUniv()));
            }
            ecranLogique = ECRAN_REDIRECTION;
        } else if (!kUserConnector && gI.estSourceAuth(SourceAuthClearTrust.SOURCE_LIBELLE_CT)) {
            infoBean.setEcranRedirection(URLResolver.getAbsoluteUrl(WebAppUtil.SG_PATH + "?PROC=" + PROC_IDENTIFICATION_FRONT + "&ACTION=VALIDER", ContexteUtil.getContexteUniv()));
            ecranLogique = ECRAN_REDIRECTION;
        } else {
            initialiserPRINCIPAL();
        }
    }

    /**
     * formulaire pour la demande d'un nouveau mot de passe.
     *
     * @throws Exception the exception
     */
    private void traiterDEMANDE_MDP() throws Exception {
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            final String message = "";
            serviceUser.requestNewPass(infoBean);
            this.traiterCONFIRMATION_MDP(message);
        }
    }

    /**
     * page qui confirme la demande de mot de passe et annonce l'envoir de l'email a l'internaute.
     *
     * @param message the message
     */
    private void traiterCONFIRMATION_MDP(final String message) {
        infoBean.set("MESSAGE", message);
        ecranLogique = ECRAN_CONFIRMATION_MDP;
    }

    /**
     * page qui presente le mot de passe a l'internaute.
     *
     * @throws Exception the exception
     */
    private void traiterGENERATION_MDP() throws Exception {
        if (StringUtils.isNotEmpty(infoBean.getString("ID"))) {
            final String id = infoBean.getString("ID");
            ecranLogique = ECRAN_PRESENTATION_MDP;
            final Map<String, String> presentation = serviceUser.handlePassRequest(id);
            if (MapUtils.isNotEmpty(presentation)) {
                infoBean.set("LOGIN", presentation.get("code"));
                infoBean.set("NOM", presentation.get("nom"));
                infoBean.set("PRENOM", presentation.get("prenom"));
                infoBean.set("MDP", presentation.get("motDePasse"));
            } else {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(getLocale(), "ST_ERR_DEMANDE_MDP_DEMANDE_INVALIDE"));
            }
        } else {
            throw new ErreurApplicative(MessageHelper.getCoreMessage(getLocale(), "ST_ERR_DEMANDE_MDP_DEMANDE_INVALIDE"));
        }
    }

    /**
     * Gestion de l'écran de saisie d'un Session.
     *
     * @throws Exception the exception
     */
    private void traiterPERSONNALISATION() throws Exception {
        if (!SessionUtil.estConnecte(getGp().getSessionUtilisateur())) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage(getLocale(), "ST_ACCES_RESERVE"));
        }
        if (InfoBean.ACTION_VALIDER.equals(action)) {
            final String codeUtilisateur = (String) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE);
            final String droitModificationPassword = PropertyHelper.getCoreProperty("dsi.droits.modification_password");
            if (droitModificationPassword == null || "1".equals(droitModificationPassword)) {
                String password = infoBean.getString("PASSWORD_1");
                if (StringUtils.isNotBlank(password)) {
                    password = password.trim();
                    serviceUser.checkPass(password);
                    if (!password.equalsIgnoreCase(infoBean.getString("PASSWORD_2"))) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage(getLocale(), "AUTHENTIFICATION.ERREUR.MOT_PASSE_CONFIRM_INCORRECT"));
                    }
                }
            }
            // récupération de l'utilisateur
            final UtilisateurBean utilisateur = serviceUser.getByCode(codeUtilisateur);
            if (utilisateur != null) {
                // mot de passe et adresse mail
                if (StringUtils.isNotBlank(infoBean.getString("PASSWORD_1"))) {
                    utilisateur.setMotDePasse(MySQLHelper.encodePassword(infoBean.getString("PASSWORD_1")));
                }
                String email = utilisateur.getAdresseMail();
                final String droitModificationEmail = PropertyHelper.getCoreProperty("dsi.droits.modification_email");
                if (droitModificationEmail == null && !serviceUser.fromLdap(utilisateur)) {
                    email = infoBean.getString("ADRESSE_MAIL");
                    if (!MailUtil.verifMail(email)) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage(getLocale(), "ST_ERR_DEMANDE_MDP_EMAIL"));
                    }
                    utilisateur.setAdresseMail(email);

                }
                // gestion des structures
                final String codeStructureRattachement = determinerCodeRattachement();
                if (codeStructureRattachement != null) {
                    utilisateur.setCodeRattachement(codeStructureRattachement);
                }
                // format d'envoi newsletter
                if (infoBean.getString("FORMAT_ENVOI") != null) {
                    utilisateur.setFormatEnvoi(infoBean.getString("FORMAT_ENVOI"));
                }
                // profil par défaut
                if (infoBean.getString("PROFIL_DEFAUT") != null) {
                    utilisateur.setProfilDefaut(infoBean.getString("PROFIL_DEFAUT"));
                }
                // centres d'intérêt
                utilisateur.setCentresInteret(retrieveCentreInteret());
                //mise à jour de l'utilisateur
                serviceUser.save(utilisateur);
                // mise à jour de la session
                getGp().getSessionUtilisateur().getInfos().put("CENTRES_INTERET", serviceUser.getCentresInteret(utilisateur));
                // report de l'email et de la structure sur la fiche annuaire de l'utilisateur
                final String synchronisation = PropertyHelper.getCoreProperty("utilisateur.synchronisation_code_avec_annuaire");
                if (synchronisation == null || "1".equals(synchronisation)) {
                    synchFicheAnnuaire(codeUtilisateur, email, codeStructureRattachement);
                }
            }
            setEcranRedirection(false);
        }
    }

    private String retrieveCentreInteret() {
        final Collection<String> interets = new ArrayList<>();
        final String centreInterets = infoBean.getString("THEMES_NB_ITEMS");
        if (StringUtils.isNotBlank(centreInterets) && StringUtils.isNumeric(centreInterets)) {
            int nbItems = 0;
            try {
                nbItems = Integer.parseInt(centreInterets);
            } catch (final NumberFormatException nfe) {
                LOG.error("unable to parse the field THEMES_NB_ITEMS", nfe);
            }
            for (int j = 0; j < nbItems; j++) {
                if ("1".equals(infoBean.getString("VALEUR_THEME#" + j))) {
                    interets.add(infoBean.getString("CODE_THEME#" + j));
                }
            }
        }
        return StringUtils.join(interets, ";");
    }
    /**
     * Synchroniser l'email et le code de rattachement de l'utilisateur à sa fiche annuaire.
     *
     * @param codeUtilisateur  le code utilisateur dont on souhaite synchroniser la fiche
     * @param email            le nouvel email
     * @param codeRattachement le nouveau code de rattachement structure
     * @throws Exception lors des requetes en BDD
     */
    private void synchFicheAnnuaire(final String codeUtilisateur, final String email, final String codeRattachement) throws Exception {
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
            if (ficheUniv instanceof AnnuaireModele) {
                ficheUniv.setCtx(this);
                ficheUniv.init();
                final int count = ficheUniv.selectCodeLangueEtat(codeUtilisateur, "", "");
                if (count > 0) {
                    while (ficheUniv.nextItem()) {
                        if (ficheUniv.getCodeRedacteur().equals(codeUtilisateur)) {
                            ((AnnuaireModele) ficheUniv).setAdresseMail(email);
                            ficheUniv.setCodeRattachement(codeRattachement);
                            ficheUniv.update();
                            final MetatagBean meta = MetatagUtils.lireMeta(ficheUniv);
                            serviceMetatag.synchroniser(meta, ficheUniv, false);
                            serviceMetatag.save(meta);
                        }
                    }
                }
            }
        }
    }

    /**
     * Affichage de l'écran d'accueil.
     */
    private void initialiserACCUEIL() {
        // gestion profil dynamique
        if (StringUtils.isNotBlank(infoBean.get("PROFIL", String.class))) {
            final Map<String, Object> hashInfosUser = getGp().getSessionUtilisateur().getInfos();
            hashInfosUser.put("PROFIL_DSI", infoBean.getString("PROFIL"));
            final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
            final ProfildsiBean profile = serviceProfildsi.getByCode(infoBean.getString("PROFIL"));
            if(profile != null) {
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                hashInfosUser.put(SessionUtilisateur.RUBRIQUE_ACCUEIL, serviceRubrique.getRubriqueByCode(profile.getCodeRubriqueAccueil()));
            }
        }
        setEcranRedirection(false);
    }

    /**
     * Renseigner les données utiles pour la personnalisation des préférences utilisateur.
     *
     * @param codeRattachement Le code de la structure à mettre dans l'infoBean.
     * @throws Exception Erreur pour récupérer le libelle affichable de la structure correspondans au code passé en poramétre.
     */
    protected void initialiserPERSONNALISATIONStructure(final String codeRattachement) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        infoBean.set("CODE_STRUCTURE", codeRattachement);
        infoBean.set("LIBELLE_CODE_STRUCTURE", serviceStructure.getDisplayableLabel(codeRattachement, ""));
    }

    /**
     * Détermine la structure de rattachement de l'utilisateur.
     *
     * @return la structure de rattachement de l'utilisateur
     */
    protected String determinerCodeRattachement() {
        return infoBean.getString("CODE_RATTACHEMENT");
    }

    /**
     * Méthode utilisée dans la gestion de la double authentification
     *
     * @param value
     */
    private void setKUserConnectorCookieValue(final String value) {
        final MyRequestWrapper myRequest = (MyRequestWrapper) this.getDatas().get(OMContext.CLE_SERVLET_SG_REQUETE_HTTP);
        final HttpServletResponse response = myRequest.getResponse();
        myRequest.getHttpServletRequest();
        final Cookie cookie = new Cookie(SourceAuthHelper.KUSER_CONNECTOR, value);
        cookie.setPath("/");
        response.addCookie(cookie);
    }
}
