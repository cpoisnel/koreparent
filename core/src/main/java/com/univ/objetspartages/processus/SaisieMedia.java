package com.univ.objetspartages.processus;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.core.ProcessusHelper;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.util.compress.Zip;
import com.univ.mediatheque.Mediatheque;
import com.univ.mediatheque.SpecificUrl;
import com.univ.mediatheque.UsingLibelleMedia;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.SpecificMedia;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.FileUtil;
import com.univ.utils.PhotoUtil;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseLimit;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * Le processus de saisie de média. Il est appelé dans la toolbox, les champs de médias (simple ou mutli)...
 * L'ihm du back pour saisir directement dans la toolbox etc.
 */
public class SaisieMedia extends ProcessusBean {

    /** The Constant MODE_SELECTION désigne le mode pour l'ajout dans un champ de type fichier. */
    public static final String MODE_SELECTION = "SELECTION";

    /** The Constant MODE_INSERTION désigne le mode pour l'insertion dans une toolbox. */
    public static final String MODE_INSERTION = "INSERTION";

    /** The Constant MODE_ADMINISTRATION désigne le mode pour la gestion indépendante de la médiathèque. */
    public static final String MODE_ADMINISTRATION = "ADMINISTRATION";

    /** Logger available to subclasses. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SaisieMedia.class);

    /** The Constant ETAT_CREATION. */
    private static final int ETAT_CREATION = 0;

    /** The Constant ETAT_MODIFICATION. */
    private static final int ETAT_MODIFICATION = 1;

    /** The Constant ID_ONGLET_RESSOURCE. */
    private static final String ID_ONGLET_RESSOURCE = "0";

    /** The Constant ID_ONGLET_RESSOURCE. */
    private static final String ID_ONGLET_RESSOURCE_SELECTION = "0";

    /** The Constant ECRAN_RECHERCHE. */
    private static final String ECRAN_RECHERCHE = "RECHERCHE";

    /** The Constant ECRAN_SELECTION. */
    private static final String ECRAN_SELECTION = "SELECTION";

    /** The Constant ECRAN_LISTE. */
    private static final String ECRAN_LISTE = "LISTE";

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_UPLOAD = "UPLOAD";

    /** The Constant ACTION_CONTROLER_MEDIA. */
    private static final String ACTION_CONTROLER_RESSOURCE = "CONTROLER_RESSOURCE";

    /** The Constant ACTION_SELECTIONNER_RESSOURCE. */
    private static final String ACTION_SELECTIONNER_RESSOURCE = "SELECTIONNER_RESSOURCE";

    /** The Constant ACTION_INSERER_RESSOURCE. */
    private static final String ACTION_INSERER_RESSOURCE = "INSERER_RESSOURCE";

    /** The Constant ECRAN_CONFIRMATION. */
    private static final String ECRAN_CONFIRMATION = "CONFIRMATION";

    /** The Constant ECRAN_APERCU. */
    private static final String ECRAN_APERCU = "APERCU";

    /** The Constant ECRAN_APERCU. */
    private static final String ECRAN_REFERENCES = "REFERENCES";

    public static final String PREVIEW_AVAILABLE = "PREVIEW_AVAILABLE";

    private final ServiceUser serviceUser;

    private final ServiceMedia serviceMedia;

    private final ServiceRessource serviceRessource;

    /** The etat. */
    private int etat = -1;

    /** The autorisations. */
    private AutorisationBean autorisations;

    /** The media. */
    private MediaBean media;

    private Mediatheque mediatheque;

    /**
     * Instantiates a new saisie media.
     *
     * @param infoBean
     *            the info bean
     */
    public SaisieMedia(final InfoBean infoBean) {
        super(infoBean);
        serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
        serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     */
    @Override
    public boolean traiterAction() {
        final boolean saisieFront = Boolean.parseBoolean(infoBean.getString("SAISIE_FRONT"));
        autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null && !saisieFront) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            try {
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                infoBean.remove("MESSAGE_CONFIRMATION");
                mediatheque = Mediatheque.getInstance();
                etat = EN_COURS;
                final Long idMedia = StringUtils.isNumeric(infoBean.get("ID_MEDIA", String.class)) ? Long.parseLong(infoBean.get("ID_MEDIA", String.class)) : 0L;
                media = serviceMedia.getById(idMedia);
                if (media == null) {
                    media = new MediaBean();
                }
                infoBean.set(PREVIEW_AVAILABLE, false);
                /* Entrée par lien hyper-texte */
                if (ecranLogique == null) {
                    if ("AJOUTER".equals(action)) {
                        etat = ETAT_CREATION;
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
                        infoBean.remove("CONTROLE_RESSOURCE"); 
                        preparerPRINCIPAL();
                    } else if ("MODIFIER".equals(action)) {
                        if (autorisations == null || (!autorisations.possedePermission(new PermissionBean("TECH", "pho", "M")) && !media.getCodeRedacteur().equals(autorisations.getCode()))) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
                        }
                        etat = ETAT_MODIFICATION;
                        media = serviceMedia.getById(Long.valueOf(infoBean.getString("ID_MEDIA")));
                        infoBean.setTitreEcran(StringUtils.isEmpty(media.getTitre()) ? media.getSource() : media.getTitre());
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                        infoBean.set("CONTROLE_RESSOURCE", "true");
                        infoBean.set(PREVIEW_AVAILABLE, true);
                        preparerReferences();
                        preparerPRINCIPAL();
                    } else if (ACTION_CONTROLER_RESSOURCE.equals(action)) {
                        infoBean.remove("CONTROLE_RESSOURCE");
                        controlerMedia();
                    }
                    // selection dans saisie fiche
                    else if ("SELECTIONNER".equals(action)) {
                        if (autorisations == null && !saisieFront) {
                            ecranLogique = "LOGIN";
                        } else {
                            infoBean.set("MODE", MODE_SELECTION);
                            preparerINSERTION();
                            preparerRECHERCHE();
                            ecranLogique = ECRAN_SELECTION;
                        }
                    }
                    // insertion dans une toolbox
                    else if ("INSERER".equals(action)) {
                        if (autorisations == null) {
                            ecranLogique = "LOGIN";
                        } else {
                            infoBean.set("MODE", MODE_INSERTION);
                            infoBean.set(PREVIEW_AVAILABLE, true);
                            preparerINSERTION();
                            preparerRECHERCHE();
                            ecranLogique = ECRAN_SELECTION;
                        }
                    } else if (ACTION_INSERER_RESSOURCE.equals(action)) {
                        infoBean.set("MODE", MODE_INSERTION);
                        insererMedia();
                    } else if ("RECHERCHER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
                        preparerRECHERCHE();
                    } else if ("IMPORTER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
                        preparerUPLOAD();
                    } else if ("REFERENCES".equals(action)) {
                        preparerReferences();
                    }
                    preparePreview(infoBean, media);
                } else {
                    /* Entrée par formulaire */
                    if (ACTION_CONTROLER_RESSOURCE.equals(action)) {
                        etat = ETAT_CREATION;
                        infoBean.remove("CONTROLE_RESSOURCE");
                        controlerMedia();
                        preparePreview(infoBean, media);
                    } else if ("AJOUTER".equals(action)) {
                        etat = ETAT_CREATION;
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
                        infoBean.remove("CONTROLE_RESSOURCE");
                        preparerPRINCIPAL();
                        preparePreview(infoBean, media);
                    } else if ("SUPPRIMER".equals(action) || ECRAN_PRINCIPAL.equals(ecranLogique)) {
                        traiterPRINCIPAL();
                    } else if ("RECHERCHER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
                        traiterRECHERCHE();
                    } else if (ACTION_SELECTIONNER_RESSOURCE.equals(action)) {
                        infoBean.set("MODE", MODE_SELECTION);
                        selectionnerMedia();
                        preparePreview(infoBean, media);
                    } else if ("APERCU".equals(action)) {
                        infoBean.set(PREVIEW_AVAILABLE, true);
                        preparerApercu();
                        preparePreview(infoBean, media);
                    } else if ("REFERENCES".equals(action)) {
                        preparerReferences();
                    } else if ("RECHERCHE".equals(action)) {
                        preparerRECHERCHE();
                    } else if (ACTION_INSERER_RESSOURCE.equals(action)) {
                        infoBean.set("MODE", MODE_INSERTION);
                        insererMedia();
                        preparePreview(infoBean, media);
                    } else if ("MODIFIER".equals(action)) {
                        if (autorisations == null || !autorisations.possedePermission(new PermissionBean("TECH", "pho", "M")) && !media.getCodeRedacteur().equals(autorisations.getCode())) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
                        }
                        etat = ETAT_MODIFICATION;
                        media = serviceMedia.getById(Long.valueOf(infoBean.getString("ID_MEDIA")));
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                        infoBean.set("CONTROLE_RESSOURCE", "true");
                        infoBean.set(PREVIEW_AVAILABLE, true);
                        preparerReferences();
                        preparerPRINCIPAL();
                        preparePreview(infoBean, media);
                    } else if (ECRAN_RECHERCHE.equals(ecranLogique) || ECRAN_LISTE.equals(ecranLogique)) {
                        traiterRECHERCHE();
                    } else if (ECRAN_UPLOAD.equals(ecranLogique)) {
                        traiterUPLOAD();
                    }
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final ErreurApplicative | IOException e) {
                LOGGER.error("An error occured during the process", e);
                infoBean.addMessageErreur(e.toString());
            }
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Affichage de l'écran de saisie d'un Media.
     *
     * @throws IOException
     *             the exception
     */
    private void preparerPRINCIPAL() throws IOException {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.set("SUPPRESSION_MEDIA", "0");
        infoBean.set("AJOUT_MEDIA", "0");
        if (autorisations != null) {
            Collection<Perimetre> vPerimetre = autorisations.getListePerimetres(new PermissionBean("TECH", "pho", "S"));
            if (CollectionUtils.isNotEmpty(vPerimetre) || media.getCodeRedacteur().equals(autorisations.getCode())) {
                infoBean.set("SUPPRESSION_MEDIA", "1");
            }
            vPerimetre = autorisations.getListePerimetres(new PermissionBean("TECH", "pho", "C"));
            if (CollectionUtils.isNotEmpty(vPerimetre)) {
                infoBean.set("AJOUT_MEDIA", "1");
            }
        } else {
            ecranLogique = "LOGIN";
            return;
        }
        if (media.getId() == null || media.getId() == 0L) {
            etat = ETAT_CREATION;
        } else {
            etat = ETAT_MODIFICATION;
        }
        infoBean.set("ID_MEDIA", media.getId().toString());
        infoBean.set("URL_RESSOURCE", MediaUtils.getUrlAbsolue(media));
        infoBean.set("URL_VIGNETTE", MediaUtils.getUrlVignetteAbsolue(media));
        // filtre des extensions sur la vignette
        String extensions = "";
        SpecificMedia specificMedia = mediatheque.getRessource("photo");
        for (final String ext : specificMedia.getExtensions()) {
            if (extensions.length() > 0) {
                extensions += ",";
            }
            extensions += ext;
        }
        infoBean.set(UploadedFile.KEY_FILE_EXTENSIONS + "_VIGNETTE", extensions);
        infoBean.set("SOURCE", media.getSource());
        infoBean.set("FORMAT", media.getFormat());
        infoBean.set("TITRE", media.getTitre());
        infoBean.set("LEGENDE", media.getLegende());
        infoBean.set("DESCRIPTION", media.getDescription());
        infoBean.set("AUTEUR", media.getAuteur());
        infoBean.set("COPYRIGHT", media.getCopyright());
        setTraductionInfoBean();
        Long fTaille = media.getPoids();
        if(fTaille != null) {
            infoBean.set("POIDS", FileUtils.byteCountToDisplaySize(fTaille));
        } else {
            infoBean.set("POIDS", "N/A");
        }
        if (infoBean.getString("TYPE_RESSOURCE") != null) {
            infoBean.set("TYPE_RESSOURCE_PREV", infoBean.getString("TYPE_RESSOURCE"));
        }
        infoBean.set("TYPE_RESSOURCE", media.getTypeRessource());
        if (etat == ETAT_CREATION) {
            // préparation des listes de types et ss type liés
            // boucle sur tous les types de média
            for (final String sKeyType : mediatheque.getTypesMedia().keySet()) {
                specificMedia = mediatheque.getRessource(sKeyType);
                infoBean.set("LISTE_TYPE_MEDIA_" + sKeyType.toUpperCase(), LabelUtils.getLabelCombo(specificMedia.getCodeTypeLibelle(), LangueUtil.getDefaultLocale()));
                infoBean.set("TEMPLATE_JSP_" + sKeyType.toUpperCase(), specificMedia.getJspSaisie());
                infoBean.set("TEMPLATE_JSP_APERCU_" + sKeyType.toUpperCase(), specificMedia.getJspApercu());
                infoBean.set("TEMPLATE_JSP_INSERTION_" + sKeyType.toUpperCase(), specificMedia.getJspInsertion());
            }
            infoBean.set("LISTE_TYPE_RESSOURCES", mediatheque.getTypesRessourcesAffichables());
            final String isLocal = infoBean.get("IS_LOCAL", String.class);
            if (StringUtils.isBlank(isLocal)) {
                infoBean.set("IS_LOCAL", "true");
            }
            if ("true".equals(infoBean.get("IS_LOCAL"))) {
                infoBean.remove("TYPE_RESSOURCE_PREV");
            }
        } else {
            specificMedia = mediatheque.getRessource(media.getTypeRessource().toLowerCase());
            infoBean.set("LIBELLE_TYPE_RESSOURCE", specificMedia.getLibelleAffichable());
            infoBean.set("TYPE_MEDIA_" + media.getTypeRessource().toUpperCase(), media.getTypeMedia());
            infoBean.set("LISTE_TYPE_MEDIA_" + media.getTypeRessource().toUpperCase(), LabelUtils.getLabelCombo(specificMedia.getCodeTypeLibelle(), LangueUtil.getDefaultLocale()));
            infoBean.set("TEMPLATE_JSP_" + media.getTypeRessource().toUpperCase(), specificMedia.getJspSaisie());
            infoBean.set("TEMPLATE_JSP_APERCU_" + media.getTypeRessource().toUpperCase(), specificMedia.getJspApercu());
            infoBean.set("TEMPLATE_JSP_INSERTION_" + media.getTypeRessource().toUpperCase(), specificMedia.getJspInsertion());
            specificMedia.prepareMedia(infoBean, media);
            final String isLocal = infoBean.get("IS_LOCAL", String.class);
            if (StringUtils.isBlank(isLocal)) {
                infoBean.set("IS_LOCAL", MediaUtils.isLocal(media) ? "true" : "false");
            }
            if (MediaUtils.isLocal(media)) {
                final String tag = "[id-image]" + media.getId() + "[/id-image]";
                // on stocke l'url du media en session pour ne pas controler les droits à la lecture cf LectureFichiergw
                final HttpSession session = infoBean.getSessionHttp();
                final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
                final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
                infosSession.put(MediaUtils.getUrlAbsolue(media), true);
                infoBean.set("URL_APERCU", "/servlet/com.univ.utils.LectureImageToolbox?TAG=" + tag);
            } else {
                final SpecificUrl specificUrl = mediatheque.getSpecificUrl(MediaUtils.getUrlAbsolue(media));
                if (specificUrl != null) {
                    infoBean.set("SPECIFIC_URL", "1");
                }
                infoBean.set("URL_APERCU", MediaUtils.getUrlAbsolue(media));
            }
        }
        infoBean.set("CODE_RATTACHEMENT", media.getCodeRattachement());
        infoBean.set("LIBELLE_CODE_RATTACHEMENT", serviceStructure.getDisplayableLabel(media.getCodeRattachement(), ""));
        infoBean.set("CODE_RUBRIQUE", media.getCodeRubrique());
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(media.getCodeRubrique());
        String rubriqueLabel = StringUtils.EMPTY;
        if (rubriqueBean != null) {
            rubriqueLabel = rubriqueBean.getIntitule();
        }
        infoBean.set("LIBELLE_CODE_RUBRIQUE", rubriqueLabel);
        infoBean.set("DATE_CREATION", media.getDateCreation());
        infoBean.set("THEMATIQUE", media.getThematique());
        infoBean.set("LIBELLE_THEMATIQUE", MediaUtils.getLibelleThematique(media));
        infoBean.set("LISTE_THEMATIQUES", LabelUtils.getLabelCombo("04", LangueUtil.getDefaultLocale()));
        infoBean.set("CODE_REDACTEUR", media.getCodeRedacteur());
        infoBean.set("LIBELLE_CODE_REDACTEUR", serviceUser.getLibelle(media.getCodeRedacteur()));
        infoBean.set("META_KEYWORDS", media.getMetaKeywords());
        if (etat == ETAT_CREATION) {
            //en création, on pré-renseigne l'auteur avec l'utilisateur courant
            final String codeRedacteur = (String) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE);
            infoBean.set("CODE_REDACTEUR", codeRedacteur);
            infoBean.set("LIBELLE_CODE_REDACTEUR", serviceUser.getLibelle(codeRedacteur));
        }
        String action = "C";
        if (etat == ETAT_MODIFICATION) {
            action = "M";
        }
        preparerPerimetre("pho", action);
        if (StringUtils.isNotBlank(MediaUtils.getLibelleAffichable(media))) {
            infoBean.setTitreEcran(MediaUtils.getLibelleAffichable(media));
        }
    }

    private void preparerPerimetre(final String permission, final String action) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        if (autorisations == null) {
            ecranLogique = "LOGIN";
            return;
        }
        // filtre les arbres de rubrique et structure en fonction du périmètre de l'utilisateur
        infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE", "CODE_RUBRIQUE");
        infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT", "CODE_RATTACHEMENT");
        // sauvegarde pour controle en cas de modification du périmètre par le rédacteur
        infoBean.set("GRS_SAUVEGARDE_CODE_RUBRIQUE", media.getCodeRubrique());
        infoBean.set("GRS_SAUVEGARDE_CODE_RATTACHEMENT", media.getCodeRattachement());
        infoBean.set("GRS_PERMISSION_TYPE", "TECH");
        infoBean.set("GRS_PERMISSION_OBJET", permission);
        infoBean.set("GRS_PERMISSION_ACTION", action);
        if ("pho".equals(permission) && "C".equals(action)) {
            // prévalorisation du périmètre en fonction de l'utilisateur
            boolean initProcessus = false;
            if (infoBean.get("CODE_RUBRIQUE") != null && infoBean.getString("CODE_RUBRIQUE").length() > 0) {
                initProcessus = true;
            }
            if (infoBean.getString("CODE_RATTACHEMENT") != null && infoBean.getString("CODE_RATTACHEMENT").length() > 0) {
                initProcessus = true;
            }
            String structureParDefaut = "";
            String rubriqueParDefaut = "";
            if (!initProcessus) {
                // On sélectionne la rubrique et la structure de + haut niveau
                // Pour cela, on balaie tous les périmètres
                int niveauRubriqueCourante = 10;
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                final Collection<Perimetre> listePerimetres = autorisations.getListePerimetres(new PermissionBean("TECH", "pho", "C"));
                for (final Perimetre perimetreCourant : listePerimetres) {
                    // Rubrique de plus haut niveau
                    if ("".equals(perimetreCourant.getCodeEspaceCollaboratif()) && perimetreCourant.getCodeRubrique().length() > 0) {
                        final RubriqueBean infosRubrique = serviceRubrique.getRubriqueByCode(perimetreCourant.getCodeRubrique());
                        //  On vérifie que le niveau est < au niveau courant
                        final int niveauRubrique = serviceRubrique.getLevel(infosRubrique);
                        if (niveauRubrique < niveauRubriqueCourante) {
                            // On vérifie que la rubrique est compatible avec la structure par défaut
                            niveauRubriqueCourante = niveauRubrique;
                            rubriqueParDefaut = perimetreCourant.getCodeRubrique();
                            structureParDefaut = perimetreCourant.getCodeStructure();
                        }
                    }
                }
                if (rubriqueParDefaut.length() > 0) {
                    infoBean.set("CODE_RUBRIQUE", rubriqueParDefaut);
                    final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(rubriqueParDefaut);
                    if (rubriqueBean != null) {
                        infoBean.set("LIBELLE_CODE_RUBRIQUE", rubriqueBean.getIntitule());
                    } else {
                        infoBean.set("LIBELLE_CODE_RUBRIQUE", StringUtils.EMPTY);
                    }
                }
                if ("".equals(structureParDefaut)) {
                    // On sélectionne la structure de l'utilisateur
                    final String structureUtilisateur = autorisations.getCodeStructure();
                    if (structureUtilisateur.length() > 0) {
                        // On regarde si l'autorisation est compatible avec la rubrique de plus haut niveau
                        if (autorisations.possedePermission(new PermissionBean("TECH", "pho", "C"), new Perimetre(structureUtilisateur, rubriqueParDefaut, "*", "*", ""))) {
                            structureParDefaut = structureUtilisateur;
                        }
                    }
                }
                if (structureParDefaut.length() > 0) {
                    infoBean.set("CODE_RATTACHEMENT", structureParDefaut);
                    infoBean.set("LIBELLE_CODE_RATTACHEMENT", serviceStructure.getDisplayableLabel(structureParDefaut, "0"));
                }
            }
        }
    }

    /**
     * Gestion de l'écran de saisie d'un Media.
     *
     * @throws ErreurApplicative lors d'une erreur de saisie
     * @throws IOException lors d'acces fichier
     */
    private void traiterPRINCIPAL() throws ErreurApplicative, IOException {
        /*************************************/
        /* Chargement de l'objet             */
        /*************************************/
        if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
            if (infoBean.getString("URL_RESSOURCE").length() == 0) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.FICHIER_SOURCE_OBLIGATOIRE"));
            }
            etat = ETAT_CREATION;
            media.setDateCreation(new Date(System.currentTimeMillis()));
            // mutualise 0, 1, 2
            // 0 mutualise dans la mediatheque /medias
            // 1 non mutualise et public /medias
            // 2 non mutualise et prive /WEB-INF/fichiergw
            if (autorisations != null && (infoBean.get("MUTUALISE") == null && infoBean.get("MODE") == null || (infoBean.get("MODE") != null && "1".equals(infoBean.get("MUTUALISE"))))) {
                media.setIsMutualise(Mediatheque.ETAT_MUTUALISE);
            } else {
                // on recupère le paramètre par defaut
                final String secure = PropertyHelper.getCoreProperty("mediatheque.secure." + infoBean.getString("TYPE_RESSOURCE").toLowerCase());
                if ("2".equals(secure)) {
                    media.setIsMutualise(Mediatheque.ETAT_NON_MUTUALISE_NON_PUBLIC);
                } else {
                    media.setIsMutualise(Mediatheque.ETAT_NON_MUTUALISE_PUBLIC);
                }
            }
        } else {
            media = serviceMedia.getById(Long.valueOf(infoBean.getString("ID_MEDIA")));
            etat = ETAT_MODIFICATION;
        }
        /*************************************/
        /* Validation de l'écran             */
        /*************************************/
        if (action.equals(InfoBean.ACTION_SUPPRIMER) && media != null) {
            if (autorisations != null) {
                if (!autorisations.possedePermission(new PermissionBean("TECH", "pho", "S"), new Perimetre(media.getCodeRattachement(), media.getCodeRubrique(), "*", "*", ""))) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + " : " + MessageHelper.getCoreMessage("AUTORISATION_SUPPRESSION_HORS_PERIMETRE"));
                }
            } else {
                ecranLogique = "LOGIN";
                return;
            }
            // on controle que le media n'est pas utilisé sur une fiche
            final ClauseWhere where = new ClauseWhere(ConditionHelper.egal("ID_MEDIA", Long.valueOf(infoBean.getString("ID_MEDIA")), TypeOperande.LONG));
            where.and(ConditionHelper.egalVarchar("ETAT", "1"));
            final List<RessourceBean> ressources = serviceRessource.getFromWhere(where);
            if (CollectionUtils.isNotEmpty(ressources)) {
                throw new ErreurApplicative("Impossible de supprimer cette ressource car elle est utilisée");
            }
            // controles spécifiques
            for (final Object bean : ClassBeanManager.getInstance().getBeanOfType(UsingLibelleMedia.class)) {
                if (((UsingLibelleMedia) bean).getCodesLibelleMedia().contains(media.getTypeMedia())) {
                    ((UsingLibelleMedia) bean).checkCodesLibelleMedia(media.getId());
                }
            }
            if (MediaUtils.isLocal(media)) {
                final File file = new File(MediaUtils.getPathAbsolu(media));
                if (file.exists()) {
                    file.delete();
                }
            }
            serviceMedia.delete(media.getId());
            infoBean.addMessageConfirmation("La suppression de «" + (StringUtils.isNotEmpty(media.getTitre()) ? media.getTitre() : media.getUrl()) + "» a bien été effectuée.");
            if ("RECHERCHE".equals(infoBean.getEtatObjet())) {
                traiterRECHERCHE();
            } else {
                infoBean.remove("TYPE_RESSOURCE");
                preparerRECHERCHE();
            }
        }
        if (action.equals(InfoBean.ACTION_ENREGISTRER)) {
            // sur l'ecran de selection on a 2 fois le perimetre (recherche et saisie)
            // on valorise le périmetre de saisie pour les controles
            String codeRubrique = infoBean.getString("CODE_RUBRIQUE");
            String codeRattachement = infoBean.getString("CODE_RATTACHEMENT");
            if (ecranLogique.equals(ECRAN_SELECTION)) {
                if (infoBean.get("CODE_RUBRIQUE2") != null) {
                    codeRubrique = infoBean.getString("CODE_RUBRIQUE2");
                }
                if (infoBean.get("CODE_RATTACHEMENT2") != null) {
                    codeRattachement = infoBean.getString("CODE_RATTACHEMENT2");
                }
            }
            // si le media est stocké dans la médiathèque, on controle les droits
            if ("0".equals(media.getIsMutualise())) {
                if (etat == ETAT_CREATION) {
                    if (!autorisations.possedePermission(new PermissionBean("TECH", "pho", "C"), new Perimetre(codeRattachement, codeRubrique, "*", "*", ""))) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + " : " + MessageHelper.getCoreMessage("AUTORISATION_ENREGISTREMENT_HORS_PERIMETRE"));
                    }
                }
                if (etat == ETAT_MODIFICATION) {
                    boolean effectuerControle = true;
                    if (autorisations.getCode().equals(media.getCodeRedacteur())) {
                        effectuerControle = false;
                        if (infoBean.get("CODE_RATTACHEMENT") != null) {
                            if (!infoBean.getString("GRS_SAUVEGARDE_CODE_RATTACHEMENT").equals(infoBean.get("CODE_RATTACHEMENT"))) {
                                effectuerControle = true;
                            }
                        }
                        if (infoBean.get("CODE_RUBRIQUE") != null) {
                            if (!infoBean.getString("GRS_SAUVEGARDE_CODE_RUBRIQUE").equals(infoBean.get("CODE_RUBRIQUE"))) {
                                effectuerControle = true;
                            }
                        }
                    }
                    if (effectuerControle) {
                        if (!autorisations.possedePermission(new PermissionBean("TECH", "pho", "M"), new Perimetre(infoBean.getString("CODE_RATTACHEMENT"), infoBean.getString("CODE_RUBRIQUE"), "*", "*", ""))) {
                            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE") + " : " + MessageHelper.getCoreMessage("AUTORISATION_ENREGISTREMENT_HORS_PERIMETRE"));
                        }
                    }
                }
                media.setCodeRattachement(codeRattachement);
                media.setCodeRubrique(codeRubrique);
            }
            if (ecranLogique.equals(ECRAN_SELECTION)) {
                media.setTitre(infoBean.getString("TITRE2"));
                media.setLegende(infoBean.getString("LEGENDE2"));
                media.setDescription(infoBean.getString("DESCRIPTION2"));
            } else {
                media.setTitre(infoBean.getString("TITRE"));
                media.setLegende(infoBean.getString("LEGENDE"));
                media.setDescription(infoBean.getString("DESCRIPTION"));
            }
            media.setFormat(infoBean.getString("FORMAT"));
            media.setSource(infoBean.getString("SOURCE"));
            media.setAuteur(infoBean.getString("AUTEUR"));
            media.setCopyright(infoBean.getString("COPYRIGHT"));
            media.setThematique(infoBean.getString("THEMATIQUE"));
            media.setMetaKeywords(infoBean.getString("META_KEYWORDS"));
            getTraductionInfoBean();
            if (StringUtils.isNotBlank(infoBean.getString("CODE_REDACTEUR"))) {
                media.setCodeRedacteur(infoBean.getString("CODE_REDACTEUR"));
            } else {
                media.setCodeRedacteur((String) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE));
            }
            media.setTypeRessource(infoBean.getString("TYPE_RESSOURCE").toLowerCase());
            // traitement de la vignette
            String sPathVignette = MediaUtils.getUrlVignetteAbsolue(media);
            final UploadedFile file = (UploadedFile) infoBean.get("VIGNETTE_FILE");
            boolean vignetteExterne = false;
            String sVignetteName = "";
            if (file != null) {
                // génération d'une vignette
                final int largeur;
                final int hauteur;
                if (StringUtils.isNotEmpty(PropertyHelper.getCoreProperty("mediatheque." + media.getTypeRessource() + ".vignette.width")) && StringUtils.isNotEmpty(PropertyHelper.getCoreProperty("mediatheque." + media.getTypeRessource() + ".vignette.height"))) {
                    largeur = Integer.parseInt(PropertyHelper.getCoreProperty("mediatheque." + media.getTypeRessource() + ".vignette.width"));
                    hauteur = Integer.parseInt(PropertyHelper.getCoreProperty("mediatheque." + media.getTypeRessource() + ".vignette.height"));
                } else if (media.getTypeRessource().equals(PropertyHelper.getCoreProperty("mediatheque.video.player.media.type"))) {
                    largeur = Integer.parseInt(PropertyHelper.getCoreProperty("mediatheque.video.player.width"));
                    hauteur = Integer.parseInt(PropertyHelper.getCoreProperty("mediatheque.video.player.height"));
                } else {
                    final String critere = PropertyHelper.getCoreProperty(MediathequeHelper.CRITERES_VIGNETTE_PROPERTIES_KEY);
                    final String[] lstLimite = critere.split("/", -2);
                    largeur = Integer.parseInt(lstLimite[1]);
                    hauteur = Integer.parseInt(lstLimite[2]);
                }
                File fVignette = file.getTemporaryFile();
                final String extension = FileUtil.getExtension(file.getContentFilename()).toLowerCase();
                fVignette = PhotoUtil.resize(fVignette.getAbsolutePath(), extension, largeur, hauteur, false);
                sPathVignette = fVignette.getAbsolutePath();
                vignetteExterne = true;
                sVignetteName = infoBean.getString("TYPE_RESSOURCE").toLowerCase() + "_" + System.currentTimeMillis() + "." + extension;
            }
            // supression de la vignette
            else if (sPathVignette.length() > 0 && "".equals(infoBean.getString("URL_VIGNETTE"))) {
                sPathVignette = "";
                vignetteExterne = true;
            }
            // traitement de la ressource
            String sPathFile = "";
            String fileName = "";
            final SpecificMedia specificMedia;
            if ("false".equals(infoBean.get("IS_LOCAL"))) {
                final String surl = infoBean.getString("URL_RESSOURCE");
                // FIXME [FLE]: le validateur Hibernate ne valide... rien. Donc je passe à une méthode connue en attendant la refonte.
                // FIXME [FLE]: Pour faire simple, on laisse java essayer de construire l'URL. S'il n'y arrive pas, c'est qu'elle n'est pas bonne.
                try {
                    new URL(surl);
                } catch (final MalformedURLException e) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("ST_MEDIATHEQUE_MSG_URL_INVALIDE"), e);
                }
                media.setSource(surl);
                media.setUrl(MediathequeHelper.convertToEmbedUrl(surl));
                media.setTypeRessource(infoBean.getString("TYPE_RESSOURCE").toLowerCase());
                final SpecificUrl specificUrl = mediatheque.getSpecificUrl(surl);
                if (specificUrl != null) {
                    media.setUrl(specificUrl.processUrl(surl));
                    media.setTypeRessource(specificUrl.getTypeRessource());
                    infoBean.set("SPECIFIC_URL", "1");
                }
                specificMedia = mediatheque.getRessource(media.getTypeRessource());
                if (infoBean.get("TYPE_MEDIA_" + media.getTypeRessource().toUpperCase()) != null && !"0000".equals(infoBean.getString("TYPE_MEDIA_" + media.getTypeRessource().toUpperCase()))) {
                    media.setTypeMedia(infoBean.getString("TYPE_MEDIA_" + media.getTypeRessource().toUpperCase()));
                }
            } else {
                specificMedia = mediatheque.getRessource(infoBean.getString("TYPE_RESSOURCE").toLowerCase());
                if (specificMedia != null) {
                    try {
                        // controle du source di fichier si changement
                        final String resourceUrl = infoBean.getString(SpecificMedia.INFOBEAN_KEY_URL_RESSOURCE);
                        // on vérifie que la source est bien un fichier récemment uploadé
                        if (!resourceUrl.equals(MediaUtils.getUrlAbsolue(media))) {
                            final File temp = new File(String.format("%s/%s", MediathequeHelper.getTempPath(), resourceUrl.replace("/medias/tmp", StringUtils.EMPTY)));
                            if (!temp.exists()) {
                                throw new IOException("le fichier source a été modifié");
                            }
                            infoBean.set(SpecificMedia.INFOBEAN_KEY_URL_RESSOURCE, temp.getAbsolutePath());
                        }
                        sPathFile = specificMedia.processMedia(infoBean, media);
                        if (!vignetteExterne) {
                            sPathVignette = specificMedia.processVignette(infoBean, media);
                        }
                        fileName = MediaUtils.generateName(media, FileUtil.getExtension(media.getSource()));
                    } catch (final Exception e) {
                        throw new ErreurApplicative(e.getMessage(), e);
                    }
                }
            }
            serviceMedia.save(media, sPathFile, sPathVignette, fileName, sVignetteName);
            infoBean.set("URL_RESSOURCE", MediaUtils.getUrlAbsolue(media));
            infoBean.addMessageConfirmation("Le média «" + (StringUtils.isNotEmpty(media.getTitre()) ? media.getTitre() : media.getUrl()) + "» a bien été enregistré.");
            // traitement du processus
            if (MODE_SELECTION.equals(infoBean.getString("MODE"))) {
                // redirection vers le formulaire complet si on vient d'un ajout simplifié et que le media est mutualisé
                if ("0".equals(media.getIsMutualise()) && ecranLogique.equals(ECRAN_SELECTION)) {
                    //                    ecranLogique = ECRAN_CONFIRMATION;
                    etat = ETAT_MODIFICATION;
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                    preparerPRINCIPAL();
                } else {
                    String libelleFichierJoint = "";
                    if (media.getTitre().length() > 0) {
                        libelleFichierJoint = media.getTitre() + " - ";
                    }
                    libelleFichierJoint += media.getSource();
                    infoBean.set("TITRE", libelleFichierJoint);
                    infoBean.set("DATE_CREATION", media.getDateCreation());
                    // creation de la ressource temporaire entre le media et la fiche sauf en mode contribution
                    final RessourceBean ressource = new RessourceBean();
                    ressource.setIdMedia(media.getId());
                    try {
                        ressource.setOrdre(Integer.valueOf(infoBean.getString("NO_FICHIER")));
                    } catch (final NumberFormatException e) {
                        LOGGER.debug("parsing error on order value", e);
                    }
                    if ("CONTRIBUTION".equals(infoBean.getString("MODE_FICHIER"))) {
                        ressource.setEtat("1");
                        ressource.setCodeParent(infoBean.getString("CODE_PARENT"));
                    } else {
                        ressource.setEtat("0");
                    }
                    serviceRessource.add(ressource);
                    infoBean.set("ID_RESSOURCE", ressource.getId());
                    ecranLogique = ECRAN_CONFIRMATION;
                }
            } else if (MODE_INSERTION.equals(infoBean.getString("MODE"))) {
                infoBean.set("ID_MEDIA", media.getId().toString());
                // redirection vers le formulaire complet si on vient d'un ajout simplifié et que le media est mutualisé
                if ("0".equals(media.getIsMutualise()) && ecranLogique.equals(ECRAN_SELECTION)) {
                    etat = ETAT_MODIFICATION;
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                    preparerPRINCIPAL();
                } else {
                    String codeParent = "";
                    if (infoBean.get("ID_FICHE") != null && !"0".equals(infoBean.get("ID_FICHE"))) {
                        codeParent += infoBean.getString("ID_FICHE") + ",";
                    }
                    codeParent += "TYPE=IMG_" + infoBean.getString("OBJET") + ",CODE=" + infoBean.getString("CODE");
                    // creation de la ressource temporaire entre le media et la fiche
                    final RessourceBean ressource = new RessourceBean();
                    ressource.setCodeParent(codeParent);
                    ressource.setCodeParent(codeParent);
                    ressource.setIdMedia(media.getId());
                    ressource.setEtat("0");
                    serviceRessource.add(ressource);
                    if (StringUtils.isNotBlank(MediaUtils.getUrlVignetteAbsolue(media))) {
                        infoBean.set("URL_VIGNETTE", MediaUtils.getUrlVignetteAbsolue(media));
                    } else if (specificMedia != null) {
                        infoBean.set("URL_VIGNETTE", specificMedia.getUrlVignette());
                    }
                    if (!"LIEN".equals(infoBean.getString("MODE_FICHIER"))) {
                        preparerRECHERCHE();
                        ecranLogique = ECRAN_SELECTION;
                    } else {
                        ecranLogique = ECRAN_CONFIRMATION;
                    }
                }
            } else {
                //ecranLogique = ECRAN_SELECTION;
                infoBean.remove("TYPE_RESSOURCE");
                preparerRECHERCHE();
            }
        }
        if (action.equals(InfoBean.ACTION_ANNULER)) {
            ecranLogique = "FIN_TOOLBOX";
        }
    }

    /**
     * Gestion de l'écran d'upload d'une archive.
     *
     * @throws IOException
     *             the exception
     */
    private void traiterUPLOAD() throws IOException {
        final UploadedFile zip = (UploadedFile) infoBean.get("ZIP_FILE");
        final File archive = zip.getTemporaryFile();
        final File folder = new File(archive.getParentFile().getAbsolutePath() + File.separator + "upload");
        if (!archive.exists()) {
            LOGGER.info("Aucune archive d'import trouvée...");
            return;
        }
        LOGGER.info("Décompression de l'archive d'import...");
        Zip.decompress(archive, folder, false);
        // controle du type
        String typeRessource = infoBean.getString("TYPE_RESSOURCE");
        if (typeRessource == null || typeRessource.length() == 0 || "0000".equals(typeRessource)) {
            typeRessource = "";
            infoBean.set("TYPE_RESSOURCE", typeRessource);
        }
        infoBean.set("NB_UPLOAD", 0);
        infoBean.set("NB_ERROR", 0);
        infoBean.set("MSG_ERROR", "");
        mediatheque.uploadMediasByFolder(infoBean, folder, true, autorisations.getCode());
        ecranLogique = ECRAN_UPLOAD;
        infoBean.setEtatObjet("UPLOAD");
        infoBean.setTitreEcran(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.SUMSUP"));
    }

    private UploadedFile moveAndGetUploadedFile() {
        final UploadedFile requestFile = (UploadedFile) infoBean.get("MEDIA_FILE");
        final File mediaTmp = new File(MediathequeHelper.getTempPath());
        final File mediaTmpFile = new File(String.format("%s/%s.%s", MediathequeHelper.getTempPath(), requestFile.getTemporaryFile().getName(), FileUtil.getExtension(requestFile.getContentFilename())));
        UploadedFile newFile;
        try {
            FileUtils.forceMkdir(mediaTmp);
            FileUtils.moveFile(requestFile.getTemporaryFile(), mediaTmpFile);
            newFile = new UploadedFile(requestFile.getContentType(), requestFile.getContentLength(), requestFile.getContentFilename(), mediaTmpFile);
            infoBean.set("MEDIA_FILE", newFile);
            infoBean.set(PREVIEW_AVAILABLE, true);
        } catch (final IOException e) {
            LOGGER.error("Une erreur est survenue lors du déplacement du fichier temporaire", e);
            newFile = requestFile;
            infoBean.set(PREVIEW_AVAILABLE, false);
        }
        return newFile;
    }

    /**
     * Gestion du controle du media uploadé.
     *
     * @throws Exception
     *             the exception
     */
    private void controlerMedia() throws ErreurApplicative, IOException {
        if (ecranLogique.equals(ECRAN_SELECTION)) {
            infoBean.set("ID_ACTIVE_ONGLET", ID_ONGLET_RESSOURCE_SELECTION);
        } else {
            infoBean.set("ID_ACTIVE_ONGLET", ID_ONGLET_RESSOURCE);
        }
        if ("true".equals(infoBean.get("IS_LOCAL"))) {
            String typeRessource = infoBean.getString("TYPE_RESSOURCE_PREV");
            boolean filtreType = true;
            // le fichier uploade
            UploadedFile file = moveAndGetUploadedFile();
            // sinon fichier existant
            if (file == null) {
                if (infoBean.get("SOURCE") != null && StringUtils.isNotEmpty(infoBean.getString("URL_RESSOURCE"))) {
                    file = new UploadedFile(infoBean.getString("FORMAT"), 0, infoBean.getString("SOURCE"), new File(infoBean.getString("URL_RESSOURCE")));
                } else {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.FICHIER_SOURCE_OBLIGATOIRE"));
                }
            }
            // controle du type, par defaut = FICHIER
            if (StringUtils.isEmpty(typeRessource) || "0000".equals(typeRessource)) {
                typeRessource = infoBean.getString("TYPE_RESSOURCE");
                if (typeRessource == null || typeRessource.length() == 0 || "0000".equals(typeRessource)) {
                    typeRessource = "fichier";
                    final String extension = FileUtil.getExtension(file.getContentFilename());
                    final String typeTemp = mediatheque.getTypesRessourcesByExtensions().get(extension);
                    if (typeTemp != null) {
                        typeRessource = typeTemp;
                    }
                    filtreType = false;
                }
            }
            String sUrlMedia = "";
            SpecificMedia specificMedia = null;
            // controle du type
            try {
                specificMedia = mediatheque.getRessource(typeRessource.toLowerCase());
            } catch (final Exception e) {
                LOGGER.debug("unable to query on ressourde table", e);
                // rien à dire le type n'est pas reconnu
            } finally {
                if (specificMedia == null) {
                    filtreType = false;
                    typeRessource = "fichier";
                    specificMedia = mediatheque.getRessource(typeRessource);
                }
            }
            if (filtreType) {
                String extensions = "";
                for (final String ext : specificMedia.getExtensions()) {
                    if (extensions.length() > 0) {
                        extensions += ",";
                    }
                    extensions += ext;
                }
                infoBean.set(UploadedFile.KEY_FILE_EXTENSIONS + "_MEDIA", extensions);
            }
            infoBean.set("LIBELLE_TYPE_RESSOURCE", specificMedia.getLibelleAffichable());
            try {
                sUrlMedia = specificMedia.checkMedia(infoBean, file);
                sUrlMedia = String.format("/medias/tmp/%s", file.getTemporaryFile().getName());
            } catch (final Exception e) {
                // on restaure les donnees du fichier source
                infoBean.remove("CONTROLE_RESSOURCE");
                infoBean.remove("NEW_RESSOURCE");
                infoBean.set("URL_RESSOURCE", infoBean.getString("URL_RESSOURCE"));
                throw new ErreurApplicative(e.getMessage(), e);
            }
            infoBean.set("EXTENSION", FileUtil.getExtension(file.getContentFullFilename()));
            infoBean.set("TYPE_RESSOURCE", typeRessource);
            infoBean.set("URL_RESSOURCE", sUrlMedia);
            Long fTaille = infoBean.get("POIDS", Long.class);
            if(fTaille != null) {
                infoBean.set("POIDS", FileUtils.byteCountToDisplaySize(fTaille));
            } else {
                infoBean.set("POIDS", FileUtils.byteCountToDisplaySize(0));
            }
        } else {
            infoBean.set("POIDS", "0");
            infoBean.set("FORMAT", "");
            String typeRessource = infoBean.getString("TYPE_RESSOURCE_PREV");
            final String source = infoBean.getString("URL_RESSOURCE");
            // controle du type, par defaut = FICHIER
            if (typeRessource == null || typeRessource.length() == 0 || "0000".equals(typeRessource)) {
                typeRessource = infoBean.getString("TYPE_RESSOURCE");
                if (typeRessource == null || typeRessource.length() == 0 || "0000".equals(typeRessource)) {
                    typeRessource = "fichier";
                    final String typeTemp = mediatheque.getTypesRessourcesByExtensions().get(FileUtil.getExtension(source));
                    if (typeTemp != null) {
                        typeRessource = typeTemp;
                    }
                }
            }
            if (typeRessource.length() > 0) {
                infoBean.set("TYPE_RESSOURCE", typeRessource);
                infoBean.set("FORMAT", mediatheque.getContentType(source));
            }
        }
        infoBean.set("CRITERE", "4");
        infoBean.set("CRITERE4", "4");
        infoBean.set("CONTROLE_RESSOURCE", "true");
        infoBean.set("NEW_RESSOURCE", "true");
        // sur l'ecran de selection, on passe directement a l'enregistrement
        if (ecranLogique.equals(ECRAN_SELECTION)) {
            action = InfoBean.ACTION_ENREGISTRER;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
            traiterPRINCIPAL();
        }
        // sinon on renvoit sur la saisie
        else {
            ecranLogique = ECRAN_PRINCIPAL;
        }
    }

    /**
     * Selectionner media.
     */
    public void selectionnerMedia() {
        media = serviceMedia.getById(Long.valueOf(infoBean.getString("ID_MEDIA")));
        String libelleFichierJoint = "";
        if (media.getTitre().length() > 0) {
            libelleFichierJoint = media.getTitre() + " - ";
        }
        libelleFichierJoint += media.getSource();
        infoBean.set("TITRE", libelleFichierJoint);
        infoBean.set("DATE_CREATION", media.getDateCreation());
        infoBean.set("FORMAT", media.getFormat());
        infoBean.set("LEGENDE", media.getLegende());
        infoBean.set("TYPE_RESSOURCE", media.getTypeRessource());
        if (StringUtils.isNotEmpty(serviceMedia.getSpecificData(media, "LARGEUR"))) {
            infoBean.set("WIDTH", serviceMedia.getSpecificData(media, "LARGEUR"));
        }
        if (StringUtils.isNotEmpty(serviceMedia.getSpecificData(media, "HAUTEUR"))) {
            infoBean.set("HEIGHT", serviceMedia.getSpecificData(media, "HAUTEUR"));
        }
        // creation de la ressource temporaire entre le media et la fiche
        final RessourceBean ressource = new RessourceBean();
        ressource.setIdMedia(media.getId());
        try {
            ressource.setOrdre(Integer.valueOf(infoBean.getString("NO_FICHIER")));
        } catch (final NumberFormatException e) {
            LOGGER.debug("parsing error on order value", e);
        }
        if ("CONTRIBUTION".equals(infoBean.getString("MODE_FICHIER"))) {
            ressource.setEtat("1");
            ressource.setCodeParent(infoBean.getString("CODE_PARENT"));
        } else {
            ressource.setEtat("0");
        }
        serviceRessource.add(ressource);
        infoBean.set("ID_RESSOURCE", ressource.getId());
        ecranLogique = ECRAN_CONFIRMATION;
    }

    /**
     * Inserer media.
     *
     * @throws IOException
     *             the exception
     */
    public void insererMedia() throws IOException {
        String codeParent = "";
        media = serviceMedia.getById(Long.valueOf(infoBean.getString("ID_MEDIA")));
        if (!MediaUtils.isLocal(media)) {
            final SpecificUrl specificUrl = mediatheque.getSpecificUrl(MediaUtils.getUrlAbsolue(media));
            if (specificUrl != null) {
                infoBean.set("SPECIFIC_URL", "1");
            }
        }
        if (infoBean.get("ID_FICHE") != null && !"0".equals(infoBean.get("ID_FICHE"))) {
            codeParent += infoBean.getString("ID_FICHE") + ",";
        }
        codeParent += "TYPE=IMG_" + infoBean.getString("OBJET") + ",CODE=" + infoBean.getString("CODE");
        // controle si la ressource est déjà liée
        RessourceBean ressource = serviceRessource.getByMediaIdAndCodeParent(media.getId(), codeParent);
        if (ressource == null) {
            ressource = new RessourceBean();
            // creation de la ressource temporaire entre le media et la fiche
            ressource.setCodeParent(codeParent);
            ressource.setIdMedia(media.getId());
            ressource.setEtat("0");
            serviceRessource.add(ressource);
        }
        infoBean.set("TYPE_RESSOURCE", media.getTypeRessource());
        infoBean.set("ID_MEDIA", media.getId().toString());
        infoBean.set("IS_LOCAL", Boolean.toString(MediaUtils.isLocal(media)));
        final SpecificMedia specificMedia = mediatheque.getRessource(media.getTypeRessource().toLowerCase());
        specificMedia.setContenuInfoBean(infoBean, media.getSpecificData());
        if (StringUtils.isNotBlank(MediaUtils.getUrlVignetteAbsolue(media))) {
            infoBean.set("URL_VIGNETTE", MediaUtils.getUrlVignetteAbsolue(media));
        } else {
            infoBean.set("URL_VIGNETTE", specificMedia.getUrlVignette());
        }
        if (!"LIEN".equals(infoBean.getString("MODE_FICHIER"))) {
            preparerINSERTION();
            preparerRECHERCHE();
            ecranLogique = ECRAN_SELECTION;
        } else {
            ecranLogique = ECRAN_CONFIRMATION;
        }
    }

    private void preparePreview(final InfoBean infoBean, final MediaBean media) {
        if (media != null && media.getId() != null && media.getId() != 0L) {
            final String tag = String.format("[id-image]%d[/id-image]", media.getId());
            final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) infoBean.getSessionHttp().getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
            final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
            infosSession.put(tag, true);
            if ("video".equals(media.getTypeRessource())) {
                prepareVideoForToolbox(infoBean, media, tag);
            } else if ("photo".equals(media.getTypeRessource())) {
                prepareImageForToolbox(infoBean, media, tag);
            } else if ("audio".equals(media.getTypeRessource())) {
                prepareAudioForToolbox(infoBean, media, tag);
            } else if ("flash".equals(media.getTypeRessource())) {
                prepareFlashForToolbox(infoBean, tag);
            } else if ("flipbook".equals(media.getTypeRessource())) {
                prepareFlipbookForToolbox(infoBean, tag);
            }
            if (StringUtils.isBlank(infoBean.get("URL_RESSOURCE", String.class))) {
                infoBean.set("URL_RESSOURCE", MediaUtils.getUrlAbsolue(media));
            }
        }
    }

    private void prepareImageForToolbox(final InfoBean infoBean, final MediaBean media, final String tag) {
        if(StringUtils.isBlank(infoBean.get("ALT", String.class))) {
            infoBean.set("ALT", media.getTitre());
        }
        infoBean.set("URL", String.format("/servlet/com.univ.utils.LectureImageToolbox?TAG=%s", tag));
        if (StringUtils.isBlank(infoBean.get("WIDTH", String.class)) && StringUtils.isNotEmpty(serviceMedia.getSpecificData(media, "LARGEUR"))) {
            infoBean.set("WIDTH", serviceMedia.getSpecificData(media, "LARGEUR"));
        }
        if (StringUtils.isBlank(infoBean.get("HEIGHT", String.class)) && StringUtils.isNotEmpty(serviceMedia.getSpecificData(media, "HAUTEUR"))) {
            infoBean.set("HEIGHT", serviceMedia.getSpecificData(media, "HAUTEUR"));
        }
    }

    private void prepareVideoForToolbox(final InfoBean infoBean, final MediaBean media, final String tag) {
        String extension = FileUtil.getExtension(MediaUtils.getPathAbsolu(media));
        if ("flv".equals(extension)) {
            extension = "x-flv";
        }
        infoBean.set("TAG", String.format("[media;video;URL=%s#LOCAL=%s#EXTENSION=%smedia;video]", tag, MediaUtils.isLocal(media) ? "1" : "0", extension));
        infoBean.set("EXTENSION", extension);
    }

    private void prepareAudioForToolbox(final InfoBean infoBean, final MediaBean media, final String tag) {
        final String extension = FileUtil.getExtension(MediaUtils.getPathAbsolu(media));
        infoBean.set("TAG", String.format("[media;audio;URL=%s#EXTENSION=%smedia;audio]", tag, extension));
        infoBean.set("EXTENSION", extension);
    }

    private void prepareFlashForToolbox(final InfoBean infoBean, final String tag) {
        infoBean.set("URL", String.format("/servlet/com.univ.utils.LectureImageToolbox?TAG=%s", tag));
    }

    private void prepareFlipbookForToolbox(final InfoBean infoBean, final String tag) {
        infoBean.set("URL", String.format("/servlet/com.univ.utils.LectureImageToolbox?TAG=%s", tag));
    }

    /**
     * Preparer apercu.
     *
     * @throws IOException
     *             the exception
     */
    public void preparerApercu() throws IOException {
        media = serviceMedia.getById(Long.valueOf(infoBean.getString("ID_MEDIA")));
        final SpecificMedia specificMedia = mediatheque.getRessource(media.getTypeRessource().toLowerCase());
        infoBean.set("ID_MEDIA", media.getId().toString());
        infoBean.set("TYPE_RESSOURCE", media.getTypeRessource());
        infoBean.set("URL_RESSOURCE", MediaUtils.getUrlAbsolue(media));
        infoBean.set("SOURCE", media.getSource());
        infoBean.set("FORMAT", media.getFormat());
        infoBean.set("POIDS", media.getPoids());
        infoBean.set("SPECIFIC_DATA", serviceMedia.getSpecificDataAsString(media).replaceAll("\n", "<br />"));
        infoBean.set("TYPE_MEDIA", media.getTypeMedia());
        infoBean.set("LIBELLE_TYPE_MEDIA", LabelUtils.getLibelle(specificMedia.getCodeTypeLibelle(), media.getTypeMedia(), LangueUtil.getDefaultLocale()));
        infoBean.set("TEMPLATE_JSP_APERCU", specificMedia.getJspApercu());
        if (MediaUtils.isLocal(media)) {
            // on stocke l'url du media en session pour ne pas controler les droits à la lecture cf LectureFichiergw
            final HttpSession session = infoBean.getSessionHttp();
            final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
            final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
            infosSession.put(MediaUtils.getUrlAbsolue(media), true);
        } else {
            final SpecificUrl specificUrl = mediatheque.getSpecificUrl(MediaUtils.getUrlAbsolue(media));
            if (specificUrl != null) {
                infoBean.set("SPECIFIC_URL", "1");
            }
        }
        specificMedia.setContenuInfoBean(infoBean, media.getSpecificData());
        ecranLogique = ECRAN_APERCU;
    }

    /**
     * Preparer references.
     *
     */
    public void preparerReferences() {
        final ClauseWhere where = new ClauseWhere(ConditionHelper.egal("ID_MEDIA", Long.valueOf(infoBean.getString("ID_MEDIA")), TypeOperande.LONG));
        where.and(ConditionHelper.egalVarchar("ETAT", "1"));
        final List<RessourceBean> ressources = serviceRessource.getFromWhere(where);
        int i = 0;
        for (final RessourceBean currentRessource : ressources) {
            final String codeParent = currentRessource.getCodeParent();
            if (codeParent != null && codeParent.trim().length() > 0) {
                String url = "";
                String redacteur = "";
                final String idFiche = codeParent.substring(0, codeParent.indexOf(","));
                String typeObjet = codeParent.substring(codeParent.indexOf("TYPE=") + 5);
                // on récupère le code objet
                if (typeObjet.length() > 4) {
                    int indiceDecoup = typeObjet.indexOf("_");
                    if (indiceDecoup != -1) {
                        typeObjet = typeObjet.substring(indiceDecoup + 1, indiceDecoup + 5);
                    }
                    indiceDecoup = typeObjet.indexOf(",");
                    if (indiceDecoup != -1) {
                        typeObjet = typeObjet.substring(0, indiceDecoup);
                    }
                }
                final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(typeObjet));
                // fiche parente
                if (ficheUniv != null) {
                    ficheUniv.setCtx(this);
                    ficheUniv.init();
                    try {
                        ficheUniv.setIdFiche(Long.valueOf(idFiche));
                        ficheUniv.retrieve();
                        if (StringUtils.isNotBlank(ficheUniv.getCodeRedacteur())) {
                            final UtilisateurBean utilisateur = serviceUser.getByCode(ficheUniv.getCodeRedacteur());
                            if (utilisateur != null) {
                                redacteur = String.format("%s %s", utilisateur.getPrenom(), utilisateur.getNom());
                            }
                        }
                        final String libelle = ficheUniv.getLibelleAffichable();
                        final String extension = ReferentielObjets.getExtension(ficheUniv);
                        final Class<?> procClass = ProcessusHelper.getClasseProcessus("SAISIE_" + ReferentielObjets.getNomObjet(typeObjet).toUpperCase(), extension);
                        if (procClass != null) {
                            url = String.format("%s?EXT=%s&PROC=SAISIE_%s&ACTION=MODIFIER&ID_FICHE=%s", WebAppUtil.SG_PATH, extension, ReferentielObjets.getNomObjet(typeObjet).toUpperCase(), idFiche);
                        }
                        infoBean.set("OBJET#" + i, ReferentielObjets.getLibelleObjet(typeObjet));
                        infoBean.set("TITRE#" + i, libelle);
                        infoBean.set("AUTEUR#" + i, redacteur);
                        infoBean.set("URL#" + i, url);
                    } catch (final Exception e) {
                        LOGGER.warn("Fiche inexistante, la référence a été supprimée", e);
                        serviceRessource.delete(currentRessource.getId());
                    }
                }
                //contenu autre
                else {
                    infoBean.set("OBJET#" + i, typeObjet);
                    infoBean.set("TITRE#" + i, "Autre contenu");
                    infoBean.set("AUTEUR#" + i, redacteur);
                    infoBean.set("URL#" + i, "");
                }
                i++;
            }
        }
        infoBean.setInt("NB_REFERENCES", i);
        ecranLogique = ECRAN_REFERENCES;
    }

    /**
     * Sets the traduction info bean.
     *
     * @throws IOException
     *             the exception
     */
    private void setTraductionInfoBean() throws IOException {
        final Hashtable<String, String> hLangue = LangueUtil.getListeLangues(getLocale());
        // français par defaut, supprimé ici
        if (hLangue.containsKey("0")) {
            hLangue.remove("0");
        }
        infoBean.set("LISTE_LANGUES", hLangue);
        if (MapUtils.isNotEmpty(hLangue)) {
            final String sDatas = media.getTraductionData();
            final Properties properties = new Properties();
            properties.load(new ByteArrayInputStream(sDatas.getBytes()));
            final Enumeration<String> en = hLangue.keys();
            while (en.hasMoreElements()) {
                final String sLg = en.nextElement();
                infoBean.set("TITRE_" + sLg, properties.getProperty("TITRE_" + sLg) != null ? properties.getProperty("TITRE_" + sLg) : "");
                infoBean.set("LEGENDE_" + sLg, properties.getProperty("LEGENDE_" + sLg) != null ? properties.getProperty("LEGENDE_" + sLg) : "");
                infoBean.set("DESCRIPTION_" + sLg, properties.getProperty("DESCRIPTION_" + sLg) != null ? properties.getProperty("DESCRIPTION_" + sLg) : "");
                infoBean.set("AUTEUR_" + sLg, properties.getProperty("AUTEUR_" + sLg) != null ? properties.getProperty("AUTEUR_" + sLg) : "");
                infoBean.set("COPYRIGHT_" + sLg, properties.getProperty("COPYRIGHT_" + sLg) != null ? properties.getProperty("COPYRIGHT_" + sLg) : "");
                infoBean.set("META_KEYWORDS_" + sLg, properties.getProperty("META_KEYWORDS_" + sLg) != null ? properties.getProperty("META_KEYWORDS_" + sLg) : "");
            }
        }
    }

    /**
     * Gets the traduction info bean.
     *
     *
     * @throws IOException
     *             the exception
     */
    private void getTraductionInfoBean() throws IOException {
        final Hashtable<String, String> hLangue = LangueUtil.getListeLangues(getLocale());
        // langue par defaut, supprimé ici
        if (hLangue.containsKey("0")) {
            hLangue.remove("0");
        }
        String sDatas = "";
        if (MapUtils.isNotEmpty(hLangue)) {
            final Properties properties = new Properties();
            properties.load(new ByteArrayInputStream(sDatas.getBytes()));
            final Enumeration<String> en = hLangue.keys();
            while (en.hasMoreElements()) {
                final String sLg = en.nextElement();
                if (infoBean.getString("TITRE_" + sLg) != null && infoBean.getString("TITRE_" + sLg).length() > 0) {
                    properties.setProperty("TITRE_" + sLg, infoBean.getString("TITRE_" + sLg));
                }
                if (infoBean.getString("LEGENDE_" + sLg) != null && infoBean.getString("LEGENDE_" + sLg).length() > 0) {
                    properties.setProperty("LEGENDE_" + sLg, infoBean.getString("LEGENDE_" + sLg));
                }
                if (infoBean.getString("DESCRIPTION_" + sLg) != null && infoBean.getString("DESCRIPTION_" + sLg).length() > 0) {
                    properties.setProperty("DESCRIPTION_" + sLg, infoBean.getString("DESCRIPTION_" + sLg));
                }
                if (infoBean.getString("AUTEUR_" + sLg) != null && infoBean.getString("AUTEUR_" + sLg).length() > 0) {
                    properties.setProperty("AUTEUR_" + sLg, infoBean.getString("AUTEUR_" + sLg));
                }
                if (infoBean.getString("COPYRIGHT_" + sLg) != null && infoBean.getString("COPYRIGHT_" + sLg).length() > 0) {
                    properties.setProperty("COPYRIGHT_" + sLg, infoBean.getString("COPYRIGHT_" + sLg));
                }
                if (infoBean.getString("META_KEYWORDS_" + sLg) != null && infoBean.getString("META_KEYWORDS_" + sLg).length() > 0) {
                    properties.setProperty("META_KEYWORDS_" + sLg, infoBean.getString("META_KEYWORDS_" + sLg));
                }
            }
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            properties.store(baos, "");
            sDatas = baos.toString();
        }
        media.setTraductionData(sDatas);
    }

    /**
     * Affichage de l'écran des critères de recherche d'un Photo.
     *
     */
    private void preparerUPLOAD() {
        ecranLogique = ECRAN_UPLOAD;
        infoBean.setEtatObjet("UPLOAD");
        infoBean.set("CODE_RATTACHEMENT", "");
        infoBean.set("LIBELLE_CODE_RATTACHEMENT", "");
        infoBean.set("CODE_RUBRIQUE", "");
        infoBean.set("LIBELLE_CODE_RUBRIQUE", "");
        preparerPerimetre("pho", "C");
        infoBean.set("DESCRIPTION", "");
        infoBean.set("AUTEUR", "");
        infoBean.set("COPYRIGHT", "");
        infoBean.set("THEMATIQUE", "");
        infoBean.set("LIBELLE_THEMATIQUE", MediaUtils.getLibelleThematique(media));
        infoBean.set("LISTE_THEMATIQUES", LabelUtils.getLabelCombo("04", LangueUtil.getDefaultLocale()));
        infoBean.set("META_KEYWORDS", "");
        infoBean.set(UploadedFile.KEY_FILE_EXTENSIONS + "_ZIP", "zip");
        // préparation des listes de types et ss type liés
        // boucle sur tous les types de média
        for (final String sKeyType : mediatheque.getTypesMedia().keySet()) {
            final SpecificMedia specificMedia = mediatheque.getRessource(sKeyType.toLowerCase());
            infoBean.set("LISTE_TYPE_MEDIA_" + sKeyType.toUpperCase(), LabelUtils.getLabelCombo(specificMedia.getCodeTypeLibelle(), LangueUtil.getDefaultLocale()));
        }
        infoBean.set("LISTE_TYPE_RESSOURCES", mediatheque.getTypesRessourcesAffichables());
    }

    /**
     * Affichage de l'écran des critères de recherche d'un Photo.
     *
     */
    private void preparerRECHERCHE() {
        ecranLogique = ECRAN_RECHERCHE;
        infoBean.setEtatObjet("RECHERCHE");
        infoBean.set("AJOUT_MEDIA", "0");
        infoBean.set("RECHERCHE_MEDIA", "0");
        if (autorisations != null) {
            Collection<Perimetre> vPerimetre = autorisations.getListePerimetres(new PermissionBean("TECH", "pho", "C"));
            if (CollectionUtils.isNotEmpty(vPerimetre)) {
                infoBean.set("AJOUT_MEDIA", "1");
            }
            vPerimetre = autorisations.getListePerimetres(new PermissionBean("TECH", "acp", ""));
            if (CollectionUtils.isNotEmpty(vPerimetre)) {
                infoBean.set("RECHERCHE_MEDIA", "1");
            }
        } else {
            ecranLogique = "LOGIN";
            return;
        }
        preparerPerimetre("acp", "");
        infoBean.set("CODE_RATTACHEMENT", "");
        infoBean.set("LIBELLE_CODE_RATTACHEMENT", "");
        infoBean.set("CODE_RUBRIQUE", "");
        infoBean.set("LIBELLE_CODE_RUBRIQUE", "");
        infoBean.set("TITRE", "");
        infoBean.set("LEGENDE", "");
        infoBean.set("DESCRIPTION", "");
        infoBean.set("AUTEUR", "");
        infoBean.set("COPYRIGHT", "");
        infoBean.set("THEMATIQUE", "");
        infoBean.set("LIBELLE_THEMATIQUE", MediaUtils.getLibelleThematique(media));
        infoBean.set("LISTE_THEMATIQUES", LabelUtils.getLabelCombo("04", getLocale()));
        infoBean.set("DATE_CREATION", "");
        infoBean.set("CODE_REDACTEUR", "");
        infoBean.set("LIBELLE_CODE_REDACTEUR", "");
        infoBean.set("META_KEYWORDS", "");
        String sTypeRessource = infoBean.getString("TYPE_RESSOURCE");
        if (sTypeRessource == null || sTypeRessource.length() == 0) {
            infoBean.remove("TYPE_RESSOURCE");
            // préparation des listes de types et ss type liés
            // boucle sur tous les types de média
            for (final String sKeyType : mediatheque.getTypesMedia().keySet()) {
                final SpecificMedia specificMedia = mediatheque.getRessource(sKeyType.toLowerCase());
                infoBean.set("LISTE_TYPE_MEDIA_" + sKeyType.toUpperCase(), LabelUtils.getLabelCombo(specificMedia.getCodeTypeLibelle(), getLocale()));
            }
            infoBean.set("LISTE_TYPE_RESSOURCES", mediatheque.getTypesRessourcesAffichables());
        } else {
            SpecificMedia specificMedia = null;
            // controle du type
            try {
                specificMedia = mediatheque.getRessource(sTypeRessource.toLowerCase());
            } catch (final Exception e) {
                // rien à dire le type n'est pas reconnu
                LOGGER.debug("unknown type " + sTypeRessource, e);
            } finally {
                if (specificMedia == null) {
                    sTypeRessource = "fichier";
                    specificMedia = mediatheque.getRessource(sTypeRessource);
                }
            }
            infoBean.set("LIBELLE_TYPE_RESSOURCE", specificMedia.getLibelleAffichable());
            infoBean.set("TYPE_MEDIA_" + sTypeRessource.toUpperCase(), "");
            infoBean.set("LISTE_TYPE_MEDIA_" + sTypeRessource.toUpperCase(), LabelUtils.getLabelCombo(specificMedia.getCodeTypeLibelle(), getLocale()));
        }
        infoBean.setTitreEcran(StringUtils.EMPTY);
    }

    /**
     * Affichage de l'écran des critères de recherche d'un Photo.
     *
     */
    private void preparerINSERTION() {
        ecranLogique = ECRAN_SELECTION;
        infoBean.set("AJOUT_MEDIA", "0");
        preparerPerimetre("pho", "C");
        infoBean.set("CODE_RATTACHEMENT2", infoBean.get("CODE_RATTACHEMENT"));
        infoBean.set("CODE_RUBRIQUE2", infoBean.get("CODE_RUBRIQUE"));
        if (autorisations != null) {
            final Collection<Perimetre> vPerimetre = autorisations.getListePerimetres(new PermissionBean("TECH", "pho", "C"));
            if (CollectionUtils.isNotEmpty(vPerimetre)) {
                infoBean.set("AJOUT_MEDIA", "1");
            }
        }
        boolean type = false;
        String typeRessource = "";
        if (infoBean.get("TYPE_RESSOURCE") != null && infoBean.getString("TYPE_RESSOURCE").length() > 0) {
            typeRessource = infoBean.getString("TYPE_RESSOURCE");
            final SpecificMedia mediaSpecific = mediatheque.getRessource(typeRessource.toLowerCase());
            infoBean.set("TEMPLATE_JSP_INSERTION", mediaSpecific.getJspInsertion());
            infoBean.set("JS_INSERTION", mediaSpecific.getJsInsertion());
            String extensions = "";
            for (final String ext : mediaSpecific.getExtensions()) {
                if (extensions.length() > 0) {
                    extensions += ",";
                }
                extensions += ext;
            }
            infoBean.set(UploadedFile.KEY_FILE_EXTENSIONS + "_MEDIA", extensions);
            type = true;
        }
        if (MODE_INSERTION.equals(infoBean.getString("MODE"))) {
            String codeParent = "";
            if (infoBean.get("ID_FICHE") != null && !"0".equals(infoBean.get("ID_FICHE"))) {
                codeParent += infoBean.getString("ID_FICHE") + ",";
            }
            codeParent += "TYPE=%_" + infoBean.getString("OBJET") + ",CODE=" + infoBean.getString("CODE");
            infoBean.set("CODE_PARENT", codeParent);
            final Collection<RessourceBean> v = serviceRessource.getFilesOrderBy(codeParent, "ID_RESSOURCE");
            int j = 0;
            if (CollectionUtils.isNotEmpty(v)) {
                for (final RessourceBean file : v) {
                    if (!type || RessourceUtils.getTypeRessource(file).equalsIgnoreCase(typeRessource)) {
                        final SpecificMedia mediaSpecific = mediatheque.getRessource(RessourceUtils.getTypeRessource(file).toLowerCase());
                        final MediaBean mediaBean = serviceMedia.getById(file.getIdMedia());
                        if (mediaBean != null) {
                            infoBean.set("ID_FICHIER#" + j, file.getIdMedia());
                            if (StringUtils.isNotEmpty(serviceMedia.getSpecificData(mediaBean, "HAUTEUR"))) {
                                infoBean.set("HEIGHT#" + j, serviceMedia.getSpecificData(mediaBean, "HAUTEUR"));
                            }
                            if (StringUtils.isNotEmpty(serviceMedia.getSpecificData(mediaBean, "LARGEUR"))) {
                                infoBean.set("WIDTH#" + j, serviceMedia.getSpecificData(mediaBean, "LARGEUR"));
                            }
                            infoBean.set("LIBELLE_FICHIER#" + j, RessourceUtils.getLibelle(file));
                            if (StringUtils.isNotBlank(MediaUtils.getUrlVignetteAbsolue(mediaBean))) {
                                infoBean.set("URL_VIGNETTE#" + j, MediaUtils.getUrlVignetteAbsolue(mediaBean));
                            } else {
                                infoBean.set("URL_VIGNETTE#" + j, mediaSpecific.getUrlVignette());
                            }
                            if (StringUtils.isNotBlank(MediaUtils.getUrlAbsolue(mediaBean))) {
                                infoBean.set("URL_RESSOURCE#" + j, MediaUtils.getUrlAbsolue(mediaBean));
                            } else {
                                infoBean.set("URL_RESSOURCE#" + j, mediaSpecific.getUrl());
                            }
                            j++;
                        }
                    }
                }
            }
            infoBean.setInt("FICHIER_NB_ITEMS", j);
        } else if (infoBean.getString("MODE") == null) {
            infoBean.set("MODE", MODE_ADMINISTRATION);
        }
    }

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     */
    private void traiterRECHERCHE() {
        Date date = null;
        Long idMedia = (long) 0;
        try {
            idMedia = Long.valueOf(infoBean.getString("ID"));
        } catch (final NumberFormatException e) {
            LOGGER.debug("unable to parse the id value", e);
        }
        final String dateCreation = (String) infoBean.get("DATE_CREATION");
        if (StringUtils.isNotBlank(dateCreation)) {
            final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                date = new Date(sdf.parse(dateCreation).getTime());
            } catch (final ParseException e) {
                LOGGER.debug("unable to parse the date value", e);
            }
        }
        // Traitement du parametre typeRessource
        String typeMedia = "";
        if (infoBean.getString("TYPE_RESSOURCE") != null && infoBean.getString("TYPE_RESSOURCE").length() > 0 && !"0000".equals(infoBean.getString("TYPE_RESSOURCE"))) {
            final String cleMedia = "TYPE_MEDIA_" + infoBean.getString("TYPE_RESSOURCE").toUpperCase();
            if (infoBean.getString(cleMedia) != null && infoBean.getString(cleMedia).length() > 0 && !"0000".equals(infoBean.getString(cleMedia))) {
                typeMedia = infoBean.getString(cleMedia);
            }
        }
        String poidsMinimum = "";
        String poidsMaximum = "";
        if (StringUtils.isNotBlank(infoBean.getString("POIDS_MINIMUM"))) {
            poidsMinimum = infoBean.getString("POIDS_MINIMUM");
        }
        if (StringUtils.isNotBlank(infoBean.getString("POIDS_MAXIMUM"))) {
            poidsMaximum = infoBean.getString("POIDS_MAXIMUM");
        }
        if (infoBean.getString("MODE") == null) {
            infoBean.set("MODE", MODE_ADMINISTRATION);
        }
        //Pagination
        Integer from = 0;
        if (infoBean.get("FROM") != null && infoBean.getString("FROM").length() > 0) {
            from = Integer.valueOf(infoBean.getString("FROM"));
        }
        //nb de photos par page (choisi par l'utilisateur)
        Integer increment = 12;
        if (infoBean.get("INCREMENT") != null) {
            increment = Integer.valueOf(infoBean.getString("INCREMENT"));
        }
        final ClauseWhere where = MediaUtils.preparerRequete(idMedia, infoBean.getString("TITRE"), infoBean.getString("LEGENDE"), infoBean.getString("DESCRIPTION"), infoBean.getString("COPYRIGHT"), infoBean.getString("AUTEUR"), infoBean.getString("TYPE_RESSOURCE"), typeMedia, infoBean.getString("THEMATIQUE"), infoBean.getString("CODE_RATTACHEMENT"), infoBean.getString("CODE_RUBRIQUE"), infoBean.getString("CODE_REDACTEUR"), infoBean.getString("META_KEYWORDS"), date, infoBean.getString("MODE"), poidsMinimum, poidsMaximum);
        final int count = serviceMedia.getCountForWhere(where);
        final ClauseLimit limite = new ClauseLimit(from, increment);
        final RequeteSQL requeteSQL = new RequeteSQL();
        requeteSQL.where(where).limit(limite);
        final List<MediaBean> medias = serviceMedia.getMediasFromRequest(requeteSQL);
        infoBean.set("COUNT", String.valueOf(count));
        //selection par petits nombres
        //compteurs pour page suivante et precedente
        if (count > increment && (from + increment) < count) {
            infoBean.set("FROMPLUS", from + increment);
        } else {
            infoBean.remove("FROMPLUS");
        }
        if (from > 0) {
            infoBean.set("FROMMOINS", from - increment);
        } else {
            infoBean.remove("FROMMOINS");
        }
        infoBean.set("INCREMENT", increment);
        preparerLISTE(medias);
    }

    /**
     * Affichage de la liste des Photos.
     *
     */
    private void preparerLISTE(final List<MediaBean> medias) {
        int i = 0;
        boolean displayList = false;
        if (StringUtils.isNotBlank(infoBean.getString("SELECTION_MEDIA"))) {
            displayList = "1".equals(infoBean.getString("SELECTION_MEDIA"));
        } else {
            infoBean.set("SELECTION_MEDIA", "0");
            if (ecranLogique.equals(ECRAN_SELECTION)) {
                displayList = true;
                infoBean.set("SELECTION_MEDIA", "1");
            }
        }
        ecranLogique = ECRAN_LISTE;
        infoBean.set("SUPPRESSION_MEDIA", "0");
        infoBean.set("MODIFICATION_MEDIA", "0");
        infoBean.set("CREATION_MEDIA", "0");
        if (autorisations != null) {
            Collection<Perimetre> vPerimetre = autorisations.getListePerimetres(new PermissionBean("TECH", "pho", "S"));
            if (CollectionUtils.isNotEmpty(vPerimetre) || media.getCodeRedacteur().equals(autorisations.getCode())) {
                infoBean.set("SUPPRESSION_MEDIA", "1");
            }
            vPerimetre = autorisations.getListePerimetres(new PermissionBean("TECH", "pho", "M"));
            if (CollectionUtils.isNotEmpty(vPerimetre) || media.getCodeRedacteur().equals(autorisations.getCode())) {
                displayList = true;
                infoBean.set("MODIFICATION_MEDIA", "1");
            }
            vPerimetre = autorisations.getListePerimetres(new PermissionBean("TECH", "pho", "C"));
            if (CollectionUtils.isNotEmpty(vPerimetre) || media.getCodeRedacteur().equals(autorisations.getCode())) {
                displayList = true;
                infoBean.set("CREATION_MEDIA", "1");
            }
        }
        for (final MediaBean currentMedia : medias) {
            if (displayList) {
                final SpecificMedia specificMedia = mediatheque.getRessource(currentMedia.getTypeRessource().toLowerCase());
                if (specificMedia != null) {
                    infoBean.set("ID_FICHE#" + i, currentMedia.getId().toString());
                    infoBean.set("URL#" + i, MediaUtils.getUrlAbsolue(currentMedia));
                    infoBean.set("SOURCE#" + i, currentMedia.getSource());
                    infoBean.set("IS_LOCAL#" + i, MediaUtils.isLocal(currentMedia) ? "true" : "false");
                    if (MediaUtils.isLocal(currentMedia) || !"photo".equals(specificMedia.getCode())) {
                        infoBean.set("URL_VIGNETTE#" + i, StringUtils.defaultIfBlank(MediaUtils.getUrlVignetteAbsolue(currentMedia), specificMedia.getUrlVignette()));
                    } else {
                        infoBean.set("URL_VIGNETTE#" + i, StringUtils.defaultIfBlank(MediaUtils.getUrlAbsolue(currentMedia), specificMedia.getUrlVignette()));
                    }
                    if (StringUtils.isNotBlank(MediaUtils.getUrlAbsolue(currentMedia))) {
                        infoBean.set("URL_RESSOURCE#" + i, MediaUtils.getUrlAbsolue(currentMedia));
                    } else {
                        infoBean.set("URL_RESSOURCE#" + i, currentMedia.getUrl());
                    }
                    infoBean.set("URL_ICONE#" + i, specificMedia.getUrlIcone());
                    infoBean.set("TITRE#" + i, MediaUtils.getLibelleAffichable(currentMedia));
                    if (StringUtils.isNotEmpty(serviceMedia.getSpecificData(currentMedia, "HAUTEUR"))) {
                        infoBean.set("HEIGHT#" + i, serviceMedia.getSpecificData(currentMedia, "HAUTEUR"));
                    }
                    if (StringUtils.isNotEmpty(serviceMedia.getSpecificData(currentMedia, "LARGEUR"))) {
                        infoBean.set("WIDTH#" + i, serviceMedia.getSpecificData(currentMedia, "LARGEUR"));
                    }
                    Long fTaille = currentMedia.getPoids();
                    if(fTaille != null) {
                        infoBean.set("TAILLE#" + i, FileUtils.byteCountToDisplaySize(fTaille));
                    } else {
                        infoBean.set("TAILLE#" + i, FileUtils.byteCountToDisplaySize(0));
                    }
                    infoBean.set("SPECIFIC_DATA#" + i, serviceMedia.getSpecificDataAsString(currentMedia).replaceAll("\n", "<br />"));
                    infoBean.set("TYPE_RESSOURCE#" + i, currentMedia.getTypeRessource());
                    infoBean.set("LIBELLE_TYPE_RESSOURCE#" + i, specificMedia.getLibelleAffichable());
                    infoBean.set("TYPE_MEDIA#" + i, currentMedia.getTypeMedia());
                    infoBean.set("LIBELLE_TYPE_MEDIA#" + i, LabelUtils.getLibelle(specificMedia.getCodeTypeLibelle(), currentMedia.getTypeMedia(), LangueUtil.getDefaultLocale()));
                    i++;
                }
            }
        }
        infoBean.set("LISTE_NB_ITEMS", i);
    }
}
