package com.univ.objetspartages.processus;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.Article;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.LabelUtils;

/**
 * Processus de gestion des articles.
 */
public class SaisieArticle extends SaisieFiche {

    /** The article. */
    private Article article = null;

    /**
     * Constructeur.
     *
     * @param infoBean
     *            the info bean
     */
    public SaisieArticle(final InfoBean infoBean) {
        super(infoBean);
    }

    /**
     * Point d'entree du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        infoBean.set("CODE_OBJET", "0015");
        article = new Article();
        article.init();
        traiterActionParDefaut(article);
        infoBean.set("NOM_ONGLET", "article");
        // on continue si on n'est pas a la FIN !!!
        return etat == FIN;
    }

    /**
     * Affichage de l'ecran des criteres de recherche d'un(e) article.
     *
     * @throws Exception
     *             the exception
     */
    @Override
    protected void preparerRECHERCHE() throws Exception {
        ecranLogique = ECRAN_RECHERCHE;
        infoBean.set("LISTE_LANGUES", LangueUtil.getListeLangues(getLocale()));
        infoBean.set("LISTE_THEMATIQUES", LabelUtils.getLabelCombo("04", getLocale()));
    }

    /**
     * Affichage de l'ecran de saisie d'un(e) article.
     *
     * @throws Exception
     *             the exception
     */
    @Override
    protected void preparerPRINCIPAL() throws Exception {
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.set("ID_ARTICLE", article.getIdFiche().toString());
        infoBean.set("TITRE", article.getTitre());
        infoBean.set("CHAPEAU", article.getChapeau());
        infoBean.set("CORPS", article.getCorps());
        infoBean.set("THEMATIQUE", article.getThematique());
        infoBean.set("LIBELLE_THEMATIQUE", article.getLibelleThematique());
        infoBean.set("LISTE_THEMATIQUES", LabelUtils.getLabelCombo("04", LangueUtil.getLocale(article.getLangue())));
        if (Formateur.estSaisie(article.getDateArticle())) {
            infoBean.set("DATE_ARTICLE", article.getDateArticle());
        } else {
            infoBean.set("DATE_ARTICLE", null);
        }
        infoBean.set("SOUS_TITRE", article.getSousTitre());
        if (1000 == article.getOrdre()) {
            infoBean.set("ORDRE", "");
        } else {
            infoBean.set("ORDRE", article.getOrdre().toString());
        }
        infoBean.set("CODE_RUBRIQUE", article.getCodeRubrique());
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(article.getCodeRubrique());
        String rubriqueLabel = StringUtils.EMPTY;
        if (rubriqueBean != null) {
            rubriqueLabel = rubriqueBean.getIntitule();
        }
        infoBean.set("LIBELLE_CODE_RUBRIQUE", rubriqueLabel);
        infoBean.set("CODE_RATTACHEMENT", article.getCodeRattachement());
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        infoBean.set("LIBELLE_CODE_RATTACHEMENT", serviceStructure.getDisplayableLabel(article.getCodeRattachement(), article.getLangue()));
        infoBean.set("LIBELLE_AFFICHABLE", article.getLibelleAffichable());
        infoBean.set("SOUS_ONGLET", "PRINCIPAL"); // onglet par defaut
        ControleurUniv.preparerPRINCIPAL(infoBean, article, this);
    }

    /**
     * Traite l'ecran de saisie d'une fiche.
     *
     * @throws Exception
     *             the exception
     */
    @Override
    protected void traiterPRINCIPAL() throws Exception {
        // chargement de l'objet
        if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
            article.init();
        } else {
            article.setIdFiche(Long.valueOf(infoBean.getString("ID_ARTICLE")));
            article.retrieve();
        }
        // changement d'onglet
        if (InfoBean.ACTION_ONGLET.equals(action)) {
            infoBean.set("SOUS_ONGLET", infoBean.getString("SOUS_ONGLET_DEMANDE"));
        }
        // validation de l'ecran
        else if (InfoBean.ACTION_ENREGISTRER.equals(action)) {
            alimenteDonneesCreation(article, false);
            article.setTitre((String) infoBean.get("TITRE"));
            article.setThematique((String) infoBean.get("THEMATIQUE"));
            article.setChapeau(infoBean.getString("CHAPEAU"));
            article.setSousTitre((String) infoBean.get("SOUS_TITRE"));
            if (Formateur.estSaisie((Date) infoBean.get("DATE_ARTICLE"))) {
                article.setDateArticle((Date) infoBean.get("DATE_ARTICLE"));
            } else {
                article.setDateArticle(null);
            }
            article.setCorps((String) infoBean.get("CORPS"));
            if (StringUtils.isNotBlank(infoBean.getString("ORDRE"))) {
                try {
                    final Integer ordreArticle = Integer.valueOf(infoBean.getString("ORDRE"));
                    article.setOrdre(ordreArticle);
                } catch (final NumberFormatException e) {
                    throw new ErreurApplicative("Format de l'ordre incorrect", e);
                }
            } else {
                article.setOrdre(1000); // 1000 par defaut
            }
            if (infoBean.get("CODE_RUBRIQUE") != null && !"0000".equals(infoBean.getString("CODE_RUBRIQUE"))) {
                article.setCodeRubrique(infoBean.getString("CODE_RUBRIQUE"));
            } else {
                article.setCodeRubrique("");
            }
            article.setCodeRattachement(infoBean.getString("CODE_RATTACHEMENT"));
        }
        // appel au traitement general
        ecranLogique = ControleurUniv.traiterPRINCIPAL(infoBean, article, this);
        if (ecranLogique.length() == 0) {
            etat = FIN;
        }
    }
}
