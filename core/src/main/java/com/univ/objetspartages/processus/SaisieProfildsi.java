package com.univ.objetspartages.processus;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantProfil;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.Chaine;

/**
 * processus saisie Profildsi.
 */
public class SaisieProfildsi extends ProcessusBean {

    /** The Constant MODE_AJOUT. */
    private static final int MODE_AJOUT = 0;

    /** The Constant MODE_MODIFICATION. */
    private static final int MODE_MODIFICATION = 1;

    /** The Constant MODE_SUPPRESSION. */
    private static final int MODE_SUPPRESSION = 2;

    /** The Constant MODE_RECHERCHE. */
    private static final int MODE_RECHERCHE = 3;

    /** The Constant ECRAN_RECHERCHE. */
    private static final String ECRAN_RECHERCHE = "RECHERCHE";

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    /** The Constant ECRAN_LISTE. */
    private static final String ECRAN_LISTE = "LISTE";

    private static final Logger LOG = LoggerFactory.getLogger(SaisieProfildsi.class);

    private final ServiceProfildsi serviceProfildsi;

    /** The profildsi. */
    ProfildsiBean profildsi = null;

    /** The liste identifiant. */
    Long listeIdentifiant[] = null;

    /** The mode. */
    private int mode = -1;

    /**
     * processus saisie Profildsi.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieProfildsi(final InfoBean ciu) {
        super(ciu);
        serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
    }

    /**
     * Initialisation du processus.
     *
     * @throws Exception
     *             the exception
     */
    private void initialisation() throws Exception {
        // initalisation objets métiers
        profildsi = new ProfildsiBean();
        etat = EN_COURS;
        if ("AJOUTER".equals(action)) {
            mode = MODE_AJOUT;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
            preparerPRINCIPAL();
        }
        if ("MODIFIER".equals(action)) {
            mode = MODE_MODIFICATION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            preparerRECHERCHE();
        }
        if ("SUPPRIMER".equals(action)) {
            mode = MODE_SUPPRESSION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_SUPPRESSION);
            preparerRECHERCHE();
        }
        if ("RECHERCHER".equals(action)) {
            mode = MODE_RECHERCHE;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
            preparerRECHERCHE();
        }
        if ("MODIFIERPARID".equals(action)) {
            modifierParId();
        }
        if ("SUPPRIMERPARID".equals(action)) {
            supprimerParId();
        }
    }

    /**
     * Affichage de l'écran de saisie d'un Profildsi.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerPRINCIPAL() throws Exception {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        ecranLogique = ECRAN_PRINCIPAL;
        if (StringUtils.isNotBlank(profildsi.getCode())) {
            infoBean.set("CODE", profildsi.getCode());
        } else {
            infoBean.set("CODE", String.valueOf(System.currentTimeMillis()));
        }
        infoBean.set("ID_PROFIL", profildsi.getIdProfildsi());
        infoBean.set("LIBELLE", profildsi.getLibelle());
        final String codeRubrique = profildsi.getCodeRubriqueAccueil();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
        String label = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
        if (rubriqueBean != null) {
            label = rubriqueBean.getIntitule();
        }
        infoBean.set("LIBELLE_CODE_RUBRIQUE_ACCUEIL", label);
        infoBean.set("CODE_RUBRIQUE_ACCUEIL", profildsi.getCodeRubriqueAccueil());
        // JSS 20050510 : profils dynamiques
        final String sTmp = StringUtils.join(profildsi.getGroupes(), ";");
        infoBean.set("GROUPES", sTmp);
        infoBean.set("LIBELLE_GROUPES", serviceGroupeDsi.getIntitules(sTmp));
        if (StringUtils.isNotBlank(profildsi.getLibelle())) {
            infoBean.setTitreEcran(profildsi.getLibelle());
        }
    }

    /**
     * Affichage de l'écran des critères de recherche d'un Profildsi.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerRECHERCHE() throws Exception {
        ecranLogique = ECRAN_RECHERCHE;
        infoBean.set("ID_PROFILDSI", profildsi.getIdProfildsi());
        infoBean.set("LISTE_PROFILDSI", serviceProfildsi.getAll());
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        try {
            ecranLogique = infoBean.getEcranLogique();
            action = infoBean.getActionUtilisateur();
            final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
            if (!ComposantProfil.isAutoriseParActionProcessus(autorisations, null)) {
                if (autorisations == null) {
                    infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
                    infoBean.setEcranLogique("LOGIN");
                } else {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
                }
            } else {
                if (etat == DEBUT) {
                    initialisation();
                } else if (ecranLogique.equals(ECRAN_RECHERCHE)) {
                    traiterRECHERCHE();
                } else if (ecranLogique.equals(ECRAN_LISTE)) {
                    traiterLISTE();
                } else if (ecranLogique.equals(ECRAN_PRINCIPAL)) {
                    traiterPRINCIPAL();
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            }
        } catch (final Exception e) {
            LOG.error("erreur de traitement sur le processus", e);
            infoBean.addMessageErreur(e.toString());
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Traitement de l'écran de sélection d'un Profildsi.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterLISTE() throws Exception {
        if (action.equals(InfoBean.ACTION_MODIFIER)) {
            modifierParId();
        }
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            final int indice = Integer.parseInt((String) infoBean.get("LISTE_INDICE"));
            profildsi = serviceProfildsi.getById(listeIdentifiant[indice]);
            remplirDonneesRecherche();
            etat = FIN;
        }
        if (action.equals(InfoBean.ACTION_SUPPRIMER)) {
            final int indice = Integer.parseInt((String) infoBean.get("LISTE_INDICE"));
            serviceProfildsi.delete(listeIdentifiant[indice]);
            etat = FIN;
        }
        if (action.equals(InfoBean.ACTION_ANNULER)) {
            infoBean.set("ID_PROFILDSI", null);
            etat = FIN;
        }
    }

    private void modifierParId() throws Exception {
        mode = MODE_MODIFICATION;
        final String idProfil = infoBean.getString("ID_PROFIL");
        profildsi = serviceProfildsi.getById(Long.valueOf(idProfil));
        preparerPRINCIPAL();
    }

    /**
     * Gestion de l'écran de saisie d'un Profildsi.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterPRINCIPAL() throws Exception {
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            profildsi.setCode((String) infoBean.get("CODE"));
            profildsi.setLibelle((String) infoBean.get("LIBELLE"));
            profildsi.setCodeRubriqueAccueil((String) infoBean.get("CODE_RUBRIQUE_ACCUEIL"));
            profildsi.setGroupes(Chaine.getVecteurPointsVirgules(infoBean.getString("GROUPES")));
            if (mode == MODE_AJOUT) {
                if (serviceProfildsi.existCode(profildsi.getCode())) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "PROFIL_DSI.ERREUR.CODE_EXISTANT"));
                }
                serviceProfildsi.save(profildsi);
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_CREATION_PROFILDSI"), profildsi.getLibelle());
                infoBean.addMessageConfirmation(confirmation);
            }
            if (mode == MODE_MODIFICATION) {
                serviceProfildsi.save(profildsi);
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_MODIFICATION_PROFILDSI"), profildsi.getLibelle());
                infoBean.addMessageConfirmation(confirmation);
            }
            etat = FIN;
        } else if (action.equals(InfoBean.ACTION_SUPPRIMER)) {
            supprimerParId();
        }
    }

    private void supprimerParId() {
        try {
            final Long idProfil = Long.parseLong((String) infoBean.get("ID_PROFIL"));
            profildsi = serviceProfildsi.getById(idProfil);
            if (profildsi != null) {
                 serviceProfildsi.delete(idProfil);
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_PROFILDSI"), profildsi.getLibelle());
                infoBean.addMessageConfirmation(confirmation);
            }
        } catch (final NumberFormatException e) {
            LOG.debug("unable to parse the given value in Long", e);
        }
        etat = FIN;
    }

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterRECHERCHE() throws Exception {
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            final String idProfil = infoBean.get("ID_PROFILDSI", String.class);
            if (StringUtils.isBlank(idProfil) || !StringUtils.isNumeric(idProfil)) {
                throw new ErreurApplicative("Aucun enregistrement sélectionné");
            }
            profildsi = serviceProfildsi.getById(Long.valueOf(infoBean.getString("ID_PROFILDSI")));
            if (profildsi == null) {
                throw new ErreurApplicative("Aucun enregistrement sélectionné");
            }
            if (mode == MODE_RECHERCHE) {
                remplirDonneesRecherche();
                etat = FIN;
            }
            if (mode == MODE_MODIFICATION) {
                preparerPRINCIPAL();
            }
        }
        if (action.equals(InfoBean.ACTION_ANNULER)) {
            if (mode == MODE_RECHERCHE) {
                infoBean.set("ID_PROFILDSI", null);
                etat = FIN;
            }
        }
    }

    /**
     * Préparation des données à renvoyer pour une rechercher Profildsi.
     *
     * @throws Exception
     *             the exception
     */
    private void remplirDonneesRecherche() throws Exception {
        infoBean.set("ID_PROFILDSI", profildsi.getIdProfildsi());
        infoBean.set("CODE", profildsi.getCode());
        infoBean.set("LIBELLE", profildsi.getLibelle());
        infoBean.set("CODE_RUBRIQUE_ACCUEIL", profildsi.getCodeRubriqueAccueil());
    }
}
