package com.univ.objetspartages.processus;

import java.util.Date;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PageLibre;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.FicheUnivMgr;
// TODO: Auto-generated Javadoc

/**
 * processus saisie PageLibre.
 */
public class TraitementPageLibre extends SaisieFiche {

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    /** The Constant ECRAN_CONFIRMATION. */
    private static final String ECRAN_CONFIRMATION = "CONFIRMATION";

    private final ServiceMetatag serviceMetatag;

    /** The page libre. */
    PageLibre pageLibre = null;

    /**
     * processus saisie PageLibre.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public TraitementPageLibre(final InfoBean ciu) {
        super(ciu);
        serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
    }

    /**
     * Affichage de la liste des PageLibres.
     *
     */
    @Override
    protected void preparerLISTE() {}

    /**
     * Affichage de l'écran de saisie d'un PageLibre.
     *
     */
    @Override
    protected void preparerPRINCIPAL() {
        infoBean.set("LISTE_LANGUES", LangueUtil.getListeLangues());
        ecranLogique = ECRAN_PRINCIPAL;
    }
    /**
     * Abandon de l'activité: on libère les ressources.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */

    /**
     * Affichage de l'écran des critères de recherche d'un PageLibre.
     *
     */
    @Override
    protected void preparerRECHERCHE() {}

    /**
     * Point d'entrée du processus
     */
    @Override
    public boolean traiterAction() throws Exception {
        infoBean.set("CODE_OBJET", "0016");
        etat = EN_COURS;
        pageLibre = new PageLibre();
        pageLibre.init();
        traiterActionParDefaut(pageLibre);
        if (ecranLogique.equals(ECRAN_CONFIRMATION)) {
            traiterCONFIRMATION();
        }
        //placer l'état dans le composant d'infoBean
        infoBean.setEcranLogique(ecranLogique);
        infoBean.set("NOM_ONGLET", "pagelibre");
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     * @throws Exception
     *             the exception
     */
    protected void traiterLISTE() throws Exception {}

    /**
     * Gestion de l'écran de saisie d'un PageLibre.
     *
     * @throws Exception
     *             the exception
     */
    @Override
    protected void traiterPRINCIPAL() throws Exception {
        if (action.equals(InfoBean.ACTION_ANNULER)) {
            ecranLogique = "FIN_TOOLBOX";
        } else if (action.equals(InfoBean.ACTION_ENREGISTRER)) {
            pageLibre.init();
            /* Données saisissables uniquement en création */
            pageLibre.setLangue((String) infoBean.get("LANGUE"));
            /***********************************************/
            /* Partie contenu                              */
            /***********************************************/
            pageLibre.setTitre((String) infoBean.get("TITRE"));
            pageLibre.setCodeRubrique((String) infoBean.get("CODE_RUBRIQUE"));
            pageLibre.setCodeRedacteur((String) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE));
            // RP20070904 ajout de la structure de l'utilisateur
            final SessionUtilisateur sessionUtilisateur = getGp().getSessionUtilisateur();
            final AutorisationBean autorisations = (AutorisationBean) sessionUtilisateur.getInfos().get(SessionUtilisateur.AUTORISATIONS);
            final String structureUtilisateur = autorisations.getCodeStructure();
            if (structureUtilisateur.length() > 0) {
                // On regarde si l'autorisation est compatible avec la rubrique
                if (autorisations.possedePermission(new PermissionBean("FICHE", ReferentielObjets.getCodeObjetParClasse(pageLibre.getClass().getName()), "C"), new Perimetre(structureUtilisateur, pageLibre.getCodeRubrique(), "*", "*", ""))) {
                    pageLibre.setCodeRattachement(structureUtilisateur);
                }
            }
            /*************************************/
            /* Appel traitement général          */
            /*************************************/
            pageLibre.setEtatObjet("0001");
            final Date date = new Date(System.currentTimeMillis());
            pageLibre.setDateModification(date);
            FicheUnivMgr.enregistrer(true, pageLibre); // document.enregistrer(true);
            // Lecture métatag
            final MetatagBean meta = MetatagUtils.creerMeta(pageLibre);
            serviceMetatag.addHistory(meta, MetatagUtils.HISTORIQUE_CREATION, (String) this.getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE), pageLibre.getEtatObjet());
            serviceMetatag.save(meta);
            infoBean.set("CODE", pageLibre.getCode());
            preparerCONFIRMATION();
        }
    }

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     */
    @Override
    protected void traiterRECHERCHE() {}

    /**
     * Preparer confirmation.
     *
     */
    private void preparerCONFIRMATION() {
        ecranLogique = ECRAN_CONFIRMATION;
    }

    /**
     * Traiter confirmation.
     *
     */
    protected void traiterCONFIRMATION() {
        if (action.equals(InfoBean.ACTION_ANNULER)) {
            ecranLogique = "FIN_TOOLBOX";
        }
    }
}
