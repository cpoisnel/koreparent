package com.univ.objetspartages.processus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantGroupe;
import com.univ.datagrid.processus.GroupeDSIDatagrid;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PageLibre;
import com.univ.objetspartages.om.RequeteGroupeDynamique;
import com.univ.objetspartages.om.RequeteGroupeDynamiqueSynchronisable;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.objetspartages.util.CritereRechercheUtil;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.utils.Chaine;
import com.univ.utils.RechercheFicheHelper;
import com.univ.utils.RequeteGroupeUtil;

/**
 * processus saisie Groupedsi.
 */
public class SaisieGroupedsi extends ProcessusBean {

    private static final Logger LOG = LoggerFactory.getLogger(SaisieGroupedsi.class);

    /** The Constant MODE_AJOUT. */
    private static final int MODE_AJOUT = 0;

    /** The Constant MODE_MODIFICATION. */
    private static final int MODE_MODIFICATION = 1;

    /** The Constant MODE_SUPPRESSION. */
    private static final int MODE_SUPPRESSION = 2;

    /** The Constant MODE_RECHERCHE. */
    private static final int MODE_RECHERCHE = 3;

    /** The Constant ECRAN_RECHERCHE. */
    private static final String ECRAN_RECHERCHE = "RECHERCHE";

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_SYNCHRONISER = "SYNCHRONISER";

    /** The Constant ECRAN_LISTE. */
    private static final String ECRAN_LISTE = "LISTE";

    /** The Constant ECRAN_LISTE_PREVISUALISE. */
    private static final String ECRAN_LISTE_PREVISUALISE = "LISTE_PREVISUALISER";

    private final ServiceUser serviceUser;

    private final ServiceGroupeDsi serviceGroupeDsi;

    private final ServiceGroupeUtilisateur serviceGroupeUtilisateur;

    /** The groupedsi. */
    GroupeDsiBean groupedsi;

    /** The liste identifiant. */
    Long listeIdentifiant[];

    /** The mode. */
    private int mode = -1;

    /**
     * processus saisie Groupedsi.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieGroupedsi(final InfoBean ciu) {
        super(ciu);
        serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Initialisation du processus.
     *
     * @throws Exception
     *             the exception
     */
    private void initialisation() throws Exception {
        // initialisation objets métiers
        groupedsi = new GroupeDsiBean();
        etat = EN_COURS;
        if ("AJOUTER".equals(action)) {
            mode = MODE_AJOUT;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
            preparerPRINCIPAL();
        } else if ("MODIFIER".equals(action)) {
            mode = MODE_MODIFICATION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            preparerRECHERCHE();
        } else if ("SUPPRIMER".equals(action)) {
            mode = MODE_SUPPRESSION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_SUPPRESSION);
            preparerRECHERCHE();
        } else if ("RECHERCHER".equals(action)) {
            mode = MODE_RECHERCHE;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
            preparerRECHERCHE();
        } else if ("MODIFIERPARID".equals(action)) {
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            modifierParId();
        } else if ("SUPPRIMERPARID".equals(action)) {
            supprimerParId();
        } else if ("SUPPRIMERDUGROUPE".equals(action)) {
            mode = MODE_MODIFICATION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            supprimerDuGroupe();
        } else if ("ACCUEIL".equals(action)) {
            preparerLISTE();
            ecranLogique = "ACCUEIL";
        }
    }

    private void supprimerDuGroupe() throws Exception {
        final String id = infoBean.getString("UTILISATEUR");
        final String groupeCode = infoBean.getString("GROUPE_DSI");
        if (StringUtils.isNotBlank(id) && StringUtils.isNumeric(id)) {
            final Long longId = Long.parseLong(id);
            final UtilisateurBean utilisateur = serviceUser.getById(longId);
            if (utilisateur != null) {
                serviceGroupeUtilisateur.deleteByUserAndGroup(utilisateur.getCode(), groupeCode);
            }
        }
        final GroupeDsiBean group = serviceGroupeDsi.getByCode(groupeCode);
        infoBean.set("ID_GROUPE", String.valueOf(group != null ? group.getId() : 0));
        modifierParId();
    }

    /**
     * Affichage de la liste des Groupedsis.
     *
     */
    private void preparerLISTE() {
        insererRechercheDansInfoBean(infoBean);
        ecranLogique = "LISTE";
    }

    /**
     * Affichage de l'écran de saisie d'un Groupedsi.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerPRINCIPAL() throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.set("CODE", groupedsi.getCode());
        infoBean.set("ID_GROUPE", String.valueOf(groupedsi.getIdGroupedsi()));
        infoBean.set("LIBELLE", groupedsi.getLibelle());
        infoBean.set("TYPE", groupedsi.getType());
        infoBean.set("LISTE_TYPES", LabelUtils.getLabelCombo("11", LangueUtil.getDefaultLocale()));
        // Le choix de 'type structure' ne peut etre fait qu'en import (pas en saisie)
        if (!"STRUCT".equals(groupedsi.getType())) {
            ((Map<?, ?>) infoBean.get("LISTE_TYPES")).remove("STRUCT");
        }
        infoBean.set("CODE_STRUCTURE", groupedsi.getCodeStructure());
        infoBean.set("LIBELLE_CODE_STRUCTURE", serviceStructure.getDisplayableLabel(groupedsi.getCodeStructure(), ""));
        String langueFiche = "0";
        String codeFiche = groupedsi.getCodePageTete();
        final int indiceLangue = codeFiche.indexOf(",LANGUE=");
        if (indiceLangue != -1) {
            langueFiche = codeFiche.substring(indiceLangue + 8);
            codeFiche = codeFiche.substring(0, indiceLangue);
        }
        infoBean.set("LIBELLE_CODE_PAGE_TETE", PageLibre.getLibelleAffichable(this, codeFiche, langueFiche));
        infoBean.set("CODE_PAGE_TETE", groupedsi.getCodePageTete());
        // Code & type non saisissable pour les structures
        infoBean.set("SAISIE_CODE", "1");
        infoBean.set("SAISIE_TYPE", "1");
        final String typeGroupeStructure = "STRUCT";
        if (groupedsi.getType().equals(typeGroupeStructure)) {
            infoBean.set("SAISIE_CODE", "0");
            infoBean.set("SAISIE_TYPE", "0");
        }
        // Code non modifiable
        if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {
            infoBean.set("SAISIE_CODE", "0");
        }
        // Structure non saisissable pour les structures
        infoBean.set("SAISIE_STRUCTURE", "1");
        if (groupedsi.getType().equals(typeGroupeStructure)) {
            infoBean.set("SAISIE_STRUCTURE", "0");
        }
        //JSS 20040409 : gestion des roles, hiérarchie
        infoBean.set("ROLES", groupedsi.getRoles());
        infoBean.set("CODE_GROUPE_PERE", groupedsi.getCodeGroupePere());
        infoBean.set("LIBELLE_CODE_GROUPE_PERE", serviceGroupeDsi.getIntitule(groupedsi.getCodeGroupePere()));
        // gestion des groupes dynamiques
        infoBean.set("REQUETE_GROUPE", groupedsi.getRequeteGroupe()); // type de groupedyn
        infoBean.set("LISTE_REQUETES_GROUPES", serviceGroupeDsi.getGroupRequestList()); // liste des types de groupedyn
        //MBO : gestion des groupes dynamiques de facon un peu plus generique
        final Map<String, String> groups = serviceGroupeDsi.getListeRequetesGroupesPourAffichage();
        for (final String key : groups.keySet()) {
            final RequeteGroupeDynamique dynGroupe = RequeteGroupeUtil.instancierRequete(key);
            if (dynGroupe != null) {
                dynGroupe.preparerPRINCIPAL(infoBean, groupedsi);
            }
        }
        // JB 20051129 : cache groupe dynamique
        infoBean.set("GESTION_CACHE", groupedsi.getGestionCache());
        infoBean.set("DELAI_EXPIRATION_CACHE", groupedsi.getDelaiExpirationCache());
        ControleurAffectationRole.preparerPRINCIPAL(infoBean, this);
        if (StringUtils.isNotBlank(groupedsi.getLibelle())) {
            infoBean.setTitreEcran(groupedsi.getLibelle());
        }
    }

    /**
     * Affichage de l'écran des critères de recherche d'un Groupedsi.
     *
     */
    private void preparerRECHERCHE() {
        ecranLogique = ECRAN_RECHERCHE;
        infoBean.set("LISTE_TYPES", LabelUtils.getLabelCombo("11", LangueUtil.getDefaultLocale()));
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     */
    @Override
    public boolean traiterAction() {
        try {
            ecranLogique = infoBean.getEcranLogique();
            action = infoBean.getActionUtilisateur();
            final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
            if (!ComposantGroupe.isAutoriseParActionProcessus(autorisations, null)) {
                if (autorisations == null) {
                    infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
                    infoBean.setEcranLogique("LOGIN");
                } else {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
                }
            } else {
                if (etat == DEBUT) {
                    initialisation();
                } else if (ecranLogique.equals(ECRAN_RECHERCHE)) {
                    traiterRECHERCHE();
                } else if (ecranLogique.equals(ECRAN_LISTE)) {
                    traiterLISTE();
                } else if (ecranLogique.equals(ECRAN_PRINCIPAL)) {
                    traiterPRINCIPAL();
                } else if (ecranLogique.equals(ECRAN_LISTE_PREVISUALISE)) {
                    traiterLISTE_PREVISUALISATION();
                } else if (ecranLogique.equals(ECRAN_SYNCHRONISER)) {
                    traiterSynchro();
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            }
        } catch (final Exception e) {
            LOG.error("erreur de traitement sur le processus", e);
            infoBean.addMessageErreur(e.toString());
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    private void traiterSynchro() throws Exception {
        ecranLogique = ECRAN_LISTE;
        final String nomRequete = infoBean.getString("REQUETE_GROUPE");
        if (nomRequete != null) {
            // récupération de l'objet correspondant à la requête pour
            // effectuer la synchronisation.
            final RequeteGroupeDynamique requeteGroupeDyn = RequeteGroupeUtil.instancierRequete(nomRequete);
            // si l'objet existe on effectue l'execution de la requete
            if (requeteGroupeDyn != null && requeteGroupeDyn instanceof RequeteGroupeDynamiqueSynchronisable) {
                ((RequeteGroupeDynamiqueSynchronisable) requeteGroupeDyn).traiterSYNCHRONISATION(infoBean);
            }
        }
    }

    /**
     * Traitement de l'écran de sélection d'un Groupedsi.
     *
     */
    private void traiterLISTE_PREVISUALISATION() {
        // placer l'état dans le composant d'infoBean
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.setEcranLogique(ecranLogique);
    }

    private void modifierParId() throws Exception {
        final String indice = infoBean.getString("ID_GROUPE");
        if (StringUtils.isBlank(indice) || !StringUtils.isNumeric(indice)) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
        }
        groupedsi = serviceGroupeDsi.getById(Long.valueOf(indice));
        if (groupedsi == null) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_GROUPE_INEXISTANT"));
        }
        mode = MODE_MODIFICATION;
        preparerPRINCIPAL();
    }

    private void supprimerParId() throws ErreurApplicative {
        final String indice = infoBean.getString("ID_GROUPE");
        if (StringUtils.isBlank(indice) || !StringUtils.isNumeric(indice)) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
        }
        groupedsi = serviceGroupeDsi.getById(Long.valueOf(indice));
        if (groupedsi == null) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_GROUPE_INEXISTANT"));
        }
        serviceGroupeDsi.delete(groupedsi.getIdGroupedsi());
        serviceGroupeUtilisateur.deleteByGroup(groupedsi.getCode());
        etat = FIN;
    }

    /**
     * Traiter liste.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterLISTE() throws Exception {
        if (InfoBean.ACTION_MODIFIER.equals(action)) {
            modifierParId();
        }
        if (InfoBean.ACTION_VALIDER.equals(action)) {
            final String indice = (String) infoBean.get("LISTE_INDICE");
            if (StringUtils.isBlank(indice) || !StringUtils.isNumeric(indice)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            groupedsi = serviceGroupeDsi.getById(listeIdentifiant[Integer.valueOf(indice)]);
            if (groupedsi == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_GROUPE_INEXISTANT"));
            }
            remplirDonneesRecherche();
            etat = FIN;
        }
        if (InfoBean.ACTION_SUPPRIMER.equals(action)) {
            supprimerParId();
        }
        if (InfoBean.ACTION_ANNULER.equals(action)) {
            infoBean.set("ID_GROUPEDSI", null);
            etat = FIN;
        }
    }

    /**
     * Gestion de l'écran de saisie d'un Groupedsi.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterPRINCIPAL() throws Exception {
        // Gestion des roles
        ControleurAffectationRole.traiterPRINCIPAL(infoBean, this);
        // Groupes dynamiques
        RequeteGroupeDynamique dynGroupe = null;
        if (StringUtils.isNotEmpty(infoBean.getString("REQUETE_GROUPE")) && !"0000".equals(infoBean.getString("REQUETE_GROUPE"))) {
            for (final String key : serviceGroupeDsi.getListeRequetesGroupesPourAffichage().keySet()) {
                if (key.equals(infoBean.getString("REQUETE_GROUPE"))) {
                    dynGroupe = RequeteGroupeUtil.instancierRequete(key);
                }
            }
        }
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            // action sur le groupe (renseignement de ce dernier dans le cas d'une action VALIDE)
            if (dynGroupe != null) {
                dynGroupe.traiterPRINCIPAL(infoBean, groupedsi);
            }
            if ("1".equals(infoBean.getString("SAISIE_CODE"))) {
                groupedsi.setCode((String) infoBean.get("CODE"));
            }
            if ("1".equals(infoBean.getString("SAISIE_TYPE"))) {
                groupedsi.setType((String) infoBean.get("TYPE"));
            }
            groupedsi.setLibelle((String) infoBean.get("LIBELLE"));
            if ("1".equals(infoBean.getString("SAISIE_STRUCTURE"))) {
                groupedsi.setCodeStructure((String) infoBean.get("CODE_STRUCTURE"));
            }
            groupedsi.setCodePageTete((String) infoBean.get("CODE_PAGE_TETE"));
            // gestion des roles, hierachie et espace collaboratif
            groupedsi.setRoles(infoBean.getString("ROLES"));
            // controle boucle
            if (infoBean.getString("CODE_GROUPE_PERE").equals(infoBean.get("CODE"))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "GROUPE_DSI.ERREUR.GROUPE_PARENT_LUI_MEME"));
            }
            // controle boucle sur groupes fils
            final GroupeDsiBean group = serviceGroupeDsi.getByCode(infoBean.get("CODE", String.class));
            final GroupeDsiBean parentGroup = serviceGroupeDsi.getByCode(infoBean.get("CODE_GROUPE_PERE", String.class));
            if (serviceGroupeDsi.contains(group, parentGroup)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "GROUPE_DSI.ERREUR.GROUPE_PARENT_FILS"));
            }
            // gestion de la hierarchie
            groupedsi.setCodeGroupePere(infoBean.getString("CODE_GROUPE_PERE"));
            // gestion des groupes dynamiques
            if (StringUtils.isNotEmpty(infoBean.getString("REQUETE_GROUPE")) && !"0000".equals(infoBean.getString("REQUETE_GROUPE"))) {
                // JB 20051129 : gestion cache groupe dynamique
                final String cacheDelay = PropertyHelper.getCoreProperty("groupedsi.groupe_dyn.gestion_cache.delay");
                final Long delai = StringUtils.isNumeric(cacheDelay) ? Long.parseLong(cacheDelay) : 0L;
                groupedsi.setGestionCache("1".equals(PropertyHelper.getCoreProperty("groupedsi.groupe_dyn.gestion_cache.active")) ? "1" : "0");
                groupedsi.setDelaiExpirationCache(delai);
                groupedsi.setRequeteGroupe(infoBean.getString("REQUETE_GROUPE"));
            } else {
                groupedsi.setRequeteGroupe("");
            }
            if (mode == MODE_AJOUT) {
                // controle saisie du code
                Chaine.controlerCodeMetier((String) infoBean.get("CODE"));
                // controle existence du code
                final GroupeDsiBean groupeDsiBean = serviceGroupeDsi.getByCode(groupedsi.getCode());
                if (groupeDsiBean != null) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "GROUPE_DSI.ERREUR.CODE_EXISTANT"));
                }
                serviceGroupeDsi.save(groupedsi);
                final String confirmation = String.format(MessageHelper.getCoreMessage(this.getLocale(), "CONFIRMATION_CREATION_GROUPEDSI"), groupedsi.getLibelle());
                infoBean.addMessageConfirmation(confirmation);
            } else if (mode == MODE_MODIFICATION) {
                serviceGroupeDsi.save(groupedsi);
                final String confirmation = String.format(MessageHelper.getCoreMessage(this.getLocale(), "CONFIRMATION_MODIFICATION_GROUPEDSI"), groupedsi.getLibelle());
                infoBean.addMessageConfirmation(confirmation);
            }
            etat = FIN;
        } else if (InfoBean.ACTION_SUPPRIMER.equals(action)) {
            if ("1".equals(PropertyHelper.getCoreProperty("saisie_limitation.groupes.suppression"))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "GROUPE_DSI.ERREUR.SUPPRESSION_DESACTIVEE"));
            }
            final String indice= (String) infoBean.get("ID_GROUPE");
            if (StringUtils.isBlank(indice) || !StringUtils.isNumeric(indice)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
            groupedsi = serviceGroupeDsi.getById(Long.valueOf(indice));
            if (groupedsi == null) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_GROUPE_INEXISTANT"));
            }
            serviceGroupeDsi.delete(groupedsi.getIdGroupedsi());
            serviceGroupeUtilisateur.deleteByGroup(groupedsi.getCode());
            final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_GROUPEDSI"), groupedsi.getLibelle());
            infoBean.addMessageConfirmation(confirmation);
            etat = FIN;
        }
        // action spécifique à une groupe dynamique LDAP
        else if (InfoBean.ACTION_DETAIL.equals(infoBean.getActionUtilisateur()) && dynGroupe != null) {
            // Si c'est un groupe LDAP le paramètre n'est pas le code du groupe mais la requete LDAP #req#
            final String requete = infoBean.getString("REQUETE_LDAP_" + infoBean.getString("REQUETE_GROUPE"));
            final Vector<String> vListeUtilisateur = dynGroupe.getVecteurUtilisateursCache("#req#" + requete, null);
            //afichage de la liste
            if (vListeUtilisateur.size() > 0) {
                preparerLISTE_PREVISUALISATION(vListeUtilisateur, infoBean);
            } else {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "GROUPE_DSI.ERREUR.PAS_UTILISATEUR"));
            }
        }
    }

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterRECHERCHE() throws Exception {
        if (InfoBean.ACTION_VALIDER.equals(action)) {
            preparerLISTE();
        }
        if (InfoBean.ACTION_ANNULER.equals(action) && mode == MODE_RECHERCHE) {
            infoBean.set("ID_GROUPEDSI", null);
            etat = FIN;
        }
    }

    private void insererRechercheDansInfoBean(final InfoBean infoBean) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final List<CritereRecherche> criteres = new ArrayList<>();
        CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVide(infoBean, "CODE"));
        CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereChaineAvecLibelle(infoBean, "TYPE", "11"));
        CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVideFormater(infoBean, "LIBELLE"));
        final String codeStructure = infoBean.getString("CODE_STRUCTURE");
        if (StringUtils.isNotBlank(infoBean.getString("CODE_STRUCTURE"))) {
            String libelleStructure = serviceStructure.getDisplayableLabel(codeStructure, LangueUtil.getLangueLocale(this.getLocale()));
            criteres.add(new CritereRecherche("CODE_STRUCTURE", codeStructure, libelleStructure));
            criteres.add(new CritereRecherche("LIBELLE_CODE_STRUCTURE", libelleStructure));
        }
        criteres.add(new CritereRecherche(DatagridUtils.PARAM_BEAN_DATAGRID, GroupeDSIDatagrid.ID_BEAN));
        infoBean.set(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES, criteres);
    }

    /**
     * Preparer list e_ previsualisation.
     *
     * @param liste
     *            liste des utilisateurs à ajouter dans l'infoBean.
     * @param infoBean
     *            L'infoBean ou sera inséré les informations sur les utilisateurs.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerLISTE_PREVISUALISATION(final Collection<String> liste, final InfoBean infoBean) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        int nbElem = 0;
        if (CollectionUtils.isNotEmpty(liste)) {
            final List<UtilisateurBean> users = serviceUser.getByCodes(liste.toArray(new String[liste.size()]));
            for (final UtilisateurBean currentUser : users) {
                infoBean.set("CODE#" + nbElem, currentUser.getCode());
                infoBean.set("NOM#" + nbElem, currentUser.getNom());
                infoBean.set("PRENOM#" + nbElem, currentUser.getPrenom());
                infoBean.set("LIBELLE_STRUCTURE#" + nbElem, serviceStructure.getDisplayableLabel(currentUser.getCodeRattachement(), "0"));
                nbElem++;
            }
        }
        infoBean.set("LISTE_NB_ITEMS", nbElem);
        //placer l'état dans le composant d'infoBean
        infoBean.setEcranLogique(SaisieGroupedsi.ECRAN_LISTE_PREVISUALISE);
        ecranLogique = SaisieGroupedsi.ECRAN_LISTE_PREVISUALISE;
    }

    /**
     * Préparation des données à renvoyer pour une rechercher Groupedsi.
     *
     */
    private void remplirDonneesRecherche() {
        infoBean.set("ID_GROUPEDSI", groupedsi.getIdGroupedsi());
        infoBean.set("CODE", groupedsi.getCode());
        infoBean.set("LIBELLE", groupedsi.getLibelle());
        infoBean.set("TYPE", groupedsi.getType());
        infoBean.set("CODE_STRUCTURE", groupedsi.getCodeStructure());
    }
}
