/*
 * Created on 2 déc. 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.objetspartages.processus;
// TODO: Auto-generated Javadoc

/**
 * The Class InfoImplementation.
 *
 * @author Administrateur
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 */
public class InfoImplementation {

    /** The code. */
    private String code = "";

    /** The libelle. */
    private String libelle = "";

    /** The nom classe. */
    private String nomClasse = "";

    /**
     * Gets the code.
     *
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            The code to set.
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the libelle.
     *
     * @return Returns the libelle.
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets the libelle.
     *
     * @param libelle
     *            The libelle to set.
     */
    public void setLibelle(final String libelle) {
        this.libelle = libelle;
    }

    /**
     * Gets the nom classe.
     *
     * @return Returns the nomClasse.
     */
    public String getNomClasse() {
        return nomClasse;
    }

    /**
     * Sets the nom classe.
     *
     * @param nomClasse
     *            The nomClasse to set.
     */
    public void setNomClasse(final String nomClasse) {
        this.nomClasse = nomClasse;
    }
}
