package com.univ.objetspartages.processus;

import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.RequeteGroupeDynamique;
import com.univ.objetspartages.om.RequeteGroupeDynamiqueSynchronisable;
import com.univ.utils.RequeteGroupeUtil;
// TODO: Auto-generated Javadoc

/**
 * Processus de synchronisation de groupe dynamique. Ce processus est appelé via l'interface de paramétrage K-Portal et permet de synchroniser les groupes dynamiques
 * synchronisables ({@link RequeteGroupeDynamiqueSynchronisable}). <br/>
 * <br/>
 * On ne peut synchroniser des groupes que si on est webmaster
 *
 * @author Julien ROZO et adapté par PCO le 02-02-2009
 */
public class SaisieSynchronisationGroupeDynamique extends ProcessusBean {

    /** The Constant ECRAN_CONFIRMATION. */
    private static final String ECRAN_CONFIRMATION = "CONFIRMATION";

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(SaisieSynchronisationGroupeDynamique.class);

    /** The autorisations. */
    private AutorisationBean autorisations = null;

    /**
     * Processus saisie Synchronisation des groupes dynamiques d'espace.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieSynchronisationGroupeDynamique(final InfoBean ciu) {
        super(ciu);
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else if (autorisations.isWebMaster()) {
            try {
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                if ("SYNCHRONISER".equals(action)) {
                    traiterSYNCHRONISATION();
                }
                // placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOG.error("erreur de traitement sur le processus", e);
                infoBean.addMessageErreur(e.toString());
            }
        } else {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Gestion de l'écran de saisie synchronisation des groupes dynamiques. Pour fonctionner il faut les paramétres suivants :
     * <ul>
     * <li>REQUETE_GROUPE : qui est le nom de la requete de groupe dynamique</li>
     * </ul>
     * En retour sera affiché le resultat de la synchronisation.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterSYNCHRONISATION() throws Exception {
        ecranLogique = ECRAN_CONFIRMATION;
        final String nomRequete = infoBean.getString("REQUETE_GROUPE");
        if (nomRequete != null) {
            // récupération de l'objet correspondant à la requête pour
            // effectuer la synchronisation.
            final RequeteGroupeDynamique requeteGroupeDyn = RequeteGroupeUtil.instancierRequete(this, nomRequete);
            // si l'objet existe on effectue l'execution de la requete
            if (requeteGroupeDyn != null && requeteGroupeDyn instanceof RequeteGroupeDynamiqueSynchronisable) {
                requeteGroupeDyn.setCtx(this);
                ((RequeteGroupeDynamiqueSynchronisable) requeteGroupeDyn).traiterSYNCHRONISATION(infoBean);
            }
        }
    }
}
