package com.univ.objetspartages.processus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.core.SynchroniseurUtilisateur;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.ldap.ISynchroLdapUtilisateur;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.security.MySQLHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantUtilisateur;
import com.univ.datagrid.processus.UtilisateurDatagrid;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AnnuaireModele;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;
import com.univ.objetspartages.services.ServiceLabel;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.objetspartages.util.CritereRechercheUtil;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.Chaine;
import com.univ.utils.RechercheFicheHelper;

/**
 * Processus de saisie d'un utilisateur.
 */
public class SaisieUtilisateur extends ProcessusBean {

    /** The Constant ECRAN_RECHERCHE. */
    private static final String ECRAN_RECHERCHE = "RECHERCHE";

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    /** The Constant ECRAN_LISTE. */
    private static final String ECRAN_LISTE = "LISTE";

    private static final Logger LOG = LoggerFactory.getLogger(SaisieUtilisateur.class);

    /** The liste identifiant. */
    private final Long[] listeIdentifiant = null;

    private final ServiceUser serviceUser;

    private final ServiceGroupeUtilisateur serviceGroupeUtilisateur;

    private final ServiceLabel serviceLabel;

    private final ServiceMetatag serviceMetatag;

    /**
     * processus saisie Utilisateur.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieUtilisateur(final InfoBean ciu) {
        super(ciu);
        serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        serviceLabel = ServiceManager.getServiceForBean(LabelBean.class);
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    private UtilisateurBean getUtilisateur() {
        final String idString = infoBean.get("ID", String.class);
        final String indiceString = infoBean.get("LISTE_INDICE", String.class);
        if (StringUtils.isNumeric(idString)) {
            final Long id = Long.parseLong(idString);
            return serviceUser.getById(id);
        } else if (StringUtils.isNumeric(indiceString)) {
            final int indice = Integer.parseInt(indiceString);
            return serviceUser.getById(listeIdentifiant[indice]);
        }
        return null;
    }

    /**
     * Affichage de l'écran de saisie d'un Utilisateur.
     *
     * @throws Exception
     *             the exception
     */
    public void preparerPRINCIPAL() throws Exception {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        UtilisateurBean utilisateur = getUtilisateur();
        if (utilisateur == null) {
            utilisateur = new UtilisateurBean();
        }
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.set("CODE", utilisateur.getCode());
        infoBean.set("CODE_LDAP", utilisateur.getCodeLdap());
        infoBean.set("CIVILITE", utilisateur.getCivilite());
        infoBean.set("NOM", utilisateur.getNom());
        infoBean.set("PRENOM", utilisateur.getPrenom());
        infoBean.set("CODE_RATTACHEMENT", utilisateur.getCodeRattachement());
        infoBean.set("LIBELLE_CODE_RATTACHEMENT", serviceStructure.getDisplayableLabel(utilisateur.getCodeRattachement(), ""));
        infoBean.set("ADRESSE_MAIL", utilisateur.getAdresseMail());
        final String formatEnvoiNL = StringUtils.defaultIfBlank(utilisateur.getFormatEnvoi(), "0");
        infoBean.set("FORMAT_ENVOI", formatEnvoiNL);
        infoBean.set("RADIO0", "HTML");
        infoBean.set("RADIO1", "Texte");
        infoBean.set("TYPE_POPULATION", "");
        infoBean.set("LISTE_TYPES_POPULATION", LabelUtils.getLabelCombo("10", LangueUtil.getDefaultLocale()));
        infoBean.set("CORPS", "");
        infoBean.set("DISCIPLINE", "");
        infoBean.set("LISTE_DISCIPLINES", LabelUtils.getLabelCombo("02", LangueUtil.getDefaultLocale()));
        final Collection<String> groupesDsi = serviceGroupeUtilisateur.getAllDatasourceGroupsCodesByUserCode(utilisateur.getCode());
        String sGroupes = StringUtils.EMPTY;
        String libelles = StringUtils.EMPTY;
        for (final String currentCode : groupesDsi) {
            final GroupeDsiBean group = serviceGroupeDsi.getByCode(currentCode);
            if (sGroupes.length() > 0) {
                sGroupes += ";";
                libelles += ";";
            }
            libelles = String.format("%s%s", libelles, group.getLibelle());
            sGroupes = String.format("%s%s%s", sGroupes, StringUtils.isNotBlank(group.getRequeteGroupe()) ? "#AUTO#" : StringUtils.EMPTY, currentCode);
        }
        infoBean.set("GROUPE_DSI", sGroupes);
        infoBean.set("LIBELLE_GROUPE_DSI", libelles);
        final Collection<String> groupesDynamic = serviceGroupeUtilisateur.getAllDynamicGroupsCodesByUserCode(utilisateur.getCode());
        final Collection<GroupeDsiBean> dynamicGroups = new ArrayList<>();
        for (final String currentCode : groupesDynamic) {
            final GroupeDsiBean currentGroup = serviceGroupeDsi.getByCode(currentCode);
            if (currentGroup != null) {
                dynamicGroups.add(currentGroup);
            }
        }
        infoBean.set("GROUPE_DSI_DYN", dynamicGroups);
        infoBean.set("PROFIL_DEFAUT", utilisateur.getProfilDefaut());
        final Map<String, String> profiles = serviceProfildsi.getListeProfilsDSIParGroupes(groupesDsi);
        profiles.putAll(serviceProfildsi.getListeProfilsDSIParGroupes(groupesDynamic));
        infoBean.set("LISTE_PROFILS", profiles);
        /***********************/
        /*    Thématiques      */
        /***********************/
        final Collection<String> vThemesUtilisateurs = serviceUser.getCentresInteret(utilisateur);
        int iTheme = 0;
        /* tri des thèmes par libellés */
        final List<LabelBean> labels = serviceLabel.getByTypeLanguage("04", "0");
        for (final LabelBean currentLabel : labels) {
            /* Pour chaque theme, on regarde s'il est sélectionné */
            String valeurTheme = "0";
            if (vThemesUtilisateurs.contains(currentLabel.getCode())) {
                valeurTheme = "1";
            }
            infoBean.set("CODE_THEME#" + iTheme, currentLabel.getCode());
            infoBean.set("LIBELLE_THEME#" + iTheme, currentLabel.getLibelle());
            infoBean.set("VALEUR_THEME#" + iTheme, valeurTheme);
            iTheme++;
        }
        infoBean.set("THEMES_NB_ITEMS", iTheme);
        infoBean.set("ROLES", utilisateur.getRoles());
        // filtre les groupes dynamiques sur l'arbre
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_TMP_GROUPE_DSI", "1");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_TYPE", "DYN");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_OBJET", "dyn");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_ACTION", "");
        // Si on modifie l'utilisateur et qu'il provient du ldap et que
        // l'authentification kportal est gérée par le ldap, cas ou cleartrust
        // on n'autorise pas la modification du mot de passe
        if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && serviceUser.isAlterationForbiddenBySource(utilisateur)) {
            infoBean.set("MODIFICATION_MOTDEPASSE_AUTORISEE", "0");
        } else {
            infoBean.set("MODIFICATION_MOTDEPASSE_AUTORISEE", "1");
        }
        if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet()) && serviceUser.fromLdap(utilisateur)) {
            infoBean.set("MODIFICATION_AUTORISEE", "0");
        } else {
            infoBean.set("MODIFICATION_AUTORISEE", "1");
        }
        if (StringUtils.isNotBlank(utilisateur.getPrenom()) || StringUtils.isNotBlank(utilisateur.getNom())) {
            infoBean.setTitreEcran(String.format("%s %s", utilisateur.getPrenom(), utilisateur.getNom()));
        }
        ControleurAffectationRole.preparerPRINCIPAL(infoBean, this);
        // appel au traitement specifique (permet de surcharger le processus sans tout reecrire)
    }

    /**
     * Affichage de l'écran des critères de recherche d'un Utilisateur.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerRECHERCHE() throws Exception {
        ecranLogique = ECRAN_RECHERCHE;
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        infoBean.set("LISTE_PROFILS_DSI", serviceProfildsi.getDisplayableProfiles());
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        try {
            ecranLogique = infoBean.getEcranLogique();
            action = infoBean.getActionUtilisateur();
            final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
            String modeAffichage = infoBean.getString("MODE");
            if (modeAffichage == null) {
                modeAffichage = "GESTION";
            }
            infoBean.set("MODE", modeAffichage);
            // retour sur login si utilisateur non connecte
            if (autorisations == null) {
                infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
                ecranLogique = "LOGIN";
            } else if (!ComposantUtilisateur.isAutoriseParActionProcessusEtEcranLogique(autorisations, action, ecranLogique)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            } else if (ecranLogique == null) {
                if ("AJOUTER".equals(action)) {
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
                    preparerPRINCIPAL();
                } else if ("MODIFIER".equals(action)) {
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                    preparerRECHERCHE();
                } else if ("RECHERCHER".equals(action)) {
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
                    preparerRECHERCHE();
                } else if ("LISTE".equals(action)) {
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
                    traiterRECHERCHE();
                } else if ("ACCUEIL".equals(action)) {
                    action = InfoBean.ACTION_VALIDER;
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
                    traiterRECHERCHE();
                } else if ("LISTE_ROLES".equals(action)) {
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_RECHERCHE);
                    infoBean.set("LISTE", "ROLES");
                    preparerRECHERCHE();
                } else if ("MODIFIERPARID".equals(action)) {
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                    final String id = infoBean.getString("ID");
                    if (StringUtils.isNotBlank(id) && StringUtils.isNumeric(id)) {
                        preparerPRINCIPAL();
                    }
                } else if ("SUPPRIMERPARID".equals(action)) {
                    infoBean.setEtatObjet(InfoBean.ETAT_OBJET_SUPPRESSION);
                    final String id = infoBean.getString("ID");
                    if (StringUtils.isNotBlank(id) && StringUtils.isNumeric(id)) {
                        supprimerUtilisateur();
                        etat = FIN;
                    }
                }
            } else {
                if (ECRAN_RECHERCHE.equals(ecranLogique)) {
                    traiterRECHERCHE();
                } else if (ECRAN_LISTE.equals(ecranLogique)) {
                    traiterLISTE();
                } else if (ECRAN_PRINCIPAL.equals(ecranLogique)) {
                    traiterPRINCIPAL();
                }
            }
            // on place l'état dans le composant d'infoBean
            infoBean.setEcranLogique(ecranLogique);
        } catch (final Exception e) {
            LOG.error("erreur dans le traitement du processus", e);
            infoBean.addMessageErreur(e.toString());
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    private void supprimerUtilisateur() throws Exception {
        final UtilisateurBean utilisateurBean = getUtilisateur();
        if (utilisateurBean != null) {
            /*try {
                Espacecollaboratif.supprimerUtilisateur(utilisateurBean.getCode());
            } catch (final Exception e) {
                action = "LISTE";
                traiterRECHERCHE();
                throw new ErreurApplicative(e.toString());
            }*/
            serviceGroupeUtilisateur.deleteByUser(utilisateurBean.getCode());
            serviceUser.delete(utilisateurBean.getId());
            final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_UTILISATEUR"), String.format("%s %s", utilisateurBean.getPrenom(), utilisateurBean.getNom()));
            infoBean.addMessageConfirmation(confirmation);
        }
    }

    /**
     * Traitement de l'écran de sélection d'un Utilisateur.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterLISTE() throws Exception {
        if (InfoBean.ACTION_MODIFIER.equals(action)) {
            preparerPRINCIPAL();
        } else if (InfoBean.ACTION_SUPPRIMER.equals(action)) {
            supprimerUtilisateur();
            etat = FIN;
        } else if ("LISTE".equals(action)) { // refresh
            traiterRECHERCHE();
        } else if (InfoBean.ACTION_ANNULER.equals(action)) {
            infoBean.set("ID_UTILISATEUR", null);
            etat = FIN;
        }
    }

    /**
     * Gestion de l'écran de saisie d'un Utilisateur.
     *
     * @throws Exception
     *             the exception
     */
    public void traiterPRINCIPAL() throws Exception {
        UtilisateurBean utilisateur = getUtilisateur();
        if (utilisateur == null) {
            utilisateur = new UtilisateurBean();
        }
        ControleurAffectationRole.traiterPRINCIPAL(infoBean, this);
        if ("SUPPRIMER".equals(action)) {
            supprimerUtilisateur();
            etat = FIN;
        } else if ("ACTUALISER_PROFIL".equals(action)) {
            final Collection<String> vGroupesDsi = new ArrayList<>();
            final StringTokenizer st = new StringTokenizer(infoBean.getString("GROUPE_DSI"), ";");
            while (st.hasMoreTokens()) {
                final String val = st.nextToken();
                vGroupesDsi.add(val);
            }
            final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
            final Map<String, String> hProfil = serviceProfildsi.getListeProfilsDSIParGroupes(vGroupesDsi);
            if (hProfil.containsKey(utilisateur.getProfilDefaut())) {
                infoBean.set("PROFIL_DEFAUT", utilisateur.getProfilDefaut());
            } else {
                infoBean.set("PROFIL_DEFAUT", "");
            }
            infoBean.set("LISTE_PROFILS", hProfil);
        } else if (InfoBean.ACTION_VALIDER.equals(action)) {
            if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                Chaine.controlerCodeMetier((String) infoBean.get("CODE"));
                final UtilisateurBean utilisateurBean = serviceUser.getByCode(infoBean.get("CODE", String.class));
                if (utilisateurBean != null && utilisateurBean.getCode().equalsIgnoreCase(infoBean.getString("CODE"))) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "UTILISATEUR.ERREUR.IDENTIFIANT_EXISTANT"));
                }
                utilisateur.setCode((String) infoBean.get("CODE"));
                final String codeLdap = (String) infoBean.get("CODE_LDAP");
                if ((codeLdap != null) && !"".equals(codeLdap)) {
                    if (serviceUser.getByCodeLdap(codeLdap) != null) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "UTILISATEUR.ERREUR.LOGIN_EXISTANT"));
                    }
                    final ISynchroLdapUtilisateur synchroLdap = SynchroniseurUtilisateur.getInstance().getSynchroLdap();
                    if (synchroLdap != null && !(synchroLdap.isUnique(codeLdap))) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "UTILISATEUR.ERREUR.LOGIN_LDAP_EXISTANT"));
                    }
                    utilisateur.setCodeLdap(codeLdap);
                }
            }
            if (StringUtils.isNotBlank(infoBean.getString("NEW_MOT_DE_PASSE"))) {
                if (!infoBean.getString("NEW_MOT_DE_PASSE").equals(infoBean.getString("CONFIRM_MOT_DE_PASSE"))) {
                    throw new ErreurApplicative("Vous avez saisi 2 mots de passe différents");
                }
                serviceUser.checkPass(infoBean.getString("NEW_MOT_DE_PASSE"));
                utilisateur.setMotDePasse(MySQLHelper.encodePassword(infoBean.getString("NEW_MOT_DE_PASSE")));
            }
            if (infoBean.get("FORMAT_ENVOI") != null) {
                utilisateur.setFormatEnvoi((String) infoBean.get("FORMAT_ENVOI"));
            }
            utilisateur.setCivilite((String) infoBean.get("CIVILITE"));
            utilisateur.setNom((String) infoBean.get("NOM"));
            utilisateur.setPrenom((String) infoBean.get("PRENOM"));
            utilisateur.setCodeRattachement((String) infoBean.get("CODE_RATTACHEMENT"));
            utilisateur.setAdresseMail((String) infoBean.get("ADRESSE_MAIL"));
            String profilDSI = infoBean.getString("PROFIL_DEFAUT");
            if ("0000".equals(profilDSI)) {
                profilDSI = "";
            }
            utilisateur.setProfilDefaut(profilDSI);
            final Collection<String> vGroupesDsi = new ArrayList<>();
            final StringTokenizer st = new StringTokenizer(infoBean.getString("GROUPE_DSI"), ";");
            while (st.hasMoreTokens()) {
                final String val = st.nextToken();
                vGroupesDsi.add(val);
            }
            // Gestion des centres d'interet
            final List<String> codesThemes = new ArrayList<>();
            for (int j = 0; j < infoBean.getInt("THEMES_NB_ITEMS"); j++) {
                if ("1".equals(infoBean.getString("VALEUR_THEME#" + j))) {
                    codesThemes.add(infoBean.getString("CODE_THEME#" + j));
                }
            }
            utilisateur.setCentresInteret(StringUtils.join(codesThemes, ";"));
            utilisateur.setRoles(infoBean.getString("ROLES"));
            if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                serviceUser.save(utilisateur);
                serviceGroupeUtilisateur.setGroupsForUser(utilisateur.getCode(), vGroupesDsi);
                /* import de la fiche */
                if ("1".equals(infoBean.getString("CREER_FICHE"))) {
                    final String typeFiche = infoBean.getString("TYPE_FICHE");
                    if (StringUtils.isNotBlank(typeFiche)) {
                        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(typeFiche);
                        if (ficheUniv != null) {
                            ficheUniv.setCtx(this);
                            ficheUniv.init();
                            ficheUniv.setCode((String) infoBean.get("CODE"));
                            ficheUniv.setCodeRedacteur((String) infoBean.get("CODE"));
                            ficheUniv.setCodeRattachement((String) infoBean.get("CODE_RATTACHEMENT"));
                            ((AnnuaireModele) ficheUniv).setCivilite((String) infoBean.get("CIVILITE"));
                            ((AnnuaireModele) ficheUniv).setDateNaissance(infoBean.get("DATE_NAISSANCE", Date.class));
                            ((AnnuaireModele) ficheUniv).setNom((String) infoBean.get("NOM"));
                            ((AnnuaireModele) ficheUniv).setPrenom((String) infoBean.get("PRENOM"));
                            ((AnnuaireModele) ficheUniv).setAdresseMail((String) infoBean.get("ADRESSE_MAIL"));
                            ((AnnuaireModele) ficheUniv).setTypePopulation((String) infoBean.get("TYPE_POPULATION"));
                            ((AnnuaireModele) ficheUniv).setCorps((String) infoBean.get("CORPS"));
                            ((AnnuaireModele) ficheUniv).setDiscipline((String) infoBean.get("DISCIPLINE"));
                            if ("1".equals(infoBean.get("CREER_FICHE_EN_LIGNE"))) {
                                ficheUniv.setEtatObjet("0003");
                            }
                            ficheUniv.add();
                            final MetatagBean meta = MetatagUtils.lireMeta(ficheUniv);
                            serviceMetatag.synchroniser(meta, ficheUniv, false);
                            serviceMetatag.save(meta);
                        }
                    }
                }
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_CREATION_UTILISATEUR"), String.format("%s %s", utilisateur.getPrenom(), utilisateur.getNom()));
                infoBean.addMessageConfirmation(confirmation);
            }
            if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {
                serviceUser.save(utilisateur);
                serviceGroupeUtilisateur.setGroupsForUser(utilisateur.getCode(), vGroupesDsi);
                final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_MODIFICATION_UTILISATEUR"), String.format("%s %s", utilisateur.getPrenom(), utilisateur.getNom()));
                infoBean.addMessageConfirmation(confirmation);
            }
            // appel au traitement specifique (permet de surcharger le processus sans tout reecrire)
            if (!"CONFIRMATION".equals(ecranLogique)) {
                etat = FIN;
            }
        }
    }

    /**
     * Traitement associé à l'écran de saisie des critères.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterRECHERCHE() throws Exception {
        if (InfoBean.ACTION_VALIDER.equals(action) || "LISTE".equals(action)) {
            final List<CritereRecherche> criteres = new ArrayList<>();
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVide(infoBean, "CODE"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVideFormater(infoBean, "NOM"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVideFormater(infoBean, "PRENOM"));
            CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVideFormater(infoBean, "ADRESSE_MAIL"));
            final String codeProfil = infoBean.getString("PROFIL_DSI");
            if (StringUtils.isNotBlank(codeProfil)) {
                final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
                final Map<String, String> listeProfil = serviceProfildsi.getDisplayableProfiles();
                if (listeProfil != null && listeProfil.get(codeProfil) != null) {
                    criteres.add(new CritereRecherche("PROFIL_DSI", codeProfil, listeProfil.get(codeProfil)));
                }
            }
            final String groupeDSI = infoBean.getString("GROUPE_DSI");
            if (StringUtils.isNotBlank(groupeDSI)) {
                final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
                final String libelleGroupeDSI = serviceGroupeDsi.getIntitules(groupeDSI);
                criteres.add(new CritereRecherche("GROUPE_DSI", groupeDSI, libelleGroupeDSI));
                criteres.add(new CritereRecherche("LIBELLE_GROUPE_DSI", libelleGroupeDSI));
            }
            final String codeStructure = infoBean.getString("CODE_RATTACHEMENT");
            if (StringUtils.isNotBlank(codeStructure)) {
                final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
                final StructureModele structure = serviceStructure.getByCodeLanguage(codeStructure, LangueUtil.getLangueLocale(this.getLocale()));
                if(structure != null) {
                    final String libelleStructure = structure.getLibelleLong();
                    criteres.add(new CritereRecherche("CODE_RATTACHEMENT", codeStructure, libelleStructure));
                    criteres.add(new CritereRecherche("LIBELLE_CODE_RATTACHEMENT", libelleStructure));
                }
            }
            criteres.add(new CritereRecherche(DatagridUtils.PARAM_BEAN_DATAGRID, UtilisateurDatagrid.ID_BEAN));
            infoBean.set(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES, criteres);
            ecranLogique = ECRAN_LISTE;
        } else if (InfoBean.ACTION_ANNULER.equals(action)) {
            if (InfoBean.ETAT_OBJET_RECHERCHE.equals(infoBean.getEtatObjet())) {
                infoBean.set("ID_UTILISATEUR", null);
                etat = FIN;
            }
        }
    }
}
