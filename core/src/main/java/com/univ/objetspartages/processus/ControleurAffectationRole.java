package com.univ.objetspartages.processus;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RoleBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.InfosRole;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRole;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.InfosRolesUtils;

/**
 * Saisie des affectations de roles
 */
public class ControleurAffectationRole {

    private static final String AFFECTATIONS_NB_ITEMS = "AFFECTATIONS_NB_ITEMS";

    private static final String GROUPE_AFFECTATION = "GROUPE_AFFECTATION#";

    private static final String GROUPE_TYPE_AFFECTATION = "GROUPE_TYPE_AFFECTATION#";

    private static final String RUBRIQUE_AFFECTATION = "RUBRIQUE_AFFECTATION#";

    private static final String RUBRIQUE_TYPE_AFFECTATION = "RUBRIQUE_TYPE_AFFECTATION#";

    private static final String STRUCTURE_AFFECTATION = "STRUCTURE_AFFECTATION#";

    private static final String STRUCTURE_TYPE_AFFECTATION = "STRUCTURE_TYPE_AFFECTATION#";

    /**
     * Preparer principal.
     *
     * @param infoBean
     *            the info bean
     * @param ctx
     *            the ctx
     *
     * @throws Exception
     *             the exception
     */
    public static void preparerPRINCIPAL(final InfoBean infoBean, final OMContext ctx) {
        /*
         * Les roles sont dans da variable ROLE de l'infoBean sous la forme
         * ([perimetr1;role1][perimètre2;role2]
         */
        final StringTokenizer st = new StringTokenizer(StringUtils.defaultString(infoBean.getString("ROLES")), "[]");
        int i = 0;
        while (st.hasMoreTokens()) {
            final String val = st.nextToken();
            final int indexPointVirgule = val.indexOf(';');
            if (indexPointVirgule != -1) {
                final String codeRole = val.substring(0, indexPointVirgule);
                final InfosRole infosRole = InfosRolesUtils.renvoyerItemRole(codeRole);
                //on verifie que le role existe
                if (infosRole.getCode().length() > 0) {
                    final String sPerimetre = val.substring(indexPointVirgule + 1);
                    final Perimetre perimetre = new Perimetre(sPerimetre);
                    // JSS 20051104 : périmètres
                    infoBean.set(STRUCTURE_TYPE_AFFECTATION + i, "");
                    infoBean.set(STRUCTURE_AFFECTATION + i, "");
                    if ("".equals(perimetre.getCodeStructure())) {
                        infoBean.set(STRUCTURE_TYPE_AFFECTATION + i, "0");
                    } else if ("-".equals(perimetre.getCodeStructure())) {
                        infoBean.set(STRUCTURE_TYPE_AFFECTATION + i, "2");
                    } else {
                        infoBean.set(STRUCTURE_TYPE_AFFECTATION + i, "1");
                        infoBean.set(STRUCTURE_AFFECTATION + i, perimetre.getCodeStructure());
                    }
                    infoBean.set(RUBRIQUE_TYPE_AFFECTATION + i, "");
                    infoBean.set(RUBRIQUE_AFFECTATION + i, "");
                    if ("".equals(perimetre.getCodeRubrique())) {
                        infoBean.set(RUBRIQUE_TYPE_AFFECTATION + i, "0");
                    } else if ("-".equals(perimetre.getCodeRubrique())) {
                        infoBean.set(RUBRIQUE_TYPE_AFFECTATION + i, "2");
                    } else {
                        infoBean.set(RUBRIQUE_TYPE_AFFECTATION + i, "1");
                        infoBean.set(RUBRIQUE_AFFECTATION + i, perimetre.getCodeRubrique());
                    }
                    infoBean.set(GROUPE_TYPE_AFFECTATION + i, "");
                    infoBean.set(GROUPE_AFFECTATION + i, "");
                    if ("".equals(perimetre.getCodeGroupe())) {
                        infoBean.set(GROUPE_TYPE_AFFECTATION + i, "0");
                    } else if ("-".equals(perimetre.getCodeGroupe())) {
                        infoBean.set(GROUPE_TYPE_AFFECTATION + i, "2");
                    } else {
                        infoBean.set(GROUPE_TYPE_AFFECTATION + i, "1");
                        infoBean.set(GROUPE_AFFECTATION + i, perimetre.getCodeGroupe());
                    }
                    infoBean.set("ROLE_AFFECTATION#" + i, codeRole);
                    i++;
                }//fin if role existe
            }
        }
        infoBean.setInt(AFFECTATIONS_NB_ITEMS, i);
        rafraichirPRINCIPAL(infoBean, ctx);
    }

    /**
     * préparation des intitulés à afficher.
     *
     * @param infoBean
     *            the info bean
     * @param ctx
     *            the ctx
     *
     * @throws Exception
     *             the exception
     */
    public static void rafraichirPRINCIPAL(final InfoBean infoBean, final OMContext ctx) {
        /* Liste des roles */
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final Map<String, String> hRoles = InfosRolesUtils.getListeRolesHorsEspace();
        infoBean.set("LISTE_ROLES", hRoles);
        final Set<String> enu = hRoles.keySet();
        int j = 0;
        for (final String codeRole : enu) {
            final InfosRole infosRole = InfosRolesUtils.renvoyerItemRole(codeRole);
            final Collection<PermissionBean> vPermissions = infosRole.getVecteurPermissions();
            final Iterator<PermissionBean> it = vPermissions.iterator();
            infoBean.set("ROLE_" + j, codeRole);
            infoBean.set("PERIMETRE_RUBRIQUE_" + j, "0");
            infoBean.set("PERIMETRE_STRUCTURE_" + j, "0");
            infoBean.set("PERIMETRE_GROUPE_" + j, "0");
            boolean perimetreRubrique = false;
            boolean perimetreStructure = false;
            boolean perimetreGroupe = false;
            boolean permissionTraitee = false;
            while (it.hasNext() && !permissionTraitee) {
                final PermissionBean pb = it.next();
                if ((!perimetreRubrique || !perimetreStructure) && (("FICHE".equalsIgnoreCase(pb.getType())) || "TECH".equalsIgnoreCase(pb.getType()) && "rub".equalsIgnoreCase(pb.getObjet()) || "TECH".equalsIgnoreCase(pb.getType()) && "fil".equalsIgnoreCase(pb.getObjet()) || "TECH".equalsIgnoreCase(pb.getType()) && "pho".equalsIgnoreCase(pb.getObjet()) || "TECH".equalsIgnoreCase(pb.getType()) && "pub".equalsIgnoreCase(pb.getObjet()) || "TECH".equalsIgnoreCase(pb.getType()) && "acp".equalsIgnoreCase(pb.getObjet()) || "TECH".equalsIgnoreCase(pb.getType()) && "enc".equalsIgnoreCase(pb.getObjet()) || "TECH".equalsIgnoreCase(pb.getType()) && "new".equalsIgnoreCase(pb.getObjet()) || "TECH".equalsIgnoreCase(pb.getType()) && "for".equalsIgnoreCase(pb.getObjet()))) {
                    infoBean.set("PERIMETRE_RUBRIQUE_" + j, "1");
                    infoBean.set("PERIMETRE_STRUCTURE_" + j, "1");
                    perimetreRubrique = true;
                    perimetreStructure = true;
                }
                if (!perimetreGroupe && "TECH".equalsIgnoreCase(pb.getType()) && "dsi".equalsIgnoreCase(pb.getObjet())) {
                    infoBean.set("PERIMETRE_GROUPE_" + j, "1");
                    perimetreGroupe = true;
                }
                if (perimetreRubrique && perimetreStructure && perimetreGroupe) {
                    permissionTraitee = true;
                }
            }
            j++;
        }
        infoBean.setInt("ROLES_NB_ITEMS", j);
        /* Liste des profils */
        // JSS 20050510 : profil dynamique
        //infoBean.set("LISTE_PROFILS", Profildsi.getListeProfilsDSI(ctx));
        for (int i = 0; i < infoBean.getInt(AFFECTATIONS_NB_ITEMS); i++) {
            /* Intitulés */
            final InfosRole infosRole = InfosRolesUtils.renvoyerItemRole(infoBean.getString("ROLE_AFFECTATION#" + i));
            final String intituleRole = infosRole.getIntitule();
            final Perimetre perimetreRole = new Perimetre(infosRole.getPerimetre());
            // JSS 20051104 : gestion des périmètres
            String structurePerimetre = "";
            if (StringUtils.isNotBlank(STRUCTURE_AFFECTATION + i)) {
                structurePerimetre = infoBean.getString(STRUCTURE_AFFECTATION + i);
            }
            String rubriquePerimetre = "";
            if (StringUtils.isNotBlank(infoBean.getString(RUBRIQUE_AFFECTATION + i))) {
                rubriquePerimetre = infoBean.getString(RUBRIQUE_AFFECTATION + i);
            }
            String groupePerimetre = "";
            if (StringUtils.isNotBlank(infoBean.getString(GROUPE_AFFECTATION + i))) {
                groupePerimetre = infoBean.getString(GROUPE_AFFECTATION + i);
            }
            final Perimetre perimetreAffectation = new Perimetre(structurePerimetre, rubriquePerimetre, "", groupePerimetre, "");
            final Perimetre perimetrePermission = Perimetre.calculerPerimetrePermission(perimetreAffectation, perimetreRole);
            String intitulePerimetre = "";
            if (perimetrePermission == null) {
                intitulePerimetre = "Périmètre invalide";
                infoBean.set("AFFECTATION_PERIMETRE_RUBRIQUE#" + i, intitulePerimetre);
                infoBean.set("AFFECTATION_PERIMETRE_STRUCTURE#" + i, intitulePerimetre);
                infoBean.set("AFFECTATION_PERIMETRE_GROUPE#" + i, intitulePerimetre);
            } else {
                if ("".equals(perimetrePermission.getCodeStructure())) {
                    intitulePerimetre = "Toutes structures";
                } else if ("-".equals(perimetrePermission.getCodeStructure())) {
                    intitulePerimetre = "Aucune structure";
                } else {
                    intitulePerimetre = "Structure : " + serviceStructure.getBreadCrumbs(perimetrePermission.getCodeStructure(), "");
                }
                infoBean.set("AFFECTATION_PERIMETRE_STRUCTURE#" + i, intitulePerimetre);
                infoBean.set(STRUCTURE_TYPE_AFFECTATION + i, "");
                infoBean.set(STRUCTURE_AFFECTATION + i, "");
                if ("".equals(perimetrePermission.getCodeStructure())) {
                    infoBean.set(STRUCTURE_TYPE_AFFECTATION + i, "0");
                } else if ("-".equals(perimetrePermission.getCodeStructure())) {
                    infoBean.set(STRUCTURE_TYPE_AFFECTATION + i, "2");
                } else {
                    infoBean.set(STRUCTURE_TYPE_AFFECTATION + i, "1");
                    infoBean.set(STRUCTURE_AFFECTATION + i, perimetrePermission.getCodeStructure());
                }
                if ("".equals(perimetrePermission.getCodeRubrique())) {
                    intitulePerimetre = "Toutes rubriques";
                } else if ("-".equals(perimetrePermission.getCodeRubrique())) {
                    intitulePerimetre = "Aucune rubrique";
                } else {
                    final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                    intitulePerimetre = "Rubrique : " + serviceRubrique.getLabelWithAscendantsLabels(perimetrePermission.getCodeRubrique());
                }
                infoBean.set("AFFECTATION_PERIMETRE_RUBRIQUE#" + i, intitulePerimetre);
                infoBean.set(RUBRIQUE_TYPE_AFFECTATION + i, "");
                infoBean.set(RUBRIQUE_AFFECTATION + i, "");
                if ("".equals(perimetrePermission.getCodeRubrique())) {
                    infoBean.set(RUBRIQUE_TYPE_AFFECTATION + i, "0");
                } else if ("-".equals(perimetrePermission.getCodeRubrique())) {
                    infoBean.set(RUBRIQUE_TYPE_AFFECTATION + i, "2");
                } else {
                    infoBean.set(RUBRIQUE_TYPE_AFFECTATION + i, "1");
                    infoBean.set(RUBRIQUE_AFFECTATION + i, perimetrePermission.getCodeRubrique());
                }
                if ("".equals(perimetrePermission.getCodeGroupe())) {
                    intitulePerimetre = "Tous groupes";
                } else if ("-".equals(perimetrePermission.getCodeGroupe())) {
                    intitulePerimetre = "Aucun groupe";
                } else {
                    intitulePerimetre = "Groupe : " + serviceGroupeDsi.getBreadCrumbs(perimetrePermission.getCodeGroupe(), StringUtils.EMPTY);
                }
                infoBean.set("AFFECTATION_PERIMETRE_GROUPE#" + i, intitulePerimetre);
                infoBean.set(GROUPE_TYPE_AFFECTATION + i, "");
                infoBean.set(GROUPE_AFFECTATION + i, "");
                if ("".equals(perimetrePermission.getCodeGroupe())) {
                    infoBean.set(GROUPE_TYPE_AFFECTATION + i, "0");
                } else if ("-".equals(perimetrePermission.getCodeGroupe())) {
                    infoBean.set(GROUPE_TYPE_AFFECTATION + i, "2");
                } else {
                    infoBean.set(GROUPE_TYPE_AFFECTATION + i, "1");
                    infoBean.set(GROUPE_AFFECTATION + i, perimetrePermission.getCodeGroupe());
                }
            }
            infoBean.set("AFFECTATION_INTITULE#" + i, intituleRole);
            final ServiceRole serviceRole = ServiceManager.getServiceForBean(RoleBean.class);
            for (final RoleBean currentRole : serviceRole.getAllWithoutCollab()) {
                if (currentRole.getCode().equals(infosRole.getCode())) {
                    infoBean.set("AFFECTATION_ID_ROLE#" + i, currentRole.getIdRole());
                    break;
                }
            }
        }
    }

    /**
     * controle et stockage des affectations de rôles dans la variable ROLE.
     *
     * @param infoBean
     *            the info bean
     * @param ctx
     *            the ctx
     *
     * @throws Exception
     *             the exception
     */
    public static void traiterPRINCIPAL(final InfoBean infoBean, final OMContext ctx) throws Exception {
        final String action = infoBean.getActionUtilisateur();
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            String roles = "";
            for (int i = 0; i < infoBean.getInt(AFFECTATIONS_NB_ITEMS); i++) {
                roles += "[";
                roles += infoBean.getString("ROLE_AFFECTATION#" + i);
                roles += ";";
                // JSS 20051104 : gestion des périmètres
                String structurePerimetre = "";
                if (StringUtils.isNotBlank(infoBean.getString(STRUCTURE_AFFECTATION + i))) {
                    structurePerimetre = infoBean.getString(STRUCTURE_AFFECTATION + i);
                }
                String rubriquePerimetre = "";
                if (StringUtils.isNotBlank(infoBean.getString(RUBRIQUE_AFFECTATION + i))) {
                    rubriquePerimetre = infoBean.getString(RUBRIQUE_AFFECTATION + i);
                }
                String groupePerimetre = "";
                if (StringUtils.isNotBlank(infoBean.getString(GROUPE_AFFECTATION + i))) {
                    groupePerimetre = infoBean.getString(GROUPE_AFFECTATION + i);
                }
                final Perimetre perimetre = new Perimetre(structurePerimetre, rubriquePerimetre, "", groupePerimetre, "");
                roles += perimetre.getChaineSerialisee();
                roles += "]";
            }
            infoBean.set("ROLES", roles);
        }
        if (action.contains("SUPPRIMER_AFFECTATION")) {
            /* Suppression de l'élément courant */
            final int indiceDiese = action.indexOf("#");
            final int indice = Integer.parseInt(action.substring(indiceDiese + 1));
            for (int j = indice; j < infoBean.getInt(AFFECTATIONS_NB_ITEMS) - 1; j++) {
                infoBean.set(STRUCTURE_AFFECTATION + j, infoBean.get(STRUCTURE_AFFECTATION + (j + 1)));
                infoBean.set(STRUCTURE_TYPE_AFFECTATION + j, infoBean.get(STRUCTURE_TYPE_AFFECTATION + (j + 1)));
                infoBean.set(RUBRIQUE_AFFECTATION + j, infoBean.get(RUBRIQUE_AFFECTATION + (j + 1)));
                infoBean.set(RUBRIQUE_TYPE_AFFECTATION + j, infoBean.get(RUBRIQUE_TYPE_AFFECTATION + (j + 1)));
                infoBean.set(GROUPE_AFFECTATION + j, infoBean.get(GROUPE_AFFECTATION + (j + 1)));
                infoBean.set(GROUPE_TYPE_AFFECTATION + j, infoBean.get(GROUPE_TYPE_AFFECTATION + (j + 1)));
                infoBean.set("ROLE_AFFECTATION#" + j, infoBean.get("ROLE_AFFECTATION#" + (j + 1)));
                infoBean.set("AFFECTATION_INTITULE#" + j, infoBean.get("AFFECTATION_INTITULE#" + (j + 1)));
            }
            infoBean.setInt(AFFECTATIONS_NB_ITEMS, infoBean.getInt(AFFECTATIONS_NB_ITEMS) - 1);
            rafraichirPRINCIPAL(infoBean, ctx);
        }
        if (action.contains("AJOUTER_AFFECTATION")) {
            if ((infoBean.getString("ROLE_AFFECTATION").length() == 0) || ("0000".equals(infoBean.getString("ROLE_AFFECTATION")))) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(ctx.getLocale(), "ROLE.ERREUR.ROLE_OBLIGATOIRE"));
            }
            final int nbItems = infoBean.getInt(AFFECTATIONS_NB_ITEMS);
            String structurePerimetre = "";
            if (StringUtils.isNotBlank(infoBean.getString("STRUCTURE_AFFECTATION"))) {
                structurePerimetre = infoBean.getString("STRUCTURE_AFFECTATION");
            }
            String rubriquePerimetre = "";
            if (StringUtils.isNotBlank(infoBean.getString("RUBRIQUE_AFFECTATION"))) {
                rubriquePerimetre = infoBean.getString("RUBRIQUE_AFFECTATION");
            }
            String groupePerimetre = "";
            if (StringUtils.isNotBlank(infoBean.getString("GROUPE_AFFECTATION"))) {
                groupePerimetre = infoBean.getString("GROUPE_AFFECTATION");
            }
            /* Controle du périmètre */
            final Perimetre perimetreAffectation = new Perimetre(structurePerimetre, rubriquePerimetre, "", groupePerimetre, "");
            final InfosRole infosRole = InfosRolesUtils.renvoyerItemRole(infoBean.getString("ROLE_AFFECTATION"));
            final Perimetre perimetreRole = new Perimetre(infosRole.getPerimetre());
            if (!perimetreAffectation.estUnSousPerimetreDe(perimetreRole, true)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(ctx.getLocale(), "ROLE.ERREUR.PERIMETRE_AFFECTATION"));
            }
            if (!checkRoleUnicity(infoBean, perimetreAffectation)) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(ctx.getLocale(), "ROLE.ERREUR.PARAMETRAGE_EXISTANT"));
            }
            infoBean.set(STRUCTURE_AFFECTATION + nbItems, infoBean.getString("STRUCTURE_AFFECTATION"));
            infoBean.set(STRUCTURE_TYPE_AFFECTATION + nbItems, infoBean.getString("STRUCTURE_TYPE_AFFECTATION"));
            infoBean.set(RUBRIQUE_AFFECTATION + nbItems, infoBean.getString("RUBRIQUE_AFFECTATION"));
            infoBean.set(RUBRIQUE_TYPE_AFFECTATION + nbItems, infoBean.getString("RUBRIQUE_TYPE_AFFECTATION"));
            infoBean.set(GROUPE_AFFECTATION + nbItems, infoBean.getString("GROUPE_AFFECTATION"));
            infoBean.set(GROUPE_TYPE_AFFECTATION + nbItems, infoBean.getString("GROUPE_TYPE_AFFECTATION"));
            infoBean.set("ROLE_AFFECTATION#" + nbItems, infoBean.getString("ROLE_AFFECTATION"));
            infoBean.setInt(AFFECTATIONS_NB_ITEMS, nbItems + 1);
            rafraichirPRINCIPAL(infoBean, ctx);
            infoBean.remove("ROLE_AFFECTATION");
        }
    }

    private static boolean checkRoleUnicity(final InfoBean infoBean, final Perimetre addPerimetre) {
        for (int i = 0; i < infoBean.getInt(AFFECTATIONS_NB_ITEMS); i++) {
            final String roleAffectation = String.format("ROLE_AFFECTATION#%d", i);
            if (infoBean.getString(roleAffectation).equals(infoBean.getString("ROLE_AFFECTATION"))) {
                final InfosRole infosRole = InfosRolesUtils.renvoyerItemRole(roleAffectation);
                final Perimetre perimetreRole = new Perimetre(infosRole.getPerimetre());
                if (perimetreRole.equals(addPerimetre)) {
                    return false;
                }
            }
        }
        return true;
    }
}
