package com.univ.objetspartages.processus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantLibelle;
import com.univ.datagrid.processus.LibelleDatagrid;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.cache.CacheLibelleManager;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.services.ServiceLabel;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.objetspartages.util.CritereRechercheUtil;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUtil;
import com.univ.utils.RechercheFicheHelper;

/**
 * Processus de saisie d'un libellé.
 */
public class SaisieLibelle extends ProcessusBean {

    private static final Logger LOG = LoggerFactory.getLogger(SaisieLibelle.class);

    /**
     * The Constant MODE_AJOUT.
     */
    private static final int MODE_AJOUT = 0;

    /**
     * The Constant MODE_MODIFICATION.
     */
    private static final int MODE_MODIFICATION = 1;

    /**
     * The Constant ECRAN_PRINCIPAL.
     */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    private static final String ECRAN_LISTE = "LISTE";

    /**
     * The Constant ECRAN_IMPORT.
     */
    private static final String ECRAN_IMPORT = "SAISIE_IMPORT";

    private static final String ECRAN_RECHERCHE = "RECHERCHE";

    private final CacheLibelleManager cache = ApplicationContextManager.getCoreContextBean(CacheLibelleManager.ID_BEAN, CacheLibelleManager.class);

    private final ServiceLabel serviceLabel;

    /**
     * The mode.
     */
    private int mode = -1;

    /**
     * The libelle.
     */
    private LabelBean libelle;

    /**
     * Constructeur.
     *
     * @param ciu com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieLibelle(final InfoBean ciu) {
        super(ciu);
        serviceLabel = ServiceManager.getServiceForBean(LabelBean.class);
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     * @throws Exception the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        try {
            final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
            if (!ComposantLibelle.isAutoriseParActionProcessus(autorisations, null)) {
                if (autorisations == null) {
                    infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
                    infoBean.setEcranLogique("LOGIN");
                } else {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
                }
            } else {
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                if (etat == DEBUT) {
                    initialisation();
                } else {
                    if (ECRAN_RECHERCHE.equals(ecranLogique)) {
                        traiterRECHERCHE();
                    }
                    if (ECRAN_PRINCIPAL.equals(ecranLogique)) {
                        traiterPRINCIPAL();
                    } else if (ECRAN_IMPORT.equals(ecranLogique)) {
                        traiterIMPORT();
                    }
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            }
        } catch (final Exception e) {
            LOG.error("erreur lors du traitement des libelles", e);
            infoBean.addMessageErreur(e.toString());
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Initialisation du processus.
     *
     * @throws Exception the exception
     */
    private void initialisation() throws Exception {
        // initialisation objets métiers
        etat = EN_COURS;
        if (InfoBean.ACTION_AJOUTER.equals(action)) {
            mode = MODE_AJOUT;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
            createLabel();
            preparerPRINCIPAL();
        } else if (InfoBean.ACTION_MODIFIER.equals(action)) {
            mode = MODE_MODIFICATION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            traiterRECHERCHE();
        } else if (InfoBean.ACTION_RECHERCHER.equals(action)) {
            mode = MODE_MODIFICATION;
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
            preparerRECHERCHE();
        } else if ("IMPORTER".equals(action)) {
            infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
            ecranLogique = ECRAN_IMPORT;
        } else if ("MODIFIERPARID".equals(action)) {
            mode = MODE_MODIFICATION;
            retrieveLabelFromInfoBean();
            preparerPRINCIPAL();
        } else if ("SUPPRIMERPARID".equals(action)) {
            retrieveLabelFromInfoBean();
            supprimerLibelle();
            preparerPRINCIPAL();
            etat = FIN;
            cache.flushListeInfosLibelles();
        }
    }

    private void preparerRECHERCHE() {
        ecranLogique = ECRAN_RECHERCHE;
        infoBean.set("LISTE_TYPES_LIBELLES", cache.getListeTypesLibelles());
        final Hashtable<String, String> langues = LangueUtil.getListeLangues();
        infoBean.set("LISTE_LANGUES", langues);
    }

    private void createLabel(){
        libelle = new LabelBean();
        libelle.setType(StringUtils.EMPTY);
        libelle.setCode(StringUtils.EMPTY);
        libelle.setLibelle(StringUtils.EMPTY);
        libelle.setLangue(StringUtils.EMPTY);
    }

    private void retrieveLabelFromInfoBean() throws ErreurApplicative {
        final Long id = Long.parseLong(infoBean.get("ID_LIBELLE", String.class));
        libelle = serviceLabel.getById(id);
        if (libelle == null) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("LIBELLE.INCONNU"));
        }
    }

    /**
     * Traitement associé à l'écran de saisie des critères. LIBELLE CODE TYPE LANGUE
     *
     * @throws Exception the exception
     */
    private void traiterRECHERCHE() throws Exception {
        final List<CritereRecherche> criteres = new ArrayList<>();
        CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVide(infoBean, "LIBELLE"));
        CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVide(infoBean, "CODE"));
        CollectionUtils.addIgnoreNull(criteres, CritereRechercheUtil.getCritereTexteNonVideSansValeurDefaut(infoBean, "LANGUE"));
        final String type = infoBean.getString("TYPE");
        if (StringUtils.isNotBlank(type) && !"0000".equals(type)) {
            criteres.add(new CritereRecherche("TYPE", type, cache.getListeTypesLibelles().get(type)));
        }
        criteres.add(new CritereRecherche(DatagridUtils.PARAM_BEAN_DATAGRID, LibelleDatagrid.ID_BEAN));
        infoBean.set(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES, criteres);
        ecranLogique = ECRAN_LISTE;
    }

    /**
     * Affichage de l'écran de saisie d'un Libelle.
     *
     * @throws Exception the exception
     */
    private void preparerPRINCIPAL() throws Exception {
        ecranLogique = ECRAN_PRINCIPAL;
        infoBean.set("ACTION_DEMANDEE", action);
        infoBean.set("TYPE", libelle.getType());
        infoBean.set("LISTE_TYPES_LIBELLES", cache.getListeTypesLibelles());
        if (mode == MODE_AJOUT) {
            libelle.setCode(determinerCode());
        }
        infoBean.set("CODE", libelle.getCode());
        final Hashtable<String, String> langues = LangueUtil.getListeLangues(ContexteUtil.getContexteUniv().getLocale());
        infoBean.set("LISTE_LANGUES", langues);
        for (final String codeLangue : langues.keySet()) {
            final LabelBean labelBean = serviceLabel.getByTypeCodeLanguage(libelle.getType(), libelle.getCode(), codeLangue);
            infoBean.set("LIBELLE#" + codeLangue, labelBean != null ? StringUtils.defaultIfEmpty(labelBean.getLibelle(), StringUtils.EMPTY) : StringUtils.EMPTY);
        }
        final String langueInfoBean = infoBean.getString("LANGUE_LIBELLE");
        if (StringUtils.isNotBlank(langueInfoBean) && StringUtils.isNumeric(langueInfoBean)) {
            libelle.setLangue(langueInfoBean);
        }
        if (StringUtils.isBlank(libelle.getLangue())) {
            libelle.setLangue("0");
        }
        infoBean.set("LANGUE", libelle.getLangue());
        if (StringUtils.isNotBlank(serviceLabel.getDisplayableLibelle(libelle))) {
            infoBean.setTitreEcran(serviceLabel.getDisplayableLibelle(libelle));
        }
    }

    private void supprimerLibelle() throws Exception {
        final List<LabelBean> labels = serviceLabel.getByTypeCode(libelle.getType(), libelle.getCode());
        for(final LabelBean currentLabel : labels) {
            serviceLabel.delete(currentLabel.getId());
        }
        final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_LIBELLE"), libelle.getCode());
        infoBean.addMessageConfirmation(confirmation);
    }

    private void ajouterLibelle() throws Exception {
        Chaine.controlerCodeMetier((String) infoBean.get("CODE"));
        final List<LabelBean> labels = serviceLabel.getByTypeCode(infoBean.getString("TYPE"), infoBean.getString("CODE"));
        if (CollectionUtils.isNotEmpty(labels)) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_LIBELLE.ERREUR.CODE_EXISTANT"));
        }
        final Hashtable<String, String> listeLangue = LangueUtil.getListeLangues(ContexteUtil.getContexteUniv().getLocale());
        int nbLibelleSaisie = 0;
        for (final String codeLangue : listeLangue.keySet()) {
            final String valeurLibelle = infoBean.getString("LIBELLE#" + codeLangue);
            if (StringUtils.isNotBlank(valeurLibelle)) {
                final LabelBean newLabel = new LabelBean();
                newLabel.setType(infoBean.getString("TYPE"));
                newLabel.setCode(infoBean.getString("CODE"));
                newLabel.setLangue(codeLangue);
                newLabel.setLibelle(valeurLibelle);
                serviceLabel.save(newLabel);
                nbLibelleSaisie++;
            }
        }
        if (nbLibelleSaisie == 0) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("ERREUR_SAISIE_SANS_LIBELLE"));
        }
        final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_CREATION_LIBELLE"), libelle.getCode());
        infoBean.addMessageConfirmation(confirmation);
    }

    private void mettreLibelleAJour() throws Exception {
        final Hashtable<String, String> listeLangue = LangueUtil.getListeLangues();
        int nbLibelleSaisie = 0;
        for (final String codeLangue : listeLangue.keySet()) {
            final String valeurLibelle = infoBean.getString("LIBELLE#" + codeLangue);
            LabelBean labelBean = serviceLabel.getByTypeCodeLanguage(infoBean.getString("TYPE"), infoBean.getString("CODE"), codeLangue);
            if (StringUtils.isNotBlank(valeurLibelle)) {
                if(labelBean == null) {
                    labelBean = new LabelBean();
                    labelBean.setType(infoBean.getString("TYPE"));
                    labelBean.setCode(infoBean.getString("CODE"));
                    labelBean.setLangue(codeLangue);
                }
                labelBean.setLibelle(valeurLibelle);
                serviceLabel.save(labelBean);
                nbLibelleSaisie++;
            } else if (labelBean != null) {
                serviceLabel.delete(labelBean.getId());
            }
        }
        if (nbLibelleSaisie == 0) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("ERREUR_SAISIE_SANS_LIBELLE"));
        }
        final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_MODIFICATION_LIBELLE"), libelle.getCode());
        infoBean.addMessageConfirmation(confirmation);
    }

    /**
     * Gestion de l'écran de saisie d'un Libelle.
     *
     * @throws Exception the exception
     */
    private void traiterPRINCIPAL() throws Exception {
        if (InfoBean.ACTION_SUPPRIMER.equals(action)) {
            supprimerLibelle();
        } else {
            verifierLibelle();
            if (mode == MODE_AJOUT) {
                ajouterLibelle();
            } else if (mode == MODE_MODIFICATION) {
                mettreLibelleAJour();
            }
        }
        etat = FIN;
        cache.flushListeInfosLibelles();
    }

    private void verifierLibelle() throws Exception {
        final Hashtable<String, String> listeLangue = LangueUtil.getListeLangues();
        for (final String codeLangue : listeLangue.keySet()) {
            final String lib = infoBean.getString("LIBELLE#" + codeLangue);
            if (lib.contains(",") || lib.contains(";")) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_LIBELLE.ERREUR.REMPLACEMENT_CARACTERE"));
            }
        }
    }

    /**
     * Détermine un code libellé aléatoire.
     *
     * @return the string
     */
    private String determinerCode() {
        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        final StringBuilder code = new StringBuilder();
        int pos;
        for (int i = 0; i < 8; i++) {
            pos = (int) (Math.random() * chars.length());
            if (pos >= chars.length()) {
                pos = 0;
            }
            code.append(chars.charAt(pos));
        }
        return code.toString();
    }

    /**
     * Gestion de l'écran de saisie d'un Libelle.
     * Cette méthode est appelée lors de l'import des libellés.
     * Les données importées doivent respecter le modèle suivant ;'typeLibelle','codelibelle','libelle','languelibelle'. Le delimiteur utilisé pour parser les données est ',' (cote virgule cote).
     * @throws Exception the exception
     */
    private void traiterIMPORT() throws Exception {
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            final UploadedFile file = (UploadedFile) infoBean.get("LIBELLE_FICHIER_FILE");
            final File f;
            if ((file != null) && (file.getContentFilename().length() != 0)) {
                f = file.getTemporaryFile();
            } else {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(this.getLocale(), "SAISIE_LIBELLE.ERREUR.FICHIER_INEXISTANT"));
            }
            final FileReader freader = new FileReader(f);
            try (final BufferedReader fichLogique = new BufferedReader(freader)) {
                String ligneLue = fichLogique.readLine();
                int nbLibelle = 0;
                int nbLigne = 0;
                final List<Integer> libellesKo = new ArrayList<>();
                while (ligneLue != null) {
                    nbLigne++;
                    String typeLibelle = StringUtils.EMPTY;
                    String code = StringUtils.EMPTY;
                    String libelle = StringUtils.EMPTY;
                    String langue = StringUtils.EMPTY;
                    final StrTokenizer st = StrTokenizer.getCSVInstance(ligneLue.substring(1,ligneLue.length() - 1));
                    st.setIgnoreEmptyTokens(false);
                    st.setDelimiterString("','");
                    int ind = 0;
                    while (st.hasNext()) {
                        final String item = st.nextToken();
                        if (ind == 0) {
                            typeLibelle = item;
                        }
                        if (ind == 1) {
                            code = item;
                        }
                        if (ind == 2) {
                            libelle = item;
                        }
                        if (ind == 3) {
                            langue = item;
                        }
                        ind++;
                    }
                    if ((typeLibelle.length() > 0) && (code.length() > 0) && (libelle.length() > 0) && (langue.length() > 0)) {
                        nbLibelle++;
                        serviceLabel.createNewLabel(typeLibelle, code, libelle, langue);
                    } else {
                        libellesKo.add(nbLigne);
                    }
                    ligneLue = fichLogique.readLine();
                }
                cache.flushListeInfosLibelles();
                String resultatImport;
                if(nbLibelle > 0) {
                    if (nbLibelle == 1) {
                        resultatImport = MessageHelper.getCoreMessage("BO_LIBELLE_RESULTAT_IMPORT_LIBELLE");
                    } else {
                        resultatImport = MessageHelper.getCoreMessage("BO_LIBELLE_RESULTAT_IMPORT_LIBELLES");
                        resultatImport = String.format(resultatImport, nbLibelle);
                    }
                    infoBean.addMessageConfirmation(resultatImport);
                    traiterRECHERCHE();
                }
                if(CollectionUtils.isNotEmpty(libellesKo)) {
                    infoBean.addMessageAlerte(MessageHelper.getCoreMessage("BO_LIBELLE_RESULTAT_KO") + " [" + StringUtils.join(libellesKo, "; ") + "]");
                    ecranLogique = ECRAN_IMPORT;
                }
            } catch (final Exception e) {
                LOG.error("Format du fichier d'import des libellés invalide", e);
                infoBean.addMessageErreur(MessageHelper.getCoreMessage("BO_LIBELLE_RESULTAT_ERREUR"));
                infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
                ecranLogique = ECRAN_IMPORT;
            }
        }
    }
}
