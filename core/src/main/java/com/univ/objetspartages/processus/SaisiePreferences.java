package com.univ.objetspartages.processus;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.ServiceBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceServiceExterne;
import com.univ.utils.Chaine;
import com.univ.utils.ServicesUtil;

/**
 * Processus saisie Preferences.
 */
public class SaisiePreferences extends ProcessusBean {

    /** The Constant ECRAN_SERVICE. */
    private static final String ECRAN_SERVICE = "SERVICE";

    /** The Constant ECRAN_RECHERCHE. */
    private static final String ECRAN_RECHERCHE = "RECHERCHE";

    private static final Logger LOG = LoggerFactory.getLogger(SaisiePreferences.class);

    private final ServiceServiceExterne serviceServiceExterne;

    /** The service bean. */
    private ServiceBean serviceBean = null;

    /**
     * Processus SaisiePreferences
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public SaisiePreferences(final InfoBean ciu) {
        super(ciu);
        serviceServiceExterne = ServiceManager.getServiceForBean(ServiceBean.class);
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            try {
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                if (etat == DEBUT) {
                    etat = EN_COURS;
                    if ("AJOUTER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_CREATION);
                        serviceBean = new ServiceBean();
                        preparerSERVICE();
                    } else if ("MODIFIER".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                        preparerRECHERCHE();
                    }  else if ("MODIFIERPARCODE".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_MODIF);
                        serviceBean = serviceServiceExterne.getByCode(infoBean.getString("CODE_SERVICE"));
                        if (serviceBean == null) {
                            throw new ErreurApplicative("Il n'existe aucun service ayant ce code.");
                        }
                        preparerSERVICE();
                    } else if ("SUPPRIMERPARCODE".equals(action)) {
                        infoBean.setEtatObjet(InfoBean.ETAT_OBJET_SUPPRESSION);
                        final ServiceBean serviceASupprimer = ServicesUtil.getService(infoBean.getString("CODE_SERVICE"));
                        ServicesUtil.removeInstanceService(this, infoBean.getString("CODE_SERVICE"));
                        ServicesUtil.getCacheServiceManager().asyncRefresh();
                        final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_SERVICE"), serviceASupprimer.getIntitule());
                        infoBean.addMessageConfirmation(confirmation);
                        etat = FIN;
                    }
                } else {
                    if (ecranLogique.equals(ECRAN_SERVICE)) {
                        traiterSERVICE();
                    }
                }
                // placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOG.error("erreur de traitement sur le processus", e);
                infoBean.addMessageErreur(e.toString());
            }
        }
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Affichage de l'écran des critères de recherche d'un Role.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerRECHERCHE() throws Exception {
        ecranLogique = ECRAN_RECHERCHE;
        infoBean.set("CODE_SERVICE", StringUtils.EMPTY);
        final Map<String, String> listeServices = new HashMap<>();
        for (final ServiceBean service : ServicesUtil.getServices().values()) {
            listeServices.put(service.getCode(), service.getIntitule());
        }
        infoBean.set("LISTE_SERVICES", listeServices);
        infoBean.set("SERVICES", ServicesUtil.getServices().values());
        infoBean.setTitreEcran(MessageHelper.getCoreMessage("BO.SAISIE_PREFERENCES.SERVICES.TITRE"));
    }

    /**
     * Affichage de l'écran de saisie d'un service.
     *
     * @throws Exception
     *             the exception
     */
    private void preparerSERVICE() throws Exception {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        infoBean.set("ACTION_SERVICE", action);
        infoBean.set("SERVICE_CODE", serviceBean.getCode());
        infoBean.set("SERVICE_INTITULE", serviceBean.getIntitule());
        if (serviceBean.getExpirationCacheService().equals(Integer.valueOf(-1))) {
            infoBean.set("SERVICE_EXPIRATION_CACHE", "");
        } else {
            infoBean.set("SERVICE_EXPIRATION_CACHE", serviceBean.getExpirationCacheService().toString());
        }
        infoBean.set("SERVICE_JETON_KPORTAL", serviceBean.getJetonKportal());
        infoBean.set("SERVICE_PROXY_CAS", serviceBean.getProxyCas());
        infoBean.set("SERVICE_URL", serviceBean.getUrl());
        infoBean.set("SERVICE_URL_POPUP", serviceBean.getUrlPopup());
        infoBean.set("SERVICE_VUE_REDUITE_URL", serviceBean.getVueReduiteUrl());
        infoBean.set("SERVICE_DIFFUSION_MODE", serviceBean.getDiffusionMode());
        infoBean.set("SERVICE_DIFFUSION_PUBLIC_VISE", Chaine.convertirPointsVirgules(serviceBean.getDiffusionPublicVise()));
        infoBean.set("SERVICE_DIFFUSION_MODE_RESTRICTION", serviceBean.getDiffusionModeRestriction());
        infoBean.set("SERVICE_DIFFUSION_PUBLIC_VISE_RESTRICTION", Chaine.convertirPointsVirgules(serviceBean.getDiffusionPublicViseRestriction()));
        infoBean.set("LIBELLE_SERVICE_DIFFUSION_PUBLIC_VISE", serviceGroupeDsi.getIntitules(infoBean.getString("SERVICE_DIFFUSION_PUBLIC_VISE")));
        infoBean.set("LIBELLE_SERVICE_DIFFUSION_PUBLIC_VISE_RESTRICTION", serviceGroupeDsi.getIntitules(infoBean.getString("SERVICE_DIFFUSION_PUBLIC_VISE_RESTRICTION")));
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE", "1");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_TYPE", "TECH");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_OBJET", "dsi");
        infoBean.set("GRS_FILTRE_ARBRE_GROUPE_ACTION", "");
        ecranLogique = ECRAN_SERVICE;
        if (StringUtils.isNotBlank(serviceBean.getIntitule())) {
            infoBean.setTitreEcran(serviceBean.getIntitule());
        }
    }

    /**
     * Gestion de l'écran de saisie d'un service.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterSERVICE() throws Exception {
        if (InfoBean.ACTION_VALIDER.equals(action)) {
            if (InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                if (infoBean.getString("SERVICE_CODE").length() == 0) {
                    throw new ErreurApplicative("Le code du service est obligatoire");
                }
                final String serviceCode = infoBean.getString("SERVICE_CODE");
                // Vérifie que le code ne contient que des caractères alphanumériques ou '-' ou '_'
                Chaine.controlerCodeMetier(serviceCode);
                final ServiceBean serviceBean = serviceServiceExterne.getByCode(serviceCode);
                if (serviceBean != null) {
                    throw new ErreurApplicative("Il existe déjà un service ayant ce code.");
                }
            }
            if (infoBean.getString("SERVICE_INTITULE").length() == 0) {
                throw new ErreurApplicative("L'intitulé du service est obligatoire");
            }
            if (";".contains(infoBean.getString("SERVICE_INTITULE"))) {
                throw new ErreurApplicative("Le service ne doit pas contrenir de point-virgule");
            }
            Integer expirationCache = -1;
            if (infoBean.getString("SERVICE_EXPIRATION_CACHE").length() > 0) {
                try {
                    expirationCache = Integer.valueOf(infoBean.getString("SERVICE_EXPIRATION_CACHE"));
                } catch (final Exception e) {
                    throw new ErreurApplicative("Format de la durée de cache incorrect", e);
                }
            }
            /* récupération indice de la ligne dans InfoBean */
            final String code = infoBean.getString("SERVICE_CODE");
            serviceBean = new ServiceBean(code, infoBean.getString("SERVICE_INTITULE"), expirationCache, expirationCache, infoBean.getString("SERVICE_JETON_KPORTAL"), infoBean.getString("SERVICE_PROXY_CAS"), infoBean.getString("SERVICE_URL"), infoBean.getString("SERVICE_URL_POPUP"), infoBean.getString("SERVICE_VUE_REDUITE_URL"), infoBean.getString("SERVICE_DIFFUSION_MODE"), Chaine.getVecteurPointsVirgules(infoBean.getString("SERVICE_DIFFUSION_PUBLIC_VISE")), infoBean.getString("SERVICE_DIFFUSION_MODE_RESTRICTION"), Chaine.getVecteurPointsVirgules(infoBean.getString("SERVICE_DIFFUSION_PUBLIC_VISE_RESTRICTION")));
            ServicesUtil.setInstanceService(this, serviceBean);
            ServicesUtil.getCacheServiceManager().asyncRefresh();
            String messageConfirmation = MessageHelper.getCoreMessage("CONFIRMATION_MODIFICATION_SERVICE");
            if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
                messageConfirmation = MessageHelper.getCoreMessage("CONFIRMATION_CREATION_SERVICE");
            }
            final String confirmation = String.format(messageConfirmation, serviceBean.getIntitule());
            infoBean.addMessageConfirmation(confirmation);
            etat = FIN;
        } else if ("SUPPRIMER".equals(action)) {
            ServicesUtil.removeInstanceService(this, serviceBean.getCode());
            ServicesUtil.getCacheServiceManager().asyncRefresh();
            final String confirmation = String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_SERVICE"), serviceBean.getIntitule());
            infoBean.addMessageConfirmation(confirmation);
            etat = FIN;
        }
    }

}
