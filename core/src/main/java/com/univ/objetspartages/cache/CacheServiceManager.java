package com.univ.objetspartages.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.kportal.cache.AbstractCacheManager;
import com.univ.objetspartages.om.ServiceBean;
import com.univ.objetspartages.services.ServiceServiceExterne;

public class CacheServiceManager extends AbstractCacheManager {

    public static final String ID_BEAN = "cacheServiceManager";

    public static final String KEY_CACHE = "CacheServiceManager.cacheServices";

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheServiceManager.class);

    private Map<String, ServiceBean> getServices() {
        final Map<String, ServiceBean> services = new HashMap<>();
        final ServiceServiceExterne serviceServiceExterne = ServiceManager.getServiceForBean(ServiceBean.class);
        List<ServiceBean> allServices = serviceServiceExterne.getAll();
        for (ServiceBean serviceBean : allServices) {
            services.put(serviceBean.getCode(),serviceBean);
            LOGGER.debug("fin chargement service " + serviceBean.getCode());
        }
        LOGGER.info("Chargement de " + services.size() + " services OK");
        return services;
    }

    @Override
    public Map<String, ServiceBean> getObjectToCache() throws Exception {
        return getServices();
    }

    @Override
    public String getCacheName() {
        return KEY_CACHE;
    }

    @Override
    public Object getObjectKey() {
        return KEY_CACHE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, ServiceBean> call() {
        return (Map<String, ServiceBean>) super.call();
    }

}
