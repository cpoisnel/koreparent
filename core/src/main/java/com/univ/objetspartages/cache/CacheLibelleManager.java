package com.univ.objetspartages.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import com.jsbsoft.jtf.core.CodeLibelle;
import com.kportal.extension.ExtensionHelper;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.services.ServiceLabel;

public class CacheLibelleManager {

    public static final String ID_BEAN = "cacheLibelleManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheLibelleManager.class);

    private ServiceLabel serviceLabel;

    public void setServiceLabel(ServiceLabel serviceLabel) {
        this.serviceLabel = serviceLabel;
    }

    @Cacheable(value = "CacheLibelleManager.getListeInfosLibelles")
    public Map<String, LabelBean> getListeInfosLibelles() {
        final Map<String, LabelBean> listeLibelles = new HashMap<>();
        // Charge en mémoire la liste des libelles
        final List<LabelBean> labels = serviceLabel.getAllLabels();
        for (LabelBean currentLabel : labels) {
            // Ajout dans la bibliothèque de rubriques
            listeLibelles.put(String.format("%s%s%s", currentLabel.getType(), currentLabel.getCode(), currentLabel.getLangue()), currentLabel);
        }
        LOGGER.info(String.format("Chargement de %d libelles OK", labels.size()));
        return listeLibelles;
    }

    /**
     *  Renvoie une Hashtable contenant la concaténation de la table type_libelle.dat et des tables type_libelle.dat propres à chaque développement spécifique.
     *
     */
    public Map<String, String> getListeTypesLibelles() {
        final Map<String, String> listeTypesLibelles = new HashMap<>();
        for (final String idExt : ExtensionHelper.getExtensionManager().getExtensions().keySet()) {
            Map<String, String> h = CodeLibelle.lireTable(idExt, "type_libelle", null);
            listeTypesLibelles.putAll(h);
            h = CodeLibelle.lireTable(idExt, idExt + "_type_libelle", null);
            listeTypesLibelles.putAll(h);
        }
        return listeTypesLibelles;
    }

    @CacheEvict(value = "CacheLibelleManager.getListeInfosLibelles")
    public void flushListeInfosLibelles() {}

    public void flushAll() {
        flushListeInfosLibelles();
    }
}