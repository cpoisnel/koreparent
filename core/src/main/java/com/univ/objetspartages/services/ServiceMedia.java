package com.univ.objetspartages.services;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.integration.annotation.Publisher;
import org.springframework.messaging.handler.annotation.Payload;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.publish.ServiceCorePublisher;
import com.kosmos.service.impl.AbstractServiceBean;
import com.univ.mediatheque.Mediatheque;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.dao.impl.MediaDAO;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.FileUtil;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseWhere;

/**
 * Created on 06/05/15.
 */
public class ServiceMedia extends AbstractServiceBean<MediaBean, MediaDAO> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceMedia.class);

    /**
     * Service de publication (pas d'interception AOP des services, publication effectuée de manière programmatique)
     */
    private ServiceCorePublisher serviceCorePublisher;

    @Override
    public void save(final MediaBean bean) {
        throw new UnsupportedOperationException("Cette méthode n'est pas supportée par ce service. Utilisez l'autre méthode \"save\"");
    }

    /**
     * This method handles passed {@link com.univ.objetspartages.bean.MediaBean} persistence and moves the uploaded media file (in application's tmp directory)
     * to the media storage path (as defined in the application's properties).
     * @param mediaBean : the {@link com.univ.objetspartages.bean.MediaBean} to save.
     * @param sAbsolutePathFile : the absolute path to the file to move from tmp to media storage as a {@link String}
     * @param sAbsolutePathVignette : the absolute path to the thumbnail file to move from tmp to media storage as a {@link String}
     * @param sFileName : the computed new file name as a {@link String}
     * @param sVignetteName : the computed new thumbnail name as a {@link String}
     * @throws ErreurApplicative : if an error occured during file moving or during bean persistence.
     */


    @Caching(evict = {
        @CacheEvict(value = "ServiceMedia.getCodeRubrique", key = "#mediaBean.getUrl()"),
        @CacheEvict(value = "ServiceMedia.getById", key = "#mediaBean.getId()")
    })
    public void save(final MediaBean mediaBean, final String sAbsolutePathFile, final String sAbsolutePathVignette, final String sFileName, final String sVignetteName) throws ErreurApplicative {
        final String sOldAbsolutePathFile = MediaUtils.getPathAbsolu(mediaBean);
        final String sOldAbsolutePathVignette = MediaUtils.getPathVignetteAbsolu(mediaBean);
        boolean suppressionFile = false;
        boolean suppressionVignette = false;
        // média stocké en local
        if (MediaUtils.isLocal(mediaBean)) {
            // nouveau fichier source
            if (!sAbsolutePathFile.equals(sOldAbsolutePathFile)) {
                suppressionFile = true;
                final File tmpFile = new File(sAbsolutePathFile);
                if (tmpFile.exists()) {
                    /* Création du répertoire si nécessaire */
                    String repertoire;
                    if (MediaUtils.isPublic(mediaBean)) {
                        repertoire = MediathequeHelper.getAbsolutePath();
                        if (mediaBean.getTypeRessource().length() > 0) {
                            repertoire = String.format("%s%s%s", repertoire, File.separator, mediaBean.getTypeRessource().toLowerCase());
                        }
                    } else {
                        repertoire = RessourceUtils.getAbsolutePath();
                    }
                    final File fRepertoire = new File(repertoire);
                    boolean okDirs = true;
                    if (!fRepertoire.exists()) {
                        okDirs = fRepertoire.mkdirs();
                    }
                    if (!okDirs) {
                        LOG.error(String.format("Impossible de creer le repertoire des medias :%s (verifier les droits d'ecriture sur le repertoire)", fRepertoire));
                    }
                    /* Valorisation du path et de l'url */
                    final String newPath = repertoire + File.separator + sFileName;
                    // pas de suppression de source s'il s'agit d'une image qui n'a pas été redimmensionnée (les deux urls et donc la source sont alors identiques)
                    try {
                        FileUtil.primitiveCopierFichier(tmpFile, new File(newPath), !sAbsolutePathFile.equals(sAbsolutePathVignette));
                    } catch (final IOException e) {
                        throw new ErreurApplicative(String.format("Une erreur est survenue lors de l'enregistrement du média : %s", e.getMessage()), e);
                    }
                    mediaBean.setUrl(sFileName);
                }
            }
        }
        // nouvelle vignette
        if (!sAbsolutePathVignette.equals(sOldAbsolutePathVignette)) {
            suppressionVignette = true;
            final File tmpFile = new File(sAbsolutePathVignette);
            if (tmpFile.exists()) {
                /* Création du répertoire si nécessaire */
                String repertoire = MediathequeHelper.getAbsolutePath();
                if (StringUtils.isNotBlank(mediaBean.getTypeRessource())) {
                    repertoire += File.separator + mediaBean.getTypeRessource().toLowerCase();
                }
                final File fRepertoire = new File(repertoire);
                boolean okDirs = true;
                if (!fRepertoire.exists()) {
                    okDirs = fRepertoire.mkdirs();
                }
                if (!okDirs) {
                    LOG.error(String.format("Impossible de creer le repertoire des medias :%s (verifier les droits d'ecriture sur le repertoire)", fRepertoire));
                }
                /* Valorisation du path et de l'url */
                String newPath = repertoire + File.separator + "v_";
                // si vignette auto on garde le meme nom
                if (StringUtils.isNotBlank(sVignetteName)) {
                    newPath += sVignetteName;
                } else {
                    newPath += sFileName;
                }
                FileUtil.copierFichier(tmpFile, new File(newPath), true);
                mediaBean.setUrlVignette("v_" + ((StringUtils.isNotBlank(sVignetteName)) ? sVignetteName : sFileName));
            }
        }
        if (mediaBean.getId() == null || mediaBean.getId() == 0L) {
            dao.add(mediaBean);
        } else {
            dao.update(mediaBean);
            // Suppression de l'ancien fichier physique
            if (suppressionFile) {
                if (StringUtils.isNotBlank(sOldAbsolutePathFile)) {
                    FileUtils.deleteQuietly(new File(sOldAbsolutePathFile));
                }
            }
            if (suppressionVignette) {
                if (StringUtils.isNotBlank(sOldAbsolutePathVignette)) {
                    FileUtils.deleteQuietly(new File(sOldAbsolutePathVignette));
                }
            }
        }
        serviceCorePublisher.publish(mediaBean,"media","save",mediaBean.getId());
    }

    @Override
    @Caching(evict = {
        @CacheEvict(value = "ServiceMedia.getCodeRubrique", key = "#root.target.getById(#id).getUrl()", beforeInvocation = true),
        @CacheEvict(value = "ServiceMedia.getById", key = "#id"),
    })
    public void delete(@Payload  final Long id) {
        super.delete(id);
        serviceCorePublisher.publish(id,"media","delete",id);
    }

    @Cacheable("ServiceMedia.getCodeRubrique")
    public String getCodeRubrique(String sUrl) {
        final List<MediaBean> medias = getByFilledCodeRubrique();
        for (MediaBean currentMedia : medias) {
            if (MediaUtils.isLocal(currentMedia) && sUrl.equals(currentMedia.getUrl())) {
                return currentMedia.getCodeRubrique();
            }
        }
        return null;
    }


    @Override
    @Cacheable("ServiceMedia.getById")
    public MediaBean getById(final Long id) {
        return dao.getById(id);
    }

    /**
     * Retrieves a traduction data according to the given property key.
     *
     * @param media : the {@link com.univ.objetspartages.bean.MediaBean} from which to retrieve the property from.
     * @param property : the key as a {@link String}.
     *
     * @return the specific data if found, StringUtils.EMPTY otherwise.
     */
    public String getTraductionData(final MediaBean media, final String property) {
        String sRes = StringUtils.EMPTY;
        final Properties properties = new Properties();
        try {
            properties.load(new ByteArrayInputStream(media.getTraductionData().getBytes()));
        } catch (final IOException e) {
            LOG.error("erreur de la lecture des properties du media", e);
        }
        if (properties.get(property) != null) {
            sRes = (String) properties.get(property);
        }
        return sRes;
    }

    /**
     * Gets the specific data according to the given property key.
     *
     * @param media : the {@link com.univ.objetspartages.bean.MediaBean} from which to retrieve the property from.
     * @param property : the key as a {@link String}.
     *
     * @return the specific data if found, StringUtils.EMPTY otherwise.
     */
    public String getSpecificData(final MediaBean media, final String property) {
        String sRes = StringUtils.EMPTY;
        final Properties properties = new Properties();
        try {
            properties.load(new ByteArrayInputStream(media.getSpecificData().getBytes()));
        } catch (final IOException e) {
            LOG.error("erreur de la lecture des properties du media", e);
        }
        if (properties.get(property) != null) {
            sRes = (String) properties.get(property);
        }
        return sRes;
    }

    /**
     * Retrieves specific data as a {@link String}.
     *
     * @param media : the {@link com.univ.objetspartages.bean.MediaBean} from which to retrieve the properties from.
     *
     * @return the specific data formatted as a {@link String}, StringUtils.EMPTY otherwise.
     */
    public String getSpecificDataAsString(final MediaBean media) {
        String sRes = StringUtils.EMPTY;
        final Properties properties = new Properties();
        try {
            properties.load(new ByteArrayInputStream(media.getSpecificData().getBytes()));
        } catch (final IOException e) {
            LOG.error("erreur de la lecture des properties du media", e);
        }
        final Enumeration<Object> e = properties.keys();
        while (e.hasMoreElements()) {
            final String propertie = (String) e.nextElement();
            final String value = properties.getProperty(propertie);
            if (value.length() > 0 && !"0".equals(value)) {
                if (sRes.length() > 0) {
                    sRes += '\n';
                }
                sRes += propertie.toLowerCase() + " : " + value;
            }
        }
        return sRes;
    }

    /**
     * Retrieves a {@link java.util.List} of {@link com.univ.objetspartages.bean.MediaBean} from a {@link com.univ.utils.sql.clause.ClauseWhere} object.
     * @param where : the {@link com.univ.utils.sql.clause.ClauseWhere} prepared.
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.MediaBean}
     */
    public List<MediaBean> getMediasFromWhere(final ClauseWhere where) {
        return dao.select(where.formaterSQL());
    }

    /**
     * Retrieves a {@link java.util.List} of {@link com.univ.objetspartages.bean.MediaBean} from a {@link com.univ.utils.sql.RequeteSQL} object.
     * @param request : the {@link com.univ.utils.sql.RequeteSQL} prepared.
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.MediaBean}
     */
    public List<MediaBean> getMediasFromRequest(final RequeteSQL request) {
        return dao.select(request.formaterRequete());
    }

    /**
     * Retrieves the {@link java.util.List} of all the persisted {@link com.univ.objetspartages.bean.MediaBean}.
     * @return the {@link java.util.List} of all the persisted {@link com.univ.objetspartages.bean.MediaBean}
     */
    public List<MediaBean> getAll() {
        return dao.getAll();
    }

    /**
     * Gets all the {@link com.univ.objetspartages.bean.MediaBean} whose section code is filled.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MediaBean}
     */
    public List<MediaBean> getByFilledCodeRubrique() {
        return dao.getByFilledCodeRubrique();
    }

    /**
     * Gets a {@link java.util.List} of {@link com.univ.objetspartages.bean.MediaBean} of the type passed as parameter.
     * @param type : the type to look for as a {@link String}
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MediaBean}
     */
    public List<MediaBean> getByResourceType(final String type) {
        return dao.getByResourceType(type);
    }

    /**
     * Update the persistence of an existing {@link com.univ.objetspartages.bean.MediaBean}.
     * @param media : the {@link com.univ.objetspartages.bean.MediaBean} to update.
     * @return the passed {@link com.univ.objetspartages.bean.MediaBean}.
     */
    @Caching(evict = {
        @CacheEvict(value = "ServiceMedia.getCodeRubrique", key = "#media.getUrl()"),
        @CacheEvict(value = "ServiceMedia.getById", key = "#media.getId()")
    })
    public MediaBean update(final MediaBean media) {
        return dao.update(media);
    }

    /**
     * Persist a {@link com.univ.objetspartages.bean.MediaBean} to the datasource.
     * @param media : the {@link com.univ.objetspartages.bean.MediaBean} to add.
     * @return the passed {@link com.univ.objetspartages.bean.MediaBean}.
     */
    public MediaBean add(final MediaBean media) {
        return dao.add(media);
    }

    public MediaBean addWithForceId(final MediaBean media) {
        return dao.addWithForcedId(media);
    }

    /**
     * Retrieves the {@link com.univ.objetspartages.bean.MediaBean} whose url matches the given one.
     * @param url : the url to look for as a {@link String}.
     * @return the {@link com.univ.objetspartages.bean.MediaBean} if found, null otherwise.
     */
    public MediaBean getByUrl(final String url) {
        return dao.getByUrl(url);
    }

    /**
     * Retrieves a {@link java.util.List} of {@link com.univ.objetspartages.bean.MediaBean} whose thumbnail is filled.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MediaBean}
     */
    public List<MediaBean> getByFilledUrlVignette() {
        return dao.getByFilledUrlVignette();
    }

    /**
     * Retrieves a {@link java.util.List} of {@link com.univ.objetspartages.bean.MediaBean} matching the given type and type media.
     * @param type : the type of the resource to look for as a {@link String}.
     * @param typeMedia : the media type to look for as a {@link String}.
     * @return the {@link java.util.List} of matching {@link com.univ.objetspartages.bean.MediaBean}
     */
    public List<MediaBean> getByTypeAndTypeMedia(final String type, final String typeMedia) {
        return dao.getByTypeAndTypeMedia(type, typeMedia);
    }

    /**
     * Gets the description of the given media in the given language. If the language is not the default language configured in the application or
     * if the media is shared, the method #getTraductionData(MediaBean, String) is invoked.
     * @param mediaBean : the media from which to retrieve the descripion from.
     * @param locale : the language as a {@link java.util.Locale}.
     * @return the found description as a {@link String}.
     * @see #getTraductionData(MediaBean, String)
     */
    public String getDescription(final MediaBean mediaBean, final Locale locale) {
        final int langue = LangueUtil.getIndiceLocale(locale);
        if (langue != 0 && Mediatheque.ETAT_MUTUALISE.equals(mediaBean.getIsMutualise())) {
            return getTraductionData(mediaBean, "DESCRIPTION_" + langue);
        } else {
            return mediaBean.getDescription();
        }
    }

    /**
     * Gets the title of the given media in the given language. If the language is not the default language configured in the application or
     * if the media is shared, the method #getTraductionData(MediaBean, String) is invoked.
     * @param mediaBean : the media from which to retrieve the descripion from.
     * @param locale : the language as a {@link java.util.Locale}.
     * @return the found title as a {@link String}.
     * @see #getTraductionData(MediaBean, String)
     */
    public String getTitre(final MediaBean mediaBean, final Locale locale) {
        final int langue = LangueUtil.getIndiceLocale(locale);
        if (langue != 0 && Mediatheque.ETAT_MUTUALISE.equals(mediaBean.getIsMutualise())) {
            return getTraductionData(mediaBean, "TITRE_" + langue);
        } else {
            return mediaBean.getTitre();
        }
    }

    /**
     * Gets the author of the given media in the given language. If the language is not the default language configured in the application or
     * if the media is shared, the method #getTraductionData(MediaBean, String) is invoked.
     * @param mediaBean : the media from which to retrieve the descripion from.
     * @param locale : the language as a {@link java.util.Locale}.
     * @return the found author as a {@link String}.
     * @see #getTraductionData(MediaBean, String)
     */
    public String getAuteur(final MediaBean mediaBean, final Locale locale) {
        final int langue = LangueUtil.getIndiceLocale(locale);
        if (langue != 0 && Mediatheque.ETAT_MUTUALISE.equals(mediaBean.getIsMutualise())) {
            return getTraductionData(mediaBean, "AUTEUR_" + langue);
        } else {
            return mediaBean.getAuteur();
        }
    }

    /**
     * Gets the copyright of the given media in the given language. If the language is not the default language configured in the application or
     * if the media is shared, the method #getTraductionData(MediaBean, String) is invoked.
     * @param mediaBean : the media from which to retrieve the descripion from.
     * @param locale : the language as a {@link java.util.Locale}.
     * @return the found copyright as a {@link String}.
     * @see #getTraductionData(MediaBean, String)
     */
    public String getCopyright(final MediaBean mediaBean, final Locale locale) {
        final int langue = LangueUtil.getIndiceLocale(locale);
        if (langue != 0 && Mediatheque.ETAT_MUTUALISE.equals(mediaBean.getIsMutualise())) {
            return getTraductionData(mediaBean, "COPYRIGHT_" + langue);
        } else {
            return mediaBean.getCopyright();
        }
    }

    /**
     * Gets the legend of the given media in the given language. If the language is not the default language configured in the application or
     * if the media is shared, the method #getTraductionData(MediaBean, String) is invoked.
     * @param mediaBean : the media from which to retrieve the descripion from.
     * @param locale : the language as a {@link java.util.Locale}.
     * @return the found legend as a {@link String}.
     * @see #getTraductionData(MediaBean, String)
     */
    public String getLegende(final MediaBean mediaBean, final Locale locale) {
        final int langue = LangueUtil.getIndiceLocale(locale);
        if (langue != 0 && Mediatheque.ETAT_MUTUALISE.equals(mediaBean.getIsMutualise())) {
            return getTraductionData(mediaBean, "LEGENDE_" + langue);
        } else {
            return mediaBean.getLegende();
        }
    }

    /**
     * Gets the metaKeywords of the given media in the given language. If the language is not the default language configured in the application or
     * if the media is shared, the method #getTraductionData(MediaBean, String) is invoked.
     * @param mediaBean : the media from which to retrieve the descripion from.
     * @param locale : the language as a {@link java.util.Locale}.
     * @return the found metaKeywords as a {@link String}.
     * @see #getTraductionData(MediaBean, String)
     */
    public String getMetaKeywords(final MediaBean mediaBean, final Locale locale) {
        final int langue = LangueUtil.getIndiceLocale(locale);
        if (langue != 0 && Mediatheque.ETAT_MUTUALISE.equals(mediaBean.getIsMutualise())) {
            return getTraductionData(mediaBean, "META_KEYWORDS_" + langue);
        } else {
            return mediaBean.getMetaKeywords();
        }
    }

    /**
     * Gets the count of matching {@link com.univ.objetspartages.bean.MediaBean}.
     * @param where : the {@link com.univ.utils.sql.clause.ClauseWhere} to match {@link com.univ.objetspartages.bean.MediaBean} against.
     * @return the found metaKeywords as a {@link String}.
     */
    public int getCountForWhere(final ClauseWhere where) {
        return dao.getCountFromWhere(where);
    }

    public void setServiceCorePublisher(final ServiceCorePublisher serviceCorePublisher) {
        this.serviceCorePublisher = serviceCorePublisher;
    }
}
