package com.univ.objetspartages.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.ServiceBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUtil;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * Created on 20/10/15.
 */
public class ServiceStructure implements ServiceBean<StructureModele> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceStructure.class);

    private ServiceMetatag serviceMetatag;

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    @Override
    public void save(final StructureModele bean) {
        throw new UnsupportedOperationException("Cette méthode n'est pas supportée par ce service. Utilisez les méthodes \"save\" et \"update\" de l'objet concerné.");
    }

    @Override
    public void delete(final Long id) {
        throw new UnsupportedOperationException("Cette méthode n'est pas supportée par ce service. Utilisez les méthodes \"delete\" de l'objet concerné.");
    }

    @Override
    public StructureModele getById(final Long id) {
        throw new UnsupportedOperationException("Cette méthode n'est pas supportée par ce service. Utilisez la méthode \"getByCode\" du service.");
    }

    @Caching(evict = {
        @CacheEvict(value = "ServiceStructure.getByCodeLanguage", key = "#structure.code + #structure.langue"),
        @CacheEvict(value = "ServiceStructure.getSubStructures", allEntries = true)
    })
    public void updateCache(final StructureModele structure) {
        LOGGER.debug("Eviction du cache pour la structure [" + structure.getLibelleLong() + ", " + structure.getCode() + "]");
    }

    public boolean isVisibleInFront(final StructureModele structure) {
        final MetatagBean meta = serviceMetatag.getByCodeAndIdFiche(ReferentielObjets.getCodeObjet(structure), structure.getIdFiche());
        return meta != null && "1".equals(meta.getMetaInTree());
    }

    /**
     * Contrôle le code des objets de type structure : - ne doit pas contenir des caractères interdits - ne doit pas déjà exister dans un autre objet de type Structure.
     * @param code : le code à tester sous la forme d'une {@link String}
     * @param langue : la langue de la structure à tester sous la forme d'une {@link String}
     * @return le code passé en paramètre s'il a passé tous les tests (!)
     * @throws Exception : si le code contient des caractères interdit ou s'il n'est pas unique au sein des structures
     * @see {@link com.univ.utils.Chaine#controlerCodeMetier(String)}
     */
    public String checkCode(final String code, final String langue) throws Exception {
        Chaine.controlerCodeMetier(code);
        FicheUniv ficheUniv;
        StructureModele structure;
        // controle de l'unicite
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
            if (ficheUniv != null && ficheUniv instanceof StructureModele) {
                structure = (StructureModele) ficheUniv;
                structure.init();
                final int count = ficheUniv.selectCodeLangueEtat(code, langue, "");
                if (count > 0) {
                    ficheUniv.nextItem();
                    throw new ErreurApplicative("Ce code est déjà utilisé pour la fiche " + ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjet(ficheUniv)) + " : " + ficheUniv.getLibelleAffichable());
                }
            }
        }
        return code;
    }

    /**
     * Vérifie des les relations parent / enfant pour l'arborescence des structures.
     * @param code : le code de la structure à vérifier sous la forme d'une {@link String}
     * @param langue : la langue de la structure sous la forme d'une {@link String}
     * @param codeRattachement : le code de rattachement vers une autre structure sous la forme d'une {@link String}
     * @throws ErreurApplicative : si les contraintes ne sont pas respectées (ex : si la structure se retrouve à contenir sa structure mère)
     */
    public void checkCycle(final String code, final String langue, final String codeRattachement) throws ErreurApplicative {
        final StructureModele infosStructureMere = getByCodeLanguage(codeRattachement, langue);
        final StructureModele infosStructure = getByCodeLanguage(code, langue);
        if(infosStructure != null && infosStructureMere!=null) {
            //On vérifie que la structure ne contient pas la structure mere.
            if (contains(infosStructure, infosStructureMere)) {
                throw new ErreurApplicative("La structure parente ne peut pas être fille de la structure courante.");
            }
        }
    }

    /**
     * Contrôle les permissions compte-tenu des autorisations.
     * @param autorisations : un {@link com.univ.objetspartages.om.AutorisationBean} sur lequel effectuer la vérification.
     * @param sPermission : la permission à vérifier sous la forme d'une {@link String}
     * @param codeStructure : le code de la structure sous la forme d'une {@link String}
     * @return true si les autorisations font bien état de la permission de modification ou de création, false sinon.
     */
    public boolean checkPermission(final AutorisationBean autorisations, final String sPermission, final String codeStructure) {
        PermissionBean permissionCourante = new PermissionBean(sPermission);
        final Perimetre perimetre = new Perimetre(codeStructure, "", "", "", "");
        boolean res = autorisations.possedePermissionPartielleSurPerimetre(permissionCourante, perimetre);
        if (!res && "FICHE".equals(permissionCourante.getType())) {
            if ("M".equals(permissionCourante.getAction())) {
                permissionCourante = new PermissionBean(permissionCourante.getType(), permissionCourante.getObjet(), "C");
                if (autorisations.possedePermissionPartielleSurPerimetre(permissionCourante, perimetre)) {
                    res = true;
                }
            }
        }
        return res;
    }

    /**
     * Récupération du code de la composante d'une structure.
     * @param code : le code de la structure à partir de laquelle chercher sous la forme d'une {@link String}.
     * @param langue : la langue de la structure sous la forme d'une {@link String}.
     * @return un {@link com.univ.objetspartages.om.StructureModele} si trouvé, null sinon.
     */
    public StructureModele getStructurePremierNiveau(final String code, final String langue) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        // On recherche la composante
        StructureModele structure = getByCodeLanguage(code, langue);
        while (getLevel(structure) > 1) {
            structure = getByCodeLanguage(structure.getCodeRattachement(), langue);
        }
        return structure;
    }

    /**
     * Récupération d'une fiche structure en base à partir de son code et de sa langue. Un calcul est effectué pour savoir si l'on est en front-office ou non.
     * Si l'on est en front-office, on cherche à récupérer la fiche dans un état "En ligne". L'état n'est pas pris en compte en back-office.
     * @param code : code de la fiche sous la forme d'une {@link String}
     * @param langue : langue de la fiche sous la forme d'une {@link String}
     * @return la fiche découverte sous la forme d'un {@link com.univ.objetspartages.om.StructureModele}, null sinon.
     */
    public StructureModele getByCodeLanguage(final String code, final String langue) {
        boolean etatEnLigne = false;
        // si le contexte est ContexteUniv, on est en front-office, donc on ne
        // renvoie pas la fiche si elle n'existe pas en ligne
        if (ContexteUtil.getContexteUniv() != null) {
            etatEnLigne = true;
        }
        return getByCodeLanguage(code, langue, etatEnLigne);
    }

    /**
     * Récupération d'une fiche structure en base à partir de son code et de sa langue.
     * @param code : code de la fiche sous la forme d'une {@link String}
     * @param language : langue de la fiche sous la forme d'une {@link String}
     * @param online : permet de spécifier si on souhaite uniquement les fiches en ligne (true), ou non (false)
     * @return la fiche découverte sous la forme d'un {@link com.univ.objetspartages.om.StructureModele}, null sinon.
     */
    @Cacheable(value = "ServiceStructure.getByCodeLanguage", key = "#code + #language")
    public StructureModele getByCodeLanguage(final String code, final String language, final boolean online) {
        StructureModele res = null, structure;
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        final String langue = StringUtils.defaultIfEmpty(language, LangueUtil.getIndiceLocaleDefaut());
        FicheUniv ficheUniv;
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            if (res == null) {
                ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
                if (ficheUniv != null && ficheUniv instanceof StructureModele) {
                    structure = (StructureModele) ficheUniv;
                    structure.init();
                    try {
                        // On cherche d'abord la version en ligne puis les autres versions
                        int count = ficheUniv.selectCodeLangueEtat(code, langue, "0003");
                        if (count == 0 && !online) {
                            count = ficheUniv.selectCodeLangueEtat(code, langue, StringUtils.EMPTY);
                        }
                        if (count > 0) {
                            while (structure.nextItem()) {
                                res = structure;
                            }
                        }
                    } catch (final Exception e) {
                        LOGGER.error("Une erreur est survenue lors de la récupération de la structure portant le code " + code, e);
                        return null;
                    }
                }
            }
        }
        return res;
    }

    /**
     * Retourne une chaine de caractère affichable pour l'ensemble des codes passés en paramètres.
     * @param codes : les codes pour lesquels calculer l'affichage sous la forme d'une {@link String}, chaque code étant séparé par ";"
     * @param langue : la langue sous la forme d'une {@link String}
     * @return la {@link String} calculée ou "-"
     */
    public String getDisplayableLabel(final String codes, final String langue) {
        if (StringUtils.isNotBlank(codes)) {
            final List<String> labels = new ArrayList<>();
            final String[] codesArray = codes.split(";");
            for (final String code : codesArray) {
                final StructureModele structure = getByCodeLanguage(code, langue);
                if (structure != null) {
                    String libelleAffichable = structure.getLibelleLong();
                    // Pas de structure chargée en mémoire (peut-être
                    // dans un autre état (type brouillon) : on cherche dans la base
                    if (StringUtils.isBlank(libelleAffichable)) {
                        libelleAffichable = structure.getLibelleAffichable();
                    }
                    labels.add(StringUtils.defaultIfBlank(libelleAffichable, "-"));
                }
            }
            return CollectionUtils.isNotEmpty(labels) ? StringUtils.join(labels, ";") : "-";
        }
        return StringUtils.EMPTY;
    }

    /**
     * Récupération du libellé de la composante d'une structure.
     * @param code : le code à partir duquel calculer le libellé sous la forme d'une {@link String}
     * @param langue : la langue dans laquelle calculer le libellé sous la forme d'une {@link String} représentant l'index de la local (ex: "0")
     * @return le libellé calculé sous la forme d'une {@link String}
     */
    public String getFirstLevelLabel(final String code, final String langue) {
        String res = "-";
        if (StringUtils.isEmpty(code)) {
            return res;
        }
        /* On recherche la composante */
        final StructureModele structure = getStructurePremierNiveau(code, langue);
        res = getDisplayableLabel(structure.getCode(), langue);
        return res;
    }

    /**
     * Récupération du libellé de rattachement (Concaténation composante Département ...)
     * @param code : le code à partir duquel calculer le libellé sous la forme d'une {@link String}
     * @param langue : la langue dans laquelle calculer le libellé sous la forme d'une {@link String} représentant l'index de la local (ex: "0")
     * @param inclureStructureDepart : un {@link boolean} indiquant si la structure de départ doit être visible dans le libellé.
     * @return le libellé calculé sous la forme d'une {@link String}
     */
    public String getAttachementLabel(final String code, final String langue, final boolean inclureStructureDepart) {
        String res = StringUtils.EMPTY;
        if (StringUtils.isBlank(code)) {
            return res;
        }
        // On lit les structures dans le sens ascendant
        StructureModele structure = getByCodeLanguage(code, langue);
        if (inclureStructureDepart) {
            res = getDisplayableLabel(structure.getCode(), langue);
        }
        while (getLevel(structure) > 1) {
            structure = getByCodeLanguage(structure.getCodeRattachement(), langue);
            if (res.length() > 0) {
                res = "<br />" + res;
            }
            res = getDisplayableLabel(structure.getCode(), langue) + res;
        }
        return res;
    }

    /**
     * Construit la liste agrégée de l'ensemble des structures.
     * @param front : un {@link boolean} indiquant si seules les fiches à l'état "0003" (en ligne) doivent être récupérée.
     * @return une {@link java.util.Map} de pair &lt;{@link String}, {@link com.univ.objetspartages.om.StructureModele}&gt; correspondant à &lt;<i>code_structure</i>, <i>structure</i>&gt;.
     */
    public Map<String, StructureModele> getAllStructures(final boolean front) {
        final Map<String, StructureModele> listeStructure = new HashMap<>();
        try {
            // Charge en mémoire les listes des différents types de structures
            for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
                final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
                if (ficheUniv != null && ficheUniv instanceof StructureModele) {
                    final StructureModele structure = (StructureModele) ficheUniv;
                    structure.select(front ? "where ETAT_OBJET = '0003'" : StringUtils.EMPTY);
                    while (structure.nextItem()) {
                        listeStructure.put(structure.getCode(), (StructureModele)BeanUtils.cloneBean(structure));
                    }
                }
            }
        } catch (final Exception e) {
            LOGGER.error("Une erreur est survenue lors du calcul de la liste agrégée des structures", e);
        }
        return listeStructure;
    }

    /**
     * Construit la liste agrégée de l'ensemble des structures disponible en front office (etat == 0003).
     * @return une {@link java.util.Map} de pair &lt;{@link String}, {@link com.univ.objetspartages.om.StructureModele}&gt; correspondant à &lt;<i>code_structure</i>, <i>structure</i>&gt;.
     * @see {@link #getAllStructures(boolean)}
     */
    public Map<String, StructureModele> getAllStructures() {
        return getAllStructures(true);
    }

    /**
     * Calcule l'ensemble des parents de la structure fournie en paramètre.
     * @param codeStructure la structure dont on souhaite connaitre les parents.
     * @param langue : la langue dans laquelle récupérer les enfants sous la forme d'une {@link String}.
     * @return l'ensemble des parents ou une collection vide si rien n'est trouvé.
     */
    public List<StructureModele> getAllAscendant(final String codeStructure, final String langue) {
        final StructureModele currentSection = getByCodeLanguage(codeStructure, langue);
        return getAllAscendant(currentSection);
    }

    /**
     * Même comportement que la méthode {@link #getAllAscendant(String,String)} mais en manipulant directement une structure.
     * Elle permet d'éviter une requête en base.
     * @param StructureModele ma structure dont on souhaite connaitre les parents
     * @return l'ensemble des parents ou une collection vide si rien n'est trouvé.
     */
    public List<StructureModele> getAllAscendant(final StructureModele structureBean) {
        final List<StructureModele> result = new ArrayList<>();
        StructureModele parent = null;
        if (structureBean != null && StringUtils.isNotBlank(structureBean.getCodeRattachement())) {
            parent = getByCodeLanguage(structureBean.getCodeRattachement(), structureBean.getLangue());
        }
        if (parent != null) {
            result.add(parent);
            while (parent != null && StringUtils.isNotBlank(parent.getCodeRattachement())) {
                parent = getByCodeLanguage(parent.getCodeRattachement(), structureBean.getLangue());
                if (parent != null) {
                    result.add(parent);
                }
            }
        }
        return result;
    }
    
    /**
     * Récupère les enfants immédiats de la structure identifiée par le code passé en paramètre.
     * @param parentCode : le code de la structure pour laquelle on doit récupérer les enfants sous la forme d'une {@link String}.
     * @param langue : la langue dans laquelle récupérer les enfants sous la forme d'une {@link String}.
     * @param front : seul les enfants à l'état "en ligne" (0003) seront récupéré, tous sinon.
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.om.StructureModele} représentant les enfants récupérés.
     */
    @Cacheable(value = "ServiceStructure.getSubStructures", key = "#parentCode + #langue")
    public List<StructureModele> getSubStructures(final String parentCode, final String langue, final boolean front) {
        final List<StructureModele> subStructures = new ArrayList<>();
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
            if (ficheUniv != null && ficheUniv instanceof StructureModele) {
                try {
                    ClauseWhere where = new ClauseWhere(ConditionHelper.egalVarchar("CODE_RATTACHEMENT", parentCode));
                    where.and(ConditionHelper.egalVarchar("LANGUE", langue));
                    if (front) {
                        where.and(ConditionHelper.egalVarchar("ETAT_OBJET", "0003"));
                    }
                    final StructureModele structure = (StructureModele) ficheUniv;
                    structure.select(where.formaterSQL());
                    while (structure.nextItem()) {
                        final StructureModele structureModele = (StructureModele) BeanUtils.cloneBean(structure);
                        subStructures.add(structureModele);
                    }
                } catch (final Exception e) {
                    LOGGER.error("Une erreur est survenue lors du calcul de la liste des sous-structures de " + parentCode, e);
                }
            }
        }
        return subStructures;
    }

    /**
     * Récupère les enfants tous niveaux confondus de la structure identifiée par le code passé en paramètre.
     * @param parentCode : le code de la structure pour laquelle on doit récupérer les enfants sous la forme d'une {@link String}.
     * @param langue : la langue dans laquelle récupérer les enfants sous la forme d'une {@link String}.
     * @param front : seul les enfants à l'état "en ligne" (0003) seront récupéré, tous sinon.
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.om.StructureModele} représentant les enfants récupérés.
     */
    public List<StructureModele> getAllSubStructures(final String parentCode, final String langue, final boolean front) {
        final List<StructureModele> subStructures = new ArrayList<>();
        final List<StructureModele> children = getSubStructures(parentCode, langue, front);
        subStructures.addAll(children);
        for(final StructureModele currentChild : children) {
            subStructures.addAll(getAllSubStructures(currentChild.getCode(), langue, front));
        }
        return subStructures;
    }

    /**
     * Calcul le fil d'ariane de la structure passée en paramètre dans la langue souhaitée.
     * @param codeStructure : le code de la structure pour laquelle on doit calculer le fil d'ariane sous la forme d'une {@link String}.
     * @param langue : la langue dans laquelle calculer le fil d'ariane sous la forme d'une {@link String}.
     * @return le fil d'ariane sous la forme d'une {@link String} ou une chaîne vide sinon.
     */
    public String getBreadCrumbs(final String codeStructure, final String langue) {
        String filAriane = StringUtils.EMPTY;
        final String separateur = " &gt; ";
        // On lit les structures dans le sens ascendant
        StructureModele structure = getByCodeLanguage(codeStructure, langue);
        String libelleStructure;
        while (getLevel(structure) > 0) {
            if (filAriane.length() > 0) {
                filAriane = separateur + filAriane;
            }
            libelleStructure = StringUtils.isNotBlank(structure.getLibelleCourt()) ? structure.getLibelleCourt() : structure.getLibelleLong();
            filAriane = libelleStructure + filAriane;
            structure = getByCodeLanguage(structure.getCodeRattachement(), langue);
        }
        return filAriane;
    }

    /**
     * Calcul le niveau de la structure passée en paramètre dans l'arborescence de structure. Par convention, une structure "null" renverra "0".
     * @param structure : le {@link com.univ.objetspartages.om.StructureModele} pour lequel calculer le niveau.
     * @return un {@link int} représentant le niveau calculer
     */
    public int getLevel(final StructureModele structure) {
        if(structure == null) {
            return 0;
        }
        int level = 1;
        if(StringUtils.isNotBlank(structure.getCodeRattachement())) {
            final StructureModele parent = getByCodeLanguage(structure.getCodeRattachement(), structure.getLangue());
            level += getLevel(parent);
        }
        return level;
    }

    /**
     * Vérifie si la structure "structureParent" contient la structure "structure" (tous niveaux confondus).
     * @param structureParent : un {@link com.univ.objetspartages.om.StructureModele} représentant le parent.
     * @param structure : un {@link com.univ.objetspartages.om.StructureModele}
     * @return true si le parent contient bien l'enfant, false sinon.
     */
    public boolean contains(final StructureModele structureParent, final StructureModele structure) {
        if(structureParent == null) {
            return true;
        }
        final List<StructureModele> children = getAllSubStructures(structureParent.getCode(), structure.getLangue(), true);
        for(final StructureModele currentChild : children) {
            if(currentChild.getCode().equals(structure.getCode())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Renvoit une l'ensemble des [<i>code_structure</i>, <i>libellé_structure</i>] des structures de l'application.
     * @return une {@link java.util.Map} de &lt;{@link String}, {@link String}&gt;
     */
    public Hashtable<String, String> getStructureListWithFullLabels() {
        final Hashtable<String, String> res = new Hashtable<>();
        for (final StructureModele currentStructure : getAllStructures().values()) {
            String libelle = getAttachementLabel(currentStructure.getCode(), currentStructure.getLangue(), true);
            libelle = StringUtils.replace(libelle, "<br />", " > ");
            res.put(currentStructure.getCode(), libelle);
        }
        return res;
    }

}
