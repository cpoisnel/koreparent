package com.univ.objetspartages.services;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.AbstractServiceBean;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.security.MySQLHelper;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RoleBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.dao.impl.UtilisateurDAO;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.util.InfosRolesUtils;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * Created on 13/04/15.
 */
public class ServiceUser extends AbstractServiceBean<UtilisateurBean, UtilisateurDAO> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceUser.class);

    protected static final String SOURCE_AUTHENTIFICATION_PROPERTY = "authentification.source";

    protected static final String SOURCE_AUTHENTIFICATION_CLEARTRUST = "ctrust";

    protected static final String SOURCE_AUTHENTIFICATION_CAS = "cas";

    protected static final String SOURCE_AUTHENTIFICATION_LDAP = "ldap";

    public static final String UTILISATEUR_ANONYME = "ANONYME";

    protected ServiceUserPass serviceUserPass;

    protected ServiceGroupeUtilisateur serviceGroupeUtilisateur;

    protected ServiceRole serviceRole;

    protected ServiceGroupeDsi serviceGroupeDsi;

    public void setServiceUserPass(final ServiceUserPass serviceUserPass) {
        this.serviceUserPass = serviceUserPass;
    }

    public void setServiceGroupeUtilisateur(final ServiceGroupeUtilisateur serviceGroupeUtilisateur) {
        this.serviceGroupeUtilisateur = serviceGroupeUtilisateur;
    }

    public void setServiceRole(final ServiceRole serviceRole) {
        this.serviceRole = serviceRole;
    }

    public void setServiceGroupeDsi(final ServiceGroupeDsi serviceGroupeDsi) {
        this.serviceGroupeDsi = serviceGroupeDsi;
    }

    /**
     * Compute the max length of a pass as described in the application's properties.
     * @return an int as defined in the properties or 64 as a default value.
     * @see ServiceUserPass#getLongueurChampMotDePasse()
     */
    public static int getLongueurChampMotDePasse() {
        return ServiceUserPass.getLongueurChampMotDePasse();
    }

    /**
     * Retrieve new pass request form's field to generate a pass request.
     * @param infoBean : an {@link InfoBean} object containing all needed informations.
     * @throws ErreurApplicative : if zero or more than one user are found.
     * @see ServiceUserPass#requestNewPass(UtilisateurBean, String)
     */
    public void requestNewPass(final InfoBean infoBean) throws ErreurApplicative {
        final String email = infoBean.getString("EMAIL");
        final EmailValidator validator = new EmailValidator();
        if (!validator.isValid(email, null)) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("JTF_ERR_FMT_EMAIL"));
        }
        final String code = infoBean.getString("LOGIN");
        final List<UtilisateurBean> users = getByMailAndCode(email, code);
        if (users != null && users.size() == 1) {
            serviceUserPass.requestNewPass(users.get(0), email);
        } else {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("ST_ERR_DEMANDE_MDP_COMPTES"));
        }
    }

    /**
     * Handles an existing pass request : reset user's pass and exposes new infos.
     * @param requestId : a {@link String} representing the request UUID.
     * @return a {@link Map} containing useful informations for user display or an empty map if an error occured.
     */
    public Map<String, String> handlePassRequest(final String requestId) {
        Map<String, String> demande = Collections.emptyMap();
        if (isValidUUID(requestId)) {
            demande = serviceUserPass.handlePassRequest(UUID.fromString(requestId));
            final UtilisateurBean user = getByCode(demande.get("code"));
            try {
                if (user != null) {
                    user.setMotDePasse(MySQLHelper.encodePassword(demande.get("motDePasse")));
                    if (StringUtils.isBlank(user.getAdresseMail())) {
                        user.setAdresseMail(demande.get("email"));
                    }
                    save(user);
                    demande.put("nom", user.getNom());
                    demande.put("prenom", user.getPrenom());
                }
            } catch (final UnsupportedEncodingException e) {
                LOG.debug("unable to obfuscate the password", e);
                LOG.error("Une erreur est survenue lors de l'encodage du mot de passe");
                demande = Collections.emptyMap();;
            }
        }
        return demande;
    }

    private boolean isValidUUID(final String requestId) {
        boolean result = Boolean.FALSE;
        if (StringUtils.isNotBlank(requestId)) {
            try {
                UUID fromStringUUID = UUID.fromString(requestId);
                result = requestId.equals(fromStringUUID.toString());
            } catch (IllegalArgumentException iae) {
                LOG.debug("the given requestId is invalid", iae);
                result = Boolean.FALSE;
            }
        }
        return result;
    }

    /**
     * Retrieve a mail address according to the given user code.
     * @param codeUtilisateur : the user code as a {@link String}.
     * @return the mail address if found, an empty String otherwise.
     */
    public String getAdresseMail(final String codeUtilisateur) {
        final UtilisateurBean utilisateurBean = dao.getByCode(codeUtilisateur);
        if (utilisateurBean != null) {
            return utilisateurBean.getAdresseMail();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Retrieve a mail address according to the given user code.
     * @param codeUtilisateur : the user code as a {@link String}.
     * @param meta if the param codeUtilisateur is empty or if its equals to {@link ServiceUser#UTILISATEUR_ANONYME}, the meta is used to retrieve the mail. it can be null.
     * @return the mail address if found, an empty String otherwise.
     */
    public String getAdresseMail(final String codeUtilisateur, final MetatagBean meta) {
        if (StringUtils.isNotEmpty(codeUtilisateur) && !ServiceUser.UTILISATEUR_ANONYME.equals(codeUtilisateur)) {
            return getAdresseMail(codeUtilisateur);
        } else if (meta != null) {
            return meta.getMetaMailAnonyme();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Compute a user label according to its code.
     * @param codeUtilisateur : the user code as a {@link String}.
     * @return computed {@link String} if the user was found.
     */
    public String getLibelle(final String codeUtilisateur) {
        final UtilisateurBean utilisateurBean = dao.getByCode(codeUtilisateur);
        return getLibelle(utilisateurBean);
    }

    /**
     * Compute a user label.
     * @param user : the user as a {@link UtilisateurBean} to compute the label for.
     * @return computed {@link String} or {@link StringUtils#EMPTY} if user was null.
     */
    public String getLibelle(final UtilisateurBean user) {
        if (user != null) {
            return String.format("%s %s", user.getPrenom(), user.getNom());
        }
        return StringUtils.EMPTY;
    }

    /**
     * Update mail and password according to the given code. This method is "null-safe".
     * @param code : the code of the {@link UtilisateurBean} to update.
     * @param mail : the new mail as a {@link String}.
     * @param password : the clear version of the password. {@link MySQLHelper#encodePassword(String)} will be used to obfuscate this parameter.
     * @throws ErreurApplicative : if the password doesn't match the prerequisites (configured by "utilisateur.password.longueur.max" and "utilisateur.password.longueur.min" core properties.
     * @throws UnsupportedEncodingException : if the password couldn't be obfuscated.
     */
    public void changerMailPassword(final String code, final String mail, final String password) throws ErreurApplicative, UnsupportedEncodingException {
        final UtilisateurBean utilisateurBean = dao.getByCode(code);
        if (utilisateurBean != null) {
            if (StringUtils.isNotBlank(password)) {
                checkPass(password);
                utilisateurBean.setMotDePasse(MySQLHelper.encodePassword(password));
            }
            if (StringUtils.isNotBlank(mail)) {
                utilisateurBean.setAdresseMail(mail);
            }
            save(utilisateurBean);
        }
    }

    /**
     * Check the given password against application rules (i.e utilisateur.password.longueur.max, utilisateur.password.longueur.min).
     * @param password : the password to check as a {@link String}
     * @throws ErreurApplicative : exception thrown if:
     * <ul>
     *     <li>The password's length doesn't fulfill utilisateur.password.longueur.max condition</li>
     *     <li>The password's length doesn't fulfill utilisateur.password.longueur.min condition</li>
     * </ul>
     */
    public void checkPass(final String password) throws ErreurApplicative {
        final int maxLength = PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.max", -1);
        final int minLength = PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.min", -1);
        if (maxLength != -1 && password.length() > maxLength) {
            throw new ErreurApplicative(String.format("Votre mot de passe ne doit pas dépasser %d caractères.", maxLength));
        }
        if (minLength != -1 && password.length() < minLength) {
            throw new ErreurApplicative(String.format("Votre mot de passe doit contenir au minimum %d caractères.", minLength));
        }
    }

    /**
     * Get a {@link Collection} of {@link UtilisateurBean} fulfilling a {@link PermissionBean} constraint.
     * @param permission : a {@link PermissionBean} to test users against.
     * @return a {@link Collection} of {@link UtilisateurBean} or an empty {@link Collection}.
     * @throws Exception
     */
    public List<UtilisateurBean> getUtilisateursPossedantPermission(final PermissionBean permission) throws Exception {
        final List<UtilisateurBean> result = new ArrayList<>();
        final Set<String> listeRoles = new TreeSet<>();
        final Set<String> listeGroupes = new TreeSet<>();
        final List<RoleBean> rolesByPermission = serviceRole.getByPermission(permission.getChaineSerialisee());
        if (CollectionUtils.isEmpty(rolesByPermission)) {
            return result;
        }
        for (final RoleBean role : rolesByPermission) {
            listeRoles.add(role.getCode());
                /* Stockage des groupes contenant le role */
            final Map<String, List<Perimetre>> hGroupes = serviceGroupeDsi.renvoyerGroupesEtPerimetres(role.getCode(), Collections.<String>emptyList(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
            for (final String codeGroupe : hGroupes.keySet()) {
                listeGroupes.add(codeGroupe);
            }
        }
        final Collection<String> codeUtilisateursGroupe = serviceGroupeUtilisateur.getAllUserCodeByGroup(listeGroupes);
        for (final String codeUtilisateur : codeUtilisateursGroupe) {
            final UtilisateurBean utilisateur = dao.getByCode(codeUtilisateur);
            if (utilisateur != null && StringUtils.isNotBlank(utilisateur.getCode())) {
                result.add(utilisateur);
            }
        }
        if (CollectionUtils.isNotEmpty(listeRoles)) {
            /* 1 - sélection des utilisateur par requete sur leurs roles */
            final ClauseWhere whereRoles = new ClauseWhere();
            for (final String codeRole : listeRoles) {
                whereRoles.or(ConditionHelper.like("ROLES", codeRole, "%[", ";%"));
            }
            /* 2 - pour chaque utilisateur, analyse du périmètre de chaque role */
            final Collection<UtilisateurBean> users = dao.select(whereRoles.formaterSQL());
            result.addAll(users);
        }
        return result;
    }

    /**
     * Get a {@link Collection} of {@link String} from each {@link UtilisateurBean} fulfilling the conditions.
     * The string can be of the following forms :
     * <ul>
     *     <li><i>mail</i>;<i>nom</i> <i>prenom</i>;<i>code</i></li>
     *     <li><i>code</i></li>
     * </ul>
     * @param permission : a {@link PermissionBean} representing the set of permissions a user must have to be eligible.
     * @param codesStructures : a {@link List} of structures codes ({@link String}) acting as permissions perimeter.
     * @param codeRubrique : the {@link com.univ.objetspartages.bean.RubriqueBean} code as a string {@link String} acting as permissions perimeter.
     * @param publicsVises : a {@link String} representing targeted audience.
     * @param codeEspaceCollaboratif :
     * @param renvoyerMails : a boolean indicating whether or not a e-mail should be sent.
     * @return a {@link Collection} of {@link String} as described below.
     * @throws Exception
     */
    public Collection<String> getListeUtilisateursPossedantPermission(final PermissionBean permission, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif, final boolean renvoyerMails) throws Exception {
        final Set<String> res = new TreeSet<>();
        final Set<String> listeRoles = new TreeSet<>();
        final Set<String> listeGroupes = new TreeSet<>();
        /* Selection des roles comprenant la permission */
        final List<RoleBean> rolesByPermission = serviceRole.getByPermission(permission.getChaineSerialisee());
        if (CollectionUtils.isEmpty(rolesByPermission)) {
            return res;
        }
        for (final RoleBean role : rolesByPermission) {
            LOG.debug(String.format("getListeUtilisateursPossedantPermission role %s", role.getCode()));
            listeRoles.add(role.getCode());
                /* Stockage des groupes contenant le role */
            final Map<String, List<Perimetre>> hGroupes = serviceGroupeDsi.renvoyerGroupesEtPerimetres(role.getCode(), codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif);
            listeGroupes.addAll(hGroupes.keySet());
        }
        final Collection<String> listeUtilisateursGroupe = serviceGroupeUtilisateur.getAllUserCodeByGroup(listeGroupes);
        for (final String codeUtilisateur : listeUtilisateursGroupe) {
            if (renvoyerMails) {
                final UtilisateurBean ut = dao.getByCode(codeUtilisateur);
                if (ut != null) {
                    res.add(String.format("%s;%s %s;%s", ut.getAdresseMail(), ut.getNom(), ut.getPrenom(), ut.getCode()));
                }
            } else {
                res.add(codeUtilisateur);
            }
        }
        /** *************************************** */
        /* PHASE 2 - ANALYSE PAR UTILISATEUR        */
        /** *************************************** */
        /* Construction de la requete sur les utilisateurs */
        if (!listeRoles.isEmpty()) {
            /* 1 - sélection des utilisateur par requete sur leurs roles */
            final ClauseWhere whereRoles = new ClauseWhere();
            for (final String codeRole : listeRoles) {
                whereRoles.or(ConditionHelper.like("ROLES", codeRole, "%[", ";%"));
            }
            /* 2 - pour chaque utilisateur, analyse du périmètre de chaque role */
            final Collection<UtilisateurBean> utilisateurBeans = dao.select(whereRoles.formaterSQL());
            for (final UtilisateurBean currentUtilisateur : utilisateurBeans) {
                for (final String listeRole : listeRoles) {
                    /* Controle périmetre sur chaque role */
                    final List<Perimetre> v = InfosRolesUtils.renvoyerPerimetresAffectation(currentUtilisateur.getRoles(), listeRole, codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif);
                    if (!v.isEmpty()) {
                        if (renvoyerMails) {
                            res.add(String.format("%s;%s %s;%s", currentUtilisateur.getAdresseMail(), currentUtilisateur.getNom(), currentUtilisateur.getPrenom(), currentUtilisateur.getCode()));
                        } else {
                            res.add(currentUtilisateur.getCode());
                        }
                    }
                }
            }
        }
        return res;
    }

    /**
     * Retrieve a {@link UtilisateurBean} given his code.
     * @param code : the code to look for as a {@link String}.
     * @return the {@link UtilisateurBean} if found, null otherwise.
     */
    public UtilisateurBean getByCode(final String code) {
        return dao.getByCode(code);
    }

    /**
     * Determine whether or not alteration is allowed according to current auth source.
     * @param utilisateurBean : the {@link UtilisateurBean} to test.
     * @return true if alteration is allowed, false otherwise.
     */
    public boolean isAlterationForbiddenBySource(final UtilisateurBean utilisateurBean) {
        final String authSource = PropertyHelper.getCoreProperty(SOURCE_AUTHENTIFICATION_PROPERTY);
        final boolean ldapActif = StringUtils.equals(authSource, SOURCE_AUTHENTIFICATION_LDAP);
        final boolean casActif = StringUtils.equals(authSource, SOURCE_AUTHENTIFICATION_CAS);
        final boolean cleartrustActif = StringUtils.equals(authSource, SOURCE_AUTHENTIFICATION_CLEARTRUST);
        return fromLdap(utilisateurBean) && (ldapActif || casActif || cleartrustActif);
    }

    /**
     * Test whether or not the passed {@link UtilisateurBean} comes from LDAP sync.
     * @param utilisateurBean : the {@link UtilisateurBean} to test.
     * @return true if the user was created by LDAP sync, false otherwise.
     */
    public boolean fromLdap(final UtilisateurBean utilisateurBean) {
        return utilisateurBean != null && StringUtils.isNotBlank(utilisateurBean.getCodeLdap());
    }

    /**
     * Retrieve {@link UtilisateurBean#centresInteret} as a {@link Collection} of {@link String}.
     * @param utilisateurBean : the {@link UtilisateurBean} to get collection from.
     * @return a {@link Collection} of {@link String}
     */
    public Collection<String> getCentresInteret(final UtilisateurBean utilisateurBean) {
        Collection<String> result = Collections.emptyList();
        if (utilisateurBean != null && StringUtils.isNotEmpty(utilisateurBean.getCentresInteret())) {
            final String[] st = utilisateurBean.getCentresInteret().split(";");
            result = Arrays.asList(st);
        }
        return result;
    }

    /**
     * Retrieves the {@link com.univ.objetspartages.bean.UtilisateurBean} corresponding to the given ldap code.
     * @param codeLdap : the ldap code to look for as a {@link String}.
     * @return the {@link com.univ.objetspartages.bean.UtilisateurBean} if found, null otherwise.
     */
    public UtilisateurBean getByCodeLdap(final String codeLdap) {
        return dao.getByCodeLdap(codeLdap);
    }

    /**
     * Retrieves the {@link com.univ.objetspartages.bean.UtilisateurBean} corresponding to the given name.
     * @param nom : the name to look for as a {@link String}.
     * @return the {@link com.univ.objetspartages.bean.UtilisateurBean} if found, null otherwise.
     */
    public List<UtilisateurBean> getByNom(final String nom) {
        return dao.getByNom(nom);
    }

    /**
     * Retrieves the {@link com.univ.objetspartages.bean.UtilisateurBean} matching the given parameters.
     * @param email : the email address to look for as a {@link String}
     * @param pass : the pass to look for as a {@link String}
     * @param code : the code of the user to look for as a {@link String}
     * @return the {@link com.univ.objetspartages.bean.UtilisateurBean} if found, null otherwise.
     */
    public UtilisateurBean getUser(final String email, final String pass, final String code) {
        return dao.getUser(email, pass, code);
    }

    /**
     * Retrieves a list of users matching the given conditions.
     * @param email : the email address to look for as a {@link String}
     * @param code : the code of the user to look for as a {@link String}
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.UtilisateurBean}
     */
    public List<UtilisateurBean> getByMailAndCode(final String email, final String code) {
        return dao.getByMailAndCode(email, code);
    }

    /**
     * Checks if one of the user's groups belongs to the given group code.
     * @param codeGroupe : a group code as a {@link String}
     * @param groupesUtilisateurs : a list of group codes belonging to a user as a {@link java.util.List} of {@link String}
     * @return true if one of the given groups belong to the given group, false otherwise.
     */
    public boolean appartientAGroupeOuSousGroupes(final String codeGroupe, final List<String> groupesUtilisateurs) {
        // Parcours du vecteur de groupes de l'utilisateurs
        final GroupeDsiBean groupeDsiBean = serviceGroupeDsi.getByCode(codeGroupe);
        for (final String currentCode : groupesUtilisateurs) {
            final GroupeDsiBean currentGroup = serviceGroupeDsi.getByCode(currentCode);
            if (serviceGroupeDsi.contains(groupeDsiBean, currentGroup)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieves a user according to the given ldap code and the given source.
     * @param codeLdap : the ldap code as a {@link String}
     * @param source : the authentification source as a {@link String}
     * @return a {@link com.univ.objetspartages.bean.UtilisateurBean} if found, null otherwise.
     */
    public UtilisateurBean getByCodeLdap(final String codeLdap, final String source) {
        return dao.getByCodeLdap(codeLdap, source);
    }

    /**
     * Retrieves a user given its code and its password.
     * @param codeUtilisateur : user's code as a {@link String}.
     * @param pass : user's password as a {@link String}.
     * @return a {@link com.univ.objetspartages.bean.UtilisateurBean} if found, null otherwise.
     */
    public UtilisateurBean getByCodeAndPass(final String codeUtilisateur, final String pass) {
        return dao.getByCodeAndPass(codeUtilisateur, pass);
    }

    /**
     * Retrieves a list of users, identified by their codes.
     * @param codes : codes of the users to retrieve as an array of {@link String}.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.UtilisateurBean}.
     */
    public List<UtilisateurBean> getByCodes(final String... codes) {
        List<UtilisateurBean> results = Collections.emptyList();
        if (codes != null) {
            results = dao.getByCodes(Arrays.asList(codes));
        }
        return results;
    }

    /**
     * Retrieves all persisted users in the datasource.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.UtilisateurBean}.
     */
    public List<UtilisateurBean> getAllUsers() {
        return dao.getAllUsers();
    }

    /**
     * Retrieves all the users which have been imported by the given source.
     * @param sourceImport : the source to look for as a {@link String}.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.UtilisateurBean}.
     */
    public List<UtilisateurBean> getByImportSource(final String sourceImport) {
        return dao.getByImportSource(sourceImport);
    }

    /**
     * Retrieves a list of users matching the given parameters.
     * @param code : a user code as a {@link String}
     * @param nom : a user last name as a {@link String}
     * @param prenom : a user name as a {@link String}
     * @param chaineDiffusion : a group(s) serialization as a {@link String}
     * @param profil : a profil as a {@link String}.
     * @param groupe : a group code as a {@link String}.
     * @param structure : a structure code as a {@link String}.
     * @param adresseMail : an email address as a {@link String}.
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.UtilisateurBean}.
     */
    public List<UtilisateurBean> select(final String code, final String nom, final String prenom, final String chaineDiffusion, final String profil, final String groupe, final String structure, final String adresseMail) {
        return dao.select(code, nom, prenom, chaineDiffusion, profil, groupe, structure, adresseMail);
    }

    /**
     * Retrieves a list of users matching the given diffusion parameter.
     * @param chaineDiffusion : a group(s) serialization as a {@link String}
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.UtilisateurBean}.
     */
    public List<UtilisateurBean> getByChaineDiffusion(final String chaineDiffusion) {
        return dao.getByChaineDiffusion(chaineDiffusion);
    }
}
