package com.univ.objetspartages.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import com.kosmos.service.impl.AbstractServiceBean;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.dao.impl.ProfildsiDAO;

/**
 * Created by olivier.camon on 05/05/15.
 */
public class ServiceProfildsi extends AbstractServiceBean<ProfildsiBean, ProfildsiDAO> {

    private ServiceGroupeDsi serviceGroupeDsi;

    public void setServiceGroupeDsi(final ServiceGroupeDsi serviceGroupeDsi) {
        this.serviceGroupeDsi = serviceGroupeDsi;
    }

    public List<ProfildsiBean> getAll() {
        return dao.selectAll();
    }

    public boolean existCode(final String code) {
        return getByCode(code) != null;
    }

    @Override
    @CacheEvict(value = "ServiceProfilDsi.getByCode", key = "#bean.getCode()", condition = "#bean.getId() != null && #bean.getId()!= 0")
    public void save(final ProfildsiBean bean) {
        super.save(bean);
    }

    @Override
    @CacheEvict(value = "ServiceProfilDsi.getByCode", key="#root.target.getById(#id).getCode()", beforeInvocation = true)
    public void delete(final Long id) {
        super.delete(id);
    }

    @Cacheable(value = "ServiceProfilDsi.getByCode", unless= "#result == null")
    public ProfildsiBean getByCode(final String code) {
        return dao.getByCode(code);
    }

    /**
     * Renvoie la liste de tous les profils DSI (code+libellé) Peut être affiché directement dans une Combo.
     *
     * @return une {@link java.util.Map} de &lt;{@link String},{@link String}&gt; peuplée de par les profils, vide sinon.
     */
    public Map<String, String> getDisplayableProfiles() {
        final Map<String, String> res = new HashMap<>();
        for (final ProfildsiBean info : getAll()) {
            res.put(info.getCode(), info.getLibelle());
        }
        return res;
    }

    /**
     * Récupère un libellé affichable.
     *
     * @param code : le code du {@link com.univ.objetspartages.bean.ProfildsiBean} à récupérer.
     *
     * @return {@link com.univ.objetspartages.bean.ProfildsiBean#getLibelle()} si le profil existe, l'intitulé calculé par la clé i18n "BO_PROFIL_INEXISTANT" sinon.
     */
    public String getDisplayableLabel(final String code) {
        if (StringUtils.isBlank(code)) {
            return MessageHelper.getCoreMessage("BO_PROFIL_INEXISTANT");
        }
        final ProfildsiBean profile = getByCode(code);
        if(profile != null && StringUtils.isNotBlank(profile.getLibelle())) {
            return profile.getLibelle();
        } else {
            return MessageHelper.getCoreMessage("BO_PROFIL_INEXISTANT");
        }
    }

    /**
     * Renvoie la liste de tous les profils pour une liste de groupes.
     *
     * @param groupes : la liste des codes de groupe sous la forme d'une {@link java.util.List} de {@link String}
     *
     * @return une {@link java.util.Map} de &lt;{@link String}, {@link String}&gt; représentant des couples &lt;<i>code_Profil</i>, <i>libelle_Profil</i>&gt; ou une map vide.
     *
     */
    public Map<String, String> getListeProfilsDSIParGroupes(final Collection<String> groupes) {
        final Collection<String> listeProfils = renvoyerProfilsGroupes(groupes);
        final Map<String, String> hListeProfils = new HashMap<>();
        for (final String codeProfil : listeProfils) {
            final ProfildsiBean profile = getByCode(codeProfil);
            if(profile != null && StringUtils.isNotBlank(profile.getLibelle())) {
                hListeProfils.put(codeProfil, profile.getLibelle());
            }
        }
        return hListeProfils;
    }

    /**
     * Calcule la liste des codes des profils sélectionnés à partir de la liste des codes de groupes utilisateurs fournie en paramètre.
     *
     * @param groupesUtilisateur : une {@link java.util.List} de {@link String} représentant les codes de groupes utilisateurs.
     *
     * @return une {@link java.util.List} de {@link String} représentant l'ensemble des codes de profil calculés.
     */
    public List<String> renvoyerProfilsGroupes(final Collection<String> groupesUtilisateur) {
        final List<String> listeProfilsUser = new ArrayList<>();
        for (final ProfildsiBean currentProfile : getAll()) {
            final Collection<String> vGroupesProfil = currentProfile.getGroupes();
            final Iterator<String> eGroupeProfil = vGroupesProfil.iterator();
            // On regarde si l'utilisateur appartient à un des groupes du profil
            boolean profil = false;
            while (eGroupeProfil.hasNext() && !profil) {
                final GroupeDsiBean profileGroup = serviceGroupeDsi.getByCode(eGroupeProfil.next());
                final Iterator<String> eGroupeUser = groupesUtilisateur.iterator();
                while (eGroupeUser.hasNext() && !profil) {
                    final GroupeDsiBean userGroup = serviceGroupeDsi.getByCode(eGroupeUser.next());
                    profil = serviceGroupeDsi.contains(profileGroup, userGroup);
                }
            }
            if (profil) {
                listeProfilsUser.add(currentProfile.getCode());
            }
        }
        return listeProfilsUser;
    }

}
