package com.univ.objetspartages.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.email.JSBMailbox;
import com.kosmos.service.impl.AbstractServiceBean;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.PassRequestBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.dao.impl.PassRequestDAO;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.URLResolver;

/**
 * Created on 20/04/15.
 */
public class ServiceUserPass extends AbstractServiceBean<PassRequestBean, PassRequestDAO> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceUserPass.class);

    private static final String PROPRIETE_LONGUEUR_MOT_DE_PASSE = "utilisateur.password.longueur.max";

    // Valeur par défaut si rien n'est spécifié dans la conf
    public static final int LONGUEUR_MOT_DE_PASSE_DEFAUT = 64;

    /**
     * Gets the length of the password fields in forms context.
     * @return la valeur de la propriété ou {@link #LONGUEUR_MOT_DE_PASSE_DEFAUT} si non défini
     */
    public static int getLongueurChampMotDePasse() {
        int tailleMotDePasse = LONGUEUR_MOT_DE_PASSE_DEFAUT;
        final String prop = PropertyHelper.getCoreProperty(PROPRIETE_LONGUEUR_MOT_DE_PASSE);
        if (StringUtils.isNotBlank(prop) && StringUtils.isNumeric(prop)) {
            tailleMotDePasse = Integer.parseInt(prop);
        }
        return tailleMotDePasse;
    }

    /**
     * Used to request a new password for the given user.
     * @param user : a {@link UtilisateurBean} representing the user for which we should reset the password.
     * @param email : a {@link String} representing the email of the user.
     */
    public void requestNewPass(final UtilisateurBean user, final String email) {
        final UUID requestId = savePassRequest(user.getCode(), email);
        sendPassRequest(requestId, email);
    }

    private UUID savePassRequest(final String code, final String email) {
        final PassRequestBean chgMdp = new PassRequestBean();
        chgMdp.setCode(EscapeString.escapeSql(code));
        chgMdp.setEmail(EscapeString.escapeSql(email));
        chgMdp.setUuid(UUID.randomUUID());
        chgMdp.setDateDemande(new Date(System.currentTimeMillis()));
        dao.add(chgMdp);
        return chgMdp.getUuid();
    }

    private void sendPassRequest(final UUID requestId, final String to) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        String universite = "K-Portal";
        if (ctx != null && ctx.getInfosSite() != null) {
            universite = ctx.getInfosSite().getIntitule();
        }
        final String sujet = universite + " " + MessageHelper.getCoreMessage("ST_OBJET_DSI_CONNEXION_INTRANET");
        final String url = String.format("%s?PROC=IDENTIFICATION_FRONT&ACTION=PRESENTER_MDP&ID=%s", WebAppUtil.SG_PATH, requestId.toString());
        String message = MessageHelper.getCoreMessage("ST_CLIQUEZ_LIEN");
        message += URLResolver.getAbsoluteUrl(url, ctx);
        message += MessageHelper.getCoreMessage("ST_EQUIPE_WEB") + universite;
        final JSBMailbox mailbox = new JSBMailbox(false);
        try {
            mailbox.sendSystemMsg(to, sujet, message);
        } catch (final MessagingException | EmailException e) {
            LOG.debug("unable to send the mail", e);
            deleteRequest(requestId);
        }
    }

    private void deleteRequest(final UUID requestId) {
        final PassRequestBean passRequestBean = dao.getByRequestId(requestId);
        if(passRequestBean != null) {
            dao.delete(passRequestBean.getId());
        }
    }

    /**
     * Handle a pass request according to the given request id.
     * @param requestId : the id of the request as a {@link UUID}
     * @return a {@link Map} containing all useful informations including generated password.
     */
    public Map<String, String> handlePassRequest(final UUID requestId) {
        Map<String, String> presentation = new HashMap<>();
        if (requestId == null) {
            return presentation;
        }
        presentation = retrievePassRequest(requestId);
        if (StringUtils.isEmpty(presentation.get("code"))) {
            return presentation;
        }
        presentation.put("motDePasse", RandomStringUtils.randomAscii(10));
        deleteAllRequestsForCode(presentation.get("code"));
        return presentation;
    }

    private Map<String, String> retrievePassRequest(final UUID requestId) {
        final Map<String, String> demande = new HashMap<>();
        final PassRequestBean passRequestBean = dao.getByRequestId(requestId);
        if (passRequestBean != null) {
            demande.put("code", passRequestBean.getCode());
            demande.put("email", passRequestBean.getEmail());
        }
        return demande;
    }

    private void deleteAllRequestsForCode(final String code) {
        final List<PassRequestBean> passRequests = dao.getByCode(code);
        if (CollectionUtils.isNotEmpty(passRequests)) {
            for(final PassRequestBean currentPassRequest : passRequests) {
                dao.delete(currentPassRequest.getId());
            }
        }
    }
}
