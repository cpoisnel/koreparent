package com.univ.objetspartages.services;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.service.impl.AbstractServiceBean;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.dao.impl.RessourceDAO;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.EscapeString;
import com.univ.utils.sql.Operateur;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 *
 * Created on 06/05/15.
 * Since V6.5 : Usage of self instead of this (used for Spring Integration).
 */
public class ServiceRessource extends AbstractServiceBean<RessourceBean, RessourceDAO> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceRessource.class);

    public static final String ETAT_SAUVEGARDE_STRUCTURE_COURANTE = "destruction_sauvegarde_courante";

    @Autowired
    private ApplicationContext applicationContext;

    private ServiceRessource self;

    private ServiceMedia serviceMedia;

    @PostConstruct
    protected void init() {
        this.self = this.applicationContext.getBean(ServiceRessource.class);
    }

    public void setServiceMedia(final ServiceMedia serviceMedia) {
        this.serviceMedia = serviceMedia;
    }

    /**
     * Retrieves the file linked to the given fiche.
     * This method invokes #getFile(FicheUniv, String) with a second parameter set to "1".
     * @param fiche : a {@link com.univ.objetspartages.om.FicheUniv}
     * @return a {@link com.univ.objetspartages.bean.RessourceBean} if found, null otherwise.
     * @see #getFile(FicheUniv, String)
     */
    public RessourceBean getFile(final FicheUniv fiche) {
        return this.self.getFile(fiche, "1");
    }

    /**
     * Retrieves the file linked to the given ID fiche & type.
     * This method invokes #getFile(Long, String, String) with a third parameter set to "1".
     * @param idFiche the ID of the bean
     * @param codeObjet the type of bean
     * @return a {@link com.univ.objetspartages.bean.RessourceBean} if found, null otherwise.
     * @see #getFile(Long, String, String)
     */
    public RessourceBean getFile(final Long idFiche, final String codeObjet) {
        return this.self.getFile(idFiche, codeObjet, "1");
    }

    /**
     * Retrieves the file linked to the given fiche with the specified index.
     *
     * @param fiche : a {@link com.univ.objetspartages.om.FicheUniv}
     * @param indice : the index of the file to look for as a {@link String}
     * @return a {@link com.univ.objetspartages.bean.RessourceBean} if found, null otherwise.
     * @see #getFile(String)
     * @see #getFile(Long, String, String)
     */
    public RessourceBean getFile(final FicheUniv fiche, final String indice) {
        RessourceBean result = null;
        if (fiche != null) {
            result = this.self.getFile(fiche.getIdFiche(), ReferentielObjets.getCodeObjet(fiche), indice);
        }
        return result;
    }

    /**
     * Retrieves the file linked to the given fiche with the specified index.
     * This method computes the CODE_PARENT as found in the datasource and invokes #getFile(String)
     * @param idFiche the ID of the bean
     * @param codeObjet the type of bean
     * @param indice the index of the file to look for as a {@link String}
     * @return a {@link com.univ.objetspartages.bean.RessourceBean} if found, null otherwise.
     * @see #getFile(String)
     */
    public RessourceBean getFile(final Long idFiche, final String codeObjet, final String indice) {
        final String code = idFiche + ",TYPE=FICHIER_" + codeObjet + ",NO=" + indice;
        return this.self.getFile(code);
    }

    /**
     * Retrieves the file linked to the given code.
     * @param code : the CODE_PARENT to look for as a {@link String}
     * @return the {@link com.univ.objetspartages.bean.RessourceBean} if found, null otherwise.
     */
    public RessourceBean getFile(final String code) {
        final List<RessourceBean> files = this.dao.getByCodeParent(code, "ID_RESSOURCE");
        if (CollectionUtils.isNotEmpty(files)) {
            return files.get(0);
        }
        return null;
    }

    /**
     * Retrieves all the resources linked to the given fiche.
     * This method invokes #getFilesOrderBy(FicheUniv, String), sorting the elements by the datasource column "ORDRE".
     * @param ficheUniv : a {@link com.univ.objetspartages.om.FicheUniv}.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.RessourceBean}.
     * @see #getFilesOrderBy(FicheUniv, String)
     */
    public List<RessourceBean> getFiles(final FicheUniv ficheUniv) {
        List<RessourceBean> result = Collections.emptyList();
        if (ficheUniv != null) {
            final String codeParent = ficheUniv.getIdFiche() + ",TYPE=" + ReferentielObjets.getCodeObjet(ficheUniv) + "%";
            result = this.self.getFilesOrderBy(codeParent, "ORDRE");
        }
        return result;
    }

    /**
     * Retrieves all the resources linked to the given fiche sorted by the specified parameter.
     * This method computes the CODE_PARENT key and invokes #getFilesOrderBy(String, String).
     * @param ficheUniv : a {@link com.univ.objetspartages.om.FicheUniv}.
     * @param orderBy : the datasource column to sort the results on as a {@link String}
     * @return the {@link java.util.List} of matching {@link com.univ.objetspartages.bean.RessourceBean}
     * @see #getFilesOrderBy(String, String).
     */
    public List<RessourceBean> getFilesOrderBy(final FicheUniv ficheUniv, final String orderBy) {
        List<RessourceBean> result = Collections.emptyList();
        if (ficheUniv != null) {
            final String codeParent = ficheUniv.getIdFiche() + ",TYPE=" + ReferentielObjets.getCodeObjet(ficheUniv) + "%";
            result = this.self.getFilesOrderBy(codeParent, orderBy);
        }
        return result;
    }

    /**
     * Retrieves all the resources linked to the given parent code sorted by the specified parameter.
     * @param codeParent : the CODE_PARENT of the resources to find as a {@link String}
     * @param orderBy : the datasource column to sort the results on as a {@link String}
     * @return the {@link java.util.List} of matching {@link com.univ.objetspartages.bean.RessourceBean}
     */
    public List<RessourceBean> getFilesOrderBy(final String codeParent, final String orderBy) {
        return this.dao.getByCodeParent(codeParent, orderBy);
    }

    /**
     * Retrives all the {@link RessourceBean} linked to the given {@link FicheUniv} ordered on the "ORDRE" field.
     * @param ficheUniv the {@link FicheUniv} of the resources to find. Must be not null
     * @return the {@link java.util.List} of matching {@link com.univ.objetspartages.bean.RessourceBean} or an empty list if there is no result
     */
    public List<RessourceBean> getFilesIndex(final FicheUniv ficheUniv) {
        List<RessourceBean> result = Collections.emptyList();
        if (ficheUniv != null) {
            final String codeParent = ficheUniv.getIdFiche() + ",TYPE=FICHIER_" + ReferentielObjets.getCodeObjet(ficheUniv) + "%";
            result = this.self.getFilesOrderBy(codeParent, "ORDRE");
        }
        return result;
    }

    /**
     * Retrives all the {@link RessourceBean} linked to the given {@link FicheUniv} and indice sorted by the "ORDRE" field.
     * @param ficheUniv the {@link FicheUniv} of the resources to find. Must be not null
     * @param indice represent the field's id of the ficheUniv parameter.
     * @return the {@link java.util.List} of matching {@link com.univ.objetspartages.bean.RessourceBean} or an empty list if there is no result
     */
    public List<RessourceBean> getFilesIndex(final FicheUniv ficheUniv, final String indice) {
        List<RessourceBean> result = Collections.emptyList();
        if (ficheUniv != null) {
            final String codeParent = ficheUniv.getIdFiche() + ",TYPE=FICHIER_" + ReferentielObjets.getCodeObjet(ficheUniv) + ",NO=" + indice;
            result = this.self.getFilesOrderBy(codeParent, "ORDRE");
        }
        return result;
    }

    /**
     * Retrieves all the resources linked to the given parent code sorted by "ID_RESSOURCE".
     * @param codeParent : the CODE_PARENT of the resources to find as a {@link String}
     * @return the {@link java.util.List} of matching {@link com.univ.objetspartages.bean.RessourceBean}
     */
    public List<RessourceBean> getByCodeParent(final String codeParent) {
        return this.dao.getByCodeParent(codeParent, "ID_RESSOURCE");
    }

    /**
     * Retrieves a {@link java.util.List} of {@link com.univ.objetspartages.bean.RessourceBean} referencing the given media id.
     * @param id : the id of the media to look for as a {@link Long}
     * @return the {@link java.util.List} of matching {@link com.univ.objetspartages.bean.RessourceBean}
     */
    public List<RessourceBean> getByMediaId(final Long id) {
        return this.dao.getByMediaId(id);
    }

    /**
     * Add the given {@link com.univ.objetspartages.bean.RessourceBean} to the datasource.
     * @param ressourceBean : the {@link com.univ.objetspartages.bean.RessourceBean} to add.
     * @return the given {@link com.univ.objetspartages.bean.RessourceBean}
     */
    public RessourceBean add(final RessourceBean ressourceBean) {
        return this.dao.add(ressourceBean);
    }

    /**
     * Add the given {@link com.univ.objetspartages.bean.RessourceBean} to the datasource with forced ID.
     * @param ressourceBean : the {@link com.univ.objetspartages.bean.RessourceBean} to add.
     * @return the given {@link com.univ.objetspartages.bean.RessourceBean}
     */
    public RessourceBean addWithForcedId(final RessourceBean ressourceBean) {
        return this.dao.addWithForcedId(ressourceBean);
    }

    /**
     * Update the given {@link com.univ.objetspartages.bean.RessourceBean} to the datasource.
     * @param ressourceBean : the {@link com.univ.objetspartages.bean.RessourceBean} to update.
     * @return the given {@link com.univ.objetspartages.bean.RessourceBean}
     */
    public RessourceBean update(final RessourceBean ressourceBean) {
        return this.dao.update(ressourceBean);
    }

    /**
     * Retrieves a list of resources to purge (i.e, matching the following conditions : WHERE ETAT='0' OR ETAT='2' OR CODE_PARENT='').
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.RessourceBean}.
     */
    public List<RessourceBean> getRessourceToPurge() {
        return this.dao.getRessourceToPurge();
    }

    /**
     * Retrieves a list of mathing resources given a where clause.
     * @param where : the {@link com.univ.utils.sql.clause.ClauseWhere} to match the resources against.
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.RessourceBean}.
     */
    public List<RessourceBean> getFromWhere(final ClauseWhere where) {
        List<RessourceBean> result = Collections.emptyList();
        if (where != null) {
            result = this.dao.select(where.formaterSQL());
        }
        return result;
    }

    /**
     * Retrieves a list of mathing resources given a sql request.
     * @param request : the {@link com.univ.utils.sql.RequeteSQL} to match the resources against.
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.RessourceBean}.
     */
    public List<RessourceBean> getFromRequest(final RequeteSQL request) {
        List<RessourceBean> result = Collections.emptyList();
        if (request != null) {
            result = this.dao.select(request.formaterRequete());
        }
        return result;
    }

    /**
     * Retrieves a resource matching the given parameters.
     * @param id : the id of the media the resource should reference as a {@link Long}.
     * @param codeParent : the CODE_PARENT of the resource as a {@link String}.
     * @return the {@link com.univ.objetspartages.bean.RessourceBean} if found, null otherwise.
     */
    public RessourceBean getByMediaIdAndCodeParent(final Long id, final String codeParent) {
        return this.dao.getByMediaIdAndCodeParent(id, codeParent);
    }

    /**
     * Deletes all the {@link com.univ.objetspartages.bean.RessourceBean} matching the given code.
     * @param codeParent : the CODE_PARENT to match resources against as a {@link String}
     */
    public void deleteFiles(final String codeParent) {
        final List<RessourceBean> listeFichier = this.self.getFilesOrderBy(codeParent, "CODE_PARENT");
        for (final RessourceBean ficdel : listeFichier) {
            final List<RessourceBean> selectedFiles = this.self.getByMediaId(ficdel.getIdMedia());
            if (selectedFiles.size() == 1) {
                this.serviceMedia.delete(ficdel.getIdMedia());
            }
            this.self.delete(ficdel.getId());
        }
    }

    /**
     * Deletes all the {@link com.univ.objetspartages.bean.RessourceBean} linked to the given fiche.
     * @param fiche : a {@link com.univ.objetspartages.om.FicheUniv}.
     */
    public void deleteFiles(final FicheUniv fiche) {
        final List<RessourceBean> files = this.self.getFullFileList(fiche);
        for (final RessourceBean ficdel : files) {
            final List<RessourceBean> selectedFiles = this.self.getByMediaId(ficdel.getIdMedia());
            if (selectedFiles.size() == 1) {
                this.serviceMedia.delete(ficdel.getIdMedia());
            }
            // en apercu on ne supprime pas le fichier mais on passe son etat à 2 (sera supprimé par le scansite)
            if ("0005".equals(fiche.getEtatObjet())) {
                ficdel.setEtat("2");
                this.self.update(ficdel);
            } else if (ETAT_SAUVEGARDE_STRUCTURE_COURANTE.equals(fiche.getMessageAlerte())) {
                ficdel.setEtat("0");
                this.self.update(ficdel);
            } else {
                this.self.delete(ficdel.getId());
            }
        }
    }

    /**
     * Synchronizes resource for the specified parent code. This method handles resource addition as well as obsolete resource deletion.
     * @param codeParent : the code parent to look for as a {@link String}
     * @param file : the file to sync as a {@link String}
     */
    public void syncFile(final String codeParent, final String file) {
        String idFichier = StringUtils.EMPTY;
        /* etape 1 : lecture et enregistrement du fichier de la fiche courante */
        if (StringUtils.isNotBlank(file) && !"0".equals(file)) {
            final String[] ligne = file.split(";", -2);
            RessourceBean fichiergw = null;
            try {
                fichiergw = getById(Long.valueOf(ligne[0]));
            } catch (NumberFormatException nfe) {
                LOG.debug("unable to parse the value " + ligne[0], nfe);
            }
            if (fichiergw != null && "0".equals(fichiergw.getEtat())) {
                fichiergw.setCodeParent(codeParent);
                fichiergw.setEtat("1");
                this.self.update(fichiergw);
            } else if (fichiergw != null && !StringUtils.equals(fichiergw.getCodeParent(), codeParent)) {
                fichiergw = this.self.duplicateFile(fichiergw, codeParent, "1");
            } else if (fichiergw == null) {
                fichiergw = new RessourceBean();
            }
            idFichier = fichiergw.getId().toString();
        }
        /*suppression du fichier non lié à cette fiche*/
        final List<RessourceBean> ficdel = this.self.getByCodeParent(codeParent);
        for (final RessourceBean currentRessource : ficdel) {
            if (!currentRessource.getId().toString().equals(idFichier)) {
                this.serviceMedia.delete(currentRessource.getIdMedia());
                this.self.delete(currentRessource.getId());
            }
        }
    }

    /**
     * Synchronizes resource for the specified fiche.
     * This method computes the parent code and invokes #syncFile(String, String).
     * @param fiche : a {@link com.univ.objetspartages.om.FicheUniv}
     * @param file : the file to sync as a {@link String}
     * @param fileNumber : the index of the file to sync as a {@link int}
     * @see #syncFile(String, String)
     */
    public void syncFile(final FicheUniv fiche, final String file, final int fileNumber) {
        final String codeParent = fiche.getIdFiche() + ",TYPE=FICHIER_" + ReferentielObjets.getCodeObjet(fiche) + ",NO=" + Integer.toString(fileNumber);
        String idFichier = "";
        /* etape 1 : lecture et enregistrement du fichier de la fiche courante */
        if (StringUtils.isNotBlank(file) && !"0".equals(file)) {
            final String[] ligne = file.split(";", -2);
            RessourceBean fichiergw = null;
            try {
                fichiergw = getById(Long.valueOf(ligne[0]));
            } catch (NumberFormatException nfe) {
                LOG.debug("unable to parse the value " + ligne[0], nfe);
            }
            if (fichiergw != null && StringUtils.isBlank(fichiergw.getCodeParent())) {
                // c'est un upload generique
                if ("0".equals(fichiergw.getEtat())) {
                    fichiergw.setEtat("1");
                }
                // duplication du tuple fichier non rattaché dans le cas d'un apercu
                if ("0005".equals(fiche.getEtatObjet()) || "0006".equals(fiche.getEtatObjet())) {
                    this.self.update(fichiergw);
                }
                // enregistrement generique du fichier
                fichiergw.setCodeParent(codeParent);
                // si mode apercu ou sauvegarde automatique on duplique complètement le tuple
                if ("0005".equals(fiche.getEtatObjet()) || "0006".equals(fiche.getEtatObjet())) {
                    // on duplique le fichier
                    fichiergw = this.self.duplicateFile(fichiergw, codeParent, "1");
                } else {
                    this.self.update(fichiergw);
                }
            } else if (fichiergw != null && !fichiergw.getCodeParent().equals(codeParent)) {
                // on duplique le fichier
                fichiergw = this.self.duplicateFile(fichiergw, codeParent, "1");
            } else if (fichiergw == null) {
                fichiergw = new RessourceBean();
            }
            idFichier = fichiergw.getId().toString();
        }
        /* etape 2 : suppression du fichier non lié à cette fiche*/
        final List<RessourceBean> resources = this.self.getByCodeParent(codeParent);
        for (final RessourceBean currentRessource : resources) {
            if (!currentRessource.getId().toString().equals(idFichier)) {
                final List<RessourceBean> existingRessources = this.self.getByMediaId(currentRessource.getIdMedia());
                // supprime le fichier physique seulement s'il ne fait référence qu'au tuple courant et hors photothèque
                if (existingRessources.size() == 1) {
                    this.serviceMedia.delete(currentRessource.getIdMedia());
                }
                this.self.delete(currentRessource.getId());
            }
        }
    }

    /**
     * Synchronizes resources for the specified fiche.
     * This method computes the parent code and invokes #syncFiles(String, String).
     * @param fiche : a {@link com.univ.objetspartages.om.FicheUniv}
     * @param chaineFichier : the file to sync as a {@link String}
     * @param fileNumber : the index of the files to sync as a {@link int}
     * @see #syncFiles(String, String)
     */
    public void syncFiles(final FicheUniv fiche, final String chaineFichier, final int fileNumber) {
        if (fiche != null) {
            final String codeParent = fiche.getIdFiche() + ",TYPE=" + ReferentielObjets.getCodeObjet(fiche) + ",NO=" + Integer.toString(fileNumber);
            this.self.syncFiles(codeParent, chaineFichier);
        }
    }

    /**
     * Synchronizes resources for the specified fiche. This method handles resources addition as well as obsoletes resources deletion.
     * @param codeParent : the parent code to sync the resources for as a {@link String}
     * @param chaineFichier : the file to sync as a {@link String}
     */
    public void syncFiles(final String codeParent, final String chaineFichier) {
        final Map<String, String> hListeFichier = new HashMap<>();
        /* etape 1 : lecture et enregistrement des fichiers de la fiche courante */
        if (StringUtils.isNotEmpty(chaineFichier)) {
            final StringTokenizer st = new StringTokenizer(chaineFichier, "|");
            while (st.hasMoreTokens()) {
                final String enregistrement = st.nextToken();
                final String[] ligne = enregistrement.split(";", -2);
                RessourceBean ressource = this.self.getById(Long.valueOf(ligne[0]));
                if (ressource != null) {
                    if (StringUtils.isBlank(ressource.getCodeParent())) {
                        // c'est un upload generique
                        if ("0".equals(ressource.getEtat())) {
                            ressource.setEtat("1");
                        }
                        // enregistrement generique du fichier
                        ressource.setCodeParent(codeParent);
                        this.self.update(ressource);
                    } else if (!ressource.getCodeParent().equals(codeParent)) {
                        ressource = this.self.duplicateFile(ressource, codeParent, "1");
                    }
                    hListeFichier.put(ressource.getId().toString(), "");
                }
            }
        }
        /* etape 2 : suppression des fichiers non liés à cette fiche ou update des fichiers dupliqués*/
        final List<RessourceBean> ficdel = this.self.getByCodeParent(codeParent);
        for (final RessourceBean currentRessource : ficdel) {
            if (!hListeFichier.containsKey(currentRessource.getId().toString())) {
                final List<RessourceBean> temp = this.self.getByMediaId(currentRessource.getIdMedia());
                if (temp.size() == 1) {
                    this.serviceMedia.delete(currentRessource.getIdMedia());
                }
                this.self.delete(currentRessource.getId());
            }
        }
    }

    /**
     * Duplicates a resource.
     * @param fichiergw : the {@link com.univ.objetspartages.bean.RessourceBean} to duplicates.
     * @param codeParent : the parent code of the resource to duplicate as a {@link String}.
     * @param etat : the state of the duplicated resource as a {@link String}.
     * @return the duplicated {@link com.univ.objetspartages.bean.RessourceBean}.
     */
    public RessourceBean duplicateFile(final RessourceBean fichiergw, final String codeParent, final String etat) {
        RessourceBean result = null;
        if (fichiergw != null) {
            final RessourceBean fichierACreer = new RessourceBean();
            fichierACreer.setId((long) 0);
            fichierACreer.setIdMedia(fichiergw.getIdMedia());
            fichierACreer.setCodeParent(codeParent);
            fichierACreer.setOrdre(fichiergw.getOrdre());
            fichierACreer.setEtat(etat);
            if (StringUtils.isEmpty(fichierACreer.getEtat())) {
                fichierACreer.setEtat(fichiergw.getEtat());
            }
            this.self.add(fichierACreer);
            result = fichierACreer;
        }
        return result;
    }

    /**
     * Duplicates a resources of a {@link com.univ.objetspartages.om.FicheUniv} to another.
     * This method invokes #duplicateFile(RessourceBean, String, String) for each resource found.
     * @param ficheOrigine : the original {@link com.univ.objetspartages.om.FicheUniv}
     * @param ficheNouvelle : the new {@link com.univ.objetspartages.om.FicheUniv}
     * @see #duplicateFile(RessourceBean, String, String)
     */
    public void duplicateFiles(final FicheUniv ficheOrigine, final FicheUniv ficheNouvelle) {
        // liste des fichiers + vignettes
        final List<RessourceBean> listeFichier = this.self.getFullFileList(ficheOrigine);
        if (!listeFichier.isEmpty()) {
            for (final RessourceBean fic : listeFichier) {
                // duplication du code parent
                String codeParent = fic.getCodeParent().replaceFirst(ficheOrigine.getIdFiche().toString(), ficheNouvelle.getIdFiche().toString());
                // duplication du code du fichier
                if (codeParent.contains(ficheOrigine.getCode())) {
                    codeParent = StringUtils.replace(codeParent, ficheOrigine.getCode(), ficheNouvelle.getCode());
                }
                // duplication du fichier avec le reste
                this.self.duplicateFile(fic, codeParent, "1");
            }
        }
    }

    /**
     * Handles resources persistence for a toolbox component.
     * @param data : a {@link java.util.Map} of {@link String},{@link Object} to save.
     * @param ficheUniv : the {@link com.univ.objetspartages.om.FicheUniv} for which the resources should be created.
     */
    public void saveContentResource(final Map<String, Object> data, final FicheUniv ficheUniv) {
        // preparation du code parent
        String type;
        String codeParentSuffix = "_" + ReferentielObjets.getCodeObjet(ficheUniv) + ",CODE=";
        final String codeRecherche;
        if (((String) data.get("TS_CODE")).startsWith("TS")) {
            codeRecherche = codeParentSuffix + data.get("TS_CODE");
        } else {
            codeRecherche = codeParentSuffix + ficheUniv.getCode();
        }
        codeParentSuffix = codeParentSuffix + ficheUniv.getCode();
        boolean duplique;
        @SuppressWarnings("unchecked")
        final Set<String> hSetNew = (Set<String>) data.get("contentNewRessources");
        @SuppressWarnings("unchecked")
        final Set<String> hSetOld = (Set<String>) data.get("contentOldRessources");
        // Parcours des fichiers courants
        for (String codeFichier : hSetNew) {
            duplique = true;
            if (codeFichier.contains("IMG")) {
                type = "IMG";
            } else {
                type = "LIEN";
            }
            codeFichier = codeFichier.substring(0, codeFichier.indexOf("#"));
            final String codeParent = "TYPE=" + type + codeParentSuffix;
            // on recupere le fichier parmi grace au code parent code
            final ClauseWhere whereCodeParentEtIdMedia = new ClauseWhere(ConditionHelper.like("CODE_PARENT", codeRecherche, "%", ""));
            whereCodeParentEtIdMedia.and(ConditionHelper.egalVarchar("ID_MEDIA", codeFichier));
            final List<RessourceBean> resources = this.self.getFromWhere(whereCodeParentEtIdMedia);
            if (CollectionUtils.isNotEmpty(resources)) {
                for (final RessourceBean currentRessource : resources) {
                    // nouveau fichier
                    if ("0".equals(currentRessource.getEtat())) {
                        // en creation sur un apercu on duplique le fichier car pas de sauvegarde automatique
                        if (data.get(InfoBean.ETAT_OBJET).equals(InfoBean.ETAT_OBJET_CREATION) && "0005".equals(ficheUniv.getEtatObjet())) {
                            this.self.update(currentRessource);
                            break;
                        }
                        currentRessource.setCodeParent(ficheUniv.getIdFiche().toString() + "," + codeParent);
                        currentRessource.setEtat("1");
                        this.self.update(currentRessource);
                        duplique = false;
                        break;
                        // le fichier existe déja pour la fiche
                    } else if (currentRessource.getCodeParent().indexOf(ficheUniv.getIdFiche().toString() + ",") == 0) {
                        duplique = false;
                        break;
                    }
                }
                // il existe un fichier du meme code sur une autre fiche, ou sur un aperçu on duplique car pas de sauvegarde automatique
                if (duplique) {
                    final MediaBean mediaBean = serviceMedia.getById(Long.valueOf(codeFichier));
                    if (mediaBean != null) {
                        final RessourceBean addRessource = new RessourceBean();
                        addRessource.setId((long) 0);
                        addRessource.setIdMedia(mediaBean.getId());
                        addRessource.setCodeParent(ficheUniv.getIdFiche().toString() + "," + codeParent);
                        addRessource.setEtat("1");
                        this.self.add(addRessource);
                    }
                }
            } else {
                // on ne retrouve pas la ressource (cela peut venir d'un copié collé ou un pb)
                // on teste que le média existe bien
                final MediaBean mediaBean = this.serviceMedia.getById(Long.valueOf(codeFichier));
                if (mediaBean != null) {
                    final RessourceBean addRessource = new RessourceBean();
                    addRessource.setIdMedia(mediaBean.getId());
                    addRessource.setId((long) 0);
                    addRessource.setCodeParent(ficheUniv.getIdFiche().toString() + "," + codeParent);
                    addRessource.setEtat("1");
                    this.self.add(addRessource);
                }
            }
        }
        // Suppression des fichiers obsoletes du contenu de la fiche (insertion toolbox)
        final String codeParentEchapper = EscapeString.escapeSql(codeParentSuffix);
        final String valeurLike = "'" + ficheUniv.getIdFiche() + ",TYPE=%" + codeParentEchapper + "'";
        final ClauseWhere whereCodeParentEtIdMedia = new ClauseWhere(ConditionHelper.genericConditionSurColonne("CODE_PARENT", valeurLike, TypeOperande.NON_ECHAPABLE, Operateur.LIKE));
        final List<RessourceBean> foundResources = this.self.getFromWhere(whereCodeParentEtIdMedia);
        for (final RessourceBean currentRessource : foundResources) {
            // si le fichier est obsolete on le supprime
            if (hSetOld.contains(currentRessource.getIdMedia() + "#LIEN") || hSetOld.contains(currentRessource.getIdMedia() + "#IMG")) {
                // supprime le fichier physique seulement s'il ne fait référence qu'au tuple courant et hors photothèque
                final List<RessourceBean> unicityRessource = this.self.getByMediaId(currentRessource.getIdMedia());
                if (unicityRessource.size() == 1) {
                    this.serviceMedia.delete(currentRessource.getIdMedia());
                }
                // suppression de l'enregistrement en base
                this.self.delete(currentRessource.getId());
            }
        }
    }

    /**
     * Gets all the resources linked to the given fiche, thumbnails excluded.
     * @param fiche : a {@link com.univ.objetspartages.om.FicheUniv}
     * @return a {@link java.util.List} of corresponding {@link com.univ.objetspartages.bean.RessourceBean}
     */
    public List<RessourceBean> getFullFileList(final FicheUniv fiche) {
        List<RessourceBean> result = Collections.emptyList();
        if (fiche != null) {
            final String codeParent = fiche.getIdFiche() + ",TYPE=%" + ReferentielObjets.getCodeObjet(fiche) + "%";
            result = this.self.getFilesOrderBy(codeParent, StringUtils.EMPTY);
        }
        return result;
    }
}
