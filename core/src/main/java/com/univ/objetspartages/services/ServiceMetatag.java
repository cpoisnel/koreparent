package com.univ.objetspartages.services;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.Formateur;
import com.kosmos.service.impl.AbstractServiceBean;
import com.kportal.core.config.PropertyHelper;
import com.kportal.extension.module.plugin.toolbox.PluginTagHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.dao.impl.MetatagDAO;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.Chaine;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseWhere;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

/**
 * Created on 16/04/15.
 * Service exposing methods to handle {@link com.univ.objetspartages.bean.MetatagBean}.
 * @see com.univ.objetspartages.bean.MetatagBean
 * @see com.univ.objetspartages.dao.impl.MetatagDAO
 */
public class ServiceMetatag extends AbstractServiceBean<MetatagBean, MetatagDAO> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceMetatag.class);

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(value = "ServiceMetatag.getById")
    public MetatagBean getById(final Long id) {
        return super.getById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Caching(
            evict = {
                    @CacheEvict(value = "ServiceMetatag.getByCodeAndIdFiche",
                                key = "{#root.target.getById(#id)?.getMetaCodeObjet(),#root.target.getById(#id)?.getMetaIdFiche()}",
                                beforeInvocation = true),
                    @CacheEvict(value = "ServiceMetatag.getByMetaCodeObjetCodeLangueEtat",
                                key = "{#root.target.getById(#id)?.getMetaCodeObjet(),#root.target.getById(#id)?.getMetaCode(),#root.target.getById(#id)?.getMetaLangue(),#root.target.getById(#id)?.getMetaEtatObjet()}",
                                beforeInvocation = true),
                    @CacheEvict(value = "ServiceMetatag.getByObjectCode", key = "#root.target.getById(#id)?.getMetaCodeObjet()", beforeInvocation = true),
                    @CacheEvict(value = "ServiceMetatag.getOnlineMetas", key = "#root.target.getById(#id)?.getMetaCodeObjet()", beforeInvocation = true),
                    @CacheEvict(value = "ServiceMetatag.getById", key = "#id", beforeInvocation = true)})
    public void delete(final Long id) {
        super.delete(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Caching(evict = {
            @CacheEvict(value = "ServiceMetatag.getByCodeAndIdFiche", key = "{#bean.getMetaCodeObjet(),#bean.getMetaIdFiche()}"),
            @CacheEvict(value = "ServiceMetatag.getByMetaCodeObjetCodeLangueEtat",
                    key = "{#bean.getMetaCodeObjet(),#bean.getMetaCode(),#bean.getMetaLangue(),#bean.getMetaEtatObjet()}"),
            @CacheEvict(value = "ServiceMetatag.getByObjectCode", key = "#bean.getMetaCodeObjet()"),
            @CacheEvict(value = "ServiceMetatag.getOnlineMetas", key = "#bean.getMetaCodeObjet()"),
            @CacheEvict(value = "ServiceMetatag.getById", key = "#bean.getId()")})
    public void save(final MetatagBean bean) {
        super.save(bean);
    }

    @Caching(evict = {
            @CacheEvict(value = "ServiceMetatag.getByCodeAndIdFiche", key = "{#bean?.getMetaCodeObjet(),#bean?.getMetaIdFiche()}"),
            @CacheEvict(value = "ServiceMetatag.getByMetaCodeObjetCodeLangueEtat",
                    key = "{#bean?.getMetaCodeObjet(),#bean?.getMetaCode(),#bean?.getMetaLangue(),#bean?.getMetaEtatObjet()}"),
            @CacheEvict(value = "ServiceMetatag.getByObjectCode", key = "{#bean?.getMetaCodeObjet()}"),
            @CacheEvict(value = "ServiceMetatag.getOnlineMetas", key = "{#bean?.getMetaCodeObjet()}"),
            @CacheEvict(value = "ServiceMetatag.getById", key = "{#bean?.getId()}")})
    public MetatagBean addWithForcedId(final MetatagBean bean) {
        return dao.addWithForcedId(bean);
    }

    /**
     * Retrieve a {@link com.univ.objetspartages.bean.MetatagBean} according to the given object code and the id of a {@link com.univ.objetspartages.om.FicheUniv}.
     *
     * @param objectCode : the object code as described in the application as a {@link String}
     * @param idFiche : the id of a {@link com.univ.objetspartages.om.FicheUniv} object as a {@link Long}
     * @return the {@link com.univ.objetspartages.bean.MetatagBean} if found, null otherwise.
     */
    @Cacheable(value = "ServiceMetatag.getByCodeAndIdFiche",key="{#objectCode,#idFiche}")
    public MetatagBean getByCodeAndIdFiche(final String objectCode, final Long idFiche) {
        return dao.getByCodeAndIdFiche(objectCode, idFiche);
    }

    /**
     * Get a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} according to the given {@link java.util.Date}.
     * More precisely, all {@link com.univ.objetspartages.bean.MetatagBean} matching this query : META_DATE_OPERATION >= date.
     *
     * This method also uses "search.restriction_objet" property to excludes certain object types.
     *
     * @param date : a {@link java.util.Date}
     *
     * @return A {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} matching the restrictions if date is not null or
     * a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} excluding abjects defined in the properties.
     */
    public List<MetatagBean> getMetasSince(final Date date) {
        final Collection<String> objetsExclus = Chaine.getVecteurPointsVirgules(PropertyHelper.getCoreProperty("search.restriction_objet"));
        if(date != null) {
            return dao.getMetasSinceExludingObjects(date, objetsExclus);
        } else {
            return dao.getAllMetas(objetsExclus);
        }
    }

    /**
     * Get a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} matching the given section codes and object codes.
     *
     * @param codesRubrique : a {@link java.util.Collection} of {@link String} representing the section codes.
     * @param objet : an {@link String} array of object codes to include.
     *
     * @return a {@link java.util.List} of the matching {@link com.univ.objetspartages.bean.MetatagBean}
     */
    public List<MetatagBean> getMetasForRubriquesAndObjet(final Collection<String> codesRubrique, final String... objet) {
        return dao.getMetasForRubriquesAndObjet(codesRubrique, objet);
    }

    /**
     * Get a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} matching the given object code.
     *
     * @param objectCode : an object code as defined is the application as a {@link String}
     *
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} matching the given object code.
     */
    @Cacheable(value = "ServiceMetatag.getByObjectCode")
    public List<MetatagBean> getByObjectCode(final String objectCode) {
        return dao.getByObjectCode(objectCode);
    }

    /**
     * Delete all the {@link com.univ.objetspartages.bean.MetatagBean} matching the given object code and a {@link java.util.List} of {@link Long}
     * representing {@link com.univ.objetspartages.om.FicheUniv} ids.
     *
     * @param codeObjet : an object code as defined in the application as a {@link String}
     * @param listeIdFiches : a {@link java.util.List} of {@link Long} representing {@link com.univ.objetspartages.om.FicheUniv} ids.
     */
    @Caching(evict = {@CacheEvict(value = "ServiceMetatag.getById", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceMetatag.getByCodeAndIdFiche", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceMetatag.getByMetaCodeObjetCodeLangueEtat", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceMetatag.getByObjectCode", key = "#codeObjet", beforeInvocation = true),
            @CacheEvict(value = "ServiceMetatag.getOnlineMetas", key = "#codeObjet", beforeInvocation = true)})
    public void deleteForCodeAndFicheIds(final String codeObjet, final Collection<Long> listeIdFiches){
        dao.deleteForCodeAndIds(codeObjet, listeIdFiches);
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} matching the given params.
     *
     * @param codeObjet : the object code as a {@link String}.
     * @param ids : an array of {@link Long} representing {@link com.univ.objetspartages.om.FicheUniv} objects.
     *
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} matching the restrictions.
     */
    public List<MetatagBean> getByCodeAndIdsFiche(final String codeObjet, final Long... ids) {
        List<MetatagBean> result = Collections.emptyList();
        if (ids != null) {
            result = dao.getByCodeAndIdsFiche(codeObjet, ids);
        }
        return result;
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} according to the given ids.
     *
     * @param ids : an array of {@link Long} representing {@link com.univ.objetspartages.bean.MetatagBean} ids.
     *
     * @return {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean}
     */
    public List<MetatagBean> getByIds(final Long... ids) {
        List<MetatagBean> result = Collections.emptyList();
        if (ids != null) {
            result = dao.getByIds(ids);
        }
        return result;
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} according to the given section code.
     *
     * @param codeRubrique : a section code as a {@link String}
     *
     * @return {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean}
     */
    public List<MetatagBean> getMetasForRubrique(final String codeRubrique) {
        return dao.getMetasForRubrique(codeRubrique);
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} according to the given section code and states.
     *
     * @param codeRubrique : a section code as a {@link String}
     * @param states : the states value of a {@link MetatagBean}
     * @return {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean}
     */
    public List<MetatagBean> getMetasForRubriqueAndStates(final String codeRubrique, final Collection<String> states) {
        return dao.getMetasForRubriqueAndStates(codeRubrique, states);
    }

    /**
     * Retrieve a {@link java.util.Map} of {@link String},{@link String} according to the given writter code.
     *
     * @param codeRedacteur : a writter code as a {@link String}
     *
     * @return {@link java.util.Map} of {@link String},{@link String}
     */
    public Map<String, String> getListeObjetsRedacteur(final String codeRedacteur) {
        final List<MetatagBean> metas = dao.getByUserCode(codeRedacteur);
        final Map<String, String> fromWritter = new HashMap<>();
        for(final MetatagBean currentMeta : metas) {
            fromWritter.put(currentMeta.getMetaCodeObjet(), StringUtils.EMPTY);
        }
        return fromWritter;
    }

    /**
     * Get the last modification date of a {@link com.univ.objetspartages.bean.MetatagBean}.
     *
     * @param metatagBean : a {@link com.univ.objetspartages.bean.MetatagBean} to compute the date for.
     *
     * @return the computed last modification date as a {@link java.util.Date} or null if not found/not parsable
     */
    public Date getLastModificationDate(final MetatagBean metatagBean) {
        if (metatagBean != null && StringUtils.isNotBlank(metatagBean.getMetaHistorique())) {
            final String[] history = metatagBean.getMetaHistorique().split(";");
            final StringTokenizer st = new StringTokenizer(history[0], "[/]");
            final SimpleDateFormat historyDate = new SimpleDateFormat("YYYYMMDDHHmmss");
            try {
                // on retient le 2ème token pour la date*/
                st.nextToken();
                return historyDate.parse(st.nextToken());
            } catch (final ParseException | NoSuchElementException e) {
                LOGGER.debug("unable to parse the last modification date", e);
                return null;
            }
        }
        return null;
    }

    /**
     * Assert the consistency of a {@link com.univ.objetspartages.bean.MetatagBean} versus a {@link com.univ.objetspartages.om.FicheUniv}.
     * @param metatagBean : the {@link com.univ.objetspartages.bean.MetatagBean} to test
     * @param ficheRef : the {@link com.univ.objetspartages.om.FicheUniv} to test against the {@link com.univ.objetspartages.bean.MetatagBean}.
     * @return a {@link java.util.List} of {@link String} (differences)
     */
    public List<String> controlerCoherenceAvecFiche(final MetatagBean metatagBean, final FicheUniv ficheRef) {
        final List<String> differences = new ArrayList<>();
        testerCoherenceChamp(differences, "code", metatagBean.getMetaCode(), ficheRef.getCode());
        testerCoherenceChamp(differences, "code_rattachement", metatagBean.getMetaCodeRattachement(), ficheRef.getCodeRattachement());
        final String codeRubriqueFiche = ficheRef.getCodeRubrique();
        testerCoherenceChamp(differences, "code_rubrique", metatagBean.getMetaCodeRubrique(), codeRubriqueFiche);
        String dateMeta = Formateur.formater(metatagBean.getMetaDateCreation());
        String dateFiche = Formateur.formater(ficheRef.getDateCreation());
        testerCoherenceChamp(differences, "date_creation", dateMeta, dateFiche);
        dateMeta = Formateur.formater(metatagBean.getMetaDateProposition());
        dateFiche = Formateur.formater(ficheRef.getDateProposition());
        testerCoherenceChamp(differences, "date_proposition", dateMeta, dateFiche);
        dateMeta = Formateur.formater(metatagBean.getMetaDateValidation());
        dateFiche = Formateur.formater(ficheRef.getDateValidation());
        testerCoherenceChamp(differences, "date_validation", dateMeta, dateFiche);
        dateMeta = Formateur.formater(metatagBean.getMetaDateModification());
        dateFiche = Formateur.formater(ficheRef.getDateModification());
        testerCoherenceChamp(differences, "date_modification", dateMeta, dateFiche);
        testerCoherenceChamp(differences, "code_redacteur", metatagBean.getMetaCodeRedacteur(), ficheRef.getCodeRedacteur());
        testerCoherenceChamp(differences, "code_validation", metatagBean.getMetaCodeValidation(), ficheRef.getCodeValidation());
        testerCoherenceChamp(differences, "langue", metatagBean.getMetaLangue(), ficheRef.getLangue());
        testerCoherenceChamp(differences, "etat_objet", metatagBean.getMetaEtatObjet(), ficheRef.getEtatObjet());
        if (ficheRef instanceof FicheRattachementsSecondaires) {
            final String codeRattachementAutres = Chaine.convertirPointsVirgulesEnAccolades(((FicheRattachementsSecondaires) ficheRef).getCodeRattachementAutres());
            testerCoherenceChamp(differences, "code_rattachement_autres", metatagBean.getMetaCodeRattachementAutres(), codeRattachementAutres);
        }
        if (ficheRef instanceof DiffusionSelective) {
            final String diffusionPublicVise = ((DiffusionSelective) ficheRef).getDiffusionPublicVise();
            testerCoherenceChamp(differences, "diffusion_public_vise", metatagBean.getMetaDiffusionPublicVise(), diffusionPublicVise);
        }
        if (ficheRef instanceof DiffusionSelective) {
            final String diffusionModeRestriction = ((DiffusionSelective) ficheRef).getDiffusionModeRestriction();
            testerCoherenceChamp(differences, "diffusion_public_vise", metatagBean.getMetaDiffusionModeRestriction(), diffusionModeRestriction);
        }
        if (ficheRef instanceof DiffusionSelective) {
            final String diffusionPublicViseRestriction = ((DiffusionSelective) ficheRef).getDiffusionPublicViseRestriction();
            testerCoherenceChamp(differences, "diffusion_public_vise", metatagBean.getMetaDiffusionPublicViseRestriction(), diffusionPublicViseRestriction);
        }
        return differences;
    }

    private void testerCoherenceChamp(final List<String> differences, final String nomChamp, final String valeurMeta, final String valeurFiche) {
        if (valeurMeta == null || !valeurMeta.equals(valeurFiche)) {
            differences.add("champ: " + nomChamp + " fiche: " + valeurFiche + " meta:" + valeurMeta);
        }
    }

    /**
     * Synchronize a {@link com.univ.objetspartages.bean.MetatagBean} and a {@link com.univ.objetspartages.om.FicheUniv} data.
     *
     * @param meta : a {@link com.univ.objetspartages.bean.MetatagBean}
     * @param ficheUniv : a {@link com.univ.objetspartages.om.FicheUniv}
     * @param calculerReferences : a {@link boolean} indicating if we should compute references before the synchronization.
     *
     * @see #calculerReferences(MetatagBean, String, String, String)
     */
    public void synchroniser(final MetatagBean meta, final FicheUniv ficheUniv, final boolean calculerReferences) {
        if (calculerReferences) {
            calculerReferences(meta, ficheUniv.getReferences(), ficheUniv.getContenuEncadre(), ficheUniv.getLangue());
        }
        meta.setMetaIdFiche(ficheUniv.getIdFiche());
        meta.setMetaCodeRattachement(ficheUniv.getCodeRattachement());
        meta.setMetaCodeRubrique(ficheUniv.getCodeRubrique());
        if (ficheUniv instanceof FicheRattachementsSecondaires) {
            meta.setMetaCodeRattachementAutres(Chaine.convertirPointsVirgulesEnAccolades(((FicheRattachementsSecondaires) ficheUniv).getCodeRattachementAutres()));
        } else {
            meta.setMetaCodeRattachementAutres("");
        }
        meta.setMetaLibelleObjet(ReferentielObjets.getLibelleObjet(meta.getMetaCodeObjet()));
        meta.setMetaLibelleFiche(ficheUniv.getLibelleAffichable());
        meta.setMetaKeywords(ficheUniv.getMetaKeywords());
        meta.setMetaDescription(ficheUniv.getMetaDescription());
        meta.setMetaDateCreation(ficheUniv.getDateCreation());
        meta.setMetaDateProposition(ficheUniv.getDateProposition());
        meta.setMetaDateValidation(ficheUniv.getDateValidation());
        meta.setMetaDateModification(ficheUniv.getDateModification());
        meta.setMetaCodeRedacteur(ficheUniv.getCodeRedacteur());
        meta.setMetaCodeValidation(ficheUniv.getCodeValidation());
        meta.setMetaCode(ficheUniv.getCode());
        meta.setMetaLangue(ficheUniv.getLangue());
        meta.setMetaEtatObjet(ficheUniv.getEtatObjet());
        meta.setMetaNbHits(ficheUniv.getNbHits());
        if (ficheUniv instanceof DiffusionSelective) {
            meta.setMetaDiffusionPublicVise(((DiffusionSelective) ficheUniv).getDiffusionPublicVise());
            meta.setMetaDiffusionModeRestriction(((DiffusionSelective) ficheUniv).getDiffusionModeRestriction());
            if ("4".equals(((DiffusionSelective) ficheUniv).getDiffusionModeRestriction())) {
                meta.setMetaForumAno("0");
            }
            meta.setMetaDiffusionPublicViseRestriction(((DiffusionSelective) ficheUniv).getDiffusionPublicViseRestriction());
        } else {
            meta.setMetaDiffusionPublicVise("");
            meta.setMetaDiffusionModeRestriction("0");
            meta.setMetaDiffusionPublicViseRestriction("");
        }
        if (meta.getMetaForumAno() == null) {
            meta.setMetaForumAno("");
        }
        if (!Formateur.estSaisie(meta.getMetaDateMiseEnLigne())) {
            meta.setMetaDateMiseEnLigne(ficheUniv.getDateCreation());
        }
    }

    /**
     * Computes and injects a list of references to the given {@link com.univ.objetspartages.bean.MetatagBean} according to the given {@link com.univ.objetspartages.om.FicheUniv}.
     *
     * @param meta : a {@link com.univ.objetspartages.bean.MetatagBean} to inject the references to
     * @param reference : the references of the given content
     * @param contenuEncadre : the box content
     * @param langue : the langage in witch we generate the references
     */
    public void calculerReferences(final MetatagBean meta, final String reference, final String contenuEncadre, final String langue) {
        final String references = reference + StringUtils.defaultString(contenuEncadre);
        String newRef = PluginTagHelper.getReferencesTags(references);
        int indexTexte = 0;
        int indexDebutMotCle;
        /********************************************************
         * LIENS INTER-FICHES *
         *********************************************************/
        /* Boucle sur chaque mot clé */
        while ((indexDebutMotCle = references.indexOf("[id-fiche]", indexTexte)) != -1) {
            // Extraction de la chaine
            final int indexFinMotCle = references.indexOf("[/id-fiche]", indexDebutMotCle);
            // Fin du traitement si  tag incomplet
            if (indexFinMotCle == -1) {
                break;
            }
            final int indexDebutMotCleSuivant = references.indexOf("[id-fiche]", indexDebutMotCle + 1);
            if ((indexDebutMotCleSuivant != -1) && (indexDebutMotCleSuivant < indexFinMotCle)) {
                break;
            }
            final String chaine = references.substring(indexDebutMotCle + 10, indexFinMotCle);
            // Analyse de la chaine
            String codeFiche = StringUtils.EMPTY;
            String objetReference = StringUtils.EMPTY;
            int indiceToken = 0;
            final StringTokenizer st = new StringTokenizer(chaine, ";");
            while (st.hasMoreTokens()) {
                if (indiceToken == 0) {
                    objetReference = st.nextToken();
                } else if (indiceToken == 1) {
                    codeFiche = st.nextToken();
                } else {
                    st.nextToken();
                }
                indiceToken++;
            }
            /* si le code est vide, ou contient le caractère = il s'agit d'une requete
               sinon, il s'agit d'une fiche
             */
            boolean traitementRequete = StringUtils.isEmpty(codeFiche) || codeFiche.contains("=") && !codeFiche.contains(",LANGUE=");
            if (!traitementRequete) {
                /* Analyse de la langue */
                final String codeReference;
                final String langueReference;
                if (!codeFiche.contains(",LANGUE=")) {
                    codeReference = codeFiche;
                    langueReference = langue;
                } else {
                    codeReference = codeFiche.substring(0, codeFiche.indexOf(",LANGUE="));
                    langueReference = codeFiche.substring(codeFiche.indexOf(",LANGUE=") + 8);
                }
                /* Ajout de la référence */
                newRef += "[" + objetReference + ";" + codeReference + ";" + langueReference + "]";
            }
            indexTexte = indexFinMotCle + 11;
        }
        /********************************************************
         * LIENS DIRECT DOCUMENTS *
         *********************************************************/
        indexTexte = 0;
        while ((indexDebutMotCle = references.indexOf("[id-document]", indexTexte)) != -1) {
            // Extraction de la chaine
            final int indexFinMotCle = references.indexOf("[/id-document]", indexDebutMotCle);
            // JSS 20020910-001 : Fin du traitement si  tag incomplet
            if (indexFinMotCle == -1) {
                break;
            }
            final int indexDebutMotCleSuivant = references.indexOf("[id-document]", indexDebutMotCle + 1);
            if (indexDebutMotCleSuivant != -1 && indexDebutMotCleSuivant < indexFinMotCle) {
                break;
            }
            final String document = references.substring(indexDebutMotCle + "[id-document]".length(), indexFinMotCle);
            final String codeReference;
            final String langueReference;
            if (!document.contains(",LANGUE=")) {
                codeReference = document;
                langueReference = langue;
            } else {
                codeReference = document.substring(0, document.indexOf(",LANGUE="));
                langueReference = document.substring(document.indexOf(",LANGUE=") + 8);
            }
            /* Ajout de la référence */
            newRef += "[document;" + codeReference + ";" + langueReference + "]";
            indexTexte = indexFinMotCle + 14;
        }
        meta.setMetaListeReferences(newRef);
    }

    /**
     * Get history kept by a {@link com.univ.objetspartages.bean.MetatagBean}
     *
     * @return a {@link java.util.List} of a {@link String} representing each action performed on a {@link com.univ.objetspartages.om.FicheUniv}     *
     */
    public List<String> getHistory(final MetatagBean metatagBean) {
        final List<String> history = new ArrayList<>();
        if (metatagBean != null) {
            final StringTokenizer st = new StringTokenizer(StringUtils.defaultString(metatagBean.getMetaHistorique()), ";");
            while (st.hasMoreTokens()) {
                final String val = st.nextToken();
                history.add(val);
            }
        }
        return history;
    }

    /**
     * Save a {@link java.util.List} of {@link String} representing actions performed on a {@link com.univ.objetspartages.om.FicheUniv}
     * as the history of the given {@link com.univ.objetspartages.bean.MetatagBean}.
     * @param meta : the {@link com.univ.objetspartages.bean.MetatagBean} to fill with the history.
     * @param list : a {@link java.util.List} of {@link String}
     */
    public void setHistory(final MetatagBean meta, final List<String> list) {
        if (meta != null && list != null) {
            meta.setMetaHistorique(StringUtils.join(list, ";"));
        }
    }

    /**
     * Add an action in the history of the given {@link com.univ.objetspartages.bean.MetatagBean}.
     *
     * @param evenement : the event that occured on a {@link com.univ.objetspartages.om.FicheUniv} as a {@link String}.
     * @param authorCode : the author of the action as a {@link String}.
     * @param objectState : the state of the object after the action.
     *
     */
    public void addHistory(final MetatagBean meta, final String evenement, final String authorCode, final String objectState) {
        final List<String> history = getHistory(meta);
        final GregorianCalendar cal = new GregorianCalendar();
        final Object[] arguments = {cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND)};
        final String dateModif = MessageFormat.format("{2,number,0000}{1,number,00}{0,number,00}{3,number,00}{4,number,00}{5,number,00}", arguments);
        final String item = String.format("[%s/%s/%s/%s]", evenement, dateModif, authorCode, objectState);
        history.add(0, item);
        setHistory(meta, history);
    }

    /**
     * Get a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} by references.
     * @param codeObjet : the object code to look for as a {@link String}.
     * @param codeFiche : a {@link com.univ.objetspartages.om.FicheUniv} code.
     * @param langue : the language to perform the search for as a {@link String}.
     * @param idPhoto : an id as a {@link Long}.
     * @return {@link java.util.List} of matching {@link com.univ.objetspartages.bean.MetatagBean}
     */
    public List<MetatagBean> getByReferences(final String codeObjet, final String codeFiche, final String langue, final Long idPhoto) {
        return dao.getByReferences(codeObjet, codeFiche, langue, idPhoto);
    }

    /**
     * Retrieve all persisted {@link com.univ.objetspartages.bean.MetatagBean}.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean}.
     */
    public List<MetatagBean> getAllMetas() {
        return dao.getAllMetas(new ArrayList<String>());
    }

    /**
     * Get a {@link java.util.List} a given amount of {@link com.univ.objetspartages.bean.MetatagBean}. The query orders metas by META_DATE_MODIFICATION DESC.
     * @param amount : the number of metas to retrieve as an {@link int}.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean}.
     */
    public List<MetatagBean> getMetas(final int amount) {
        return dao.getMetas(amount);
    }

    /**
     * Retrieve a {@link com.univ.objetspartages.bean.MetatagBean} matching the given informations.
     * @param codeObjet : the object code as a {@link String} as set in the application properties
     * @param codeFiche : a {@link com.univ.objetspartages.om.FicheUniv} code as a {@link String}
     * @param langueFiche : the language to look for as a {@link String}
     * @param etatFiche : the state of the {@link com.univ.objetspartages.om.FicheUniv} as a {@link String}
     * @return the maching {@link com.univ.objetspartages.bean.MetatagBean} or null otherwise
     */
    @Cacheable(value = "ServiceMetatag.getByMetaCodeObjetCodeLangueEtat",key="{#codeObjet, #codeFiche, #langueFiche, #etatFiche}")
    public MetatagBean getByMetaCodeObjetCodeLangueEtat(final String codeObjet, final String codeFiche, final String langueFiche, final String etatFiche) {
        return dao.getByMetaCodeObjetCodeLangueEtat(codeObjet, codeFiche, langueFiche, etatFiche);
    }

    /**
     * Get a {@link java.util.List} of online metas (with a {@link com.univ.objetspartages.om.EtatFiche#EN_LIGNE} state).
     * @param codeObjet : the object code to look for as a {@link String}. If this parameter is empty, no query will be performed
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} or an empty list if there is no result
     */
    @Cacheable(value = "ServiceMetatag.getOnlineMetas")
    public List<MetatagBean> getOnlineMetas(final String codeObjet) {
        List<MetatagBean> results = Collections.emptyList();
        if (StringUtils.isNotEmpty(codeObjet)) {
            results = dao.getMetaByObjectCodeAndState(Collections.singletonList(codeObjet), EtatFiche.EN_LIGNE);
        }
        return results;
    }

    /**
     * Get a {@link java.util.List} of online metas (with a {@link com.univ.objetspartages.om.EtatFiche#A_VALIDER} state).
     * @param objectCodes : a list of object code to look for as a {@link String}. If the list is null or empty, no query will be performed
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} or an empty list if there is no result
     */
    public List<MetatagBean> getToValidateMetas(final Collection<String> objectCodes) {
        List<MetatagBean> results = Collections.emptyList();
        if (CollectionUtils.isNotEmpty(objectCodes)) {
            results = dao.getMetaByObjectCodeAndState(objectCodes, EtatFiche.A_VALIDER);
        }
        return results;
    }
    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} matching the given writter code and section code.
     * @param codeRedacteur : the writter code as a {@link String}.
     * @param codeRubrique : the section code as a {@link String}.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean}.
     */
    public List<MetatagBean> getByCodeRedacteurAndCodeRubrique(final String codeRedacteur, final String codeRubrique) {
        return dao.getByCodeRedacteurAndCodeRubrique(codeRedacteur, codeRubrique);
    }

    /**
     * Get the {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} for which the META_LISTE_REFERENCES contains "photo".
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean}
     */
    public List<MetatagBean> getByPhotoReferences() {
        return dao.getByPhotoReferences();
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} from a SQL request.
     * @param request : a {@link com.univ.utils.sql.RequeteSQL}
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean}
     */
    public List<MetatagBean> getFromRequest(final RequeteSQL request) {
        List<MetatagBean> result = Collections.emptyList();
        if (request != null) {
            result = dao.select(request.formaterRequete());
        }
        return result;
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean} from a where clause.
     * @param whereFiche : a {@link com.univ.utils.sql.clause.ClauseWhere}
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.MetatagBean}
     */
    public List<MetatagBean> getFromWhereClause(final ClauseWhere whereFiche) {
        List<MetatagBean> result = Collections.emptyList();
        if (whereFiche != null) {
            result = dao.select(whereFiche.formaterSQL());
        }
        return result;
    }

}
