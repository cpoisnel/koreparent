package com.univ.objetspartages.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.kosmos.service.impl.AbstractServiceBean;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.dao.impl.GroupeUtilisateurDAO;
import com.univ.objetspartages.om.InfosRequeteGroupe;
import com.univ.utils.RequeteGroupeUtil;

/**
 * Service gérant les groupes utilisateurs. Les groupes utilisateeurs sont une table de jointure entre les groupes et les utilisateurs associés..
 */
public class ServiceGroupeUtilisateur extends AbstractServiceBean<GroupeUtilisateurBean, GroupeUtilisateurDAO> {

    /**
     * Préfixe pour les groupes automatiques...
     */
    public static final String PREFIX_AUTOMATIC_GROUP = "#AUTO#";

    private ServiceGroupeDsi serviceGroupeDsi;

    public void setServiceGroupeDsi(final ServiceGroupeDsi serviceGroupeDsi) {
        this.serviceGroupeDsi = serviceGroupeDsi;
    }

    /**
     * Supprime l'ensemble des groupes utilisateurs liés au groupe dont le code est fourni en paramètre.
     * @param groupCode le code du groupe dont on souhaite supprimer toutes les entrées
     */
    public void deleteByGroup(final String groupCode) {
        dao.deleteByGroupCode(groupCode);
    }

    /**
     * Supprime l'ensemble des groupes utilisateurs liés à l'utilisateur dont le code est fourni en paramètre.
     * @param userCode le code de l'utilisateur dont on souhaite supprimer toutes les entrées
     */
    public void deleteByUser(final String userCode) {
        dao.deleteByUserCode(userCode);
    }

    /**
     * Supprime l'ensemble des groupes utilisateurs liés à l'utilisateur et au groupe dont les codes sont fourni en paramètre.
     * @param userCode le code de l'utilisateur dont on souhaite supprimer les entrées
     * @param groupCode le code du groupe dont on souhaite supprimer les entrées
     */
    public void deleteByUserAndGroup(final String userCode, final String groupCode) {
        dao.deleteByUserCodeAndGroupCode(userCode, groupCode);
    }

    /**
     * Suppression des relations associées à un groupe et a une source d'import.
     *
     * @param sourceImport la source d'import dont on souhaite supprimer les entrées
     * @param groupCode le code du groupe dont on souhaite supprimer les entrées
     *
     */
    public void deleteByGroupAndImportSource(final String groupCode, final String sourceImport) {
        dao.deleteByGroupCodeAndImportSource(groupCode, sourceImport);
    }

    /**
     * Suppression des relations associées à un utilisateur et a une source d'import.
     *
     * @param userCode le code de l'utilisateur dont on souhaite supprimer les entrées
     * @param sourceImport la source d'import dont on souhaite supprimer les entrées
     */
    public void deleteByUserAndImportSource(final String userCode, final String sourceImport) {
        dao.deleteByUserCodeAndImportSource(userCode, sourceImport);
    }

    /**
     * Récupération des groupes utilisateurs liés au code utilisateur fourni en paramètre.
     * @param userCode le code de l'utilisateur dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des groupes utilisateurs liés au code utilisateur fourni en paramètre ou une liste vide si rien n'est trouvé
     */
    public List<GroupeUtilisateurBean> getByUserCode(final String userCode) {
        return dao.selectByUserCode(userCode);
    }

    /**
     * Récupération des groupes utilisateurs liés à la source d'impport fourni en paramètre.
     * @param importSource la source d'import dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des groupes utilisateurs liés à la source d'impport fourni en paramètre ou une liste vide si rien n'est trouvé
     */
    public List<GroupeUtilisateurBean> getByImportSource(final String importSource) {
        return dao.selectByImportSource(importSource);
    }

    /**
     * Récupération des groupes utilisateurs liés au code utilisateur et à la source d'impport fourni en paramètre.
     * @param userCode le code de l'utilisateur dont on souhaite récupérer les groupes utilisateurs
     * @param importSource la source d'import dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des groupes utilisateurs liés aux paramètres ou une liste vide si rien n'est trouvé
     */
    public List<GroupeUtilisateurBean> getByUserCodeAndImportSource(final String userCode, final String importSource) {
        return dao.selectByUserCodeAndImportSource(userCode, importSource);
    }

    /**
     * Récupération des groupes utilisateurs de l'utilisateur et de la source d'import fourni en paramètre mais qui ne sont pas dans la liste des groupes fourni.
     * @param userCode le code de l'utilisateur dont on souhaite récupérer les groupes utilisateurs
     * @param groupsCodes l'ensemble des groupes à exclure
     * @param importSource la source d'import dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des groupes utilisateurs liés aux paramètres ou une liste vide si rien n'est trouvé
     */
    public List<GroupeUtilisateurBean> getByUserNotInGroupCodeAndImportSource(final String userCode, final Collection<String> groupsCodes, final String importSource) {
        return dao.selectByUserNotInGroupCodeAndImportSource(userCode, groupsCodes, importSource);
    }

    /**
     * Récupération des groupes utilisateurs liés au groupe et à la source d'impport fourni en paramètre.
     * @param groupCode le code du groupe dont on souhaite récupérer les groupes utilisateurs
     * @param importSource la source d'import dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des groupes utilisateurs liés aux paramètres ou une liste vide si rien n'est trouvé
     */
    public List<GroupeUtilisateurBean> getByGroupCodeAndImportSource(final String groupCode, final String importSource) {
        return dao.selectByGroupCodeAndImportSource(groupCode, importSource);
    }

    /**
     * Récupération des groupes utilisateurs liés au groupe et à l'utilisateur fourni en paramètre.
     * @param userCode le code de l'utilisateur dont on souhaite récupérer les groupes utilisateurs
     * @param groupCode le code du groupe dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des groupes utilisateurs liés aux paramètres ou une liste vide si rien n'est trouvé
     */
    public List<GroupeUtilisateurBean> getByUserCodeAndGroupCode(final String userCode, final String groupCode) {
        return dao.selectByUserAndGroup(userCode, groupCode);
    }

    /**
     * Récupération des groupes utilisateurs liés au groupe fourni en paramètre.
     * @param groupCode le code du groupe dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des groupes utilisateurs liés au paramètre ou une liste vide si rien n'est trouvé
     */
    public List<GroupeUtilisateurBean> getByGroupCode(final String groupCode) {
        return dao.selectByGroupCode(groupCode);
    }

    /**
     * Récupération des groupes utilisateurs liés aux groupes fourni en paramètre.
     * @param groupCodes l'ensemble des codes de groupes dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des groupes utilisateurs liés au paramètre ou une liste vide si rien n'est trouvé
     */
    public List<GroupeUtilisateurBean> getInGroupCode(final Collection<String> groupCodes) {
        return dao.selectInGroupCode(groupCodes);
    }

    /**
     * Récupération du groupe utilisateur par ses 3 paramètres.
     * @param userCode le code de l'utilisateur dont on souhaite récupérer le groupe utilisateur
     * @param groupCode le code du groupe dont on souhaite récupérer le groupe utilisateur
     * @param importSource la source d'import dont on souhaite récupérer le groupes utilisateur
     * @return le groupe lié aux paramètres ou null si non trouvé
     */
    public GroupeUtilisateurBean getByUserCodeGroupCodeAndImportSource(final String userCode, final String groupCode, final String importSource) {
        return dao.selectByUserGroupAndImportSource(userCode, groupCode, importSource);
    }

    /**
     * Récupération des codes de groupes utilisateurs liés au code du groupe et à la source d'impport fourni en paramètre.
     * @param groupCode le code du groupe dont on souhaite récupérer les groupes utilisateurs
     * @param importSource la source d'import dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des code des groupes utilisateurs liés aux paramètres ou une liste vide si rien n'est trouvé
     */
    public Collection<String> getUserCodeByGroupAndImportSource(final String groupCode, final String importSource) {
        final Collection<String> usersCodes = new ArrayList<>();
        final List<GroupeUtilisateurBean> groupsByCodeAndSource = getByGroupCodeAndImportSource(groupCode, importSource);
        for (final GroupeUtilisateurBean groupeUtilisateurBean : groupsByCodeAndSource) {
            usersCodes.add(groupeUtilisateurBean.getCodeUtilisateur());
        }
        return usersCodes;
    }

    /**
     * Récupération des code sde groupes utilisateurs liés au code de l'utilisateur et à la source d'impport fourni en paramètre.
     * @param userCode le code de l'utilisateur dont on souhaite récupérer les groupes utilisateurs
     * @param importSource la source d'import dont on souhaite récupérer les groupes utilisateurs
     * @return l'ensemble des code des groupes utilisateurs liés aux paramètres ou une liste vide si rien n'est trouvé
     */
    public Collection<String> getGroupCodeByUserAndImportSource(final String userCode, final String importSource) {
        final Collection<String> groupsCodes = new ArrayList<>();
        final List<GroupeUtilisateurBean> groupsByCodeAndSource = getByUserCodeAndImportSource(userCode, importSource);
        for (final GroupeUtilisateurBean groupeUtilisateurBean : groupsByCodeAndSource) {
            groupsCodes.add(groupeUtilisateurBean.getCodeGroupe());
        }
        return groupsCodes;
    }


    /**
     * Renvoie la liste des utilisateurs des groupes (parcours hiérarchique).
     * @param codesGroupe La chaine de caractère contenant le ou les codes (pour rétro-compatibilité, lorsque le code contient un ";", il est splitté en plusieurs code de groupe)
     * @return la liste des codes utilisateurs des groupes (parcours hiérarchique).
     * @throws Exception A cause des espaces collaboratifs, à Supprimer lorsqu'ils auront étés migrés
     */
    public Collection<String> getAllUserCodeByGroup(final String codesGroupe) throws Exception {
        Collection<String> result = Collections.emptyList();
        if (StringUtils.isNotEmpty(codesGroupe))  {
            result = getAllUserCodeByGroup(Arrays.asList(StringUtils.split(codesGroupe,";")));
        }
        return result;
    }


    /**
     * Renvoie la liste des codes utilisateurs des groupes (parcours hiérarchique).
     * @param codesGroupe l'ensemble des groupes dont on souhaite récupérer les code des utilisateurs
     * @return la liste des codes utilisateurs des groupes fourni (parcours hiérarchique).
     * @throws Exception A cause des espaces collaboratifs, à Supprimer lorsqu'ils auront étés migrés
     */
    public Collection<String> getAllUserCodeByGroup(final Collection<String> codesGroupe) throws Exception {
        final Set<String> allUserCode = new HashSet<>();
        if (CollectionUtils.isNotEmpty(codesGroupe)) {
            final Set<GroupeDsiBean> allGroups = new HashSet<>();
            for (final String codeGroupe : codesGroupe) {
                final GroupeDsiBean currentGroup = serviceGroupeDsi.getByCode(codeGroupe);
                // Pas d'héritage sur les groupes dynamiques
                if (StringUtils.isEmpty(currentGroup.getRequeteGroupe())) {
                    allGroups.addAll(serviceGroupeDsi.getAllSubGroups(codeGroupe));
                }
                allGroups.add(currentGroup);
            }
            final Collection<String> codesGroupesPossible = new ArrayList<>();
            for (final GroupeDsiBean currentGroup : allGroups) {
                if (StringUtils.isNotBlank(currentGroup.getRequeteGroupe())) {
                    /* JSS 20050510 : groupes dynamiques */
                    final Collection<String> codesUtilisateurs = RequeteGroupeUtil.getVecteurUtilisateurs(currentGroup.getCode(), null);
                    allUserCode.addAll(codesUtilisateurs);
                } else {
                    codesGroupesPossible.add(currentGroup.getCode());
                }
            }
            if (CollectionUtils.isNotEmpty(codesGroupesPossible)) {
                final List<GroupeUtilisateurBean> allByGroupCode = getInGroupCode(codesGroupesPossible);
                for (final GroupeUtilisateurBean groupeUtilisateurBean : allByGroupCode) {
                    allUserCode.add(groupeUtilisateurBean.getCodeUtilisateur());
                }
            }
        }
        return allUserCode;
    }
    /**
     * Valorise la liste des utilisateurs d'un groupe. Si d'autre entrée existait précédement, ces dernières sont effacées et remplacés par la liste fourni en paramètre.
     * @param usersCodes la liste des utilisateurs à mettre à jour pour le groupe donnée.
     * @param groupCode le code du groupe dont on souhaite mettre à jour la liste des utilisateurs
     */
    public void setUsersForGroup(final Collection<String> usersCodes, final String groupCode) {
        /* On supprime les relations obsolètes */
        final List<GroupeUtilisateurBean> userGroupsByCode = getByGroupCode(groupCode);
        for (final GroupeUtilisateurBean groupeUtilisateurBean : userGroupsByCode) {
            if (!usersCodes.contains(groupeUtilisateurBean.getCodeUtilisateur())) {
                delete(groupeUtilisateurBean.getIdGroupeutilisateur());
            }
        }
        addUsersToGroup(usersCodes, groupCode);
    }

    /**
     * Ajoute en base de données l'ensemble des utilisateurs fourni en paramètre pour le groupe donnée.
     * @param usersCodes les codes des utilisateurs à ajouter
     * @param groupCode le code du groupe à mettre à jour
     */
    public void addUsersToGroup(final Collection<String> usersCodes, final String groupCode) {
        for (final String userCode : usersCodes) {
            if (CollectionUtils.isEmpty(getByUserCodeAndGroupCode(userCode, groupCode))) {
                final GroupeUtilisateurBean userGroup = new GroupeUtilisateurBean();
                userGroup.setCodeUtilisateur(userCode);
                userGroup.setCodeGroupe(groupCode);
                save(userGroup);
            }
        }
    }

    /**
     * Synchronise la liste des utilisateurs d'un groupe pour une source d'import.
     * @param usersCodes la liste des codes utilisateurs à mettre à jour pour le groupe et la source d'import donnée
     * @param groupCode le code du groupe dont on souhaite mettre à jour les données
     * @param sourceImport la source d'import dont on souhaite mettre à jour les données
     */
    public void synchronizeUsersForGroup(final Collection<String> usersCodes, final String groupCode, final String sourceImport) {
        /* Ajout des nouvelles valeurs */
        for (final String codeUtilisateur : usersCodes) {
            final List<GroupeUtilisateurBean> usersGroups = getByUserCodeAndGroupCode(codeUtilisateur, groupCode);
            if (CollectionUtils.isEmpty(usersGroups)) {
                final GroupeUtilisateurBean userGroup = new GroupeUtilisateurBean();
                userGroup.setCodeUtilisateur(codeUtilisateur);
                userGroup.setCodeGroupe(groupCode);
                userGroup.setSourceImport(sourceImport);
                save(userGroup);
            } else {
                for (final GroupeUtilisateurBean userGroup : usersGroups) {
                    userGroup.setSourceImport(sourceImport);
                    save(userGroup);
                }
            }
        }
        final Collection<String> oldUsers = getUserCodeByGroupAndImportSource(groupCode, sourceImport);
        /* Suppression des valeurs qui ne sont plus passées dans usersCodes */
        for (final String codeUtilisateurAPurger : oldUsers) {
            if (!usersCodes.contains(codeUtilisateurAPurger)) {
                final List<GroupeUtilisateurBean> usersGroupsToDelete = getByUserCodeAndGroupCode(codeUtilisateurAPurger, groupCode);
                for (final GroupeUtilisateurBean userGroupToDelete : usersGroupsToDelete) {
                    delete(userGroupToDelete.getIdGroupeutilisateur());
                }
            }
        }
    }


    /**
     * Valorise la liste des groupes d'un utilisateur.
     * Cette méthode conserve les sources d'import pour les relations déjà présentes.
     * @param codeUtilisateur l'utilisateur dont on souhaite mettre à jour les groupes
     * @param listeGroupes la liste des groupes à mettre à jour pour l'utilisateur. Si le code du groupe commence par {@link ServiceGroupeUtilisateur#PREFIX_AUTOMATIC_GROUP}, le groupe n'est pas pris en compte
     * @throws Exception A cause des espaces collaboratifs, à Supprimer lorsqu'ils auront étés migrés
     */
    public void setGroupsForUser(final String codeUtilisateur, final Collection<String> listeGroupes) throws Exception {
        /* Ajout des nouvelles valeurs */
        for (final String codeGroupe : listeGroupes) {
            if (!StringUtils.startsWith(codeGroupe, PREFIX_AUTOMATIC_GROUP)) {
                final GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupe);
                /* JSS 20050510 : On ne stocke pas les groupes dynamiques */
                if (StringUtils.isEmpty(group.getRequeteGroupe())) {
                    final List<GroupeUtilisateurBean> usersGroups = getByUserCodeAndGroupCode(codeUtilisateur, codeGroupe);
                    if (CollectionUtils.isEmpty(usersGroups)) {
                        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
                        groupeUtilisateurBean.setCodeUtilisateur(codeUtilisateur);
                        groupeUtilisateurBean.setCodeGroupe(codeGroupe);
                        save(groupeUtilisateurBean);
                    }
                }
            }
        }
        /* Suppression des valeurs qui ne sont plus passées dans listeGroupe */
        final Collection<String> listeAPurger = getAllGroupsCodesByUserCode(codeUtilisateur);
        for (final String codeGroupeAPurger : listeAPurger) {
            if (!listeGroupes.contains(codeGroupeAPurger)) {
                final List<GroupeUtilisateurBean> usersGroupsToDelete = getByUserCodeAndGroupCode(codeUtilisateur, codeGroupeAPurger);
                for (final GroupeUtilisateurBean groupeUtilisateurBean : usersGroupsToDelete) {
                    delete(groupeUtilisateurBean.getIdGroupeutilisateur());
                }
            }
        }
    }

    /**
     * Ajoute en base de données l'ensemble des groupes fourni en paramètre pour l'utilisateur donnée.
     * @param groupsCodes les codes des groupes à ajouter
     * @param userCode  le code de l'utilisateur à mettre à jour
     */
    public void addGroupsToUser(final Collection<String> groupsCodes, final String userCode) {
        if (groupsCodes != null) {
            for (final String groupCode : groupsCodes) {
                if (CollectionUtils.isEmpty(getByUserCodeAndGroupCode(userCode, groupCode))) {
                    final GroupeUtilisateurBean userGroup = new GroupeUtilisateurBean();
                    userGroup.setCodeUtilisateur(userCode);
                    userGroup.setCodeGroupe(groupCode);
                    save(userGroup);
                }
            }
        }
    }

    /**
     * Synchronise la liste des groupes d'un utilisateur pour une source d'import.
     * @param groupsCodes la liste des codes de groupes à mettre à jour pour le groupe et la source d'import donnée
     * @param userCode le code de l'utilisateur dont on souhaite mettre à jour les données
     * @param sourceImport la source d'import dont on souhaite mettre à jour les données
     */
    public void synchronizeGroupsForUser(final Collection<String> groupsCodes, final String userCode, final String sourceImport) {
        /* Ajout des nouvelles valeurs */
        for (final String codeGroupe : groupsCodes) {
            final List<GroupeUtilisateurBean> usersGroups = getByUserCodeAndGroupCode(userCode, codeGroupe);
            if (CollectionUtils.isEmpty(usersGroups)) {
                final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
                groupeUtilisateurBean.setCodeGroupe(codeGroupe);
                groupeUtilisateurBean.setCodeUtilisateur(userCode);
                groupeUtilisateurBean.setSourceImport(sourceImport);
                save(groupeUtilisateurBean);
            } else {
                for (final GroupeUtilisateurBean groupeUtilisateurBean : usersGroups) {
                    groupeUtilisateurBean.setSourceImport(sourceImport);
                    save(groupeUtilisateurBean);
                }
            }
        }
        /* Suppression des valeurs qui ne sont plus passées dans listeGroupe */
        final Collection<String> oldGroupes = getGroupCodeByUserAndImportSource(userCode, sourceImport);
        for (final String codeGroupeAPurger : oldGroupes) {
            if (!groupsCodes.contains(codeGroupeAPurger)) {
                final List<GroupeUtilisateurBean> usersGroupsToDelete = getByUserCodeAndGroupCode(userCode, codeGroupeAPurger);
                for (final GroupeUtilisateurBean groupeUtilisateurBean : usersGroupsToDelete) {
                    delete(groupeUtilisateurBean.getIdGroupeutilisateur());
                }
            }
        }
    }

    /**
     * Renvoie la liste des codes de groupes utilisateurs d'un utilisateur.
     * @param codeUtilisateur le code de l'utilisateur dont on souhaite récupérer les groupes.
     * @return La liste des groupes de l'utilisateur. Si le code de l'utilisateur est vide/null ou si l'utilisateur n'a aucun groupe, on retourne une liste vide.
     * @throws Exception A cause des espaces collaboratifs, à Supprimer lorsqu'ils auront étés migrés
     */
    public Collection<String> getAllGroupsCodesByUserCode(final String codeUtilisateur) throws Exception {
        final Set<String> allGroupsByUser = new HashSet<>();
        if (StringUtils.isEmpty(codeUtilisateur)) {
            return Collections.emptyList();
        }
        final Map<String, InfosRequeteGroupe> listeRequetes = serviceGroupeDsi.getGroupRequestList();
        if (listeRequetes != null) {
            for (final InfosRequeteGroupe infosRequete : listeRequetes.values()) {
                final long ts = System.currentTimeMillis();
                if (infosRequete.getAlias().length() > 0) {
                    // appel du calcul des groupes de l'utilisateur
                    final Collection<String> listeGroupes = RequeteGroupeUtil.getGroupesUtilisateur(infosRequete, codeUtilisateur, ts);
                    allGroupsByUser.addAll(listeGroupes);
                }
            }
        }
        final List<GroupeUtilisateurBean> results = dao.selectByUserCode(codeUtilisateur);
        for (final GroupeUtilisateurBean groupeUtilisateurBean : results) {
            allGroupsByUser.add(groupeUtilisateurBean.getCodeGroupe());
        }
        return allGroupsByUser;
    }

    /**
     * Renvoie la liste des codes de groupes d'un utilisateur (hors groupes dynamiques).
     * @param codeUtilisateur le code de l'utilisateur dont on souhaite récupérer les groupes.
     * @return La liste des groupes de l'utilisateur. Si le code de l'utilisateur est vide/null ou si l'utilisateur n'a aucun groupe, on retourne une liste vide.
     */
    public Collection<String> getAllDatasourceGroupsCodesByUserCode(final String codeUtilisateur) {
        final Set<String> allGroupsByUser = new HashSet<>();
        if (StringUtils.isEmpty(codeUtilisateur)) {
            return Collections.emptyList();
        }
        final List<GroupeUtilisateurBean> results = dao.selectByUserCode(codeUtilisateur);
        for (final GroupeUtilisateurBean groupeUtilisateurBean : results) {
            allGroupsByUser.add(groupeUtilisateurBean.getCodeGroupe());
        }
        return allGroupsByUser;
    }

    /**
     * Renvoie la liste des codes de groupes dynamiques d'un utilisateur.
     * @param codeUtilisateur le code de l'utilisateur dont on souhaite récupérer les groupes.
     * @return La liste des groupes de l'utilisateur. Si le code de l'utilisateur est vide/null ou si l'utilisateur n'a aucun groupe, on retourne une liste vide.
     * @throws Exception A cause des groupes dynamiques, à Supprimer lorsqu'ils auront étés migrés
     */
    public Collection<String> getAllDynamicGroupsCodesByUserCode(final String codeUtilisateur) throws Exception {
        final Set<String> allGroupsByUser = new HashSet<>();
        if (StringUtils.isEmpty(codeUtilisateur)) {
            return Collections.emptyList();
        }
        final Map<String, InfosRequeteGroupe> listeRequetes = serviceGroupeDsi.getGroupRequestList();
        if (listeRequetes != null) {
            for (final InfosRequeteGroupe infosRequete : listeRequetes.values()) {
                final long ts = System.currentTimeMillis();
                if (infosRequete.getAlias().length() > 0) {
                    // appel du calcul des groupes de l'utilisateur
                    final Collection<String> listeGroupes = RequeteGroupeUtil.getGroupesUtilisateur(infosRequete, codeUtilisateur, ts);
                    allGroupsByUser.addAll(listeGroupes);
                }
            }
        }
        return allGroupsByUser;
    }
}
