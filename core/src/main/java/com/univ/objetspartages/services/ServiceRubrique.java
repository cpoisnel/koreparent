package com.univ.objetspartages.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.AbstractServiceBean;
import com.kportal.extension.module.plugin.rubrique.impl.BeanFichePageAccueil;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.dao.impl.RubriqueDAO;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;
import com.univ.utils.json.CodecJSon;

/**
 * Implémentation d'un service pour les rubriques. Il  permet de gérer l'accès au données etc.
 */
public class ServiceRubrique extends AbstractServiceBean<RubriqueBean, RubriqueDAO> {

    /**
     * Le code de la rubrique racine de l'arbre des rubriques.
     */
    public static final String CODE_RUBRIQUE_ROOT = "00";

    /**
     * un code de rubrique qui ne peut exister. Uniquement présent pour d'ancien comportement...
     */
    public static final String CODE_RUBRIQUE_INEXISTANTE = "ZYZYZYZYZYZYZYZYZYZYZY";

    private static final String ASCENDANT_SEPARATOR = "&nbsp;&gt;&nbsp;";

    private ServiceRubriquePublication serviceRubriquePublication;

    private ServiceMetatag serviceMetatag;

    public void setServiceRubriquePublication(final ServiceRubriquePublication serviceRubriquePublication) {
        this.serviceRubriquePublication = serviceRubriquePublication;
    }

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    /**
     * Méthode surchargés afin de rajouter la mise à jours des caches liés aux rurbriques.
     * Le cache est mis à jour si on est en modification.
     * @param bean le bean à sauvegarder (mettre à jours, ou créer)
     */
    @Override
    @Caching(evict = {
        @CacheEvict(value = "ServiceRubrique.getRubriqueByCode", key = "#bean.getCode()"),
        @CacheEvict(value = "ServiceRubrique.getRubriqueByCodeParent", allEntries = true),
	    @CacheEvict(value = "ServiceRubrique.getFirstOfCategoryInSubTree", allEntries = true)
    })
    public void save(final RubriqueBean bean) {
        final RubriqueBean oldValue = getById(bean.getId());
        super.save(bean);
        if (oldValue == null || isUrlChanged(oldValue, bean)) {
            setChanged();
            notifyObservers(bean.getCode());
        }
    }

    private boolean isUrlChanged(final RubriqueBean oldValue, final RubriqueBean newValue) {
        boolean isChanged = oldValue.getIntitule() != null && !oldValue.getIntitule().equals(newValue.getIntitule());
        isChanged = isChanged || (oldValue.getCodeRubriqueMere() != null && !oldValue.getCodeRubriqueMere().equals(newValue.getCodeRubriqueMere()));
        isChanged = isChanged || (oldValue.getPageAccueil() != null && !oldValue.getPageAccueil().equals(newValue.getPageAccueil()));
        return isChanged;
    }

    /**
     * Va chercher en bdd la rubrique ayant pour code la valeur fourni en parametre.
     *
     * @param code le code de la rubrique à récupérer
     * @return la rubrique ayant pour code, la valeur fourni en paramètre ou null si non trouvé
     */
    @Cacheable(value = "ServiceRubrique.getRubriqueByCode")
    public RubriqueBean getRubriqueByCode(final String code) {
        return dao.selectByCode(code);
    }

    /**
     * Va chercher en bdd l'ensemble des rubriques ayant pour code de rubrique mere la valeur fourni en paramètre.
     *
     * @param codeParent le code de la rubrique parente
     * @return l'ensemble des rubriques beans ayant pour rubrique parente le code fourni en paramètre
     */
    @Cacheable(value = "ServiceRubrique.getRubriqueByCodeParent")
    public List<RubriqueBean> getRubriqueByCodeParent(final String codeParent) {
        return dao.selectByCodeParent(codeParent);
    }

    /**
     * équivalent à {@link #getRubriqueByCodeParent(String)} mais en rajoutant les contrôles de droit d'acces.
     * @param codeParent le code de la rubrique parentes
     * @param listeGroupes les groupes de l'utilisateurs courant. Si le contexte est disponnible ils sont stocké dans ctx.getGroupesDsiAvecAscendants()
     * @return les tubriques ou on a les droits et dont la catégorie n'est pas "HIDDEN" (pour la rétro compat...)
     */
    public List<RubriqueBean> getRubriqueByCodeParentFront(final String codeParent,final Set<String> listeGroupes) {
        final List<RubriqueBean> result = new ArrayList<>();
        for (final RubriqueBean child : getRubriqueByCodeParent(codeParent)) {
            if (controlerRestrictionRubrique(listeGroupes, child) && !"HIDDEN".equalsIgnoreCase(child.getCategorie())) {
                result.add(child);
            }
        }
        return result;
    }

    /**
     * Récupère l'ensemble des rubriques de l'application.
     *
     * @return l'ensemble des rubriques de l'application ou une liste vide si rien n'est trouvé.
     */
    public List<RubriqueBean> getAllRubriques() {
        return dao.selectAllRubriques();
    }

    /**
     * Récupère l'ensemble des rubriques de l'application.
     *
     * @return l'ensemble des rubriques de l'application ou une liste vide si rien n'est trouvé.
     */
    public List<RubriqueBean> getAllWithoutUrl() {
        return dao.selectAllWithoutUrl();
    }

    /**
     * Récupère les rubriques dont le code est fourni en paramètre.
     *
     * @param codes l'ensemble des codes dont on souhaite récupérer les rubriques
     * @return la liste des rubriques correspondantes ou une liste vide si rien n'est trouvé.
     */
    public List<RubriqueBean> getRubriqueInCodes(final Collection<String> codes) {
        List<RubriqueBean> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(codes)) {
            result = dao.selectByCodes(codes);
        }
        return result;
    }

    /**
     * Surcharge pour permettre de mettre à jour le cache lié aux rubriques.
     * Attention, contrairement à {@link ServiceRubrique#deletebyId(Long)} aucun contrôle n'est fait sur les contenus de la rubrique.
     * @param id l'id de la rubrique à supprimer
     */
    @Override
    @Caching(
        evict = {
            @CacheEvict(value = "ServiceRubrique.getRubriqueByCode", key="#root.target.getById(#id).getCode()", beforeInvocation = true),
            @CacheEvict(value = "ServiceRubrique.getRubriqueByCodeParent", allEntries = true, beforeInvocation = true),
	        @CacheEvict(value = "ServiceRubrique.getFirstOfCategoryInSubTree", allEntries = true, beforeInvocation = true)
        }
    )
    public void delete(final Long id) {
        final RubriqueBean rubriqueBean = getById(id);
        if (rubriqueBean != null) {
            setChanged();
            notifyObservers(rubriqueBean.getCode());
        }
        super.delete(id);
    }

    /**
     * Vérifie si pour le code de rubrique donnée, on possède des contenus associés.
     * On regarde sur : les metas, les sous rubriques, et les rubrique de multipublication (si ce n'est pas des rubriques auto)
     * @param code le code de la rubrique.
     * @return vrai ssi la rubrique à du contenu associés.
     */
    public boolean hasContent(final String code) {
        boolean result = false;
        if (StringUtils.isNotEmpty(code)) {
            result = hasMetas(code) || hasChilds(code) || hasContentOnRubPub(code);
        }
        return result;
    }

    /**
     * Vérifie si pour le code de rubrique donnée, on possède des fiches associées.
     * Le test n'est pas fait si le code n'est pas renseigné.
     * @param code le code de la rubrique
     * @return vrai ssi il y a des meta avec pour code de rubrique, le code fourni en paramètre.
     */
    public boolean hasMetas(final String code) {
        boolean result = false;
        if (StringUtils.isNotEmpty(code)) {
            final List<MetatagBean> metas = serviceMetatag.getMetasForRubriqueAndStates(code, Arrays.asList(EtatFiche.BROUILLON.getEtat(),EtatFiche.A_VALIDER.getEtat(), EtatFiche.EN_LIGNE.getEtat()));
            result = !metas.isEmpty();
        }
        return result;
    }

    /**
     * Vérifie si pour le code de rubrique donnée, il y a des sous rubriques.
     * Le test n'est pas fait si le code n'est pas renseigné.
     * @param code le code de la rubrique
     * @return vrai ssi il y a des rubriques ayant pour code parent, le code fourni en paramètre.
     */
    public boolean hasChilds(final String code) {
        boolean result = false;
        if (StringUtils.isNotEmpty(code)) {
            final List<RubriqueBean> childs = getRubriqueByCodeParent(code);
            result = !childs.isEmpty();
        }
        return result;
    }

    /**
     * Vérifie si pour le code de rubrique donnée, on possède des contenu associé via des rubriques de publication.
     * Le test n'est pas fait si le code n'est pas renseigné.
     * @param code le code de la rubrique
     * @return vrai ssi il y a des rubrique de publication avec le code fourni en paramètre et que ce ne sont pas des rubriques auto.
     */
    public boolean hasContentOnRubPub(final String code) {
        boolean result = false;
        if (StringUtils.isNotEmpty(code)) {
            final List<RubriquepublicationBean> metas = serviceRubriquePublication.getByRubriqueDestWithoutSource(code);
            result = !metas.isEmpty();
        }
        return result;
    }

    /**
     *
     * Supprime une rubrique par rapport à son id. Avant la suppression, on vérifie si des contenus ne lui sont pas rattachés.
     * Si c'est le cas, la rubrique n'est pas supprimer et une exception est levé.
     *
     * @param id l'id de la rubrique à supprimer
     * @throws ErreurApplicative lorsque du contenu est rattaché à la rubrique.
     */
    @Caching(
        evict = {
            @CacheEvict(value = "ServiceRubrique.getRubriqueByCode", key="#root.target.getById(#id).getCode()", beforeInvocation = true),
            @CacheEvict(value = "ServiceRubrique.getRubriqueByCodeParent", allEntries = true),
            @CacheEvict(value = "ServiceRubrique.getFirstOfCategoryInSubTree", allEntries = true)
        }
    )
    public void deletebyId(final Long id) throws ErreurApplicative {
        final RubriqueBean rubriqueToDelete = dao.getById(id);
        if (rubriqueToDelete != null) {
            if (hasContent(rubriqueToDelete.getCode())) {
                throw new ErreurApplicative(String.format("La rubrique \"%s\" ne peut pas être supprimée : des fiches y sont rattachées.", rubriqueToDelete.getIntitule()));
            } else if (hasChilds(rubriqueToDelete.getCode())) {
                throw new ErreurApplicative(String.format("La rubrique \"%s\" ne peut pas être supprimée : des rubriques y sont rattachées", rubriqueToDelete.getIntitule()));
            }
            dao.delete(id);
            serviceRubriquePublication.deleteByRubriqueDestAndNoSource(rubriqueToDelete.getCode());
            setChanged();
            notifyObservers(rubriqueToDelete.getCode());
        }
    }

    /**
     * Supprime les rubriques dont le code est fourni en paramètre.
     * Attention, contrairement à {@link ServiceRubrique#deletebyId(Long)} aucun contrôle n'est fait sur les contenus de la rubrique.
     *
     * @param codes l'ensemble des codes de rubriques à supprimer
     */
    @CacheEvict(value = { "ServiceRubrique.getRubriqueByCode", "ServiceRubrique.getRubriqueByCodeParent", "ServiceRubrique.getFirstOfCategoryInSubTree" }, allEntries = true)
    public void deleteByCodes(final Collection<String> codes) {
        dao.deleteByCodes(codes);
        for (final String code : codes) {
            setChanged();
            notifyObservers(code);
        }
    }

    /**
     * Supprime la rubrique dont le code est fourni en paramètre.
     * Attention, contrairement à {@link ServiceRubrique#deletebyId(Long)} aucun contrôle n'est fait sur les contenus de la rubrique.
     *
     * @param code le code de rubrique à supprimer
     */
    public void deleteByCode(final String code) throws ErreurApplicative {
        if (hasMetas(code)) {
            throw new ErreurApplicative(String.format("La rubrique ayant pour code \"%s\" ne peut pas être supprimée : des fiches y sont rattachées.", code));
        } else if (hasChilds(code)) {
            throw new ErreurApplicative(String.format("La rubrique ayant pour code \"%s\" ne peut pas être supprimée : des rubriques y sont rattachées", code));
        }
        deleteByCodes(Collections.singleton(code));
    }

    /**
     * check if the code is already used in the DB or not.
     *
     * @param code a string to check
     * @return true when the code exist in the DB
     */
    public boolean codeAlreadyExist(final String code) {
        final RubriqueBean rubriqueBean = dao.selectByCode(code);
        return rubriqueBean != null;
    }

    /**
     * Calcule la valeur max du champ ordre pour l'ensemble des rubriques ayant pour rubrique parente le code fourni en parametre.
     *
     * @param codeRubriqueMere le code de la rubrique parente
     * @return l'ordre max de la rubrique incrémenté de 1 pour connaitre la nouvelle valeur possible
     */
    public int getMaxOrderAvailable(final String codeRubriqueMere) {
        int result = 0;
        if (codeRubriqueMere != null) {
            result = dao.selectMaxOrder(codeRubriqueMere);
        }
        result++;
        return result;
    }

    /**
     * Retourne l'ensemble des enfants de la rubrique fourni en paramètre.
     * @param codeRubrique le code de la rubrique dont on souhaite connaitre les enfants.
     * @return l'ensemble des fils ou une collection vide si rien n'est trouvé.
     */
    public List<RubriqueBean> getAllChilds(final String codeRubrique) {
        final List<RubriqueBean> result = new ArrayList<>();
        if (StringUtils.isNotBlank(codeRubrique)) {
            final List<RubriqueBean> directChild = getRubriqueByCodeParent(codeRubrique);
            for (final RubriqueBean currentChild : directChild) {
                result.addAll(getAllChilds(currentChild.getCode()));
                result.add(currentChild);
            }
        }
        return result;
    }

    /**
     * Retourne une chaine contenant la concaténation des labels des rubriques parentes et de la rubrique fourni en paramètre séparé par des {@link #ASCENDANT_SEPARATOR}.
     *
     * @param codeRubrique le code de la rubrique à dont on souhaite récupérer le fil d'ariane
     * @return le fil d'ariane en incluant la rubrique courante
     */
    public String getLabelWithAscendantsLabels(final String codeRubrique) {
        final RubriqueBean rubriqueBean = getRubriqueByCode(codeRubrique);
        return getLabelWithAscendantsLabels(rubriqueBean);
    }

    /**
     * Retourne une chaine contenant la concaténation des labels des rubriques parentes et de la rubrique fourni en paramètre séparé par des {@link #ASCENDANT_SEPARATOR}.
     *
     * @param rubriqueBean la rubrique à dont on souhaite récupérer le fil d'ariane
     * @return le fil d'ariane en incluant la rubrique courante
     */
    public String getLabelWithAscendantsLabels(final RubriqueBean rubriqueBean) {
        String label = StringUtils.EMPTY;
        if (rubriqueBean != null) {
            label = getAscendantsLabelOnly(rubriqueBean);
            if (StringUtils.isNotEmpty(label)) {
                label += ASCENDANT_SEPARATOR;
            }
            label += rubriqueBean.getIntitule();
        }
        return label;
    }

    /**
     * Retourne une chaine contenant la concaténation des labels des rubriques parentes de la rubrique fourni en paramètre séparé par des {@link #ASCENDANT_SEPARATOR}.
     * Attention, il peut être préférable de récupérer directement la liste des ascendants via {@link #getAllAscendant(RubriqueBean)} ou {@link #getAllAscendant(String)}
     *
     * @param codeRubrique le code de la rubrique à dont on souhaite récupérer le fil d'ariane
     * @return le fil d'ariane sans inclure la rubrique courante
     */
    public String getAscendantsLabelOnly(final String codeRubrique) {
        final RubriqueBean rubriqueBean = getRubriqueByCode(codeRubrique);
        return getAscendantsLabelOnly(rubriqueBean);
    }

    /**
     * Retourne une chaine contenant la concaténation des labels des rubriques parentes de la rubrique fourni en paramètre séparé par des {@link #ASCENDANT_SEPARATOR}.
     * Attention, il peut être préférable de récupérer directement la liste des ascendants via {@link #getAllAscendant(RubriqueBean)} ou {@link #getAllAscendant(String)}
     *
     * @param rubriqueBean la rubrique à dont on souhaite récupérer le fil d'ariane
     * @return le fil d'ariane sans inclure la rubrique courante
     */
    public String getAscendantsLabelOnly(final RubriqueBean rubriqueBean) {
        String label = StringUtils.EMPTY;
        if (rubriqueBean != null) {
            final List<RubriqueBean> allAscendant = getAllAscendant(rubriqueBean);
            for (final RubriqueBean currentAscendant : allAscendant) {
                if (StringUtils.isNotEmpty(label)) {
                    label = ASCENDANT_SEPARATOR + label;
                }
                label = currentAscendant.getIntitule() + label;
            }
        }
        return label;
    }

    /**
     * Calcule l'ensemble des parents de la roubrique fourni en paramètre.
     * @param codeRubrique la rubrique dont on souhaite connaitre les parents. Il peut être vide ou null.
     * @return l'ensemble des parents ou une collection vide si rien n'est trouvé.
     */
    public List<RubriqueBean> getAllAscendant(final String codeRubrique) {
        final RubriqueBean currentSection = getRubriqueByCode(codeRubrique);
        return getAllAscendant(currentSection);
    }

    /**
     * Même comportement que la méthode {@link #getAllAscendant(String)} mais en manipulant directement une rubrique.
     * Elle permet d'éviter une requête en base.
     * @param rubriqueBean ma rubrique dont no souhaite connaitre les parents
     * @return l'ensemble des parents ou une collection vide si rien n'est trouvé.
     */
    public List<RubriqueBean> getAllAscendant(final RubriqueBean rubriqueBean) {
        final List<RubriqueBean> result = new ArrayList<>();
        RubriqueBean parent = null;
        if (rubriqueBean != null && StringUtils.isNotBlank(rubriqueBean.getCodeRubriqueMere())) {
            parent = getRubriqueByCode(rubriqueBean.getCodeRubriqueMere());
        }
        if (parent != null) {
            result.add(parent);
            while (parent != null && StringUtils.isNotBlank(parent.getCodeRubriqueMere())) {
                parent = getRubriqueByCode(parent.getCodeRubriqueMere());
                if (parent != null) {
                    result.add(parent);
                }
            }
        }
        return result;
    }

    /**
     * Récupère une liste de rubrique en fonction des paramètres fourni. Il vaut mieux ne pas utiliser cette méthode dans la mesure du possible.
     * Car 1/ Le code est unique, donc à partir du moment ou il est renseigné ce sera lui qui sera determinant.
     * 2/ Les méthodes getByCode/getByLanguage/getByLabel sont là pour ces cas précis plutôt que de passer par cette méthode en fournissant des paramètres vides.
     * 3/ Si aucun paramètre n'est renseigné, cela retourne l'ensemble des rubriques de la base, ça peut faire des gros traitement.
     *
     * @param code le code de la rubrique à renseigné, ou vide
     * @param language la langue de la rubrique
     * @param label l'intitulé. Lorsque ce dernier est renseigné, c'est un like qui est utilisé, pas un equals
     * @return l'ensemble des rubriques qui correspondent aux critères ou une liste vide si non trouvé.
     */
    public List<RubriqueBean> getRubriqueByCodeLanguageLabel(final String code, final String language, final String label) {
        return dao.getRubriqueByCodeLanguageLabel(code, language, label);
    }

    /**
     * Récupère une liste de rubrique en fonction des paramètres fourni. Il vaut mieux ne pas utiliser cette méthode dans la mesure du possible.
     * Car 1/ Le code est unique, donc à partir du moment ou il est renseigné ce sera lui qui sera determinant.
     * 2/ Les méthodes getByCode/getByLanguage/getByLabel sont là pour ces cas précis plutôt que de passer par cette méthode en fournissant des paramètres vides.
     * 3/ Si aucun paramètre n'est renseigné, cela retourne l'ensemble des rubriques de la base, ça peut faire des gros traitement.
     *
     * @param code le code de la rubrique à renseigné, ou vide
     * @param language la langue de la rubrique
     * @param label l'intitulé. Lorsque ce dernier est renseigné, c'est un like qui est utilisé, pas un equals
     * @return l'ensemble des rubriques qui correspondent aux critères ou une liste vide si non trouvé.
     */
    public List<RubriqueBean> getRubriqueByCodeLanguageLabelCategory(final String code, final String language, final String label, final String category) {
        return dao.getRubriqueByCodeLanguageLabelCategory(code, language, label, category);
    }

    /**
     * Va chercher en bdd, toutes les rubriques ayant des rubriques de publication.
     *
     * @return l'ensemble des rubriques ayant des rubriques de publication ou une liste vide si non trouvé
     */
    public List<RubriqueBean> getAllRubriqueWithPublicationRubrique() {
        return dao.selectAllRubriquesWithRubPub();
    }

    /**
     * Récupère les rubriques à partir de leur id de bandeau.
     *
     * @param bandeauId l'id du bandeau à rechercher
     * @return l'ensemble des rubriques ayant pour id bandeau ou une liste vide si non trouvé
     */
    public List<RubriqueBean> getByBandeau(final long bandeauId) {
        return dao.selectByBandeau(bandeauId);
    }

    /**
     * Récupère les rubriques possédant des références vers la fiche univ dont les infos sont fourni en paramètre.
     *
     * @param codeFiche le code de la fiche
     * @param language la langue de la fiche
     * @param nomObjet le nom de l'objet de la fiche
     * @return l'ensemble des rubriques correspondante ou une liste vide si non trouvé
     */
    public List<RubriqueBean> getRubriqueByReferences(final String codeFiche, final String language, final String nomObjet) throws IOException {
        final BeanFichePageAccueil beanFichePageTete = new BeanFichePageAccueil();
        beanFichePageTete.setCode(codeFiche);
        beanFichePageTete.setLangue(language);
        beanFichePageTete.setObjet(nomObjet);
        final String codePageTete = CodecJSon.encodeObjectToJSonInString(beanFichePageTete);
        final String idFicheReference = "[id-fiche]" + nomObjet + "[/id-fiche]";
        return dao.selectByReferences(codePageTete, idFicheReference);
    }

    /**
     * Récupère les rubriques à partir de leur libellé.
     *
     * @param label le libellé des rubriques que l'on souhaite récupérer
     * @return l'ensemble des rubriques correspondante ou une liste vide si non trouvé
     */
    public List<RubriqueBean> getRubriqueByLabel(final String label) {
        return dao.selectByLabel(label);
    }

    /**
     * Récupère la première rubrique correspondant à la catégorie fourni et étant fille du code de rubrique fourni en paramètre
     * @param rootCode la racine de l'arbo que l'on souhaite explorer
     * @param category la catégorie de rubrique que l'on souhaite récupérer.
     * @return la première rubrique retournée par la bdd. Il n'y a pas d'order by
     */
    @Cacheable(value = "ServiceRubrique.getFirstOfCategoryInSubTree")
    public RubriqueBean getFirstOfCategoryInSubTree(final String rootCode, final String category) {
        RubriqueBean result = null;
        RubriqueBean root = getRubriqueByCode(rootCode);
        if (StringUtils.isNotBlank(category)) {
            List<RubriqueBean> allForCategory = dao.selectByCategories(Collections.singletonList(category));
            int index = 0;
            while (result == null && allForCategory.size() > index) {
                final RubriqueBean currentSection = allForCategory.get(index);
                if (isParentSection(root, currentSection)) {
                    result = currentSection;
                }
                index++;
            }
        }
        return result;
    }

    /**
     * Récupérer toutes les rubriques d'une branche de l'arborescence de rubriques ayant une catégorie présente dans la liste des catgéories.
     * Remarque : la racine de la branche peut être retournée.
     *
     * @param codeRacine racine de la branche explorée pour retourner les rubriques.
     * @param categories Liste de codes catégories servant à filtrer les rubrique de l'arborescence.
     * @return La liste de rubriques de la branche ayant une catégorie présente dans la liste passée en paramétre.
     */
    public List<RubriqueBean> getRubriquesByCategorieInSubTree(final String codeRacine, final Collection<String> categories) {
        return getRubriquesByCategorieInSubTree(getRubriqueByCode(codeRacine), categories);
    }

    /**
     * Récupérer toutes les rubriques d'une branche de l'arborescence de rubriques ayant une catégorie présente dans la liste des catgéories.
     * Remarque : la racine de la branche peut être retournée.
     *
     * @param parent Racine de l'arborescence explorée.
     * @param categories Liste de codes catégories servant à filtrer les rubrique de l'arborescence.
     * @return La liste de rubriques de la branche ayant une catégorie présente dans la liste passée en paramétre.
     */
    public List<RubriqueBean> getRubriquesByCategorieInSubTree(final RubriqueBean parent, final Collection<String> categories) {
        List<RubriqueBean> result = new ArrayList<>();
        List<RubriqueBean> allCategories = dao.selectByCategories(categories);
        for (RubriqueBean currentSection : allCategories) {
            if (isParentSection(parent, currentSection)) {
                result.add(currentSection);
            }
        }
        return result;
    }

    /**
     * Détermine de facon récursive la liste des rubriques autorisées pour un utilisateur donné.
     *
     * @param allGroups contexte pour la base et les autorisations du user
     * @param rubrique the rubrique
     * @return the collection
     */
    public Collection<RubriqueBean> determinerListeSousRubriquesAutorisees(final Set<String> allGroups, final RubriqueBean rubrique) {
        final Collection<RubriqueBean> listeRubriquesAutorisees = new HashSet<>();
        determinerListeSousRubriquesAutorisees(allGroups, rubrique, listeRubriquesAutorisees);
        return listeRubriquesAutorisees;
    }

    /**
     * Détermine la liste des rubriques autorisées pour un utilisateur donné.
     *
     * @param listeGroupes the liste groupes
     * @param rubrique the rubrique
     * @param listeRecursive the liste recursive
     */
    public void determinerListeSousRubriquesAutorisees(final Set<String> listeGroupes, final RubriqueBean rubrique, final Collection<RubriqueBean> listeRecursive) {
        String codeRubrique = StringUtils.EMPTY;
        if (rubrique != null) {
            codeRubrique = rubrique.getCode();
        }
        determinerListeSousRubriquesAutorisees(listeGroupes, codeRubrique, listeRecursive);
    }

    /**
     * Détermine de facon récursive la liste des rubriques autorisées pour un utilisateur donné.
     *
     * @param allGroups contexte pour la base et les autorisations du user
     * @param codeRubrique the rubrique
     * @return the collection
     */
    public Collection<RubriqueBean> determinerListeSousRubriquesAutorisees(final Set<String> allGroups, final String codeRubrique) {
        final Collection<RubriqueBean> listeRubriquesAutorisees = new HashSet<>();
        determinerListeSousRubriquesAutorisees(allGroups, codeRubrique, listeRubriquesAutorisees);
        return listeRubriquesAutorisees;
    }

    /**
     * Détermine la liste des rubriques autorisées pour un utilisateur donné.
     *
     * @param listeGroupes the liste groupes
     * @param codeRubrique the rubrique
     * @param listeRecursive the liste recursive
     */
    public void determinerListeSousRubriquesAutorisees(final Set<String> listeGroupes, final String codeRubrique, final Collection<RubriqueBean> listeRecursive) {
        final Collection<RubriqueBean> listeSousRubriques = getRubriqueByCodeParent(StringUtils.defaultString(codeRubrique));
        // on boucle sur les rubriques
        for (final RubriqueBean rubriqueCourante : listeSousRubriques) {
            // JSS 20051013 : bug sur récursivité si pas de DSI
            boolean autorisationRubrique = true;
            // on boucle sur les groupes de restriction de la rubrique
            final Set<String> groupesDSI = Chaine.getHashSetAccolades(rubriqueCourante.getGroupesDsi());
            if (!groupesDSI.isEmpty()) {
                autorisationRubrique = !CollectionUtils.intersection(groupesDSI, listeGroupes).isEmpty();
            }
            if (autorisationRubrique) {
                listeRecursive.add(rubriqueCourante);
                if (!getRubriqueByCodeParent(rubriqueCourante.getCode()).isEmpty()) {
                    determinerListeSousRubriquesAutorisees(listeGroupes, rubriqueCourante, listeRecursive);
                }
            }
        }
    }

    /**
     * Vérifie l'accès à la rubrique fournie en paramètre.
     * Pour cela, la liste des code de groupes fourni doit contenir au moins un des groupes de la rubrique ainsi que pour chacune des rubriques parentes.
     * @param listeGroupes la liste des codes de grupes de l'utilisateurs.
     * @param codeRubriqueFiche le code rubrique dont on souhaite vérifier l'accès.
     * @return vrai ssi la liste des codes de groupes contient au moins un groupe de la rubrique fourni ainsi que pour toutes ses rubriques parentes
     * @see {@link #controlerRestrictionRubrique(Set, RubriqueBean)}
     */
    public boolean controlerRestrictionRubrique(final Set<String> listeGroupes, final String codeRubriqueFiche) {
        final RubriqueBean infoRubrique = getRubriqueByCode(codeRubriqueFiche);
        return controlerRestrictionRubrique(listeGroupes, infoRubrique);
    }

    /**
     * Vérifie l'accès à la rubrique fournie en paramètre.
     * Pour cela, la liste des code de groupes fourni doit contenir au moins un des groupes de la rubrique ainsi que pour chacune des rubriques parentes.
     * @param listeGroupes la liste des codes de grupes de l'utilisateurs.
     * @param rubriqueFiche la rubrique dont on souhaite vérifier l'accès.
     * @return vrai ssi la liste des codes de groupes contient au moins un groupe de la rubrique fourni ainsi que pour toutes ses rubriques parentes
     */
    public boolean controlerRestrictionRubrique(final Set<String> listeGroupes, final RubriqueBean rubriqueFiche) {
        boolean accesOK = true;
        if (rubriqueFiche != null && !CODE_RUBRIQUE_ROOT.equals(rubriqueFiche.getCode())) {
            boolean boucleSurLaRubriqueParente = true;
            // on initialise le test avec la rubrique de la fiche
            RubriqueBean rubriqueCourante = rubriqueFiche;
            while (boucleSurLaRubriqueParente) {
                boolean droitRubriqueCourante = false;
                if (rubriqueCourante != null) {
                    // si la rubrique a des groupes de restriction
                    final Set<String> groupesDSI = Chaine.getHashSetAccolades(rubriqueCourante.getGroupesDsi());
                    if (groupesDSI != null && !groupesDSI.isEmpty()) {
                        for (final String codeGroupe : groupesDSI) {
                            if (listeGroupes.contains(codeGroupe)) {
                                droitRubriqueCourante = true;
                                break;
                            }
                        }
                    } else {
                        droitRubriqueCourante = true;
                    }
                    // si droit sur la rubrique courante on boucle sur la rubrique parente si elle existe
                    if (droitRubriqueCourante) {
                        if (StringUtils.isNotEmpty(rubriqueCourante.getCodeRubriqueMere())) {
                            rubriqueCourante = getRubriqueByCode(rubriqueCourante.getCodeRubriqueMere());
                        } else {
                            boucleSurLaRubriqueParente = false;
                        }
                    } else {
                        // sinon on sort de la boucle et on renvoit false
                        boucleSurLaRubriqueParente = false;
                        accesOK = false;
                    }
                } else {
                    boucleSurLaRubriqueParente = false;
                    accesOK = true;
                }
            }
        }
        return accesOK;
    }

    /**
     * Calcule le niveau dans l'arbre d'une rubrique
     *
     * @param section la rubrique dont on souhaite connaitre le niveau
     * @return le niveau courant de la rubrique.
     */
    public int getLevel(final RubriqueBean section) {
        int level = 1;
        if (section == null || StringUtils.isBlank(section.getCode()) || CODE_RUBRIQUE_ROOT.equals(section.getCode())) {
            level = 0;
        } else if (StringUtils.isNotBlank(section.getCodeRubriqueMere())) {
            final RubriqueBean parentSection = getRubriqueByCode(section.getCodeRubriqueMere());
            level += getLevel(parentSection);
        }
        return level;
    }

    /**
     * Méthode présente pour la rétrocompatibilité d'InfosRubrique.
     *
     * @param parentSectionCode le code de la rubrique parente de la rubrique dont on souhaite connaitre le niveau.
     * @return le niveau courant de la rubrique.
     */
    public int getLevelFromParentCode(final String parentSectionCode) {
        int level = 1;
        if (StringUtils.isNotBlank(parentSectionCode) && !CODE_RUBRIQUE_ROOT.equals(parentSectionCode)) {
            final RubriqueBean parentGroup = getRubriqueByCode(parentSectionCode);
            level += getLevelFromParentCode(parentGroup.getCodeRubriqueMere());
        }
        return level;
    }

    /**
     * Vérifie si la rubrique parent contient la rubrique child.
     * @param parent la rubrique parente...
     * @param child la rubrique dont on souhaite vérifier si elle est contenu dans les enfants de la rubrique parente
     * @return vrai si parent contient child ou que c'est les mêmes rubriques (c'est historique), si une des deux rubrique est null, retourne faux.
     */
    public boolean isParentSection(final RubriqueBean parent, final RubriqueBean child) {
        boolean result = Boolean.FALSE;
        if (parent != null && child != null && parent.getCode() != null && child.getCode() != null) {
            if (parent.getCode().equals(child.getCode())) {
                result = Boolean.TRUE;
            } else {
                result = isParentSection(parent, getRubriqueByCode(child.getCodeRubriqueMere()));
            }
        }
        return result;
    }

    /**
     *
     * Cette méthode renvoit une rubrique de la fiche dans le site courant.
     * par defaut la rubrique principale ou la première de ces rubriques de publication
     * si précisé, on cherchera en priorité la rubrique incluse dans la rubrique courante sinon on renverra celle par défaut
     * @param ctx
     *            the ctx
     * @param rubriqueCourante
     *            the infos rubrique courante
     * @param lstCodeRubriquePubliable
     *            the lst code rubrique publiable
     * @param inRubriqueCourante
     *            the _in rubrique courante
     *
     * @return the rubrique publication
     *
     */
    public String getRubriquePublication(final OMContext ctx, final RubriqueBean rubriqueCourante, final List<String> lstCodeRubriquePubliable, final boolean inRubriqueCourante) {
        String codeRubrique = StringUtils.EMPTY;
        if (rubriqueCourante != null) {
            codeRubrique = rubriqueCourante.getCode();
        }
        return getRubriquePublication(ctx, codeRubrique, lstCodeRubriquePubliable, inRubriqueCourante);
    }


    /**
     * Cette méthode renvoit une rubrique de la fiche dans le site courant.
     * par defaut la rubrique principale ou la première de ces rubriques de publication
     * si précisé, on cherchera en priorité la rubrique incluse dans la rubrique courante sinon on renverra celle par défaut
     * @param ctx
     *            the ctx
     * @param rubriqueCourante
     *            the infos rubrique courante
     * @param lstCodeRubriquePubliable
     *            the lst code rubrique publiable
     * @param inRubriqueCourante
     *            the _in rubrique courante
     *
     * @return the rubrique publication
     *
     */
    public String getRubriquePublication(final OMContext ctx, final String rubriqueCourante, final List<String> lstCodeRubriquePubliable, final boolean inRubriqueCourante) {
        final InfosSite siteCourant = ctx.getInfosSite();
        boolean controlDsi = false;
        boolean rubriqueAutorisee = false;
        boolean possedeRubrique = false;
        if (ctx instanceof ContexteUniv) {
            controlDsi = true;
        }
        String codeRubrique = null;
        for (final String aLstCodeRubriquePubliable : lstCodeRubriquePubliable) {
            possedeRubrique = true;
            final RubriqueBean rubrique = getRubriqueByCode(aLstCodeRubriquePubliable);
            // si la rubrique n'existe pas/plus on passe
            if (rubrique == null) {
                continue;
            }
            if (!controlDsi || controlerRestrictionRubrique(((ContexteUniv) ctx).getGroupesDsiAvecAscendants(), rubrique.getCode())) {
                rubriqueAutorisee = true;
                if (siteCourant.isRubriqueVisibleInSite(rubrique)) {
                    if (!inRubriqueCourante) {
                        return rubrique.getCode();
                    }
                    if (codeRubrique == null) {
                        codeRubrique = rubrique.getCode();
                    }
                    if (rubrique.getCode().equals(rubriqueCourante) || isParentSection(getRubriqueByCode(rubriqueCourante), rubrique)) {
                        return rubrique.getCode();
                    }
                }
            }
        }
        if (controlDsi && possedeRubrique && !rubriqueAutorisee) {
            return ServiceRubrique.CODE_RUBRIQUE_INEXISTANTE;
        }
        return codeRubrique;
    }

}
