package com.univ.objetspartages.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import com.kosmos.service.impl.AbstractServiceBean;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.dao.impl.GroupeDsiDAO;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.InfosRequeteGroupe;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.util.InfosRolesUtils;

/**
 * Service permettant de gérer les Groupes de l'application.
 */
public class ServiceGroupeDsi extends AbstractServiceBean<GroupeDsiBean, GroupeDsiDAO> {

    private static final String DEFAULT_BREADCRUMBS_SEPARATOR = " &gt; ";

    /** Le code du groupe racine de l'application. */
    public static final String CODE_GROUPE_ROOT = "00";

    /**
     * Retourne l'ensemble des groupes présents dans l'application.
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} ou une liste vide si aucun groupe n'est présent.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getAll() {
        return dao.selectAll();
    }

    /**
     * Retourne l'ensemble des groupes dynamiques présents au sein de l'application.
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si aucun groupe dynamique n'est présent.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getAllDynamics() {
        return dao.selectAllDynamics();
    }

    /**
     * Retourne l'ensemble des groupes dont le {@link com.univ.objetspartages.bean.GroupeDsiBean#libelle} contient le paramètre fournit.
     *
     * @param label : la {@link String} que le {@link com.univ.objetspartages.bean.GroupeDsiBean#libelle} du groupe doit contenir pour être élligible.
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si un ou plusieurs groupes sont élligibles, une liste vide sinon.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByLabel(final String label) {
        return dao.selectByLabel(label);
    }

    /**
     * Retourne l'ensemble des groupes correspondant à la requête Ldap fournie en paramètre.
     *
     * @param ldapRequest : la requête sous forme de {@link String}
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si un ou plusieurs groupes sont élligibles, une liste vide sinon.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByLdapRequest(final String ldapRequest) {
        return dao.selectByLdapRequest(ldapRequest);
    }

    /**
     * Retourne l'ensemble des groupes correspondant à la requête groupe fournie en paramètre.
     *
     * @param groupRequest : la requête sous forme de {@link String}
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si un ou plusieurs groupes sont élligibles, une liste vide sinon.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByGroupRequest(final String groupRequest) {
        return dao.selectByGroupRequest(groupRequest);
    }

    /**
     * Retourne l'ensemble des groupes issus de la source passée en paramètre.
     *
     * @param importSource : la source sous forme de {@link String}
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si un ou plusieurs groupes sont élligibles, une liste vide sinon.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByImportSource(final String importSource) {
        return dao.selectByImportSource(importSource);
    }

    /**
     * Retourne l'ensemble des groupes issus de la source passée en paramètre et dont le parent possède le code passé en paramètre.
     *
     * @param importSource : la source sous forme de {@link String}
     * @param parentGroup : le code du groupe parent sous forme de {@link String}
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si un ou plusieurs groupes sont élligibles, une liste vide sinon.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByImportSourceAndParentGroup(final String importSource, final String parentGroup) {
        return dao.selectByImportSourceAndParentGroup(importSource, parentGroup);
    }

    /**
     * Retourne l'ensemble des groupes répondants au critères fournis.
     *
     * @param code : le code de groupe sous forme de {@link String}
     * @param type : le type de groupe sous forme de {@link String}
     * @param label : le libellé du groupe sous forme de {@link String}
     * @param codeStructure : le code de rattachement sous forme de {@link String}
     * @param gestionCache : honnêtement, aucune idée...
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si un ou plusieurs groupes sont élligibles, une liste vide sinon.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByCodeTypeLabelStructureAndCache(final String code, final String type, final String label, final String codeStructure, final String gestionCache) {
        return dao.getByCodeTypeLabelStructureAndCache(code, type, label, codeStructure, gestionCache);
    }

    /**
     * Retourne l'ensemble des groupes répondants au critères fournis.
     *
     * @param code : le code de groupe sous forme de {@link String}
     * @param type : le type de groupe sous forme de {@link String}
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si un ou plusieurs groupes sont élligibles, une liste vide sinon.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByCodeAndType(final String code, final String type) {
        return dao.getByCodeAndType(code, type);
    }

    /**
     * Retourne l'ensemble des groupes répondants au critères fournis.
     *
     * @param code : le code de groupe sous forme de {@link String}
     * @param gestionCache : toujours, aucune idée...
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si un ou plusieurs groupes sont élligibles, une liste vide sinon.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByCodeAndCache(final String code, final String gestionCache) {
        return dao.getByCodeAndCache(code, gestionCache);
    }

    /**
     * Récupérer la liste des groupes rattachés à un groupe.
     *
     * @param parentGroupCode code du groupe parent.
     *
     * @return la liste des groupes.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    @Cacheable(value = "ServiceGroupeDsi.getByParentGroup")
    public List<GroupeDsiBean> getByParentGroup(final String parentGroupCode) {
        return dao.selectByParentGroup(StringUtils.defaultString(parentGroupCode));
    }

    /**
     * Retourne les groupes correspondant au code et à la source d'import fournis.
     *
     * @param code le code du groupe à récupérer sous la forme d'une {@link String}.
     * @param importSource la source d'import sous la forme d'une {@link String}.
     *
     * @return la {@link java.util.List} des {@link com.univ.objetspartages.bean.GroupeDsiBean} correspondant aux critères ou une list vide sinon.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByCodeAndImportSource(final String code, final String importSource) {
        return dao.selectByCodeAndImportSource(code, importSource);
    }

    /**
     * Récupérer la liste des groupes rattachés à une structure.
     *
     * @param codeStructure code la structure.
     *
     * @return La liste des groupes rattachés à la structure.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByStructure(final String codeStructure) {
        return dao.getByStructure(codeStructure);
    }

    /**
     * Récupérer la liste des groupes rattachés à une structure et d'une source d'import spécifique.
     *
     * @param codeStructure code la structure.
     * @param importSource source d'import.
     *
     * @return La liste des groupes rattachés à la structure.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     */
    public List<GroupeDsiBean> getByStructureAndImportSource(final String codeStructure, final String importSource) {
        return dao.getByStructureAndImportSource(codeStructure, importSource);
    }

    /**
     * Permet de persister le {@link com.univ.objetspartages.bean.GroupeDsiBean} passé en paramètre. Si le bean ne possède pas d'id ou que ce dernier est égal à "0", la méthode
     * "add" du dao est appelé, sinon c'est la méthode "update".
     *
     * Cette méthode est responsable de l'éviction des caches suivants (lors d'une mise à jour) :
     * <ul>
     *     <li>ServiceGroupeDsi.getByCode</li>
     *     <li>ServiceGroupeDsi.getByType</li>
     *     <li>ServiceGroupeDsi.getLevel</li>
     *     <li>ServiceGroupeDsi.getAllSubGroups</li>
     * </ul>
     *
     * @param bean le {@link com.univ.objetspartages.bean.GroupeDsiBean} à persister.
     *
     * @see com.univ.objetspartages.bean.GroupeDsiBean
     * @see <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html#cache-annotations-evict">@CacheEvict</a>
     * @see <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html#cache-annotations-caching">@Caching</a>
     */
    @Override
    @Caching(
        evict = {
            @CacheEvict(value = "ServiceGroupeDsi.getByCode", key="#bean.getCode()"),
            @CacheEvict(value = "ServiceGroupeDsi.getByType", key="#bean.getType()"),
            @CacheEvict(value = "ServiceGroupeDsi.getLevel", key="#bean.getCode()", allEntries = true),
            @CacheEvict(value = "ServiceGroupeDsi.getAllSubGroups", key="#bean.getCode()", allEntries = true),
            @CacheEvict(value = "ServiceGroupeDsi.getByParentGroup", key = "#bean.getCodeGroupePere()")
        }
    )
    public void save(final GroupeDsiBean bean) {
        super.save(bean);
    }

    /**
     * Permet de supprimer le {@link com.univ.objetspartages.bean.GroupeDsiBean} correspondant à l'id passé en paramètre.     *
     * Cette méthode est responsable de l'éviction des caches suivants :
     * <ul>
     *     <li>ServiceGroupeDsi.getByCode</li>
     *     <li>ServiceGroupeDsi.getByType</li>
     *     <li>ServiceGroupeDsi.getLevel</li>
     *     <li>ServiceGroupeDsi.getAllSubGroups</li>
     * </ul>
     *
     * @param id le {@link Long} correspondant au groupe à supprimer.
     *
     * @see <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html#cache-annotations-evict">@CacheEvict</a>
     * @see <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html#cache-annotations-caching">@Caching</a>
     */
    @Override
    @Caching(
        evict = {
            @CacheEvict(value = "ServiceGroupeDsi.getByCode", key="#root.target.getById(#id).getCode()", beforeInvocation = true),
            @CacheEvict(value = "ServiceGroupeDsi.getByType", key="#root.target.getById(#id).getType()", beforeInvocation = true),
            @CacheEvict(value = "ServiceGroupeDsi.getByParentGroup", key = "#root.target.getById(#id).getCodeGroupePere()", beforeInvocation = true),
            @CacheEvict(value = "ServiceGroupeDsi.getLevel", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceGroupeDsi.getAllSubGroups", allEntries = true, beforeInvocation = true)
        }
    )
    public void delete(final Long id) {
        super.delete(id);
    }

    /**
     * Va chercher en bdd le groupe ayant pour code la valeur fournie en parametre.
     * Cette méthode est responsable de la population du cache "ServiceGroupeDsi.getByCode".
     *
     * @param code le code du groupe à récupérer.
     *
     * @return le groupe ayant pour code, la valeur fourni en paramètre ou null si non trouvé.
     *
     * @see <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html#cache-annotations-cacheable">@Cacheable</a>
     */
    @Cacheable(value = "ServiceGroupeDsi.getByCode")
    public GroupeDsiBean getByCode(final String code) {
        if(StringUtils.isNotBlank(code)) {
            return dao.selectByCode(code);
        }
        return null;
    }

    /**
     * Retourne le niveau dans l'arborescence du groupe passé en paramètre. Le calcul s'effectue jusqu'au noeud "root", le niveau du noeud "root" étant 0.
     * Cette méthode est responsable de la population du cache "ServiceGroupeDsi.getLevel".
     *
     * @param group : le {@link com.univ.objetspartages.bean.GroupeDsiBean} pour lequel on souhaite calculer le niveau.
     *
     * @return un entier correspondant au niveau du groupe.
     *
     * @see <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html#cache-annotations-cacheable">@Cacheable</a>
     */
    @Cacheable(value = "ServiceGroupeDsi.getLevel", condition = "#group != null" ,key="#group.getCode()")
    public int getLevel(final GroupeDsiBean group) {
        if(group == null) {
            return 0;
        }
        int level = 1;
        if(StringUtils.isNotBlank(group.getCodeGroupePere())) {
            final GroupeDsiBean parentGroup = getByCode(group.getCodeGroupePere());
            level += getLevel(parentGroup);
        }
        return level;
    }

    /**
     * Calcule l'ensemble des parents du groupe fourni en paramètre.
     * @param codeGroupe le groupe dont on souhaite connaitre les parents.
     * @return l'ensemble des parents ou une collection vide si rien n'est trouvé.
     */
    public List<GroupeDsiBean> getAllAscendant(final String codeGroupe) {
        final GroupeDsiBean currentSection = getByCode(codeGroupe);
        return getAllAscendant(currentSection);
    }

    /**
     * Même comportement que la méthode {@link #getAllAscendant(String)} mais en manipulant directement un groupe.
     * Elle permet d'éviter une requête en base.
     * @param GroupeDsiBean mon groupe dont on souhaite connaitre les parents
     * @return l'ensemble des parents ou une collection vide si rien n'est trouvé.
     */
    public List<GroupeDsiBean> getAllAscendant(final GroupeDsiBean groupeBean) {
        final List<GroupeDsiBean> result = new ArrayList<>();
        GroupeDsiBean parent = null;
        if (groupeBean != null && StringUtils.isNotBlank(groupeBean.getCodeGroupePere())) {
            parent = getByCode(groupeBean.getCodeGroupePere());
        }
        if (parent != null) {
            result.add(parent);
            while (parent != null && StringUtils.isNotBlank(parent.getCodeGroupePere())) {
                parent = getByCode(parent.getCodeGroupePere());
                if (parent != null) {
                    result.add(parent);
                }
            }
        }
        return result;
    }
    
    /**
     * Retourne une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} correspondant aux enfants immédiat du noeud possédant le code "code".
     * Cette méthode est responsable de la population du cache "ServiceGroupeDsi.getAllSubGroups".
     *
     * @param code : une {@link String} représentant le noeud pour lequel on souhaite obtenir les enfants.
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} si le noeud existe et possède des enfants, une {@link java.util.List} vide sinon.
     *
     * @see <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html#cache-annotations-cacheable">@Cacheable</a>
     */
    @Cacheable(value = "ServiceGroupeDsi.getAllSubGroups")
    public List<GroupeDsiBean> getAllSubGroups(final String code) {
        final List<GroupeDsiBean> subGroups = new ArrayList<>();
        final List<GroupeDsiBean> currentSubGroup = getByParentGroup(code);
        subGroups.addAll(currentSubGroup);
        for(final GroupeDsiBean currentGroup : currentSubGroup) {
            subGroups.addAll(getAllSubGroups(currentGroup.getCode()));
        }
        return subGroups;
    }

    /**
     * Retourne une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean} filtrer selon le type passé en paramètre.
     * Cette méthode est responsable de la population du cache "ServiceGroupeDsi.getByType".
     *
     * @param type : une {@link String} représentant le type de groupe à récupérer.
     *
     * @return une {@link java.util.List} de {@link com.univ.objetspartages.bean.GroupeDsiBean}
     *
     * @see <a href="https://docs.spring.io/spring/docs/current/spring-framework-reference/html/cache.html#cache-annotations-cacheable">@Cacheable</a>
     */
    @Cacheable(value = "ServiceGroupeDsi.getByType")
    public List<GroupeDsiBean> getByType(final String type) {
        final List<GroupeDsiBean> res = new ArrayList<>();
        for (final GroupeDsiBean group : getAll()) {
            if (group.getType().equalsIgnoreCase(type)) {
                res.add(group);
            }
        }
        return res;
    }

    /**
     * Permet d'identifier si le {@link com.univ.objetspartages.bean.GroupeDsiBean} "parentGroup" contient le {@link com.univ.objetspartages.bean.GroupeDsiBean} "childGroup"  (tous niveaux confondus).
     *
     * @param parentGroup : le {@link com.univ.objetspartages.bean.GroupeDsiBean} dans lequel rechercher.
     * @param childGroup : le {@link com.univ.objetspartages.bean.GroupeDsiBean} que l'on cherche à retrouver dans les enfants.
     *
     * @return true si le groupe enfant appartient bien au parent, false sinon. Cette méthode renvoit également "true" si le groupe parent est égal au groupe enfant.
     */
    public boolean contains(final GroupeDsiBean parentGroup, final GroupeDsiBean childGroup) {
        if(parentGroup != null && childGroup != null) {
            if(StringUtils.isNotBlank(parentGroup.getCode()) && parentGroup.getCode().equals(childGroup.getCode())) {
                return true;
            }
            for (final GroupeDsiBean currentChild : getAllSubGroups(parentGroup.getCode())) {
                if (childGroup.getCode().equals(currentChild.getCode())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Calcul l'ensemble des requêtes de groupes récupérées depuis les properties et les retourne sous la forme d'une {@link java.util.Map}.
     *
     * @return la {@link java.util.Map} de {@link String} d'{@link com.univ.objetspartages.om.InfosRequeteGroupe}
     */
    public Map<String, InfosRequeteGroupe> getGroupRequestList() {
        final Map<String, InfosRequeteGroupe> listeRequetesGroupes = new HashMap<>();
        for (final IExtension extension : ExtensionHelper.getExtensionManager().getExtensions().values()) {
            final Properties propertiesExtensions = extension.getProperties();
            for (final String nomProp : propertiesExtensions.stringPropertyNames()) {
                if (StringUtils.startsWith(nomProp, "requete_groupe.")) {
                    final String[] items = nomProp.split("\\.", -2);
                    if ("classe".equals(items[2])) {
                        final String aliasRequete = items[1];
                        final String classe = propertiesExtensions.getProperty(nomProp);
                        final String intitule = propertiesExtensions.getProperty("requete_groupe." + aliasRequete + ".intitule");
                        String templateJSP = propertiesExtensions.getProperty("requete_groupe." + aliasRequete + ".template_jsp");
                        if (StringUtils.isNotBlank(templateJSP)) {
                            templateJSP = extension.getRelativePath() + templateJSP;
                        }
                        final String tsCacheGroupesUtilisateur = propertiesExtensions.getProperty("requete_groupe." + aliasRequete + ".expiration_cache_utilisateur");
                        if (tsCacheGroupesUtilisateur != null) {
                            listeRequetesGroupes.put(aliasRequete, new InfosRequeteGroupe(aliasRequete, intitule, classe, Long.valueOf(tsCacheGroupesUtilisateur), templateJSP));
                        } else {
                            listeRequetesGroupes.put(aliasRequete, new InfosRequeteGroupe(aliasRequete, intitule, classe, 0, templateJSP));
                        }
                    }
                }
            }
        }
        return listeRequetesGroupes;
    }

    /**
     * Méthode permettant de récupérer le fil d'ariane pour le {@link com.univ.objetspartages.bean.GroupeDsiBean} correspondant au code passé en paramètre. Par défaut,
     * si le paramètre "separator" est vide ou null, le séparateur utilisé est {@link ServiceGroupeDsi#DEFAULT_BREADCRUMBS_SEPARATOR}.
     *
     * @param code : la {@link String} correspondant au {@link com.univ.objetspartages.bean.GroupeDsiBean} pour lequel on  souhaite calculer le fil d'ariane.
     * @param separator : une {@link String} correspondant au séparateur à appliquer entre les éléments du fil.
     *
     * @return une {@link String} représentant le fil d'ariane formaté à partir des {@link com.univ.objetspartages.bean.GroupeDsiBean#getLibelle()} et du "separator" ou null si le code passé en paramètre ne correspond à aucun {@link com.univ.objetspartages.bean.GroupeDsiBean}.
     */
    public String getBreadCrumbs(final String code, final String separator) {
        final String actualSeparator = StringUtils.defaultIfBlank(separator, DEFAULT_BREADCRUMBS_SEPARATOR);
        return StringUtils.join(getBreadCrumbsAsList(code), actualSeparator);
    }

    /**
     * Méthode permettant de récupérer le fil d'ariane sous la forme d'une {@link java.util.List} ordonnée (parent > enfant > sous-enfant).
     *
     * @param code une {@link String} représentant le code du {@link com.univ.objetspartages.bean.GroupeDsiBean} pour lequel on souhaite calculer le fil d'ariane.
     * @return une {@link java.util.List} regroupant l'ensemble des intitulés des {@link com.univ.objetspartages.bean.GroupeDsiBean} récupérés à partir du code ou une liste vide si rien n'a été trouvé.
     */
    public List<String> getBreadCrumbsAsList(final String code) {
        final List<String> breadCrumbs = new ArrayList<>();
        if(StringUtils.isNotBlank(code)) {
            final GroupeDsiBean group = getByCode(code);
            if (StringUtils.isNotBlank(group.getCodeGroupePere())) {
                breadCrumbs.addAll(getBreadCrumbsAsList(group.getCodeGroupePere()));
            }
            breadCrumbs.add(group.getLibelle());
        }
        return breadCrumbs;
    }

    /**
     * Récupération de l'intitulé : gestion de plusieurs codes.(pour retro compat...)
     * @param codes une liste de code séparé par des ";" (malin n'est ce pas?)
     * @return l'intitule si il y en a plusieurs ils sont séparés pa rdes ";" (toujours malin n'est ce pas?)
     */
    public String getIntitules(final String codes) {
        if (StringUtils.isEmpty(codes)) {
            return StringUtils.EMPTY;
        }
        final List<String> res = new ArrayList<>();
        for (final String code : StringUtils.split(codes, ";")) {
            res.add(getIntitule(code));
        }
        return StringUtils.join(res, ";");
    }

    /**
     * Récupération de l'intitulé : gestion d'un seule code...
     * @param code le code du groupe dont on cherche l'intitule
     * @return l'intitule du groupe si il est trouvé, sinon BO_GROUPE_INEXISTANT. Enfin si le code est vide ou null, on retourne une chaine vide.
     */
    public String getIntitule(final String code) {
        String res = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(code)) {
            final GroupeDsiBean group = getByCode(code);
            if (group != null && StringUtils.isNotBlank(group.getLibelle())) {
                res = group.getLibelle();
            } else {
                res = MessageHelper.getCoreMessage("BO_GROUPE_INEXISTANT");
            }
        }
        return res;
    }

    /**
     * Récupération pour affichage liste des requêtes.
     *
     * @return une Map contenant l'alias et l'intitulé du groupe contenu dans le cache
     */
    public Map<String, String> getListeRequetesGroupesPourAffichage() {
        final Hashtable<String, String> res = new Hashtable<>();
        for (final InfosRequeteGroupe info : getGroupRequestList().values()) {
            res.put(info.getAlias(), info.getIntitule());
        }
        return res;
    }


    public Map<String, List<Perimetre>> renvoyerGroupesEtPerimetres(final String role, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif) {
        final Hashtable<String, List<Perimetre>> h = new Hashtable<>();
        for (final GroupeDsiBean info : getAll()) {
            final List<Perimetre> perimetres = InfosRolesUtils.renvoyerPerimetresAffectation(info.getRoles(), role, codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif);
            if (CollectionUtils.isNotEmpty(perimetres)) {
                h.put(info.getCode(), perimetres);
            }
        }
        return h;
    }

    public boolean controlerPermission(final AutorisationBean autorisations, final String sPermission, final String codeGroupe) {
        final PermissionBean permission = new PermissionBean(sPermission);
        final Perimetre perimetre = new Perimetre("", "", "", codeGroupe, "");
        return autorisations.possedePermissionPartielleSurPerimetre(permission, perimetre);
    }
}
