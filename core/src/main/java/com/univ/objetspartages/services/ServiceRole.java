package com.univ.objetspartages.services;

import java.util.List;

import com.kosmos.service.impl.AbstractServiceBean;
import com.univ.objetspartages.bean.RoleBean;
import com.univ.objetspartages.dao.impl.RoleDAO;

/**
 * Service permettant de gérer les roles des utilisateurs.
 */
public class ServiceRole extends AbstractServiceBean<RoleBean, RoleDAO> {

    public RoleBean getByCode(final String code) {
        return dao.getByCode(code);
    }

    /**
     * Renvoie la liste de tous les roles Peut être affiché directement dans une Combo.
     *
     * @return the liste id roles
     *
     */
    public List<RoleBean> getAllWithoutCollab() {
        return dao.getAllWithoutCollab();
    }

    /**
     * Renvoie la liste de tous les roles.
     *
     * @return la listes de tout les roles de l'application ou une liste vide si rien n'est trovué
     *
     */
    public List<RoleBean> getAll() {
        return dao.getAll();
    }

    public List<RoleBean> getByPermission(final String permission) {
        return dao.getByPermission(permission);
    }

}
