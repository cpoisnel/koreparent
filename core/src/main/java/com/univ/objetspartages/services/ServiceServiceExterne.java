package com.univ.objetspartages.services;

import java.util.List;

import com.kosmos.service.impl.AbstractServiceBean;
import com.univ.objetspartages.dao.impl.ServiceDAO;
import com.univ.objetspartages.om.ServiceBean;

/**
 * Created by olivier.camon on 06/05/15.
 */
public class ServiceServiceExterne extends AbstractServiceBean<ServiceBean, ServiceDAO> {

    /**
     * Retrieve a {@link ServiceBean} given his code.
     * @param code : the code to look for as a {@link String}.
     * @return the {@link ServiceBean} if found, null otherwise.
     */
    public ServiceBean getByCode(final String code) {
        return dao.selectByCode(code);
    }

    /**
     * Retrieves all persisted users in the datasource.
     * @return a {@link java.util.List} of {@link ServiceBean}, an empty list if there is nothing persited
     */
    public List<ServiceBean> getAll() {
        return dao.selectAll();
    }

}
