package com.univ.objetspartages.services;

import java.util.List;

import com.kosmos.service.impl.AbstractServiceBean;
import com.univ.objetspartages.bean.SiteExterneBean;
import com.univ.objetspartages.dao.impl.SiteExterneDAO;

/**
 * Service permettant de gerer les {@link SiteExterneBean}
 */
public class ServiceSiteExterne extends AbstractServiceBean<SiteExterneBean, SiteExterneDAO> {

    /**
     * Récupère l'ensemble des sites enregistrer en base de données
     * @return l'ensemble des sites ou une liste vide si rien n'est trouvé.
     */
    public List<SiteExterneBean> getAllExernalWebSite() {
        return dao.getAllSite();
    }

}
