package com.univ.objetspartages.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.AbstractServiceBean;
import com.univ.multisites.InfosFicheComparator;
import com.univ.multisites.InfosFicheReferencee;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.dao.impl.RubriquepublicationDAO;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.ContexteDao;

/**
 * Service permettant de gérer les rubriques de publications (aka le multi rubriquage d'une fiche)
 */
public class ServiceRubriquePublication extends AbstractServiceBean<RubriquepublicationBean, RubriquepublicationDAO> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceRubriquePublication.class);

    private static final String REQUEST_SEPARATOR = "/";

    private static final String CODE_RUBRIQUE_AUTO = "#AUTO#";

    private ServiceRubrique serviceRubrique;

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    /**
     * Supprime l'ensemble des rubrique de publication dont l'id est fourni dans la liste en paramètre.
     * @param ids les IDs des données à supprimer
     */
    public void deleteByIds(final Collection<Long> ids) {
        dao.deleteByIds(ids);
    }

    /**
     * Racourci pour la méthode {@link ServiceRubriquePublication#deleteByIds(Collection)} permettant de passer directement les rubriques de publications
     * @param rubriquepublicationBeans l'ensemble des rubriques à supprimer
     */
    public void deleteByRubPub(final Collection<RubriquepublicationBean> rubriquepublicationBeans) {
        final Collection<Long> ids = new ArrayList<>();
        if (rubriquepublicationBeans != null) {
            for (final RubriquepublicationBean rubriquepublicationBean : rubriquepublicationBeans) {
                ids.add(rubriquepublicationBean.getIdRubriquepublication());
            }
        }
        dao.deleteByIds(ids);
    }

    /**
     * récupère l'ensemble des rubriques de publication dont le type, le code et la langue fourni en paramètre correspondent.
     * Cette méthode est principalement utiliser pour récupérer les rubriques de publications d'une fiche lorsque l'on a pas l'objet FicheUniv instancié.
     * @param type le type de la fiche d'origine
     * @param code le code de la fiche d'origine
     * @param language la langue de la fiche d'origine
     * @return l'ensemble des rubriques  de publications de la fiche ou une liste vide si rien n'est trouvé
     */
    public List<RubriquepublicationBean> getByTypeCodeLanguage(final String type, final String code, final String language) {
        return dao.getByTypeCodeLanguage(type, code, language);
    }

    /**
     * Récupérer les rubriques publications par type d'objet, code de rubrique et langue.
     *
     * @param type Type d'objet.
     * @param rubriqueDest
     * @param language
     * @return Liste des rubriques publications.
     */
    public List<RubriquepublicationBean> getByTypeRubriqueDestLangue(final String type, final String rubriqueDest, final String language) {
        return dao.getByTypeRubriqueDestLangue(type, rubriqueDest, language);
    }

    /**
     * récupère l'ensemble des rubriques de publications pour le type de fiche fourni en paramètre
     * @param type le type de fiche dont on souhaite récupérer les rubriques de publication.
     * @return La liste des rub pub correspondantes ou une liste vide si rien n'est trouvé
     */
    public List<RubriquepublicationBean> getByType(final String type) {
        return dao.getByType(type);
    }

    /**
     * récupère l'ensemble des rubriques de publications pour la rubrique fourni en paramètre uniquement si elle n'a pas de source de requête (aka pas une rubrique auto)
     * @param rubriqueDest la rubrique dont on souhaite avoir les rubriques de publications
     * @return La liste des rub pub correspondantes ou une liste vide si rien n'est trouvé
     */
    public List<RubriquepublicationBean> getByRubriqueDestWithoutSource(final String rubriqueDest) {
        return dao.getByRubriqueDestAndSource(rubriqueDest, StringUtils.EMPTY);
    }

    /**
     * récupère l'ensemble des rubriques de publications pour la rubrique fourni en paramètre et ayant une source de requête (aka rubrique auto)
     * @param rubriqueDest la rubrique dont on souhaite avoir les rubriques de publications
     * @return La liste des rub pub correspondantes ou une liste vide si rien n'est trouvé
     */
    public List<RubriquepublicationBean> getByRubriqueDestAndSource(final String rubriqueDest, final String source) {
        return dao.getByRubriqueDestAndSource(rubriqueDest, source);
    }

    /**
     * Récupère les rubriques de publications pour la fiche fourni en paramètre
     * @param ficheUniv la fiche dont on souhaite récupérer les données. Elle peut être null
     * @return Les résultats correspondant ou une liste vide si rien n'est trouvé
     */
    public List<RubriquepublicationBean> getByFicheUniv(final FicheUniv ficheUniv) {
        List<RubriquepublicationBean> results = new ArrayList<>();
        if (ficheUniv != null) {
            results = getByTypeCodeLanguage(ReferentielObjets.getCodeObjet(ficheUniv), ficheUniv.getCode(), ficheUniv.getLangue());
        }
        return results;
    }

    /**
     * Récupère la liste des codes des rubriqe liées à la fiche fourni en paramètre
     * @param ficheUniv la fiche dont on souhaite avoir les codes des rubriques liés
     * @return Les résultats correspondant ou une liste vide si rien n'est trouvé
     */
    public Collection<String> getRubriqueDestByFicheUniv(final FicheUniv ficheUniv) {
        final List<RubriquepublicationBean> allRubPub = getByFicheUniv(ficheUniv);
        final Collection<String> allRubriqueDest = new HashSet<>();
        for (final RubriquepublicationBean rubriquepublicationBean : allRubPub) {
            allRubriqueDest.add(rubriquepublicationBean.getRubriqueDest());
        }
        return allRubriqueDest;
    }

    /**
     * Récupère la liste des codes des rubriqe liées à la fiche fourni en paramètre.
     * A la différence de {@link ServiceRubriquePublication#getRubriqueDestByFicheUniv(FicheUniv)}, si la source de la requête est renseigné (aka requete automatique)
     * on rajoute ces codes en les préfixant de #AUTO# (va savoir pourquoi c'est fait que là...)
     * @param ficheUniv la fiche dont on souhaite avoir les codes des rubriques liés
     * @return Les résultats correspondant ou une liste vide si rien n'est trouvé
     */
    public Collection<String> getRubriqueDestByFicheWithSourceControl(final FicheUniv ficheUniv) {
        final List<RubriquepublicationBean> allRubPub = getByFicheUniv(ficheUniv);
        final Collection<String> allRubriqueDest = new HashSet<>();
        for (final RubriquepublicationBean rubriquepublicationBean : allRubPub) {
            String code = rubriquepublicationBean.getRubriqueDest();
            if (StringUtils.isNotBlank(rubriquepublicationBean.getSourceRequete())) {
                code = CODE_RUBRIQUE_AUTO + code;
            }
            allRubriqueDest.add(code);
        }
        return allRubriqueDest;
    }

    /**
     * Récupère la liste des codes des rubriqe liées à la fiche et à la rubrique (ainsi que ses descendantes) fourni en paramètre
     *
     * @param ficheUniv la fiche dont on souhaite avoir les codes des rubriques liés
     * @param rubriqueCode le code de la rubrique
     * @return Les résultats correspondant ou une liste vide si rien n'est trouvé
     */
    public Collection<String> getRubriqueDestByFicheUnivAndRubrique(final FicheUniv ficheUniv, final String rubriqueCode) {
        final Collection<String> allRubriqueDest = new HashSet<>();
        final List<RubriquepublicationBean> allRubPub = getByFicheUniv(ficheUniv);
        if (CollectionUtils.isNotEmpty(allRubPub)) {
            final Collection<String> lstCodeRubriqueRecherche = getAllRubriquesCodes(rubriqueCode);
            for (final RubriquepublicationBean rubriquepublicationBean : allRubPub) {
                if (lstCodeRubriqueRecherche.contains(rubriquepublicationBean.getRubriqueDest())) {
                    allRubriqueDest.add(rubriquepublicationBean.getRubriqueDest());
                }
            }
        }
        return allRubriqueDest;
    }

    /**
     * Récupère la liste des codes des rubriqe liées à la fiche et à la rubrique (ainsi que ses descendantes) fourni en paramètre
     * A la différence de {@link ServiceRubriquePublication#getRubriqueDestByFicheUnivAndRubrique(FicheUniv, String)}, si la source de la requête est renseigné (aka requete automatique)
     * on rajoute ces codes en les préfixant de #AUTO# (va savoir pourquoi c'est fait que là...)     * @param ficheUniv la fiche dont on souhaite avoir les codes des rubriques liés
     * @param rubriqueCode le code de la rubrique
     * @return Les résultats correspondant ou une liste vide si rien n'est trouvé
     */
    public Collection<String> getRubriqueDestByFicheUnivAndRubriqueWithSourceControl(final FicheUniv ficheUniv, final String rubriqueCode) {
        final Collection<String> allRubriqueDest = new HashSet<>();
        final List<RubriquepublicationBean> allRubPub = getByFicheUniv(ficheUniv);
        if (CollectionUtils.isNotEmpty(allRubPub)) {
            final Collection<String> lstCodeRubriqueRecherche = getAllRubriquesCodes(rubriqueCode);
            for (final RubriquepublicationBean rubriquepublicationBean : allRubPub) {
                computeSpecificCode(allRubriqueDest, lstCodeRubriqueRecherche, rubriquepublicationBean);
            }
        }
        return allRubriqueDest;
    }

    private void computeSpecificCode(final Collection<String> allRubriqueDest, final Collection<String> lstCodeRubriqueRecherche, final RubriquepublicationBean rubriquepublicationBean) {
        if (lstCodeRubriqueRecherche.contains(rubriquepublicationBean.getRubriqueDest())) {
            String code = rubriquepublicationBean.getRubriqueDest();
            if (StringUtils.isNotBlank(rubriquepublicationBean.getSourceRequete())) {
                code = CODE_RUBRIQUE_AUTO + code;
            }
            allRubriqueDest.add(code);
        }
    }

    private Collection<String> getAllRubriquesCodes(final String rubriqueCode) {
        final List<String> lstCodeRubriqueRecherche = new ArrayList<>();
        if (StringUtils.isNotBlank(rubriqueCode)) {
            lstCodeRubriqueRecherche.add(rubriqueCode);
            final RubriqueBean infosRub = serviceRubrique.getRubriqueByCode(rubriqueCode);
            final Collection<RubriqueBean> listeSousRubriques = serviceRubrique.getAllChilds(infosRub.getCode());
            if (!listeSousRubriques.isEmpty()) {
                for (final RubriqueBean rubrique : listeSousRubriques) {
                    lstCodeRubriqueRecherche.add(rubrique.getCode());
                }
            }
        }
        return lstCodeRubriqueRecherche;
    }

    /**
     * Supprime l'ensemble des rubriques qui n'ont pas de source de requête (aka rubrique auto) et qui ont comme rubrique destinataire le code fourni en paramètre
     * @param rubriqueDest la rubrique dont on souhaite supprimer les rubriques de publications
     */
    public void deleteByRubriqueDestAndNoSource(final String rubriqueDest) {
        dao.deleteByRubriqueDest(rubriqueDest);
    }

    /**
     * Supprime l'ensemble des rubriques qui ont en source de requête (aka rubrique auto) le paramètre fourni et qui ont comme rubrique destinataire le code fourni en paramètre
     * @param rubriqueDest la rubrique dont on souhaite supprimer les rubriques de publications
     * @param source la source de requête fourni
     */
    public void deleteByRubriqueDestAndSource(final String rubriqueDest, final String source) {
        dao.deleteByRubriqueDestAndSource(rubriqueDest, source);
    }

    /**
     * Supprime les rubriques de publciations qui sont liés à une fiches précise et qui on une source de requête (aka rubrique auto)
     * @param ficheReference la fiche référence contenant les infos pour la suppression. Si elle est null, on ne supprime rien
     */
    public void deleteByFicheReference(final InfosFicheReferencee ficheReference) {
        if (ficheReference != null) {
            dao.deleteByTypeCodeLanguageAndSource(ficheReference.getType(), ficheReference.getCode(), ficheReference.getLangue(), ficheReference.getRequete());
        }
    }

    /**
     * Supprime les rubriques de publciations qui sont liés à une fiches précise et qui n'on pas de source de requête (aka rubrique auto)
     * @param ficheReference la fiche référence contenant les infos pour la suppression. Si elle est null, on ne supprime rien
     */
    public void deleteByFicheReferenceWithoutSource(final InfosFicheReferencee ficheReference) {
        if (ficheReference != null) {
            dao.deleteByTypeCodeLanguageAndSource(ficheReference.getType(), ficheReference.getCode(), ficheReference.getLangue(), StringUtils.EMPTY);
        }
    }

    /**
     * Supprime les rubriques de publciations qui sont liés à une fiches précise sans tenir compte de la source de requête
     * @param fiche la fiche contenant les infos pour la suppression. Si elle est null, on ne supprime rien
     */
    public void deleteByFiche(final FicheUniv fiche) {
        if (fiche != null) {
            dao.deleteByTypeCodeLanguage(ReferentielObjets.getCodeObjet(fiche), fiche.getCode(), fiche.getLangue());
        }
    }

    /**
     * Supprimer les rubriques publication d'une fiche.
     * @param type
     * @param code
     * @param langue
     */
    public void deleteByTypeCodeLangue(final String type, final String code, final String langue) {
        dao.deleteByTypeCodeLanguage(type, code, langue);
    }

    /**
     * Enregistre pour une fiche la liste des rubriques référencées.
     *
     * @param infosFiche un bean contenant les infos de la fiche dont on souhaite mettre à jour les rubriques de publications
     * @param listeRubriques l'ensemble des codes de rubriques à rajouter
     */
    public void saveAllRubPubForFiche(final InfosFicheReferencee infosFiche, final Collection<String> listeRubriques) {
        deleteByFicheReferenceWithoutSource(infosFiche);
        if (listeRubriques != null) {
            for (final String codeRubriquePublication : listeRubriques) {
                final RubriquepublicationBean rubP = mapInfosFicheInRubPub(infosFiche, codeRubriquePublication);
                save(rubP);
            }
        }
    }

    /**
     * Enregistre pour une rubrique la liste des fiches référencées.
     *
     * @param codeRubriquePublication le code de rubrique dont on souhaite mettre à jour les rubriques de publication
     * @param listeFiches la liste des fiches à mettre à jour
     */
    public void saveAllFicheForRubPub(final String codeRubriquePublication, final Collection<InfosFicheReferencee> listeFiches) {
        deleteByRubriqueDestAndNoSource(codeRubriquePublication);
        /* Insertion des nouvelles fiches */
        if (listeFiches != null) {
            for (final InfosFicheReferencee infosFiche : listeFiches) {
                final RubriquepublicationBean rubP = mapInfosFicheInRubPub(infosFiche, codeRubriquePublication);
                save(rubP);
            }
        }
    }

    private RubriquepublicationBean mapInfosFicheInRubPub(final InfosFicheReferencee infosFiche, final String codeRubriquePublication) {
        final RubriquepublicationBean rubP = new RubriquepublicationBean();
        rubP.setTypeFicheOrig(infosFiche.getType());
        rubP.setCodeFicheOrig(infosFiche.getCode());
        rubP.setLangueFicheOrig(infosFiche.getLangue());
        rubP.setRubriqueDest(codeRubriquePublication);
        return rubP;
    }

    /**
     * Vieux code du produit qui récupère pour une requête données les infos des fiches liés au code de rubrique fourni en paramètre
     * @param codeRubrique le code de la rubrique dont on souhaite avoir les {@link InfosFicheReferencee}
     * @param requete la requête pour laquelle on souhaite avoir les {@link InfosFicheReferencee}. Un traitement spécial est fait pour un "ancien format"...
     * @return Un set contenant les informations des fiches, ou une liste vide si rien n'est trouvé. Si un des arguments ou les deux sont null, on retourne aussi une liste vide
     */
    public Set<InfosFicheReferencee> getInfosFichesReferencees(final String codeRubrique, final String requete) {
        final Set<InfosFicheReferencee> listeReferencement = new TreeSet<>(new InfosFicheComparator());
        if (codeRubrique != null && requete != null) {
            // sauvegarde des fiches pour indexation
            final List<RubriquepublicationBean> results = getByRubriqueDestAndSource(codeRubrique, requete);

            final String[] item = requete.split(REQUEST_SEPARATOR, -2);
            //si la requete est au nouveau format et que la langue de cette requete n'est pas renseignée, on recherche la requete aussi dans l'ancien format
            if (item.length > 4 && item[3].length() == 0) {
                String requeteOldFormat = new StringBuilder(item[0]).append(REQUEST_SEPARATOR)
                        .append(item[1]).append(REQUEST_SEPARATOR)
                        .append(item[2]).append(REQUEST_SEPARATOR)
                        .append(item[4]).toString();
                results.addAll( getByRubriqueDestAndSource(codeRubrique, requeteOldFormat) );
            }
            for (final RubriquepublicationBean rubP : results) {
                final InfosFicheReferencee infosFiche = new InfosFicheReferencee();
                infosFiche.setCode(rubP.getCodeFicheOrig());
                infosFiche.setLangue(rubP.getLangueFicheOrig());
                infosFiche.setType(rubP.getTypeFicheOrig());
                infosFiche.setRequete(rubP.getSourceRequete());
                listeReferencement.add(infosFiche);
            }
        }
        return listeReferencement;
    }

    /**
     * Supprime le rattachement des fiches à une rubrique provenant d'une requete de publication automatique car cette requete a ete supprimee AA 200805 : ajout d'un test car les
     * requetes comportent desormais un critère de plus : la langue leur syntaxe diffère donc entre les anciennes requetes et les nouvelles.
     *
     * @param codeRubrique le code de la rubrique dont on souhaite avoir les {@link InfosFicheReferencee}
     * @param requete la requête pour laquelle on souhaite avoir les {@link InfosFicheReferencee}. Un traitement spécial est fait pour un "ancien format"...
     *
     * @return Un set contenant les informations des fiches, ou une liste vide si rien n'est trouvé. Si un des arguments ou les deux sont null, on retourne aussi une liste vide
     *
     */
    public Set<InfosFicheReferencee> deleteRubPubAuto(final String codeRubrique, final String requete) {
        final Set<InfosFicheReferencee> listeReferencement = getInfosFichesReferencees(codeRubrique, requete);
        if (codeRubrique != null && requete != null) {
            deleteByRubriqueDestAndSource(codeRubrique, requete);

            final String[] item = requete.split(REQUEST_SEPARATOR, -2);
            //si la requete est au nouveau format et que la langue de cette requete n'est pas renseignée, on recherche la requete aussi dans l'ancien format
            if (item.length > 4 && item[3].length() == 0) {
                String requeteOldFormat = new StringBuilder(item[0]).append(REQUEST_SEPARATOR)
                        .append(item[1]).append(REQUEST_SEPARATOR)
                        .append(item[2]).append(REQUEST_SEPARATOR)
                        .append(item[4]).toString();
                deleteByRubriqueDestAndSource(codeRubrique, requeteOldFormat);
            }
        }
        return listeReferencement;
    }


    /**
     * Renvoie pour rubrique la liste des fiches référéencés.
     *
     * @param codeRubriquePublication le code de la rubrique dont on souhaite avoir les {@link InfosFicheReferencee}
     * @param avecInfosFiche rajoute l'intitulé l'état et l'id de la fiche si il est mis à true... OK.
     *
     * @return la liste contenant les infosFichesReferences ou une liste vide si rien n'est trouvé
     *
     * @throws ErreurApplicative lors de la requête sur les fiches
     */
    public List<InfosFicheReferencee> getListeFichesReferences(final String codeRubriquePublication, final boolean avecInfosFiche) throws ErreurApplicative {
        final List<InfosFicheReferencee> liste = new ArrayList<>();
        if (codeRubriquePublication != null) {
            final List<RubriquepublicationBean> results = getByRubriqueDestWithoutSource(codeRubriquePublication);
            for (final RubriquepublicationBean rubP : results) {
                final InfosFicheReferencee infosFiche = new InfosFicheReferencee();
                infosFiche.setCode(rubP.getCodeFicheOrig());
                infosFiche.setLangue(rubP.getLangueFicheOrig());
                infosFiche.setType(rubP.getTypeFicheOrig());
                infosFiche.setRequete(rubP.getSourceRequete());
                // Et on rajoute pour chaque élément le libellé
                if (avecInfosFiche) {
                    final FicheUniv fiche = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(rubP.getTypeFicheOrig()));
                    if (fiche != null) {
                        try (ContexteDao contexteDao = new ContexteDao()) {
                            fiche.setCtx(contexteDao);
                            if (fiche.selectCodeLangueEtat(rubP.getCodeFicheOrig(), rubP.getLangueFicheOrig(), EtatFiche.EN_LIGNE.getEtat()) > 0) {
                                while (fiche.nextItem()) {
                                    infosFiche.setIntitule(fiche.getLibelleAffichable());
                                    infosFiche.setEtat(fiche.getEtatObjet());
                                    infosFiche.setId(fiche.getIdFiche().toString());
                                    liste.add(infosFiche);
                                }
                            }
                        } catch (final Exception e) {
                            throw new ErreurApplicative("An error occured requesting on a 'ficheUniv'",e);
                        }
                    } else {
                        LOG.warn(String.format("Object with code %s couldn't be instanciated.", rubP.getTypeFicheOrig()));
                    }
                } else {
                    liste.add(infosFiche);
                }
            }
        }
        return liste;
    }
}
