package com.univ.objetspartages.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.AbstractServiceBean;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.cache.CacheLibelleManager;
import com.univ.objetspartages.dao.impl.LabelDAO;
import com.univ.utils.ContexteUtil;
import com.univ.utils.sql.RequeteSQL;

/**
 * Created on 21/04/15.
 * Service exposing methods to handle {@link com.univ.objetspartages.bean.LabelBean}
 * @see com.univ.objetspartages.bean.LabelBean
 * @see com.univ.objetspartages.dao.impl.LabelDAO
 */
public class ServiceLabel extends AbstractServiceBean<LabelBean, LabelDAO> {

    private CacheLibelleManager cacheLibelleManager;

    public void setCacheLibelleManager(final CacheLibelleManager cacheLibelleManager) {
        this.cacheLibelleManager = cacheLibelleManager;
    }

    /**
     * Creates and save a new {@link com.univ.objetspartages.bean.LabelBean}.
     * @param type : the label type as a {@link String}
     * @param code : the label code as a {@link String}
     * @param libelle : the value of the label as a {@link String}
     * @param langue : the language of the label as a {@link String}
     */
    public void createNewLabel(final String type, final String code, final String libelle, final String langue) {
        LabelBean labelBean = getByTypeCodeLanguage(type, code, langue);
        if(labelBean != null) {
            labelBean.setLibelle(libelle);
        } else {
            labelBean = new LabelBean();
            labelBean.setCode(code);
            labelBean.setLangue(langue);
            labelBean.setType(type);
            labelBean.setLibelle(libelle);
        }
        save(labelBean);
    }

    /**
     * Computes and get a label value which can be used in a display context (e.g front-office).
     * @param labelBean : the {@link com.univ.objetspartages.bean.LabelBean} to get the displayable value from.
     * @return the compute {@link String}
     */
    public String getDisplayableLibelle(final LabelBean labelBean) {
        if (ContexteUtil.getContexteUniv() != null && labelBean.getLibelle().startsWith("[")) {
            final int i = labelBean.getLibelle().indexOf("]") + 1;
            if (i != 0 && i != labelBean.getLibelle().length()) {
                return labelBean.getLibelle().substring(i);
            }
        }
        return labelBean.getLibelle();
    }

    /**
     * Get site code of a given {@link com.univ.objetspartages.bean.LabelBean}.
     * @param labelBean : the {@link com.univ.objetspartages.bean.LabelBean} to retrieve the site code from.
     * @return the site code as a {@link java.lang.String}
     */
    public String getCodeSite(final LabelBean labelBean) {
        if (labelBean.getLibelle().startsWith("[")) {
            final int i = labelBean.getLibelle().indexOf("]") + 1;
            if (i != 0 && i != labelBean.getLibelle().length()) {
                return labelBean.getLibelle().substring(1, i - 1);
            }
        }
        return StringUtils.EMPTY;
    }
    
    /**
     * Retrieve the {@link com.univ.objetspartages.bean.LabelBean} identified by the given parameters.
     * This method is cached.
     * @param type : the type of the label to find as a {@link String}.
     * @param code : the code of the label as a {@link String}.
     * @param language : the language to in which the label should be retrieved as a {@link String}.
     * @return the matching {@link com.univ.objetspartages.bean.LabelBean} or null otherwise.
     * @see com.univ.objetspartages.cache.CacheLibelleManager
     */
    public LabelBean getByTypeCodeLanguage(final String type, final String code, final String language) {
        return cacheLibelleManager.getListeInfosLibelles().get(String.format("%s%s%s", type, code, language));
    }

    /**
     * Retrieve the {@link com.univ.objetspartages.bean.LabelBean} identified by the given parameters.
     * @param libelle : the value of the label as a {@link String}
     * @param type : the type of the label to find as a {@link String}.
     * @param language : the language to in which the label should be retrieved as a {@link String}.
     * @return the matching {@link com.univ.objetspartages.bean.LabelBean} or null otherwise.
     */
    public LabelBean getByLibelleTypeLanguage(final String libelle, final String type, final String language) {
        return dao.getByLibelleTypeLanguage(libelle, type, language);
    }

    /**
     * Get a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean} according to the given parameters.
     * @param type : the type to find as a {@link String}.
     * @param code : the code to find as a {@link String}.
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.LabelBean}
     */
    public List<LabelBean> getByTypeCode(final String type, final String code) {
        return dao.getByTypeCode(type, code);
    }

    /**
     * Get a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean} according to the given parameters.
     * @param code : the code to find as a {@link String}.
     * @param language : the language to in which labels should be retrieved as a {@link String}.
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.LabelBean}
     */
    public List<LabelBean> getByCodeLanguage(final String code, final String language) {
        return dao.getByCodeLanguage(code, language);
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean} identified by the given parameters.
     * @param type : the type labels to find as a {@link String}.
     * @param codes : label codes as a {@link java.util.List} of {@link String}.
     * @param language : the language to in which labels should be retrieved as a {@link String}.
     * @return the matching {@link com.univ.objetspartages.bean.LabelBean} or null otherwise.
     */
    public List<LabelBean> getByTypeCodesLanguage(final String type, final List<String> codes, final String language) {
        return dao.getByTypeCodesLanguage(type, codes, language);
    }

    /**
     * Get a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean} according to the given parameters.
     * @param type : the type labels to find as a {@link String}.
     * @param language : the language to in which labels should be retrieved as a {@link String}.
     * @return a {@link java.util.List} of matching {@link com.univ.objetspartages.bean.LabelBean}
     */
    public List<LabelBean> getByTypeLanguage(final String type, final String language) {
        return dao.getByTypeLanguage(type, language);
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean} identified by the given parameters.
     * @param libelle : the value of the label as a {@link String}
     * @param type : the type of the label to find as a {@link String}.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean}
     */
    public List<LabelBean> getByTypeMatchingLibelle(final String type, final String libelle) {
        return dao.getByTypeLibelle(type, libelle);
    }

    /**
     * Retrieve a {@link java.util.List} of {@link String} identified by the given parameters.
     * @param type : the type labels to find as a {@link String}.
     * @param codes : label codes as a {@link java.util.List} of {@link String}.
     * @param language : the language to in which labels should be retrieved as a {@link String}.
     * @return a {@link java.util.List} of {@link String}.
     */
    public List<String> getLabelsLibelles(final String type, final List<String> codes, final String language) {
        final List<String> libelles = new ArrayList<>();
        for(final String currentCode : codes) {
            final LabelBean labelBean = getByTypeCodeLanguage(type, currentCode, language);
            if(labelBean != null) {
                libelles.add(getDisplayableLibelle(labelBean));
            } else {
                libelles.add(MessageHelper.getCoreMessage("LIBELLE.INCONNU"));
            }
        }
        return libelles;
    }

    /**
     * Retrieve a {@link java.util.List} of {@link String} identified by the given parameters.
     * @param type : the type labels to find as a {@link String}.
     * @param codes : label codes as a {@link java.util.List} of {@link String}.
     * @param locale : the language to in which labels should be retrieved as a {@link java.util.Locale}.
     * @return the matching {@link com.univ.objetspartages.bean.LabelBean} or null otherwise.
     */
    public List<String> getLabelsLibelles(final String type, final List<String> codes, final Locale locale) {
        return getLabelsLibelles(type, codes, LangueUtil.getLangueLocale(locale));
    }

    /**
     * Retrieve a {@link java.util.Map} of {@link String},{@link String} identified by the given parameters.
     * @param type : the type labels to find as a {@link String}.
     * @param locale : the language to in which labels should be retrieved as a {@link java.util.Locale}.
     * @return a {@link java.util.Map} of {@link String},{@link String}.
     * @see #getLabelForCombo(String, String)
     */
    public Map<String, String> getLabelForCombo(final String type, final Locale locale) {
        return getLabelForCombo(type, String.valueOf(LangueUtil.getIndiceLocale(locale)));
    }

    /**
     * Retrieve a {@link java.util.Map} of {@link String},{@link String} identified by the given parameters.
     * @param type : the type labels to find as a {@link String}.
     * @param language : the language to in which labels should be retrieved as a {@link java.lang.String}.
     * @return a {@link java.util.Map} of {@link String},{@link String}.
     * @see #getLabelForCombo(String, String)
     */
    public Map<String, String> getLabelForCombo(final String type, final String language) {
        final Map<String, String> comboMap = new HashMap<>();
        final List<LabelBean> labels = getByTypeLanguage(type, language);
        for(final LabelBean currentLabel : labels) {
            if (ContexteUtil.getContexteUniv() != null) {
                final String codeSite = ContexteUtil.getContexteUniv().getInfosSite().getAlias();
                final String codeSiteLibelle = getCodeSite(currentLabel);
                if (StringUtils.isEmpty(codeSiteLibelle) || codeSiteLibelle.equalsIgnoreCase(codeSite)) {
                    comboMap.put(currentLabel.getCode(), getDisplayableLibelle(currentLabel));
                }
            } else {
                comboMap.put(currentLabel.getCode(), currentLabel.getLibelle());
            }
        }
        return comboMap;
    }

    /**
     * Retrieve a {@link java.util.Map} of {@link String},{@link String} identified by the given parameters.
     * @param type : the type labels to find as a {@link String}.
     * @param libelle : the libelle to match as a {@link java.lang.String}
     * @return a {@link java.util.Map} of {@link String},{@link String}.
     * @see #getLabelForCombo(String, Locale)
     */
    public Map<String, String> getMatchingLabelForCombo(final String type, final String libelle) {
        final Map<String, String> comboMap = new HashMap<>();
        final List<LabelBean> labels = getByTypeMatchingLibelle(type, libelle);
        for(final LabelBean currentLabel : labels) {
            comboMap.put(currentLabel.getCode(), currentLabel.getLibelle());
        }
        return comboMap;
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean} from an SQL request.
     * @param req : the SQL request as a {@link com.univ.utils.sql.RequeteSQL}
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean}
     */
    public List<LabelBean> getFromRequest(final RequeteSQL req) {
        return dao.select(req.formaterRequete());
    }

    /**
     * Retrieve a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean} according to the given type.
     * @param type : the type to match as a {@link String}
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean}
     */
    public List<LabelBean> getByType(final String type) {
        return dao.getByType(type);
    }

    /**
     * Get all persisted labels
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.LabelBean}
     */
    public List<LabelBean> getAllLabels() {
        return dao.getAll();
    }
}
