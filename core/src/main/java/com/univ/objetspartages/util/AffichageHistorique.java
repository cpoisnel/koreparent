package com.univ.objetspartages.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.HistoriqueBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.ContexteDao;
// TODO: Auto-generated Javadoc

/**
 * processus saisie Utilisateur.
 */
public class AffichageHistorique {

    private static final Logger LOG = LoggerFactory.getLogger(AffichageHistorique.class);

    public static List<HistoriqueBean> getListeHistorique(final OMContext ctx, final InfoBean infoBean) throws Exception {
        final String codeObjet = infoBean.getString("CODE_OBJET");
        final Long idFiche = Long.valueOf(infoBean.getString("ID_FICHE"));
        final FicheUniv ficheUniv = retrouverFiche(ctx, ReferentielObjets.getNomObjet(codeObjet), idFiche);
        return getAllHistorique(ficheUniv);
    }

    public static List<HistoriqueBean> getAllHistorique(final FicheUniv ficheUniv) throws Exception {
        final List<HistoriqueBean> historiqueDispo = new ArrayList<>();
        if (ficheUniv != null) {
            final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
            MetatagBean meta = MetatagUtils.lireMeta(ficheUniv);
            historiqueDispo.addAll(instancierFicheHistoriqueDepuisInfoMeta(ficheUniv, serviceMetatag.getHistory(meta)));
            final String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
            final FicheUniv fichesAparentees = ReferentielObjets.instancierFiche(codeObjet);
            try (ContexteDao ctx = new ContexteDao()) {
                fichesAparentees.setCtx(ctx);
                fichesAparentees.selectCodeLangueEtat(ficheUniv.getCode(), ficheUniv.getLangue(), "");
                while (fichesAparentees.nextItem()) {
                    if (!fichesAparentees.getEtatObjet().equals(ficheUniv.getEtatObjet()) || !fichesAparentees.getIdFiche().equals(ficheUniv.getIdFiche())) {
                        meta = MetatagUtils.lireMeta(codeObjet, ficheUniv.getIdFiche());
                        historiqueDispo.addAll(instancierFicheHistoriqueDepuisInfoMeta(ficheUniv, serviceMetatag.getHistory(meta)));
                    }
                }
            }
        }
        return historiqueDispo;
    }

    public static List<HistoriqueBean> getHistorique(final FicheUniv ficheUniv) {
        final List<HistoriqueBean> historiqueDispo = new ArrayList<>();
        if (ficheUniv != null) {
            final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
            final MetatagBean meta = MetatagUtils.lireMeta(ficheUniv);
            historiqueDispo.addAll(instancierFicheHistoriqueDepuisInfoMeta(ficheUniv, serviceMetatag.getHistory(meta)));
        }
        return historiqueDispo;
    }

    private static List<HistoriqueBean> instancierFicheHistoriqueDepuisInfoMeta(final FicheUniv fiche, final List<String> vecteurHistorique) {
        final List<HistoriqueBean> historiques = new ArrayList<>();
        for (String valeur : vecteurHistorique) {
            final HistoriqueBean historique = new HistoriqueBean();
            valeur = StringUtils.substringBetween(valeur, "[", "]");
            final String[] valeursSplittes = valeur.split("/");
            historique.setIdFiche(fiche.getIdFiche());
            historique.setAction(MetatagUtils.getIntituleAction(valeursSplittes[0]));
            try {
                historique.setDateAction(DateUtils.parseDate(valeursSplittes[1], "yyyyMMddhhmmss"));
            } catch (final ParseException e) {
                LOG.info("impossible de parser la date de l'historique de la fiche.", e);
            }
            historique.setUtilisateur(valeursSplittes[2]);
            historique.setEtat(EtatFiche.getEtatParCode(valeursSplittes[3]));
            historiques.add(historique);
        }
        return historiques;
    }

    private static FicheUniv retrouverFiche(final OMContext ctx, final String codeObjet, final Long idFiche) {
        FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
        ficheUniv.setCtx(ctx);
        ficheUniv.init();
        try {
            /* Lecture de la fiche */
            ficheUniv.setIdFiche(idFiche);
            ficheUniv.retrieve();
        } catch (final Exception ex) {
            LOG.error("unable to retrieve the content", ex);
            ficheUniv = null;
        }
        return ficheUniv;
    }

}
