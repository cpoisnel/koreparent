package com.univ.objetspartages.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.services.ServiceLabel;
import com.univ.utils.ContexteUtil;

/**
 * Created on 23/04/15.
 */
public class LabelUtils {

    private static ServiceLabel getServiceLabel() {
        return ServiceManager.getServiceForBean(LabelBean.class);
    }

    public static String getLibelle(final String type, final String code, final Locale language) {
        return getLibelle(type, code, String.valueOf(LangueUtil.getIndiceLocale(language)));
    }

    public static String getLibelle(final String type, final String code, final String language) {
        final List<String> codes = new ArrayList<>();
        if(StringUtils.isNotBlank(code) && !"0000".equals(code)) {
            codes.add(code);
        }
        final List<String> libelles = getServiceLabel().getLabelsLibelles(type, codes, language);
        if(ContexteUtil.getContexteUniv() != null) {
            return StringUtils.join(libelles, ", ");
        } else {
            return StringUtils.join(libelles, ";");
        }
    }

    public static List<String> getLibelleList(final String type, final String codes, final Locale locale) {
        List<String> codeList = new ArrayList<>();
        if(StringUtils.isNotBlank(codes) && !"0000".equals(codes)) {
            codeList = Arrays.asList(codes.split(";"));
        }
        return getServiceLabel().getLabelsLibelles(type, codeList, Integer.toString(LangueUtil.getIndiceLocale(locale)));
    }

    public static Map<String, String> getLabelCombo(final String code, final Locale locale) {
        return getServiceLabel().getLabelForCombo(code, locale);
    }

    public static LabelBean getLabel(final String type, final String code, final String language) {
        return getServiceLabel().getByTypeCodeLanguage(type, code, language);
    }
}
