package com.univ.objetspartages.util;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.mediatheque.Mediatheque;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.processus.SaisieMedia;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FileUtil;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * Created on 06/05/15.
 */
public class MediaUtils {

    /**
     * Checks if is local.
     *
     * @return true, if is local
     */
    public static boolean isLocal(final MediaBean mediaBean) {
        return mediaBean != null && !StringUtils.contains(mediaBean.getUrl(),"/");
    }

    public static boolean isPublic(final MediaBean mediaBean) {
        return mediaBean != null && !mediaBean.getIsMutualise().equals(Mediatheque.ETAT_NON_MUTUALISE_NON_PUBLIC);
    }

    public static Map<String, String> getMediasAsMap(final List<MediaBean> medias) {
        final Map<String, String> mediasMap = new HashMap<>();
        for(final MediaBean currentMedia : medias) {
            mediasMap.put(currentMedia.getId().toString(), getLibelleAffichable(currentMedia));
        }
        return mediasMap;
    }

    public static String getUrlAbsolue(final MediaBean mediaBean) {
        if(mediaBean != null) {
            String sUrl = mediaBean.getUrl();
            if (StringUtils.isNotBlank(sUrl) && isLocal(mediaBean)) {
                if (isPublic(mediaBean)) {
                    sUrl = MediathequeHelper.getAbsolutePath();
                    String sServerPath = new File(WebAppUtil.getWebInfPath()).getAbsolutePath();
                    if(!sServerPath.endsWith(File.separator)) {
                        sServerPath += File.separator;
                    }
                    if (sUrl.indexOf(sServerPath) == 0) {
                        if (sServerPath.endsWith("/")) {
                            sUrl = sUrl.substring(sServerPath.length());
                        } else {
                            sUrl = "/" + sUrl.substring(sServerPath.length());
                        }
                    } else {
                        sUrl = MediathequeHelper.getDefaultRelativePath();
                        if(!StringUtils.startsWith(sUrl, "/")) {
                            sUrl = "/" + sUrl;
                        }
                    }
                    if (StringUtils.isNotBlank(mediaBean.getTypeRessource())) {
                        sUrl += "/" + mediaBean.getTypeRessource().toLowerCase();
                    }
                    sUrl += "/" + mediaBean.getUrl();
                }
                // soit un fichier privé
                else {
                    sUrl = String.format("%s?ID_FICHIER=%d", RessourceUtils.PATH_SERVLET_LECTURE_FICHIER, mediaBean.getId());
                }
            }
            if(StringUtils.startsWith(sUrl, "/") || !isLocal(mediaBean)) {
                return sUrl;
            } else {
                return "/" + sUrl;
            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the url vignette absolue.
     *
     * @return the url vignette absolue
     */
    public static String getUrlVignetteAbsolue(final MediaBean mediaBean) {
        if(mediaBean != null) {
            final File thumbnailFile = new File(MediathequeHelper.getAbsolutePath() + "/" + mediaBean.getTypeRessource() + "/" + mediaBean.getUrlVignette());
            String sUrl = mediaBean.getUrlVignette();
            if (StringUtils.isNotBlank(sUrl) && thumbnailFile.exists()) {
                sUrl = MediathequeHelper.getAbsolutePath();
                String sServerPath = new File(WebAppUtil.getWebInfPath()).getAbsolutePath();
                if(!sServerPath.endsWith(File.separator)) {
                    sServerPath += File.separator;
                }
                if (sUrl.indexOf(sServerPath) == 0) {
                    if (sServerPath.endsWith("/")) {
                        sUrl = sUrl.substring(sServerPath.length());
                    } else {
                        sUrl = "/" + sUrl.substring(sServerPath.length());
                    }
                } else {
                    sUrl = MediathequeHelper.getDefaultRelativePath();
                    if(!StringUtils.startsWith(sUrl, "/")) {
                        sUrl = "/" + sUrl;
                    }
                }
                if (StringUtils.isNotBlank(mediaBean.getTypeRessource())) {
                    sUrl += "/" + mediaBean.getTypeRessource().toLowerCase();
                }
                sUrl += "/" + mediaBean.getUrlVignette();
                if(StringUtils.startsWith(sUrl, "/") || !isLocal(mediaBean)) {
                    return sUrl;
                } else {
                    return "/" + sUrl;
                }
            }
        }
        return StringUtils.EMPTY;
    }

    public static String getPathVignetteAbsolu(final MediaBean mediaBean) {
        if(mediaBean != null) {
            String sUrl = mediaBean.getUrlVignette();
            if (sUrl.length() > 0) {
                sUrl = MediathequeHelper.getAbsolutePath();
                if (StringUtils.isNotBlank(mediaBean.getTypeRessource())) {
                    sUrl += "/" + mediaBean.getTypeRessource().toLowerCase();
                }
                sUrl += "/" + mediaBean.getUrlVignette();
            }
            return sUrl;
        }
        return StringUtils.EMPTY;
    }

    /**
     * Renvoie le libelle pour l'attribut thematique.
     *
     * @return the libelle thematique
     *
     */
    public static String getLibelleThematique(final MediaBean mediaBean) {
        return LabelUtils.getLibelle("04", mediaBean.getThematique(), LangueUtil.getDefaultLocale());
    }

    /**
     * Renvoie le libelle a afficher.
     *
     * @return the libelle affichable
     */
    public static String getLibelleAffichable(final MediaBean mediaBean) {
        String res = mediaBean.getTitre();
        if (res.length() == 0) {
            res = mediaBean.getSource();
        }
        return res;
    }

    public static String getPathAbsolu(final MediaBean media) {
        if(media != null) {
            String sUrl = media.getUrl();
            if (sUrl.length() > 0 && isLocal(media)) {
                if (isPublic(media)) {
                    sUrl = MediathequeHelper.getAbsolutePath();
                    if (StringUtils.isNotBlank(media.getTypeRessource())) {
                        sUrl += "/" + media.getTypeRessource().toLowerCase();
                    }
                } else {
                    sUrl = RessourceUtils.getAbsolutePath();
                }
                sUrl += "/" + media.getUrl();
            }
            return sUrl;
        }
        return StringUtils.EMPTY;
    }

    /**
     * Generate name.
     *
     * @param extension
     *            the extension
     *
     * @return the string
     */
    public static String generateName(final MediaBean mediaBean, String extension) {
        if (extension.length() == 0) {
            extension = FileUtil.getExtension(mediaBean.getSource());
        }
        final String nomFichierSansExtension = StringUtils.defaultString(StringUtils.substringBeforeLast(mediaBean.getSource(), "."));
        return Chaine.formatString(nomFichierSansExtension) + "_" + System.currentTimeMillis() + MediathequeHelper.PRE_EXTENSION_CHARACTER + extension;
    }

    public static ConditionList conditionPerimetreMedia(final String mode) {
        final ConditionList conditionBOMedia = new ConditionList();
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final AutorisationBean autorisations = ctx.getAutorisation();
        if (autorisations != null) {
            Boolean accesTousPerimetre = Boolean.FALSE;
            final Map<String, Vector<Perimetre>> permisssions = autorisations.getListePermissions();
            for (final Map.Entry<String, Vector<Perimetre>> clePermission : permisssions.entrySet()) {
                final PermissionBean permission = new PermissionBean(clePermission.getKey());
                if (isValidePermission(permission, mode)) {
                    final Vector<Perimetre> lesPerimetres = clePermission.getValue();
                    for (final Perimetre perimetre : lesPerimetres) {
                        final ConditionList conditionPerimetre = new ConditionList();
                        accesTousPerimetre = isAccesTousPerimetre(perimetre);
                        if (!accesTousPerimetre) {
                            conditionPerimetre.setPremiereCondtion(traiterConditionRubrique(perimetre));
                            conditionPerimetre.and(traiterConditionStructure(perimetre));
                        }
                        conditionBOMedia.or(conditionPerimetre);
                    }
                }
            }
            if (!accesTousPerimetre) {
                conditionBOMedia.or(ConditionHelper.egalVarchar("CODE_REDACTEUR", autorisations.getCode()));
            }
        }
        return conditionBOMedia;
    }

    private static Condition traiterConditionStructure(final Perimetre perimetre) {
        Condition conditionsStructures = new ConditionList();
        final String codeStructure = perimetre.getCodeStructure();
        if (StringUtils.isNotEmpty(codeStructure)) {
            conditionsStructures = ConditionHelper.getConditionStructure("CODE_RATTACHEMENT", codeStructure);
        }
        return conditionsStructures;
    }

    private static Condition traiterConditionRubrique(final Perimetre perimetre) {
        Condition conditionsRubriques = new ConditionList();
        final String codeRubrique = perimetre.getCodeRubrique();
        if (StringUtils.isNotEmpty(codeRubrique)) {
            conditionsRubriques = ConditionHelper.getConditionRubrique("CODE_RUBRIQUE", codeRubrique);
        }
        return conditionsRubriques;
    }

    private static boolean isAccesTousPerimetre(final Perimetre perimetre) {
        return perimetre.getCodeRubrique().length() == 0 && perimetre.getCodeStructure().length() == 0;
    }

    private static boolean isValidePermission(final PermissionBean permission, final String mode) {
        return "TECH".equals(permission.getType()) && (SaisieMedia.MODE_ADMINISTRATION.equals(mode) && "pho".equals(permission.getObjet()) && ("M".equals(permission.getAction()) || "S".equals(permission.getAction())) || (!SaisieMedia.MODE_ADMINISTRATION.equals(mode) && "acp".equals(permission.getObjet())));
    }

    public static ClauseWhere preparerRequete(final Long idMedia, final String titre, final String legende, final String description, final String copyright, final String auteur, final String typeRessource, final String typeMedia, final String thematique, final String codeRattachement, final String codeRubrique, final String codeRedacteur, final String metaKeywords, final Date dateCreation, final String _mode) {
        final ClauseWhere where = new ClauseWhere();
        //CQCB? -> TODO : type de donnees non pris en compte, controler le parametre
        if (!idMedia.equals(Long.valueOf(0))) {
            where.setPremiereCondition(ConditionHelper.egal("T1.ID_MEDIA", idMedia, TypeOperande.LONG));
        }
        if (StringUtils.isNotEmpty(titre)) {
            final ConditionList titreEtSource = new ConditionList(ConditionHelper.rechercheMots("T1.TITRE", titre));
            titreEtSource.or(ConditionHelper.rechercheMots("T1.SOURCE", titre));
            where.and(titreEtSource);
        }
        if (StringUtils.isNotEmpty(legende)) {
            where.and(ConditionHelper.rechercheMots("T1.LEGENDE", legende));
        }
        if (StringUtils.isNotEmpty(description)) {
            where.and(ConditionHelper.rechercheMots("T1.DESCRIPTION", description));
        }
        if (StringUtils.isNotEmpty(copyright)) {
            where.and(ConditionHelper.rechercheMots("T1.COPYRIGHT", copyright));
        }
        if (StringUtils.isNotEmpty(auteur)) {
            where.and(ConditionHelper.rechercheMots("T1.AUTEUR", auteur));
        }
        if (StringUtils.isNotEmpty(typeRessource) && !"0000".equals(typeRessource)) {
            where.and(ConditionHelper.egalVarchar("T1.TYPE_RESSOURCE", typeRessource));
        } else {
            where.and(ConditionHelper.notEgal("T1.TYPE_RESSOURCE", "", TypeOperande.TEXTE));
        }
        if (StringUtils.isNotEmpty(typeMedia) && !"0000".equals(typeMedia)) {
            where.and(ConditionHelper.egalVarchar("T1.TYPE_MEDIA", typeMedia));
        }
        if (StringUtils.isNotEmpty(thematique) && !"0000".equals(thematique)) {
            where.and(ConditionHelper.egalVarchar("T1.THEMATIQUE", thematique));
        }
        if (StringUtils.isNotEmpty(codeRattachement)) {
            where.and(ConditionHelper.egalVarchar("T1.CODE_RATTACHEMENT", codeRattachement));
        }
        if (StringUtils.isNotEmpty(codeRubrique)) {
            where.and(ConditionHelper.getConditionRubrique("CODE_RUBRIQUE", codeRubrique));
        }
        if (StringUtils.isNotEmpty(codeRedacteur)) {
            where.and(ConditionHelper.egalVarchar("T1.CODE_REDACTEUR", codeRedacteur));
        }
        if (StringUtils.isNotEmpty(metaKeywords)) {
            where.and(ConditionHelper.rechercheMots("META_KEYWORDS", metaKeywords));
        }
        if (Formateur.estSaisie(dateCreation)) {
            where.and(ConditionHelper.greaterEquals("T1.DATE_CREATION", dateCreation, TypeOperande.DATE_ET_HEURE));
        }
        where.and(ConditionHelper.egalVarchar("T1.IS_MUTUALISE", "0"));
        where.and(conditionPerimetreMedia(_mode));
        return where;
    }

    public static ClauseWhere preparerRequete(final Long idMedia, final String titre, final String legende, final String description, final String copyright, final String auteur, final String typeRessource, final String typeMedia, final String thematique, final String codeRattachement, final String codeRubrique, final String codeRedacteur, final String metaKeywords, final Date dateCreation, final String _mode, final String poidsMinimum, final String poidsMaximum) {
        final ClauseWhere where = preparerRequete(idMedia, titre, legende, description, copyright, auteur, typeRessource, typeMedia, thematique, codeRattachement, codeRubrique, codeRedacteur, metaKeywords, dateCreation, _mode);
        if (StringUtils.isNotBlank(poidsMinimum)) {
            where.and(ConditionHelper.greaterEquals("T1.POIDS", poidsMinimum, TypeOperande.VARCHAR));
        }
        if (StringUtils.isNotBlank(poidsMaximum)) {
            where.and(ConditionHelper.lessEquals("T1.POIDS", poidsMaximum, TypeOperande.VARCHAR));
        }
        return where;
    }

    /**
     * Renvoie le libelle pour l'attribut typeRessource.
     *
     * @return the libelle type ressource
     *
     */
    public String getLibelleTypeRessource(final MediaBean mediaBean) {
        return Mediatheque.getInstance().getRessourceByExtension(FileUtil.getExtension(mediaBean.getSource())).getLibelle();
    }

    /**
     * Renvoie le libelle pour l'attribut typeMedia.
     *
     * @return the libelle type media
     *
     */
    public String getLibelleTypeMedia(final MediaBean mediaBean) {
        return LabelUtils.getLibelle("201", mediaBean.getTypeMedia(), LangueUtil.getDefaultLocale());
    }

    public static boolean isPhoto(final MediaBean mediaBean) {
        return mediaBean != null && "photo".equalsIgnoreCase(mediaBean.getTypeRessource());
    }
}
