package com.univ.objetspartages.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.utils.ContexteUniv;
import com.univ.utils.FicheUnivHelper;
import com.univ.utils.SessionUtil;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseLimit;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.RequeteSQLHelper;

public class ReferentielObjetsUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ReferentielObjetsUtil.class);

    /**
     * Methode renvoyant une Hashtable contenant les couples(TypeObjet,Thematique) des objets metiers
     *
     * @param ctx
     * @return Hashtable<type objet, nom objet>
     * @deprecated méthode plus utilisée
     */
    @Deprecated
    public static Hashtable<String, String> getListeTypedeFiche(final ContexteUniv ctx) {
        final Hashtable<String, String> mapsListeFiche = new Hashtable<>();
        for (final Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
            if (objet.isRecherchable()) {
                mapsListeFiche.put(objet.getNomObjet(), objet.getLibelleObjet());
            }
        }
        return mapsListeFiche;
    }

    /**
     * Calcule les types de fiches dont on peut créer des fiches via le backoffice
     *
     * @return la liste des objets créable par l'utilisateur
     */
    public static List<Objetpartage> getListeFichesCreableBO(final ContexteUniv ctx) {
        final AutorisationBean autorisations = ctx.getAutorisation();
        final List<Objetpartage> objetCreableBO = new ArrayList<>();
        for (final Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
            final FicheUniv fiche = FicheUnivHelper.instancierFiche(objet.getCodeObjet());
            if (autorisations.getAutorisation(objet.getCodeObjet(), AutorisationBean.INDICE_CREATION)) {
                if (fiche != null && FicheAnnotationHelper.isLienInterne(fiche) && FicheAnnotationHelper.isAccessibleBo(fiche)) {
                    objetCreableBO.add(objet);
                }
            }
        }
        return objetCreableBO;
    }

    public static List<MetatagBean> getListeMesFichesRecentes(final ContexteUniv ctx, final int combien) {
        final String codeRedacteur = (String) SessionUtil.getInfosSession(ctx.getRequeteHTTP()).get(SessionUtilisateur.CODE);
        final AutorisationBean autorisations = ctx.getAutorisation();
        final ClauseWhere whereCodeRedacteur = new ClauseWhere(ConditionHelper.egalVarchar("META_CODE_REDACTEUR", codeRedacteur));
        final ClauseOrderBy orderBy = new ClauseOrderBy("META_DATE_MODIFICATION", SensDeTri.DESC);
        final ClauseLimit limit = new ClauseLimit(combien);
        final RequeteSQL req = new RequeteSQL();
        final String codeRubrique = "";
        final ConditionList conditionSurObjets = new ConditionList();
        for (final String nomObjet : ReferentielObjets.getListeNomsObjet()) {
            final FicheUniv fiche = ReferentielObjets.instancierFiche(nomObjet);
            if (fiche != null && FicheAnnotationHelper.isAccessibleBo(fiche)) {
                fiche.setCodeRubrique(codeRubrique);
                conditionSurObjets.or(RequeteSQLHelper.traiterConditionDsiMeta(autorisations, fiche, "", codeRubrique, null));
            }
        }
        whereCodeRedacteur.and(conditionSurObjets);
        whereCodeRedacteur.and(ConditionHelper.getConditionRubPubSuivantAction(ctx, "", "T1.META_CODE_RUBRIQUE", codeRubrique));
        req.where(whereCodeRedacteur).orderBy(orderBy).limit(limit);
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        return serviceMetatag.getFromRequest(req);
    }

    public static int compterMesFiches(final ContexteUniv ctx) {
        final String codeRedacteur = (String) SessionUtil.getInfosSession(ctx.getRequeteHTTP()).get(SessionUtilisateur.CODE);
        final AutorisationBean autorisations = ctx.getAutorisation();
        final ClauseWhere whereCodeRedacteur = new ClauseWhere(ConditionHelper.egalVarchar("META_CODE_REDACTEUR", codeRedacteur));
        final ClauseOrderBy orderBy = new ClauseOrderBy("META_DATE_MODIFICATION", SensDeTri.DESC);
        final RequeteSQL req = new RequeteSQL();
        final String codeRubrique = "";
        final ConditionList conditionSurObjets = new ConditionList();
        for (final Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
            final FicheUniv fiche = ReferentielObjets.instancierFiche(objet.getNomObjet());
            if (!objet.isStrictlyCollaboratif() && fiche != null) {
                fiche.setCodeRubrique(codeRubrique);
                conditionSurObjets.or(RequeteSQLHelper.traiterConditionDsiMeta(autorisations, fiche, "", codeRubrique, null));
            }
        }
        whereCodeRedacteur.and(conditionSurObjets);
        whereCodeRedacteur.and(ConditionHelper.getConditionRubPubSuivantAction(ctx, "", "T1.META_CODE_RUBRIQUE", codeRubrique));
        req.where(whereCodeRedacteur).orderBy(orderBy);
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        return serviceMetatag.getFromRequest(req).size();
    }

    public static List<MetatagBean> getListeDernieresModifications(final ContexteUniv ctx, final int combien) {
        final AutorisationBean autorisations = ctx.getAutorisation();
        final List<MetatagBean> liste = new ArrayList<>();
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final List<MetatagBean> metatag = serviceMetatag.getMetas(combien);
        for (final MetatagBean currentMeta : metatag) {
            final String codeApprobation = StringUtils.defaultString(currentMeta.getMetaNiveauApprobation());
            if (autorisations.getAutorisationParMeta(currentMeta, AutorisationBean.INDICE_APPROBATION, codeApprobation)) {
                liste.add(currentMeta);
            }
        }
        return liste;
    }

    /**
     * @deprecated Hmm, en fait cette méthode retourne le nombre de tuple qu'il y a dans la table meta. C'est bien, mais pas top.
     */
    @Deprecated
    public static int compterDernieresModifications(final ContexteUniv ctx) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final List<MetatagBean> metas = serviceMetatag.getAllMetas();
        return metas.size();
    }
}
