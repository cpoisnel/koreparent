package com.univ.objetspartages.util;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.textsearch.RechercheFmt;

/**
 * Objet contenant les informations d'un critère de recherche Il permet d'avoir pour un critère son nom, sa valeur formater pour une requete SQL ou autre et la valeur a afficher à
 * l'utilisateur. Exemple : recherche sur le champ titre "foo bar" pour une requete en bdd on le formatera via {@link RechercheFmt#formaterTexteRecherche(String)}
 *
 * @author olivier.camon
 *
 */
public class CritereRecherche {

    private String nomChamp;

    private String valeurARechercher;

    private String valeurAAfficher;

    private boolean affichageFront;

    /**
     * Constructeur d'un critère ayant juste un nom de champ & une valeur à afficher. Il ne sera pas affiché en front office dans ce cas
     *
     * @param nom
     *            le nom du champ à rechercher
     * @param valeurARechercher
     *            la valeur à rechercher en bdd
     */
    public CritereRecherche(final String nom, final String valeurARechercher) {
        this.nomChamp = nom;
        this.valeurARechercher = valeurARechercher;
        this.setAffichageFront(StringUtils.isNotBlank(valeurAAfficher));
    }

    public CritereRecherche(final String nom, final String valeurARechercher, final String valeurAAfficher) {
        this.nomChamp = nom;
        this.valeurAAfficher = valeurAAfficher;
        this.valeurARechercher = valeurARechercher;
        this.setAffichageFront(StringUtils.isNotBlank(valeurAAfficher));
    }

    public String getNomChamp() {
        return nomChamp;
    }

    public void setNomChamp(final String nomChamp) {
        this.nomChamp = nomChamp;
    }

    public String getValeurARechercher() {
        return valeurARechercher;
    }

    public void setValeurARechercher(final String valeurARechercher) {
        this.valeurARechercher = valeurARechercher;
    }

    public String getValeurAAfficher() {
        return valeurAAfficher;
    }

    public void setValeurAAfficher(final String valeurAAfficher) {
        this.valeurAAfficher = valeurAAfficher;
    }

    public boolean isAffichageFront() {
        return affichageFront;
    }

    public void setAffichageFront(final boolean affichageFront) {
        this.affichageFront = affichageFront;
    }
}
