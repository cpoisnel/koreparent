package com.univ.objetspartages.util;

import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.cache.CacheRoleManager;
import com.univ.objetspartages.om.InfosRole;
import com.univ.objetspartages.om.Perimetre;

/**
 *
 * Classe contenant l'ensemble des méthodes traitant des {@link InfosRole} cette classe est amené à être dépréciée
 * lors de la maj de la gestion des caches du produit
 */
public class InfosRolesUtils {

    private static final Logger LOG = LoggerFactory.getLogger(InfosRolesUtils.class);

    /**
     * Renvoie la liste des rôles
     * @return la liste de tout les rôles du cache
     */
    public static Map<String, InfosRole> getListeRoles() {
        final CacheRoleManager cache = (CacheRoleManager) ApplicationContextManager.getCoreContextBean(CacheRoleManager.ID_BEAN);
        return cache.getListeRoles();
    }


    /**
     * Récupération d'un profil stocké en cache
     *
     * @param code le code du role à récupérer
     *
     * @return l'{@link InfosRole} correspondant ou un {@link InfosRole} instancié sinon (laisser pour cause de rétro compatibilité...)
     *
     */
    public static InfosRole renvoyerItemRole(final String code) {
        final Map<String, InfosRole> listeRoles = getListeRoles();
        InfosRole res = listeRoles.get(code);
        if (res == null) {
            res = new InfosRole();
        }
        return res;
    }


    /**
     * Récupération de l'intitulé : gestion de plusieurs codes.(pour retro compat...)
     * @param codes une liste de code séparé par des ";" (malin n'est ce pas?)
     * @return l'intitule si il y en a plusieurs ils sont séparés pa rdes ";" (toujours malin n'est ce pas?)
     */
    public static String getIntitules(final String codes) {
        String res = StringUtils.EMPTY;
        if (StringUtils.isEmpty(codes)) {
            return res;
        }
        for (final String code : StringUtils.split(codes, ";")) {
            if (StringUtils.isNotEmpty(res)) {
                res += ";";
            }
            String intitule = renvoyerItemRole(code).getIntitule();
            if (StringUtils.isEmpty(intitule)) {
                intitule = MessageHelper.getCoreMessage("BO_ROLE_INEXISTANT");
            }
            res += intitule;
        }
        return res;
    }

    /**
     * Récupération de l'intitulé : gestion d'un seule code...
     * @param code le code du role dont on cherche l'intitule
     * @return l'intitule du role si il est trouvé, sinon BO_ROLE_INEXISTANT. Enfin si le code est vide ou null, on retourne une chaine vide.
     */
    public static String getIntitule(final String code) {
        String res = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(code)) {
            final InfosRole infosRole = renvoyerItemRole(code);
            if (infosRole != null && StringUtils.isNotBlank(infosRole.getIntitule())) {
                res = infosRole.getIntitule();
            } else {
                res = MessageHelper.getCoreMessage("BO_ROLE_INEXISTANT");
            }
        }
        return res;
    }


    /**
     * Renvoie la liste de tous les roles dédiés aux espaces collaboratif Peut être affiché directement dans une Combo
     *
     *
     * @return the liste roles espace collaboratif
     *
     */
    public static Map<String, String> getListeRolesEspaceCollaboratif() {
        final Map<String, InfosRole> listeRoles = InfosRolesUtils.getListeRoles();
        final Map<String, String> res = new Hashtable<>();
        for (final InfosRole info : listeRoles.values()) {
            if ("*".equals(new Perimetre(info.getPerimetre()).getCodeEspaceCollaboratif())) {
                res.put(info.getCode(), info.getIntitule());
            }
        }
        return res;
    }

    /**
     * Renvoie la liste de tous les roles (hors espace collaboratif) Peut être affiché directement dans une Combo
     *
     * @return the liste roles hors espace
     *
     */
    public static Map<String, String> getListeRolesHorsEspace() {
        final Map<String, InfosRole> listeRoles = InfosRolesUtils.getListeRoles();
        final Collection<InfosRole> toutLesInfosRoles = listeRoles.values();
        final Hashtable<String, String> res = new Hashtable<>(listeRoles.size());
        for (final InfosRole info : toutLesInfosRoles) {
            if (new Perimetre(info.getPerimetre()).getCodeEspaceCollaboratif().length() == 0) {
                res.put(info.getCode(), info.getIntitule());
            }
        }
        return res;
    }

    /**
     * renvoie le périmètre d'affectation d'un role dans une liste d'affectations stockée sous la forme sous la forme [role1;per1][role2;per2]
     *
     * Cette fonction applique en plus du péimètre d'affectation le masque de périmètre du role.
     *
     * @param roles
     *            the roles
     * @param _codeRole
     *            the _code role
     * @param codesStructures
     *            the _codes structures
     * @param codeRubrique
     *            the _code rubrique
     * @param publicsVises
     *            the _publics vises
     * @param codeEspaceCollaboratif
     *            the _code espace collaboratif
     *
     * @return the vector
     */
    public static Vector<Perimetre> renvoyerPerimetresAffectation(final String roles, final String _codeRole, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif) {
        if (_codeRole.contains("RESP_COL") && roles.contains("RESP_COL")) {
            LOG.debug("renvoyerPerimetresAffection [" + _codeRole + "] dans " + roles);
        }
        final Vector<Perimetre> v = new Vector<>();
        final StringTokenizer st = new StringTokenizer(roles, "[]");
        while (st.hasMoreTokens()) {
            final String val = st.nextToken();
            final int indexPointVirgule = val.indexOf(';');
            if (indexPointVirgule != -1) {
                final String codeRole = val.substring(0, indexPointVirgule);
                // On controle que le role affecté correspond
                // au role recherché
                if (codeRole.equals(_codeRole)) {
                    final String sPerimetre = val.substring(indexPointVirgule + 1);
                    Perimetre perimetre = new Perimetre(sPerimetre);
                    // Ajout du masque associé au role
                    final InfosRole infos = InfosRolesUtils.renvoyerItemRole(_codeRole);
                    if (infos.getCode().length() > 0) {
                        perimetre = Perimetre.calculerPerimetrePermission(perimetre, new Perimetre(infos.getPerimetre()));
                    } else {
                        LOG.warn("renvoyerPerimetresAffection() role " + _codeRole + " affecté mais non défini");
                    }
                    // Recherche si le role s'applique au périmètre
                    final StringTokenizer stPublics = new StringTokenizer(publicsVises, "[]");
                    if (stPublics.countTokens() > 0) {
                        while (stPublics.hasMoreTokens()) {
                            final String publicVise = stPublics.nextToken();
                            String profil = "";
                            String groupe = "";
                            /* analyse du périmètre d'affectation du role pour
                             * chaque public visé recherché */
                            final int indiceSeparateur = publicVise.indexOf("/");
                            if (indiceSeparateur != -1) {
                                profil = publicVise.substring(0, indiceSeparateur);
                                groupe = publicVise.substring(indiceSeparateur + 1);
                            }
                            for (final String codesStructure : codesStructures) {
                                final Perimetre perimetreRecherche = new Perimetre(codesStructure, codeRubrique, profil, groupe, codeEspaceCollaboratif);
                                if (perimetre != null && perimetreRecherche.estConformeAuPerimetre(perimetre)) {
                                    v.add(perimetre);
                                }
                            }
                        }
                    } else {
                        // pas de public
                        for (final String codesStructure : codesStructures) {
                            final Perimetre perimetreRecherche = new Perimetre(codesStructure, codeRubrique, "", "", codeEspaceCollaboratif);
                            if (perimetre != null && perimetreRecherche.estConformeAuPerimetre(perimetre)) {
                                v.add(perimetre);
                            }
                        }
                    }
                }
            }
        }
        return v;
    }
}
