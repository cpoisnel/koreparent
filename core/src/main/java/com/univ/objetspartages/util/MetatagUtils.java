package com.univ.objetspartages.util;

import java.util.Hashtable;
import java.util.List;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.utils.Chaine;

/**
 * Created on 16/04/15.
 */
public class MetatagUtils {

    /**
     * The HISTORIQU e_ annulatio n_ demand e_ validation.
     */
    public static final String HISTORIQUE_ANNULATION_DEMANDE_VALIDATION = "ANNULATION_DEMANDE_VALIDATION";

    /**
     * The HISTORIQU e_ annulatio n_ suppression.
     */
    public static final String HISTORIQUE_ANNULATION_SUPPRESSION = "ANNULATION_SUPPRESSION";

    /**
     * The HISTORIQU e_ archivage.
     */
    public static final String HISTORIQUE_ARCHIVAGE = "ARCHIVAGE";

    /**
     * The HISTORIQU e_ archivage.
     */
    public static final String HISTORIQUE_ARCHIVAGE_AUTO = "ARCHIVAGE_AUTO";

    /**
     * The HISTORIQU e_ rubriquage.
     */
    public static final String HISTORIQUE_RUBRIQUAGE = "RUBRIQUAGE";

    /**
     * The HISTORIQU e_ rubriquage.
     */
    public static final String HISTORIQUE_RUBRIQUAGE_AUTO = "RUBRIQUAGE_AUTO";

    /**
     * The HISTORIQU e_ creation.
     */
    public static final String HISTORIQUE_CREATION = "CREATION";

    /**
     * The HISTORIQU e_ demand e_ validation.
     */
    public static final String HISTORIQUE_DEMANDE_VALIDATION = "DEMANDE_VALIDATION";

    /**
     * The HISTORIQU e_ duplication.
     */
    public static final String HISTORIQUE_DUPLICATION = "DUPLICATION";

    /**
     * The HISTORIQU e_ traduction.
     */
    public static final String HISTORIQUE_TRADUCTION = "TRADUCTION";

    /**
     * The HISTORIQU e_ modification.
     */
    public static final String HISTORIQUE_MODIFICATION = "MODIFICATION";

    /**
     * The HISTORIQU e_ restauratio n_ archivage.
     */
    public static final String HISTORIQUE_RESTAURATION_ARCHIVAGE = "RESTAURATION_ARCHIVAGE";

    /**
     * The HISTORIQU e_ restauratio n_ sauvegarde.
     */
    public static final String HISTORIQUE_RESTAURATION_SAUVEGARDE = "RESTAURATION_SAUVEGARDE";

    /**
     * The HISTORIQU e_ retou r_ auteur.
     */
    public static final String HISTORIQUE_RETOUR_AUTEUR = "RETOUR_AUTEUR";

    /**
     * The HISTORIQU e_ suppression.
     */
    public static final String HISTORIQUE_SUPPRESSION = "SUPPRESSION";

    /**
     * The HISTORIQU e_ validation.
     */
    public static final String HISTORIQUE_VALIDATION = "VALIDATION";

    // FIXME : garde-fou pour garder la rétro-compatibilité. A supprimer dans la prochaine version.
    // On tape systématiquement sur le contexte pour éviter les problèmes en case de rechargement (la propriété devrait être static...).

    /**
     * Permet de récupérer le service des metatags.
     * @deprecated : utiliser le {@link com.kosmos.service.impl.ServiceManager} pour récupérer le service.
     * @return le service metatag
     */
    @Deprecated
    public static ServiceMetatag getMetatagService() {
        return ServiceManager.getServiceForBean(MetatagBean.class);
    }

    /**
     * Renvoie un libellé d'action (à partir du code stocké dans l'historique).
     *
     * @param action the action
     * @return the intitule action
     */
    public static String getIntituleAction(final String action) {
        String res = "";
        final Hashtable<String, String> actions = new Hashtable<>();
        actions.put(HISTORIQUE_SUPPRESSION, "Suppression");
        actions.put(HISTORIQUE_ANNULATION_SUPPRESSION, "Annulation suppression");
        actions.put(HISTORIQUE_RESTAURATION_SAUVEGARDE, "Restauration");
        actions.put(HISTORIQUE_DUPLICATION, "Duplication");
        actions.put(HISTORIQUE_TRADUCTION, "Traduction");
        actions.put(HISTORIQUE_CREATION, "Création");
        actions.put(HISTORIQUE_MODIFICATION, "Modification");
        actions.put(HISTORIQUE_VALIDATION, "Validation");
        actions.put(HISTORIQUE_DEMANDE_VALIDATION, "Demande de validation");
        actions.put(HISTORIQUE_ANNULATION_DEMANDE_VALIDATION, "Annulation demande ");
        actions.put(HISTORIQUE_RETOUR_AUTEUR, "Retour au rédacteur");
        actions.put(HISTORIQUE_RUBRIQUAGE, "Rubriquage ");
        actions.put(HISTORIQUE_RUBRIQUAGE_AUTO, "Rubriquage automatique ");
        actions.put(HISTORIQUE_ARCHIVAGE, "Archivage ");
        actions.put(HISTORIQUE_ARCHIVAGE_AUTO, "Archivage automatique ");
        actions.put(HISTORIQUE_RESTAURATION_ARCHIVAGE, "Restauration archive");
        if (actions.get(action) != null) {
            res = actions.get(action);
        }
        return res;
    }

    public static MetatagBean lireMeta(final FicheUniv ficheUniv) {
        MetatagBean result = null;
        if (ficheUniv != null) {
            final String codeObjet = ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName());
            result = lireMeta(codeObjet, ficheUniv.getIdFiche());
        }
        return result;
    }

    public static MetatagBean lireMeta(final String code_objet, final Long idFiche) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        MetatagBean meta = serviceMetatag.getByCodeAndIdFiche(code_objet, idFiche);
        if (meta == null) {
            meta = new MetatagBean();
            meta.setMetaCodeObjet(code_objet);
            meta.setMetaLibelleObjet(ReferentielObjets.getLibelleObjet(meta.getMetaCodeObjet()));
            meta.setMetaIdFiche(idFiche);
            meta.setMetaDiffusionModeRestriction("0");
        }
        return meta;
    }

    public static MetatagBean initMeta(FicheUniv ficheUniv){
        if (ficheUniv instanceof AbstractFicheBean) {
            return initMeta((AbstractFicheBean) ficheUniv, ficheUniv.getLibelleAffichable(), ficheUniv.getReferences(), ReferentielObjets.getCodeObjet(ficheUniv));
        } else {
            return initMetaLegacy(ficheUniv);
        }
    }

    /**
     * creation d'un métatag.
     *
     * @param ficheUniv fiche dont on veut créer le méta Cette méthode est appelée uniquement dans le scansite en cas de perte de meta données ce qui est normalement impossible sauf en
     *                  cas de crash de la bd !!!
     */
    public static MetatagBean creerMeta(final FicheUniv ficheUniv) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        MetatagBean meta = initMeta(ficheUniv);
        serviceMetatag.save(meta);
        return meta;
    }

    private static MetatagBean initMetaLegacy(final FicheUniv ficheUniv) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final MetatagBean meta = new MetatagBean();
        meta.setMetaIdFiche(ficheUniv.getIdFiche());
        meta.setMetaCodeObjet(ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName()));
        serviceMetatag.calculerReferences(meta, ficheUniv.getReferences(), ficheUniv.getContenuEncadre(), ficheUniv.getLangue());
        meta.setMetaLibelleObjet(ReferentielObjets.getLibelleObjet(meta.getMetaCodeObjet()));
        meta.setMetaCodeRattachement(ficheUniv.getCodeRattachement());
        meta.setMetaCodeRubrique(ficheUniv.getCodeRubrique());
        if (ficheUniv instanceof FicheRattachementsSecondaires) {
            meta.setMetaCodeRattachementAutres(Chaine.convertirPointsVirgulesEnAccolades(((FicheRattachementsSecondaires) ficheUniv).getCodeRattachementAutres()));
        }
        meta.setMetaLibelleObjet(ReferentielObjets.getLibelleObjet(meta.getMetaCodeObjet()));
        meta.setMetaLibelleFiche(ficheUniv.getLibelleAffichable());
        meta.setMetaKeywords(ficheUniv.getMetaKeywords());
        meta.setMetaDescription(ficheUniv.getMetaDescription());
        meta.setMetaDateCreation(ficheUniv.getDateCreation());
        meta.setMetaDateProposition(ficheUniv.getDateProposition());
        meta.setMetaDateValidation(ficheUniv.getDateValidation());
        meta.setMetaDateMiseEnLigne(ficheUniv.getDateCreation());
        meta.setMetaDateModification(ficheUniv.getDateModification());
        meta.setMetaCodeRedacteur(ficheUniv.getCodeRedacteur());
        meta.setMetaCodeValidation(ficheUniv.getCodeValidation());
        meta.setMetaCode(ficheUniv.getCode());
        meta.setMetaLangue(ficheUniv.getLangue());
        meta.setMetaEtatObjet(ficheUniv.getEtatObjet());
        meta.setMetaNbHits(ficheUniv.getNbHits());
        if (ficheUniv instanceof DiffusionSelective) {
            meta.setMetaDiffusionPublicVise(((DiffusionSelective) ficheUniv).getDiffusionPublicVise());
            meta.setMetaDiffusionModeRestriction(((DiffusionSelective) ficheUniv).getDiffusionModeRestriction());
            meta.setMetaDiffusionPublicViseRestriction(((DiffusionSelective) ficheUniv).getDiffusionPublicViseRestriction());
        }
        return meta;
    }
    /**
     * creation d'un métatag.
     *
     * @param ficheBean fiche dont on veut créer le méta Cette méthode est appelée uniquement dans le scansite en cas de perte de meta données ce qui est normalement impossible sauf en
     *                  cas de crash de la bd !!!
     */
    private static MetatagBean initMeta(final AbstractFicheBean ficheBean, final String libelleAffichable, final String reference, final String codeObjet) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final MetatagBean meta = new MetatagBean();
        meta.setMetaIdFiche(ficheBean.getId());
        meta.setMetaCodeObjet(codeObjet);
        serviceMetatag.calculerReferences(meta, reference, ficheBean.getContenuEncadre(), ficheBean.getLangue());
        meta.setMetaLibelleObjet(ReferentielObjets.getLibelleObjet(meta.getMetaCodeObjet()));
        meta.setMetaCodeRattachement(ficheBean.getCodeRattachement());
        meta.setMetaCodeRubrique(ficheBean.getCodeRubrique());
        if (ficheBean instanceof FicheRattachementsSecondaires) {
            meta.setMetaCodeRattachementAutres(Chaine.convertirPointsVirgulesEnAccolades(((FicheRattachementsSecondaires) ficheBean).getCodeRattachementAutres()));
        }
        meta.setMetaLibelleFiche(libelleAffichable);
        meta.setMetaKeywords(ficheBean.getMetaKeywords());
        meta.setMetaDescription(ficheBean.getMetaDescription());
        meta.setMetaDateCreation(ficheBean.getDateCreation());
        meta.setMetaDateProposition(ficheBean.getDateProposition());
        meta.setMetaDateValidation(ficheBean.getDateValidation());
        meta.setMetaDateMiseEnLigne(ficheBean.getDateCreation());
        meta.setMetaDateModification(ficheBean.getDateModification());
        meta.setMetaCodeRedacteur(ficheBean.getCodeRedacteur());
        meta.setMetaCodeValidation(ficheBean.getCodeValidation());
        meta.setMetaCode(ficheBean.getCode());
        meta.setMetaLangue(ficheBean.getLangue());
        meta.setMetaEtatObjet(ficheBean.getEtatObjet());
        meta.setMetaNbHits(ficheBean.getNbHits());
        if (ficheBean instanceof DiffusionSelective) {
            meta.setMetaDiffusionPublicVise(((DiffusionSelective) ficheBean).getDiffusionPublicVise());
            meta.setMetaDiffusionModeRestriction(((DiffusionSelective) ficheBean).getDiffusionModeRestriction());
            meta.setMetaDiffusionPublicViseRestriction(((DiffusionSelective) ficheBean).getDiffusionPublicViseRestriction());
        }
        return meta;
    }

    public static MetatagBean supprimerMeta(final FicheUniv ficheUniv) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final MetatagBean meta = serviceMetatag.getByCodeAndIdFiche(ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName()), ficheUniv.getIdFiche());
        if (meta != null) {
            serviceMetatag.delete(meta.getId());
        }
        return meta;
    }

    /**
     * Synchronisation des méta-tags Traitement sur les fiches.
     *
     * @param ficheUniv     the fiche univ
     * @param majReferences the maj references
     * @return true, if synchroniser meta tag
     */
    public static MetatagBean synchroniserMetaTag(final FicheUniv ficheUniv, final String majReferences) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
        // Lecture métatag
        MetatagBean meta = serviceMetatag.getByCodeAndIdFiche(codeObjet, ficheUniv.getIdFiche());
        if (meta != null) {
            boolean update = false;
            // La mise à jour des références croisées est en option
            if ("1".equals(majReferences)) {
                final String references = meta.getMetaListeReferences();
                serviceMetatag.calculerReferences(meta, ficheUniv.getReferences(), ficheUniv.getContenuEncadre(), ficheUniv.getLangue());
                if (!references.equals(meta.getMetaListeReferences())) {
                    update = true;
                }
            }
            // Synchronisation du méta avec sa fiche
            final List<String> differences = serviceMetatag.controlerCoherenceAvecFiche(meta, ficheUniv);
            if (!differences.isEmpty()) {
                serviceMetatag.synchroniser(meta, ficheUniv, false);
                update = true;
            }
            if (update) {
                serviceMetatag.save(meta);
            }
        } else {
            meta = creerMeta(ficheUniv);
        }
        return meta;
    }
}
