package com.univ.objetspartages.util;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.om.MediaPhoto;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.utils.FileUtil;

public class RessourceUtils {

    /** The Constant PATH_SERVLET_LECTURE_FICHIER. */
    public static final String PATH_SERVLET_LECTURE_FICHIER = "/servlet/com.univ.collaboratif.utils.LectureFichiergw";

    /** The Constant FICHIERGW_PATH_PROPERTIES_KEY. */
    private static final String FICHIERGW_PATH_PROPERTIES_KEY = "fichiergw.path";

    private static final String DEFAULT_RESSOURCE_PATH = "fichiergw";

    public static String getAbsolutePath() {
        return StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(FICHIERGW_PATH_PROPERTIES_KEY), WebAppUtil.getStorageDir() + DEFAULT_RESSOURCE_PATH);
    }

    public static String getDefaultRelativePath() {
        return DEFAULT_RESSOURCE_PATH;
    }

    private static ServiceMedia getServiceMedia(){
        return ServiceManager.getServiceForBean(MediaBean.class);
    }

    /**
     * Gets the path absolu.
     *
     * @return le path physique absolu du fichier
     */
    public static String getPathAbsolu(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if (mediaBean != null) {
            return MediaUtils.getPathAbsolu(mediaBean);
        }
        return StringUtils.EMPTY;
    }

    public static String getUrl(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if (mediaBean != null) {
            return MediaUtils.getUrlAbsolue(mediaBean);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the url photo.
     *
     * @return l'url de la photo
     *
     */
    public static String getUrlPhoto(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if (MediaUtils.isPhoto(mediaBean)) {
            return MediaUtils.getUrlAbsolue(mediaBean);
        }
        return StringUtils.EMPTY;
    }

    public static String getLegende(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getLegende();
        }
        return StringUtils.EMPTY;
    }

    public static String getUrlVignette(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return MediaUtils.getUrlVignetteAbsolue(mediaBean);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the source.
     *
     * @return the source
     */
    public static String getSource(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getSource();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the titre.
     *
     * @return the titre
     */
    public static String getTitre(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getTitre();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the auteur.
     *
     * @return the auteur
     */
    public static String getAuteur(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getAuteur();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the code redacteur.
     *
     * @return the code redacteur
     */
    public static String getCodeRedacteur(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getCodeRedacteur();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the copyright.
     *
     * @return the copyright
     */
    public static String getCopyright(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getCopyright();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the date creation.
     *
     * @return the date creation
     */
    public static Date getDateCreation(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getDateCreation();
        }
        return null;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public static String getDescription(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getDescription();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the format.
     *
     * @return the format
     */
    public static String getFormat(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getFormat();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the meta keywords.
     *
     * @return the meta keywords
     */
    public static String getMetaKeywords(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getMetaKeywords();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the poids.
     *
     * @return the poids
     */
    public static Long getPoids(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getPoids();
        }
        return -1L;
    }

    /**
     * Gets the specific data.
     *
     * @return the specific data
     */
    public static String getSpecificData(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getSpecificData();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets width attribute contained in the specific data field
     * @param ressourceBean : the ressource as a {@link com.univ.objetspartages.bean.RessourceBean}
     * @return the width if valid, -1 orherwise
     */
    public static int getLargeurPhoto(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            final String width = getServiceMedia().getSpecificData(mediaBean, MediaPhoto.ATTRIBUT_LARGEUR);
            if(StringUtils.isNumeric(width)) {
                return Integer.parseInt(width);
            }
        }
        return -1;
    }

    /**
     * Gets width attribute contained in the specific data field
     * @param ressourceBean : the ressource as a {@link com.univ.objetspartages.bean.RessourceBean}
     * @return the height if valid, -1 orherwise
     */
    public static int getHauteurPhoto(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            final String height = getServiceMedia().getSpecificData(mediaBean, MediaPhoto.ATTRIBUT_HAUTEUR);
            if(StringUtils.isNumeric(height)) {
                return Integer.parseInt(height);
            }
        }
        return -1;
    }

    /**
     * Gets width attribute contained in the specific data field
     * @param ressourceBean : the ressource as a {@link com.univ.objetspartages.bean.RessourceBean}
     * @param defaultValue : the value to return if the width cannot be retrieved
     * @return the width if valid, -1 orherwise
     */
    public static int getLargeurPhoto(final RessourceBean ressourceBean, final int defaultValue) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            final String width = getServiceMedia().getSpecificData(mediaBean, MediaPhoto.ATTRIBUT_LARGEUR);
            if(StringUtils.isNumeric(width)) {
                return Integer.parseInt(width);
            }
        }
        return defaultValue;
    }

    /**
     * Gets width attribute contained in the specific data field
     * @param ressourceBean : the ressource as a {@link com.univ.objetspartages.bean.RessourceBean}
     * @param defaultValue : the value to return if the width cannot be retrieved
     * @return the height if valid, -1 orherwise
     */
    public static int getHauteurPhoto(final RessourceBean ressourceBean, final int defaultValue) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            final String height = getServiceMedia().getSpecificData(mediaBean, MediaPhoto.ATTRIBUT_HAUTEUR);
            if(StringUtils.isNumeric(height)) {
                return Integer.parseInt(height);
            }
        }
        return defaultValue;
    }

    /**
     * Checks if is securise.
     *
     * @return true, if is securise
     */
    public static boolean isSecurise(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        return mediaBean != null && !MediaUtils.isPublic(mediaBean);
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public static String getLibelle(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        return MediaUtils.getLibelleAffichable(mediaBean);
    }

    /**
     * Gets the type media.
     *
     * @return the type media
     */
    public static String getTypeMedia(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getTypeMedia();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Gets the type ressource.
     *
     * @return the type ressource
     */
    public static String getTypeRessource(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        if(mediaBean != null) {
            return mediaBean.getTypeRessource();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Retourne si possible l'extension de la ressource
     *
     * @return l'extension de la ressource, si le nom de la ressource se termine par un . ou un -
     */
    public static String getExtension(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        return FileUtil.getExtension(mediaBean.getSource());
    }

    public static boolean isLocal(final RessourceBean ressourceBean) {
        final MediaBean mediaBean = getServiceMedia().getById(ressourceBean.getIdMedia());
        return MediaUtils.isLocal(mediaBean);
    }
}
