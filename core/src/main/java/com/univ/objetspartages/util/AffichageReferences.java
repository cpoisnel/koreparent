package com.univ.objetspartages.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.ReferenceBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * processus d'affichage des références croisées.
 */
public class AffichageReferences {

    private static final Logger LOG = LoggerFactory.getLogger(AffichageReferences.class);

    public static List<ReferenceBean> getListeReferences(final OMContext ctx, final InfoBean infoBean) throws Exception {
        final List<ReferenceBean> referencesDispo = new ArrayList<>();
        final String codeObjet = infoBean.getString("CODE_OBJET");
        final String codeFiche = infoBean.getString("CODE");
        final String langue = infoBean.getString("LANGUE");
        if (StringUtils.isNotBlank(codeObjet)) {
            final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
            final List<MetatagBean> metas = serviceMetatag.getByReferences(codeObjet, codeFiche, langue, (long) 0);
            for (final MetatagBean meta : metas) {
                final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(meta.getMetaCodeObjet()));
                ficheUniv.setCtx(ctx);
                ficheUniv.init();
                ficheUniv.setIdFiche(meta.getMetaIdFiche());
                try {
                    ficheUniv.retrieve();
                    if (EtatFiche.isEtatPerene(ficheUniv.getEtatObjet())) {
                        final ReferenceBean reference = new ReferenceBean();
                        reference.setTitre(ficheUniv.getLibelleAffichable());
                        reference.setLangue(ficheUniv.getLangue());
                        reference.setType(ReferentielObjets.getLibelleObjet(meta.getMetaCodeObjet()));
                        reference.setEtat(EtatFiche.getEtatParCode(ficheUniv.getEtatObjet()));
                        referencesDispo.add(reference);
                    }
                } catch (final Exception e) {
                    LOG.info("impossible de récuperer la fiche depuis les metas", e);
                }
            }
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final List<RubriqueBean> results = serviceRubrique.getRubriqueByReferences(codeFiche, langue, ReferentielObjets.getNomObjet(codeObjet));
            for (RubriqueBean rub : results) {
                String titre = rub.getIntitule();
                RubriqueBean rubAriane = serviceRubrique.getRubriqueByCode(rub.getCodeRubriqueMere());
                while (rubAriane != null && StringUtils.isNotBlank(rubAriane.getNomOnglet())) {
                    titre = rubAriane.getNomOnglet() + " &gt; " + titre;
                    rubAriane = serviceRubrique.getRubriqueByCode(rubAriane.getCodeRubriqueMere());
                }
                final ReferenceBean reference = new ReferenceBean();
                reference.setTitre(titre);
                reference.setType("Rubrique");
                reference.setEtat(EtatFiche.EN_LIGNE);
                reference.setLangue(rub.getLangue());
                referencesDispo.add(reference);
            }
        }
        return referencesDispo;
    }
}
