package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.univ.datagrid.processus.FicheToolboxDatagrid;
import com.univ.datagrid.processus.FicheUnivDatagrid;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.RechercheFicheHelper;

import static com.univ.objetspartages.util.CritereRechercheUtil.getCritereTexteNonVide;

public abstract class AbstractRechercheExterne implements RechercheExterne {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRechercheExterne.class);

    private final ServiceUser serviceUser;

    public AbstractRechercheExterne() {
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Ajoute les paramètres commun à toutes les fiches
     *
     * @param infoBean
     * @param requete
     * @return
     * @Deprecated ancienne façon de faire, la nouvelle méthode doit utiliser getCritereDefaut
     */
    @Deprecated
    public String ajouterCritereDefaut(final InfoBean infoBean, String requete) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "CODE");
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "ETAT_OBJET", Boolean.TRUE, Boolean.FALSE);
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "LANGUE");
        requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "IDS");
        final String codeRedacteur = infoBean.getString("CODE_REDACTEUR");
        if (StringUtils.isNotBlank(codeRedacteur)) {
            requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "CODE_REDACTEUR");
            final UtilisateurBean user = serviceUser.getByCode(codeRedacteur);
            if (user != null) {
                final String libelleRedacteur = String.format("%s %s", user.getPrenom(), user.getNom());
                if (StringUtils.isNotBlank(libelleRedacteur)) {
                    requete += "&LIBELLE_CODE_REDACTEUR=" + libelleRedacteur;
                }
            } else {
                LOG.debug("impossible de récuperer le libelle rédacteur ayant pour code " + codeRedacteur + " dans la recherche");
            }
        }
        final String codeStructure = infoBean.getString("CODE_RATTACHEMENT");
        if (StringUtils.isNotBlank(codeStructure)) {
            requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "CODE_RATTACHEMENT");
            final String libelleStructure = serviceStructure.getDisplayableLabel(codeStructure, LangueUtil.getLangueLocale(ctx.getLocale()));
            if (StringUtils.isNotBlank(libelleStructure)) {
                requete += "&LIBELLE_CODE_RATTACHEMENT=" + libelleStructure;
            } else {
                LOG.debug("impossible de récuperer le libelle structure de code " + codeStructure + " dans la recherche");
            }
        }
        final String codeRubrique = infoBean.getString("CODE_RUBRIQUE");
        if (StringUtils.isNotBlank(codeRubrique)) {
            requete = RechercheFicheHelper.ajouterCritereRequete(infoBean.getValues(), requete, "CODE_RUBRIQUE");
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (rubriqueBean != null && StringUtils.isNotBlank(rubriqueBean.getIntitule())) {
                requete += "&LIBELLE_CODE_RUBRIQUE=" + rubriqueBean.getIntitule();
            }
        }
        String idBean = FicheUnivDatagrid.ID_BEAN;
        if (infoBean.get("TOOLBOX") != null) {
            idBean = FicheToolboxDatagrid.ID_BEAN;
        }
        requete += "&" + DatagridUtils.PARAM_BEAN_DATAGRID + "=" + idBean;
        return requete;
    }

    public List<CritereRecherche> getCritereDefaut(final InfoBean infoBean) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final List<CritereRecherche> criteres = new ArrayList<>();
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        CollectionUtils.addIgnoreNull(criteres, getCritereTexteNonVide(infoBean, "CODE"));
        final String etatObjet = infoBean.getString("ETAT_OBJET");
        if (StringUtils.isNotBlank(etatObjet)) {
            criteres.add(new CritereRecherche("ETAT_OBJET", etatObjet, MessageHelper.getCoreMessage("ETATFICHE_" + etatObjet)));
        }
        final String langue = infoBean.getString("LANGUE");
        if (StringUtils.isNotBlank(langue) && !"0000".equals(langue)) {
            criteres.add(new CritereRecherche("LANGUE", langue, LangueUtil.getLocale(langue).getDisplayLanguage(ctx.getLocale())));
        }
        final String idsFil = infoBean.getString("IDS");
        final String valeurFil = infoBean.getString("FIL");
        if (StringUtils.isNotBlank(idsFil) && !"0000".equals(idsFil) && StringUtils.isNotBlank(valeurFil)) {
            criteres.add(new CritereRecherche("IDS", idsFil));
            final String libelleFil = infoBean.getString("LIBELLE_FIL");
            if (StringUtils.isNotBlank(libelleFil)) {
                criteres.add(new CritereRecherche("FIL", valeurFil, libelleFil));
            }
        }
        final String codeRedacteur = infoBean.getString("CODE_REDACTEUR");
        if (StringUtils.isNotBlank(codeRedacteur)) {
            final UtilisateurBean user = serviceUser.getByCode(codeRedacteur);
            if (user != null) {
                final String libelleRedacteur = String.format("%s %s", user.getPrenom(), user.getNom());
                criteres.add(new CritereRecherche("CODE_REDACTEUR", codeRedacteur, libelleRedacteur));
                criteres.add(new CritereRecherche("LIBELLE_CODE_REDACTEUR", libelleRedacteur));
            } else {
                LOG.debug("impossible de récuperer le libelle rédacteur de code " + codeRedacteur + " dans la recherche");
            }
        }
        final String codeStructure = infoBean.getString("CODE_RATTACHEMENT");
        if (StringUtils.isNotBlank(codeStructure)) {
            final StructureModele structure = serviceStructure.getByCodeLanguage(codeStructure, LangueUtil.getLangueLocale(ctx.getLocale()));
            if (structure != null) {
                final String libelleStructure = structure.getLibelleLong();
                criteres.add(new CritereRecherche("CODE_RATTACHEMENT", codeStructure, libelleStructure));
                criteres.add(new CritereRecherche("LIBELLE_CODE_RATTACHEMENT", libelleStructure));
            } else {
                LOG.debug("impossible de récuperer le libelle structure de code " + codeStructure + " dans la recherche");
            }
        }
        final String codeRubrique = infoBean.getString("CODE_RUBRIQUE");
        if (StringUtils.isNotBlank(codeRubrique)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (rubriqueBean != null) {
                criteres.add(new CritereRecherche("CODE_RUBRIQUE", codeRubrique, rubriqueBean.getIntitule()));
                criteres.add(new CritereRecherche("LIBELLE_CODE_RUBRIQUE", rubriqueBean.getIntitule()));
            }
        }
        String idBean = FicheUnivDatagrid.ID_BEAN;
        if (infoBean.get("TOOLBOX") != null) {
            idBean = FicheToolboxDatagrid.ID_BEAN;
        }
        criteres.add(new CritereRecherche(DatagridUtils.PARAM_BEAN_DATAGRID, idBean));
        return criteres;
    }

    /**
     * Permet de remettre la requete dans l'infoBean comme avant histoire que les front 5.1 fonctionnent correctement
     *
     * @param infoBean
     * @param criteres
     */
    @SuppressWarnings({"unchecked", "deprecation"})
    protected void gestionAncienFront(final InfoBean infoBean, final List<CritereRecherche> criteres) {
        final Collection<String> requete = CollectionUtils.collect(criteres, new Transformer() {

            @Override
            public Object transform(final Object input) {
                final CritereRecherche critere = (CritereRecherche) input;
                return critere.getNomChamp() + "=" + critere.getValeurARechercher();
            }
        });
        infoBean.set(RechercheFicheHelper.ATTRIBUT_INFOBEAN_REQUETE, StringUtils.join(requete, "&"));
    }
}
