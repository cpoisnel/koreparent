package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cache.CacheUtil;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.core.config.MessageHelper;
import com.univ.mediatheque.UsingLibelleMedia;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.dao.impl.RubriqueDAO;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.url.CacheUrlManager;
import com.univ.url.CacheUrlRubrique;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.ExceptionFicheNonTrouvee;
import com.univ.utils.UnivWebFmt;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.xhtml.JavascriptTreeRubrique;

/**
 * La classe de gestion des rubriques.
 * @deprecated cette classe contenait à la fois des méthodes utilitaires, des méthodes de services et des méthodes d'un DAO.
 * Elle a été séparée en plusieurs classes : {@link ServiceRubrique} se chargeant de l'accès au données et quelque règles métiers.
 *
 */
@Deprecated
public class Rubrique extends AbstractOm<RubriqueBean, RubriqueDAO> implements UsingLibelleMedia {

    /** The Constant CODE_RUBRIQUE_ROOT. */
    public final static String CODE_RUBRIQUE_ROOT = "00";

    /** The Constant CODE_RUBRIQUE_INEXISTANTE. */
    public final static String CODE_RUBRIQUE_INEXISTANTE = "ZYZYZYZYZYZYZYZYZYZYZY";

    /**
     * Forcage du rechargement des rubriques en mémoire.
     *
     * @throws Exception
     *             the exception
     * @deprecated cette néthode ne recharge plus les caches
     */
    @Deprecated
    public static void rechargement() {
        // nothing to reload
    }

    /**
     * Méthode qui permet de tester le périmètre de modification d'un objet sur une rubrique : si le droit en modification renvoit faux on va regarder le droit en création.
     *
     * @param autorisations
     *            the autorisations
     * @param sPermission
     *            the s permission
     * @param codeRubrique
     *            the code rubrique
     *
     * @return true, if controler permission
     *
     */
    public static boolean controlerPermission(final AutorisationBean autorisations, final String sPermission, final String codeRubrique) {
        return PermissionUtil.controlerPermissionRubrique(autorisations, sPermission, codeRubrique);
    }

    /**
     * Affichage dynamique de l'arbre javascript des rubriques.
     *
     * @param _code
     *            the code
     * @param autorisations
     *            the autorisations
     * @param _sPermission
     *            the _s permission
     *
     * @return the arbre java script
     *
     * @throws Exception
     *             the exception
     */
    public static String getArbreJavaScript(final String _code, final AutorisationBean autorisations, final String _sPermission) throws Exception {
        return getArbreJavaScript(_code, autorisations, _sPermission, ServiceRubrique.CODE_RUBRIQUE_ROOT);
    }

    /**
     * FG 20060712 Affichage dynamique de l'arbre javascript des rubriques pour le front office Le paramètre codeRacine permet de définir la rubrique de plus haut niveau de
     * l'arbre Permet de restreindre l'arbre à une sous-arborescence (ex: sous site).
     *
     * @param code
     *            the code
     * @param autorisations
     *            the autorisations
     * @param sPermission
     *            the _s permission
     * @param codeRacine
     *            the code racine
     *
     * @return the arbre java script
     *
     * @throws Exception
     *             the exception
     */
    public static String getArbreJavaScript(String code, final AutorisationBean autorisations, final String sPermission, String codeRacine) throws Exception {
        if (StringUtils.isEmpty(codeRacine)) {
            codeRacine = ServiceRubrique.CODE_RUBRIQUE_ROOT;
        }
        if (StringUtils.isEmpty(code)) {
            code = codeRacine;
        }
        // teste si la rubrique de sélection est une sous-rubrique de la rubrique racine
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean racine = serviceRubrique.getRubriqueByCode(codeRacine);
        final RubriqueBean rubriqueCourante = serviceRubrique.getRubriqueByCode(code);
        if (!codeRacine.equals(ServiceRubrique.CODE_RUBRIQUE_ROOT) && !codeRacine.equals(code) && serviceRubrique.isParentSection(racine, rubriqueCourante)) {
            codeRacine = ServiceRubrique.CODE_RUBRIQUE_ROOT;
        }
        final JavascriptTreeRubrique tree = new JavascriptTreeRubrique(autorisations, sPermission);
        InfosRubriques infosRacines = new InfosRubriques("");
        InfosRubriques infosCourante = new InfosRubriques("");
        if (racine != null) {
            infosRacines = new InfosRubriques(racine);
        }
        if (rubriqueCourante != null) {
            infosCourante = new InfosRubriques(rubriqueCourante);
        }
        tree.load(infosRacines, infosCourante);
        return tree.print();
    }

    /**
     * Récupération de l'intitulé.
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the code
     * @param inclureRubriqueCourante
     *            the _inclure rubrique courante
     *
     * @return the intitule
     *
     * @deprecated le contexte et le booleen ne servent à rien...
     */
    @Deprecated
    public static String getIntitule(final OMContext ctx, final String code, final boolean inclureRubriqueCourante) {
        return getIntitule(code);
    }

    /**
     * Nouvelle méthode avec juste les paramètres dont on a besoin...
     *
     * @param codeRubrique
     *            le code de la rubrique dont on veut l'intitulé
     * @return l'intitulé de la rubrique ou vide si elle n'existe pas
     * @throws Exception
     *             possible sur la récupération de la rubrique
     */
    public static String getIntitule(final String codeRubrique) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
        String intitule = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
        if (rubriqueBean != null) {
            intitule = rubriqueBean.getIntitule();
        }
        return intitule;
    }

    /**
     * Récupération de l'intitulé complet (Concaténation mere + fille 1 niveau + ...)
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the code
     * @param inclureRubriqueCourante
     *            the _inclure rubrique courante
     *
     * @return the intitule complet
     * @deprecated utiliser {@link ServiceRubrique#getLabelWithAscendantsLabels(String)} si inclureRubriqueCourante = true
     * et {@link ServiceRubrique#getAscendantsLabelOnly(String)} sinon. Attention, à la difference de cette méthode, le service ne renvera que les intitules des rubriques. Cette
     * méthode renvoie pour la rubrique courante son intitulé puis la valeur du champ "nomOnglet". Si ceux comportement est réelement voulu, il faut utiliser {@link ServiceRubrique#getAllAscendant(String)}
     */
    @Deprecated
    public static String getIntituleComplet(final OMContext ctx, final String code, final boolean inclureRubriqueCourante) {
        return getIntituleComplet(code, inclureRubriqueCourante);
    }

    /**
     * Récupération de l'intitulé complet (Concaténation mere + fille 1 niveau + ...)
     *
     * @param code
     *            le code de la rubrique à partir du quel on doit partir
     * @param inclureRubriqueCourante
     *            Doit on inclure la rubrique courante dans la chaine générée?
     *
     * @return L'intitulé de toute les rubriques jusqu'a la racine
     *
     * @deprecated utiliser {@link ServiceRubrique#getLabelWithAscendantsLabels(String)} si inclureRubriqueCourante = true
     * et {@link ServiceRubrique#getAscendantsLabelOnly(String)} sinon. Attention, à la difference de cette méthode, le service ne renvera que les intitules des rubriques. Cette
     * méthode renvoie pour la rubrique courante son intitulé puis la valeur du champ "nomOnglet". Si ceux comportement est réelement voulu, il faut utiliser {@link ServiceRubrique#getAllAscendant(String)}
     */
    @Deprecated
    public static String getIntituleComplet(final String code, final boolean inclureRubriqueCourante) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        String res = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(code)) {
            if (inclureRubriqueCourante) {
                res = serviceRubrique.getLabelWithAscendantsLabels(code);
            } else {
                res = serviceRubrique.getAscendantsLabelOnly(code);
            }
        }
        return res;
    }

    /**
     * Récupération des rubriques.
     *
     * @param _ctx
     *            the _ctx
     *
     * @return la liste rubriques par intitule complet
     * @deprecated ne pas utiliser, cette méthode retourne une map contenant l'ensemble des rubriques de l'applications... c'est un peu trop...
     */
    @Deprecated
    public static Hashtable<String, String> getListeRubriquesParIntituleComplet(final OMContext _ctx) {
        return getListeRubriquesParIntituleComplet();

    }

    /**
     * Récupération des rubriques.
     *
     *
     * @return la liste rubriques par intitule complet
     * @deprecated ne pas utiliser, cette méthode retourne une map contenant l'ensemble des rubriques de l'applications... c'est un peu trop...
     *
     */
    @Deprecated
    public static Hashtable<String, String> getListeRubriquesParIntituleComplet() {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final List<RubriqueBean> allRubrique = serviceRubrique.getAllRubriques();
        final Hashtable<String, String> res = new Hashtable<>(allRubrique.size());
        for (final RubriqueBean rubrique : allRubrique) {
            res.put(rubrique.getCode(), serviceRubrique.getLabelWithAscendantsLabels(rubrique));
        }
        return res;

    }

    /**
     * Récupération des rubriques.
     *
     * @param _ctx
     *            the _ctx
     *
     * @return the liste codes rubriques
     * @deprecated ne pas utiliser, ça requête sur toutes les rubriques de l'application... C'est un peu trop.
     */
    @Deprecated
    public static Vector<String> getListeCodesRubriques(final OMContext _ctx) {
        return new Vector<>(getListeCodesRubriques());
    }

    /**
     * Récupère l'ensemble des codes de rubriques présent en base.
     * @return la liste complète des codes de rubrique.
     * @deprecated ne pas utiliser, ça requête sur toutes les rubriques de l'application... C'est un peu trop.
     */
    @Deprecated
    public static List<String> getListeCodesRubriques() {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final List<RubriqueBean> allSection = serviceRubrique.getAllRubriques();
        final List<String> res = new ArrayList<>();
        for (final RubriqueBean infosRubriques : allSection) {
            res.add(infosRubriques.getCode());
        }
        return res;
    }


    /**
     * @deprecated ce cache n'est plus présent dans l'application
     * @return
     */
    @Deprecated
    public static CacheUrlRubrique getCacheUrlRubrique() {
        final Object res = CacheUtil.getObjectValue(CacheUrlManager.KEY_CACHE, CacheUrlManager.KEY_CACHE);
        if (res != null) {
            return (CacheUrlRubrique) res;
        } else {
            return new CacheUrlRubrique();
        }
    }

    /**
     * Renvoie la liste des rubriques principales (premier niveau) triée en fonction de l'ordre (méthode back, pas de dsi).
     *
     * @return Une liste d'InfosRubrique
     *
     */
    public static Collection<InfosRubriques> getListeRubriquesPrincipale() {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final List<RubriqueBean> principales = serviceRubrique.getRubriqueByCodeParent(StringUtils.EMPTY);
        final Collection<InfosRubriques> infosPrincipales = new ArrayList<>();
        for (final RubriqueBean rubPrincipale : principales) {
            final InfosRubriques infosPrincipale = new InfosRubriques(rubPrincipale);
            infosPrincipales.add(infosPrincipale);
        }
        return infosPrincipales;
    }

    /**
     * Renvoie la liste des rubriques principales (premier niveau) triée en fonction de l'ordre.
     *
     * @param ctx
     *            le contexte
     * @param front
     *            si front on applique la dsi
     *
     * @return Une liste d'InfosRubrique
     *
     */
    public static Collection<InfosRubriques> getListeRubriquesPrincipale(final ContexteUniv ctx, final boolean front) {
        final Collection<InfosRubriques> list;
        if (front) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final Collection<InfosRubriques> lSousRubriquesFront = new ArrayList<>();
            final List<RubriqueBean> principales = serviceRubrique.getRubriqueByCodeParent(StringUtils.EMPTY);
            for (final RubriqueBean sousRubrique : principales) {
                if (serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), sousRubrique.getCode()) && !"HIDDEN".equalsIgnoreCase(sousRubrique.getCategorie())) {
                    lSousRubriquesFront.add(new InfosRubriques(sousRubrique));
                }
            }
            list = lSousRubriquesFront;
        } else {
            list = getListeRubriquesPrincipale();
        }
        return list;
    }

    /**
     * Renvoie la rubrique de plus haut niveau (mère des rubriques de 1er niveau).
     *
     * @return Un InfosRubrique
     *
     */
    public static InfosRubriques getTopLevelRubrique() {
        return new InfosRubriques(ServiceRubrique.CODE_RUBRIQUE_ROOT);
    }

    /**
     * Récupération de l'objet rubrique.
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the code
     * @param langue
     *            the _langue
     *
     * @return the fiche rubrique
     *
     * @throws Exception
     *             the exception
     * @deprecated utiliser {@link Rubrique#getRubrique(String, String)} le contexte ne sert à rien ici...
     */
    @Deprecated
    public static Rubrique getFicheRubrique(final OMContext ctx, final String code, final String langue) throws Exception {
        return getRubrique(code, langue);
    }

    /**
     *
     * @param code
     * @param langue
     * @return
     * @throws Exception
     * @deprecated cette méthode est déprécié étant donné que son nom peut engendrer un mal entendu avec la fiche en page de tête de rubrique.
     * Utiliser {@link Rubrique#getRubrique(String, String)}
     */
    @Deprecated
    public static Rubrique getFicheRubrique(final String code, final String langue) throws Exception {
        return getRubrique(code, langue);
    }

    /**
     * Migration de getFicheRubrique.
     * Récupére la rubrique suivant le code et la langue fourni en paramètre.
     * @param code le code de la rubrique, si non renseigné la rubrique retourné sera null
     * @param langue la langue de la rubrique
     * @return Une rubrique ou null si non trouvé
     * @throws Exception
     */
    public static Rubrique getRubrique(final String code, final String langue) throws Exception {
        Rubrique res = null;
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(code);
        if (rubriqueBean != null) {
            res = new Rubrique();
            res.setPersistenceBean(rubriqueBean);
        }
        return res;
    }

    /**
     * Récupération d'une rubrique stockée en mémoire.
     *
     * @param code
     *            the code
     *
     * @return the infos rubriques
     *
     */
    public static InfosRubriques renvoyerItemRubrique(final String code) {
        InfosRubriques res = new InfosRubriques("");
        if (StringUtils.isNotBlank(code)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(code);
            if (rubriqueBean != null) {
                res = new InfosRubriques(rubriqueBean);
            }
        }
        return res;
    }

    /**
     * Renvoie la rubrique à afficher dans l'encadré, qui peut être différente de la rubrique courante lorsqu'on affiche une liste d'articles.
     *
     * @param codeRubrique
     *            Le code de la rubrique dont on souhaite récupérer l'encadré.
     *
     * @return L'encadré de la rubrique ou une chaine vide si non retrouvé
     *
     */
    public static String renvoyerRubriqueEncadre(final String codeRubrique) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueEncadre = serviceRubrique.getRubriqueByCode(codeRubrique);
        String rubriqueNavigation = StringUtils.EMPTY;
        if (rubriqueEncadre != null) {
            if ("0001".equals(rubriqueEncadre.getTypeRubrique())) {
                rubriqueNavigation = codeRubrique;
            } else {
                rubriqueNavigation = rubriqueEncadre.getCodeRubriqueMere();
            }
        }
        return rubriqueNavigation;
    }

    /**
     * Renvoie les infos du bandeau a partir du code de la rubrique.
     *
     * @param _ctx
     *            the _ctx
     * @param code
     *            the code
     * @param _langue
     *            the langue
     *
     * @retun InfosBandeau
     * @deprecated utiliser {@link Rubrique#getInfosBandeau(String)} car le contexte ne sert à rien idem pour la langue...
     */
    @Deprecated
    public static InfosBandeau getInfosBandeau(final OMContext _ctx, final String code, final String _langue) {
        return getInfosBandeau(code);
    }

    /**
     * Renvoie les infos du bandeau a partir du code de la rubrique.
     *
     * @param code
     *            le code de la rubrique
     *
     * @return InfosBandeau
     */
    public static InfosBandeau getInfosBandeau(final String code) {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        final InfosBandeau infosBandeau = new InfosBandeau();
        if (StringUtils.isBlank(code)) {
            return infosBandeau;
        }
        // Recherche du bandeau  et des couleurs
        // On recherche la première rubrique dans l'arborescence contenant un bandeau
        String codeRubrique = code;
        InfosRubriques infos = renvoyerItemRubrique(codeRubrique);
        int niveau = infos.getNiveau();
        String urlBandeau = MediaUtils.getUrlAbsolue(serviceMedia.getById(infos.getIdMediaBandeau()));
        String couleurFond = infos.getCouleurFond();
        String couleurTitre = infos.getCouleurTitre();
        while ((niveau > 1) && (urlBandeau.length() == 0)) {
            codeRubrique = infos.getCodeRubriqueMere();
            infos = renvoyerItemRubrique(codeRubrique);
            urlBandeau = MediaUtils.getUrlAbsolue(serviceMedia.getById(infos.getIdMediaBandeau()));
            couleurFond = infos.getCouleurFond();
            couleurTitre = infos.getCouleurTitre();
            niveau--;
        }
        infosBandeau.setUrlBandeau(urlBandeau);
        infosBandeau.setCouleurFond(couleurFond);
        infosBandeau.setCouleurTitre(couleurTitre);
        return infosBandeau;
    }

    /**
     * Controle si l'utilisateur a les droits sur la rubrique de la fiche courante.
     *
     * @param ctx
     *            contexte pour la base et les autorisations du user
     * @param codeRubriqueFiche
     *            the code rubrique fiche
     *
     * @return boolean accesOK
     */
    public static boolean controlerRestrictionRubrique(final ContexteUniv ctx, final String codeRubriqueFiche) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        return serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), codeRubriqueFiche);
    }

    /*
     * Cette méthode renvoit une rubrique de la fiche dans le site courant
     * par defaut la rubrique principale ou la première de ces rubriques de publication
     * si précisé, on cherchera en priorité la rubrique incluse dans la rubrique courante sinon on renverra celle par défaut
     * @param ctx
     *            the ctx
     * @param infosRubriqueCourante
     *            the infos rubrique courante
     * @param lstCodeRubriquePubliable
     *            the lst code rubrique publiable
     * @param inRubriqueCourante
     *            the in rubrique courante
     *
     * @return the rubrique publication
     *
     */
    public static String getRubriquePublication(final OMContext ctx, final InfosRubriques infosRubriqueCourante, final ArrayList<String> lstCodeRubriquePubliable, final boolean inRubriqueCourante) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        return serviceRubrique.getRubriquePublication(ctx, infosRubriqueCourante.getCode(), lstCodeRubriquePubliable, inRubriqueCourante);
    }

    /**
     * Détermine de facon récursive la liste des rubriques autorisées pour un utilisateur donné.
     *
     * @param ctx
     *            contexte pour la base et les autorisations du user
     * @param rubrique
     *            the rubrique
     *
     * @return the collection
     * @deprecated utiliser {@link ServiceRubrique#determinerListeSousRubriquesAutorisees(Set, RubriqueBean)}
     */
    @Deprecated
    public static Collection<InfosRubriques> determinerListeSousRubriquesAutorisees(final ContexteUniv ctx, final InfosRubriques rubrique) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final Collection<RubriqueBean> sousRubriqueAutorisees = serviceRubrique.determinerListeSousRubriquesAutorisees(ctx.getGroupesDsiAvecAscendants(), rubrique.getCode());
        final Collection<InfosRubriques> resultats = new ArrayList<>(sousRubriqueAutorisees.size());
        for (final RubriqueBean sousRubrique : sousRubriqueAutorisees) {
            resultats.add(new InfosRubriques(sousRubrique));
        }
        return resultats;
    }

    /**
     * Détermine la liste des rubriques autorisées pour un utilisateur donné.
     *
     * @param listeGroupes
     *            the liste groupes
     * @param rubrique
     *            the rubrique
     * @param rechercheArborescente
     *            the recherche arborescente
     * @param listeRecursive
     *            the liste recursive
     * @deprecated utiliser {@link ServiceRubrique#determinerListeSousRubriquesAutorisees(Set, RubriqueBean)}
     */
    @Deprecated
    public static void determinerListeSousRubriquesAutorisees(final Set<String> listeGroupes, final InfosRubriques rubrique, final boolean rechercheArborescente, final Collection<InfosRubriques> listeRecursive) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final Collection<RubriqueBean> sousRubriqueAutorisees = serviceRubrique.determinerListeSousRubriquesAutorisees(listeGroupes, rubrique.getCode());
        for (final RubriqueBean sousRubrique : sousRubriqueAutorisees) {
            listeRecursive.add(new InfosRubriques(sousRubrique));
        }
    }

    /**
     * Gets the fil ariane.
     *
     * @param codeRubrique
     *            the code rubrique
     * @param separateur
     *            the separateur
     *
     * @return the fil ariane
     * @deprecated utiliser {@link ServiceRubrique#getLabelWithAscendantsLabels(String)}
     */
    @Deprecated
    public static String getFilAriane(final String codeRubrique, final String separateur) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        return serviceRubrique.getLabelWithAscendantsLabels(codeRubrique);
    }

    /**
     * Inits the.
     */
    public void init() {
        setIdRubrique(0L);
        setCode(String.valueOf(System.currentTimeMillis()));
        setLangue("0");
        setIntitule("");
        setAccroche("");
        setIdBandeau(0L);
        setCouleurFond("");
        setCouleurTitre("");
        setNomOnglet("");
        setCodeRubriqueMere("");
        setTypeRubrique("");
        setPageAccueil("");
        setGestionEncadre("0");
        setEncadre("");
        setEncadreSousRubrique("0");
        setOrdre("");
        setContact("");
        setGroupesDsi("");
        setRequetesRubriquePublication("");
        setCategorie("");
        setIdPicto(0L);
    }

    /**
     * Renvoie le contenu formate en HTML pour l'attribut accroche.
     *
     * @return the formated accroche
     *
     * @throws Exception
     *             the exception
     */
    public String getFormatedAccroche() throws Exception {
        return UnivWebFmt.formaterEnHTML(ContexteUtil.getContexteUniv(), getAccroche());
    }

    /**
     * Renvoie le contenu formate en HTML pour l'attribut encadre.
     *
     * @return the formated encadre
     *
     * @throws Exception
     *             the exception
     */
    public String getFormatedEncadre() throws Exception {
        return UnivWebFmt.formaterEnHTML(ContexteUtil.getContexteUniv(), getEncadre());
    }

    /**
     * Suppression d'une rubrique . On vérifie que la rubrique est vide, elle ne doit contenir : - ni page libres - ni sous-rubriques - ni articles
     *
     * @throws ErreurApplicative
     *             Lors de la suppression d'une rubrique si elle contient des doc liés ...
     */
    public void delete() throws ErreurApplicative {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        serviceRubrique.deletebyId(getIdRubrique());
    }

    /**
     * Sélection d'une rubrique à partir de l'ensemble des critères combinés.
     *
     * @param code
     *            the code
     * @param langue
     *            the langue
     * @param intitule
     *            the _intitule
     * @param codeSaisi
     *            the code saisi
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String code, final String langue, final String intitule, final String codeSaisi) throws Exception {
        String codeAVerifier = "";
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseOrderBy orderBy = new ClauseOrderBy("INTITULE", SensDeTri.ASC);
        final ClauseWhere where = new ClauseWhere();
        if (StringUtils.isNotEmpty(code)) {
            codeAVerifier = code;
        } else if (StringUtils.isNotEmpty(codeSaisi)) {
            codeAVerifier = codeSaisi;
        }
        if (StringUtils.isNotEmpty(codeAVerifier)) {
            where.setPremiereCondition(ConditionHelper.egalVarchar("CODE", codeAVerifier));
        }
        if (StringUtils.isNotEmpty(langue) && !"0000".equals(langue)) {
            where.and(ConditionHelper.egalVarchar("LANGUE", langue));
        }
        if (StringUtils.isNotEmpty(intitule)) {
            where.and(ConditionHelper.rechercheMots("INTITULE", intitule));
        }
        requeteSelect.where(where).orderBy(orderBy);
        return select(requeteSelect.formaterRequete());
    }

    /**
     * FIXME : La selection sur l'état n'est pas gérer????? Select code langue etat.
     *
     * @param code
     *            the code
     * @param langue
     *            the langue
     * @param etatObjet
     *            the _etat objet
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     * @deprecated il n'y a pas d'état sur les rubriques... {@link Rubrique#selectCodeLangue}
     */
    @Deprecated
    public int selectCodeLangueEtat(final String code, final String langue, final String etatObjet) throws Exception {
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseOrderBy orderBy = new ClauseOrderBy("INTITULE", SensDeTri.ASC);
        final ClauseWhere where = new ClauseWhere();
        if (StringUtils.isNotEmpty(code)) {
            where.setPremiereCondition(ConditionHelper.egalVarchar("CODE", code));
        }
        if (StringUtils.isNotEmpty(langue)) {
            where.and(ConditionHelper.egalVarchar("LANGUE", langue));
        }
        requeteSelect.where(where).orderBy(orderBy);
        return select(requeteSelect.formaterRequete());
    }

    /**
     * Calcule la rubrique en fonction du code et de la langue fourni en paramètre
     *
     * @param code
     *            le code de la rubrique
     * @param langue
     *            la langue de la rubrique
     *
     * @return le nombre de résultat obtenu
     *
     * @throws Exception
     *             lors de la requête SQL
     */
    public int selectCodeLangue(final String code, final String langue) throws Exception {
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseOrderBy orderBy = new ClauseOrderBy("INTITULE", SensDeTri.ASC);
        final ClauseWhere where = new ClauseWhere();
        if (StringUtils.isNotEmpty(code)) {
            where.setPremiereCondition(ConditionHelper.egalVarchar("CODE", code));
        }
        if (StringUtils.isNotEmpty(langue)) {
            where.and(ConditionHelper.egalVarchar("LANGUE", langue));
        }
        requeteSelect.where(where).orderBy(orderBy);
        return select(requeteSelect.formaterRequete());
    }

    @Override
    public List<String> getCodesLibelleMedia() {
        // 0100 Bandeau rubrique  est un type photo de libelle utilise dans la mediatheque
        return Collections.singletonList("0100");
    }

    @Override
    public void checkCodesLibelleMedia(final long idMedia) throws ErreurApplicative {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final List<RubriqueBean> results = serviceRubrique.getByBandeau(idMedia);
        if (CollectionUtils.isNotEmpty(serviceRubrique.getByBandeau(idMedia))) {
            throw new ErreurApplicative(String.format(MessageHelper.getCoreMessage(ContexteUtil.getContexteUniv().getLocale(), "RUBRIQUE.BANDEAU.ERREUR.SUPPRESSION_RESSOURCE"), results.get(0).getIntitule()));
        }
    }

    /**
     * Sets the ctx.
     *
     * @param ctx
     *            the new ctx
     */
    public void setCtx(final OMContext ctx) {}

    /**
     * Gets the accroche.
     *
     * @return the accroche
     */
    public String getAccroche() {
        return persistenceBean.getAccroche();
    }

    /**
     * Sets the accroche.
     *
     * @param accroche
     *            the accroche to set
     */
    public void setAccroche(final String accroche) {
        persistenceBean.setAccroche(accroche);
    }

    /**
     * Gets the categorie.
     *
     * @return the categorie
     */
    public String getCategorie() {
        return persistenceBean.getCategorie();
    }

    /**
     * Sets the categorie.
     *
     * @param categorie
     *            the categorie to set
     */
    public void setCategorie(final String categorie) {
        persistenceBean.setCategorie(categorie);
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return persistenceBean.getCode();
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        persistenceBean.setCode(code);
    }

    /**
     * Gets the code rubrique mere.
     *
     * @return the codeRubriqueMere
     */
    public String getCodeRubriqueMere() {
        return persistenceBean.getCodeRubriqueMere();
    }

    /**
     * Sets the code rubrique mere.
     *
     * @param codeRubriqueMere
     *            the codeRubriqueMere to set
     */
    public void setCodeRubriqueMere(final String codeRubriqueMere) {
        persistenceBean.setCodeRubriqueMere(codeRubriqueMere);
    }

    /**
     * Gets the contact.
     *
     * @return the contact
     */
    public String getContact() {
        return persistenceBean.getContact();
    }

    /**
     * Sets the contact.
     *
     * @param contact
     *            the contact to set
     */
    public void setContact(final String contact) {
        persistenceBean.setContact(contact);
    }

    /**
     * Gets the couleur fond.
     *
     * @return the couleurFond
     */
    public String getCouleurFond() {
        return persistenceBean.getCouleurFond();
    }

    /**
     * Sets the couleur fond.
     *
     * @param couleurFond
     *            the couleurFond to set
     */
    public void setCouleurFond(final String couleurFond) {
        persistenceBean.setCouleurFond(couleurFond);
    }

    /**
     * Gets the couleur titre.
     *
     * @return the couleurTitre
     */
    public String getCouleurTitre() {
        return persistenceBean.getCouleurTitre();
    }

    /**
     * Sets the couleur titre.
     *
     * @param couleurTitre
     *            the couleurTitre to set
     */
    public void setCouleurTitre(final String couleurTitre) {
        persistenceBean.setCouleurTitre(couleurTitre);
    }

    /**
     * Gets the encadre.
     *
     * @return the encadre
     */
    public String getEncadre() {
        return persistenceBean.getEncadre();
    }

    /**
     * Sets the encadre.
     *
     * @param encadre
     *            the encadre to set
     */
    public void setEncadre(final String encadre) {
        persistenceBean.setEncadre(encadre);
    }

    /**
     * Gets the encadre sous rubrique.
     *
     * @return the encadreSousRubrique
     */
    public String getEncadreSousRubrique() {
        return persistenceBean.getEncadreSousRubrique();
    }

    /**
     * Sets the encadre sous rubrique.
     *
     * @param encadreSousRubrique
     *            the encadreSousRubrique to set
     */
    public void setEncadreSousRubrique(final String encadreSousRubrique) {
        persistenceBean.setEncadreSousRubrique(encadreSousRubrique);
    }

    /**
     * Gets the gestion encadre.
     *
     * @return the gestionEncadre
     */
    public String getGestionEncadre() {
        return persistenceBean.getGestionEncadre();
    }

    /**
     * Sets the gestion encadre.
     *
     * @param gestionEncadre
     *            the gestionEncadre to set
     */
    public void setGestionEncadre(final String gestionEncadre) {
        persistenceBean.setGestionEncadre(gestionEncadre);
    }

    /**
     * Gets the groupes dsi.
     *
     * @return the groupesDsi
     */
    public String getGroupesDsi() {
        return persistenceBean.getGroupesDsi();
    }

    /**
     * Sets the groupes dsi.
     *
     * @param groupesDsi
     *            the groupesDsi to set
     */
    public void setGroupesDsi(final String groupesDsi) {
        persistenceBean.setGroupesDsi(groupesDsi);
    }

    /**
     * Gets the id bandeau.
     *
     * @return the idBandeau
     */
    public Long getIdBandeau() {
        return persistenceBean.getIdBandeau();
    }

    /**
     * Sets the id bandeau.
     *
     * @param idBandeau
     *            the idBandeau to set
     */
    public void setIdBandeau(final Long idBandeau) {
        persistenceBean.setIdBandeau(idBandeau);
    }

    /**
     * Gets the id rubrique.
     *
     * @return the idRubrique
     */
    public Long getIdRubrique() {
        return persistenceBean.getIdRubrique();
    }

    /**
     * Sets the id rubrique.
     *
     * @param idRubrique
     *            the idRubrique to set
     */
    public void setIdRubrique(final Long idRubrique) {
        persistenceBean.setIdRubrique(idRubrique);
    }

    /**
     * Gets the intitule.
     *
     * @return the intitule
     */
    public String getIntitule() {
        return persistenceBean.getIntitule();
    }

    /**
     * Sets the intitule.
     *
     * @param intitule
     *            the intitule to set
     */
    public void setIntitule(final String intitule) {
        persistenceBean.setIntitule(intitule);
    }

    /**
     * Gets the langue.
     *
     * @return the langue
     */
    public String getLangue() {
        return persistenceBean.getLangue();
    }

    /**
     * Sets the langue.
     *
     * @param langue
     *            the langue to set
     */
    public void setLangue(final String langue) {
        persistenceBean.setLangue(langue);
    }

    /**
     * Gets the nom onglet.
     *
     * @return the nomOnglet
     */
    public String getNomOnglet() {
        return persistenceBean.getNomOnglet();
    }

    /**
     * Sets the nom onglet.
     *
     * @param nomOnglet
     *            the nomOnglet to set
     */
    public void setNomOnglet(final String nomOnglet) {
        persistenceBean.setNomOnglet(nomOnglet);
    }

    /**
     * Gets the ordre.
     *
     * @return the ordre
     */
    public String getOrdre() {
        return persistenceBean.getOrdre();
    }

    /**
     * Sets the ordre.
     *
     * @param ordre
     *            the ordre to set
     */
    public void setOrdre(final String ordre) {
        persistenceBean.setOrdre(ordre);
    }

    /**
     * Gets the requetes rubrique publication.
     *
     * @return the requetesRubriquePublication
     */
    public String getRequetesRubriquePublication() {
        return persistenceBean.getRequetesRubriquePublication();
    }

    /**
     * Sets the requetes rubrique publication.
     *
     * @param requetesRubriquePublication
     *            the requetesRubriquePublication to set
     */
    public void setRequetesRubriquePublication(final String requetesRubriquePublication) {
        persistenceBean.setRequetesRubriquePublication(requetesRubriquePublication);
    }

    /**
     * Gets the type rubrique.
     *
     * @return the typeRubrique
     */
    public String getTypeRubrique() {
        return persistenceBean.getTypeRubrique();
    }

    /**
     * Sets the type rubrique.
     *
     * @param typeRubrique
     *            the typeRubrique to set
     */
    public void setTypeRubrique(final String typeRubrique) {
        persistenceBean.setTypeRubrique(typeRubrique);
    }

    public String getPageAccueil() {
        return persistenceBean.getPageAccueil();
    }

    public void setPageAccueil(final String pageAccueil) {
        persistenceBean.setPageAccueil(pageAccueil);
    }

    public Long getIdPicto() {
        return persistenceBean.getIdPicto();
    }

    public void setIdPicto(final Long idPicto) {
        persistenceBean.setIdPicto(idPicto);
    }

    @Deprecated
    public void retrieve() throws Exception {
        persistenceBean = commonDao.getById(getIdRubrique());
        if (persistenceBean == null) {
            throw new ExceptionFicheNonTrouvee("unable to find any content with this id");
        }
    }
}
