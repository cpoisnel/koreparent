package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe representant une liste de paragraphes d'une ligne de page libre.
 */
public class LigneContenu {

    private int numLigne = 0;

    private ArrayList<ParagrapheBean> paragraphesLigne = null;

    public LigneContenu(int numLigne) {
        this.numLigne = numLigne;
        paragraphesLigne = new ArrayList<>();
    }

    public void addParagraphe(ParagrapheBean paragraphe) {
        paragraphesLigne.add(paragraphe);
    }

    public int getNumLigne() {
        return numLigne;
    }

    public List<ParagrapheBean> getParagraphes() {
        return paragraphesLigne;
    }
}
