package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;

/**
 * Classe abstraite qui étend la classe {@link RequeteGroupeDynamique} et qui permet de rendre cette requete de construction de groupe dynamique synchronisable.<br/>
 *
 * A besoin des propriétés du JTF suivante :
 * <ul>
 * <li>requete_groupe.NOM_REQUETE.type_synchronise</li>
 * <li>requete_groupe.NOM_REQUETE.prefixe_code_groupe_synchronise</li>
 * <li>requete_groupe.NOM_REQUETE.auto_synchronise</li>
 * <li>requete_groupe.NOM_REQUETE.code_groupe_parent</li>
 * </ul>
 *
 * @author Pierre Cosson
 */
public abstract class RequeteGroupeDynamiqueSynchronisable extends RequeteGroupeDynamique {

    /** Loggeur. */
    private static final Logger LOG = LoggerFactory.getLogger(RequeteGroupeDynamiqueSynchronisable.class);

    /**
     * Savoir si cette requete de groupe dynamique doit être synchronisé automatiquement (par un batch par exemple). Cette propriété est à définir dans le JTF :
     * requete_groupe.NOM_REQUETE.auto_synchronise
     *
     * @return TRUE si on veut que se soit synchronisé automatiquement sinon FALSE.
     */
    public boolean isAutoSynchronise() {
        return "1".equals(PropertyHelper.getCoreProperty("requete_groupe." + getNomRequete() + ".auto_synchronise"));
    }

    /**
     * Récupérer le type de synchronisation sur cette requête. Cette option est définie dans le JTF par la propriété : requete_groupe.NOM_REQUETE.type_synchronise.
     *
     * @return Retourne le type de synchronisation définie dans le JTF à la proprité : requete_groupe.NOM_REQUETE.type_synchronise
     */
    protected String getTypeSynchronisation() {
        return PropertyHelper.getCoreProperty("requete_groupe." + this.getNomRequete() + ".type_synchronise");
    }

    /**
     * Récupérer le préfixe des codes de groupe qu'auront les groupes synchronisés. Pour cela il faut définir la propriété : requete_groupe.NOM_REQUETE.prefixe_groupe_synchronise
     * dans le JTF. Si celle-ci n'est pas définie, c'est le nom de requête qui sera retourné.
     *
     * @return Le préfixe qu'aura les groupes synchronisés
     */
    protected String getPrefixeNomGroupeSynchro() {
        final String prefixe = PropertyHelper.getCoreProperty("requete_groupe." + getNomRequete() + ".prefixe_code_groupe_synchronise");
        if (prefixe == null) {
            return this.getNomRequete();
        } else {
            return prefixe;
        }
    }

    /**
     * Récupérer le code de groupe auquel sera rattacher les groupes dynamiques par défaut. Pour cela la valeur est récupérée dans le JTF à la propriété suivante :
     * requete_groupe.NOM_REQUETE.code_groupe_parent.
     *
     * @return le code du groupe de rattachement ou bien "" si il n'est pas défini dans le JTF.
     */
    protected String getCodeGroupePere() {
        final String codePere = PropertyHelper.getCoreProperty("requete_groupe." + this.getNomRequete() + ".code_groupe_parent");
        if (codePere != null) {
            return codePere;
        } else {
            return "";
        }
    }

    /**
     * Synchroniser les groupes de la requête dynamique. Cette synchronisation : ajoute, modifie et supprime les groupes en fonction de la liste des données à synchroniser.
     *
     * @return L'état de la synchronisation : {@link SynchronisationGroupeDynamiqueEtat}.
     */
    public SynchronisationGroupeDynamiqueEtat synchronize() {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final SynchronisationGroupeDynamiqueEtat result = new SynchronisationGroupeDynamiqueEtat();
        try {
            //MAP<String, SynchronizationData>
            final Map<String, SynchronisationGroupeDynamiqueData> dataToSynchronise = getDataToSynchronize();
            //s'il y a des données à synchroniser
            if (dataToSynchronise != null) {
                //Collection<String>
                final Set<String> objectsCodesToSync = dataToSynchronise.keySet();
                final String codeTypeGroupeSynchronise = this.getTypeSynchronisation();
                final String prefixeCode = this.getPrefixeNomGroupeSynchro();
                final Collection<String> codesGroupesSynchronises = new ArrayList<>();
                //récupération des codes des groupes dynamiques synchronisés
                for (GroupeDsiBean currentGroup : serviceGroupeDsi.getAllDynamics()) {
                    if (codeTypeGroupeSynchronise.equals(currentGroup.getType())) {
                        codesGroupesSynchronises.add(currentGroup.getCode());
                    }
                }
                // Première étape : on supprime les groupes dynamiques liés à des objets qui n'existent plus
                for (String codeCourant : codesGroupesSynchronises) {
                    if (!objectsCodesToSync.contains(codeCourant.substring(prefixeCode.length()))) {
                        List<GroupeDsiBean> groupeDsiBeans = serviceGroupeDsi.getByCodeAndType(codeCourant, codeTypeGroupeSynchronise);
                        if (CollectionUtils.isNotEmpty(groupeDsiBeans)) {
                            GroupeDsiBean groupeDsi = groupeDsiBeans.get(0);
                            result.addGroupeSynchroSupprime(groupeDsi.getCode(), groupeDsi.getLibelle());
                            serviceGroupeDsi.delete(groupeDsi.getIdGroupedsi());
                        }
                    }
                }
                // Seconde étape : on crée des groupes dynamiques pour les nouveaux espaces
                for (String codeCourant : objectsCodesToSync) {
                    //récupération des données du groupe à synchroniser
                    SynchronisationGroupeDynamiqueData synchronisationData = dataToSynchronise.get(codeCourant);
                    GroupeDsiBean groupeDsiBean = serviceGroupeDsi.getByCode(prefixeCode + codeCourant);
                    if (!codesGroupesSynchronises.contains(prefixeCode + codeCourant)) {
                        if (groupeDsiBean != null) {
                            if (isUpToDateGroupeDsi(groupeDsiBean, synchronisationData, prefixeCode)) {
                                synchroniseDataAndGroupeDsi(groupeDsiBean, synchronisationData, codeTypeGroupeSynchronise, prefixeCode);
                                serviceGroupeDsi.save(groupeDsiBean);
                                result.addGroupeSynchroModifie(groupeDsiBean.getCode(), groupeDsiBean.getLibelle());
                            }
                        } else {
                            //Création
                            groupeDsiBean = new GroupeDsiBean();
                            groupeDsiBean.setCode(prefixeCode + codeCourant);
                            synchroniseDataAndGroupeDsi(groupeDsiBean, synchronisationData, codeTypeGroupeSynchronise, prefixeCode);
                            serviceGroupeDsi.save(groupeDsiBean);
                            result.addGroupeSynchroAjoute(groupeDsiBean.getCode(), groupeDsiBean.getLibelle());
                        }
                    } else if (groupeDsiBean != null) {
                        //on vérifie s'il n'y a pas une mise à jour à faire
                        if (isUpToDateGroupeDsi(groupeDsiBean, synchronisationData, prefixeCode)) {
                            synchroniseDataAndGroupeDsi(groupeDsiBean, synchronisationData, codeTypeGroupeSynchronise, prefixeCode);
                            serviceGroupeDsi.save(groupeDsiBean);
                            result.addGroupeSynchroModifie(groupeDsiBean.getCode(), groupeDsiBean.getLibelle());
                        }
                    }
                }
                result.setValide(true);
            }
        } catch (final Exception e) {
            LOG.error("Erreur lors de la synchronisation des groupes dynamiques (" + this.getNomRequete() + ") : ", e);
        }
        return result;
    }

    /**
     * Savoir si un groupe doit être mis à jour ou non.
     *
     * @param groupe
     *            Le groupe à tester.
     * @param data
     *            Les données de synchronisation.
     * @param prefixeGroupe
     *            Le préfixe du code de groupe.
     *
     * @return TRUE si les données groupe sont différentes de celles à synchroniser.
     */
    private boolean isUpToDateGroupeDsi(final GroupeDsiBean groupe, final SynchronisationGroupeDynamiqueData data, final String prefixeGroupe) {
        boolean sameData = data.getIntitule().equals(groupe.getLibelle());
        sameData &= data.getCodeDuParent() == null || (prefixeGroupe + data.getCodeDuParent()).equals(groupe.getCodeGroupePere()) || "".equals(data.getCodeDuParent()) && this.getCodeGroupePere().equals(groupe.getCodeGroupePere());
        sameData &= data.getCodePageDeTete() == null || data.getCodePageDeTete().equals(groupe.getCodePageTete());
        sameData &= data.getCodeStructureCode() == null || data.getCodeStructureCode().equals(groupe.getCodeStructure());
        sameData &= data.getRoles() == null || data.getRoles().equals(groupe.getRoles());
        return !sameData;
    }

    /**
     * Synchroniser le groupe avec les données passées en paramétre.
     *
     * @param groupeDsi
     *            Le {@link GroupeDsiBean} auquel on va affecter les valeurs.
     * @param synchronisationData
     *            les données à synchroniser.
     * @param codeTypeGroupeSynchronise
     *            Le code du type de groupe à synchroniser.
     * @param prefixeCode
     *            Le préfixe du code de groupe.
     */
    private void synchroniseDataAndGroupeDsi(final GroupeDsiBean groupeDsi, final SynchronisationGroupeDynamiqueData synchronisationData, final String codeTypeGroupeSynchronise, final String prefixeCode) {
        groupeDsi.setLibelle(synchronisationData.getIntitule());
        groupeDsi.setType(codeTypeGroupeSynchronise);
        groupeDsi.setRequeteGroupe(this.getNomRequete());
        //on ne change la requete LDAP que si elle ne contient pas le code de l'objet
        final String codeLDAP = groupeDsi.getRequeteLdap();
        final String codeObjet = synchronisationData.getCode();
        final int positionCodeObjet = codeLDAP.indexOf(codeObjet);
        //Cas le code n'existe pas OU
        //NON (Cas de codeLDAP:="CODE_OBJET" ET
        //     Cas de codeLDAP:="CODE_OBJET;AUTRES_CODES..." ET
        //     Cas de codeLDAP:="...AUTRES_CODES;CODE_OBJET;AUTRES_CODES..." ET
        //     Cas de codeLDAP:="...AUTRES_CODES;CODE_OBJET")
        if ((positionCodeObjet == -1) || !(codeLDAP.equals(codeObjet) || (positionCodeObjet == 0 && codeLDAP.indexOf(codeObjet + ";") == positionCodeObjet) || (positionCodeObjet > 0 && (positionCodeObjet + codeObjet.length()) < codeLDAP.length() && codeLDAP.indexOf(";" + codeObjet + ";") == (positionCodeObjet - 1)) || (positionCodeObjet > 0 && (positionCodeObjet + codeObjet.length()) == codeLDAP.length() && codeLDAP.indexOf(";" + codeObjet) == (positionCodeObjet - 1)))) {
            //si le code objet n'a pas été trouvé
            if (codeLDAP.length() > 0) {
                //on ajoute le code au reste (pas de reinitialisation totale)
                groupeDsi.setRequeteLdap(synchronisationData.getCode() + ";" + codeLDAP);
            } else {
                groupeDsi.setRequeteLdap(synchronisationData.getCode());
            }
        }
        if (synchronisationData.getCodeDuParent() != null && synchronisationData.getCodeDuParent().length() > 0) {
            //le code du parent est defini donc se sera lui qui sera le parent
            groupeDsi.setCodeGroupePere(prefixeCode + synchronisationData.getCodeDuParent());
        } else {
            //pas de code parent défini alors c'est le code parent par défaut
            groupeDsi.setCodeGroupePere(this.getCodeGroupePere());
        }
        if (synchronisationData.getCodePageDeTete() != null) {
            groupeDsi.setCodePageTete(synchronisationData.getCodePageDeTete());
        }
        if (synchronisationData.getCodeStructureCode() != null) {
            groupeDsi.setCodeStructure(synchronisationData.getCodeStructureCode());
        }
        if (synchronisationData.getRoles() != null) {
            groupeDsi.setRoles(synchronisationData.getRoles());
        }
    }

    /**
     * Cette fonction permet d'effectuer la synchronisation d'une requête de groupe dynamique. Cette fonction permet de Synchroniser le groupe mais aussi d'affecter les résultats
     * dans l'InfoBean pour l'affichage. Cette fonction est doit être appelé dans un processus de saisie (par defaut dans le processus SaisieSynchronisationGroupeDynamique)
     *
     * @param infoBean
     *            the info bean
     *
     * @throws Exception
     *             the exception
     */
    public void traiterSYNCHRONISATION(final InfoBean infoBean) throws Exception {
        LOG.debug(" *** Début de la synchronisation des groupes dynamiques ");
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final SynchronisationGroupeDynamiqueEtat resultatSynchronisation = this.synchronize();
        /* On prépare l'affichage de l'écran de résultat */
        infoBean.set("RESULTAT", resultatSynchronisation.isValide());
        infoBean.set("INTITULE_REQUETE", serviceGroupeDsi.getListeRequetesGroupesPourAffichage().get(this.getNomRequete()));
        infoBean.set("NB_AJOUTS", resultatSynchronisation.getNbGroupesAjoutes());
        infoBean.set("NB_MODIFICATIONS", resultatSynchronisation.getNbGroupesModifies());
        infoBean.set("NB_SUPPRESSIONS", resultatSynchronisation.getNbGroupesSupprimes());
        infoBean.set("GROUPES_AJOUTES", resultatSynchronisation.getLibellesGroupeSynchroAjoutes());
        infoBean.set("GROUPES_MODIFIES", resultatSynchronisation.getLibellesGroupeSynchroModifies());
        infoBean.set("GROUPES_SUPPRIMES", resultatSynchronisation.getLibellesGroupeSynchroSupprimes());
        LOG.debug(" *** Fin de la synchronisation des groupes dynamiques ");
    }

    /**
     * Récupérer la liste des codes des objets à synchroniser ainsi que l'ensemble des données à affecter au nouveau groupe.
     *
     * @return {@link Map} de la forme Map&lt; {@link String}, {@link SynchronisationGroupeDynamiqueData} &gt; où :
     *         <ul>
     *         <li>Le {@link String} est le code de l'objet à synchroniser</li>
     *         <li>Le {@link SynchronisationGroupeDynamiqueData} correspond à l'ensemble des données qui seront affectées au groupe dynamique</li>
     *         </ul>
     *         Il faut s'assurer que cette {@link Map} soit bien formée, c'est à dire que tous les codes parents contenus dans les objets {@link SynchronisationGroupeDynamiqueData}
     *         soient bien des objets ayant fait l'objet de création de groupes dynamiques (que chaque parent soit aussi un groupe dynamique). Il faut aussi s'assurer qu'il n'y ait
     *         pas de références circulaires : le pére d'un objet à pour pére l'objet dont il est le parent.<br/>
     *         <strong>Ces vérifications ne sont pas faites dans la fonction de synchronisation, il faut donc s'en assurer avant d'effectuer le Return</strong>
     *
     * @throws Exception
     *             Erreur durant la construction de la liste de données.
     */
    protected abstract Map<String, SynchronisationGroupeDynamiqueData> getDataToSynchronize() throws Exception;
}
