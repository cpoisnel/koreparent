package com.univ.objetspartages.om;
// TODO: Auto-generated Javadoc

/**
 * JSS 20051031 A implémenter pour gérer les structures secondaires de rattachement.
 */
public interface FicheRattachementsSecondaires {

    /**
     * Gets the code rattachement autres.
     *
     * @return the code rattachement autres
     */
    String getCodeRattachementAutres();

    /**
     * Sets the code rattachement autres.
     *
     * @param codesRattachement
     *            the new code rattachement autres
     */
    void setCodeRattachementAutres(String codesRattachement);
}