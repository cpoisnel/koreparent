package com.univ.objetspartages.om;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;

/**
 * The Class NiveauPerimetre.
 */
public class NiveauPerimetre {

    /** The EGAL. */
    public static final int EGAL = 0;

    /** The DESSOUS. */
    public static final int DESSOUS = 1;

    /** The DESSUS. */
    public static final int DESSUS = 2;

    /** The DIFFERENT. */
    public static final int DIFFERENT = -1;

    /** The code structure. */
    private String codeStructure = "";

    /** The structure ancestors. */
    private Set<String> structureAncestors = null;

    /** The code rubrique. */
    private String codeRubrique = "";

    /** The rubrique ancestor. */
    private Set<String> rubriqueAncestors = null;

    /** The code groupe. */
    private String codeGroupe = "";

    /** The groupeancestors. */
    private Set<String> groupeAncestors = null;

    /** The code espace collaboratif. */
    private String codeEspaceCollaboratif = "";

    /** The chaine serialisee. */
    private String chaineSerialisee = "";

    /** The lst enfant. */
    private HashSet<NiveauPerimetre> lstEnfant = new HashSet<>();

    /** The lst parent. */
    private HashSet<NiveauPerimetre> lstParent = new HashSet<>();

    /** The hash code chaine serialisee. */
    private int hashCodeChaineSerialisee = 0;

    /**
     * Instantiates a new niveau perimetre.
     */
    public NiveauPerimetre() {
        super();
    }

    /**
     * Instantiates a new niveau perimetre.
     *
     * @param _perimetre
     *            the _perimetre
     */
    public NiveauPerimetre(Perimetre _perimetre) {
        super();
        if (_perimetre.getCodeEspaceCollaboratif().length() == 0) {
            //si le perimetre est restreint sur un espace alors on ignore les autres champs
            codeStructure = _perimetre.getCodeStructure();
            codeRubrique = _perimetre.getCodeRubrique();
            codeGroupe = _perimetre.getCodeGroupe();
        }
        codeEspaceCollaboratif = _perimetre.getCodeEspaceCollaboratif();
        chaineSerialisee = _perimetre.getIdentifiantNiveau();
    }

    /**
     * To string.
     *
     * @return Returns the lstEnfant.
     */
    @Override
    public String toString() {
        return chaineSerialisee;
    }

    /**
     * Gets the lst enfant.
     *
     * @return Returns the lstEnfant.
     */
    public HashSet<NiveauPerimetre> getLstEnfant() {
        return lstEnfant;
    }

    /**
     * Sets the lst enfant.
     *
     * @param lstEnfant
     *            The lstEnfant to set.
     */
    public void setLstEnfant(HashSet<NiveauPerimetre> lstEnfant) {
        this.lstEnfant = lstEnfant;
    }

    /**
     * Gets the lst parent.
     *
     * @return Returns the lstParent.
     */
    public HashSet<NiveauPerimetre> getLstParent() {
        return lstParent;
    }

    /**
     * Sets the lst parent.
     *
     * @param lstParent
     *            The lstParent to set.
     */
    public void setLstParent(HashSet<NiveauPerimetre> lstParent) {
        this.lstParent = lstParent;
    }

    /**
     * Gets the code groupe.
     *
     * @return Returns the codeGroupe.
     */
    public String getCodeGroupe() {
        return codeGroupe;
    }

    /**
     * Sets the code groupe.
     *
     * @param codeGroupe
     *            The codeGroupe to set.
     */
    public void setCodeGroupe(String codeGroupe) {
        this.codeGroupe = codeGroupe;
    }

    /**
     * Gets the groupeAncestors.
     *
     * @return Returns the groupe's ancestors codes.
     */
    public Set<String> getGroupeAncestors() {
        return groupeAncestors;
    }


    /**
     * sets the groupeAncestors.
     *
     */
    public void setGroupeAncestors(final Set<String> ancestors) {
        this.groupeAncestors = ancestors;
    }

    /**
     * Gets the code rubrique.
     *
     * @return Returns the codeRubrique.
     */
    public String getCodeRubrique() {
        return codeRubrique;
    }

    /**
     * Sets the code rubrique.
     *
     * @param codeRubrique
     *            The codeRubrique to set.
     */
    public void setCodeRubrique(String codeRubrique) {
        this.codeRubrique = codeRubrique;
    }

    /**
     * Gets the rubrique's ancestors.
     *
     * @return Returns the Rubrique's Ancestors codes.
     */
    public Set<String> getRubriqueAncestors() {
        return rubriqueAncestors;
    }

    /**
     * sets the rubriqueAncestors.
     *
     */
    public void setRubriqueAncestors(final Set<String> ancestors) {
        this.rubriqueAncestors = ancestors;
    }

    /**
     * Gets the code structure.
     *
     * @return Returns the codeStructure.
     */
    public String getCodeStructure() {
        return codeStructure;
    }

    /**
     * Sets the code structure.
     *
     * @param codeStructure
     *            The codeStructure to set.
     */
    public void setCodeStructure(String codeStructure) {
        this.codeStructure = codeStructure;
    }

    /**
     * Gets the structure's ancestors.
     *
     * @return Returns the Structure's Ancestors codes.
     */
    public Set<String> getStructureAncestors() {
        return structureAncestors;
    }

    /**
     * sets the structureAncestors.
     *
     */
    public void setStructureAncestors(final Set<String> ancestors) {
        this.structureAncestors = ancestors;
    }

    /* (non-Javadoc)
    * @see java.lang.Object#equals(java.lang.Object)
    */
    @Override
    public boolean equals(Object o) {
        return chaineSerialisee.equals(o.toString());
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (hashCodeChaineSerialisee == 0) {
            hashCodeChaineSerialisee = chaineSerialisee.hashCode();
        }
        return hashCodeChaineSerialisee;
    }

    /**
     * Gets the niveau perimetre.
     *
     * @param _perimetre
     *            the _perimetre
     *
     * @return the niveau perimetre
     *
     */
    public int getNiveauPerimetre(NiveauPerimetre _perimetre) {
        if (_perimetre.equals(this)) {
            return EGAL;
        }
        /*
         * Gestion des rubriques
         */
        int niveauRubrique = DIFFERENT;
        String codeRubriquePerimetre = _perimetre.getCodeRubrique();
        if (codeRubriquePerimetre.equals(codeRubrique)) {
            niveauRubrique = EGAL;
        } else if ("".equals(codeRubriquePerimetre)) {
            niveauRubrique = DESSOUS;
        } else if ("".equals(codeRubrique)) {
            niveauRubrique = DESSUS;
        } else if ("-".equals(codeRubrique)) {
            niveauRubrique = DIFFERENT;
        } else if ("-".equals(codeRubriquePerimetre)) {
            niveauRubrique = DIFFERENT;
        } else {
            //Prebuild rubrique's ancestors if null
            if ( rubriqueAncestors==null ) {
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                final List<RubriqueBean> listeRubriqueParente = serviceRubrique.getAllAscendant(codeRubrique);
                rubriqueAncestors = new HashSet<>();
                for (RubriqueBean rubrique : listeRubriqueParente) {
                    rubriqueAncestors.add(rubrique.getCode());
                }
            }
            Set<String> rubriqueAncestorsPerimetre = _perimetre.getRubriqueAncestors();
            if ( rubriqueAncestorsPerimetre==null ) {
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                final List<RubriqueBean> listeRubriqueParente = serviceRubrique.getAllAscendant(codeRubriquePerimetre);
                rubriqueAncestorsPerimetre = new HashSet<>();
                for (RubriqueBean rubrique : listeRubriqueParente) {
                	rubriqueAncestorsPerimetre.add(rubrique.getCode());
                }
                _perimetre.setRubriqueAncestors(rubriqueAncestorsPerimetre);
            }
            if ( rubriqueAncestors.contains(codeRubriquePerimetre) ) {
                 niveauRubrique = DESSOUS;
             } else if ( rubriqueAncestorsPerimetre.contains(codeRubrique) ) {
                 niveauRubrique = DESSUS;
             }
        }
        if (niveauRubrique == DIFFERENT) {
            return DIFFERENT;
        }
        /*
         * Gestion des structures
         */
        int niveauStructure = DIFFERENT;
        String codeStructurePerimetre = _perimetre.getCodeStructure();
        if (codeStructurePerimetre.equals(codeStructure)) {
            niveauStructure = EGAL;
        } else if ("".equals(codeStructurePerimetre)) {
            niveauStructure = DESSOUS;
        } else if ("".equals(codeStructure)) {
            niveauStructure = DESSUS;
        } else if ("-".equals(codeStructure)) {
            niveauStructure = DIFFERENT;
        } else if ("-".equals(codeStructurePerimetre)) {
            niveauStructure = DIFFERENT;
        } else {
            //Prebuild structure's ancestors if null
            if ( structureAncestors==null ) {
                final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
                final List<StructureModele>  listeStructureParente = serviceStructure.getAllAscendant(codeStructure, LangueUtil.getIndiceLocaleDefaut());
                structureAncestors = new HashSet<>();
                for (StructureModele structure : listeStructureParente) {
                    structureAncestors.add(structure.getCode());
                }
            }
            Set<String> structureAncestorsPerimetre = _perimetre.getStructureAncestors();
            if ( structureAncestorsPerimetre==null ) {
                final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
                final List<StructureModele>  listeStructureParente = serviceStructure.getAllAscendant(codeStructurePerimetre, LangueUtil.getIndiceLocaleDefaut());
                structureAncestorsPerimetre = new HashSet<>();
                for (StructureModele structure : listeStructureParente) {
                	structureAncestorsPerimetre.add(structure.getCode());
                }
                _perimetre.setStructureAncestors(structureAncestorsPerimetre);
            }

            if ( structureAncestors.contains(codeStructurePerimetre) ) {
                niveauStructure = DESSOUS;
            } else if ( structureAncestorsPerimetre.contains(codeStructure) ) {
            	niveauStructure = DESSUS;
            }
        }
        if (niveauStructure == DIFFERENT) {
            return DIFFERENT;
        }
        int niveauPerimetre = EGAL;
        if (niveauRubrique == niveauStructure) {
            niveauPerimetre = niveauRubrique;
        } else {
            if (niveauRubrique == EGAL) {
                niveauPerimetre = niveauStructure;
            } else if (niveauStructure == EGAL) {
                niveauPerimetre = niveauRubrique;
            } else {
                return DIFFERENT;
            }
        }
        /*
         * Gestion groupes
         */
        int niveauGroupes = DIFFERENT;
        String codeGroupePerimetre = _perimetre.getCodeGroupe();
        if (codeGroupePerimetre.equals(codeGroupe)) {
            niveauGroupes = EGAL;
        } else if (codeGroupePerimetre.length() == 0) {
            niveauGroupes = DESSOUS;
        } else if (codeGroupe.length() == 0) {
            niveauGroupes = DESSUS;
        } else if ("-".equals(codeGroupe)) {
            niveauGroupes = DIFFERENT;
        } else if ("-".equals(codeGroupePerimetre)) {
            niveauGroupes = DIFFERENT;
        } else {
            //Prebuild group's ancestors if null
            if ( groupeAncestors==null ) {
                final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
                final List<GroupeDsiBean>  listeGroupeParent = serviceGroupeDsi.getAllAscendant(codeGroupe);
                groupeAncestors = new HashSet<>();
                for (GroupeDsiBean groupe : listeGroupeParent) {
                    groupeAncestors.add(groupe.getCode());
                }
            }
            Set<String> groupeAncestorsPerimetre = _perimetre.getGroupeAncestors();
            if ( groupeAncestorsPerimetre==null ) {
                final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
                final List<GroupeDsiBean>  listeGroupeParent = serviceGroupeDsi.getAllAscendant(codeGroupePerimetre);
                groupeAncestorsPerimetre = new HashSet<>();
                for (GroupeDsiBean groupe : listeGroupeParent) {
                	groupeAncestorsPerimetre.add(groupe.getCode());
                }
                _perimetre.setGroupeAncestors(groupeAncestorsPerimetre);
            }

            if ( groupeAncestors.contains(codeGroupePerimetre) ) {
                niveauGroupes = DESSOUS;
            } else if ( groupeAncestorsPerimetre.contains(codeGroupe) ) {
            	niveauGroupes = DESSUS;
            }
        }
        if (niveauGroupes == DIFFERENT) {
            return DIFFERENT;
        }
        if (niveauGroupes == niveauPerimetre) {
            return niveauPerimetre;
        } else {
            if (niveauGroupes == EGAL) {
                return niveauPerimetre;
            } else if (niveauPerimetre == EGAL) {
                return niveauGroupes;
            } else {
                return DIFFERENT;
            }
        }
    }

    /**
     * Ajoute enfant.
     *
     * @param _enfant
     *            the _enfant
     */
    public void ajouteEnfant(NiveauPerimetre _enfant) {
        if (lstEnfant == null) {
            lstEnfant = new HashSet<>();
        }
        lstEnfant.add(_enfant);
    }

    /**
     * Ajoute parent.
     *
     * @param _parent
     *            the _parent
     */
    public void ajouteParent(NiveauPerimetre _parent) {
        if (lstParent == null) {
            lstParent = new HashSet<>();
        }
        lstParent.add(_parent);
    }

    /**
     * Gets the code espace collaboratif.
     *
     * @return Returns the codeEspaceCollaboratif.
     */
    public String getCodeEspaceCollaboratif() {
        return codeEspaceCollaboratif;
    }

    /**
     * Sets the code espace collaboratif.
     *
     * @param codeEspaceCollaboratif
     *            The codeEspaceCollaboratif to set.
     */
    public void setCodeEspaceCollaboratif(String codeEspaceCollaboratif) {
        this.codeEspaceCollaboratif = codeEspaceCollaboratif;
    }
}