/*
 * Created on 1 sept. 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.objetspartages.om;
// TODO: Auto-generated Javadoc

/**
 * The Class CriterePhoto.
 *
 * @author Administrateur
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 */
public class CriterePhoto {

    /** The code. */
    private String code = "";

    /** The largeur. */
    private int largeur = 600;

    /** The hauteur. */
    private int hauteur = 600;

    /** The poids. */
    private long poids = 30;

    /**
     * Gets the hauteur.
     *
     * @return Returns the hauteur.
     */
    public int getHauteur() {
        return hauteur;
    }

    /**
     * Sets the hauteur.
     *
     * @param hauteur
     *            The hauteur to set.
     */
    public void setHauteur(final int hauteur) {
        this.hauteur = hauteur;
    }

    /**
     * Gets the largeur.
     *
     * @return Returns the largeur.
     */
    public int getLargeur() {
        return largeur;
    }

    /**
     * Sets the largeur.
     *
     * @param largeur
     *            The largeur to set.
     */
    public void setLargeur(final int largeur) {
        this.largeur = largeur;
    }

    /**
     * Gets the poids.
     *
     * @return Returns the poids.
     */
    public long getPoids() {
        return poids;
    }

    /**
     * Sets the poids.
     *
     * @param poids
     *            The poids to set.
     */
    public void setPoids(final long poids) {
        this.poids = poids;
    }

    /**
     * Gets the code.
     *
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            The code to set.
     */
    public void setCode(final String code) {
        this.code = code;
    }
}
