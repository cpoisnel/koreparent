package com.univ.objetspartages.om;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

/**
 * Cette classe permet ce construire des groupe DSI dynamiquement selon les structures. C'est à dire que dans ces groupe dynamique de structure nous allons préciser les codes
 * structures afin de fabriquer un groupe. Tous les utilisateur rattachés à ces structures feront parti du groupe.<br/>
 * De plus ce groupe est synchronisable cela signifie que qu'il sera possible de synchroniser toutes les structures afin de former des groupes.
 *
 * @author Pierre Cosson
 */
public class GroupeDynamiqueDeStructure extends RequeteGroupeDynamiqueSynchronisable {

    /** Nom de la requete, on peut retrouver ce nom dans le JTF. */
    private static final String NOM_REQUETE = "req_structure";

    private static final Logger LOG = LoggerFactory.getLogger(GroupeDynamiqueDeStructure.class);

    private final ServiceUser serviceUser;

    public GroupeDynamiqueDeStructure() {
        super();
        setNomRequete(NOM_REQUETE);
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.objetspartages.om.RequeteGroupeDynamique#estGereParReqDyn(java
     * .lang.String)
     */
    @Override
    protected boolean estGereParReqDyn(final String requeteGroupe) {
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.objetspartages.om.RequeteGroupeDynamique#getGroupesUtilisateur
     * (java.lang.String)
     */
    @Override
    protected Vector<String> getGroupesUtilisateur(final String codeUtilisateur) {
        LOG.debug("Début de requete dynamique de structure : getGroupesUtilisateur (" + codeUtilisateur + ")");
        final Vector<String> vGroupes = new Vector<>();
        final UtilisateurBean user = serviceUser.getByCode(codeUtilisateur);
        if (user != null && StringUtils.isNotEmpty(user.getCodeRattachement())) {
            final String codeRattachement = user.getCodeRattachement();
            final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
            for (GroupeDsiBean groupeDsi : serviceGroupeDsi.getByLdapRequest(codeRattachement)) {
                vGroupes.add(groupeDsi.getCode());
            }
        }
        return vGroupes;
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.RequeteGroupeDynamique#getVecteurUtilisateurs(java.lang.String)
     */
    @Override
    protected Vector<String> getVecteurUtilisateurs(final String codeGroupe) throws Exception {
        LOG.debug("Debut de requete dynamique : getVecteurUtilisateur (" + codeGroupe + ")");
        final Vector<String> vUtilisateurs = new Vector<>();
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        GroupeDsiBean groupeDsiBean = serviceGroupeDsi.getByCode(codeGroupe);
        if (groupeDsiBean != null) {
            final HashSet<String> hsUtilisateurs = new HashSet<>();
            /* dans ce groupe, on récupère la liste des structures
             * sélectionnés via le contenu du champ REQUETE_LDAP */
            final StringTokenizer stCodesStructures = new StringTokenizer(groupeDsiBean.getRequeteLdap(), ";");
            //pour chaque structure selectionnée, on récupère la liste des membres directement rattachés à cette structure
            while (stCodesStructures.hasMoreTokens()) {
                final List<UtilisateurBean> users = serviceUser.select("", "", "", "", "", "", stCodesStructures.nextToken(), "");
                if (CollectionUtils.isNotEmpty(users)) {
                    for (UtilisateurBean currentUser : users) {
                        hsUtilisateurs.add(currentUser.getCode());
                    }
                }
            }
            vUtilisateurs.addAll(hsUtilisateurs);
        }
        LOG.debug("Fin de requete dynamique : getVecteurUtilisateur (" + codeGroupe + ")");
        return vUtilisateurs;
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.RequeteGroupeDynamiqueSynchronisable#getListeCodeObjetASynchroniser()
     */
    @Override
    protected Map<String, SynchronisationGroupeDynamiqueData> getDataToSynchronize() throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        SynchronisationGroupeDynamiqueData syncData;
        final Collection<StructureModele> listeStructure = serviceStructure.getSubStructures(StringUtils.EMPTY, getCurrentLanguage(), true);
        if (listeStructure != null) {
            final Map<String, SynchronisationGroupeDynamiqueData> returnMap = new HashMap<>();
            final Iterator<StructureModele> codesStructures = listeStructure.iterator();
            String codeStructure;
            while (codesStructures.hasNext()) {
                final StructureModele structure = codesStructures.next();
                codeStructure = structure.getCode();
                if (codeStructure != null) {
                    syncData = new SynchronisationGroupeDynamiqueData(codeStructure, structure.getLibelleAffichable());
                    syncData.setCodeDuParent(structure.getCodeRattachement());
                    returnMap.put(codeStructure, syncData);
                }
            }
            return returnMap;
        } else {
            return null;
        }
    }

    @Override
    public void preparerPRINCIPAL(final InfoBean infoBean, final GroupeDsiBean groupe) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        if (groupe.getRequeteGroupe().length() > 0 && groupe.getRequeteGroupe().equals(this.getNomRequete())) {
            infoBean.set("REQUETE_LDAP_" + groupe.getRequeteGroupe(), groupe.getRequeteLdap());
            // Groupe de structure
            infoBean.set("REQUETE_LDAP_" + this.getNomRequete(), groupe.getRequeteLdap());
            String libellesStructure = "";
            final StringTokenizer stCodesStructures = new StringTokenizer(groupe.getRequeteLdap(), ";");
            String langue = getCurrentLanguage();
            while (stCodesStructures.hasMoreTokens()) {
                libellesStructure += serviceStructure.getDisplayableLabel(stCodesStructures.nextToken(), langue) + ";";
            }
            infoBean.set("LIBELLE_REQUETE_LDAP_" + this.getNomRequete(), libellesStructure);
            infoBean.set("INFOBULLE_REQUETE_LDAP_" + this.getNomRequete(), libellesStructure);
        }
    }

    private String getCurrentLanguage() {
        ContexteUniv ctx = ContexteUtil.getContexteUniv();
        Locale currentLocale = LangueUtil.getDefaultLocale();
        if (ctx != null) {
            currentLocale = ctx.getLocale();
        }
        return LangueUtil.getLangueLocale(currentLocale);
    }
}
