package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * The Class InfosRole.
 *
 * @author jean-sébastien steux
 *
 *         Stockage des rôles en mémoire
 */
public class InfosRole implements Serializable {

    private static final long serialVersionUID = 7428195403872271772L;

    /** The code. */
    private String code = "";

    /** The intitule. */
    private String intitule = "";

    /** The permissions. */
    private String permissions = "";

    /** The perimetre. */
    private String perimetre = "";

    /** The v permissions. */
    private Vector<PermissionBean> vPermissions = null;

    /**
     * Constructeur vide.
     */
    public InfosRole() {
        calculerVecteurPermissions();
    }

    /**
     * The Constructor.
     *
     * @param code
     *            the code
     * @param intitule
     *            the intitule
     * @param permissions
     *            the permissions
     * @param perimetre
     *            the perimetre
     */
    public InfosRole(final String code, final String intitule, final String perimetre, final String permissions) {
        this.code = code;
        this.intitule = intitule;
        this.permissions = permissions;
        this.perimetre = perimetre;
        calculerVecteurPermissions();
    } /**
     * The Constructor.
     *
     * @param code
     *            the code
     * @param intitule
     *            the intitule
     * @param permissions
     *            the permissions
     * @param perimetre
     *            the perimetre
     */
    public InfosRole(final String code, final String intitule, final String perimetre, final Collection<PermissionBean> permissions) {
        this.code = code;
        this.intitule = intitule;
        this.vPermissions = new Vector<>(permissions);
        this.perimetre = perimetre;
    }

    /**
     * Gets the code.
     *
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            The code to set.
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Gets the intitule.
     *
     * @return Returns the intitule.
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Sets the intitule.
     *
     * @param intitule
     *            The intitule to set.
     */
    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    /**
     * Gets the perimetre.
     *
     * @return Returns the perimetre.
     */
    public String getPerimetre() {
        return perimetre;
    }

    /**
     * Sets the perimetre.
     *
     * @param perimetre
     *            The perimetre to set.
     */
    public void setPerimetre(final String perimetre) {
        this.perimetre = perimetre;
    }

    /**
     * Gets the permissions.
     *
     * @return Returns the permissions.
     * @deprecated les permissions sont stocké dans {@link InfosRole#getVecteurPermissions()}
     */
    @Deprecated
    public String getPermissions() {
        return permissions;
    }

    /**
     * Sets the permissions.
     *
     * @param permissions
     *            The permissions to set.
     */
    public void setPermissions(final String permissions) {
        this.permissions = permissions;
    }

    /**
     * Renvoie la liste des permissions sous forme de vecteur.
     *
     * @return Returns the vPermissions.
     */
    public Vector<PermissionBean> getVecteurPermissions() {
        return vPermissions;
    }

    /**
     * Calculer vecteur permissions.
     */
    private void calculerVecteurPermissions() {
        vPermissions = new Vector<>();
        final StringTokenizer st = new StringTokenizer(getPermissions(), "[]");
        while (st.hasMoreTokens()) {
            vPermissions.add(new PermissionBean(st.nextToken()));
        }
    }
}
