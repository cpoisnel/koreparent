package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;

/**
 * Classe permettant de stocker en mémoire les informations relatives à une rubrique.
 */
public class InfosRubriques implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8136833859411710834L;

    /** The id rubrique. */
    private Long idRubrique = (long) 0;

    /** The code. */
    private String code = StringUtils.EMPTY;

    /** The code rubrique mere. */
    private String codeRubriqueMere = StringUtils.EMPTY;

    /** The langue. */
    private String langue = "0";

    /** The type rubrique. */
    private String typeRubrique = StringUtils.EMPTY;

    /** The page accueil. */
    private String pageAccueil = StringUtils.EMPTY;

    /** The intitule. */
    private String intitule = StringUtils.EMPTY;

    /** The onglet. */
    private String onglet = StringUtils.EMPTY;

    /** The accroche. */
    private String accroche = StringUtils.EMPTY;

    /** The contact. */
    private String contact = StringUtils.EMPTY;

    /** The couleur fond. */
    private String couleurFond = StringUtils.EMPTY;

    /** The couleur titre. */
    private String couleurTitre = StringUtils.EMPTY;

    /** The encadre. */
    private String encadre = StringUtils.EMPTY;

    private long idMediaBandeau = 0l;

    /** The ordre. */
    private String ordre = StringUtils.EMPTY;

    /** The gestion encadre. */
    private String gestionEncadre = StringUtils.EMPTY;

    /** The categorie. */
    private String categorie = StringUtils.EMPTY;

    /** The encadre sous rubrique. */
    private boolean encadreSousRubrique = false;

    /** The groupes dsi. */
    private Set<String> groupesDsi = new HashSet<>();

    private long idMediaPicto = 0l;

    /**
     * Constructeur vide.
     *
     * @param code
     *            the code
     */
    public InfosRubriques(final String code) {
        this.code = code;
    }

    /**
     * Constructeur.
     *
     * @param rubrique
     *            La rubrique à partir de laquelle on construit l'objet InfosRubriques
     * @deprecated utiliser le constructeur avec un RubriqueBean à la place. L'objet Rubrique va être déprécié
     */
    @Deprecated
    public InfosRubriques(final Rubrique rubrique) {
        this(rubrique.getPersistenceBean());
    }

    /**
     * Constructeur.
     *
     * @param rubrique
     *            La rubrique à partir de laquelle on construit l'objet InfosRubriques
     */
    public InfosRubriques(final RubriqueBean rubrique) {
        this.idRubrique = rubrique.getIdRubrique();
        this.code = rubrique.getCode();
        this.codeRubriqueMere = rubrique.getCodeRubriqueMere();
        this.langue = rubrique.getLangue();
        this.typeRubrique = rubrique.getTypeRubrique();
        this.pageAccueil = rubrique.getPageAccueil();
        this.intitule = rubrique.getIntitule();
        this.onglet = rubrique.getNomOnglet();
        this.accroche = rubrique.getAccroche();
        this.contact = rubrique.getContact();
        this.couleurTitre = rubrique.getCouleurTitre();
        this.couleurFond = rubrique.getCouleurFond();
        this.encadre = rubrique.getEncadre();
        this.ordre = rubrique.getOrdre();
        this.groupesDsi = Chaine.getHashSetAccolades(rubrique.getGroupesDsi());
        this.gestionEncadre = rubrique.getGestionEncadre();
        this.encadreSousRubrique = "1".equals(rubrique.getEncadreSousRubrique());
        this.categorie = rubrique.getCategorie();
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the code rubrique mere.
     *
     * @return the code rubrique mere
     */
    public String getCodeRubriqueMere() {
        return codeRubriqueMere;
    }

    /**
     * Gets the langue.
     *
     * @return the langue
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Gets the gestion encadre.
     *
     * @return the gestion encadre
     */
    public String getGestionEncadre() {
        return gestionEncadre;
    }

    /**
     * Gets the intitule.
     *
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Traitement de l'ordre des rubriques avec numero d'ordre + separateur ex : 01#intitule.
     *
     * @param strSeparateur
     *            : caractère séparateur
     *
     * @return the intitule
     * @deprecated l'ordre des rubriques est géré via un champ dédié depuis la v5
     */
    @Deprecated
    public String getIntitule(final String strSeparateur) {
        String strRes = StringUtils.EMPTY;
        if (intitule.contains(strSeparateur)) {
            strRes = intitule.substring(intitule.indexOf(strSeparateur) + 1);
        } else {
            strRes = intitule;
        }
        return strRes;
    }

    /**
     * Gets the niveau.
     *
     * @return the niveau
     */
    public int getNiveau() {
        int result = 0;
        if (StringUtils.isNotBlank(getCode()) && !getCode().equals(ServiceRubrique.CODE_RUBRIQUE_ROOT)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            result = serviceRubrique.getLevelFromParentCode(getCodeRubriqueMere());
        }
        return result;
    }

    /**
     * Gets the onglet.
     *
     * @return the onglet
     */
    public String getOnglet() {
        return onglet;
    }

    /**
     * Gets the contact.
     *
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * Renvoie la rubrique mère de cette rubrique.
     *
     * @return La rubrique mère de cette rubrique
     */
    public InfosRubriques getRubriqueMere() {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(getCodeRubriqueMere());
        InfosRubriques rubriqueMere = new InfosRubriques("");
        if (rubriqueBean != null) {
            rubriqueMere = new InfosRubriques(rubriqueBean);
        }
        return rubriqueMere;
    }

    /**
     * Gets the type rubrique.
     *
     * @return the type rubrique
     */
    public String getTypeRubrique() {
        return typeRubrique;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:13:49)
     *
     * @return String
     */
    public String getCouleurFond() {
        return couleurFond;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:13:28)
     *
     * @return String
     */
    public String getCouleurTitre() {
        return couleurTitre;
    }

    /**
     * Gets the encadre.
     *
     * @return the encadre
     */
    public String getEncadre() {
        return encadre;
    }

    /**
     * Renvoie true si l'encadré est affichable dans les sous-rubriques.
     *
     * @return true, if checks if is encadre sous rubrique
     */
    public boolean isEncadreSousRubrique() {
        return encadreSousRubrique;
    }

    /**
     * Gets the id rubrique.
     *
     * @return the id rubrique
     */
    public Long getIdRubrique() {
        return idRubrique;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:12:34)
     *
     * @return String
     * @deprecated ne se met pas à jour lors de la modification d'un média
     */
    @Deprecated
    public String getUrlBandeau() {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        return MediaUtils.getUrlAbsolue(serviceMedia.getById(getIdMediaBandeau()));
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:12:34)
     *
     * @param urlBandeau
     *            the url bandeau
     * @deprecated ne se met pas à jour lors de la modification d'un média
     */
    @Deprecated
    public void setUrlBandeau(final String urlBandeau) {
        // le paramètre ne sert plus
    }

    /**
     * Gets the accroche.
     *
     * @return Returns the accroche.
     */
    public String getAccroche() {
        return accroche;
    }

    /**
     * Gets the ordre.
     *
     * @return Returns the ordre.
     */
    public String getOrdre() {
        return ordre;
    }

    /**
     * Gets the groupes dsi.
     *
     * @return the groupes dsi
     */
    public Set<String> getGroupesDsi() {
        return groupesDsi;
    }

    /**
     * Gets the libelle affichable.
     *
     * @return the libelle affichable
     */
    public String getLibelleAffichable() {
        return intitule;
    }

    /**
     * Renvoie la liste triée sur l'ordre des sous-rubriques de cette rubrique.
     *
     * @return La liste triée sur l'ordre des sous-rubriques de cette rubrique
     */
    public Collection<InfosRubriques> getListeSousRubriques() {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final List<RubriqueBean> sousRubriques = serviceRubrique.getRubriqueByCodeParent(getCode());
        final Collection<InfosRubriques> resultats = new ArrayList<>();
        for  (RubriqueBean current : sousRubriques) {
            resultats.add(new InfosRubriques(current));
        }
        return resultats;
    }

    /**
     * Renvoie la liste des sous-rubriques de cette rubrique tous niveaux confondus.
     *
     * @return La liste des sous-rubriques de cette rubrique tous niveaux confondus
     */
    public Collection<InfosRubriques> getListeSousRubriquesTousNiveaux() {
        final Collection<InfosRubriques> allSection = new ArrayList<>();
        for (final InfosRubriques infosRubriques : getListeSousRubriques()) {
            allSection.addAll(infosRubriques.getListeSousRubriquesTousNiveaux());
            allSection.add(infosRubriques);
        }
        return allSection;
    }

    /**
     * Renvoie la liste des sous-rubriques (sous forme d'instances de rubriques) d'une rubrique, ordonnée par l'ordre des rubriques + application de la DSI Si InfoRubrique vide
     * (new InfosRubrique()) on va chercher les rubriques principales.
     *
     * @param ctx
     *            the ctx
     *
     * @return : Vector : liste des rubriques filles. Dans l'instance, <b>seul le code et l'intitule sont renseignes.</b>
     *
     * @deprecated utiliser {@link InfosRubriques#getListeSousRubriquesFront(Set)}
     */
    @Deprecated
    public Collection<InfosRubriques> getListeSousRubriquesFront(final ContexteUniv ctx) {
        final Collection<InfosRubriques> lSousRubriquesFront = new ArrayList<>();
        final Collection<InfosRubriques> lSousRubriques = getListeSousRubriques();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        for (final InfosRubriques sousRubrique : lSousRubriques) {
            if (serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), sousRubrique.getCode()) && !"HIDDEN".equalsIgnoreCase(sousRubrique.getCategorie())) {
                lSousRubriquesFront.add(sousRubrique);
            }
        }
        return lSousRubriquesFront;
    }

    /**
     * Renvoie la liste des sous-rubriques (sous forme d'instances de rubriques) d'une rubrique, ordonnée par l'ordre des rubriques + application de la DSI Si InfoRubrique vide
     * (new InfosRubrique()) on va chercher les rubriques principales.
     *
     * @param listeGroupes
     *            l'ensemble des groupes de l'utilisateurs (pour avoir le même comportement que la méthode avec le ctx, utiliser ctx.getGroupesDsiAvecAscendants()
     *
     * @return : Vector : liste des rubriques filles. Dans l'instance, <b>seul le code et l'intitule sont renseignes.</b>
     *
     *
     */
    public Collection<InfosRubriques> getListeSousRubriquesFront(Set<String> listeGroupes) {
        final Collection<InfosRubriques> lSousRubriquesFront = new ArrayList<>();
        final Collection<InfosRubriques> lSousRubriques = getListeSousRubriques();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        for (final InfosRubriques sousRubrique : lSousRubriques) {
            if (serviceRubrique.controlerRestrictionRubrique(listeGroupes, sousRubrique.getCode()) && !"HIDDEN".equalsIgnoreCase(sousRubrique.getCategorie())) {
                lSousRubriquesFront.add(sousRubrique);
            }
        }
        return lSousRubriquesFront;
    }

    /**
     * Ajoute une sous-rubrique à la liste des sous-rubriques (triée sur l'ordre).
     *
     * @param sousRubrique
     *            Une sous-rubrique
     * @deprecated cette méthode ne sert plus. Elle ne rajoute plus de sous rubrique étant donné que cette liste est maintenant calculé dynamiquement
     */
    @Deprecated
    public void addSousRubrique(final InfosRubriques sousRubrique) {
       //plus utiles
    }

    /**
     * Rattache la rubrique à sa rubrique mère et met à jour son niveau et la liste des sous-rubriques de la rubrique mère.
     *
     * @param rubrique
     *            La rubrique mère
     * @deprecated cette méthode ne rattache plus de rubrique parent/enfant étant donné que c'est calculé dynamiquement.
     */
    @Deprecated
    public void rattacheA(final InfosRubriques rubrique) {
        //plus utiles
    }

    /**
     * Vérifie si cette rubrique contient la rubrique passée en paramètre (on teste si la rubrique passée en paramètre ou une de ses rubriques mère ne serait pas par hasard égale à
     * cette rubrique).
     *
     * @param rubrique
     *            the rubrique
     *
     * @return true, if contains
     */
    public boolean contains(InfosRubriques rubrique) {
        boolean inFamilia = false;
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        while (rubrique != null && !inFamilia) {
            if (rubrique.getCode().equals(this.getCode())) {
                inFamilia = true;
            } else {
                final RubriqueBean parent = serviceRubrique.getRubriqueByCode(rubrique.getCodeRubriqueMere());
                if (parent != null) {
                    rubrique = new InfosRubriques(parent);
                } else {
                    rubrique = null;
                }
            }
        }
        return inFamilia;
    }

    /**
     * Gets the categorie.
     *
     * @return Returns the categorie.
     */
    public String getCategorie() {
        return categorie;
    }

    public String getPageAccueil() {
        return pageAccueil;
    }

    /**
     * @deprecated l'url n'est pas mise à jour lors de la modification du média
     */
    @Deprecated
    public String getUrlPicto() {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        return MediaUtils.getUrlAbsolue(serviceMedia.getById(getIdMediaPicto()));
    }

    /**
     * @deprecated l'url n'est pas mise à jour lors de la modification du média
     * @param urlPicto ne sert plus
     */
    @Deprecated
    public void setUrlPicto(final String urlPicto) {
        // le paramètre ne sert plus
    }

    public long getIdMediaBandeau() {
        return idMediaBandeau;
    }

    public void setIdMediaBandeau(long idMediaBandeau) {
        this.idMediaBandeau = idMediaBandeau;
    }

    public long getIdMediaPicto() {
        return idMediaPicto;
    }

    public void setIdMediaPicto(long idMediaPicto) {
        this.idMediaPicto = idMediaPicto;
    }
}
