package com.univ.objetspartages.om;

public enum EtatFiche {
    BROUILLON("0001", true),
    A_VALIDER("0002", true),
    EN_LIGNE("0003", true),
    A_SUPPRIMER("0004", false),
    APERCU("0005", false),
    SAUVEGARDE_AUTO("0006", false),
    ARCHIVE("0007", false);

    private String etat;

    private boolean isFront;

    EtatFiche(final String codeEtat, final boolean isVisibleInFront) {
        etat = codeEtat;
        isFront = isVisibleInFront;
    }

    public static EtatFiche getEtatParCode(final String code) {
        for (final EtatFiche etatFiche : values()) {
            if (etatFiche.getEtat().equals(code)) {
                return etatFiche;
            }
        }
        return null;
    }

    /**
     * Vérifie que l'état passé en paramètre correspond à un état dit "stable" A_SUPPRIMER va être supprimer par le scansite APERCU est un état temporaire uniquement dispo lors de
     * l'apercu et supprimé tout de suite après SAUVEGARDE_AUTO n'est dispo qu'en édition et une fois la fiche sauvegarder il disparait
     *
     * @param code le code de l"état à vérifier
     * @return vrai si l'état est un état "stable" (ni sauvegarde auto, ni a supprimer, ni apercu, ni brouillon)
     */
    public static boolean isEtatPerene(final String code) {
        final EtatFiche etatCourant = EtatFiche.getEtatParCode(code);
        return isEtatPerene(etatCourant);
    }

    /**
     * Vérifie que l'état passé en paramètre correspond à un état dit "stable" A_SUPPRIMER va être supprimer par le scansite APERCU est un état temporaire uniquement dispo lors de
     * l'apercu et supprimé tout de suite après SAUVEGARDE_AUTO n'est dispo qu'en édition et une fois la fiche sauvegarder il disparait
     *
     * @param etat "état à vérifier
     * @return vrai si l'état est un état "stable" (ni sauvegarde auto, ni a supprimer, ni apercu, ni brouillon)
     */
    public static boolean isEtatPerene(final EtatFiche etat) {
        return BROUILLON.equals(etat) || A_VALIDER.equals(etat) || EN_LIGNE.equals(etat) || ARCHIVE.equals(etat);
    }

    public static boolean isAValider(final String code) {
        final EtatFiche etatCourant = EtatFiche.getEtatParCode(code);
        return A_VALIDER.equals(etatCourant);
    }

    public static boolean isEtatEnregistrable(final String code) {
        final EtatFiche etatCourant = EtatFiche.getEtatParCode(code);
        return !A_VALIDER.equals(etatCourant) && !SAUVEGARDE_AUTO.equals(etatCourant) && !A_SUPPRIMER.equals(etatCourant);
    }

    public String getEtat() {
        return etat;
    }

    @Override
    public String toString() {
        return etat;
    }

    public boolean isFront() {
        return isFront;
    }
}
