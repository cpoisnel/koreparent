/*
 * Created on 9 nov. 07
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.objetspartages.om;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.iptc.IptcDirectory;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.univ.mediatheque.Mediatheque;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.FileUtil;
import com.univ.utils.ImageInfo;
import com.univ.utils.PhotoUtil;

public class MediaPhoto extends SpecificMedia {

    /** The lst critere ATTRIBUT_LARGEUR. */
    public static final String ATTRIBUT_LARGEUR = "LARGEUR";

    /** The lst critere ATTRIBUT_HAUTEUR. */
    public static final String ATTRIBUT_HAUTEUR = "HAUTEUR";

    private static final Logger LOG = LoggerFactory.getLogger(MediaPhoto.class);

    /** The critere limite. */
    private static CriterePhoto critereLimite;

    /** The critere vignette. */
    private static CriterePhoto critereVignette;

    /** The lst critere photo. */
    private static ArrayList<Object> lstCriterePhoto;

    /**
     * Check criteres.
     */
    static {
        critereLimite = new CriterePhoto();
        critereVignette = new CriterePhoto();
        lstCriterePhoto = new ArrayList<>();
        String critere = PropertyHelper.getCoreProperty(MediathequeHelper.CRITERES_LIMITE_PROPERTIES_KEY);
        String[] lstLimite = critere.split("/", -2);
        critereLimite.setPoids(Long.parseLong(lstLimite[0]));
        critereLimite.setLargeur(Integer.parseInt(lstLimite[1]));
        critereLimite.setHauteur(Integer.parseInt(lstLimite[2]));
        LOG.debug(String.format("Critère limite construit : [ poids : %s, largeur : %s , hauteur : %s", lstLimite[0], lstLimite[1], lstLimite[2]));
        critere = PropertyHelper.getCoreProperty(MediathequeHelper.CRITERES_VIGNETTE_PROPERTIES_KEY);
        lstLimite = critere.split("/", -2);
        critereVignette.setCode(lstLimite[0]);
        critereVignette.setLargeur(Integer.parseInt(lstLimite[1]));
        critereVignette.setHauteur(Integer.parseInt(lstLimite[2]));
        LOG.debug(String.format("Critère vignette construit : [ code : %s, largeur : %s , hauteur : %s", lstLimite[0], lstLimite[1], lstLimite[2]));
        final String dimension = PropertyHelper.getCoreProperty(MediathequeHelper.CRITERES_DIMENSION_PROPERTIES_KEY);
        if (dimension != null) {
            final String[] lstDimension = dimension.split(";", -2);
            for (final String element : lstDimension) {
                critere = element;
                lstLimite = critere.split("/", -2);
                final CriterePhoto aCritere = new CriterePhoto();
                aCritere.setCode(lstLimite[0]);
                aCritere.setPoids(Long.parseLong(lstLimite[1]));
                aCritere.setLargeur(Integer.parseInt(lstLimite[2]));
                aCritere.setHauteur(Integer.parseInt(lstLimite[3]));
                lstCriterePhoto.add(aCritere);
            }
        }
        LOG.debug("Liste des critères photo (taille : %d) : " + lstCriterePhoto, lstCriterePhoto.size());
    }

    /**
     * Gets the critere limite.
     *
     * @return the critere limite
     */
    public static CriterePhoto getCritereLimite() {
        return critereLimite;
    }

    /**
     * Gets the critere vignette.
     *
     * @return the critere vignette
     */
    public static CriterePhoto getCritereVignette() {
        return critereVignette;
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.processus.MediaSpecific#setSpecificProperties(com.jsbsoft.jtf.core.InfoBean, java.util.Properties)
     */
    @Override
    public void setSpecificProperties(final InfoBean infoBean, final Properties properties) {
        String prop = "";
        if (infoBean.get(ATTRIBUT_LARGEUR) != null) {
            prop = infoBean.get(ATTRIBUT_LARGEUR).toString();
        }
        properties.setProperty(ATTRIBUT_LARGEUR, prop);
        prop = "";
        if (infoBean.get(ATTRIBUT_HAUTEUR) != null) {
            prop = infoBean.get(ATTRIBUT_HAUTEUR).toString();
        }
        properties.setProperty(ATTRIBUT_HAUTEUR, prop);
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.processus.MediaSpecific#prepareMedia(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.RessourceUniv)
     */
    @Override
    public void prepareMedia(final InfoBean infoBean, final MediaBean media) throws IOException {
        setContenuInfoBean(infoBean, media.getSpecificData());
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.processus.MediaSpecific#checkMedia(com.jsbsoft.jtf.core.InfoBean)
     */
    @Override
    public String checkMedia(final InfoBean infoBean, final UploadedFile file) throws Exception {
        // récupération du fichier uploadé
        // UploadedFile file = (UploadedFile) infoBean.get("MEDIA_FILE");
        File f = null;
        if (file != null) {
            f = file.getTemporaryFile();
        } else {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.FICHIER_SOURCE_OBLIGATOIRE"));
        }
        boolean check = false;
        final ImageInfo ii = new ImageInfo();
        final String path = f.getAbsolutePath();
        try (final FileInputStream fis = new FileInputStream(path);) {
            ii.setInput(fis);
            check = ii.check();
        } catch (final IOException e) {
            throw new ErreurApplicative(e.getMessage(), e);
        } finally {
            ii.close();
        }
        if (!check) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.FORMAT_IMAGE"));
        }
        infoBean.set("SOURCE", file.getContentFilename());
        infoBean.set("POIDS", f.length());
        if (StringUtils.isNotEmpty(file.getContentType())) {
            infoBean.set("FORMAT", file.getContentType());
        } else {
            String nomFichierUploade = file.getContentFilename();
            nomFichierUploade = StringUtils.defaultIfBlank(nomFichierUploade, f.getName());
            final String formatFichierUploade = Mediatheque.getInstance().getContentType(nomFichierUploade);
            infoBean.set("FORMAT", formatFichierUploade);
        }
        infoBean.set("LARGEUR", Integer.toString(ii.getWidth()));
        infoBean.set("HAUTEUR", Integer.toString(ii.getHeight()));
        infoBean.set("CRITERE", String.valueOf(lstCriterePhoto.size() + 1));
        infoBean.set("criteres", lstCriterePhoto);
        infoBean.set("HAUTEUR_PERSONNALISE", "");
        infoBean.set("LARGEUR_PERSONNALISE", "");
        LOG.debug("caractéristiques image : " + ii.getFormatName() + ", " + ii.getWidth() + " x " + ii.getHeight() + " pixels, " + ii.getBitsPerPixel() + " bits per pixel.");
        populateFromIptc(infoBean, f);
        return f.getAbsolutePath();
    }

    private void populateFromIptc(InfoBean infoBean, File f) throws ImageProcessingException, IOException {
        final Metadata metadata = ImageMetadataReader.readMetadata(f);
        final IptcDirectory iptcDirectory = metadata.getFirstDirectoryOfType(IptcDirectory.class);
        if(iptcDirectory != null) {
            infoBean.set("META_KEYWORDS", StringUtils.join(iptcDirectory.getKeywords(), " "));
            infoBean.set("TITRE", iptcDirectory.getString(IptcDirectory.TAG_HEADLINE));
            infoBean.set("LEGENDE", iptcDirectory.getString(IptcDirectory.TAG_HEADLINE));
            infoBean.set("DESCRIPTION", iptcDirectory.getString(IptcDirectory.TAG_CAPTION));
            infoBean.set("AUTEUR", iptcDirectory.getString(IptcDirectory.TAG_BY_LINE));
            infoBean.set("COPYRIGHT", iptcDirectory.getString(IptcDirectory.TAG_COPYRIGHT_NOTICE));
        }
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.processus.MediaSpecific#processMedia(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.RessourceUniv)
     */
    @Override
    public String processMedia(final InfoBean infoBean, final MediaBean media) throws Exception {
        String sPathFile = infoBean.getString(INFOBEAN_KEY_URL_RESSOURCE);
        final String extension = FileUtil.getExtension(media.getSource());
        boolean replace = false;
        // Traitement du fichier uploadé si nouveau
        if (sPathFile.equals(MediaUtils.getUrlAbsolue(media))) {
            replace = true;
            sPathFile = MediaUtils.getPathAbsolu(media);
        }
        if (infoBean.get("CRITERE") != null) {
            File f = new File(sPathFile);
            if (!f.exists()) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.FICHIER_SOURCE_PHOTO_OBLIGATOIRE"));
            }
            final int index = Integer.parseInt((String) infoBean.get("CRITERE"));
            final int largeur = Integer.parseInt(infoBean.getString("LARGEUR"));
            final int hauteur = Integer.parseInt(infoBean.getString("HAUTEUR"));
            int newLargeur = largeur;
            int newHauteur = hauteur;
            if (index < lstCriterePhoto.size()) {
                final CriterePhoto critere = (CriterePhoto) lstCriterePhoto.get(index);
                newLargeur = critere.getLargeur();
                newHauteur = critere.getHauteur();
                final double rapportHauteur = (double) newHauteur / (double) (hauteur);
                final double rapportLargeur = (double) newLargeur / (double) (largeur);
                if (newLargeur == 0) {
                    newLargeur = (int) (largeur * rapportHauteur);
                } else if (newHauteur == 0) {
                    newHauteur = (int) (hauteur * rapportLargeur);
                } else if (rapportLargeur < rapportHauteur) {
                    newHauteur = (int) (hauteur * rapportLargeur);
                } else {
                    newLargeur = (int) (largeur * rapportHauteur);
                }
                f = PhotoUtil.resize(sPathFile, extension, newLargeur, newHauteur, replace);
            }
            // traitement personnalisé
            else if (index == lstCriterePhoto.size()) {
                newHauteur = (Integer) infoBean.get("HAUTEUR_PERSONNALISE");
                newLargeur = (Integer) infoBean.get("LARGEUR_PERSONNALISE");
                if ((newHauteur == 0) && (newLargeur == 0)) {
                    throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.TAILLE_REDIMENSION"));
                }
                final double rapportHauteur = (double) newHauteur / (double) (hauteur);
                final double rapportLargeur = (double) newLargeur / (double) (largeur);
                if (newLargeur == 0) {
                    newLargeur = (int) (largeur * rapportHauteur);
                } else if (newHauteur == 0) {
                    newHauteur = (int) (hauteur * rapportLargeur);
                } else if (rapportLargeur < rapportHauteur) {
                    newHauteur = (int) (hauteur * rapportLargeur);
                } else {
                    newLargeur = (int) (largeur * rapportHauteur);
                }
                f = PhotoUtil.resize(sPathFile, extension, newLargeur, newHauteur, replace);
            }
            infoBean.set(ATTRIBUT_LARGEUR, Integer.toString(newLargeur));
            infoBean.set(ATTRIBUT_HAUTEUR, Integer.toString(newHauteur));
            infoBean.set("WIDTH", Integer.toString(newLargeur));
            infoBean.set("HEIGHT", Integer.toString(newHauteur));
            media.setSpecificData(getPropertiesAsString(infoBean));
            sPathFile = f.getAbsolutePath();
            media.setPoids(f.length());
            // critères d'affichage
            if (newLargeur > critereLimite.getLargeur()) {
                newLargeur = critereLimite.getLargeur();
            }
            if (newHauteur > critereLimite.getHauteur()) {
                newHauteur = critereLimite.getHauteur();
            }
            infoBean.set(ATTRIBUT_LARGEUR, Integer.toString(newLargeur));
            infoBean.set(ATTRIBUT_HAUTEUR, Integer.toString(newHauteur));
            final Metadata metadata = ImageMetadataReader.readMetadata(f);
            final IptcDirectory iptcDirectory = metadata.getFirstDirectoryOfType(IptcDirectory.class);
            if(iptcDirectory != null) {
                media.setDateCreation(iptcDirectory.getDate(IptcDirectory.TAG_DATE_CREATED));
            }
        } else {
            sPathFile = MediaUtils.getPathAbsolu(media);
        }
        if (infoBean.get("TYPE_MEDIA_" + getCode().toUpperCase()) != null && !"0000".equals(infoBean.getString("TYPE_MEDIA_" + getCode().toUpperCase()))) {
            media.setTypeMedia(infoBean.getString("TYPE_MEDIA_" + getCode().toUpperCase()));
        }
        return sPathFile;
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.MediaSpecific#processVignette(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.RessourceUniv)
     */
    @Override
    public String processVignette(final InfoBean infoBean, final MediaBean media) throws Exception {
        String sPathFile = infoBean.getString(INFOBEAN_KEY_URL_RESSOURCE);
        // si la source a change
        if (!sPathFile.equals(MediaUtils.getUrlAbsolue(media))) {
            File f = new File(sPathFile);
            if (!f.exists()) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.FICHIER_SOURCE_PHOTO_OBLIGATOIRE"));
            }
            // si la vignette existante a bien ete generee automatiquement a la base
            if (media.getUrlVignette().endsWith(media.getUrl())) {
                final String extension = FileUtil.getExtension(media.getSource());
                f = PhotoUtil.resize(f.getAbsolutePath(), extension, critereVignette.getLargeur(), critereVignette.getHauteur(), false);
                sPathFile = f.getAbsolutePath();
            }
        } else {
            sPathFile = MediaUtils.getPathVignetteAbsolu(media);
        }
        return sPathFile;
    }
}
