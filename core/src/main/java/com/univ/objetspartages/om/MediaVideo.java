package com.univ.objetspartages.om;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kportal.core.config.MessageHelper;
import com.univ.mediatheque.Mediatheque;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.FileUtil;

public class MediaVideo extends SpecificMedia {

    /** The lst critere ATTRIBUT_LARGEUR. */
    public static final String ATTRIBUT_LARGEUR = "LARGEUR";

    /** The lst critere ATTRIBUT_HAUTEUR. */
    public static final String ATTRIBUT_HAUTEUR = "HAUTEUR";

    /* (non-Javadoc)
     * @see com.univ.objetspartages.processus.MediaSpecific#setSpecificProperties(com.jsbsoft.jtf.core.InfoBean, java.util.Properties)
     */
    @Override
    public void setSpecificProperties(final InfoBean infoBean, final Properties properties) {
        String prop = "";
        if (infoBean.get(ATTRIBUT_LARGEUR) != null) {
            prop = infoBean.get(ATTRIBUT_LARGEUR).toString();
        }
        properties.setProperty(ATTRIBUT_LARGEUR, prop);
        prop = "";
        if (infoBean.get(ATTRIBUT_HAUTEUR) != null) {
            prop = infoBean.get(ATTRIBUT_HAUTEUR).toString();
        }
        properties.setProperty(ATTRIBUT_HAUTEUR, prop);
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.processus.MediaSpecific#prepareMedia(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.RessourceUniv)
     */
    @Override
    public void prepareMedia(final InfoBean infoBean, final MediaBean media) throws IOException {
        setContenuInfoBean(infoBean, media.getSpecificData());
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.processus.MediaSpecific#checkMedia(com.jsbsoft.jtf.core.InfoBean)
     */
    @Override
    public String checkMedia(final InfoBean infoBean, final UploadedFile file) throws Exception {
        // récupération du fichier uploadé
        //UploadedFile file = (UploadedFile) infoBean.get("MEDIA_FILE");
        File f = null;
        if (file != null) {
            f = file.getTemporaryFile();
        } else {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.FICHIER_SOURCE_OBLIGATOIRE"));
        }
        boolean check = true;
        if (file.getContentType() != null && file.getContentType().length() > 0) {
            final String extension = FileUtil.getExtension(file.getContentFilename());
            if (!extensions.contains(extension)) {
                check = false;
            }
        }
        if (!check) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.FORMAT_FLV"));
        }
        infoBean.set("SOURCE", file.getContentFilename());
        infoBean.set("POIDS", f.length());
        if (StringUtils.isNotEmpty(file.getContentType())) {
            infoBean.set("FORMAT", file.getContentType());
        } else {
            String nomFichierUploade = file.getContentFilename();
            nomFichierUploade = StringUtils.defaultIfBlank(nomFichierUploade, f.getName());
            final String formatFichierUploade = Mediatheque.getInstance().getContentType(nomFichierUploade);
            infoBean.set("FORMAT", formatFichierUploade);
        }
        return f.getAbsolutePath();
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.processus.MediaSpecific#processMedia(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.RessourceUniv)
     */
    @Override
    public String processMedia(final InfoBean infoBean, final MediaBean media) throws Exception {
        String sPathFile = infoBean.getString(INFOBEAN_KEY_URL_RESSOURCE);
        // Traitement du fichier uploadé si nouveau
        if (!sPathFile.equals(MediaUtils.getUrlAbsolue(media))) {
            final File f = new File(sPathFile);
            if (!f.exists()) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.ERREUR.FICHIER_SOURCE_OBLIGATOIRE"));
            }
            sPathFile = f.getAbsolutePath();
            media.setPoids(f.length());
        } else {
            sPathFile = MediaUtils.getPathAbsolu(media);
        }
        if (infoBean.get("TYPE_MEDIA_" + getCode().toUpperCase()) != null && !"0000".equals(infoBean.getString("TYPE_MEDIA_" + getCode().toUpperCase()))) {
            media.setTypeMedia(infoBean.getString("TYPE_MEDIA_" + getCode().toUpperCase()));
        }
        media.setSpecificData(getPropertiesAsString(infoBean));
        return sPathFile;
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.MediaSpecific#processVignette(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.RessourceUniv)
     */
    @Override
    public String processVignette(final InfoBean infoBean, final MediaBean media) throws Exception {
        return MediaUtils.getPathVignetteAbsolu(media);
    }
}
