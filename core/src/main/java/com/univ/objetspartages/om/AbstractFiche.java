package com.univ.objetspartages.om;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.dao.AbstractFicheDAO;
import com.univ.utils.ExceptionFicheNonTrouvee;
import com.univ.utils.FicheUnivMgr;

public abstract class AbstractFiche<T extends AbstractFicheBean> extends AbstractOm<T, AbstractFicheDAO<T>> implements FicheUniv {

    protected transient OMContext ctx;

    @Override
    public void addWithForcedId() throws Exception {
        commonDao.addWithForcedId(persistenceBean);
    }

    /**
     * Relicas des anciens objets de kportal. Cette méthode sert uniquement pour les méthodes select avec 50 paramètres dessus ou pour la rétro compatibilité.
     * Le contexte ne sert dans aucun autre cas, il ne sert donc à rien d'initialiser ce paramètre pour faire des requêtes SQL.
     * @param ctx le contexte permettant de calculer les critères de recherches génériques à appliquer sur les méthodes select(...)
     */
    @Override
    public void setCtx(final OMContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public void retrieve() throws Exception {
        if(getIdFiche() != 0L) {
            persistenceBean = commonDao.getById(getIdFiche());
            if (persistenceBean == null) {
                throw new ExceptionFicheNonTrouvee("unable to find any content with this id");
            }
        }
    }

    @Override
    public int selectCodeLangueEtat(final String code, final String langue, final String etat) throws Exception {
        currentSelect = commonDao.selectCodeLangueEtat(code, langue, etat);
        currentSelectIt = currentSelect.iterator();
        return currentSelect.size();
    }

    public List<T> selectAllCodeLangueEtat(final String code, final String langue, final String etat) throws Exception {
        return commonDao.selectCodeLangueEtat(code, langue, etat);
    }

    /**
     * Renvoie la liste des fiches présentes dans la rubrique en paramètre
     * @param codeRubrique the code rubrique
     * @param langue the langue
     * @return Une liste de fiches
     * @throws Exception
     */
    @Override
    public int selectParCodeRubrique(final String codeRubrique, final String langue) throws Exception {
        currentSelect = commonDao.selectParCodeRubrique(codeRubrique, langue);
        currentSelectIt = currentSelect.iterator();
        return currentSelect.size();
    }

    /**
     * Renvoie la liste des fiches présentes dans la rubrique en paramètre
     * @param codeRubrique the code rubrique
     * @param langue the langue
     * @return Une liste de fiches
     * @throws Exception
     */
    public List<T> selectAllParCodeRubrique(final String codeRubrique, final String langue) throws Exception {
        return commonDao.selectParCodeRubrique(codeRubrique, langue);
    }

    @Override
    public void init() {
        FicheUnivMgr.init(this);
        persistenceBean.setId((long) 0);
        persistenceBean.setCode(StringUtils.EMPTY);
        persistenceBean.setTitre(StringUtils.EMPTY);
        persistenceBean.setLangue("0");
    }

    /**
     * Duplication dans une nouvelle langue, le nouvel objet devient l'objet courant.
     */
    @Override
    public void dupliquer(final String nouvelleLangue) throws Exception {
        // donnees a reinitialiser dans le nouvel objet
        FicheUnivMgr.dupliquer(this);
        // creation objet dans la langue demandee
        setIdFiche(0L);
        setLangue(nouvelleLangue);
        add();
        // recuperation
        selectCodeLangueEtat(getCode(), nouvelleLangue, "0001");
        nextItem();
    }

    @Override
    public Long getIdFiche() {
        return persistenceBean.getId();
    }

    @Override
    public void setIdFiche(final Long idFiche) {
        persistenceBean.setId(idFiche);
    }

    @Override
    public String getCodeRubrique() {
        return persistenceBean.getCodeRubrique();
    }

    @Override
    public void setCodeRubrique(final String codeRubrique) {
        persistenceBean.setCodeRubrique(codeRubrique);
    }

    @Override
    public String getCode() {
        return persistenceBean.getCode();
    }

    public String getTitre() {
        return persistenceBean.getTitre();
    }

    public void setTitre(final String titre) {
        persistenceBean.setTitre(titre);
    }

    @Override
    public String getLangue() {
        return persistenceBean.getLangue();
    }

    @Override
    public String getCodeRedacteur() {
        return persistenceBean.getCodeRedacteur();
    }

    @Override
    public String getCodeValidation() {
        return persistenceBean.getCodeValidation();
    }

    @Override
    public String getCodeRattachement() {
        return persistenceBean.getCodeRattachement();
    }

    @Override
    public String getEtatObjet() {
        return persistenceBean.getEtatObjet();
    }

    @Override
    public String getMessageAlerte() {
        return persistenceBean.getMessageAlerte();
    }

    @Override
    public String getMetaKeywords() {
        return persistenceBean.getMetaKeywords();
    }

    @Override
    public String getMetaDescription() {
        return persistenceBean.getMetaDescription();
    }

    @Override
    public String getTitreEncadre() {
        return persistenceBean.getTitreEncadre();
    }

    @Override
    public String getContenuEncadre() {
        return persistenceBean.getContenuEncadre();
    }

    @Override
    public String getEncadreRecherche() {
        return persistenceBean.getEncadreRecherche();
    }

    @Override
    public String getEncadreRechercheBis() {
        return persistenceBean.getEncadreRechercheBis();
    }

    @Override
    public Date getDateAlerte() {
        return persistenceBean.getDateAlerte();
    }

    @Override
    public Date getDateCreation() {
        return persistenceBean.getDateCreation();
    }

    @Override
    public Date getDateModification() {
        return persistenceBean.getDateModification();
    }

    @Override
    public Date getDateProposition() {
        return persistenceBean.getDateProposition();
    }

    @Override
    public Date getDateValidation() {
        return persistenceBean.getDateValidation();
    }

    @Override
    public Long getNbHits() {
        return persistenceBean.getNbHits();
    }

    @Override
    public void setCode(final String code) {
        persistenceBean.setCode(code);
    }

    @Override
    public void setLangue(final String langue) {
        persistenceBean.setLangue(langue);
    }

    @Override
    public void setCodeRedacteur(final String codeRedacteur) {
        persistenceBean.setCodeRedacteur(codeRedacteur);
    }

    @Override
    public void setCodeValidation(final String codeValidation) {
        persistenceBean.setCodeValidation(codeValidation);
    }

    @Override
    public void setCodeRattachement(final String codeRattachement) {
        persistenceBean.setCodeRattachement(codeRattachement);
    }

    @Override
    public void setEtatObjet(final String etatObjet) {
        persistenceBean.setEtatObjet(etatObjet);
    }

    @Override
    public void setMessageAlerte(final String messageAlerte) {
        persistenceBean.setMessageAlerte(messageAlerte);
    }

    @Override
    public void setMetaKeywords(final String metaKeywords) {
        persistenceBean.setMetaKeywords(metaKeywords);
    }

    @Override
    public void setMetaDescription(final String metaDescription) {
        persistenceBean.setMetaDescription(metaDescription);
    }

    @Override
    public void setTitreEncadre(final String titreEncadre) {
        persistenceBean.setTitreEncadre(titreEncadre);
    }

    @Override
    public void setContenuEncadre(final String contenuEncadre) {
        persistenceBean.setContenuEncadre(contenuEncadre);
    }

    @Override
    public void setEncadreRecherche(final String encadreRecherche) {
        persistenceBean.setEncadreRecherche(encadreRecherche);
    }

    @Override
    public void setEncadreRechercheBis(final String encadreRechercheBis) {
        persistenceBean.setEncadreRechercheBis(encadreRechercheBis);
    }

    @Override
    public void setDateAlerte(final Date dateAlerte) {
        persistenceBean.setDateAlerte(dateAlerte);
    }

    @Override
    public void setDateCreation(final Date dateCreation) {
        persistenceBean.setDateCreation(dateCreation);
    }

    @Override
    public void setDateModification(final Date dateModification) {
        persistenceBean.setDateModification(dateModification);
    }

    @Override
    public void setDateProposition(final Date dateProposition) {
        persistenceBean.setDateProposition(dateProposition);
    }

    @Override
    public void setDateValidation(final Date dateValidation) {
        persistenceBean.setDateValidation(dateValidation);
    }

    @Override
    public void setNbHits(final Long nbHits) {
        persistenceBean.setNbHits(nbHits);
    }
}
