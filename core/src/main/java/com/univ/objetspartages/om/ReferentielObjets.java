package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.cms.objetspartages.ReferentielManager;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.dao.AbstractFicheDAO;

/**
 * Contient les caractéristiques des objets spécifiques à chaque application.
 */
public class ReferentielObjets {

    /** The Constant LOG. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReferentielObjets.class);

    /**
     * Gets the objet by code.
     *
     * @param code
     *            the code
     * @return the objet by code
     */
    public static Objetpartage getObjetByCode(final String code) {
        return getReferentiel().getObjetsByCode().get(code);
    }

    /**
     * Gets the objet by nom.
     *
     * @param nom
     *            the nom
     * @return the objet by code
     */
    public static Objetpartage getObjetByNom(final String nom) {
        return getReferentiel().getObjetByNom(nom);
    }

    /**
     * Gets the referentiel.
     *
     * @return the referentiel
     */
    public static ReferentielManager getReferentiel() {
        return (ReferentielManager) ApplicationContextManager.getCoreContextBean(ReferentielManager.ID_BEAN);
    }

    /**
     * Indique si l'objet gere les liens de requete.
     *
     * @param code
     *            the code
     *
     * @return true, if gere lien requete
     *
     */
    public static boolean gereLienRequete(final String code) {
        try {
            final Class<?> classeObjet = Class.forName(getObjetByCode(code).getNomClasse());
            return FicheAnnotationHelper.isLienRequete(classeObjet.newInstance());
        }catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            LOGGER.debug("impossible d'instancier l'objet rechercher", e);
            return false;
        }
    }

    /**
     * Indique si l'objet gere les liens internes.
     *
     * @param _code
     *            the code
     *
     * @return true, if gere liens internes
     *
     * @throws Exception
     *             the exception
     */
    public static boolean gereLienInterne(final String _code) throws Exception {
        boolean res = true;
        final Class<?> classeObjet = Class.forName(getObjetByCode(_code).getNomClasse());
        if (!FicheAnnotationHelper.isLienInterne(classeObjet.newInstance())) {
            res = false;
        }
        return res;
    }

    /**
     * Indique si l'objet possède des encadres de recherche
     *
     * @param code
     *            le code de l'objet ficheuniv
     *
     * @return true, si gere liens internes
     *
     * @throws Exception
     *             the exception
     */
    public static boolean gereEncadreRecherche(final String code) throws Exception {
        final Class<?> classeObjet = Class.forName(getObjetByCode(code).getNomClasse());
        return FicheAnnotationHelper.isEncadreRechercheSurFicheCourante(classeObjet.newInstance());
    }

    /**
     * Indique si l'objet possède des encadres de recherche
     *
     * @param code
     *            le code de l'objet ficheuniv
     *
     * @return true, si gere liens internes
     *
     * @throws ReflectiveOperationException lors de l'instanciation de la fiche
     */
    public static boolean gereEncadreRechercheEmbarquable(final String code) throws ReflectiveOperationException {
        final Class<?> classeObjet = Class.forName(getObjetByCode(code).getNomClasse());
        return FicheAnnotationHelper.isEncadreRechercheEmbarquable(classeObjet.newInstance());
    }

    /**
     * calcul du code objet à partir de la classe.
     *
     * @param nomClasse
     *            the nom classe
     * @return the code objet par classe
     */
    public static String getCodeObjetParClasse(final String nomClasse) {
        return getReferentiel().getCodesObjetsByClasse().get(nomClasse);
    }

    /**
     * calcul du code à partir d'une instance de fiche.
     *
     * @param _ficheUniv
     *            the _fiche univ
     *
     * @return the code objet
     *
     */
    public static String getCodeObjet(final FicheUniv _ficheUniv) {
        final String classe = _ficheUniv.getClass().getName();
        return getCodeObjetParClasse(classe);
    }

    /**
     * Calcul du libelle de l'objet à partir du code ou nom.
     *
     * @param codeOrNom
     *            the code
     * @return the libelle objet
     */
    public static String getLibelleObjet(final String codeOrNom) {
        if (getObjetByCode(codeOrNom) != null) {
            return getObjetByCode(codeOrNom).getLibelleObjet();
        } else if (getObjetByNom(codeOrNom) != null) {
            return getObjetByNom(codeOrNom).getLibelleObjet();
        }
        return "";
    }

    /**
     * Détermine si le référentiel possède des objets de type structure.
     *
     * @return le nombre d'objets de type structure
     *
     */
    public static int getNombreObjetsTypeStructure() {
        return getReferentiel().getCodesObjetsStructure().size();
    }

    /**
     * Renvoie la liste des objets (key = nom, value = code).
     *
     * @return the liste objets
     *
     * @deprecated utiliser {@link ReferentielObjets#getObjetsPartagesTries()}
     */
    @Deprecated
    public static Hashtable<String, String> getListeObjets() {
        final Hashtable<String, String> res = new Hashtable<>();
        final List<Objetpartage> objetsPartages = getObjetsPartagesTries();
        for (final Objetpartage objet : objetsPartages) {
            res.put(objet.getCodeObjet(), objet.getLibelleObjet());
        }
        return res;
    }

    /**
     * Calcule la liste de l'ensemble des {@link Objetpartage} déclaré dans le projet et les retourne triés
     *
     * @return l'ensemble des objets partagés du projet triés sur leur ordre & leur label
     */
    public static List<Objetpartage> getObjetsPartagesTries() {
        return new ArrayList<Objetpartage>(getReferentiel().getObjetsTries());
    }

    /**
     * Calcul du nom de l'objet à partir du code.
     *
     * @param code
     *            the code
     * @return the nom objet
     */
    public static String getNomObjet(final String code) {
        if (getObjetByCode(code) != null) {
            return getObjetByCode(code).getNomObjet();
        }
        return "";
    }

    /**
     * Calcul du nom de l'objet à partir de la fiche.
     *
     * @param fiche
     *            la fiche
     * @return the nom objet
     */
    public static String getNomObjet(final FicheUniv fiche) {
        return getNomObjet(getCodeObjet(fiche));
    }

    /**
     * Calcul de la page à partir du code.
     *
     * @param code
     *            the code
     * @return the nom page
     */
    public static String getNomPage(final String code) {
        return getNomObjet(code);
    }

    /**
     * Calcul de la page à partir du code.
     *
     * @param codeObjet
     *            the code
     * @return the nom page
     */
    public static String getNomTableSql(final String codeObjet) {
        final Objetpartage objet = getObjetByCode(codeObjet);
        return StringUtils.substringAfterLast(objet.getNomClasse(), ".").toUpperCase();
    }

    /**
     * Calcul du processus à partir du code.
     *
     * @param code
     *            the code
     * @return the processus
     */
    public static String getProcessus(final String code) {
        String processus = StringUtils.EMPTY;
        final Objetpartage objet = getObjetByCode(code);
        if (objet != null) {
            processus = objet.getParametreProcessus();
        }
        return processus;
    }

    /**
     * Instanciation d'un objet à partir de son nom.
     *
     * @param nomOUcode
     *            le nom ou le code de la fiche à instancier
     * @return the fiche univ
     */
    public static FicheUniv instancierFiche(String nomOUcode) {
        nomOUcode = nomOUcode.toLowerCase();
        try {
            Objetpartage objet = getObjetByNom(nomOUcode);
            if (objet == null) {
                objet = getObjetByCode(nomOUcode);
            }
            if (objet != null) {
                return (FicheUniv) Class.forName(objet.getNomClasse()).newInstance();
            }
        } catch (final ReflectiveOperationException e) {
            LOGGER.warn("Erreur d'instanciation de la fiche " + nomOUcode, e);
        }
        return null;
    }

    /**
     * Instancie le dao lié à l'objet partagé fourni en paramètre
     * @param nomOuCode le nom ou le code de l'objet partagé dont on souhaite avoir le DAO
     * @param <T> le bean de persistance lié au dao
     * @return le dao ou null si non trouvé
     */
    public static <T extends AbstractFicheBean> AbstractFicheDAO<T> instancierFicheDao(final String nomOuCode) {
        Objetpartage objet = getObjetByNom(nomOuCode);
        if (objet == null) {
            objet = getObjetByCode(nomOuCode);
        }
        AbstractFicheDAO<T> dao = null;
        if (objet != null) {
            dao = (AbstractFicheDAO<T>) objet.getFicheDAO();
        }
        return dao;
    }

    /**
     * Instancie le dao lié à l'objet partagé fourni en paramètre
     * @param ficheUniv la fiche de l'objet partagé dont on souhaite avoir le DAO
     * @param <T> le bean de persistance lié au dao
     * @return le dao ou null si non trouvé
     */
    public static <T extends AbstractFicheBean> AbstractFicheDAO<T> instancierFicheDao(final FicheUniv ficheUniv) {
        final String nomObjet = getNomObjet(ficheUniv);
        final Objetpartage objet = getObjetByNom(nomObjet);
        AbstractFicheDAO<T> dao = null;
        if (objet != null) {
            dao = (AbstractFicheDAO<T>) objet.getFicheDAO();
        }
        return dao;
    }

    /**
     * Instanciation d'un objet à partir de son nom.
     *
     * @param nomOUcode
     *            le nom ou le code de l'objet à instancier
     * @return the fiche univ
     */
    public static Object instancierObjet(String nomOUcode) {
        nomOUcode = nomOUcode.toLowerCase();
        try {
            Objetpartage objet = getObjetByNom(nomOUcode);
            if (objet == null) {
                objet = getObjetByCode(nomOUcode);
            }
            if (objet != null) {
                return Class.forName(objet.getNomClasse()).newInstance();
            }
        } catch (final ReflectiveOperationException e) {
            LOGGER.error("Erreur d'instanciation de la fiche " + nomOUcode, e);
        }
        return null;
    }

    /**
     * Indique si l'objet est exportable en XML.
     *
     * @param code
     *            the code
     * @return true, if checks if is exportable
     * @throws Exception
     *             the exception
     */
    public static boolean isExportable(final String code) throws Exception {
        boolean res = false;
        final Class<?> classeObjet = Class.forName(getObjetByCode(code).getNomClasse());
        if (classeObjet.newInstance() instanceof FicheExportableXML) {
            res = true;
        }
        return res;
    }

    /**
     * Renvoi l'ordre des onglets cf BO, rôles etc
     *
     * @return the ordre onglets
     *
     */
    public static String[] getOrdreOnglets() {
        final List<String> var = getReferentiel().getCodesObjetsTries();
        return var.toArray(new String[var.size()]);
    }

    /**
     * Calcul du code de l'objet à partir du nom.
     *
     * @param nom
     *            the nom
     * @return the code objet
     */
    public static String getCodeObjet(final String nom) {
        return StringUtils.defaultIfEmpty(getReferentiel().getCodesObjetsByNom().get(nom.toLowerCase()), "");
    }

    /**
     * Calcul de la classe à parti du nom objet
     *
     * @param nomObjet
     *            the nom objet
     * @return the classe objet par nom
     */
    protected static String getClasseObjetParNom(final String nomObjet) {
        if (getObjetByNom(nomObjet) != null) {
            return getObjetByNom(nomObjet).getNomClasse();
        }
        return "";
    }

    /**
     * Calcul de la classe à parti du code objet
     *
     * @param codeObjet
     *            the code objet
     * @return the classe objet par nom
     */
    protected static String getClasseObjetParCode(final String codeObjet) {
        if (getObjetByCode(codeObjet) != null) {
            return getObjetByCode(codeObjet).getNomClasse();
        }
        return "";
    }

    /**
     * Calcul de la classe à parti du code OU du nom objet
     *
     * @param codeOuNomObjet
     *            the code ou nom objet
     * @return the classe objet par nom
     */
    public static String getClasseObjet(final String codeOuNomObjet) {
        String classeObjet = "";
        Objetpartage objet = getObjetByNom(codeOuNomObjet);
        if (objet == null) {
            objet = getObjetByCode(codeOuNomObjet);
        }
        if (objet != null) {
            classeObjet = objet.getNomClasse();
        }
        return classeObjet;
    }

    /**
     * Teste si l'objet supporte la recherche avancée (paramétrage via le jtf, ex : fiche.ACTUALITE.recherche_avancee=1)
     *
     * @param nomObjet
     *            L'objet qu'on veut rechercher
     *
     * @return true si on peut rechercher l'objet
     */
    public static boolean isObjetRecherchable(final String nomObjet) {
        boolean recherchable = false;
        final Objetpartage objet = getObjetByNom(nomObjet);
        if (objet != null) {
            recherchable = objet.isRecherchable();
        }
        return recherchable;
    }

    /**
     * Teste si l'objet supporte le collaboratif ex : fiche.ACTUALITE.collaboratif=1)
     *
     * @param nomObjet
     *            L'objet qu'on veut rechercher
     * @param strict ??!
     *
     * @return true si on peut ajouter l'objet comme service du collaboratif
     */
    public static boolean isObjetCollaboratif(final String nomObjet, final boolean strict) {
        boolean collaboratif = false;
        final Objetpartage objet = getObjetByNom(nomObjet);
        if (objet != null) {
            collaboratif = (strict ? objet.isStrictlyCollaboratif() : objet.isCollaboratif());
        }
        return collaboratif;
    }

    /**
     * Calcule la liste des {@link Objetpartage} faisant partie des espaces collaboratifs
     * @return les objets du collab ou une liste vide sinon
     */
    public static List<Objetpartage> getAllObjetCollaboratif() {
        final List<Objetpartage> objetCollaboratifs = getObjetsPartagesTries();
        CollectionUtils.filter(objetCollaboratifs, new Predicate() {

            @Override
            public boolean evaluate(final Object arg0) {
                return ((Objetpartage) arg0).isCollaboratif();
            }
        });
        return objetCollaboratifs;
    }

    /**
     * Teste si l'objet supporte la saisie des droits dans les roles.
     *
     * @param objet
     *            L'objet qu'on veut rechercher
     * @param action
     *            the action
     *
     * @return true par defaut, false si l'objet doit être filtré dans la saisie des rôles (fiche.OBJET.role=0)
     */
    public static boolean isActionParametrableDansRole(final String objet, final String action) {
        final String param = getObjetByNom(objet).getProperty("fiche." + objet.toUpperCase() + ".saisie_role." + action);
        boolean recherchable = true;
        if ("0".equals(param)) {
            recherchable = false;
        }
        return recherchable;
    }

    /**
     * Teste si l'objet n'est pas desactive(paramétrage via le jtf, ex : fiche.ACTUALITE.activation=0)
     *
     * @param objet
     *            L'objet qu'on veut rechercher
     *
     * @return true si desactive
     */
    public static boolean isActif(final String objet) {
        final String param = getObjetByNom(objet).getProperty("fiche." + objet.toUpperCase() + ".activation");
        boolean actif = true;
        if ("0".equals(param)) {
            actif = false;
        }
        return actif;
    }

    /**
     * Teste si l'objet possede un libelle personnalise ex : fiche.ACTUALITE.libelle=xxxxxxx)
     *
     * @param objet
     *            L'objet qu'on veut rechercher
     *
     * @return le libelle personnalise
     *
     */
    public static String getLibellePersonnalisable(final String objet) {
        String res = StringUtils.EMPTY;
        final String param = getObjetByCode(objet).getProperty("fiche." + objet.toUpperCase() + ".libelle");
        if (param != null) {
            res = param;
        } else {
            res = getLibelleObjet(objet);
        }
        return res;
    }

    /**
     * Renvoit la liste des noms objet non triés
     *
     * @return the liste noms objet
     */
    public static Collection<String> getListeNomsObjet() {
        return getReferentiel().getCodesObjetsByNom().keySet();
    }

    /**
     * Renvoit la liste des codes objet triés dans l'ordre cf onglets du BO
     *
     * @return the liste codes objet
     */
    public static Collection<String> getListeCodesObjet() {
        return getReferentiel().getCodesObjetsTries();
    }

    public static String getExtension(final String codeObjet) {
        String extension = StringUtils.EMPTY;
        final Objetpartage objetParCode = getObjetByCode(codeObjet);
        if (objetParCode != null) {
            extension = objetParCode.getIdExtension();
        }
        return extension;
    }

    public static String getExtension(final FicheUniv fiche) {
        String extension = StringUtils.EMPTY;
        final String codeObjet = getCodeObjet(fiche);
        if (StringUtils.isNotBlank(codeObjet)) {
            extension = getExtension(codeObjet);
        }
        return extension;
    }

    /**
     * Renvoit la liste des états objet
     *
     * @return la liste des états
     */
    public static Map<String, String> getEtatsObjet() {
        final Map<String, String> etatsObjetEtLibelle = new HashMap<>();
        final Collection<String> codesEtats = getReferentiel().getEtatsObjet();
        for (final String codeEtat : codesEtats) {
            etatsObjetEtLibelle.put(codeEtat, MessageHelper.getCoreMessage("ETATFICHE_" + codeEtat));
        }
        return etatsObjetEtLibelle;
    }

    /**
     * Renvoit la liste des états objet disponibles en front
     *
     * @return la liste des états
     */
    public static Map<String, String> getEtatsObjetFront() {
        final Map<String, String> etatsObjetEtLibelle = new HashMap<>();
        final Collection<String> codesEtats = getReferentiel().getEtatsObjetFront();
        for (final String codeEtat : codesEtats) {
            etatsObjetEtLibelle.put(codeEtat, MessageHelper.getCoreMessage("ETATFICHE_" + codeEtat));
        }
        return etatsObjetEtLibelle;
    }

    /**
     * Renvoit le libellé de l'état
     *
     * @param code
     *           le code de l'état
     * @return le libellé ou null si non trouvé
     */
    public static String getLibelleEtatObjet(final String code) {
        return getEtatsObjet().get(code);
    }
}
