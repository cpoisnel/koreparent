package com.univ.objetspartages.om;

import java.util.Date;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.sgbd.FicheUnivDB;

/**
 * Interface décrivant une fiche K-Portal.
 */
public interface FicheUniv extends FicheUnivDB {

    /**
     * Inits the.
     */
    void init();

    /**
     * Gets the id fiche.
     *
     * @return the id fiche
     */
    Long getIdFiche();

    /**
     * Sets the id fiche.
     *
     * @param idFiche
     *            the new id fiche
     */
    void setIdFiche(Long idFiche);

    /**
     * Gets the code rubrique.
     *
     * @return the code rubrique
     */
    String getCodeRubrique();

    /**
     * Sets the code rubrique.
     *
     * @param codeRubrique
     *            the new code rubrique
     */
    void setCodeRubrique(String codeRubrique);

    /**
     * Gets the code.
     *
     * @return the code
     */
    String getCode();

    /**
     * Gets the langue.
     *
     * @return the langue
     */
    String getLangue();

    /**
     * Gets the code redacteur.
     *
     * @return the code redacteur
     */
    String getCodeRedacteur();

    /**
     * Gets the code validation.
     *
     * @return the code validation
     */
    String getCodeValidation();

    /**
     * Gets the code rattachement.
     *
     * @return the code rattachement
     */
    String getCodeRattachement();

    /**
     * Gets the etat objet.
     *
     * @return the etat objet
     */
    String getEtatObjet();

    /**
     * Gets the message alerte.
     *
     * @return the message alerte
     */
    String getMessageAlerte();

    /**
     * Gets the meta keywords.
     *
     * @return the meta keywords
     */
    String getMetaKeywords();

    /**
     * Gets the meta description.
     *
     * @return the meta description
     */
    String getMetaDescription();

    /**
     * Gets the titre encadre.
     *
     * @return the titre encadre
     */
    String getTitreEncadre();

    /**
     * Gets the contenu encadre.
     *
     * @return the contenu encadre
     */
    String getContenuEncadre();

    /**
     * Gets the encadre recherche.
     *
     * @return the encadre recherche
     */
    String getEncadreRecherche();

    /**
     * Gets the encadre recherche bis.
     *
     * @return the encadre recherche bis
     */
    String getEncadreRechercheBis();

    /**
     * Gets the date alerte.
     *
     * @return the date alerte
     */
    Date getDateAlerte();

    /**
     * Gets the date creation.
     *
     * @return the date creation
     */
    Date getDateCreation();

    /**
     * Gets the date modification.
     *
     * @return the date modification
     */
    Date getDateModification();

    /**
     * Gets the date proposition.
     *
     * @return the date proposition
     */
    Date getDateProposition();

    /**
     * Gets the date validation.
     *
     * @return the date validation
     */
    Date getDateValidation();

    /**
     * Gets the nb hits.
     *
     * @return the nb hits
     */
    Long getNbHits();


    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    void setCode(String code);

    /**
     * Sets the langue.
     *
     * @param langue
     *            the new langue
     */
    void setLangue(String langue);

    /**
     * Sets the ctx.
     *
     * @param ctx
     *            the new ctx
     */
    void setCtx(OMContext ctx);

    /**
     * Sets the code redacteur.
     *
     * @param codeRedacteur
     *            the new code redacteur
     */
    void setCodeRedacteur(String codeRedacteur);

    /**
     * Sets the code validation.
     *
     * @param codeValidation
     *            the new code validation
     */
    void setCodeValidation(String codeValidation);

    /**
     * Sets the code rattachement.
     *
     * @param codeRattachement
     *            the new code rattachement
     */
    void setCodeRattachement(String codeRattachement);

    /**
     * Sets the etat objet.
     *
     * @param etatObjet
     *            the new etat objet
     */
    void setEtatObjet(String etatObjet);

    /**
     * Sets the message alerte.
     *
     * @param messageAlerte
     *            the new message alerte
     */
    void setMessageAlerte(String messageAlerte);

    /**
     * Sets the meta keywords.
     *
     * @param metaKeywords
     *            the new meta keywords
     */
    void setMetaKeywords(String metaKeywords);

    /**
     * Sets the meta description.
     *
     * @param metaDescription
     *            the new meta description
     */
    void setMetaDescription(String metaDescription);

    /**
     * Sets the titre encadre.
     *
     * @param titreEncadre
     *            the new titre encadre
     */
    void setTitreEncadre(String titreEncadre);

    /**
     * Sets the contenu encadre.
     *
     * @param contenuEncadre
     *            the new contenu encadre
     */
    void setContenuEncadre(String contenuEncadre);

    /**
     * Sets the encadre recherche.
     *
     * @param encadreRecherche
     *            the new encadre recherche
     */
    void setEncadreRecherche(String encadreRecherche);

    /**
     * Sets the encadre recherche bis.
     *
     * @param encadreRecherche
     *            the new encadre recherche bis
     */
    void setEncadreRechercheBis(String encadreRecherche);

    /**
     * Sets the date alerte.
     *
     * @param dateAlerte
     *            the new date alerte
     */
    void setDateAlerte(Date dateAlerte);

    /**
     * Sets the date creation.
     *
     * @param dateCreation
     *            the new date creation
     */
    void setDateCreation(Date dateCreation);

    /**
     * Sets the date modification.
     *
     * @param dateCreation
     *            the new date modification
     */
    void setDateModification(Date dateCreation);

    /**
     * Sets the date proposition.
     *
     * @param dateProposition
     *            the new date proposition
     */
    void setDateProposition(Date dateProposition);

    /**
     * Sets the date validation.
     *
     * @param dateValidation
     *            the new date validation
     */
    void setDateValidation(Date dateValidation);

    /**
     * Sets the nb hits.
     *
     * @param nbHits
     *            the new nb hits
     */
    void setNbHits(Long nbHits);

    /**
     * Gets the libelle affichable.
     *
     * @return the libelle affichable
     *
     * @throws Exception
     *             the exception
     */
    String getLibelleAffichable();

    /**
     * Gets the references.
     *
     * @return the references
     */
    String getReferences();

    /**
     * Dupliquer.
     *
     * @param nouvelleLangue
     *            the nouvelle langue
     *
     * @throws Exception
     *             the exception
     */
    void dupliquer(String nouvelleLangue) throws Exception;

    /**
     * Select code langue etat.
     *
     * @param code
     *            the code
     * @param langue
     *            the langue
     * @param etat
     *            the etat
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    int selectCodeLangueEtat(String code, String langue, String etat) throws Exception;

    /**
     * Traiter requete.
     *
     * @param requete the requete
     * @return the int
     * @throws Exception the exception
     */
    int traiterRequete(String requete) throws Exception;

    /**
     * Select par code rubrique.
     *
     * @param codeRubrique the code rubrique
     * @param langue the langue
     * @return the int
     * @throws Exception the exception
     */
    int selectParCodeRubrique(String codeRubrique, String langue) throws Exception;
}
