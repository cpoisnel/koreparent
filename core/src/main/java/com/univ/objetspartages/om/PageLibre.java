package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.annotation.FicheAnnotation;
import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;
import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.kportal.extension.module.plugin.rubrique.impl.BeanFichePageAccueil;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.PagelibreBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.RequeteUtil;
import com.univ.utils.UnivWebFmt;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseLimit;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.LimitHelper;
import com.univ.utils.sql.criterespecifique.OrderByHelper;
import com.univ.utils.sql.criterespecifique.RequeteSQLHelper;
import com.univ.xml.NodeUtil;

/**
 * Classe representant un objet pagelibre.
 */
@FicheAnnotation(isSaisieFrontOffice = false, isEncadreRecherche = false, isEncadreRechercheEmbarquable = false)
public class PageLibre extends AbstractFicheRestriction<PagelibreBean> implements FicheExportableXML {

    /**
     * Renvoie le libelle a afficher (methode statique utilisee pour les jointures entre fiches).
     *
     * @param ctx
     *            the ctx
     * @param codes
     *            the codes
     * @param langue
     *            the langue
     *
     * @return the libelle affichable
     *
     * @throws Exception
     *             the exception
     */
    public static String getLibelleAffichable(final OMContext ctx, final String codes, String langue) throws Exception {
        String res = StringUtils.EMPTY;
        if (codes == null || codes.length() == 0) {
            return res;
        }
        final PageLibre pagelibre = new PageLibre();
        pagelibre.init();
        if (langue == null || langue.length() == 0) {
            langue = "0";
        }
        final StringTokenizer st = new StringTokenizer(codes, ";");
        while (st.hasMoreTokens()) {
            final String code = st.nextToken();
            // On cherche d'abord la version en ligne puis les autres versions
            int count = pagelibre.selectCodeLangueEtat(code, langue, EtatFiche.EN_LIGNE.getEtat());
            if (count == 0) {
                count = pagelibre.selectCodeLangueEtat(code, langue, StringUtils.EMPTY);
            }
            if (count > 0) {
                if (res.length() > 1) {
                    res += ";";
                }
                if (pagelibre.nextItem()) {
                    res += pagelibre.getLibelleAffichable();
                }
            } else {
                res += "-";
            }
        }
        return res;
    }

    /**
     * Renvoie la fiche demandee (methode statique utilisee pour les jointures entre fiches).
     *
     * @param ctx
     *            the ctx
     * @param code
     *            the code
     * @param langue
     *            the langue
     *
     * @return the fiche pagelibre
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link com.univ.utils.FicheUnivHelper#getFiche(String, String, String)}
     */
    @Deprecated
    public static PageLibre getFichePagelibre(final OMContext ctx, final String code, String langue) throws Exception {
        PageLibre fiche = null;
        if (code != null && code.length() > 0) {
            if (langue == null || langue.length() == 0) {
                langue = "0"; // francais par defaut
            }
            fiche = new PageLibre();
            fiche.init();
            int count = fiche.selectCodeLangueEtat(code, langue, EtatFiche.EN_LIGNE.getEtat());
            if (count == 0) // si pas de version en ligne
            {
                count = fiche.selectCodeLangueEtat(code, langue, StringUtils.EMPTY);
            }
            if (count == 0) // si pas de version dans la langue demandee
            {
                count = fiche.selectCodeLangueEtat(code, StringUtils.EMPTY, EtatFiche.EN_LIGNE.getEtat());
            }
            if (count > 0) {
                fiche.nextItem();
            }
        }
        return fiche;
    }

    /**
     * Initialise l'objet metier.
     */
    @Override
    public void init() {
        FicheUnivMgr.init(this);
        setIdFiche(0L);
        final String timeStamp = Long.toString(System.currentTimeMillis());
        setCode(timeStamp.substring(timeStamp.length() - 8, timeStamp.length()));
        setLangue("0");
    }

    /**
     * Renvoie le libelle a afficher.
     *
     * @return the libelle affichable
     */
    @Override
    public String getLibelleAffichable() {
        return getTitre().length() > 0 ? getTitre() : "-";
    }

    /**
     * Renvoie l'identifiant de la fiche.
     *
     * @return the id fiche
     */
    @Override
    public Long getIdFiche() {
        return persistenceBean.getId();
    }

    /**
     * Valorise l'identifiant de la fiche.
     *
     * @param idFiche
     *            the id fiche
     */
    @Override
    public void setIdFiche(final Long idFiche) {
        persistenceBean.setId(idFiche);
    }

    /**
     * Renvoie le contenu formate en HTML pour l'attribut complements.
     *
     * @return the formated complements
     *
     * @throws Exception
     *             the exception
     */
    public String getFormatedComplements() throws Exception {
        return UnivWebFmt.formaterEnHTML(ContexteUtil.getContexteUniv(), persistenceBean.getComplements());
    }

    /**
     * Renvoie la rubrique de la fiche.
     *
     * @return the infos rubrique
     *
     * @throws Exception
     *             the exception
     */
    public InfosRubriques getInfosRubrique() throws Exception {
        InfosRubriques res = new InfosRubriques("");
        if (StringUtils.isNotBlank(getCodeRubrique())) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(getCodeRubrique());
            if (rubriqueBean != null) {
                res = new InfosRubriques(rubriqueBean);
            }
        }
        return res;
    }

    /**
     * Renvoie la structure de la fiche.
     *
     * @return the infos structure
     *
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getByCodeLanguage(String, String)}
     */
    @Deprecated
    public StructureModele getInfosStructure() throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getByCodeLanguage(getCodeRattachement(), getLangue());
    }

    /**
     * Renvoie les metadonnees de la fiche.
     *
     * @return the meta
     *
     * @throws Exception
     *             the exception
     */
    public MetatagBean getMeta() throws Exception {
        return MetatagUtils.lireMeta(this);
    }

    /**
     * Duplication dans une nouvelle langue, le nouvel objet devient l'objet courant.
     *
     * @param nouvelleLangue
     *            the nouvelle langue
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public void dupliquer(final String nouvelleLangue) throws Exception {
        // donnees a reinitialiser dans le nouvel objet
        FicheUnivMgr.dupliquer(this);
        // creation objet dans la langue demandee
        setIdFiche(Long.valueOf(0));
        setLangue(nouvelleLangue);
        add();
    }

    /**
     * Traitement d'une requete sur l'objet.
     *
     * @param requete
     *            the requete
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public int traiterRequete(final String requete) throws Exception {
        // Recuperation des parametres de la requete
        final String titre = RequeteUtil.renvoyerParametre(requete, "TITRE");
        final String motsCles = RequeteUtil.renvoyerParametre(requete, "MOTS_CLES");
        final String codeRubrique = RequeteUtil.renvoyerParametre(requete, "CODE_RUBRIQUE");
        final String codeRattachement = RequeteUtil.renvoyerParametre(requete, "CODE_RATTACHEMENT");
        final String langue = RequeteUtil.renvoyerParametre(requete, "LANGUE");
        final String code = RequeteUtil.renvoyerParametre(requete, "CODE");
        final String selection = RequeteUtil.renvoyerParametre(requete, "SELECTION");
        final String nombre = RequeteUtil.renvoyerParametre(requete, "NOMBRE");
        final String etatObjet = StringUtils.defaultIfBlank(RequeteUtil.renvoyerParametre(requete, "ETAT_OBJET"), EtatFiche.EN_LIGNE.toString());
        final String codeRedacteur = RequeteUtil.renvoyerParametre(requete, "CODE_REDACTEUR");
        String order = StringUtils.EMPTY;
        if (selection.length() > 0) {
            // Dernières pages libres publiées
            if ("0007".equals(selection)) {
                order = "META.META_DATE_MISE_EN_LIGNE DESC";
            }
        }
        return select(code, titre, motsCles, langue, codeRattachement, codeRubrique, codeRedacteur, etatObjet, nombre, order);
    }

    /**
     * Selection d'une fiche a partir de son code, de sa langue et de son etat.
     *
     * @param code
     *            the code
     * @param langue
     *            the langue
     * @param etatObjet
     *            the etat objet
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public int selectCodeLangueEtat(final String code, final String langue, final String etatObjet) throws Exception {
        final RequeteSQL codeLangueEtatOrdreSurTitre = new RequeteSQL();
        final ClauseWhere whereCodeLangueEtat = ConditionHelper.whereCodeLangueEtat(code, langue, etatObjet);
        final ClauseOrderBy orderBy = new ClauseOrderBy();
        orderBy.orderBy("T1.TITRE", SensDeTri.ASC);
        codeLangueEtatOrdreSurTitre.where(whereCodeLangueEtat).orderBy(orderBy);
        return select(codeLangueEtatOrdreSurTitre.formaterRequete());
    }

    /**
     * Renvoie les pagelibres pour la rubrique demandee.
     *
     * @param codeRubrique
     *            the code rubrique
     * @param langue
     *            the langue
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    @Override
    @Deprecated
    public int selectParCodeRubrique(final String codeRubrique, final String langue) throws Exception {
        final RequeteSQL codeRubriqueLangueEnLigneOrdreTitre = new RequeteSQL();
        final ClauseWhere whereCodeRubriqueLangueEtat = new ClauseWhere();
        if (StringUtils.isNotEmpty(codeRubrique)) {
            whereCodeRubriqueLangueEtat.setPremiereCondition(ConditionHelper.egalVarchar("T1.CODE_RUBRIQUE", codeRubrique));
        }
        if (StringUtils.isNotEmpty(langue)) {
            whereCodeRubriqueLangueEtat.and(ConditionHelper.egalVarchar("T1.LANGUE", langue));
        }
        whereCodeRubriqueLangueEtat.and(ConditionHelper.egalVarchar("T1.ETAT_OBJET", EtatFiche.EN_LIGNE.getEtat()));
        final ClauseOrderBy orderBy = new ClauseOrderBy();
        orderBy.orderBy("T1.TITRE", SensDeTri.ASC);
        codeRubriqueLangueEnLigneOrdreTitre.where(whereCodeRubriqueLangueEtat).orderBy(orderBy);
        return select(codeRubriqueLangueEnLigneOrdreTitre.formaterRequete());
    }

    /**
     * Renvoie la liste des references a cette fiche.
     *
     * @return the references
     */
    @Override
    public String getReferences() {
        return StringUtils.defaultString(persistenceBean.getContenu()) + StringUtils.defaultString(persistenceBean.getComplements()) + FicheUnivMgr.getReferenceParJointure("structure", getCodeRattachement());
    }

    /**
     * Selection d'une instance de l'objet Pagelibre a partir de l'ensemble des criteres.
     *
     * @param code
     *            the code
     * @param titre
     *            the titre
     * @param motsCles
     *            the mots cles
     * @param langue
     *            the langue
     * @param codeRattachement
     *            the code rattachement
     * @param codeRubrique
     *            the code rubrique
     * @param codeRedacteur
     *            the code redacteur
     * @param etatObjet
     *            the etat objet
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String code, final String titre, final String motsCles, final String langue, final String codeRattachement, final String codeRubrique, final String codeRedacteur, final String etatObjet, final String nombre, final String order) throws Exception {
        RequeteSQL requeteSelect;
        final ClauseWhere where = new ClauseWhere();
        if (StringUtils.isNotEmpty(titre)) {
            where.setPremiereCondition(ConditionHelper.rechercheMots("TITRE", titre));
        }
        if (StringUtils.isNotEmpty(motsCles)) {
            where.and(ConditionHelper.rechercheMots("META_KEYWORDS", motsCles));
        }
        if (StringUtils.isNotEmpty(codeRattachement)) {
            where.and(ConditionHelper.getConditionStructure("CODE_RATTACHEMENT", codeRattachement));
        }
        if (StringUtils.isNotEmpty(langue) && !"0000".equals(langue)) {
            where.and(ConditionHelper.egalVarchar("T1.LANGUE", langue));
        }
        if (StringUtils.isNotEmpty(etatObjet) && !"0000".equals(etatObjet)) {
            where.and(ConditionHelper.egalVarchar("T1.ETAT_OBJET", etatObjet));
        }
        if (StringUtils.isNotEmpty(codeRedacteur)) {
            where.and(ConditionHelper.egalVarchar("CODE_REDACTEUR", codeRedacteur));
        }
        if (StringUtils.isNotEmpty(code)) {
            where.and(ConditionHelper.egalVarchar("T1.CODE", code));
        }
        requeteSelect = RequeteSQLHelper.getRequeteGenerique(where, ctx, this, codeRubrique);
        ClauseOrderBy orderBy = new ClauseOrderBy();
        if (StringUtils.isNotEmpty(order)) {
            orderBy = OrderByHelper.reconstruireClauseOrderBy(order);
        } else {
            orderBy.orderBy("T1.TITRE", SensDeTri.ASC);
        }
        requeteSelect.orderBy(orderBy);
        final ClauseLimit limite = LimitHelper.ajouterCriteresLimitesEtOptimisation(ctx, nombre);
        requeteSelect.limit(limite);
        return select(requeteSelect.formaterRequete());
    }

    /**
     * Récupération du libellé à afficher (spécifique au back-office pour les listes de résultats).
     *
     * @return the libelle back office
     *
     */
    public String getLibelleBackOffice() {
        return getLibelleAffichable();
    }

    /**
     * Renvoie la liste des paragraphes.
     *
     * @return the paragraphes
     * @deprecated
     * @see #getLignes()
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    public Vector<ParagrapheBean> getParagraphes() throws Exception {
        return ParagrapheContenuHelper.getParagraphes(persistenceBean.getContenu());
    }

    /**
     * Stocke la liste des paragraphes triés.
     *
     * @param listeParagraphes
     *            the _liste paragraphes
     *
     * @throws Exception
     *             the exception
     */
    public void setParagraphes(final ArrayList<ParagrapheBean> listeParagraphes) throws Exception {
        persistenceBean.setContenu(ParagrapheContenuHelper.formateParagraphes(listeParagraphes));
    }

    /**
     * Renvoie la liste des lignes.
     *
     * @return the lignes
     *
     * @throws Exception
     *             the exception
     */
    public Collection<LigneContenu> getLignes() throws Exception {
        return ParagrapheContenuHelper.getLignes(persistenceBean.getContenu());
    }

    /**
     * Renvoie la rubrique de l'encadré associé à la page libre courante (il s'agit des soeurs de la page courante).
     *
     * @return the rubrique encadre
     *
     */
    public String getRubriqueEncadre() {
        String rubrique =  getCodeRubrique();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueFiche = serviceRubrique.getRubriqueByCode(getCodeRubrique());
        if (rubriqueFiche != null) {
            final BeanPageAccueil beanAccueil = PageAccueilRubriqueManager.getInstance().getBeanPageAccueil(rubriqueFiche);
            if (beanAccueil != null && beanAccueil instanceof BeanFichePageAccueil && getCode().equals(((BeanFichePageAccueil) beanAccueil).getCode()) && getLangue().equals(((BeanFichePageAccueil) beanAccueil).getLangue()) && ReferentielObjets.getNomObjet(this).equals(((BeanFichePageAccueil) beanAccueil).getObjet())) {
                // Tete de rubrique : on renvoie la rubrique de rang supérieur
                rubrique = rubriqueFiche.getCodeRubriqueMere();
            }
        }
        return rubrique;
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.FicheExportableXML#exportNodeFiche(org.w3c.dom.Node, java.lang.String)
     */
    @Override
    public Node exportNodeFiche(final Node liste, final String formatExport) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final org.w3c.dom.Document document = liste.getOwnerDocument();
        final Node fiche = document.createElement("PAGELIBRE");
        NodeUtil.addNode(fiche, "CODE", getCode());
        NodeUtil.addNode(fiche, "LANGUE", getLangue());
        NodeUtil.addNode(fiche, "RATTACHEMENT", serviceStructure.getAttachementLabel(getCodeRattachement(), getLangue(), true));
        NodeUtil.addNode(fiche, "LIBELLE", getLibelleAffichable());
        NodeUtil.addNode(fiche, "TITRE", getTitre());
        NodeUtil.addNode(fiche, "CODE_RUBRIQUE", getCodeRubrique());
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueFiche = serviceRubrique.getRubriqueByCode(getCodeRubrique());
        if (rubriqueFiche != null) {
            NodeUtil.addNode(fiche, "RUBRIQUE", rubriqueFiche.getIntitule());
        } else {
            NodeUtil.addNode(fiche, "RUBRIQUE", StringUtils.EMPTY);
        }
        /* Gestion du contenu */
        final Node nodeContenu = document.createElement("CONTENU");
        final Vector<ParagrapheBean> listeParagraphes = getParagraphes();
        final Enumeration<ParagrapheBean> e = listeParagraphes.elements();
        while (e.hasMoreElements()) {
            final Node nodeParagraphe = document.createElement("PARAGRAPHE");
            /********************************************************************/
            /********* CHAQUE PARAGRAPHE *************/
            /********************************************************************/
            ParagrapheBean paragraphe = e.nextElement();
            /* Boucle sur les sous-paragraphes */
            final Enumeration<SousParagrapheBean> e3 = paragraphe.getSousParagraphes().elements();
            while (e3.hasMoreElements()) {
                final Node nodeSousParagraphe = document.createElement("SOUS_PARAGRAPHE");
                final SousParagrapheBean sousParagraphe = e3.nextElement();
                NodeUtil.addNode(nodeSousParagraphe, "TITRE", sousParagraphe.getTitre());
                NodeUtil.addNode(nodeSousParagraphe, "VALEUR", sousParagraphe.getContenu());
                nodeParagraphe.appendChild(nodeSousParagraphe);
            }
            nodeContenu.appendChild(nodeParagraphe);
        }
        fiche.appendChild(nodeContenu);
        return fiche;
    }

    /**
     * Gets the complements.
     *
     * @return the complements
     */
    public String getComplements() {
        return persistenceBean.getComplements();
    }

    /**
     * Sets the complements.
     *
     * @param complements
     *            the complements to set
     */
    public void setComplements(String complements) {
        persistenceBean.setComplements(complements);
    }

    /**
     * Gets the contenu.
     *
     * @return the contenu
     */
    public String getContenu() {
        return persistenceBean.getContenu();
    }

    /**
     * Sets the contenu.
     *
     * @param contenu
     *            the contenu to set
     */
    public void setContenu(String contenu) {
        persistenceBean.setContenu(contenu);
    }

    /**
     * Gets the id pagelibre.
     *
     * @return the idPagelibre
     * @deprecated Utilisez {@link PageLibre#getIdFiche()}
     */
    @Deprecated
    public Long getIdPagelibre() {
        return persistenceBean.getId();
    }

    /**
     * Sets the id pagelibre.
     *
     * @param idPagelibre
     *            the idPagelibre to set
     * @deprecated Utilisez {@link PageLibre#setIdFiche(Long)}
     */
    @Deprecated
    public void setIdPagelibre(Long idPagelibre) {
        persistenceBean.setId(idPagelibre);
    }

    /**
     * Gets the rattachement bandeau.
     *
     * @return the rattachementBandeau
     */
    public String getRattachementBandeau() {
        return persistenceBean.getRattachementBandeau();
    }

    /**
     * Sets the rattachement bandeau.
     *
     * @param rattachementBandeau
     *            the rattachementBandeau to set
     */
    public void setRattachementBandeau(String rattachementBandeau) {
        persistenceBean.setRattachementBandeau(rattachementBandeau);
    }
}
