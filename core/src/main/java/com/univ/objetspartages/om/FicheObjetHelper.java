package com.univ.objetspartages.om;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.datasource.exceptions.UpdateToDataSourceException;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.EscapeString;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

public class FicheObjetHelper {

    /**
     * Methode d'update global d'une arborescence de commentaire en fonction de la fiche parente principale
     *
     * @param ctx
     *            the _ctx
     * @param ficheUniv
     *            the _fiche univ
     * @param idMeta
     *            'id du metatag de la fiche parente
     *
     */
    public static void updateAll(final OMContext ctx, final FicheUniv ficheUniv, final String classeObjet, final Long idMeta) {
        final String codeObjet = ReferentielObjets.getCodeObjetParClasse(classeObjet);
        final FicheUniv ficheObjet = ReferentielObjets.instancierFiche(codeObjet);
        if (ficheObjet instanceof FicheObjet) {
            final String codeRattachement = ficheUniv.getCodeRattachement();
            final String codeRubrique = ficheUniv.getCodeRubrique();
            String codeRattachementAutres = "";
            if (ficheUniv instanceof FicheRattachementsSecondaires) {
                codeRattachementAutres = ((FicheRattachementsSecondaires) ficheUniv).getCodeRattachementAutres();
            }
            String publicVise = "";
            String publicModeRestriction = "";
            String publicViseRestriction = "";
            if (ficheUniv instanceof DiffusionSelective) {
                publicVise = ((DiffusionSelective) ficheUniv).getDiffusionPublicVise();
                publicModeRestriction = ((DiffusionSelective) ficheUniv).getDiffusionModeRestriction();
                publicViseRestriction = ((DiffusionSelective) ficheUniv).getDiffusionPublicViseRestriction();
            }
            final String etatObjet = ficheUniv.getEtatObjet();
            final ClauseWhere whereIdMeta = new ClauseWhere(ConditionHelper.egal("ID_META", idMeta, TypeOperande.LONG));
            String query = "SELECT ID_" + ReferentielObjets.getNomTableSql(codeObjet) + " FROM " + ReferentielObjets.getNomTableSql(codeObjet) + whereIdMeta.formaterSQL();
            final Collection<String> idsMeta = new ArrayList<>();
            try (PreparedStatement stmt = ctx.getConnection().prepareStatement(query);
                final ResultSet rs = stmt.executeQuery();) {
                while (rs.next()) {
                    idsMeta.add(EscapeString.escapeSqlValue(String.valueOf(rs.getLong(1))));
                }
            } catch (final SQLException exc) {
                throw new UpdateToDataSourceException("ERROR_IN_METHOD FicheObjetHelper.updateAll()",exc);
            }
            if (CollectionUtils.isNotEmpty(idsMeta)) {
                query = "UPDATE " + ReferentielObjets.getNomTableSql(codeObjet) + " SET " + "ETAT_OBJET='" + EscapeString.escapeSqlValue(etatObjet) + "', " + "CODE_RUBRIQUE='" + EscapeString.escapeSql(codeRubrique) + "', " + "CODE_RATTACHEMENT='" + EscapeString.escapeSqlValue(codeRattachement) + "', " + "CODE_RATTACHEMENT_AUTRES='" + EscapeString.escapeSqlValue(codeRattachementAutres) + "', " + "DIFFUSION_PUBLIC_VISE='" + EscapeString.escapeSqlValue(publicVise) + "'," + "DIFFUSION_MODE_RESTRICTION='" + EscapeString.escapeSqlValue(publicModeRestriction) + "', " + "DIFFUSION_PUBLIC_VISE_RESTRICTION='" + EscapeString.escapeSqlValue(publicViseRestriction) + "', " + "DATE_MODIFICATION='" + SqlDateConverter.toDate(ficheUniv.getDateModification()) + "' " + " WHERE ID_" + ReferentielObjets.getNomTableSql(codeObjet) + " IN (" + StringUtils.join(idsMeta, " , ") + " )";
                try (PreparedStatement stmt = ctx.getConnection().prepareStatement(query);) {
                    stmt.executeUpdate();
                } catch (final SQLException exc) {
                    throw new UpdateToDataSourceException("ERROR_IN_METHOD FicheObjetHelper.updateAll()",exc);
                }
                query = "UPDATE METATAG SET META_ETAT_OBJET='" + EscapeString.escapeSqlValue(etatObjet) + "', " + "META_CODE_RUBRIQUE='" + EscapeString.escapeSqlValue(codeRubrique) + "', " + "META_CODE_RATTACHEMENT='" + EscapeString.escapeSqlValue(codeRattachement) + "', " + "META_CODE_RATTACHEMENT_AUTRES='" + EscapeString.escapeSqlValue(codeRattachementAutres) + "', " + "META_DIFFUSION_PUBLIC_VISE='" + EscapeString.escapeSqlValue(publicVise) + "'," + "META_DIFFUSION_MODE_RESTRICTION='" + EscapeString.escapeSqlValue(publicModeRestriction) + "', META_DIFFUSION_PUBLIC_VISE_RESTRICTION='" + EscapeString.escapeSqlValue(publicViseRestriction) + "',  META_DATE_MODIFICATION='" + SqlDateConverter.toDate(ficheUniv.getDateModification()) + "' WHERE META_ID_FICHE IN (" + StringUtils.join(idsMeta, " , ") + ") AND META_CODE_OBJET='" + EscapeString.escapeSqlValue(codeObjet) + "'";
                try (PreparedStatement stmt = ctx.getConnection().prepareStatement(query);) {
                    stmt.executeUpdate();
                } catch (final SQLException exc) {
                    throw new UpdateToDataSourceException("ERROR_IN_METHOD FicheObjetHelper.updateAll()",exc);
                }
            }
        }
    }

    /**
     * Methode de suppression globale d'une arborescence de commentaires
     *
     * @param ctx le contexte uniquement pour la request
     * @param classeObjet la classe de l'objet
     * @param idMeta lid du metatag de la fiche parente
     *
     */
    public static void deleteAll(final OMContext ctx, final String classeObjet, final Long idMeta) {
        final String codeObjet = ReferentielObjets.getCodeObjetParClasse(classeObjet);
        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
        if (ficheUniv instanceof FicheObjet) {
            final Collection<Long> idsMeta = new ArrayList<>();
            final ClauseWhere whereIdMeta = new ClauseWhere(ConditionHelper.egal("ID_META", idMeta, TypeOperande.LONG));
            String query = "SELECT ID_" + ReferentielObjets.getNomTableSql(codeObjet) + " FROM " + ReferentielObjets.getNomTableSql(codeObjet) + whereIdMeta.formaterSQL();
            try (PreparedStatement stmt =  ctx.getConnection().prepareStatement(query);
                ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    idsMeta.add(rs.getLong(1));
                }
            } catch (final SQLException exc) {
                throw new UpdateToDataSourceException("ERROR_IN_METHOD FicheObjetHelper.deleteAll()",exc);
            }
            if (CollectionUtils.isNotEmpty(idsMeta)) {
                @SuppressWarnings("unchecked")
                final Collection<String> idsMetaForOldQuery = CollectionUtils.collect(idsMeta, new Transformer() {

                    @Override
                    public String transform(final Object o) {
                        return String.valueOf(o);
                    }
                });
                final ClauseWhere whereIdMetaIn = new ClauseWhere(ConditionHelper.in("ID_" + ReferentielObjets.getNomTableSql(codeObjet), idsMetaForOldQuery));
                query = "DELETE FROM " + ReferentielObjets.getNomTableSql(codeObjet) + whereIdMetaIn.formaterSQL();
                try (PreparedStatement stmt = ctx.getConnection().prepareStatement(query);) {
                    stmt.executeUpdate();
                } catch (final SQLException exc) {
                    throw new UpdateToDataSourceException("ERROR_IN_METHOD FicheObjetHelper.deleteAll()",exc);
                }
                MetatagUtils.getMetatagService().deleteForCodeAndFicheIds(codeObjet,idsMeta);
            }
        }
    }
}
