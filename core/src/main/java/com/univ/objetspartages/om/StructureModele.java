package com.univ.objetspartages.om;
// TODO: Auto-generated Javadoc

import java.io.Serializable;

/**
 * The Interface StructureModele.
 *
 * @author malice
 *
 *         Interface qui permet à un objet d'apparaître dans l'arbre des structures et de faire partie de la famille des objets de type "Structure"
 */
public interface StructureModele extends FicheUniv, Serializable {

    /**
     * Gets the libelle court.
     *
     * @return the libelle court
     */
    String getLibelleCourt();

    void setLibelleCourt(String libelle);

    /**
     * Gets the libelle long.
     *
     * @return the libelle long
     */
    String getLibelleLong();

    void setLibelleLong(String libelle);

    /**
     * Gets the onglets.
     *
     * @return the onglets
     */
    String getOnglets();

    void setOnglets(String onglets);

    /**
     * Gets the type structure.
     *
     * @return the type structure
     */
    String getTypeStructure();

    void setTypeStructure(String typeStructure);

    /**
     * Gets the id bandeau.
     *
     * @return the id bandeau
     */
    Long getIdBandeau();

    void setIdBandeau(Long idBandeau);

    /**
     * Gets the couleur titre.
     *
     * @return the couleur titre
     */
    String getCouleurTitre();

    void setCouleurTitre(String couleurTitre);

    /**
     * Gets the couleur fond.
     *
     * @return the couleur fond
     */
    String getCouleurFond();

    void setCouleurFond(String couleurFond);

    /**
     * Gets the adresse.
     *
     * @return the adresse
     */
    String getAdresse();

    void setAdresse(String adresse);

    /**
     * Gets the code postal.
     *
     * @return the code postal
     */
    String getCodePostal();

    void setCodePostal(String codePostal);

    /**
     * Gets the ville.
     *
     * @return the ville
     */
    String getVille();

    void setVille(String ville);

    /**
     * Gets the telephone.
     *
     * @return the telephone
     */
    String getTelephone();

    void setTelephone(String telephone);

    /**
     * Gets the telecopie.
     *
     * @return the telecopie
     */
    String getTelecopie();

    void setTelecopie(String telecopie);

    /**
     * Gets the adresse mail.
     *
     * @return the adresse mail
     */
    String getAdresseMail();

    void setAdresseMail(String adresseMail);

    /**
     * Gets the site web.
     *
     * @return the site web
     */
    String getSiteWeb();

    void setSiteWeb(String siteWeb);

    /**
     * Gets the id plan acces.
     *
     * @return the id plan acces
     */
    Long getIdPlanAcces();

    /**
     * Gets the attribut specifique1.
     *
     * @return the attribut specifique1
     */
    String getAttributSpecifique1();

    /**
     * Gets the attribut specifique2.
     *
     * @return the attribut specifique2
     *
     */
    String getAttributSpecifique2();

    /**
     * Gets the attribut specifique3.
     *
     * @return the attribut specifique3
     */
    String getAttributSpecifique3();

    /**
     * Gets the attribut specifique4.
     *
     * @return the attribut specifique4
     */
    String getAttributSpecifique4();

    /**
     * Gets the attribut specifique5.
     *
     * @return the attribut specifique5
     */
    String getAttributSpecifique5();

    String getComplementCoordonnees();

    void setComplementCoordonnees(String complements);

    String getActivite();

    void setActivite(String activite);

    String getCodeResponsable();

    void setCodeResponsable(String codeResponsable);
}
