package com.univ.objetspartages.om;

import org.w3c.dom.Node;

/**
 * Insérez la description du type à cet endroit. Date de création : (09/04/2002 09:41:04)
 *
 * @author :
 */
public interface FicheExportableXML {

    /**
     * Export node fiche.
     *
     * @param liste
     *            the liste
     * @param formatExport
     *            the format export
     *
     * @return the node
     *
     * @throws Exception
     *             the exception
     */
    Node exportNodeFiche(Node liste, String formatExport) throws Exception;
}