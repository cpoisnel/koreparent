package com.univ.objetspartages.om;

import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.annotation.FicheAnnotation;
import com.univ.objetspartages.bean.ArticleBean;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.dao.AbstractFicheDAO;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUtil;
import com.univ.utils.DateUtil;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.RequeteUtil;
import com.univ.utils.UnivWebFmt;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseJoin;
import com.univ.utils.sql.clause.ClauseLimit;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ClauseJoinHelper;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.LimitHelper;
import com.univ.utils.sql.criterespecifique.OrderByHelper;
import com.univ.utils.sql.criterespecifique.RequeteSQLHelper;
import com.univ.xml.NodeUtil;

/**
 * Classe representant un objet article.
 */
@FicheAnnotation()
public class Article extends AbstractFicheRestriction<ArticleBean> implements DiffusionSelective, FicheExportableXML {

    private final ServiceMedia serviceMedia;

    private final ServiceRessource serviceRessource;

    public Article() {
        serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
        serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
    }

    /**
     * Renvoie le libelle a afficher (methode statique utilisee pour les jointures entre fiches).
     *
     * @param codes
     *            the codes
     * @param langue
     *            the langue
     *
     * @return the libelle affichable
     *
     * @throws Exception
     *             the exception
     */
    public static String getLibelleAffichable(final String codes, String langue) throws Exception {
        String res = StringUtils.EMPTY;
        if (StringUtils.isEmpty(codes)) {
            return res;
        }
        final Article article = new Article();
        article.init();
        if (StringUtils.isEmpty(langue)) {
            langue = "0";
        }
        final StringTokenizer st = new StringTokenizer(codes, ";");
        while (st.hasMoreTokens()) {
            final String code = st.nextToken();
            // On cherche d'abord la version en ligne puis les autres versions
            List<ArticleBean> articles = article.selectAllCodeLangueEtat(code, langue, EtatFiche.EN_LIGNE.getEtat());
            if (CollectionUtils.isEmpty(articles)) {
                // si pas de version en ligne
                articles = article.selectAllCodeLangueEtat(code, langue, StringUtils.EMPTY);
            }
            if (CollectionUtils.isNotEmpty(articles)) {
                article.setPersistenceBean(articles.get(0));
                if (res.length() > 1) {
                    res += ";";
                }
                res += article.getLibelleAffichable();
            } else {
                res += "-";
            }
        }
        return res;
    }

    /**
     * Renvoie la fiche demandee (methode statique utilisee pour les jointures entre fiches).
     *
     * @param code
     *            the code
     * @param langue
     *            the langue
     *
     * @return the fiche article
     *
     * @throws Exception
     *             the exception
     */
    public static Article getFicheArticle(final String code, String langue) throws Exception {
        Article fiche = null;
        if (code != null && code.length() > 0) {
            langue = StringUtils.defaultIfEmpty(langue, "0");
            fiche = new Article();
            final AbstractFicheDAO<ArticleBean> dao = ReferentielObjets.instancierFicheDao(fiche);
            List<ArticleBean> articles = dao.selectCodeLangueEtat(code, langue, EtatFiche.EN_LIGNE.getEtat());
            if (CollectionUtils.isEmpty(articles)) {
                // si pas de version en ligne
                articles = dao.selectCodeLangueEtat(code, langue, StringUtils.EMPTY);
            }
            if (CollectionUtils.isEmpty(articles)) {
                // si pas de version dans la langue demandee
                articles = dao.selectCodeLangueEtat(code, StringUtils.EMPTY, EtatFiche.EN_LIGNE.getEtat());
            }
            if (CollectionUtils.isNotEmpty(articles)) {
                fiche.setPersistenceBean(articles.get(0));
            }
        }
        return fiche;
    }

    /**
     * Initialise l'objet metier.
     */
    @Override
    public void init() {
        super.init();
        setSousTitre(StringUtils.EMPTY);
        setDateArticle(new Date(System.currentTimeMillis()));
        setIdVignette((long) 0);
        setChapeau(StringUtils.EMPTY);
        setCorps(StringUtils.EMPTY);
        setThematique(StringUtils.EMPTY);
        setOrdre(1000);
    }

    public Integer getOrdre() {
        return persistenceBean.getOrdre();
    }

    public void setOrdre(final Integer ordre) {
        persistenceBean.setOrdre(ordre);
    }

    public String getThematique() {
        return persistenceBean.getThematique();
    }

    public void setThematique(final String thematique) {
        persistenceBean.setThematique(thematique);
    }

    public String getCorps() {
        return persistenceBean.getCorps();
    }

    public void setCorps(final String corps) {
        persistenceBean.setCorps(corps);
    }

    public String getChapeau() {
        return persistenceBean.getChapeau();
    }

    public void setChapeau(final String chapeau) {
        persistenceBean.setChapeau(chapeau);
    }

    public Long getIdVignette() {
        return persistenceBean.getIdVignette();
    }

    public void setIdVignette(final Long idVignette) {
        persistenceBean.setIdVignette(idVignette);
    }

    public Date getDateArticle() {
        return persistenceBean.getDateArticle();
    }

    public void setDateArticle(final Date dateArticle) {
        persistenceBean.setDateArticle(dateArticle);
    }

    public String getSousTitre() {
        return persistenceBean.getSousTitre();
    }

    public void setSousTitre(final String sousTitre) {
        persistenceBean.setSousTitre(sousTitre);
    }

    /**
     * Renvoie le libelle a afficher.
     *
     * @return the libelle affichable
     */
    @Override
    public String getLibelleAffichable() {
        return getTitre();
    }

    /**
     * Renvoie le contenu formate en HTML pour l'attribut corps.
     *
     * @return the formated corps
     *
     * @throws Exception
     *             the exception
     */
    public String getFormatedCorps() throws Exception {
        return UnivWebFmt.formaterEnHTML(ContexteUtil.getContexteUniv(), getCorps());
    }

    /**
     * Renvoie le libelle pour l'attribut thematique.
     *
     * @return the libelle thematique
     *
     */
    public String getLibelleThematique() {
        return LabelUtils.getLibelle("04", getThematique(), LangueUtil.getLocale(Integer.parseInt(getLangue())));
    }

    /**
     * Renvoie le libelle pour l'attribut thematique.
     *
     * @return the libelle thematique
     *
     */
    public List<String> getListLibelleThematique() {
        return LabelUtils.getLibelleList("04", getThematique(), LangueUtil.getLocale(Integer.parseInt(getLangue())));
    }

    /**
     * Renvoie la liste des fichiers joints de la fiche.
     *
     * @return the liste fichiers
     *
     */
    public List<RessourceBean> getListeFichiers() {
        return serviceRessource.getFiles(this);
    }

    /**
     * Renvoie la liste des fichiers indicés joints de la fiche.
     *
     * @return the liste fichiers indices
     *
     */
    public List<RessourceBean> getListeFichiersIndices() {
        return serviceRessource.getFilesOrderBy(this, "CODE_PARENT");
    }

    /**
     * Renvoie la rubrique de la fiche.
     *
     * @return the infos rubrique
     *
     */
    public InfosRubriques getInfosRubrique() {
        InfosRubriques res = new InfosRubriques("");
        if (StringUtils.isNotBlank(getCodeRubrique())) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(getCodeRubrique());
            if (rubriqueBean != null) {
                res = new InfosRubriques(rubriqueBean);
            }
        }
        return res;
    }

    /**
     * Renvoie la structure de la fiche.
     *
     * @return the infos structure
     *
     * @deprecated Utiliser {@link com.univ.objetspartages.services.ServiceStructure#getByCodeLanguage(String, String)}
     */
    @Deprecated
    public StructureModele getInfosStructure() {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        return serviceStructure.getByCodeLanguage(getCodeRattachement(), getLangue());
    }

    /**
     * Renvoie les metadonnees de la fiche.
     *
     * @return the meta
     *
     * @throws Exception
     *             the exception
     */
    public MetatagBean getMeta() throws Exception {
        return MetatagUtils.lireMeta(this);
    }

    /**
     * Traitement d'une requete sur l'objet.
     *
     * @param requete
     *            the requete
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public int traiterRequete(final String requete) throws Exception {
        // Recuperation des parametres de la requete
        final String code = RequeteUtil.renvoyerParametre(requete, "CODE");
        final String titre = RequeteUtil.renvoyerParametre(requete, "TITRE");
        String sDateDebut = RequeteUtil.renvoyerParametre(requete, "DATE_DEBUT");
        String sDateFin = RequeteUtil.renvoyerParametre(requete, "DATE_FIN");
        final String thematique = RequeteUtil.renvoyerParametre(requete, "THEMATIQUE");
        final String codeRubrique = RequeteUtil.renvoyerParametre(requete, "CODE_RUBRIQUE");
        final String codeRattachement = RequeteUtil.renvoyerParametre(requete, "CODE_RATTACHEMENT");
        final String langue = RequeteUtil.renvoyerParametre(requete, "LANGUE");
        final String selection = RequeteUtil.renvoyerParametre(requete, "SELECTION");
        final String tri = RequeteUtil.renvoyerParametre(requete, "TRI_DATE");
        final String nbJours = RequeteUtil.renvoyerParametre(requete, "JOUR");
        final String nombre = RequeteUtil.renvoyerParametre(requete, "NOMBRE");
        final String ids = RequeteUtil.renvoyerParametre(requete, "IDS");
        final String etatObjet = StringUtils.defaultIfBlank(RequeteUtil.renvoyerParametre(requete, "ETAT_OBJET"), EtatFiche.EN_LIGNE.toString());
        final String codeRedacteur = RequeteUtil.renvoyerParametre(requete, "CODE_REDACTEUR");
        // A SUPPRIMER dans une future version
        // (présent pour des raisons de compatibilité avec les contenus existants)
        if (sDateDebut.length() == 0) {
            sDateDebut = RequeteUtil.renvoyerParametre(requete, "DATE_ARTICLE_DEBUT");
        }
        if (sDateFin.length() == 0) {
            sDateFin = RequeteUtil.renvoyerParametre(requete, "DATE_ARTICLE_FIN");
        }
        Date dateDebut = null;
        Date dateFin = null;
        String orderDate = StringUtils.EMPTY;
        String orderTri = StringUtils.EMPTY;
        if (selection.length() > 0) {
            final Date today = new Date(System.currentTimeMillis());
            orderDate = "T1.DATE_ARTICLE";
            // Derniers articles parus
            if ("0003".equals(selection)) {
                dateFin = today; // aujourd'hui
                orderTri = " DESC";
            }
            // Articles du jour
            else if ("0004".equals(selection)) {
                // date début dans la journée ou date de fin dans la journée
                dateDebut = today; // aujourd'hui
                dateFin = today; // aujourd'hui
            }
            // Articles de la semaine
            else if ("0005".equals(selection)) {
                // date début dans la semaine ou date de fin dans la semaine
                dateDebut = DateUtil.getFirstDayOfWeek(today);
                dateFin = DateUtil.getLastDayOfWeek(today);
            }
            // Articles du mois
            else if ("0006".equals(selection)) {
                // date début dans le mois ou date de fin dans le mois
                dateDebut = DateUtil.getFirstDayOfMonth(today);
                dateFin = DateUtil.getLastDayOfMonth(today);
            }
            // Dernières articles publiés
            else if ("0007".equals(selection)) {
                orderDate = "META.META_DATE_MISE_EN_LIGNE";
                orderTri = "DESC";
            }
        } else if (nbJours.length() > 0) {
            dateDebut = DateUtil.addDays(new Date(System.currentTimeMillis()), -Integer.parseInt(nbJours)); // aujourd'hui - nbJours
            dateFin = new Date(System.currentTimeMillis());
            orderDate = "T1.DATE_ARTICLE";
            orderTri = "DESC";
        } else if (sDateDebut.length() > 0 || sDateFin.length() > 0) {
            dateDebut = DateUtil.parseDate(sDateDebut);
            dateFin = DateUtil.parseDate(sDateFin);
            orderDate = "T1.DATE_ARTICLE";
            orderTri = "DESC";
        }
        if ("DATE_DESC".equals(tri)) {
            orderTri = "DESC";
        }
        if ("DATE_ASC".equals(tri)) {
            orderTri = "ASC";
        }
        String order = "T1.ORDRE, T1.TITRE";
        if (orderDate.length() > 0) {
            order = orderDate + " " + orderTri + ", " + order;
        }
        return select(titre, dateDebut, dateFin, thematique, codeRubrique, codeRattachement, etatObjet, langue, codeRedacteur, nombre, order, ids, code);
    }

    /**
     * Renvoie la liste des references a cette fiche.
     *
     * @return the references
     */
    @Override
    public String getReferences() {
        String res = FicheUnivMgr.getReferenceParPhoto(getIdVignette()) + StringUtils.defaultString(getCorps()) + FicheUnivMgr.getReferenceParJointure("structure", getCodeRattachement());
        res += FicheUnivMgr.getReferenceParListePhoto(getListeFichiers()) + FicheUnivMgr.getReferenceParListePhoto(getListeFichiersIndices());
        return res;
    }

    /**
     * Selection d'une instance de l'objet Article a partir de l'ensemble des criteres.
     *
     * @param titre
     *            the titre
     * @param dateDebut
     *            the date debut
     * @param dateFin
     *            the date fin
     * @param thematique
     *            the thematique
     * @param codeRubrique
     *            the code rubrique
     * @param codeRattachement
     *            the code rattachement
     * @param etatObjet
     *            the etat objet
     * @param langue
     *            the langue
     * @param codeRedacteur
     *            the code redacteur
     * @param nombre
     *            the nombre
     * @param ordre
     *            the ordre
     * @param ids
     *            the ids
     * @param code
     *            the code
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String titre, final Date dateDebut, final Date dateFin, final String thematique, final String codeRubrique, final String codeRattachement, final String etatObjet, final String langue, final String codeRedacteur, final String nombre, final String ordre, final String ids, final String code) throws Exception {
        final ClauseWhere where = new ClauseWhere();
        if (StringUtils.isNotEmpty(titre)) {
            where.setPremiereCondition(ConditionHelper.rechercheMots("TITRE", titre));
        }
        if (Formateur.estSaisie(dateDebut)) {
            where.and(ConditionHelper.critereDateDebut("DATE_ARTICLE", dateDebut));
        }
        if (Formateur.estSaisie(dateFin)) {
            where.and(ConditionHelper.critereDateFin("DATE_ARTICLE", dateFin));
        }
        if (StringUtils.isNotEmpty(thematique) && !"0000".equals(thematique)) {
            where.and(ConditionHelper.likePourValeursMultiple("THEMATIQUE", thematique));
        }
        if (StringUtils.isNotEmpty(ids) && !"0000".equals(ids)) {
            where.and(ConditionHelper.in("ID_ARTICLE", Chaine.getVecteurPointsVirgules(ids)));
        }
        if (StringUtils.isNotEmpty(codeRattachement)) {
            where.and(ConditionHelper.getConditionStructure("CODE_RATTACHEMENT", codeRattachement));
        }
        if (StringUtils.isNotEmpty(langue) && !"0000".equals(langue)) {
            where.and(ConditionHelper.egalVarchar("T1.LANGUE", langue));
        }
        if (StringUtils.isNotEmpty(etatObjet) && !"0000".equals(etatObjet)) {
            where.and(ConditionHelper.egalVarchar("T1.ETAT_OBJET", etatObjet));
        }
        if (StringUtils.isNotEmpty(codeRedacteur)) {
            where.and(ConditionHelper.egalVarchar("CODE_REDACTEUR", codeRedacteur));
        }
        if (StringUtils.isNotEmpty(code)) {
            where.and(ConditionHelper.egalVarchar("T1.CODE", code));
        }
        final RequeteSQL requeteSelect = RequeteSQLHelper.getRequeteGenerique(where, ctx, this, codeRubrique);
        ClauseOrderBy orderBy = new ClauseOrderBy();
        if (StringUtils.isNotEmpty(ordre)) {
            orderBy = OrderByHelper.reconstruireClauseOrderBy(ordre);
        } else {
            orderBy.orderBy("T1.ORDRE", ClauseOrderBy.SensDeTri.ASC).orderBy("T1.TITRE", ClauseOrderBy.SensDeTri.ASC);
        }
        if (orderBy != null && StringUtils.contains(orderBy.formaterSQL(), "META.")) {
            final boolean existJoinMeta = CollectionUtils.exists(requeteSelect.getJointures(), new Predicate() {

                @Override
                public boolean evaluate(final Object o) {
                    final ClauseJoin jointure = (ClauseJoin) o;
                    return StringUtils.contains(jointure.getNomTable(), "METATAG");
                }
            });
            if (!existJoinMeta) {
                requeteSelect.join(ClauseJoinHelper.creerJointureMetaTag(this));
            }
        }
        requeteSelect.orderBy(orderBy);
        final ClauseLimit limite = LimitHelper.ajouterCriteresLimitesEtOptimisation(ctx, nombre);
        requeteSelect.limit(limite);
        return select(requeteSelect.formaterRequete());
    }

    /**
     * Récupération du libellé à afficher (spécifique au back-office).
     *
     * @return the libelle back office
     *
     * @throws Exception
     *             the exception
     */
    public String getLibelleBackOffice() {
        return getLibelleAffichable();
    }

    /**
     * renvoie le corps de l'article (utilise dans SaisieCommentaire) (non-Javadoc).
     *
     * @return the contenu
     *
     * voir com.univ.objetspartages.om.DescriptionCommentaire#getContenu()
     */
    public String getContenu() {
        return getCorps();
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.FicheExportableXML#exportNodeFiche(org.w3c.dom.Node, java.lang.String)
     */
    @Override
    public Node exportNodeFiche(final Node liste, final String formatExport) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final org.w3c.dom.Document document = liste.getOwnerDocument();
        final Node fiche = document.createElement("ARTICLE");
        NodeUtil.addNode(fiche, "CODE", getCode());
        NodeUtil.addNode(fiche, "LANGUE", getLangue());
        NodeUtil.addNode(fiche, "RATTACHEMENT", serviceStructure.getAttachementLabel(getCodeRattachement(), getLangue(), true));
        NodeUtil.addNode(fiche, "LIBELLE", getLibelleAffichable());
        NodeUtil.addNode(fiche, "TITRE", getTitre());
        NodeUtil.addNode(fiche, "SOUS_TITRE", getTitre());
        NodeUtil.addNode(fiche, "DATE_ARTICLE", Formateur.formaterDate(getDateArticle(), StringUtils.EMPTY));
        NodeUtil.addNode(fiche, "CHAPEAU", getChapeau());
        NodeUtil.addNode(fiche, "CORPS", getCorps());
        NodeUtil.addNode(fiche, "URL_VIGNETTE", MediaUtils.getUrlAbsolue(serviceMedia.getById(getIdVignette())));
        NodeUtil.addNode(fiche, "CODE_RUBRIQUE", getCodeRubrique());
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueFiche = serviceRubrique.getRubriqueByCode(getCodeRubrique());
        if (rubriqueFiche != null) {
            NodeUtil.addNode(fiche, "RUBRIQUE", rubriqueFiche.getIntitule());
        } else {
            NodeUtil.addNode(fiche, "RUBRIQUE", StringUtils.EMPTY);
        }
        return fiche;
    }

    /**
     * Gets the diffusion mode restriction.
     *
     * @return the diffusionModeRestriction
     */
    @Override
    public String getDiffusionModeRestriction() {
        return persistenceBean.getDiffusionModeRestriction();
    }

    /**
     * Sets the diffusion mode restriction.
     *
     * @param diffusionModeRestriction
     *            the diffusionModeRestriction to set
     */
    @Override
    public void setDiffusionModeRestriction(final String diffusionModeRestriction) {
        persistenceBean.setDiffusionModeRestriction(diffusionModeRestriction);
    }

    /**
     * Gets the diffusion public vise.
     *
     * @return the diffusionPublicVise
     */
    @Override
    public String getDiffusionPublicVise() {
        return persistenceBean.getDiffusionPublicVise();
    }

    /**
     * Sets the diffusion public vise.
     *
     * @param diffusionPublicVise
     *            the diffusionPublicVise to set
     */
    @Override
    public void setDiffusionPublicVise(final String diffusionPublicVise) {
        persistenceBean.setDiffusionPublicVise(diffusionPublicVise);
    }

    /**
     * Gets the diffusion public vise restriction.
     *
     * @return the diffusionPublicViseRestriction
     */
    @Override
    public String getDiffusionPublicViseRestriction() {
        return persistenceBean.getDiffusionPublicViseRestriction();
    }

    /**
     * Sets the diffusion public vise restriction.
     *
     * @param diffusionPublicViseRestriction
     *            the diffusionPublicViseRestriction to set
     */
    @Override
    public void setDiffusionPublicViseRestriction(final String diffusionPublicViseRestriction) {
        persistenceBean.setDiffusionPublicViseRestriction(diffusionPublicViseRestriction);
    }
}
