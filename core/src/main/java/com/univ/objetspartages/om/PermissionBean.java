package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.StringTokenizer;

/**
 * The Class PermissionBean.
 *
 * @author jean-sébastien steux
 *
 *         Stockage d'une permission 3 types de permission existe FICHE, TECH, MODULE - FICHE/CODEOBJET/ACTION (ex:FICHE/0016/C) cf les fichiers
 *         /WEB-INF/tables/permission_action_fiche.dat - TECH/CODETECHNIQUE/ACTION (ex:TECH/rub/) cf les fichier /WEB-INF/tables/permission_objet_tech.dat et
 *         /WEB-INF/tables/permission_action_tech_CODETECHNIQUE.dat - IDMODULE/CODETECHNIQUE/ACTION ces permissions sont chargées dynamiquement par spring sur le module lui même
 */
public class PermissionBean implements Serializable {

    private static final long serialVersionUID = -5844953727518029499L;

    public static final String TYPE_FICHE = "FICHE";

    public static final String TYPE_TECHNIQUE = "TECH";

    /** The type. */
    private String type = "";

    /** The objet. */
    private String objet = "";

    /** The action. */
    private String action = "";

    /** The chaine serialisee. */
    private String chaineSerialisee = null;

    /**
     * The Constructor.
     *
     * @param type
     *            the type
     * @param objet
     *            the objet
     * @param action
     *            the action
     */
    public PermissionBean(String type, String objet, String action) {
        this.type = type;
        this.objet = objet;
        this.action = action;
        chaineSerialisee = type + "/" + objet + "/" + action;
    }

    /**
     * The Constructor.
     *
     * @param chaineStockage
     *            the chaine stockage
     */
    public PermissionBean(String chaineStockage) {
        super();
        StringTokenizer st = new StringTokenizer(chaineStockage, "/");
        int i = 0;
        while (st.hasMoreTokens()) {
            String val = st.nextToken();
            switch (i) {
                case 0:
                    this.type = val;
                    break;
                case 1:
                    this.objet = val;
                    break;
                case 2:
                    this.action = val;
                    break;
            }
            i++;
        }
        chaineSerialisee = type + "/" + objet + "/" + action;
    }

    /**
     * Gets the action.
     *
     * @return Returns the action.
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the action.
     *
     * @param action
     *            The action to set.
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Gets the objet.
     *
     * @return Returns the objet.
     */
    public String getObjet() {
        return objet;
    }

    /**
     * Sets the objet.
     *
     * @param objet
     *            The objet to set.
     */
    public void setObjet(String objet) {
        this.objet = objet;
    }

    /**
     * Gets the type.
     *
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the chaine serialisee.
     *
     * @return the chaine serialisee
     */
    public String getChaineSerialisee() {
        return chaineSerialisee;
    }

    /**
     * Méthode toString
     * @return toString
     */
    public String toString() {
        return getChaineSerialisee();
    }
}
