package com.univ.objetspartages.om;

import java.util.Date;
// TODO: Auto-generated Javadoc

/**
 * Interface definissant les accès sur un objet mappé avec un utilisateur. Elle est notamment utilisée pour la synchronisation des objets métier avec un utilisateur.
 *
 * @author jbiard
 * @see com.univ.utils.ISynchroniseurUtilisateurMetier
 */
public interface IUtilisateurMapping {

    /**
     * Gets the code.
     *
     * @return the code
     */
    String getCode();

    /**
     * Sets the code.
     *
     * @param codeUtilisateur
     *            the new code
     */
    void setCode(String codeUtilisateur);

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    String getNom();

    /**
     * Sets the nom.
     *
     * @param nom
     *            the new nom
     */
    void setNom(String nom);

    /**
     * Gets the prenom.
     *
     * @return the prenom
     */
    String getPrenom();

    /**
     * Sets the prenom.
     *
     * @param prenom
     *            the new prenom
     */
    void setPrenom(String prenom);

    /**
     * Gets the adresse mail.
     *
     * @return the adresse mail
     */
    String getAdresseMail();

    /**
     * Sets the adresse mail.
     *
     * @param adresseMail
     *            the new adresse mail
     */
    void setAdresseMail(String adresseMail);

    /**
     * Gets the civilite.
     *
     * @return the civilite
     */
    String getCivilite();

    /**
     * Sets the civilite.
     *
     * @param civilite
     *            the new civilite
     */
    void setCivilite(String civilite);

    /**
     * Gets the login.
     *
     * @return the login
     */
    String getLogin();

    /**
     * Sets the login.
     *
     * @param login
     *            the new login
     */
    void setLogin(String login);

    /**
     * Gets the date naissance.
     *
     * @return the date naissance
     */
    Date getDateNaissance();

    /**
     * Sets the date naissance.
     *
     * @param date
     *            the new date naissance
     */
    void setDateNaissance(Date date);

    /**
     * Gets the code rattachement.
     *
     * @return the code rattachement
     */
    String getCodeRattachement();

    /**
     * Sets the code rattachement.
     *
     * @param codeRattachement
     *            the new code rattachement
     */
    void setCodeRattachement(String codeRattachement);
    // definis la source import de l'utilisateur, cad la provennance lors de l'import

    /**
     * Sets the source import utilisateur.
     *
     * @param sourceImportUtilisateur
     *            the new source import utilisateur
     */
    void setSourceImportUtilisateur(String sourceImportUtilisateur);

    /**
     * Gets the source import utilisateur.
     *
     * @return the source import utilisateur
     */
    String getSourceImportUtilisateur();
}
