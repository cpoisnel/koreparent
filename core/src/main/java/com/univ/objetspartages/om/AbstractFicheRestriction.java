package com.univ.objetspartages.om;

import com.univ.objetspartages.bean.AbstractRestrictionFicheBean;

public abstract class AbstractFicheRestriction<T extends AbstractRestrictionFicheBean> extends AbstractFiche<T> implements DiffusionSelective {

    @Override
    public String getDiffusionModeRestriction() {
        return persistenceBean.getDiffusionModeRestriction();
    }

    @Override
    public String getDiffusionPublicVise() {
        return persistenceBean.getDiffusionPublicVise();
    }

    @Override
    public String getDiffusionPublicViseRestriction() {
        return persistenceBean.getDiffusionPublicViseRestriction();
    }

    @Override
    public void setDiffusionModeRestriction(final String diffusionModeRestriction) {
        persistenceBean.setDiffusionModeRestriction(diffusionModeRestriction);
    }

    @Override
    public void setDiffusionPublicVise(final String diffusionPublicVise) {
        persistenceBean.setDiffusionPublicVise(diffusionPublicVise);
    }

    @Override
    public void setDiffusionPublicViseRestriction(final String diffusionPublicViseRestriction) {
        persistenceBean.setDiffusionPublicViseRestriction(diffusionPublicViseRestriction);
    }
}
