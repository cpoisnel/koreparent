package com.univ.objetspartages.om;

/**
 * Fiche à diffusion sélective.
 */
public interface DiffusionSelective {

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (10/01/2003 14:52:43)
     *
     * @return java.lang.String
     */
    String getDiffusionModeRestriction();

    /**
     * Gets the diffusion public vise.
     *
     * @return the diffusion public vise
     */
    String getDiffusionPublicVise();

    /**
     * Gets the diffusion public vise restriction.
     *
     * @return the diffusion public vise restriction
     */
    String getDiffusionPublicViseRestriction();

    /**
     * Sets the diffusion mode restriction.
     *
     * @param diffusionModeRestriction
     *            the new diffusion mode restriction
     */
    void setDiffusionModeRestriction(String diffusionModeRestriction);

    /**
     * Sets the diffusion public vise.
     *
     * @param diffusionPublicVise
     *            the new diffusion public vise
     */
    void setDiffusionPublicVise(String diffusionPublicVise);

    /**
     * Sets the diffusion public vise restriction.
     *
     * @param diffusionPublicViseRestriction
     *            the new diffusion public vise restriction
     */
    void setDiffusionPublicViseRestriction(String diffusionPublicViseRestriction);
}