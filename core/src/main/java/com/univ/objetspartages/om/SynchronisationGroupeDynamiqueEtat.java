package com.univ.objetspartages.om;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
// TODO: Auto-generated Javadoc

/**
 * Cette classe permet de retourner un les résultats d'une synchronisation. Ces resulats présentent un ensemble d'information ayant fait l'objet de synchronisation. Cette classe
 * permet de contenir :
 * <ul>
 * <li>la liste des libellés des objets qui pendants la synchronisation ont fait l'objet d'un ajout de groupe</li>
 * <li>la liste des libellés des objets qui pendants la synchronisation ont fait l'objet d'une modification de groupe</li>
 * <li>la liste des libellés des objets qui pendants la synchronisation ont fait l'objet d'une suppression de groupe</li>
 * </ul>
 *
 * @author Pierre Cosson
 */
public class SynchronisationGroupeDynamiqueEtat {

    /** Variqble qui indique si la synchronisation s'est bien effectuée. */
    private boolean valide = false;

    /**
     * Liste des libelles des objets ayant entrainés un ajout de groupe. <br/>
     * Type : {@link Map}&lt; {@link String}, {@link String} &gt;
     */
    private Map<String, String> libellesCodesSynchroAjoutes = null;

    /**
     * Liste des libelles des objets ayant entrainés une suppression de groupe. <br/>
     * Type : {@link Map}&lt; {@link String}, {@link String} &gt;
     */
    private Map<String, String> libellesCodesSynchroSupprimes = null;

    /**
     * Liste des libelles des objets ayant entrainés une modification de groupe. <br/>
     * Type : {@link Map}&lt; {@link String}, {@link String} &gt;
     */
    private Map<String, String> libellesCodesSynchroModifies = null;

    /** Le type de synchronisation. */
    private String codeTypeGroupeSynchronise = "";

    /**
     * Constructeur.
     */
    public SynchronisationGroupeDynamiqueEtat() {
        this.valide = false;
        this.libellesCodesSynchroAjoutes = new HashMap<>();
        this.libellesCodesSynchroModifies = new HashMap<>();
        this.libellesCodesSynchroSupprimes = new HashMap<>();
        this.codeTypeGroupeSynchronise = "";
    }

    /**
     * Retourne le nombre de groupes dynamiques créés.
     *
     * @return le nombre de groupes dynamiques d'espaces créés
     */
    public int getNbGroupesAjoutes() {
        return this.libellesCodesSynchroAjoutes.size();
    }

    /**
     * Retourne le nombre de groupes dynamiques modifiés.
     *
     * @return le nombre de groupes dynamiques d'espaces modifiés
     */
    public int getNbGroupesModifies() {
        return this.libellesCodesSynchroModifies.size();
    }

    /**
     * Retourne le nombre de groupes dynamiques supprimés.
     *
     * @return le nombre de groupes dynamiques d'espaces supprimés
     */
    public int getNbGroupesSupprimes() {
        return this.libellesCodesSynchroSupprimes.size();
    }

    /**
     * Savoir si l'état de la synchronisation est valide ou non.
     *
     * @return Si l'état est valide alors on retourne TRUE.
     */
    public boolean isValide() {
        return valide;
    }

    /**
     * Définir l'état de la synchronisation.
     *
     * @param valide
     *            TRUE si valide sinon FALSE.
     */
    public void setValide(final boolean valide) {
        this.valide = valide;
    }

    /**
     * LSpécifier le code du type de groupe synchronisé. Cela permet de connaitre quel type de groupe a été synchronisé.
     *
     * @return Le code du type de groupe.
     */
    public String getCodeTypeGroupeSynchronise() {
        return codeTypeGroupeSynchronise;
    }

    /**
     * Spécifier le code de type des groupes synchronisés afin de connaityre quel sorte de groupes a été synchronisés.
     *
     * @param codeTypeGroupeSynchronise
     *            Le code de type de groupe synchronisé.
     */
    public void setCodeTypeGroupeSynchronise(final String codeTypeGroupeSynchronise) {
        this.codeTypeGroupeSynchronise = codeTypeGroupeSynchronise;
    }

    /**
     * Récupérer la liste des libellés d'objets ayant nécessité un ajout de groupe.
     *
     * @return La liste des libellés d'objet ayant necessité un ajout de groupes (cette liste est ordonnées selon les labelles des groupes).. <br/>
     *         Type : {@link Collection}&lt; {@link String} &gt;
     */
    public Collection<String> getLibellesGroupeSynchroAjoutes() {
        final Collection<String> returnValues = new TreeSet<>();
        returnValues.addAll(this.libellesCodesSynchroAjoutes.values());
        return returnValues;
    }

    /**
     * Récupérer la liste des libellés d'objets ayant nécessité une suppression de groupe (cette liste est ordonnées selon les labelles des groupes).
     *
     * @return La liste des libellés d'objet ayant necessité une suppression de groupes (cette liste est ordonnées selon les labelles des groupes).<br/>
     *         Type : {@link Collection}&lt; {@link String} &gt;
     */
    public Collection<String> getLibellesGroupeSynchroSupprimes() {
        final Collection<String> returnValues = new TreeSet<>();
        returnValues.addAll(this.libellesCodesSynchroSupprimes.values());
        return returnValues;
    }

    /**
     * Récupérer la liste des libellés d'objets ayant nécessité une modification de groupes (cette liste est ordonnées selon les labelles des groupes).
     *
     * @return La liste des libellés d'objet ayant necessité une modification de groupes (cette liste est ordonnées selon les labelles des groupes).<br/>
     *         Type : {@link Collection}&lt; {@link String} &gt;
     */
    public Collection<String> getLibellesGroupeSynchroModifies() {
        final Collection<String> returnValues = new TreeSet<>();
        returnValues.addAll(this.libellesCodesSynchroModifies.values());
        return returnValues;
    }

    /**
     * Ajouter un libelle à la liste des libelles ayant antrainé un ajout de groupe.
     *
     * @param codeGroupe
     *            le code du groupe qui a été ajouté.
     * @param libelle
     *            Le libelle de l'objet.
     */
    public void addGroupeSynchroAjoute(final String codeGroupe, final String libelle) {
        this.libellesCodesSynchroAjoutes.put(codeGroupe, libelle);
    }

    /**
     * Ajouter un libelle à la liste des libelles ayant antrainé une modification de groupe.
     *
     * @param codeGroupe
     *            le code du groupe qui a été modifié.
     * @param libelle
     *            Le libelle de l'objet.
     */
    public void addGroupeSynchroModifie(final String codeGroupe, final String libelle) {
        this.libellesCodesSynchroModifies.put(codeGroupe, libelle);
    }

    /**
     * Ajouter un libelle à la liste des libelles ayant antrainé une suppression de groupe.
     *
     * @param codeGroupe
     *            le code du groupe qui a été supprimé.
     * @param libelle
     *            Le libelle de l'objet.
     */
    public void addGroupeSynchroSupprime(final String codeGroupe, final String libelle) {
        this.libellesCodesSynchroSupprimes.put(codeGroupe, libelle);
    }

    /**
     * Récupérer les codes liés aux libellés des groupes ajoutés.
     *
     * @return {@link Map} contenant le code du groupe en clé et le libellé en valeur. <br/>
     *         Type : {@link Map}&lt; {@link String}, {@link String} &gt;
     */
    public Map<String, String> getLibellesCodesSynchroAjoutes() {
        return libellesCodesSynchroAjoutes;
    }

    /**
     * Récupérer les codes liés aux libellés des groupes supprimés.
     *
     * @return {@link Map} contenant le code du groupe en clé et le libellé en valeur. <br/>
     *         Type : {@link Map}&lt; {@link String}, {@link String} &gt;
     */
    public Map<String, String> getLibellesCodesSynchroSupprimes() {
        return libellesCodesSynchroSupprimes;
    }

    /**
     * Récupérer les codes liés aux libellés des groupes modifiés.
     *
     * @return {@link Map} contenant le code du groupe en clé et le libellé en valeur. <br/>
     *         Type : {@link Map}&lt; {@link String}, {@link String} &gt;
     */
    public Map<String, String> getLibellesCodesSynchroModifies() {
        return libellesCodesSynchroModifies;
    }
}
