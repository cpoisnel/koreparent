package com.univ.objetspartages.om;

import java.util.Date;

/**
 * The Interface AnnuaireModele.
 */
public interface AnnuaireModele extends FicheUniv {

    /**
     * Gets the civilite.
     *
     * @return the civilite
     */
    String getCivilite();

    /**
     * Sets the civilite.
     *
     * @param _civilite
     *            the new civilite
     */
    void setCivilite(String _civilite);

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    String getNom();

    /**
     * Sets the nom.
     *
     * @param _nom
     *            the new nom
     */
    void setNom(String _nom);

    /**
     * Gets the prenom.
     *
     * @return the prenom
     */
    String getPrenom();

    /**
     * Sets the prenom.
     *
     * @param _nom
     *            the new prenom
     */
    void setPrenom(String _nom);

    /**
     * Gets the type population.
     *
     * @return the type population
     */
    String getTypePopulation();

    /**
     * Sets the type population.
     *
     * @param _typePopulation
     *            the new type population
     */
    void setTypePopulation(String _typePopulation);

    /**
     * Gets the fonction.
     *
     * @return the fonction
     */
    String getFonction();

    /**
     * Sets the fonction.
     *
     * @param _fonction
     *            the new fonction
     */
    void setFonction(String _fonction);

    /**
     * Gets the corps.
     *
     * @return the corps
     */
    String getCorps();

    /**
     * Sets the corps.
     *
     * @param _corps
     *            the new corps
     */
    void setCorps(String _corps);

    /**
     * Gets the discipline.
     *
     * @return the discipline
     */
    String getDiscipline();

    /**
     * Sets the discipline.
     *
     * @param _discipline
     *            the new discipline
     */
    void setDiscipline(String _discipline);

    /**
     * Gets the adresse.
     *
     * @return the adresse
     */
    String getAdresse();

    /**
     * Sets the adresse.
     *
     * @param _adresse
     *            the new adresse
     */
    void setAdresse(String _adresse);

    /**
     * Gets the adresse mail.
     *
     * @return the adresse mail
     */
    String getAdresseMail();

    /**
     * Sets the adresse mail.
     *
     * @param _adresseMail
     *            the new adresse mail
     */
    void setAdresseMail(String _adresseMail);

    /**
     * Gets the bureau1.
     *
     * @return the bureau1
     */
    String getBureau1();

    /**
     * Sets the bureau1.
     *
     * @param _bureau1
     *            the new bureau1
     */
    void setBureau1(String _bureau1);

    /**
     * Gets the bureau2.
     *
     * @return the bureau2
     */
    String getBureau2();

    /**
     * Sets the bureau2.
     *
     * @param _bureau2
     *            the new bureau2
     */
    void setBureau2(String _bureau2);

    /**
     * Gets the telephone1.
     *
     * @return the telephone1
     */
    String getTelephone1();

    /**
     * Sets the telephone1.
     *
     * @param _telephone1
     *            the new telephone1
     */
    void setTelephone1(String _telephone1);

    /**
     * Gets the telephone2.
     *
     * @return the telephone2
     */
    String getTelephone2();

    /**
     * Sets the telephone2.
     *
     * @param _telephone2
     *            the new telephone2
     */
    void setTelephone2(String _telephone2);

    /**
     * Gets the telecopie1.
     *
     * @return the telecopie1
     */
    String getTelecopie1();

    /**
     * Sets the telecopie1.
     *
     * @param _telecopie1
     *            the new telecopie1
     */
    void setTelecopie1(String _telecopie1);

    /**
     * Gets the telecopie2.
     *
     * @return the telecopie2
     */
    String getTelecopie2();

    /**
     * Sets the telecopie2.
     *
     * @param _telecopie2
     *            the new telecopie2
     */
    void setTelecopie2(String _telecopie2);

    /**
     * Gets the liste rouge.
     *
     * @return the liste rouge
     */
    String getListeRouge();

    /**
     * Sets the liste rouge.
     *
     * @param _listeRouge
     *            the new liste rouge
     */
    void setListeRouge(String _listeRouge);

    /**
     * Gets the complements.
     *
     * @return the complements
     */
    String getComplements();

    /**
     * Sets the complements.
     *
     * @param _complements
     *            the new complements
     */
    void setComplements(String _complements);

    /**
     * Gets the cv.
     *
     * @return the cv
     */
    String getCv();

    /**
     * Sets the cv.
     *
     * @param _cv
     *            the new cv
     */
    void setCv(String _cv);

    /* (non-Javadoc)
     * @see com.univ.objetspartages.sgbd.FicheUnivDB#getCodeRattachement()
     */
    @Override
    String getCodeRattachement();

    /* (non-Javadoc)
     * @see com.univ.objetspartages.sgbd.FicheUnivDB#setCodeRattachement(java.lang.String)
     */
    @Override
    void setCodeRattachement(String _codeRattachement);

    /**
     * Gets the code rattachement autres.
     *
     * @return the code rattachement autres
     */
    String getCodeRattachementAutres();

    /**
     * Sets the code rattachement autres.
     *
     * @param _codeRattachement
     *            the new code rattachement autres
     */
    void setCodeRattachementAutres(String _codeRattachement);

    /**
     * Gets the date naissance.
     *
     * @return the date naissance
     */
    Date getDateNaissance();

    /**
     * Sets the date naissance.
     *
     * @param _dateNaissance
     *            the new date naissance
     */
    void setDateNaissance(Date _dateNaissance);

    /**
     * Gets the discipline enseignee.
     *
     * @return the discipline enseignee
     */
    String getDisciplineEnseignee();

    /**
     * Sets the discipline enseignee.
     *
     * @param _disciplineEnseignee
     *            the new discipline enseignee
     */
    void setDisciplineEnseignee(String _disciplineEnseignee);

    /**
     * Gets the adresse site perso.
     *
     * @return the adresse site perso
     */
    String getAdresseSitePerso();

    /**
     * Sets the adresse site perso.
     *
     * @param _adresseSitePerso
     *            the new adresse site perso
     */
    void setAdresseSitePerso(String _adresseSitePerso);
}
