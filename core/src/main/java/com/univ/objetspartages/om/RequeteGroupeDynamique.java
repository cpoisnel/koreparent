package com.univ.objetspartages.om;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;
import com.univ.objetspartages.services.ServiceUser;

/**
 * Classe absraite definissant la gestion de groupe dynamique et notamment la prise en charge de cache.
 *
 * Pour le cache, on utilise la source d'import de la table Groupeutilisateur pour identifier ce qui est genere par la requete.
 *
 * PCO 02-02-2009 : Patch pour rendre plus générique et évolutif la gestion des groupes dynamiques.
 *
 * @author jean-seb, jbiard
 */
public abstract class RequeteGroupeDynamique {

    /** The Constant SYNCHRO_ACTION_AJOUT_MODIFICATION. */
    public final static int SYNCHRO_ACTION_AJOUT_MODIFICATION = 0;

    /** The Constant SYNCHRO_ACTION_SUPPRESSION. */
    public final static int SYNCHRO_ACTION_SUPPRESSION = 1;

    private static final Logger LOG = LoggerFactory.getLogger(RequeteGroupeDynamique.class);

    /** The ctx. */
    @Deprecated
    protected OMContext ctx;

    protected ServiceUser serviceUser;

    protected ServiceGroupeDsi serviceGroupeDsi;

    protected ServiceGroupeUtilisateur serviceGroupeUtilisateur;

    /** The delai rechgt resolution groupes ut. */
    private long delaiRechgtResolutionGroupesUt;

    private String nomRequete;

    public RequeteGroupeDynamique() {
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
        serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
    }

    /**
     * Positionne le contexte.
     *
     * @param ctx
     *            contexte
     * @deprecated Méthode plus nécessaire
     */
    @Deprecated
    public void setCtx(final OMContext ctx) {
    }

    public void setNomRequete(final String nomRequete) {
        this.nomRequete = nomRequete;
    }

    public String getNomRequete() {
        return nomRequete;
    }

    /**
     * Positionne le delai d'expiration du cache des groupes de l'utilisateur.
     *
     * @param delaiRechgtResolutionGroupesUt
     *            delai en minutes
     */
    public void setDelaiRechargementResolutionGroupesUtilisateur(final long delaiRechgtResolutionGroupesUt) {
        this.delaiRechgtResolutionGroupesUt = delaiRechgtResolutionGroupesUt;
    }

    /**
     * Prend en charge la resolution code utilisateur -> groupes. Gere un cache si le delai de rechargement est different de 0.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur dont on souhaite recuperer les groupes
     * @param ts
     *            the ts
     *
     * @return codes des groupes rattaches
     *
     * @throws Exception
     *             the exception
     */
    public Vector<String> getGroupesUtilisateurCache(final String codeUtilisateur, final long ts) throws Exception {
        final String sourceImport = "[" + getNomRequete() + "]";
        if (delaiRechgtResolutionGroupesUt == 0) {
            LOG.debug("*** Cache desactive pour " + sourceImport);
            // cache desactive
            return getGroupesUtilisateur(codeUtilisateur);
        }
        // cache active
        final UtilisateurBean utilisateur = serviceUser.getByCode(codeUtilisateur);
        // lecture des relations relatives a la requete
        // on etablit le timestamp
        final long tsMin = ts / 60000;
        final Set<String> setGroupesCache = new HashSet<>();
        List<GroupeUtilisateurBean> results = serviceGroupeUtilisateur.getByUserCodeAndImportSource(codeUtilisateur, sourceImport);
        for (GroupeUtilisateurBean groupeUtilisateurBean : results) {
            setGroupesCache.add(groupeUtilisateurBean.getCodeGroupe());
        }
        // recuperation du timestamp du dernier chargement des groupes rattaches
        final long timestamp = utilisateur.getTsCacheGroupes();
        // verification si on doit recharger
        if (timestamp != tsMin && (timestamp + delaiRechgtResolutionGroupesUt >= tsMin)) {
            LOG.debug("*** Cache encore valable pour " + codeUtilisateur + sourceImport);
            // rechargement : lecture du cache en BD
            // on remonte les groupes de l'utilisateur (il peut n'y en avoir aucun)
            return new Vector<>(setGroupesCache);
        }
        LOG.debug("*** Cache expire pour " + codeUtilisateur + sourceImport);
        LOG.debug("*** Création d'un cache utilisateur pour " + codeUtilisateur);
        // on doit effectuer le chargement
        final Vector<String> listeGroupes = getGroupesUtilisateur(codeUtilisateur);
        // comparaison entre ce qui a ete lu en cache et ce qui vient d etre charge
        if (!setGroupesCache.isEmpty()) {
            String codeGroupe;
            // invalidation des groupes en cache qui n'ont pas de lien de parente avec ceux calcules
            for (final Iterator<String> itCodeGroupeCharge = listeGroupes.iterator(); itCodeGroupeCharge.hasNext() && !setGroupesCache.isEmpty(); ) {
                codeGroupe = itCodeGroupeCharge.next();
                // l'utilisateur fait il maintenant partie d'un nouveau groupe qui a ete mis en cache ?
                if (!setGroupesCache.remove(codeGroupe)) {
                    if (!supprimeGroupesHierarchie(codeGroupe, setGroupesCache)) {
                        // le groupe n'a pas pu etre supprime des groupes en cache et ne fait pas partie de la hierarchie d'un groupe en cache
                        // => on l'invalide
                        invalideCacheGroupe(codeGroupe, sourceImport);
                    }
                } else {
                    supprimeGroupesHierarchie(codeGroupe, setGroupesCache);
                }
            }
            // reste-t-il des groupes en cache
            if (!setGroupesCache.isEmpty()) {
                // s'il reste des groupes en cache auquel apppartient l'utilisateur, cela signifie que l'utilisateur
                // n'en fait plus partie => on invalide le groupe
                for (final String string : setGroupesCache) {
                    invalideCacheGroupe(string, sourceImport);
                }
            }
        }
        miseEnCacheGroupesUtilisateurCache(utilisateur, listeGroupes, tsMin, sourceImport);
        return listeGroupes;
    }

    /**
     * Invalide un groupe et ses parents.
     *
     * @param codeGroupeCache
     *            code du groupe en cache
     * @param sourceImport
     *            the source import
     *
     * @throws Exception
     *             the exception
     */
    private void invalideCacheGroupe(final String codeGroupeCache, final String sourceImport) throws Exception {
        final Long tsInit = (long) 0;
        List<GroupeDsiBean> groupsByCodeAndCache = serviceGroupeDsi.getByCodeAndCache(codeGroupeCache, "1");
        if (CollectionUtils.isNotEmpty(groupsByCodeAndCache)) {
            GroupeDsiBean groupeDsiBean = groupsByCodeAndCache.get(0);
            groupeDsiBean.setDerniereMajCache(tsInit);
            serviceGroupeDsi.save(groupeDsiBean);
        }
        serviceGroupeUtilisateur.deleteByGroupAndImportSource(codeGroupeCache, sourceImport);
        LOG.debug("*** on invalide le cache du groupe " + codeGroupeCache);
        final GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupeCache);
        // on invalide les groupes parents
        for (int niveau = serviceGroupeDsi.getLevel(group); niveau > 0; --niveau) {
            serviceGroupeUtilisateur.deleteByGroupAndImportSource(group.getCodeGroupePere(), sourceImport);
            List<GroupeDsiBean> parentGroupsByCodeAndCache = serviceGroupeDsi.getByCodeAndCache(group.getCodeGroupePere(), "1");
            if (CollectionUtils.isNotEmpty(parentGroupsByCodeAndCache)) {
                final GroupeDsiBean groupeDsiBean = groupsByCodeAndCache.get(0);
                groupeDsiBean.setDerniereMajCache(tsInit);
                serviceGroupeDsi.save(groupeDsiBean);
            }
        }
    }


    /**
     * Supprime de l'ensemble des groupes en cache ceux qui sont dans la hierarchie.
     *
     * @param codeGroupe
     *            the code groupe
     * @param setGroupesCache
     *            the set groupes cache
     *
     * @return true, if supprime groupes hierarchie
     *
     * @throws Exception
     *             the exception
     */
    private boolean supprimeGroupesHierarchie(final String codeGroupe, final Set<String> setGroupesCache) throws Exception {
        boolean groupeSupprime = false;
        final GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupe);
        // recherche dans les groupes parents
        // on supprime de la hierarchie les groupes peres
        for (int niveau = serviceGroupeDsi.getLevel(group); niveau > 0; --niveau) {
            groupeSupprime = setGroupesCache.remove(group.getCodeGroupePere());
            if (groupeSupprime) {
                LOG.debug("*** Suppression du groupe pere " + group.getCodeGroupePere());
            }
        }
        return groupeSupprime;
    }

    /**
     * Met en cache les groupes de l'utilisateur.
     *
     * @param listeGroupes
     *            liste des groupes rattaches
     * @param tsMin
     *            timestamp en min pour le cache
     * @param sourceImport
     *            source import pour la requete
     * @param utilisateur
     *            the utilisateur
     *
     * @throws Exception
     *             the exception
     */
    private void miseEnCacheGroupesUtilisateurCache(final UtilisateurBean utilisateur, final Vector<String> listeGroupes, final Long tsMin, final String sourceImport) throws Exception {
        if (!listeGroupes.isEmpty()) {
            // ajout des nouvelles relations avec timestamp
            for (String codeGroupe : listeGroupes) {
                GroupeUtilisateurBean groupeUtilisateurBean = serviceGroupeUtilisateur.getByUserCodeGroupCodeAndImportSource(utilisateur.getCode(), codeGroupe, sourceImport);
                // on ajoute les couples que s'ils n'existent pas deja
                if (groupeUtilisateurBean == null) {
                    groupeUtilisateurBean = new GroupeUtilisateurBean();
                    groupeUtilisateurBean.setCodeUtilisateur(utilisateur.getCode());
                    groupeUtilisateurBean.setCodeGroupe(codeGroupe);
                    groupeUtilisateurBean.setSourceImport(sourceImport);
                    serviceGroupeUtilisateur.save(groupeUtilisateurBean);
                }
            }
        }
        // maj timestamp au niveau de l'utilisateur
        utilisateur.setTsCacheGroupes(tsMin);
        serviceUser.save(utilisateur);
    }

    /**
     * Renvoie les groupes d'un utilisateur.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur
     *
     * @return codes des groupes rattaches
     *
     * @throws Exception
     *             the exception
     */
    protected abstract Vector<String> getGroupesUtilisateur(String codeUtilisateur) throws Exception;

    /**
     * Renvoie les utilisateurs correspondant a une requete de groupe avec prise en charge du cache.
     *
     * @param codeGroupe
     *            the requete groupe
     * @param setCodesGroupesCache
     *            the set codes groupes cache
     *
     * @return the vecteur utilisateurs cache
     *
     * @throws Exception
     *             the exception
     */
    public Vector<String> getVecteurUtilisateursCache(final String codeGroupe, final Set<String> setCodesGroupesCache) throws Exception {
        return getVecteurUtilisateursCache(codeGroupe, setCodesGroupesCache, System.currentTimeMillis());
    }

    /**
     * Renvoie les utilisateurs correspondant a une requete de groupe avec prise en charge du cache.
     *
     * @param codeGroupe
     *            the requete groupe
     * @param setCodesGroupesCache
     *            the set codes groupes cache
     * @param ts
     *            the ts
     *
     * @return the vecteur utilisateurs cache
     *
     * @throws Exception
     *             the exception
     */
    private Vector<String> getVecteurUtilisateursCache(final String codeGroupe, final Set<String> setCodesGroupesCache, final long ts) throws Exception {
        Vector<String> listeCodesUtilisateurs = null;
        if (!estGereParReqDyn(codeGroupe)) {
            listeCodesUtilisateurs = new Vector<>(0);
        } else {
            List<GroupeDsiBean> groupsByCodeAndCache = serviceGroupeDsi.getByCodeAndCache(codeGroupe, "1");
            if (CollectionUtils.isEmpty(groupsByCodeAndCache)) {
                return getVecteurUtilisateurs(codeGroupe);
            }
            GroupeDsiBean groupeDsi = groupsByCodeAndCache.get(0);
            final long tsMin = ts / 60000; // timestamp stocke en minutes
            final String sourceImport = "[" + getNomRequete() + "]";
            if (groupeDsi.getDelaiExpirationCache() + groupeDsi.getDerniereMajCache() >= tsMin) {
                LOG.debug("*** Cache encore valable pour " + codeGroupe);
                if (setCodesGroupesCache == null) {
                    return new Vector<>(serviceGroupeUtilisateur.getUserCodeByGroupAndImportSource(codeGroupe, sourceImport));
                }
            } else {
                LOG.debug("*** Cache expire pour " + codeGroupe);
                Set<String> setUtilisateursCache = new HashSet<>();
                final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
                List<GroupeUtilisateurBean> results = serviceGroupeUtilisateur.getByGroupCodeAndImportSource(codeGroupe, sourceImport);
                for (GroupeUtilisateurBean groupeUtilisateurBean : results) {
                    setUtilisateursCache.add(groupeUtilisateurBean.getCodeUtilisateur());
                }
                // rechargement necessaire
                listeCodesUtilisateurs = getVecteurUtilisateurs(codeGroupe);
                if (!setUtilisateursCache.isEmpty()) {
                    String codeUtilisateur;
                    for (final String string : listeCodesUtilisateurs) {
                        codeUtilisateur = string;
                        if (!setUtilisateursCache.remove(codeUtilisateur)) {
                            // l'utilisateur apparait maintenant dans le groupe => on invalide son cache
                            invalideCacheUtilisateur(codeUtilisateur);
                        }
                    }
                    if (setUtilisateursCache.size() > 0) {
                        for (final String string : setUtilisateursCache) {
                            // l'utilisateur a ete supprime du groupe => on invalide son cache
                            invalideCacheUtilisateur(string);
                        }
                    }
                }
                miseEnCacheUtilisateursGroupes(groupeDsi, listeCodesUtilisateurs, sourceImport, tsMin);
            }
            if (setCodesGroupesCache != null) {
                setCodesGroupesCache.add(groupeDsi.getCode());
            }
        }
        return listeCodesUtilisateurs;
    }

    /**
     * Invalide le cache d'un utilisateur.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur
     *
     * @throws Exception
     *             the exception
     */
    private void invalideCacheUtilisateur(final String codeUtilisateur) throws Exception {
        final UtilisateurBean utilisateur = serviceUser.getByCode(codeUtilisateur);
        utilisateur.setTsCacheGroupes((long) 0);
        serviceUser.save(utilisateur);
        LOG.debug("*** Cache de l'utilisateur " + codeUtilisateur + " invalide.");
    }

    /**
     * Met en cache les groupes de l'utilisateur.
     *
     * @param groupeDsi
     *            groupe a mettre en cache
     * @param listeCodesUtilisateurs
     *            utilisateurs rattaches
     * @param sourceImport
     *            source de l'import de la requete
     * @param tsMin
     *            timestamp
     *
     * @throws Exception
     *             the exception
     */
    private void miseEnCacheUtilisateursGroupes(final GroupeDsiBean groupeDsi, final Vector<String> listeCodesUtilisateurs, final String sourceImport, final Long tsMin) throws Exception {
        // suppression de l'ancien cache
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        serviceGroupeUtilisateur.deleteByGroupAndImportSource(groupeDsi.getCode(), sourceImport);
        if (!listeCodesUtilisateurs.isEmpty()) {
            // maj du cache : ajout des nouvelles relations avec timestamp
            final GroupeUtilisateurBean groupeutilisateur = new GroupeUtilisateurBean();
            for (String codeUtilisateur : listeCodesUtilisateurs) {
                groupeutilisateur.setCodeUtilisateur(codeUtilisateur);
                groupeutilisateur.setCodeGroupe(groupeDsi.getCode());
                groupeutilisateur.setSourceImport(sourceImport);
                serviceGroupeUtilisateur.save(groupeutilisateur);
            }
            // maj du timestamp en minutes, et ce meme si il n'y a personne dans le groupe
            groupeDsi.setDerniereMajCache(tsMin);
            serviceGroupeDsi.save(groupeDsi);
        }
    }

    /**
     * Retourne les utilisateurs d'un groupe a partir de la requete d'un groupe.
     *
     * @param requeteGroupe
     *            requete du groupe
     *
     * @return utilisateurs rattaches
     *
     * @throws Exception
     *             the exception
     */
    protected abstract Vector<String> getVecteurUtilisateurs(String requeteGroupe) throws Exception;

    /**
     * Indique si la requete est prise en charge par l'implementation. Permet d'optimiser la gestion de cache.
     *
     * @param requeteGroupe
     *            requete du groupe dynamique
     *
     * @return vrai si la requete est geree
     */
    protected abstract boolean estGereParReqDyn(String requeteGroupe);

    /**
     * Préparer l'interface de saisie d'un groupe dynamique .Cette fonction est appelée lors de l'appel au processus de saisie de groupe DSI.<br/>
     * Il faut absolument qu'il y ait une propriété dans {@link InfoBean} nommée : "REQUETE_LDAP_" + NOM_REQUETE ainsi que dans l'interface pour être retournée au processsus.
     *
     * @param infoBean
     *            the info bean
     * @param groupe
     *            the groupe
     *
     * @throws Exception
     *             the exception
     */
    public abstract void preparerPRINCIPAL(InfoBean infoBean, GroupeDsiBean groupe) throws Exception;

    /**
     * Ce traitement qui permet de sauvegarder la requete de groupe dynamique, il s'agit d'un traitement de base : ajout de la requete de groupe dynamique dnas le groupe et du type
     * de requete.
     *
     * @param infoBean
     *            the info bean
     * @param groupe
     *            the groupe
     *
     * @throws Exception
     *             the exception
     */
    public void traiterPRINCIPAL(final InfoBean infoBean, final GroupeDsiBean groupe) throws Exception {
        groupe.setRequeteGroupe(this.getNomRequete());
        groupe.setRequeteLdap(infoBean.getString("REQUETE_LDAP_" + this.getNomRequete()));
    }
}
