package com.univ.objetspartages.om;

import java.util.Vector;
// TODO: Auto-generated Javadoc

/**
 * Paragraphe de page libre Date de création : (11/01/2002 16:15:16).
 *
 * @author :
 */
public class ParagrapheBean {

    /** The ligne. */
    private int ligne = 0;

    /** The colonne. */
    private int colonne = 0;

    /** The largeur. */
    private int largeur = 0;

    /** The contenu. */
    private java.lang.String contenu = "";

    /**
     * Commentaire relatif au constructeur ParagrapheBean.
     */
    public ParagrapheBean() {
        super();
    }

    /**
     * Commentaire relatif au constructeur ParagrapheBean.
     *
     * @param _ligne
     *            the _ligne
     * @param _colonne
     *            the _colonne
     * @param _largeur
     *            the _largeur
     */
    public ParagrapheBean(int _ligne, int _colonne, int _largeur) {
        super();
        setLigne(_ligne);
        setColonne(_colonne);
        setLargeur(_largeur);
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (11/01/2002 16:16:28)
     *
     * @return int
     */
    public int getColonne() {
        return colonne;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (11/01/2002 16:29:32)
     *
     * @return java.lang.String
     */
    public java.lang.String getContenu() {
        return contenu;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (11/01/2002 16:16:43)
     *
     * @return int
     */
    public int getLargeur() {
        return largeur;
    }

    /**
     * Gets the ligne.
     *
     * @return the ligne
     */
    public int getLigne() {
        return ligne;
    }

    /**
     * Renvoie la liste des sous-paragraphes
     *
     * Le texte est formaté de la manière suivante : dfsdqf dqsfqsdf qsdf [titre;libellé-titre;couleur] sdfsdf qsdfqsdf qfdqsdf [titre;libellé-titre;couleur] sqdfqsdf qsdfqsdf.
     *
     * @return the sous paragraphes
     *
     * @throws Exception
     *             the exception
     */
    public Vector<SousParagrapheBean> getSousParagraphes() throws Exception {
        return SousParagrapheBean.getSousParagraphes(getContenu());
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (11/01/2002 16:16:28)
     *
     * @param newColonne
     *            int
     */
    public void setColonne(int newColonne) {
        colonne = newColonne;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (11/01/2002 16:29:32)
     *
     * @param newContenu
     *            java.lang.String
     */
    public void setContenu(java.lang.String newContenu) {
        contenu = newContenu;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (11/01/2002 16:16:43)
     *
     * @param newLargeur
     *            int
     */
    public void setLargeur(int newLargeur) {
        largeur = newLargeur;
    }

    /**
     * Sets the ligne.
     *
     * @param newLigne
     *            the new ligne
     */
    public void setLigne(int newLigne) {
        ligne = newLigne;
    }
}