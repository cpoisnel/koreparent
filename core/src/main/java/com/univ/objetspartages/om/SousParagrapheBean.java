package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.utils.Chaine;
// TODO: Auto-generated Javadoc

/**
 * Fonctions de découpage d'un paragraphe en sous-paragraphes Chaque sous-paragraphe est précédé d'une balise [titre].
 */
public class SousParagrapheBean implements Serializable {

    /** The Constant STYLE_TITRE_INVISIBLE. */
    static final public int STYLE_TITRE_INVISIBLE = 0;

    /** The Constant STYLE_TITRE_NORMAL. */
    static final public int STYLE_TITRE_NORMAL = 1;

    /** The Constant STYLE_TITRE_MINI. */
    static final public int STYLE_TITRE_MINI = 2;

    /** The Constant STYLE_TITRE_MAXI. */
    static final public int STYLE_TITRE_MAXI = 3;

    /** The Constant STYLE1. */
    static final public int STYLE1 = 1;

    /** The Constant STYLE2. */
    static final public int STYLE2 = 2;

    /** The Constant STYLE3. */
    static final public int STYLE3 = 3;

    private static final Logger LOGGER = LoggerFactory.getLogger(SousParagrapheBean.class);

    private static final long timeout = 10000;

    private static final long serialVersionUID = -9144551189789663592L;

    /** The contenu. */
    private String contenu = "";

    /** The titre. */
    private String titre = "";

    /** The couleur. */
    private String couleur = "";

    /** The style. */
    private int style = STYLE_TITRE_INVISIBLE;

    /**
     * Extraction d'un sous-paragraphe
     *
     * On enleve le dernier '\n' avant le tag titre et le '\n' après le tag titre
     *
     * qsfsqfd qsdfqsdf <..... [titre] <...... qsdfsqdf qsdfqsfd qsdfqsfdsqdf
     *
     * @param texte
     *            the texte
     * @param _indexDebutParagraphe
     *            the _index debut paragraphe
     * @param _indexFinParagraphe
     *            the _index fin paragraphe
     *
     * @return the string
     */
    private static String extraireSousParagraphe(final String texte, final int _indexDebutParagraphe, final int _indexFinParagraphe) {
        String contenu = texte.substring(_indexDebutParagraphe, _indexFinParagraphe);
        /* Nettoyage début sous-Paragraphe */
        if (_indexDebutParagraphe > 0) {
            final int indiceCR = contenu.indexOf("<br />");
            /*On eleve le '\n' qui précède le titre */
            if ((indiceCR == 0) || (indiceCR == 1)) {
                contenu = contenu.substring(indiceCR + 6, contenu.length());
            }
        }
        /* Nettoyage fin sous-Paragraphe */
        if (contenu.length() > 0) {
            if (contenu.endsWith("<br />")) {
                contenu = contenu.substring(0, contenu.length() - 6);
            }
        }
        return contenu;
    }

    /**
     * test le temps passé depuis start et renvoit vrai si supérieur au timeout
     *
     * @param start
     * @return
     */
    public static boolean timeout(final long start, final String texte) {
        if ((System.currentTimeMillis() - start) > timeout) {
            LOGGER.warn("Timeout sur l'éxecution de la méthode getSousParagraphes avec le contenu suivant : " + texte);
            return true;
        }
        return false;
    }

    /**
     * Renvoie la liste des sous-paragraphes
     *
     * Le texte est formaté de la manière suivante : dfsdqf dqsfqsdf qsdf [titre;libellé-titre;couleur] sdfsdf qsdfqsdf qfdqsdf [titre;libellé-titre;couleur] sqdfqsdf qsdfqsdf.
     *
     * @param texte le contenu que l'on souhaite transformer en collection de SousParagrapheBean
     *
     * @return l'ensenble des sous paragraphes
     *
     */
    public static Vector<SousParagrapheBean> getSousParagraphes(String texte) {
        final long start = System.currentTimeMillis();
        final Vector<SousParagrapheBean> listeSousParagraphes = new Vector<>();
        if (StringUtils.isNotBlank(texte)) {
            SousParagrapheBean sousParagraphe = new SousParagrapheBean();
            int indexTexte = 0;
            int indexDebutMotCle = -1;
            int indexDebutParagraphe = 0;
            //AM 2003 10 : nouveaux tags : style1,style2,style3, on les transforme pour qu'ils soient interprétés
            int indexDebutStyle = -1, indexFinStyle = -1;
            String titre = "";
            // VIN Gestion des styles supplementaires
            final StringBuilder strbTexte = new StringBuilder();
            String strTexteRestant = texte;
            //on boucle sur les tags [stylexxx; valides pour substituer les styles connus
            while (contientTagStyleValide(strTexteRestant)) {
                // test timeout
                if (timeout(start, texte)) {
                    return listeSousParagraphes;
                }
                indexDebutStyle = StringUtils.indexOfIgnoreCase(strTexteRestant, "[style");
                indexFinStyle = strTexteRestant.indexOf(";", indexDebutStyle);
                final String style = strTexteRestant.substring(indexDebutStyle, indexFinStyle);
                if ("[style1".equalsIgnoreCase(style)) {
                    titre = "[titre";
                } else if ("[style2".equalsIgnoreCase(style)) {
                    titre = "[titrecourt";
                } else if ("[style3".equalsIgnoreCase(style)) {
                    titre = "[titrelong";
                } else {
                    titre = style;
                }
                // recopie du debut de la chaine jusqu'au debut de [style
                strbTexte.append(strTexteRestant.substring(indexTexte, indexDebutStyle));
                strbTexte.append(titre);
                strTexteRestant = strTexteRestant.substring(indexFinStyle);
            }
            // recopie de toute la chaine apres le dernier [style
            strbTexte.append(strTexteRestant);
            texte = strbTexte.toString();
            strTexteRestant = texte;
            int intIndexTitre, intIndexStyle;
            /* Boucle sur chaque  paragraphe */
            while (contientTagTitreValide(StringUtils.substring(texte, indexTexte)) || contientTagStyleValide(StringUtils.substring(texte, indexTexte))) {
                // test timeout
                if (timeout(start, texte)) {
                    return listeSousParagraphes;
                }
                // On regarde quelle est la premiere occurence trouvee
                intIndexTitre = texte.indexOf("[titre", indexTexte);
                intIndexStyle = texte.indexOf("[style", indexTexte);
                if (intIndexTitre != -1 && intIndexStyle != -1) {
                    if (intIndexTitre < intIndexStyle) {
                        indexDebutMotCle = intIndexTitre;
                    } else {
                        indexDebutMotCle = intIndexStyle;
                    }
                } else if (intIndexTitre != -1) {
                    indexDebutMotCle = intIndexTitre;
                } else if (intIndexStyle != -1) {
                    indexDebutMotCle = intIndexStyle;
                }
                /* Extraction et ajout du paragraphe courant */
                if (indexDebutMotCle > 0) {
                    sousParagraphe.setContenu(extraireSousParagraphe(texte, indexDebutParagraphe, indexDebutMotCle));
                    listeSousParagraphes.add(sousParagraphe);
                }
                /* Construction du paragraphe suivant */
                sousParagraphe = new SousParagrapheBean();
                /* Analyse de la ligne contenant le tag */
                int indiceFinLigne = texte.indexOf("\n", indexDebutMotCle);
                if (indiceFinLigne == -1) {
                    indiceFinLigne = texte.length();
                }
                final String contenuLigne = texte.substring(indexDebutMotCle + 1, indiceFinLigne);
                final int indexFinMotCle = contenuLigne.indexOf(']');
                if (indexFinMotCle != -1) {
                    /* Analyse du tag paragraphe */
                    final String contenuTag = Chaine.remplacerPointsVirgules(contenuLigne.substring(0, indexFinMotCle), true);
                    final StringTokenizer st = new StringTokenizer(contenuTag, "*");
                    int indiceToken = 0;
                    while (st.hasMoreTokens()) {
                        // test timeout
                        if (timeout(start, texte)) {
                            return listeSousParagraphes;
                        }
                        final String itemTag = st.nextToken();
                        if (indiceToken == 0) {
                            if ("titre".equalsIgnoreCase(itemTag)) {
                                sousParagraphe.setStyle(SousParagrapheBean.STYLE_TITRE_NORMAL);
                            } else if ("titrelong".equalsIgnoreCase(itemTag)) {
                                sousParagraphe.setStyle(SousParagrapheBean.STYLE_TITRE_MAXI);
                            } else if ("titrecourt".equalsIgnoreCase(itemTag)) {
                                sousParagraphe.setStyle(SousParagrapheBean.STYLE_TITRE_MINI);
                            } else {
                                // gestion des styles superieurs a 9
                                final String numStyle = itemTag.substring(5);
                                try {
                                    sousParagraphe.setStyle(Integer.parseInt(numStyle));
                                } catch (final NumberFormatException e) {
                                    LOGGER.debug("le style insere n'est pas un entier :" + itemTag, e);
                                }
                            }
                        }
                        if (indiceToken == 1) {
                            sousParagraphe.setTitre(itemTag);
                        }
                        if (indiceToken == 2) {
                            sousParagraphe.setCouleur(itemTag);
                        }
                        indiceToken++;
                    }
                    indexTexte = indexDebutMotCle + indexFinMotCle + 2;
                    indexDebutParagraphe = indexDebutMotCle + indexFinMotCle + 2;
                } else {
                    /* tag mal formaté : on ignore le tag */
                    indexDebutParagraphe = indiceFinLigne + 1;
                    indexTexte = indiceFinLigne + 1;
                }
            }
            /* Extration et ajout du dernier paragraphe */
            sousParagraphe.setContenu(extraireSousParagraphe(texte, indexDebutParagraphe, texte.length()));
            listeSousParagraphes.add(sousParagraphe);
        }
        return listeSousParagraphes;
    }

    /**
     * Retourne vrai si le texte contient une balise style bien formée, c'est à dire si le texte contient le texte "[style suivi d'un ";" suivi d'un "]"
     *
     * @param texte
     *            Le texte à analyser
     * @return vrai si le texte contient au moins une balise style valide
     */
    private static boolean contientTagStyleValide(final String texte) {
        return contientTagValide(texte, TagType.style);
    }

    /**
     * Retourne vrai si le texte contient une balise titre bien formée, c'est à dire si le texte contient le texte "[titre suivi d'un ";" suivi d'un "]"
     *
     * @param texte
     *            Le texte à analyser
     * @return vrai si le texte contient au moins une balise titre valide
     */
    private static boolean contientTagTitreValide(final String texte) {
        return contientTagValide(texte, TagType.titre);
    }

    /**
     * Retourne vrai si le texte contient une balise du type en parametre bien formée, c'est à dire si le texte contient le texte "["+type du tag suivi d'un ";" suivi d'un "]"
     *
     * @param texte
     *            Le texte à analyser
     * @return vrai si le texte contient au moins une balise du type du tag en parametre valide
     */
    private static boolean contientTagValide(final String texte, final TagType tagType) {
        final Pattern pattern = Pattern.compile("^.*\\[" + tagType + ".*;.*\\].*$", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        final Matcher m = pattern.matcher(texte);
        return m.matches();
    }

    /**
     * Remplacement d'une chaine par une autre.
     *
     * @param _tag
     *            the _tag
     * @param _chaine1
     *            the _chaine1
     * @param _chaine2
     *            the _chaine2
     *
     * @return the string
     */
    public static String remplacerChaine1parChaine2(final String _tag, final String _chaine1, final String _chaine2) {
        String tmp = "";
        int indexDebutTag = -1;
        int indexFinTag = 0;
        while ((indexDebutTag = _tag.indexOf(_chaine1, indexFinTag)) != -1) {
            tmp = tmp + _tag.substring(indexFinTag, indexDebutTag) + _chaine2;
            indexFinTag = indexDebutTag + _chaine1.length();
        }
        tmp = tmp + _tag.substring(indexFinTag);
        return tmp;
    }

    /**
     * Gets the contenu.
     *
     * @return the contenu
     */
    public String getContenu() {
        return contenu;
    }

    /**
     * Gets the couleur.
     *
     * @return the couleur
     */
    public String getCouleur() {
        return couleur;
    }

    /**
     * Gets the style.
     *
     * @return the style
     */
    public int getStyle() {
        return style;
    }

    /**
     * Gets the titre.
     *
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Retourne le titre converti en majuscule.
     *
     * @return the titre uppercase
     */
    public String getTitreUppercase() {
        String up = titre;
        up = remplacerChaine1parChaine2(up, "&#224;", "a");
        up = remplacerChaine1parChaine2(up, "&#232;", "e");
        up = remplacerChaine1parChaine2(up, "&#233;", "e");
        return up.toUpperCase();
    }

    /**
     * Sets the contenu.
     *
     * @param newContenu
     *            the new contenu
     */
    public void setContenu(final String newContenu) {
        contenu = newContenu;
    }

    /**
     * Sets the couleur.
     *
     * @param newCouleur
     *            the new couleur
     */
    public void setCouleur(final String newCouleur) {
        couleur = newCouleur;
    }

    /**
     * Sets the style.
     *
     * @param newStyle
     *            the new style
     */
    public void setStyle(final int newStyle) {
        style = newStyle;
    }

    /**
     * Sets the titre.
     *
     * @param newTitre
     *            the new titre
     */
    public void setTitre(final String newTitre) {
        titre = newTitre;
    }

    /**
     * Enum représentant les différents types de tag kportal utilisés par cette classe
     *
     */
    private enum TagType {
        style, titre
    }
}
