package com.univ.objetspartages.om;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.module.DefaultModuleImpl;
import com.univ.objetspartages.bean.MediaBean;

/**
 * The Class MediaSpecific.
 */
public abstract class SpecificMedia extends DefaultModuleImpl {

    /** The Constant INFOBEAN_KEY_URL_RESSOURCE. */
    public static final String INFOBEAN_KEY_URL_RESSOURCE = "URL_RESSOURCE";

    public String code;

    public String libelle;

    public String codeTypeLibelle;

    public String jspSaisie;

    public String jspApercu;

    public String jsInsertion;

    public String jspInsertion;

    public String urlVignette;

    public String urlIcone;

    public List<String> extensions;

    public String getCode() {
        return code;
    }

    @Override
    public String getLibelle() {
        return libelle;
    }

    @Override
    public String getLibelleAffichable() {
        return ExtensionHelper.getMessage(getIdExtension(), libelle);
    }

    public String getCodeTypeLibelle() {
        return codeTypeLibelle;
    }

    public String getJspSaisie() {
        final String extensionPath = ExtensionHelper.getExtensionManager().getExtension(getIdExtension()).getRelativePath();
        return extensionPath + "/" + jspSaisie;
    }

    public String getJspApercu() {
        final String extensionPath = ExtensionHelper.getExtensionManager().getExtension(getIdExtension()).getRelativePath();
        return extensionPath + "/" + jspApercu;
    }

    public String getJspInsertion() {
        final String extensionPath = ExtensionHelper.getExtensionManager().getExtension(getIdExtension()).getRelativePath();
        return extensionPath + "/" + jspInsertion;
    }

    public String getUrlVignette() {
        final String extensionPath = ExtensionHelper.getExtensionManager().getExtension(getIdExtension()).getRelativePath();
        return String.format("%s%s%s", extensionPath, urlVignette.startsWith("/") ? "" : "/", urlVignette);
    }

    public String getUrlIcone() {
        final String extensionPath = ExtensionHelper.getExtensionManager().getExtension(getIdExtension()).getRelativePath();
        return String.format("%s%s%s", extensionPath, urlIcone.startsWith("/") ? "" : "/", urlIcone);
    }

    public String getJsInsertion() {
        return jsInsertion;
    }

    public void setJsInsertion(final String jsInsertion) {
        this.jsInsertion = jsInsertion;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    @Override
    public void setLibelle(final String libelle) {
        this.libelle = libelle;
    }

    public void setCodeTypeLibelle(final String codeTypeLibelle) {
        this.codeTypeLibelle = codeTypeLibelle;
    }

    public void setJspSaisie(final String jspSaisie) {
        this.jspSaisie = jspSaisie;
    }

    public void setJspApercu(final String jspApercu) {
        this.jspApercu = jspApercu;
    }

    public void setJspInsertion(final String jspInsertion) {
        this.jspInsertion = jspInsertion;
    }

    public void setUrlVignette(final String urlVignette) {
        this.urlVignette = urlVignette;
    }

    public void setUrlIcone(final String urlIcone) {
        this.urlIcone = urlIcone;
    }

    public void setExtensions(final List<String> extensions) {
        this.extensions = extensions;
    }

    public List<String> getExtensions() {
        return extensions;
    }

    /**
     * Sets the contenu info bean.
     *
     * @param infoBean
     *            the info bean
     * @param sDatas
     *            the s datas
     *
     * @throws IOException
     *             the exception
     */
    public void setContenuInfoBean(final InfoBean infoBean, final String sDatas) throws IOException {
        setContenuInfoBean(infoBean, sDatas, "");
    }

    /**
     * Sets the contenu info bean.
     *
     * @param infoBean
     *            the info bean
     * @param sDatas
     *            the s datas
     * @param indice
     *            the indice
     *
     * @throws IOException
     *             the exception
     */
    public void setContenuInfoBean(final InfoBean infoBean, final String sDatas, final String indice) throws IOException {
        final Properties properties = new Properties();
        properties.load(new ByteArrayInputStream(sDatas.getBytes()));
        final Enumeration<Object> en = properties.keys();
        String sKey;
        while (en.hasMoreElements()) {
            sKey = (String) en.nextElement();
            infoBean.set(sKey + (indice.length() > 0 ? "#" + indice : ""), properties.get(sKey));
        }
    }

    /**
     * Gets the properties as string.
     *
     * @param infoBean
     *            the info bean
     *
     * @return the properties as string
     *
     * @throws IOException
     *             the exception
     */
    public String getPropertiesAsString(final InfoBean infoBean) throws IOException {
        final Properties properties = new Properties();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        setSpecificProperties(infoBean, properties);
        properties.store(baos, null);
        return baos.toString();
    }

    /**
     * Sets the specific properties.
     *
     * @param infoBean
     *            the info bean
     * @param properties
     *            the properties
     *
     */
    public abstract void setSpecificProperties(InfoBean infoBean, Properties properties);

    /**
     * Check media.
     *
     * @param infoBean
     *            the info bean
     * @param file
     *            the file
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public abstract String checkMedia(InfoBean infoBean, UploadedFile file) throws Exception;

    /**
     * Process media.
     *
     * @param infoBean
     *            the info bean
     * @param ressource
     *            the ressource
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public abstract String processMedia(InfoBean infoBean, MediaBean ressource) throws Exception;

    /**
     * Process vignette.
     *
     * @param infoBean
     *            the info bean
     * @param ressource
     *            the ressource
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public abstract String processVignette(InfoBean infoBean, MediaBean ressource) throws Exception;

    /**
     * Prepare media.
     *
     * @param infoBean
     *            the info bean
     * @param ressource
     *            the ressource
     *
     * @throws IOException
     *             the exception
     */
    public abstract void prepareMedia(InfoBean infoBean, MediaBean ressource) throws IOException;
}
