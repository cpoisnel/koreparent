package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.univ.objetspartages.bean.AbstractPersistenceBean;

/**
 * The Class ServiceBean.
 *
 * @author jean-sébastien steux
 *
 *         Stockage en mémoire des caractéristiques d'un service
 */
public class ServiceBean extends AbstractPersistenceBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1245708607172931004L;

    private Long id = 0L;

    private String code = StringUtils.EMPTY;

    private String intitule = StringUtils.EMPTY;

    private Integer expirationCache = 0;

    private Integer expirationCacheService = 0;

    private String jetonKportal = "0";

    private String proxyCas = "0";

    private String url = StringUtils.EMPTY;

    private String urlPopup = "0";

    private String vueReduiteUrl = StringUtils.EMPTY;

    private String diffusionMode = "0";

    private Collection<String> diffusionPublicVise = new ArrayList<>();

    private String diffusionModeRestriction = StringUtils.EMPTY;

    private Collection<String> diffusionPublicViseRestriction = new ArrayList<>();

    public ServiceBean(){
        //constructeur vide à cause de l'immonde présent en dessous
    }
    /**
     * The Constructor.
     *
     * @param code
     *            the code
     * @param intitule
     *            the intitule
     * @param expirationCache
     *            the expiration cache
     * @param expirationCacheService
     *            the expiration cache service
     * @param jetonKportal
     *            the jeton kportal
     * @param proxyCas
     *            the proxy cas
     * @param url
     *            the url
     * @param urlPopup
     *            the url popup
     * @param vueReduiteUrl
     *            the vue reduite url
     * @param diffusionMode
     *            the diffusion mode
     * @param diffusionPublicVise
     *            the diffusion public vise
     * @param diffusionModeRestriction
     *            the diffusion mode restriction
     * @param diffusionPublicViseRestriction
     *            the diffusion public vise restriction
     */
    public ServiceBean(final String code, final String intitule, final Integer expirationCache, final Integer expirationCacheService, final String jetonKportal, final String proxyCas, final String url, final String urlPopup, final String vueReduiteUrl, final String diffusionMode, final Vector<String> diffusionPublicVise, final String diffusionModeRestriction, final Vector<String> diffusionPublicViseRestriction) {
        this.code = code;
        this.intitule = intitule;
        this.expirationCache = expirationCache;
        this.expirationCacheService = expirationCacheService;
        this.jetonKportal = jetonKportal;
        this.proxyCas = proxyCas;
        this.url = url;
        this.urlPopup = urlPopup;
        this.vueReduiteUrl = vueReduiteUrl;
        this.diffusionMode = diffusionMode;
        this.diffusionPublicVise = diffusionPublicVise;
        this.diffusionModeRestriction = diffusionModeRestriction;
        this.diffusionPublicViseRestriction = diffusionPublicViseRestriction;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public void setExpirationCache(Integer expirationCache) {
        this.expirationCache = expirationCache;
    }

    public void setExpirationCacheService(Integer expirationCacheService) {
        this.expirationCacheService = expirationCacheService;
    }

    public void setJetonKportal(String jetonKportal) {
        this.jetonKportal = jetonKportal;
    }

    public void setProxyCas(String proxyCas) {
        this.proxyCas = proxyCas;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUrlPopup(String urlPopup) {
        this.urlPopup = urlPopup;
    }

    public void setVueReduiteUrl(String vueReduiteUrl) {
        this.vueReduiteUrl = vueReduiteUrl;
    }

    public void setDiffusionMode(String diffusionMode) {
        this.diffusionMode = diffusionMode;
    }

    public void setDiffusionPublicVise(Collection<String> diffusionPublicVise) {
        this.diffusionPublicVise = diffusionPublicVise;
    }

    public void setDiffusionModeRestriction(String diffusionModeRestriction) {
        this.diffusionModeRestriction = diffusionModeRestriction;
    }

    public void setDiffusionPublicViseRestriction(Collection<String> diffusionPublicViseRestriction) {
        this.diffusionPublicViseRestriction = diffusionPublicViseRestriction;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the intitule.
     *
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Gets the diffusion mode restriction.
     *
     * @return the diffusion mode restriction
     */
    public String getDiffusionModeRestriction() {
        return diffusionModeRestriction;
    }

    /**
     * Gets the diffusion public vise.
     *
     * @return the diffusion public vise
     */
    public Collection<String> getDiffusionPublicVise() {
        return diffusionPublicVise;
    }

    /**
     * Gets the diffusion public vise restriction.
     *
     * @return the diffusion public vise restriction
     */
    public Collection<String> getDiffusionPublicViseRestriction() {
        return diffusionPublicViseRestriction;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets the vue reduite url.
     *
     * @return the vue reduite url
     */
    public String getVueReduiteUrl() {
        return vueReduiteUrl;
    }

    /**
     * Gets the jeton kportal.
     *
     * @return the jeton kportal
     */
    public String getJetonKportal() {
        return jetonKportal;
    }

    /**
     * Gets the diffusion mode.
     *
     * @return the diffusion mode
     */
    public String getDiffusionMode() {
        return diffusionMode;
    }

    /**
     * Gets the url popup.
     *
     * @return the url popup
     */
    public String getUrlPopup() {
        return urlPopup;
    }

    /**
     * Gets the expiration cache.
     *
     * @return the expiration cache
     */
    public Integer getExpirationCache() {
        return expirationCache;
    }

    /**
     * Gets the proxy cas.
     *
     * @return the proxy cas
     */
    public String getProxyCas() {
        return proxyCas;
    }

    /**
     * Gets the expiration cache service.
     *
     * @return the expiration cache service
     */
    public Integer getExpirationCacheService() {
        return expirationCacheService;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}