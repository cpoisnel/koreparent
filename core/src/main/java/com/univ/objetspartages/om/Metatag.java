package com.univ.objetspartages.om;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.dao.impl.MetatagDAO;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * The Class Metatag.
 */
@Deprecated
public class Metatag extends AbstractOm<MetatagBean, MetatagDAO> implements Cloneable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Metatag.class);

    /** The HISTORIQU e_ annulatio n_ demand e_ validation. */
    public static final String HISTORIQUE_ANNULATION_DEMANDE_VALIDATION = "ANNULATION_DEMANDE_VALIDATION";

    /** The HISTORIQU e_ annulatio n_ suppression. */
    public static final String HISTORIQUE_ANNULATION_SUPPRESSION = "ANNULATION_SUPPRESSION";

    /** The HISTORIQU e_ archivage. */
    public static final String HISTORIQUE_ARCHIVAGE = "ARCHIVAGE";

    /** The HISTORIQU e_ archivage. */
    public static final String HISTORIQUE_ARCHIVAGE_AUTO = "ARCHIVAGE_AUTO";

    /** The HISTORIQU e_ rubriquage. */
    public static final String HISTORIQUE_RUBRIQUAGE = "RUBRIQUAGE";

    /** The HISTORIQU e_ rubriquage. */
    public static final String HISTORIQUE_RUBRIQUAGE_AUTO = "RUBRIQUAGE_AUTO";

    /** The HISTORIQU e_ creation. */
    public static final String HISTORIQUE_CREATION = "CREATION";

    /** The HISTORIQU e_ demand e_ validation. */
    public static final String HISTORIQUE_DEMANDE_VALIDATION = "DEMANDE_VALIDATION";

    /** The HISTORIQU e_ duplication. */
    public static final String HISTORIQUE_DUPLICATION = "DUPLICATION";

    /** The HISTORIQU e_ traduction. */
    public static final String HISTORIQUE_TRADUCTION = "TRADUCTION";

    /** The HISTORIQU e_ modification. */
    public static final String HISTORIQUE_MODIFICATION = "MODIFICATION";

    /** The HISTORIQU e_ restauratio n_ archivage. */
    public static final String HISTORIQUE_RESTAURATION_ARCHIVAGE = "RESTAURATION_ARCHIVAGE";

    /** The HISTORIQU e_ restauratio n_ sauvegarde. */
    public static final String HISTORIQUE_RESTAURATION_SAUVEGARDE = "RESTAURATION_SAUVEGARDE";

    /** The HISTORIQU e_ retou r_ auteur. */
    public static final String HISTORIQUE_RETOUR_AUTEUR = "RETOUR_AUTEUR";

    /** The HISTORIQU e_ suppression. */
    public static final String HISTORIQUE_SUPPRESSION = "SUPPRESSION";

    /** The HISTORIQU e_ validation. */
    public static final String HISTORIQUE_VALIDATION = "VALIDATION";

    private final ServiceMetatag serviceMetatag;

    /**
     * Instantiates a new metatag.
     */
    public Metatag() {
        super();
        serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
    }

    /**
     * Renvoie un libellé d'action (à partir du code stocké dans l'historique).
     *
     * @param action
     *            the action
     *
     * @return the intitule action
     * @deprecated Utilisez {@link MetatagUtils#getIntituleAction(String)}
     */
    @Deprecated
    public static String getIntituleAction(final String action) {
        String res = "";
        final Hashtable<String, String> actions = new Hashtable<>();
        actions.put(HISTORIQUE_SUPPRESSION, "Suppression");
        actions.put(HISTORIQUE_ANNULATION_SUPPRESSION, "Annulation suppression");
        actions.put(HISTORIQUE_RESTAURATION_SAUVEGARDE, "Restauration");
        actions.put(HISTORIQUE_DUPLICATION, "Duplication");
        actions.put(HISTORIQUE_TRADUCTION, "Traduction");
        actions.put(HISTORIQUE_CREATION, "Création");
        actions.put(HISTORIQUE_MODIFICATION, "Modification");
        actions.put(HISTORIQUE_VALIDATION, "Validation");
        actions.put(HISTORIQUE_DEMANDE_VALIDATION, "Demande de validation");
        actions.put(HISTORIQUE_ANNULATION_DEMANDE_VALIDATION, "Annulation demande ");
        actions.put(HISTORIQUE_RETOUR_AUTEUR, "Retour au rédacteur");
        actions.put(HISTORIQUE_RUBRIQUAGE, "Rubriquage ");
        actions.put(HISTORIQUE_RUBRIQUAGE_AUTO, "Rubriquage automatique ");
        actions.put(HISTORIQUE_ARCHIVAGE, "Archivage ");
        actions.put(HISTORIQUE_ARCHIVAGE_AUTO, "Archivage automatique ");
        actions.put(HISTORIQUE_RESTAURATION_ARCHIVAGE, "Restauration archive");
        if (actions.get(action) != null) {
            res = actions.get(action);
        }
        return res;
    }
    
    /**
     * Inits the.
     */
    public void init() {
    }

    /**
     * Ajoute un élément dans l'historique.
     *
     * @param evenement
     *            the evenement
     * @param code_auteur
     *            the code_auteur
     * @param etat_objet
     *            the etat_objet
     * @deprecated Utilisez {@link ServiceMetatag#addHistory(MetatagBean, String, String, String)}
     */
    @Deprecated
    public void addHistorique(final String evenement, final String code_auteur, final String etat_objet) {
        final Vector<String> v = getVecteurHistorique();
        // Suppression dernier élément
        if (v.size() > 19) {
            v.removeElementAt(v.size() - 1);
        }
        final java.util.GregorianCalendar cal = new java.util.GregorianCalendar();
        final Object[] arguments = {cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND)};
        final String dateModif = java.text.MessageFormat.format("{2,number,0000}{1,number,00}{0,number,00}{3,number,00}{4,number,00}{5,number,00}", arguments);
        final String item = "[" + evenement + "/" + dateModif + "/" + code_auteur + "/" + etat_objet + "]";
        v.insertElementAt(item, 0);
        setVecteurHistorique(v);
    }

    /**
     * Etablit la liste des références à partir de la chaine renvoyée par FicheUniv.getReferences().
     *
     * @param ficheUniv
     *            the fiche univ
     */
    public void calculerReferences(final FicheUniv ficheUniv) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        serviceMetatag.calculerReferences(getPersistenceBean(), ficheUniv.getReferences(), ficheUniv.getContenuEncadre(), ficheUniv.getLangue());
    }

    /**
     * Renvoie la date de dernière modif à partir de l'historique.
     *
     * @return the date derniere modification
     *
     * @throws Exception
     *             the exception
     */
    public GregorianCalendar getDateDerniereModification() throws Exception {
        GregorianCalendar cal = null;
        if (getVecteurHistorique().size() > 0) {
            final StringTokenizer st = new StringTokenizer(getVecteurHistorique().elementAt(0), "[/]");
            // on retient le 2ème token pour la date*/
            st.nextToken();
            final String date = st.nextToken();
            // Conversion en Calendar
            int annee, mois, jour, heure, minute, secondes;
            annee = Integer.parseInt(date.substring(0, 4));
            mois = Integer.parseInt(date.substring(4, 6));
            jour = Integer.parseInt(date.substring(6, 8));
            heure = Integer.parseInt(date.substring(8, 10));
            minute = Integer.parseInt(date.substring(10, 12));
            secondes = Integer.parseInt(date.substring(12, 14));
            cal = new java.util.GregorianCalendar(annee, mois - 1, jour, heure, minute, secondes);
        }
        return cal;
    }

    /**
     * Renvoie la liste de de l'historique.
     *
     * @return the vecteur historique
     *
     */
    public Vector<String> getVecteurHistorique() {
        final Vector<String> v = new Vector<>();
        final StringTokenizer st = new StringTokenizer(getMetaHistorique(), ";");
        while (st.hasMoreTokens()) {
            final String val = st.nextToken();
            v.add(val);
        }
        return v;
    }

    /**
     * Valorise la liste de l'historique sous forme de vecteur.
     *
     * @param v
     *            the new vecteur historique
     *            @deprecated Utilisez {@link ServiceMetatag#setHistory(MetatagBean, List)}
     *
     */
    @Deprecated
    public void setVecteurHistorique(final Vector<String> v) {
        String liste = "";
        final Enumeration<String> en = v.elements();
        while (en.hasMoreElements()) {
            liste = liste + (en.nextElement()) + ";";
        }
        setMetaHistorique(liste);
    }

    /**
     * Sélection d'une Article à partir de l'ensemble des critères combinés.
     *
     * @param codeObjet
     *            the _code objet
     * @param idFiche
     *            the id fiche
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int select(final String codeObjet, final Long idFiche) throws Exception {
        final ClauseWhere whereCodeObjetIdFiche = new ClauseWhere();
        if (StringUtils.isNotEmpty(codeObjet)) {
            whereCodeObjetIdFiche.setPremiereCondition(ConditionHelper.egalVarchar("META_CODE_OBJET", codeObjet));
        }
        if (idFiche != null) {
            whereCodeObjetIdFiche.and(ConditionHelper.egal("META_ID_FICHE", idFiche, TypeOperande.LONG));
        }
        return select(whereCodeObjetIdFiche.formaterSQL());
    }

    /**
     * Sélection de Metatag par type d'objet sur une liste de valeur
     *
     * @param codeObjet
     *            the _code objet
     * @param idsFiches
     *            the id fiche
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int selectParCodeIds(final String codeObjet, final Collection<String> idsFiches) throws Exception {
        final ClauseWhere whereCodeObjetIdFiche = new ClauseWhere();
        if (StringUtils.isNotEmpty(codeObjet)) {
            whereCodeObjetIdFiche.setPremiereCondition(ConditionHelper.egalVarchar("META_CODE_OBJET", codeObjet));
        }
        if (CollectionUtils.isNotEmpty(idsFiches)) {
            whereCodeObjetIdFiche.and(ConditionHelper.in("META_ID_FICHE", idsFiches));
        }
        return select(whereCodeObjetIdFiche.formaterSQL());
    }

    /**
     * Sélection d'une liste de Méta-tags des fiches qui référencent une fiche ou une photo déterminée .
     *
     * @param codeObjet
     *            the _code objet
     * @param code
     *            the code
     * @param langue
     *            the langue
     * @param idPhoto
     *            the id photo
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceMetatag#getByReferences(String, String, String, Long)}
     */
    @Deprecated
    public int selectParReferences(final String codeObjet, final String code, final String langue, final Long idPhoto) throws Exception {
        final ClauseWhere whereReferences = new ClauseWhere();
        if (StringUtils.isNotEmpty(codeObjet)) {
            final String nomObjet = ReferentielObjets.getNomObjet(codeObjet);
            whereReferences.setPremiereCondition(ConditionHelper.like("META_LISTE_REFERENCES", nomObjet + ";" + code + ";" + langue, "%[", "]%"));
        }
        if (!idPhoto.equals(Long.valueOf(0))) {
            whereReferences.and(ConditionHelper.like("META_LISTE_REFERENCES", String.valueOf(idPhoto), "%[photo;", "]%"));
        }
        return select(whereReferences.formaterSQL());
    }

    /**
     * Select count.
     *
     * @param sqlSuffix
     *            the sql suffix
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    public int selectCount(String sqlSuffix) throws Exception {
        return 0;
    }

    /**
     * Gets the liste objets redacteur.
     *
     * @param codeRedacteur
     *            the code redacteur
     *
     * @return the liste objets redacteur
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceMetatag#getListeObjetsRedacteur(String)}
     */
    @Deprecated
    public Hashtable<String, String> getListeObjetsRedacteur(final String codeRedacteur) throws Exception {
        return new Hashtable<>(serviceMetatag.getListeObjetsRedacteur(codeRedacteur));
    }

    /**
     * Teste si les données dupliquées dans les métas sont cohérentesavec celles de la la fiche d'origine.
     *
     * @param ficheRef
     *            the fiche ref
     *
     * @return the list
     * @deprecated Utilisez {@link ServiceMetatag#controlerCoherenceAvecFiche(MetatagBean, FicheUniv)}
     */
    @Deprecated
    public List<String> controlerCoherenceAvecFiche(final FicheUniv ficheRef) {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        return serviceMetatag.controlerCoherenceAvecFiche(getPersistenceBean(), ficheRef);
    }

    /**
     * Synchroniser.
     *
     * @param ficheUniv
     *            the fiche univ
     * @param calculerReferences
     *            the calculer references
     *
     * @throws Exception
     *             the exception
     */
    public void synchroniser(final FicheUniv ficheUniv, final boolean calculerReferences) throws Exception {
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        serviceMetatag.synchroniser(getPersistenceBean(), ficheUniv, calculerReferences);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    @Override
    public MetatagBean clone() throws CloneNotSupportedException {
        return (MetatagBean) super.clone();
    }
    
    //Rétro-compatibilité
    /**
     * Gets the meta diffusion mode restriction.
     *
     * @return the metaDiffusionModeRestriction
     */
    public String getMetaDiffusionModeRestriction() {
        return this.getPersistenceBean().getMetaDiffusionModeRestriction();
    }

    /**
     * Sets the meta diffusion mode restriction.
     *
     * @param metaDiffusionModeRestriction
     *            the metaDiffusionModeRestriction to set
     */
    public void setMetaDiffusionModeRestriction(final String metaDiffusionModeRestriction) {
        this.getPersistenceBean().setMetaDiffusionModeRestriction(metaDiffusionModeRestriction);
    }

    /**
     * Gets the meta code.
     *
     * @return the metaCode
     */
    public String getMetaCode() {
        return this.getPersistenceBean().getMetaCode();
    }

    /**
     * Sets the meta code.
     *
     * @param metaCode
     *            the metaCode to set
     */
    public void setMetaCode(final String metaCode) {
        this.getPersistenceBean().setMetaCode(metaCode);
    }

    /**
     * Gets the meta code objet.
     *
     * @return the metaCodeObjet
     */
    public String getMetaCodeObjet() {
        return this.getPersistenceBean().getMetaCodeObjet();
    }

    /**
     * Sets the meta code objet.
     *
     * @param metaCodeObjet
     *            the metaCodeObjet to set
     */
    public void setMetaCodeObjet(final String metaCodeObjet) {
        this.getPersistenceBean().setMetaCodeObjet(metaCodeObjet);
    }

    /**
     * Gets the meta code rattachement.
     *
     * @return the metaCodeRattachement
     */
    public String getMetaCodeRattachement() {
        return this.getPersistenceBean().getMetaCodeRattachement();
    }

    /**
     * Sets the meta code rattachement.
     *
     * @param metaCodeRattachement
     *            the metaCodeRattachement to set
     */
    public void setMetaCodeRattachement(final String metaCodeRattachement) {
        this.getPersistenceBean().setMetaCodeRattachement(metaCodeRattachement);
    }

    /**
     * Gets the meta code rattachement autres.
     *
     * @return the metaCodeRattachementAutres
     */
    public String getMetaCodeRattachementAutres() {
        return this.getPersistenceBean().getMetaCodeRattachementAutres();
    }

    /**
     * Sets the meta code rattachement autres.
     *
     * @param metaCodeRattachementAutres
     *            the metaCodeRattachementAutres to set
     */
    public void setMetaCodeRattachementAutres(final String metaCodeRattachementAutres) {
        this.getPersistenceBean().setMetaCodeRattachementAutres(metaCodeRattachementAutres);
    }

    /**
     * Gets the meta code redacteur.
     *
     * @return the metaCodeRedacteur
     */
    public String getMetaCodeRedacteur() {
        return this.getPersistenceBean().getMetaCodeRedacteur();
    }

    /**
     * Sets the meta code redacteur.
     *
     * @param metaCodeRedacteur
     *            the metaCodeRedacteur to set
     */
    public void setMetaCodeRedacteur(final String metaCodeRedacteur) {
        this.getPersistenceBean().setMetaCodeRedacteur(metaCodeRedacteur);
    }

    /**
     * Gets the meta code rubriquage.
     *
     * @return the metaCodeRubriquage
     */
    public String getMetaCodeRubriquage() {
        return this.getPersistenceBean().getMetaCodeRubriquage();
    }

    /**
     * Sets the meta code rubriquage.
     *
     * @param metaCodeRubriquage
     *            the metaCodeRubriquage to set
     */
    public void setMetaCodeRubriquage(final String metaCodeRubriquage) {
        this.getPersistenceBean().setMetaCodeRubriquage(metaCodeRubriquage);
    }

    /**
     * Gets the meta code rubrique.
     *
     * @return the metaCodeRubrique
     */
    public String getMetaCodeRubrique() {
        return this.getPersistenceBean().getMetaCodeRubrique();
    }

    /**
     * Sets the meta code rubrique.
     *
     * @param metaCodeRubrique
     *            the metaCodeRubrique to set
     */
    public void setMetaCodeRubrique(final String metaCodeRubrique) {
        this.getPersistenceBean().setMetaCodeRubrique(metaCodeRubrique);
    }

    /**
     * Gets the meta code validation.
     *
     * @return the metaCodeValidation
     */
    public String getMetaCodeValidation() {
        return this.getPersistenceBean().getMetaCodeValidation();
    }

    /**
     * Sets the meta code validation.
     *
     * @param metaCodeValidation
     *            the metaCodeValidation to set
     */
    public void setMetaCodeValidation(final String metaCodeValidation) {
        this.getPersistenceBean().setMetaCodeValidation(metaCodeValidation);
    }

    /**
     * Gets the meta date archivage.
     *
     * @return the metaDateArchivage
     */
    public Date getMetaDateArchivage() {
        return this.getPersistenceBean().getMetaDateArchivage();
    }

    /**
     * Sets the meta date archivage.
     *
     * @param metaDateArchivage
     *            the metaDateArchivage to set
     */
    public void setMetaDateArchivage(final Date metaDateArchivage) {
        this.getPersistenceBean().setMetaDateArchivage(metaDateArchivage);
    }

    /**
     * Gets the meta date creation.
     *
     * @return the metaDateCreation
     */
    public Date getMetaDateCreation() {
        return this.getPersistenceBean().getMetaDateCreation();
    }

    /**
     * Sets the meta date creation.
     *
     * @param metaDateCreation
     *            the metaDateCreation to set
     */
    public void setMetaDateCreation(final Date metaDateCreation) {
        this.getPersistenceBean().setMetaDateCreation(metaDateCreation);
    }

    /**
     * Gets the meta date mise en ligne.
     *
     * @return the metaDateMiseEnLigne
     */
    public Date getMetaDateMiseEnLigne() {
        return this.getPersistenceBean().getMetaDateMiseEnLigne();
    }

    /**
     * Sets the meta date mise en ligne.
     *
     * @param metaDateMiseEnLigne
     *            the metaDateMiseEnLigne to set
     */
    public void setMetaDateMiseEnLigne(final Date metaDateMiseEnLigne) {
        this.getPersistenceBean().setMetaDateMiseEnLigne(metaDateMiseEnLigne);
    }

    /**
     * Gets the meta date modification.
     *
     * @return the metaDateModification
     */
    public Date getMetaDateModification() {
        return this.getPersistenceBean().getMetaDateModification();
    }

    /**
     * Sets the meta date modification.
     *
     * @param metaDateModification
     *            the metaDateModification to set
     */
    public void setMetaDateModification(final Date metaDateModification) {
        this.getPersistenceBean().setMetaDateModification(metaDateModification);
    }

    /**
     * Gets the meta date proposition.
     *
     * @return the metaDateProposition
     */
    public Date getMetaDateProposition() {
        return this.getPersistenceBean().getMetaDateProposition();
    }

    /**
     * Sets the meta date proposition.
     *
     * @param metaDateProposition
     *            the metaDateProposition to set
     */
    public void setMetaDateProposition(final Date metaDateProposition) {
        this.getPersistenceBean().setMetaDateProposition(metaDateProposition);
    }

    /**
     * Gets the meta date rubriquage.
     *
     * @return the metaDateRubriquage
     */
    public Date getMetaDateRubriquage() {
        return this.getPersistenceBean().getMetaDateRubriquage();
    }

    /**
     * Sets the meta date rubriquage.
     *
     * @param metaDateRubriquage
     *            the metaDateRubriquage to set
     */
    public void setMetaDateRubriquage(final Date metaDateRubriquage) {
        this.getPersistenceBean().setMetaDateRubriquage(metaDateRubriquage);
    }

    /**
     * Gets the meta date suppression.
     *
     * @return the metaDateSuppression
     */
    public Date getMetaDateSuppression() {
        return this.getPersistenceBean().getMetaDateSuppression();
    }

    /**
     * Sets the meta date suppression.
     *
     * @param metaDateSuppression
     *            the metaDateSuppression to set
     */
    public void setMetaDateSuppression(final Date metaDateSuppression) {
        this.getPersistenceBean().setMetaDateSuppression(metaDateSuppression);
    }

    /**
     * Gets the meta date validation.
     *
     * @return the metaDateValidation
     */
    public Date getMetaDateValidation() {
        return this.getPersistenceBean().getMetaDateValidation();
    }

    /**
     * Sets the meta date validation.
     *
     * @param metaDateValidation
     *            the metaDateValidation to set
     */
    public void setMetaDateValidation(final Date metaDateValidation) {
        this.getPersistenceBean().setMetaDateValidation(metaDateValidation);
    }

    /**
     * Gets the meta diffusion public vise.
     *
     * @return the metaDiffusionPublicVise
     */
    public String getMetaDiffusionPublicVise() {
        return this.getPersistenceBean().getMetaDiffusionPublicVise();
    }

    /**
     * Sets the meta diffusion public vise.
     *
     * @param metaDiffusionPublicVise
     *            the metaDiffusionPublicVise to set
     */
    public void setMetaDiffusionPublicVise(final String metaDiffusionPublicVise) {
        this.getPersistenceBean().setMetaDiffusionPublicVise(metaDiffusionPublicVise);
    }

    /**
     * Gets the meta diffusion public vise restriction.
     *
     * @return the metaDiffusionPublicViseRestriction
     */
    public String getMetaDiffusionPublicViseRestriction() {
        return this.getPersistenceBean().getMetaDiffusionPublicViseRestriction();
    }

    /**
     * Sets the meta diffusion public vise restriction.
     *
     * @param metaDiffusionPublicViseRestriction
     *            the metaDiffusionPublicViseRestriction to set
     */
    public void setMetaDiffusionPublicViseRestriction(final String metaDiffusionPublicViseRestriction) {
        this.getPersistenceBean().setMetaDiffusionPublicViseRestriction(metaDiffusionPublicViseRestriction);
    }

    /**
     * Gets the meta document fichiergw.
     *
     * @return the metaDocumentFichiergw
     */
    public String getMetaDocumentFichiergw() {
        return this.getPersistenceBean().getMetaDocumentFichiergw();
    }

    /**
     * Sets the meta document fichiergw.
     *
     * @param metaDocumentFichiergw
     *            the metaDocumentFichiergw to set
     */
    public void setMetaDocumentFichiergw(final String metaDocumentFichiergw) {
        this.getPersistenceBean().setMetaDocumentFichiergw(metaDocumentFichiergw);
    }

    /**
     * Gets the meta etat objet.
     *
     * @return the metaEtatObjet
     */
    public String getMetaEtatObjet() {
        return this.getPersistenceBean().getMetaEtatObjet();
    }

    /**
     * Sets the meta etat objet.
     *
     * @param metaEtatObjet
     *            the metaEtatObjet to set
     */
    public void setMetaEtatObjet(final String metaEtatObjet) {
        this.getPersistenceBean().setMetaEtatObjet(metaEtatObjet);
    }

    /**
     * Gets the meta forum.
     *
     * @return the metaForum
     */
    public String getMetaForum() {
        return this.getPersistenceBean().getMetaForum();
    }

    /**
     * Sets the meta forum.
     *
     * @param metaForum
     *            the metaForum to set
     */
    public void setMetaForum(final String metaForum) {
        this.getPersistenceBean().setMetaForum(metaForum);
    }

    /**
     * Gets the meta forum ano.
     *
     * @return the metaForumAno
     */
    public String getMetaForumAno() {
        return this.getPersistenceBean().getMetaForumAno();
    }

    /**
     * Sets the meta forum ano.
     *
     * @param metaForumAno
     *            the metaForumAno to set
     */
    public void setMetaForumAno(final String metaForumAno) {
        this.getPersistenceBean().setMetaForumAno(metaForumAno);
    }

    /**
     * Gets the meta historique.
     *
     * @return the metaHistorique
     */
    public String getMetaHistorique() {
        return this.getPersistenceBean().getMetaHistorique();
    }

    /**
     * Sets the meta historique.
     *
     * @param metaHistorique
     *            the metaHistorique to set
     */
    public void setMetaHistorique(final String metaHistorique) {
        this.getPersistenceBean().setMetaHistorique(metaHistorique);
    }

    /**
     * Gets the meta id fiche.
     *
     * @return the metaIdFiche
     */
    public Long getMetaIdFiche() {
        return this.getPersistenceBean().getMetaIdFiche();
    }

    /**
     * Sets the meta id fiche.
     *
     * @param metaIdFiche
     *            the metaIdFiche to set
     */
    public void setMetaIdFiche(final Long metaIdFiche) {
        this.getPersistenceBean().setMetaIdFiche(metaIdFiche);
    }

    /**
     * Gets the meta in tree.
     *
     * @return the metaInTree
     */
    public String getMetaInTree() {
        return this.getPersistenceBean().getMetaInTree();
    }

    /**
     * Sets the meta in tree.
     *
     * @param metaInTree
     *            the metaInTree to set
     */
    public void setMetaInTree(final String metaInTree) {
        this.getPersistenceBean().setMetaInTree(metaInTree);
    }

    /**
     * Gets the meta langue.
     *
     * @return the metaLangue
     */
    public String getMetaLangue() {
        return this.getPersistenceBean().getMetaLangue();
    }

    /**
     * Sets the meta langue.
     *
     * @param metaLangue
     *            the metaLangue to set
     */
    public void setMetaLangue(final String metaLangue) {
        this.getPersistenceBean().setMetaLangue(metaLangue);
    }

    /**
     * Gets the meta libelle fiche.
     *
     * @return the metaLibelleFiche
     */
    public String getMetaLibelleFiche() {
        return this.getPersistenceBean().getMetaLibelleFiche();
    }

    /**
     * Sets the meta libelle fiche.
     *
     * @param metaLibelleFiche
     *            the metaLibelleFiche to set
     */
    public void setMetaLibelleFiche(final String metaLibelleFiche) {
        this.getPersistenceBean().setMetaLibelleFiche(metaLibelleFiche);
    }

    /**
     * Gets the meta libelle objet.
     *
     * @return the metaLibelleObjet
     */
    public String getMetaLibelleObjet() {
        return this.getPersistenceBean().getMetaLibelleObjet();
    }

    /**
     * Sets the meta libelle objet.
     *
     * @param metaLibelleObjet
     *            the metaLibelleObjet to set
     */
    public void setMetaLibelleObjet(final String metaLibelleObjet) {
        this.getPersistenceBean().setMetaLibelleObjet(metaLibelleObjet);
    }

    /**
     * Gets the meta liste references.
     *
     * @return the metaListeReferences
     */
    public String getMetaListeReferences() {
        return this.getPersistenceBean().getMetaListeReferences();
    }

    /**
     * Sets the meta liste references.
     *
     * @param metaListeReferences
     *            the metaListeReferences to set
     */
    public void setMetaListeReferences(final String metaListeReferences) {
        this.getPersistenceBean().setMetaListeReferences(metaListeReferences);
    }

    /**
     * Gets the meta mail anonyme.
     *
     * @return the metaMailAnonyme
     */
    public String getMetaMailAnonyme() {
        return this.getPersistenceBean().getMetaMailAnonyme();
    }

    /**
     * Sets the meta mail anonyme.
     *
     * @param metaMailAnonyme
     *            the metaMailAnonyme to set
     */
    public void setMetaMailAnonyme(final String metaMailAnonyme) {
        this.getPersistenceBean().setMetaMailAnonyme(metaMailAnonyme);
    }

    /**
     * Gets the meta meta description.
     *
     * @return the metaDescription
     */
    public String getMetaMetaDescription() {
        return this.getPersistenceBean().getMetaDescription();
    }

    /**
     * Sets the meta meta description.
     *
     * @param metaMetaDescription
     *            the metaDescription to set
     */
    public void setMetaMetaDescription(final String metaMetaDescription) {
        this.getPersistenceBean().setMetaDescription(metaMetaDescription);
    }

    /**
     * Gets the meta meta keywords.
     *
     * @return the metaKeywords
     */
    public String getMetaMetaKeywords() {
        return this.getPersistenceBean().getMetaKeywords();
    }

    /**
     * Sets the meta meta keywords.
     *
     * @param metaMetaKeywords
     *            the metaKeywords to set
     */
    public void setMetaMetaKeywords(final String metaMetaKeywords) {
        this.getPersistenceBean().setMetaKeywords(metaMetaKeywords);
    }

    /**
     * Gets the meta nb hits.
     *
     * @return the metaNbHits
     */
    public Long getMetaNbHits() {
        return this.getPersistenceBean().getMetaNbHits();
    }

    /**
     * Sets the meta nb hits.
     *
     * @param metaNbHits
     *            the metaNbHits to set
     */
    public void setMetaNbHits(final Long metaNbHits) {
        this.getPersistenceBean().setMetaNbHits(metaNbHits);
    }

    /**
     * Gets the meta niveau approbation.
     *
     * @return the metaNiveauApprobation
     */
    public String getMetaNiveauApprobation() {
        return this.getPersistenceBean().getMetaNiveauApprobation();
    }

    /**
     * Sets the meta niveau approbation.
     *
     * @param metaNiveauApprobation
     *            the metaNiveauApprobation to set
     */
    public void setMetaNiveauApprobation(final String metaNiveauApprobation) {
        this.getPersistenceBean().setMetaNiveauApprobation(metaNiveauApprobation);
    }

    /**
     * Gets the meta notification mail.
     *
     * @return the metaNotificationMail
     */
    public String getMetaNotificationMail() {
        return this.getPersistenceBean().getMetaNotificationMail();
    }

    /**
     * Sets the meta notification mail.
     *
     * @param metaNotificationMail
     *            the metaNotificationMail to set
     */
    public void setMetaNotificationMail(final String metaNotificationMail) {
        this.getPersistenceBean().setMetaNotificationMail(metaNotificationMail);
    }

    /**
     * Gets the meta rubriques publication.
     *
     * @return the metaRubriquesPublication
     */
    public String getMetaRubriquesPublication() {
        return this.getPersistenceBean().getMetaRubriquesPublication();
    }

    /**
     * Sets the meta rubriques publication.
     *
     * @param metaRubriquesPublication
     *            the metaRubriquesPublication to set
     */
    public void setMetaRubriquesPublication(final String metaRubriquesPublication) {
        this.getPersistenceBean().setMetaRubriquesPublication(metaRubriquesPublication);
    }

    /**
     * Gets the meta saisie front.
     *
     * @return the metaSaisieFront
     */
    public String getMetaSaisieFront() {
        return this.getPersistenceBean().getMetaSaisieFront();
    }

    /**
     * Sets the meta saisie front.
     *
     * @param metaSaisieFront
     *            the metaSaisieFront to set
     */
    public void setMetaSaisieFront(final String metaSaisieFront) {
        this.getPersistenceBean().setMetaSaisieFront(metaSaisieFront);
    }

    /**
     * Gets the meta source import.
     *
     * @return the metaSourceImport
     */
    public String getMetaSourceImport() {
        return this.getPersistenceBean().getMetaSourceImport();
    }

    /**
     * Sets the meta source import.
     *
     * @param metaSourceImport
     *            the metaSourceImport to set
     */
    public void setMetaSourceImport(final String metaSourceImport) {
        this.getPersistenceBean().setMetaSourceImport(metaSourceImport);
    }

    /**
     * Gets the meta date operation.
     *
     * @return the meta date operation
     */
    public Date getMetaDateOperation() {
        return this.getPersistenceBean().getMetaDateOperation();
    }

    /**
     * Sets the meta date operation.
     *
     * @param metaDateOperation
     *            the new meta date operation
     */
    public void setMetaDateOperation(final Date metaDateOperation) {
        this.getPersistenceBean().setMetaDateOperation(metaDateOperation);
    }

    public Long getIdMetatag() {
        return persistenceBean.getId();
    }

    public void setIdMetatag(Long idMetatag) {
        persistenceBean.setId(idMetatag);
    }

    public Long getId() {
        return persistenceBean.getId();
    }

    public void setId(Long idMetatag) {
        persistenceBean.setId(idMetatag);
    }

    /**
     * @deprecated no need for that anymore
     */
    @Deprecated
    public void setCtx(OMContext ctx) {
        // nothing to do here, just legacy
    }
}
