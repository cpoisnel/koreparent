package com.univ.objetspartages.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.SiteExterneBean;

/**
 * DAO gérant la liste des site externe à indexer
 */
public class SiteExterneDAO extends AbstractCommonDAO<SiteExterneBean> {

    public SiteExterneDAO() {
        tableName = "SITE";
    }

    public List<SiteExterneBean> getAllSite() {
        final List<SiteExterneBean> result;
        try {
            result = namedParameterJdbcTemplate.query("SELECT * FROM SITE ORDER BY CODE", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to query table \"SITE\"", dae);
        }
        return result;
    }
}
