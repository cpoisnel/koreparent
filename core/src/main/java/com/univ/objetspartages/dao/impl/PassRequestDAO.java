package com.univ.objetspartages.dao.impl;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.PassRequestBean;

/**
 * Created on 20/04/15.
 */
public class PassRequestDAO extends AbstractCommonDAO<PassRequestBean> {

    private static final Logger LOG = LoggerFactory.getLogger(PassRequestDAO.class);

    public PassRequestDAO() {
        tableName = "CHANGEMENTMOTPASSE";
    }

    @Override
    protected SqlParameterSource getParameters(final PassRequestBean bean) {
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("code", bean.getCode());
        parameterSource.addValue("id", bean.getId());
        parameterSource.addValue("email", bean.getEmail());
        parameterSource.addValue("uuid", bean.getUuid().toString());
        parameterSource.addValue("dateDemande", bean.getDateDemande());
        return parameterSource;
    }

    public PassRequestBean getByRequestId(final UUID requestId) {
        PassRequestBean result = null;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("requestId", requestId.toString());
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM CHANGEMENTMOTPASSE WHERE UUID = :requestId", namedParameters, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for id %s on table %s", requestId.toString(), tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect result size for id %s", requestId.toString()));
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with id %s from table %s", requestId.toString(), tableName), dae);
        }
        return result;
    }

    public List<PassRequestBean> getByCode(final String code) {
        final List<PassRequestBean> results;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("code", code);
            results = namedParameterJdbcTemplate.query("SELECT * FROM CHANGEMENTMOTPASSE WHERE CODE = :code", namedParameters, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with code %s from table %s", code, tableName),dae);
        }
        return results;
    }
}
