package com.univ.objetspartages.dao.impl;

import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.ReferentielObjets;

/**
 * Created on 15/04/15.
 */
public class MetatagDAO extends AbstractLegacyDAO<MetatagBean> {

    private static final Logger LOG = LoggerFactory.getLogger(MetatagDAO.class);

    public MetatagDAO() {
        this.tableName = "METATAG";
    }

    public MetatagBean addWithForcedId(final MetatagBean metatagBean) throws AddToDataSourceException {
        try {
            namedParameterJdbcTemplate.update("insert into METATAG (ID_METATAG, META_ID_FICHE, META_CODE_OBJET, META_LIBELLE_OBJET, META_HISTORIQUE, META_DATE_ARCHIVAGE, META_LISTE_REFERENCES, META_FORUM, META_FORUM_ANO, META_SAISIE_FRONT, META_MAIL_ANONYME, META_NOTIFICATION_MAIL, META_IN_TREE, META_DOCUMENT_FICHIERGW, META_RUBRIQUES_PUBLICATION, META_NIVEAU_APPROBATION, META_LIBELLE_FICHE, META_CODE, META_CODE_RATTACHEMENT, META_CODE_RUBRIQUE, META_KEYWORDS, META_DESCRIPTION, META_DATE_CREATION, META_DATE_PROPOSITION, META_DATE_VALIDATION, META_DATE_MODIFICATION, META_DATE_OPERATION, META_CODE_REDACTEUR, META_CODE_VALIDATION, META_LANGUE, META_ETAT_OBJET, META_NB_HITS, META_SOURCE_IMPORT, META_CODE_RATTACHEMENT_AUTRES, META_DIFFUSION_PUBLIC_VISE, META_DIFFUSION_MODE_RESTRICTION, META_DIFFUSION_PUBLIC_VISE_RESTRICTION, META_DATE_MISE_EN_LIGNE, META_DATE_SUPPRESSION, META_DATE_RUBRIQUAGE, META_CODE_RUBRIQUAGE) values (:id, :metaIdFiche, :metaCodeObjet, :metaLibelleObjet, :metaHistorique, :metaDateArchivage, :metaListeReferences, :metaForum, :metaForumAno, :metaSaisieFront, :metaMailAnonyme, :metaNotificationMail, :metaInTree, :metaDocumentFichiergw, :metaRubriquesPublication, :metaNiveauApprobation, :metaLibelleFiche, :metaCode, :metaCodeRattachement, :metaCodeRubrique, :metaKeywords, :metaDescription, :metaDateCreation, :metaDateProposition, :metaDateValidation, :metaDateModification, :metaDateOperation, :metaCodeRedacteur, :metaCodeValidation, :metaLangue, :metaEtatObjet, :metaNbHits, :metaSourceImport, :metaCodeRattachementAutres, :metaDiffusionPublicVise, :metaDiffusionModeRestriction, :metaDiffusionPublicViseRestriction, :metaDateMiseEnLigne, :metaDateSuppression, :metaDateRubriquage, :metaCodeRubriquage)", getParameters(metatagBean));
        } catch (final DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"METATAG\"", metatagBean.toString()), dae);
        }
        return metatagBean;
    }

    public MetatagBean getByCodeAndIdFiche(final String codeObjet, final Long idFiche) {
        MetatagBean result = null;
        final MapSqlParameterSource params = new MapSqlParameterSource("codeObjet", codeObjet);
        params.addValue("idFiche", idFiche);
        try {
            final List<MetatagBean> allResults = namedParameterJdbcTemplate.query("SELECT * FROM METATAG T1 WHERE T1.META_CODE_OBJET = :codeObjet AND T1.META_ID_FICHE = :idFiche;", params,
                rowMapper);
            if (allResults.isEmpty()) {
                LOG.debug(String.format("empty resultset size for codeObjet %s and idFiche %d on table %s", codeObjet, idFiche, tableName));
            } else {
                if (allResults.size() > 1) {
                    LOG.info(String.format("incorrect resultset size for codeObjet %s and idFiche %s. Found %s instead of 1", codeObjet, idFiche, allResults.size()));
                }
                result = allResults.get(0);
            }
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with codeObjet set as %s and idFiche as %s",codeObjet, idFiche), dae);
        }
        return result;
    }

    public List<MetatagBean> getMetasSinceExludingObjects(final Date date, final Collection<String> objetsExclus) {
        final List<MetatagBean> metas;
        try {
            final StringBuilder query = new StringBuilder("SELECT * FROM METATAG WHERE META_DATE_MODIFICATION >= :dateModification");
            final MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("dateModification", date, Types.DATE);
            if (CollectionUtils.isNotEmpty(objetsExclus)) {
                params.addValue("codesObjet", objetsExclus);
                query.append(" AND META_CODE_OBJET NOT IN (:codesObjet)");
            }
            metas = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with META_DATE_OPERATION set as %s and META_CODE_OBJET not in (%s)", dateFormat.format(date), StringUtils.join(objetsExclus, ",")), dae);
        }
        return metas;
    }

    public List<MetatagBean> getAllMetas(final Collection<String> objetsExclus) {
        final List<MetatagBean> metas;
        final StringBuilder query = new StringBuilder("SELECT * FROM METATAG");
        if (CollectionUtils.isNotEmpty(objetsExclus)) {
            query.append(" WHERE META_CODE_OBJET NOT IN (:codesObjet)");
        }
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("codesObjet", objetsExclus);
            metas = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with META_CODE_OBJET not in (%s)",StringUtils.join(objetsExclus, ",")), dae);
        }
        return metas;
    }

    public List<MetatagBean> getMetasForRubriquesAndObjet(final Collection<String> codesRubrique, final String... objets) {
        final List<MetatagBean> metas = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(codesRubrique) || (objets != null && objets.length > 0)) {
            try {
                final MapSqlParameterSource params = new MapSqlParameterSource();
                final StringBuilder query = new StringBuilder("SELECT * FROM METATAG");
                final StringBuilder clause = new StringBuilder();
                if(CollectionUtils.isNotEmpty(codesRubrique)) {
                    params.addValue("metaCodesRubrique", codesRubrique);
                    clause.append(" WHERE META_CODE_RUBRIQUE IN (:metaCodesRubrique)");
                }
                if (objets != null && objets.length > 0) {
                    params.addValue("codeObjet", Arrays.asList(objets));
                    clause.append(clause.length() > 0 ? " AND" : " WHERE");
                    clause.append(" META_CODE_OBJET IN (:codeObjet)");
                }
                query.append(clause);
                metas.addAll(namedParameterJdbcTemplate.query(query.toString(), params, rowMapper));
            } catch (final DataAccessException dae) {
                throw new DataSourceException(String.format("An error occured requesting on METATAG table with META_CODE_RUBRIQUE in (%s) and META_CODE_OBJET in (%s)", StringUtils.join(codesRubrique, ","), StringUtils.join(objets, ",")), dae);
            }
        }
        return metas;
    }

    public List<MetatagBean> getByObjectCode(final String objectCode) {
        final List<MetatagBean> metas;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("codeObjet", objectCode);
            metas = namedParameterJdbcTemplate.query("SELECT * FROM METATAG T1 WHERE T1.META_CODE_OBJET = :codeObjet;", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with codeObjet set as %s", objectCode), dae);
        }
        return metas;
    }

    public void deleteForCodeAndIds(final String codeObjet, final Collection<Long> listeIdFiches) {
        try {
            final StringBuilder query = new StringBuilder("DELETE FROM METATAG WHERE META_CODE_OBJET = :codeObjet");
            final MapSqlParameterSource namedParameters = new MapSqlParameterSource("codeObjet", codeObjet);
            if(CollectionUtils.isNotEmpty(listeIdFiches)) {
                namedParameters.addValue("ids", listeIdFiches);
                query.append(" AND META_ID_FICHE IN (:ids)");
            }
            namedParameterJdbcTemplate.update(query.toString(), namedParameters);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with codeObjet %s from table \"%s\"", codeObjet, tableName), dae);
        }
    }

    public List<MetatagBean> getByCodeAndIdsFiche(final String objectCode, final Long... ids) {
        final List<MetatagBean> metas;
        try {
            final StringBuilder query = new StringBuilder("SELECT * FROM METATAG T1 WHERE T1.META_CODE_OBJET = :codeObjet");
            final MapSqlParameterSource params = new MapSqlParameterSource("codeObjet", objectCode);
            if (ids != null) {
                params.addValue("ids", Arrays.asList(ids));
                query.append(" AND T1.META_ID_FICHE IN (:ids)");
            }
            metas = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with codeObjet set as %s and META_ID_FICHE in (%s)", objectCode, StringUtils.join(ids, ",")), dae);
        }
        return metas;
    }

    public List<MetatagBean> getByIds(final Long... ids) {
        final List<MetatagBean> metas = new ArrayList<>();
        if(ids != null) {
            try {
                final MapSqlParameterSource params = new MapSqlParameterSource("ids", Arrays.asList(ids));
                metas.addAll(namedParameterJdbcTemplate.query("SELECT * FROM METATAG T1 WHERE T1.ID_METATAG IN (:ids)", params, rowMapper));
            } catch (final DataAccessException dae) {
                throw new DataSourceException(String.format("An error occured requesting on METATAG table with ID_METATAG in (%s)", StringUtils.join(ids, ",")), dae);
            }
        }
        return metas;
    }

    public List<MetatagBean> getMetasForRubrique(final String codeRubrique) {
        final List<MetatagBean> metas;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("codeRubrique", codeRubrique);
            metas = namedParameterJdbcTemplate.query("SELECT * FROM METATAG T1 WHERE T1.META_CODE_RUBRIQUE = :codeRubrique", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with META_CODE_RUBRIQUE set as %s", codeRubrique), dae);
        }
        return metas;
    }

    public List<MetatagBean> getMetasForRubriqueAndStates(final String codeRubrique, Collection<String> state) {
        final List<MetatagBean> metas;
        try {
            final StringBuilder query = new StringBuilder("SELECT * FROM METATAG T1 WHERE T1.META_CODE_RUBRIQUE = :codeRubrique");
            final MapSqlParameterSource params = new MapSqlParameterSource("codeRubrique", codeRubrique);
            if(CollectionUtils.isNotEmpty(state)) {
                params.addValue("etatObjet", state);
                query.append(" AND META_ETAT_OBJET IN (:etatObjet)");
            }
            metas = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with META_CODE_RUBRIQUE set as %s", codeRubrique), dae);
        }
        return metas;
    }

    public List<MetatagBean> getByUserCode(final String codeRedacteur) {
        final List<MetatagBean> metas;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("codeRedacteur", codeRedacteur);
            metas = namedParameterJdbcTemplate.query("SELECT * FROM METATAG T1 WHERE T1.META_CODE_REDACTEUR = :codeRedacteur", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on METATAG with value %s on META_CODE_REDACTEUR", codeRedacteur), dae);
        }
        return metas;
    }

    public List<MetatagBean> getByReferences(final String codeObjet, final String codeFiche, final String langue, final Long idPhoto) {
        List<MetatagBean> metas = new ArrayList<>();
        if (StringUtils.isNotEmpty(codeObjet)) {
            final StringBuilder query = new StringBuilder("SELECT * FROM METATAG WHERE META_LISTE_REFERENCES like :references");
            final MapSqlParameterSource params = new MapSqlParameterSource();
            final String nomObjet = ReferentielObjets.getNomObjet(codeObjet);
            params.addValue("references", "%" + String.format("[%s;%s;%s]", nomObjet, codeFiche, langue)  + "%");
            if (!idPhoto.equals(0L)) {
                params.addValue("idPhoto", "%[photo;" + idPhoto + "]%");
                    query.append(" AND META_LISTE_REFERENCES like :idPhoto");
            }
            try {
                metas = namedParameterJdbcTemplate.query(query.append(";").toString(), params, rowMapper);
            } catch (final DataAccessException dae) {
                throw new DataSourceException(String.format("Unable to query on METATAG with value %s;%s;%s on META_LISTE_REFERENCES", nomObjet, codeFiche, langue), dae);
            }
        }
        return metas;
    }

    public List<MetatagBean> getMetas(final int amount) {
        final List<MetatagBean> metas;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("limit", amount);
            metas = namedParameterJdbcTemplate.query("SELECT * FROM METATAG T1 ORDER BY META_DATE_MODIFICATION DESC LIMIT :limit", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with a limit set as %s",amount), dae);
        }
        return metas;
    }

    public MetatagBean getByMetaCodeObjetCodeLangueEtat(final String codeObjet, final String code, final String langue, final String etat) {
        final StringBuilder query = new StringBuilder(
            "SELECT * FROM METATAG T1 WHERE T1.META_CODE_OBJET = :codeObjet AND T1.META_CODE = :metaCode AND T1.META_LANGUE = :metaLangue");
        MetatagBean result = null;
        if (StringUtils.isNotBlank(etat)) {
            query.append(" AND T1.META_ETAT_OBJET = :metaEtatObjet");
        }
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("codeObjet", codeObjet);
            params.addValue("metaCode", code);
            params.addValue("metaLangue", langue);
            params.addValue("metaEtatObjet", etat);
            result = namedParameterJdbcTemplate.queryForObject(query.append(";").toString(), params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for codeobjet %s metaCode %s metalangue %s and metaEtatObjet %s on table %s", codeObjet, code, langue, etat, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size for codeObjet %s and metaCode %s and metaLangue %s and metaEtatObjet %s", codeObjet, code, langue, etat));
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with codeObjet set as %s, metaCode as %s, metaLangue as %s and metaEtatObjet as %s",codeObjet, code, langue, etat), dae);
        }
        return result;
    }

    public List<MetatagBean> getMetaByObjectCodeAndState(final Collection<String> codeObjet, final EtatFiche etat) {
        final List<MetatagBean> metas;
        try {
            final StringBuilder query = new StringBuilder("SELECT * FROM METATAG WHERE META_ETAT_OBJET = :etatObjet");
            final MapSqlParameterSource params = new MapSqlParameterSource("etatObjet", etat.toString());
            if (CollectionUtils.isNotEmpty(codeObjet)) {
                query.append(" AND META_CODE_OBJET in (:codeObjet) ");
                params.addValue("codeObjet", codeObjet);
            }
            query.append(" ORDER BY META_DATE_MODIFICATION DESC");
            metas = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on METATAG table with codeObjet set as %s and metaEtatObjet as %s",codeObjet, etat.toString()), dae);
        }
        return metas;
    }

    public List<MetatagBean> getByCodeRedacteurAndCodeRubrique(final String codeRedacteur, final String codeRubrique) {
        final List<MetatagBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("codeRubrique", codeRubrique);
            params.addValue("codeRedacteur", codeRedacteur);
            results = namedParameterJdbcTemplate.query("SELECT * FROM METATAG T1 WHERE T1.META_CODE_RUBRIQUE = :codeRubrique AND T1.META_CODE_REDACTEUR = :codeRedacteur;", params,
                rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(
                String.format("Unable to query on METATAG with value %s on META_CODE_RUBRIQUE AND %s on META_CODE_REDACTEUR", codeRubrique, codeRedacteur), dae);
        }
        return results;
    }

    public List<MetatagBean> getByPhotoReferences() {
        final List<MetatagBean> results;
        try {
            results = namedParameterJdbcTemplate.query("SELECT * FROM METATAG T1 WHERE T1.META_LISTE_REFERENCES LIKE '%[photo;%'", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to query on METATAG to get the picture references ", dae);
        }
        return results;
    }
}
