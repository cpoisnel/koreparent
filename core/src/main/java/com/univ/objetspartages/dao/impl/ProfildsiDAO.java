package com.univ.objetspartages.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.ProfildsiBean;

/**
 * DAO des profils de l'appli
 */
public class ProfildsiDAO  extends AbstractLegacyDAO<ProfildsiBean> {

    private static final Logger LOG = LoggerFactory.getLogger(ProfildsiDAO.class);

    public ProfildsiDAO() {
        tableName = "PROFILDSI";
        rowMapper = new RowMapper<ProfildsiBean>() {

            @Override
            public ProfildsiBean mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final ProfildsiBean profildsiBean = new ProfildsiBean();
                profildsiBean.setIdProfildsi(rs.getLong("ID_PROFILDSI"));
                profildsiBean.setCode(rs.getString("CODE"));
                profildsiBean.setLibelle(rs.getString("LIBELLE"));
                profildsiBean.setCodeRubriqueAccueil(rs.getString("CODE_RUBRIQUE_ACCUEIL"));
                profildsiBean.setRoles(rs.getString("ROLES"));
                profildsiBean.setGroupes(deserializeBackwardCompat(rs.getString("GROUPES")));
                profildsiBean.setCodeRattachement(rs.getString("CODE_RATTACHEMENT"));
                return profildsiBean;
            }
        };
    }

    protected SqlParameterSource getParameters(final ProfildsiBean profildsiBean) {
        final MapSqlParameterSource params = new MapSqlParameterSource("id", profildsiBean.getIdProfildsi());
        params.addValue("code", profildsiBean.getCode());
        params.addValue("libelle", profildsiBean.getLibelle());
        params.addValue("codeRubriqueAccueil", profildsiBean.getCodeRubriqueAccueil());
        params.addValue("roles", profildsiBean.getRoles());
        params.addValue("groupes",  serializeBackwardCompat(profildsiBean.getGroupes()));
        params.addValue("codeRattachement", profildsiBean.getCodeRattachement());
        return params;
    }

    private String serializeBackwardCompat(final Collection<String> values) {
        String serializedValues = StringUtils.EMPTY;
        for (final String value : values) {
            serializedValues += "[" + value + "]";
        }
        return serializedValues;
    }

    private Collection<String> deserializeBackwardCompat(final String serializedValue) {
        final Collection<String> results = new ArrayList<>();
        final StringTokenizer st = new StringTokenizer(serializedValue, "[]");
        while (st.hasMoreTokens()) {
            results.add(st.nextToken());
        }
        return results;
    }

    public List<ProfildsiBean> selectAll() {
        final List<ProfildsiBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from PROFILDSI", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to query on all profile in table \"PROFILDSI\"", dae);
        }
        return results;
    }

    public ProfildsiBean getByCode(final String code) {
        ProfildsiBean result = null;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("code", code);
            result = namedParameterJdbcTemplate.queryForObject("select * from PROFILDSI T1 WHERE T1.CODE = :code", namedParameters, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", code, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect result size for code" + code);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with code %s from table %s", code, tableName), dae);
        }
        return result;
    }
}
