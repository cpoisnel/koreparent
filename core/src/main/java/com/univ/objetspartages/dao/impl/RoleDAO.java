package com.univ.objetspartages.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.RoleBean;
import com.univ.objetspartages.om.PermissionBean;

/**
 * Created by olivier.camon on 30/04/15.
 */
public class RoleDAO extends AbstractLegacyDAO<RoleBean> {

    private static final Logger LOG = LoggerFactory.getLogger(PassRequestDAO.class);

    public RoleDAO() {
        tableName = "ROLE";
        rowMapper = new RowMapper<RoleBean>() {

            @Override
            public RoleBean mapRow(final ResultSet resultSet, final int i) throws SQLException {
                final RoleBean roleBean = new RoleBean();
                roleBean.setIdRole(resultSet.getLong("ID_ROLE"));
                roleBean.setCode(resultSet.getString("CODE"));
                roleBean.setLibelle(resultSet.getString("LIBELLE"));
                roleBean.setPerimetre(resultSet.getString("PERIMETRE"));
                roleBean.setPermissions(deserializePermissions(resultSet.getString("PERMISSIONS")));
                return roleBean;
            }
        };
    }

    protected SqlParameterSource getParameters(final RoleBean roleBean) {
        final MapSqlParameterSource params = new MapSqlParameterSource("id", roleBean.getIdRole());
        params.addValue("code", roleBean.getCode());
        params.addValue("libelle", roleBean.getLibelle());
        params.addValue("perimetre", roleBean.getPerimetre());
        params.addValue("permissions", serializePermissions(roleBean.getPermissions()));
        return params;
    }

    private String serializePermissions(final Collection<PermissionBean> permissionBeans) {
        String serializedPermissions = StringUtils.EMPTY;
        for (final PermissionBean permissionBean : permissionBeans) {
            serializedPermissions += "[" + permissionBean.getChaineSerialisee() + "]";
        }
        return serializedPermissions;
    }

    private Collection<PermissionBean> deserializePermissions(final String serializedPermissions) {
        final Collection<PermissionBean> results = new ArrayList<>();
        final StringTokenizer st = new StringTokenizer(serializedPermissions, "[]");
        while (st.hasMoreTokens()) {
            final String val = st.nextToken();
            results.add(new PermissionBean(val));
        }
        return results;
    }

    public RoleBean getByCode(final String code) {
        RoleBean result = null;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("code", code);
            result = namedParameterJdbcTemplate.queryForObject("select * from ROLE T1 WHERE T1.CODE = :code", namedParameters, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", code, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect result size for code" + code);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with code %s from table %s", code, tableName),dae);
        }
        return result;
    }
    /**
     * Renvoie la liste de tous les roles Peut être affiché directement dans une Combo
     *
     * @return the liste id roles
     *
     */
    public List<RoleBean> getAllWithoutCollab() {
        final List<RoleBean> result;
        try {
            result = namedParameterJdbcTemplate.query("select * from ROLE T1 WHERE PERIMETRE NOT LIKE '%/*'", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with perimetre not like '%/*' from table %s", tableName),dae);
        }
        return result;
    }

    /**
     * Renvoie la liste de tous les roles Peut être affiché directement dans une Combo
     *
     * @return the liste id roles
     *
     */
    public List<RoleBean> getAll() {
        final List<RoleBean> result;
        try {
            result = namedParameterJdbcTemplate.query("select * from ROLE", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving all objects from table %s", tableName),dae);
        }
        return result;
    }

    public List<RoleBean> getByPermission(final String permission) {
        final List<RoleBean> result;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("permission", "%[" + permission + "]%");
            result = namedParameterJdbcTemplate.query("select * from ROLE WHERE PERMISSIONS LIKE :permission",namedParameters, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving objets for permission like [%s] from table %s", permission, tableName),dae);
        }
        return result;
    }

}
