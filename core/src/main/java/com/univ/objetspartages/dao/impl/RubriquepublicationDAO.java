package com.univ.objetspartages.dao.impl;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.UpdateToDataSourceException;
import com.univ.objetspartages.bean.RubriquepublicationBean;

/**
 * DAO se chargeant de l'accès en BDD des {@link RubriquepublicationBean}
 */
public class RubriquepublicationDAO extends AbstractLegacyDAO<RubriquepublicationBean> {

    public RubriquepublicationDAO() {
        tableName = "RUBRIQUEPUBLICATION";
    }

    public List<RubriquepublicationBean> getByTypeCodeLanguage(final String type, final String code, final String language) {
        final List<RubriquepublicationBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            params.addValue("code", code);
            params.addValue("language", language);
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUEPUBLICATION where TYPE_FICHE_ORIG = :type and CODE_FICHE_ORIG = :code and LANGUE_FICHE_ORIG = :language", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new UpdateToDataSourceException(String.format("Unable to query on type %s, code %s, language %s on table \"RUBRIQUEPUBLICATION\"", type, code, language), dae);
        }
        return results;
    }

    public List<RubriquepublicationBean> getByRubriqueDestAndSource(final String rubriqueDest, final String source) {
        final List<RubriquepublicationBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("rubriqueDest", rubriqueDest);
            params.addValue("source", source);
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUEPUBLICATION WHERE RUBRIQUE_DEST = :rubriqueDest and SOURCE_REQUETE = :source", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new UpdateToDataSourceException(String.format("Unable to query on rubriqueDest %s on table \"RUBRIQUEPUBLICATION\"", rubriqueDest), dae);
        }
        return results;
    }

    public List<RubriquepublicationBean> getByTypeRubriqueDestLangue(final String type, final String rubriqueDest, final String language) {
        final List<RubriquepublicationBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            params.addValue("rubriqueDest", rubriqueDest);
            params.addValue("language", language);
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUEPUBLICATION where TYPE_FICHE_ORIG = :type and RUBRIQUE_DEST = :rubriqueDest and LANGUE_FICHE_ORIG = :language", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new UpdateToDataSourceException(String.format("Unable to query on type %s, rubriqueDest %s, language %s on table \"RUBRIQUEPUBLICATION\"", type, rubriqueDest, language), dae);
        }
        return results;
    }

    public void deleteByRubriqueDest(final String rubriqueDest) {
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("rubriqueDest",rubriqueDest);
            namedParameterJdbcTemplate.update("DELETE from RUBRIQUEPUBLICATION WHERE RUBRIQUE_DEST = :rubriqueDest", params);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with rubriqueDest %s from table \"RUBRIQUEPUBLICATION\"", rubriqueDest), dae);
        }
    }

    public void deleteByRubriqueDestAndSource(final String rubriqueDest, final String source) {
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("rubriqueDest",rubriqueDest);
            params.addValue("source",source);
            namedParameterJdbcTemplate.update("DELETE from RUBRIQUEPUBLICATION WHERE RUBRIQUE_DEST = :rubriqueDest and SOURCE_REQUETE = :source", params);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with rubriqueDest %s from table \"RUBRIQUEPUBLICATION\"", rubriqueDest), dae);
        }
    }

    public List<RubriquepublicationBean> getByType(final String typeFiche) {
        final List<RubriquepublicationBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("typeFiche", typeFiche);
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUEPUBLICATION WHERE TYPE_FICHE_ORIG = :typeFiche ", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new UpdateToDataSourceException(String.format("Unable to query on typeFiche %s on table \"RUBRIQUEPUBLICATION\"", typeFiche), dae);
        }
        return results;
    }

    public void deleteByTypeCodeLanguageAndSource(final String type, final String code, final String language, final String source) {
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type",type);
            params.addValue("code", code);
            params.addValue("language",language);
            params.addValue("source",source);
            namedParameterJdbcTemplate.update("DELETE from RUBRIQUEPUBLICATION WHERE TYPE_FICHE_ORIG = :type and CODE_FICHE_ORIG = :code and LANGUE_FICHE_ORIG = :language and SOURCE_REQUETE = :source", params);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with type %s, code %s, language %s and source %s from table \"RUBRIQUEPUBLICATION\"", type, code, language, source), dae);
        }
    }

    public void deleteByTypeCodeLanguage(final String type, final String code, final String language) {
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type",type);
            params.addValue("code", code);
            params.addValue("language",language);
            namedParameterJdbcTemplate.update("DELETE from RUBRIQUEPUBLICATION WHERE TYPE_FICHE_ORIG = :type and CODE_FICHE_ORIG = :code and LANGUE_FICHE_ORIG = :language", params);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with type %s, code %s, language %s from table \"RUBRIQUEPUBLICATION\"", type, code, language), dae);
        }
    }

    public void deleteByIds(final Collection<Long> ids) throws DeleteFromDataSourceException {
        if(CollectionUtils.isNotEmpty(ids)) {
            try {
                final SqlParameterSource namedParameters = new MapSqlParameterSource("ids", ids);
                namedParameterJdbcTemplate.update("delete from RUBRIQUEPUBLICATION WHERE ID_RUBRIQUEPUBLICATION in (:ids)", namedParameters);
            } catch (final DataAccessException dae) {
                throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with id in (%s) from table \"%s\"", StringUtils.join(ids, ","), tableName), dae);
            }
        }
    }

}
