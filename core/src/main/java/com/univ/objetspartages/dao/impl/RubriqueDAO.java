package com.univ.objetspartages.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.RubriqueBean;

/**
 * Created by olivier.camon on 13/04/15.
 */
public class RubriqueDAO extends AbstractLegacyDAO<RubriqueBean> {

    private static final Logger LOG = LoggerFactory.getLogger(RubriqueDAO.class);

    public RubriqueDAO() {
        tableName = "RUBRIQUE";
    }

    public RubriqueBean selectByCode(final String codeRubrique) {
        RubriqueBean rubriqueBean = null;
        try {
            rubriqueBean = namedParameterJdbcTemplate.queryForObject("select * from RUBRIQUE WHERE CODE = :code", new MapSqlParameterSource("code", codeRubrique), rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", codeRubrique, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect result size for query with CODE = '%s'", codeRubrique));
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE field with %s value in table \"RUBRIQUE\"", codeRubrique), dae);
        }
        return rubriqueBean;
    }

    public List<RubriqueBean> selectByCodeParent(final String codeParent) {
        final List<RubriqueBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUE WHERE CODE_RUBRIQUE_MERE = :codeParent ORDER BY cast(ORDRE as signed)", new MapSqlParameterSource("codeParent", codeParent), rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_RUBRIQUE_MERE field with %s value in table \"RUBRIQUE\"", codeParent), dae);
        }
        return results;
    }

    public List<RubriqueBean> selectByLabel(final String label) {
        final List<RubriqueBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUE WHERE INTITULE like :intitule", new MapSqlParameterSource("intitule", "%" + label + "%"), rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on INTITULE field with %s value in table \"RUBRIQUE\"", label), dae);
        }
        return results;
    }

    public List<RubriqueBean> selectByBandeau(final long bandeauId) {
        final List<RubriqueBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUE WHERE ID_BANDEAU = :bandeauId", new MapSqlParameterSource("bandeauId", bandeauId), rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on ID_BANDEAU field with %s value in table \"RUBRIQUE\"", bandeauId), dae);
        }
        return results;
    }

    public List<RubriqueBean> selectAllRubriques() {
        final List<RubriqueBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUE", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to query on all Rubrique in table \"RUBRIQUE\"", dae);
        }
        return results;
    }

    public List<RubriqueBean> selectAllRubriquesWithRubPub() {
        final List<RubriqueBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUE WHERE REQUETES_RUBRIQUE_PUBLICATION != ''", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to query on all Rubrique in table \"RUBRIQUE\"", dae);
        }
        return results;
    }

    public List<RubriqueBean> selectByLanguage(final String language) {
        final List<RubriqueBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUE WHERE LANGUE = :langue", new MapSqlParameterSource("langue", language), rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on LANGUE field with %s value in table \"RUBRIQUE\"", language), dae);
        }
        return results;
    }

    public List<RubriqueBean> selectByCodes(final Collection<String> codes) {
        final List<RubriqueBean> results = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(codes)) {
            try {
                results.addAll(namedParameterJdbcTemplate.query("select * from RUBRIQUE WHERE CODE in (:codes)", new MapSqlParameterSource("codes", codes), rowMapper));
            } catch (final DataAccessException dae) {
                throw new DataSourceException(String.format("Unable to query on CODE field with %s value in table \"RUBRIQUE\"", StringUtils.join(codes, ", ")), dae);
            }
        }
        return results;
    }

    public List<RubriqueBean> selectByCategories(final Collection<String> categories) {
        final List<RubriqueBean> results = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(categories)) {
            try {
                results.addAll(namedParameterJdbcTemplate.query("select * from RUBRIQUE WHERE CATEGORIE in (:categories)", new MapSqlParameterSource("categories", categories), rowMapper));
            } catch (final DataAccessException dae) {
                throw new DataSourceException(String.format("Unable to query on CODE field with %s value in table \"RUBRIQUE\"", StringUtils.join(categories, ", ")), dae);
            }
        }
        return results;
    }

    public List<RubriqueBean> selectByReferences(final String homePage, final String idFicheReference) {
        final List<RubriqueBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("homePage", homePage);
            params.addValue("idFiche", "%" + idFicheReference + "%");
            results = namedParameterJdbcTemplate.query("select * from RUBRIQUE WHERE PAGE_ACCUEIL = :homePage OR ENCADRE LIKE :idFiche OR ACCROCHE like :idFiche", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on PAGE_ACCUEIL field with %s value and on field ENCADRE and ACCROCHE with %s values in table \"RUBRIQUE\"", homePage, idFicheReference), dae);
        }
        return results;
    }

    public void deleteByCodes(final Collection<String> codes) {
        if(CollectionUtils.isNotEmpty(codes)) {
            try {
                final MapSqlParameterSource params = new MapSqlParameterSource("codes", codes);
                namedParameterJdbcTemplate.update("DELETE from RUBRIQUE WHERE CODE in (:codes)", params);
            } catch (final DataAccessException dae) {
                throw new DataSourceException(String.format("Unable to delete RUBRIQUE with %s code in table \"RUBRIQUE\"", StringUtils.join(codes, ", ")), dae);
            }
        }
    }

    public int selectMaxOrder(final String code) {
        int results = 0;
        try {
            results = namedParameterJdbcTemplate.queryForObject("select max(T1.ORDRE) from RUBRIQUE T1 WHERE T1.CODE_RUBRIQUE_MERE = :code", new MapSqlParameterSource("code", code), new RowMapper<Integer>() {

                @Override
                public Integer mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                    return rs.getInt(1);
                }
            });
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", code, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect result size for query with CODE = '%s'", code));
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_RUBRIQUE_MERE field with %s value in table \"RUBRIQUE\"", code), dae);
        }
        return results;
    }

    public List<RubriqueBean> getRubriqueByCodeLanguageLabel(final String code, final String language, final String label) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        final StringBuilder request = new StringBuilder();
        if (StringUtils.isNotBlank(code)) {
            request.append(" CODE = :code ");
            params.addValue("code", code);
        }
        if (StringUtils.isNotBlank(language) && !"0000".equals(language)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" LANGUE = :langue ");
            params.addValue("langue", language);
        }
        if (StringUtils.isNotBlank(label)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" INTITULE = :codeParent ");
            params.addValue("codeParent", label);
        }
        if (request.length() > 0) {
            request.insert(0, "WHERE ");
        }
        request.insert(0, "SELECT * FROM RUBRIQUE ");
        final List<RubriqueBean> results;
        try {
            results = namedParameterJdbcTemplate.query(request.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }

    public List<RubriqueBean> getRubriqueByCodeLanguageLabelCategory(final String code, final String language, final String label, final String category) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        final StringBuilder request = new StringBuilder();
        if (StringUtils.isNotBlank(code)) {
            request.append(" CODE = :code ");
            params.addValue("code", code);
        }
        if (StringUtils.isNotBlank(language) && !"0000".equals(language)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" LANGUE = :langue ");
            params.addValue("langue", language);
        }
        if (StringUtils.isNotBlank(label)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" INTITULE = :codeParent ");
            params.addValue("codeParent", label);
        }
        if (StringUtils.isNotBlank(category) && !"0000".equals(category)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" CATEGORIE = :category ");
            params.addValue("category", category);
        }
        if (request.length() > 0) {
            request.insert(0, "WHERE ");
        }
        request.insert(0, "SELECT * FROM RUBRIQUE ");
        final List<RubriqueBean> results;
        try {
            results = namedParameterJdbcTemplate.query(request.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }

    public List<RubriqueBean> selectAllWithoutUrl() {
        final List<RubriqueBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * FROM RUBRIQUE WHERE CODE NOT IN (select SECTION_CODE from URL)", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }
}
