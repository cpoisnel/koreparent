package com.univ.objetspartages.dao.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.GroupeDsiBean;

/**
 * Created by olivier.camon on 16/04/15.
 */
public class GroupeDsiDAO extends AbstractLegacyDAO<GroupeDsiBean> {

    private static final Logger LOG = LoggerFactory.getLogger(GroupeDsiDAO.class);

    public GroupeDsiDAO() {
        tableName = "GROUPEDSI";
    }

    public GroupeDsiBean selectByCode(final String code) {
        GroupeDsiBean groupeDsiBean = null;
        try {
            groupeDsiBean = namedParameterJdbcTemplate.queryForObject("select * from GROUPEDSI WHERE CODE = :code", new MapSqlParameterSource("code", code), rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", code, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect result size for query with CODE = '%s'", code));
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE field with %s value in table \"GROUPEDSI\"", code), dae);
        }
        return groupeDsiBean;
    }

    public List<GroupeDsiBean> selectAll() {
        final List<GroupeDsiBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from GROUPEDSI", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to query on table \"GROUPEDSI\"", dae);
        }
        return results;
    }

    public List<GroupeDsiBean> selectAllDynamics() {
        final List<GroupeDsiBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from GROUPEDSI WHERE REQUETE_GROUPE != ''", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to query on table \"GROUPEDSI\"", dae);
        }
        return results;
    }

    public List<GroupeDsiBean> selectByLabel(final String label) {
        final List<GroupeDsiBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from GROUPEDSI WHERE LIBELLE like  :libelle", new MapSqlParameterSource("libelle", "%" + label + "%"), rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on LIBELLE field with %s value in table \"GROUPEDSI\"", label), dae);
        }
        return results;
    }

    public List<GroupeDsiBean> selectByLdapRequest(final String ldapRequest) {
        final List<GroupeDsiBean> results;
        final MapSqlParameterSource params = new MapSqlParameterSource();
        final StringBuilder query = new StringBuilder();
        final String[] valeursMultiples = StringUtils.split(ldapRequest, "+");
        int i = 0;
        for (final String valeurPossible : valeursMultiples) {
            if (query.length() > 0) {
                query.append(" OR ");
            }
            query.append("  REQUETE_LDAP = :ldapRequest").append(i).append(" ");
            params.addValue("ldapRequest" + i, valeurPossible);
            i++;
            query.append(" OR REQUETE_LDAP like :ldapRequest").append(i).append(" ");
            params.addValue("ldapRequest" + i, valeurPossible + ";%");
            i++;
            query.append(" OR REQUETE_LDAP like :ldapRequest").append(i).append(" ");
            params.addValue("ldapRequest" + i, "%;" + valeurPossible);
            i++;
            query.append(" OR REQUETE_LDAP like :ldapRequest").append(i).append(" ");
            params.addValue("ldapRequest" + i, "%;" + valeurPossible + ";%");
            i++;
        }
        query.insert(0, "select * from GROUPEDSI WHERE ");
        try {
            results = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on REQUETE_LDAP field with %s value in table \"GROUPEDSI\"", ldapRequest), dae);
        }
        return results;
    }

    public List<GroupeDsiBean> selectByGroupRequest(final String groupRequest) {
        final List<GroupeDsiBean> results;
        final MapSqlParameterSource params = new MapSqlParameterSource();
        final StringBuilder query = new StringBuilder();
        final String[] valeursMultiples = StringUtils.split(groupRequest, "+");
        int i = 0;
        for (final String valeurPossible : valeursMultiples) {
            if (query.length() > 0) {
                query.append(" OR ");
            }
            query.append("  REQUETE_GROUPE = :groupRequest").append(i).append(" ");
            params.addValue("groupRequest" + i, valeurPossible);
            i++;
            query.append(" OR REQUETE_GROUPE like :groupRequest").append(i).append(" ");
            params.addValue("groupRequest" + i, valeurPossible + ";%");
            i++;
            query.append(" OR REQUETE_GROUPE like :groupRequest").append(i).append(" ");
            params.addValue("groupRequest" + i, "%;" + valeurPossible);
            i++;
            query.append(" OR REQUETE_GROUPE like :groupRequest").append(i).append(" ");
            params.addValue("groupRequest" + i, "%;" + valeurPossible + ";%");
            i++;
        }
        query.insert(0, "select * from GROUPEDSI WHERE ");
        try {
            results = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on REQUETE_GROUPE field with %s value in table \"GROUPEDSI\"", groupRequest), dae);
        }
        return results;
    }

    public List<GroupeDsiBean> selectByImportSource(final String importSource) {
        final List<GroupeDsiBean> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from GROUPEDSI WHERE SOURCE_IMPORT = :importSource", new MapSqlParameterSource("importSource", importSource), rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on SOURCE_IMPORT field with %s value in table \"GROUPEDSI\"", importSource), dae);
        }
        return results;
    }

    /**
     * Requêter la table GROUPEDSI pour récupérer l'ensemble des groupes ayant CODE_GROUPE_PERE valorisé avec le code passé en paramétre.
     * @param parentGroup
     * @return Liste des groupes (cette liste peut être vide)
     * @throws DataAccessException Erreur lors des accés à la source de données SQL/
     */
    public List<GroupeDsiBean> selectByParentGroup(final String parentGroup) {
        final MapSqlParameterSource params = new MapSqlParameterSource("parentGroup", parentGroup);
        try {
            return namedParameterJdbcTemplate.query("select * from GROUPEDSI WHERE CODE_GROUPE_PERE = :parentGroup ORDER BY LIBELLE", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_GROUPE_PERE field with %s value in table \"GROUPEDSI\"", parentGroup), dae);
        }
    }

    public List<GroupeDsiBean> selectByImportSourceAndParentGroup(final String importSource, final String parentGroup) {
        final List<GroupeDsiBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("importSource", importSource);
            params.addValue("parentGroup", parentGroup);
            results = namedParameterJdbcTemplate.query("select * from GROUPEDSI WHERE SOURCE_IMPORT = :importSource AND CODE_GROUPE_PERE = :parentGroup ", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on SOURCE_IMPORT field with %s value in table \"GROUPEDSI\"", importSource), dae);
        }
        return results;
    }

    public List<GroupeDsiBean> selectByCodeAndImportSource(final String code, final String importSource) {
        final List<GroupeDsiBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("importSource", importSource);
            params.addValue("code", code);
            results = namedParameterJdbcTemplate.query("select * from GROUPEDSI WHERE CODE = :code AND SOURCE_IMPORT = :importSource ", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on SOURCE_IMPORT field with %s value in table \"GROUPEDSI\"", importSource), dae);
        }
        return results;
    }

    /**
     * Récupérer la liste des groupes rattachés à une structure.
     * @param codeStructure Le code de structure.
     * @return Une liste de bean GroupeDsiBean.
     * @throws DataAccessException Erreur lors des accés à la source de données SQL.
     */
    public List<GroupeDsiBean> getByStructure(final String codeStructure) {
        final MapSqlParameterSource params = new MapSqlParameterSource("codeStructure", codeStructure);
        try {
            return namedParameterJdbcTemplate.query("select * from GROUPEDSI WHERE CODE_STRUCTURE = :codeStructure ", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_STRUCTURE field with %s value in table \"GROUPEDSI\"", codeStructure), dae);
        }
    }

    /**
     * Récupérer la liste des groupes rattachés à une structure et d'une source d'import spécifique.
     * @param codeStructure Le code de structure.
     * @param importSource La source d'import.
     * @return Une liste de bean GroupeDsiBean.
     * @throws DataAccessException Erreur lors des accés à la source de données SQL.
     */
    public List<GroupeDsiBean> getByStructureAndImportSource(final String codeStructure, final String importSource) {
        final MapSqlParameterSource params = new MapSqlParameterSource("codeStructure", codeStructure);
        params.addValue("importSource", importSource);
        try {
            return namedParameterJdbcTemplate.query("select * from GROUPEDSI WHERE CODE_STRUCTURE = :codeStructure AND SOURCE_IMPORT = :importSource", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_STRUCTURE field with %s value and SOURCE_IMPORT field with %s value in table \"GROUPEDSI\"", codeStructure, importSource), dae);
        }
    }

    public List<GroupeDsiBean> getByCodeTypeLabelStructureAndCache(final String code, final String type, final String label, final String codeStructure, final String gestionCache) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        final StringBuilder request = new StringBuilder();
        if (StringUtils.isNotBlank(code)) {
            request.append(" CODE = :code ");
            params.addValue("code", code);
        }
        if (StringUtils.isNotBlank(type) && !"0000".equals(type)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" TYPE = :type ");
            params.addValue("type", type);
        }
        if (StringUtils.isNotBlank(label)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" LIBELLE = :label ");
            params.addValue("label", label);
        }
        if (StringUtils.isNotBlank(codeStructure)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" CODE_STRUCTURE = :codeStructure ");
            params.addValue("codeStructure", codeStructure);
        }
        if (StringUtils.isNotBlank(gestionCache)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" GESTION_CACHE = :gestionCache ");
            params.addValue("gestionCache", gestionCache);
        }
        if (request.length() > 0) {
            request.insert(0, "WHERE ");
        }
        request.insert(0, "SELECT * FROM GROUPEDSI ");
        final List<GroupeDsiBean> results;
        try {
            results = namedParameterJdbcTemplate.query(request.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }

    public List<GroupeDsiBean> getByCodeAndType(final String code, final String type) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        final StringBuilder request = new StringBuilder();
        if (StringUtils.isNotBlank(code)) {
            request.append(" CODE = :code ");
            params.addValue("code", code);
        }
        if (StringUtils.isNotBlank(type)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" TYPE = :type ");
            params.addValue("type", type);
        }
        if (request.length() > 0) {
            request.insert(0, "WHERE ");
        }
        request.insert(0, "SELECT * FROM GROUPEDSI ");
        final List<GroupeDsiBean> results;
        try {
            results = namedParameterJdbcTemplate.query(request.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }

    public List<GroupeDsiBean> getByCodeAndCache(final String code, final String gestionCache) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        final StringBuilder request = new StringBuilder();
        if (StringUtils.isNotBlank(code)) {
            request.append(" CODE = :code ");
            params.addValue("code", code);
        }
        if (StringUtils.isNotBlank(gestionCache)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" GESTION_CACHE = :gestionCache ");
            params.addValue("gestionCache", gestionCache);
        }
        if (request.length() > 0) {
            request.insert(0, "WHERE ");
        }
        request.insert(0, "SELECT * FROM GROUPEDSI ");
        final List<GroupeDsiBean> results;
        try {
            results = namedParameterJdbcTemplate.query(request.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }
}
