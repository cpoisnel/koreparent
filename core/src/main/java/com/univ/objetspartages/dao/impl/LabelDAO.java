package com.univ.objetspartages.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.LabelBean;

/**
 * Created on 20/04/15.
 */
public class LabelDAO extends AbstractLegacyDAO<LabelBean> {

    private static final Logger LOG = LoggerFactory.getLogger(LabelDAO.class);

    public LabelDAO() {
        tableName = "LIBELLE";
    }

    public LabelBean getByTypeCodeLanguage(final String type, final String code, final String language) {
        LabelBean result = null;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            params.addValue("code", code);
            params.addValue("langue", language);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM LIBELLE T1 WHERE T1.`TYPE` = :type AND T1.CODE = :code AND T1.LANGUE = :langue", params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", code, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for code " + code);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for LIBELLE row with type %s, code %s and language %s", type, code, language), dae);
        }
        return result;
    }

    public List<LabelBean> getByTypeCodesLanguage(final String type, final List<String> codes, final String language) {
        final List<LabelBean> result;
        try {
            final StringBuilder query = new StringBuilder("SELECT * FROM LIBELLE T1 WHERE T1.`TYPE` = :type AND T1.LANGUE = :langue");
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            params.addValue("langue", language);
            if(CollectionUtils.isNotEmpty(codes)) {
                params.addValue("codes", codes);
                query.append(" AND T1.CODE IN (:codes)");
            }
            result = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for LIBELLE row with type %s, code (%s) and language %s", type, StringUtils.join(codes, ","), language), dae);
        }
        return result;
    }

    public List<LabelBean> getByTypeLanguage(final String type, final String language) {
        final List<LabelBean> result;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            params.addValue("langue", language);
            result = namedParameterJdbcTemplate.query("SELECT * FROM LIBELLE T1 WHERE T1.`TYPE` = :type AND T1.LANGUE = :langue ORDER BY LIBELLE", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for LIBELLE row with type %s and language %s", type, language), dae);
        }
        return result;
    }

    public List<LabelBean> getByTypeLibelle(final String type, final String libelle) {
        final List<LabelBean> result;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            params.addValue("libelle", libelle);
            result = namedParameterJdbcTemplate.query("SELECT * FROM LIBELLE T1 WHERE T1.`TYPE` = :type AND T1.LIBELLE LIKE :libelle ORDER BY LIBELLE", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for LIBELLE row with type %s and label like %s", type, libelle), dae);
        }
        return result;
    }

    public List<LabelBean> getByTypeCode(final String type, final String code) {
        final List<LabelBean> result;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            params.addValue("code", code);
            result = namedParameterJdbcTemplate.query("SELECT * FROM LIBELLE T1 WHERE T1.`TYPE` = :type AND T1.CODE = :code", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for LIBELLE row with type %s and code %s", type, code), dae);
        }
        return result;
    }

    public List<LabelBean> getByCodeLanguage(final String code, final String language) {
        final List<LabelBean> result;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("code", code);
            params.addValue("langue", language);
            result = namedParameterJdbcTemplate.query("SELECT * FROM LIBELLE T1 WHERE T1.CODE = :code AND LANGUE = :langue", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for LIBELLE row with language %s and code %s", language, code), dae);
        }
        return result;
    }

    public LabelBean getByLibelleTypeLanguage(final String libelle, final String type, final String language) {
        LabelBean result = null;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            params.addValue("libelle", libelle);
            params.addValue("langue", language);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM LIBELLE T1 WHERE T1.`TYPE` = :type AND T1.LIBELLE = :libelle AND T1.LANGUE = :langue", params,
                rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for label %s, type %s and language %s on table %s", libelle, type, language, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size for libelle %s, type %s, language %s", libelle, type, language));
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for LIBELLE row with label %s, type %s and language %s", libelle, type, language), dae);
        }
        return result;
    }

    public List<LabelBean> getByType(final String type) {
        final List<LabelBean> result;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            result = namedParameterJdbcTemplate.query("SELECT * FROM LIBELLE T1 WHERE T1.`TYPE` = :type", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for LIBELLE row with type %s", type), dae);
        }
        return result;
    }

    public List<LabelBean> getAll() {
        final List<LabelBean> result;
        try {
            result = namedParameterJdbcTemplate.query("SELECT * FROM LIBELLE", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("An error occured requesting for all LIBELLE row in the database", dae);
        }
        return result;
    }
}
