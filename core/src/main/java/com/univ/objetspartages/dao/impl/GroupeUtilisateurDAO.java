package com.univ.objetspartages.dao.impl;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;

/**
 * Created by olivier.camon on 23/04/15.
 */
public class GroupeUtilisateurDAO extends AbstractLegacyDAO<GroupeUtilisateurBean> {

    private static final Logger LOG = LoggerFactory.getLogger(GroupeUtilisateurDAO.class);

    public GroupeUtilisateurDAO() {
        tableName = "GROUPEUTILISATEUR";
    }

    public void deleteByGroupCode(final String groupCode) {
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("groupCode", groupCode);
            namedParameterJdbcTemplate.update("DELETE FROM GROUPEUTILISATEUR WHERE CODE_GROUPE = :groupCode", namedParameters);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with group code %s from table \"%s\"", groupCode, tableName), dae);
        }
    }

    public void deleteByUserCode(final String userCode) {
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("userCode", userCode);
            namedParameterJdbcTemplate.update("DELETE FROM GROUPEUTILISATEUR WHERE CODE_UTILISATEUR = :userCode", namedParameters);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with user code %s from table \"%s\"", userCode, tableName), dae);
        }
    }

    public void deleteByUserCodeAndGroupCode(final String userCode, final String groupCode) {
        try {
            final MapSqlParameterSource namedParameters = new MapSqlParameterSource("userCode", userCode);
            namedParameters.addValue("groupCode", groupCode);
            namedParameterJdbcTemplate.update("DELETE FROM GROUPEUTILISATEUR WHERE CODE_UTILISATEUR = :userCode AND CODE_GROUPE = :groupCode", namedParameters);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(
                String.format("An error occured during deletion of row with user code %s and group code %s from table \"%s\"", userCode, groupCode, tableName), dae);
        }
    }

    public void deleteByGroupCodeAndImportSource(final String groupCode, final String importSource) {
        try {
            final MapSqlParameterSource namedParameters = new MapSqlParameterSource("importSource", importSource);
            namedParameters.addValue("groupCode", groupCode);
            namedParameterJdbcTemplate.update("DELETE FROM GROUPEUTILISATEUR WHERE SOURCE_IMPORT = :importSource AND CODE_GROUPE = :groupCode", namedParameters);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(
                String.format("An error occured during deletion of row with import source %s and group code %s from table \"%s\"", importSource, groupCode, tableName), dae);
        }
    }

    public void deleteByUserCodeAndImportSource(final String userCode, final String importSource) {
        try {
            final MapSqlParameterSource namedParameters = new MapSqlParameterSource("importSource", importSource);
            namedParameters.addValue("userCode", userCode);
            namedParameterJdbcTemplate.update("DELETE FROM GROUPEUTILISATEUR WHERE SOURCE_IMPORT = :importSource AND CODE_UTILISATEUR = :userCode", namedParameters);
        } catch (final DataAccessException dae) {
            throw new DeleteFromDataSourceException(
                String.format("An error occured during deletion of row with import source %s and group code %s from table \"%s\"", importSource, userCode, tableName), dae);
        }
    }

    public List<GroupeUtilisateurBean> selectByUserCode(final String userCode) {
        final List<GroupeUtilisateurBean> results;
        try {
            results = namedParameterJdbcTemplate.query("SELECT * FROM GROUPEUTILISATEUR WHERE CODE_UTILISATEUR = :userCode", new MapSqlParameterSource("userCode", userCode),
                rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_UTILISATEUR field with %s value in table \"GROUPEUTILISATEUR\"", userCode), dae);
        }
        return results;
    }

    public List<GroupeUtilisateurBean> selectByImportSource(final String importSource) {
        final List<GroupeUtilisateurBean> results;
        try {
            results = namedParameterJdbcTemplate.query("SELECT * FROM GROUPEUTILISATEUR WHERE SOURCE_IMPORT = :importSource",
                new MapSqlParameterSource("importSource", importSource), rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_UTILISATEUR field with %s value in table \"GROUPEUTILISATEUR\"", importSource), dae);
        }
        return results;
    }

    public List<GroupeUtilisateurBean> selectByUserCodeAndImportSource(final String userCode, final String importSource) {
        final List<GroupeUtilisateurBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("userCode", userCode);
            params.addValue("importSource", importSource);
            results = namedParameterJdbcTemplate.query("SELECT * FROM GROUPEUTILISATEUR WHERE CODE_UTILISATEUR = :userCode AND SOURCE_IMPORT = :importSource", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_UTILISATEUR field with %s value in table \"GROUPEUTILISATEUR\"", userCode), dae);
        }
        return results;
    }

    public List<GroupeUtilisateurBean> selectByGroupCodeAndImportSource(final String groupCode, final String importSource) {
        final List<GroupeUtilisateurBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("groupCode", groupCode);
            params.addValue("importSource", importSource);
            results = namedParameterJdbcTemplate.query("SELECT * FROM GROUPEUTILISATEUR WHERE CODE_GROUPE = :groupCode AND SOURCE_IMPORT = :importSource", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_GROUPE field with %s value in table \"GROUPEUTILISATEUR\"", groupCode), dae);
        }
        return results;
    }

    public List<GroupeUtilisateurBean> selectByUserAndGroup(final String userCode, final String groupCode) {
        final List<GroupeUtilisateurBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("groupCode", groupCode);
            params.addValue("userCode", userCode);
            results = namedParameterJdbcTemplate.query("SELECT * FROM GROUPEUTILISATEUR WHERE CODE_GROUPE = :groupCode AND CODE_UTILISATEUR = :userCode", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(
                String.format("Unable to query on CODE_GROUPE field with %s value AND on CODE_UTILISATEUR with %s value in table \"GROUPEUTILISATEUR\"", groupCode, userCode), dae);
        }
        return results;
    }

    public GroupeUtilisateurBean selectByUserGroupAndImportSource(final String userCode, final String groupCode, final String importSource) {
        GroupeUtilisateurBean results = null;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("groupCode", groupCode);
            params.addValue("userCode", userCode);
            params.addValue("importSource", importSource);
            results = namedParameterJdbcTemplate.queryForObject(
                "SELECT * FROM GROUPEUTILISATEUR WHERE CODE_GROUPE = :groupCode AND CODE_UTILISATEUR = :userCode AND SOURCE_IMPORT = :importSource", params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for user code %s, group code %s and import source %s on table %s", userCode, groupCode, importSource, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect result size for user code %s, group code %s and import source %s", userCode, groupCode, importSource));
        } catch (final DataAccessException dae) {
            throw new DataSourceException(
                String.format("Unable to query on CODE_GROUPE field with %s value AND on CODE_UTILISATEUR with %s value in table \"GROUPEUTILISATEUR\"", groupCode, userCode), dae);
        }
        return results;
    }

    public List<GroupeUtilisateurBean> selectByGroupCode(final String groupCode) {
        final List<GroupeUtilisateurBean> results;
        try {
            results = namedParameterJdbcTemplate.query("SELECT * FROM GROUPEUTILISATEUR WHERE CODE_GROUPE = :groupCode", new MapSqlParameterSource("groupCode", groupCode),
                rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_UTILISATEUR field with %s value in table \"GROUPEUTILISATEUR\"", groupCode), dae);
        }
        return results;
    }

    public List<GroupeUtilisateurBean> selectInGroupCode(final Collection<String> groupCode) {
        final List<GroupeUtilisateurBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource();
            final StringBuilder query = new StringBuilder("SELECT * FROM GROUPEUTILISATEUR");
            if(CollectionUtils.isNotEmpty(groupCode)) {
                params.addValue("groupCode", groupCode);
                query.append(" WHERE CODE_GROUPE IN (:groupCode)");
            }
            results = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_UTILISATEUR field with %s value in table \"GROUPEUTILISATEUR\"", groupCode), dae);
        }
        return results;
    }

    public List<GroupeUtilisateurBean> selectByUserNotInGroupCodeAndImportSource(final String userCode, final Collection<String> groupCode, final String importSource) {
        final List<GroupeUtilisateurBean> results;
        try {
            final StringBuilder query = new StringBuilder("SELECT * FROM GROUPEUTILISATEUR WHERE CODE_UTILISATEUR = :userCode AND SOURCE_IMPORT = :importSource");
            final MapSqlParameterSource params = new MapSqlParameterSource("userCode", userCode);
            params.addValue("importSource", importSource);
            if(CollectionUtils.isNotEmpty(groupCode)) {
                params.addValue("groupCode", groupCode);
                query.append(" AND CODE_GROUPE NOT IN (:groupCode)");
            }
            results = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_UTILISATEUR field with %s value in table \"GROUPEUTILISATEUR\"", groupCode), dae);
        }
        return results;
    }
}
