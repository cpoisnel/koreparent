package com.univ.objetspartages.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.om.ServiceBean;
import com.univ.utils.Chaine;

/**
 * Created by olivier.camon on 05/05/15.
 */
public class ServiceDAO  extends AbstractLegacyDAO<ServiceBean> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceDAO.class);

    public ServiceDAO() {
        tableName = "SERVICE";
        rowMapper = new RowMapper<ServiceBean>() {

            @Override
            public ServiceBean mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final ServiceBean serviceBean = new ServiceBean();
                serviceBean.setId(rs.getLong("ID_SERVICE"));
                serviceBean.setCode(rs.getString("CODE"));
                serviceBean.setIntitule(rs.getString("INTITULE"));
                serviceBean.setJetonKportal(rs.getString("JETON_KPORTAL"));
                serviceBean.setUrl(rs.getString("URL"));
                serviceBean.setUrlPopup(rs.getString("URL_POPUP"));
                serviceBean.setExpirationCache(rs.getInt("EXPIRATION_CACHE"));
                serviceBean.setVueReduiteUrl(rs.getString("VUE_REDUITE_URL"));
                serviceBean.setDiffusionMode(rs.getString("DIFFUSION_MODE"));
                serviceBean.setDiffusionPublicVise(Chaine.getVecteurAccolades(rs.getString("DIFFUSION_PUBLIC_VISE")));
                serviceBean.setDiffusionModeRestriction(rs.getString("DIFFUSION_MODE_RESTRICTION"));
                serviceBean.setDiffusionPublicViseRestriction(Chaine.getVecteurAccolades(rs.getString("DIFFUSION_PUBLIC_VISE_RESTRICTION")));
                serviceBean.setProxyCas(rs.getString("PROXY_CAS"));
                return serviceBean;
            }
        };
    }

    protected SqlParameterSource getParameters(final ServiceBean serviceBean) {
        final MapSqlParameterSource params = new MapSqlParameterSource("id", serviceBean.getId());
        params.addValue("code", serviceBean.getCode());
        params.addValue("intitule", serviceBean.getIntitule());
        params.addValue("jetonKportal", serviceBean.getJetonKportal());
        params.addValue("url", serviceBean.getUrl());
        params.addValue("urlPopup", serviceBean.getUrlPopup());
        params.addValue("expirationCache", serviceBean.getExpirationCache());
        params.addValue("vueReduiteUrl", serviceBean.getVueReduiteUrl());
        params.addValue("diffusionMode", serviceBean.getDiffusionMode());
        params.addValue("diffusionPublicVise", Chaine.convertirAccolades(serviceBean.getDiffusionPublicVise()));
        params.addValue("diffusionModeRestriction", serviceBean.getDiffusionModeRestriction());
        params.addValue("diffusionPublicViseRestriction", Chaine.convertirAccolades(serviceBean.getDiffusionPublicViseRestriction()));
        params.addValue("proxyCas", serviceBean.getProxyCas());
        return params;
    }

    public List<ServiceBean> selectAll() {
        final List<ServiceBean> result;
        try {
            result = namedParameterJdbcTemplate.query("select * from SERVICE", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving all objects from table %s", tableName), dae);
        }
        return result;
    }

    public ServiceBean selectByCode(final String code) {
        ServiceBean result = null;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("code", code);
            result = namedParameterJdbcTemplate.queryForObject("select * from SERVICE  WHERE CODE = :code", namedParameters, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", code, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect result size for code" + code);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with code %s from table %s", code, tableName), dae);
        }
        return result;
    }
}
