package com.univ.objetspartages.dao;

import java.sql.Types;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.utils.DaoUtils;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * Classe abstraite permettant de rajouter les méthodes qui sont présentes sur les fiches. À savoir selection par code de rubrique
 * et sélection par code langue état.
 * ATTENTION : les implémentations selectParCodeRubrique et selectCodeLangueEtat se base sur le champ "TITRE", si ce champ n'est pas présent sur vos fiches, il faut
 * réimplémenter ces méthodes
 * @param <T> le bean à retourner par les requêtes, il doit étendre {@link AbstractFicheBean}
 */
public abstract class AbstractFicheDAO<T extends AbstractFicheBean> extends AbstractLegacyDAO<T> implements FicheDAO<T> {

    protected String orderClause = "ORDER BY T1.TITRE ASC";

    /**
     * Cette méthode permet de persister le bean passé en paramètre en forçant la valeur de son id.
     * Ce mécanisme est surtout utilisé dans le cadre de la duplication de l'usine à site.
     * @param bean le bean à persister.
     * @return le bean persisté.
     */
    public T addWithForcedId(T bean) {
        try {
            namedParameterJdbcTemplate.update(DaoUtils.getAddWithForcedIdQuery(tableName, getColumns(), columnNamingStrategy), getParameters(bean));
        } catch (DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"%s\"", bean.toString(), tableName), dae);
        }
        return bean;
    }

    /**
     * Fourni une implémentation par défaut des anciennes méthode selectParCodeRubrique présentent sur les fiches
     * ATTENTION : les implémentations selectParCodeRubrique et selectCodeLangueEtat se base sur le champ "TITRE", si ce champ n'est pas présent sur vos fiches, il faut
     * réimplémenter ces méthodes.
     * @param codeRubrique Le code de la rubrique
     * @param langue La langue demandée
     * @return une liste d'{@link AbstractFicheBean} contenant les résultats, ou une liste vide si non trouvé.
     * @throws DataSourceException
     */
    @Override
    public List<T> selectParCodeRubrique(String codeRubrique, String langue) throws DataSourceException {
        initColumns();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        StringBuilder request = new StringBuilder(" WHERE T1.ETAT_OBJET = :etat ");
        parameterSource.addValue("etat", EtatFiche.EN_LIGNE.getEtat(), Types.VARCHAR);
        if (StringUtils.isNotBlank(codeRubrique)) {
            request.append("AND T1.CODE_RUBRIQUE = :codeRubrique ");
            parameterSource.addValue("codeRubrique", codeRubrique, Types.VARCHAR);
        }
        if (StringUtils.isNotBlank(langue)) {
            request.append("AND T1.LANGUE = :langue ");
            parameterSource.addValue("langue", langue, Types.VARCHAR);
        }
        request.append(" ").append(orderClause);
        String query = String.format(SELECT_DISTINCT, columnsNameForRequest, tableName) + request.toString();
        return select(query, parameterSource);
    }

    /**
     * Fourni une implémentation par défaut des anciennes méthodes selectCodeLangueEtat présentent sur les fiches
     * ATTENTION : les implémentations selectParCodeRubrique et selectCodeLangueEtat se base sur le champ "TITRE", si ce champ n'est pas présent sur vos fiches, il faut
     * réimplémenter ces méthodes.     * @param code Le code de la fiche
     * @param code le code de la fiche à récupérer
     * @param langue La langue de la fiche
     * @param etat L'état de la fiche
     * @return une liste d'{@link AbstractFicheBean} contenant les résultats, ou une liste vide si non trouvé.
     * @throws DataSourceException
     */
    @Override
    public List<T> selectCodeLangueEtat(String code, String langue, String etat) throws DataSourceException {
        initColumns();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        StringBuilder request = new StringBuilder();
        if (StringUtils.isNotBlank(code)) {
            request.append(" T1.CODE = :code ");
            parameterSource.addValue("code", code, Types.VARCHAR);
        }
        if (StringUtils.isNotBlank(langue)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" T1.LANGUE = :langue ");
            parameterSource.addValue("langue", langue, Types.VARCHAR);
        }
        if (StringUtils.isNotBlank(etat) && !ConditionHelper.CODE_DEFAUT.equals(etat)) {
            if (request.length() > 0) {
                request.append(" AND ");
            }
            request.append(" T1.ETAT_OBJET = :etat ");
            parameterSource.addValue("etat", etat, Types.VARCHAR);
        }
        if (request.length() > 0) {
            request.insert(0, " WHERE ");
        }
        request.append(" ").append(orderClause);
        String query = String.format(SELECT_DISTINCT, columnsNameForRequest, tableName) + request.toString();
        return select(query, parameterSource);
    }
}
