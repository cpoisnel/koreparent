package com.univ.objetspartages.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.jsbsoft.jtf.datasource.utils.DaoUtils;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * Created on 20/04/15.
 */
public class EncadreDAO extends AbstractLegacyDAO<EncadreBean> {

    private static final Logger LOG = LoggerFactory.getLogger(EncadreDAO.class);

    private ServiceRubrique serviceRubrique;

    private ServiceStructure serviceStructure;

    public EncadreDAO() {
        tableName = "ENCADRE";
    }

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void setServiceStructure(final ServiceStructure serviceStructure) {
        this.serviceStructure = serviceStructure;
    }

    public EncadreBean getByCode(final String code) {
        EncadreBean result = null;
        try {
            final SqlParameterSource params = new MapSqlParameterSource("code", code);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM ENCADRE T1 WHERE T1.CODE = :code", params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", code, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for code {}", code);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on ENCADRE table with CODE set as %s", code), dae);
        }
        return result;
    }

    public List<EncadreBean> getByCodesRubrique(final List<String> codes) {
        final List<EncadreBean> result;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource();
            final StringBuilder query = new StringBuilder("SELECT * FROM ENCADRE T1");
            if(CollectionUtils.isNotEmpty(codes)) {
                params.addValue("codesRubrique", codes);
                query.append(" WHERE T1.CODE_RUBRIQUE IN (:codesRubrique)");
            }
            result = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting on ENCADRE table with codeRubrique set as (%s)", StringUtils.join(codes, ",")), dae);
        }
        return result;
    }

    public List<EncadreBean> getAllEncadres() {
        final List<EncadreBean> result;
        try {
            result = namedParameterJdbcTemplate.query("SELECT * FROM ENCADRE ORDER BY INTITULE", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("An error occured requesting on all ENCADRE in the database", dae);
        }
        return result;
    }

    public List<EncadreBean> getEncadreObjectList(final String codeRubrique, final String codeStructure, final String codeObjet, final String langue) {
        return select(construireRequeteObjetsEncadres(langue, codeRubrique, codeStructure, codeObjet).formaterRequete());
    }

    public void deleteByCodesRubriques(final List<String> codesRubriques) throws DeleteFromDataSourceException {
        if(CollectionUtils.isNotEmpty(codesRubriques)) {
            try {
                final SqlParameterSource namedParameters = new MapSqlParameterSource("codeRubrique", codesRubriques);
                namedParameterJdbcTemplate.update("DELETE FROM ENCADRE WHERE CODE_RUBRIQUE IN (:codeRubrique)", namedParameters);
            } catch (final DataAccessException dae) {
                throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with codeRubrique in (%s) from table \"%s\"", StringUtils.join(codesRubriques, ","), tableName), dae);
            }
        }
    }

    /**
     * Construit la requete pour retourner les objets encadré lié à la rubrique ou la structure ou l'objet dans la langue courante.
     *
     * @param langue
     * @param codeRubrique
     * @param codeStructure
     * @param codeObjet
     * @return
     */
    private RequeteSQL construireRequeteObjetsEncadres(String langue, final String codeRubrique, final String codeStructure, final String codeObjet) {
        final RequeteSQL requete = new RequeteSQL();
        final ClauseWhere where = new ClauseWhere();
        final ClauseOrderBy orderBy = new ClauseOrderBy("POIDS", ClauseOrderBy.SensDeTri.ASC);
        where.setPremiereCondition(ConditionHelper.egalVarchar("ACTIF", "1"));
        if (StringUtils.isEmpty(langue)) {
            langue = "0";
        }
        where.and(ConditionHelper.egalVarchar("LANGUE", langue));
        where.and(conditionRubriqueStructureEtObjet(codeRubrique, codeStructure, codeObjet));
        requete.where(where).orderBy(orderBy);
        return requete;
    }

    private ConditionList conditionRubriqueStructureEtObjet(String codeRubrique, final String codeStructure, final String codeObjet) {
        final ConditionList listeConditionRubStructEtObjet = new ConditionList(ConditionHelper.egalVarchar("OBJETS", ""));
        if (StringUtils.isNotEmpty(codeRubrique)) {
            listeConditionRubStructEtObjet.or(ConditionHelper.likePourValeursMultiple("OBJETS", "RUB_" + codeRubrique));
            final RubriqueBean currentSection = serviceRubrique.getRubriqueByCode(codeRubrique);
            int niveau = serviceRubrique.getLevel(currentSection);
            while (niveau > 1) {
                codeRubrique = currentSection.getCodeRubriqueMere();
                listeConditionRubStructEtObjet.or(ConditionHelper.likePourValeursMultiple("OBJETS", "RUB_" + codeRubrique));
                niveau--;
            }
        }
        listeConditionRubStructEtObjet.or(conditionStructure(codeStructure, codeObjet));
        listeConditionRubStructEtObjet.or(ConditionHelper.likePourValeursMultiple("OBJETS", codeObjet + "/"));
        return listeConditionRubStructEtObjet;
    }

    private ConditionList conditionStructure(String codeStructure, final String codeObjet) {
        final ConditionList conditionStructure = new ConditionList();
        if (StringUtils.isNotEmpty(codeStructure)) {
            conditionStructure.setPremiereCondtion(ConditionHelper.likePourValeursMultiple("OBJETS", "/" + codeStructure));
            conditionStructure.or(ConditionHelper.likePourValeursMultiple("OBJETS", codeObjet + "/" + codeStructure));
            final StructureModele structure = serviceStructure.getByCodeLanguage(codeStructure, LangueUtil.getIndiceLocaleDefaut());
            int niveau = serviceStructure.getLevel(structure);
            while (niveau > 1) {
                codeStructure = structure.getCodeRattachement();
                conditionStructure.or(ConditionHelper.likePourValeursMultiple("OBJETS", "/" + codeStructure));
                conditionStructure.or(ConditionHelper.likePourValeursMultiple("OBJETS", codeObjet + "/" + codeStructure));
                niveau--;
            }
        }
        return conditionStructure;
    }

    public EncadreBean addWithForcedId(EncadreBean bean) {
        try {
            namedParameterJdbcTemplate.update(DaoUtils.getAddWithForcedIdQuery(tableName, getColumns(), columnNamingStrategy), getParameters(bean));
        } catch (DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"%s\"", bean.toString(), tableName), dae);
        }
        return bean;
    }

}
