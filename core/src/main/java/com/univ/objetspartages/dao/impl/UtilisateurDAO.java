package com.univ.objetspartages.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.utils.RequeteGroupeUtil;
import com.univ.utils.sql.ConstanteSQL;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseJoin;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ClauseJoinHelper;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * Created on 10/04/15.
 */
public class UtilisateurDAO extends AbstractLegacyDAO<UtilisateurBean> {

    private static final Logger LOG = LoggerFactory.getLogger(UtilisateurDAO.class);

    // FIXME : le dao ne devrait pas porter ce service. Pour le moment, c'est inévitable, essayer de revoir ça dans le chantier DSI.
    private ServiceProfildsi serviceProfildsi;

    private ServiceGroupeDsi serviceGroupeDsi;

    public UtilisateurDAO() {
        this.tableName = "UTILISATEUR";
    }

    public void setServiceProfildsi(final ServiceProfildsi serviceProfildsi) {
        this.serviceProfildsi = serviceProfildsi;
    }

    public void setServiceGroupeDsi(final ServiceGroupeDsi serviceGroupeDsi) {
        this.serviceGroupeDsi = serviceGroupeDsi;
    }

    private static Collection<String> despecialiseChaineDiffusion(final String chaineDiffusion) {
        final Collection<String> listeChaineDiffusion = new ArrayList<>();
        if (StringUtils.isNotEmpty(chaineDiffusion)) {
            final String[] codesChaines = chaineDiffusion.split(";");
            for (String codeChaine : codesChaines) {
                // On enleve les crochets
                codeChaine = codeChaine.substring(1, codeChaine.length() - 1);
                //Extraction groupe
                final int iSlash = codeChaine.indexOf("/");
                final String groupe = codeChaine.substring(iSlash + 1);
                listeChaineDiffusion.add(groupe);
            }
        }
        return listeChaineDiffusion;
    }

    public UtilisateurBean getByCode(final String code) {
        UtilisateurBean result = null;
        try {
            final SqlParameterSource params = new MapSqlParameterSource("code", code);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM UTILISATEUR WHERE CODE = :code", params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", code, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for code {}", code);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE field with %s value in table \"UTILISATEUR\"", code), dae);
        }
        return result;
    }

    public UtilisateurBean getByCodeLdap(final String codeLdap) {
        UtilisateurBean result = null;
        try {
            final SqlParameterSource params = new MapSqlParameterSource("codeLdap", codeLdap);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM UTILISATEUR WHERE CODE_LDAP = :codeLdap", params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", codeLdap, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for code ldap {}", codeLdap);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on CODE_LDAP field with %s value in table \"UTILISATEUR\"", codeLdap), dae);
        }
        return result;
    }

    public List<UtilisateurBean> getByNom(final String nom) {
        final List<UtilisateurBean> result;
        try {
            final SqlParameterSource params = new MapSqlParameterSource("nom", "%" + nom + "%");
            result = namedParameterJdbcTemplate.query("SELECT * FROM UTILISATEUR WHERE NOM LIKE :nom", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on NOM field with %s value in table \"UTILISATEUR\"", nom), dae);
        }
        return result;
    }

    public UtilisateurBean getUser(final String email, final String pass, final String code) {
        UtilisateurBean result = null;
        final StringBuilder query = new StringBuilder("SELECT * FROM UTILISATEUR WHERE ADRESSE_MAIL = :adresseMail AND MOT_DE_PASSE = :motDePasse");
        if (StringUtils.isNotBlank(code)) {
            query.append(" AND CODE = :code");
        }
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("adresseMail", email);
            params.addValue("motDePasse", pass);
            params.addValue("code", code);
            result = namedParameterJdbcTemplate.queryForObject(query.append(";").toString(), params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for email %s, pass %s and code %s on table %s", email, pass, code, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size for email %s, pass %s, code %s", email, pass, code));
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on email %s, pass %s and code %s value in table \"UTILISATEUR\"", email, pass, code), dae);
        }
        return result;
    }

    public List<UtilisateurBean> getByMailAndCode(final String email, final String code) {
        final List<UtilisateurBean> result = new ArrayList<>();
        final StringBuilder query = new StringBuilder("SELECT * FROM UTILISATEUR WHERE ADRESSE_MAIL = :adresseMail");
        if (StringUtils.isNotBlank(code)) {
            query.append(" AND CODE = :code");
        }
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("adresseMail", email);
            params.addValue("code", code);
            result.addAll(namedParameterJdbcTemplate.query(query.append(";").toString(), params, rowMapper));
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on email %s and code %s value in table \"UTILISATEUR\"", email, code), dae);
        }
        return result;
    }

    public UtilisateurBean getByCodeLdap(final String codeLdap, final String source) {
        final StringBuilder query = new StringBuilder("SELECT * FROM UTILISATEUR WHERE CODE_LDAP = :codeLdap");
        if (StringUtils.isBlank(source)) {
            query.append(" AND SOURCE_IMPORT != :source");
        } else {
            query.append(" AND SOURCE_IMPORT = :source");
        }
        UtilisateurBean result = null;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("codeLdap", codeLdap);
            params.addValue("source", source);
            result = namedParameterJdbcTemplate.queryForObject(query.append(";").toString(), params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s and source %s on table %s", codeLdap, source, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for codeLdap {}, source {}", codeLdap, source);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on codeLdap %s and source %s value in table \"UTILISATEUR\"", codeLdap, source), dae);
        }
        return result;
    }

    public UtilisateurBean getByCodeAndPass(final String codeUtilisateur, final String pass) {
        UtilisateurBean result = null;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("code", codeUtilisateur);
            params.addValue("mdp", pass);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM UTILISATEUR WHERE CODE = :code AND MOT_DE_PASSE = :mdp", params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s and pass %s on table %s", codeUtilisateur, pass, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for code {} and pass {}", codeUtilisateur, pass);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on code %s and pass %s value in table \"UTILISATEUR\"", codeUtilisateur, pass), dae);
        }
        return result;
    }

    public List<UtilisateurBean> getByCodes(final Collection<String> codes) {
        final List<UtilisateurBean> result = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(codes)) {
            try {
                final MapSqlParameterSource params = new MapSqlParameterSource("codes", codes);
                result.addAll(namedParameterJdbcTemplate.query("SELECT * FROM UTILISATEUR WHERE CODE in (:codes)", params, rowMapper));
            } catch (final DataAccessException dae) {
                throw new DataSourceException(String.format("Unable to query on codes (%s) value in table \"UTILISATEUR\"", StringUtils.join(codes, "','")), dae);
            }
        }
        return result;
    }

    public List<UtilisateurBean> getAllUsers() {
        final List<UtilisateurBean> result;
        try {
            result = namedParameterJdbcTemplate.query("SELECT * FROM UTILISATEUR T1", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to fetch all the user values on table \"UTILISATEUR\"", dae);
        }
        return result;
    }

    public List<UtilisateurBean> getByImportSource(final String sourceImport) {
        final List<UtilisateurBean> result;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("sourceImport", sourceImport);
            result = namedParameterJdbcTemplate.query("SELECT * FROM UTILISATEUR WHERE SOURCE_IMPORT = :sourceImport", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on sourceImport %s value in table \"UTILISATEUR\"", sourceImport), dae);
        }
        return result;
    }

    //-----------------------------------------------------------------------------//
    // FIXME : ces méthodes ne devrait plus exister. Pour le moment, trop coûteux de les supprimer. On utilise donc l'ancienne méthode avec le select legacy.
    public List<UtilisateurBean> select(final String code, final String nom, final String prenom, final String chaineDiffusion, final String profil, final String groupe,
        final String structure, final String adresseMail) {
        try {
            return select(construireRequete(code, nom, prenom, chaineDiffusion, profil, groupe, structure, adresseMail, true));
        } catch (final Exception e) {
            LOG.error("Une erreur est survenue lors de l'exécution du select.", e);
            return Collections.emptyList();
        }
    }

    /**
     * Requete de selection des membres d'un espace collaboratif.
     *
     * @param code the _code
     * @param nom the _nom
     * @param prenom the _prenom
     * @param chaineDiffusion the _chaine diffusion
     * @param profil the _profil
     * @param groupe the _groupe
     * @param structure the _structure
     * @param rechercheDansSousGroupes the _recherche dans sous groupes
     * @param adresseMail the _adresse mail
     * @return the string
     * @throws Exception the exception
     */
    private String construireRequete(final String code, final String nom, final String prenom, final String chaineDiffusion, final String profil, final String groupe,
        final String structure, final String adresseMail, final boolean rechercheDansSousGroupes) throws Exception {
        final RequeteSQL requeteSelect = new RequeteSQL();
        ClauseWhere where = new ClauseWhere();
        final ClauseOrderBy orderBy = new ClauseOrderBy();
        orderBy.orderBy("T1.NOM", ClauseOrderBy.SensDeTri.ASC).orderBy("T1.PRENOM", ClauseOrderBy.SensDeTri.ASC);
        if (StringUtils.isNotEmpty(code)) {
            where.setPremiereCondition(ConditionHelper.egalVarchar("T1.CODE", code));
        }
        if (StringUtils.isNotEmpty(nom)) {
            where.and(ConditionHelper.like("T1.NOM", nom, "%", "%"));
        }
        if (StringUtils.isNotEmpty(prenom)) {
            where.and(ConditionHelper.like("T1.PRENOM", prenom, "%", "%"));
        }
        if (StringUtils.isNotEmpty(adresseMail)) {
            where.and(ConditionHelper.like("T1.ADRESSE_MAIL", adresseMail, "%", "%"));
        }
        if (StringUtils.isNotEmpty(chaineDiffusion)) {
            final Collection<String> codeDespecialise = despecialiseChaineDiffusion(chaineDiffusion);
            where.and(getCondtionCodeOuCodeGroupe(codeDespecialise));
        }
        if (StringUtils.isNotEmpty(structure)) {
            where.and(ConditionHelper.getConditionStructure("T1.CODE_RATTACHEMENT", structure));
        }
        if (StringUtils.isNotEmpty(profil) && !"0000".equals(profil)) {
            final ProfildsiBean profile = serviceProfildsi.getByCode(profil);
            if(profile!= null) {
                final Collection<String> groupesProfilDsi = profile.getGroupes();
                if (groupesProfilDsi.isEmpty()) {
                    where = new ClauseWhere(ConstanteSQL.CONDITION_IMPOSSIBLE);
                } else {
                    where.and(getCondtionCodeOuCodeGroupe(groupesProfilDsi));
                }
            } else {
                where = new ClauseWhere(ConstanteSQL.CONDITION_IMPOSSIBLE);
            }
        }
        if (StringUtils.isNotEmpty(groupe) && !"0000".equals(groupe)) {
            if (rechercheDansSousGroupes) {
                final Collection<String> codeDesGroupes = Arrays.asList(groupe.split(";"));
                where.and(getCondtionCodeOuCodeGroupe(codeDesGroupes));
            } else {
                where.and(ConditionHelper.egalVarchar("T2.CODE_GROUPE", groupe));
            }
        }
        final ClauseJoin join = traiterJointureGroupeUtilisateur(where);
        requeteSelect.where(where).join(join).orderBy(orderBy);
        return requeteSelect.formaterRequete();
    }

    /**
     * Neccessaire du au mécanisme de construction de la requête, on ne peut pas savoir autrement à l'heure actuelle si il y a une jointure sur la table GROUPEUTILISATEUR ou non.
     *
     * @param where
     * @return
     */
    private ClauseJoin traiterJointureGroupeUtilisateur(final ClauseWhere where) {
        ClauseJoin join = new ClauseJoin();
        final String requeteFormater = where.formaterSQL();
        if (StringUtils.isNotEmpty(requeteFormater) && requeteFormater.contains("T2.")) {
            join = ClauseJoinHelper.creerJointure(ClauseJoin.TypeJointure.LEFT_JOIN, "GROUPEUTILISATEUR T2",
                ConditionHelper.critereJointureSimple("T1.CODE", "T2.CODE_UTILISATEUR"));
        }
        return join;
    }

    /**
     * CQCB?
     *
     * @param codeDesGroupes
     * @return
     * @throws Exception
     */
    private Condition getCondtionCodeOuCodeGroupe(final Collection<String> codeDesGroupes) throws Exception {
        final ConditionList conditioncodeOuCodeGroupe = new ConditionList();
        if (!codeDesGroupes.isEmpty()) {
            // Le treeset permet d'optimiser la gestion des doublons
            final Set<String> usersDynamiques = new TreeSet<>();
            final Set<String> groupes = new TreeSet<>();
            for (final String groupeDiffusion : codeDesGroupes) {
                if (StringUtils.isNotEmpty(groupeDiffusion)) {
                    determinerUsersEtGroupes(groupeDiffusion, usersDynamiques, groupes);
                }
            }
            if (!groupes.isEmpty()) {
                conditioncodeOuCodeGroupe.or(ConditionHelper.in("T2.CODE_GROUPE", groupes));
            }
            if (!usersDynamiques.isEmpty()) {
                conditioncodeOuCodeGroupe.or(ConditionHelper.in("T1.CODE", usersDynamiques));
            }
        }
        if (conditioncodeOuCodeGroupe.isEmpty()) {
            conditioncodeOuCodeGroupe.setPremiereCondtion(ConstanteSQL.CONDITION_IMPOSSIBLE);
        }
        return conditioncodeOuCodeGroupe;
    }

    /**
     * Détermine pour un groupe
     * <br>
     * - la liste des groupes à inclure dans la requête <br>
     * - la liste des users issus des groupes dynamiques.
     *
     * @param codeGroupe the code groupe
     * @param listeUsers the liste users
     * @param listeGroupes the liste groupes
     * @throws Exception the exception
     */
    private void determinerUsersEtGroupes(final String codeGroupe, final Set<String> listeUsers, final Set<String> listeGroupes) throws Exception {
        final GroupeDsiBean infoGroupe = serviceGroupeDsi.getByCode(codeGroupe);
        if (StringUtils.isBlank(infoGroupe.getRequeteGroupe())) {
            // Cas d'un groupe statique
            listeGroupes.add(codeGroupe);
            // Calcul des sous-groupes
            final List<GroupeDsiBean> subGroups = serviceGroupeDsi.getByParentGroup(codeGroupe);
            for(final GroupeDsiBean currentGroup : subGroups) {
                determinerUsersEtGroupes(currentGroup.getCode(), listeUsers, listeGroupes);
            }
        } else {
            // cas groupe dynamique
            final Set<String> setCodesGroupesCache = new HashSet<>();
            final Collection<String> listeUsersGroupe = RequeteGroupeUtil.getVecteurUtilisateurs(codeGroupe, setCodesGroupesCache);
            if (setCodesGroupesCache.isEmpty()) {
                listeUsers.addAll(listeUsersGroupe);
            } else {
                for (final String string : setCodesGroupesCache) {
                    listeGroupes.add(string);
                }
            }
        }
    }

    public List<UtilisateurBean> getByChaineDiffusion(final String chaineDiffusion) {
        List<UtilisateurBean> results = new ArrayList<>();
        try {
            final ClauseWhere where = new ClauseWhere();
            if (StringUtils.isNotEmpty(chaineDiffusion)) {
                final Collection<String> codeDespecialise = despecialiseChaineDiffusion(chaineDiffusion);
                where.setPremiereCondition(getCondtionCodeOuCodeGroupe(codeDespecialise));
            }
            final ClauseJoin join = traiterJointureGroupeUtilisateur(where);
            final RequeteSQL requeteSelect = new RequeteSQL();
            requeteSelect.join(join).where(where);
            results = select(requeteSelect.formaterRequete());
        } catch (final Exception e) {
            LOG.error("Une erreur est survenue lors de la récupération des utilisateurs correspondant à la chaine de diffusion " + chaineDiffusion, e);
        }
        return results;
    }
}

