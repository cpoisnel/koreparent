package com.univ.objetspartages.dao.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.RessourceBean;

/**
 * Created on 20/04/15.
 */
public class RessourceDAO extends AbstractLegacyDAO<RessourceBean> {

    private static final Logger LOG = LoggerFactory.getLogger(RessourceDAO.class);

    public RessourceDAO() {
        tableName = "RESSOURCE";
    }

    public List<RessourceBean> getByCodeParent(final String code, final String orderBy) {
        final List<RessourceBean> result;
        try {
            final StringBuilder query = new StringBuilder("SELECT * FROM RESSOURCE WHERE CODE_PARENT LIKE :codeParent");
            if (StringUtils.isNotBlank(orderBy)) {
                query.append(String.format(" ORDER BY %s ASC", orderBy.toUpperCase()));
            }
            final MapSqlParameterSource params = new MapSqlParameterSource("codeParent", code);
            result = namedParameterJdbcTemplate.query(query.toString(), params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Une erreur est survenue lors de la récupération des ressources portant le code parent \"%s\"", code), dae);
        }
        return result;
    }

    public List<RessourceBean> getRessourceToPurge() {
        final List<RessourceBean> result;
        try {
            result = namedParameterJdbcTemplate.query("SELECT * FROM RESSOURCE WHERE ETAT='0' OR ETAT='2' OR CODE_PARENT=''", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Une erreur est survenue lors de la récupération des ressources élligibles à la purge", dae);
        }
        return result;
    }

    public List<RessourceBean> getByMediaId(final Long id) {
        final List<RessourceBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("idMedia", id);
            results = namedParameterJdbcTemplate.query("SELECT * FROM RESSOURCE WHERE ID_MEDIA = :idMedia ORDER BY ID_RESSOURCE DESC", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Une erreur est survenue lors la récupération des ressources portant l'id media \"%d\"", id), dae);
        }
        return results;
    }

    public RessourceBean getByMediaIdAndCodeParent(final Long id, final String codeParent) {
        RessourceBean result = null;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("idMedia", id);
            params.addValue("codeParent", codeParent);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM RESSOURCE WHERE ID_MEDIA = :idMedia AND CODE_PARENT = :codeParent", params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for idMedia %d and codeParent %s on table %s", id, codeParent, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for idMedia {} and codeParent {} ", id, codeParent);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Une erreur est survenue lors de la récupération de la ressource portant l'id média \"%d\" et le code parent \"%s\"", id, codeParent), dae);
        }
        return result;
    }

    public RessourceBean addWithForcedId(final RessourceBean ressourceBean) throws AddToDataSourceException {
        try {
            namedParameterJdbcTemplate.update("insert into RESSOURCE (ID_RESSOURCE, ID_MEDIA, CODE_PARENT, ETAT, ORDRE) values (:id, :idMedia, :codeParent, :etat, :ordre)", getParameters(ressourceBean));
        } catch (final DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"RESSOURCE\"", ressourceBean.toString()), dae);
        }
        return ressourceBean;
    }

}
