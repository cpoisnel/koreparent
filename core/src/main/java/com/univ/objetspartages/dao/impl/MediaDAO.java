package com.univ.objetspartages.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.utils.sql.clause.ClauseWhere;

/**
 * Created on 20/04/15.
 */
public class MediaDAO extends AbstractLegacyDAO<MediaBean> {

    private static final Logger LOG = LoggerFactory.getLogger(MediaDAO.class);

    public MediaDAO() {
        tableName = "MEDIA";
    }

    public List<MediaBean> getAll() {
        final List<MediaBean> results;
        try {
            results = namedParameterJdbcTemplate.query("SELECT * FROM MEDIA", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Une erreur est survenue lors de la récupération de l'intégralité des médias.", dae);
        }
        return results;
    }

    public List<MediaBean> getByFilledCodeRubrique() {
        final List<MediaBean> results;
        try {
            results = namedParameterJdbcTemplate.query("SELECT * FROM MEDIA WHERE CODE_RUBRIQUE != ''", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Une erreur est survenue lors de la récupération des médias dont le code rubrique est renseigné", dae);
        }
        return results;
    }

    public List<MediaBean> getByResourceType(final String type) {
        final List<MediaBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            results = namedParameterJdbcTemplate.query("SELECT * FROM MEDIA WHERE TYPE_RESSOURCE = :type", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Une erreur est survenue lors de la récupération des médias de type \"%s\"", type), dae);
        }
        return results;
    }

    public MediaBean getByUrl(final String url) {
        MediaBean result = null;
        try {
            final SqlParameterSource params = new MapSqlParameterSource("url", url);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM MEDIA WHERE URL = :url", params, rowMapper);
        } catch (final EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for url %s on table %s", url, tableName), e);
        } catch (final IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for url " + url);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Une erreur est survenue lors de la récupération du média ayant pour url \"%s\"", url), dae);
        }
        return result;
    }

    public List<MediaBean> getByFilledUrlVignette() {
        final List<MediaBean> results;
        try {
            results = namedParameterJdbcTemplate.query("SELECT * FROM MEDIA WHERE TYPE_RESSOURCE != ''", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Une erreur est survenue lors de la récupération des médias dont la vignette est renseignée", dae);
        }
        return results;
    }

    public List<MediaBean> getByTypeAndTypeMedia(final String type, final String typeMedia) {
        final List<MediaBean> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("type", type);
            params.addValue("typeMedia", typeMedia);
            results = namedParameterJdbcTemplate.query("SELECT * FROM MEDIA WHERE TYPE_RESSOURCE = :type AND TYPE_MEDIA = :typeMedia", params, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException(String.format("Une erreur est survenue lors de la récupération des médias dont le type est \"%s\" et le type média est \"%s\"", type, typeMedia), dae);
        }
        return results;
    }

    public int getCountFromWhere(final ClauseWhere where) {
        final int count;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource();
            count = namedParameterJdbcTemplate.queryForObject(String.format("SELECT COUNT(*) FROM MEDIA T1 %s", where.formaterSQL()), params, Integer.class);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Une erreur est survenue lors de la récupération du nombre de résultats", dae);
        }
        return count;
    }

    public MediaBean addWithForcedId(final MediaBean mediaBean) throws AddToDataSourceException {
        try {
            namedParameterJdbcTemplate.update("insert into MEDIA (ID_MEDIA, TITRE, LEGENDE, DESCRIPTION, AUTEUR, COPYRIGHT, TYPE_RESSOURCE, TYPE_MEDIA, SOURCE, FORMAT, CODE_RUBRIQUE, CODE_RATTACHEMENT, URL, URL_VIGNETTE, POIDS, CODE_REDACTEUR, DATE_CREATION, THEMATIQUE, META_KEYWORDS, SPECIFIC_DATA, TRADUCTION_DATA, ACCESSIBILITY_DATA, IS_MUTUALISE) values (:id, :titre, :legende, :description, :auteur, :copyright, :typeRessource, :typeMedia, :source, :format, :codeRubrique, :codeRattachement, :url, :urlVignette, :poids, :codeRedacteur, :dateCreation, :thematique, :metaKeywords, :specificData, :traductionData, :accessibilityData, :isMutualise)", getParameters(mediaBean));
        }catch (final DataIntegrityViolationException die){
            LOG.warn(String.format("Unable to add the MEDIA with the id : %s and the url : \"%s\" caused by DataIntegrityViolationException. Maybe the media already exists.", mediaBean.getId(), mediaBean.getUrl()));
        }
        catch (final DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"MEDIA\"", mediaBean.toString()), dae);
        }
        return mediaBean;
    }
}
