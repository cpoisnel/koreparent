package com.univ.collaboratif.utils;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.RequeteMgr;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.context.ContextLoaderListener;
import com.univ.mediatheque.Mediatheque;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.FileUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * The Class LectureFichiergw.
 *
 * @author Rom, cpoisnel
 *
 *         <p>
 *         A part of this was adapted from a post from BalusC on 20090218 to a forum here: http://balusc.blogspot.co.uk/2009/02/fileservlet-supporting-resume-and.html
 */
public class LectureFichiergw extends HttpServlet {

    private static final String[] extraType = new String[] {"LOGO_ESPACE", "NEWSLETTER"};

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8923414367463884140L;

    // 604800000 ms = 1 week.
    private static final long DEFAULT_EXPIRE_TIME = 604800000L;

    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    private static final String MULTIPART_BOUNDARY = "MULTIPART_BYTERANGES";

    private static final String CHARSET = "UTF-8";

    private static final Logger LOG = LoggerFactory.getLogger(LectureFichiergw.class);

    boolean isApercu = Boolean.FALSE;

    private final transient ServiceMedia serviceMedia;

    private final transient ServiceRessource serviceRessource;

    public LectureFichiergw(){
        serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
        serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
    }

    /**
     * Returns true if the given match header matches the given value.
     *
     * @param matchHeader
     *            The match header.
     * @param toMatch
     *            The value to be matched.
     * @return True if the given match header matches the given value.
     */
    private static boolean matches(final String matchHeader, final String toMatch) {
        final String[] matchValues = matchHeader.split("\\s*,\\s*");
        Arrays.sort(matchValues);
        return Arrays.binarySearch(matchValues, toMatch) > -1 || Arrays.binarySearch(matchValues, "*") > -1;
    }

    /**
     * Returns a substring of the given string value from the given begin index to the given end index as a long. If the substring is empty, then -1 will be returned
     *
     * @param value
     *            The string value to return a substring as long for.
     * @param beginIndex
     *            The begin index of the substring to be returned as long.
     * @param endIndex
     *            The end index of the substring to be returned as long.
     * @return A substring of the given string value as long or -1 if substring is empty.
     */
    private static long sublong(final String value, final int beginIndex, final int endIndex) {
        final String substring = value.substring(beginIndex, endIndex);
        return (substring.length() > 0) ? Long.parseLong(substring) : -1;
    }

    /**
     * Copy the given byte range of the given input to the given writer.
     *
     * @param input
     *            The input to copy the given range to the given writer for.
     * @param writer
     *            The output to copy the given range from the given input for.
     * @param start
     *            Start of the byte range.
     * @param length
     *            Length of the byte range.
     * @throws IOException
     *             If something fails at I/O level.
     */
    private static void copy(final RandomAccessFile input, final OutputStream writer, final long start, final long length) throws IOException {
        final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int read;
        if (input.length() == length) {
            // Write full range.
            while ((read = input.read(buffer)) > 0) {
                writer.write(buffer, 0, read);
            }
        } else {
            // Write partial range.
            input.seek(start);
            long toRead = length;
            while ((read = input.read(buffer)) > 0) {
                if ((toRead -= read) > 0) {
                    writer.write(buffer, 0, read);
                } else {
                    writer.write(buffer, 0, (int) toRead + read);
                    break;
                }
            }
        }
    }

    /**
     * Close the given resource.
     *
     * @param resource
     *            The resource to be closed.
     */
    private static void close(final Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (final IOException e) {
                LOG.debug("IO exception, possibly because of a client aborted request", e);
                // Ignore IOException. If you want to handle this anyway, it might be useful to know
                // that this will generally only be thrown when the client aborted the request.
            }
        }
    }

    /**
     * Process incoming HTTP GET requests.
     *
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        setApercu(request);
        final String uri = request.getRequestURI();
        final Pattern compile = Pattern.compile(StringUtils.replace(System.getProperty(ContextLoaderListener.MEDIA_URL_PATTERN), "/*", "(/.*)"));
        final Matcher matcher = compile.matcher(uri);
        if (matcher.find()) {
            performMedia(request, response, matcher.group(1));
        } else {
            performFichiergw(request, response);
        }
    }

    private void setApercu(final HttpServletRequest request) {
        final String uri = request.getRequestURI();
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) request.getSession().getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        if (sessionUtilisateur != null) {
            final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
            if (infosSession != null && infosSession.get(uri) != null) {
                isApercu = Boolean.TRUE;
                infosSession.remove(uri);
            } else {
                isApercu = Boolean.FALSE;
            }
        }
    }

    /**
     * Perform media request
     *
     * @param request
     * @param response
     * @param path
     */
    public void performMedia(final HttpServletRequest request, final HttpServletResponse response, final String path) {
        final String uri = request.getRequestURI();
        final String sMedia = StringUtils.substringAfterLast(uri, "/");
        final String codeRubrique = serviceMedia.getCodeRubrique(sMedia);
        boolean dsi = true;
        String pageErreur = "";
        if (!isApercu && StringUtils.isNotEmpty(codeRubrique)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            dsi = serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), codeRubrique);
            pageErreur = ctx.getInfosSite().getJspFo() + "/error/403.jsp";
        }
        try {
            if (dsi) {
                final Mediatheque mediatheque = Mediatheque.getInstance();
                String extension = FileUtil.getExtension(sMedia);
                if (StringUtils.isEmpty(extension)) {
                    final int iDot = sMedia.lastIndexOf(MediathequeHelper.PRE_EXTENSION_CHARACTER);
                    if (iDot != -1) {
                        extension = (sMedia.substring(iDot + 1));
                    }
                }
                final String contentType = mediatheque.getContentType(extension);
                final String sName = StringUtils.substringBeforeLast(sMedia, "_") + "." + extension;
                String typeDisposition = "inline";
                if ("FALSE".equals(request.getParameter("INLINE"))) {
                    typeDisposition = "attachment";
                }
                if (StringUtils.isNotEmpty(path)) {
                    traiterFluxFile(ContexteUtil.getContexteUniv(), request, response, MediathequeHelper.getAbsolutePath() + path, contentType, sName, typeDisposition);
                } else {
                    traiterFluxError(ContexteUtil.getContexteUniv(), request, response, HttpServletResponse.SC_NOT_FOUND, "", "");
                }
            } else {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                final RequestDispatcher rd = getServletContext().getRequestDispatcher(pageErreur);
                rd.forward(request, response);
            }
        } catch (final IOException | ServletException e) {
            LOG.error("unable to send the given file", e);
        }
    }

    /**
     * Process incoming requests for information.
     *
     * @param request
     *            Object that encapsulates the request to the servlet
     * @param response
     *            Object that encapsulates the response from the servlet
     */
    private void performFichiergw(final HttpServletRequest request, final HttpServletResponse response) {
        try {
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            int status = HttpServletResponse.SC_NOT_FOUND;
            final String idFichier = request.getParameter("ID_FICHIER");
            String idFiche = request.getParameter("ID_FICHE");
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            Long lidFichier = null;
            try {
                lidFichier = Long.parseLong(idFichier);
            } catch (final NumberFormatException e) {
                LOG.debug("unable to parse the file's id", e);
                traiterFluxError(ctx, request, response, status, idFichier, idFiche);
                return;
            }
            final MediaBean mediaBean = serviceMedia.getById(lidFichier);
            if (mediaBean == null || StringUtils.isBlank(mediaBean.getSource())) {
                traiterFluxError(ctx, request, response, status, idFichier, idFiche);
                return;
            }
            boolean ok = Boolean.TRUE;
            boolean controleApercu = Boolean.FALSE;
            // si le fichier n'est pas public
            if (!isApercu) {
                if (!MediaUtils.isPublic(mediaBean)) {
                    ok = Boolean.FALSE;
                    // selection des ressources associées au média
                    final RequeteSQL requeteSelect = new RequeteSQL();
                    final ClauseOrderBy orderBy = new ClauseOrderBy("ID_RESSOURCE", SensDeTri.DESC);
                    final ClauseWhere where = new ClauseWhere(ConditionHelper.egalVarchar("ID_MEDIA", String.valueOf(mediaBean.getId())));
                    // si la fiche parente est specifiee pour controle
                    if (StringUtils.isNotEmpty(idFiche)) {
                        where.and(ConditionHelper.like("CODE_PARENT", idFiche, "", ",%"));
                        controleApercu = Boolean.TRUE;
                    }
                    where.and(ConditionHelper.notEgal("ETAT", "0", TypeOperande.VARCHAR));
                    requeteSelect.where(where).orderBy(orderBy);
                    final List<RessourceBean> ressources = serviceRessource.getFromRequest(requeteSelect);
                    // Cas 1 : le fichier est rattaché à une fiche via une ressource
                    if (CollectionUtils.isNotEmpty(ressources)) {
                        FicheUniv ficheUniv = null;
                        // boucle sur les ressources
                        for (RessourceBean currentRessource : ressources) {
                            final String codeParent = currentRessource.getCodeParent();
                            // en apercu on ne controle pas la fiche car elle a déja été supprimée
                            if (StringUtils.isNotEmpty(codeParent) && !codeParent.startsWith("TYPE") && ("1".equals(currentRessource.getEtat()) || (controleApercu && "2".equals(currentRessource.getEtat())))) {
                                // si on ne connait pas l'id de la fiche on le récupère dans le code parent
                                if (idFiche == null) {
                                    idFiche = codeParent.substring(0, codeParent.indexOf(","));
                                }
                                String typeObjet = codeParent.substring(codeParent.indexOf("TYPE=") + 5);
                                // on récupère le code objet
                                if (typeObjet.length() > 4) {
                                    if (typeObjet.contains("_")) {
                                        typeObjet = typeObjet.substring(typeObjet.indexOf("_") + 1, typeObjet.indexOf("_") + 5);
                                    }
                                    if (typeObjet.contains(",")) {
                                        typeObjet = typeObjet.substring(0, typeObjet.indexOf(","));
                                    }
                                }
                                ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(typeObjet));
                                // fiche parente
                                if (ficheUniv != null) {
                                    ficheUniv.setCtx(ctx);
                                    ficheUniv.init();
                                    ficheUniv.setIdFiche(Long.valueOf(idFiche));
                                    try {
                                        ficheUniv.retrieve();
                                    } catch (final Exception e) {
                                        LOG.debug("unable to retrieve the content", e);
                                        // la fiche n'existe plus et on controle apercu
                                        if ("2".equals(currentRessource.getEtat()) && controleApercu) {
                                            ok = Boolean.TRUE;
                                            break;
                                        }
                                        idFiche = null;
                                        continue;
                                    }
                                    //on gère les codes de renvoi pour les robots
                                    if ("0001".equals(ficheUniv.getEtatObjet())) {
                                        status = HttpServletResponse.SC_FORBIDDEN;
                                    } else if ("0007".equals(ficheUniv.getEtatObjet())) {
                                        status = HttpServletResponse.SC_GONE;
                                    }
                                    // test l'autorisation sur la fiche en ligne
                                    if ("0003".equals(ficheUniv.getEtatObjet())) {
                                        if (FicheUnivMgr.controlerRestriction(ficheUniv, ctx)) {
                                            ok = Boolean.TRUE;
                                            break;
                                        } else {
                                            status = HttpServletResponse.SC_FORBIDDEN;
                                        }
                                    }
                                } else {
                                    for (final String type : extraType) {
                                        if (codeParent.contains("TYPE=" + type)) {
                                            ok = Boolean.TRUE;
                                            break;
                                        }
                                    }
                                }
                            }
                            idFiche = null;
                        }
                    } else {
                        // Cas 2 : le fichier n'est pas rattaché à une fiche et existe uniquement en tant que média
                        final ClauseWhere orphanWhere = new ClauseWhere(ConditionHelper.egalVarchar("ID_MEDIA", String.valueOf(mediaBean.getId())));
                        final List<RessourceBean> orphanRessources = serviceRessource.getFromWhere(orphanWhere);
                        if (CollectionUtils.isEmpty(orphanRessources)) {
                            ok = serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), mediaBean.getCodeRubrique());
                        }
                    }
                } else {
                    // sur un fichier public on ne controle que la dsi sur la rubrique
                    ok = serviceRubrique.controlerRestrictionRubrique(ctx.getGroupesDsiAvecAscendants(), mediaBean.getCodeRubrique());
                }
            }
            if (ok) {
                traiterFluxMedia(ctx, request, response, mediaBean);
            } else {
                traiterFluxError(ctx, request, response, status, idFichier, idFiche);
            }
        } catch (final IOException | ServletException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * Process file, compute disposition : inline or attachment
     *
     * @param ctx
     *            the ctx
     * @param response
     *            the response
     * @param media
     *            the fichiergw
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void traiterFluxMedia(final ContexteUniv ctx, final HttpServletRequest request, final HttpServletResponse response, final MediaBean media) throws IOException {
        final String pathFichier = MediaUtils.getPathAbsolu(media);
        final String contentType = media.getFormat();
        final String source = media.getSource();
        String typeDisposition = "inline";
        if ("FALSE".equals(request.getParameter("INLINE"))) {
            typeDisposition = "attachment";
        }
        traiterFluxFile(ctx, request, response, pathFichier, contentType, source, typeDisposition);
    }

    /**
     * Process File, write Header & content
     *
     * @param ctx
     * @param request
     * @param response
     * @param pathFichier
     * @param contentType
     * @param source
     * @param typeDisposition
     * @throws IOException
     */
    private void traiterFluxFile(final ContexteUniv ctx, final HttpServletRequest request, final HttpServletResponse response, final String pathFichier, final String contentType, final String source, final String typeDisposition) throws IOException {
        final ServletOutputStream writer = response.getOutputStream();
        // calcul (approximatif) du contentType en se basant sur l'extension du fichier
        // dans la base, il y a /application/octet-stream
        // (sauf pour les images)
        // JSS 20040222 : Gestion des identifiants de requete
        // On n'attend pas que le  document soit téléchargé
        // (détection de boucles intempestives)
        RequeteMgr.terminerRequete(ctx.getIdRequete());
        final File f = new File(pathFichier);
        if (f.exists()) {
            // Gestion des différents types d'appels (Avec Cache / Gestion des ranges, requêtes partielles)
            final String fileName = f.getName();
            final long fsize = f.length();
            final long lastModified = f.lastModified();
            final String eTag = fileName + "_" + fsize + "_" + lastModified;
            final long expires = System.currentTimeMillis() + DEFAULT_EXPIRE_TIME;
            final String ifNoneMatch = request.getHeader("If-None-Match");
            boolean processDone = processIfNoneMatch(response, ifNoneMatch, eTag, expires);
            if (!processDone) {
                final long ifModifiedSince = request.getDateHeader("If-Modified-Since");
                processDone = processIfModifiedSince(response, ifNoneMatch, ifModifiedSince, eTag, expires, lastModified);
            }
            if (!processDone) {
                final String ifMatch = request.getHeader("If-Match");
                processDone = processIfMatch(response, ifMatch, eTag);
            }
            if (!processDone) {
                final long ifUnmodifiedSince = request.getDateHeader("If-Unmodified-Since");
                processDone = processIfUnmodifiedSince(response, ifUnmodifiedSince, lastModified);
            }
            if (!processDone) {
                final Range full = new Range(0, fsize - 1, fsize);
                final List<Range> ranges = new ArrayList<>();
                // Validate and process Range and If-Range headers.
                final String range = request.getHeader("Range");
                final String ifRange = request.getHeader("If-Range");
                processDone = processRange(response, request, full, ifRange, ranges, range, fsize, eTag, lastModified);
                if (!processDone) {
                    response.setBufferSize(DEFAULT_BUFFER_SIZE);
                    response.setHeader("Content-Disposition", "" + typeDisposition + ";filename=\"" + source + "\"");
                    // Adding filesize pour partial content
                    response.setHeader("Content-Length", String.valueOf(fsize));
                    response.setContentType(contentType);
                    response.setHeader("Accept-Ranges", "bytes");
                    response.setHeader("ETag", eTag);
                    response.setDateHeader("Last-Modified", lastModified);
                    response.setDateHeader("Expires", expires);
                    this.writeFileOnResponse(f, response, writer, full, ranges, contentType);
                }
            }
        } else {
            LOG.warn("no file was found with path " + pathFichier);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            final String s = MessageHelper.getCoreMessage(ctx.getLocale(), "ST_DOCUMENT_INEXISTANT");
            final byte[] msg = s.getBytes(CHARSET);
            writer.write(msg);
            writer.close();
        }
    }

    /**
     * Write file on output stream, append header attributes
     *
     * @param file
     *            The input file
     * @param response
     *            The response
     * @param writer
     *            The response's stream
     * @param full
     *            The entire file
     * @param ranges
     *            The list of asked ranges
     * @param contentType
     *            contentType of file (computed with extension)
     * @throws IOException
     */
    private void writeFileOnResponse(final File file, final HttpServletResponse response, final ServletOutputStream writer, final Range full, final List<Range> ranges, final String contentType) throws IOException {
        // Prepare streams.
        RandomAccessFile input = null;
        try {
            // Open streams.
            input = new RandomAccessFile(file, "r");
            if (ranges.isEmpty() || ranges.get(0) == full) {
                // Return full file.
                final Range r = full;
                response.setContentType(contentType);
                response.setHeader("Content-Range", "bytes " + r.start + "-" + r.end + "/" + r.total);
                // Content length is not directly predictable in case of GZIP.
                // So only add it if there is no means of GZIP, else browser will hang.
                response.setHeader("Content-Length", String.valueOf(r.length));
                // Copy full range.
                copy(input, writer, r.start, r.length);
            } else if (ranges.size() == 1) {
                // Return single part of file.
                final Range r = ranges.get(0);
                response.setContentType(contentType);
                response.setHeader("Content-Range", "bytes " + r.start + "-" + r.end + "/" + r.total);
                response.setHeader("Content-Length", String.valueOf(r.length));
                response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT); // 206.
                // Copy single part range.
                copy(input, writer, r.start, r.length);
            } else {
                // Return multiple parts of file.
                response.setContentType("multipart/byteranges; boundary=" + MULTIPART_BOUNDARY);
                response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT); // 206.
                // Cast back to ServletOutputStream to get the easy println methods.
                // Copy multi part range.
                for (final Range r : ranges) {
                    // Add multipart boundary and header fields for every range.
                    writer.println();
                    writer.println("--" + MULTIPART_BOUNDARY);
                    writer.println("Content-Type: " + contentType);
                    writer.println("Content-Range: bytes " + r.start + "-" + r.end + "/" + r.total);
                    // Copy single part range of multi part range.
                    copy(input, writer, r.start, r.length);
                    // End with multipart boundary.
                    writer.println();
                    writer.println("--" + MULTIPART_BOUNDARY + "--");
                }
            }
        } finally {
            close(writer);
            close(input);
        }
    }

    /**
     * If-None-Match header should contain "*" or ETag. If so, then return 304.
     *
     * @return true if it matches
     */
    private boolean processIfNoneMatch(final HttpServletResponse response, final String ifNoneMatch, final String eTag, final long expires) {
        boolean result = false;
        if (ifNoneMatch != null && matches(ifNoneMatch, eTag)) {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            // Required in 304.
            response.setHeader("ETag", eTag);
            // Postpone cache with 1 week.
            response.setDateHeader("Expires", expires);
            result = true;
        }
        return result;
    }

    /**
     * If-Modified-Since header should be greater than LastModified. If so, then return 304. This header is ignored if any If-None-Match header is specified.
     *
     * @param response
     * @param ifNoneMatch
     * @param ifModifiedSince
     * @param eTag
     *            Header Request attribute (Token computed with filename_filesize_lastModified)
     * @param expires
     * @param lastModified
     * @return True if it's totally processed, false otherwise
     */
    private boolean processIfModifiedSince(final HttpServletResponse response, final String ifNoneMatch, final long ifModifiedSince, final String eTag, final long expires, final long lastModified) {
        boolean result = false;
        if (ifNoneMatch == null && ifModifiedSince != -1 && ifModifiedSince + 1000 > lastModified) {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            // Required in 304.
            response.setHeader("ETag", eTag);
            // Postpone cache with 1 week.
            response.setDateHeader("Expires", expires);
            result = true;
        }
        return result;
    }

    /**
     * If-Match header should contain "*" or ETag. If not, then return 412.
     *
     * @param response
     * @param ifMatch
     * @param eTag
     *            Header Request attribute (Token computed with filename_filesize_lastModified)
     * @return True if it's totally processed, false otherwise
     * @throws IOException
     */
    private boolean processIfMatch(final HttpServletResponse response, final String ifMatch, final String eTag) throws IOException {
        boolean result = false;
        if (ifMatch != null && !matches(ifMatch, eTag)) {
            response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED);
            result = true;
        }
        return result;
    }

    /**
     * If-Unmodified-Since header should be greater than LastModified. If not, then return 412
     *
     * @param response
     * @param ifUnmodifiedSince
     * @param lastModified
     * @return True if it's totally processed, false otherwise
     * @throws IOException
     */
    private boolean processIfUnmodifiedSince(final HttpServletResponse response, final long ifUnmodifiedSince, final long lastModified) throws IOException {
        boolean result = false;
        if (ifUnmodifiedSince != -1 && ifUnmodifiedSince + 1000 <= lastModified) {
            response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED);
            result = true;
        }
        return result;
    }

    /**
     * Compute file, with asked range
     *
     * @param response
     *            The response
     * @param full
     *            The entire file
     * @param ifRange
     *            Header attribute
     * @param ranges
     *            The ranges of the file
     * @param range
     *            Header attribute
     * @param length
     *            Filesize
     * @param eTag
     *            Header Request attribute (Token computed with filename_filesize_lastModified)
     * @param lastModified
     *            Last modification of file
     * @return True if it's totally processed, false otherwise
     * @throws IOException
     */
    private boolean processRange(final HttpServletResponse response, final HttpServletRequest request, final Range full, final String ifRange, final List<Range> ranges, final String range, final long length, final String eTag, final long lastModified) throws IOException {
        boolean result = false;
        if (range != null) {
            // Range header should match format "bytes=n-n,n-n,n-n...". If not, then return 416.
            if (!range.matches("^bytes=\\d*-\\d*(,\\d*-\\d*)*$")) {
                // Required in 416.
                response.setHeader("Content-Range", "bytes */" + length);
                response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
                result = true;
            } else {
                // If-Range header should either match ETag or be greater then LastModified. If not,
                // then return full file.
                if (ifRange != null && !ifRange.equals(eTag)) {
                    try {
                        // Can throw an IAE
                        final long ifRangeTime = request.getDateHeader("If-Range");
                        if (ifRangeTime != -1 && ifRangeTime + 1000 < lastModified) {
                            ranges.add(full);
                        }
                    } catch (final IllegalArgumentException e) {
                        LOG.debug("no header found for If-Range", e);
                        ranges.add(full);
                    }
                }
                // If any valid If-Range header, then process each part of byte range.
                if (ranges.isEmpty()) {
                    for (final String part : range.substring(6).split(",")) {
                        // Assuming a file with length of 100, the following examples returns bytes at:
                        // 50-80 (50 to 80), 40- (40 to length=100), -20 (length-20=80 to length=100).
                        long start = sublong(part, 0, part.indexOf("-"));
                        long end = sublong(part, part.indexOf("-") + 1, part.length());
                        if (start == -1) {
                            start = length - end;
                            end = length - 1;
                        } else if (end == -1 || end > length - 1) {
                            end = length - 1;
                        }
                        // Check if Range is syntactically valid. If not, then return 416.
                        if (start > end) {
                            // Required in 416.
                            response.setHeader("Content-Range", "bytes */" + length);
                            response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
                            result = true;
                        } else {
                            // Add range.
                            ranges.add(new Range(start, end, length));
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Forward to Error page
     *
     * @param ctx
     * @param request
     * @param response
     * @param status
     * @param idFichier
     * @param idFiche
     * @throws Exception
     */
    public void traiterFluxError(final ContexteUniv ctx, final HttpServletRequest request, final HttpServletResponse response, final int status, final String idFichier, final String idFiche) throws IOException, ServletException {
        response.setStatus(status);
        String pageErreur = StringUtils.EMPTY;
        if (status == HttpServletResponse.SC_NOT_FOUND || status == HttpServletResponse.SC_GONE) {
            pageErreur = ctx.getInfosSite().getJspFo() + "/error/404.jsp";
        } else {
            if (ctx.getCode().length() > 0) {
                pageErreur = ctx.getInfosSite().getJspFo() + "/error/403.jsp";
            } else {
                final String url = URLResolver.getAbsoluteUrl(RessourceUtils.PATH_SERVLET_LECTURE_FICHIER + "?ID_FICHIER=" + idFichier + ((idFiche != null) ? "&ID_FICHE=" + idFiche : ""), ctx);
                UnivWebFmt.redirigerVersLogin(ctx, response, url);
            }
        }
        if (StringUtils.isNotEmpty(pageErreur)) {
            final RequestDispatcher rd = getServletContext().getRequestDispatcher(pageErreur);
            rd.forward(request, response);
        }
    }

    /**
     * This class represents a byte range.
     */
    protected static class Range {

        private long start;

        private long end;

        private long length;

        private long total;

        /**
         * Construct a byte range.
         *
         * @param start
         *            Start of the byte range.
         * @param end
         *            End of the byte range.
         * @param total
         *            Total length of the byte source.
         */
        public Range(final long start, final long end, final long total) {
            this.start = start;
            this.end = end;
            this.length = end - start + 1;
            this.total = total;
        }
    }
}