package com.univ.collaboratif.bean;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.univ.objetspartages.bean.AbstractPersistenceBean;

/**
 * Created on 27/04/15.
 */
public class UserRolesCollaboratifBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = 1673694997162517665L;

    private String codeUser;

    private String codeRole;

    private Long idCollaboratif;

    public String getCodeUser() {
        return codeUser;
    }

    public void setCodeUser(String codeUser) {
        this.codeUser = codeUser;
    }

    public String getCodeRole() {
        return codeRole;
    }

    public void setCodeRole(String codeRole) {
        this.codeRole = codeRole;
    }

    public Long getIdCollaboratif() {
        return idCollaboratif;
    }

    public void setIdCollaboratif(Long idCollaboratif) {
        this.idCollaboratif = idCollaboratif;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(codeUser)
            .append(codeRole)
            .append(idCollaboratif)
            .toHashCode();
    }

    @Override
    public boolean equals(final Object obj){
        if(obj != null && obj.getClass() == this.getClass()) {
            final UserRolesCollaboratifBean other = (UserRolesCollaboratifBean) obj;
            return new EqualsBuilder()
                .append(codeUser, other.codeUser)
                .append(codeRole, other.codeRole)
                .append(idCollaboratif, other.idCollaboratif)
                .isEquals();
        } else {
            return false;
        }
    }
}
