package com.univ.collaboratif.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.univ.collaboratif.bean.UserRolesCollaboratifBean;

/**
 * Created on 27/04/15.
 */
public class UserRolesCollaboratifDAO extends AbstractCommonDAO<UserRolesCollaboratifBean> {

    private static final Logger LOG = LoggerFactory.getLogger(UserRolesCollaboratifDAO.class);

    public UserRolesCollaboratifDAO() {
        tableName = "USER_ROLES_COLLABORATIF";
    }

    public List<UserRolesCollaboratifBean> getByIdCollaboratif(Long id) {
        List<UserRolesCollaboratifBean> results;
        try {
            MapSqlParameterSource params = new MapSqlParameterSource("idCollaboratif", id);
            results = namedParameterJdbcTemplate.query("SELECT * FROM USER_ROLES_COLLABORATIF WHERE ID_COLLABORATIF = :idCollaboratif", params, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for USER_ROLES_COLLABORATIF row with collaboratif id %d", id), dae);
        }
        return results;
    }

    public List<UserRolesCollaboratifBean> getByCodeUser(String code) {
        List<UserRolesCollaboratifBean> results;
        try {
            MapSqlParameterSource params = new MapSqlParameterSource("codeUser", code);
            results = namedParameterJdbcTemplate.query("SELECT * FROM USER_ROLES_COLLABORATIF WHERE CODE_USER = :codeUser", params, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for USER_ROLES_COLLABORATIF row with user code %s", code), dae);
        }
        return results;
    }

    public List<UserRolesCollaboratifBean> getByCodeRoleAndIdCollab(final String codeRole, final long idEspacecollaboratif) {
        List<UserRolesCollaboratifBean> results;
        try {
            MapSqlParameterSource params = new MapSqlParameterSource("codeRole", codeRole);
            params.addValue("idCollaboratif", idEspacecollaboratif);
            results = namedParameterJdbcTemplate.query("SELECT * FROM USER_ROLES_COLLABORATIF WHERE CODE_ROLE = :codeRole AND ID_COLLABORATIF = :idCollaboratif", params, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for USER_ROLES_COLLABORATIF row with role code %s and collaboratif id %s", codeRole, idEspacecollaboratif), dae);
        }
        return results;
    }

    public void deleteByIdCollaboratif(final Long idEspace) {
        try {
            SqlParameterSource namedParameters = new MapSqlParameterSource("id_collaboratif", idEspace);
            namedParameterJdbcTemplate.update(String.format("delete from `%1$s` WHERE ID_COLLABORATIF = :id_collaboratif", tableName), namedParameters);
        } catch (DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with id_collaboratif %d from table \"%s\"", idEspace, tableName), dae);
        }
    }

    public UserRolesCollaboratifBean getByCodeUserAndIdCollab(final String codeUser, final Long idEspacecollaboratif) {
        UserRolesCollaboratifBean result = null;
        try {
            MapSqlParameterSource params = new MapSqlParameterSource("codeUser", codeUser);
            params.addValue("idCollaboratif", idEspacecollaboratif);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM USER_ROLES_COLLABORATIF WHERE CODE_USER = :codeUser AND ID_COLLABORATIF = :idCollaboratif", params, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s and espace id %d on table %s", codeUser, idEspacecollaboratif, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for code user {} and id espace collaboratif {}", codeUser, idEspacecollaboratif);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for USER_ROLES_COLLABORATIF row with user code %s and collaboratif id %s", codeUser, idEspacecollaboratif), dae);
        }
        return result;
    }

    public void deleteByCodeUserAndIdCollab(String codeUser, Long idEspacecollaboratif) {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource("idCollaboratif", idEspacecollaboratif);
            params.addValue("codeUser", codeUser);
            namedParameterJdbcTemplate.update(String.format("delete from `%1$s` WHERE ID_COLLABORATIF = :idCollaboratif AND CODE_USER = :codeUser", tableName), params);
        } catch (DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with idCollaboratif %d and code user %s from table \"%s\"", idEspacecollaboratif, codeUser, tableName), dae);
        }
    }

    public void deleteByCodeUser(String codeMembre) {
        try {
            SqlParameterSource namedParameters = new MapSqlParameterSource("code_user", codeMembre);
            namedParameterJdbcTemplate.update(String.format("delete from `%1$s` WHERE CODE_USER = :code_user", tableName), namedParameters);
        } catch (DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with code_user %s from table \"%s\"", codeMembre, tableName), dae);
        }
    }
}
