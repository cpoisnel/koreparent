package com.univ.collaboratif.om;

import java.util.Collection;

/**
 * The Class InfosEspaceCollaboratif.
 */
public class InfosEspaceCollaboratif {

    /** The id. */
    private long id = 0L;

    /** The code. */
    private String code = "";

    /** The intitule. */
    private String intitule = "";

    /** The groupes membres. */
    private String groupesMembres = "";

    /** The groupes consultation. */
    private String groupesConsultation = "";

    /** The groupes inscription. */
    private String groupesInscription = "";

    /** The inscription front. */
    private String inscriptionFront = "";

    /** The roles membre. */
    private String rolesMembre = "";

    /** The description. */
    private String description = "";

    /** The code theme. */
    private String codeTheme = "";

    /** The code rubrique. */
    private String codeRubrique = "";

    /** The langue. */
    private String langue = "";

    /** The actif. */
    private boolean actif;

    /** The services list. */
    private Collection<String> servicesList = null;

    /**
     * Instantiates a new info espace bean.
     */
    public InfosEspaceCollaboratif() {
        super();
    }

    /**
     * Instantiates a new infos espace collaboratif.
     *
     * @param id the id
     * @param code the code
     * @param intitule the intitule
     * @param groupesMembres the groupes membres
     * @param groupesConsultation the groupes consultation
     * @param groupesInscription the groupes inscription
     * @param inscriptionFront the inscription front
     * @param rolesMembre the roles membre
     * @param theme the theme
     * @param description the description
     * @param codeRubrique the code rubrique
     * @param langue the langue
     * @param actif the actif
     * @param servicesList the services list
     */
    public InfosEspaceCollaboratif(final long id, final String code, final String intitule, final String groupesMembres, final String groupesConsultation, final String groupesInscription, final String inscriptionFront, final String rolesMembre, final String theme, final String description, final String codeRubrique, final String langue, final boolean actif, final Collection<String> servicesList) {
        super();
        this.id = id;
        this.code = code;
        this.intitule = intitule;
        this.groupesMembres = groupesMembres;
        this.groupesConsultation = groupesConsultation;
        this.groupesInscription = groupesInscription;
        this.inscriptionFront = inscriptionFront;
        this.rolesMembre = rolesMembre;
        this.codeTheme = theme;
        this.description = description;
        this.servicesList = servicesList;
        this.codeRubrique = codeRubrique;
        this.langue = langue;
        this.actif = actif;
    }

    /**
     * Gets the id.
     *
     * @return Returns the id.
     */
    public long getId() {
        return id;
    }

    /**
     * Gets the code.
     *
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the code rubrique.
     *
     * @return the code rubrique
     */
    public String getCodeRubrique() {
        return codeRubrique;
    }

    /**
     * Gets the langue.
     *
     * @return the langue
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Gets the description.
     *
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the groupes membres.
     *
     * @return Returns the groupesMembres.
     */
    public String getGroupesMembres() {
        return groupesMembres;
    }

    /**
     * Gets the intitule.
     *
     * @return Returns the intitule.
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Gets the roles membre.
     *
     * @return Returns the rolesMembre.
     */
    public String getRolesMembre() {
        return rolesMembre;
    }

    /**
     * Gets the services list.
     *
     * @return Returns the services list.
     */
    public Collection<String> getServicesList() {
        return servicesList;
    }

    /**
     * Gets the code theme.
     *
     * @return Returns the codeTheme.
     */
    public String getCodeTheme() {
        return codeTheme;
    }

    /**
     * Gets the groupes consultation.
     *
     * @return the groupes consultation
     */
    public String getGroupesConsultation() {
        return groupesConsultation;
    }

    /**
     * Gets the groupes inscription.
     *
     * @return the groupes inscription
     */
    public String getGroupesInscription() {
        return groupesInscription;
    }

    /**
     * Gets the inscription front.
     *
     * @return the inscription front
     */
    public String getInscriptionFront() {
        return inscriptionFront;
    }

    /**
     * Checks if is actif.
     *
     * @return true, if is actif
     */
    public boolean isActif() {
        return actif;
    }
}
