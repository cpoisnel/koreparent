package com.univ.identity.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.univ.objetspartages.bean.AbstractPersistenceBean;
import com.univ.utils.json.Views;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class IdentityBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = -4310523108544923506L;

    @JsonView(Views.FrontOfficeView.class)
    protected Long id = null;

    @JsonView(Views.FrontOfficeView.class)
    protected String civilite = null;

    @JsonView(Views.FrontOfficeView.class)
    protected String nom = null;

    @JsonView(Views.FrontOfficeView.class)
    protected String prenom = null;

    @JsonView(Views.FrontOfficeView.class)
    protected String adresse = null;

    @JsonView(Views.FrontOfficeView.class)
    protected String codePostal = null;

    @JsonView(Views.FrontOfficeView.class)
    protected String ville = null;

    @JsonView(Views.FrontOfficeView.class)
    protected String pays = null;

    @JsonView(Views.FrontOfficeView.class)
    protected String telephone = null;

    @JsonView(Views.FrontOfficeView.class)
    protected String email = null;

    @JsonIgnore
    protected String data = null;

    protected String jspComplementFragment = null;

    public void init(IdentityBean bean) {
        this.id = bean.id;
        this.civilite = bean.civilite;
        this.nom = bean.nom;
        this.prenom = bean.prenom;
        this.adresse = bean.adresse;
        this.codePostal = bean.codePostal;
        this.ville = bean.ville;
        this.pays = bean.pays;
        this.telephone = bean.telephone;
        this.email = bean.email;
        this.data = bean.data;
        this.jspComplementFragment = bean.jspComplementFragment;
    }

    public void setIdIdentity(Long idIndentity) {
        this.id = idIndentity;
    }

    public Long getIdIdentity() {
        return id;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getVille() {
        return ville;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getPays() {
        return pays;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public String getJspComplementFragment() {
        return jspComplementFragment;
    }

    public void setJspComplementFragment(String jspComplementFragment) {
        this.jspComplementFragment = jspComplementFragment;
    }
}
