package com.univ.identity.processus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;

public abstract class AbstractIdentityProcess extends ProcessusBean {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractIdentityProcess.class);

    protected boolean proceed = true;

    public AbstractIdentityProcess(InfoBean infoBean) {
        super(infoBean);
    }

    @Override
    /**
     * Point d'entree du processus.
     */
    public boolean traiterAction() throws Exception {
        action = infoBean.getString("ACTION");
        try {
            if ("INSCRIRE".equals(action)) {
                preparerSaisie();
            } else {
                gererActions();
            }
        } catch (Exception e) {
            LOG.debug("an error occured processing identity", e);
            infoBean.addMessageErreur(e.toString());
        }
        //placer l'état dans le composant d'infoBean
        infoBean.setEcranLogique(ecranLogique);
        return etat == FIN;
    }

    protected void gererActions() throws Exception {
        if (InfoBean.ACTION_ENREGISTRER.equals(action)) {
            if (proceed) {
                preTraiterEnregistrer();
            }
            if (proceed) {
                traiterEnregistrer();
            }
            if (proceed) {
                postTraiterEnregistrer();
            }
        }
    }

    // Action par défaut
    protected abstract void preparerSaisie() throws Exception;

    // Action enregitrer
    protected void preTraiterEnregistrer() throws Exception {}

    protected abstract void traiterEnregistrer() throws Exception;

    protected void postTraiterEnregistrer() throws Exception {}

}
