/*
 * Created on 14 sept. 2005
 *
 * Gestionnaire de pools de threads pour les services
 * 
 * utilise concurrent.jar
 */
package com.univ.services;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.LoggerFactory;

import com.kportal.core.config.PropertyHelper;
// TODO: Auto-generated Javadoc

/**
 * Délègue au module concurrent l'exécution d'un service.
 *
 * @author jeanseb
 */
public class ThreadsPool {

    /** The Constant MAX_THREADS_DEFAUT. */
    private static final int MAX_THREADS_DEFAUT = 50;

    /** The Constant MIN_THREADS_DEFAUT. */
    private static final int MIN_THREADS_DEFAUT = 10;

    /** The Constant MAX_REQUETES_ATTENTE. */
    private static final int MAX_REQUETES_ATTENTE = 100;

    /**
     * Temps maximum en milliseconds de maintien d'un thread idle dans le pool
     */
    private static final int KEEP_ALIVE_TIME = 30000;

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(ThreadsPool.class);

    /** The INSTANCE. */
    private static ThreadsPool INSTANCE = null;

    /** The pool. */
    private final ThreadPoolExecutor pool;

    /**
     * Instantiates a new threads pool.
     */
    private ThreadsPool() {
        int maxThreads = MAX_THREADS_DEFAUT;
        final String sMaxThreads = PropertyHelper.getCoreProperty("services.max_threads");
        if (sMaxThreads != null) {
            maxThreads = Integer.parseInt(sMaxThreads);
        }
        int minThreads = MIN_THREADS_DEFAUT;
        final String sMinThreads = PropertyHelper.getCoreProperty("services.min_thread");
        if (sMinThreads != null) {
            minThreads = Integer.parseInt(sMinThreads);
        }
        int tailleQueue = MAX_REQUETES_ATTENTE;
        final String sTailleQueue = PropertyHelper.getCoreProperty("services.nb_max_requetes_attente");
        if (sTailleQueue != null) {
            tailleQueue = Integer.parseInt(sTailleQueue);
        }
        LOG.debug("Création pool threads maxThreads=" + maxThreads + " minThreads=" + minThreads + " tailleQueue=" + tailleQueue);
        final BlockingQueue<Runnable> workingQueue = new ArrayBlockingQueue<>(tailleQueue);
        pool = new ThreadPoolExecutor(minThreads, maxThreads, KEEP_ALIVE_TIME, TimeUnit.MILLISECONDS, workingQueue);
    }

    /**
     * Gets the single instance of ThreadsPool.
     *
     * @return single instance of ThreadsPool
     */
    public static ThreadsPool getInstance() {
        if (INSTANCE == null) {
            INSTANCE = createInstance();
        }
        return INSTANCE;
    }

    /**
     * Creates the instance.
     *
     * @return the threads pool
     */
    public static synchronized ThreadsPool createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ThreadsPool();
        }
        return INSTANCE;
    }

    /**
     * Execute.
     *
     * @param service
     *            the service
     *
     * @throws Exception
     *             the exception
     */
    public void execute(final Runnable service) throws Exception {
        pool.execute(service);
    }
}