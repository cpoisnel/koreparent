package com.univ.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.identification.GestionnaireIdentification;
import com.univ.objetspartages.om.ServiceBean;
import com.univ.utils.ContexteUniv;
import com.univ.utils.RequeteUtil;
import com.univ.utils.ServicesUtil;
import com.univ.utils.URLResolver;

/**
 * Gestion des caches de services.
 */
public class CacheServiceViewManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "cacheServiceViewManager";

    private static final Logger LOG = LoggerFactory.getLogger(CacheServiceViewManager.class);

    /**
     * Supprime le cache d'un service à partir d'une application externe.
     *
     * @param writer
     *            the writer
     * @param requete
     *            the _requete
     *
     * @throws Exception
     *             the exception
     */
    public static void suppressionCacheService(final Writer writer, final String requete) throws Exception {
        LOG.debug("suppressionCacheParIdentifiantExterne cache externe ");
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder builder = factory.newDocumentBuilder();
        final String codeRetour = "0";
        final String ksession = RequeteUtil.renvoyerParametre(requete, "ksession");
        final String codeService = RequeteUtil.renvoyerParametre(requete, "service");
        final ServiceBean service = ServicesUtil.getService(codeService);
        final String key = ksession + "@" + codeService + "@" + service.getVueReduiteUrl();
        final CacheServiceViewManager cacheServiceMgr = (CacheServiceViewManager) ApplicationContextManager.getCoreContextBean(CacheServiceViewManager.ID_BEAN);
        cacheServiceMgr.flushView(key);
        /** *********************************************** */
        /* Génération du fichier XML SSO */
        /** *********************************************** */
        final TransformerFactory transFactory = TransformerFactory.newInstance();
        final Transformer transformer = transFactory.newTransformer();
        Document document = builder.newDocument();
        final Node nodeFiche = document.createElement("REPONSE_KPORTAL");
        final Node nodeCodeRetour = document.createElement("CODE_RETOUR");
        nodeCodeRetour.appendChild(document.createTextNode(codeRetour));
        nodeFiche.appendChild(nodeCodeRetour);
        document.appendChild(nodeFiche);
        /*
         * Using the DOM tree root node, the following line of code constructs a
         * DOMSource object as the source of the transformation.
         */
        final DOMSource source = new DOMSource(document);
        /*
         * The following code fragment creates a StreamResult object to take the
         * results of the transformation and transforms the tree to XML.
         */
        final StreamResult result = new StreamResult(writer);
        transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
        transformer.transform(source, result);
    }

    /**
     * Gets the contenu service.
     *
     * @param ctx
     *            the ctx
     * @param requete
     *            the requete
     *
     * @return the contenu service
     *
     * @throws Exception
     *             the exception
     */
    public static String getContenuService(final ContexteUniv ctx, final RequeteService requete) throws Exception {
        final CacheServiceViewManager cacheServiceMgr = (CacheServiceViewManager) ApplicationContextManager.getCoreContextBean(CacheServiceViewManager.ID_BEAN);
        return cacheServiceMgr.getServiceView(ctx, requete.getService(), requete.getVue());
    }

    /**
     * Execution de la vue réduite
     *
     * Pour l'instant, seule l'url est gérée.
     *
     * @param ctx
     *            the ctx
     * @param serviceBean
     *            the service bean
     * @param nomVue
     *            the nom vue
     *
     * @return @throws Exception
     *
     * @throws Exception
     *             the exception
     */
    @Cacheable(value = "CacheServiceViewManager.getServiceView")
    public String getServiceView(final ContexteUniv ctx, final ServiceBean serviceBean, final String nomVue) throws Exception {
        String contenu = "";
        String url = serviceBean.getVueReduiteUrl();
        if (StringUtils.isNotEmpty(url)) {
            if (nomVue.length() > 0) {
                url += (!url.contains("?") ? "?" : "&");
                url += "nomvue=" + nomVue;
            }
            String kticket = "";
            if ("1".equals(serviceBean.getJetonKportal())) {
                kticket = ServicesUtil.genererTicketService(ctx);
            }
            url = ServicesUtil.ajouterParametresService(ctx, serviceBean, url, kticket);
            // Ajout proxy CAS
            if ("1".equals(serviceBean.getProxyCas())) {
                final String ticket = GestionnaireIdentification.getInstance().getValidateurCAS().getProxyTicket(ctx.getCode(), ctx.getCodeGestion(), url);
                if (ticket != null) {
                    url += (!url.contains("?") ? "?" : "&");
                    url += "PT=" + ticket;
                }
            }
            // Si l'url commence par /, on rajoute le host
            if (url.charAt(0) == '/') {
                url = URLResolver.getAbsoluteUrl(url, ctx);
            }
            LOG.debug("*** url service appelée " + url);
            try {
                final URL urlAppel = new URL(url);
                final URLConnection urlConnection = urlAppel.openConnection();
                String ligne = "";
                InputStreamReader reader = new InputStreamReader(urlConnection.getInputStream());
                final BufferedReader flux = new BufferedReader(reader);
                while ((ligne = flux.readLine()) != null) {
                    contenu += ligne;
                }
                flux.close();
            } catch (final IOException e) {
                LOG.error("erreur lors de l'appel de l'url", e);
            }
        }
        return contenu;
    }

    /**
     * Flush view.
     *
     * @param key
     *            the key
     */
    @CacheEvict(value = "CacheServiceViewManager.getServiceView")
    public void flushView(final String key) {}
}
