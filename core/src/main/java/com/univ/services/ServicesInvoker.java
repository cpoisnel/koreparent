/*
 * Gestion des appels  de service
 */
package com.univ.services;

import java.util.Hashtable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.PropertyHelper;
import com.univ.utils.ContexteUniv;
// TODO: Auto-generated Javadoc

/**
 * Lancement de n threads (un par service) en passant par un pool de threads puis attente de toutes les réponses
 *
 * Les données envoyées et recues sont stockées dans une Map.
 *
 * @author jeanseb
 */
public class ServicesInvoker {

    /** The Constant TIMEOUT_THREAD. */
    private static final int TIMEOUT_THREAD = 10;

    private static final Logger LOG = LoggerFactory.getLogger(ServicesInvoker.class);
    // Timeout par défaut en secondes

    /** The nb requetes traitees. */
    int nbRequetesTraitees = 0;

    /** The reponses. */
    Hashtable<String, ReponseService> reponses = new Hashtable<>();

    /**
     * Executer.
     *
     * @param _ctx
     *            the _ctx
     * @param _requetes
     *            the _requetes
     *
     * @return the map
     *
     * @throws Exception
     *             the exception
     */
    public Map<String, ReponseService> executer(final ContexteUniv _ctx, final Map<String, RequeteService> _requetes) throws Exception {
        LOG.debug("ServicesInvoker  -- début");
        for (String cleRequete : _requetes.keySet()) {
            final RequeteService requete = _requetes.get(cleRequete);
            // Création d'un enregistrement pour la réponse
            reponses.put(cleRequete, new ReponseService());
            // Création de l'action
            final ServiceThread thread = new ServiceThread(this, _ctx, cleRequete, requete);
            ThreadsPool.getInstance().execute(thread);
        }
        // Boucle jusquà la fin ou + de 5s.
        final long debut = System.currentTimeMillis();
        int timeout = TIMEOUT_THREAD;
        final String sTimeout = PropertyHelper.getCoreProperty("services.timeout_execution");
        if (sTimeout != null) {
            timeout = Integer.parseInt(sTimeout);
        }
        while (!estTermine() && System.currentTimeMillis() - debut < timeout * 1000) {
        }
        LOG.debug("ServicesInvoker  -- fin");
        return reponses;
    }

    /**
     * Fin requete.
     *
     * @param _idRequete
     *            the _id requete
     * @param _reponse
     *            the _reponse
     */
    protected synchronized void finRequete(final String _idRequete, final String _reponse) {
        reponses.get(_idRequete).setTerminated(true);
        reponses.get(_idRequete).setReponse(_reponse);
        nbRequetesTraitees++;
    }

    /**
     * Est termine.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    private synchronized boolean estTermine() throws Exception {
        final boolean res = nbRequetesTraitees == reponses.size();
        if (!res) {
            wait(100);
        }
        return res;
    }
}