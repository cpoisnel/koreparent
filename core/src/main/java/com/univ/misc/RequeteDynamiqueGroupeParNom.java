/*
 * Created on 12 mai 2005
 *
 * Test perfs groupes dynamiques
 */
package com.univ.misc;

import java.util.Collection;
import java.util.Map;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.RequeteGroupeDynamique;

/**
 * The Class RequeteDynamiqueGroupeParNom.
 *
 * @author jeanseb
 *
 *         Gestion d'un groupe dynamique de test qui renvoie tous les utilisateurs pour tester les perfs de k-portal sur des volumes importants
 */
@Deprecated
public class RequeteDynamiqueGroupeParNom extends RequeteGroupeDynamique {

    private static final Logger LOG = LoggerFactory.getLogger(RequeteDynamiqueGroupeParNom.class);

    public RequeteDynamiqueGroupeParNom() {
        super();
        setNomRequete("REQUETE_GROUPE_NOM");
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.RequeteGroupeDynamique#getVecteurUtilisateurs(java.lang.String)
     */
    @Override
    public Vector<String> getVecteurUtilisateurs(final String _requeteGroupe) throws Exception {
        LOG.debug("RequeteDynamiquePerfs.getVecteurUtilisateur (" + _requeteGroupe + ")");
        final Vector<String> res = new Vector<>();
        final Collection<UtilisateurBean> utilisateurs = serviceUser.getByNom(_requeteGroupe.substring(_requeteGroupe.lastIndexOf("_") + 1));
        for (UtilisateurBean currentUser : utilisateurs) {
            res.add(currentUser.getCode());
        }
        return res;
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.RequeteGroupeDynamique#getGroupesUtilisateur(java.lang.String)
     */
    @Override
    public Vector<String> getGroupesUtilisateur(final String codeUtilisateur) {
        LOG.debug("RequeteDynamiquePerfs.getGroupesUtilisateur ()");
        final Vector<String> res = new Vector<>();
        final UtilisateurBean utilisateur = serviceUser.getByCode(codeUtilisateur);
        // on recupere les groupes gérés par cette requete dynamique
        for (final GroupeDsiBean infos : serviceGroupeDsi.getAllDynamics()) {
            if (infos.getRequeteGroupe().equals(getNomRequete())) {
                String select = "";
                if (infos.getCode().lastIndexOf("_") != -1) {
                    select = infos.getCode().substring(infos.getCode().lastIndexOf("_") + 1);
                }
                if (utilisateur.getNom().startsWith(select)) {
                    res.add(infos.getCode());
                }
            }
        }
        return res;
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.RequeteGroupeDynamique#synchroniserGroupes(int, java.util.Map)
     */

    /**
     * Synchroniser groupes.
     *
     * @param action
     *            the action
     * @param mapProprietes
     *            the map proprietes
     *
     * @throws Exception
     *             the exception
     */
    public void synchroniserGroupes(final int action, final Map mapProprietes) throws Exception {
        LOG.debug("RequeteGroupeFormation.synchroniserGroupes ()");
    }

    /* (non-Javadoc)
     * @see com.univ.objetspartages.om.RequeteGroupeDynamique#estGereParReqDyn(java.lang.String)
     */
    @Override
    protected boolean estGereParReqDyn(final String requeteGroupe) {
        return true;
    }

    /**
     * Preparer principal.
     *
     * @param infoBean
     *            the info bean
     * @param groupe
     *            the groupe
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public void preparerPRINCIPAL(final InfoBean infoBean, final GroupeDsiBean groupe) throws Exception {}
}
