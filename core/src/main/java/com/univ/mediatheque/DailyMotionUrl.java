package com.univ.mediatheque;

public class DailyMotionUrl implements SpecificUrl {

    private static String URL_SOURCE = "//www.dailymotion.com/embed/video/";

    private static String TYP_RESSOURCE = "video";

    @Override
    public String processUrl(String url) {
        // url entrée http://www.dailymotion.com/featured/video/x35714_cap-nord-projet-1_creation
        // url sortie http://www.dailymotion.com/swf/x35714
        String res = url;
        String codeVideo = "";
        if (url.contains("/")) {
            url = url.substring(url.lastIndexOf("/") + 1, url.length());
        }
        if (url.contains("_")) {
            codeVideo = url.substring(0, url.indexOf("_"));
            res = URL_SOURCE + codeVideo;
        }
        return res;
    }

    @Override
    public String getTypeRessource() {
        return TYP_RESSOURCE;
    }

    @Override
    public String getUrlSource() {
        return URL_SOURCE;
    }
}
