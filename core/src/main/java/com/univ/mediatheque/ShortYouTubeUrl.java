package com.univ.mediatheque;

import org.apache.commons.lang3.StringUtils;

public class ShortYouTubeUrl implements SpecificUrl {

    private static String URL_SOURCE = "//www.youtube.com/embed/";

    private static String TYP_RESSOURCE = "video";

    @Override
    public String processUrl(final String url) {
        // Url entrée http://www.youtu.be/auRo72Yg5YE
        // Url sortie http://www.youtube.com/embed/auRo72Yg5YE
        String codeVideo = StringUtils.substringAfter(url, "youtu.be/");
        codeVideo = StringUtils.substringBefore(codeVideo, "?");
        String res = url;
        if (StringUtils.isNotBlank(codeVideo)) {
            res = URL_SOURCE + codeVideo;
        }
        return res;
    }

    @Override
    public String getTypeRessource() {
        return TYP_RESSOURCE;
    }

    @Override
    public String getUrlSource() {
        return URL_SOURCE;
    }
}
