package com.univ.mediatheque.player.visionneuse;

import java.io.Writer;

import com.univ.mediatheque.player.MediaPlayer;
import com.univ.mediatheque.style.MediaStyle;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.utils.ContexteUniv;

/**
 * Element qui permet de produire du code HTML afin d'afficher un certain type de média.
 *
 * @author Pierre Cosson
 */
public interface VisionneusePlayer {

    /**
     * Savoir dans quel mode va se faire l'affichage du player.
     *
     * @return Le type d'affichage de la visionneuse.
     */
    VisionneusePlayerType getType();

    void initialiserVisionneuse(ContexteUniv ctx, Writer out, MediaPlayer player) throws Exception;

    /**
     * Initialise le style de média : {@link MediaStyle} en lui affectant le lanceur.
     *
     * @param ctx
     * @param media
     * @param player
     * @param mediaStyle
     */
    void setLanceurMedia(ContexteUniv ctx, MediaBean media, MediaPlayer player, MediaStyle mediaStyle);

    void init();

    int getMaxHeight();

    int getMaxWidth();
}
