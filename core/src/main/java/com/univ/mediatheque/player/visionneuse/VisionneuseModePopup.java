package com.univ.mediatheque.player.visionneuse;

import java.io.Writer;

import org.apache.commons.lang3.StringEscapeUtils;

import com.univ.mediatheque.container.AttributBaliseHTML;
import com.univ.mediatheque.container.LanceurMedia;
import com.univ.mediatheque.player.MediaPlayer;
import com.univ.mediatheque.style.MediaStyle;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;

/**
 * Visionneuse de média qui affiche tous les players des médias dans une popup.<br/>
 * Utilise le JS : lightboxXL.js<br/>
 * L'identifiant du player utilisé par cette classe est construit à partir d'un identifiant généré en même temps que l'instance mais aussi en fonction du nom du style de média.
 * Cela signifie que si vous souhaitez afficher des médias dans la "même popup" (en faisant suivant ou précédent par exemple) il faut que le nom du style soit similaire et que
 * l'instance utilisé soit la même. <br/>
 * <strong>Dans le mode popup, c'est les players distants qui sont utilisés.</strong>
 *
 * @author Pierre Cosson
 */
public class VisionneuseModePopup implements VisionneusePlayer {

    /** Le mode d'affichage de cette visionneuse. */
    public static final VisionneusePlayerType MODE = VisionneusePlayerType.POPUP;

    private final int maxHeight = -1;

    private final int maxWidth = -1;

    /**
     * Constructeur.
     */
    public VisionneuseModePopup() {
        this.init();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.visionneuse.VisionneusePlayer#getMaxHeight()
     */
    @Override
    public int getMaxHeight() {
        return this.maxHeight;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.visionneuse.VisionneusePlayer#getMaxWidth()
     */
    @Override
    public int getMaxWidth() {
        return this.maxWidth;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.visionneuse.VisionneusePlayer#init()
     */
    @Override
    public void init() {}

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.visionneuse.VisionneusePlayer#getType()
     */
    @Override
    public VisionneusePlayerType getType() {
        return MODE;
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.player.visionneuse.VisionneusePlayer#
     * initialiserVisionneuse(com.univ.utils.ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.player.MediaPlayer)
     */
    @Override
    public void initialiserVisionneuse(final ContexteUniv ctx, final Writer out, final MediaPlayer player) throws Exception {
        // ne fait rien
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.visionneuse.VisionneusePlayer#setLanceurMedia
     * (com.univ.utils.ContexteUniv, com.univ.objetspartages.om.Media,
     * com.univ.mediatheque.player.MediaPlayer,
     * com.univ.mediatheque.style.MediaStyle)
     */
    @Override
    public void setLanceurMedia(final ContexteUniv ctx, final MediaBean media, final MediaPlayer player, final MediaStyle mediaStyle) {
        final LanceurMedia lanceur = new LanceurMedia(media);
        if (MediaUtils.isLocal(media)) {
            lanceur.addAttributHTML(AttributBaliseHTML.HREF, player.getURLPlayerDistant(ctx, media));
        } else {
            lanceur.addAttributHTML(AttributBaliseHTML.HREF, MediaUtils.getUrlAbsolue(media));
        }
        lanceur.addAttributHTML(AttributBaliseHTML.TITLE, StringEscapeUtils.escapeHtml4(media.getTitre()));
        lanceur.addAttributHTML(AttributBaliseHTML.CLASS, "lanceur_media ");
        mediaStyle.setLanceurMedia(lanceur);
    }
}
