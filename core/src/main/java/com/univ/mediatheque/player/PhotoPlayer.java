package com.univ.mediatheque.player;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.univ.mediatheque.container.AttributBaliseHTML;
import com.univ.mediatheque.container.LanceurMedia;
import com.univ.mediatheque.utils.HTMLUtils;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;

/**
 * Ce player permet de jouer les médias de type photo. De plus cette classe produit des éléments avec les class CSS suivantes :
 * <ul>
 * <li>player_photo sur une balise P</li>
 * <li>lanceur_media sur une balise A (la balise qui permet de jouer le média dans le player)</li>
 * <li>lanceur_media_photo sur une balise A (la balise qui permet de jouer le média dans le player)</li>
 * <li>lanceur_media_play sur une balise A (quand c'est le media lié à ce lanceur qui est jouer) FAIT VIA DU JS</li>
 * </ul>
 * On utilise la propriété JTF suivante : <code>mediatheque.imagePath</code>, pour récupérer les pictogrammes.
 *
 * @author Pierre Cosson
 */
public class PhotoPlayer extends AbstractMediaPlayer implements MediaPlayer {

    public static final MediaPlayerType PLAYER_TYPE = MediaPlayerType.PHOTO_PLAYER;

    private static String HTML_NOM_CLASS_CSS_LANCEUR = "lanceur_media lanceur_media_photo";

    private static String HTML_NOM_CLASS_CSS_PLAYER = "player_photo";

    private String photoLoadingURL;

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getInialiserVuePlayer(com.univ
     * .utils.ContexteUniv, java.lang.String)
     */
    @Override
    public String getInialiserVuePlayer(ContexteUniv ctx, String palyerId) {
        int width = super.getWidth();
        int height = super.getHeight();
        // ajout de la fonction à lancer pour afficher le player.
        return "<p id=\"" + palyerId + "\" class=\"" + HTML_NOM_CLASS_CSS_PLAYER + "\"></p>\n" + HTMLUtils.HTML_SCRIPT_JS_OPEN + "var obj_" + palyerId + " = new PhotoPlayer(\"" + palyerId + "\", " + width + ", " + height + ", \"" + this.photoLoadingURL + "\");\n" + this.getInitJSPlayerStyle("obj_" + palyerId) + HTMLUtils.genererMediathequeLanceurJSAdd("obj_" + palyerId + ".genererPlayer()", 100) + HTMLUtils.HTML_SCRIPT_JS_CLOSE;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getLanceurMedia(com.univ.utils
     * .ContexteUniv, com.univ.objetspartages.om.Media, java.lang.String)
     */
    @Override
    public LanceurMedia getLanceurMedia(ContexteUniv ctx, MediaBean media, String playerId) {
        LanceurMedia lanceur = new LanceurMedia(media);
        lanceur.addAttributHTML(AttributBaliseHTML.CLASS, HTML_NOM_CLASS_CSS_LANCEUR);
        lanceur.addAttributHTML(AttributBaliseHTML.ON_CLICK, "obj_" + playerId + ".lancerMedia('" + StringEscapeUtils.escapeEcmaScript(URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx)) + "', this);");
        return lanceur;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.MediaPlayer#getType()
     */
    @Override
    public MediaPlayerType getType() {
        return PLAYER_TYPE;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getURLPlayerDistant(com.univ.
     * utils.ContexteUniv, com.univ.objetspartages.om.Media)
     */
    @Override
    public String getURLPlayerDistant(ContexteUniv ctx, MediaBean media) {
        // dans le cas ou une URL de player distant spécifique est donnée on ne
        // passe plus par chikbox et son affichage des images
        String urlPlayerDistantSpecif = style.getSpecificUrlPlayerDistant();
        if (StringUtils.isNotEmpty(urlPlayerDistantSpecif)) {
            return this.genererDefaultURLplayerDistant(ctx, media);
        } else {
            return URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx);
        }
    }

    /**
     * Affecter une nouvelle imlage de chargement au player.
     *
     * @param photoLoadingUrl
     *            L'URL de la photo.
     */
    public void setPhotoLoading(String photoLoadingUrl) {
        this.photoLoadingURL = photoLoadingUrl;
    }
}
