package com.univ.mediatheque.player;

import org.apache.commons.lang3.StringEscapeUtils;

import com.univ.mediatheque.container.AttributBaliseHTML;
import com.univ.mediatheque.container.LanceurMedia;
import com.univ.mediatheque.utils.HTMLUtils;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;
/**
 * Ce player permet de jouer les médias de type video. De plus cette classe
 * produit des éléments avec les class CSS suivantes :
 * <ul>
 * <li>player_video sur une balise P</li>
 * <li>lanceur_media sur une balise A (la balise qui permet de jouer le média
 * dans le player)</li>
 * <li>lanceur_media_video sur une balise A (la balise qui permet de jouer le
 * média dans le player)</li>
 * <li>lanceur_media_play sur une balise A (quand c'est le media lié à ce
 * lanceur qui est jouer) FAIT VIA DU JS</li>
 * </ul>
 *
 * @author Pierre Cosson
 */

/**
 * @author pierre.cosson
 *
 */
public class VideoPlayer extends AbstractMediaPlayer {

    public static final MediaPlayerType PLAYER_TYPE = MediaPlayerType.VIDEO_PLAYER;

    private static String HTML_NOM_CLASS_CSS_LANCEUR = "lanceur_media lanceur_media_video";

    private static String HTML_NOM_CLASS_CSS_PLAYER = "player_video";

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getInialiserVuePlayer(com.univ
     * .utils.ContexteUniv, java.lang.String)
     */
    @Override
    public String getInialiserVuePlayer(ContexteUniv ctx, String playerId) {
        StringBuilder returnPlayer = new StringBuilder();
        int width = super.getWidth();
        int height = super.getHeight();
        // on génére la balise qui va contenir le player Video
        returnPlayer.append("<div class=\"");
        returnPlayer.append(HTML_NOM_CLASS_CSS_PLAYER);
        returnPlayer.append(" ");
        returnPlayer.append(style.getName());
        returnPlayer.append("\" style=\"height:");
        returnPlayer.append(String.valueOf(style.getHeight()));
        returnPlayer.append("px;\" id=\"");
        returnPlayer.append(playerId);
        returnPlayer.append("\"></div>\n");
        // on génére le script JS qui va permettre l'affichage du player
        returnPlayer.append(HTMLUtils.HTML_SCRIPT_JS_OPEN);
        returnPlayer.append("var obj_");
        returnPlayer.append(playerId);
        returnPlayer.append(" = new VideoPlayer('");
        returnPlayer.append(playerId);
        returnPlayer.append("', ");
        returnPlayer.append(width);
        returnPlayer.append(", ");
        returnPlayer.append(height);
        returnPlayer.append(",'");
        returnPlayer.append(this.getType().getPlayerUrl());
        returnPlayer.append("');\n");
        returnPlayer.append(super.getInitJSPlayerStyle("obj_" + playerId));
        // ajout de la fonction à lancer pour afficher le player.
        returnPlayer.append(HTMLUtils.genererMediathequeLanceurJSAdd("obj_" + playerId + ".genererPlayer()", 100));
        returnPlayer.append(HTMLUtils.HTML_SCRIPT_JS_CLOSE);
        return returnPlayer.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getLanceurMedia(com.univ.utils
     * .ContexteUniv, com.univ.objetspartages.om.Media, java.lang.String)
     */
    @Override
    public LanceurMedia getLanceurMedia(ContexteUniv ctx, MediaBean media, String playerId) {
        LanceurMedia lanceur = new LanceurMedia(media);
        String titreMedia = media.getTitre();
        String urlMedia = URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx);
        lanceur.addJSAvantLanceur(HTMLUtils.genererMediathequeLanceurJSAdd("obj_" + playerId + ".ajouterVideo('" + StringEscapeUtils.escapeEcmaScript(urlMedia) + "', '" + StringEscapeUtils.escapeEcmaScript(titreMedia) + "')", 1));
        lanceur.addAttributHTML(AttributBaliseHTML.CLASS, HTML_NOM_CLASS_CSS_LANCEUR);
        lanceur.addAttributHTML(AttributBaliseHTML.ON_CLICK, "obj_" + playerId + ".lancerMedia('" + StringEscapeUtils.escapeEcmaScript(URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx)) + "', this);");
        return lanceur;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.MediaPlayer#getType()
     */
    @Override
    public MediaPlayerType getType() {
        return PLAYER_TYPE;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getURLPlayerDistant(com.univ.
     * utils.ContexteUniv, com.univ.objetspartages.om.Media)
     */
    @Override
    public String getURLPlayerDistant(ContexteUniv ctx, MediaBean media) {
        return this.genererDefaultURLplayerDistant(ctx, media);
    }
}
