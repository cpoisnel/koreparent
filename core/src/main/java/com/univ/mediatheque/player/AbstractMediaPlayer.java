package com.univ.mediatheque.player;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.univ.mediatheque.style.PlayerStyle;
import com.univ.mediatheque.utils.HTMLUtils;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.FileUtil;
import com.univ.utils.URLResolver;

/**
 * Fournit une base de fonctionnement aux classes qui doivent implémenter l'interface {@link MediaPlayer}.
 *
 * @author Pierre Cosson
 */
public abstract class AbstractMediaPlayer implements MediaPlayer {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractMediaPlayer.class);

    /** Début des nom des fonctions JS. */
    protected static String FONCTION_JS_START = "set";

    /**
     * Style du player.
     */
    protected PlayerStyle style;

    public void setStyle(final PlayerStyle style) {
        this.style = style;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.MediaPlayer#getStyle()
     */
    @Override
    public PlayerStyle getStyle() {
        return this.style;
    }

    /**
     * Permet d'initialiser le fonctionnement du player. Pour cela on utilise les propriétés contenu dans le style.
     *
     * @param playerJSObject
     *            Le nom de l'objet Javascript correspondant au player.
     *
     * @return La liste des appels de fonction d'initialisation de valeur.
     */
    protected String getInitJSPlayerStyle(final String playerJSObject) {
        final StringBuilder returnStr = new StringBuilder();
        StringBuilder fonctionJS;
        String valeurPropiete;
        final Collection<String> proprietesPlayer = style.getListeNomsProprieteInitialisation();
        if (proprietesPlayer != null) {
            for (final String nomPropriete : proprietesPlayer) {
                valeurPropiete = style.getValeurProprieteInitialisation(nomPropriete);
                if (this.getType().isAutorisee(nomPropriete) && valeurPropiete != null && valeurPropiete.length() > 0) {
                    fonctionJS = new StringBuilder();
                    fonctionJS.append(FONCTION_JS_START);
                    // premiére lettre en majuscule et le reste en minuscule
                    fonctionJS.append(nomPropriete.substring(0, 1).toUpperCase());
                    fonctionJS.append(nomPropriete.substring(1).toLowerCase());
                    returnStr.append(HTMLUtils.genererMediathequeLanceurJSAdd(playerJSObject + "." + fonctionJS + "('" + StringEscapeUtils.escapeEcmaScript(valeurPropiete) + "')", 5));
                }
            }
        }
        return returnStr.toString();
    }

    /**
     * Cette fonction permet de générer l'URL du player distant. Par défaut elle construit l'URL du player avec celle contenue dans les PROPERTIES du player sauf si l'URL
     * spécifique est définie dans le {@link PlayerStyle}.
     *
     * @param ctx
     *            Le contexte de l'application.
     * @param media
     *            Le {@link com.univ.objetspartages.bean.MediaBean} qui sera lancé par le player.
     *
     * @return L'URL absolue correctemment formatée et préte à être utilisée ou une NULL dans le cas d'une erreur d'encodage d'url.
     */
    protected String genererDefaultURLplayerDistant(final ContexteUniv ctx, final MediaBean media) {
        try {
            final StringBuilder urlHTTP = new StringBuilder();
            // test si on doit prendre l'URL spécifique ou non.
            final String urlPlayerDistantSpecific = style.getSpecificUrlPlayerDistant();
            if (StringUtils.isEmpty(urlPlayerDistantSpecific)) {
                urlHTTP.append(this.getType().getUrlDistante());
            } else {
                urlHTTP.append(urlPlayerDistantSpecific);
            }
            urlHTTP.append("?URL_MEDIA=");
            urlHTTP.append(URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx));
            String valeurPropiete;
            final Collection<String> proprietesPlayer = style.getListeNomsProprieteInitialisation();
            if (proprietesPlayer != null) {
                for (final String nomPropriete : proprietesPlayer) {
                    valeurPropiete = style.getValeurProprieteInitialisation(nomPropriete);
                    if (this.getType().isAutorisee(nomPropriete) && valeurPropiete != null && valeurPropiete.length() > 0) {
                        urlHTTP.append("&amp");
                        urlHTTP.append(nomPropriete);
                        urlHTTP.append("=");
                        urlHTTP.append(URLEncoder.encode(valeurPropiete, CharEncoding.DEFAULT));
                    }
                }
            }
            // ajout du heigh
            final int width = this.getWidth();
            if (urlHTTP.length() > 0) {
                urlHTTP.append("&amp;");
            }
            urlHTTP.append("width=");
            urlHTTP.append(width);
            final int height = this.getHeight();
            urlHTTP.append("&amp;height=");
            urlHTTP.append(height);
            final String extension = FileUtil.getExtension(MediaUtils.getUrlAbsolue(media));
            urlHTTP.append("&amp;EXTENSION=");
            urlHTTP.append(extension);
            return URLResolver.getAbsoluteUrl(urlHTTP.toString(), ctx);
        } catch (final UnsupportedEncodingException e) {
            LOG.error("erreur d'encodage", e);
            return null;
        }
    }

    /**
     * Récupérer la longueur spécifiés dans un style d'affichage ou celle par defaut si rien n'est défini dans le style.
     *
     * @return La longueur définie dans le style ou celle par défaut si aucune n'est présente dans le style (en pixel). La valeur peut aussi être -1 pour indiquer qu'ilf aut
     *         prendre le maximum de place sur l'écran.
     */
    protected int getWidth() {
        int width = style.getWidth();
        // la -1 permet de dire (pour le mode popup) de prendre le maximum de
        // place sur l'écran.
        if (width < -1) {
            width = this.getType().getWidth();
        }
        return width;
    }

    /**
     * Récupérer la hauteur spécifiés dans un style d'affichage ou celle par defaut si rien n'est défini dans le style.
     *
     * @return La hauteur définie dans le style ou celle par défaut si aucune n'est présente dans le style (en pixel). La valeur peut aussi être -1 pour indiquer qu'ilf aut prendre
     *         le maximum de place sur l'écran.
     */
    protected int getHeight() {
        int height = style.getHeight();
        // la -1 permet de dire (pour le mode popup) de prendre le maximum de
        // place sur l'écran.
        if (height < -1) {
            height = this.getType().getHeight();
        }
        return height;
    }
}
