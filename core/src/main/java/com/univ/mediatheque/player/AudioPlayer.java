package com.univ.mediatheque.player;

import org.apache.commons.lang3.StringEscapeUtils;

import com.univ.mediatheque.container.AttributBaliseHTML;
import com.univ.mediatheque.container.LanceurMedia;
import com.univ.mediatheque.utils.HTMLUtils;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;

/**
 * Cette classe permet de produire el code HTML qui peremttra d'utiliser le player audio (JS + flash dewPlayer). De plus cette classe produit des éléments avec les class CSS
 * suivantes :
 * <ul>
 * <li>player_audio sur une balise DIV (la div qui délimite tout les élements du player)</li>
 * <li>lanceur_media sur une balise A (la balise qui permet de jouer le média dans le player)</li>
 * <li>lanceur_media_audio sur une balise A (la balise qui permet de jouer le média dans le player)</li>
 * <li>lanceur_media_play sur une balise A (quand c'est le media lié à ce lanceur qui est jouer) FAIT VIA DU JS</li>
 * </ul>
 *
 * @author Pierre Cosson
 */
public class AudioPlayer extends AbstractMediaPlayer implements MediaPlayer {

    public static final MediaPlayerType PLAYER_TYPE = MediaPlayerType.AUDIO_PLAYER;

    public static final String HTML_CSS_CLASS_DIV_PLAYER = "player_audio";

    public static final String HTML_CSS_CLASS_A_LANCEUR = "lanceur_media lanceur_media_audio";

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getInialiserVuePlayer(com.univ
     * .utils.ContexteUniv, java.lang.String)
     */
    @Override
    public String getInialiserVuePlayer(ContexteUniv ctx, String playerId) {
        StringBuilder returnPlayer = new StringBuilder();
        String playerURL = URLResolver.getAbsoluteUrl(PLAYER_TYPE.getPlayerUrl(), ctx);
        // on génére la balise qui va contenir le player audio
        returnPlayer.append("<div class=\"");
        returnPlayer.append(HTML_CSS_CLASS_DIV_PLAYER);
        returnPlayer.append("\" style=\"height:");
        returnPlayer.append(String.valueOf(style.getHeight()));
        returnPlayer.append("px;line-height:");
        returnPlayer.append(String.valueOf(style.getHeight()));
        returnPlayer.append("px;\" id=\"");
        returnPlayer.append(playerId);
        returnPlayer.append("\"></div>\n");
        // on génére le script JS qui va permettre l'affichage du player
        returnPlayer.append(HTMLUtils.HTML_SCRIPT_JS_OPEN);
        returnPlayer.append("var obj_");
        returnPlayer.append(playerId);
        returnPlayer.append("= new AudioPlayer(\"");
        returnPlayer.append(playerId);
        returnPlayer.append("\", ");
        // attention le player audio n'est pas extensible
        returnPlayer.append(String.valueOf(PLAYER_TYPE.getWidth()));
        returnPlayer.append(", ");
        returnPlayer.append(String.valueOf(PLAYER_TYPE.getHeight()));
        returnPlayer.append(", \"");
        returnPlayer.append(StringEscapeUtils.escapeEcmaScript(playerURL));
        returnPlayer.append("\");\n");
        returnPlayer.append(super.getInitJSPlayerStyle("obj_" + playerId));
        // ajout de la fonction à lancer pour afficher le player.
        returnPlayer.append(HTMLUtils.genererMediathequeLanceurJSAdd("obj_" + playerId + ".genererPlayer()", 100));
        returnPlayer.append(HTMLUtils.HTML_SCRIPT_JS_CLOSE);
        return returnPlayer.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getLanceurMedia(com.univ.utils
     * .ContexteUniv, com.univ.objetspartages.om.Media, java.lang.String)
     */
    @Override
    public LanceurMedia getLanceurMedia(ContexteUniv ctx, MediaBean media, String playerId) {
        LanceurMedia lanceur = new LanceurMedia(media);
        String titreMedia = media.getTitre();
        String urlMedia = URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx);
        lanceur.addJSAvantLanceur(HTMLUtils.genererMediathequeLanceurJSAdd("obj_" + playerId + ".ajouterMedia(\"" + StringEscapeUtils.escapeEcmaScript(urlMedia) + "\",\"" + StringEscapeUtils.escapeEcmaScript(StringEscapeUtils.escapeHtml4(titreMedia)) + "\")", 1));
        lanceur.addAttributHTML(AttributBaliseHTML.CLASS, HTML_CSS_CLASS_A_LANCEUR);
        lanceur.addAttributHTML(AttributBaliseHTML.ON_CLICK, "obj_" + playerId + ".lancerMedia('" + StringEscapeUtils.escapeEcmaScript(URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx)) + "', this);");
        return lanceur;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.MediaPlayer#getMediaPlayerType()
     */
    @Override
    public MediaPlayerType getType() {
        return PLAYER_TYPE;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getURLPlayerDistant(com.univ.
     * utils.ContexteUniv, com.univ.objetspartages.om.Media)
     */
    @Override
    public String getURLPlayerDistant(ContexteUniv ctx, MediaBean media) {
        return super.genererDefaultURLplayerDistant(ctx, media);
    }
}
