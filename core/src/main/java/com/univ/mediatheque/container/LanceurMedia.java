package com.univ.mediatheque.container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.univ.objetspartages.bean.MediaBean;

public class LanceurMedia {

    private MediaBean media;

    private Map<AttributBaliseHTML, List<String>> attributBaliseHTML;

    private List<String> htmlAvantLanceur;

    private List<String> htmlApresLanceur;

    private List<String> jsAvantLanceur;

    private List<String> jsApresLanceur;

    public LanceurMedia(final MediaBean media) {
        this.attributBaliseHTML = new HashMap<>();
        this.htmlApresLanceur = new ArrayList<>();
        this.htmlAvantLanceur = new ArrayList<>();
        this.jsAvantLanceur = new ArrayList<>();
        this.jsApresLanceur = new ArrayList<>();
        this.media = media;
    }

    public void addAttributHTML(final AttributBaliseHTML evenement, final String action) {
        List<String> actionExistante = this.attributBaliseHTML.get(evenement);
        if (actionExistante == null) {
            actionExistante = new ArrayList<>();
            this.attributBaliseHTML.put(evenement, actionExistante);
        }
        // si l'action n'a pas déjà été ajoutée
        if (!actionExistante.contains(action)) {
            actionExistante.add(action);
        }
    }

    public void addAttributHTMLPremier(final AttributBaliseHTML evenement, final String action) {
        List<String> actionExistante = this.attributBaliseHTML.get(evenement);
        if (actionExistante == null) {
            actionExistante = new ArrayList<>();
            this.attributBaliseHTML.put(evenement, actionExistante);
        }
        // si l'action n'a pas déjà été ajoutée
        if (!actionExistante.contains(action)) {
            actionExistante.add(0, action);
        }
    }

    public void addHTMLAvantLanceur(final String htmlAvantLanceur) {
        this.htmlAvantLanceur.add(htmlAvantLanceur);
    }

    public void addHTMLApresLanceur(final String htmlApresLanceur) {
        this.htmlApresLanceur.add(htmlApresLanceur);
    }

    public void addJSAvantLanceur(final String jsAvantLanceur) {
        if (!this.jsAvantLanceur.contains(jsAvantLanceur)) {
            this.jsAvantLanceur.add(jsAvantLanceur);
        }
    }

    public void addJSApresLanceur(final String jsApresLanceur) {
        if (!this.jsApresLanceur.contains(jsApresLanceur)) {
            this.jsApresLanceur.add(jsApresLanceur);
        }
    }

    public MediaBean getMedia() {
        return media;
    }

    public void setMedia(final MediaBean media) {
        this.media = media;
    }

    public String getHTMLAvantLanceur() {
        return this.listStringToString(this.htmlAvantLanceur);
    }

    public String getHTMLApresLanceur() {
        return this.listStringToStringInversee(this.htmlApresLanceur);
    }

    public String getJSAvantLanceur() {
        return this.listStringToString(this.jsAvantLanceur);
    }

    public String getJSApresLanceur() {
        return this.listStringToString(this.jsApresLanceur);
    }

    public String getValeurAttributHTML(final AttributBaliseHTML evenement) {
        final List<String> attributHTML = this.attributBaliseHTML.get(evenement);
        if (attributHTML != null && attributHTML.size() > 0) {
            return this.listStringToString(attributHTML);
        } else {
            return null;
        }
    }

    public Set<AttributBaliseHTML> getAttributsBaliseHTML() {
        return this.attributBaliseHTML.keySet();
    }

    private String listStringToString(final List<String> liste) {
        final StringBuilder returnString = new StringBuilder();
        for (final String element : liste) {
            returnString.append(element);
        }
        return returnString.toString();
    }

    private String listStringToStringInversee(final List<String> liste) {
        final StringBuilder returnString = new StringBuilder();
        for (int i = liste.size() - 1; i >= 0; i--) {
            returnString.append(liste.get(i));
        }
        return returnString.toString();
    }
}
