package com.univ.mediatheque.container;

/**
 * Enumération des attributs de balise HTML.
 *
 * @author pierre.cosson
 *
 */
public enum AttributBaliseHTML {
    /**
     * Evenement JS ONCLICK.
     */
    ON_CLICK("onclick"),
    /**
     * Evenement JS ONMOUSEOVER.
     */
    ON_MOUVE_OVER("onmouseover"),
    /**
     * Classe CSS
     */
    CLASS("class"),
    /**
     * attribut Style CSS
     */
    STYLE("style"),
    /**
     * attribut REL
     */
    TITLE("title"),
    /**
     * attribut href
     */
    HREF("href");

    /**
     * Libellé de l'événement
     */
    private String libelle;

    /**
     * Constructeur
     *
     * @param libelle
     *            libellé de l'évenement javascript
     */
    private AttributBaliseHTML(final String libelle) {
        this.libelle = libelle;
    }

    /**
     * Récupérer le libellé de l'evenement javascript.
     *
     * @return le libellé de l'événement directement insérable dans de l'HTML.
     */
    public String getLibelle() {
        return this.libelle;
    }
}
