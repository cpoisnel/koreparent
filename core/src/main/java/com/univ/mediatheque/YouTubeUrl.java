package com.univ.mediatheque;

public class YouTubeUrl implements SpecificUrl {

    private static String URL_SOURCE = "//www.youtube.com/embed/";

    private static String TYP_RESSOURCE = "video";

    @Override
    public String processUrl(String url) {
        // Url entrée http://www.youtube.com/watch?v=auRo72Yg5YE
        // Url sortie http://www.youtube.com/embed/auRo72Yg5YE
        String res = url;
        String codeVideo = "";
        if (url.contains("v=")) {
            codeVideo = url.substring(url.indexOf("v=") + 2, url.length());
            if (codeVideo.contains("&")) {
                codeVideo = codeVideo.substring(0, codeVideo.indexOf("&"));
            }
            res = URL_SOURCE + codeVideo;
        }
        return res;
    }

    @Override
    public String getTypeRessource() {
        return TYP_RESSOURCE;
    }

    @Override
    public String getUrlSource() {
        return URL_SOURCE;
    }
}
