package com.univ.mediatheque.playlist.visionneuse.pagination;

public enum PositionMessagePagination {
    /**
     * Afficher le message de pagination en haut de la page playlist
     */
    HAUT("haut"),
    /**
     * Afficher le message de pagination en bas de la page playlist
     */
    BAS("bas"),
    /**
     * Afficher le message de pagination en haut et en bas de la page playlist
     */
    TOUT("tout");

    /**
     * Le libellé correspondant au mode d'affichage.
     */
    private String libelle = null;

    /**
     * Le libellé du mode d'affichage.
     *
     * @param libelle
     *            le libellé.
     */
    private PositionMessagePagination(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Récupérer un ENUM en fonction du libellé
     *
     * @param libelle
     *            Le libellé du {@link PositionMessagePagination} à récupérer.
     * @return {@link PositionMessagePagination} correspondant au libellé s'il existe sinon NULL.
     */
    public static PositionMessagePagination getPositionMessagePagination(String libelle) {
        int i;
        PositionMessagePagination[] values = PositionMessagePagination.values();
        for (i = 0; i < values.length; i++) {
            if (values[i].isSameLibelle(libelle)) {
                break;
            }
        }
        if (i >= values.length) {
            return null;
        } else {
            return values[i];
        }
    }

    /**
     * Tester un ENUM avec un libellé. C'est le libellé de l'ENUM qui est testé (le test est non case sensitive).
     *
     * @param libelle
     *            Le libellé de test.
     * @return TRUE si le libellé et l'ENUM sont similaire sinon FALSE.
     */
    public boolean isSameLibelle(String libelle) {
        return this.libelle.equalsIgnoreCase(libelle);
    }
}
