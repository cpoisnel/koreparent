package com.univ.mediatheque.playlist.visionneuse;

import java.io.Writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.mediatheque.playlist.MediaPlaylist;
import com.univ.utils.ContexteUniv;

public class VisionneusePlaylistDefaut extends AbstractVisionneusePlaylistDefaut {

    private static final Logger LOG = LoggerFactory.getLogger(VisionneusePlaylistDefaut.class);

    private static VisionneusePlaylistType TYPE = VisionneusePlaylistType.DEFAUT;

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClass()
     */
    @Override
    public String getLiClass() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClassDernier()
     */
    @Override
    public String getLiClassDernier() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClassImpair()
     */
    @Override
    public String getLiClassImpair() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClassPair()
     */
    @Override
    public String getLiClassPair() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClassPremier()
     */
    @Override
    public String getLiClassPremier() {
        return "visionneuse_defaut_li_premier";
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getClassTitre()
     */
    @Override
    public String getClassTitre() {
        return "visionneuse_defaut_titre";
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getUlClass()
     */
    @Override
    public String getUlClass() {
        return "visionneuse_defaut " + this.getName();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getUlId()
     */
    @Override
    public String getUlId() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.playlist.visionneuse.VisionneusePaylist#
     * genererVionneusePlaylist(com.univ.utils.ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.playlist.MediaPlaylist)
     */
    @Override
    public void genererVionneusePlaylist(ContexteUniv ctx, Writer out, MediaPlaylist playList) throws Exception {
        LOG.debug("Affichage par DEFAUT de la playlist");
        super.produireVisionneusePlaylistDefaut(out, playList);
        LOG.debug("Fin de l'affichage playlist");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.VisionneusePaylist#getType()
     */
    @Override
    public VisionneusePlaylistType getType() {
        return TYPE;
    }
}
