package com.univ.mediatheque.playlist.visionneuse;

import java.io.Writer;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.MessageHelper;
import com.univ.mediatheque.playlist.MediaPlaylist;
import com.univ.mediatheque.utils.HTMLUtils;
import com.univ.utils.ContexteUniv;

public class VisionneusePlaylistShowcase extends AbstractVisionneusePlaylistDefaut {

    /**
     * Le message affiché lors du survol de l'image de navigation précédente (il s'agit du code du libellé contenu dans les fichiers de langue)
     */
    protected String messageNavigationPrecedent;

    /**
     * Le message affiché lors du survol de l'image de navigation suivante (il s'agit du code du libellé contenu dans les fichiers de langue)
     */
    protected String messageNavigationSuivante;

    /**
     * URL de l'image utilisée dans la navigation entre les pages pour aller à la page précédente.
     */
    protected String imagePagePrecedente;

    /**
     * URL de l'image utilisée dans la navigation entre les pages pour aller à la page suivante.
     */
    protected String imagePageSuivante;

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.playlist.visionneuse.VisionneusePlaylist#
     * genererVionneusePlaylist(com.univ.utils.ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.playlist.MediaPlaylist)
     */
    @Override
    public void genererVionneusePlaylist(final ContexteUniv ctx, final Writer out, final MediaPlaylist playList) throws Exception {
        final String key = "showCase_" + String.valueOf(System.currentTimeMillis());
        String messageNext = MessageHelper.getCoreMessage(this.messageNavigationSuivante);
        if (StringUtils.isNotEmpty(messageNext)) {
            messageNext = StringEscapeUtils.escapeHtml4(messageNext);
        } else {
            messageNext = "";
        }
        String messagePrev = MessageHelper.getCoreMessage(this.messageNavigationPrecedent);
        if (StringUtils.isNotEmpty(messagePrev)) {
            messagePrev = StringEscapeUtils.escapeHtml4(messagePrev);
        } else {
            messagePrev = "";
        }
        out.write("<div id=\"");
        out.write(key);
        out.write("\" class=\"showcase\">");
        out.write("<a href=\"#next\" class=\"controls next\" rel=\"next\" title=\"");
        out.write(messageNext);
        out.write("\"><img src=\"");
        out.write(this.imagePageSuivante);
        out.write("\" alt=\"");
        out.write(messageNext);
        out.write("\"/></a>");
        out.write("<a href=\"#previous\" class=\"controls previous\" rel=\"previous\" title=\"");
        out.write(messagePrev);
        out.write("\"><img src=\"");
        out.write(this.imagePagePrecedente);
        out.write("\" alt=\"");
        out.write(messagePrev);
        out.write("\"/></a>");
        super.produireVisionneusePlaylistDefaut(out, playList);
        out.write(HTMLUtils.HTML_SCRIPT_JS_OPEN);
        out.write(HTMLUtils.genererMediathequeLanceurJSAdd("new Showcase.Horizontal($$('#" + key + " ul li'), $$('#" + key + " a.controls'), {duration: 0.3})", 999));
        out.write(HTMLUtils.HTML_SCRIPT_JS_CLOSE);
        out.write("</div>");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClass()
     */
    @Override
    public String getLiClass() {
        return null;
    }

    @Override
    public String getLiClassDernier() {
        return null;
    }

    @Override
    public String getLiClassImpair() {
        return null;
    }

    @Override
    public String getLiClassPair() {
        return null;
    }

    @Override
    public String getLiClassPremier() {
        return null;
    }

    @Override
    public String getUlClass() {
        return null;
    }

    @Override
    public String getUlId() {
        return null;
    }

    @Override
    public VisionneusePlaylistType getType() {
        return null;
    }

    @Override
    public String getClassTitre() {
        return null;
    }

    public void setMessageNavigationPrecedent(final String messageNavigationPrecedent) {
        this.messageNavigationPrecedent = messageNavigationPrecedent;
    }

    public void setMessageNavigationSuivante(final String messageNavigationSuivante) {
        this.messageNavigationSuivante = messageNavigationSuivante;
    }

    public void setImagePagePrecedente(final String imagePagePrecedente) {
        this.imagePagePrecedente = imagePagePrecedente;
    }

    public void setImagePageSuivante(final String imagePageSuivante) {
        this.imagePageSuivante = imagePageSuivante;
    }
}
