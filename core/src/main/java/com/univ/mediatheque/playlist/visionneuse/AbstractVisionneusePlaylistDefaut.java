package com.univ.mediatheque.playlist.visionneuse;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.mediatheque.playlist.MediaPlaylist;
import com.univ.mediatheque.style.MediaStyle;
import com.univ.objetspartages.bean.MediaBean;

/**
 * Cette classe permet de produire l'affichage d'une liste de médias sous forme d'une liste UL - LI.
 *
 * @author pierre.cosson
 *
 */
public abstract class AbstractVisionneusePlaylistDefaut implements VisionneusePlaylist {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(AbstractVisionneusePlaylistDefaut.class);

    /**
     * Le nom de la visionneuse de média
     */
    protected String name;

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.VisionneusePaylist#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Définir le nom (identifiant) de la visionneuse de playlist.
     *
     * @param name
     *            Le nom
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Produire une liste UL LI de média.
     *
     * @param out
     *            Le flux de sortie
     * @throws IOException
     *             Erreur lors de l'écriture dans le flux de sortie.
     */
    public void produireVisionneusePlaylistDefaut(Writer out, MediaPlaylist playlist) throws IOException {
        List<MediaBean> medias = playlist.getMediaList();
        if (medias != null && medias.size() > 0) {
            StringBuilder stringBuider = new StringBuilder();
            // affichage légende
            if (StringUtils.isNotEmpty(playlist.getTitre())) {
                String classTitre = this.getClassTitre();
                stringBuider.append("<p");
                if (StringUtils.isNotEmpty(classTitre)) {
                    stringBuider.append(" class=\"");
                    stringBuider.append(classTitre);
                    stringBuider.append("\"");
                } else {
                    LOG.debug("Classe CSS du titre de la playlist VIDE");
                }
                stringBuider.append(">");
                stringBuider.append(playlist.getTitre());
                stringBuider.append("</p>");
            } else {
                LOG.debug("Titre de la playlist VIDE");
            }
            stringBuider.append("<ul ");
            String ulId = this.getUlId();
            if (StringUtils.isNotEmpty(ulId)) {
                stringBuider.append("id=\"").append(ulId).append("\" ");
            }
            String ulClass = this.getUlClass();
            if (StringUtils.isNotEmpty(ulClass)) {
                stringBuider.append("class=\"").append(ulClass).append("\" ");
            }
            stringBuider.append(">");
            String liClassPair = this.getLiClassPair();
            String liClassImpair = this.getLiClassImpair();
            String liClassPremier = this.getLiClassPremier();
            String liClassDernier = this.getLiClassDernier();
            String liClass = this.getLiClass();
            StringBuilder liClassLi;
            MediaBean media;
            // parcours des medias
            LOG.debug("Affichage des " + medias.size() + " dans une liste UL - LI");
            for (int i = 0; i < medias.size(); i++) {
                // initialisation
                liClassLi = new StringBuilder();
                media = medias.get(i);
                stringBuider.append("<li");
                this.addClassCSS(liClassLi, liClass);
                if (i == 0) {
                    this.addClassCSS(liClassLi, liClassPremier);
                } else if ((i + 1) == medias.size()) {
                    this.addClassCSS(liClassLi, liClassDernier);
                }
                if (i % 2 == 0) {
                    this.addClassCSS(liClassLi, liClassPair);
                } else {
                    this.addClassCSS(liClassLi, liClassImpair);
                }
                if (liClassLi.length() > 0) {
                    stringBuider.append(" class=\"");
                    stringBuider.append(liClassLi);
                    stringBuider.append("\"");
                }
                stringBuider.append(">");
                // affichage des medias
                stringBuider.append(this.getElementHTML(media, playlist));
                stringBuider.append("</li>");
            }
            stringBuider.append("</ul>");
            out.write(stringBuider.toString());
        } else {
            LOG.info("La liste de média contenus dans la playlist est vide");
        }
    }

    /**
     * Construire le contenu HTML présentant un MEDIA.
     *
     * @param media
     *            Le média à présenter.
     * @param playlist
     *            La playlist dont ce média est issu.
     * @return La représentation HTML du média.
     */
    protected String getElementHTML(MediaBean media, MediaPlaylist playlist) {
        MediaStyle style = playlist.getMediaStyle(media);
        if (style != null) {
            return style.getContenuHTML(media);
        } else {
            LOG.info("Impossible de trouver un style de média pour le média ID :" + media.getId());
            return "";
        }
    }

    /**
     * Ajouter une classe CSS à l'ensemble des autres classes. Le but de cette finction est d'ajouter les classes CSS en les séparants par des esapces.
     *
     * @param container
     *            Le conteneur de classes CSS.
     * @param classCss
     *            La classe à ajouter. La fonction va vérifier que cette derniére est valide (nn NULL et taille supérieure à 0)
     */
    private void addClassCSS(StringBuilder container, String classCss) {
        if (classCss != null && classCss.length() > 0) {
            if (container.length() > 0) {
                container.append(" ");
            }
            container.append(classCss);
        }
    }

    /**
     * Récupérer l'identifiant HTML du UL.
     *
     * @return l'identifiant HTML.
     */
    public abstract String getUlId();

    /**
     * Récupérer la classe CSS du UL.
     *
     * @return la classe CSS.
     */
    public abstract String getUlClass();

    /**
     * Récupérer la classe CSS à appliquer à tous les LI de la liste.
     *
     * @return La classe CSS.
     */
    public abstract String getLiClass();

    /**
     * Récupérer la classe CSS à appliquer au premier LI de la liste.
     *
     * @return La classe CSS.
     */
    public abstract String getLiClassPremier();

    /**
     * Récupérer la classe CSS à appliquer au dernier LI de la liste.
     *
     * @return La classe CSS.
     */
    public abstract String getLiClassDernier();

    /**
     * Récupérer la classe CSS à appliquer lorsque le LI est sur PAIR.
     *
     * @return La classe CSS.
     */
    public abstract String getLiClassPair();

    /**
     * Récupérer la classe CSS à appliquer lorsque le LI est sur IMPAIR.
     *
     * @return La classe CSS.
     */
    public abstract String getLiClassImpair();

    public abstract String getClassTitre();
}
