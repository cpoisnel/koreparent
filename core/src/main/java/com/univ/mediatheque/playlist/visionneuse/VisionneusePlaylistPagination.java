package com.univ.mediatheque.playlist.visionneuse;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.MessageHelper;
import com.univ.mediatheque.playlist.MediaPlaylist;
import com.univ.mediatheque.playlist.visionneuse.pagination.PositionMessagePagination;
import com.univ.mediatheque.playlist.visionneuse.pagination.VisionneusePlaylistPaginationContexte;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.utils.ContexteUniv;

public class VisionneusePlaylistPagination extends AbstractVisionneusePlaylistDefaut {

    private static final Logger LOG = LoggerFactory.getLogger(VisionneusePlaylistPagination.class);

    private static final String MESSAGE_PAGINATION_NUM_PAGE_COURANTE_ESCAPED = "\\[PG_COURANTE\\]";

    private static final String MESSAGE_PAGINATION_NB_PAGE_TOTAL_ESCAPED = "\\[NB_PG_TOTAL\\]";

    public static final VisionneusePlaylistType TYPE = VisionneusePlaylistType.PAGINATION;

    /***************************
     * PARTIE CSS ET HTML
     ***************************/
    protected static String HTML_ID_VISIONNEUSE_PAGINATION = "visionneuse_pagination";

    protected static String CSS_CLASS_VISIONNEUSE_PAGINATION = "visionneuse_pagination";

    protected static String CSS_CLASS_PAGINATION_CONTENU = "pagination_contenu";

    protected static String CSS_CLASS_PAGINATION_VIDE = "pagination_vide";

    protected static String CSS_CLASS_PAGINATION_AUCUNE = "pagination_aucune";

    protected static String CSS_CLASS_PAGINATION_GAUCHE = "pagination_gauche";

    protected static String CSS_CLASS_PAGINATION_PRECEDENT = "pagination_precedent";

    protected static String CSS_CLASS_PAGINATION_PAGE_PRECEDENTE = "pagination_gauche pagination_precedent";

    protected static String CSS_CLASS_PAGINATION_DROITE = "pagination_droite";

    protected static String CSS_CLASS_PAGINATION_SUIVANT = "pagination_suivant";

    protected static String CSS_CLASS_PAGINATION_PAGE_SUIVANTE = "pagination_droite pagination_suivant";

    protected static String CSS_CLASS_PAGINATION_PAIR = "pagination_pair";

    protected static String CSS_CLASS_PAGINATION_IMPAIR = "pagination_impair";

    /**
     * Elément à ajouter dans la chaine de caratéres de pagination pour être remplacé par le numéro de la page courante.
     */
    public String MESSAGE_PAGINATION_NUM_PAGE_COURANTE = "[PG_COURANTE]";

    /**
     * Elément à ajouter dans la chaine de caratéres de pagination pour être remplacé par le nombre de page.
     */
    public String MESSAGE_PAGINATION_NB_PAGE_TOTAL = "[NB_PG_TOTAL]";

    /**
     * URL de l'image utilisée dans la navigation entre les pages pour aller à la page précédente.
     */
    protected String imagePagePrecedente;

    /**
     * URL de l'image utilisée dans la navigation entre les pages pour aller à la page suivante.
     */
    protected String imagePageSuivante;

    /**
     * URL de l'image utilisée dans la navigation entre les pages pour indiquer qu'il n'est plus possible de faire défiler les pages (cas de la premiére page et de la derniére
     * page).
     */
    protected String imagePageSuivanteAucune;

    /**
     * URL de l'image utilisée dans la navigation entre les pages pour indiquer qu'il n'est plus possible de faire défiler les pages (cas de la premiére page et de la derniére
     * page).
     */
    protected String imagePagePrecedenteAucune;

    /**
     * Le nombre de lignes souhaitées à l'afficahage d'une page.
     */
    protected int nombreLignes;

    /**
     * Le nombre de colonnes souhaitées à l'afficahage d'une page.
     */
    protected int nombreColonnes;

    /**
     * Le message présentant la pagination (il s'agit du code du libellé contenu dans les fichiers de langue)
     */
    protected PositionMessagePagination positionPagination;

    /**
     * Le message indiquant le nombre de page totales ainsi que la page courante (il s'agit du code du libellé contenu dans les fichiers de langue)
     */
    protected String messagePagination;

    /**
     * Le message affiché lors du survol de l'image de navigation précédente (il s'agit du code du libellé contenu dans les fichiers de langue)
     */
    protected String messageNavigationPrecedent;

    /**
     * Le message affiché lors du survol de l'image de navigation suivante (il s'agit du code du libellé contenu dans les fichiers de langue)
     */
    protected String messageNavigationSuivante;

    /**
     * Le message affiché lors du survol de l'image indiquant qu'aucune navigation n'est possible (il s'agit du code du libellé contenu dans les fichiers de langue)
     */
    protected String messageNavigationAucune;

    /**
     * Constructeur
     */
    public VisionneusePlaylistPagination() {
        this.imagePagePrecedente = "";
        this.imagePageSuivante = "";
        this.imagePagePrecedenteAucune = "";
        this.imagePageSuivanteAucune = "";
        this.nombreColonnes = 0;
        this.nombreLignes = 0;
        this.messagePagination = "";
        this.messageNavigationPrecedent = "";
        this.messageNavigationSuivante = "";
        this.messageNavigationAucune = "";
        this.positionPagination = PositionMessagePagination.BAS;
    }

    /**
     * Inserer la colonne de navigation permettant de faire défiler les pages d'éléments d'une visionneuse. Il s'agit de la colonne gauche qui permet d'afficher le bouton
     * permettant de faire defiler les pages précédentes. Dans le cas de la premiére page, on affiche un bouton ne permettant pas de faire défiler la page précédente page
     * précédente.Cette fonction accéde aux fichiers de langue de récupérer le message.
     *
     * @param context
     *            Le contexte de la visionneuse pagination (état courant de la construction du code HTML)
     * @param numPage
     *            Le numéro de la page courante
     *
     * @throws IOException
     *             Erreur durant l'écriture dans le flux de sortie.
     */
    private void paginationGenererNavPrecedent(VisionneusePlaylistPaginationContexte context, int numPage) throws IOException {
        Writer out = context.getOut();
        if (numPage != 0) {
            String idPagePrecedente = context.getKey() + (numPage - 1);
            String message = MessageHelper.getCoreMessage(this.messageNavigationPrecedent);
            if (StringUtils.isNotEmpty(message)) {
                message = StringEscapeUtils.escapeHtml4(message);
            } else {
                message = "";
            }
            // on affiche le lien pour afficher la page
            // précédente (car il y a une page précédente)
            out.write("<div class=\"");
            out.write(CSS_CLASS_PAGINATION_PAGE_PRECEDENTE);
            out.write("\"><a href=\"#ANC_");
            out.write(idPagePrecedente);
            out.write("\" title=\"");
            out.write(message);
            out.write("\"><img src=\"");
            out.write(this.imagePagePrecedente);
            out.write("\" alt=\"");
            out.write(message);
            out.write("\"/></a></div>");
        } else {
            genererPaginationNavPrecedenteAucune(context, CSS_CLASS_PAGINATION_GAUCHE);
        }
    }

    /**
     * Inserer la colonne de navigation permettant de faire défiler les pages d'éléments d'une visionneuse. Il s'agit de la colonne droite qui permet d'afficher le bouton
     * permettant de faire defiler les pages suivante. Dans le cas de la derniére page, on affiche un bouton ne permettant pas de faire le défilement vers la page suivante.Cette
     * fonction accéde aux fichiers de langue de récupérer le message.
     *
     * @param context
     *            Le contexte de la visionneuse pagination (état courant de la construction du code HTML)
     * @param numPage
     *            Le numéro de la page courante
     *
     * @throws IOException
     *             Erreur durant l'écriture dans le flux de sortie.
     */
    private void paginationGenererNavSuivant(VisionneusePlaylistPaginationContexte context, int numPage) throws IOException {
        Writer out = context.getOut();
        if (numPage < (context.getNombrePage() - 1)) {
            String idPageSuivante = context.getKey() + (numPage + 1);
            String message = MessageHelper.getCoreMessage(this.messageNavigationSuivante);
            if (StringUtils.isNotEmpty(message)) {
                message = StringEscapeUtils.escapeHtml4(message);
            } else {
                message = "";
            }
            // on affiche le lien pour afficher la page
            // précédente (car il y a une page précédente)
            out.write("<div class=\"");
            out.write(CSS_CLASS_PAGINATION_PAGE_SUIVANTE);
            // ajout du lien vers l'ancre de la page => accéssibilité
            out.write("\"><a href=\"#ANC_");
            out.write(idPageSuivante);
            out.write("\" title=\"");
            out.write(message);
            out.write("\"><img src=\"");
            out.write(this.imagePageSuivante);
            out.write("\" alt=\"");
            out.write(message);
            out.write("\"/></a></div>");
        } else {
            genererPaginationNavSuivanteAucune(context, CSS_CLASS_PAGINATION_DROITE);
        }
    }

    /**
     * Generer le code HTML correspondant à aucune pagination. l'image qui indique qu'il n'y a aucune pagination n'est affichée que si une URL est renseignée.Cette fonction accéde
     * aux fichiers de langue de récupérer le message.
     *
     * @param context
     *            Le contexte de la visionneuse pagination (état courant de la construction du code HTML)
     *
     * @throws IOException
     *             Erreur durant l'écriture dans le flux de sortie.
     */
    private void genererPaginationNavPrecedenteAucune(VisionneusePlaylistPaginationContexte context, String complementClass) throws IOException {
        // le lien pour aller sur la page précédente est
        // désactivé
        Writer out = context.getOut();
        out.write("<div class=\"");
        out.write(CSS_CLASS_PAGINATION_AUCUNE);
        out.write(" ");
        out.write(complementClass);
        out.write("\">");
        if (StringUtils.isNotEmpty(this.imagePagePrecedenteAucune)) {
            String message = MessageHelper.getCoreMessage(this.messageNavigationAucune);
            if (StringUtils.isNotEmpty(message)) {
                message = StringEscapeUtils.escapeHtml4(message);
            } else {
                message = "";
            }
            out.write("<img src=\"");
            out.write(this.imagePagePrecedenteAucune);
            out.write("\" alt=\"");
            out.write(message);
            out.write("\"/>");
        }
        out.write("</div>");
    }

    /**
     * Generer le code HTML correspondant à aucune pagination. l'image qui indique qu'il n'y a aucune pagination n'est affichée que si une URL est renseignée.Cette fonction accéde
     * aux fichiers de langue de récupérer le message.
     *
     * @param context
     *            Le contexte de la visionneuse pagination (état courant de la construction du code HTML)
     *
     * @throws IOException
     *             Erreur durant l'écriture dans le flux de sortie.
     */
    private void genererPaginationNavSuivanteAucune(VisionneusePlaylistPaginationContexte context, String complementClass) throws IOException {
        // le lien pour aller sur la page précédente est
        // désactivé
        Writer out = context.getOut();
        out.write("<div class=\"");
        out.write(CSS_CLASS_PAGINATION_AUCUNE);
        out.write(" ");
        out.write(complementClass);
        out.write("\">");
        if (StringUtils.isNotEmpty(this.imagePageSuivanteAucune)) {
            String message = MessageHelper.getCoreMessage(this.messageNavigationAucune);
            if (StringUtils.isNotEmpty(message)) {
                message = StringEscapeUtils.escapeHtml4(message);
            } else {
                message = "";
            }
            out.write("<img src=\"");
            out.write(this.imagePageSuivanteAucune);
            out.write("\" alt=\"");
            out.write(message);
            out.write("\"/>");
        }
        out.write("</div>");
    }

    /**
     * Ecriture de la balise TFOOT d'un tableau HTML.
     *
     * @param context
     *            Le contexte de la visionneuse pagination (état courant de la construction du code HTML)
     * @param numPage
     *            Le numéro de la page courante.
     *
     * @throws IOException
     *             Erreur durant l'écriture dans le flux de sortie.
     */
    private void genererTFoot(VisionneusePlaylistPaginationContexte context, int numPage) throws IOException {
        Writer out = context.getOut();
        out.write("<div class=\"footer\">");
        this.pagniationGenererContenuTheadTfoot(context, PositionMessagePagination.BAS, numPage);
        out.write("</div>");
    }

    /**
     * Ecriture de la balise THEAD d'un tableau HTML.
     *
     * @param context
     *            Le contexte de la visionneuse pagination (état courant de la construction du code HTML)
     * @param numPage
     *            Le numéro de la page courante.
     *
     * @throws IOException
     *             Erreur durant l'écriture dans le flux de sortie.
     */
    private void genererTHead(VisionneusePlaylistPaginationContexte context, int numPage) throws IOException {
        Writer out = context.getOut();
        out.write("<div class=\"head\">");
        this.pagniationGenererContenuTheadTfoot(context, PositionMessagePagination.HAUT, numPage);
        out.write("</div>");
    }

    /**
     * Générer le code HTML du message de pagination.
     *
     * @param context
     *            Le contexte d'exécution.
     * @param position
     *            la position que représente la fonction qui appelle
     * @param numPage
     *            le numéro de la page courante
     *
     * @throws IOException
     *             Erreur lors de l'écriture
     */
    private void pagniationGenererContenuTheadTfoot(VisionneusePlaylistPaginationContexte context, PositionMessagePagination position, int numPage) throws IOException {
        Writer out = context.getOut();
        // si on doit afficher le message de la pagination : Position courante
        // ou TOUT ET OBLIGATOIREMENT si la navigation est affiché (plus de une
        // page)
        if ((position.equals(this.positionPagination) || this.positionPagination.equals(PositionMessagePagination.TOUT)) && context.afficherBoutonsNavigation()) {
            out.write(this.genererMessagePagination(context, numPage));
        }
    }

    /**
     * Génrer le message de pagination. Cette fonction remplace tous les [PG_COURANTE] et [NB_PG_TOTAL] par la valeur. Cette fonction accéde aux fichiers de langue de récupérer le
     * message.
     *
     * @param context
     *            Le contexte de la visionneuse pagination pour retrouver les informations.
     * @param pageCourante
     *            Le numéro de la page courante.
     * @return La chaine de caratére initialisé avec les valeurs de nombre de pages totales et la page courante.
     */
    private String genererMessagePagination(VisionneusePlaylistPaginationContexte context, int pageCourante) {
        String nombrePageTotal = String.valueOf(context.getNombrePage());
        String numPageCourante = String.valueOf(pageCourante + 1);
        String message = MessageHelper.getCoreMessage(this.messagePagination);
        if (StringUtils.isNotEmpty(message)) {
            return message.replaceAll(MESSAGE_PAGINATION_NB_PAGE_TOTAL_ESCAPED, nombrePageTotal).replaceAll(MESSAGE_PAGINATION_NUM_PAGE_COURANTE_ESCAPED, numPageCourante);
        } else {
            return "";
        }
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.playlist.visionneuse.VisionneusePlaylist#
     * genererVionneusePlaylist(com.univ.utils.ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.playlist.MediaPlaylist)
     */
    @Override
    public void genererVionneusePlaylist(ContexteUniv ctx, Writer out, MediaPlaylist playList) throws Exception {
        LOG.debug("Debut de la generation du code de la visionneuse de playlist Pagination");
        List<MediaBean> medias = playList.getMediaList();
        if (medias != null && medias.size() > 0) {
            VisionneusePlaylistPaginationContexte context = new VisionneusePlaylistPaginationContexte(ctx, out, medias.size(), this.nombreLignes, this.nombreColonnes);
            MediaBean media;
            // Test pour savoir si on veut bien afficher sous forme de page
            if (context.getNombrePage() > 0) {
                LOG.debug("Nombre de pages à afficher : " + context.getNombrePage());
                LOG.debug("Nombre de lignes par page : " + context.getNombreLigne());
                LOG.debug("Nombre de colonnes par page : " + context.getNombreColonne());
                int numElement = 0;
                // ajout des pages (la visionneuse affiche autant de pages que
                // necesaire cependant une seul est visible, les autres sont
                // hors du flux via un display : non CSS)
                for (int numPage = 0; numPage < context.getNombrePage(); numPage++) {
                    LOG.debug("Construction de la page : " + numPage);
                    // ajout d'une ancre => accéssibilité
                    out.write("<a name=\"ANC_");
                    out.write(context.getKey());
                    out.write(String.valueOf(numPage));
                    out.write("\"></a>");
                    // DIV de page
                    out.write("<div id=\"");
                    out.write(HTML_ID_VISIONNEUSE_PAGINATION);
                    out.write(context.getKey());
                    out.write(String.valueOf(numPage));
                    out.write("\" class=\"");
                    out.write(CSS_CLASS_VISIONNEUSE_PAGINATION);
                    if(numPage == 0) {
                        out.write(" " + CSS_CLASS_VISIONNEUSE_PAGINATION + "--show");
                    }
                    out.write("\">");
                    // affichage légende
                    out.write("<div class=\"caption\">");
                    out.write(playList.getTitre());
                    out.write("</div>");
                    // Ajout des THEAD et TFOOT
                    this.genererTHead(context, numPage);
                    // Accessibilité : ancre
                    if (context.afficherBoutonsNavigation()) {
                        // si on est sur la premiére ligne on affiche le
                        // message qui va permettre de changer de page (page
                        // précédente) ce message d'affiche sur toute la
                        // colonne (d'où le rowspan)
                        paginationGenererNavPrecedent(context, numPage);
                    }
                    // la page se trouve dans une div qui contient un tableau
                    // HTML
                    // ID du tableau : visionneuse_pagination + KEY + numPage
                    out.write("<div class=\"");
                    out.write(CSS_CLASS_PAGINATION_CONTENU);
                    out.write("\" summary=\"\">");
                    for (int numCol = 0; numCol < context.getNombreColonne(); numCol++) {
                        if (numCol % 2 == 0) {
                            // colonne pair
                            out.write("<col class=\"");
                            out.write(CSS_CLASS_PAGINATION_PAIR);
                            out.write("\" />");
                        } else {
                            out.write("<col class=\"");
                            out.write(CSS_CLASS_PAGINATION_IMPAIR);
                            out.write("\" />");
                        }
                    }
                    // ajout du contenu
                    out.write("<div>");
                    for (int numRow = 0; numRow < context.getNombreLigne(); numRow++) {
                        if (numRow % 2 == 0) {
                            // ligne paire
                            out.write("<div class=\"");
                            out.write(CSS_CLASS_PAGINATION_PAIR);
                            out.write("\">");
                        } else {
                            out.write("<div>");
                        }
                        for (int numCol = 0; numCol < context.getNombreColonne(); numCol++) {
                            if (numElement < context.getNombreElement()) {
                                media = medias.get(numElement);
                                out.write(super.getElementHTML(media, playList));
                                numElement++;
                            } else {
                                // on affiche du vide (il n'y a plus de photos à
                                // afficher)
                                out.write("&nbsp;");
                            }
                        }
                        out.write("</div>");
                    }
                    out.write("</div>");
                    out.write("</div>");
                    if (context.afficherBoutonsNavigation()) {
                        // affichage du lien pour aller sur la page suivante
                        // de la même maniére que pour aller sur la page
                        // précédente.
                        paginationGenererNavSuivant(context, numPage);
                    }
                    this.genererTFoot(context, numPage);
                    out.write("</div>");
                }
            } else {
                LOG.debug("Aucune information sur le nombre de colonnes et de lignes à afficher => affichage en UL - LI");
                // il n'y a pas de page à definir alors on affiche tous les
                // éléments dans une liste UL LI
                super.produireVisionneusePlaylistDefaut(out, playList);
            }
        } else {
            LOG.info("La liste d emédias à afficher est vide");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.VisionneusePaylist#getType()
     */
    @Override
    public VisionneusePlaylistType getType() {
        return TYPE;
    }

    /**
     * URL de l'image servant à la navigation entre les pages de visionneuse : navigation vers la page précédente (exemple petite fléche à gauche).
     *
     * @param imagePagePrecedente
     *            URL de l'image.
     */
    public void setImagePagePrecedente(String imagePagePrecedente) {
        this.imagePagePrecedente = imagePagePrecedente;
    }

    /**
     * URL de l'image servant à la navigation entre les pages de visionneuse : navigation vers la page suivante (exemple petite fléche à droite).
     *
     * @param imagePageSuivante
     *            URL de l'image.
     */
    public void setImagePageSuivante(String imagePageSuivante) {
        this.imagePageSuivante = imagePageSuivante;
    }

    /**
     * URL de l'image servant à la navigation entre les pages de visionneuse : aucune navigation possible, aucune page suivante ou précédente (exemple petite rectangle).
     *
     * @param imagePageAucune
     *            URL de l'image.
     */
    public void setImagePageAucune(String imagePageAucune) {
        this.imagePagePrecedenteAucune = imagePageAucune;
    }

    /**
     * Définir le nombre de lignes à afficher dans une page.
     *
     * @param nombreLignes
     *            Nombre de lignes
     */
    public void setNombreLignes(int nombreLignes) {
        this.nombreLignes = nombreLignes;
    }

    /**
     * Définir le nombre de ligne à afficher dans une page.
     *
     * @param nombreColonnes
     *            Nombre de colonne.
     */
    public void setNombreColonnes(int nombreColonnes) {
        this.nombreColonnes = nombreColonnes;
    }

    /**
     * Définir le message qui indique la pagination de la page. Par exemple : "Page 1 sur 2".<br/>
     * Afin de rendre dynamique ce message il faut qu'il contienne des clés. Les valeurs de ces clés sont définies dans des variable de classe :
     * <ul>
     * <li>MESSAGE_PAGINATION_NUM_PAGE_COURANTE</li>
     * <li>MESSAGE_PAGINATION_NB_PAGE_TOTAL</li>
     * </ul>
     * <br/>
     * <strong>Remarque : </strong> il doit s'agir d'une clé d'un fichier de LANGUE.
     *
     * @param messagePagination
     *            La clé qui correspond au message de pagination.
     */
    public void setMessagePagination(String messagePagination) {
        this.messagePagination = messagePagination;
    }

    /**
     * Définir la position du message de pagination.
     *
     * @param positionPagination
     *            Position.
     */
    public void setPositionPagination(PositionMessagePagination positionPagination) {
        this.positionPagination = positionPagination;
    }

    /**
     * Affecter la position du message de pagination dans la visionneuse de playlist.
     *
     * @param positionPaginationLibelle
     *            Le libellé de la position.
     */
    public void setPositionPaginationParLibelle(String positionPaginationLibelle) {
        PositionMessagePagination positionPagination = PositionMessagePagination.getPositionMessagePagination(positionPaginationLibelle);
        if (positionPagination != null) {
            this.setPositionPagination(positionPagination);
        } else {
            LOG.info("Aucun ENUM PositionMessagePagination ne correspond au libellé : " + positionPaginationLibelle);
        }
    }

    /**
     * Définir le message utilisé pour le TITLE de la balise A et le ALT de l'image de la navigation vers une page précédente.<br/>
     * <strong>Remarque : </strong> il doit s'agir d'une clé d'un fichier de LANGUE.
     *
     * @param messageNavigationPrecedent
     *            La clé qui correspond au message.
     */
    public void setMessageNavigationPrecedent(String messageNavigationPrecedent) {
        this.messageNavigationPrecedent = messageNavigationPrecedent;
    }

    /**
     * Définir le message utilisé pour le TITLE de la balise A et le ALT de l'image de la navigation vers une page suivante.<br/>
     * <strong>Remarque : </strong> il doit s'agir d'une clé d'un fichier de LANGUE.
     *
     * @param messageNavigationSuivante
     *            La clé qui correspond au message.
     */
    public void setMessageNavigationSuivante(String messageNavigationSuivante) {
        this.messageNavigationSuivante = messageNavigationSuivante;
    }

    /**
     * Définir le message utilisé pour le ALT de l'image qui indique qu'aucune navigation n'est possible.<br/>
     * <strong>Remarque : </strong> il doit s'agir d'une clé d'un fichier de LANGUE.
     *
     * @param messageNavigationAucune
     *            La clé qui correspond au message.
     */
    public void setMessageNavigationAucune(String messageNavigationAucune) {
        this.messageNavigationAucune = messageNavigationAucune;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClass()
     */
    @Override
    public String getLiClass() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClassDernier()
     */
    @Override
    public String getLiClassDernier() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClassImpair()
     */
    @Override
    public String getLiClassImpair() {
        return CSS_CLASS_PAGINATION_IMPAIR;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClassPair()
     */
    @Override
    public String getLiClassPair() {
        return CSS_CLASS_PAGINATION_PAIR;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getLiClassPremier()
     */
    @Override
    public String getLiClassPremier() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getUlClass()
     */
    @Override
    public String getUlClass() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut
     * #getUlId()
     */
    @Override
    public String getUlId() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.univ.mediatheque.playlist.visionneuse.AbstractVisionneusePlaylistDefaut#getClassTitre()
     */
    @Override
    public String getClassTitre() {
        return null;
    }

    public String getImagePageSuivanteAucune() {
        return imagePageSuivanteAucune;
    }

    public void setImagePageSuivanteAucune(String imagePageSuivanteAucune) {
        this.imagePageSuivanteAucune = imagePageSuivanteAucune;
    }

    public String getImagePagePrecedenteAucune() {
        return imagePagePrecedenteAucune;
    }

    public void setImagePagePrecedenteAucune(String imagePagePrecedenteAucune) {
        this.imagePagePrecedenteAucune = imagePagePrecedenteAucune;
    }
}
