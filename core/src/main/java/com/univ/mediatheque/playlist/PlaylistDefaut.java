package com.univ.mediatheque.playlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.mediatheque.Mediatheque;
import com.univ.mediatheque.SpecificUrl;
import com.univ.mediatheque.player.MediaPlayer;
import com.univ.mediatheque.player.visionneuse.VisionneusePlayer;
import com.univ.mediatheque.style.MediaStyle;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;

public class PlaylistDefaut implements MediaPlaylist {

    private static final Logger LOG = LoggerFactory.getLogger(PlaylistDefaut.class);

    private ContexteUniv ctx;

    private VisionneusePlayer visionneusePlayer;

    private MediaStyle mediaStyle;

    /**
     * Player utilisé qaund on ne touve pas un player pour un MEDIA dans la liste de player
     */
    private MediaPlayer playerDefaut;

    private Map<String, MediaPlayer> players;

    private List<MediaBean> mediaList;

    private String titre;

    private String name;

    /**
     * Définir les players qui peuvent être utilisée
     *
     * @param players
     */
    public void setPlayers(List<MediaPlayer> players) {
        this.players = new HashMap<>();
        for (MediaPlayer player : players) {
            LOG.debug("Ajout du player : " + player.getType());
            this.players.put(player.getType().getMediaType(), player);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.MediaPlaylist#setMediaList(java.util.List)
     */
    @Override
    public void setMediaList(List<MediaBean> mediaList) {
        this.mediaList = new ArrayList<>(mediaList);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.playlist.MediaPlaylist#getMediaList()
     */
    @Override
    public List<MediaBean> getMediaList() {
        return this.mediaList;
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.playlist.MediaPlaylist#getMediaPlayer(com.univ.
     * objetspartages.om.Media)
     */
    @Override
    public MediaPlayer getMediaPlayer(MediaBean media) {
        String urlMedia = MediaUtils.getUrlAbsolue(media);
        MediaPlayer player = null;
        LOG.debug("Demande de player pour le media ID : " + media.getId());
        Boolean isVideoExterneSupport = Boolean.FALSE;
        //Si c'est une vidéo, on cherche les vidéos externe supporté.
        //Si c'est une vidéo externe supporté on prend le player flash et non le player vidéo
        if ("video".equalsIgnoreCase(media.getTypeRessource())) {
            Map<String, SpecificUrl> supportVideosExternes = Mediatheque.getInstance().getTypesUrl();
            for (SpecificUrl specUrl : supportVideosExternes.values()) {
                if (urlMedia.startsWith(specUrl.getUrlSource())) {
                    isVideoExterneSupport = Boolean.TRUE;
                    player = this.players.get("flash");
                    LOG.debug("Player FLASH car média de type vidéo externe");
                    break;
                }
            }
        }
        //Dans les autres cas, on cherche le bon player dans la liste
        if (!isVideoExterneSupport) {
            player = this.players.get(media.getTypeRessource());
        }
        if (player == null) {
            LOG.info("Pas de player pour le media car il est de type '" + media.getTypeMedia() + "'");
            return this.playerDefaut;
        } else {
            return player;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.playlist.MediaPlaylist#getMediaStyle(com.univ.
     * objetspartages.om.Media)
     */
    @Override
    public MediaStyle getMediaStyle(MediaBean media) {
        return this.mediaStyle;
    }

    /**
     * Définir le style de média.
     *
     * @param mediaStyle
     */
    public void setMediaStyle(MediaStyle mediaStyle) {
        this.mediaStyle = mediaStyle;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.playlist.MediaPlaylist#getName()
     */
    @Override
    public String getName() {
        return this.name;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.playlist.MediaPlaylist#getTitre()
     */
    @Override
    public String getTitre() {
        return this.titre;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.MediaPlaylist#getVisionneusePlayer(com.
     * univ.objetspartages.om.Media)
     */
    @Override
    public VisionneusePlayer getVisionneusePlayer(MediaBean media) {
        return this.visionneusePlayer;
    }

    /**
     * @param visionneusePlayer
     */
    public void setVisionneusePlayer(VisionneusePlayer visionneusePlayer) {
        this.visionneusePlayer = visionneusePlayer;
    }

    @Override
    public void init() {
        this.visionneusePlayer.init();
        this.mediaStyle.init();
        this.mediaStyle.setCtx(this.ctx);
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.playlist.MediaPlaylist#setCtx(com.univ.utils.
     * ContexteUniv)
     */
    @Override
    public void setCtx(ContexteUniv ctx) {
        this.ctx = ctx;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.playlist.MediaPlaylist#setTitre(java.lang.String)
     */
    @Override
    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MediaPlayer getPlayerDefaut() {
        return playerDefaut;
    }

    public void setPlayerDefaut(MediaPlayer playerDefaut) {
        this.playerDefaut = playerDefaut;
    }
}
