package com.univ.mediatheque.playlist;

import java.util.List;

import com.univ.mediatheque.player.MediaPlayer;
import com.univ.mediatheque.player.visionneuse.VisionneusePlayer;
import com.univ.mediatheque.style.MediaStyle;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.utils.ContexteUniv;

public interface MediaPlaylist {

    /**
     * Récupérer le nom de la playlist.
     *
     * @return le nom de la playlist.
     */
    String getName();

    /**
     * Définir le contexte de l'application dans la playlist.
     *
     * @param ctx
     */
    void setCtx(ContexteUniv ctx);

    /**
     * Définir le titre de la playlist qui sera affichée en fonction mode d'affichage choisi.
     *
     * @param titre
     *            Le titre de la playlist.
     */
    void setTitre(String titre);

    /**
     * Récupérer le titre de la playlist.
     *
     * @return le titre de la playlist.
     */
    String getTitre();

    /**
     * Récupérer la liste de média associée à cette playlist.
     *
     * @return La liste de média.
     *
     */
    List<MediaBean> getMediaList();

    void setMediaList(List<MediaBean> medias);

    /**
     * Récupérer le style associé au média
     *
     * @param media
     *            Le média.
     * @return Le style du média.
     */
    MediaStyle getMediaStyle(MediaBean media);

    /**
     * Récupérer le player en fonction du média.
     *
     * @param media
     *            Le média à jouer.
     * @return le player capable de jouer le média.
     */
    MediaPlayer getMediaPlayer(MediaBean media);

    /**
     * Récupérer la visionneuse de player de média en fonction du média à afficher.
     *
     * @param media
     *            Le média qui sera affiché par le player
     * @return La visionneuse de player.
     */
    VisionneusePlayer getVisionneusePlayer(MediaBean media);

    /**
     * Initialiser la playlist.s
     */
    void init();
}
