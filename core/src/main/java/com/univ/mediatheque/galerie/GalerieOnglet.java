package com.univ.mediatheque.galerie;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.MessageHelper;
import com.univ.mediatheque.container.ClassificationMedia;
import com.univ.mediatheque.container.ClassifieurMedia;
import com.univ.mediatheque.player.MediaPlayer;
import com.univ.mediatheque.player.visionneuse.VisionneusePlayer;
import com.univ.mediatheque.playlist.MediaPlaylist;
import com.univ.mediatheque.playlist.visionneuse.VisionneusePlaylist;
import com.univ.mediatheque.style.MediaStyle;
import com.univ.mediatheque.utils.HTMLUtils;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.utils.ContexteUniv;

public class GalerieOnglet implements Galerie {

    protected static GalerieType TYPE = GalerieType.ONGLET;

    protected static String HTML_CSS_VISIONNEUSE = "galerie_onglet";

    protected static String HTML_CSS_UL_OGLETS = "galerie_onglets";

    protected static String HTML_CSS_PLAYER = "galerie_player";

    protected static String HTML_CSS_PLAYLIST = "galerie_playlist";

    protected static String HTML_CSS_TITRE = "galerie_titre";

    protected static String HTML_CSS_ONGLET_CONTENU = "galerie_onglet_contenu";

    protected static String HTML_CSS_ONGLET_CONTENU_AUTRE_DEBUT = "galerie_onglet_contenu_";

    protected static String HTML_CSS_ONGLET_DEBUT = "galerie_onglet_";

    protected static String JS_ONGLET_MANAGER_DEBUT = "ongletManager";

    private ClassifieurMedia classifieur;

    private String name;

    private String titre;

    private VisionneusePlaylist visionneusePlaylist;

    /**
     * @param visionneusePlaylist
     */
    @Override
    public void setVisionneusePlaylist(final VisionneusePlaylist visionneusePlaylist) {
        this.visionneusePlaylist = visionneusePlaylist;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.galerie.Galerie#setTitre(java.lang.String)
     */
    @Override
    public void setTitre(final String titre) {
        this.titre = titre;
    }

    /**
     * Définir le nom de la galerie (peut servir d'ID). N'est pas affiché.
     *
     * @param name
     *            Le nom de la galerie.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.galerie.Galerie#getName()
     */
    @Override
    public String getName() {
        return this.name;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.galerie.Galerie#getType()
     */
    @Override
    public GalerieType getType() {
        return TYPE;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.galerie.Galerie#afficherGalerieComplete(com.univ
     * .utils.ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.playlist.MediaPlaylist)
     */
    @Override
    public void afficherGalerieComplete(final ContexteUniv ctx, final Writer out, final MediaPlaylist mediaList) throws Exception {
        mediaList.setCtx(ctx);
        mediaList.init();
        // tri de la liste de média
        final Collection<ClassificationMedia> classifications = this.classifieur.classifierMedias(mediaList.getMediaList());
        // récupération de tous les onglets avec du contenus
        final List<ClassificationMedia> ongletsActifs = this.getOngletsActifs(classifications);
        if (ongletsActifs.size() > 0) {
            out.write("<div class=\"");
            out.write(HTML_CSS_VISIONNEUSE);
            out.write("\">");
            if (StringUtils.isNotEmpty(this.titre)) {
                out.write("<p class=\"");
                out.write(HTML_CSS_TITRE);
                out.write("\">");
                out.write(this.titre);
                out.write("</p>");
            }
            if (ongletsActifs.size() > 1) {
                final String key = String.valueOf(System.currentTimeMillis());
                // on génére l'affichage des onglets dans une liste UL LI
                out.write("<ul class=\"");
                out.write(HTML_CSS_UL_OGLETS);
                out.write("\">");
                for (int i = 0; i < ongletsActifs.size(); i++) {
                    this.afficherOnglet(out, ongletsActifs.get(i).getCode(), ongletsActifs.get(i).getIntitule(), i, key);
                }
                out.write("</ul>");
                final List<MediaBean> medias = mediaList.getMediaList();
                // on produits les pages de contenu
                int index = 0;
                for (final ClassificationMedia ongletCourant : ongletsActifs) {
                    medias.clear();
                    medias.addAll(ongletCourant.getMedias());
                    out.write("<div id=\"");
                    out.write(this.genererIdContenuOnglet(ongletCourant.getCode()));
                    out.write("\" class=\"");
                    out.write(HTML_CSS_ONGLET_CONTENU);
                    out.write(" ");
                    out.write(HTML_CSS_ONGLET_CONTENU_AUTRE_DEBUT);
                    out.write(String.valueOf(index));
                    if(index == 0) {
                        out.write(" " + HTML_CSS_ONGLET_CONTENU + "--show");
                    }
                    out.write("\">");
                    out.write("<h3>" + MessageHelper.getCoreMessage(ongletCourant.getIntitule()) + "</h3>");
                    this.initialiserPlayer(ctx, out, mediaList);
                    this.initialiserPlaylist(ctx, out, mediaList);
                    out.write("</div>");
                    mediaList.init();
                    index++;
                }
                // initialisation JAVASCRIPT
                out.write(HTMLUtils.HTML_SCRIPT_JS_OPEN);
                out.write("var ");
                out.write(JS_ONGLET_MANAGER_DEBUT);
                out.write(key);
                out.write(" = ");
                out.write(HTMLUtils.OBJET_JS_ONGLET_MANAGER);
                out.write(";\n");
                for (ClassificationMedia ongletsActif : ongletsActifs) {
                    // ajout des onglets et vues dans le manager d'onglet JS.
                    out.write(HTMLUtils.genererMediathequeLanceurJSAdd(JS_ONGLET_MANAGER_DEBUT + key + ".addNewView('" + this.genererIdVueOnglet(ongletsActif.getCode()) + "','" + this.genererIdContenuOnglet(ongletsActif.getCode()) + "','" + this.genererIdOnglet(ongletsActif.getCode()) + "')", 2));
                }
                // initialisation de la premiére vue
                out.write(HTMLUtils.genererMediathequeLanceurJSAdd(JS_ONGLET_MANAGER_DEBUT + key + ".initView('" + this.genererIdVueOnglet(ongletsActifs.get(0).getCode()) + "')", 999));
                out.write(HTMLUtils.HTML_SCRIPT_JS_CLOSE);
            } else if (ongletsActifs.size() == 1) {
                this.initialiserPlayer(ctx, out, mediaList);
                this.initialiserPlaylist(ctx, out, mediaList);
            }
            out.write("</div>");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.galerie.Galerie#afficherPlayer(com.univ.utils.
     * ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.playlist.MediaPlaylist)
     */
    @Override
    public void initialiserPlayer(final ContexteUniv ctx, final Writer out, final MediaPlaylist mediaList) throws Exception {
        final StringWriter strWriter = new StringWriter();
        final List<MediaBean> medias = mediaList.getMediaList();
        int maxHeight = -1;
        int maxWidth = -1;
        if (medias != null && medias.size() > 0) {
            VisionneusePlayer vPlayer;
            MediaStyle mStyle;
            MediaPlayer mPlayer;
            for (final MediaBean media : medias) {
                vPlayer = mediaList.getVisionneusePlayer(media);
                mStyle = mediaList.getMediaStyle(media);
                mPlayer = mediaList.getMediaPlayer(media);
                vPlayer.initialiserVisionneuse(ctx, strWriter, mPlayer);
                vPlayer.setLanceurMedia(ctx, media, mPlayer, mStyle);
                maxHeight = Math.max(maxHeight, vPlayer.getMaxHeight());
                maxWidth = Math.max(maxWidth, vPlayer.getMaxWidth());
            }
        }
        out.write("<div class=\"");
        out.write(HTML_CSS_PLAYER);
        if (maxHeight > -1 || maxWidth > -1) {
            out.write("\" style=\"");
            if (maxHeight > -1) {
                out.write("height:");
                out.write(String.valueOf(maxHeight));
                out.write("px;");
            }
            if (maxWidth > -1) {
                out.write("width:");
                out.write(String.valueOf(maxWidth));
                out.write("px;");
            }
        }
        out.write("\">");
        out.write(strWriter.toString());
        out.write("</div>");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.galerie.Galerie#afficherPlaylist(com.univ.utils.
     * ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.playlist.MediaPlaylist)
     */
    @Override
    public void initialiserPlaylist(final ContexteUniv ctx, final Writer out, final MediaPlaylist mediaList) throws Exception {
        out.write("<div class=\"");
        out.write(HTML_CSS_PLAYLIST);
        out.write("\">");
        this.visionneusePlaylist.genererVionneusePlaylist(ctx, out, mediaList);
        out.write("</div>");
    }

    /**
     * Récupération de tous les "onglets" qui contiennent des médias à afficher. Les onglet sont en faite des objet {@link ClassificationMedia} qui contiennent un grand nombre
     * d'informations sur l'affichage et le contenu de l'affichage.
     *
     * @param classifications
     *            La liste de toutes les {@link ClassificationMedia} produites par un {@link ClassifieurMedia}.
     *
     * @return La liste des {@link ClassificationMedia} qui ont des {@link com.univ.objetspartages.bean.MediaBean} à afficher.
     */
    private List<ClassificationMedia> getOngletsActifs(final Collection<ClassificationMedia> classifications) {
        final List<ClassificationMedia> classificationsReturn = new ArrayList<>();
        final Iterator<ClassificationMedia> classificationsIt = classifications.iterator();
        ClassificationMedia classification;
        while (classificationsIt.hasNext()) {
            classification = classificationsIt.next();
            if (!classification.isEmpty()) {
                classificationsReturn.add(classification);
            }
        }
        return classificationsReturn;
    }

    /**
     * Afficher sur le flux de sortie un onglet (pas le contenu) soit une balise LI.
     *
     * @param out
     *            Flux de sortie
     * @param code
     *            Le code unique de l'onglet
     * @param intitule
     *            Intitulé de l'onglet, visible à l'écran.
     * @param index
     *            La position de l'onglet par rapport à tous les autres (commence à 0)
     * @param key
     *            La clé permettant d'identifier le manager d'onglet JS.
     *
     * @throws IOException
     *             Erreur lors de l'ecriture sur le flux de sortie.
     */
    private void afficherOnglet(final Writer out, final String code, final String intitule, final int index, final String key) throws IOException {
        out.write("<li data-tabid=\"");
        out.write(this.genererIdContenuOnglet(code));
        out.write("\" class=\"");
        out.write(HTML_CSS_ONGLET_DEBUT);
        out.write(String.valueOf(index));
        out.write("\">");
        out.write(MessageHelper.getCoreMessage(intitule));
        out.write("</li>");
    }

    private String genererIdVueOnglet(final String codeOnglet) {
        return "vue_" + codeOnglet;
    }

    private String genererIdContenuOnglet(final String codeOnglet) {
        return "galerie_onglet_contenu_" + codeOnglet;
    }

    private String genererIdOnglet(final String codeOnglet) {
        return "galerie_onglet_" + codeOnglet;
    }

    public ClassifieurMedia getClassifieur() {
        return classifieur;
    }

    public void setClassifieur(final ClassifieurMedia classifieur) {
        this.classifieur = classifieur;
    }
}
