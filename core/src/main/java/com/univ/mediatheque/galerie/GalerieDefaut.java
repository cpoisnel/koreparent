package com.univ.mediatheque.galerie;

import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.univ.mediatheque.player.MediaPlayer;
import com.univ.mediatheque.player.visionneuse.VisionneusePlayer;
import com.univ.mediatheque.playlist.MediaPlaylist;
import com.univ.mediatheque.playlist.visionneuse.VisionneusePlaylist;
import com.univ.mediatheque.style.MediaStyle;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.utils.ContexteUniv;

public class GalerieDefaut implements Galerie {

    protected static GalerieType TYPE = GalerieType.STANDARD;

    protected static String HTML_CSS_VISIONNEUSE = "galerie_defaut";

    protected static String HTML_CSS_PLAYER = "galerie_player";

    protected static String HTML_CSS_PLAYLIST = "galerie_playlist ";

    protected static String HTML_CSS_TITRE = "galerie_titre";

    private String name;

    /**
     * Le titre de la visionneuse. Affiché lors de la production de la galerie
     */
    private String titre;

    /**
     *
     */
    private VisionneusePlaylist visionneusePlaylist;

    /**
     * Récupérer le titre affichable de la galerie
     *
     * @return Le titre de la galerie.
     */
    public String getTitre() {
        return titre;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.galerie.Galerie#afficherGalerieComplete(com.univ
     * .utils.ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.playlist.MediaPlaylist)
     */
    @Override
    public void afficherGalerieComplete(ContexteUniv ctx, Writer out, MediaPlaylist mediaList) throws Exception {
        mediaList.setCtx(ctx);
        mediaList.init();
        out.write("<div class=\"");
        out.write(HTML_CSS_VISIONNEUSE);
        out.write("\">");
        // affichage du titre
        if (StringUtils.isNotEmpty(this.getTitre())) {
            out.write("<p class=\"");
            out.write(HTML_CSS_TITRE);
            out.write("\">");
            out.write(this.getTitre());
            out.write("</p>");
        }
        this.initialiserPlayer(ctx, out, mediaList);
        this.initialiserPlaylist(ctx, out, mediaList);
        out.write("</div>");
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.galerie.Galerie#afficherPlayer(com.univ.utils.
     * ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.playlist.MediaPlaylist)
     */
    @Override
    public void initialiserPlayer(ContexteUniv ctx, Writer out, MediaPlaylist mediaList) throws Exception {
        StringWriter strWriter = new StringWriter();
        List<MediaBean> medias = mediaList.getMediaList();
        int maxHeight = -1;
        int maxWidth = -1;
        if (medias != null && medias.size() > 0) {
            VisionneusePlayer vPlayer;
            MediaStyle mStyle;
            MediaPlayer mPlayer;
            for (MediaBean media : medias) {
                vPlayer = mediaList.getVisionneusePlayer(media);
                mStyle = mediaList.getMediaStyle(media);
                mPlayer = mediaList.getMediaPlayer(media);
                vPlayer.initialiserVisionneuse(ctx, strWriter, mPlayer);
                vPlayer.setLanceurMedia(ctx, media, mPlayer, mStyle);
                maxHeight = Math.max(maxHeight, vPlayer.getMaxHeight());
                maxWidth = Math.max(maxWidth, vPlayer.getMaxWidth());
            }
        }
        out.write("<div class=\"");
        out.write(HTML_CSS_PLAYER);
        if (maxHeight > -1 || maxWidth > -1) {
            out.write("\" style=\"");
            if (maxHeight > -1) {
                out.write("height:");
                out.write(String.valueOf(maxHeight));
                out.write("px;");
            }
            if (maxWidth > -1) {
                out.write("width:");
                out.write(String.valueOf(maxWidth));
                out.write("px;");
            }
        }
        out.write("\">");
        out.write(strWriter.toString());
        out.write("</div>");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.galerie.Galerie#afficherPlaylist(com.univ.utils.
     * ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.playlist.MediaPlaylist)
     */
    @Override
    public void initialiserPlaylist(ContexteUniv ctx, Writer out, MediaPlaylist mediaList) throws Exception {
        out.write("<div class=\"");
        out.write(HTML_CSS_PLAYLIST);
        out.write(visionneusePlaylist.getName());
        out.write("\">");
        this.visionneusePlaylist.genererVionneusePlaylist(ctx, out, mediaList);
        out.write("</div>");
    }

    /**
     * Définir le nom de la galerie.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.galerie.Galerie#getName()
     */
    @Override
    public String getName() {
        return this.name;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.galerie.Galerie#getType()
     */
    @Override
    public GalerieType getType() {
        return TYPE;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.galerie.Galerie#setTitre(java.lang.String)
     */
    @Override
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * Affecter une visionneuse de playlist afin de produire la visualisation de la playlist.
     *
     * @param VisionneusePlaylist
     *            La visionneuse de playlist
     */
    @Override
    public void setVisionneusePlaylist(VisionneusePlaylist VisionneusePlaylist) {
        this.visionneusePlaylist = VisionneusePlaylist;
    }
}
