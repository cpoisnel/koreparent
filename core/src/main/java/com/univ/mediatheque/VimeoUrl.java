package com.univ.mediatheque;

/**
 * Classe utilitaire permettant de générer l'url du player embarqué d'une vidéo Vimeo Attention : L'url générée est compatible avec l'ancien code embarqué de Vimeo : <xmp><object
 * width="400" height="225"><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value=
 * "http://vimeo.com/moogaloop.swf?clip_id=15271355&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=ffffff&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"
 * /><embed src=
 * "http://vimeo.com/moogaloop.swf?clip_id=15271355&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=ffffff&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"
 * type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="400" height="225"></embed></object>
 * <p>
 * <a href="http://vimeo.com/15271355">FOTB 2010 Titles</a> from <a href="http://vimeo.com/superfad">Süperfad</a> on <a href="http://vimeo.com">Vimeo</a>.
 * </p>
 * </xmp> Le nouveau étant : <xmp><iframe src="http://player.vimeo.com/video/15271355?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="400" height="225"
 * frameborder="0"></iframe>
 * <p>
 * <a href="http://vimeo.com/15271355">FOTB 2010 Titles</a> from <a href="http://vimeo.com/superfad">Süperfad</a> on <a href="http://vimeo.com">Vimeo</a>.
 * </p>
 * </xmp>
 *
 * @author olivier.camon
 */
public class VimeoUrl implements SpecificUrl {

    private static final String URL_SOURCE = "//player.vimeo.com/video/";

    private static final String TYP_RESSOURCE = "video";

    @Override
    public String processUrl(String url) {
        // Url entrée http://vimeo.com/15271355
        String res = url;
        String codeVideo = "";
        //Si c'est bien une url mais pas l'url déjà formaté
        if (res.contains(".com/") && !res.startsWith(URL_SOURCE)) {
            codeVideo = res.substring(res.indexOf(".com/") + 5, res.length());
            if (codeVideo.contains("&")) {
                codeVideo = codeVideo.substring(0, codeVideo.indexOf("&"));
            }
            res = URL_SOURCE + codeVideo;
        }
        return res;
    }

    @Override
    public String getTypeRessource() {
        return TYP_RESSOURCE;
    }

    @Override
    public String getUrlSource() {
        return URL_SOURCE;
    }
}