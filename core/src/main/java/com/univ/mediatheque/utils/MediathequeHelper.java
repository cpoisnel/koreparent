package com.univ.mediatheque.utils;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.PropertyHelper;
import com.kportal.core.context.ContextLoaderListener;
import com.kportal.core.webapp.WebAppUtil;

public class MediathequeHelper {

    /** The critere vignette. */
    public static final String CRITERES_VIGNETTE_PROPERTIES_KEY = "phototheque.vignette";

    public static final String CRITERES_LIMITE_PROPERTIES_KEY = "phototheque.limite";

    public static final String CRITERES_DIMENSION_PROPERTIES_KEY = "phototheque.dimension";

    public static final String CRITERES_PROPORTIONS_PROPERTIES_KEY = "phototheque.proportions";

    public static final String PRE_EXTENSION_CHARACTER = "-";

    /** The Constant DEFAULT_STYLES_TAG_GALERIE. */
    private static final String DEFAULT_STYLES_TAG_GALERIE = "/adminsite/fcktoolbox/kosmos/plugins/k_galerie/styles.jsp";

    /** The Constant MEDIATHEQUE_STYLES_TAG_GALERIE. */
    private static final String MEDIATHEQUE_STYLES_TAG_GALERIE = "mediatheque.styles.tag.galerie";

    /** The Constant MEDIATHEQUE_PATH_PROPERTIES_KEY. */
    private static final String MEDIATHEQUE_PATH_PROPERTIES_KEY = "mediatheque.path";

    /** The Constant SERVER_HTML_PATH. */
    private static final String DEFAULT_MEDIATHEQUE_PATH = "medias";

    private static final Pattern PATTERN_VIMEO = Pattern.compile("(http://|http://)vimeo.com/([0-9]+)");

    private static final Pattern PATTERN_YOUTUBE = Pattern.compile("(http|https):\\/\\/(?:youtu\\.be\\/|(?:[a-z]{2,3}\\.)?youtube\\.com\\/watch(?:\\?|#\\!)v=)([\\w-]{11}).*");

    private static final Pattern PATTERN_DAILYMOTION = Pattern.compile("(http|https):\\/\\/www.dailymotion.com\\/(embed\\/video|video|swf)\\/([^_&]+).*");

    public static String getAbsolutePath() {
        String res = StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(MEDIATHEQUE_PATH_PROPERTIES_KEY), WebAppUtil.getStorageDir() + DEFAULT_MEDIATHEQUE_PATH);
        File path = new File(res);
        if (StringUtils.isNotEmpty(res) && !(path.exists())) {
            path.mkdirs();
        }
        return path.getAbsolutePath();
    }

    public static String getTempPath() {
        String res = String.format("%s/tmp/", getAbsolutePath());
        File path = new File(res);
        if (StringUtils.isNotEmpty(res) && !(path.exists())) {
            path.mkdirs();
        }
        return res;
    }

    public static String getDefaultRelativePath() {
        return DEFAULT_MEDIATHEQUE_PATH;
    }

    public static String getUrlRelativePath() {
        return StringUtils.replace(System.getProperty(ContextLoaderListener.MEDIA_URL_PATTERN), "/*", "/");
    }

    public static String getTemplateStylesTagGalerie() {
        return StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(MEDIATHEQUE_STYLES_TAG_GALERIE), DEFAULT_STYLES_TAG_GALERIE);
    }

    public static String convertToEmbedUrl(String url) {
        String convertedUrl = url;
        Matcher matcherVimeo = PATTERN_VIMEO.matcher(url);
        Matcher matcherYoutube = PATTERN_YOUTUBE.matcher(url);
        Matcher matcherDailymotion = PATTERN_DAILYMOTION.matcher(url);
        if (matcherYoutube.matches()) {
            convertedUrl = String.format("//www.youtube.com/embed/%s", matcherYoutube.group(2));
        } else if (matcherDailymotion.matches()) {
            convertedUrl = String.format("//www.dailymotion.com/embed/video/%s", matcherDailymotion.group(3));
        } else if (matcherVimeo.matches()) {
            convertedUrl = String.format("//player.vimeo.com/video/%s", matcherVimeo.group(2));
        }
        return convertedUrl;
    }
}
