package com.univ.mediatheque.utils;

import org.apache.commons.lang3.StringEscapeUtils;

public class HTMLUtils {

    public static final String HTML_MEDIATHEQUE_LANCEUR_JS = "_MediathequeLuncher";

    public static final String HTML_MEDIATHEQUE_LANCEUR_JS_ADD_START = "_MediathequeLuncher.addFunctionLunchable(";

    public static final String HTML_MEDIATHEQUE_LANCEUR_JS_ADD_END = ");\n";

    public static final String HTML_SCRIPT_JS_OPEN = "<script type=\"text/javascript\">\n//<![CDATA[\n";

    public static final String HTML_SCRIPT_JS_CLOSE = "//]]>\n</script>";

    public static final String OBJET_JS_ONGLET_MANAGER = "new OngletManager()";

    /**
     * Ajouter l'exécution d'une fonction JS au chargement de la page HTML.
     *
     * @param fonctionJS
     *            la fonction à lancer
     * @param priorite
     *            La priorité d'écution (entre 0 et 999)
     * @return le code JS à intégrer dans un script.
     */
    public static String genererMediathequeLanceurJSAdd(String fonctionJS, int priorite) {
        return HTML_MEDIATHEQUE_LANCEUR_JS_ADD_START + "'" + StringEscapeUtils.escapeEcmaScript(fonctionJS) + "'," + priorite + HTML_MEDIATHEQUE_LANCEUR_JS_ADD_END;
    }
}
