package com.univ.mediatheque;

public interface SpecificUrl {

    String processUrl(String url);

    String getTypeRessource();

    String getUrlSource();
}
