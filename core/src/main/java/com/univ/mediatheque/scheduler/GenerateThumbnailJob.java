package com.univ.mediatheque.scheduler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.scheduling.spring.quartz.LogReportJob;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.FileUtil;
import com.univ.utils.ImageInfo;
import com.univ.utils.PhotoUtil;

public class GenerateThumbnailJob extends LogReportJob {

    private static final Logger LOG = LoggerFactory.getLogger(GenerateThumbnailJob.class);

    private final ServiceMedia serviceMedia;

    public GenerateThumbnailJob(){
        serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
    }

    @Override
    public void perform() {
        final String[] thumbnailDimensions = PropertyHelper.getCoreProperty(MediathequeHelper.CRITERES_VIGNETTE_PROPERTIES_KEY).split("/", -2);
        final int width = Integer.parseInt(thumbnailDimensions[1]);
        final int height = Integer.parseInt(thumbnailDimensions[2]);
        rebuildThumbnails(width, height);
    }

    public void rebuildThumbnails(final int requestedWidth, final int requestedHeight) {
        boolean resize;
        final List<MediaBean> medias = serviceMedia.getByResourceType("photo");
        LOG.info(String.format("%d médias à traiter", medias.size()));
        for (final MediaBean currentMedia : medias) {
            try {
                LOG.info(String.format("Traitement du média \"%s\"", MediaUtils.getUrlAbsolue(currentMedia)));
                resize = true;
                String vignetteDestinationPath = "";
                if (StringUtils.isNotBlank(currentMedia.getUrlVignette())) {
                    resize = false;
                    LOG.info(String.format("Pas de vignette pour le média \"%s\". Passage au média suivant.", MediaUtils.getUrlAbsolue(currentMedia)));
                } else {
                    LOG.info(String.format("Vignette détectée pour le média \"%s\". Récupération des informations.", MediaUtils.getUrlAbsolue(currentMedia)));
                    vignetteDestinationPath = MediaUtils.getPathAbsolu(currentMedia).replaceFirst("[^/\\\\]*$", currentMedia.getUrlVignette());
                    final File vignetteDestination = new File(vignetteDestinationPath);
                    if (vignetteDestination.exists() && vignetteDestination.isFile()) {
                        // recuperation des informations de la vignette
                        FileInputStream fis = null;
                        final ImageInfo ii = new ImageInfo();
                        try {
                            fis = new FileInputStream(vignetteDestinationPath);
                            ii.setInput(fis);
                            ii.check();
                            ii.close();
                        } finally {
                            if (fis != null) {
                                fis.close();
                                ii.close();
                            }
                        }
                    }
                }
                if (resize) {
                    LOG.info(String.format("Création de la vignette pour le média \"%s\".", MediaUtils.getUrlAbsolue(currentMedia)));
                    // pas de données enregistrées : on essaye de calculer les dimensions de l'image
                    // à partir du fichier physique
                    FileInputStream mediaFileIs = null;
                    File mediaFile;
                    final ImageInfo imageInfo = new ImageInfo();
                    try {
                        LOG.info(String.format("Tentative de calcul des dimensions du média %s", MediaUtils.getUrlAbsolue(currentMedia)));
                        mediaFile = new File(MediaUtils.getPathAbsolu(currentMedia));
                        //uniquement si fichier source existe
                        if (mediaFile.exists()) {
                            mediaFileIs = new FileInputStream(mediaFile);
                            imageInfo.setInput(mediaFileIs);
                        } else {
                            continue;
                        }
                    } catch (final FileNotFoundException e) {
                        LOG.error(String.format("Echec lors de la tentative de calcul des dimensions du média %s", MediaUtils.getUrlAbsolue(currentMedia)), e);
                    } finally {
                        if (mediaFileIs != null) {
                            mediaFileIs.close();
                            imageInfo.close();
                        }
                    }
                    LOG.info(String.format("Préparation du redimmensionnement du média %s : dimension ciblée [%dx%d]", MediaUtils.getUrlAbsolue(currentMedia), requestedWidth, requestedHeight));
                    // création de la vignette
                    mediaFile = new File(MediaUtils.getPathAbsolu(currentMedia));
                    //uniquement si fichier source existe
                    if (mediaFile.exists()) {
                        final File vignette = PhotoUtil.resize(MediaUtils.getPathAbsolu(currentMedia), FileUtil.getExtension(currentMedia.getUrl()), requestedWidth, requestedHeight, false);
                        FileUtil.copierFichier(vignette, new File(vignetteDestinationPath), vignette.getName().startsWith("tmp_"));
                    } else {
                        LOG.info(String.format("Média %s inexistant. Passage au média suivant.", MediaUtils.getUrlAbsolue(currentMedia)));
                    }
                }
            } catch (final IOException e) {
                LOG.error(String.format("Une erreur est survenue lors de la tentative de génération de vignette pour le média %s", MediaUtils.getUrlAbsolue(currentMedia)), e);
            }
        }
    }
}
