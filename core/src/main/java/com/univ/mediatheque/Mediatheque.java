package com.univ.mediatheque;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.AbstractBeanManager;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.om.SpecificMedia;
import com.univ.objetspartages.processus.SaisieMedia;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.DateUtil;
import com.univ.utils.FileUtil;
import com.univ.utils.IRequeteurConstantes;
import com.univ.utils.RequeteUtil;
import com.univ.utils.media.CodeTypeRessourceCodeTypeMedia;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseLimit;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.criterespecifique.LimitHelper;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * The Class Mediatheque.
 */
public class Mediatheque extends AbstractBeanManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "mediatheque";

    public static final String MIME_TYPES_PATH = "/adminsite/utils/mediatheque/mime.types";

    // 0 mutualise dans la mediatheque /medias
    public static final String ETAT_MUTUALISE = "0";

    // 1 non mutualise et public /medias
    public static final String ETAT_NON_MUTUALISE_PUBLIC = "1";

    // 2 non mutualise et prive /WEB-INF/fichiergw
    public static final String ETAT_NON_MUTUALISE_NON_PUBLIC = "2";

    private static final Logger LOGGER = LoggerFactory.getLogger(Mediatheque.class);

    /** The styles. */
    public HashMap<String, String> styles;

    /** The types media. */
    public HashMap<String, SpecificMedia> typesMedia;

    public Hashtable<String, String> typesRessourcesByExtensions;

    /** The types url. */
    public HashMap<String, SpecificUrl> typesUrl;

    public String uploadPath = "";

    public String mimeTypesPath = "";

    private Properties mimeTypes;

    private ServiceMedia serviceMedia;

    public void setServiceMedia(final ServiceMedia serviceMedia) {
        this.serviceMedia = serviceMedia;
    }

    public static Mediatheque getInstance() {
        return (Mediatheque) ApplicationContextManager.getCoreContextBean(Mediatheque.ID_BEAN);
    }

    /**
     * Gets the types.
     *
     * @return the types
     */
    public HashMap<String, SpecificMedia> getTypesMedia() {
        return typesMedia;
    }

    /**
     * Sets the types.
     *
     * @param types
     *            the types
     */
    public void setTypesMedia(final HashMap<String, SpecificMedia> types) {
        this.typesMedia = types;
    }

    /**
     * Gets the ressource.
     *
     * @param type
     *            the type
     *
     * @return the ressource
     */
    public SpecificMedia getRessource(final String type) {
        return typesMedia.get(type);
    }

    /**
     * Gets the ressource.
     *
     * @return the ressource
     */
    public SpecificMedia getRessourceByExtension(final String extension) {
        if (typesRessourcesByExtensions.get(extension) != null) {
            return getRessource(typesRessourcesByExtensions.get(extension));
        } else {
            return getRessource("fichier");
        }
    }

    /**
     * Renvoie la liste de tous les medias photos d'un type donné Peut être affiché directement dans une Combo.
     *
     * @param ctx
     *            the ctx
     * @param param
     *            the param
     *
     * @return the liste par type
     *
     */
    public List<MediaBean> getMediasByParam(String param, final ContexteUniv ctx) {
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseWhere where = new ClauseWhere();
        ClauseOrderBy orderBy = null;
        ClauseLimit limite;
        param = StringUtils.replace(param, "%20", " ");
        final String typeMedia = RequeteUtil.renvoyerParametre(param, "TYPE_MEDIA");
        final String libelle = RequeteUtil.renvoyerParametre(param, "TITRE");
        final String legende = RequeteUtil.renvoyerParametre(param, "LEGENDE");
        String codeRedacteur = RequeteUtil.renvoyerParametre(param, "CODE_REDACTEUR");
        String codeRubrique = RequeteUtil.renvoyerParametre(param, "CODE_RUBRIQUE");
        final String sDateDebut = RequeteUtil.renvoyerParametre(param, "DATE_DEBUT");
        final String sDateFin = RequeteUtil.renvoyerParametre(param, "DATE_FIN");
        final String codeStructure = RequeteUtil.renvoyerParametre(param, "CODE_RATTACHEMENT");
        final String thematique = RequeteUtil.renvoyerParametre(param, "THEMATIQUE");
        final String nbJours = RequeteUtil.renvoyerParametre(param, "JOUR");
        final String selection = RequeteUtil.renvoyerParametre(param, "SELECTION");
        final String tri = RequeteUtil.renvoyerParametre(param, "TRI_DATE");
        final String nombre = RequeteUtil.renvoyerParametre(param, "NOMBRE");
        Date dateDebut = null;
        Date dateFin = null;
        if (selection.length() > 0) {
            final Date today = new Date(System.currentTimeMillis());
            // media créés dans la semaine
            if ("0005".equals(selection)) {
                // date début dans la semaine ou date de fin dans la semaine
                dateDebut = DateUtil.getFirstDayOfWeek(today);
                dateFin = DateUtil.getLastDayOfWeek(today);
            }
            // media créés dans le mois
            else if ("0006".equals(selection)) {
                // date début dans le mois ou date de fin dans le mois
                dateDebut = DateUtil.getFirstDayOfMonth(today);
                dateFin = DateUtil.getLastDayOfMonth(today);
            }
            // Dernières médias créés
            else if ("0007".equals(selection)) {
                orderBy = new ClauseOrderBy("DATE_CREATION", SensDeTri.DESC);
            }
        } else if (nbJours.length() > 0) {
            dateDebut = DateUtil.addDays(new Date(System.currentTimeMillis()), -Integer.parseInt(nbJours));
            dateFin = new Date(System.currentTimeMillis()); // aujourd'hui +
            // nbJours
        } else {
            dateDebut = DateUtil.parseDate(sDateDebut);
            dateFin = DateUtil.parseDate(sDateFin);
        }
        if (orderBy == null) {
            orderBy = new ClauseOrderBy();
            if ("DATE_ASC".equals(tri)) {
                orderBy = orderBy.orderBy("T1.DATE_CREATION", SensDeTri.ASC);
            } else if ("DATE_DESC".equals(tri)) {
                orderBy = orderBy.orderBy("T1.DATE_CREATION", SensDeTri.DESC);
            }
        }
        where.setPremiereCondition(ConditionHelper.egal("IS_MUTUALISE", 0, TypeOperande.INTEGER));
        if (StringUtils.isNotEmpty(typeMedia)) {
            where.and(getConditionSurTypes(typeMedia));
        }
        if (StringUtils.isNotEmpty(libelle)) {
            where.and(ConditionHelper.rechercheMots("TITRE", libelle));
        }
        if (StringUtils.isNotEmpty(legende)) {
            where.and(ConditionHelper.rechercheMots("LEGENDE", legende));
        }
        if (StringUtils.isNotEmpty(thematique)) {
            where.and(ConditionHelper.egalVarchar("THEMATIQUE", thematique));
        }
        if (StringUtils.isNotEmpty(codeRedacteur)) {
            if (codeRedacteur.equals(IRequeteurConstantes.CODE_DYNAMIQUE)) {
                codeRedacteur = ctx.getCode();
            }
            where.and(ConditionHelper.egalVarchar("CODE_REDACTEUR", codeRedacteur));
        }
        if (StringUtils.isNotEmpty(codeStructure)) {
            where.and(ConditionHelper.getConditionStructure("CODE_RATTACHEMENT", codeStructure));
        }
        if (StringUtils.isNotEmpty(codeRubrique)) {
            if (codeRubrique.startsWith(IRequeteurConstantes.CODE_DYNAMIQUE)) {
                codeRubrique = codeRubrique.replaceAll(IRequeteurConstantes.CODE_DYNAMIQUE, ctx.getCodeRubriquePageCourante());
            }
            where.and(ConditionHelper.getConditionRubrique("CODE_RUBRIQUE", codeRubrique));
        }
        if (Formateur.estSaisie(dateDebut)) {
            where.and(ConditionHelper.greaterEquals("DATE_CREATION", dateDebut, TypeOperande.DATE));
        }
        if (Formateur.estSaisie(dateFin)) {
            where.and(ConditionHelper.critereDateFin("DATE_CREATION", dateFin));
        }
        where.and(MediaUtils.conditionPerimetreMedia(SaisieMedia.MODE_SELECTION));
        // Gestion de l'ordre d'affichage
        orderBy.orderBy("T1.TITRE", SensDeTri.ASC);
        limite = LimitHelper.ajouterCriteresLimitesEtOptimisation(ctx, nombre);
        requeteSelect.where(where).orderBy(orderBy).limit(limite);
        return serviceMedia.getMediasFromRequest(requeteSelect);
    }

    /**
     * Permet de calculer à partir du type de média, les conditions sur le type de ressource et le type de média
     *
     * @param typeMedia
     * @return
     */
    private Condition getConditionSurTypes(final String typeMedia) {
        final String[] typesRessourcesTypesMedias = typeMedia.split(",");
        final ConditionList conditionsSurTypes = new ConditionList();
        for (final String typesRessourcesTypesMedia : typesRessourcesTypesMedias) {
            final CodeTypeRessourceCodeTypeMedia codeTypeRessourceCodeTypeMedia = CodeTypeRessourceCodeTypeMedia.fromString(typesRessourcesTypesMedia);
            if (StringUtils.isNotBlank(codeTypeRessourceCodeTypeMedia.getCodeTypeRessource())) {
                final Collection<String> codesTypeRessources = Arrays.asList(codeTypeRessourceCodeTypeMedia.getCodeTypeRessource().split("\\+"));
                final ConditionList conditionTypeRessourceMedia = new ConditionList(ConditionHelper.in("TYPE_RESSOURCE", codesTypeRessources));
                final String codeTypeMedia = codeTypeRessourceCodeTypeMedia.getCodeTypeMedia();
                if (StringUtils.isNotBlank(codeTypeMedia)) {
                    final Collection<String> codesTypeMedia = Arrays.asList(codeTypeRessourceCodeTypeMedia.getCodeTypeMedia().split("\\+"));
                    conditionTypeRessourceMedia.and(ConditionHelper.in("TYPE_MEDIA", codesTypeMedia));
                }
                conditionsSurTypes.or(conditionTypeRessourceMedia);
            }
        }
        return conditionsSurTypes;
    }

    /**
     * Upload medias by folder.
     *
     * @param infoBean
     *            the infobean
     * @param folder
     *            the folder
     *
     */
    public void uploadMediasByFolder(final InfoBean infoBean, final File folder, final boolean searchDeep, final String codeRedacteur) {
        // traitement
        final boolean autoUpload = infoBean.getString("AUTO_UPLOAD") != null;
        // paramètres d'upload
        // 0 ignorer les autres fichiers
        // 1 ajouter les autres fichiers selon leur type natif
        String typeRessource = "";
        if (infoBean.getString("TYPE_RESSOURCE") != null) {
            typeRessource = infoBean.getString("TYPE_RESSOURCE");
        }
        int nbUpload = 0;
        if (infoBean.get("NB_UPLOAD") != null) {
            nbUpload = infoBean.getInt("NB_UPLOAD");
        }
        int nbError = 0;
        if (infoBean.get("NB_ERROR") != null) {
            nbError = infoBean.getInt("NB_ERROR");
        }
        String message = "";
        if (infoBean.getString("MSG_ERROR") != null) {
            message = infoBean.getString("MSG_ERROR");
        }
        SpecificMedia specificMedia = null;
        final File[] list = folder.listFiles();
        if (list != null) {
            for (final File f : list) {
                specificMedia = null;
                if (f.isDirectory() && searchDeep) {
                    infoBean.set("NB_UPLOAD", nbUpload);
                    infoBean.set("NB_ERROR", nbError);
                    infoBean.set("MSG_ERROR", message);
                    uploadMediasByFolder(infoBean, f, true, codeRedacteur);
                    nbUpload = infoBean.getInt("NB_UPLOAD");
                    nbError = infoBean.getInt("NB_ERROR");
                    message = infoBean.getString("MSG_ERROR");
                } else {
                    // on n'importe que les fichiers non modifiés depuis plus de 2 min
                    // seuleument sur l'autoupload où searchDepp=false
                    if (!searchDeep && f.exists() && (System.currentTimeMillis() - f.lastModified()) < 120000) {
                        LOGGER.warn(f.getName() + " [le fichier a été ignoré car il est en cours de modification]");
                        if (message.length() > 0) {
                            message += (autoUpload ? "\n" : "<br />");
                        }
                        message += "WARNING - " + f.getName() + " [le fichier a été ignoré car il est en cours de modification]";
                        continue;
                    }
                    if (autoUpload && "autoupload.properties".equals(f.getName())) {
                        continue;
                    }
                    specificMedia = getRessourceByExtension(FileUtil.getExtension(f.getName()));
                    if (specificMedia != null) {
                        if (typeRessource.length() > 0 && !typeRessource.equalsIgnoreCase(specificMedia.getCode())) {
                            nbError++;
                            if (message.length() > 0) {
                                message += (autoUpload ? "\nINFO - " : "<br />");
                            }
                            message += "IGNORE : " + f.getName();
                        } else {
                            try {
                                final UploadedFile file = new UploadedFile(null, 0, f.getName(), f);
                                specificMedia.checkMedia(infoBean, file);
                                final MediaBean media = new MediaBean();
                                media.setSource(f.getName());
                                media.setDateCreation(new Date(System.currentTimeMillis()));
                                media.setCodeRedacteur(codeRedacteur);
                                try {
                                    media.setPoids(infoBean.get("POIDS", Long.class));
                                } catch (final NumberFormatException e) {
                                    LOGGER.debug("unable to parse file size", e);
                                }
                                media.setIsMutualise(Mediatheque.ETAT_MUTUALISE + "");
                                media.setTypeRessource(specificMedia.getCode());
                                String title = f.getName();
                                final int iDot = title.lastIndexOf(".");
                                if (iDot != -1) {
                                    title = title.substring(0, iDot);
                                }
                                if (infoBean.getString("TITRE") != null) {
                                    media.setTitre(infoBean.getString("TITRE").replaceAll("\\[file\\]", title));
                                } else {
                                    media.setTitre(title);
                                }
                                if (infoBean.getString("LEGENDE") != null) {
                                    media.setLegende(infoBean.getString("LEGENDE").replaceAll("\\[file\\]", title));
                                }
                                if (infoBean.getString("DESCRIPTION") != null) {
                                    media.setDescription(infoBean.getString("DESCRIPTION").replaceAll("\\[file\\]", title));
                                }
                                if (infoBean.getString("FORMAT") != null) {
                                    media.setFormat(infoBean.getString("FORMAT"));
                                }
                                if (infoBean.getString("AUTEUR") != null) {
                                    media.setAuteur(infoBean.getString("AUTEUR"));
                                }
                                if (infoBean.getString("COPYRIGHT") != null) {
                                    media.setCopyright(infoBean.getString("COPYRIGHT"));
                                }
                                if (infoBean.getString("META_KEYWORDS") != null) {
                                    media.setMetaKeywords(infoBean.getString("META_KEYWORDS"));
                                }
                                if (infoBean.getString("THEMATIQUE") != null) {
                                    media.setThematique(infoBean.getString("THEMATIQUE"));
                                }
                                if (infoBean.getString("CODE_RATTACHEMENT") != null) {
                                    media.setCodeRattachement(infoBean.getString("CODE_RATTACHEMENT"));
                                }
                                if (infoBean.getString("CODE_RUBRIQUE") != null) {
                                    media.setCodeRubrique(infoBean.getString("CODE_RUBRIQUE"));
                                }
                                infoBean.set(SpecificMedia.INFOBEAN_KEY_URL_RESSOURCE, f.getAbsolutePath());
                                final String path = specificMedia.processMedia(infoBean, media);
                                final String pathVignette = specificMedia.processVignette(infoBean, media);
                                serviceMedia.save(media, path, pathVignette, MediaUtils.generateName(media, FileUtil.getExtension(media.getSource()).toLowerCase()), "");
                                if (message.length() > 0) {
                                    message += (autoUpload ? "\nINFO - " : "<br />");
                                }
                                message += "UPLOAD : " + f.getName();
                                nbUpload++;
                            } catch (final Exception e) {
                                LOGGER.error(f.getName() + " [" + e.getMessage() + "]", e);
                                nbError++;
                                if (message.length() > 0) {
                                    message += (autoUpload ? "\n" : "<br />");
                                }
                                message += "ERROR - " + f.getName() + " [" + e.getMessage() + "]";
                            }
                        }
                    }
                }
            }
            infoBean.set("NB_UPLOAD", nbUpload);
            infoBean.set("NB_ERROR", nbError);
            infoBean.set("MSG_ERROR", message);
        }
    }

    /**
     * Gets the types ressources.
     *
     * @return the types ressources
     */
    public Hashtable<String, String> getTypesRessourcesAffichables() {
        final Hashtable<String, String> res = new Hashtable<>();
        for (final SpecificMedia media : typesMedia.values()) {
            res.put(media.getCode(), media.getLibelleAffichable());
        }
        return res;
    }

    /**
     * Gets the styles.
     *
     * @return the styles
     */
    public HashMap<String, String> getStyles() {
        return styles;
    }

    /**
     * Sets the styles.
     *
     * @param styles
     *            the styles
     */
    public void setStyles(final HashMap<String, String> styles) {
        this.styles = styles;
    }

    public HashMap<String, SpecificUrl> getTypesUrl() {
        return typesUrl;
    }

    public void setTypesUrl(final HashMap<String, SpecificUrl> typesUrl) {
        this.typesUrl = typesUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(final String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public Hashtable<String, String> getTypesRessourcesByExtensions() {
        return typesRessourcesByExtensions;
    }

    public SpecificUrl getSpecificUrl(final String surl) {
        URL url = null;
        String host = "";
        try {
            url = new URL(surl);
            host = url.getHost();
        } catch (final MalformedURLException e) {
            LOGGER.debug("unable to create an URL", e);
        }
        if (host.matches("^([^\\.]+)(\\.)([^\\.]+)(\\.)([^\\.]+)$")) {
            host = host.substring(host.indexOf(".") + 1, host.lastIndexOf("."));
        } else if (host.matches("^([^\\.]+)(\\.)([^\\.]+)$")) {
            host = host.substring(0, host.lastIndexOf("."));
        }
        return getTypesUrl().get(host);
    }

    public String getMimeTypesPath() {
        return mimeTypesPath;
    }

    public void setMimeTypesPath(final String mimeTypesPath) {
        this.mimeTypesPath = mimeTypesPath;
    }

    public String getContentType(String extension) {
        if (extension.contains(".")) {
            extension = FileUtil.getExtension(extension);
        }
        extension = extension.toLowerCase();
        if (mimeTypes.get(extension) != null) {
            return (String) mimeTypes.get(extension);
        } else {
            return "application/octet-stream";
        }
    }

    public Object getPlayList(final String key) {
        return ApplicationContextManager.getCoreContextBean(key);
    }

    @Override
    public void refresh() {
        typesMedia = new HashMap<>();
        final Collection<SpecificMedia> specificMedias = moduleManager.getModules(SpecificMedia.class);
        for (final SpecificMedia module : specificMedias) {
            typesMedia.put(module.getCode(), module);
        }
        final Hashtable<String, String> hTypeRessourceByExtension = new Hashtable<>();
        for (String sKeyType : typesMedia.keySet()) {
            final SpecificMedia specificMedia = typesMedia.get(sKeyType);
            for (final String extension : specificMedia.getExtensions()) {
                hTypeRessourceByExtension.put(extension, sKeyType);
            }
        }
        typesRessourcesByExtensions = hTypeRessourceByExtension;
        mimeTypes = new Properties();
        if (mimeTypesPath.length() == 0) {
            setMimeTypesPath(MIME_TYPES_PATH);
        }
        try (final InputStream in = new FileInputStream(new File(WebAppUtil.getAbsolutePath() + mimeTypesPath));){
            mimeTypes.load(in);
        } catch (final IOException e) {
            LOGGER.error("Erreur de chargement du fichier mime.types", e);
        }
    }
}
