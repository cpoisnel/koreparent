package com.univ.mediatheque.style;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.mediatheque.Mediatheque;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.om.SpecificMedia;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.URLResolver;

/**
 * Affichage d'un ensemble d'information sur le média dans les "cases" des playlists.
 *
 * @author pierre.cosson
 *
 */
public class MediaStyleComplet extends AbstractMediaStyle {

    private static final Logger LOG = LoggerFactory.getLogger(MediaStyleComplet.class);

    /**
     * Hauteur max de la vignette par défaut.
     */
    protected int HAUTEUR_MAX_VIGNETTE_DEFAUT = 75;

    /**
     * Largeur max de la vignette par défaut
     */
    protected int LARGEUR_MAX_VIGNETTE_DEFAUT = 100;

    private String messageInformatifLanceur;

    private int hauteurMaxVignette = HAUTEUR_MAX_VIGNETTE_DEFAUT;

    private int largeurMaxVignette = LARGEUR_MAX_VIGNETTE_DEFAUT;

    public void setMessageInformatifLanceur(String messageInformatifLanceur) {
        this.messageInformatifLanceur = messageInformatifLanceur;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.style.AbstractMediaStyle#getHTML(com.univ.objetspartages
     * .om.Media)
     */
    @Override
    protected String getHTML(MediaBean media) {
        StringBuilder returnString = new StringBuilder();
        String legende = media.getLegende();
        returnString.append("<div class=\"style1_media\">");
        Properties proprieteMedia = new Properties();
        try {
            proprieteMedia.load(new ByteArrayInputStream(media.getSpecificData().getBytes()));
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        returnString.append(this.genererLanceurMediaDebut(media));
        returnString.append("<span class=\"style1_media_vignette\" style=\"height:");
        returnString.append(String.valueOf(this.getHauteurMaxVignette()));
        returnString.append("px; line-height:");
        returnString.append(String.valueOf(this.getHauteurMaxVignette()));
        returnString.append("px\"><img src=\"");
        if (StringUtils.isNotBlank(MediaUtils.getUrlVignetteAbsolue(media))) {
            returnString.append(URLResolver.getAbsoluteUrl(MediaUtils.getUrlVignetteAbsolue(media), this.ctx));
        } else {
            Mediatheque mediatheque = Mediatheque.getInstance();
            SpecificMedia specificMedia = mediatheque.getRessource(media.getTypeRessource());
            returnString.append(URLResolver.getAbsoluteUrl(specificMedia.getUrlVignette(), this.ctx));
        }
        returnString.append("\"");
        String strHauteurVignette = (String) proprieteMedia.get("HAUTEUR");
        String strLargeurVignette = (String) proprieteMedia.get("LARGEUR");
        if (StringUtils.isNotEmpty(strHauteurVignette) && StringUtils.isNotEmpty(strLargeurVignette)) {
            int hauteur = Integer.valueOf(strHauteurVignette);
            int largeur = Integer.valueOf(strLargeurVignette);
            if (hauteur > 0 && largeur > 0) {
                int newHauteur;
                if (hauteur > this.getHauteurMaxVignette()) {
                    newHauteur = this.getHauteurMaxVignette();
                } else {
                    newHauteur = hauteur;
                }
                // calcul de la nouvelle largeur
                int newLargeur = (int) (largeur / (double) (hauteur / newHauteur));
                if (newLargeur > this.getLargeurMaxVignette()) {
                    newHauteur = (int) (newHauteur / (double) (newLargeur / this.getLargeurMaxVignette()));
                    newLargeur = this.getLargeurMaxVignette();
                }
                returnString.append(" height=\"");
                returnString.append(String.valueOf(newHauteur));
                returnString.append("px\"");
                returnString.append(" width=\"");
                returnString.append(String.valueOf(newLargeur));
                returnString.append("px\"");
            }
        }
        returnString.append(" alt=\"");
        returnString.append(StringEscapeUtils.escapeHtml4(legende));
        returnString.append("\" /></span>");
        returnString.append(this.genererLanceurMediaFin(media));
        returnString.append("<div class=\"style1_media_contenu\"><strong class=\"style1_media_titre\">");
        String titre = media.getTitre();
        if (StringUtils.isEmpty(titre)) {
            titre = media.getSource();
        }
        returnString.append(titre);
        returnString.append("</strong>");
        if (StringUtils.isNotEmpty(legende)) {
            returnString.append("<p class=\"style1_media_legende\">");
            returnString.append(legende);
            returnString.append("</p>");
        }
        String description = media.getDescription();
        if (StringUtils.isNotEmpty(description)) {
            returnString.append("<p class=\"style1_media_description\">");
            returnString.append(description);
            returnString.append("</p>");
        }
        String auteur = media.getAuteur();
        if (StringUtils.isNotEmpty(auteur)) {
            returnString.append("<p class=\"style1_media_auteur\">");
            returnString.append(auteur);
            returnString.append("</p>");
        }
        returnString.append("</div><!-- style1_media_contenu --></div><!-- style1_media -->");
        return returnString.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.style.AbstractMediaStyle#getMessageInformatifLanceur
     * (com.univ.objetspartages.om.Media)
     */
    @Override
    protected String getMessageInformatifLanceur(MediaBean media) {
        return this.messageInformatifLanceur;
    }

    public int getHauteurMaxVignette() {
        return hauteurMaxVignette;
    }

    public void setHauteurMaxVignette(int hauteurMaxVignette) {
        this.hauteurMaxVignette = hauteurMaxVignette;
    }

    public int getLargeurMaxVignette() {
        return largeurMaxVignette;
    }

    public void setLargeurMaxVignette(int largeurMaxVignette) {
        this.largeurMaxVignette = largeurMaxVignette;
    }
}
