package com.univ.mediatheque.style;

import com.univ.mediatheque.container.LanceurMedia;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.utils.ContexteUniv;

/**
 * Style d'affichage d'un média. Cette interface est à implémenter pour afficher changer l'affichage d'un média dans la playlist d'une galerie.
 *
 * @author pierre.cosson
 *
 */
public interface MediaStyle {

    /**
     * Récupérer le nom du style (est utilisé comme classe CSS)
     *
     * @return le nom du style.
     */
    String getName();

    void setCtx(ContexteUniv ctx);

    /**
     * Injecter dans le style d'affichage le lanceur de média.
     *
     * @param lanceur
     *            Le lanceur de média.
     */
    void setLanceurMedia(LanceurMedia lanceur);

    /**
     * Cette fonction permet de génerer pour un média donné son affichage dans une playlist.
     *
     * @param media
     *            Le média à a
     * @return
     */
    String getContenuHTML(MediaBean media);

    void init();
}
