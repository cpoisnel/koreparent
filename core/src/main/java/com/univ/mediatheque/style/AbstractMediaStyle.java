package com.univ.mediatheque.style;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.MessageHelper;
import com.univ.mediatheque.container.AttributBaliseHTML;
import com.univ.mediatheque.container.LanceurMedia;
import com.univ.mediatheque.utils.HTMLUtils;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;

public abstract class AbstractMediaStyle implements MediaStyle {

    /**
     * Le contexte de l'applciation
     */
    protected ContexteUniv ctx;

    /**
     * La liste des lanceur de média associé à l'identifiant du média.
     */
    Map<Long, List<LanceurMedia>> lanceursMedia;

    private String name;

    /**
     *
     */
    public AbstractMediaStyle() {
        this.ctx = null;
        this.lanceursMedia = new HashMap<>();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.style.MediaStyle#setCtx(com.univ.utils.ContexteUniv)
     */
    @Override
    public void setCtx(final ContexteUniv ctx) {
        this.ctx = ctx;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.style.MediaStyle#setLanceurMedia(com.univ.mediatheque
     * .container.LanceurMedia)
     */
    @Override
    public void setLanceurMedia(final LanceurMedia lanceur) {
        final Long idMedia = lanceur.getMedia().getId();
        List<LanceurMedia> listeLanceur = this.lanceursMedia.get(idMedia);
        if (listeLanceur == null) {
            listeLanceur = new ArrayList<>();
            this.lanceursMedia.put(idMedia, listeLanceur);
        }
        listeLanceur.add(lanceur);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.style.MediaStyle#getContenuHTML(com.univ.objetspartages
     * .om.Media)
     */
    @Override
    public String getContenuHTML(final MediaBean media) {
        final List<LanceurMedia> lanceurs = this.lanceursMedia.get(media.getId());
        if (lanceurs != null && lanceurs.size() > 0) {
            final StringBuilder contenuHtmlAvant = new StringBuilder();
            final StringBuilder contenuHtmlApres = new StringBuilder();
            final StringBuilder contenuJsAvant = new StringBuilder();
            final StringBuilder contenuJsApres = new StringBuilder();
            final StringBuilder contenuHtml = new StringBuilder();
            for (final LanceurMedia lanceur : lanceurs) {
                contenuHtmlAvant.append(lanceur.getHTMLAvantLanceur());
                contenuHtmlApres.append(lanceur.getHTMLApresLanceur());
                contenuJsAvant.append(lanceur.getJSAvantLanceur());
                contenuJsApres.append(lanceur.getJSApresLanceur());
            }
            contenuHtml.append(contenuHtmlAvant);
            if (contenuJsAvant.length() > 0) {
                contenuHtml.append(HTMLUtils.HTML_SCRIPT_JS_OPEN);
                contenuHtml.append(contenuJsAvant);
                contenuHtml.append(HTMLUtils.HTML_SCRIPT_JS_CLOSE);
            }
            contenuHtml.append(this.getHTML(media));
            contenuHtml.append(contenuHtmlApres);
            if (contenuJsApres.length() > 0) {
                contenuHtml.append(HTMLUtils.HTML_SCRIPT_JS_OPEN);
                contenuHtml.append(contenuJsApres);
                contenuHtml.append(HTMLUtils.HTML_SCRIPT_JS_CLOSE);
            }
            return contenuHtml.toString();
        } else {
            return this.getHTML(media);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.style.MediaStyle#init()
     */
    @Override
    public void init() {
        // rien
    }

    protected String genererLanceurMediaDebut(final MediaBean media) {
        final List<LanceurMedia> lanceurs = this.lanceursMedia.get(media.getId());
        final StringBuilder startLanceur = new StringBuilder();
        startLanceur.append("<a");
        final Map<AttributBaliseHTML, StringBuilder> attributsHTML = new HashMap<>();
        StringBuilder valeurAttributHTMLTmp;
        Set<AttributBaliseHTML> attributHTMLTmp;
        String valeurAttributLanceur;
        for (final LanceurMedia lanceur : lanceurs) {
            attributHTMLTmp = lanceur.getAttributsBaliseHTML();
            for (final AttributBaliseHTML attribut : attributHTMLTmp) {
                valeurAttributLanceur = lanceur.getValeurAttributHTML(attribut);
                if (StringUtils.isNotEmpty(valeurAttributLanceur)) {
                    valeurAttributHTMLTmp = attributsHTML.get(attribut);
                    if (valeurAttributHTMLTmp == null) {
                        valeurAttributHTMLTmp = new StringBuilder();
                        attributsHTML.put(attribut, valeurAttributHTMLTmp);
                        valeurAttributHTMLTmp.append(valeurAttributLanceur);
                    } else {
                        valeurAttributHTMLTmp.append(" ");
                        valeurAttributHTMLTmp.append(valeurAttributLanceur);
                    }
                }
            }
        }
        attributHTMLTmp = attributsHTML.keySet();
        for (final AttributBaliseHTML attribut : attributHTMLTmp) {
            valeurAttributHTMLTmp = attributsHTML.get(attribut);
            if (valeurAttributHTMLTmp != null && valeurAttributHTMLTmp.length() > 0) {
                startLanceur.append(" ");
                startLanceur.append(attribut.getLibelle());
                startLanceur.append("=\"");
                startLanceur.append(valeurAttributHTMLTmp);
                if (AttributBaliseHTML.HREF.equals(attribut) && MediaUtils.isLocal(media)) {
                    // ajout de la propriété pour éviter les lien de médias
                    // externes (sera supprimé par UnivWebFmt)
                    startLanceur.append(String.format("%sMEDIA_ID=%s", startLanceur.toString().contains("?") ? "&" : "?", media.getId().toString()));
                    startLanceur.append("#TARGET=BLANK");
                }
                startLanceur.append("\"");
            }
        }
        if (!attributHTMLTmp.contains(AttributBaliseHTML.TITLE)) {
            final String messageInfo = this.getMessageInformatifLanceur(media);
            if (StringUtils.isNotEmpty(messageInfo)) {
                startLanceur.append(" title=\"");
                startLanceur.append(StringEscapeUtils.escapeHtml4(MessageHelper.getCoreMessage(messageInfo)));
                startLanceur.append("\"");
            }
        }
        //Ajout du type de media dans un attribut
        if (StringUtils.isNotBlank(media.getTypeRessource())) {
            startLanceur.append(" data-type=\"");
            startLanceur.append(MapMedia.getInstance().getTypeMediaFront(media.getTypeRessource()));
            startLanceur.append("\"");
        }
        startLanceur.append(">");
        return startLanceur.toString();
    }

    protected String genererLanceurMediaFin(final MediaBean media) {
        return "</a>";
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.style.MediaStyle#getName()
     */
    @Override
    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    protected abstract String getHTML(MediaBean media);

    protected abstract String getMessageInformatifLanceur(MediaBean media);
}
