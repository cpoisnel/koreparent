package com.univ.mediatheque.style;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.mediatheque.Mediatheque;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.om.SpecificMedia;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.URLResolver;

public class MediaStyleSimple extends AbstractMediaStyle {

    private static final Logger LOG = LoggerFactory.getLogger(MediaStyleSimple.class);

    /**
     * Hauteur max de la vignette par défaut.
     */
    protected int HAUTEUR_MAX_VIGNETTE_DEFAUT = 75;

    /**
     * Largeur max de la vignette par défaut
     */
    protected int LARGEUR_MAX_VIGNETTE_DEFAUT = 100;

    private String messageInformatifLanceur;

    private int hauteurMaxVignette = HAUTEUR_MAX_VIGNETTE_DEFAUT;

    private int largeurMaxVignette = LARGEUR_MAX_VIGNETTE_DEFAUT;

    public void setMessageInformatifLanceur(final String messageInformatifLanceur) {
        this.messageInformatifLanceur = messageInformatifLanceur;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.style.AbstractMediaStyle#getHTML(com.univ.objetspartages
     * .om.Media)
     */
    @Override
    protected String getHTML(final MediaBean media) {
        final StringBuilder returnString = new StringBuilder();
        final String legende = media.getLegende();
        returnString.append("<div class=\"style1_media\">");
        final Properties proprieteMedia = new Properties();
        try {
            proprieteMedia.load(new ByteArrayInputStream(media.getSpecificData().getBytes()));
        } catch (final IOException e) {
            LOG.error(e.getMessage(), e);
        }
        returnString.append(this.genererLanceurMediaDebut(media));
        returnString.append("<span class=\"style1_media_vignette\" style=\"height:");
        returnString.append(String.valueOf(this.getHauteurMaxVignette()));
        returnString.append("px; line-height:");
        returnString.append(String.valueOf(this.getHauteurMaxVignette()));
        returnString.append("px\"><img src=\"");
        if (StringUtils.isNotBlank(MediaUtils.getUrlVignetteAbsolue(media))) {
            returnString.append(URLResolver.getAbsoluteUrl(MediaUtils.getUrlVignetteAbsolue(media), this.ctx));
        } else {
            final Mediatheque mediatheque = Mediatheque.getInstance();
            final SpecificMedia specificMedia = mediatheque.getRessource(media.getTypeRessource());
            returnString.append(URLResolver.getAbsoluteUrl(specificMedia.getUrlVignette(), this.ctx));
            returnString.append("\"");
            returnString.append("class=\"default-thumbnail defaut-thumbnail--" + specificMedia.getCode());
        }
        returnString.append("\"");
        returnString.append(" alt=\"");
        returnString.append(StringEscapeUtils.escapeHtml4(legende));
        returnString.append("\" />");
        returnString.append("<span class=\"style1_media_contenu\">");
        String titre = media.getTitre();
        if (StringUtils.isEmpty(titre)) {
            titre = media.getSource();
        }
        returnString.append("<strong class=\"style1_media_titre\">");
        returnString.append("<p class=\"titre_corps\">");
        returnString.append(titre);
        returnString.append("</p>");
        if (StringUtils.isNotEmpty(legende)) {
            returnString.append("<p class=\"style1_media_legende\">");
            returnString.append(legende);
            returnString.append("</p>");
        }
        returnString.append("</strong>");
        returnString.append("</span><!-- style1_media_contenu -->");
        returnString.append("</span>");
        returnString.append(this.genererLanceurMediaFin(media));
        returnString.append("</div><!-- style1_media -->");
        return returnString.toString();
    }

    @Override
    protected String getMessageInformatifLanceur(final MediaBean media) {
        return messageInformatifLanceur;
    }

    public int getHauteurMaxVignette() {
        return hauteurMaxVignette;
    }

    public void setHauteurMaxVignette(final int hauteurMaxVignette) {
        this.hauteurMaxVignette = hauteurMaxVignette;
    }

    public int getLargeurMaxVignette() {
        return largeurMaxVignette;
    }

    public void setLargeurMaxVignette(final int largeurMaxVignette) {
        this.largeurMaxVignette = largeurMaxVignette;
    }
}