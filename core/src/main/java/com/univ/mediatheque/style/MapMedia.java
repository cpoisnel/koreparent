package com.univ.mediatheque.style;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

/**
 * This class is a key type media mapper that maps a key used in back office with a key used in front office. The map is a TreeMap and contains those values :
 *
 * ("photo", "image") ("fichier", "doc") ("audio", "audio") ("video", "undefined") ("flash", "flash")
 *
 * Note that "video" is mapped to "undefined" letting the front library calculate the type to determine if it should used a local player or just load the target video in is site's
 * player (e.g. Youtube videos).
 *
 *
 * @author alexandre.baillif
 *
 */
public final class MapMedia {

    private final static String UNDEFINED = "undefined";

    private static MapMedia instance = new MapMedia();

    private final Map<String, String> correspondanceMedia = new TreeMap<>();

    private MapMedia() {
        correspondanceMedia.put("photo", "image");
        correspondanceMedia.put("fichier", "doc");
        correspondanceMedia.put("audio", "audio");
        correspondanceMedia.put("video", UNDEFINED); //Special case, cf. Javadoc
        correspondanceMedia.put("flash", "flash");
    }

    public static MapMedia getInstance() {
        return instance;
    }

    /**
     * Return the type of a media, making a corresponding between the back office key types and the front office key types.
     *
     * @return
     */
    public String getTypeMediaFront(final String backOfficeKey) {
        String result = correspondanceMedia.get(backOfficeKey);
        if (StringUtils.isBlank(result)) {
            result = UNDEFINED;
        }
        return result;
    }
}
