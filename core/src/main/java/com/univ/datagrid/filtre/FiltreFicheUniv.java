package com.univ.datagrid.filtre;

import org.apache.commons.collections.Predicate;

import com.univ.datagrid.bean.ResultatFicheDatagrid;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

/**
 * Retourne un objet permettant de vérifier si un bean {@link ResultatFicheDatagrid} contient le terme fourni via {@link FiltreFicheUniv#critere} dans un de ses champs
 *
 * @author olivier.camon
 *
 */
public class FiltreFicheUniv implements Predicate {

    private final String critere;

    public FiltreFicheUniv(final String critere) {
        this.critere = critere;
    }

    @Override
    public boolean evaluate(final Object object) {
        boolean isARetourner = Boolean.FALSE;
        if (object instanceof ResultatFicheDatagrid) {
            final ResultatFicheDatagrid resultat = (ResultatFicheDatagrid) object;
            isARetourner = containsIgnoreCase(resultat.getLibelle(), critere);
            isARetourner = isARetourner || containsIgnoreCase(resultat.getTypeObjet(), critere) || containsIgnoreCase(resultat.getRubrique(), critere);
            isARetourner = isARetourner || containsIgnoreCase(resultat.getLangue(), critere) || containsIgnoreCase(resultat.getEtatFiche(), critere);
        }
        return isARetourner;
    }
}
