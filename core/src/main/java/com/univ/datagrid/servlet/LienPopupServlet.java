package com.univ.datagrid.servlet;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.FicheUnivHelper;

/**
 * FIXME Servlet à revoir, c'est +/- un copier coller de ce qu'il y avait dans les JSP
 *
 * @author olivier.camon
 *
 */
public class LienPopupServlet extends HttpServlet {

    public static final String LIEN_SERVLET = "/servlet/com.kportal.servlet.LienPopupServlet";

    public static final String CODE_OBJET = "CODE";

    public static final String TYPE_OBJET = "OBJET";

    public static final String LANGUE = "LANGUE";

    public static final String LIBELLE = "LIBELLE";

    public static final String TYPE_TOOLBOX = "TOOLBOX";

    public static final String TYPE_LISTE = "LISTE";

    public static final String URL = "URL";

    /**
     *
     */
    private static final long serialVersionUID = -7317972252389032829L;

    private static final Logger LOG = LoggerFactory.getLogger(LienPopupServlet.class);

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse res) {
        final String typeToolbox = req.getParameter(TYPE_TOOLBOX);
        final String typeListe = req.getParameter(TYPE_LISTE);
        final String url = req.getParameter(URL);
        final String codeObjet = StringUtils.defaultString(req.getParameter(CODE_OBJET));
        final String typeObjet = StringUtils.defaultString(req.getParameter(TYPE_OBJET));
        final String langue = StringUtils.defaultString(req.getParameter(LANGUE));
        final String reponse;
        if ("MAILTO".equals(typeToolbox)) {
            reponse = "/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_lien_mailto.jsp";
        } else if (StringUtils.isNotBlank(url) && !"LIEN_INTERNE_JOINTURE".equals(typeToolbox) && !"LIEN_INTERNE_PAGE_TETE".equals(typeToolbox)) {
            reponse = "/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_lien_lien.jsp";
        } else {
            if ("PARCOURS".equals(typeListe)) {
                reponse = WebAppUtil.SG_PATH + "?EXT=offreformation&PROC=TRAITEMENT_PARCOURS&ACTION=AJOUTER&CODE_UEUP=" + codeObjet + ",LANGUE=" + langue + "&TOOLBOX=true";
            } else if ("RESPONSABLE".equals(typeListe)) {
                reponse = WebAppUtil.SG_PATH + "?EXT=offreformation&PROC=TRAITEMENT_RESPONSABLE&ACTION=AJOUTER&CODE_RESPONSABLE=" + codeObjet + ",LANGUE=" + langue;
            } else if ("DIPLOME".equals(typeListe) || "FORMATION".equals(typeListe)) {
                reponse = WebAppUtil.SG_PATH + "?EXT=offreformation&PROC=TRAITEMENT_DIPLOME&ACTION=AJOUTER&CODE_FORMATION=" + codeObjet + ",LANGUE=" + langue + "&&LISTE=" + typeListe;
            } else {
                FicheUniv fiche = null;
                MetatagBean meta = null;
                try {
                    fiche = FicheUnivHelper.getFiche(typeObjet, codeObjet, langue);
                    meta = MetatagUtils.lireMeta(fiche);
                } catch (final Exception e) {
                    LOG.error("impossible de récuperer la fiche", e);
                }
                if (isChoixRubriqueForcage(fiche, typeToolbox)) {
                    reponse = "/adminsite/toolbox/choix_rubrique_forcage.jsp";
                } else if ("LIEN_INTERNE".equals(typeToolbox)) {
                    reponse = "/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_lien_interne.jsp";
                } else if ("LIEN_INTRANET".equals(typeToolbox)) {
                    reponse = "/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_lien_intranet.jsp";
                } else if ("LIEN_RECHERCHE".equals(typeToolbox)) {
                    reponse = "/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_lien_recherche.jsp";
                } else if ("LIEN_INTERNE_PLUGIN".equals(typeToolbox)) {
                    // pour les jointures de fiche sur un plugin (ex: commentaire) on remplace le code par l'id du meta
                    reponse = "/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_lien_generique.jsp?" + CODE_OBJET + "=" + meta.getId();
                } else {
                    // pour les jointures entre fiche, on renvoit code,LANGUE=langue
                    reponse = "/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_lien_generique.jsp?" + CODE_OBJET + "=" + codeObjet + ",LANGUE=" + langue + (typeToolbox.endsWith("PAGE_TETE") ? ",TYPE=" + typeObjet : "");
                }
            }
        }
        try {
            getServletContext().getRequestDispatcher(reponse).forward(req, res);
        } catch (final ServletException | IOException e) {
            LOG.error("unable to forward the request", e);
        }
    }

    private boolean isChoixRubriqueForcage(final FicheUniv fiche, final String typeToolbox) {
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        final Collection<String> rubriquesPublication = serviceRubriquePublication.getRubriqueDestByFicheUniv(fiche);
        return CollectionUtils.isNotEmpty(rubriquesPublication) && !"LIEN_INTERNE_PAGE_TETE".equals(typeToolbox) && !"LIEN_INTERNE_JOINTURE".equals(typeToolbox);
    }
}
