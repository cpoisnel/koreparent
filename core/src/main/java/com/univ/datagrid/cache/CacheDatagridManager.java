package com.univ.datagrid.cache;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.processus.ServiceDatagrid;

public class CacheDatagridManager {

    public static final String ID_BEAN = "cacheDatagridManager";

    @Cacheable(value = "CacheDatagridManager.traiterRecherche", key = "#idRequete")
    public List<ResultatDatagrid> traiterRecherche(final String idRequete, final ServiceDatagrid datagrid, final HttpServletRequest req) {
        return datagrid.traiterRechercheDepuisRequete(req);
    }

    @CacheEvict(value = "CacheDatagridManager.traiterRecherche")
    public void flush(final String idRequete) {}
}
