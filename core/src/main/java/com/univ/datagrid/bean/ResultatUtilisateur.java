package com.univ.datagrid.bean;

public class ResultatUtilisateur extends ResultatDatagrid {

    /**
     *
     */
    private static final long serialVersionUID = 2897181666264679554L;

    private String nom;

    private String code;

    private String prenom;

    private String civilite;

    private String structure;

    private String arboStructure;

    private String mail;

    private String urlSuppression;

    private String urlSuppressionGroupe;

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    @Override
    public String getUrlSuppression() {
        return urlSuppression;
    }

    @Override
    public void setUrlSuppression(String urlSuppression) {
        this.urlSuppression = urlSuppression;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getArboStructure() {
        return arboStructure;
    }

    public void setArboStructure(String arboStructure) {
        this.arboStructure = arboStructure;
    }

    public String getUrlSuppressionGroupe() {
        return urlSuppressionGroupe;
    }

    public void setUrlSuppressionGroupe(String urlSuppressionGroupe) {
        this.urlSuppressionGroupe = urlSuppressionGroupe;
    }
}
