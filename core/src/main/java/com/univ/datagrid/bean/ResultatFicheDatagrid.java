package com.univ.datagrid.bean;

import java.util.Date;

public class ResultatFicheDatagrid extends ResultatDatagrid {

    /**
     *
     */
    private static final long serialVersionUID = -4451239316579495971L;

    private String codeFiche;

    private String etatFiche;

    private String classEtatFiche;

    private String langue;

    private String urlDrapeauLangue;

    private String rubrique;

    private String url;

    private String filAriane;

    private Date dateModification;

    public String getEtatFiche() {
        return etatFiche;
    }

    public void setEtatFiche(final String etatFiche) {
        this.etatFiche = etatFiche;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(final String langue) {
        this.langue = langue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getFilAriane() {
        return filAriane;
    }

    public void setFilAriane(final String filAriane) {
        this.filAriane = filAriane;
    }

    public String getCodeFiche() {
        return codeFiche;
    }

    public void setCodeFiche(final String codeFiche) {
        this.codeFiche = codeFiche;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(final Date dateModification) {
        this.dateModification = dateModification;
    }

    public String getRubrique() {
        return rubrique;
    }

    public void setRubrique(final String rubrique) {
        this.rubrique = rubrique;
    }

    public String getClassEtatFiche() {
        return classEtatFiche;
    }

    public void setClassEtatFiche(final String classEtatFiche) {
        this.classEtatFiche = classEtatFiche;
    }

    public String getUrlDrapeauLangue() {
        return urlDrapeauLangue;
    }

    public void setUrlDrapeauLangue(final String urlDrapeauLangue) {
        this.urlDrapeauLangue = urlDrapeauLangue;
    }
}
