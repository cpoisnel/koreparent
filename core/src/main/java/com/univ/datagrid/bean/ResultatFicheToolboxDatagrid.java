package com.univ.datagrid.bean;

import java.util.List;

public class ResultatFicheToolboxDatagrid extends ResultatFicheDatagrid {

    /**
     *
     */
    private static final long serialVersionUID = -4451239316579495971L;

    private List<MultiSectionGroup> multiSections;

    public List<MultiSectionGroup> getMultiSections() {
        return multiSections;
    }

    public void setMultiSections(final List<MultiSectionGroup> multiSections) {
        this.multiSections = multiSections;
    }
}
