package com.univ.datagrid.bean;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.univ.datagrid.utils.DatagridUtils;

public class CriteresDatagrid {

    private String recherche;

    private int debutAffichage;

    private int nombreAAfficher;

    private String nomColonneATrier;

    private String sensDuTri;

    private int sEcho;

    public static CriteresDatagrid getCritereDepuisRequete(HttpServletRequest request) {
        CriteresDatagrid criteres = new CriteresDatagrid();
        criteres.setRecherche(StringUtils.defaultString(request.getParameter(DatagridUtils.RECHERCHE)));
        String debutAffichage = request.getParameter(DatagridUtils.DEBUT_AFFICHAGE);
        if (StringUtils.isNotBlank(debutAffichage) && StringUtils.isNumeric(debutAffichage)) {
            criteres.setDebutAffichage(Integer.valueOf(debutAffichage));
        } else {
            criteres.setDebutAffichage(0);
        }
        String nombreAAfficher = request.getParameter(DatagridUtils.NOMBRE_A_AFFICHER);
        if (StringUtils.isNotBlank(nombreAAfficher) && StringUtils.isNumeric(nombreAAfficher)) {
            criteres.setNombreAAfficher(Integer.valueOf(nombreAAfficher));
        } else {
            criteres.setNombreAAfficher(10);
        }
        String indiceColonneATrier = request.getParameter(DatagridUtils.INDICE_COLONNE_A_TRIER);
        boolean isColonneTriable = Boolean.parseBoolean(request.getParameter(DatagridUtils.IS_COLONNE_TRIABLE + indiceColonneATrier));
        int indiceColonne = Integer.MAX_VALUE;
        if (StringUtils.isNotBlank(indiceColonneATrier) && StringUtils.isNumeric(indiceColonneATrier) && isColonneTriable) {
            indiceColonne = Integer.valueOf(indiceColonneATrier);
            criteres.setNomColonneATrier(request.getParameter(DatagridUtils.NOM_COLONNE_A_TRIER + indiceColonne));
        }
        criteres.setSensDuTri(request.getParameter(DatagridUtils.SENS_DU_TRI));
        String valeurSEcho = request.getParameter(DatagridUtils.S_ECHO);
        if (StringUtils.isNotBlank(valeurSEcho) && StringUtils.isNumeric(valeurSEcho)) {
            criteres.setsEcho(Integer.valueOf(valeurSEcho));
        }
        return criteres;
    }

    public String getRecherche() {
        return recherche;
    }

    public void setRecherche(String recherche) {
        this.recherche = recherche;
    }

    public int getDebutAffichage() {
        return debutAffichage;
    }

    public void setDebutAffichage(int debutAffichage) {
        this.debutAffichage = debutAffichage;
    }

    public int getNombreAAfficher() {
        return nombreAAfficher;
    }

    public void setNombreAAfficher(int nombreAAfficher) {
        this.nombreAAfficher = nombreAAfficher;
    }

    public String getNomColonneATrier() {
        return nomColonneATrier;
    }

    public void setNomColonneATrier(String nomColonneATrier) {
        this.nomColonneATrier = nomColonneATrier;
    }

    public String getSensDuTri() {
        return sensDuTri;
    }

    public void setSensDuTri(String sensDuTri) {
        this.sensDuTri = sensDuTri;
    }

    public int getsEcho() {
        return sEcho;
    }

    public void setsEcho(int sEcho) {
        this.sEcho = sEcho;
    }
}
