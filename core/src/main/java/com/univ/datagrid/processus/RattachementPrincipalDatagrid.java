package com.univ.datagrid.processus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.bean.ResultatFicheDatagrid;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.url.UrlManager;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FicheUnivHelper;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.SessionUtil;
import com.univ.utils.URLResolver;

public class RattachementPrincipalDatagrid extends AbstractServiceDatagrid {

    private ServiceMetatag serviceMetatag;

    private ServiceRubrique serviceRubrique;

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    public void setServiceRubrique(ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public List<ResultatDatagrid> mapperResultatRechercheDepuisMeta(final List<MetatagBean> metas) {
        final List<ResultatDatagrid> resultats = new ArrayList<>();
        final InfosSite siteCourant = ContexteUtil.getContexteUniv().getInfosSite();
        final Map<String, String> libellesParEtat = ReferentielObjets.getEtatsObjet();
        final Map<String, String> classParEtat = FicheUnivHelper.getClassEtatsObjet();
        for (MetatagBean currentMeta : metas) {
            final ResultatFicheDatagrid resultat = new ResultatFicheDatagrid();
            final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(currentMeta.getMetaCodeRubrique());
            if (rubrique != null) {
                resultat.setFilAriane(serviceRubrique.getLabelWithAscendantsLabels(rubrique));
                resultat.setRubrique(rubrique.getIntitule());
            }
            final FicheUniv fiche = FicheUnivMgr.init(currentMeta);
            if (fiche != null) {
                resultat.setId(currentMeta.getId());
                final AutorisationBean autorisations = ContexteUtil.getContexteUniv().getAutorisation();
                if (autorisations != null && autorisations.estAutoriseAModifierLaFiche(fiche)) {
                    final String url = DatagridUtils.getUrlActionFiche(currentMeta.getMetaCodeObjet(), currentMeta.getMetaIdFiche(), "MODIFIER");
                    resultat.setUrlModification(URLResolver.getAbsoluteBoUrl(url, siteCourant));
                }
                if (autorisations != null && autorisations.estAutoriseASupprimerLaFiche(fiche) && !EtatFiche.A_SUPPRIMER.getEtat().equals(currentMeta.getMetaEtatObjet())) {
                    final String url = DatagridUtils.getUrlActionFiche(currentMeta.getMetaCodeObjet(), currentMeta.getMetaIdFiche(), "SUPPRIMER");
                    resultat.setUrlSuppression(URLResolver.getAbsoluteBoUrl(url, siteCourant));
                }
                resultat.setLibelle(currentMeta.getMetaLibelleFiche());
                resultat.setEtatFiche(libellesParEtat.get(currentMeta.getMetaEtatObjet()));
                resultat.setClassEtatFiche(classParEtat.get(currentMeta.getMetaEtatObjet()));
                resultat.setLangue(LangueUtil.getLocale(currentMeta.getMetaLangue()).getLanguage());
                resultat.setUrlDrapeauLangue(LangueUtil.getPathImageDrapeau(currentMeta.getMetaLangue()));
                resultat.setTypeObjet(ReferentielObjets.getLibelleObjet(currentMeta.getMetaCodeObjet()));
                resultat.setDateModification(currentMeta.getMetaDateModification());
                if (EtatFiche.EN_LIGNE.getEtat().equals(currentMeta.getMetaEtatObjet())) {
                    resultat.setUrl(URLResolver.getAbsoluteBoUrl(UrlManager.calculerUrlFiche(currentMeta), siteCourant));
                }
                resultats.add(resultat);
            }
        }
        return resultats;
    }

    @Override
    public List<ResultatDatagrid> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null || !autorisations.isAdministrateurRubrique()) {
            return Collections.emptyList();
        }
        final String codeRubrique = StringUtils.defaultString(req.getParameter("CODE_RUBRIQUE"));
        // Récupération des critère de recherche de date
        List<ResultatDatagrid> resultats;
        final List<MetatagBean> metas = serviceMetatag.getMetasForRubrique(codeRubrique);
        resultats = mapperResultatRechercheDepuisMeta(metas);
        return resultats;
    }

}
