package com.univ.datagrid.processus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.univ.datagrid.bean.MultiSectionGroup;
import com.univ.datagrid.bean.MultiSectionItem;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.bean.ResultatFicheToolboxDatagrid;
import com.univ.datagrid.filtre.FiltreFicheUniv;
import com.univ.datagrid.servlet.LienPopupServlet;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.tree.processus.RubriquesJsTree;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.DateUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.FicheUnivHelper;
import com.univ.utils.SessionUtil;
import com.univ.utils.recherche.RechercheMultificheHelper;
import com.univ.utils.recherche.ResultatRechercheMultifiche;

public class MultiFicheToolboxDatagrid extends AbstractServiceDatagrid {

    /** l'id Spring du bean. */
    public static final String ID_BEAN = "multiFicheToolboxDatagrid";

    private static final Logger LOG = LoggerFactory.getLogger(MultiFicheToolboxDatagrid.class);

    private ServiceRubrique serviceRubrique;

    public void setServiceRubrique(ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    private List<MultiSectionGroup> getMultiSections(MetatagBean meta) {
        final Map<String, MultiSectionGroup> multiSections = new TreeMap<>();
        try {
            final FicheUniv fiche = FicheUnivHelper.getFicheParIdMeta(meta.getId());
            if(fiche != null) {
                final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                final Collection<String> sectionCodes = serviceRubriquePublication.getRubriqueDestByFicheUniv(fiche);
                // On ajoute le code de rubrique de la fiche que si on a d'autres rubriques possibles (sinon, la rubrique principale est l'affichage par défaut).
                if(sectionCodes.size() > 0) {
                    sectionCodes.add(fiche.getCodeRubrique());
                }
                for (String currentCode : sectionCodes) {
                    final RubriqueBean section = serviceRubrique.getRubriqueByCode(currentCode);
                    if (section != null) {
                        final String path = RubriquesJsTree.getPath("00", section.getCode(), " > ");
                        final String key = RubriquesJsTree.getPath("00", section.getCodeRubriqueMere(), " > ");
                        if (!multiSections.containsKey(key)) {
                            final MultiSectionGroup group = new MultiSectionGroup();
                            group.setLabel(key);
                            group.setItems(new ArrayList<MultiSectionItem>());
                            multiSections.put(key, group);
                        }
                        final MultiSectionItem item = new MultiSectionItem();
                        item.setLabel(section.getIntitule());
                        item.setCode(section.getCode());
                        item.setPath(path);
                        multiSections.get(key).getItems().add(item);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Une erreur est survenue lors de la récupération de la fiche pour l'id meta " + meta.getId(), e);
        }
        return new ArrayList<>(multiSections.values());
    }

    public List<ResultatDatagrid> mapperResultatRechercheDepuisMeta(final Collection<MetatagBean> metatags) {
        final List<ResultatDatagrid> resultats = new ArrayList<>();
        for (final MetatagBean leMeta : metatags) {
            final ResultatFicheToolboxDatagrid resultat = new ResultatFicheToolboxDatagrid();
            resultat.setLibelle(leMeta.getMetaLibelleFiche());
            final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(leMeta.getMetaCodeRubrique());
            if (rubrique != null) {
                resultat.setFilAriane(serviceRubrique.getLabelWithAscendantsLabels(rubrique));
                resultat.setRubrique(rubrique.getIntitule());
            } else {
                resultat.setFilAriane(StringUtils.EMPTY);
                resultat.setRubrique(StringUtils.EMPTY);
            }
            resultat.setId(leMeta.getMetaIdFiche());
            resultat.setMultiSections(getMultiSections(leMeta));
            resultat.setLangue(LangueUtil.getLocale(leMeta.getMetaLangue()).getLanguage());
            resultat.setDateModification(leMeta.getMetaDateModification());
            resultat.setUrlDrapeauLangue(LangueUtil.getPathImageDrapeau(leMeta.getMetaLangue()));
            resultat.setTypeObjet(ReferentielObjets.getLibelleObjet(leMeta.getMetaCodeObjet()));
            resultat.setUrl(LienPopupServlet.LIEN_SERVLET + "?" + LienPopupServlet.CODE_OBJET + "=" + leMeta.getMetaCode() + "&amp;" + LienPopupServlet.TYPE_OBJET + "=" + ReferentielObjets.getNomObjet(leMeta.getMetaCodeObjet()) + "&amp;" + LienPopupServlet.LANGUE + "=" + leMeta.getMetaLangue() + "&amp;" + LienPopupServlet.LIBELLE + "=" + EscapeString.escapeURL(leMeta.getMetaLibelleFiche()));
            resultats.add(resultat);
        }
        return resultats;
    }

    @Override
    public List<ResultatDatagrid> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            return Collections.emptyList();
        }
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final String libelle = req.getParameter("TITRE");
        final String codeObjet = req.getParameter("CODE_OBJET");
        final String codeRubrique = req.getParameter("CODE_RUBRIQUE_RECHERCHE");
        final String codeRattachement = req.getParameter("CODE_RATTACHEMENT");
        final String codeRedacteur = req.getParameter("CODE_REDACTEUR");
        final String idMeta = req.getParameter("ID_META");
        final String urlFiche = req.getParameter("URL_FICHE");
        final String langue = req.getParameter("LANGUE");
        final String etatObjet = EtatFiche.EN_LIGNE.getEtat();
        // Récupération des critère de recherche de date
        final Date dateDebutCreation = recupererAttributDate(req, "DATE_CREATION_DEBUT");
        final Date dateFinCreation = recupererAttributDate(req, "DATE_CREATION_FIN");
        final Date dateDebutModification = recupererAttributDate(req, "DATE_MODIFICATION_DEBUT");
        final Date dateFinModification = recupererAttributDate(req, "DATE_MODIFICATION_FIN");
        final Date dateDebutMiseEnLigne = recupererAttributDate(req, "DATE_MISE_EN_LIGNE_DEBUT");
        final Date dateFinMiseEnLigne = recupererAttributDate(req, "DATE_MISE_EN_LIGNE_FIN");
        ResultatRechercheMultifiche resultatRechercheMultifiche;
        List<ResultatDatagrid> resultats;
        try {
            resultatRechercheMultifiche = RechercheMultificheHelper.rerchercherParmisToutesLesFiches(ctx, autorisations, libelle, codeObjet, "", codeRubrique, codeRattachement, codeRedacteur, idMeta, urlFiche, dateDebutCreation, dateFinCreation, dateDebutModification, dateFinModification, dateDebutMiseEnLigne, dateFinMiseEnLigne, langue, etatObjet, String.valueOf(DatagridUtils.getNombreMaxDatagrid()));
            resultats = mapperResultatRechercheDepuisMeta(resultatRechercheMultifiche.getResultats());
        } catch (final ErreurApplicative e) {
            LOG.error("impossible d'executer la recherche", e);
            resultats = Collections.emptyList();
        }
        return resultats;
    }

    private Date recupererAttributDate(final HttpServletRequest req, final String nomAttribut) {
        Date dateDepuisRequete = DateUtil.parseDate(req.getParameter(nomAttribut));
        if (!Formateur.estSaisie(dateDepuisRequete)) {
            dateDepuisRequete = null;
        }

        return dateDepuisRequete;
    }

    @Override
    protected Predicate retrouvePredicate(final String critereRecherche) {
        return new FiltreFicheUniv(critereRecherche);
    }

}
