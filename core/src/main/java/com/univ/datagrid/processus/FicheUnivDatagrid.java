package com.univ.datagrid.processus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurAsyncException;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.core.config.PropertyHelper;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.bean.ResultatFicheDatagrid;
import com.univ.datagrid.filtre.FiltreFicheUniv;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.url.UrlManager;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.FicheUnivHelper;
import com.univ.utils.URLResolver;

public class FicheUnivDatagrid extends AbstractServiceDatagrid {

    public static final String ID_BEAN = "ficheUnivDatagrid";

    private static final Logger LOG = LoggerFactory.getLogger(FicheUnivDatagrid.class);

    private static final String PARAM_OBJET = "OBJET";

    private ServiceMetatag serviceMetatag;

    private ServiceRubrique serviceRubrique;

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    public void setServiceRubrique(ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }
    /**
     * A partir de l'objet FicheUniv, on retrouve l'ensemble des fiches et on les map dans une liste de résultat exploitable
     *
     * @param fiche la fiche à mapper (avec la requête déjà faite;...)
     * @return la liste des fiches mapper dans un POJO
     * @throws Exception lors de la requete SQL
     */
    public List<ResultatDatagrid> mapperResultatRechercheDepuisFicheUniv(final FicheUniv fiche) throws Exception {
        final List<ResultatDatagrid> resultats = new ArrayList<>();
        final String codeObjet = ReferentielObjets.getCodeObjet(fiche);
        final Map<String, String> libellesParEtat = ReferentielObjets.getEtatsObjet();
        final Map<String, String> classParEtat = FicheUnivHelper.getClassEtatsObjet();
        final Map<String, String> languageParCodeLangue = DatagridUtils.getLanguageParCodeLangue();
        while (fiche.nextItem()) {
            final ResultatFicheDatagrid res = new ResultatFicheDatagrid();
            res.setTypeObjet(codeObjet);
            res.setLibelle(fiche.getLibelleAffichable());
            res.setEtatFiche(libellesParEtat.get(fiche.getEtatObjet()));
            res.setClassEtatFiche(classParEtat.get(fiche.getEtatObjet()));
            res.setId(fiche.getIdFiche());
            res.setCodeFiche(fiche.getCode());
            final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(fiche.getCodeRubrique());
            if (rubrique != null) {
                res.setFilAriane(serviceRubrique.getLabelWithAscendantsLabels(rubrique));
                res.setRubrique(rubrique.getIntitule());
            }
            res.setDateModification(fiche.getDateModification());
            res.setLangue(languageParCodeLangue.get(fiche.getLangue()));
            res.setUrlDrapeauLangue(LangueUtil.getPathImageDrapeau(fiche.getLangue()));
            resultats.add(res);
        }
        return resultats;
    }

    private static FicheUniv instancierFicheUniv(final MetatagBean meta, final Map<String, Objetpartage> objetPartageParCode) {
        final Objetpartage objetCourant = objetPartageParCode.get(meta.getMetaCodeObjet());
        FicheUniv ficheUniv;
        if (objetCourant != null) {
            try {
                ficheUniv = (FicheUniv) Class.forName(objetCourant.getNomClasse()).newInstance();
            } catch (IllegalAccessException | ClassNotFoundException | InstantiationException e) {
                LOG.debug("unable to instanciate the current object", e);
                return null;
            }
        } else {
            return null;
        }
        ficheUniv.setCodeRattachement(meta.getMetaCodeRattachement());
        ficheUniv.setCodeRubrique(meta.getMetaCodeRubrique());
        if (ficheUniv instanceof FicheRattachementsSecondaires) {
            ((FicheRattachementsSecondaires) ficheUniv).setCodeRattachementAutres(Chaine.convertirPointsVirgulesEnAccolades(meta.getMetaCodeRattachementAutres()));
        }
        ficheUniv.setCodeRedacteur(meta.getMetaCodeRedacteur());
        ficheUniv.setCodeValidation(meta.getMetaCodeValidation());
        ficheUniv.setCode(meta.getMetaCode());
        ficheUniv.setLangue(meta.getMetaLangue());
        ficheUniv.setEtatObjet(meta.getMetaEtatObjet());
        if (ficheUniv instanceof DiffusionSelective) {
            ((DiffusionSelective) ficheUniv).setDiffusionPublicVise(meta.getMetaDiffusionPublicVise());
            ((DiffusionSelective) ficheUniv).setDiffusionModeRestriction(meta.getMetaDiffusionModeRestriction());
            ((DiffusionSelective) ficheUniv).setDiffusionPublicViseRestriction(meta.getMetaDiffusionPublicViseRestriction());
        }
        return ficheUniv;
    }

    @Override
    public List<ResultatDatagrid> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final String codeFiche = req.getParameter(PARAM_OBJET);
        List<ResultatDatagrid> resultats = null;
        final FicheUniv fiche = FicheUnivHelper.instancierFiche(codeFiche);
        if (fiche != null) {
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            fiche.setCtx(ctx);
            try {
                initialiserControleBo(fiche, ctx);
                final String requete = req.getQueryString() + "&NOMBRE=" + DatagridUtils.getNombreMaxDatagrid();
                fiche.traiterRequete(EscapeString.unescapeURL(requete));
                supprimerControleBO(ctx);
                resultats = mapperResultatRechercheDepuisFicheUniv(fiche);
            } catch (final Exception e) {
                LOG.error("impossible de récuperer les resultats de la recherche", e);
            }
        }
        if (resultats == null) {
            resultats = Collections.emptyList();
        }
        return resultats;
    }

    /**
     * Ajout de contrôle un peu obscur sur la recherche de fiche... Ils sont utilisés dans la construction de la requête pour ajouter des controles sur le périmetre
     *
     * @param fiche la fiche dont on souhaite rajouter les controles
     * @param ctx le contexte courant
     */
    private void initialiserControleBo(final FicheUniv fiche, final ContexteUniv ctx) {
        final String idExtension = ReferentielObjets.getExtension(fiche);
        final String nomObjet = ReferentielObjets.getNomObjet(fiche);
        final String controleBO = PropertyHelper.getProperty(idExtension, "fiche." + nomObjet.toUpperCase() + ".recherche_anonyme");
        ctx.getDatas().put("CONTROLE_PERIMETRE_BO", StringUtils.defaultIfBlank(controleBO, "1"));
        ctx.getDatas().put("AUTORISATIONS", ctx.getAutorisation());
    }

    /**
     * On les supprimes après la requête pour éviter de poluer le ctx
     *
     * @param ctx Le contexte courant
     */
    private void supprimerControleBO(final ContexteUniv ctx) {
        ctx.getDatas().remove("CONTROLE_PERIMETRE_BO");
        ctx.getDatas().remove("AUTORISATIONS");
    }

    @Override
    public void traiterAction(final HttpServletRequest req) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final AutorisationBean autorisations = ctx.getAutorisation();
        final String[] idsFichesString = req.getParameterValues("idsFiches");
        final String action = req.getParameter("ACTION");
        final String typeObjet = req.getParameter("OBJET");
        final List<Long> idsFiches = new ArrayList<>();
        for (String currentId : idsFichesString) {
            if (StringUtils.isNumeric(currentId)) {
                idsFiches.add(Long.parseLong(currentId));
            }
        }
        if (CollectionUtils.isNotEmpty(idsFiches) && StringUtils.isNotBlank(action) && StringUtils.isNotBlank(typeObjet)) {
            try {
                final List<MetatagBean> metas = serviceMetatag.getByCodeAndIdsFiche(ReferentielObjets.getCodeObjet(typeObjet), idsFiches.toArray(new Long[idsFiches.size()]));
                if (metas.size() != idsFiches.size()) {
                    throw new ErreurAsyncException("impossible de supprimer les fiches selectionnées");
                }
                final Collection<Long> idsMetasATraiter = new ArrayList<>();
                for (MetatagBean currentMeta : metas) {
                    idsMetasATraiter.add(currentMeta.getId());
                }
                if ("SUPPRIMER".equals(action)) {
                    FicheUnivHelper.supprimerListeFichesParIdsMeta(idsMetasATraiter, autorisations);
                } else if ("ARCHIVER".equals(action)) {
                    FicheUnivHelper.archiverListeFichesParIdsMeta(idsMetasATraiter, autorisations);
                } else if ("EN_LIGNE".equals(action)) {
                    FicheUnivHelper.mettreEnLigneListeFichesParIdsMeta(idsMetasATraiter, autorisations);
                }
            } catch (final ErreurAsyncException e) {
                LOG.error("Pas le meme nombre de fiches à supprimer que de meta retrouvés", e);
                throw e;
            } catch (final Exception e) {
                LOG.error("impossible de traiter l'action : " + action + " sur les Ids metas : " + StringUtils.join(idsFiches, " ,") + " pour l'utilisateur : " + ctx.getCode(), e);
                throw new ErreurAsyncException(e.getMessage());
            }
        }
    }

    @Override
    protected Predicate retrouvePredicate(final String critereRecherche) {
        return new FiltreFicheUniv(critereRecherche);
    }

    @Override
    public List<ResultatDatagrid> postTraitementResultat(final List<ResultatDatagrid> resultats) {
        if (CollectionUtils.isNotEmpty(resultats)) {
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            final ResultatDatagrid premierResultat = resultats.get(0);
            final String codeObjet = premierResultat.getTypeObjet();
            final AutorisationBean autorisations = ctx.getAutorisation();
            final Map<String, Objetpartage> objetPartageParCode = ReferentielObjets.getReferentiel().getObjetsByCode();
            final Map<Long, ResultatFicheDatagrid> resultatsParId = getResultatParIDFiche(resultats);
            final Set<Long> var = resultatsParId.keySet();
            final List<MetatagBean> metas = serviceMetatag.getByCodeAndIdsFiche(codeObjet, var.toArray(new Long[var.size()]));
            for (MetatagBean currentMeta : metas) {
                final ResultatFicheDatagrid resultat = resultatsParId.get(currentMeta.getMetaIdFiche());
                modifUrlEnLigneResultat(currentMeta, resultat);
                modifUrlsContributionsResultat(autorisations, objetPartageParCode, currentMeta, resultat);
            }
        }
        return resultats;
    }

    private Map<Long, ResultatFicheDatagrid> getResultatParIDFiche(final List<ResultatDatagrid> resultats) {
        final Map<Long, ResultatFicheDatagrid> resultatsParId = new HashMap<>();
        for (final ResultatDatagrid resultatDatagrid : resultats) {
            resultatsParId.put(resultatDatagrid.getId(), (ResultatFicheDatagrid) resultatDatagrid);
        }
        return resultatsParId;
    }

    private void modifUrlsContributionsResultat(final AutorisationBean autorisations, final Map<String, Objetpartage> objetPartageParCode, final MetatagBean leMeta, final ResultatFicheDatagrid resultat) {
        final FicheUniv fiche = instancierFicheUniv(leMeta, objetPartageParCode);
        if (autorisations != null && autorisations.estAutoriseAModifierLaFiche(fiche)) {
            resultat.setUrlModification(DatagridUtils.getUrlActionFiche(leMeta.getMetaCodeObjet(), leMeta.getMetaIdFiche(), "MODIFIER"));
        }
        if (autorisations != null && autorisations.estAutoriseASupprimerLaFiche(fiche) && !EtatFiche.A_SUPPRIMER.getEtat().equals(leMeta.getMetaEtatObjet())) {
            resultat.setUrlSuppression(DatagridUtils.getUrlActionFiche(leMeta.getMetaCodeObjet(), leMeta.getMetaIdFiche(), "SUPPRIMER"));
        }
    }

    private void modifUrlEnLigneResultat(final MetatagBean leMeta, final ResultatFicheDatagrid resultat) {
        if (EtatFiche.EN_LIGNE.getEtat().equals(leMeta.getMetaEtatObjet())) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final InfosSite siteFiche = serviceInfosSite.determineSite(leMeta.getMetaCodeRubrique(), true);
            resultat.setUrl(URLResolver.getAbsoluteUrl(UrlManager.calculerUrlFiche(leMeta), siteFiche, URLResolver.STANDARD_ACTION));
        }
    }

}
