package com.univ.datagrid.processus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.extension.module.composant.ComposantGroupe;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.bean.ResultatGroupeDSI;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.SessionUtil;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class GroupeDSIDatagrid extends AbstractServiceDatagrid {

    public static final String ID_BEAN = "groupeDSIDatagrid";

    private static final Logger LOG = LoggerFactory.getLogger(GroupeDSIDatagrid.class);

    @Override
    public List<ResultatDatagrid> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        if (isPermissionNonValide(autorisations)) {
            return Collections.emptyList();
        }
        List<ResultatDatagrid> resultats = null;
        try {
            final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
            List<GroupeDsiBean> results = serviceGroupeDsi.getByCodeTypeLabelStructureAndCache(req.getParameter("CODE"), req.getParameter("TYPE"), req.getParameter("LIBELLE"), req.getParameter("CODE_STRUCTURE"), StringUtils.EMPTY);
            resultats = mapperResultatRechercheDepuisRubrique(results);
        } catch (final Exception e) {
            LOG.error("impossible de récuperer les resultats de la recherche", e);
        }
        if (resultats == null) {
            resultats = Collections.emptyList();
        }
        return resultats;
    }

    private boolean isPermissionNonValide(final AutorisationBean autorisations) {
        return autorisations == null || !autorisations.isWebMaster() && !autorisations.possedePermission(ComposantGroupe.getPermissionGestion());
    }

    private List<ResultatDatagrid> mapperResultatRechercheDepuisRubrique(final List<GroupeDsiBean> dbResults) throws Exception {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final List<ResultatDatagrid> resultats = new ArrayList<>();
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        for (GroupeDsiBean groupedsi : dbResults) {
            final ResultatGroupeDSI res = new ResultatGroupeDSI();
            res.setLibelle(groupedsi.getLibelle());
            res.setTypeGroupe(LabelUtils.getLibelle("11", groupedsi.getType(), ctx.getLocale()));
            res.setStructure(serviceStructure.getDisplayableLabel(groupedsi.getCodeStructure(), ctx.getLangue()));
            res.setUrlModification(DatagridUtils.getUrlActionGroupeDSI(String.valueOf(groupedsi.getIdGroupedsi()), "MODIFIERPARID"));
            res.setUrlSuppression(DatagridUtils.getUrlActionGroupeDSI(String.valueOf(groupedsi.getIdGroupedsi()), "SUPPRIMERPARID"));
            resultats.add(res);
        }
        return resultats;
    }

    @Override
    protected Predicate retrouvePredicate(final String critereRecherche) {
        return new Predicate() {

            @Override
            public boolean evaluate(final Object object) {
                boolean isARetourner = Boolean.FALSE;
                if (object instanceof ResultatGroupeDSI) {
                    final ResultatGroupeDSI groupes = (ResultatGroupeDSI) object;
                    isARetourner = containsIgnoreCase(groupes.getLibelle(), critereRecherche) || containsIgnoreCase(groupes.getTypeGroupe(), critereRecherche) || containsIgnoreCase(groupes.getStructure(), critereRecherche);
                }
                return isARetourner;
            }
        };
    }
}
