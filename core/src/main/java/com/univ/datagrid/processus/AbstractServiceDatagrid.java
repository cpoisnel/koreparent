package com.univ.datagrid.processus;

import java.lang.reflect.InvocationTargetException;
import java.text.Collator;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.datagrid.bean.CriteresDatagrid;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.utils.ContexteUtil;

/**
 * Classe abstraite à implementer pour réaliser les traitements d'un datagrid
 *
 * @author olivier.camon
 *
 */
public abstract class AbstractServiceDatagrid implements ServiceDatagrid {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractServiceDatagrid.class);

    /**
     * Par défaut, il n'y a pas d'action implémenter, on ne fait rien \o/.
     */
    @Override
    public void traiterAction(final HttpServletRequest req) {}

    /**
     * Depuis la liste fourni en paramètre, on vérifie que les champs du résultats contienent le critère de recherche saisie par l'utilisateur et on ne retourne que ceux qui l'ont.
     * A surcharger pour rajouter d'autre critère
     *
     * @param resultats l'ensemble des résultats de recherche récupérer depuis la bdd
     * @param criteres le critères de filtre fourni depuis le datagrid
     * @return la liste de résultat filtrer en fonction des critères de recherches
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<ResultatDatagrid> filtrerResultats(List<ResultatDatagrid> resultats, final CriteresDatagrid criteres) {
        if (StringUtils.isNotBlank(criteres.getRecherche())) {
            resultats = (List<ResultatDatagrid>) CollectionUtils.select(resultats, retrouvePredicate(criteres.getRecherche()));
        }
        return resultats;
    }

    /**
     * Récupère le prédicat du datagrid courant, ici on ne filtre que sur le libellé. Cette méthode est surchargé sur certaines implémentations.
     *
     * @param critereRecherche
     *            le terme à rechercher dans le libelle
     * @return le prédicat permettant d'évaluer les critères de recherche
     */
    protected Predicate retrouvePredicate(final String critereRecherche) {
        return new Predicate() {

            @Override
            public boolean evaluate(final Object object) {
                return object != null && object instanceof ResultatDatagrid && StringUtils.containsIgnoreCase(((ResultatDatagrid) object).getLibelle(), critereRecherche);
            }
        };
    }

    @Override
    public void trierResultats(final List<ResultatDatagrid> resultats, final CriteresDatagrid criteres) {
        if (StringUtils.isNotBlank(criteres.getNomColonneATrier()) && !resultats.isEmpty()) {
            final Comparator<ResultatDatagrid> comparaison = new CompareGenerique(criteres.getNomColonneATrier());
            Collections.sort(resultats, comparaison);
            if ("DESC".equalsIgnoreCase(criteres.getSensDuTri())) {
                Collections.reverse(resultats);
            }
        }
    }

    /**
     * Par défaut on ne fait aucun post traitement.
     */
    @Override
    public List<ResultatDatagrid> postTraitementResultat(final List<ResultatDatagrid> resultats) {
        return resultats;
    }

    /**
     * Un comparateur générique pour les autres type de champs (int, date ...).
     *
     * @author olivier.camon
     *
     */
    protected static class CompareGenerique implements Comparator<ResultatDatagrid> {

        private final String nomChamp;

        CompareGenerique(final String nomChamp) {
            this.nomChamp = nomChamp;
        }

        @Override
        public int compare(final ResultatDatagrid resultatA, final ResultatDatagrid resultatB) {
            int retour = 0;
            final Collator localeCollator = Collator.getInstance(ContexteUtil.getContexteUniv().getLocale());
            localeCollator.setStrength(Collator.SECONDARY);
            try {
                Object fieldValueA = PropertyUtils.getProperty(resultatA, nomChamp);
                Object fieldValueB = PropertyUtils.getProperty(resultatB, nomChamp);
                if (fieldValueA == null && fieldValueB == null) {
                    retour = 0;
                } else if (fieldValueA == null) {
                    retour = -1;
                } else if (fieldValueB == null) {
                    retour = 1;
                } else {
                    if (fieldValueA instanceof Date) {
                        retour = ((Date) fieldValueA).compareTo((Date) fieldValueB);
                    } else if (fieldValueA instanceof Collection && !((Collection<? extends Comparable<?>>) fieldValueA).isEmpty() && !((Collection<? extends Comparable>) fieldValueA).isEmpty() ){
                        retour = ((Collection<? extends Comparable>) fieldValueA).iterator().next().compareTo(((Collection<? extends Comparable<?>>) fieldValueB).iterator().next());
                    } else if (fieldValueA instanceof String) {
                        retour = localeCollator.compare(fieldValueA, fieldValueB);
                    } else if (fieldValueA instanceof Comparable) {
                        retour = ((Comparable)fieldValueA).compareTo(fieldValueB);
                    } else {
                        retour = 0;
                    }
                }
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                LOG.debug("unable to find the field " + nomChamp, e);
                return 0;
            }
            return retour;
        }
    }
}
