package com.univ.datagrid.processus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.cms.objetspartages.Objetpartage;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.bean.ResultatFicheDatagrid;
import com.univ.datagrid.filtre.FiltreFicheUniv;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.url.UrlManager;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.DateUtil;
import com.univ.utils.FicheUnivHelper;
import com.univ.utils.SessionUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.recherche.RechercheMultificheHelper;
import com.univ.utils.recherche.ResultatRechercheMultifiche;

public class MultiFicheDatagrid extends AbstractServiceDatagrid {

    /** l'id Spring du bean. */
    public static final String ID_BEAN = "multiFicheDatagrid";

    private static final Logger LOG = LoggerFactory.getLogger(MultiFicheDatagrid.class);

    private ServiceMetatag serviceMetatag;

    private ServiceRubrique serviceRubrique;

    public void setServiceRubrique(ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    public List<ResultatDatagrid> mapperResultatRechercheDepuisMeta(final Collection<MetatagBean> metatags) {
        final List<ResultatDatagrid> resultats = new ArrayList<>();
        final Map<String, String> libellesParEtat = ReferentielObjets.getEtatsObjet();
        final Map<String, String> classParEtat = FicheUnivHelper.getClassEtatsObjet();
        for (final MetatagBean leMeta : metatags) {
            final ResultatFicheDatagrid resultat = new ResultatFicheDatagrid();
            final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(leMeta.getMetaCodeRubrique());
            if (rubrique != null) {
                resultat.setFilAriane(serviceRubrique.getLabelWithAscendantsLabels(rubrique));
                resultat.setRubrique(rubrique.getIntitule());
            } else {
                resultat.setFilAriane(StringUtils.EMPTY);
                resultat.setRubrique(StringUtils.EMPTY);
            }
            resultat.setId(leMeta.getId());
            resultat.setLibelle(leMeta.getMetaLibelleFiche());
            resultat.setEtatFiche(libellesParEtat.get(leMeta.getMetaEtatObjet()));
            resultat.setClassEtatFiche(classParEtat.get(leMeta.getMetaEtatObjet()));
            resultat.setLangue(LangueUtil.getLocale(leMeta.getMetaLangue()).getLanguage());
            resultat.setUrlDrapeauLangue(LangueUtil.getPathImageDrapeau(leMeta.getMetaLangue()));
            resultat.setTypeObjet(ReferentielObjets.getLibelleObjet(leMeta.getMetaCodeObjet()));
            resultat.setDateModification(leMeta.getMetaDateModification());
            resultats.add(resultat);
        }
        return resultats;
    }

    private static FicheUniv instancierFicheUniv(final MetatagBean meta, final Map<String, Objetpartage> objetPartageParCode) {
        final Objetpartage objetCourant = objetPartageParCode.get(meta.getMetaCodeObjet());
        FicheUniv ficheUniv;
        if (objetCourant != null) {
            try {
                ficheUniv = (FicheUniv) Class.forName(objetCourant.getNomClasse()).newInstance();
            } catch (IllegalAccessException | ClassNotFoundException | InstantiationException e) {
                LOG.debug("unable to instanciate the current object", e);
                return null;
            }
        } else {
            return null;
        }
        ficheUniv.setCodeRattachement(meta.getMetaCodeRattachement());
        ficheUniv.setCodeRubrique(meta.getMetaCodeRubrique());
        if (ficheUniv instanceof FicheRattachementsSecondaires) {
            ((FicheRattachementsSecondaires) ficheUniv).setCodeRattachementAutres(Chaine.convertirPointsVirgulesEnAccolades(meta.getMetaCodeRattachementAutres()));
        }
        ficheUniv.setCodeRedacteur(meta.getMetaCodeRedacteur());
        ficheUniv.setCodeValidation(meta.getMetaCodeValidation());
        ficheUniv.setCode(meta.getMetaCode());
        ficheUniv.setLangue(meta.getMetaLangue());
        ficheUniv.setEtatObjet(meta.getMetaEtatObjet());
        if (ficheUniv instanceof DiffusionSelective) {
            ((DiffusionSelective) ficheUniv).setDiffusionPublicVise(meta.getMetaDiffusionPublicVise());
            ((DiffusionSelective) ficheUniv).setDiffusionModeRestriction(meta.getMetaDiffusionModeRestriction());
            ((DiffusionSelective) ficheUniv).setDiffusionPublicViseRestriction(meta.getMetaDiffusionPublicViseRestriction());
        }
        return ficheUniv;
    }

    @Override
    public List<ResultatDatagrid> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            return Collections.emptyList();
        }
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final String libelle = req.getParameter("TITRE");
        final String codeObjet = req.getParameter("CODE_OBJET");
        final String codeFiche = req.getParameter("CODE_FICHE");
        final String codeRubrique = req.getParameter("CODE_RUBRIQUE_RECHERCHE");
        final String codeRattachement = req.getParameter("CODE_RATTACHEMENT");
        final String codeRedacteur = req.getParameter("CODE_REDACTEUR");
        final String idMeta = req.getParameter("ID_META");
        final String urlFiche = req.getParameter("URL_FICHE");
        final String langue = req.getParameter("LANGUE");
        final String etatObjet = req.getParameter("ETAT_OBJET");
        // Récupération des critère de recherche de date
        final Date dateDebutCreation = recupererAttributDate(req, "DATE_CREATION_DEBUT");
        final Date dateFinCreation = recupererAttributDate(req, "DATE_CREATION_FIN");
        final Date dateDebutModification = recupererAttributDate(req, "DATE_MODIFICATION_DEBUT");
        final Date dateFinModification = recupererAttributDate(req, "DATE_MODIFICATION_FIN");
        final Date dateDebutMiseEnLigne = recupererAttributDate(req, "DATE_MISE_EN_LIGNE_DEBUT");
        final Date dateFinMiseEnLigne = recupererAttributDate(req, "DATE_MISE_EN_LIGNE_FIN");
        ResultatRechercheMultifiche resultatRechercheMultifiche;
        List<ResultatDatagrid> resultats;
        try {
            resultatRechercheMultifiche = RechercheMultificheHelper.rerchercherParmisToutesLesFiches(ctx, autorisations, libelle, codeObjet, codeFiche, codeRubrique, codeRattachement, codeRedacteur, idMeta, urlFiche, dateDebutCreation, dateFinCreation, dateDebutModification, dateFinModification, dateDebutMiseEnLigne, dateFinMiseEnLigne, langue, etatObjet, String.valueOf(DatagridUtils.getNombreMaxDatagrid()));
            resultats = mapperResultatRechercheDepuisMeta(resultatRechercheMultifiche.getResultats());
        } catch (final ErreurApplicative e) {
            LOG.error("impossible d'executer la recherche", e);
            return Collections.emptyList();
        }
        return resultats;
    }

    private Date recupererAttributDate(final HttpServletRequest req, final String nomAttribut) {
        Date dateDepuisRequete = DateUtil.parseDate(req.getParameter(nomAttribut));
        if (!Formateur.estSaisie(dateDepuisRequete)) {
            dateDepuisRequete = null;
        }
        return dateDepuisRequete;
    }

    @Override
    public void traiterAction(final HttpServletRequest req) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final AutorisationBean autorisations = ctx.getAutorisation();
        final String[] idsMetas = req.getParameterValues("idsMetas");
        final String action = req.getParameter("ACTION");
        if (idsMetas != null && idsMetas.length > 0 && StringUtils.isNotBlank(action)) {
            @SuppressWarnings("unchecked")
            final Collection<Long> idsMetasATraiter = CollectionUtils.collect(Arrays.asList(idsMetas), new Transformer() {

                @Override
                public Object transform(final Object input) {
                    return Long.valueOf((String) input);
                }
            });
            try {
                if ("SUPPRIMER".equals(action)) {
                    FicheUnivHelper.supprimerListeFichesParIdsMeta(idsMetasATraiter, autorisations);
                } else if ("ARCHIVER".equals(action)) {
                    FicheUnivHelper.archiverListeFichesParIdsMeta(idsMetasATraiter, autorisations);
                } else if ("EN_LIGNE".equals(action)) {
                    FicheUnivHelper.mettreEnLigneListeFichesParIdsMeta(idsMetasATraiter, autorisations);
                }
            } catch (final Exception e) {
                LOG.error("impossible de traiter l'action : " + action + " sur les Ids metas : " + StringUtils.join(idsMetas, " ,") + " pour l'utilisateur : " + ctx.getCode(), e);
            }
        }
    }

    @Override
    protected Predicate retrouvePredicate(final String critereRecherche) {
        return new FiltreFicheUniv(critereRecherche);
    }

    @Override
    public List<ResultatDatagrid> postTraitementResultat(final List<ResultatDatagrid> resultats) {
        if (CollectionUtils.isNotEmpty(resultats)) {
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            final Map<Long, ResultatFicheDatagrid> resultatsParId = getResultatParIDMetatag(resultats);
            final AutorisationBean autorisations = ctx.getAutorisation();
            final Map<String, Objetpartage> objetPartageParCode = ReferentielObjets.getReferentiel().getObjetsByCode();
            Set<Long> var = resultatsParId.keySet();
            final List<MetatagBean> metas = serviceMetatag.getByIds(var.toArray(new Long[var.size()]));
            for (MetatagBean currentMeta : metas) {
                final ResultatFicheDatagrid resultat = resultatsParId.get(currentMeta.getId());
                modifUrlEnLigneResultat(currentMeta, resultat);
                modifUrlsContributionsResultat(autorisations, objetPartageParCode, currentMeta, resultat);
            }
        }
        return resultats;
    }

    private void modifUrlsContributionsResultat(final AutorisationBean autorisations, final Map<String, Objetpartage> objetPartageParCode, final MetatagBean leMeta, final ResultatFicheDatagrid resultat) {
        final FicheUniv fiche = instancierFicheUniv(leMeta, objetPartageParCode);
        if (autorisations != null && autorisations.estAutoriseAModifierLaFiche(fiche)) {
            resultat.setUrlModification(DatagridUtils.getUrlActionFiche(leMeta.getMetaCodeObjet(), leMeta.getMetaIdFiche(), "MODIFIER"));
        }
        if (autorisations != null && autorisations.estAutoriseASupprimerLaFiche(fiche) && !EtatFiche.A_SUPPRIMER.getEtat().equals(leMeta.getMetaEtatObjet())) {
            resultat.setUrlSuppression(DatagridUtils.getUrlActionFiche(leMeta.getMetaCodeObjet(), leMeta.getMetaIdFiche(), "SUPPRIMER"));
        }
    }

    private void modifUrlEnLigneResultat(final MetatagBean leMeta, final ResultatFicheDatagrid resultat) {
        if (EtatFiche.EN_LIGNE.getEtat().equals(leMeta.getMetaEtatObjet())) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final InfosSite siteFiche = serviceInfosSite.determineSite(leMeta.getMetaCodeRubrique(), true);
            resultat.setUrl(URLResolver.getAbsoluteUrl(UrlManager.calculerUrlFiche(leMeta), siteFiche, URLResolver.STANDARD_ACTION));
        }
    }

    private Map<Long, ResultatFicheDatagrid> getResultatParIDMetatag(final List<ResultatDatagrid> resultats) {
        final Map<Long, ResultatFicheDatagrid> resultatsParId = new HashMap<>();
        for (final ResultatDatagrid resultatDatagrid : resultats) {
            resultatsParId.put(resultatDatagrid.getId(), (ResultatFicheDatagrid) resultatDatagrid);
        }
        return resultatsParId;
    }

}
