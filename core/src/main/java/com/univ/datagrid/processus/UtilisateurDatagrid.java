package com.univ.datagrid.processus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantUtilisateur;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.bean.ResultatUtilisateur;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.SessionUtil;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class UtilisateurDatagrid extends AbstractServiceDatagrid {

    public static final String ID_BEAN = "utilisateurDatagrid";

    private ServiceUser serviceUser;

    private ServiceGroupeUtilisateur serviceGroupeUtilisateur;

    public void setServiceUser(ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    public void setServiceGroupeUtilisateur(ServiceGroupeUtilisateur serviceGroupeUtilisateur) {
        this.serviceGroupeUtilisateur = serviceGroupeUtilisateur;
    }

    @Override
    public List<ResultatDatagrid> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        if (isPermissionNonValide(autorisations)) {
            return Collections.emptyList();
        }
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final String code = req.getParameter("CODE");
        final String nom = req.getParameter("NOM");
        final String prenom = req.getParameter("PRENOM");
        final String profil = req.getParameter("PROFIL_DSI");
        final String groupe = req.getParameter("GROUPE_DSI");
        final String codeRattachement = req.getParameter("CODE_RATTACHEMENT");
        final String adresseMail = req.getParameter("ADRESSE_MAIL");
        final List<UtilisateurBean> users = serviceUser.select(code, nom, prenom, StringUtils.EMPTY, profil, groupe, codeRattachement, adresseMail);
        return mapperResultatRecherche(users, groupe, ctx.getLangue(), autorisations);
    }

    private boolean isPermissionNonValide(final AutorisationBean autorisations) {
        return autorisations == null || !autorisations.isWebMaster() && !autorisations.possedePermission(ComposantUtilisateur.getPermissionConsultation());
    }

    private List<ResultatDatagrid> mapperResultatRecherche(final List<UtilisateurBean> users, final String groupe, final String langue, final AutorisationBean autorisations) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final List<ResultatDatagrid> resultats = new ArrayList<>();
        final boolean isUtilisateurGestion = autorisations.possedePermission(ComposantUtilisateur.getPermissionGestion()) || autorisations.isWebMaster();
        for (UtilisateurBean currentUser : users) {
            final ResultatUtilisateur res = new ResultatUtilisateur();
            res.setId(currentUser.getId());
            res.setCode(currentUser.getCode());
            res.setCivilite(currentUser.getCivilite());
            res.setNom(currentUser.getNom());
            res.setMail(currentUser.getAdresseMail());
            res.setLibelle(String.format("%s %s", currentUser.getNom(), currentUser.getPrenom()));
            res.setPrenom(currentUser.getPrenom());
            res.setArboStructure(serviceStructure.getBreadCrumbs(currentUser.getCodeRattachement(), langue));
            final StructureModele info = serviceStructure.getByCodeLanguage(currentUser.getCodeRattachement(), langue);
            if(info != null) {
                res.setStructure(info.getLibelleAffichable());
            }
            if (isUtilisateurGestion) {
                final StringBuilder baseUrlAction = new StringBuilder(WebAppUtil.SG_PATH + "?EXT=core&PROC=SAISIE_UTILISATEUR&ID=");
                baseUrlAction.append(String.valueOf(currentUser.getId()));
                baseUrlAction.append("&ACTION=");
                res.setUrlModification(baseUrlAction.toString() + "MODIFIERPARID");
                res.setUrlSuppression(baseUrlAction.toString() + "SUPPRIMERPARID");
                res.setUrlSuppressionGroupe(WebAppUtil.SG_PATH + "?EXT=core&PROC=SAISIE_GROUPEDSI&ACTION=SUPPRIMERDUGROUPE&GROUPE_DSI=" + groupe + "&UTILISATEUR=" + currentUser.getId());
            }
            resultats.add(res);
        }
        return resultats;
    }

    @Override
    public void traiterAction(final HttpServletRequest req) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final String[] idsUtilisateur = req.getParameterValues("idsUtilisateur");
        final String action = req.getParameter("ACTION");
        final AutorisationBean autorisation = ctx.getAutorisation();
        if (isActionValide(action, autorisation, idsUtilisateur)) {
            @SuppressWarnings("unchecked")
            final Collection<Long> idsMetasATraiter = CollectionUtils.collect(Arrays.asList(idsUtilisateur), new Transformer() {

                @Override
                public Object transform(final Object input) {
                    return Long.valueOf((String) input);
                }
            });
            if ("SUPPRIMER".equals(action)) {
                for (final Long idUser : idsMetasATraiter) {
                    final UtilisateurBean utilisateur = serviceUser.getById(idUser);
                    if (utilisateur != null) {
                        serviceGroupeUtilisateur.deleteByUser(utilisateur.getCode());
                        serviceUser.delete(idUser);
                    }
                }
            }
        }
    }

    private boolean isActionValide(final String action, final AutorisationBean autorisation, final String[] idsUtilisateur) {
        final boolean isAutorise = autorisation != null && (autorisation.possedePermission(ComposantUtilisateur.getPermissionGestion()) || autorisation.isWebMaster());
        final boolean isUtilisateursValide = idsUtilisateur != null && idsUtilisateur.length > 0;
        return isAutorise && isUtilisateursValide && StringUtils.isNotBlank(action);
    }

    @Override
    protected Predicate retrouvePredicate(final String critereRecherche) {
        return new Predicate() {

            @Override
            public boolean evaluate(final Object object) {
                boolean isARetourner = Boolean.FALSE;
                if (object instanceof ResultatUtilisateur) {
                    final ResultatUtilisateur groupes = (ResultatUtilisateur) object;
                    isARetourner = containsIgnoreCase(groupes.getLibelle(), critereRecherche) || containsIgnoreCase(groupes.getMail(), critereRecherche) || containsIgnoreCase(groupes.getStructure(), critereRecherche) || containsIgnoreCase(groupes.getCode(), critereRecherche);
                }
                return isARetourner;
            }
        };
    }
}
