package com.univ.datagrid.processus;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kportal.core.config.MessageHelper;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.bean.ResultatFicheDatagrid;
import com.univ.datagrid.filtre.FiltreFicheUniv;
import com.univ.datagrid.servlet.LienPopupServlet;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.FicheUnivHelper;

public class FicheToolboxDatagrid extends AbstractServiceDatagrid {

    public static final String ID_BEAN = "ficheToolboxDatagrid";

    private static final Logger LOG = LoggerFactory.getLogger(FicheUnivDatagrid.class);

    private static final String PARAM_OBJET = "OBJET";

    private ServiceRubrique serviceRubrique;

    public void setServiceRubrique(ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }
    /**
     * Mappe les résultats d'une requete sur un objet FicheUniv dans une liste de bean exploitable
     *
     * @param fiche
     * @return
     * @throws Exception
     */
    public List<ResultatDatagrid> mapperResultatRechercheDepuisFicheUniv(final FicheUniv fiche) throws Exception {
        final List<ResultatDatagrid> resultats = new ArrayList<>();
        while (fiche.nextItem()) {
            final ResultatFicheDatagrid res = new ResultatFicheDatagrid();
            res.setId(fiche.getIdFiche());
            res.setTypeObjet(ReferentielObjets.getCodeObjet(fiche));
            res.setLangue(LangueUtil.getLocale(fiche.getLangue()).getLanguage());
            res.setUrlDrapeauLangue(LangueUtil.getPathImageDrapeau(fiche.getLangue()));
            res.setLibelle(fiche.getLibelleAffichable());
            res.setCodeFiche(fiche.getCode());
            res.setEtatFiche(MessageHelper.getCoreMessage("ETATFICHE_" + fiche.getEtatObjet()));
            final String codeObjet = ReferentielObjets.getCodeObjet(fiche);
            final StringBuilder lienToolbox = new StringBuilder(LienPopupServlet.LIEN_SERVLET).append("?");
            lienToolbox.append(LienPopupServlet.CODE_OBJET).append("=").append(fiche.getCode()).append("&amp;");
            lienToolbox.append(LienPopupServlet.TYPE_OBJET).append("=").append(ReferentielObjets.getNomObjet(codeObjet)).append("&amp;");
            lienToolbox.append(LienPopupServlet.LANGUE).append("=").append(fiche.getLangue()).append("&amp;");
            lienToolbox.append(LienPopupServlet.LIBELLE).append("=").append(EscapeString.escapeURL(fiche.getLibelleAffichable()));
            final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(fiche.getCode());
            if (rubrique != null) {
                res.setRubrique(rubrique.getIntitule());
                res.setFilAriane(serviceRubrique.getLabelWithAscendantsLabels(rubrique));
            }
            try {
                final Method m = fiche.getClass().getMethod("getUrl");
                final String url = (String) m.invoke(fiche);
                lienToolbox.append("&amp;").append(LienPopupServlet.URL).append("=").append(url);
            } catch (final ReflectiveOperationException | IllegalArgumentException e) {
                LOG.debug("unable to invoke the current method", e);
            }
            res.setUrl(lienToolbox.toString());
            resultats.add(res);
        }
        return resultats;
    }

    @Override
    public List<ResultatDatagrid> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final String codeFiche = req.getParameter(PARAM_OBJET);
        List<ResultatDatagrid> resultats = null;
        final FicheUniv fiche = FicheUnivHelper.instancierFiche(codeFiche);
        if (fiche != null) {
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            fiche.setCtx(ctx);
            try {
                final String requete = req.getQueryString() + "&NOMBRE=" + DatagridUtils.getNombreMaxDatagrid();
                fiche.traiterRequete(EscapeString.unescapeURL(requete));
                resultats = mapperResultatRechercheDepuisFicheUniv(fiche);
            } catch (final Exception e) {
                LOG.error("impossible de récuperer les resultats de la recherche", e);
            }
        }
        if (resultats == null) {
            resultats = Collections.emptyList();
        }
        return resultats;
    }

    @Override
    protected Predicate retrouvePredicate(final String critereRecherche) {
        return new FiltreFicheUniv(critereRecherche);
    }

}
