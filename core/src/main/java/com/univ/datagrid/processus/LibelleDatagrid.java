package com.univ.datagrid.processus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kportal.extension.module.composant.ComposantLibelle;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.bean.ResultatLibelle;
import com.univ.datagrid.bean.ResultatLibelle.LibelleParLangue;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.cache.CacheLibelleManager;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.services.ServiceLabel;
import com.univ.utils.SessionUtil;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class LibelleDatagrid extends AbstractServiceDatagrid {

    public static final String ID_BEAN = "libelleDatagrid";

    private final CacheLibelleManager cache = (CacheLibelleManager) ApplicationContextManager.getCoreContextBean(CacheLibelleManager.ID_BEAN);

    private ServiceLabel serviceLabel;

    public void setServiceLabel(final ServiceLabel serviceLabel) {
        this.serviceLabel = serviceLabel;
    }

    @Override
    public List<ResultatDatagrid> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        if (isPermissionNonValide(autorisations)) {
            return Collections.emptyList();
        }
        final RequeteSQL requete = constuireRequete(req);
        final List<LabelBean> labels = serviceLabel.getFromRequest(requete);
        return mapperResultatRechercheDepuisLibelle(labels);
    }

    private RequeteSQL constuireRequete(final HttpServletRequest req) {
        final RequeteSQL requete = new RequeteSQL();
        final ClauseWhere where = new ClauseWhere();
        final String libelle = req.getParameter("LIBELLE");
        if (StringUtils.isNotBlank(libelle)) {
            where.setPremiereCondition(ConditionHelper.egalVarchar("LIBELLE", libelle));
        }
        final String code = req.getParameter("CODE");
        if (StringUtils.isNotBlank(code)) {
            where.and(ConditionHelper.egalVarchar("CODE", code));
        }
        final String type = req.getParameter("TYPE");
        if (StringUtils.isNotBlank(type) && !"0000".equals(type)) {
            where.and(ConditionHelper.egalVarchar("TYPE", type));
        }
        final String langue = req.getParameter("LANGUE");
        if (StringUtils.isNotBlank(langue) && !"0000".equals(langue)) {
            where.and(ConditionHelper.egalVarchar("LANGUE", langue));
        }
        final ClauseOrderBy orderBy = new ClauseOrderBy();
        orderBy.orderBy("TYPE", SensDeTri.ASC).orderBy("LANGUE", SensDeTri.ASC).orderBy("LIBELLE", SensDeTri.ASC);
        requete.where(where);
        requete.orderBy(orderBy);
        return requete;
    }

    private List<ResultatDatagrid> mapperResultatRechercheDepuisLibelle(final List<LabelBean> labels) {
        final List<ResultatDatagrid> resultats = new ArrayList<>();
        final Map<String, ResultatLibelle> libellesParTypeCode = new HashMap<>();
        for (LabelBean currentLabel : labels) {
            final ResultatLibelle lib = new ResultatLibelle();
            lib.setCode(currentLabel.getCode());
            lib.setType(cache.getListeTypesLibelles().get(currentLabel.getType()));
            lib.setUrlModification(DatagridUtils.getUrlActionLibelle(String.valueOf(currentLabel.getId()), "MODIFIERPARID"));
            lib.setUrlSuppression(DatagridUtils.getUrlActionLibelle(String.valueOf(currentLabel.getId()), "SUPPRIMERPARID"));
            if (!libellesParTypeCode.containsKey(currentLabel.getType() + currentLabel.getCode())) {
                lib.addLibelleParLangue(currentLabel.getLangue(), currentLabel.getLibelle());
                libellesParTypeCode.put(currentLabel.getType() + currentLabel.getCode(), lib);
            } else {
                final ResultatLibelle libelleCourant = libellesParTypeCode.get(currentLabel.getType() + currentLabel.getCode());
                libelleCourant.addLibelleParLangue(currentLabel.getLangue(), currentLabel.getLibelle());
            }
        }
        resultats.addAll(libellesParTypeCode.values());
        return resultats;
    }

    private boolean isPermissionNonValide(final AutorisationBean autorisations) {
        return autorisations == null || !autorisations.isWebMaster() && !autorisations.possedePermission(ComposantLibelle.getPermissionGestion());
    }

    @Override
    protected Predicate retrouvePredicate(final String critereRecherche) {
        return new Predicate() {

            @Override
            public boolean evaluate(final Object object) {
                boolean isARetourner = Boolean.FALSE;
                if (object instanceof ResultatLibelle) {
                    final ResultatLibelle libelle = (ResultatLibelle) object;
                    isARetourner = containsIgnoreCase(libelle.getType(), critereRecherche) || containsIgnoreCase(libelle.getCode(), critereRecherche);
                    if (!isARetourner) {
                        for (final LibelleParLangue libParLangue : libelle.getLibelleParLangue()) {
                            isARetourner = containsIgnoreCase(libParLangue.getLibelle(), critereRecherche) || containsIgnoreCase(libParLangue.getLangue(), critereRecherche);
                            if (isARetourner) {
                                break;
                            }
                        }
                    }
                }
                return isARetourner;
            }
        };
    }
}
