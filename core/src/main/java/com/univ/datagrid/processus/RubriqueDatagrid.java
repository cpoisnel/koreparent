package com.univ.datagrid.processus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.bean.ResultatRubrique;
import com.univ.datagrid.utils.DatagridUtils;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.ContexteUtil;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class RubriqueDatagrid extends AbstractServiceDatagrid {

    public static final String ID_BEAN = "rubriqueDatagrid";

    private ServiceRubrique serviceRubrique;

    public void setServiceRubrique(ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }
    /**
     * A partir de l'objet Rubrique, on retrouve l'ensemble des rubriques et on les map dans une liste de résultat exploitable
     *
     * @param requestResults
     * @return
     */
    public List<ResultatDatagrid> mapperResultatRechercheDepuisRubrique(final List<RubriqueBean> requestResults) {
        final List<ResultatDatagrid> resultats = new ArrayList<>();
        final AutorisationBean autorisations = ContexteUtil.getContexteUniv().getAutorisation();
        final Map<String, String> languageParCodeLangue = DatagridUtils.getLanguageParCodeLangue();
        final PermissionBean permissionSuppressionRubrique = new PermissionBean("TECH", "rub", "S");
        final PermissionBean permissionModificationRubrique = new PermissionBean("TECH", "rub", "M");
        for (RubriqueBean rubriqueBean : requestResults) {
            final Perimetre perimetreRubriqueCourante = new Perimetre("*", rubriqueBean.getCode(), "*", "*", "");
            if (autorisations != null && (autorisations.possedePermission(permissionModificationRubrique, perimetreRubriqueCourante) || autorisations.possedePermission(permissionSuppressionRubrique, perimetreRubriqueCourante))) {
                final ResultatRubrique res = new ResultatRubrique();
                res.setLibelle(rubriqueBean.getIntitule());
                res.setCode(rubriqueBean.getCode());
                res.setLangue(languageParCodeLangue.get(rubriqueBean.getLangue()));
                res.setUrlDrapeauLangue(LangueUtil.getPathImageDrapeau(rubriqueBean.getLangue()));
                if (autorisations.possedePermission(permissionModificationRubrique, perimetreRubriqueCourante)) {
                    res.setUrlModification(DatagridUtils.getUrlActionRubrique(rubriqueBean.getCode(), "MODIFIERRUBRIQUEPARCODE"));
                }
                if (autorisations.possedePermission(permissionSuppressionRubrique, perimetreRubriqueCourante)) {
                    res.setUrlSuppression(DatagridUtils.getUrlActionRubrique(rubriqueBean.getCode(), "SUPPRIMERRUBRIQUEPARCODE"));
                }
                resultats.add(res);
            }
        }
        return resultats;
    }

    @Override
    public List<ResultatDatagrid> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        final String codeForRequest = StringUtils.defaultIfEmpty(req.getParameter("CODE"), req.getParameter("CODE_SAISIE"));
        final List<RubriqueBean> results = serviceRubrique.getRubriqueByCodeLanguageLabelCategory(codeForRequest, req.getParameter("LANGUE"), req.getParameter("INTITULE"), req.getParameter("CATEGORIE"));
        return mapperResultatRechercheDepuisRubrique(results);
    }

    @Override
    protected Predicate retrouvePredicate(final String critereRecherche) {
        return new Predicate() {

            @Override
            public boolean evaluate(final Object object) {
                boolean isARetourner = Boolean.FALSE;
                if (object instanceof ResultatRubrique) {
                    final ResultatRubrique rubrique = (ResultatRubrique) object;
                    isARetourner = containsIgnoreCase(rubrique.getLibelle(), critereRecherche) || containsIgnoreCase(rubrique.getRubriqueMere(), critereRecherche) || containsIgnoreCase(rubrique.getLangue(), critereRecherche) || containsIgnoreCase(rubrique.getCode(), critereRecherche);
                }
                return isARetourner;
            }
        };
    }


    /**
     * Par défaut on ne fait aucun post traitement.
     */
    @Override
    public List<ResultatDatagrid> postTraitementResultat(final List<ResultatDatagrid> resultats) {
        for (ResultatDatagrid currentResult : resultats) {
            if (currentResult instanceof ResultatRubrique) {
                final ResultatRubrique sectionResult = (ResultatRubrique) currentResult;
                sectionResult.setRubriqueMere(serviceRubrique.getAscendantsLabelOnly(sectionResult.getCode()));
            }
        }
        return resultats;
    }


}
