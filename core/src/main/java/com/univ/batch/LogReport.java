package com.univ.batch;

import java.io.File;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.slf4j.MDC;

import com.jsbsoft.jtf.email.JSBMailbox;
import com.kportal.core.cluster.ClusterHelper;
import com.kportal.core.config.PropertyHelper;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

public class LogReport {

    public static void initLoggerValue(final LogReporter logReporter) {
        StringBuilder logContext = new StringBuilder();
        logContext.append(logReporter.getLogFileName());
        if (logReporter.isReportByMail()) {
            logContext.append("-").append(System.currentTimeMillis());
        }
        MDC.put("log-context", logContext.toString());
    }

    public static void sendReportByMail(final Logger logger, final LogReporter logReporter) {
        FileAppender<ILoggingEvent> appenderMail = (FileAppender<ILoggingEvent>) logger.getAppender(MDC.get("log-context"));
        try {
            if (appenderMail != null) {
                File fLog = new File(appenderMail.getFile());
                if (fLog.exists()) {
                    JSBMailbox mailbox = new JSBMailbox(false);
                    String to = PropertyHelper.getCoreProperty("batch." + logReporter.getLogFileName() + ".mail.to");
                    if (StringUtils.isEmpty(to)) {
                        to = PropertyHelper.getCoreProperty("mail.webmaster");
                    }
                    mailbox.sendSystemMsgWithAttachedFiles(to, logReporter.getReportSubject(), "Ci-joint le rapport d'exécution du script " + logReporter.getLogFileName()  + ".", new String[] {appenderMail.getFile()});
                }
            }
        } catch (MessagingException | EmailException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (appenderMail != null) {
                logger.detachAppender(appenderMail);
                appenderMail.stop();
            }
        }
    }
    public static void clearLoggerContext() {
        MDC.remove("log-context");
    }
}
