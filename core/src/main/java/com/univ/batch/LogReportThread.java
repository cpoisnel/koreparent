package com.univ.batch;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class LogReportThread implements Runnable, LogReporter {

    private static final Logger LOG = LoggerFactory.getLogger(LogReportThread.class);

    @Override
    public void run() {
        // creation de l'objet logReport
        LogReport.initLoggerValue(this);
        // indication automatique du début du batch
        Long start = System.currentTimeMillis();
        LOG.info("DEBUT " + this.getClass().getSimpleName());
        // exécution du batch
        perform();
        // indication automatique de la fin du batch et durée
        Long duree = System.currentTimeMillis() - start;
        LOG.info("FIN " + this.getClass().getSimpleName() + ", durée : " + String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(duree), TimeUnit.MILLISECONDS.toMinutes(duree) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duree)), TimeUnit.MILLISECONDS.toSeconds(duree) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duree))));
        // finalisation du report
        if (isReportByMail()) {
            LogReport.sendReportByMail((ch.qos.logback.classic.Logger) LOG, this);
        }
        LogReport.clearLoggerContext();
    }

    public abstract void perform();

    /**
     * Par défaut on revoit le nom de la classe en minuscule
     */
    @Override
    public String getLogFileName() {
        return this.getClass().getSimpleName().toLowerCase();
    }


    /**
     * Par défaut on revoit vide
     */
    @Override
    public String getLogDescription() {
        return StringUtils.EMPTY;
    }


    @Override
    public String getReportSubject() {
        return StringUtils.EMPTY;
    }

    @Override
    public boolean isReportByMail() {
        return false;
    }
}
