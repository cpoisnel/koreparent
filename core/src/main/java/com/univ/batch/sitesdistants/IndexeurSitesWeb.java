package com.univ.batch.sitesdistants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.textsearch.sitesdistants.IndexeurSitesDistants;
import com.kportal.scheduling.spring.quartz.LogReportJob;
// TODO: Auto-generated Javadoc

/**
 * Lance l'indexation des sites en base.
 *
 * @author jbiard
 */
public class IndexeurSitesWeb extends LogReportJob {

    private static final Logger LOG = LoggerFactory.getLogger(IndexeurSitesWeb.class);

    public void run() {
        IndexeurSitesDistants.getInstance().setLogger(LOG);
        IndexeurSitesDistants.getInstance().indexeSites();
    }

    @Override
    public void perform() {
        run();
    }
}
