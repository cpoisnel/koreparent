package com.univ.batch.process;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.mail.MessagingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.email.JSBMailbox;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.webutils.ContextePage;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper;
import com.kportal.scheduling.spring.quartz.LogReportJob;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.multisites.InfosFicheComparator;
import com.univ.multisites.InfosFicheReferencee;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.HistoriqueBean;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.AffichageHistorique;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteDao;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.URLResolver;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

import static com.univ.objetspartages.om.EtatFiche.A_SUPPRIMER;

/**
 * The Class ScanSite.
 */
public class ScanSite extends LogReportJob implements Runnable {

    /** The separateur langue. */
    private static final String SEPARATEUR_LANGUE = ",LANGUE=";

    private static final Logger LOG = LoggerFactory.getLogger(ScanSite.class);

    private final ServiceMetatag serviceMetatag;

    private final ServiceRubriquePublication serviceRubriquePublication;

    private final ServiceMedia serviceMedia;

    private final ServiceRessource serviceRessource;

    public ScanSite () {
        serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
        serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(final String[] args) {
        final ScanSite capture = new ScanSite();
        final Thread thread = new Thread(capture);
        thread.start();
        try {
            // JB 20050530 : attente du thread
            thread.join();
        } catch (final InterruptedException e) {
            LOG.error("Echec thread.join()", e);
        }
    }

    /**
     * Calcul du répertoire d'une page
     *
     * @param url the url
     * @return the directory
     * @deprecated Méthode plus utilisée
     */
    @Deprecated
    public String getDirectory(final String url) {
        final int irepertoire = url.lastIndexOf('/');
        final String directory;
        if (irepertoire <= 6) {
            directory = url;
        } else {
            directory = url.substring(0, irepertoire);
        }
        return directory;
    }

    /**
     *
     *
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        try {
            // ajout d'un contexte dans le thread local
            ContexteUtil.setContexteSansRequete();
            LOG.info("***** Suppression des fichiers temporaires *****");
            final String tmpDir = WebAppUtil.getWorkDefaultPath();
            // PCO - 18/06/2010
            // pour le dossier TMP, suppression de tout son contenu, même des
            // dossiers.
            final File dossierTemporaire = new File(tmpDir);
            removeContenuDossier(dossierTemporaire);
            // => Fin patch
            // JSS 20040114 : suppression fichiers session
            final String tmpDirSession = WebAppUtil.getSessionsPath();
            // PCO - 18/06/2010
            // pour le dossier SESSION, suppression de tout son contenu, même
            // des dossiers.
            removeContenuDossier(new File(tmpDirSession));
            // => Fin patch
            final String majReferences = StringUtils.defaultString(PropertyHelper.getCoreProperty("static.maj.references"), "0");
            /* Parsing des pages */
            final GregorianCalendar dateLimite = new GregorianCalendar();
            dateLimite.add(Calendar.DATE, -1);
            ContextePage ctx;
            LOG.info("***** Traitement des fiches *****");
            LOG.info("Purge des fiches obsolètes");
            LOG.info("Synchronisation des métatags");
            LOG.info("Gestion des alertes");
            try {
                for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
                    final String libelleObjet = ReferentielObjets.getLibelleObjet(codeObjet);
                    LOG.info("***** " + libelleObjet + " *****");
                    final ArrayList<Long> arrayMetatag = new ArrayList<>();
                    final ArrayList<String> arrayRubriquePublication = new ArrayList<>();
                    // new ArrayList();
                    ctx = new ContextePage("");
                    try {
                        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
                        if (ficheUniv == null) {
                            continue;
                        }
                        ficheUniv.setCtx(ctx);
                        ficheUniv.init();
                        final int count = ficheUniv.selectCodeLangueEtat("", "", "");
                        if (count > 0) {
                            while (ficheUniv.nextItem()) {
                                // sauvegarde des fiches traitees
                                arrayMetatag.add(ficheUniv.getIdFiche());
                                if ("0003".equals(ficheUniv.getEtatObjet())) {
                                    arrayRubriquePublication.add(ficheUniv.getCode() + SEPARATEUR_LANGUE + ficheUniv.getLangue());
                                }
                                // Purge des fiches à supprimer, apercus,
                                // sauvegardes automatiques
                                if ("0004".equals(ficheUniv.getEtatObjet()) || "0005".equals(ficheUniv.getEtatObjet()) || "0006".equals(ficheUniv.getEtatObjet())) {
                                    String traceSuppression = StringUtils.EMPTY;
                                    if (A_SUPPRIMER.getEtat().equals(ficheUniv.getEtatObjet())) {
                                        traceSuppression = String.format("Suppression de la fiche '%s' de type '%s' et de langue '%s'", ficheUniv.getLibelleAffichable(), libelleObjet, LangueUtil.getDisplayName(ficheUniv.getLangue()));
                                        final List<HistoriqueBean> historiqueFiche = AffichageHistorique.getHistorique(ficheUniv);
                                        Collections.sort(historiqueFiche);
                                        try {
                                            final HistoriqueBean lastOperation = historiqueFiche.get(historiqueFiche.size() - 1);
                                            if (MetatagUtils.getIntituleAction(MetatagUtils.HISTORIQUE_SUPPRESSION).equals(lastOperation.getAction()) && StringUtils.isNotBlank(lastOperation.getUtilisateur())) {
                                                traceSuppression += String.format(" par l'utilisateur '%s'", lastOperation.getUtilisateur());
                                            }
                                        } catch (final ArrayIndexOutOfBoundsException e) {
                                            LOG.error("impossible de récupérer l'historique de la fiche", e);
                                        }
                                    }
                                    FicheUnivMgr.supprimerFiche(ficheUniv, true);
                                    if (StringUtils.isNotBlank(traceSuppression)) {
                                        LOG.info(traceSuppression);
                                    }
                                    continue;
                                }
                                if (ficheUniv instanceof DiffusionSelective) {
                                    if ("".equals(((DiffusionSelective) ficheUniv).getDiffusionModeRestriction())) {
                                        ((DiffusionSelective) ficheUniv).setDiffusionModeRestriction("0");
                                        ficheUniv.update();
                                    }
                                }
                                // Traitement des meta tags
                                FicheUnivMgr.synchroniserMetaTag(ficheUniv, majReferences, true);
                                // Gestion des alertes
                                genererAlerte(codeObjet, ficheUniv);
                            }// fin de la boucle sur les fiches du meme type
                        }
                        // Suppression des metatags obsolètes
                        supprimerMetaTag(codeObjet, arrayMetatag);
                        supprimerRubriquePublication(codeObjet, arrayRubriquePublication);
                    } catch (final Exception e) {
                        LOG.error("Exception sur le traitement des fiches " + ReferentielObjets.getLibelleObjet(codeObjet), e);
                    } finally {
                        ctx.release();
                    }
                }// fin for
            } catch (final Exception e) {
                LOG.error("Ereur dans le traitement de la liste d'objet", e);
            }
            ctx = new ContextePage("");
            try {
                // synchronisation du référencement automatique
                LOG.info("***** Rubriquage automatique des fiches *****");
                rubriquerFiches(ctx);
                // archivage des demandes de mot de passe
                LOG.info("***** Archivage des demandes de mot de passe *****");
                archiverDemandesMotPasse();
                // purge fichiergw non associés à une fiche ou autre etat=0 ou code parent = ""
                // et purge fichiers physiques techniques fiches supprimées
                LOG.info("***** Purge des fichiers joints *****");
                purgeFichiersgw(ctx);
            } catch (final ErreurApplicative e) {
                LOG.error("Echec maintenance", e);
            } finally {
                ctx.release();
            }
        } catch (final Exception e) {
            LOG.error("Erreur d'execution du scansite : ", e);
        } finally {
            // suppression du contexte du thread local
            ContexteUtil.releaseContexteUniv();
        }
    }

    /**
     * Suppression des méta-tags non référencés c'est à dire pour lequels la fiche correspondante a été supprimée.
     *
     * @param codeObjet      the _code objet
     * @param listeExistante the _liste existante
     */
    public void supprimerMetaTag(final String codeObjet, final ArrayList<Long> listeExistante) {
        if (listeExistante.size() > 0) {
            final List<Long> listeIdFiches = new ArrayList<>();
            final List<MetatagBean> metas = serviceMetatag.getByObjectCode(codeObjet);
            for (final MetatagBean currentMeta : metas) {
                if (!listeExistante.contains(currentMeta.getMetaIdFiche())) {
                    listeIdFiches.add(currentMeta.getMetaIdFiche());
                }
            }
            if (CollectionUtils.isNotEmpty(listeIdFiches)) {
                serviceMetatag.deleteForCodeAndFicheIds(codeObjet, listeIdFiches);
            }
        }
    }

    /**
     * Suppression des rubriques de publication non référencées c'est à dire pour lequels la fiche correspondante a été supprimée.
     *
     * @param codeObjet      the _code objet
     * @param listeExistante the _liste existante
     */
    public void supprimerRubriquePublication(final String codeObjet, final List<String> listeExistante) {
        if (listeExistante.size() > 0) {
            final List<RubriquepublicationBean> results = serviceRubriquePublication.getByType(codeObjet);
            for (final RubriquepublicationBean rubpub : results) {
                final String rubpubExistante = rubpub.getCodeFicheOrig() + SEPARATEUR_LANGUE + rubpub.getLangueFicheOrig();
                if (!listeExistante.contains(rubpubExistante)) {
                    serviceRubriquePublication.delete(rubpub.getIdRubriquepublication());
                }
            }
        }
    }

    /**
     * Archivage des demandes de mot de passe ayant + de 1 jour.
     *
     * @throws ErreurApplicative lors de la requête en BDD
     */
    private void archiverDemandesMotPasse() throws ErreurApplicative {
        final GregorianCalendar dateLimite = new GregorianCalendar();
        dateLimite.add(Calendar.DATE, -1);
        try (ContexteDao ctx = new ContexteDao()) {
            final PreparedStatement stmt = ctx.getConnection().prepareStatement("DELETE FROM CHANGEMENTMOTPASSE WHERE DATE_DEMANDE < ?");
            stmt.setDate(1, SqlDateConverter.toDate(dateLimite.getTime()));
            stmt.executeUpdate();
            stmt.close();
        } catch (final SQLException exc) {
            throw new ErreurApplicative("ERROR_IN_METHOD ArchiverDemandesMotsDePasse ", exc);
        }
    }

    /**
     *
     * @param ctx le contexte ne sert pas
     * @throws Exception
     * @deprecated Utiliser la méthode {@link ScanSite#archiverDemandesMotPasse()}
     */
    @Deprecated
    public void archiverDemandesMotPasse(final OMContext ctx) throws Exception {
        archiverDemandesMotPasse();
    }

    /**
     * Purge des fichiers techniques qui ne sont plus référencées dans les fiches ou autre.
     *
     * @param ctx the ctx
     */
    public void purgeFichiersgw(final OMContext ctx) {
        try {
            // Nettoie la table fichiergw
            final List<RessourceBean> ressources = serviceRessource.getRessourceToPurge();
            int nbFichiersNonAssocies = 0;
            for (final RessourceBean ressourceBean : ressources) {
                LOG.info("Suppression de la ressource d'id " + ressourceBean.getId() + " ayant pour fichier source " + RessourceUtils.getSource(ressourceBean));
                serviceRessource.delete(ressourceBean.getId());
                nbFichiersNonAssocies++;
            }
            LOG.info(nbFichiersNonAssocies + " tuple(s) fichiergw supprimé(s) ");
            LOG.info("***** Purge des fichiers techniques non référencés *****");
            final Set<String> tree = new TreeSet<>();
            /* Recherche de tous les fichiers joints référencés */
            final List<MediaBean> medias = serviceMedia.getAll();
            for (final MediaBean currentMedia : medias) {
                tree.add(currentMedia.getUrl());
                if (currentMedia.getUrlVignette().length() > 0) {
                    tree.add(currentMedia.getUrlVignette());
                }
            }
            /* Nettoyage des répertoires de stockage */
            // répertoire privé des fichiergw
            int nbFichiersNonReferences = 0;
            final File dir = new File(RessourceUtils.getAbsolutePath());
            nbFichiersNonReferences = nettoyerRepertoire(dir, tree);
            // répertoire public de la médiathèque
            // nettoyage des dossiers mediathéque.
            nbFichiersNonReferences += nettoyerMediatheque(tree);
            LOG.info(nbFichiersNonReferences + " fichier(s) techniques supprimés ");
        } catch (final Exception e) {
            LOG.error("Exception pendant la purge des fichiers : methode purgeFichiersgw()", e);
        }
    }

    /**
     * Nettoyer repertoire.
     *
     * @param repertoire         the repertoire
     * @param fichiersReferences the liste
     * @return the int
     */
    public int nettoyerRepertoire(final File repertoire, final Set<String> fichiersReferences) {
        int nbFichiersNonReferences = 0;
        if (repertoire.isDirectory()) {
            final File[] list = repertoire.listFiles();
            if (list != null) {
                for (final File fichierASupprimer : list) {
                    // Appel récursif sur les sous-répertoires
                    if (fichierASupprimer.isDirectory()) {
                        nbFichiersNonReferences = nbFichiersNonReferences + nettoyerRepertoire(fichierASupprimer, fichiersReferences);
                    } else if (!fichiersReferences.contains(fichierASupprimer.getName())) {
                        if (fichierASupprimer.delete()) {
                            LOG.info("Suppression fichier " + repertoire.getAbsolutePath() + File.separator + fichierASupprimer.getName());
                        } else {
                            LOG.error("Erreur de suppression du fichier " + repertoire.getAbsolutePath() + File.separator + fichierASupprimer.getName());
                        }
                        nbFichiersNonReferences++;
                    }
                }
            }
        }
        return nbFichiersNonReferences;
    }

    /**
     *
     * @param ctx
     * @param codeObjet
     * @param ficheUniv
     * @param mailbox
     * @deprecated Méthode remplacée par sa version privée
     */
    @Deprecated
    public void genererAlerte(final OMContext ctx, final String codeObjet, final FicheUniv ficheUniv, final JSBMailbox mailbox) {
        genererAlerte(codeObjet, ficheUniv);
    }

    /**
     * Génération d'un page statique.
     *
     * @param codeObjet the _code objet
     * @param ficheUniv the fiche univ
     */
    private void genererAlerte(final String codeObjet, final FicheUniv ficheUniv) {
        try {
            if (Formateur.estSaisie(ficheUniv.getDateAlerte()) && ficheUniv.getDateAlerte().compareTo(new Date(System.currentTimeMillis())) <= 0) {
                final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
                final ServiceUser serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
                final String mail = serviceUser.getAdresseMail(ficheUniv.getCodeRedacteur(), null);
                if (mail.length() > 0) {
                    final String libelle = ReferentielObjets.getLibelleObjet(codeObjet);
                    // recup de l'url du site web
                    final String url_site = URLResolver.getAbsoluteBoUrl("/adminsite/", serviceInfosSite.getPrincipalSite());
                    final StringBuilder message = new StringBuilder();
                    message.append("Fiche concernée : fiche ").append(libelle).append(" intitulée ").append(ficheUniv.getLibelleAffichable()).append(" (code : ").append(ficheUniv.getCode()).append(").\n\n");
                    message.append("Message de l'alerte : \n");
                    message.append(ficheUniv.getMessageAlerte()).append("\n\n");
                    message.append("Pour ne plus recevoir cette alerte, supprimez ou modifiez la date d'alerte de la fiche \n");
                    message.append("dans le module d'administration du site web (").append(url_site).append(")\n\n");
                    message.append("Cordialement.");
                    final JSBMailbox mailbox = new JSBMailbox(false);
                    mailbox.sendSystemMsg(mail, "Alerte site web", message.toString());
                }
            }
        } catch (final MessagingException | EmailException e) {
            LOG.error(String.format("Exception pendant génération alerte objet : %s code : %s langue : %s", codeObjet, ficheUniv.getCode(), ficheUniv.getLangue()), e);
        }
    }

    /**
     * Cette méthode assigne des rubriques de publication de façon automatique.
     *
     * @param ctx the ctx
     */
    public void rubriquerFiches(final OMContext ctx) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final TreeSet<InfosFicheReferencee> listeFichesAIndexer = new TreeSet<>(new InfosFicheComparator());
        try {
            for (final RubriqueBean rubrique : serviceRubrique.getAllRubriqueWithPublicationRubrique()) {
                boolean rubriqueModifiee = false;
                String messageInvalide = "";
                String codeObjet = "";
                // boucle sur les requetes
                final Vector<String> vRequete = Chaine.getVecteurAccolades(rubrique.getRequetesRubriquePublication());
                for (final String requete : vRequete) {
                    LOG.debug("requete de multi-publication : " + requete);
                    final String[] item = requete.split("/", -2);
                    boolean requeteValide = true;
                    boolean rechercheRubriqueRecursive = false;
                    boolean rechercheStructureRecursive = false;
                    // on vérifie que la rubrique est valide
                    if (item[0].length() > 0) {
                        codeObjet = ReferentielObjets.getCodeObjet(item[0]);
                        if ("".equals(codeObjet)) {
                            messageInvalide = "requete invalide : objet non trouve " + item[0];
                            requeteValide = false;
                        }
                    }
                    String codeStructure = item[1];
                    if (codeStructure.length() > 0) {
                        final String[] temp = codeStructure.split(";", -2);
                        codeStructure = temp[0];
                        final StructureModele structure = serviceStructure.getByCodeLanguage(codeStructure, LangueUtil.getIndiceLocaleDefaut());
                        if (structure != null && !structure.getCode().equals(codeStructure)) {
                            messageInvalide = "requete invalide : structure non trouvee " + codeStructure;
                            requeteValide = false;
                        } else if ("1".equals(temp[1])) {
                            rechercheStructureRecursive = true;
                        }
                    }
                    String codeRubrique = item[2];
                    if (codeRubrique.length() > 0) {
                        final String[] temp = codeRubrique.split(";", -2);
                        codeRubrique = temp[0];
                        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(temp[0]);
                        if (rubriqueBean != null && !rubriqueBean.getCode().equals(codeRubrique)) {
                            messageInvalide = "requete invalide : rubrique non trouvee " + codeRubrique;
                            requeteValide = false;
                        } else if ("1".equals(temp[1])) {
                            rechercheRubriqueRecursive = true;
                        }
                    }
                    String langue = "";
                    if (item.length > 4) {
                        langue = item[3];
                        if (LangueUtil.getLocale(langue) == null) {
                            messageInvalide = "requete invalide : langue non trouvee " + langue;
                            requeteValide = false;
                        }
                    }
                    // requete valide
                    if (requeteValide) {
                        final Set<InfosFicheReferencee> listeFicheAvantSynchro = serviceRubriquePublication.getInfosFichesReferencees(rubrique.getCode(), requete);
                        LOG.debug("nb de fiches avant la synchro : " + listeFicheAvantSynchro.size());
                        // requête sur la table méta
                        final ClauseWhere whereFiche = new ClauseWhere();
                        if (codeObjet.length() > 0) {
                            whereFiche.setPremiereCondition(ConditionHelper.egalVarchar("META_CODE_OBJET", codeObjet));
                        }
                        if (codeStructure.length() > 0) {
                            if (rechercheStructureRecursive) {
                                final ConditionList conditionsStructure = new ConditionList(ConditionHelper.getConditionStructure("META_CODE_RATTACHEMENT", codeStructure));
                                conditionsStructure.or(ConditionHelper.getConditionStructureMultiple("META_CODE_RATTACHEMENT_AUTRES", codeStructure));
                                whereFiche.and(conditionsStructure);
                            } else {
                                final ConditionList conditionsStructure = new ConditionList(ConditionHelper.egalVarchar("META_CODE_RATTACHEMENT", codeStructure));
                                conditionsStructure.or(ConditionHelper.like("META_CODE_RATTACHEMENT_AUTRES", codeStructure, "%[", "]%"));
                                whereFiche.and(conditionsStructure);
                            }
                        }
                        if (codeRubrique.length() > 0) {
                            if (rechercheRubriqueRecursive) {
                                whereFiche.and(ConditionHelper.getConditionRubrique("META_CODE_RUBRIQUE", codeRubrique));
                            } else {
                                whereFiche.and(ConditionHelper.egalVarchar("META_CODE_RUBRIQUE", codeRubrique));
                            }
                        }
                        if (langue.length() > 0) {
                            whereFiche.and(ConditionHelper.egalVarchar("META_LANGUE", langue));
                        }
                        // les rubriques de publication ne concernent que
                        // les fiches en ligne
                        whereFiche.and(ConditionHelper.egalVarchar("META_ETAT_OBJET", EtatFiche.EN_LIGNE.getEtat()));

                        final List<MetatagBean> metas = serviceMetatag.getFromWhereClause(whereFiche);
                        LOG.debug(String.format("nb de fiches correspondant à la requête :%d", metas.size()));
                        if (CollectionUtils.isNotEmpty(metas)) {
                            for (final MetatagBean currentMeta : metas) {
                                final InfosFicheReferencee infosFiche = new InfosFicheReferencee();
                                infosFiche.setCode(currentMeta.getMetaCode());
                                infosFiche.setLangue(currentMeta.getMetaLangue());
                                infosFiche.setType(currentMeta.getMetaCodeObjet());
                                infosFiche.setRequete(requete);
                                if (!listeFicheAvantSynchro.contains(infosFiche)) {
                                    LOG.debug("ajout à indexer de :" + infosFiche.getCode() + " " + infosFiche.getType());
                                    final RubriquepublicationBean rubP = new RubriquepublicationBean();
                                    rubP.setTypeFicheOrig(infosFiche.getType());
                                    rubP.setCodeFicheOrig(infosFiche.getCode());
                                    rubP.setLangueFicheOrig(infosFiche.getLangue());
                                    rubP.setRubriqueDest(rubrique.getCode());
                                    rubP.setSourceRequete(requete);
                                    serviceRubriquePublication.save(rubP);
                                    listeFichesAIndexer.add(infosFiche);
                                } else {
                                    listeFicheAvantSynchro.remove(infosFiche);
                                }
                            }
                        }
                        // suppression des fiches obsoletes
                        if (listeFicheAvantSynchro.size() > 0) {
                            for (final InfosFicheReferencee infosFiche : listeFicheAvantSynchro) {
                                serviceRubriquePublication.deleteByFicheReference(infosFiche);
                                LOG.debug("suite à suppression, ajout à indexer de :" + infosFiche.getCode() + " " + infosFiche.getType());
                                listeFichesAIndexer.add(infosFiche);
                            }
                        }
                    } else {
                        // requete non valide on supprime la rubrique de
                        // publication
                        listeFichesAIndexer.addAll(serviceRubriquePublication.deleteRubPubAuto(rubrique.getCode(), requete));
                        // vRequete.remove(requete);
                        LOG.warn("Probleme sur une requete automatique definie pour la rubrique " + rubrique.getIntitule() + " : " + messageInvalide + ", la requete est ignoree");
                        rubriqueModifiee = true;
                    }
                    LOG.debug("nb de fiches à indexer : " + listeFichesAIndexer.size());
                }
                // mise à jour de la rubrique si modifiée
                if (rubriqueModifiee) {
                    serviceRubrique.save(rubrique);
                }
                LOG.info("re-indexation des nouvelles fiches rubriquées, nb de fiches à ré-indexer : " + listeFichesAIndexer.size());
                // réindexation de toutes les fiches modifiées
                for (final InfosFicheReferencee infosFiche : listeFichesAIndexer) {
                    // Lecture de la fiche
                    final FicheUniv ficheAreindexee = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(infosFiche.getType()));
                    if (ficheAreindexee != null) {
                        ficheAreindexee.init();
                        ficheAreindexee.setCtx(ctx);
                        if (ficheAreindexee.selectCodeLangueEtat(infosFiche.getCode(), infosFiche.getLangue(), "0003") > 0) {
                            ficheAreindexee.nextItem();
                            ficheAreindexee.setDateProposition(new Date(System.currentTimeMillis()));
                            ficheAreindexee.update();
                            final MetatagBean meta = MetatagUtils.lireMeta(ficheAreindexee);
                            meta.setMetaDateProposition(ficheAreindexee.getDateProposition());
                            serviceMetatag.save(meta);
                            PluginFicheHelper.synchroniserObjets(ficheAreindexee, meta, null);
                        }
                    }
                }
            }
        } catch (final Exception e) {
            LOG.error("Exception pendant le référencement des fiches : methode rubriquerFiches()", e);
        }
    }

    /**
     * Remove all of the content of a directory .
     *
     * @param toRemove Directory or file to be removed
     * @return false if failed
     */
    protected boolean remove(final File toRemove) {
        boolean result = true;
        if (toRemove.isDirectory()) {
            final String list[] = toRemove.list();
            for (final String element : list) {
                final File file = new File(toRemove, element);
                if (file.isDirectory()) {
                    result = remove(file);
                } else {
                    if (!file.delete()) {
                        result = false;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void perform() {
        run();
    }

    /**
     * Supprimer l'ensemble des fichiers de la médiathéque qui n'existent plus dans la base de données. Cette fonction tient compte des spécificité de la médiathéque à savoir : un
     * dossier par type de média et pas de dossiers dans les dossiers de types de média.
     *
     * @param listeNomsFichersAGarder La liste des nom de fichier à garder.
     * @return le nombre de fichiers supprimés.
     * @author pierre.cosson (18/06/2010)
     */
    public int nettoyerMediatheque(final Set<String> listeNomsFichersAGarder) {
        final File dossierMedias = new File(MediathequeHelper.getAbsolutePath());
        if (dossierMedias.exists() && dossierMedias.isDirectory()) {
            // Sous le dossier MEDIA un dossier apr type de média
            final File[] listeDossiersTypeMedia = dossierMedias.listFiles();
            int nbFichiersSupprimes = 0;
            for (final File enCours : listeDossiersTypeMedia) {
                if (enCours.isDirectory()) {
                    // cas d'un dossier de type de média (on va le parcourir)
                    final File[] listeMediasTypes = enCours.listFiles();
                    for (final File listeMediasType : listeMediasTypes) {
                        final File mediaType = listeMediasType;
                        if (!mediaType.isDirectory() && !listeNomsFichersAGarder.contains(mediaType.getName())) {
                            // on ne supprime pas les dossiers
                            mediaType.delete();
                            LOG.info("Suppression fichier " + mediaType.getAbsolutePath());
                            nbFichiersSupprimes++;
                        }
                    }
                } else if (!listeNomsFichersAGarder.contains(enCours.getName())) {
                    // fichier qui n'est pas la liste des fichiers à garder
                    enCours.delete();
                    LOG.info("Suppression fichier " + enCours.getAbsolutePath());
                    nbFichiersSupprimes++;
                }
            }
            return nbFichiersSupprimes;
        }
        return 0;
    }

    /**
     * Supprimer tous les éléments contenu dans un dossier (cette fonction supprime tous les fichiers et dossier)
     *
     * @param dossierTemporaire le dossier à vider
     */
    private void removeContenuDossier(final File dossierTemporaire) {
        if (dossierTemporaire.exists() && dossierTemporaire.isDirectory()) {
            final File[] listeFichiersASuppr = dossierTemporaire.listFiles();
            for (final File element : listeFichiersASuppr) {
                final File fichierASuppr = element;
                if (fichierASuppr.isDirectory()) {
                    try {
                        FileUtils.deleteDirectory(fichierASuppr);
                    } catch (final IOException e) {
                        LOG.error("Erreur lors de la suppression du dossier : " + fichierASuppr.getAbsolutePath(), e);
                    }
                } else {
                    fichierASuppr.delete();
                }
            }
        }
    }

}
