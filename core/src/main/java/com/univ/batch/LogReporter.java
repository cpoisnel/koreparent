package com.univ.batch;

public interface LogReporter {

    String getLogFileName();

    String getReportSubject();

    String getLogDescription();

    boolean isReportByMail();

}
