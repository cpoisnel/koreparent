/*
 * Created on 12 mars 2004
 *
 */
package com.univ.batch.imports;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.CodeLibelle;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.webutils.ContextePage;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.scheduling.spring.quartz.LogReportJob;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.cache.CacheLibelleManager;
import com.univ.objetspartages.services.ServiceLabel;
import com.univ.utils.Chaine;

/**
 * The Class ImportLibelles.
 *
 * @author malice
 *
 *         Importation de libellés arguments fournis : un dossier ou un fichier. s'il s'agit d'un dossier, on importe tous les fichiers qui ont l'extension .txt s'il s'agit d'un
 *         fichier, on importe ce fichier Il s'agit d'un import différentiel : si le libellé existe déjà, on le met à jour, sinon, on l'ajoute
 */
public class ImportLibelles extends LogReportJob {

    private static final Logger LOG = LoggerFactory.getLogger(ImportLibelles.class);

    /**
     * clé permettant de retrouver le paramètre fichier dans la map du job
     */
    private static final String SELECTED_FILE = "SELECTED_FILE";

    private final ServiceLabel serviceLabel;

    /** The s folder or file. */
    private String sFolderOrFile = "";

    public ImportLibelles() {
        serviceLabel = ServiceManager.getServiceForBean(LabelBean.class);
    }

    /**
     * Importe le fichier de libellés.
     *
     * @param f
     *            Fichier de libellés à importer
     * @param _ctx
     *            contexte
     *
     * @throws IOException
     *             the exception
     */
    public void importFichier(final File f, final ContextePage _ctx) throws IOException {
        final FileReader freader = new FileReader(f);
        final CacheLibelleManager cache = (CacheLibelleManager) ApplicationContextManager.getCoreContextBean(CacheLibelleManager.ID_BEAN);
        /* lecture du fichier */
        try (final BufferedReader fichLogique = new BufferedReader(freader)) {
            /* chaque ligne correspond à un libellé */
            String ligneLue = fichLogique.readLine();
            while (ligneLue != null) {
                String typeLibelle = "";
                String code = "";
                String libelle = "";
                String langue = "";
                /* la ligne se présente ainsi :
                 * 'typelibelle','codelibelle','libelle','languelibelle'
                 * les quotes ne sont pas forcément présentes
                 * */
                final StringTokenizer st = new StringTokenizer(ligneLue, ",");
                int ind = 0;
                while (st.hasMoreTokens()) {
                    final String item = st.nextToken();
                    if (ind == 0) {
                        typeLibelle = enleverQuote(item);
                    }
                    if (ind == 1) {
                        code = enleverQuote(item);
                    }
                    if (ind == 2) {
                        libelle = enleverQuote(item);
                    }
                    if (ind == 3) {
                        langue = enleverQuote(item);
                    }
                    ind++;
                }
                /* insertion du libellé dans la base, ou mise à jour si déjà
                 * existant (pour le savoir, recherche par code,langue et type)
                 */
                boolean isCodeValide = Boolean.TRUE;
                try {
                    Chaine.controlerCodeMetier(code);
                } catch (final ErreurApplicative e) {
                    LOG.debug("le code " + code + " est invalide", e);
                    isCodeValide = Boolean.FALSE;
                }
                final boolean isTypeValide = cache.getListeTypesLibelles().containsKey(typeLibelle);
                if (isCodeValide && isTypeValide && (libelle.length() > 0) && (langue.length() > 0)) {
                    LabelBean labelBean = serviceLabel.getByTypeCodeLanguage(typeLibelle, code, langue);
                    if (labelBean != null) {
                        labelBean.setLibelle(libelle);
                        serviceLabel.save(labelBean);
                        LOG.debug("libellé mis à jour : " + libelle + "(code : " + code + ", langue : " + langue + ", type : " + CodeLibelle.lireLibelle(null, "type_libelle", typeLibelle) + ")");
                    } else {
                        labelBean = serviceLabel.getByLibelleTypeLanguage(StringUtils.replace(libelle, "'", "\\'"), typeLibelle, langue);
                        if (labelBean != null) {
                            LOG.debug("Le même libellé existe déjà avec un code différent. Ce libellé ('" + libelle + "', langue : " + langue + ", type :" + typeLibelle + ") n'est donc pas importé.");
                        } else {
                            serviceLabel.createNewLabel(typeLibelle, code, libelle, langue);
                            LOG.debug("libellé ajouté : " + libelle + "(code : " + code + ", langue : " + langue + ", type : " + CodeLibelle.lireLibelle(null, "type_libelle", typeLibelle) + ")");
                        }
                    }
                }
                ligneLue = fichLogique.readLine();
            }
        }
    }

    @Override
    public void init(final JobExecutionContext arg0) {
        sFolderOrFile = (String) arg0.getMergedJobDataMap().get(SELECTED_FILE);
    }

    @Override
    public void perform() {
        try {
            final ContextePage ctx = new ContextePage("");
            final File folderorfile = new File(sFolderOrFile);
            File file;
            LOG.debug(folderorfile.isDirectory() ? "Dossier " : "Fichier ");
            if (!folderorfile.exists()) {
                throw new ErreurApplicative((folderorfile.isDirectory() ? "Dossier " : "Fichier ") + sFolderOrFile + " inexistant");
            } else if (!folderorfile.canRead()) {
                throw new ErreurApplicative((folderorfile.isDirectory() ? "Dossier " : "Fichier ") + sFolderOrFile + " non accessible en lecture");
            }
            /* le paramètre fourni est un dossier */
            if (folderorfile.isDirectory()) {
                /* on importe tous les fichiers .TXT */
                final FilenameFilter filtre = new TXTFilter();
                final File[] listeFiles = folderorfile.listFiles(filtre);
                for (final File listeFile : listeFiles) {
                    LOG.debug("fichier : " + listeFile.getName());
                    importFichier(listeFile, ctx);
                }
            } else {
                file = folderorfile;
                importFichier(file, ctx);
            }
        } catch (final IOException | ErreurApplicative e) {
            LOG.error("Erreur d'import de libellés", e);
        }
    }

    /**
     * Enleve les quotes de debut et fin de chaine.
     *
     * @param chaine
     *            : la chaine dont on souhaite enlever les quotes
     *
     * @retun chaine: la chaine après traitement
     */
    private String enleverQuote(final String chaine) {
        String res = chaine.trim();
        if (res.charAt(0) == '\'') {
            res = res.substring(1);
        }
        if (res.charAt(res.length() - 1) == '\'') {
            res = res.substring(0, res.length() - 1);
        }
        return res;
    }
}
