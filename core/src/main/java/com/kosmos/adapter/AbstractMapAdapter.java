package com.kosmos.adapter;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.adapter.converter.EnumConvertUtilsBean;
import com.univ.utils.json.NamingStrategyHelper;

/**
 * Created on 31/12/14.
 */
public abstract class AbstractMapAdapter implements Adapter<Map<String, Object>> {

    public static final String DEFAULT_PREFIX = "registration.";

    private static final Logger LOG = LoggerFactory.getLogger(AbstractMapAdapter.class);

    private BeanUtilsBean beanUtilsBean;

    public AbstractMapAdapter() {
        final DateConverter converter = new DateConverter(null);
        String[] patterns = new String[2];
        patterns[0] = "dd/MM/yyyy";
        patterns[1] = "dd/MM/yyyy HH:mm:ss";
        converter.setPatterns(patterns);
        final IntegerConverter integerConverter = new IntegerConverter(null);
        BeanUtilsBean.setInstance(new BeanUtilsBean(new EnumConvertUtilsBean()));
        beanUtilsBean = BeanUtilsBean.getInstance();
        beanUtilsBean.getConvertUtils().register(converter, Date.class);
        beanUtilsBean.getConvertUtils().register(integerConverter, Integer.class);
    }

    @Override
    public <E> E retrieveData(Map<String, Object> dataSource, Class<E> clazz) {
        try {
            final E registrationData = clazz.newInstance();
            final Map<String, Object> relevantDatas = getRelevantData(dataSource, clazz);
            beanUtilsBean.populate(registrationData, relevantDatas);
            return registrationData;
        } catch (InstantiationException | IllegalAccessException e) {
            LOG.error(String.format("Une erreur est survenue lors de la tentative d'instanciation d'un bean de type %s", clazz), e);
        } catch (InvocationTargetException e) {
            LOG.error(String.format("Une erreur est survenue lors de la tentative de population du bean de type %s", clazz), e);
        }
        return null;
    }

    protected Map<String, Object> getRelevantData(Map<String, Object> dataSource, Class<?> clazz) {
        final Map<String, Object> relevantDatas = new HashMap<>();
        final String classPrefix = getClassPrefix(clazz);
        for (Map.Entry<String, Object> currentEntry : dataSource.entrySet()) {
            if (currentEntry.getKey().startsWith(classPrefix)) {
                String relevantKey = currentEntry.getKey().replace(classPrefix, StringUtils.EMPTY);
                relevantKey = NamingStrategyHelper.translateUnderscoreToUppercase(relevantKey);
                relevantDatas.put(relevantKey, currentEntry.getValue());
            } else if (currentEntry.getKey().startsWith(DEFAULT_PREFIX)) {
                String relevantKey = StringUtils.removeStart(currentEntry.getKey(), DEFAULT_PREFIX);
                relevantKey = NamingStrategyHelper.translateUnderscoreToUppercase(relevantKey);
                if (relevantDatas.get(relevantKey) == null) {
                    relevantDatas.put(relevantKey, currentEntry.getValue());
                }
            }
        }
        return relevantDatas;
    }

    protected String getClassPrefix(Class<?> clazz) {
        return String.format("%s.", NamingStrategyHelper.translateUppercaseToUnderscore(clazz.getSimpleName()));
    }
}
