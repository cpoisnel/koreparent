package com.kosmos.usinesite.processus;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.exception.ErreurActionNonAutorisee;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.jsbsoft.jtf.exception.ErreurUtilisateurNonAuthentifie;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.utils.InfosSiteHelper;
import com.kosmos.usinesite.utils.SaisieInfosSiteInfoBeanHelper;
import com.kosmos.usinesite.utils.UsineSiteComposantUtil;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantUsineASite;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;

/**
 * Processus de saisie d'un {@link InfosSite} en back office. Ce processus permet de : creer, modifier, supprimer et lister. Ce processus est uniquement accessible au Webmaster.
 *
 * @author pierre.cosson
 *
 */
public class SaisieInfosSite extends SaisieInfosSiteCommun {

    public static final String CODE_SITE_DUPLIQUE = "CODE_SITE_DUPLIQUE";

    private static final Logger LOG = LoggerFactory.getLogger(SaisieInfosSite.class);

    /**
     * Autorisation de l'utilisateur actuellement en train d'accéder au processus.
     */
    private ActionUtilisateur actionUtilisateur = ActionUtilisateur.AUCUNE;

    private EcranLogique ecranLogiqueHistorique;

    /**
     * Constructeur.
     *
     * @param infoBean
     */
    public SaisieInfosSite(final InfoBean infoBean) {
        super(infoBean);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.jsbsoft.jtf.core.AbstractProcessusBean#traiterAction()
     */
    @Override
    protected boolean traiterAction() throws Exception {
        initialiserServices();
        actionUtilisateur = getActionUtilisateurDepuisInfosBean();
        ecranLogiqueHistorique = getEcranLogiqueHistorique();
        EcranLogique ecranDestination = null;
        try {
            verificationDroitUtilisateur();
            ecranDestination = traiterActionUtilisateur();
        } catch (final ErreurApplicative e) {
            LOG.debug("An error occured on site management", e);
            ecranDestination = traiterErreur(e);
        }
        return redirection(ecranDestination);
    }

    @Override
    protected void verificationDroitUtilisateur() throws ErreurUtilisateurNonAuthentifie, ErreurActionNonAutorisee {
        if (autorisations == null || StringUtils.isEmpty(autorisations.getCode())) {
            throw new ErreurUtilisateurNonAuthentifie();
        }
        if (!ComposantUsineASite.isAutoriseParActionProcessus(autorisations, actionUtilisateur.name())) {
            throw new ErreurActionNonAutorisee();
        }
    }

    /**
     * Gestion des erreurs. Laisser remonter ou non un exception et/ou rediriger vers les écrans logique adéquat.
     *
     * @param e
     *            Exception à l'origine de l'appel.
     * @return l'ecran logique de redirection si cela est nécessaire.
     * @throws Exception
     *             erreur non traitée par le fonction que nous laissons remonter.
     */
    private EcranLogique traiterErreur(final ErreurApplicative e) throws Exception {
        if (e instanceof ErreurUtilisateurNonAuthentifie) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            return EcranLogique.LOGIN;
        }
        if (ecranLogiqueHistorique == EcranLogique.AUCUN) {
            infoBean.addMessageErreur(e.getMessage());
            return EcranLogique.ERREUR;
        } else if (ecranLogiqueHistorique == EcranLogique.SAISIE) {
            if (e instanceof ErreursSaisieInfosSite) {
                final ErreursSaisieInfosSite erreurs = (ErreursSaisieInfosSite) e;
                InfosSite infosSite = erreurs.getInfosSiteOrigineErreurs();
                final TemplateSite template = erreurs.getTemplateInfossiteOrigineErreurs();
                if (infosSite == null) {
                    infosSite = new InfosSiteImpl();
                }
                setDonneesPourSaisieDansInfoBean(infosSite, template);
                infoBean.addMessageErreur(StringUtils.join(erreurs.getListeMessagesErreur(), "\n"));
            } else {
                infoBean.addMessageErreur(e.getMessage());
            }
            if (ActionUtilisateur.VALIDER_AJOUTER.equals(actionUtilisateur)) {
                infoBean.set(INFOBEAN_ETAT_INTERFACE_SAISIE, EtatInterfaceSaisie.CREATION);
            } else {
                infoBean.set(INFOBEAN_ETAT_INTERFACE_SAISIE, EtatInterfaceSaisie.MODIFICATION);
            }
            return ecranLogiqueHistorique;
        } else {
            infoBean.addMessageErreur(e.getMessage());
            return ecranLogiqueHistorique;
        }
    }

    /**
     * Récupérer l'action faite par l'utilisateur.
     *
     * @return le {@link ActionUtilisateur} si le paramétre action existe sinon {@link ActionUtilisateur#AUCUNE}
     */
    private ActionUtilisateur getActionUtilisateurDepuisInfosBean() {
        String actionString = infoBean.getActionUtilisateur();
        if (StringUtils.isEmpty(actionString)) {
            return ActionUtilisateur.AUCUNE;
        }
        try {
            actionString = actionString.toUpperCase();
            return ActionUtilisateur.valueOf(actionString);
        } catch (final IllegalArgumentException e) {
            LOG.debug("impossible de faire la correspondance entre le String et l ennumeration ActionUtilisateur", e);
            return ActionUtilisateur.AUCUNE;
        }
    }

    /**
     * Récupérer l'écran logique à l'origine de l'appel du processus.
     *
     * @return
     */
    private EcranLogique getEcranLogiqueHistorique() {
        String ecranLogique = infoBean.getEcranLogique();
        if (StringUtils.isEmpty(ecranLogique)) {
            return EcranLogique.AUCUN;
        }
        try {
            ecranLogique = ecranLogique.toUpperCase();
            return EcranLogique.valueOf(ecranLogique);
        } catch (final IllegalArgumentException e) {
            LOG.debug("impossible de faire la correspondance entre le String et l ennumeration EcranLogique", e);
            return EcranLogique.AUCUN;
        }
    }

    /**
     * Vérifie si les autorisations courantes possèdent bien la permission de modification sur le périmètre donnée.
     *
     * @param siteAVerifier
     * @return
     */
    @Override
    protected boolean possedePermissionModificationPourSite(final InfosSite siteAVerifier) {
        final PermissionBean permissionModification = UsineSiteComposantUtil.getPermissionModification();
        final Perimetre perimetreFiche = new Perimetre(StringUtils.EMPTY, siteAVerifier.getCodeRubrique(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        return autorisations.isWebMaster() || autorisations.possedePermission(permissionModification, perimetreFiche);
    }

    /**
     * Vérifie si les autorisations courantes possèdent bien la permission de CRUD sur le périmètre donnée.
     *
     * @param siteAVerifier
     * @return
     */
    @Override
    protected boolean possedePermissionGestionPourSite(final InfosSite siteAVerifier) {
        final PermissionBean permissionGestion = UsineSiteComposantUtil.getPermissionGestion();
        final Perimetre perimetreFiche = new Perimetre(StringUtils.EMPTY, siteAVerifier.getCodeRubrique(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        return autorisations.isWebMaster() || autorisations.possedePermission(permissionGestion, perimetreFiche);
    }

    /**
     * Traiter l'action utilisateur.
     *
     * @return
     * @throws Exception
     */
    private EcranLogique traiterActionUtilisateur() throws Exception {
        if (actionUtilisateur == ActionUtilisateur.ACCUEIL) {
            listerInfosSite();
            return EcranLogique.LISTE;
        } else if (actionUtilisateur == ActionUtilisateur.AJOUTER) {
            preparerAjoutInfosSite();
            return EcranLogique.SAISIE;
        } else if (actionUtilisateur == ActionUtilisateur.DUPLIQUER) {
            preparerDuplicationInfosSite();
            return EcranLogique.SAISIE;
        } else if (actionUtilisateur == ActionUtilisateur.MODIFIER) {
            preparerModificationInfosSite();
            return EcranLogique.SAISIE;
        } else if (actionUtilisateur == ActionUtilisateur.VALIDER_AJOUTER) {
            validerAjouter();
            return EcranLogique.FIN;
        } else if (actionUtilisateur == ActionUtilisateur.VALIDER_MODIFICATION) {
            validerModification();
            return EcranLogique.FIN;
        } else if (actionUtilisateur == ActionUtilisateur.SUPPRIMER) {
            validerSuppression();
            return EcranLogique.FIN;
        } else {
            throw new ErreurActionNonAutorisee();
        }
    }

    /**
     * Récupère l'infosSite à dupliqué, supprime les données non duplicable du site et ajoute à l'infobean ces valeurs.
     *
     * @return
     * @throws Exception
     */
    @Override
    protected void preparerDuplicationInfosSite() throws Exception {
        final String code = getCodeDepuisInfoBean();
        try {
            final InfosSiteImpl infosSiteADupliquer = (InfosSiteImpl) serviceInfosSite.getInfosSite(code);
            if (!possedePermissionGestionPourSite(infosSiteADupliquer)) {
                throw new ErreurActionNonAutorisee();
            }
            infosSiteADupliquer.setHttpHostname(StringUtils.EMPTY);
            infosSiteADupliquer.setHttpsHostname(StringUtils.EMPTY);
            infosSiteADupliquer.setAlias(StringUtils.EMPTY);
            infosSiteADupliquer.setListeHostAlias(Collections.<String>emptySet());
            infosSiteADupliquer.setSitePrincipal(Boolean.FALSE);
            infosSiteADupliquer.setActif(Boolean.FALSE);
            final TemplateSite template = serviceTemplateSite.getTemplateSiteParCodeSite(code);
            setDonneesPourSaisieDansInfoBean(infosSiteADupliquer, template);
            infoBean.setTitreEcran(MessageHelper.getCoreMessage("USINESITE_DUPLICATION") + " " + infosSiteADupliquer.getIntitule());
            infoBean.set(CODE_SITE_DUPLIQUE, code);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("data not found for the current website", e);
            setDonneesPourSaisieDansInfoBean(new InfosSiteImpl(), null);
        }
        infoBean.set(INFOBEAN_CODE, StringUtils.EMPTY);
        infoBean.set(INFOBEAN_ETAT_INTERFACE_SAISIE, EtatInterfaceSaisie.CREATION);
        infoBean.set(INFOBEAN_INFOSSITE_ACTIF, "0");
    }

    /**
     * Redirection vers l'écran logique spécifié.
     *
     * @param ecranDestination
     * @return {@link Boolean} qui indique la fin ou non du traitement.
     */
    private boolean redirection(final EcranLogique ecranDestination) {
        if (ecranDestination == EcranLogique.FIN) {
            etat = FIN;
        } else {
            ecranLogique = ecranDestination.toString();
            infoBean.setEcranLogique(ecranLogique);
        }
        return etat == FIN;
    }

    /**
     * Prépare la liste des infosSite visible par l'utilisateur.
     *
     * @return
     * @throws Exception
     */
    @Override
    protected void listerInfosSite() throws Exception {
        final Collection<InfosSite> listeInfosSites = serviceInfosSite.getListeTousInfosSites();
        if (!autorisations.isWebMaster()) {
            CollectionUtils.filter(listeInfosSites, new Predicate() {

                @Override
                public boolean evaluate(final Object object) {
                    final InfosSite siteCourant = (InfosSite) object;
                    return possedePermissionGestionPourSite(siteCourant) || possedePermissionModificationPourSite(siteCourant);
                }
            });
        }
        final Map<String, TemplateSite> templatesParCodesTemplates = new HashMap<>();
        @SuppressWarnings("unchecked")
        final Collection<String> codesTemplate = CollectionUtils.collect(listeInfosSites, new Transformer() {

            @Override
            public String transform(final Object input) {
                return ((InfosSite) input).getCodeTemplate();
            }
        });
        for (final String codeTemplate : codesTemplate) {
            try {
                templatesParCodesTemplates.put(codeTemplate, serviceTemplateSite.getTemplateSiteParCode(codeTemplate));
            } catch (final Exception e) {
                LOG.debug("unable to find the current template", e);
                infoBean.addMessageAlerte(e.getMessage());
            }
        }
        infoBean.set(INFOBEAN_IS_GESTIONNAIRE_SITE, autorisations.possedePermission(UsineSiteComposantUtil.getPermissionGestion()) || autorisations.isWebMaster());
        infoBean.set(INFOBEAN_VALEURS_LISTE_TEMPLATES_INFOSSITE, templatesParCodesTemplates);
        infoBean.set(INFOBEAN_LISTE_INFOSSITES, listeInfosSites);
    }

    /**
     *
     * @return Ecran logique de redirection.
     * @throws Exception
     */
    @Override
    protected void preparerAjoutInfosSite() throws Exception {
        final InfosSite infosSiteVide = new InfosSiteImpl();
        setDonneesPourSaisieDansInfoBean(infosSiteVide, null);
        infoBean.set(INFOBEAN_ETAT_INTERFACE_SAISIE, EtatInterfaceSaisie.CREATION);
    }

    /**
     * Préparer l'infoBean pour préparer la modification des données du {@link InfosSite}.
     *
     * @return
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurActionNonAutorisee} : impossible de trouver le paramètre CODE dans l'infoBean.</li>
     *             <li> {@link ErreurDonneeNonTrouve} : impossible de charger le {@link InfosSite} à modifier.</li>
     *             <li>Autres :
     *             <ul>
     *             <li>Erreur durant l'accés à la source de données des {@link InfosSite}</li>
     *             <li>Erreur durant la récupération du template</li>
     *             </ul>
     *             </li>
     *             </ul>
     */
    @Override
    protected void preparerModificationInfosSite() throws Exception {
        final String code = getCodeDepuisInfoBean();
        final InfosSite infosSite = serviceInfosSite.getInfosSite(code);
        try {
            if (!possedePermissionGestionPourSite(infosSite) && !possedePermissionModificationPourSite(infosSite)) {
                throw new ErreurActionNonAutorisee();
            }
            final TemplateSite template = serviceTemplateSite.getTemplateSiteParCodeSite(code);
            setDonneesPourSaisieDansInfoBean(infosSite, template);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("data not found for editing a website", e);
            setDonneesPourSaisieDansInfoBean(infosSite, null);
        }
        infoBean.set(INFOBEAN_ETAT_INTERFACE_SAISIE, EtatInterfaceSaisie.MODIFICATION);
        infoBean.setTitreEcran(infosSite.getIntitule());
    }

    @Override
    protected void validerAjouter() throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        final TemplateSite template = getTemplateDepuisInfosBean();
        SaisieInfosSiteInfoBeanHelper.peuplerInfosSiteDepuisInfoBean(infoBean, infosSite, template);
        verifierDonneesInfosSite(infosSite, template);
        if (!possedePermissionGestionPourSite(infosSite)) {
            throw new ErreurActionNonAutorisee();
        }
        final String codeUtilisateur = autorisations.getCode();
        try {
            serviceInfosSite.creer(infosSite, codeUtilisateur);
            lancementTraitementsComplementairesTemplate(infosSite, template);
            updateFilesOnDuplication(infosSite);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("data not found while adding a website", e);
            throw new ErreursSaisieInfosSite(e.getMessage(), infosSite, template);
        }
        infoBean.addMessageConfirmation(String.format(MessageHelper.getCoreMessage("CONFIRMATION_CREATION_SITE"), infosSite.getIntitule()));
    }

    private void updateFilesOnDuplication(final InfosSiteImpl infosSite) throws IOException {
        final String codeSiteDuplique = infoBean.getString(CODE_SITE_DUPLIQUE);
        if (StringUtils.isNotBlank(codeSiteDuplique)) {
            final File oldSiteFilesDirectory = new File(InfosSiteHelper.getPathAbsoluFichiersTemplateDuSite(codeSiteDuplique));
            // Si le site dupliqué dispose d'un répertoire de contenu
            if (oldSiteFilesDirectory.exists()) {
                final File newSiteFilesDirectory = new File(InfosSiteHelper.getPathAbsoluFichiersTemplateDuSite(infosSite.getAlias()));
                FileUtils.copyDirectory(oldSiteFilesDirectory, newSiteFilesDirectory);
            }
        }
    }

    /**
     * Supprimer un {@link InfosSite} de la source de données.
     *
     * @return L'écran de redirection.
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurActionNonAutorisee} : impossible de trouver le paramètre CODE dans l'infoBean.</li>
     *             <li>Autres : Erreur durant l'accés à la source de données des {@link InfosSite}</li>
     *             </ul>
     */
    @Override
    protected void validerSuppression() throws Exception {
        final String code = getCodeDepuisInfoBean();
        final InfosSiteImpl infosSite = (InfosSiteImpl) serviceInfosSite.getInfosSite(code);
        if (!possedePermissionGestionPourSite(infosSite)) {
            throw new ErreurActionNonAutorisee();
        }
        if (infosSite.isSitePrincipal()) {
            throw new ErreursSaisieInfosSite(MessageHelper.getCoreMessage("USINESITE_SAISIE_SUPPRESSION_SITE_PRINCIPAL"));
        }
        serviceInfosSite.supprimer(code);
        serviceInfosSite.supprimerTousFichiers(infosSite);
        infoBean.addMessageConfirmation(String.format(MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_SITE"), infosSite.getIntitule()));
    }

    @Override
    protected void preparerSuppressionInfosSite() throws Exception {
        //Cette méthode est vide car elle sert lors de la suppression d'un site ET de son contenu dans l'uas.
    }

    /**
     * Enumération des actions autorisées.
     *
     * @author pierre.cosson
     *
     */
    public enum ActionUtilisateur {
        AUCUNE, ACCUEIL, AJOUTER, MODIFIER, VALIDER_AJOUTER, VALIDER_MODIFICATION, SUPPRIMER, DUPLIQUER
    }

    /**
     * Enumération des différents écran (JSP) gérés par ce processus.
     *
     * @author pierre.cosson
     *
     */
    public enum EcranLogique {
        ERREUR, LISTE, SAISIE, LOGIN, FIN, AUCUN
    }
}
