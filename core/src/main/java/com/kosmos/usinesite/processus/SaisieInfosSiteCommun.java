package com.kosmos.usinesite.processus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurActionNonAutorisee;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.jsbsoft.jtf.exception.ErreurUniciteNonRespectee;
import com.jsbsoft.jtf.exception.ErreurUtilisateurNonAuthentifie;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.kosmos.usinesite.template.property.service.ServiceTemplateSiteProperty;
import com.kosmos.usinesite.template.property.service.ServiceTemplateSitePropertyFactory;
import com.kosmos.usinesite.template.property.traitement.TemplateSitePropertyTraitement;
import com.kosmos.usinesite.template.service.ServiceTemplateSite;
import com.kosmos.usinesite.template.service.ServiceTemplateSiteFactory;
import com.kosmos.usinesite.utils.InfosSiteHelper;
import com.kosmos.usinesite.utils.SaisieInfosSiteInfoBeanHelper;
import com.kosmos.usinesite.utils.UsineSiteComposantUtil;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.multisites.service.ServiceInfosSiteFactory;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;

public abstract class SaisieInfosSiteCommun extends ProcessusBean {

    /**
     * Ci-dessous la liste des clés {@link InfoBean} contenant les données saisies par l'utilisateur.
     */
    public static final String INFOBEAN_LISTE_INFOSSITES = "LISTE_INFOSSITES";

    public static final String INFOBEAN_IS_GESTIONNAIRE_SITE = "IS_GESTIONNAIRE_SITE";

    public static final String INFOBEAN_INFOSSITE = "INFOSSITE";

    public static final String INFOBEAN_CONTENU_SOURCE = "CONTENU_SOURCE";

    public static final String INFOBEAN_ETAT_INTERFACE_SAISIE = "ETAT_INTERFACE_SAISIE";

    public static final String INFOBEAN_INFOSSITE_ACTIF = "INFOSSITE_ACTIF";

    public static final String INFOBEAN_ACTION = "ACTION";

    public static final String INFOBEAN_CODE = "CODE";

    public static final String INFOBEAN_INTITULE = "INTITULE";

    public static final String INFOBEAN_INTITULE_RUBRIQUE = "INTITULE_RUBRIQUE";

    public static final String INFOBEAN_CODE_RUBRIQUE = "CODE_RUBRIQUE";

    public static final String INFOBEAN_CODE_RUBRIQUE_SOURCE = "CODE_RUBRIQUE_SOURCE";

    public static final String INFOBEAN_PRINCIPAL = "PRINCIPAL";

    public static final String INFOBEAN_ACTIF = "ACTIF";

    public static final String INFOBEAN_HTTP_HOSTNAME = "HTTP_HOSTNAME";

    public static final String INFOBEAN_HTTP_PORT = "HTTP_PORT";

    public static final String INFOBEAN_HTTPS_HOSTNAME = "HTTPS_HOSTNAME";

    public static final String INFOBEAN_HTTPS_PORT = "HTTPS_PORT";

    public static final String INFOBEAN_MODE_SSL = "MODE_SSL";

    public static final String INFOBEAN_BO_MODE_SSL = "BO_MODE_SSL";

    public static final String INFOBEAN_ACTIONS_HTTP = "ACTIONS_HTTP";

    public static final String INFOBEAN_ACTIONS_HTTPS = "ACTIONS_HTTPS";

    public static final String INFOBEAN_ALIAS = "ALIAS";

    public static final String INFOBEAN_PAGE_ACCUEIL = "PAGE_ACCUEIL";

    public static final String INFOBEAN_MODE_REECRITURE = "MODE_REECRITURE";

    public static final String INFOBEAN_NIVEAU_REECRITURE_MIN = "NIVEAU_REECRITURE_MIN";

    public static final String INFOBEAN_NIVEAU_REECRITURE_MAX = "NIVEAU_REECRITURE_MAX";

    public static final String INFOBEAN_RESTREINT = "RESTREINT";

    public static final String INFOBEAN_SSO = "SSO";

    public static final String INFOBEAN_TEMPLATE = "TEMPLATE";

    public static final String INFOBEAN_VALEURS_MODE_SSL = "VALEURS_MODE_SSL";

    public static final String INFOBEAN_VALEURS_MODE_BO_SSL = "VALEURS_MODE_BO_SSL";

    public static final String INFOBEAN_VALEURS_ACTIONS_HTTP = "VALEURS_ACTIONS_HTTP";

    public static final String INFOBEAN_VALEURS_ACTIONS_HTTPS = "VALEURS_ACTIONS_HTTPS";

    public static final String INFOBEAN_VALEURS_MODE_REECRITURE_RUBRIQUE = "MODE_REECRITURE_RUBRIQUE";

    public static final String INFOBEAN_VALEURS_TEMPLATE_INFOSSITE = "VALEURS_TEMPLATE_INFOSSITE";

    public static final String INFOBEAN_VALEURS_LISTE_TEMPLATES_INFOSSITE = "VALEURS_LISTE_TEMPLATE_INFOSSITE";

    public static final String INFOBEAN_TYPE_SUPPRESSION = "TYPE_SUPPRESSION";

    public static final String INFOBEAN_SUPPRESSION_RUBRIQUE = "SUPPRESSION_RUBRIQUE";

    public static final String INFOBEAN_NO_ARBO = "NO_ARBO";

    public static final String INFOBEAN_WITH_ENCADRE = "WITH_ENCADRE";

    public static final String INFOBEAN_WITH_FORMULAIRE = "WITH_FORMULAIRE";

    public static final String INFOBEAN_WITH_NEWSLETTER = "WITH_NEWSLETTER";

    public static final String INFOBEAN_WITH_MEDIA = "WITH_MEDIA";

    public static final String INFOBEAN_WITH_FIL = "WITH_FIL";

    public static final String INFOBEAN_LISTE_CODE_OBJET = "LISTE_CODE_OBJET";

    public static final String INFOBEAN_CLE_CODE_OBJET_TOUS = "TOUSLESOBJETS";

    protected static final String MESSAGE_ERREUR_CODE_NON_PRESENT_INFOBEAN = "BO_USINESITE_ERREUR_CODE_NON_PRESENT_INFOBEAN";

    private static final Logger LOG = LoggerFactory.getLogger(SaisieInfosSiteCommun.class);

    /**
     * Autorisation de l'utilisateur actuellement en train d'accéder au processus.
     */
    protected AutorisationBean autorisations;

    protected ServiceInfosSite serviceInfosSite;

    protected ServiceTemplateSite serviceTemplateSite;

    protected ServiceTemplateSiteProperty serviceTemplateSiteProperty;

    /**
     * Constructeur.
     *
     * @param infoBean
     */
    public SaisieInfosSiteCommun(final InfoBean infoBean) {
        super(infoBean);
    }

    /**
     * Vérification de la cohérence des données.
     *
     * @param infosSite
     * @param template
     * @throws ErreursSaisieInfosSite: Erreur dans la cohérance des données.
     */
    public static void verifierDonneesInfosSite(final InfosSiteImpl infosSite, final TemplateSite template) throws ErreursSaisieInfosSite {
        final ArrayList<String> fluxMessagesErreurs = new ArrayList<>();
        if (infosSite.getNiveauMinReecritureRubrique() > infosSite.getNiveauMaxReecritureRubrique()) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage("BO_USINESITE_ERREUR_NIVEAU_REECRITURE_INVALIDE"));
        }
        if (infosSite.isSitePrincipal() && !infosSite.isActif()) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage("BO_USINESITE_ERREUR_SITE_PRINCIPAL_INACTIF"));
        }
        if (!fluxMessagesErreurs.isEmpty()) {
            throw new ErreursSaisieInfosSite(fluxMessagesErreurs, infosSite, template);
        }
    }

    /**
     * Initialiser les variables de classe du processus.
     */
    protected void initialiserServices() {
        final SessionUtilisateur session = getGp().getSessionUtilisateur();
        if (session == null) {
            return;
        }
        autorisations = (AutorisationBean) session.getInfos().get(SessionUtilisateur.AUTORISATIONS);
        serviceInfosSite = ServiceInfosSiteFactory.getServiceInfosSite();
        serviceTemplateSite = ServiceTemplateSiteFactory.getServiceTemplateSite();
        serviceTemplateSiteProperty = ServiceTemplateSitePropertyFactory.getServiceTemplateSite();
    }

    protected abstract void verificationDroitUtilisateur() throws ErreurUtilisateurNonAuthentifie, ErreurActionNonAutorisee;

    /**
     * Vérifie si les autorisations courantes possèdent bien la permission de modification sur le périmètre donnée.
     *
     * @param siteAVerifier
     * @return
     */
    protected boolean possedePermissionModificationPourSite(final InfosSite siteAVerifier) {
        final PermissionBean permissionModification = UsineSiteComposantUtil.getPermissionModification();
        final Perimetre perimetreFiche = new Perimetre(StringUtils.EMPTY, siteAVerifier.getCodeRubrique(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        return autorisations.isWebMaster() || autorisations.possedePermission(permissionModification, perimetreFiche);
    }

    /**
     * Vérifie si les autorisations courantes possèdent bien la permission de CRUD sur le périmètre donnée.
     *
     * @param siteAVerifier
     * @return
     */
    protected boolean possedePermissionGestionPourSite(final InfosSite siteAVerifier) {
        final PermissionBean permissionGestion = UsineSiteComposantUtil.getPermissionGestion();
        final Perimetre perimetreFiche = new Perimetre(StringUtils.EMPTY, siteAVerifier.getCodeRubrique(), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
        return autorisations.isWebMaster() || autorisations.possedePermission(permissionGestion, perimetreFiche);
    }

    /**
     * Récupère l'infosSite à dupliqué, supprime les données non duplicable du site et ajoute à l'infobean ces valeurs.
     *
     * @return
     * @throws Exception
     */
    protected abstract void preparerDuplicationInfosSite() throws Exception;

    /**
     * Récupère l'infosSite à supprimer et ajoute à l'infobean ses valeurs.
     *
     * @return
     * @throws Exception
     */
    protected abstract void preparerSuppressionInfosSite() throws Exception;

    /**
     * Prépare la liste des infosSite visible par l'utilisateur.
     *
     * @return
     * @throws Exception
     */
    protected void listerInfosSite() throws Exception {
        final Collection<InfosSite> listeInfosSites = serviceInfosSite.getListeTousInfosSites();
        if (!autorisations.isWebMaster()) {
            CollectionUtils.filter(listeInfosSites, new Predicate() {

                @Override
                public boolean evaluate(final Object object) {
                    final InfosSite siteCourant = (InfosSite) object;
                    return possedePermissionGestionPourSite(siteCourant) || possedePermissionModificationPourSite(siteCourant);
                }
            });
        }
        final Map<String, TemplateSite> templatesParCodesTemplates = new HashMap<>();
        @SuppressWarnings("unchecked")
        final Collection<String> codesTemplate = CollectionUtils.collect(listeInfosSites, new Transformer() {

            @Override
            public String transform(final Object input) {
                return ((InfosSite) input).getCodeTemplate();
            }
        });
        for (final String codeTemplate : codesTemplate) {
            templatesParCodesTemplates.put(codeTemplate, serviceTemplateSite.getTemplateSiteParCode(codeTemplate));
        }
        infoBean.set(INFOBEAN_IS_GESTIONNAIRE_SITE, autorisations.possedePermission(UsineSiteComposantUtil.getPermissionGestion()) || autorisations.isWebMaster());
        infoBean.set(INFOBEAN_VALEURS_LISTE_TEMPLATES_INFOSSITE, templatesParCodesTemplates);
        infoBean.set(INFOBEAN_LISTE_INFOSSITES, listeInfosSites);
    }

    /**
     *
     * @return Ecran logique de redirection.
     * @throws Exception
     */
    protected void preparerAjoutInfosSite() throws Exception {
        final InfosSite infosSiteVide = new InfosSiteImpl();
        setDonneesPourSaisieDansInfoBean(infosSiteVide, null);
        infoBean.set(INFOBEAN_ETAT_INTERFACE_SAISIE, EtatInterfaceSaisie.CREATION);
    }

    /**
     * @param infosSite
     * @param template
     * @throws Exception
     *             Erreur durant la récupération des templates de sites.
     */
    protected void setDonneesPourSaisieDansInfoBean(final InfosSite infosSite, final TemplateSite template) throws Exception {
        infoBean.set(INFOBEAN_INFOSSITE, infosSite);
        infoBean.set(INFOBEAN_VALEURS_TEMPLATE_INFOSSITE, template);
        infoBean.set(INFOBEAN_VALEURS_LISTE_TEMPLATES_INFOSSITE, serviceTemplateSite.getListeTemplatesSite());
        infoBean.set(INFOBEAN_VALEURS_MODE_SSL, InfosSiteHelper.SSL_MODE);
        infoBean.set(INFOBEAN_VALEURS_MODE_BO_SSL, InfosSiteHelper.BO_SSL_MODE);
        infoBean.set(INFOBEAN_VALEURS_ACTIONS_HTTP, InfosSiteHelper.HTTP_ACTIONS);
        infoBean.set(INFOBEAN_VALEURS_ACTIONS_HTTPS, InfosSiteHelper.HTTPS_ACTIONS);
        infoBean.set(INFOBEAN_VALEURS_MODE_REECRITURE_RUBRIQUE, InfosSiteHelper.MODE_REECRITURE_RUBRIQUE);
    }

    /**
     * Préparer l'infoBean pour préparer la modification des données du {@link InfosSite}.
     *
     * @return
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurActionNonAutorisee} : impossible de trouver le paramètre CODE dans l'infoBean.</li>
     *             <li> {@link ErreurDonneeNonTrouve} : impossible de charger le {@link InfosSite} à modifier.</li>
     *             <li>Autres :
     *             <ul>
     *             <li>Erreur durant l'accés à la source de données des {@link InfosSite}</li>
     *             <li>Erreur durant la récupération du template</li>
     *             </ul>
     *             </li>
     *             </ul>
     */
    protected void preparerModificationInfosSite() throws Exception {
        final String code = getCodeDepuisInfoBean();
        final InfosSite infosSite = serviceInfosSite.getInfosSite(code);
        try {
            if (!possedePermissionGestionPourSite(infosSite) && !possedePermissionModificationPourSite(infosSite)) {
                throw new ErreurActionNonAutorisee();
            }
            final TemplateSite template = serviceTemplateSite.getTemplateSiteParCodeSite(code);
            setDonneesPourSaisieDansInfoBean(infosSite, template);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("unable to find any data for this website", e);
            setDonneesPourSaisieDansInfoBean(infosSite, null);
        }
        infoBean.set(INFOBEAN_ETAT_INTERFACE_SAISIE, EtatInterfaceSaisie.MODIFICATION);
        infoBean.setTitreEcran(infosSite.getIntitule());
    }

    protected void validerAjouter() throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        final TemplateSite template = getTemplateDepuisInfosBean();
        SaisieInfosSiteInfoBeanHelper.peuplerInfosSiteDepuisInfoBean(infoBean, infosSite, template);
        verifierDonneesInfosSite(infosSite, template);
        if (!possedePermissionGestionPourSite(infosSite)) {
            throw new ErreurActionNonAutorisee();
        }
        final String codeUtilisateur = autorisations.getCode();
        try {
            serviceInfosSite.creer(infosSite, codeUtilisateur);
            lancementTraitementsComplementairesTemplate(infosSite, template);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("unable to find any data for this website", e);
            throw new ErreursSaisieInfosSite(e.getMessage(), infosSite, template);
        }
        infoBean.addMessageConfirmation(String.format(MessageHelper.getCoreMessage("CONFIRMATION_CREATION_SITE"), infosSite.getIntitule()));
    }

    /**
     * Récupérer le template de site depuis l'infoBean.
     *
     * @return Le {@link TemplateSite} référencé dans le {@link InfoBean} (clé infoBean {@link #INFOBEAN_TEMPLATE})
     * @throws Exception
     *             Erreur durant l'accés à la source de données des {@link TemplateSite}.
     */
    protected TemplateSite getTemplateDepuisInfosBean() throws Exception {
        final String codeTemplate = infoBean.getString(INFOBEAN_TEMPLATE);
        if (StringUtils.isEmpty(codeTemplate)) {
            return null;
        }
        try {
            return serviceTemplateSite.getTemplateSiteParCode(codeTemplate);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("le template n'existe pas", e);
            return null;
        }
    }

    /**
     * Modifier un {@link InfosSite}.
     *
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurActionNonAutorisee} : impossible de trouver le paramétre CODE dans l'infoBean.</li>
     *             <li> {@link ErreurDonneeNonTrouve} : impossible de charger le {@link InfosSite} à modifier.</li>
     *             <li> {@link ErreurUniciteNonRespectee} : le code du site est déjà utilisé par un autre site => ne devrait pas arriver</li>
     *             <li> {@link ErreursSaisieInfosSite} : erreur durant la varfication des données saisies</li>
     *             <li>Autres :
     *             <ul>
     *             <li>Erreur durant l'accés à la source de données des {@link InfosSite}</li>
     *             <li>Erreur durant la récupération du template</li>
     *             </ul>
     *             </li>
     *             </ul>
     */
    protected void validerModification() throws Exception {
        final String code = getCodeDepuisInfoBean();
        final InfosSiteImpl infosSite = (InfosSiteImpl) serviceInfosSite.getInfosSite(code);
        if (!possedePermissionModificationPourSite(infosSite) && !possedePermissionGestionPourSite(infosSite)) {
            throw new ErreurActionNonAutorisee();
        }
        final InfosSite sitePrincipal = serviceInfosSite.getPrincipalSite();
        final TemplateSite template = getTemplateDepuisInfosBean();
        SaisieInfosSiteInfoBeanHelper.peuplerInfosSiteDepuisInfoBean(infoBean, infosSite, template);
        verifierDonneesInfosSite(infosSite, template);
        if (infoBean.get(INFOBEAN_PRINCIPAL) != null && sitePrincipal != null && !code.equals(sitePrincipal.getAlias())) {
            if (!possedePermissionModificationPourSite(sitePrincipal) && !possedePermissionGestionPourSite(sitePrincipal)) {
                throw new ErreurActionNonAutorisee();
            }
            final InfosSiteImpl ancienSitePrincipal = (InfosSiteImpl) sitePrincipal;
            ancienSitePrincipal.setSitePrincipal(Boolean.FALSE);
            serviceInfosSite.modifier(ancienSitePrincipal);
        }
        final String codeUtilisateur = autorisations.getCode();
        try {
            serviceInfosSite.modifier(infosSite, codeUtilisateur);
            lancementTraitementsComplementairesTemplate(infosSite, template);
            serviceInfosSite.cleanFichiers(infosSite);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("unable to find any data for this website", e);
            throw new ErreursSaisieInfosSite(e.getMessage(), infosSite, template);
        }
        infoBean.addMessageConfirmation(String.format(MessageHelper.getCoreMessage("CONFIRMATION_MODIFICATION_SITE"), infosSite.getIntitule()));
    }

    protected void lancementTraitementsComplementairesTemplate(final InfosSiteImpl infosSite, final TemplateSite template) throws Exception {
        if (template == null) {
            return;
        }
        for (final TemplateSiteProperty property : template.getListeProprietesComplementaires()) {
            try {
                final TemplateSitePropertyTraitement<TemplateSiteProperty> traitement = serviceTemplateSiteProperty.getTemplateSitePropertyTraitement(property);
                traitement.traiter(infosSite, template, property, infoBean);
            } catch (final ErreurDonneeNonTrouve e) {
                LOG.debug("la propriété n'a pas été trouvé", e);
            }
        }
    }

    /**
     * Supprimer un {@link InfosSite} de la source de données.
     *
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurActionNonAutorisee} : impossible de trouver le paramètre CODE dans l'infoBean.</li>
     *             <li>Autres : Erreur durant l'accés à la source de données des {@link InfosSite}</li>
     *             </ul>
     */
    protected abstract void validerSuppression() throws Exception;

    /**
     * récupérer le code du {@link InfosSite}. Si ce code n'est pas présent dans requête HTTP alors une exception est levée.
     *
     * @return Le code du {@link InfosSite} renseigné dans le {@link InfoBean} du processus (clé infoBean {@link #INFOBEAN_CODE}).
     * @throws ErreurActionNonAutorisee
     *             Exception levée quand le code est introuvable dans le {@link InfoBean}
     */
    protected String getCodeDepuisInfoBean() throws ErreurActionNonAutorisee {
        final String code = infoBean.getString(INFOBEAN_CODE);
        if (StringUtils.isEmpty(code)) {
            throw new ErreurActionNonAutorisee(MessageHelper.getCoreMessage(MESSAGE_ERREUR_CODE_NON_PRESENT_INFOBEAN));
        }
        return code;
    }

    /**
     * Enumération des états de l'interface de saisie.
     *
     * @author pierre.cosson
     *
     */
    public enum EtatInterfaceSaisie {
        CREATION, MODIFICATION
    }
}
