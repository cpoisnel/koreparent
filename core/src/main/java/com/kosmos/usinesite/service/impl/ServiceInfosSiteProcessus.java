package com.kosmos.usinesite.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.service.ServiceBean;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.utils.InfosSiteHelper;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.dao.InfosSiteDao;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.utils.URLResolver;

/**
 * Service qui permet de gérer des {@link InfosSite} depuis la source de données. Il permet de créer/modifier/supprimer et lister l'ensemble des infosSite de l'application.
 *
 * @author pierre.cosson
 */
public class ServiceInfosSiteProcessus extends Observable implements ServiceInfosSite, ServiceBean<InfosSiteImpl> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceInfosSiteProcessus.class);

    private static final String HTTPS = "https";

    private static final String HTTP = "http";

    /**
     * DAO permettant l'accés au sauvegarde de {@link InfosSite}.
     */
    private InfosSiteDao infosSiteDao = null;

    private ServiceRubrique serviceRubrique;

    private ServiceRubriquePublication serviceRubriquePublication;

    public void setServiceRubrique(ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void setServiceRubriquePublication(ServiceRubriquePublication serviceRubriquePublication) {
        this.serviceRubriquePublication = serviceRubriquePublication;
    }

    public void setInfosSiteDao(final InfosSiteDao infosSiteDao) {
        this.infosSiteDao = infosSiteDao;
    }

    @Override
    public void save(final InfosSiteImpl bean) {
        throw new UnsupportedOperationException("Cette opération n'est pas supportée par le service. Utilisez \"creer\" ou \"modifier\"");
    }

    @Override
    public void delete(final Long id) {
        throw new UnsupportedOperationException("Cette opération n'est pas supportée par le service. Utilisez \"supprimer\"");
    }

    @Override
    public InfosSiteImpl getById(final Long id) {
        throw new UnsupportedOperationException("Cette opération n'est pas supportée par le service. Utilisez \"getInfosSite\"");
    }

    @Override
    public void creer(final InfosSiteImpl infosSite) throws Exception {
        creer(infosSite, null);
    }

    @Override
    @Caching(
        evict = {
            @CacheEvict(value = "ServiceInfosSiteProcessus.getByHost", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getBySection", key = "#infosSite.getCodeRubrique()", beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getAliasHostList", key = "#infosSite.getAlias()", beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getListeTousInfosSites", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getSitePrincipal", condition = "#infosSite.isSitePrincipal() == true", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getSitesList", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getInfosSite", beforeInvocation = true, key = "#infosSite.getAlias()")
        }
    )
    public void creer(final InfosSiteImpl infosSite, final String codeUtilisateur) throws Exception {
        verifierUniciteCode(infosSite);
        checkHostUnicity(infosSite);
        peuplerDonneesSauvegardeEnCreation(infosSite, codeUtilisateur);
        infosSiteDao.creer(infosSite);
        setChanged();
        notifyObservers(infosSite);
    }

    private void peuplerDonneesSauvegardeEnCreation(final InfosSiteImpl nouvelleSauvegarde, final String codeUtilisateur) {
        nouvelleSauvegarde.setCodeCreateur(StringUtils.defaultString(codeUtilisateur));
        nouvelleSauvegarde.setDateCreation(new Date());
        nouvelleSauvegarde.setCodeDernierModificateur(StringUtils.defaultString(codeUtilisateur));
        nouvelleSauvegarde.setDateDerniereModification(new Date());
    }

    @Override
    public void modifier(final InfosSiteImpl infosSite) throws Exception {
        modifier(infosSite, null);
    }

    @Override
    @Caching(
        evict = {
            @CacheEvict(value = "ServiceInfosSiteProcessus.getByHost", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getBySection", key = "#root.target.getInfosSite(#nouvelleSauvegarde.getAlias()).getCodeRubrique()", beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getSitePrincipal", condition = "#nouvelleSauvegarde.isSitePrincipal() == true || #root.target.getInfosSite(#nouvelleSauvegarde.getAlias()).isSitePrincipal() == true", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getBySection", key = "#nouvelleSauvegarde.getCodeRubrique()", beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getAliasHostList", key = "#nouvelleSauvegarde.getAlias()", beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getListeTousInfosSites", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getSitesList", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getInfosSite", beforeInvocation = true, key = "#nouvelleSauvegarde.getAlias()")
        }

    )
    public void modifier(final InfosSiteImpl nouvelleSauvegarde, final String codeUtilisateur) throws Exception {
        final InfosSite sauvegardeOriginale = infosSiteDao.getInfosSite(nouvelleSauvegarde.getAlias());
        peuplerDonneesSauvegardeEnModification(nouvelleSauvegarde, sauvegardeOriginale, codeUtilisateur);
        verifierUniciteCode(nouvelleSauvegarde);
        checkHostUnicity(nouvelleSauvegarde);
        infosSiteDao.miseAJour(nouvelleSauvegarde);
        if (isUrlChanged(sauvegardeOriginale, nouvelleSauvegarde)) {
            setChanged();
            notifyObservers(nouvelleSauvegarde);
        }
    }

    private boolean isUrlChanged(InfosSite sauvegardeOriginale, InfosSite nouvelleSauvegarde) {
        boolean isUrlChanged = !sauvegardeOriginale.getHttpHostname().equals(nouvelleSauvegarde.getHttpHostname());
        isUrlChanged = isUrlChanged || !sauvegardeOriginale.getHttpsHostname().equals(nouvelleSauvegarde.getHttpsHostname());
        isUrlChanged = isUrlChanged || !sauvegardeOriginale.getCodeRubrique().equals(nouvelleSauvegarde.getCodeRubrique());
        isUrlChanged = isUrlChanged || sauvegardeOriginale.getHttpPort() != nouvelleSauvegarde.getHttpPort();
        isUrlChanged = isUrlChanged || sauvegardeOriginale.getHttpsPort() != nouvelleSauvegarde.getHttpsPort();
        isUrlChanged = isUrlChanged || sauvegardeOriginale.getModeReecritureRubrique() != nouvelleSauvegarde.getModeReecritureRubrique();
        isUrlChanged = isUrlChanged || sauvegardeOriginale.getNiveauMaxReecritureRubrique() != nouvelleSauvegarde.getNiveauMaxReecritureRubrique();
        isUrlChanged = isUrlChanged || sauvegardeOriginale.getNiveauMinReecritureRubrique() != nouvelleSauvegarde.getNiveauMinReecritureRubrique();
        isUrlChanged = isUrlChanged || sauvegardeOriginale.getSslMode() != nouvelleSauvegarde.getSslMode();
        return isUrlChanged;
    }

    @Override
    @Caching(
        evict = {
            @CacheEvict(value = "ServiceInfosSiteProcessus.getByHost", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getBySection", key = "#root.target.getInfosSite(#code).getCodeRubrique()", beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getSitePrincipal", condition = "#root.target.getInfosSite(#code).isSitePrincipal() == true", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getAliasHostList", key = "#root.target.getInfosSite(#code).getAlias()", beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getListeTousInfosSites", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getSitesList", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "ServiceInfosSiteProcessus.getInfosSite", beforeInvocation = true,  key = "#code")
        }
    )
    public void supprimer(final String code) throws Exception {
        final InfosSite siteASupprimer = getInfosSite(code);
        infosSiteDao.supprimer(code);
        if (siteASupprimer != null) {
            setChanged();
            notifyObservers(siteASupprimer);
        }
    }

    @Override
    @Cacheable(value="ServiceInfosSiteProcessus.getInfosSite", key="#code", unless= "#result == null")
    public InfosSite getInfosSite(final String code) {
        try {
            return infosSiteDao.getInfosSite(code);
        }catch(Exception e) {
            LOG.error(String.format("Une erreur est survenue lors de la récupération du site correspondant au code %s", code), e);
        }
        return null;
    }

    @Override
    @Cacheable(value = "ServiceInfosSiteProcessus.getListeTousInfosSites")
    public Collection<InfosSite> getListeTousInfosSites() throws Exception {
        return infosSiteDao.getListeInfosSites();
    }

    @Override
    public void enregistrerFichier(final InfosSite infosSite, final File fichierTemporaireSource, final String nomPropertyTemplateSite) throws Exception {
        try {
            final String pathFichier = InfosSiteHelper.getPathAbsoluFichierPropertyTemplate(infosSite, nomPropertyTemplateSite);
            final File fichierUploade = new File(pathFichier);
            if (fichierUploade.exists()) {
                if (!fichierUploade.delete()) {
                    LOG.info("impossible de supprimer le fichier " + pathFichier);
                }
            }
            FileUtils.moveFile(fichierTemporaireSource, fichierUploade);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.info("impossible de trouver le chemin du fichier", e);
        }
    }

    @Override
    public void supprimerFichier(final InfosSite infosSite, final String nomPropertyTemplateSite) {
        try {
            final String pathFichier = InfosSiteHelper.getPathAbsoluFichierPropertyTemplate(infosSite, nomPropertyTemplateSite);
            final File fichierASupprimer = new File(pathFichier);
            if (fichierASupprimer.exists()) {
                if (!fichierASupprimer.delete()) {
                    LOG.info("impossible de supprimer le fichier " + pathFichier);
                }
            }
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.info("impossible de trouver le chemin du fichier", e);
        }
    }

    @Override
    public void supprimerTousFichiers(final InfosSite infosSite) throws IOException {
        final String pathDossier = InfosSiteHelper.getPathAbsoluFichiersTemplateDuSite(infosSite);
        final File dossierDepot = new File(pathDossier);
        if (dossierDepot.exists()) {
            FileUtils.deleteDirectory(dossierDepot);
        }
    }

    @Override
    public void cleanFichiers(final InfosSite infosSite) throws IOException {
        final String pathDossier = InfosSiteHelper.getPathAbsoluFichiersTemplateDuSite(infosSite);
        final File dossierDepot = new File(pathDossier);
        if (!dossierDepot.exists()) {
            return;
        }
        for (final File dossierPropertyTemplate : dossierDepot.listFiles()) {
            if (!dossierPropertyTemplate.isDirectory()) {
                // dans le dossier de dépôt, il doit seulement y avoir des
                // dossiers.
                if (!dossierPropertyTemplate.delete()) {
                    LOG.info("impossible de supprimer le dossier " + pathDossier);
                }
                continue;
            }
            final String nomPropertyTemplate = dossierPropertyTemplate.getName();
            final Object valeurPropertyTemplate = infosSite.getProprieteComplementaire(nomPropertyTemplate);
            if (valeurPropertyTemplate == null || !(valeurPropertyTemplate instanceof String) || StringUtils.isEmpty((String) valeurPropertyTemplate)) {
                // le dossier ne correspond à aucuen propriété valide
                FileUtils.deleteDirectory(dossierPropertyTemplate);
                continue;
            }
            final String nomFichierAttendu = (String) valeurPropertyTemplate;
            for (final File fichierContenuDansDossier : dossierPropertyTemplate.listFiles()) {
                if (fichierContenuDansDossier.isDirectory()) {
                    // pas de dossier à ce niveau, seulement des fichiers
                    FileUtils.deleteDirectory(fichierContenuDansDossier);
                    continue;
                }
                final String nomFichier = fichierContenuDansDossier.getName();
                if (!StringUtils.equalsIgnoreCase(nomFichierAttendu, nomFichier)) {
                    fichierContenuDansDossier.delete();
                }
            }
        }
        // Suppression du dossier du site si vide
        if (dossierDepot.listFiles().length == 0) {
            FileUtils.deleteDirectory(dossierDepot);
        }
    }

    private void peuplerDonneesSauvegardeEnModification(final InfosSiteImpl nouvelleSauvegarde, final InfosSite sauvegardeOriginale, final String codeUtilisateur) {
        nouvelleSauvegarde.setIdInfosSite(sauvegardeOriginale.getIdInfosSite());
        nouvelleSauvegarde.setCodeCreateur(sauvegardeOriginale.getCodeCreateur());
        nouvelleSauvegarde.setDateCreation(sauvegardeOriginale.getDateCreation());
        nouvelleSauvegarde.setCodeDernierModificateur(StringUtils.defaultString(codeUtilisateur));
        nouvelleSauvegarde.setDateDerniereModification(new Date());
        nouvelleSauvegarde.setHistorique(sauvegardeOriginale.toString());
    }

    /**
     * Vérifier que le code est déjà utilisé ou non dans la source de donnée.
     *
     * @param sauvegarde
     * @throws Exception
     *             Erreur d'accés à la source de données.
     */
    private void verifierUniciteCode(final InfosSite sauvegarde) throws Exception {
        final String code = sauvegarde.getAlias();
        try {
            final InfosSite sauvegardeBis = infosSiteDao.getInfosSite(code);
            if (sauvegarde.getIdInfosSite() == sauvegardeBis.getIdInfosSite()) {
                // cas d'une modification : le code est le même mais les IDs
                // aussi donc les mêmes sites
                return;
            }
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("sauvegarde non trouvée, code inexistant", e);
            return;
        }
        throw new ErreursSaisieInfosSite("Code '" + code + "' déjà utilisé pour un autre site.", sauvegarde, null);
    }

    private void checkHostUnicity(final InfosSite infosSite) throws Exception {
        for(InfosSite currentInfosSite : infosSiteDao.getListeInfosSites()) {
            if(currentInfosSite.getAlias().equals(infosSite.getAlias())) {
                continue;
            }
            if (StringUtils.isNotBlank(infosSite.getHttpHostname())) {
                if (infosSite.getHttpHostname().equals(currentInfosSite.getHttpHostname()) || infosSite.getHttpHostname().equals(currentInfosSite.getHttpsHostname())) {
                    throw new ErreursSaisieInfosSite(String.format(MessageHelper.getCoreMessage("BO_USINESITE_ERREUR_HOST_HTTP_EXISTANT"), infosSite.getHttpHostname(), currentInfosSite.getAlias()), infosSite, null);
                }
            }
            if (StringUtils.isNotBlank(infosSite.getHttpsHostname())) {
                if (infosSite.getHttpsHostname().equals(currentInfosSite.getHttpHostname()) || infosSite.getHttpsHostname().equals(currentInfosSite.getHttpsHostname())) {
                    throw new ErreursSaisieInfosSite(String.format(MessageHelper.getCoreMessage("BO_USINESITE_ERREUR_HOST_HTTPS_EXISTANT"), infosSite.getHttpsHostname(), currentInfosSite.getAlias()), infosSite, null);
                }
            }
        }
    }

    @Override
    @Cacheable(value = "ServiceInfosSiteProcessus.getByHost")
    public InfosSite getSiteByHost(String host) {
        try {
            final Collection<InfosSite> sites = getListeTousInfosSites();
            for (InfosSite currentSite : sites) {
                if (host.equals(currentSite.getHttpHostname()) || host.equals(currentSite.getHttpsHostname()) || currentSite.getListeHostAlias().contains(host)) {
                    return currentSite;
                }
            }
        } catch (Exception e) {
            LOG.warn(String.format("Le site correspondant à l'host %s n'a pas pu être récupéré.", host), e);
        }
        return new InfosSiteImpl();
    }

    @Override
    @Cacheable(value = "ServiceInfosSiteProcessus.getBySection")
    public InfosSite getSiteBySection(String section) {
        try {
            final Collection<InfosSite> sites = getListeTousInfosSites();
            for (InfosSite currentSite : sites) {
                if (section.equals(currentSite.getCodeRubrique())) {
                    return currentSite;
                }
            }
        } catch (Exception e) {
            LOG.warn(String.format("Le site correspondant à la rubrique %s n'a pas pu être récupéré.", section), e);
        }
        return null;
    }

    @Override
    @Cacheable(value = "ServiceInfosSiteProcessus.getSitePrincipal")
    public InfosSite getPrincipalSite() {
        try {
            final Collection<InfosSite> sites = infosSiteDao.getListeInfosSites();
            for (InfosSite currentSite : sites) {
                if (currentSite.isSitePrincipal()) {
                    return currentSite;
                }
            }
            if(CollectionUtils.isNotEmpty(sites)) {
                final InfosSiteImpl newSitePrincipal = (InfosSiteImpl)sites.iterator().next();
                newSitePrincipal.setSitePrincipal(Boolean.TRUE);
                modifier(newSitePrincipal);
                return newSitePrincipal;
            }
        } catch (Exception e) {
            LOG.warn("Le site principal n'a pas pu être récupéré.", e);
        }
        return new InfosSiteImpl();
    }

    @Override
    @Cacheable(value = "ServiceInfosSiteProcessus.getAliasHostList")
    public Collection<String> getAliasHostList(String code) {
        final Collection<String> list = new ArrayList<>();
        try {
            final InfosSite currentSite = infosSiteDao.getInfosSite(code);
            if (StringUtils.isNotEmpty(currentSite.getHttpHostname())) {
                list.add(URLResolver.getBasePath(currentSite, Boolean.FALSE));
                list.add(URLResolver.getBasePath(currentSite, Boolean.TRUE));
            }
            if (StringUtils.isNotEmpty(currentSite.getHttpsHostname())) {
                list.add(URLResolver.getBasePath(currentSite, Boolean.TRUE));
            }
            for (final String hostAlias : currentSite.getListeHostAlias()) {
                if (StringUtils.isNotEmpty(hostAlias)) {
                    list.add(URLResolver.getBasePath(HTTP, hostAlias, currentSite.getHttpPort()));
                    list.add(URLResolver.getBasePath(HTTPS, hostAlias, currentSite.getHttpsPort()));
                }
            }
        } catch (Exception e) {
            LOG.error("Une erreur est survenue lors de la récupération de la liste des Hosts et Alias.", e);
        }
        return list;
    }

    @Override
    public InfosSite determineSite(final String code, boolean fallBack) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        RubriqueBean infosRubrique = serviceRubrique.getRubriqueByCode(code);
        return determineSite(infosRubrique, fallBack);
    }

    public InfosSite determineSite(final RubriqueBean rubrique, boolean fallBack) {
        InfosSite result = null;
        if (rubrique != null) {
            RubriqueBean currentSection = rubrique;
            while (result == null && currentSection != null) {
                result = getSiteBySection(currentSection.getCode());
                if (result == null) {
                    currentSection = serviceRubrique.getRubriqueByCode(currentSection.getCodeRubriqueMere());
                }
            }
        }
        if (result == null && fallBack) {
            result = getPrincipalSite();
        }
        return result;
    }

    @Override
    public InfosSite displaySite(final FicheUniv fiche) {
        // on regarde si la fiche est rattachée dans un autre site
        // par sa rubrique principale
        String rubriquePrincipale = "";
        if (fiche != null) {
            rubriquePrincipale = fiche.getCodeRubrique();
        }
        InfosSite infosSite = determineSite(rubriquePrincipale, true);
        if (infosSite == null) {
            Collection<String> rubsPubs = serviceRubriquePublication.getRubriqueDestByFicheUniv(fiche);
            final Iterator<String> it = rubsPubs.iterator();
            while (it.hasNext() && infosSite == null) {
                infosSite = determineSite(it.next(), true);
            }
        }
        return infosSite;
    }

    @Override
    public Collection<String> getFullAliasHostList() {
        final Collection<String> hosts = new ArrayList<>();
        final Map<String, InfosSite> sites = getSitesList();
        for(String currentCode : sites.keySet()) {
            for (final String host : getAliasHostList(currentCode)) {
                hosts.add(host);
            }
        }
        return hosts;
    }

    @Override
    @Cacheable(value = "ServiceInfosSiteProcessus.getSitesList")
    public Map<String, InfosSite> getSitesList() {
        final Map<String, InfosSite> list = new HashMap<>();
        try {
            final Collection<InfosSite> sites = infosSiteDao.getListeInfosSites();
            for (InfosSite currentSite : sites) {
                list.put(currentSite.getAlias(), currentSite);
            }
        } catch (Exception e) {
            LOG.error("Une erreur est survenue lors de la récupération de la liste des sites.", e);
        }
        return list;
    }

}
