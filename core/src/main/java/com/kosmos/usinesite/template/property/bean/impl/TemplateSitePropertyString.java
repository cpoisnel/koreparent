package com.kosmos.usinesite.template.property.bean.impl;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;

public class TemplateSitePropertyString extends AbstractTemplateSiteProperty {

    private static final int TAILLE_MAX = 255;

    private String valeurDefaut = StringUtils.EMPTY;

    private int tailleMaximum = TAILLE_MAX;

    private String regExpValidation = StringUtils.EMPTY;

    private Collection<String> listeValeursAutorisees = Collections.emptyList();

    public String getValeurDefaut() {
        return valeurDefaut;
    }

    public void setValeurDefaut(final String valeurDefaut) {
        this.valeurDefaut = valeurDefaut;
    }

    public int getTailleMaximum() {
        return tailleMaximum;
    }

    public void setTailleMaximum(final int tailleMaximum) {
        this.tailleMaximum = tailleMaximum;
    }

    public String getRegExpValidation() {
        return regExpValidation;
    }

    public void setRegExpValidation(final String regExpValidation) {
        this.regExpValidation = regExpValidation;
    }

    public Collection<String> getListeValeursAutorisees() {
        return listeValeursAutorisees;
    }

    public void setListeValeursAutorisees(final Collection<String> listeValeursAutorisees) {
        this.listeValeursAutorisees = listeValeursAutorisees;
    }
}
