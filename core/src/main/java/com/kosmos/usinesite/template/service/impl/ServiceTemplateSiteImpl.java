package com.kosmos.usinesite.template.service.impl;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.service.ServiceTemplateSite;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.dao.InfosSiteDao;

/**
 * Service permettant de récupérer les {@link TemplateSite} injectés via spring. De plus, ce service met en cache les {@link TemplateSite}.
 *
 * @author pierre.cosson
 *
 */
public class ServiceTemplateSiteImpl implements ServiceTemplateSite {

    /**
     * Dao utilisé pour récupérer les sauvegarde de site
     */
    private InfosSiteDao infosSiteDao = null;

    /**
     * Cache utilisé pour contenir la liste de tous les sites de l'application.
     */
    private List<TemplateSite> listeTemplatesSite = Collections.emptyList();

    public void setListeTemplatesSite(final List<TemplateSite> listeTemplateApplication) {
        listeTemplatesSite = listeTemplateApplication;
    }

    /**
     * @param infosSiteDao
     */
    public void setInfosSiteDao(final InfosSiteDao infosSiteDao) {
        this.infosSiteDao = infosSiteDao;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.service.impl.ServiceTemplateSite#
     * getListeTemplateSiteApplication()
     */
    @Override
    public List<TemplateSite> getListeTemplatesSite() {
        return listeTemplatesSite;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.service.impl.ServiceTemplateSite#getTemplateSiteParCode
     * (java.lang.String)
     */
    @Override
    public TemplateSite getTemplateSiteParCode(final String code) throws ErreurDonneeNonTrouve {
        for (final TemplateSite templateSite : listeTemplatesSite) {
            if (StringUtils.equalsIgnoreCase(templateSite.getCode(), code)) {
                return templateSite;
            }
        }
        throw new ErreurDonneeNonTrouve(MessageHelper.getCoreMessage("BO_USINESITE_ERREUR_AUCUN_TEMPLATE_CODE") + code);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.service.ServiceTemplateSite#getTemplateSiteParCodeSite
     * (java.lang.String)
     */
    @Override
    public TemplateSite getTemplateSiteParCodeSite(final String codeSite) throws Exception {
        final InfosSite sauvegarde = infosSiteDao.getInfosSite(codeSite);
        return getTemplateSiteParCode(sauvegarde.getCodeTemplate());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.service.ServiceTemplateSite#getTemplateSiteParDossier
     * (java.lang.String)
     */
    @Override
    public TemplateSite getTemplateSiteParDossier(final String dossierJsp) throws ErreurDonneeNonTrouve {
        for (final TemplateSite template : getListeTemplatesSite()) {
            if (StringUtils.equalsIgnoreCase(template.getDossierJSP(), dossierJsp)) {
                return template;
            }
        }
        throw new ErreurDonneeNonTrouve("Aucun TemplateSite pour le dossier : " + dossierJsp);
    }
}
