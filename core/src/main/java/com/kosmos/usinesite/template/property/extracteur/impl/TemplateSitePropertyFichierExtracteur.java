package com.kosmos.usinesite.template.property.extracteur.impl;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyFichier;
import com.kosmos.usinesite.template.property.extracteur.TemplateSitePropertyExtracteur;
import com.kosmos.usinesite.template.utils.FichierSimpleUpload;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyFichierExtracteur implements TemplateSitePropertyExtracteur<TemplateSitePropertyFichier, String> {

    public String extraire(InfosSite infosSite, TemplateSite template, TemplateSitePropertyFichier property, InfoBean data) throws Exception {
        FichierSimpleUpload fichierSimpleUpload = TemplateSiteHelper.getFichierSimpleUploadTemplateSiteProperty(template, property, data);
        if (fichierSimpleUpload != null && StringUtils.isNotEmpty(fichierSimpleUpload.intituleFichier)) {
            return fichierSimpleUpload.intituleFichier;
        } else {
            return StringUtils.EMPTY;
        }
    }
}
