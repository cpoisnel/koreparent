package com.kosmos.usinesite.template.utils;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

/**
 * Representation Java du composant d'upload de fichier. Ce "bean" permet
 * d'avoir l'ensemble des informations concernant le fichier uploadé (ou non).
 * Pour plus d'information
 *
 * @author pierre.cosson
 *
 */
public class FichierSimpleUpload {

    /**
     * Identifiant du composant (préfixe de tous les input HTML utilisés par le
     * composant)
     */
    public String code = StringUtils.EMPTY;

    /**
     * Contient une référence vers le fichier temporaire uploadé via le
     * composant d'upload de fichier
     */
    public File fichierUploade = null;

    /**
     * Intitulé du fichier présent dans ce composant. Il peut s'agir de
     * l'intitulé du fichier qui vient d'être uploadé ou de l'intitulé du
     * fichier qui a servi à initialiser le composant (qui est donc le fichier
     * déjà présent dans l'élèment)
     */
    public String intituleFichier = StringUtils.EMPTY;

    /**
     * La valeur <code>true</code> indique s'il s'agit d'un nouveau fichier
     * uplodé vennant écraser un fichier déjà présent dans l'élèment utilisant
     * ce composant (par exemple lors d'une modification d'un élèment).
     */
    public boolean isNouveauFichier = Boolean.FALSE;

    /**
     * La valeur <code>true</code> indique que le composant d'upload de fichier
     * dispose d'aucun fichier joint. Cela peut vouloir dire qu'aucun fichier
     * n'a été uploadé et que l'élèment utilisant ce composant n'a aucun fichier
     * (par exemple lors de la création de l'élèment) ou que le fichier
     * anciennement en place dans le composant a été supprimé.
     */
    public boolean isSansFichier = Boolean.TRUE;

    @Override
    public String toString() {
        return intituleFichier;
    }
}
