package com.kosmos.usinesite.template.property.traitement.impl;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyFichier;
import com.kosmos.usinesite.template.property.traitement.TemplateSitePropertyTraitement;
import com.kosmos.usinesite.template.utils.FichierSimpleUpload;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.service.ServiceInfosSiteFactory;

/**
 * Traitements liés à la gestion des fichiers (déplacement ou suppression).
 *
 * @author pierre.cosson
 *
 */
public class TemplateSitePropertyTraitementFichier implements TemplateSitePropertyTraitement<TemplateSitePropertyFichier> {

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.template.property.traitement.
     * TemplateSitePropertyTraitement
     * #traiter(com.univ.multisites.bean.impl.InfosSiteImpl,
     * com.kosmos.usinesite.template.bean.TemplateSite,
     * com.kosmos.usinesite.template.property.bean.TemplateSiteProperty,
     * com.jsbsoft.jtf.core.InfoBean)
     */
    @Override
    public void traiter(final InfosSiteImpl infosSite, final TemplateSite template, final TemplateSitePropertyFichier property, final InfoBean data) throws Exception {
        final FichierSimpleUpload fichierUploade = TemplateSiteHelper.getFichierSimpleUploadTemplateSiteProperty(template, property, data);
        if (!fichierUploade.isSansFichier && fichierUploade.isNouveauFichier && fichierUploade.fichierUploade != null) {
            ServiceInfosSiteFactory.getServiceInfosSite().enregistrerFichier(infosSite, fichierUploade.fichierUploade, property.getCode());
        }
    }
}
