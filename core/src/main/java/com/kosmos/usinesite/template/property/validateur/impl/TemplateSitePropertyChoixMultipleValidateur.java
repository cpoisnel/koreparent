package com.kosmos.usinesite.template.property.validateur.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyChoixMultiple;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyChoixMultipleValidateur implements TemplateSitePropertyValidateur<TemplateSitePropertyChoixMultiple> {

    private static final String MSG_ERREUR_PROPRIETE_TAILLE_MIN = "La propriété '%s' du template n'a pas le nombre minimal de valeurs souhaité : %d";

    private static final String MSG_ERREUR_PROPRIETE_TAILLE_MAX = "La propriété '%s' du template n'a pas le nombre maximal de valeurs souhaité : %d";

    private static final String MSG_ERREUR_CODE_INEXISTANT = "Le code '%s' n'existe pas dans la liste des choix";

    @Override
    public void valider(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyChoixMultiple property, final InfoBean data) throws Exception {
        final ArrayList<String> fluxErreurs = new ArrayList<>();
        final Collection<String> valeurs = TemplateSiteHelper.getChoixMultipleTemplateSiteProperty(template, property, data);
        testerChampObligatoire(valeurs, property, fluxErreurs);
        testerNombreValeurs(valeurs, property, fluxErreurs);
        testerValiditeCodes(valeurs, property, fluxErreurs);
        traiterFluxErreurs(fluxErreurs, infosSite, template);
    }

    private void testerChampObligatoire(final Collection<String> valeurs, final TemplateSitePropertyChoixMultiple property, final ArrayList<String> fluxErreurs) {
        if (property.isObligatoire() && CollectionUtils.isEmpty(valeurs)) {
            fluxErreurs.add(String.format(TemplateSitePropertyValidateur.MSG_ERREUR_PROPRIETE_OBLIGATOIRE, property.getLibelle()));
        }
    }

    private void testerValiditeCodes(final Collection<String> valeurs, final TemplateSitePropertyChoixMultiple property, final ArrayList<String> fluxErreurs) {
        for (final String v : valeurs) {
            if (!property.getValeurs().keySet().contains(v)) {
                fluxErreurs.add(String.format(MSG_ERREUR_CODE_INEXISTANT, v));
            }
        }
    }

    private void testerNombreValeurs(final Collection<String> valeurs, final TemplateSitePropertyChoixMultiple property, final ArrayList<String> fluxErreurs) {
        if (valeurs.size() < property.getNombreElementMin()) {
            fluxErreurs.add(String.format(MSG_ERREUR_PROPRIETE_TAILLE_MIN, property.getLibelle(), property.getNombreElementMin()));
        }
        if (valeurs.size() > property.getNombreElementMax()) {
            fluxErreurs.add(String.format(MSG_ERREUR_PROPRIETE_TAILLE_MAX, property.getLibelle(), property.getNombreElementMax()));
        }
    }

    protected void traiterFluxErreurs(final ArrayList<String> fluxErreurs, final InfosSite infosSite, final TemplateSite template) throws ErreursSaisieInfosSite {
        if (!fluxErreurs.isEmpty()) {
            throw new ErreursSaisieInfosSite(fluxErreurs, infosSite, template);
        }
    }
}
