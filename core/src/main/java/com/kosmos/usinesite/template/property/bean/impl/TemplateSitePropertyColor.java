package com.kosmos.usinesite.template.property.bean.impl;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;

public class TemplateSitePropertyColor extends TemplateSitePropertyString {

    private static final int TAILLE_MAX = 7;

    private String valeurDefaut = StringUtils.EMPTY;

    private int tailleMaximum = TAILLE_MAX;

    private String regExpValidation = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";

    private Collection<String> listeValeursAutorisees = Collections.emptyList();

    @Override
    public String getValeurDefaut() {
        return valeurDefaut;
    }

    @Override
    public void setValeurDefaut(final String valeurDefaut) {
        this.valeurDefaut = valeurDefaut;
    }

    @Override
    public int getTailleMaximum() {
        return tailleMaximum;
    }

    @Override
    public void setTailleMaximum(final int tailleMaximum) {
        this.tailleMaximum = tailleMaximum;
    }

    @Override
    public String getRegExpValidation() {
        return regExpValidation;
    }

    @Override
    public void setRegExpValidation(final String regExpValidation) {
        this.regExpValidation = regExpValidation;
    }

    @Override
    public Collection<String> getListeValeursAutorisees() {
        return listeValeursAutorisees;
    }

    @Override
    public void setListeValeursAutorisees(final Collection<String> listeValeursAutorisees) {
        this.listeValeursAutorisees = listeValeursAutorisees;
    }
}
