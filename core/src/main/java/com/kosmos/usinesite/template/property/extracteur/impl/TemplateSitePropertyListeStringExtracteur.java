package com.kosmos.usinesite.template.property.extracteur.impl;

import java.util.Collection;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyListeString;
import com.kosmos.usinesite.template.property.extracteur.TemplateSitePropertyExtracteur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyListeStringExtracteur implements TemplateSitePropertyExtracteur<TemplateSitePropertyListeString, Collection<String>> {

    public Collection<String> extraire(InfosSite infosSite, TemplateSite template, TemplateSitePropertyListeString property, InfoBean data) throws Exception {
        return TemplateSiteHelper.getListeStringTemplateSiteProperty(template, property, data);
    }
}