package com.kosmos.usinesite.template.property.service;

import com.jsbsoft.jtf.core.ApplicationContextManager;

public class ServiceTemplateSitePropertyFactory {

    public static final String ID_BEAN = "serviceTemplateSitePropertyFactory";

    private ServiceTemplateSiteProperty serviceTemplateSitePropertyInstance = null;

    /**
     * Récupérer le singleton de la factory.
     *
     * @return
     */
    private static ServiceTemplateSitePropertyFactory getInstance() {
        return (ServiceTemplateSitePropertyFactory) ApplicationContextManager.getBean(ApplicationContextManager.DEFAULT_CORE_CONTEXT, ID_BEAN);
    }

    /**
     * Récupérer l'instance de {@link ServiceTemplateSiteProperty} de l'application.
     *
     * @return L'instance (singleton).
     */
    public static ServiceTemplateSiteProperty getServiceTemplateSite() {
        return getInstance().serviceTemplateSitePropertyInstance;
    }

    public void setServiceTemplateSiteProperty(final ServiceTemplateSiteProperty serviceTemplateSiteProperty) {
        serviceTemplateSitePropertyInstance = serviceTemplateSiteProperty;
    }
}
