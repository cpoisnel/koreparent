package com.kosmos.usinesite.template.property.extracteur;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.univ.multisites.InfosSite;

/**
 * @author pierre.cosson
 *
 * @param <T>
 *            Le {@link TemplateSiteProperty} auquel s'applique cet extracteur de donnée.
 * @param <E>
 *            La nature de la donnée que l'extracteur va produire.
 */
public interface TemplateSitePropertyExtracteur<T extends TemplateSiteProperty, E> {

    E extraire(InfosSite infosSite, TemplateSite template, T property, InfoBean data) throws Exception;
}
