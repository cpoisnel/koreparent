package com.kosmos.usinesite.template.property.bean;

/**
 * Propriété de template de site. Ces propriétés servent à lister les informations complémentaires nécéssaires au bon affichage du template.
 *
 * @author pierre.cosson
 *
 */
public interface TemplateSiteProperty {

    String getCode();

    String getLibelle();

    String getDescription();

    boolean isObligatoire();
}