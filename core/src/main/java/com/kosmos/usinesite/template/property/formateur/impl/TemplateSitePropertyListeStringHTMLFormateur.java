package com.kosmos.usinesite.template.property.formateur.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyListeString;
import com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur;
import com.kosmos.usinesite.utils.FrontUASHelper;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyListeStringHTMLFormateur implements TemplateSitePropertyHTMLFormateur<TemplateSitePropertyListeString> {

    @Override
    public String formater(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyListeString property, final FormateurJSP fmt, final InfoBean data) throws Exception {
        final String nomComponsant = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        final StringBuilder out = new StringBuilder("<p>");
        out.append("<label class=\"colonne\" for=\"").append(nomComponsant).append("\">");
        if (property.isObligatoire()) {
            out.append("<span class=\"obligatoire\">").append(property.libelle).append(" * </span>");
        } else {
            out.append(property.libelle);
        }
        out.append(FrontUASHelper.genererMessageInformatif(property.getDescription()));
        out.append("</label>");
        List<String> listeValeurs = infosSite.getProprieteComplementaireListString(property.getCode());
        if (listeValeurs == null) {
            listeValeurs = property.getValeurDefaut();
        }
        out.append("<input type=\"text\"  data-role=\"tagsinput\" placeholder=\"").append(MessageHelper.getCoreMessage("USINESITE_AJOUTER_UNE_VALEUR")).append("\" name=\"");
        final String valeurFormatter = StringUtils.defaultString(StringUtils.join(listeValeurs, ","));
        out.append(nomComponsant).append("\" id=\"").append(nomComponsant).append("\" value=\"").append(valeurFormatter).append("\" />");
        out.append("</p>");
        return out.toString();
    }
}
