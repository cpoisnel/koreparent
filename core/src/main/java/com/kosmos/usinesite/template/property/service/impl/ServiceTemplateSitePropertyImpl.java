package com.kosmos.usinesite.template.property.service.impl;

import java.util.Collections;
import java.util.Map;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.kosmos.usinesite.template.property.extracteur.TemplateSitePropertyExtracteur;
import com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur;
import com.kosmos.usinesite.template.property.service.ServiceTemplateSiteProperty;
import com.kosmos.usinesite.template.property.traitement.TemplateSitePropertyTraitement;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;

public class ServiceTemplateSitePropertyImpl implements ServiceTemplateSiteProperty {

    private Map<String, TemplateSitePropertyExtracteur<TemplateSiteProperty, Object>> mapExtracteur = Collections.emptyMap();

    private Map<String, TemplateSitePropertyValidateur<TemplateSiteProperty>> mapValidateur = Collections.emptyMap();

    private Map<String, TemplateSitePropertyTraitement<TemplateSiteProperty>> mapTraitement = Collections.emptyMap();

    private Map<String, TemplateSitePropertyHTMLFormateur<TemplateSiteProperty>> mapFormateur = Collections.emptyMap();

    public void setMapExtracteur(final Map<String, TemplateSitePropertyExtracteur<TemplateSiteProperty, Object>> mapExtracteur) {
        this.mapExtracteur = mapExtracteur;
    }

    public void setMapValidateur(final Map<String, TemplateSitePropertyValidateur<TemplateSiteProperty>> mapValidateur) {
        this.mapValidateur = mapValidateur;
    }

    public void setMapTraitement(final Map<String, TemplateSitePropertyTraitement<TemplateSiteProperty>> mapTraitement) {
        this.mapTraitement = mapTraitement;
    }

    public void setMapFormateur(final Map<String, TemplateSitePropertyHTMLFormateur<TemplateSiteProperty>> mapFormateur) {
        this.mapFormateur = mapFormateur;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.template.property.service.ServiceTemplateSiteProperty
     * #
     * getTemplateSitePropertyExtracteur(com.kosmos.usinesite.template.property.
     * bean.TemplateSiteProperty)
     */
    @Override
    public TemplateSitePropertyExtracteur<TemplateSiteProperty, Object> getTemplateSitePropertyExtracteur(final TemplateSiteProperty property) throws ErreurDonneeNonTrouve {
        final String cleMap = property.getClass().getName();
        final TemplateSitePropertyExtracteur<TemplateSiteProperty, Object> extracteur = mapExtracteur.get(cleMap);
        if (extracteur != null) {
            return extracteur;
        } else {
            throw new ErreurDonneeNonTrouve("Impossible de trouver l'extracteur de données pour la property : " + cleMap);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.template.property.service.ServiceTemplateSiteProperty
     * #
     * getTemplateSitePropertyValidateur(com.kosmos.usinesite.template.property.
     * bean.TemplateSiteProperty)
     */
    @Override
    public TemplateSitePropertyValidateur<TemplateSiteProperty> getTemplateSitePropertyValidateur(final TemplateSiteProperty property) throws ErreurDonneeNonTrouve {
        final String cleMap = property.getClass().getName();
        final TemplateSitePropertyValidateur<TemplateSiteProperty> validateur = mapValidateur.get(cleMap);
        if (validateur != null) {
            return validateur;
        } else {
            throw new ErreurDonneeNonTrouve("Impossible de trouver le validateur de données pour la property : " + cleMap);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.template.property.service.ServiceTemplateSiteProperty
     * #
     * getTemplateSitePropertyTraitement(com.kosmos.usinesite.template.property.
     * bean.TemplateSiteProperty)
     */
    @Override
    public TemplateSitePropertyTraitement<TemplateSiteProperty> getTemplateSitePropertyTraitement(final TemplateSiteProperty property) throws ErreurDonneeNonTrouve {
        final String cleMap = property.getClass().getName();
        final TemplateSitePropertyTraitement<TemplateSiteProperty> traitement = mapTraitement.get(cleMap);
        if (traitement != null) {
            return traitement;
        } else {
            throw new ErreurDonneeNonTrouve("Impossible de trouver le traitement pour la property : " + cleMap);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.template.property.service.ServiceTemplateSiteProperty
     * #
     * getTemplateSitePropertyFormateur(com.kosmos.usinesite.template.property.bean
     * .TemplateSiteProperty)
     */
    @Override
    public TemplateSitePropertyHTMLFormateur<TemplateSiteProperty> getTemplateSitePropertyFormateur(final TemplateSiteProperty property) throws ErreurDonneeNonTrouve {
        final String cleMap = property.getClass().getName();
        final TemplateSitePropertyHTMLFormateur<TemplateSiteProperty> formateur = mapFormateur.get(cleMap);
        if (formateur != null) {
            return formateur;
        } else {
            throw new ErreurDonneeNonTrouve("Impossible de trouver le formateur HTML pour la property : " + cleMap);
        }
    }

    public Map<String, TemplateSitePropertyExtracteur<TemplateSiteProperty, Object>> getMapExtracteur() {
        return mapExtracteur;
    }

    public Map<String, TemplateSitePropertyValidateur<TemplateSiteProperty>> getMapValidateur() {
        return mapValidateur;
    }

    public Map<String, TemplateSitePropertyTraitement<TemplateSiteProperty>> getMapTraitement() {
        return mapTraitement;
    }

    public Map<String, TemplateSitePropertyHTMLFormateur<TemplateSiteProperty>> getMapFormateur() {
        return mapFormateur;
    }
}
