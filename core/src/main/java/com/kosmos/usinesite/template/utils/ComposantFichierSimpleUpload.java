package com.kosmos.usinesite.template.utils;

import java.io.File;

import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.utils.EscapeString;

/**
 * Composant d'upload de fichier simple (pas de fichiergw).
 *
 * <br/>
 * L'intégration du composant dans une interface doit aussi inclure le script JS permettant de rendre dynamique le composant. Sans ce JS le composant est inutilisable.
 *
 * @author pierre.cosson
 *
 */
public class ComposantFichierSimpleUpload {

    private static final String SUFFIXE_FILEUPLOAD = "_FILE";

    private static final String SUFFIXE_NOM_FICHIER_ORIGINAL = "_ORI";

    /**
     * Initialiser l'infoBean pour pouvoir générer le composant de saisie d'upload de fichier avec une valeur déjà présente. Cette fonction doit etre appelée côté processus, avant
     * l'appel de la JSP. Son rôle est de placer les données permettant d'initialiser le champ d'upload en modification.
     *
     * @param nomComponsant
     *            nom du champ HTML input[type=file] à produire.
     * @param intituleFichierOrginal
     *            intitulé du fichier
     * @param data
     *            infoBean qui contiendra les données.
     */
    public static void initialiserInfoBean(final String nomComponsant, final String intituleFichierOrginal, final InfoBean data) {
        if (StringUtils.isNotEmpty(intituleFichierOrginal)) {
            final String nameInputFichierOriginal = getNameInputFichierOriginal(nomComponsant);
            data.set(nameInputFichierOriginal, intituleFichierOrginal);
        }
    }

    /**
     * Récupérer le {@link FichierSimpleUpload} depuis l'infoBean. Cet objet va permettre de gérer les différentes action à réaliser suite à l'upload de fichier.<br/>
     * Ce composant permet de savoir entre autre :
     * <ul>
     * <li>si un fihcier à été uploadé ou non</li>
     * <li>si le fichier uplodé est un nouveau fichier ou le fichier déjà présent dans l'élèment</li>
     * <li>si le fichier est nouveau, un {@link File} poitant vers le fichier temporaire</li>
     * <li>l'intitulé du fichier</li>
     * </ul>
     *
     * @param nomComposant
     *            non du champ HTML ayant servi à faire l'UPLOAD.
     * @param data
     *            Conteneur de données en provence du client. Cet objet contient normalement les données saisie par l'utilisateur.
     * @return Un {@link FichierSimpleUpload} permettant de modélier l'état de l'upload de fichier. Si aucun fichier n'est présent dans l'infoBean ou même que rien ne correspond au
     *         composant alors c'est une instance vide qui sera reotournée.
     */
    public static FichierSimpleUpload getFichierSimpleUploadDepuisInfoBean(final String nomComposant, final InfoBean data) {
        final FichierSimpleUpload fichierSimpleUpload = new FichierSimpleUpload();
        fichierSimpleUpload.code = nomComposant;
        final UploadedFile uploadedFile = (UploadedFile) data.get(getNameInputFichier(nomComposant));
        if (uploadedFile != null) {
            final String intituleFichierUploade = uploadedFile.toString();
            final String nomFichierUploadePhysique = uploadedFile.getTemporaryFile().getName();
            setDonneesDansFichierSimpleUploadPourFichierUploade(fichierSimpleUpload, intituleFichierUploade, nomFichierUploadePhysique);
        } else if (data.get(getNameInputFichierOriginal(nomComposant)) != null) {
            final String nameInputFichierOriginal = getNameInputFichierOriginal(nomComposant);
            fichierSimpleUpload.intituleFichier = data.getString(nameInputFichierOriginal);
            fichierSimpleUpload.isNouveauFichier = Boolean.FALSE;
            fichierSimpleUpload.isSansFichier = Boolean.FALSE;
        } else {
            fichierSimpleUpload.isNouveauFichier = Boolean.FALSE;
            fichierSimpleUpload.isSansFichier = Boolean.TRUE;
        }
        return fichierSimpleUpload;
    }

    private static void setDonneesDansFichierSimpleUploadPourFichierUploade(final FichierSimpleUpload fichierSimpleUpload, final String intituleFichierUploade, final String nomFichierUploadePhysique) {
        fichierSimpleUpload.isNouveauFichier = Boolean.TRUE;
        fichierSimpleUpload.isSansFichier = Boolean.FALSE;
        fichierSimpleUpload.intituleFichier = intituleFichierUploade;
        final String pathAbsoluFichier = WebAppUtil.getUploadDefaultPath() + File.separator + nomFichierUploadePhysique;
        fichierSimpleUpload.fichierUploade = new File(pathAbsoluFichier);
    }

    /**
     * Générer le composant HTML d'upload de fichier simple. Ce composant est initialisé via le {@link InfoBean} passé en paramétre.<br/>
     * <strong>Imporant :</strong> pour que ce composant HTML soit fonctionnel dans l'interface il est indispensable d'intégrer le JS associé.
     *
     * @see #initialiserInfoBean(String, String, InfoBean)
     * @param nomComposant
     *            Nom du composant et aussi des INPUT HTML.
     * @param libelle
     *            libellé permettant d'introduire le composant d'upload
     * @param fmt
     *            formateur HTML
     * @param data
     *            l'infoBean contenant les données d'initialisation du composant.
     * @param obligatoire
     *            Indiquer si ce champ est obligatoire ou non
     * @return Le code HTML perettant d'afficher le composant.
     */
    public static String genererChampsHTMLFichierSimpleUpload(final String nomComposant, final String libelle, final FormateurJSP fmt, final InfoBean data, final boolean obligatoire) {
        final StringBuilder out = new StringBuilder();
        out.append("<div class=\"fichier_simple\" id=\"");
        out.append(nomComposant);
        out.append("\">");
        ecrireLabel(out, nomComposant, libelle, obligatoire);
        final boolean existeFichier = existeFichierUploade(nomComposant, data);
        ecrireInputHiddenFormatKPortal(out, fmt, nomComposant, libelle, obligatoire);
        String nomFichierOriginal = StringUtils.EMPTY;
        String classFichierExiste = "fileupload-new";
        if (existeFichier) {
            final String nameInputFichierOriginal = getNameInputFichierOriginal(nomComposant);
            nomFichierOriginal = data.getString(nameInputFichierOriginal);
            classFichierExiste = "fileupload-exists";
        }
        out.append("<div class=\"fileupload ").append(classFichierExiste).append("\" data-name=\"").append(nomComposant).append(SUFFIXE_FILEUPLOAD).append("\">");
        out.append("<div class=\"input-append\">");
        out.append("<div class=\"uneditable-input span3\"><span class=\"fileupload-preview\">").append(nomFichierOriginal).append("</span></div>");
        out.append("<span class=\"btn btn-file\">");
        out.append("<span class=\"fileupload-new\">").append(MessageHelper.getCoreMessage("ST_AJOUT_FICHIER")).append("</span>");
        out.append("<span class=\"fileupload-exists\">").append(MessageHelper.getCoreMessage("JTF_BOUTON_MODIFIER")).append("</span>");
        out.append("<input type=\"file\" />");
        out.append("</span>");
        ecrireInputHidden(out, getNameInputFichierOriginal(nomComposant), nomFichierOriginal);
        out.append("<button type=\"button\" class=\"btn fileupload-exists\" data-hiddeninputname=\"").append(getNameInputFichierOriginal(nomComposant)).append("\" data-dismiss=\"fileupload\">").append(MessageHelper.getCoreMessage("JTF_BOUTON_SUPPRIMER")).append("</button>");
        out.append("</div></div>");
        out.append("</div>");
        return out.toString();
    }

    private static void ecrireLabel(final StringBuilder out, final String nomComposant, final String libelle, final boolean obligatoire) {
        if (obligatoire) {
            out.append("<label for=\"").append(StringUtils.lowerCase(nomComposant)).append("\" class=\"colonne\" >");
            out.append("<span class=\"obligatoire\">").append(libelle).append(" *</span>");
            out.append("</label>");
        } else {
            out.append("<label for=\"").append(StringUtils.lowerCase(nomComposant)).append("\"  class=\"colonne\" >").append(libelle).append("</label>");
        }
    }

    /**
     * Produire le code HTML du INPUT[type=hidden] fromat nécessaire au produit pour gérer l'upload de fichier et surtout charger le {@link FileUpload} dans l'infoBean.
     *
     * @param out
     *            flux où sera écrit le code HTML
     * @param fmt
     *            utilisé pour incrémenter l'indice de champ
     * @param nomComposant
     * @param libelle
     * @param obligatoire
     */
    private static void ecrireInputHiddenFormatKPortal(final StringBuilder out, final FormateurJSP fmt, final String nomComposant, final String libelle, final boolean obligatoire) {
        int indiceChamp = fmt.getIndiceChamp();
        indiceChamp++;
        out.append("<div class=\"masquer\">");
        out.append("<input type=\"hidden\" value=\"");
        if (obligatoire) {
            out.append(FormateurJSP.SAISIE_OBLIGATOIRE);
        } else {
            out.append(FormateurJSP.SAISIE_FACULTATIF);
        }
        // décryption du truc moisi
        // 9 = champ fichier
        // 0 et 0 = pas utilisé
        out.append(";9;0;0;LIB=");
        out.append(EscapeString.escapeAttributHtml(libelle));
        out.append(";");
        out.append(indiceChamp);
        out.append("\" name=\"#FORMAT_");
        out.append(nomComposant);
        out.append("\"/>");
        out.append("</div>");
        fmt.setIndiceChamp(indiceChamp);
    }

    private static String getNameInputFichier(final String nomComposant) {
        return nomComposant + SUFFIXE_FILEUPLOAD;
    }

    private static boolean existeFichierUploade(final String nomComposant, final InfoBean data) {
        return data.get(getNameInputFichier(nomComposant)) != null || data.get(getNameInputFichierOriginal(nomComposant)) != null;
    }

    private static String getNameInputFichierOriginal(final String nameInput) {
        return nameInput + SUFFIXE_NOM_FICHIER_ORIGINAL;
    }

    /**
     * Générer le code HTML d'un input hidden.
     *
     * @param out
     *            flux de sortie où sera écrit le code HTML.
     * @param nameInput
     *            nom et id de l'input
     * @param valueInput
     *            valeur de l'input
     */
    private static void ecrireInputHidden(final StringBuilder out, final String nameInput, final String valueInput) {
        out.append("<input id=\"");
        out.append(nameInput);
        out.append("\" type=\"hidden\" name=\"");
        out.append(nameInput);
        out.append("\" value=\"");
        out.append(EscapeString.escapeAttributHtml(valueInput));
        out.append("\"/>");
    }
}
