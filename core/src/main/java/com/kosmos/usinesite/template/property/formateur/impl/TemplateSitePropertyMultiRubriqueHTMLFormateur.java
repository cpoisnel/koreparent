package com.kosmos.usinesite.template.property.formateur.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyMultiRubrique;
import com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur;
import com.kosmos.usinesite.utils.FrontUASHelper;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.tree.processus.RubriquesJsTree;
import com.univ.utils.EscapeString;

public class TemplateSitePropertyMultiRubriqueHTMLFormateur implements TemplateSitePropertyHTMLFormateur<TemplateSitePropertyMultiRubrique> {

    @Override
    public String formater(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyMultiRubrique property, final FormateurJSP fmt, final InfoBean data) throws Exception {
        final String nomComponsant = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        List<String> valeur = infosSite.getProprieteComplementaireListString(property.getCode());
        if (valeur == null) {
            valeur = property.getValeurDefaut();
        }
        final StringBuilder kMultiSelect = new StringBuilder("<div>");
        kMultiSelect.append("<strong class=\"label colonne ").append(property.isObligatoire() ? "obligatoire" : "").append("\">").append(property.libelle).append(property.isObligatoire() ? " (*)" : "").append("</strong>");
        kMultiSelect.append("<div id=\"kmultiselect").append(nomComponsant).append("\" class=\"kmultiselect-ttl \" ").append("data-addAction=\"/adminsite/tree/tree.jsp?JSTREEBEAN=rubriquesJsTree&DISPLAY=full&SELECTED={0}&CODE={1}\" data-popintitle=\"LOCALE_BO.popin.title.rubrique.multi\" ").append("data-popinwidth=\"530\" data-popinvalidate=\"true\">");
        // Hidden input fields
        kMultiSelect.append("<input type=\"hidden\" id =\"").append(nomComponsant).append("\" name=\"").append(nomComponsant).append("\" value=\"").append(EscapeString.escapeAttributHtml(StringUtils.join(valeur, ";"))).append("\" ").append(property.isObligatoire() ? "required=\"required\"" : "").append("/>\n");
        kMultiSelect.append("<input type=\"hidden\" name=\"LIBELLE_").append(nomComponsant).append("\" value=\"").append(EscapeString.escapeAttributHtml(getLibelleSelection(valeur))).append("\"/>\n");
        kMultiSelect.append("<input type=\"hidden\" name=\"ARIANE_").append(nomComponsant).append("\" value=\"").append(EscapeString.escapeAttributHtml(getFilAriane(valeur))).append("\"/>\n");
        // Composition list
        kMultiSelect.append("<div class=\"kscrollable\">");
        kMultiSelect.append("<ul class=\"ui-sortable kmultiselect-list kmultiselect-composition-list\">");
        kMultiSelect.append("</ul>");
        kMultiSelect.append("</div>");
        // Close tags
        kMultiSelect.append("</div>");
        kMultiSelect.append("</div>");
        return kMultiSelect.toString();
    }

    private String getLibelleSelection(final List<String> valeur) {
        return StringUtils.join(CollectionUtils.collect(valeur, new Transformer() {

            @Override
            public Object transform(final Object input) {
                final String codeRubrique = (String) input;
                final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
                String intitule = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
                if (rubriqueBean != null) {
                    intitule = rubriqueBean.getIntitule();
                }
                return intitule;
            }
        }), ";");
    }

    private String getFilAriane(final List<String> valeur) {
        return StringUtils.join(CollectionUtils.collect(valeur, new Transformer() {

            @Override
            public Object transform(final Object input) {
                final String codeRubrique = (String) input;
                return RubriquesJsTree.getPath("", codeRubrique, " > ");
            }
        }), ";");
    }
}
