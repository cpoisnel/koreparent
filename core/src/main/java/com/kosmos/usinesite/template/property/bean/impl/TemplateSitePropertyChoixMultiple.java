package com.kosmos.usinesite.template.property.bean.impl;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

public class TemplateSitePropertyChoixMultiple extends AbstractTemplateSiteProperty {

    private static final int NOMBRE_ELEMENT_MIN_DEFAUT = 0;

    private static final int NOMBRE_ELEMENT_MAX_DEFAUT = 3000;

    private List<String> valeurDefaut = Collections.emptyList();

    private LinkedHashMap<String, String> valeurs = new LinkedHashMap<>();

    private int nombreElementMin = NOMBRE_ELEMENT_MIN_DEFAUT;

    private int nombreElementMax = NOMBRE_ELEMENT_MAX_DEFAUT;

    public List<String> getValeurDefaut() {
        return valeurDefaut;
    }

    public void setValeurDefaut(final List<String> valeurDefaut) {
        this.valeurDefaut = valeurDefaut;
    }

    public int getNombreElementMin() {
        return nombreElementMin;
    }

    public void setNombreElementMin(final int nombreElementMin) {
        this.nombreElementMin = nombreElementMin;
    }

    public int getNombreElementMax() {
        return nombreElementMax;
    }

    public void setNombreElementMax(final int nombreElementMax) {
        this.nombreElementMax = nombreElementMax;
    }

    public LinkedHashMap<String, String> getValeurs() {
        return this.valeurs;
    }

    public void setValeurs(final LinkedHashMap<String, String> valeurs) {
        this.valeurs = valeurs;
    }
}
