package com.kosmos.usinesite.template.property.validateur.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyListeString;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyListeStringValidateur extends AbstractTemplateSitePropertyStringValidateur implements TemplateSitePropertyValidateur<TemplateSitePropertyListeString> {

    protected static final String MSG_ERREUR_PROPRIETE_NON_AUTORISEE = "La propriété '%s' du template contient une valeur non autorisée : '%s'";

    protected static final String MSG_ERREUR_PROPRIETE_INVALIDE = "La propriété '%s' du template contient une valeur invalide : '%s'";

    protected static final String MSG_ERREUR_PROPRIETE_TAILLE = "La propriété '%s' du template dépasse les %d caractéres.";

    private static final String MSG_ERREUR_PROPRIETE_TAILLE_MIN = "La propriété '%s' du template n'a pas le nombre minimal de valeurs souhaité : %d";

    private static final String MSG_ERREUR_PROPRIETE_TAILLE_MAX = "La propriété '%s' du template n'a pas le nombre maximal de valeurs souhaité : %d";

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.template.validateur.TemplateSitePropertyValidateur
     * #valider(com.univ.multisites.InfosSite,
     * com.kosmos.usinesite.template.bean.site.TemplateSite,
     * com.kosmos.usinesite.template.bean.property.TemplateSiteProperty,
     * com.jsbsoft.jtf.core.InfoBean)
     */
    @Override
    public void valider(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyListeString property, final InfoBean data) throws Exception {
        final Collection<String> listeValeurs = TemplateSiteHelper.getListeStringTemplateSiteProperty(template, property, data);
        testerListeValeursString(listeValeurs, infosSite, template, property);
    }

    private void testerListeValeursString(final Collection<String> listeValeurs, final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyListeString property) throws Exception, ErreursSaisieInfosSite {
        final ArrayList<String> fluxErreurs = new ArrayList<>();
        // tests des propriétés de la liste
        testChampObligatoire(listeValeurs, property, fluxErreurs);
        testTailleListe(listeValeurs, property, fluxErreurs);
        for (final String valeur : listeValeurs) {
            testTailleMaximum(valeur, property.getTailleMaximum(), property.getLibelle(), fluxErreurs);
            testRegExp(valeur, property.getRegExpValidation(), property.getLibelle(), fluxErreurs);
            testListeValeurs(valeur, property.getListeValeursAutorisees(), property.getLibelle(), fluxErreurs);
        }
        traiterFluxErreurs(fluxErreurs, infosSite, template);
    }

    private void testChampObligatoire(final Collection<String> listeValeurs, final TemplateSitePropertyListeString property, final ArrayList<String> fluxErreurs) {
        if (property.isObligatoire() && CollectionUtils.isEmpty(listeValeurs)) {
            final String messageErreur = String.format(TemplateSitePropertyValidateur.MSG_ERREUR_PROPRIETE_OBLIGATOIRE, property.getLibelle());
            fluxErreurs.add(messageErreur);
        }
    }

    private void testTailleListe(final Collection<String> listeValeurs, final TemplateSitePropertyListeString property, final ArrayList<String> fluxErreurs) {
        if (CollectionUtils.isEmpty(listeValeurs)) {
            return;
        }
        if (CollectionUtils.size(listeValeurs) < property.getNombreElementMin()) {
            final String messageErreur = String.format(MSG_ERREUR_PROPRIETE_TAILLE_MIN, property.getLibelle(), property.getNombreElementMin());
            fluxErreurs.add(messageErreur);
        } else if (CollectionUtils.size(listeValeurs) > property.getNombreElementMax()) {
            final String messageErreur = String.format(MSG_ERREUR_PROPRIETE_TAILLE_MAX, property.getLibelle(), property.getNombreElementMax());
            fluxErreurs.add(messageErreur);
        }
    }
}
