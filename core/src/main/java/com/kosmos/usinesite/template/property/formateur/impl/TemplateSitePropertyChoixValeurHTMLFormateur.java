package com.kosmos.usinesite.template.property.formateur.impl;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyChoixValeur;
import com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur;
import com.kosmos.usinesite.utils.FrontUASHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyChoixValeurHTMLFormateur implements TemplateSitePropertyHTMLFormateur<TemplateSitePropertyChoixValeur> {

    @Override
    public String formater(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyChoixValeur property, final FormateurJSP fmt, final InfoBean data) throws Exception {
        final String nomInput = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        final StringBuilder out = new StringBuilder("<p>");
        out.append("<label class=\"colonne\" for=\"").append(nomInput).append("\">");
        if (property.isObligatoire()) {
            out.append("<span class=\"obligatoire\">").append(property.libelle).append(" * </span>");
        } else {
            out.append(property.libelle);
        }
        out.append(FrontUASHelper.genererMessageInformatif(property.getDescription()));
        out.append("</label>");
        final String valeur = StringUtils.defaultString(infosSite.getProprieteComplementaireString(property.getCode()));
        out.append(FrontUASHelper.genererSelect(nomInput, property.getListeValeurs(), valeur));
        out.append("</p>");
        return out.toString();
    }
}
