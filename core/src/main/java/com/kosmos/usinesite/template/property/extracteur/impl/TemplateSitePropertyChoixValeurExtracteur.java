package com.kosmos.usinesite.template.property.extracteur.impl;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyChoixValeur;
import com.kosmos.usinesite.template.property.extracteur.TemplateSitePropertyExtracteur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyChoixValeurExtracteur implements TemplateSitePropertyExtracteur<TemplateSitePropertyChoixValeur, String> {

    public String extraire(InfosSite infosSite, TemplateSite template, TemplateSitePropertyChoixValeur property, InfoBean data) throws Exception {
        return TemplateSiteHelper.getStringTemplateSiteProperty(template, property, data);
    }
}
