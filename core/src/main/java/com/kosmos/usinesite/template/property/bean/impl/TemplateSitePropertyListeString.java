package com.kosmos.usinesite.template.property.bean.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class TemplateSitePropertyListeString extends AbstractTemplateSiteProperty {

    /**
     * Chiffre au hasard mais parrait suffisamment grand pour ne pas poser de probléme.
     */
    private static final int NOMBRE_ELEMENT_MAX_DEFAUT = 3000;

    private static final int TAILLE_MAX = 255;

    private int nombreElementMin = 0;

    private int nombreElementMax = NOMBRE_ELEMENT_MAX_DEFAUT;

    private List<String> valeurDefaut = Collections.emptyList();

    private int tailleMaximum = TAILLE_MAX;

    private String regExpValidation = StringUtils.EMPTY;

    private Collection<String> listeValeursAutorisees = Collections.emptyList();

    public int getTailleMaximum() {
        return tailleMaximum;
    }

    public void setTailleMaximum(final int tailleMaximum) {
        this.tailleMaximum = tailleMaximum;
    }

    public String getRegExpValidation() {
        return regExpValidation;
    }

    public void setRegExpValidation(final String regExpValidation) {
        this.regExpValidation = regExpValidation;
    }

    public Collection<String> getListeValeursAutorisees() {
        return listeValeursAutorisees;
    }

    public void setListeValeursAutorisees(final Collection<String> listeValeursAutorisees) {
        this.listeValeursAutorisees = listeValeursAutorisees;
    }

    public List<String> getValeurDefaut() {
        return valeurDefaut;
    }

    public void setValeurDefaut(final List<String> valeurDefaut) {
        this.valeurDefaut = valeurDefaut;
    }

    public int getNombreElementMin() {
        return nombreElementMin;
    }

    public void setNombreElementMin(final int nombreElementMin) {
        this.nombreElementMin = nombreElementMin;
    }

    public int getNombreElementMax() {
        return nombreElementMax;
    }

    public void setNombreElementMax(final int nombreElementMax) {
        this.nombreElementMax = nombreElementMax;
    }
}
