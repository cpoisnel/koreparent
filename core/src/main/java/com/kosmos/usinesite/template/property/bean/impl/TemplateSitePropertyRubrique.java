package com.kosmos.usinesite.template.property.bean.impl;

import org.apache.commons.lang3.StringUtils;

public class TemplateSitePropertyRubrique extends AbstractTemplateSiteProperty {

    private String valeurDefaut = StringUtils.EMPTY;

    public void setValeurDefaut(final String valeurDefaut) {
        this.valeurDefaut = valeurDefaut;
    }

    public String getValeurDefaut() {
        return valeurDefaut;
    }
}
