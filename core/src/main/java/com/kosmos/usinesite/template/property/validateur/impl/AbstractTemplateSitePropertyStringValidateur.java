package com.kosmos.usinesite.template.property.validateur.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.AbstractTemplateSiteProperty;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;
import com.univ.multisites.InfosSite;

/**
 * Apporte des fonctions de bases pour le traitements des propriété de template utilisant des chaines de caractéres.
 *
 * @author pierre.cosson
 *
 */
public abstract class AbstractTemplateSitePropertyStringValidateur {

    protected static final String MSG_ERREUR_PROPRIETE_NON_AUTORISEE = "La propriété '%s' du template contient une valeur non autorisée : '%s'";

    protected static final String MSG_ERREUR_PROPRIETE_INVALIDE = "La propriété '%s' du template contient une valeur invalide : '%s'";

    protected static final String MSG_ERREUR_PROPRIETE_TAILLE = "La propriété '%s' du template dépasse les %d caractéres.";

    protected void testChampObligatoire(final String valeur, final AbstractTemplateSiteProperty property, final ArrayList<String> fluxErreurs) {
        if (property.isObligatoire() && StringUtils.isEmpty(valeur)) {
            final String messageErreur = String.format(TemplateSitePropertyValidateur.MSG_ERREUR_PROPRIETE_OBLIGATOIRE, property.getLibelle());
            fluxErreurs.add(messageErreur);
        }
    }

    protected void testTailleMaximum(final String valeur, final int tailleMaximum, final String libelle, final ArrayList<String> fluxErreurs) {
        if (StringUtils.isEmpty(valeur) || tailleMaximum == 0) {
            return;
        }
        if (StringUtils.length(valeur) > tailleMaximum) {
            final String messageErreur = String.format(MSG_ERREUR_PROPRIETE_TAILLE, libelle, tailleMaximum);
            fluxErreurs.add(messageErreur);
        }
    }

    protected void testRegExp(final String valeur, final String regExpValidation, final String libelle, final ArrayList<String> fluxErreurs) {
        if (StringUtils.isEmpty(valeur)) {
            return;
        }
        if (StringUtils.isEmpty(regExpValidation)) {
            return;
        }
        if (!valeur.matches(regExpValidation)) {
            final String messageErreur = String.format(MSG_ERREUR_PROPRIETE_INVALIDE, libelle, valeur);
            fluxErreurs.add(messageErreur);
        }
    }

    protected void testListeValeurs(final String valeur, final Collection<String> listeValeursAutorisees, final String libelle, final ArrayList<String> fluxErreurs) {
        if (StringUtils.isEmpty(valeur)) {
            return;
        }
        if (CollectionUtils.isEmpty(listeValeursAutorisees)) {
            return;
        }
        if (!listeValeursAutorisees.contains(valeur)) {
            final String messageErreur = String.format(MSG_ERREUR_PROPRIETE_NON_AUTORISEE, libelle, valeur);
            fluxErreurs.add(messageErreur);
        }
    }

    protected void traiterFluxErreurs(final ArrayList<String> fluxErreurs, final InfosSite infosSite, final TemplateSite template) throws ErreursSaisieInfosSite {
        if (!fluxErreurs.isEmpty()) {
            throw new ErreursSaisieInfosSite(fluxErreurs, infosSite, template);
        }
    }
}
