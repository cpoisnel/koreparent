package com.kosmos.usinesite.template.property.extracteur.impl;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyColor;
import com.kosmos.usinesite.template.property.extracteur.TemplateSitePropertyExtracteur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyColorExtracteur implements TemplateSitePropertyExtracteur<TemplateSitePropertyColor, String> {

    public String extraire(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyColor property, final InfoBean data) throws Exception {
        return TemplateSiteHelper.getStringTemplateSiteProperty(template, property, data);
    }
}
