package com.kosmos.usinesite.template.property.bean.impl;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;

public class AbstractTemplateSiteProperty implements TemplateSiteProperty {

    public String code = StringUtils.EMPTY;

    public String libelle = StringUtils.EMPTY;

    public String description = StringUtils.EMPTY;

    public boolean obligatoire = Boolean.FALSE;

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSiteProperty#getCode()
     */
    public String getCode() {
        return code;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSiteProperty#getLibelle()
     */
    public String getLibelle() {
        return libelle;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSiteProperty#getDescription()
     */
    public String getDescription() {
        return description;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSiteProperty#isObligatoire()
     */
    public boolean isObligatoire() {
        return obligatoire;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setObligatoire(boolean obligatoire) {
        this.obligatoire = obligatoire;
    }
}
