package com.kosmos.usinesite.template.property.formateur.impl;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyRubrique;
import com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur;
import com.kosmos.usinesite.utils.FrontUASHelper;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.tree.processus.RubriquesJsTree;
import com.univ.utils.EscapeString;

public class TemplateSitePropertyRubriqueHTMLFormateur implements TemplateSitePropertyHTMLFormateur<TemplateSitePropertyRubrique> {

    @Override
    public String formater(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyRubrique property, final FormateurJSP fmt, final InfoBean data) throws Exception {
        final String nomComponsant = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        String valeur = infosSite.getProprieteComplementaireString(property.getCode());
        if (valeur == null) {
            valeur = property.getValeurDefaut();
        }
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(valeur);
        String label = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
        if (rubriqueBean != null) {
            label = rubriqueBean.getIntitule();
        }
        final StringBuilder out = new StringBuilder("<div>");
        out.append("<span class=\"label colonne ");
        if (property.isObligatoire()) {
            out.append("obligatoire\">").append(property.libelle).append(" * ");
        } else {
            out.append("\">").append(property.libelle);
        }
        out.append(FrontUASHelper.genererMessageInformatif(property.getDescription()));
        out.append("</span>");
        out.append("<div id=\"kMonoSelect").append(nomComponsant).append("\" class=\"kmonoselect\" ").append("data-value=\"").append(EscapeString.escapeAttributHtml(label)).append("\" ");
        out.append("data-placeholder=\"f\"  data-editAction=\"/adminsite/tree/tree.jsp?JSTREEBEAN=rubriquesJsTree&DISPLAY=full&SELECTED={0}&CODE={1}\" data-popintitle=\"LOCALE_BO.popin.title.rubrique.mono\" ");
        out.append("data-popinwidth=\"530\" data-popinvalidate=\"false\" data-title=\"").append(RubriquesJsTree.getPath("", valeur, " > ")).append("\">");
        out.append("<input type=\"hidden\" id =\"").append(nomComponsant).append("\" name=\"").append(nomComponsant).append("\" value=\"").append(EscapeString.escapeAttributHtml(valeur)).append("\" ").append(property.isObligatoire() ? "required=\"required\"" : "").append("/>\n");
        out.append("<input type=\"hidden\" name=\"LIBELLE_").append(nomComponsant).append("\" value=\"").append(EscapeString.escapeAttributHtml(label)).append("\"/>\n");
        out.append("<input type=\"hidden\" name=\"ARIANE_").append(nomComponsant).append("\" value=\"").append(EscapeString.escapeAttributHtml(RubriquesJsTree.getPath("", valeur, " > "))).append("\"/>\n");
        out.append("</div>");
        out.append("</div>");
        return out.toString();
    }
}
