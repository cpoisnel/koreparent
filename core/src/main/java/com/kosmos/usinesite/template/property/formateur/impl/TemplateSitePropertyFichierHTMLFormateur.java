package com.kosmos.usinesite.template.property.formateur.impl;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyFichier;
import com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur;
import com.kosmos.usinesite.template.utils.ComposantFichierSimpleUpload;
import com.kosmos.usinesite.utils.FrontUASHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyFichierHTMLFormateur implements TemplateSitePropertyHTMLFormateur<TemplateSitePropertyFichier> {

    public String formater(InfosSite infosSite, TemplateSite template, TemplateSitePropertyFichier property, FormateurJSP fmt, InfoBean data) throws Exception {
        String nomComponsant = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        String intituleFichierDepuisInfosSite = infosSite.getProprieteComplementaireString(property.getCode());
        if (StringUtils.isNotEmpty(intituleFichierDepuisInfosSite)) {
            ComposantFichierSimpleUpload.initialiserInfoBean(nomComponsant, intituleFichierDepuisInfosSite, data);
        } else {
            ComposantFichierSimpleUpload.initialiserInfoBean(nomComponsant, StringUtils.EMPTY, data);
        }
        // FAKE spéficique Usine Site sur le fait que la donnée est obligatoire
        // (ne pas utiliser le FRAMEWORK pour gérer le fait que le fichier est
        // obligatoire)
        String libelleComposant = property.getLibelle();
        if (property.isObligatoire()) {
            libelleComposant = "<span class=\"obligatoire\">" + libelleComposant + " *</span>";
        }
        return ComposantFichierSimpleUpload.genererChampsHTMLFichierSimpleUpload(nomComponsant, libelleComposant, fmt, data, Boolean.FALSE) + FrontUASHelper.genererMessageInformatif(property.getDescription());
    }
}
