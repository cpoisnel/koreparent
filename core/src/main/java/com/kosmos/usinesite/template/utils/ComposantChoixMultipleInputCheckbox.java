package com.kosmos.usinesite.template.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.utils.SaisieInfosSiteInfoBeanHelper;

public class ComposantChoixMultipleInputCheckbox {

    public static final String TAB = "  ";

    public static final String NL = "\n";

    /**
     * Extraire du {@link InfoBean} la liste des cases cochées sans le composant de liste de textes.
     *
     * @param data
     *            la source de données.
     * @param nomComposant
     *            le nom du composant
     * @return la liste des clefs des cases à cocher. Liste vide si aucune case cochée
     */
    public static Collection<String> getListeClefs(final InfoBean data, final List<String> listeClefs) {
        final List<String> out = new ArrayList<>();
        for (final String clef : listeClefs) {
            if (SaisieInfosSiteInfoBeanHelper.isChecked(data, clef)) {
                out.add(clef);
            }
        }
        return out;
    }

    public static String genererListeCasesACocher(final String nomComposant, final Hashtable<String, String> listeValeurs, final List<String> listeDefaut) {
        final StringBuilder out = new StringBuilder();
        out.append("<ul class=\"liste__horizontale--2 liste__horizontale\" id=\"").append(nomComposant).append("\">");
        java.util.Set<String> var = listeValeurs.keySet();
        final String[] clefs = var.toArray(new String[var.size()]);
        Arrays.sort(clefs);
        for (final String clef : clefs) {
            final String valeur = listeValeurs.get(clef);
            final String id = HtmlUtils.htmlEscape(clef);
            final String checked = listeDefaut.contains(clef) ? " checked=\"checked\"" : StringUtils.EMPTY;
            out.append("<li class=\"liste__horizontale--item\" >");
            out.append("<input type=\"checkbox\" value=\"on\" id=\"").append(id).append("\" name=\"").append(id).append("\"").append(checked).append(" />");
            out.append("<label for=\"").append(id).append("\">").append(valeur).append("</label>");
            out.append("</li>");
        }
        return out.toString();
    }
}
