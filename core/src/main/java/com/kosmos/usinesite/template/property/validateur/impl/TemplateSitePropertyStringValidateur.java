package com.kosmos.usinesite.template.property.validateur.impl;

import java.util.ArrayList;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyString;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

/**
 * Permet de valider les données d'une {@link TemplateSitePropertyString}
 *
 * @author pierre.cosson
 *
 */
public class TemplateSitePropertyStringValidateur extends AbstractTemplateSitePropertyStringValidateur implements TemplateSitePropertyValidateur<TemplateSitePropertyString> {

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.template.validateur.TemplateSitePropertyValidateur
     * #valider(com.univ.multisites.InfosSite,
     * com.kosmos.usinesite.template.bean.site.TemplateSite,
     * com.kosmos.usinesite.template.bean.property.TemplateSiteProperty,
     * com.jsbsoft.jtf.core.InfoBean)
     */
    @Override
    public void valider(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyString property, final InfoBean data) throws Exception {
        final String valeur = TemplateSiteHelper.getStringTemplateSiteProperty(template, property, data);
        final ArrayList<String> fluxErreurs = new ArrayList<>();
        testChampObligatoire(valeur, property, fluxErreurs);
        testTailleMaximum(valeur, property.getTailleMaximum(), property.getLibelle(), fluxErreurs);
        testRegExp(valeur, property.getRegExpValidation(), property.getLibelle(), fluxErreurs);
        testListeValeurs(valeur, property.getListeValeursAutorisees(), property.getLibelle(), fluxErreurs);
        traiterFluxErreurs(fluxErreurs, infosSite, template);
    }
}
