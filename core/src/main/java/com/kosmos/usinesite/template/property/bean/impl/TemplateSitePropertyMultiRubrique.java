package com.kosmos.usinesite.template.property.bean.impl;

import java.util.Collections;
import java.util.List;

public class TemplateSitePropertyMultiRubrique extends AbstractTemplateSiteProperty {

    /**
     * Chiffre au hasard mais parrait suffisamment grand pour ne pas poser de probléme.
     */
    private static final int NOMBRE_ELEMENT_MAX_DEFAUT = 3000;

    private int nombreElementMin = 0;

    private int nombreElementMax = NOMBRE_ELEMENT_MAX_DEFAUT;

    private List<String> valeurDefaut = Collections.emptyList();

    public List<String> getValeurDefaut() {
        return valeurDefaut;
    }

    public void setValeurDefaut(final List<String> valeurDefaut) {
        this.valeurDefaut = valeurDefaut;
    }

    public int getNombreElementMin() {
        return nombreElementMin;
    }

    public void setNombreElementMin(final int nombreElementMin) {
        this.nombreElementMin = nombreElementMin;
    }

    public int getNombreElementMax() {
        return nombreElementMax;
    }

    public void setNombreElementMax(final int nombreElementMax) {
        this.nombreElementMax = nombreElementMax;
    }
}
