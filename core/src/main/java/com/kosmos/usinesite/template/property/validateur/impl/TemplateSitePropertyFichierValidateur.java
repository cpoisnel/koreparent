package com.kosmos.usinesite.template.property.validateur.impl;

import java.util.ArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyFichier;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;
import com.kosmos.usinesite.template.utils.FichierSimpleUpload;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

/**
 * Valider des données saisies dans le cadre d'une propiété de template de type fichier.
 *
 * @author pierre.cosson
 *
 */
public class TemplateSitePropertyFichierValidateur implements TemplateSitePropertyValidateur<TemplateSitePropertyFichier> {

    private static final String MSG_ERREUR_PROPRIETE_EXTENTION = "Le fichier de la propriété '%s' du template n'a pas une extention valide.";

    private static final String MSG_ERREUR_PROPRIETE_TAILLE = "Le fichier de la propriété '%s' du template dépasse les %dko autorisés.";

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.template.validateur.TemplateSitePropertyValidateur
     * #valider(com.univ.multisites.InfosSite,
     * com.kosmos.usinesite.template.bean.site.TemplateSite,
     * com.kosmos.usinesite.template.bean.property.TemplateSiteProperty,
     * com.jsbsoft.jtf.core.InfoBean)
     */
    @Override
    public void valider(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyFichier property, final InfoBean data) throws Exception {
        final FichierSimpleUpload fichierSimpleUpload = TemplateSiteHelper.getFichierSimpleUploadTemplateSiteProperty(template, property, data);
        testerFichier(fichierSimpleUpload, infosSite, template, property);
    }

    private void testerFichier(final FichierSimpleUpload fichierSimpleUpload, final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyFichier property) throws ErreursSaisieInfosSite {
        final ArrayList<String> fluxErreurs = new ArrayList<>();
        testChampObligatoire(fichierSimpleUpload, property, fluxErreurs);
        testExtention(fichierSimpleUpload, property, fluxErreurs);
        testTaille(fichierSimpleUpload, property, fluxErreurs);
        traiterFluxErreurs(fluxErreurs, infosSite, template);
    }

    private void testChampObligatoire(final FichierSimpleUpload fichierSimpleUpload, final TemplateSitePropertyFichier property, final ArrayList<String> fluxErreurs) {
        if (property.isObligatoire() && (fichierSimpleUpload == null || fichierSimpleUpload.isSansFichier || StringUtils.isEmpty(fichierSimpleUpload.intituleFichier))) {
            final String messageErreur = String.format(TemplateSitePropertyValidateur.MSG_ERREUR_PROPRIETE_OBLIGATOIRE, property.getLibelle());
            fluxErreurs.add(messageErreur);
        }
    }

    private void testExtention(final FichierSimpleUpload fichierSimpleUpload, final TemplateSitePropertyFichier property, final ArrayList<String> fluxErreurs) {
        if (CollectionUtils.isEmpty(property.getListeExtensionsAutorisees())) {
            return;
        }
        if (fichierSimpleUpload == null || fichierSimpleUpload.isSansFichier || StringUtils.isEmpty(fichierSimpleUpload.intituleFichier)) {
            return;
        }
        final String nomFichier = fichierSimpleUpload.intituleFichier;
        boolean isExtentionOk = Boolean.FALSE;
        for (final String extention : property.getListeExtensionsAutorisees()) {
            if (StringUtils.endsWith(nomFichier, "." + extention)) {
                isExtentionOk = Boolean.TRUE;
                break;
            }
        }
        if (!isExtentionOk) {
            final String messageErreur = String.format(MSG_ERREUR_PROPRIETE_EXTENTION, property.getLibelle());
            fluxErreurs.add(messageErreur);
        }
    }

    private void testTaille(final FichierSimpleUpload fichierSimpleUpload, final TemplateSitePropertyFichier property, final ArrayList<String> fluxErreurs) {
        if (fichierSimpleUpload == null || fichierSimpleUpload.isSansFichier || StringUtils.isEmpty(fichierSimpleUpload.intituleFichier)) {
            return;
        }
        if (!fichierSimpleUpload.isNouveauFichier || fichierSimpleUpload.fichierUploade == null || !fichierSimpleUpload.fichierUploade.exists()) {
            return;
        }
        if (fichierSimpleUpload.fichierUploade.length() > (property.getTailleMaximumFichier() * 1024)) {
            final String messageErreur = String.format(MSG_ERREUR_PROPRIETE_TAILLE, property.getLibelle(), property.getTailleMaximumFichier());
            fluxErreurs.add(messageErreur);
        }
    }

    protected void traiterFluxErreurs(final ArrayList<String> fluxErreurs, final InfosSite infosSite, final TemplateSite template) throws ErreursSaisieInfosSite {
        if (!fluxErreurs.isEmpty()) {
            throw new ErreursSaisieInfosSite(fluxErreurs, infosSite, template);
        }
    }
}
