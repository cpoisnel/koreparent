package com.kosmos.usinesite.utils;

import java.util.Collection;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.kportal.core.config.MessageHelper;
import com.univ.utils.EscapeString;

public class FrontUASHelper {

    public static <T> String genererSelect(final String inputName, final Collection<Entry<T, String>> listeValeursLibelles, final T valeurSelectionnee) {
        if (CollectionUtils.isEmpty(listeValeursLibelles)) {
            return StringUtils.EMPTY;
        }
        final StringBuilder out = new StringBuilder();
        out.append("<select id=\"").append(inputName).append("\" name=\"").append(inputName).append("\">");
        //supression de cette option pour avoir la première valeur par défaut
        //out.append("<option>Sélectionnez une valeur</option>");
        for (final Entry<T, String> valeurLibelle : listeValeursLibelles) {
            final T valeur = valeurLibelle.getKey();
            final String valeurRadioProperties = MessageHelper.getCoreMessage(valeurLibelle.getValue());
            final String libelle = StringUtils.defaultIfBlank(valeurRadioProperties, EscapeString.escapeHtml(valeurLibelle.getValue()));
            out.append("<option value=\"").append(valeur).append("\"");
            if (valeurSelectionnee != null && valeurSelectionnee.equals(valeur)) {
                out.append(" selected=\"selected\"");
            }
            out.append(">");
            out.append(libelle);
            out.append("</option>");
        }
        out.append("</select>");
        return out.toString();
    }

    /**
     * Génrer une liste d'inputs radio en HTML. Cette liste est composée d'une balise UL, de balises LI, de INPUT[type=radio] et de LABEL associés.
     *
     * @param inputName
     *            non du groupe d'input radio
     * @param listeValeursLibelles
     *            liste de valeurs possibles : à chacune de ces valeurs sera associée un input
     * @param valeurSelectionnee
     *            Il est possible de préciser une valeur à "checker"
     * @return Le code HTML affichant la liste d'input radio. Il est possible que la chaine de caractéres soit vide dans le cas ou la liste de valeurs est vide.
     */
    public static <T> String genererListeRadio(final String inputName, final Collection<Entry<T, String>> listeValeursLibelles, final T valeurSelectionnee) {
        if (CollectionUtils.isEmpty(listeValeursLibelles)) {
            return StringUtils.EMPTY;
        }
        final StringBuilder out = new StringBuilder("<ul class=\"en_ligne\" >");
        for (final Entry<T, String> valeurLibelle : listeValeursLibelles) {
            final T valeurRadio = valeurLibelle.getKey();
            final String idRadio = inputName + valeurRadio;
            final String valeurRadioProperties = MessageHelper.getCoreMessage(valeurLibelle.getValue());
            final String libelleRadio = StringUtils.defaultIfBlank(valeurRadioProperties, EscapeString.escapeHtml(valeurLibelle.getValue()));
            out.append("<li><input type=\"radio\" name=\"").append(inputName).append("\" value=\"").append(valeurRadio).append("\" id=\"").append(idRadio).append("\"");
            if (valeurSelectionnee != null && valeurSelectionnee.equals(valeurRadio)) {
                out.append(" checked=\"checked\"");
            }
            out.append("/>");
            out.append("<label for=\"").append(idRadio).append("\">").append(libelleRadio).append("</label></li>");
        }
        out.append("</ul>");
        return out.toString();
    }

    /**
     * Générer une liste de checkbox HTML. Cette liste est composée d'une balise UL, de balises LI, de INPUT[type=checbox] et de LABEL associés.
     *
     * @param inputNamePrefixe
     *            debut du name des inputs (chaque name sera construit avec cette donnée suivie de "_" et de la valeur associée).
     * @param listeValeursLibelles
     *            liste de valeurs et de libellés à afficher dans la liste
     * @param valeursSelectionnees
     *            liste de valeurs qui seront sélèctionnées.
     * @return le code HTML permettant d'afficher la liste de valeur. Cette chaine de caractéres sera vide dans le cas où la liste de valeur est vide.
     */
    public static <T> String genererListeCheckbox(final String inputNamePrefixe, final Collection<Entry<T, String>> listeValeursLibelles, final Collection<T> valeursSelectionnees) {
        if (CollectionUtils.isEmpty(listeValeursLibelles)) {
            return StringUtils.EMPTY;
        }
        final StringBuilder out = new StringBuilder("<ul class=\"en_ligne\" >");
        for (final Entry<T, String> valeurLibelle : listeValeursLibelles) {
            final T valeurCheckbox = valeurLibelle.getKey();
            final String nameCheckbox = inputNamePrefixe + "_" + valeurCheckbox;
            final String valeurRadioProperties = MessageHelper.getCoreMessage(valeurLibelle.getValue());
            final String libelleCheckbox = StringUtils.defaultIfBlank(valeurRadioProperties, EscapeString.escapeHtml(valeurLibelle.getValue()));
            out.append("<li><input type=\"checkbox\" name=\"").append(nameCheckbox).append("\" value=\"").append(valeurCheckbox).append("\" id=\"").append(nameCheckbox).append("\"");
            if (valeursSelectionnees != null && valeursSelectionnees.contains(valeurCheckbox)) {
                out.append(" checked=\"checked\"");
            }
            out.append("/>");
            out.append("<label for=\"").append(nameCheckbox).append("\">").append(libelleCheckbox).append("</label></li>");
        }
        out.append("</ul>");
        return out.toString();
    }

    /**
     *
     *
     * @param message
     * @return
     */
    public static String genererMessageInformatif(final String message) {
        if (StringUtils.isEmpty(message)) {
            return StringUtils.EMPTY;
        }
        return String.format("<button type=\"button\" class=\"infobulle\" data-original-title=\"%s\"></button>", EscapeString.escapeAttributHtml(message));
    }

    public static String genererInputHTML(final String type, final String name, final String id, final String value, final String classCss) {
        return genererInputHTML(type, name, id, value, -1, 0, classCss, Boolean.FALSE);
    }

    /**
     * Produire un champ HTML input "simple".
     *
     * @param type
     * @param name
     * @param id
     * @param value
     * @param maxLength
     * @param classCss
     * @param readOnly
     * @return le code HTML du champ HTML input.
     */
    public static String genererInputHTML(final String type, final String name, final String id, final String value, final int maxLength, final String classCss, final boolean readOnly) {
        return genererInputHTML(type, name, id, value, maxLength, 0, classCss, StringUtils.EMPTY, readOnly);
    }

    /**
     * Produire un champ HTML input "simple".
     *
     * @param type
     * @param name
     * @param id
     * @param value
     * @param maxLength
     * @param size
     * @param classCss
     * @param readOnly
     * @return le code HTML du champ HTML input.
     */
    public static String genererInputHTML(final String type, final String name, final String id, final String value, final int maxLength, final int size, final String classCss, final boolean readOnly) {
        return genererInputHTML(type, name, id, value, maxLength, size, classCss, StringUtils.EMPTY, readOnly);
    }

    public static String genererTexteareaHTML(final String name, final String id, final String value, final int maxLength, final String classCss, final String placeHolder, final boolean readOnly) {
        final StringBuilder out = new StringBuilder();
        out.append("<textarea ");
        if (StringUtils.isNotEmpty(name)) {
            out.append(" name=\"");
            out.append(EscapeString.escapeAttributHtml(name));
            out.append("\"");
        }
        if (StringUtils.isNotEmpty(id)) {
            out.append(" id=\"");
            out.append(EscapeString.escapeAttributHtml(id));
            out.append("\"");
        }
        if (maxLength > 0) {
            out.append(" maxlength=\"");
            out.append(maxLength);
            out.append("\"");
        }
        if (StringUtils.isNotEmpty(classCss)) {
            out.append(" class=\"");
            out.append(EscapeString.escapeAttributHtml(classCss));
            out.append("\"");
        }
        if (StringUtils.isNotEmpty(placeHolder)) {
            out.append(" placeHolder=\"");
            out.append(EscapeString.escapeAttributHtml(placeHolder));
            out.append("\"");
        }
        if (readOnly) {
            out.append(" readonly=\"readonly\"");
        }
        out.append(">");
        if (StringUtils.isNotEmpty(value)) {
            out.append(EscapeString.escapeHtml(value));
        }
        out.append("</textarea>");
        return out.toString();
    }

    /**
     * Génère un champ input simple.
     *
     * @param type
     * @param name
     * @param id
     * @param value
     * @param maxLength
     * @param classCss
     * @param placeHolder
     * @param readOnly
     * @return
     */
    public static String genererInputHTML(final String type, final String name, final String id, final String value, final int maxLength, final int size, final String classCss, final String placeHolder, final boolean readOnly) {
        final StringBuilder out = new StringBuilder();
        out.append("<input");
        if (StringUtils.isNotEmpty(type)) {
            out.append(" type=\"");
            out.append(EscapeString.escapeAttributHtml(type));
            out.append("\"");
        }
        if (StringUtils.isNotEmpty(name)) {
            out.append(" name=\"");
            out.append(EscapeString.escapeAttributHtml(name));
            out.append("\"");
        }
        if (StringUtils.isNotEmpty(id)) {
            out.append(" id=\"");
            out.append(EscapeString.escapeAttributHtml(id));
            out.append("\"");
        }
        if (StringUtils.isNotEmpty(value)) {
            out.append(" value=\"");
            out.append(EscapeString.escapeAttributHtml(value));
            out.append("\"");
        }
        if (maxLength > 0) {
            out.append(" maxlength=\"");
            out.append(maxLength);
            out.append("\"");
        }
        if (size > 0) {
            out.append(" size=\"");
            out.append(size);
            out.append("\"");
        }
        if (StringUtils.isNotEmpty(classCss)) {
            out.append(" class=\"");
            out.append(EscapeString.escapeAttributHtml(classCss));
            out.append("\"");
        }
        if (StringUtils.isNotEmpty(placeHolder)) {
            out.append(" placeHolder=\"");
            out.append(EscapeString.escapeAttributHtml(placeHolder));
            out.append("\"");
        }
        if (readOnly) {
            out.append(" readonly=\"readonly\"");
        }
        out.append("/>");
        return out.toString();
    }

    /**
     * Générer le name de l'input HTML contenant la {@link TemplateSiteProperty} spécifique au {@link TemplateSite}.
     *
     * @param template
     * @param propriete
     * @return le name de l'input HTML
     */
    public static String genererNameInputProprieteTemplate(final TemplateSite template, final TemplateSiteProperty propriete) {
        return genererNameInputProprieteTemplate(template.getCode(), propriete.getCode());
    }

    /**
     * Générer le name de l'input HTML contenant la {@link TemplateSiteProperty} spécifique au {@link TemplateSite}.
     *
     * @param codeTemplate
     * @param codePropriete
     * @return le name de l'input HTML
     */
    public static String genererNameInputProprieteTemplate(final String codeTemplate, final String codePropriete) {
        return String.format("template_%s_%s", codeTemplate, codePropriete);
    }
}
