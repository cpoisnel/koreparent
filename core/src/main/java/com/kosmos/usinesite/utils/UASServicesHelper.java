package com.kosmos.usinesite.utils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kportal.cms.objetspartages.annotation.GetterAnnotationHelper;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.bean.FicheBeanExport;
import com.kportal.extension.module.bean.PluginFicheBeanExport;
import com.kportal.extension.module.service.ServiceBeanDeletion;
import com.kportal.extension.module.service.ServiceBeanExport;
import com.kportal.extension.module.service.ServiceBeanImport;
import com.kportal.extension.module.service.ServiceContenuModule;
import com.kportal.extension.module.utils.ServiceContenuModuleHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.om.AbstractFiche;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.MediaUtils;

public class UASServicesHelper {

    /** Logger available to subclasses. */
    private static final Logger LOG = LoggerFactory.getLogger(UASServicesHelper.class);

    /**
     * export des ressources d'une fiche ou d'un modele de mail (fichiers joints au modèle).
     *
     * @param vRessources
     * @param pathExport
     * @return
     * @throws ReflectiveOperationException
     * @throws IOException
     */
    public static Map<String, MediaBean> getMediasByRessources(final Collection<RessourceBean> vRessources, final String pathExport) throws ReflectiveOperationException, IOException {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        final Map<String, MediaBean> medias = new HashMap<>();
        final Iterator<RessourceBean> it = vRessources.iterator();
        while (it.hasNext()) {
            final RessourceBean ressource = it.next();
            final MediaBean media = serviceMedia.getById(ressource.getIdMedia());
            if(media != null) {
                boolean save = processMedia(media, pathExport);
                if (save) {
                    final MediaBean mediaBean = new MediaBean();
                    UASOmHelper.copyProperties(mediaBean, media);
                    medias.put(media.getId().toString(), mediaBean);
                } else {
                    // on supprime la ressource si le média n'est pas exporté
                    it.remove();
                }
            }
        }
        return medias;
    }

    /**
     * @deprecated cette méthode passe le contexte pour rien, utilisez {@link UASServicesHelper#getMedias(Object, String, boolean)} avec le dernier boolean à true pour avoir le même comportement
     * @param ctx
     * @param object
     * @param pathExport
     * @return
     */
    @Deprecated
    public static Map<String, MediaBean> getMedias(final OMContext ctx, final Object object, final String pathExport) {
        return getMedias(object, pathExport, Boolean.TRUE);
    }

    /**
     * recupere les medias insérés dans une toolbox (pour les toolbox pas associées à une fiche : newsletter, rubrique, encadrés) et les medias uniques associés à un champ.
     * @deprecated cette méthode passe le contexte pour rien, utilisez {@link UASServicesHelper#getMedias(Object, String, boolean)}
     * @return
     */
    @Deprecated
    public static Map<String, MediaBean> getMedias(final OMContext ctx, final Object object, final String pathExport, final boolean inToolbox) {
        return getMedias(object, pathExport, inToolbox);
    }

    /**
     * recupere les medias insérés dans une toolbox (pour les toolbox pas associées à une fiche : newsletter, rubrique, encadrés) et les medias uniques associés à un champ.
     *
     * @return
     */
    public static Map<String, MediaBean> getMedias(final Object object, final String pathExport, final boolean inToolbox) {
        final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
        final Map<String, MediaBean> listeMedias = new HashMap<>();
        final List<Long> listeIdMedias = new ArrayList<>();
        if (inToolbox) {
            final List<Method> methodsToolBox = GetterAnnotationHelper.getMethodToolbox(object);
            if (object instanceof AbstractOm) {
                methodsToolBox.addAll(GetterAnnotationHelper.getMethodToolbox(((AbstractOm)object).getPersistenceBean()));
            }
            if (CollectionUtils.isNotEmpty(methodsToolBox)) {
                for (final Method method : methodsToolBox) {
                    try {
                        final Object contenuToolbox = method.invoke(object);
                        if (contenuToolbox instanceof String && !"".equals(contenuToolbox)) {
                            final String[] listeIdFichiers = StringUtils.substringsBetween((String) contenuToolbox, "[id-fichier]", "[/id-fichier]");
                            final String[] listeIdImages = StringUtils.substringsBetween((String) contenuToolbox, "[id-image]", "[/id-image]");
                            if (listeIdFichiers != null) {
                                for (final String idMedia : listeIdFichiers) {
                                    listeIdMedias.add(Long.valueOf(idMedia));
                                }
                            }
                            if (listeIdImages != null) {
                                for (final String idMedia : listeIdImages) {
                                    listeIdMedias.add(Long.valueOf(idMedia));
                                }
                            }
                        }
                    } catch (final ReflectiveOperationException | IllegalArgumentException e) {
                        LOG.debug("unable to invoke object", e);
                    }
                }
            }
        }
        final List<Method> methodIdMedia = GetterAnnotationHelper.getMethodIdMedia(object);
        if (object instanceof AbstractOm) {
            methodIdMedia.addAll(GetterAnnotationHelper.getMethodIdMedia(((AbstractOm)object).getPersistenceBean()));
        }
        if (CollectionUtils.isNotEmpty(methodIdMedia)) {
            for (final Method method : methodIdMedia) {
                try {
                    final Object idMedia = method.invoke(object);
                    final MediaBean mediaBean = serviceMedia.getById(Long.valueOf(idMedia.toString()));
                    if (mediaBean != null) {
                        listeIdMedias.add(mediaBean.getId());
                    }
                } catch (final ReflectiveOperationException | IllegalArgumentException e) {
                    LOG.debug("unable to invoke object", e);
                } catch (final DataSourceException dae) {
                    LOG.error("unable to query on media", dae);
                }
            }
        }
        for (final Long idMedia : listeIdMedias) {
            try {
                final MediaBean currentMedia = serviceMedia.getById(idMedia);
                if (currentMedia != null && processMedia(currentMedia, pathExport)) {
                    listeMedias.put(currentMedia.getId().toString(), currentMedia);
                }
            } catch (final IOException e) {
                LOG.error("Erreur lors de la recuperation d'un média de contenu de toolbox", e);
            }
        }
        return listeMedias;
    }

    /**
     *
     * @param media
     * @return
     * @throws IOException
     */
    private static boolean processMedia(final MediaBean media, final String pathExport) throws IOException {
        boolean save = false;
        if (MediaUtils.isLocal(media)) {
            final File source = new File(MediaUtils.getPathAbsolu(media));
            if (source.exists()) {
                final File destination = new File(pathExport, media.getUrl());
                FileUtils.copyFile(source, destination);
                save = true;
            }
        } else if (!media.getUrl().startsWith("/") && !media.getUrl().contains("\\")) {
            // garde fou si on a un fichier obsolète qui possède encore une url absolue /home/... ou c:\.
            save = true;
        }
        if (media.getUrlVignette().length() > 0) {
            final File source = new File(MediaUtils.getPathVignetteAbsolu(media));
            if (source.exists()) {
                final File destination = new File(pathExport, media.getUrlVignette());
                FileUtils.copyFile(source, destination);
            } else {
                media.setUrlVignette(StringUtils.EMPTY);
            }
        }
        return save;
    }

    public static <T extends Serializable> T getBean(final Object object, final Class<T> clazz) throws Exception {
        if (ClassUtils.isAssignable(object.getClass(), AbstractFiche.class)) {
            final AbstractFiche<?> fiche = (AbstractFiche<?>) object;
            return clazz.cast(fiche.getPersistenceBean());
        } else if (ClassUtils.isAssignable(object.getClass(), AbstractOm.class)) {
            final AbstractOm<?, ?> om = (AbstractOm<?, ?>) object;
            return clazz.cast(om.getPersistenceBean());
        } else {
            final T clone = clazz.newInstance();
            UASOmHelper.copyProperties(clone, object);
            return clone;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Serializable> Class<T> getBeanClass(final Object object) {
        if (ClassUtils.isAssignable(object.getClass(), AbstractFiche.class)) {
            final AbstractFiche<?> fiche = (AbstractFiche<?>) object;
            return (Class<T>) fiche.getPersistenceBean().getClass();
        } else if (ClassUtils.isAssignable(object.getClass(), AbstractOm.class)) {
            final AbstractOm<?, ?> om = (AbstractOm<?, ?>) object;
            return (Class<T>) om.getPersistenceBean().getClass();
        } else {
            return getOldParentBeanClass(object.getClass());
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Serializable> Class<T> getOldParentBeanClass(final Class<?> clazz) {
        final Iterator<Class<?>> hierarchy = ClassUtils.hierarchy(clazz, ClassUtils.Interfaces.EXCLUDE).iterator();
        Class<?> beanType = null;
        int counter = 0;
        while (hierarchy.hasNext() && counter < 3) {
            beanType = hierarchy.next();
            counter++;
        }
        return (Class<T>) beanType;
    }

    public static <T extends Serializable> FicheBeanExport<T> getFicheBeanExport(final FicheUniv ficheUniv, final Class<T> clazz) throws Exception {
        final FicheBeanExport<T> beanExport = new FicheBeanExport<>();
        beanExport.setBean(UASServicesHelper.getBean(ficheUniv, clazz));
        return beanExport;
    }

    public static <T extends Serializable> PluginFicheBeanExport<T> getPluginFicheBeanExport(final Object objet, final Class<T> clazz) throws Exception {
        final PluginFicheBeanExport<T> beanExport = new PluginFicheBeanExport<>();
        beanExport.setBean(UASServicesHelper.getBean(objet, clazz));
        return beanExport;
    }

    public static ServiceBeanExport<?> getServiceBeanExport(final IModule module) {
        ServiceBeanExport<?> serviceARetourner = null;
        final Collection<ServiceContenuModule> servicesModulesDisponibles = ServiceContenuModuleHelper.getServiceContenuModules();
        for (final ServiceContenuModule serviceModuleDispo : servicesModulesDisponibles) {
            if (serviceModuleDispo.getModules().contains(module)) {
                serviceARetourner = serviceModuleDispo.getServiceBeanExport();
            }
        }
        return serviceARetourner;
    }

    public static ServiceBeanImport<?> getServiceBeanImport(final IModule module) {
        ServiceBeanImport<?> serviceARetourner = null;
        final Collection<ServiceContenuModule> servicesModulesDisponibles = ServiceContenuModuleHelper.getServiceContenuModules();
        for (final ServiceContenuModule serviceModuleDispo : servicesModulesDisponibles) {
            if (serviceModuleDispo.getModules().contains(module)) {
                serviceARetourner = serviceModuleDispo.getServiceBeanImport();
            }
        }
        return serviceARetourner;
    }

    public static ServiceBeanDeletion getServiceBeanSuppression(final IModule module) {
        ServiceBeanDeletion serviceARetourner = null;
        final Collection<ServiceContenuModule> servicesModulesDisponibles = ServiceContenuModuleHelper.getServiceContenuModules();
        for (final ServiceContenuModule serviceModuleDispo : servicesModulesDisponibles) {
            if (serviceModuleDispo.getModules().contains(module)) {
                serviceARetourner = serviceModuleDispo.getServiceBeanDeletion();
            }
        }
        return serviceARetourner;
    }

    public static ServiceBeanReference getServiceBeanReference(final IModule module) {
        ServiceBeanReference serviceARetourner = null;
        final Collection<ServiceContenuModule> servicesModulesDisponibles = ServiceContenuModuleHelper.getServiceContenuModules();
        for (final ServiceContenuModule serviceModuleDispo : servicesModulesDisponibles) {
            if (serviceModuleDispo.getModules().contains(module)) {
                serviceARetourner = serviceModuleDispo.getServiceBeanReference();
            }
        }
        return serviceARetourner;
    }

    public static boolean hasAttachedService(final IModule module) {
        return getServiceBeanExport(module) != null;
    }
}
