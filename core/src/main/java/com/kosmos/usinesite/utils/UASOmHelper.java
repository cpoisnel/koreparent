package com.kosmos.usinesite.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.mediatheque.utils.MediathequeHelper;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.objetspartages.util.RessourceUtils;
import com.univ.utils.FileUtil;

public class UASOmHelper {

    /** Logger available to subclasses. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UASOmHelper.class);

    private static BeanUtilsBean beanUtils;

    static {
        final java.sql.Date defaultValue = null;
        final Converter converter = new DateConverter(defaultValue);
        beanUtils = BeanUtilsBean.getInstance();
        beanUtils.getConvertUtils().register(converter, java.sql.Date.class);
    }

    public static void addAllRessourceMediaBean(List<RessourceBean> ressources, Map<String, MediaBean> medias, String pathImport) {
        // import des ressources
        if (CollectionUtils.isNotEmpty(ressources) && MapUtils.isNotEmpty(medias)) {
            for (final RessourceBean currentRessourceBean : ressources) {
                addRessourceBean(currentRessourceBean);
            }
        }
        // import des médias
        if (MapUtils.isNotEmpty(medias)) {
            for (final MediaBean currentMediaBean : medias.values()) {
                addMediaBean(currentMediaBean, pathImport);
            }
        }
    }

    public static void addAllMediaBean(Map<String, MediaBean> medias, String pathImport) {
        if (MapUtils.isNotEmpty(medias)) {
            for (MediaBean currentMediaBean : medias.values()) {
                addMediaBean(currentMediaBean, pathImport);
            }
        }
    }

    public static void addMediaBean(MediaBean mediaBean, String pathImport) {
        if (mediaBean != null) {
            try {
                if (saveMedia(mediaBean, pathImport)) {
                    final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
                    serviceMedia.addWithForceId(mediaBean);
                }
            } catch (final IOException e) {
                LOGGER.error("Erreur lors de l'upload du media " + mediaBean.getId(), e);
            }
        }
    }

    private static boolean saveMedia(MediaBean media, String pathImport) throws IOException {
        boolean save = Boolean.FALSE;
        if (MediaUtils.isLocal(media)) {
            final File source = new File(pathImport, media.getUrl());
            if (source.exists()) {
                String destination;
                if (MediaUtils.isPublic(media)) {
                    destination = MediathequeHelper.getAbsolutePath();
                    if (StringUtils.isNotBlank(media.getTypeRessource())) {
                        destination += File.separator + media.getTypeRessource().toLowerCase();
                    }
                } else {
                    destination = RessourceUtils.getAbsolutePath();
                }
                File file = new File(destination, media.getUrl());
                // si le fichier existe il faut le renommer
                if (file.exists()) {
                    String fileName = MediaUtils.generateName(media, FileUtil.getExtension(media.getSource()));
                    media.setUrl(fileName);
                    file = new File(destination, media.getUrl());
                }
                FileUtils.copyFile(source, file);
                save = true;
            }
        } else {
            save = true;
        }
        if (media.getUrlVignette().length() > 0) {
            final File source = new File(pathImport, media.getUrlVignette());
            if (source.exists()) {
                String destination = MediathequeHelper.getAbsolutePath();
                if (media.getTypeRessource().length() > 0) {
                    destination += File.separator + media.getTypeRessource().toLowerCase();
                }
                File file = new File(destination, media.getUrlVignette());
                // si la vignette existe il faut la renommer
                if (file.exists()) {
                    media.setUrlVignette("v_" + media.getUrl());
                    file = new File(destination, media.getUrlVignette());
                }
                FileUtils.copyFile(source, file);
            } else {
                media.setUrlVignette("");
            }
        }
        return save;
    }

    public static void addRessourceBean(RessourceBean ressourceBean) {
        if (ressourceBean != null) {
            final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
            serviceRessource.addWithForcedId(ressourceBean);
        }
    }

    /**
     * Permet d'enregistrer un metatag en base.
     * @param ctx le contexte ne sert plus
     * @param metaTagBean le metatag à ajouter
     * @deprecated la méthode n'a plus trop d'interet, utiliser directement ServiceMetatag#save(MetatagBean)
     */
    @Deprecated
    public static void addMetatagBean(OMContext ctx, MetatagBean metaTagBean) {
        if (metaTagBean != null) {
            MetatagUtils.getMetatagService().save(metaTagBean);
        }
    }

    public static void copyProperties(Object object1, Object object2) throws ReflectiveOperationException {
        beanUtils.copyProperties(object1, object2);
    }
}
