package com.kosmos.usinesite.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;
import com.univ.utils.URLResolver;

/**
 * Classe utilitaire contenant toutes les listes de
 *
 * @author pierre.cosson
 *
 */
public abstract class InfosSiteHelper {

    public static final Collection<Entry<Integer, String>> SSL_MODE;

    public static final Collection<Entry<Integer, String>> BO_SSL_MODE;

    public static final Collection<Entry<Integer, String>> HTTP_ACTIONS;

    public static final Collection<Entry<Integer, String>> HTTPS_ACTIONS;

    public static final List<Entry<Integer, String>> MODE_REECRITURE_RUBRIQUE;

    private static final Pattern codeMetierPattern = Pattern.compile("[a-zA-Z0-9\\-_@\\.]+");

    private static final String URL_SEPARATEUR = "/";

    static {
        final ArrayList<Entry<Integer, String>> modeSsl = new ArrayList<>(3);
        modeSsl.add(new ImmutablePair<>(0, "BO_DESACTIVE"));
        modeSsl.add(new ImmutablePair<>(1, "BO_USINESITE_ACTIVE_CONTEXTUEL"));
        modeSsl.add(new ImmutablePair<>(2, "BO_USINESITE_ACTIVE_TOUT_SITE"));
        SSL_MODE = Collections.unmodifiableList(modeSsl);
    }

    static {
        final ArrayList<Entry<Integer, String>> boSslMode = new ArrayList<>(2);
        boSslMode.add(new ImmutablePair<>(0, "BO_DESACTIVE"));
        boSslMode.add(new ImmutablePair<>(1, "BO_ACTIVE"));
        BO_SSL_MODE = Collections.unmodifiableList(boSslMode);
    }

    static {
        final ArrayList<Entry<Integer, String>> actionsHTTP = new ArrayList<>(1);
        actionsHTTP.add(new ImmutablePair<>(URLResolver.DECONNECTER_FRONT, "ST_DSI_DECONNEXION"));
        HTTP_ACTIONS = Collections.unmodifiableList(actionsHTTP);
    }

    static {
        final ArrayList<Entry<Integer, String>> actionsHTTPS = new ArrayList<>(4);
        actionsHTTPS.add(new ImmutablePair<>(URLResolver.LOGIN_FRONT, "ST_DSI_PREFIDENTIFICATION"));
        actionsHTTPS.add(new ImmutablePair<>(URLResolver.PERSONNALISER_FRONT, "BO_USINESITE_PERSONNALISATION_INFOS"));
        actionsHTTPS.add(new ImmutablePair<>(URLResolver.DEMANDER_MDP_FRONT, "BO_USINESITE_DEMANDE_MDP"));
        actionsHTTPS.add(new ImmutablePair<>(URLResolver.PRESENTER_MDP_FRONT, "BO_USINESITE_PRESENTATION_MDP"));
        HTTPS_ACTIONS = Collections.unmodifiableList(actionsHTTPS);
    }

    static {
        final ArrayList<Entry<Integer, String>> reecritureRubrique = new ArrayList<>(4);
        reecritureRubrique.add(new ImmutablePair<>(0, "BO_USINESITE_UNIQUEMENT_NOM_PAGE"));
        reecritureRubrique.add(new ImmutablePair<>(1, "BO_USINESITE_NOM_PAGE_RUBRIQUE"));
        MODE_REECRITURE_RUBRIQUE = Collections.unmodifiableList(reecritureRubrique);
    }

    public static boolean isHttpHostNameParDefaut(final InfosSite infosSite) {
        return infosSite.getHttpHostname() == null;
    }

    public static boolean isHttpsHostNameParDefaut(final InfosSite infosSite) {
        return infosSite.getHttpsHostname() == null;
    }

    public static boolean isHttpPortParDefaut(final InfosSite infosSite) {
        return infosSite.getHttpPort() == -1;
    }

    public static boolean isHttpsPortParDefaut(final InfosSite infosSite) {
        return infosSite.getHttpsPort() == -1;
    }

    public static boolean isSslModeValide(final int sslMode) {
        return isCleContenuDansListeValeursAutorisee(sslMode, InfosSiteHelper.SSL_MODE);
    }

    public static boolean isBoSslModeValide(final int boSslMode) {
        return isCleContenuDansListeValeursAutorisee(boSslMode, InfosSiteHelper.BO_SSL_MODE);
    }

    public static boolean isHttpActionValide(final int valeuHttpAction) {
        return isCleContenuDansListeValeursAutorisee(valeuHttpAction, InfosSiteHelper.HTTP_ACTIONS);
    }

    public static boolean isHttpsActionValide(final int valeuHttpsAction) {
        return isCleContenuDansListeValeursAutorisee(valeuHttpsAction, InfosSiteHelper.HTTPS_ACTIONS);
    }

    public static boolean isModeReecritureValide(final int valeuModeReecriture) {
        return isCleContenuDansListeValeursAutorisee(valeuModeReecriture, InfosSiteHelper.MODE_REECRITURE_RUBRIQUE);
    }

    public static String getPathAbsoluFichiersTemplateDuSite(final InfosSite infosSite) {
        return getPathAbsoluFichiersTemplateDuSite(infosSite.getAlias());
    }

    public static String getPathAbsoluFichiersTemplateDuSite(final String codeSite) {
        return WebAppUtil.getAbsoluteFichiersSitesPath() + File.separator + codeSite + File.separator;
    }

    /**
     * Calcule le path du fichier sur le serveur concernant la propriété fourni en paramètre
     *
     * @param infosSite
     *            L'infossite sur lequel on souhaite chercher le fichier
     * @param nomPropertyTemplate
     *            le nom de la propriété contenant le fichier
     * @return Le path absolu du fichier à récupérer
     * @throws ErreurDonneeNonTrouve
     *             si la propriété n'est pas trouvé ou si elle ne peut pas être trouvé
     */
    public static String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
        if (infosSite == null) {
            throw new ErreurDonneeNonTrouve("Impossible de récupérer la propriété : " + nomPropertyTemplate + " sur un InfosSite null");
        }
        final String codeSite = infosSite.getAlias();
        final String intituleFichier = infosSite.getProprieteComplementaireString(nomPropertyTemplate);
        if (StringUtils.isEmpty(intituleFichier)) {
            throw new ErreurDonneeNonTrouve("Intitulé de fichier introuvable pour la property de template : " + nomPropertyTemplate + ", du site : " + codeSite);
        }
        return WebAppUtil.getAbsoluteFichiersSitesPath() + File.separator + codeSite + File.separator + nomPropertyTemplate + File.separator + intituleFichier;
    }

    /**
     * Calcule l'url relative du fichier concernant la propriété fourni en paramètre
     *
     * @param infosSite
     *            L'infossite sur lequel on souhaite chercher le fichier
     * @param nomPropertyTemplate
     *            le nom de la propriété contenant le fichier
     * @return L'url relative du fichier à récupérer
     * @throws ErreurDonneeNonTrouve
     *             si la propriété n'est pas trouvé ou si elle ne peut pas être trouvé
     */
    public static String getURLRelativeFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
        if (infosSite == null) {
            throw new ErreurDonneeNonTrouve("Impossible de récupérer la propriété : " + nomPropertyTemplate + " sur un InfosSite null");
        }
        final String codeSite = infosSite.getAlias();
        final String intituleFichier = infosSite.getProprieteComplementaireString(nomPropertyTemplate);
        if (StringUtils.isEmpty(intituleFichier)) {
            throw new ErreurDonneeNonTrouve("Intitulé de fichier introuvable pour la property de template : " + nomPropertyTemplate + ", du site : " + codeSite);
        }
        return WebAppUtil.getRelatifFichiersSitesPath() + codeSite + URL_SEPARATEUR + nomPropertyTemplate + URL_SEPARATEUR + intituleFichier;
    }

    /**
     * Vérifier que la valeur passée en paramétre est présente dans la {@link Collection} de {@link Pair} en tant que clé (élèment à gauche du Tuple)
     *
     * @param cle
     * @param listeValeurs
     * @return
     */
    private static boolean isCleContenuDansListeValeursAutorisee(final int cle, final Collection<Entry<Integer, String>> listeValeurs) {
        for (final Entry<Integer, String> valeurValide : listeValeurs) {
            if (valeurValide.getKey() == cle) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public static void controlerCode(final String code) throws ErreurApplicative {
        final Matcher matcher = codeMetierPattern.matcher(code);
        if (!matcher.matches()) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_USINESITE_ERREUR_CODE_INCORRECT"));
        }
    }
}
