package com.kosmos.usinesite.reference.impl;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.JDBCUtils;
import com.kosmos.usinesite.exception.ErreurReferenceException;
import com.kosmos.usinesite.reference.BeanReference;
import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kosmos.usinesite.reference.processor.ReferenceProcessor;
import com.kosmos.usinesite.utils.UASOmHelper;
import com.kportal.extension.module.bean.PluginFicheBeanExport;
import com.kportal.extension.module.plugin.objetspartages.om.ObjetPluginContenu;
import com.univ.objetspartages.bean.AbstractPersistenceBean;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.utils.ContexteDao;

public class PluginFicheServiceBeanReference implements ServiceBeanReference {

    private static final Logger LOG = LoggerFactory.getLogger(PluginFicheServiceBeanReference.class);

    /**
     * cette méthode regarde si la fiche existe et si c'est le cas, on créé un nouveau id fiche
     */
    @SuppressWarnings("deprecation")
    @Override
    public Collection<BeanReference> check(final Map<String, ? extends Serializable> beansOrig, final ReferenceProcessor processor) throws ErreurReferenceException {
        final Collection<BeanReference> referencesToUpdate = new ArrayList<>();
        final Map<ObjetPluginContenu, Map<String, MediaBean>> objetsContenus = new HashMap<>();
        PluginFicheBeanExport<?> pluginFicheBean = null;
        for (final String idBean : beansOrig.keySet()) {
            final PluginFicheBeanExport<?> currentFicheBean = (PluginFicheBeanExport<?>) beansOrig.get(idBean);
            // on ne traite que les objets (et pas les sous objets exportés dans la même map)
            ObjetPluginContenu currentObjet;
            try {
                currentObjet = updateObjectValues(currentFicheBean);
            } catch (final ReflectiveOperationException e) {
                throw new ErreurReferenceException("Erreur lors du calcul des références", e);
            }
            if (currentObjet != null) {
                objetsContenus.put(currentObjet, currentFicheBean.getMedias());
                pluginFicheBean = currentFicheBean;
            }
        }
        final BeanReference referencesById = new BeanReference();
        final Collection<String> moduleToSearch = new ArrayList<>();
        if (pluginFicheBean != null) {
            moduleToSearch.add(pluginFicheBean.getIdModule());
            final String nomTableSql = StringUtils.substringAfterLast(pluginFicheBean.getClasseObjet(), ".").toUpperCase();
            Long maxIdPlugin;
            try (ContexteDao ctxUtils = new ContexteDao()) {
                maxIdPlugin = JDBCUtils.updateTableAutoIncrement(ctxUtils.getConnection(), nomTableSql, Long.valueOf(objetsContenus.size()));
            } catch (final SQLException sqle) {
                throw new ErreurReferenceException("Une erreur est survenue lors de la réservation des clés primaires.", sqle);
            }
            for (final ObjetPluginContenu currentObjet : objetsContenus.keySet()) {
                try (ContexteDao ctx = new ContexteDao()) {
                    try {
                        currentObjet.setCtx(ctx);
                        currentObjet.retrieve();
                    } catch (final Exception e) {
                        LOG.debug("unable to retrive the ficheUniv", e);
                        continue;
                    }
                    referencesById.addNewValueByOldValue(currentObjet.getIdObjet().toString(), maxIdPlugin.toString());
                    // mise à jour de l'id sur les sous objets potentiels
                    referencesById.addModulesBySearchString("\"id\":\"" + S1 + "\"", moduleToSearch);
                    referencesById.addModulesBySearchString("\"id_" + nomTableSql.toLowerCase() + "\":\"" + S1 + "\"", moduleToSearch);
                    // traitement des médias associés
                    if (MapUtils.isNotEmpty(objetsContenus.get(currentObjet))) {
                        final MediaServiceBeanReference mediaServiceBeanRef = new MediaServiceBeanReference();
                        mediaServiceBeanRef.addSearchStringForBean(pluginFicheBean, moduleToSearch);
                        referencesToUpdate.addAll(mediaServiceBeanRef.check(objetsContenus.get(currentObjet), processor));
                    }
                    maxIdPlugin--;
                } catch (final Exception e) {
                    throw new ErreurReferenceException("erreur lors du calcul des références", e);
                }
            }
            referencesToUpdate.add(referencesById);
        }
        return referencesToUpdate;
    }

    /**
     * Instancie un objet et set ses valeurs via les données du beanAMapper
     *
     * @param beanAMapper
     *            Le bean contenant les valeurs venant de l'export
     * @return une FicheUniv du type contenu dans le bean à mapper
     * @throws ReflectiveOperationException
     *             Lors de la recopie des properties via BeanUtils
     */
    @SuppressWarnings("unchecked")
    private <T extends AbstractPersistenceBean> ObjetPluginContenu updateObjectValues(final PluginFicheBeanExport<?> beanAMapper) throws ReflectiveOperationException {
        final Object object = Class.forName(beanAMapper.getClasseObjet()).newInstance();
        if (object instanceof ObjetPluginContenu) {
            final ObjetPluginContenu objetPluginContenu = (ObjetPluginContenu) object;
            objetPluginContenu.init();
            if (object instanceof AbstractOm<?, ?>) {
                final AbstractOm<T, ?> fiche = (AbstractOm<T, ?>) object;
                final Class<T> clazz = (Class<T>) fiche.getPersistenceBean().getClass();
                fiche.setPersistenceBean(clazz.cast(beanAMapper.getBean()));
            } else {
                UASOmHelper.copyProperties(objetPluginContenu, beanAMapper.getBean());
            }
            return (ObjetPluginContenu) object;
        }
        return null;
    }
}
