package com.kosmos.usinesite.reference;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

public class BeanReference {

    private String typeReference;

    private Map<String, String> newValuesByOldValues;

    private Map<String, Collection<String>> modulesBySearchString;

    public Map<String, String> getNewValuesByOldValues() {
        return newValuesByOldValues;
    }

    public void setNewValuesByOldValues(final Map<String, String> newValuesByOldValues) {
        this.newValuesByOldValues = newValuesByOldValues;
    }

    public String getTypeReference() {
        return typeReference;
    }

    public void setTypeReference(final String typeReference) {
        this.typeReference = typeReference;
    }

    public BeanReference() {
    }

    public BeanReference(final String typeReference) {
        this.typeReference = typeReference;
    }

    public void addNewValueByOldValue(final String oldValue, final String newValue) {
        if (newValuesByOldValues == null) {
            newValuesByOldValues = new HashMap<>();
        }
        newValuesByOldValues.put(oldValue, newValue);
    }

    public Map<String, Collection<String>> getModulesBySearchString() {
        return modulesBySearchString;
    }

    public void setModulesBySearchString(final Map<String, Collection<String>> modulesBySearchString) {
        this.modulesBySearchString = modulesBySearchString;
    }

    @SuppressWarnings("unchecked")
    public void addModulesBySearchString(final String value, final Collection<String> modules) {
        if (modulesBySearchString == null) {
            modulesBySearchString = new HashMap<>();
        }
        if (modulesBySearchString.containsKey(value)) {
            modulesBySearchString.put(value, CollectionUtils.union(modulesBySearchString.get(value), modules));
        } else {
            modulesBySearchString.put(value, modules);
        }
    }

    @SuppressWarnings("unchecked")
    public void addAllModulesBySearchString(Map<String, Collection<String>> map) {
        if (modulesBySearchString == null) {
            modulesBySearchString = new HashMap<>();
        }
        for (Map.Entry<String, Collection<String>> currentEntry : map.entrySet()) {
            if (modulesBySearchString.containsKey(currentEntry.getKey())) {
                modulesBySearchString.put(currentEntry.getKey(), CollectionUtils.union(modulesBySearchString.get(currentEntry.getKey()), currentEntry.getValue()));
            } else {
                modulesBySearchString.put(currentEntry.getKey(), currentEntry.getValue());
            }
        }
    }
}
