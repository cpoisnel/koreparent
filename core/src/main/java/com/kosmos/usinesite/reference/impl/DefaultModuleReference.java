package com.kosmos.usinesite.reference.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.kosmos.usinesite.reference.ModuleReference;
import com.kportal.extension.module.IModule;

public class DefaultModuleReference implements ModuleReference {

    /**
     * la liste des modules sur lesquels on va calculer la référence
     */
    public Collection<IModule> modules;

    /**
     * la liste des modules sur lesquels on va appliquer une mise à jour du code
     */
    public Map<String, Collection<String>> modulesBySearchStringForCode;

    /**
     * la liste des modules sur lesquels on va appliquer une mise à jour de l'id
     */
    public Map<String, Collection<String>> modulesBySearchStringForId;

    @Override
    public void init() {}

    @Override
    public Collection<IModule> getModules() {
        return modules;
    }

    @Override
    public Map<String, Collection<String>> getModulesBySearchStringForCode() {
        return modulesBySearchStringForCode;
    }

    @Override
    public Map<String, Collection<String>> getModulesBySearchStringForId() {
        return modulesBySearchStringForId;
    }

    public void setModules(Collection<IModule> modules) {
        this.modules = modules;
    }

    public void setModulesBySearchStringForCode(Map<String, Collection<String>> modulesBySearchStringForCode) {
        this.modulesBySearchStringForCode = modulesBySearchStringForCode;
    }

    public void setModulesBySearchStringForId(Map<String, Collection<String>> modulesBySearchStringForId) {
        this.modulesBySearchStringForId = modulesBySearchStringForId;
    }

    @SuppressWarnings("unchecked")
    public void addModulesBySearchStringForCode(final String value, final Collection<String> modules) {
        if (modulesBySearchStringForCode == null) {
            modulesBySearchStringForCode = new HashMap<>();
        }
        if (modulesBySearchStringForCode.containsKey(value)) {
            modulesBySearchStringForCode.put(value, CollectionUtils.union(modulesBySearchStringForCode.get(value), modules));
        } else {
            modulesBySearchStringForCode.put(value, modules);
        }
    }

    @SuppressWarnings("unchecked")
    public void addModulesBySearchStringForId(final String value, final Collection<String> modules) {
        if (modulesBySearchStringForId == null) {
            modulesBySearchStringForId = new HashMap<>();
        }
        if (modulesBySearchStringForId.containsKey(value)) {
            modulesBySearchStringForId.put(value, CollectionUtils.union(modulesBySearchStringForId.get(value), modules));
        } else {
            modulesBySearchStringForId.put(value, modules);
        }
    }
}
