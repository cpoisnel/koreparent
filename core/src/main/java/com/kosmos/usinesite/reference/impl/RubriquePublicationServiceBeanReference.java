package com.kosmos.usinesite.reference.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.exception.ErreurReferenceException;
import com.kosmos.usinesite.reference.BeanReference;
import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kosmos.usinesite.reference.processor.ReferenceProcessor;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.services.ServiceRubriquePublication;

public class RubriquePublicationServiceBeanReference implements ServiceBeanReference {

    @Override
    public Collection<BeanReference> check(final Map<String, ? extends Serializable> beansOrig, final ReferenceProcessor processor) throws ErreurReferenceException {
        // cette méthode regarde si le meta existe et si c'est le cas, créé un nouvel id
        final Collection<BeanReference> beansReferences = new ArrayList<>();
        final BeanReference beanRef = new BeanReference();
        final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
        for (final String idRubPub : beansOrig.keySet()) {
            beansOrig.get(idRubPub);
            RubriquepublicationBean rubriquepublicationBean = serviceRubriquePublication.getById(Long.valueOf(idRubPub));
            if (rubriquepublicationBean != null) {
                beanRef.addNewValueByOldValue(idRubPub, "0");
            }
        }
        beansReferences.add(beanRef);
        return beansReferences;
    }
}
