package com.kosmos.usinesite.reference.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.exception.ErreurReferenceException;
import com.kosmos.usinesite.reference.BeanReference;
import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kosmos.usinesite.reference.processor.ReferenceProcessor;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.services.ServiceRessource;

public class RessourceServiceBeanReference implements ServiceBeanReference {

    private final ServiceRessource serviceRessource;

    public RessourceServiceBeanReference() {
        serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
    }

    @Override
    public Collection<BeanReference> check(final Map<String, ? extends Serializable> beansOrig, final ReferenceProcessor processor) throws ErreurReferenceException {
        // cette méthode regarde si le meta existe et si c'est le cas, créé un nouvel id
        final Collection<BeanReference> beansReferences = new ArrayList<>();
        final BeanReference beanRef = new BeanReference();
        for (final String idRessource : beansOrig.keySet()) {
            beansOrig.get(idRessource);
            final RessourceBean ressourceBean = serviceRessource.getById(Long.valueOf(idRessource));
            if(ressourceBean != null) {
                beanRef.addNewValueByOldValue(idRessource, "0");
                // on passe les ids à zero
            }
        }
        beansReferences.add(beanRef);
        return beansReferences;
    }
}
