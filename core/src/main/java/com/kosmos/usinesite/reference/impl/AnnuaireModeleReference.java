package com.kosmos.usinesite.reference.impl;

import java.util.ArrayList;

import com.univ.objetspartages.om.AnnuaireModele;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;

/**
 * Cette classe initialise les modules de type AnnuaireModele pour les calculs de reférence sur les attributs de type jointure annuaire
 *
 */
public class AnnuaireModeleReference extends DefaultModuleReference {

    @Override
    public void init() {
        modules = new ArrayList<>();
        // les modules cibles pour ajouter les références sont toutes les fiches annuaires
        for (String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
            if (ficheUniv instanceof AnnuaireModele) {
                modules.add(ReferentielObjets.getObjetByCode(codeObjet));
            }
        }
    }
}
