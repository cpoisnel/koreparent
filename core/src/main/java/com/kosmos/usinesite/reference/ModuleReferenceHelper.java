package com.kosmos.usinesite.reference;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.kportal.extension.module.IModule;
import com.kportal.extension.module.utils.ServiceContenuModuleHelper;

public class ModuleReferenceHelper {

    public static Collection<ModuleReference> getFichesReferences() {
        return ServiceContenuModuleHelper.getServiceContenuModuleManager().getModulesReferences();
    }

    public static boolean containsModule(ModuleReference moduleReference, String idModule) {
        for (IModule module : moduleReference.getModules()) {
            if (module.getId().equals(idModule)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public static void addAllModules(String currentIdModule, final BeanReference referencesById, final BeanReference referencesByCode) {
        for (ModuleReference moduleFicheReference : ServiceContenuModuleHelper.getServiceContenuModuleManager().getModulesReferences()) {
            if (moduleFicheReference.getModules() == null || containsModule(moduleFicheReference, currentIdModule)) {
                final Map<String, Collection<String>> modulesForId = moduleFicheReference.getModulesBySearchStringForId();
                final Map<String, Collection<String>> modulesForCode = moduleFicheReference.getModulesBySearchStringForCode();
                if (referencesById != null && MapUtils.isNotEmpty(modulesForId)) {
                    referencesById.addAllModulesBySearchString(modulesForId);
                }
                if (referencesByCode != null && MapUtils.isNotEmpty(modulesForCode)) {
                    referencesByCode.addAllModulesBySearchString(modulesForCode);
                }
            }
        }
    }
}
