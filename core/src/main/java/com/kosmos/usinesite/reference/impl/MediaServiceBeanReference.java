package com.kosmos.usinesite.reference.impl;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.exception.ErreurReferenceException;
import com.kosmos.usinesite.reference.BeanReference;
import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kosmos.usinesite.reference.processor.ReferenceProcessor;
import com.kportal.cms.objetspartages.annotation.GetterAnnotationHelper;
import com.kportal.extension.module.bean.AbstractBeanExport;
import com.univ.mediatheque.Mediatheque;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.utils.json.NamingStrategyHelper;

public class MediaServiceBeanReference implements ServiceBeanReference {

    final private BeanReference beanReference;

    final private ServiceMedia serviceMedia;

    public static String TYPE_REFERENCE_MEDIA = "MEDIA";

    public MediaServiceBeanReference() {
        beanReference = new BeanReference(TYPE_REFERENCE_MEDIA);
        serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
    }

    @Override
    public Collection<BeanReference> check(final Map<String, ? extends Serializable> beansOrig, final ReferenceProcessor processor) throws ErreurReferenceException {
        final Collection<BeanReference> referencesToUpdate = new ArrayList<>();
        beanReference.addModulesBySearchString("\"id_media\":\"" + S1 + "\"", Collections.<String>emptyList());
        beanReference.addModulesBySearchString("\\[id-fichier\\]" + S1 + "\\[\\/id-fichier\\]", Collections.<String>emptyList());
        beanReference.addModulesBySearchString("\\[id-image\\]" + S1 + "\\[\\/id-image\\]", Collections.<String>emptyList());
        beanReference.addModulesBySearchString("\\[legende-image\\]" + S1 + "\\[\\/legende-image\\]", Collections.<String>emptyList());
        beanReference.addModulesBySearchString("\\[title-image\\]" + S1 + "\\[\\/title-image\\]", Collections.<String>emptyList());
        for (final String idMedia : beansOrig.keySet()) {
            // on teste la présence en base du média grâce à son id
            final MediaBean mediaBean = serviceMedia.getById(Long.valueOf(idMedia));
            if(mediaBean != null) {
                // si le média est mutualise et que la source est la même on ne réimporte pas le fichier
                if (mediaBean.getIsMutualise().equals(Mediatheque.ETAT_MUTUALISE) && mediaBean.getUrl().equals(((MediaBean) beansOrig.get(idMedia)).getUrl())) {
                    continue;
                }
            }
            //On sette vide à la valeur du media. La nouvelle référence sera calculer dans le service parent.
            beanReference.addNewValueByOldValue(idMedia, StringUtils.EMPTY);
        }
        referencesToUpdate.add(beanReference);
        return referencesToUpdate;
    }

    public void addSearchStringForBean(final AbstractBeanExport<?> bean, final Collection<String> modules) {
        final List<Method> methods = GetterAnnotationHelper.getMethodIdMedia(bean.getBean());
        if (bean.getBean() instanceof AbstractOm) {
            methods.addAll(GetterAnnotationHelper.getMethodIdMedia(((AbstractOm)bean.getBean()).getPersistenceBean()));
        }
        for (final Method method : methods) {
            final PropertyDescriptor pd = BeanUtils.findPropertyForMethod(method);
            beanReference.addModulesBySearchString("\"" + NamingStrategyHelper.translateUppercaseToUnderscore(pd.getName()) + "\":\"" + S1 + "\"", modules);
        }
    }
}
