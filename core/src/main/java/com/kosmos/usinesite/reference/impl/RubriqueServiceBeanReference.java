package com.kosmos.usinesite.reference.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.kosmos.usinesite.exception.ErreurReferenceException;
import com.kosmos.usinesite.reference.BeanReference;
import com.kosmos.usinesite.reference.ModuleReferenceHelper;
import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kosmos.usinesite.reference.processor.ReferenceProcessor;
import com.kportal.extension.module.bean.AbstractBeanExport;
import com.kportal.extension.module.bean.RubriqueBeanExport;
import com.univ.objetspartages.bean.MediaBean;

public class RubriqueServiceBeanReference implements ServiceBeanReference {

    @Override
    public Collection<BeanReference> check(final Map<String, ? extends Serializable> beansOrig, final ReferenceProcessor processor) throws ErreurReferenceException {
        // cette méthode regarde si la rubrique existe et si c'est le cas, créé un nouveau code
        final Collection<BeanReference> beansReferences = new ArrayList<>();
        final Map<String, MediaBean> medias = new HashMap<>();
        final Collection<String> moduleToSearch = new ArrayList<>();
        String currentIdModule = "";
        if (MapUtils.isNotEmpty(beansOrig)) {
            currentIdModule = ((AbstractBeanExport<?>) beansOrig.values().iterator().next()).getIdModule();
            moduleToSearch.add(currentIdModule);
        }
        final BeanReference referencesByCode = new BeanReference();
        // mise à jour du code sur l'objet rubrique
        referencesByCode.addModulesBySearchString("\"code\":\"" + S1 + "\"", moduleToSearch);
        // mise à jour du code rubrique mere sur l'objet rubrique
        referencesByCode.addModulesBySearchString("\"code_rubrique_mere\":\"" + S1 + "\"", moduleToSearch);
        // mise à jour du code rubrique sur tous les objets
        referencesByCode.addModulesBySearchString("\"code_rubrique\":\"" + S1 + "\"", Collections.<String>emptyList());
        // mise à jour des codes rubriques pour les encadrés
        referencesByCode.addModulesBySearchString("RUB_" + S1 + ";", Collections.<String> emptyList());
        // mise à jour du code rubrique sur les metas
        referencesByCode.addModulesBySearchString("\"meta_code_rubrique\":\"" + S1 + "\"", Collections.<String>emptyList());
        // mise à jour du rubrique dest dans la table rubrique publication
        referencesByCode.addModulesBySearchString("\"rubrique_dest\":\"" + S1 + "\"", Collections.<String>emptyList());
        // mise à jour des requetes dans les toolbox ex [traitement;requete;objet=article#TRI_DATE=DATE_ASC#CODE_RUBRIQUE=1168593604668]
        referencesByCode.addModulesBySearchString("(codeRubrique|CODE_RUBRIQUE)=" + S1 + "(#|&|;|\\]|\\[)", Collections.<String>emptyList());
        // mise à jour du source requete dans la table rubrique publication (OBJET/STRUCTURE;O|1/RUBRIQUE;0|1//LANGUE) -> /RUBRIQUE;01//
        referencesByCode.addModulesBySearchString("\\/" + S1 + ";.\\/\\/", Collections.<String>emptyList());
        // mise à jour des toolbox lien vers la rubrique ex : [id-rubrique]ACCUEIL-FR[/id-rubrique]
        referencesByCode.addModulesBySearchString("\\[id-rubrique\\]" + S1 + "\\[\\/id-rubrique\\]", Collections.<String>emptyList());
        // ajout des références dynamiques pour les modules
        ModuleReferenceHelper.addAllModules(currentIdModule, null, referencesByCode);
        // mise à jour de l'id rubrique
        final BeanReference referencesById = new BeanReference();
        referencesById.addModulesBySearchString("\"id_rubrique\":\"" + S1 + "\"", moduleToSearch);
        for (final String codeRubrique : beansOrig.keySet()) {
            final RubriqueBeanExport rubriqueBean = (RubriqueBeanExport) beansOrig.get(codeRubrique);
            try {
                // Gestion des rubriques
                referencesByCode.addNewValueByOldValue(codeRubrique, String.valueOf(System.nanoTime()));
                referencesById.addNewValueByOldValue(rubriqueBean.getBean().getIdRubrique().toString(), "0");
                // Gestion des médias
                if (rubriqueBean.getMedias() != null) {
                    medias.putAll(rubriqueBean.getMedias());
                }
            } catch (final Exception e) {
                throw new ErreurReferenceException("Erreur lors de la récupération de la rubrique", e);
            }
        }
        beansReferences.add(referencesByCode);
        beansReferences.add(referencesById);
        // check des références de médias
        final MediaServiceBeanReference mediaTypeReference = new MediaServiceBeanReference();
        final Collection<BeanReference> mediaReferences = mediaTypeReference.check(medias, processor);
        beansReferences.addAll(mediaReferences);
        return beansReferences;
    }
}
