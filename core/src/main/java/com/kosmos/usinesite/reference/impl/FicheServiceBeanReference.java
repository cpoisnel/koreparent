package com.kosmos.usinesite.reference.impl;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.JDBCUtils;
import com.kosmos.usinesite.exception.ErreurReferenceException;
import com.kosmos.usinesite.reference.BeanReference;
import com.kosmos.usinesite.reference.ModuleReferenceHelper;
import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kosmos.usinesite.reference.processor.ReferenceProcessor;
import com.kosmos.usinesite.utils.UASOmHelper;
import com.kportal.extension.module.bean.FicheBeanExport;
import com.univ.objetspartages.bean.AbstractPersistenceBean;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.objetspartages.om.AnnuaireModele;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.utils.ContexteDao;

public class FicheServiceBeanReference implements ServiceBeanReference {

    /**
     * mise à jour de l'id du metatag sur le champ "id_meta" des plugins & ressources
     */
    protected static final String ID_META_ON_CONTENT = "\"id_meta\":\"" + S1 + "\"";

    /**
     * mise à jour de l'id du metatag sur le meta
     */
    protected static final String ID_METATAG_ON_META = "\"id_metatag\":\"" + S1 + "\"";

    /**
     * mise à jour du code dans rubrique publication
     */
    protected static final String CODE_ON_RUB_PUB = "\"code_fiche_orig\":\"" + S1 + "\"";

    /**
     * mise à jour du code parent des ressources ex 590,TYPE=LIEN_0016,CODE=88315544
     */
    protected static final String CODE_ON_CODEPARENT = ",CODE=" + S1 + "\"";

    /**
     * références sur le code de la fiche
     */
    protected static final String CODE_ON_FICHEUNIV = "\"code\":\"" + S1 + "\"";

    /**
     * mise à jour de l'id de la fiche usr le champ meta_id_fiche des les métas
     */
    protected static final String ID_ON_META_ID_FICHE = "\"meta_id_fiche\":\"" + S1 + "\"";

    /**
     * mise à jour du code de la fiche sur les métas
     */
    protected static final String CODE_ON_META_CODE = "\"meta_code\":\"" + S1 + "\"";

    /**
     * mise à jour du code rattachement sur le meta
     */
    protected static final String CODE_RATTACHEMENT_ON_META_CODE_RATTACHEMENT = "\"meta_code_rattachement\":\"" + S1 + "\"";

    /**
     * code parent de la ressource ID_FICHE,TYPE=%,% ex : 20,TYPE=FICHIER_0001,NO=1
     */
    protected static final String ID_ON_CODEPARENT = "\"" + S1 + ",TYPE=";

    /**
     * références sur le code rattachement de la fiche
     */
    protected static final String CODE_RATTACHEMENT_ON_CONTENT = "\"(code_rattachement|code_structure)\":\"" + S1 + "\"";

    /**
     * références sur les codes rattachement autres de la fiche
     */
    protected static final String CODE_RATTACHEMENT_AUTRES_ON_CONTENT = "\"code_rattachement_autres\":\"(.*;)?" + S1 + "(;.*)?\"";

    /**
     * références sur le code structure ex code rattachement de la fiche ex [traitement;requete;objet=article#TRI_DATE=DATE_ASC#CODE_RATTACHEMENT=LABO]
     */
    protected static final String CODE_RATTACHEMENT_ON_TOOLBOX = "(codeRattachement|CODE_RATTACHEMENT)=" + S1 + "(#|&|;|\\]|\\[)";

    /**
     * références sur le code annuaire dans les liens mailto
     */
    protected static final String ANNUAIRE_ON_TOOLBOX = "\\[mailto\\]email=annuaire;" + S1 + "\\[\\/mailto\\]";

    /**
     * cette méthode regarde si la fiche existe et si c'est le cas, on créé un nouveau id fiche
     */
    @Override
    public synchronized Collection<BeanReference> check(final Map<String, ? extends Serializable> beansOrig, final ReferenceProcessor processor) throws ErreurReferenceException {
        final Collection<BeanReference> referencesToUpdate = new ArrayList<>();
        FicheBeanExport<?> ficheBean = (FicheBeanExport<?>) beansOrig.values().iterator().next();
        final Collection<String> moduleToSearch = Arrays.asList(ficheBean.getIdModule());
        final BeanReference referencesById = new BeanReference();
        referencesById.addModulesBySearchString(ID_ON_CODEPARENT, moduleToSearch);
        referencesById.addModulesBySearchString(ID_ON_META_ID_FICHE, moduleToSearch);
        final BeanReference referencesByCode = new BeanReference();
        referencesByCode.addModulesBySearchString(CODE_ON_FICHEUNIV, moduleToSearch);
        referencesByCode.addModulesBySearchString(CODE_ON_CODEPARENT, moduleToSearch);
        referencesByCode.addModulesBySearchString(CODE_ON_RUB_PUB, moduleToSearch);
        referencesByCode.addModulesBySearchString(CODE_ON_META_CODE, moduleToSearch);
        final BeanReference beanRefMetatag = new BeanReference();
        beanRefMetatag.addModulesBySearchString(ID_METATAG_ON_META, Collections.<String>emptyList());
        beanRefMetatag.addModulesBySearchString(ID_META_ON_CONTENT, Collections.<String>emptyList());
        // ajout des références pour les structures
        if (ReferentielObjets.instancierFiche(ficheBean.getTypeObjet()) instanceof StructureModele) {
            referencesByCode.addModulesBySearchString(CODE_RATTACHEMENT_ON_META_CODE_RATTACHEMENT, Collections.<String>emptyList());
            referencesByCode.addModulesBySearchString(CODE_RATTACHEMENT_ON_CONTENT, Collections.<String>emptyList());
            referencesByCode.addModulesBySearchString(CODE_RATTACHEMENT_AUTRES_ON_CONTENT, Collections.<String>emptyList());
            referencesByCode.addModulesBySearchString(CODE_RATTACHEMENT_ON_TOOLBOX, Collections.<String>emptyList());
        }
        // ajout des références pour les fiches annuaires
        if (ReferentielObjets.instancierFiche(ficheBean.getTypeObjet()) instanceof AnnuaireModele) {
            referencesByCode.addModulesBySearchString(ANNUAIRE_ON_TOOLBOX, Collections.<String>emptyList());
        }
        // ajout des références dynamiques de type ModuleReference
        ModuleReferenceHelper.addAllModules(ficheBean.getIdModule(), referencesById, referencesByCode);
        final String nomTableSql = ReferentielObjets.getNomTableSql(ReferentielObjets.getCodeObjet(ficheBean.getTypeObjet()));
        Long maxIdFiche;
        Long maxIdMeta;
        try (ContexteDao ctxUtils = new ContexteDao()) {
            maxIdFiche = JDBCUtils.updateTableAutoIncrement(ctxUtils.getConnection(), nomTableSql, Long.valueOf(beansOrig.keySet().size()));
            maxIdMeta = JDBCUtils.updateTableAutoIncrement(ctxUtils.getConnection(), "METATAG", Long.valueOf(beansOrig.keySet().size()));
        } catch (final SQLException sqle) {
            throw new ErreurReferenceException("Une erreur est survenue lors de la réservation des clés primaires.", sqle);
        }
        if(maxIdFiche < 1000000L){
            maxIdFiche = 1000000L + beansOrig.keySet().size();
        }

        final Map<String, MediaBean> medias = new HashMap<>();
        final Map<String, RessourceBean> ressources = new HashMap<>();
        final Map<String, RubriquepublicationBean> rubPubs = new HashMap<>();
        for (final String objetCodeLangue : beansOrig.keySet()) {
            ficheBean = (FicheBeanExport<?>) beansOrig.get(objetCodeLangue);
            try (ContexteDao ctx = new ContexteDao()) {
                // gestion de la fiche
                final FicheUniv ficheUniv = updateFicheUnivValues(ficheBean);
                ficheUniv.setCtx(ctx);
                // Mise à jour des id et meta
                referencesById.addNewValueByOldValue(ficheUniv.getIdFiche().toString(), Long.toString(maxIdFiche));
                // ajout de la référence
                // mise à jour de l'id
                referencesById.addModulesBySearchString("\"id_" + nomTableSql.toLowerCase() + "\":\"" + S1 + "\"", moduleToSearch);
                // mise à jour du code de la fiche sur tous les contenus ex [id-fiche]pagelibre;04628859[/id-fiche]
                // Ajout de {1,2} afin de contourner le mauvais formatage
                referencesByCode.addModulesBySearchString("\\[id-fiche\\]" + nomTableSql.toLowerCase() + ";{1,2}" + S1 + "(;.)?\\[\\/id-fiche\\]", Collections.<String>emptyList());
                // mise à jour du code de la fiche sur toutes les reférences ex [actualite;1167917177523;0][actualite;1167917177523;0]
                referencesByCode.addModulesBySearchString("\\[" + nomTableSql.toLowerCase() + ";" + S1 + "(;.)?\\]", Collections.<String>emptyList());
                referencesByCode.addNewValueByOldValue(ficheUniv.getCode(), String.valueOf(System.nanoTime()));
                beanRefMetatag.addNewValueByOldValue(String.valueOf(ficheBean.getMetatag().getId()), String.valueOf(maxIdMeta));
                if (processor != null) {
                    processor.process(ficheUniv.getCode(), ficheBean.getMetatag().getMetaCode(), ficheBean.getTypeObjet(), maxIdMeta, ficheBean.getMetatag().getId());
                }
                // gestion des medias
                if (MapUtils.isNotEmpty(ficheBean.getMedias())) {
                    medias.putAll(ficheBean.getMedias());
                }
                // gestion des ressources
                if (CollectionUtils.isNotEmpty(ficheBean.getRessources())) {
                    final Map<String, RessourceBean> map = new HashMap<>();
                    for (final RessourceBean res : ficheBean.getRessources()) {
                        map.put(res.getId().toString(), res);
                    }
                    ressources.putAll(map);
                }
                // gestion des rubriques de publication
                if (CollectionUtils.isNotEmpty(ficheBean.getRubriquespublication())) {
                    final Map<String, RubriquepublicationBean> map = new HashMap<>();
                    for (final RubriquepublicationBean res : ficheBean.getRubriquespublication()) {
                        map.put(res.getIdRubriquepublication().toString(), res);
                    }
                    rubPubs.putAll(map);
                }
                maxIdFiche--;
                maxIdMeta--;
            } catch (final Exception e) {
                throw new ErreurReferenceException("erreur lors du calcul des références", e);
            }
        }
        referencesToUpdate.add(referencesById);
        // ajout des références sur les codes
        referencesToUpdate.add(referencesByCode);
        // check des ressources seulement sur le module courant
        final Collection<BeanReference> referencesByResources = checkExternalReferences(new RessourceServiceBeanReference(), ressources, "\"id_ressource\":\"" + S1 + "\"",
            moduleToSearch, processor);
        referencesToUpdate.addAll(referencesByResources);
        // check des références de médias sur tous les modules
        final MediaServiceBeanReference mediaServiceBeanRef = new MediaServiceBeanReference();
        mediaServiceBeanRef.addSearchStringForBean(ficheBean, moduleToSearch);
        final Collection<BeanReference> referencesByMedia = checkExternalReferences(mediaServiceBeanRef, medias, StringUtils.EMPTY, Collections.<String>emptyList(), processor);
        referencesToUpdate.addAll(referencesByMedia);
        // check des references des métas sur tous les modules
        referencesToUpdate.add(beanRefMetatag);
        // check des rubriques de publication seulement sur le module courant
        final Collection<BeanReference> referencesByRubPub = checkExternalReferences(new RubriquePublicationServiceBeanReference(), rubPubs,
            "\"id_rubriquepublication\":\"" + S1 + "\"", moduleToSearch, processor);
        referencesToUpdate.addAll(referencesByRubPub);
        return referencesToUpdate;
    }

    /**
     * Instancie une FicheUniv et set ses valeurs via les données du beanAMapper
     *
     * @param beanAMapper
     *            Le bean contenant les valeurs venant de l'export
     * @return une FicheUniv du type contenu dans le bean à mapper
     * @throws ReflectiveOperationException
     *             Lors de la recopie des properties via BeanUtils
     */
    @SuppressWarnings("unchecked")
    private <T extends AbstractPersistenceBean> FicheUniv updateFicheUnivValues(final FicheBeanExport<?> beanAMapper) throws ReflectiveOperationException {
        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(beanAMapper.getTypeObjet());
        ficheUniv.init();
        if (ficheUniv instanceof AbstractOm<?, ?>) {
            final AbstractOm<T, ?> fiche = (AbstractOm<T, ?>) ficheUniv;
            final Class<T> clazz = (Class<T>) fiche.getPersistenceBean().getClass();
            fiche.setPersistenceBean(clazz.cast(beanAMapper.getBean()));
        } else {
            UASOmHelper.copyProperties(ficheUniv, beanAMapper.getBean());
        }
        return ficheUniv;
    }

    /**
     * Vérifie si il n'existe pas des références externes aux fiches
     *
     * @param serviceToCheck
     *            le service à appeler pour récupérer ces références
     * @param beanstoCheck
     *            les beans à exporter
     * @param searchString
     *            la chaine de la référence
     * @param moduleToSearch
     *            les modules concernés par cette référence
     * @param processor
     *            possible traitement à executer
     * @return la listes des références externe à ajouter à celles des fiches
     * @throws ErreurReferenceException
     *             lors de l'appel du service de référence externe
     */
    private Collection<BeanReference> checkExternalReferences(final ServiceBeanReference serviceToCheck, final Map<String, ? extends Serializable> beanstoCheck, final String searchString, final Collection<String> moduleToSearch, final ReferenceProcessor processor) throws ErreurReferenceException {
        final Collection<BeanReference> references = serviceToCheck.check(beanstoCheck, processor);
        if (StringUtils.isNotBlank(searchString)) {
            for (final BeanReference beanRef : references) {
                beanRef.addModulesBySearchString(searchString, moduleToSearch);
            }
        }
        return references;
    }
}
