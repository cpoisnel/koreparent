package com.kosmos.usinesite.reference.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.service.impl.RubriqueServiceContenuModule;

public class RubriqueFicheReference extends DefaultModuleReference {

    @Override
    public void init() {
        String s1 = ServiceBeanReference.S1;
        RubriqueServiceContenuModule service = (RubriqueServiceContenuModule) ApplicationContextManager.getCoreContextBean(RubriqueServiceContenuModule.ID_BEAN);
        if (service != null) {
            if (CollectionUtils.isNotEmpty(service.getModules())) {
                List<String> idModules = new ArrayList<>();
                for (IModule module : service.getModules()) {
                    idModules.add(module.getId());
                }
                // mise à jour du code de page d'accueil ex "page_accueil":"{\"code\":\"64932802\",\"langue\":\"0\",\"objet\":\"pagelibre\"}",
                addModulesBySearchStringForCode("\"page_accueil\":\"\\{\\\\\"code\\\\\":\\\\\"" + s1 + "\\\\\",\\\\\"langue\\\\\"", idModules);
            }
        }
    }
}
