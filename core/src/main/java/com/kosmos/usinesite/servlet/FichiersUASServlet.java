package com.kosmos.usinesite.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.webapp.WebAppUtil;

/**
 * Va récupérer les fichiers de conf de l'usine à site. Qu'ils soient dans le storage ou non.
 *
 * @author olivier.camon
 *
 */
public class FichiersUASServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -8038208278480906860L;

    private static final Logger LOG = LoggerFactory.getLogger(FichiersUASServlet.class);

    private static final int DEFAULT_BUFFER_SIZE = 10240;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String requestedFile = request.getPathInfo();
        try {
            if (requestedFile == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            final File file = new File(WebAppUtil.getAbsoluteFichiersSitesPath(), URLDecoder.decode(requestedFile, "UTF-8"));
            if (!file.exists()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            String contentType = getServletContext().getMimeType(file.getName());
            if (contentType == null) {
                contentType = "application/octet-stream";
            }
            response.reset();
            response.setBufferSize(DEFAULT_BUFFER_SIZE);
            response.setContentType(contentType);
            response.setHeader("Content-Length", String.valueOf(file.length()));
            response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
            try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);
                 BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);) {
                final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                int length;
                while ((length = input.read(buffer)) > 0) {
                    output.write(buffer, 0, length);
                }
            }
        } catch (final IOException io) {
            LOG.error("unable to send the UAS file", io);
        }
    }
}
