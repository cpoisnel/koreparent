package com.kosmos.usinesite.migration.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.usinesite.migration.bean.InfosSiteSauvegarde;
import com.kosmos.usinesite.migration.bean.ProprietesSite;
import com.kosmos.usinesite.migration.utils.InfosSiteCodec;
import com.univ.multisites.InfosSite;
import com.univ.multisites.dao.impl.InfosSiteDaoProperties;
import com.univ.utils.ContexteDao;
import com.univ.utils.json.CodecJSon;

/**
 * Permet de gérer les {@link InfosSiteSauvegarde} contenus dans la base de données. <br/>
 * <br/>
 * Toutes les méthodes peuvent retourner des exception de type : {@link SQLException}. Cela signifie des problèmes d'accés à la base de données ou de récupération des données.
 *
 * @author pierre.cosson
 *
 */
public class InfosSiteSauvegardeDaoImpl extends InfosSiteDaoProperties {

    private static final Logger LOG = LoggerFactory.getLogger(InfosSiteSauvegardeDaoImpl.class);

    private static final String SELECT_TOUT = "SELECT * FROM USINESITE_INFOSSITE;";

    private static final String SELECT_PAR_CODE = "SELECT * FROM USINESITE_INFOSSITE WHERE CODE = ? ORDER BY INTITULE;";

    private static final String SELECT_PAR_ID = "SELECT * FROM USINESITE_INFOSSITE WHERE ID_USINESITE_INFOSSITE = ? ORDER BY INTITULE;";

    private static final String DELETE_PAR_CODE = "DELETE FROM USINESITE_INFOSSITE WHERE CODE = ?;";

    protected ContexteDao instancierContexteDao() {
        return new ContexteDao();
    }

    /**
     * Fermer les accés ouvert pour accéder à la base de données.
     *
     * @param ctx
     * @param stmt
     */
    protected void fermerAccesBD(final ContexteDao ctx, final PreparedStatement stmt) {
        fermerAccesBD(ctx, stmt, null);
    }

    /**
     * Fermer les accés ouvert pour accéder à la base de données.
     *
     * @param ctx
     * @param stmt
     * @param res
     */
    protected void fermerAccesBD(final ContexteDao ctx, final PreparedStatement stmt, final ResultSet res) {
        try {
            if (res != null) {
                res.close();
            }
        } catch (final SQLException e) {
            LOG.debug("Erreur lors de la fermeture du resultSet", e);
        }
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (final SQLException e) {
            LOG.debug("Erreur lors de la fermeture du statement", e);
        }
        if (ctx != null) {
            ctx.close();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.multisites.dao.InfosSiteDao#getInfosSite(java.lang.String)
     */
    @Override
    public InfosSite getInfosSite(final String code) throws Exception {
        final InfosSiteSauvegarde infosSiteBDD = getInfosSiteSauvegarde(code);
        return InfosSiteCodec.decode(infosSiteBDD);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.dao.InfosSiteSauvegardeDao#getInfosSiteSauvegarde
     * (java.lang.String)
     */
    public InfosSiteSauvegarde getInfosSiteSauvegarde(final String code) throws Exception {
        final ContexteDao ctx = instancierContexteDao();
        PreparedStatement stmt = null;
        ResultSet res = null;
        try {
            stmt = ctx.getConnection().prepareStatement(SELECT_PAR_CODE);
            stmt.setString(1, code);
            res = stmt.executeQuery();
            if (res.next()) {
                return genererInfosSiteDepuisResultSet(res);
            } else {
                throw new ErreurDonneeNonTrouve("Aucune configuration infosSite ne correspond au code " + code);
            }
        } finally {
            fermerAccesBD(ctx, stmt, res);
        }
    }

    public InfosSiteSauvegarde getInfosSiteSauvegarde(final long idSite) throws Exception {
        final ContexteDao ctx = instancierContexteDao();
        PreparedStatement stmt = null;
        ResultSet res = null;
        try {
            stmt = ctx.getConnection().prepareStatement(SELECT_PAR_ID);
            stmt.setLong(1, idSite);
            res = stmt.executeQuery();
            if (res.next()) {
                return genererInfosSiteDepuisResultSet(res);
            } else {
                throw new ErreurDonneeNonTrouve("Aucune configuration infosSite ne correspond à l'identifiant " + idSite);
            }
        } finally {
            fermerAccesBD(ctx, stmt, res);
        }
    }

    private InfosSiteSauvegarde genererInfosSiteDepuisResultSet(final ResultSet res) throws Exception {
        final InfosSiteSauvegarde sauvegarde = new InfosSiteSauvegarde();
        sauvegarde.idInfosSite = res.getLong("ID_USINESITE_INFOSSITE");
        sauvegarde.code = res.getString("CODE");
        sauvegarde.intitule = res.getString("INTITULE");
        sauvegarde.httpHostname = res.getString("HTTP_HOSTNAME");
        sauvegarde.httpPort = res.getInt("HTTP_PORT");
        sauvegarde.httpsHostname = res.getString("HTTPS_HOSTNAME");
        sauvegarde.httpsPort = res.getInt("HTTPS_PORT");
        sauvegarde.codeRubrique = res.getString("CODE_RUBRIQUE");
        sauvegarde.codeTemplate = res.getString("CODE_TEMPLATE");
        sauvegarde.isSitePrincipal = res.getBoolean("SITE_PRINCIPAL");
        sauvegarde.isActif = res.getBoolean("ETAT");
        sauvegarde.proprietesSites = CodecJSon.decodeStringJSonToClass(res.getString("PROPERIETES_SITE"), ProprietesSite.class);
        sauvegarde.proprietesTemplate = res.getString("PROPERIETES_TEMPLATE");
        sauvegarde.dateCreation = new Date(res.getTimestamp("DATE_CREATION").getTime());
        sauvegarde.dateDerniereModification = new Date(res.getTimestamp("DATE_DERNIERE_MODIFICATION").getTime());
        sauvegarde.codeCreateur = res.getString("CODE_CREATEUR");
        sauvegarde.codeDernierModificateur = res.getString("CODE_DERNIER_MODIFICATEUR");
        sauvegarde.historique = res.getString("HISTORIQUE");
        return sauvegarde;
    }

    @Override
    public Collection<InfosSite> getListeInfosSites() throws Exception {
        final Collection<InfosSite> resultats = new ArrayList<>();
        for (final InfosSiteSauvegarde sauvegarde : getListeInfosSiteSauvegardes()) {
            resultats.add(InfosSiteCodec.decode(sauvegarde));
        }
        return resultats;
    }

    public Collection<InfosSiteSauvegarde> getListeInfosSiteSauvegardes() throws Exception {
        final ContexteDao ctx = instancierContexteDao();
        PreparedStatement stmt = null;
        ResultSet res = null;
        try {
            stmt = ctx.getConnection().prepareStatement(SELECT_TOUT);
            res = stmt.executeQuery();
            final ArrayList<InfosSiteSauvegarde> listeSauvegardes = new ArrayList<>();
            while (res.next()) {
                final InfosSiteSauvegarde sauvegarde = genererInfosSiteDepuisResultSet(res);
                listeSauvegardes.add(sauvegarde);
            }
            return listeSauvegardes;
        } finally {
            fermerAccesBD(ctx, stmt, res);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.dao.InfosSiteSauvegardeDao#supprimer(java.lang.String
     * )
     */
    @Override
    public void supprimer(final String code) throws Exception {
        final ContexteDao ctx = instancierContexteDao();
        PreparedStatement stmt = null;
        try {
            stmt = ctx.getConnection().prepareStatement(DELETE_PAR_CODE);
            stmt.setString(1, code);
            stmt.executeUpdate();
        } finally {
            fermerAccesBD(ctx, stmt);
        }
    }
}