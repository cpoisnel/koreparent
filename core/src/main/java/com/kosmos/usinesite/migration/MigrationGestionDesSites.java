package com.kosmos.usinesite.migration;

import java.io.File;
import java.io.IOException;
import java.sql.SQLSyntaxErrorException;
import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.usinesite.migration.dao.impl.InfosSiteDaoMigrationProperties;
import com.kosmos.usinesite.migration.dao.impl.InfosSiteSauvegardeDaoImpl;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.service.ServiceTemplateSite;
import com.kosmos.usinesite.template.service.ServiceTemplateSiteFactory;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.IExtension;
import com.kportal.extension.IExtensionConfig;
import com.kportal.extension.Version;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.service.ServiceInfosSiteFactory;

/**
 * Migrer les {@link InfosSite} du JTF ou de la base de données vers les properties par site.
 *
 * @author pierre.cosson
 *
 */
public class MigrationGestionDesSites implements IExtensionConfig {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationGestionDesSites.class);

    /**
     * Récupération des {@link InfosSite} définis dans le JTF.
     *
     * @return La liste de l'ensemble des {@link InfosSite} référencé dans le JTF.
     * @throws Exception
     *             Erreur durant les accés à la source de données (JTF)
     */
    private static Collection<InfosSite> getListeInfosSiteDepuisJtf() throws Exception {
        final InfosSiteDaoMigrationProperties daoMigration = new InfosSiteDaoMigrationProperties();
        return daoMigration.getListeInfosSites();
    }

    /**
     * Récupération des {@link InfosSite} définis dans la base de données.
     *
     * @return La liste de l'ensemble des {@link InfosSite} référencé dans la base de données.
     * @throws Exception
     *             Erreur durant les accés à la source de données
     */
    private static Collection<InfosSite> getListeInfosSiteDepuisBDD() throws Exception {
        final InfosSiteSauvegardeDaoImpl daoMigration = new InfosSiteSauvegardeDaoImpl();
        return daoMigration.getListeInfosSites();
    }

    /**
     * Enregristrement de la liste de {@link InfosSite} du JTF vers la base de données.
     *
     * @param listeSites
     *            Liste des sites à enregistrer.
     * @throws Exception
     *             Erreur durant l'accés à la source de données.
     */
    @SuppressWarnings("deprecation")
    private static void enregsitrerDansPropertiesInfosSites(final Collection<InfosSite> listeSites) throws Exception {
        final ServiceInfosSiteProcessus serviceClassique = (ServiceInfosSiteProcessus) ServiceInfosSiteFactory.getServiceInfosSite();
        final ServiceTemplateSite serviceTemplateSite = ServiceTemplateSiteFactory.getServiceTemplateSite();
        for (final InfosSite site : listeSites) {
            LOG.info("Traitement du site : " + site.getAlias());
            try {
                TemplateSite templateSite;
                if (StringUtils.isNotEmpty(site.getCodeTemplate())) {
                    templateSite = serviceTemplateSite.getTemplateSiteParCode(site.getCodeTemplate());
                } else {
                    templateSite = serviceTemplateSite.getTemplateSiteParDossier(site.getJspFo());
                }
                LOG.info(" - utilisation du template : " + templateSite.getCode());
                final InfosSiteImpl infosSite = (InfosSiteImpl) site;
                infosSite.setCodeTemplate(templateSite.getCode());
                infosSite.setJspFo(templateSite.getDossierJSP());
                serviceClassique.creer(infosSite);
            } catch (final ErreurDonneeNonTrouve e) {
                LOG.info("donnee non trouvé", e);
                serviceClassique.creer((InfosSiteImpl) site, null);
            } catch (final Exception e) {
                LOG.info(" - ", e);
            }
            LOG.info(" - création terminée");
            LOG.info("Fin traitement");
        }
    }

    /**
     * Lancement de la migration de l'ensemble des sites contenus dans le JTF. <br/>
     * Toutes les actions sont loggées dans le Logger de l'application en niveau INFO.
     *
     */
    @Override
    public void init(final IExtension extension, final Version old) {
        if (isFichierAMigrer(old)) {
            migrerFichiersSite();
        }
        try {
            if (CollectionUtils.isEmpty(ServiceInfosSiteFactory.getServiceInfosSite().getListeTousInfosSites())) {
                lancementMigration();
            }
        } catch (final Exception e) {
            LOG.error("erreur lors de la récupération des sites", e);
        }
    }

    private boolean isFichierAMigrer(final Version old) {
        return new Version("6.00.01").greater(old) && StringUtils.isNotBlank(WebAppUtil.getConfDir());
    }

    private void lancementMigration() {
        LOG.info("*********** MIGRATION SITES");
        Collection<InfosSite> sitesAMigrer = null;
        try {
            try {
                sitesAMigrer = getListeInfosSiteDepuisBDD();
            } catch (final SQLSyntaxErrorException e1) {
                LOG.debug("l'usine a site n'était pas installée", e1);
                sitesAMigrer = getListeInfosSiteDepuisJtf();
            }
            enregsitrerDansPropertiesInfosSites(sitesAMigrer);
        } catch (final Exception e) {
            LOG.error("impossible de migrer la déclaration de site", e);
        }
        LOG.info("*********** FIN MIGRATION SITES");
    }

    private void migrerFichiersSite() {
        final String ancienCheminDossierUAS = StringUtils.defaultString(PropertyHelper.getCoreProperty("usinesite.fichiers"), "/fichiers_usinesite");
        File ancienDossierUAS = new File(ancienCheminDossierUAS);
        if (!ancienDossierUAS.exists()) {
            ancienDossierUAS = new File(WebAppUtil.getAbsolutePath() + ancienCheminDossierUAS);
        }
        if (ancienDossierUAS.exists()) {
            final File nouveauDossierUAS = new File(WebAppUtil.getAbsoluteFichiersSitesPath());
            try {
                FileUtils.moveDirectory(ancienDossierUAS, nouveauDossierUAS);
            } catch (final IOException e) {
                LOG.error("impossible de migrer les fichiers liés aux sites", e);
            }
        }
    }

    @Override
    public void clean(final IExtension extension) {
        // nothing
    }
}
