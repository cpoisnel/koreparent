package com.kosmos.usinesite.migration.bean;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.univ.multisites.InfosSite;

/**
 * Bean utilisé pour représenter les données stockées en base de données.
 *
 * @author pierre.cosson
 *
 */
public class InfosSiteSauvegarde {

    public long idInfosSite = 0;

    public String code = StringUtils.EMPTY;

    public String intitule = StringUtils.EMPTY;

    public String httpHostname = InfosSite.HTTPHOSTNAME_DEFAUT;

    public String urlAccueil = StringUtils.EMPTY;

    public int httpPort = InfosSite.HTTPPORT_DEFAUT;

    public String httpsHostname = InfosSite.HTTPSHOSTNAME_DEFAUT;

    public int httpsPort = InfosSite.HTTPSPORT_DEFAUT;

    public String codeRubrique = StringUtils.EMPTY;

    public boolean isSitePrincipal = Boolean.FALSE;

    public boolean isActif = Boolean.TRUE;

    public String codeTemplate = StringUtils.EMPTY;

    public ProprietesSite proprietesSites = new ProprietesSite();

    public String proprietesTemplate = StringUtils.EMPTY;

    public Date dateCreation = new Date();

    public Date dateDerniereModification = new Date();

    public String codeCreateur = StringUtils.EMPTY;

    public String codeDernierModificateur = StringUtils.EMPTY;

    public transient String historique = StringUtils.EMPTY;

    public long getIdInfosSite() {
        return idInfosSite;
    }

    public void setIdInfosSite(final long idInfosSite) {
        this.idInfosSite = idInfosSite;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    public String getHttpHostname() {
        return httpHostname;
    }

    public void setHttpHostname(final String httpHostname) {
        this.httpHostname = httpHostname;
    }

    public String getUrlAccueil() {
        return urlAccueil;
    }

    public void setUrlAccueil(final String urlAccueil) {
        this.urlAccueil = urlAccueil;
    }

    public int getHttpPort() {
        return httpPort;
    }

    public void setHttpPort(final int httpPort) {
        this.httpPort = httpPort;
    }

    public String getHttpsHostname() {
        return httpsHostname;
    }

    public void setHttpsHostname(final String httpsHostname) {
        this.httpsHostname = httpsHostname;
    }

    public int getHttpsPort() {
        return httpsPort;
    }

    public void setHttpsPort(final int httpsPort) {
        this.httpsPort = httpsPort;
    }

    public String getCodeRubrique() {
        return codeRubrique;
    }

    public void setCodeRubrique(final String codeRubrique) {
        this.codeRubrique = codeRubrique;
    }

    public boolean isSitePrincipal() {
        return isSitePrincipal;
    }

    public void setSitePrincipal(final boolean isSitePrincipal) {
        this.isSitePrincipal = isSitePrincipal;
    }

    public boolean isActif() {
        return isActif;
    }

    public void setActif(final boolean isActif) {
        this.isActif = isActif;
    }

    public String getCodeTemplate() {
        return codeTemplate;
    }

    public void setCodeTemplate(final String codeTemplate) {
        this.codeTemplate = codeTemplate;
    }

    public ProprietesSite getProprietesSites() {
        return proprietesSites;
    }

    public void setProprietesSites(final ProprietesSite proprietesSites) {
        this.proprietesSites = proprietesSites;
    }

    public String getProprietesTemplate() {
        return proprietesTemplate;
    }

    public void setProprietesTemplate(final String proprietesTemplate) {
        this.proprietesTemplate = proprietesTemplate;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(final Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateDerniereModification() {
        return dateDerniereModification;
    }

    public void setDateDerniereModification(final Date dateDerniereModification) {
        this.dateDerniereModification = dateDerniereModification;
    }

    public String getCodeCreateur() {
        return codeCreateur;
    }

    public void setCodeCreateur(final String codeCreateur) {
        this.codeCreateur = codeCreateur;
    }

    public String getCodeDernierModificateur() {
        return codeDernierModificateur;
    }

    public void setCodeDernierModificateur(final String codeDernierModificateur) {
        this.codeDernierModificateur = codeDernierModificateur;
    }

    public String getHistorique() {
        return historique;
    }

    public void setHistorique(final String historique) {
        this.historique = historique;
    }

    /**
     * Produire la chaine de caractères représentant l'objet. Il s'agit d'une semi-sérialisation des données.
     *
     * @return L'instance en chaine de caractères.
     * @see ToStringBuilder#reflectionToString(Object)
     */
    public String toStringSansHistorique() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
