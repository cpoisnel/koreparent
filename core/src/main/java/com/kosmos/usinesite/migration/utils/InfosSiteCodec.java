package com.kosmos.usinesite.migration.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.usinesite.migration.bean.InfosSiteSauvegarde;
import com.kosmos.usinesite.migration.bean.ProprietesSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.kosmos.usinesite.template.service.ServiceTemplateSiteFactory;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.utils.json.CodecJSon;

/**
 * Permet de passer d'un {@link InfosSite} à un {@link InfosSiteSauvegarde} ou inversement.
 *
 * @author pierre.cosson
 *
 */
public class InfosSiteCodec {

    private static final Logger LOG = LoggerFactory.getLogger(InfosSiteCodec.class);

    /**
     * Décoder un {@link InfosSiteSauvegarde} en {@link InfosSite}. Pour cela, cette fonction va mapper les informations contenue dans le {@link InfosSiteSauvegarde} vers le
     * {@link InfosSite} en utilisant le {@link TemplateSite}.
     *
     * @param sauvegarde
     *            La sauvegarde qui contient les données au {@link InfosSite}
     * @return {@link InfosSite} créé à partir des données contenu dans la sauvegarde
     * @throws Exception
     *             Erreur durant les transfert de données
     */
    public static InfosSite decode(final InfosSiteSauvegarde sauvegarde) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        mapperDonneesBasesDansInfosSite(infosSite, sauvegarde);
        mapperProprietesTemplateDansInfosSite(infosSite, sauvegarde);
        return infosSite;
    }

    private static void mapperDonneesBasesDansInfosSite(final InfosSiteImpl infosSite, final InfosSiteSauvegarde sauvegarde) {
        infosSite.setAlias(sauvegarde.code);
        infosSite.setIntitule(sauvegarde.intitule);
        infosSite.setHttpHostname(sauvegarde.httpHostname);
        infosSite.setHttpPort(sauvegarde.httpPort);
        infosSite.setHttpsHostname(sauvegarde.httpsHostname);
        infosSite.setHttpsPort(sauvegarde.httpsPort);
        infosSite.setCodeRubrique(sauvegarde.codeRubrique);
        infosSite.setActif(sauvegarde.isActif);
        infosSite.setCodeTemplate(sauvegarde.codeTemplate);
        final ProprietesSite propSite = sauvegarde.proprietesSites;
        if (propSite == null) {
            return;
        }
        infosSite.setSslMode(propSite.sslMode);
        infosSite.setBoSslMode(propSite.boSslMode);
        infosSite.setRestriction(propSite.restriction);
        infosSite.setModeReecritureRubrique(propSite.modeReecritureRubrique);
        infosSite.setNiveauMaxReecritureRubrique(propSite.niveauMaxReecritureRubrique);
        infosSite.setNiveauMinReecritureRubrique(propSite.niveauMinReecritureRubrique);
        infosSite.setUrlAccueil(propSite.urlAccueil);
        infosSite.setSso(propSite.sso);
        infosSite.setSitePrincipal(propSite.sitePrincipal);
        infosSite.setHttpActions(new TreeSet<>(propSite.httpActions));
        infosSite.setHttpsActions(new TreeSet<>(propSite.httpsActions));
        infosSite.setListeHostAlias(propSite.listeHostAlias);
    }

    private static void mapperProprietesTemplateDansInfosSite(final InfosSiteImpl infosSite, final InfosSiteSauvegarde sauvegarde) throws Exception {
        final String codeTemplate = sauvegarde.codeTemplate;
        if (StringUtils.isEmpty(codeTemplate)) {
            return;
        }
        try {
            final TemplateSite template = ServiceTemplateSiteFactory.getServiceTemplateSite().getTemplateSiteParCode(codeTemplate);
            infosSite.setJspFo(template.getDossierJSP());
            mapperDonneesComplementairesTemplateDansInfosSite(infosSite, sauvegarde, template);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.info("le template n'a pas été trouvé ", e);
        }
    }

    private static void mapperDonneesComplementairesTemplateDansInfosSite(final InfosSiteImpl infosSite, final InfosSiteSauvegarde sauvegarde, final TemplateSite template) throws Exception {
        final String propertiesJSonString = sauvegarde.proprietesTemplate;
        if (StringUtils.isEmpty(propertiesJSonString)) {
            // aucune données spéciques template
            return;
        }
        final Map<String, Object> proprietesComplementaires = CodecJSon.decodeStringJSonToClass(propertiesJSonString, HashMap.class);
        for (final Map.Entry<String, Object> proprieteComplementaire : proprietesComplementaires.entrySet()) {
            infosSite.putProperty(proprieteComplementaire.getKey(), proprieteComplementaire.getValue());
        }
    }

    /**
     * Permet de construire un {@link InfosSiteSauvegarde} depuis un {@link InfosSite}. Cette fonction n'utilisera pas les données spécifique liées au template de site (si de
     * telles données sont présentes de le site, elles seront perdues ainsi que le dossier de JSP front qui deviendra le dossier par défaut).
     *
     * @param infosSite
     *            données qui seront utilisées pour construire la sauvegarde.
     * @return La sauvegarde des données du {@link InfosSite} passé en paramétre
     * @throws Exception
     *             Erreur durant le "mapping/serialisation" des données
     */
    public static InfosSiteSauvegarde encode(final InfosSite infosSite) throws Exception {
        return encode(infosSite, null);
    }

    /**
     * Permet de construire un {@link InfosSiteSauvegarde} depuis un {@link InfosSite}.
     *
     * @param infosSite
     *            données qui seront utilisées pour construire la sauvegarde.
     * @param template
     *            Utilisé pour mapper/serialiser les données spécifiques relatives au template de site
     * @return La sauvegarde des données du {@link InfosSite} passé en paramètre
     * @throws Exception
     *             Erreur durant le "mapping/serialisation" des données
     */
    public static InfosSiteSauvegarde encode(final InfosSite infosSite, final TemplateSite template) throws Exception {
        final InfosSiteSauvegarde sauvegarde = new InfosSiteSauvegarde();
        mapperDonneesBasesDansSauvegarde(sauvegarde, infosSite);
        mapperProprietesComplementairesDansSauvegarde(sauvegarde, infosSite);
        mapperProprietesTemplateDansSauvegarde(sauvegarde, infosSite, template);
        return sauvegarde;
    }

    /**
     * Mapping simple des données du site.
     *
     * @param sauvegarde
     * @param infosSite
     */
    private static void mapperDonneesBasesDansSauvegarde(final InfosSiteSauvegarde sauvegarde, final InfosSite infosSite) {
        sauvegarde.code = infosSite.getAlias();
        sauvegarde.intitule = infosSite.getIntitule();
        sauvegarde.httpHostname = infosSite.getHttpHostname();
        sauvegarde.httpPort = infosSite.getHttpPort();
        sauvegarde.httpsHostname = infosSite.getHttpsHostname();
        sauvegarde.httpsPort = infosSite.getHttpsPort();
        sauvegarde.codeRubrique = infosSite.getCodeRubrique();
        sauvegarde.isSitePrincipal = infosSite.isSitePrincipal();
        sauvegarde.urlAccueil = infosSite.getUrlAccueil();
    }

    /**
     * Sérialisation JSon des données du site.
     *
     * @param sauvegarde
     * @param infosSite
     */
    private static void mapperProprietesComplementairesDansSauvegarde(final InfosSiteSauvegarde sauvegarde, final InfosSite infosSite) {
        final ProprietesSite propSite = new ProprietesSite();
        propSite.sslMode = infosSite.getSslMode();
        propSite.boSslMode = infosSite.getBoSslMode();
        propSite.restriction = infosSite.getRestriction();
        propSite.modeReecritureRubrique = infosSite.getModeReecritureRubrique();
        propSite.niveauMaxReecritureRubrique = infosSite.getNiveauMaxReecritureRubrique();
        propSite.niveauMinReecritureRubrique = infosSite.getNiveauMinReecritureRubrique();
        propSite.sso = infosSite.isSso();
        propSite.sitePrincipal = infosSite.isSitePrincipal();
        propSite.urlAccueil = infosSite.getUrlAccueil();
        propSite.httpActions = infosSite.getHttpActions();
        propSite.httpsActions = infosSite.getHttpsActions();
        propSite.listeHostAlias = infosSite.getListeHostAlias();
        sauvegarde.proprietesSites = propSite;
    }

    /**
     * Sérialisation JSON des données relatives au template de site.
     *
     * @param sauvegarde
     * @param infosSite
     * @param template
     * @throws Exception
     */
    private static void mapperProprietesTemplateDansSauvegarde(final InfosSiteSauvegarde sauvegarde, final InfosSite infosSite, final TemplateSite template) throws Exception {
        if (template == null) {
            // pas de template donc pas de données spécifiques template
            return;
        }
        final Map<String, Object> proprietesTemplate = new HashMap<>();
        for (final TemplateSiteProperty property : template.getListeProprietesComplementaires()) {
            final Object valeurProperty = infosSite.getProprieteComplementaire(property.getCode());
            if (valeurProperty != null) {
                proprietesTemplate.put(property.getCode(), valeurProperty);
            }
        }
        sauvegarde.proprietesTemplate = CodecJSon.encodeObjectToJSonInString(proprietesTemplate);
        sauvegarde.codeTemplate = template.getCode();
    }
}
