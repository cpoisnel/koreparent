package com.kosmos.usinesite.migration.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.SortedSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kportal.core.config.PropertyConfigurer;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.dao.impl.InfosSiteDaoProperties;
import com.univ.multisites.helper.InfosSitePropertiesHelper;
import com.univ.multisites.helper.InfosSitePropertiesHelper.FinNomProprieteSite;

import static com.univ.multisites.helper.InfosSitePropertiesHelper.DEBUT_PROPERTIES_SITE;

/**
 * DAO qui permet de LIRE (et uniquement) les {@link InfosSite} depuis les proerties chargée dans l'application.<br/>
 * <br/>
 *
 *
 * <br/>
 * <strong>INFO sur le format des properties</strong><br/>
 * Déclaration du site principal :
 * <ul>
 * <li>site.principal=[ID]</li>
 * </ul>
 * Paramétrage pour chaque site dans le jtf :
 * <ul>
 * <li>site.[ID].intitule=[INTITULE]</li>
 * <li>
 * site.[ID].host=[HTTP_HOSTNAME]</li>
 * <li>
 * site.[ID].port=[HTTP_PORT]</li>
 * <li>
 * site.[ID].https_host=[HTTPS_HOSTNAME]</li>
 * <li>
 * site.[ID].https_port=[HTTPS_PORT]</li>
 * <li>
 * site.[ID].ssl_mode=[SSL_MODE]</li>
 * <li>
 * site.[ID].http_actions=[HTTP_ACTIONS]</li>
 * <li>
 * site.[ID].https_actions=[HTTPS_ACTIONS]</li>
 * <li>
 * site.[ID].bo.ssl_mode=[BO_SSL_MODE]</li>
 * <li>
 * site.[ID].rubrique=[CODE_RUBRIQUE]</li>
 * <li>
 * site.[ID].alias=[ALIAS]</li>
 * </ul>
 * où :
 * <ul>
 * <li>[ID] est l'identifiant du site</li>
 * <li>
 * [INTITULE] est le nom du site</li>
 * <li>
 * [HTTP_HOSTNAME] est le nom du virtual host pour l'appli en http</li>
 * <li>
 * [HTTP_PORT] est le numéro de port pour l'appli en http (paramètre optionnel, par défaut = 80)</li>
 * <li>
 * [HTTPS_HOSTNAME] est le nom du virtual host pour l'appli en https (paramètre optionnel, par défaut = [HTTP_HOSTNAME])</li>
 * <li>
 * [HTTPS_PORT] est le numéro de port pour l'appli en https (paramètre optionnel, par défaut = 443)</li>
 * <li>
 * [SSL_MODE] est le mode de fonctionnement pour la détermination des URLs (paramètre optionnel, par défaut = 0, valeurs possibles : 0 pour mode non contextuel, 1 pour mode
 * contextuel)</li>
 * <li>
 * [HTTP_ACTIONS] est la liste des actions que l'on force en http, utile uniquement en mode contextuel (paramètre optionnel, par défaut = "", valeurs possibles : "" | "DECONNECTER"
 * )</li>
 * <li>
 * [HTTPS_ACTIONS] est la liste des actions que l'on force en https, séparées par un ';' (paramètre optionnel, par défaut = "", valeurs possibles : "" | "LOGIN" | "PERSONNALISER" |
 * "DEMANDER_MDP" | "PRESENTER_MDP" )</li>
 * <li>
 * [BO_SSL_MODE] est le mode de fonctionnement SSL du back-office, utile uniquement pour CAS en mode proxy (paramètre optionnel, par défaut = 0, valeurs possibles : 0 pour http, 1
 * pour https)</li>
 * <li>
 * [CODE_RUBRIQUE] est le code de la rubrique mère du site (paramètre optionnel, par défaut = "")</li>
 * <li>
 * [ALIAS] est la liste des différents alias du host principal séparés par des points virgules</li>
 * </ul>
 *
 * @author pierre.cosson
 *
 */
public class InfosSiteDaoMigrationProperties extends InfosSiteDaoProperties {

    /*
     * (non-Javadoc)
     *
     * @see com.univ.multisites.dao.InfosSiteDao#getInfosSite(java.lang.String)
     */
    @Override
    public InfosSite getInfosSite(final String code) throws Exception {
        verifierInfoSiteExiste(code);
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        infosSite.setAlias(code);
        final String intitule = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_INTITULE);
        if (StringUtils.isNotEmpty(intitule)) {
            infosSite.setIntitule(intitule);
        }
        final String httpHost = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HOST);
        if (StringUtils.isNotEmpty(httpHost)) {
            infosSite.setHttpHostname(httpHost);
        }
        final String httpsHost = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HTTPS_HOST);
        if (StringUtils.isNotEmpty(httpsHost)) {
            infosSite.setHttpsHostname(httpsHost);
        }
        final String listeAliasHostsSite = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_ALIAS);
        if (StringUtils.isNotEmpty(listeAliasHostsSite)) {
            final String tableauAliasHost[] = listeAliasHostsSite.split(String.valueOf(InfosSitePropertiesHelper.VALEUR_PROPERTIE_LISTE_DELIMITEUR));
            final HashSet<String> listeAliasHost = new HashSet<>();
            CollectionUtils.addAll(listeAliasHost, tableauAliasHost);
            infosSite.setListeHostAlias(listeAliasHost);
        }
        final String httpPort = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_PORT);
        if (StringUtils.isNotEmpty(httpPort) && StringUtils.isNumeric(httpPort)) {
            infosSite.setHttpPort(Integer.parseInt(httpPort));
        }
        final String httpsPort = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HTTPS_PORT);
        if (StringUtils.isNotEmpty(httpsPort) && StringUtils.isNumeric(httpsPort)) {
            infosSite.setHttpsPort(Integer.parseInt(httpsPort));
        }
        final String sslMode = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_SSL_MODE);
        if (StringUtils.isNotEmpty(sslMode) && StringUtils.isNumeric(sslMode)) {
            infosSite.setSslMode(Integer.parseInt(sslMode));
        }
        final String boSSLMode = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_BO_SSL_MODE);
        if (StringUtils.isNotEmpty(boSSLMode) && StringUtils.isNumeric(boSSLMode)) {
            infosSite.setBoSslMode(Integer.parseInt(boSSLMode));
        }
        final String httpAction = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HTTP_ACTIONS);
        if (StringUtils.isNotEmpty(httpAction)) {
            final SortedSet<Integer> listeActions = InfosSitePropertiesHelper.transformerActionsStringEnListe(httpAction);
            infosSite.setHttpActions(listeActions);
        }
        final String httpsAction = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HTTPS_ACTIONS);
        if (StringUtils.isNotEmpty(httpsAction)) {
            final SortedSet<Integer> listeActions = InfosSitePropertiesHelper.transformerActionsStringEnListe(httpsAction);
            infosSite.setHttpsActions(listeActions);
        }
        final String siteRubrique = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_RUBRIQUE);
        if (StringUtils.isNotEmpty(siteRubrique)) {
            infosSite.setCodeRubrique(siteRubrique);
        } else {
            infosSite.setCodeRubrique(StringUtils.EMPTY);
        }
        gestionReecritureUrl(code, infosSite);
        final String siteURLAccueil = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_URL_ACCUEIL);
        if (StringUtils.isNotEmpty(siteURLAccueil)) {
            infosSite.setUrlAccueil(siteURLAccueil);
        }
        final String siteRestriction = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_RESTRICTION);
        if (StringUtils.isNotEmpty(siteRestriction) && StringUtils.isNumeric(siteRestriction)) {
            infosSite.setRestriction(Integer.parseInt(siteRestriction));
        }
        final String sso = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_SSO);
        if (StringUtils.isNotEmpty(sso)) {
            infosSite.setSso(InfosSitePropertiesHelper.STRING_BOOLEAN_TRUE.equals(sso));
        }
        final String jspFO = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_JSP_FO);
        if (StringUtils.isNotEmpty(jspFO)) {
            infosSite.setJspFo(StringUtils.removeEnd(jspFO, InfosSitePropertiesHelper.SLASH));
        }
        final String codeTemplate = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_TEMPLATE);
        if (StringUtils.isNotEmpty(codeTemplate)) {
            infosSite.setCodeTemplate(codeTemplate);
        }
        final String codeSitePrincipal = PropertyConfigurer.getProperty(InfosSitePropertiesHelper.PROPERTIES_SITE_PRINCIPAL);
        if (StringUtils.isNotEmpty(codeSitePrincipal)) {
            infosSite.setSitePrincipal(codeSitePrincipal.equalsIgnoreCase(code));
        }
        infosSite.setActif(Boolean.TRUE);
        setListeProprietesComplementairesDansInfosSite(infosSite);
        return infosSite;
    }

    /**
     * Il existe un bug dans la gestion des réécritures des URLS en 5.1 et dans l'ancienne version de l'UAS. Le comportement choisi est le suivant : si la valeur est != 0 on
     * rajoute les rubriques dans l'url (correspond au code du produit) Cependant, dans le produit si la valeur du mode est != 0 mais que les niveaux min & max ne sont pas
     * renseigné seul le nom de la page est affiché.
     *
     * @param code le code du site dont on souhaite récupérer les proprités de gestion des urls
     * @param infosSite l'infossite à mettre à jour en fonction de ces parametres
     */
    private void gestionReecritureUrl(final String code, final InfosSiteImpl infosSite) {
        final String reecritureRubriqueMode = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MODE);
        if (StringUtils.isNotEmpty(reecritureRubriqueMode) && StringUtils.isNumeric(reecritureRubriqueMode)) {
            final int valeurModeReecriture = Integer.parseInt(reecritureRubriqueMode);
            if (valeurModeReecriture > 0) {
                infosSite.setModeReecritureRubrique(1);
            } else {
                infosSite.setModeReecritureRubrique(0);
            }
        }
        if (infosSite.getModeReecritureRubrique() > 0) {
            final String reecritureRubriqueMin = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MIN);
            if (StringUtils.isNotEmpty(reecritureRubriqueMin) && StringUtils.isNumeric(reecritureRubriqueMin)) {
                infosSite.setNiveauMinReecritureRubrique(Integer.parseInt(reecritureRubriqueMin));
            }
            final String reecritureRubriqueMax = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MAX);
            if (StringUtils.isNotEmpty(reecritureRubriqueMax) && StringUtils.isNumeric(reecritureRubriqueMax)) {
                infosSite.setNiveauMaxReecritureRubrique(Integer.parseInt(reecritureRubriqueMax));
            }
        }
    }

    private void verifierInfoSiteExiste(final String code) throws ErreurDonneeNonTrouve {
        // on vérifie sur le host car c'est une données obligatoire et
        // indispensable
        final String host = PropertyConfigurer.getProperty(DEBUT_PROPERTIES_SITE + code + InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HOST);
        if (StringUtils.isEmpty(host)) {
            throw new ErreurDonneeNonTrouve("Impossible de trouver le site \"" + code + "\"");
        }
    }

    /**
     * Insérer dans l'infosSite toutes les propriétés en rapport avec le site.
     *
     * @param infosSite l'infossite dont on souhaite ajouter l'ensemble des propriétes qui le concerne et qui ne sont pas par défaut.
     */
    private void setListeProprietesComplementairesDansInfosSite(final InfosSiteImpl infosSite) {
        final String debutCleProprieteSite = InfosSitePropertiesHelper.DEBUT_PROPERTIES_SITE + infosSite.getAlias() + ".";
        final Properties toutesLesProprietes = PropertyConfigurer.getInstance().getProperties();
        for (final Entry<Object, Object> property : toutesLesProprietes.entrySet()) {
            final String clePropriete = String.valueOf(property.getKey());
            if (!StringUtils.startsWithIgnoreCase(clePropriete, debutCleProprieteSite)) {
                continue;
            }
            // site.code_site.cleProprieteSite === DEVIENT ==> cleProprieteSite
            // (il s'agit de la proriete site)
            final String cleProprieteSite = StringUtils.removeStartIgnoreCase(clePropriete, debutCleProprieteSite);
            if (FinNomProprieteSite.isProprieteSite(cleProprieteSite)) {
                continue;
            }
            if (StringUtils.isNotEmpty(cleProprieteSite)) {
                final String valeur = PropertyConfigurer.getProperty(clePropriete);
                infosSite.putProperty(cleProprieteSite, valeur);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.multisites.dao.InfosSiteDao#getListeInfosSites()
     */
    @Override
    public Collection<InfosSite> getListeInfosSites() throws Exception {
        final ArrayList<InfosSite> listeInfosSites = new ArrayList<>();
        // parcours de toutes les properties afin de trouvers les alias des
        // sites => on se base sur la propriété site.CODE_SITE.host
        final Properties toutesLesProprietes = PropertyConfigurer.getInstance().getProperties();
        for (final Entry<Object, Object> property : toutesLesProprietes.entrySet()) {
            final String clePropriete = String.valueOf(property.getKey());
            if (!StringUtils.startsWith(clePropriete, InfosSitePropertiesHelper.DEBUT_PROPERTIES_SITE)) {
                continue;
            }
            final String cleProprieteSplitte[] = StringUtils.split(clePropriete, InfosSitePropertiesHelper.PROPERTIE_KEY_DELIMITEUR);
            if (cleProprieteSplitte.length > 2 && cleProprieteSplitte[2].equals(InfosSitePropertiesHelper.HOST)) {
                final String codeSite = cleProprieteSplitte[1];
                final InfosSite infosSite = getInfosSite(codeSite);
                listeInfosSites.add(infosSite);
            }
        }
        final String sitePrincipal = toutesLesProprietes.getProperty("site.principal");
        if (StringUtils.isNotBlank(sitePrincipal)) {
            for (final InfosSite site : listeInfosSites) {
                if (sitePrincipal.equals(site.getAlias())) {
                    ((InfosSiteImpl) site).setSitePrincipal(Boolean.TRUE);
                }
            }
        }
        return listeInfosSites;
    }

    @Override
    public void miseAJour(final InfosSite infosSite) throws Exception {
        throw new UnsupportedOperationException("ce DAO n'implemente pas de mise à jour car il sert uniquement à la migration du parametrage");
    }

    @Override
    public void supprimer(final String code) throws Exception {
        throw new UnsupportedOperationException("ce DAO n'implemente pas de suppression ");
    }
}
