package com.kosmos.content.link.service;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.content.link.view.model.LinkInputViewModel;
import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.service.ServiceTagTypeToolbox;
import com.univ.utils.EscapeString;

/**
 * Created on 15/06/15.
 */
public class ServiceLinkType extends ServiceTagTypeToolbox {

    public static final String TYPE_TAG = "URL";

    public static final String ID_BEAN = "linkTypeService";

    public static final String VIEW_MODEL = "viewModel";

    @Override
    public String generateUrlTag(InfoBean infoBean) {
        infoBean.set(VIEW_MODEL, getViewModel(infoBean));
        infoBean.set("FCK_PLUGIN", "true");
        return "./link/generalInput.jsp";
    }


    public LinkInputViewModel getViewModel(InfoBean infoBean) {
        final String value = EscapeString.unescapeURL(StringUtils.defaultString(infoBean.get("FORMATTER_VALUE", String.class)));
        final LinkInputViewModel model = new LinkInputViewModel();
        final TagValue linkValue = new TagValue();
        final boolean textMandatory = !StringUtils.isNotBlank(infoBean.get("LINK_TEXT_MANDATORY", String.class)) || Boolean.parseBoolean(infoBean.getString("LINK_TEXT_MANDATORY"));
        linkValue.setValue(value);
        identifyTag(linkValue, TYPE_TAG);
        model.setTextMandatory(textMandatory);
        model.setLinkLabel(infoBean.get("LINK_LABEL", String.class));
        model.setTagValue(linkValue);
        model.setTagViewDescriptors(getTagViewDescriptors(linkValue, TYPE_TAG));
        return model;
    }


}
