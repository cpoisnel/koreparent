package com.kosmos.plansite;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.ContexteUniv;
import com.univ.utils.UnivWebFmt;

/**
 * Classe permettant de générer un plan de site.
 *
 * @author FBI
 */
public class PlanSite {

    private static final Logger LOG = LoggerFactory.getLogger(PlanSite.class);

    /** The ctx. */
    private ContexteUniv ctx = null;

    /** The rubrique mere. */
    private InfosRubriques rubriqueMere = null;

    /** The nb niveaux. */
    private int nbNiveaux = 0;

    /**
     * Constructeur.
     *
     * @param ctx
     *            Le contexte
     * @param rubriqueMere
     *            La rubrique à partir de laquelle on construit le plan
     * @param nbNiveaux
     *            Le nombre de niveaux du plan à partir de la rubrique mère (0 = pas de limitation de niveau)
     * @deprecated ne pas utiliser ce constructeur, il est présent uniquement pour la rétro compat
     */
    @Deprecated
    public PlanSite(final ContexteUniv ctx, final InfosRubriques rubriqueMere, final int nbNiveaux) {
        this.ctx = ctx;
        this.rubriqueMere = rubriqueMere;
        this.nbNiveaux = nbNiveaux;
    }

    /**
     * Renvoie le plan du site pour la rubrique et le nombre de niveaux demandés.
     *
     * @return Une chaine contenant le code HTML du plan de site
     *
     * @throws Exception
     *             the exception
     */
    public String getPlan() throws Exception {
        return buildPlan(rubriqueMere, 1);
    }

    /**
     * Méthode récursive construisant le plan de site noeud par noeud.
     *
     * @param rubrique
     *            La rubrique à partir de laquelle on construit le noeud
     * @param niveau
     *            Le niveau du noeud dans l'arbre
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    private String buildPlan(final InfosRubriques rubrique, final int niveau) throws Exception {
        final StringBuilder sb = new StringBuilder();
        final Collection<InfosRubriques> cSousRubriques = rubrique.getListeSousRubriquesFront(ctx);
        if (!cSousRubriques.isEmpty()) {
            final Iterator<InfosRubriques> it = cSousRubriques.iterator();
            sb.append("<ul class=\"plan-site__").append(niveau).append("\">");
            while (it.hasNext()) {
                InfosRubriques rubCourante = it.next();
                // ECLISSON : 0004047 : ajout du test if
                if (rubCourante != null) {
                    sb.append("<li>");
                    sb.append("<span class=\"plan-site__rubrique\">");
                    String urlAccueil = UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, rubCourante.getCode());
                    if (urlAccueil.length() > 0) {
                        sb.append("<a href=\"").append(urlAccueil).append("\">");
                    }
                    sb.append(rubCourante.getIntitule());
                    if (urlAccueil.length() > 0) {
                        sb.append("</a>");
                    }
                    sb.append("</span>");
                    if (niveau < nbNiveaux || nbNiveaux == 0) {
                        sb.append(buildPlan(rubCourante, niveau + 1)); // appel récursif
                    }
                    sb.append("</li>");
                }
            }
            sb.append("</ul>");
        }
        return sb.toString();
    }

    public static String buildPlan(final RubriqueBean rubrique, final ContexteUniv ctx, final int nbNiveaux) {
        return buildPlan(rubrique, ctx, nbNiveaux, 1);
    }
    /**
     * Méthode récursive construisant le plan de site noeud par noeud.
     *
     * @param rubrique
     *            La rubrique à partir de laquelle on construit le noeud
     * @param niveau
     *            Le niveau du noeud dans l'arbre
     *
     * @return le code html du plan de site
     *
     */
    public static String buildPlan(final RubriqueBean rubrique, final ContexteUniv ctx, final int nbNiveaux, final int niveau) {
        final StringBuilder sb = new StringBuilder();
        if (rubrique != null) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final List<RubriqueBean> sousRubriques = serviceRubrique.getRubriqueByCodeParentFront(rubrique.getCode(), ctx.getGroupesDsiAvecAscendants());
            if (!sousRubriques.isEmpty()) {
                sb.append(generateHtml(ctx, nbNiveaux, niveau, sousRubriques));
            }
        }
        return sb.toString();
    }

    private static String generateHtml(final ContexteUniv ctx, final int nbNiveaux, final int niveau, final List<RubriqueBean> sousRubriques) {
        final StringBuilder sb = new StringBuilder();
        sb.append("<ul class=\"plan-site__").append(niveau).append("\">");
        for (RubriqueBean rubriqueCourante : sousRubriques) {
            String urlAccueil = "";
            sb.append("<li>");
            sb.append("<span class=\"plan-site__rubrique\">");
            try {
                urlAccueil = UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, rubriqueCourante.getCode());
            } catch (Exception e) {
                LOG.error("unable to compute section URL", e);
            }
            if (StringUtils.isNotBlank(urlAccueil)) {
                sb.append("<a href=\"").append(urlAccueil).append("\">");
            }
            sb.append(rubriqueCourante.getIntitule());
            if (StringUtils.isNotBlank(urlAccueil)) {
                sb.append("</a>");
            }
            sb.append("</span>");
            if (niveau < nbNiveaux || nbNiveaux == 0) {
                sb.append(buildPlan(rubriqueCourante, ctx, nbNiveaux, niveau + 1)); // appel récursif
            }
            sb.append("</li>");
        }
        sb.append("</ul>");
        return sb.toString();
    }
}
