package com.kosmos.toolbox.provider.impl.tag;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.toolbox.bean.TagValue;

/**
 * Created by olivier.camon on 01/09/15.
 */
public class KTagOtherDataProvider extends AbstractKTagDataProvider  {

    @Override
    public void populateData(final Map<String, String> data, final TagValue tagValue) {
        if (StringUtils.startsWith(tagValue.getValue(), "[plan_site;")) {
            final String value = StringUtils.substringBetween(tagValue.getValue(), "[plan_site;", "]");
            splitValues(data, value);
            retrieveSectionInfosExceptDynamik(data);
        } else if (StringUtils.startsWith(tagValue.getValue(), "[url;")) {
            data.put("url", StringUtils.substringBetween(tagValue.getValue(), "[url;", "]"));
        } else if (StringUtils.startsWith(tagValue.getValue(), "[page;")) {
            data.put("page", StringUtils.substringBetween(tagValue.getValue(), "[page;", "]"));
        }
    }

    @Override
    protected void splitValues(final Map<String, String> data, final String tagValue) {
        final String[] values = tagValue.split(";");
        for (String currentValue : values) {
            final String[] keyValue = currentValue.split("=");
            if (keyValue.length == 2) {
                data.put(keyValue[0], keyValue[1]);
            }
        }
    }
}
