package com.kosmos.toolbox.provider.impl.tag;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.toolbox.bean.TagValue;

/**
 * Created on 03/08/15.
 */
public class KTagDashboardDataProvider extends AbstractKTagDataProvider {

    @Override
    public void populateData(final Map<String, String> data, final TagValue tagValue) {
        final String value = StringUtils.substringBetween(tagValue.getValue(), "[traitement;liste_fiches;", "]");
        splitValues(data, value);
        final Map<String, String> rectifiedData = updateSpecificFields(data);
        data.putAll(rectifiedData);
        retrieveSectionInfosExceptDynamik(data);
        retrieveStructureInfosExceptDynamik(data);
        final String publicVise = data.get("DIFFUSION_PUBLIC_VISE");
        if (StringUtils.isNotBlank(publicVise) && !publicVise.startsWith("|")) {
            retrieveDsiInfosExceptDynamik(data);
        }
    }

    private Map<String, String> updateSpecificFields(final Map<String, String> data) {
        final Map<String, String> rectifiedData = new HashMap<>(data);
        for (Map.Entry<String, String> dataEntry : data.entrySet()) {
            if (StringUtils.contains(dataEntry.getValue(), "|") && !"ACTION".equals(dataEntry.getKey())) {
                if (StringUtils.startsWith(dataEntry.getValue(), "|")) {
                    rectifiedData.putAll(setSpecificValue(dataEntry.getKey(), dataEntry.getValue()));
                } else {
                    rectifiedData.put(dataEntry.getKey(), StringUtils.substringBefore(dataEntry.getValue(), "|"));
                    rectifiedData.putAll(setSpecificValue(dataEntry.getKey(), StringUtils.substringAfter(dataEntry.getValue(), "|")));
                }
            }

        }
        final String objets = data.get("OBJET");
        if (StringUtils.isNotBlank(objets) && objets.contains(",")) {
            rectifiedData.put("OBJET", StringUtils.replace(objets, ",", ";"));
        }
        return rectifiedData;
    }

    private Map<String, String> setSpecificValue(final String key, final String tag) {
        final Map<String, String> values = new HashMap<>();
        final String[] foo = StringUtils.split(tag, "|");
        values.put("liste_fiches_req_" + key, foo[0]);
        values.put("liste_fiches_tri_" + key, foo[1]);
        return values;
    }
}
