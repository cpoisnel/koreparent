package com.kosmos.toolbox.provider.manager;

import java.util.HashMap;
import java.util.Map;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.toolbox.provider.TagDataProvider;
import com.kportal.extension.module.AbstractBeanManager;

/**
 * Created on 15/06/15.
 */
public class TagDataManager extends AbstractBeanManager {

    private Map<String, TagDataProvider> builders = new HashMap<>();

    public Map<String, TagDataProvider> getBuilders() {
        return builders;
    }

    @Override
    public void refresh() {
        builders.clear();
        final Map<String, TagDataProvider> beans = ApplicationContextManager.getAllBeansOfType(TagDataProvider.class);
        for(TagDataProvider currentBuilder : beans.values()) {
            builders.put(currentBuilder.getForId(), currentBuilder);
        }
    }

    public TagDataProvider getForId(String id) {
        return builders.get(id);
    }
}
