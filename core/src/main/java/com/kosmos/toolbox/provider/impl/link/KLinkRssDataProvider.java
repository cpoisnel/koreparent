package com.kosmos.toolbox.provider.impl.link;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.provider.impl.AbstractTagDataProvider;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.utils.ContexteUtil;

/**
 * Created on 22/06/15.
 */
public class KLinkRssDataProvider extends AbstractTagDataProvider {

    private static final Logger LOG = LoggerFactory.getLogger(KLinkRssDataProvider.class);

    private ServiceStructure serviceStructure;

    public void setServiceStructure(final ServiceStructure serviceStructure) {
        this.serviceStructure = serviceStructure;
    }

    @Override
    public void populateData(final Map<String, String> data, final TagValue tagValue) {
        final String request = data.get("REQUEST");
        if(StringUtils.isNotBlank(request)) {
            final Map<String, String> requestData = buildRequestData(request.substring(request.indexOf(";") + 1, request.length()));
            handleRubrique(requestData);
            handleStructure(requestData);
            handleDsi(requestData);
            data.put("OBJECT_NAME", requestData.get("objet"));
            data.put("REQUEST_DATA", buildEditString(requestData));
        }
    }

    private String buildEditString(final Map<String, String> params) {
        final StringBuilder editString = new StringBuilder();
        for(Map.Entry<String, String> currentEntry : params.entrySet()) {
            editString.append(String.format("&%s=%s", currentEntry.getKey(), currentEntry.getValue()));
        }
        return editString.toString();
    }

    private Map<String, String> buildRequestData(final String request) {
        final Map<String, String> params = new HashMap<>();
        final String[] splitted = request.split("&");
        for (final String aSplitted : splitted) {
            final String[] pair = aSplitted.split("=");
            if (pair.length == 2) {
                params.put(pair[0], pair[1]);
            }
        }
        return params;
    }

    private void handleDsi(final Map<String, String> requestData) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final String groupeDsi = requestData.get("GROUPE_DSI");
        if(StringUtils.isNotBlank(groupeDsi)) {
            if("DYNAMIK".equals(groupeDsi)) {
                requestData.remove("GROUPE_DSI");
                requestData.put("GROUPE_DSI_COURANT_1", "1");
            } else if("[groupe]".equals(groupeDsi)) {
                requestData.remove("GROUPE_DSI");
                requestData.put("GROUPE_DSI_COURANT_2", "2");
            } else if (groupeDsi.contains("_TYPEGROUPE") && belongToGroupType(groupeDsi)) {
                requestData.remove("GROUPE_DSI");
                requestData.put("GROUPE_DSI_COURANT_3", "3");
                requestData.put("TYPE_GROUPE_DSI", groupeDsi.replace("_TYPEGROUPE", StringUtils.EMPTY));
            } else {
                requestData.put("LIBELLE_GROUPE_DSI", serviceGroupeDsi.getIntitule(groupeDsi));
            }
        }
    }

    private boolean belongToGroupType(final String groupeDsi) {
        final Collection<String> groupTypes = LabelUtils.getLabelCombo("11", ContexteUtil.getContexteUniv().getLocale()).keySet();
        return groupTypes.contains(groupeDsi.replace("_TYPEGROUPE", StringUtils.EMPTY));
    }

    private void handleRubrique(final Map<String, String> requestData) {
        String codeRubrique = requestData.get("CODE_RUBRIQUE");
        if(StringUtils.isNotBlank(codeRubrique)) {
            if (codeRubrique.contains("_NOARBO")) {
                codeRubrique = codeRubrique.replace("_NOARBO", StringUtils.EMPTY);
                requestData.put("NOARBO_RUBRIQUE", "1");
                requestData.put("CODE_RUBRIQUE", codeRubrique);
            }
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (rubriqueBean != null) {
                requestData.put("LIBELLE_CODE_RUBRIQUE", rubriqueBean.getIntitule());
            }
        }
    }

    private void handleStructure(final Map<String, String> requestData) {
        final String dataKey = requestData.get("CODE_RATTACHEMENT") != null ? "CODE_RATTACHEMENT" : "CODE_STRUCTURE";
        String codeStructure = StringUtils.defaultIfEmpty(requestData.get(dataKey), StringUtils.EMPTY);
        if(StringUtils.isNotBlank(codeStructure)) {
            if (codeStructure.contains("_NOARBO")) {
                codeStructure = codeStructure.replace("_NOARBO", StringUtils.EMPTY);
                requestData.put("NOARBO_CODE_RATTACHEMENT", "1");
                requestData.put("CODE_RATTACHEMENT", codeStructure);
            }
            final String label = serviceStructure.getDisplayableLabel(codeStructure, ContexteUtil.getContexteUniv().getLangue());
            requestData.put("LIBELLE_CODE_RATTACHEMENT", label);
        }
    }
}
