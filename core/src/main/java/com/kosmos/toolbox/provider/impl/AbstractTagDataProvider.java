package com.kosmos.toolbox.provider.impl;

import com.kosmos.toolbox.provider.TagDataProvider;

/**
 * Created on 22/06/15.
 */
public abstract class AbstractTagDataProvider implements TagDataProvider {

    private String forId;

    public void setForId(final String forId) {
        this.forId = forId;
    }

    @Override
    public String getForId() {
        return forId;
    }
}
