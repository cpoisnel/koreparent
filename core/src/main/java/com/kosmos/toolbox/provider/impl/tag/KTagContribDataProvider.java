package com.kosmos.toolbox.provider.impl.tag;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.toolbox.bean.TagValue;

/**
 * Created on 03/08/15.
 */
public class KTagContribDataProvider extends AbstractKTagDataProvider {

    @Override
    public void populateData(final Map<String, String> data, final TagValue tagValue) {
        final String value = StringUtils.substringBetween(tagValue.getValue(), "[traitement;lien_fiches;", "]");
        splitValues(data, value);
        retrieveSectionInfosExceptDynamik(data);
        retrieveStructureInfosExceptDynamik(data);
        retrieveDsiInfosExceptDynamik(data);
    }

}
