package com.kosmos.toolbox.config;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.util.PropertyPlaceholderHelper;

/**
 * The Class PropertyConfigurer.
 */
public class CkeditorConfigurer extends PropertyPlaceholderConfigurer implements ApplicationContextAware {

    protected static final String ID_BEAN = "ckeditorConfiguration";

    private static ApplicationContext applicationContext;

    /** The properties. */
    private Properties properties;

    private PropertyPlaceholderHelper placeHolderHelper;

    /** The locations. */
    private Resource[] locations;

    public static CkeditorConfigurer getInstance() {
        return (CkeditorConfigurer) applicationContext.getBean(ID_BEAN);
    }

    public static String getProperty(String key) {
        String message = getInstance().getProperties().getProperty(key);
        if (StringUtils.isNotEmpty(message)) {
            // le helper permet de substituer la valeur d'une propriété ${key} par la valeur de 'key'
            return getInstance().getPlaceHolderHelper().replacePlaceholders(message, getInstance().getProperties());
        }
        return null;
    }

    public Properties getProperties() {
        return properties;
    }

    public Resource[] getLocations() {
        return locations;
    }

    /*
     * Surcharge de la méthode
     * @see org.springframework.core.io.support.PropertiesLoaderSupport#setLocations(org.springframework.core.io.Resource[])
     */
    @Override
    public void setLocations(Resource... locs) {
        super.setLocations(locs);
        locations = locs;
    }

    /*
     * Surcharge de la méthode pour stocker la liste finale des properties chargées dans l'ordre des fichiers #locations
     * @see org.springframework.beans.factory.config.PropertyPlaceholderConfigurer#processProperties(org.springframework.beans.factory.config.ConfigurableListableBeanFactory, java.util.Properties)
     */
    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) throws BeansException {
        placeHolderHelper = new PropertyPlaceholderHelper(placeholderPrefix, placeholderSuffix, valueSeparator, ignoreUnresolvablePlaceholders);
        super.processProperties(beanFactory, props);
        properties = props;
    }

    /*
     * Cette méthode permet de recharger les properties à partir de la liste des fichiers #locations,
     * liste qui pourrait être mise à jour avec un module de surcharge par ex
     */
    public synchronized void reload() throws IOException {
        properties = mergeProperties();
    }

    public PropertyPlaceholderHelper getPlaceHolderHelper() {
        return placeHolderHelper;
    }

    @Override
    public void setApplicationContext(ApplicationContext appCtx) throws BeansException {
        applicationContext = appCtx;
    }
}
