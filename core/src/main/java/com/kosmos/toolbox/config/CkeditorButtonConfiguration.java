package com.kosmos.toolbox.config;

/**
 * Created by on 10/08/15.
 */
public class CkeditorButtonConfiguration extends AbstractCkeditorObjectConfiguration {

    private String toolbarName;

    public String getToolbarName() {
        return toolbarName;
    }

    public void setToolbarName(final String toolbarName) {
        this.toolbarName = toolbarName;
    }
}
