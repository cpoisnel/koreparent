package com.kosmos.toolbox.config;

import java.io.IOException;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.extension.ExtensionHelper;

public class CkeditorConfigurerManager {

    public static synchronized void reload() throws IOException {
        // on boucle sur toutes les extensions chargees
        for (final String contextName : ExtensionHelper.getExtensionManager().getExtensions().keySet()) {
            for (final CkeditorConfigurer loader : ApplicationContextManager.getBeansOfType(contextName, CkeditorConfigurer.class).values()) {
                loader.reload();
            }
        }
    }

    public static synchronized void reloadByContext(final String contextName) throws IOException {
        // on verifie que l'extension est bien chargee
        if (ExtensionHelper.getExtensionManager().getExtension(contextName) != null) {
            for (final CkeditorConfigurer loader : ApplicationContextManager.getBeansOfType(contextName, CkeditorConfigurer.class).values()) {
                loader.reload();
            }
        }
    }
}
