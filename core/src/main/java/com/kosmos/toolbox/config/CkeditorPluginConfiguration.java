package com.kosmos.toolbox.config;

import java.util.List;

import com.kportal.core.autorisation.Permission;

/**
 * Created on 10/08/15.
 */
public class CkeditorPluginConfiguration {

    private List<CkeditorToolbarConfiguration> confToolbar;

    private List<CkeditorButtonConfiguration> confButton;

    private String path;

    private String extraPlugin;

    private List<Permission> permissions;

    public List<CkeditorToolbarConfiguration> getConfToolbar() {
        return confToolbar;
    }

    public void setConfToolbar(final List<CkeditorToolbarConfiguration> confToolbar) {
        this.confToolbar = confToolbar;
    }

    public List<CkeditorButtonConfiguration> getConfButton() {
        return confButton;
    }

    public void setConfButton(final List<CkeditorButtonConfiguration> confButton) {
        this.confButton = confButton;
    }

    public String getPath() {
        return path;
    }

    public void setPath(final String path) {
        this.path = path;
    }

    public String getExtraPlugin() {
        return extraPlugin;
    }

    public void setExtraPlugin(final String extraPlugin) {
        this.extraPlugin = extraPlugin;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(final List<Permission> permissions) {
        this.permissions = permissions;
    }
}
