package com.kosmos.toolbox.config;

/**
 * Created on 10/08/15.
 */
public abstract class AbstractCkeditorObjectConfiguration {

    private String name;

    private String insertBefore;

    private String insertAfter;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getInsertBefore() {
        return insertBefore;
    }

    public void setInsertBefore(final String insertBefore) {
        this.insertBefore = insertBefore;
    }

    public String getInsertAfter() {
        return insertAfter;
    }

    public void setInsertAfter(final String insertAfter) {
        this.insertAfter = insertAfter;
    }
}
