package com.kosmos.toolbox.bean;

/**
 * Created by olivier.camon on 24/06/15.
 */
public class TagValue {

    private String value;

    private String tagTypeId;

    private boolean identified;

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public String getTagTypeId() {
        return tagTypeId;
    }

    public void setTagTypeId(final String tagTypeId) {
        this.tagTypeId = tagTypeId;
    }

    public boolean isIdentified() {
        return identified;
    }

    public void setIdentified(final boolean identified) {
        this.identified = identified;
    }
}
