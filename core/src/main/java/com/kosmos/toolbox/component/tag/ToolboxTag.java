package com.kosmos.toolbox.component.tag;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.components.tags.AbstractComponentTag;
import com.kosmos.toolbox.config.CkeditorConfigurerUtils;
import com.kosmos.toolbox.service.CkeditorConfigurationService;
import com.kosmos.toolbox.component.view.model.ToolboxViewModel;
import com.kosmos.toolbox.utils.ToolboxHelper;
import com.kportal.core.config.MessageHelper;
import com.univ.utils.ContexteUtil;

/**
 * Created on 26/03/15.
 * This class is part of the tag lib "http://kportal.kosmos.fr/tags/components".
 */
public class ToolboxTag extends AbstractComponentTag<ToolboxViewModel> {

    private static final long serialVersionUID = -6238061552717520115L;

    private final transient CkeditorConfigurationService ckeditorConfigurationService;

    protected int min;

    protected int max;

    /**
     * Instanciate a new ToolboxTag with a default path setted and a ckedtorConfig initialized
     */
    public ToolboxTag() {
        path = "/adminsite/components/toolbox/toolbox.jsp";
        ckeditorConfigurationService = ApplicationContextManager.getCoreContextBean(CkeditorConfigurationService.ID_BEAN, CkeditorConfigurationService.class);
    }
    public void setMin(final int min) {
        this.min = min;
    }

    public void setMax(final int max) {
        this.max = max;
    }


    @Override
    public int doStartTag() throws JspException {
        if (StringUtils.isBlank(extension)) {
            extension = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
        }
        pageContext.getRequest().setAttribute(VIEW_MODEL, buildModel());
        return super.doStartTag();
    }

    @Override
    protected ToolboxViewModel buildModel() {
        final FormateurJSP fmt = getFormateur();
        final String rawConfKey = CkeditorConfigurerUtils.buildConfKey(retrieveInfoBean(), fieldName);
        final String userCode = ContexteUtil.getContexteUniv().getCode();
        final ToolboxViewModel model = new ToolboxViewModel();
        final String name = StringUtils.defaultIfBlank(MessageHelper.getExtensionMessage(extension, label), label);
        final String helpMessage = StringUtils.defaultIfBlank(MessageHelper.getExtensionMessage(extension, tooltip), tooltip);
        int currentIndex = fmt.getIndiceChamp();
        final String formatValue = String.format("%d;%d;%d;%d;APPLET_HTML=1,CODE_HTML=1,LIB=%s;%d", editOption, FormateurJSP.FORMAT_TEXTE_CACHE, min, max, name, currentIndex++);
        fmt.setIndiceChamp(currentIndex);
        model.setConfKey(ckeditorConfigurationService.getWeakConfigurationKey(userCode, extension, rawConfKey));
        model.setName(fieldName);
        model.setLabel(name);
        model.setTooltip(helpMessage);
        model.setValue(StringUtils.defaultString(ToolboxHelper.prepareForBo(retrieveInfoBean(), retrieveValue(fieldName))));
        model.setFormatValue(formatValue);
        model.setDisabled(editOption == FormateurJSP.SAISIE_AFFICHAGE);
        return model;
    }

    private FormateurJSP getFormateur() {
        return (FormateurJSP) pageContext.getAttribute("fmt");
    }

    private String retrieveValue(String fieldKey) {
        final InfoBean infoBean = retrieveInfoBean();
        return infoBean.get(fieldKey, String.class);
    }

    @Override
    public void doFinally() {
        super.doFinally();
        label = null;
        fieldName = null;
    }
}
