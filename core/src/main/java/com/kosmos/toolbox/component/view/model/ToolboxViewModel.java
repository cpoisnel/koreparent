package com.kosmos.toolbox.component.view.model;

import java.util.UUID;

import com.kosmos.components.view.model.ComponentViewModel;

/**
 * Created on 26/03/15.
 */
public class ToolboxViewModel extends ComponentViewModel {

    private static final long serialVersionUID = 6220936116575723351L;

    private UUID confKey;

    private String value;

    private String formatValue;

    private boolean disabled;


    public UUID getConfKey() {
        return confKey;
    }

    public void setConfKey(final UUID confKey) {
        this.confKey = confKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public String getFormatValue() {
        return formatValue;
    }

    public void setFormatValue(final String formatValue) {
        this.formatValue = formatValue;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(final boolean disabled) {
        this.disabled = disabled;
    }

}
