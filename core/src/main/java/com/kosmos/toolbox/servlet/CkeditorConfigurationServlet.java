package com.kosmos.toolbox.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.toolbox.config.CkeditorButtonConfiguration;
import com.kosmos.toolbox.config.CkeditorPluginConfiguration;
import com.kosmos.toolbox.config.CkeditorToolbarConfiguration;
import com.kosmos.toolbox.service.CkeditorConfigurationService;
import com.kosmos.toolbox.utils.ToolboxHelper;
import com.kportal.core.autorisation.Permission;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.utils.SessionUtil;

/**
 * Created on 10/05/2015.
 */
public class CkeditorConfigurationServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(CkeditorConfigurationServlet.class);

    private static final String PARAM_CONF_WEAK_KEY = "conf_keys[]";

    private static final long serialVersionUID = 7880449280824325116L;

    private final transient CkeditorConfigurationService ckeditorConfigurationService;

    private final transient ObjectMapper mapper;

    public CkeditorConfigurationServlet() {
        mapper = new ObjectMapper();
        ckeditorConfigurationService = ApplicationContextManager.getCoreContextBean(CkeditorConfigurationService.ID_BEAN, CkeditorConfigurationService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        try {
            if (SessionUtil.getInfosSession(req).get(SessionUtilisateur.CODE) == null || autorisations == null) {
                resp.sendError(HttpServletResponse.SC_FORBIDDEN);
            } else {
                generateResponse(req, resp, autorisations);
            }
        } catch (IOException e) {
            LOG.error("unable to write the response", e);
        }
    }

    private void generateResponse(final HttpServletRequest req, final HttpServletResponse resp, final AutorisationBean autorisations) throws IOException {
        final String userCode = (String) SessionUtil.getInfosSession(req).get(SessionUtilisateur.CODE);
        final String[] confKeys = req.getParameterMap().get(PARAM_CONF_WEAK_KEY);
        final Collection<String> confs = new ArrayList<>();
        final StringBuilder result = new StringBuilder("{\r\n");
        if (confKeys != null) {
            for (String currentConfKey : confKeys) {
                final UUID weakConfKey = UUID.fromString(currentConfKey);
                final String confPath = ckeditorConfigurationService.getConfPathFromWeakKey(userCode, weakConfKey);
                final Map<String, Object> currentConfiguration = getMapConf(confPath);
                handlePluginsConfigurations(autorisations, currentConfiguration);
                confs.add(String.format("\"%s\":%s", currentConfKey, mapper.writeValueAsString(currentConfiguration)));
            }
        }
        result.append(StringUtils.join(confs, ","));
        result.append("\r\n}");
        if (StringUtils.isNotBlank(result)) {
            resp.getWriter().write(result.toString());
            resp.setContentType("application/json");
            resp.setStatus(HttpServletResponse.SC_OK);
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private void handlePluginsConfigurations(final AutorisationBean autorisations, final Map<String, Object> currentConfiguration) {
        final Map<String, CkeditorPluginConfiguration> configurations = ApplicationContextManager.getAllBeansOfType(CkeditorPluginConfiguration.class);
        for (CkeditorPluginConfiguration currentPluginConfiguration : configurations.values()) {
            if (hasAuthorization(autorisations, currentPluginConfiguration)) {
                ToolboxHelper.addExternalPath(currentConfiguration, currentPluginConfiguration.getExtraPlugin(), currentPluginConfiguration.getPath());
                ToolboxHelper.addPlugin(currentConfiguration, currentPluginConfiguration.getExtraPlugin());
                if (CollectionUtils.isNotEmpty(currentPluginConfiguration.getConfToolbar())) {
                    addToolbars(currentConfiguration, currentPluginConfiguration.getConfToolbar());
                }
                if (CollectionUtils.isNotEmpty(currentPluginConfiguration.getConfButton())) {
                    addButtons(currentConfiguration, currentPluginConfiguration.getConfButton());
                }
            }
        }
    }

    private void addToolbars(final Map<String, Object> conf, List<CkeditorToolbarConfiguration> toolbarOptions) {
        for (CkeditorToolbarConfiguration currentToolbarConf : toolbarOptions) {
            if (StringUtils.isNotBlank(currentToolbarConf.getInsertBefore())) {
                ToolboxHelper.addToolbarBefore(conf, currentToolbarConf.getName(), currentToolbarConf.getInsertBefore());
            } else if (StringUtils.isNotBlank(currentToolbarConf.getInsertAfter())) {
                ToolboxHelper.addToolbarAfter(conf, currentToolbarConf.getName(), currentToolbarConf.getInsertAfter());
            } else {
                ToolboxHelper.addToolbar(conf, currentToolbarConf.getName());
            }
        }
    }

    private void addButtons(final Map<String, Object> conf, List<CkeditorButtonConfiguration> buttonOptions) {
        for (CkeditorButtonConfiguration currentButtonConf : buttonOptions) {
            if (StringUtils.isNotBlank(currentButtonConf.getToolbarName())) {
                if (StringUtils.isNotBlank(currentButtonConf.getInsertBefore())) {
                    ToolboxHelper.addToolbarItemBefore(conf, currentButtonConf.getToolbarName(), currentButtonConf.getName(), currentButtonConf.getInsertBefore());
                } else if (StringUtils.isNotBlank(currentButtonConf.getInsertAfter())) {
                    ToolboxHelper.addToolbarItemAfter(conf, currentButtonConf.getToolbarName(), currentButtonConf.getName(), currentButtonConf.getInsertAfter());
                } else {
                    ToolboxHelper.addToolbarItem(conf, currentButtonConf.getToolbarName(), currentButtonConf.getName());
                }
            }
        }
    }

    private boolean isAllowed(AutorisationBean autorisations, Permission permission) {
        boolean isAllowed = true;
        final Collection<PermissionBean> permissions = PermissionUtil.getPermissionsBeans(permission);
        for (PermissionBean currentPermissionBean : permissions) {
            if ("TECH".equals(permission.getId())) {
                isAllowed &= autorisations.possedePermissionTech(permission.getCode());
            } else {
                isAllowed &= autorisations.possedePermission(currentPermissionBean);
            }
        }
        return isAllowed;
    }

    private boolean hasAuthorization(AutorisationBean autorisations, CkeditorPluginConfiguration pluginConfiguration) {
        boolean isAllowed = true;
        if (CollectionUtils.isNotEmpty(pluginConfiguration.getPermissions())) {
            for (Permission currentPermission : pluginConfiguration.getPermissions()) {
                isAllowed &= isAllowed(autorisations, currentPermission);
            }
        }
        return isAllowed;
    }

    private Map<String, Object> getMapConf(String confPath) throws IOException {
        final File confFile = new File(String.format("%s/%s", WebAppUtil.getAbsolutePath(), confPath));
        if (confFile.exists()) {
            final JavaType type = mapper.getTypeFactory().constructMapType(HashMap.class, String.class, Object.class);
            return mapper.readValue(confFile, type);
        } else {
            throw new FileNotFoundException(String.format("Le fichier de configuration pour Ckeditor n'a pas été trouvé [%s]", confPath));
        }
    }
}
