package com.kosmos.toolbox.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.toolbox.bean.TagType;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;
import com.kportal.extension.module.AbstractBeanManager;
import com.kportal.extension.module.IModule;

/**
 * Created by olivier.camon on 24/06/15.
 */
public class TagTypeManager extends AbstractBeanManager {

    private Map<String, Collection<TagType>> tagByExtension = new HashMap<>();

    private Map<String, List<TagType>> tagTypes = new HashMap<>();

    public Map<String, List<TagType>> getTagTypes() {
        return tagTypes;
    }

    public List<TagType> getTagTypesByCategory(String category) {
        List<TagType> tagTypesForCategory = tagTypes.get(StringUtils.defaultString(category));
        if (tagTypesForCategory == null) {
            tagTypesForCategory = Collections.emptyList();
        }
        return tagTypesForCategory;
    }

    public Map<String, List<TagType>> getTagTypesByCategories(String... categories) {
        Map<String, List<TagType>> result = new HashMap<>();
        for (String category : categories) {
            List<TagType> tagTypesForCategory = tagTypes.get(StringUtils.defaultString(category));
            if (tagTypesForCategory == null) {
                tagTypesForCategory = Collections.emptyList();
            }
            result.put(category, tagTypesForCategory);
        }
        return result;
    }

    @Override
    public void refresh() {
        tagTypes.clear();
        tagByExtension = ApplicationContextManager.getAllBeansOfTypeByExtension(TagType.class);
        for (Map.Entry<String, Collection<TagType>> tagsByExtension : tagByExtension.entrySet()) {
            IExtension currentExtension = ExtensionHelper.getExtension(tagsByExtension.getKey());
            if (currentExtension != null && currentExtension.getEtat() == IModule.ETAT_ACTIF) {
                for (TagType currentTagType : tagsByExtension.getValue()) {
                    if(!tagTypes.containsKey(currentTagType.getCategory())) {
                        tagTypes.put(currentTagType.getCategory(), new ArrayList<TagType>());
                    }
                    tagTypes.get(currentTagType.getCategory()).add(currentTagType);
                }
            }
        }
    }

    public String getExtensionIdForCurrentTag(TagType tagType) {
        for (Map.Entry<String, Collection<TagType>> tagForExtension: tagByExtension.entrySet()) {
            if (tagForExtension.getValue().contains(tagType)) {
                return tagForExtension.getKey();
            }
        }
        return StringUtils.EMPTY;
    }

    public TagType getTagTypeById(String id) {
        if(StringUtils.isNotBlank(id)) {
            for (List<TagType> currentList : tagTypes.values()) {
                for(TagType currentTagType : currentList) {
                    if(id.equals(currentTagType.getId())) {
                        return currentTagType;
                    }
                }
            }
        }
        return null;
    }
}
