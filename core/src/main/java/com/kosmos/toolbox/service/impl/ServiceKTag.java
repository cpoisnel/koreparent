package com.kosmos.toolbox.service.impl;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.service.ServiceTagTypeToolbox;
import com.kportal.extension.module.ModuleHelper;
import com.univ.utils.EscapeString;

/**
 * Created by olivier.camon on 30/06/15.
 */
public class ServiceKTag extends ServiceTagTypeToolbox {

    public static final String TYPE_TAG = "tag";

    public static final String TAG_VALUE = "tagValue";

    public static final String TAG_TYPE = "TAG_TYPE";

    public static final String PLACE_HOLDER_PATTERN = "(.*)#\\([A-Za-z0-9\\.\\-_]*\\)(.*)";
    @Override
    public String generateUrlTag(InfoBean infoBean) {
        infoBean.set("TOOLBOX", "LIEN_REQUETE");
        infoBean.set("FCK_PLUGIN", "true");
        infoBean.set("LANGUE_FICHE", "0");
        final String value = EscapeString.unescapeURL(StringUtils.defaultString(infoBean.get("FORMATTER_VALUE", String.class)));
        final TagValue listValue = new TagValue();
        listValue.setValue(value);
        identifyTag(listValue, TYPE_TAG);
        infoBean.set(TAG_VALUE, listValue.getValue());
        infoBean.set(TAG_TYPE, getTagViewDescriptors(listValue, TYPE_TAG));
        return ModuleHelper.getRelativePathExtension(this) + getPath();
    }

    @Override
    protected Pattern getPatternFromFormatter(String formatter) {
        Pattern resultPattern;
        if (formatter.matches(PLACE_HOLDER_PATTERN)) {
            resultPattern = super.getPatternFromFormatter(formatter);
        } else {
            resultPattern = Pattern.compile(formatter);
        }
        return resultPattern;
    }
}
