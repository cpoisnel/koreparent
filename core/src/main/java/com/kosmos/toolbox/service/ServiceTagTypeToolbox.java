package com.kosmos.toolbox.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.toolbox.bean.TagType;
import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.manager.TagTypeManager;
import com.kosmos.toolbox.model.TagViewDescriptor;
import com.kosmos.toolbox.provider.TagDataProvider;
import com.kosmos.toolbox.provider.manager.TagDataManager;
import com.kportal.core.autorisation.ActionPermission;
import com.kportal.core.autorisation.Permission;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.ExtensionHelper;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.json.CodecJSon;

/**
 * Created by olivier.camon on 24/06/15.
 */
public abstract class ServiceTagTypeToolbox extends ServiceTagToolbox {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceTagTypeToolbox.class);

    protected static final Pattern PLACEHOLDERS_PATTERN = Pattern.compile("(#|~)\\(([A-Za-z0-9\\-_]+)\\)");

    private TagDataManager tagDataManager;

    private TagTypeManager tagTypeManager;

    private String defaultTagTypeId;


    public void setTagDataManager(TagDataManager tagDataManager) {
        this.tagDataManager = tagDataManager;
    }

    public TagDataManager getTagDataManager() {
        return tagDataManager;
    }

    public void setDefaultTagTypeId(final String defaultTagTypeId) {
        this.defaultTagTypeId = defaultTagTypeId;
    }

    public String getDefaultTagTypeId() {
        return defaultTagTypeId;
    }

    public void setTagTypeManager(final TagTypeManager tagTypeManager) {
        this.tagTypeManager = tagTypeManager;
    }

    public TagTypeManager getTagTypeManager() {
        return tagTypeManager;
    }

    public boolean canHandle(TagType tagType, String value) {
        Pattern pattern;
        if(StringUtils.isNotBlank(tagType.getValidationPattern())) {
            pattern = Pattern.compile(tagType.getValidationPattern(), Pattern.DOTALL);
        } else {
            pattern = getPatternFromFormatter(tagType.getTagPattern());
        }
        return StringUtils.isNotBlank(value) && pattern.matcher(value).matches();
    }

    protected Pattern getPatternFromFormatter(String formatter) {
        String stringPattern = String.format("\\Q%s\\E", formatter.replaceAll("(#|~)\\([A-Za-z0-9\\.\\-_]*\\)", "\\\\E(.*)\\\\Q"));
        stringPattern = stringPattern.replace("\\Q\\E", StringUtils.EMPTY);
        return Pattern.compile(stringPattern, Pattern.DOTALL);
    }

    /**
     * Retrieves all {@link TagType} availables for a given category.
     * @return a {@link Map} of {@link String} and {@link List} of {@link TagType}
     */
    public List<TagType> getAvailableTagTypes(final String category) {
        final AutorisationBean autorisationBean = ContexteUtil.getContexteUniv().getAutorisation();
        final List<TagType> tagTypes = tagTypeManager.getTagTypesByCategory(category);
        final List<TagType> availableTagTypes = new ArrayList<>();
        for (TagType currentTagType : tagTypes) {
            if (isAuthorized(currentTagType.getPermissions(), autorisationBean)) {
                availableTagTypes.add(currentTagType);
            }
        }
        return availableTagTypes;
    }

    protected void identifyTag(TagValue tagValue, String category) {
        int maxRegexLength = 0;
        for (TagType tagType : getAvailableTagTypes(category)) {
            if(StringUtils.isNotBlank(tagType.getTagPattern()) &&
                tagType.getTagPattern().length() > maxRegexLength &&
                canHandle(tagType, tagValue.getValue())) {
                maxRegexLength = tagType.getTagPattern().length();
                tagValue.setTagTypeId(tagType.getId());
                tagValue.setIdentified(true);
            }
        }
    }

    protected boolean isAuthorized(Collection<Permission> permissions, AutorisationBean autorisationBean) {
        if (CollectionUtils.isEmpty(permissions)) {
            return true;
        }
        boolean isAllowed = true;
        for (Permission currentPermission : permissions) {
            if(CollectionUtils.isNotEmpty(currentPermission.actions)) {
                for (ActionPermission currentAction : currentPermission.actions) {
                    isAllowed &= autorisationBean.possedePermission(PermissionUtil.getPermissionBean(currentPermission, currentAction));
                }
            } else {
                isAllowed &= autorisationBean.possedePermissionTech(currentPermission.getCode());
            }
        }
        return isAllowed;
    }

    protected Map<String, String> computeDataCore(TagType tagType) {
        final Map<String, String> data = new HashMap<>();
        if(tagType != null) {
            final Matcher matcher = PLACEHOLDERS_PATTERN.matcher(tagType.getTagPattern());
            while (matcher.find()) {
                data.put(matcher.group(2), StringUtils.EMPTY);
            }
        }
        return data;
    }


    protected Map<String, String> generateData(TagValue tagValue) {
        final TagType tagType = tagTypeManager.getTagTypeById(tagValue.getTagTypeId());
        final Map<String, String> data = computeDataCore(tagType);
        if(StringUtils.isNotBlank(tagValue.getValue())) {
            final Matcher matcher = PLACEHOLDERS_PATTERN.matcher(tagType.getTagPattern());
            final Pattern formatterPattern = getPatternFromFormatter(tagType.getTagPattern());
            final Matcher formatterMatcher = formatterPattern.matcher(tagValue.getValue());
            final boolean found = formatterMatcher.find();
            int groupIndex = 1;
            while (matcher.find()) {
                try {
                    if (found && StringUtils.isNotBlank(formatterMatcher.group(groupIndex))) {
                        data.put(matcher.group(2), formatterMatcher.group(groupIndex));
                    }
                    groupIndex++;
                } catch (IllegalStateException e) {
                    LOG.debug("unable to found data", e);
                    data.put(matcher.group(2), StringUtils.EMPTY);
                }
            }
        }
        final TagDataProvider builder = tagDataManager.getForId(tagType.getId());
        if (builder != null) {
            builder.populateData(data, tagValue);
        }
        return data;
    }


    private boolean hasSelected(List<TagViewDescriptor> models) {
        for (TagViewDescriptor currentModel : models) {
            if (currentModel.isSelected()) {
                return true;
            }
        }
        return false;
    }

    protected List<TagViewDescriptor> getTagViewDescriptors(TagValue tagValue, String category) {
        final List<TagViewDescriptor> viewDescriptors = new ArrayList<>();
        for (TagType currentEntry : getAvailableTagTypes(category)) {
            viewDescriptors.add(getTagViewDescriptor(currentEntry, tagValue));
        }
        // Alphabetical sorting
        Collections.sort(viewDescriptors, new Comparator<TagViewDescriptor>() {

            @Override
            public int compare(final TagViewDescriptor t0, final TagViewDescriptor t1) {
                return t0.getLabel().compareTo(t1.getLabel());
            }
        });
        if (!hasSelected(viewDescriptors)) {
            selectDefault(viewDescriptors);
        }
        return viewDescriptors;
    }

    private TagViewDescriptor getTagViewDescriptor(TagType tagType, TagValue tagValue) {
        final TagViewDescriptor viewDescriptor = new TagViewDescriptor();
        viewDescriptor.setFormatterPattern(tagType.getTagPattern());
        viewDescriptor.setId(tagType.getId());
        final String extensionId = tagTypeManager.getExtensionIdForCurrentTag(tagType);
        if (StringUtils.isNotBlank(extensionId)) {
            final String relativePath = ExtensionHelper.getExtension(extensionId).getRelativePath();
            viewDescriptor.setInputFragment(StringUtils.defaultString(relativePath) + tagType.getInputFragment());
            viewDescriptor.setLabel(MessageHelper.getMessage(extensionId, tagType.getLabelKey()));
            if (StringUtils.isNotBlank(tagType.getJsScript())) {
                viewDescriptor.setJsScript(StringUtils.defaultString(relativePath) + tagType.getJsScript());
            }
            if(StringUtils.isNotBlank(tagType.getIcons().getLeft()) && StringUtils.isNotBlank(tagType.getIcons().getRight())) {
                final Pair<String, String> icons = new ImmutablePair<>(StringUtils.defaultString(relativePath) + tagType.getIcons().getLeft(), StringUtils.defaultString(relativePath) + tagType.getIcons().getRight());
                viewDescriptor.setIcons(icons);
            }
        } else {
            viewDescriptor.setInputFragment(tagType.getInputFragment());
            viewDescriptor.setLabel(MessageHelper.getCoreMessage(tagType.getLabelKey()));
            viewDescriptor.setJsScript(tagType.getJsScript());
        }
        viewDescriptor.setValidationPattern(tagType.getValidationPattern());
        if (tagType.getId().equals(tagValue.getTagTypeId())) {
            viewDescriptor.setData(generateData(tagValue));
            viewDescriptor.setSelected(true);
        } else {
            viewDescriptor.setData(computeDataCore(tagType));
        }
        try {
            if(MapUtils.isNotEmpty(tagType.getHtmlAttributes())) {
                viewDescriptor.setHtmlAttributes(EscapeString.escapeAttributHtml(CodecJSon.encodeObjectToJSonInString(tagType.getHtmlAttributes())));
            } else {
                viewDescriptor.setHtmlAttributes(StringUtils.EMPTY);
            }
        } catch (IOException e) {
            LOG.debug("unable to encode the html attributes", e);
            viewDescriptor.setHtmlAttributes(StringUtils.EMPTY);
        }
        return viewDescriptor;
    }

    private void selectDefault(List<TagViewDescriptor> descriptor) {
        for (TagViewDescriptor currentModel : descriptor) {
            if (currentModel.getId().equals(getDefaultTagTypeId())) {
                currentModel.setSelected(true);
                return;
            }
        }
    }
}
