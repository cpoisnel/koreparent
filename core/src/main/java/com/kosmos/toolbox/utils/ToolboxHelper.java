package com.kosmos.toolbox.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.univ.utils.ContexteUtil;
import com.univ.utils.UnivWebFmt;

/**
 * Created on 09/07/15.
 */
public class ToolboxHelper {

    private static final Logger LOG = LoggerFactory.getLogger(ToolboxHelper.class);

    private static final String TOOLBAR_KEY = "toolbar";

    private static final String EXTRA_PLUGINS_KEY = "extraPlugins";

    private static final String EXTERNAL_PATHS_KEY = "externalPaths";

    public static final String BUTTON_SEPARATOR = "-";

    public static final String TOOLBAR_SEPARATOR = "/";

    private static List<String> getToolbarConfiguration(Map<String, Object> config, String name) {
        final List<Object> toolbars = (List<Object>) config.get(TOOLBAR_KEY);
        for (Object currentObject : toolbars) {
            if (currentObject instanceof LinkedHashMap) {
                final LinkedHashMap<String, ArrayList<String>> currentMap = (LinkedHashMap<String, ArrayList<String>>) currentObject;
                if (currentMap.containsKey("name") && name.equals(currentMap.get("name"))) {
                    return currentMap.get("items");
                }
            }
        }
        return new ArrayList<>();
    }

    public static void addToolbar(Map<String, Object> config, String toolbarName) {
        final List<Object> toolbarsConf = (List<Object>) config.get(TOOLBAR_KEY);
        final LinkedHashMap<String, Object> toolbar = new LinkedHashMap<>();
        toolbar.put("name", toolbarName);
        toolbar.put("items", new ArrayList<String>());
        toolbarsConf.add(toolbar);
    }

    public static void addToolbarBefore(Map<String, Object> config, String toolbarName, String itemBeforeName) {
        final List<Object> toolbarsConf = (List<Object>) config.get(TOOLBAR_KEY);
        final LinkedHashMap<String, Object> toolbar = new LinkedHashMap<>();
        boolean added = false;
        toolbar.put("name", toolbarName);
        toolbar.put("items", new ArrayList<String>());
        for (Object currentObject : toolbarsConf) {
            if (currentObject instanceof LinkedHashMap) {
                final LinkedHashMap<String, Object> currentToolbar = (LinkedHashMap<String, Object>) currentObject;
                if (itemBeforeName.equals(currentToolbar.get("name"))) {
                    toolbarsConf.add(toolbarsConf.indexOf(currentToolbar), toolbar);
                    added = true;
                    break;
                }
            }
        }
        if (!added) {
            toolbarsConf.add(toolbar);
        }
    }

    public static void addToolbarAfter(Map<String, Object> config, String toolbarName, String itemAfterName) {
        final List<Object> toolbarsConf = (List<Object>) config.get(TOOLBAR_KEY);
        final LinkedHashMap<String, Object> toolbar = new LinkedHashMap<>();
        boolean added = false;
        toolbar.put("name", toolbarName);
        toolbar.put("items", new ArrayList<String>());
        for (Object currentObject : toolbarsConf) {
            if (currentObject instanceof LinkedHashMap) {
                final LinkedHashMap<String, Object> currentToolbar = (LinkedHashMap<String, Object>) currentObject;
                if (itemAfterName.equals(currentToolbar.get("name"))) {
                    toolbarsConf.add(toolbarsConf.indexOf(currentToolbar) + 1, toolbar);
                    added = true;
                    break;
                }
            }
        }
        if (!added) {
            toolbarsConf.add(toolbar);
        }
    }

    public static void addToolbarItem(Map<String, Object> config, String toolbarName, String itemName) {
        final List<String> toolbarsConf = getToolbarConfiguration(config, toolbarName);
        if (toolbarsConf != null && (!toolbarsConf.contains(itemName) || BUTTON_SEPARATOR.equals(itemName))) {
            toolbarsConf.add(itemName);
        }
    }

    public static void addToolbarItemBefore(Map<String, Object> config, String toolbarName, String itemName, String itemBeforeName) {
        final List<String> toolbarsConf = getToolbarConfiguration(config, toolbarName);
        if (toolbarsConf != null && (!toolbarsConf.contains(itemName) || BUTTON_SEPARATOR.equals(itemName))) {
            final int index = toolbarsConf.indexOf(itemBeforeName);
            if (index >= 0) {
                toolbarsConf.add(index, itemName);
            } else {
                toolbarsConf.add(itemName);
            }
        }
    }

    public static void addToolbarItemAfter(Map<String, Object> config, String toolbarName, String itemName, String itemAfterName) {
        final List<String> toolbarsConf = getToolbarConfiguration(config, toolbarName);
        if (toolbarsConf != null  && (!toolbarsConf.contains(itemName) || BUTTON_SEPARATOR.equals(itemName))) {
            int index = toolbarsConf.indexOf(itemAfterName);
            index = index == -1 ? index : index + 1;
            if (index >= 0) {
                toolbarsConf.add(index, itemName);
            } else {
                toolbarsConf.add(itemName);
            }
        }
    }

    /**
     * Add a plugin name to the "extraPlugins" key of the configuration object. If the plugin was already added, nothing happens.
     * @param config : the CkEditor configuration as a {@link java.util.Map} of {@link String}, {@link String}
     * @param pluginName : the name of the plugin to add as a {@link String}
     */
    public static void addPlugin(Map<String, Object> config, String pluginName) {
        final String extraPlugins = (String) config.get(EXTRA_PLUGINS_KEY);
        if (StringUtils.isNotBlank(extraPlugins)) {
            final List<String> extraPluginsList = new ArrayList<>(Arrays.asList(extraPlugins.split(",")));
            if (CollectionUtils.isNotEmpty(extraPluginsList) && !extraPluginsList.contains(pluginName)) {
                extraPluginsList.add(pluginName);
            }
            config.put(EXTRA_PLUGINS_KEY, StringUtils.join(extraPluginsList, ","));
        }
    }

    public static void addExternalPath(Map<String, Object> config, String pluginName, String path) {
        if (StringUtils.isNotBlank(pluginName) && StringUtils.isNotBlank(path)) {
            Map<String, String> externalPaths = (Map<String, String>) config.get(EXTERNAL_PATHS_KEY);
            if (MapUtils.isEmpty(externalPaths)) {
                externalPaths = new HashMap<>();
            }
            externalPaths.put(pluginName, path);
            config.put(EXTERNAL_PATHS_KEY, externalPaths);
        }
    }

    /**
     * Remove a plugin name of the "extraPlugins" key of the configuration object. If the plugin is not part of the configuration, nothing happens.
     * @param config : the CkEditor configuration as a {@link java.util.Map} of {@link String}, {@link String}
     * @param pluginName : the name of the plugin to remove as a {@link String}
     */
    public static void removePlugin(Map<String, Object> config, String pluginName) {
        final String extraPlugins = (String) config.get(EXTRA_PLUGINS_KEY);
        if (StringUtils.isNotBlank(extraPlugins)) {
            final List<String> extraPluginsList = Arrays.asList(extraPlugins.split(","));
            if (CollectionUtils.isNotEmpty(extraPluginsList)) {
                extraPluginsList.remove(pluginName);
                config.put(EXTRA_PLUGINS_KEY, StringUtils.join(extraPluginsList, ","));
            }
        }
    }

    /**
     * Remove a plugin name of the "extraPlugins" key of the configuration object. If the plugin is not part of the configuration, nothing happens.
     * @param config : the CkEditor configuration as a {@link java.util.Map} of {@link String}, {@link String}
     * @param buttonName : the name of the button to remove as a {@link String}
     */
    public static void removeButton(Map<String, Object> config, String buttonName) {
        final List<Object> rawToolbars = (List<Object>) config.get(TOOLBAR_KEY);
        for (Object currentObject : rawToolbars) {
            if (currentObject instanceof LinkedHashMap) {
                final LinkedHashMap<String, ArrayList<String>> toolbars = (LinkedHashMap<String, ArrayList<String>>) currentObject;
                findAndRemoveButton(toolbars, buttonName);
            }
        }
    }

    private static void findAndRemoveButton(final LinkedHashMap<String, ArrayList<String>> toolbars, final String buttonName) {
        for(ArrayList<String> currentToolbar : toolbars.values()) {
            final Iterator<String> toolbarIt = currentToolbar.iterator();
            while (toolbarIt.hasNext()) {
                final String currentButton = toolbarIt.next();
                if(currentButton.equals(buttonName)) {
                    toolbarIt.remove();
                }
            }
        }
    }

    /**
     * Compute content of a toolbox according to the {@link String} passed, ready to be displayed in a BO context.
     * This method handles [id-image] tags transformations and interact with {@link com.univ.utils.LectureImageToolbox} to give
     * the user requesting medias the necessary rights.
     *
     * @param value
     *         as a {@link String}
     * @return the content formated as valid HTML or the original value if any error occured.
     */
    public static String prepareForBo(InfoBean infoBean, String value) {
        if (StringUtils.isNotBlank(value)) {
            // Securisation des images toolbox
            final HttpSession session = infoBean.getSessionHttp();
            final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
            final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
            final String sTag = "(\\[id-image\\][0-9A-Z]+\\[/id-image\\])";
            final Pattern p = Pattern.compile(sTag);
            final Matcher m = p.matcher(value);
            while (m.find()) {
                infosSession.put(m.group(1), false);
            }
            // on transforme les tags [id-image] en
            // /servlet/com.univ.utils.LectureImageToolbox?TAG=[id-image]
            final String replacePattern = "src=\"(http://)*([a-zA-Z0-9:]+)*(/servlet/com.univ.utils.LectureImageToolbox\\?TAG=)*\\[id-image\\]";
            value = StringUtils.replacePattern(value, replacePattern, "src=\"/servlet/com.univ.utils.LectureImageToolbox?TAG=[id-image]");
        }
        return value;
    }

    /**
     * Compute content of a toolbox according to the {@link String} passed.
     *
     * @param value
     *         as a {@link String}
     * @return the content formated as valid HTML or the original value if any error occured.
     */
    public static String getFormatedContent(String value) {
        String processedContent;
        try {
            processedContent = UnivWebFmt.transformerTagsPersonnalisation(ContexteUtil.getContexteUniv(), value);
            processedContent = UnivWebFmt.formaterEnHTML(ContexteUtil.getContexteUniv(), processedContent);
            return StringUtils.replacePattern(processedContent, "src=\"((http|https)\\://(.*)(\\:[0-9]+)*)?/servlet/com\\.univ\\.utils\\.LectureImageToolbox\\?TAG=", "src=\"");
        } catch (Exception e) {
            LOG.debug("unable to format the given value into html", e);
            return value;
        }
    }
}
