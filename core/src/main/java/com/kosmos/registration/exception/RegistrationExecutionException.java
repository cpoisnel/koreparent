package com.kosmos.registration.exception;

/**
 * Created by olivier.camon on 08/01/15.
 */
public class RegistrationExecutionException extends RegistrationException {

    private static final long serialVersionUID = 2875772324968192400L;

    public RegistrationExecutionException(final String message) {
        super(message);
    }

    public RegistrationExecutionException(final String message, final String extensionId) {
        super(message, extensionId);
    }

    public RegistrationExecutionException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public RegistrationExecutionException(final String message, final String extensionId, final Throwable cause) {
        super(message, extensionId, cause);
    }
}
