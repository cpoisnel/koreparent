package com.kosmos.registration.exception;

import com.jsbsoft.jtf.core.ApplicationContextManager;

/**
 * Exception permettant de gérer les Exceptions de
 */
public abstract class RegistrationException extends Exception {

    private static final long serialVersionUID = 9094858027442378841L;

    private String extensionId;

    public RegistrationException(final String message, final String extensionId) {
        super(message);
        this.extensionId = extensionId;
    }

    public RegistrationException(final String message, final String extensionId, final Throwable cause) {
        super(message, cause);
        this.extensionId = extensionId;
    }

    public RegistrationException(final String message) {
        super(message);
        this.extensionId = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
    }

    public RegistrationException(final String message, final Throwable cause) {
        super(message, cause);
        this.extensionId = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
    }

    public String getExtensionId() {
        return extensionId;
    }

    public void setExtensionId(final String extensionId) {
        this.extensionId = extensionId;
    }
}
