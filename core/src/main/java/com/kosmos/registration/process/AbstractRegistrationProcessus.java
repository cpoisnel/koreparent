package com.kosmos.registration.process;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurUtilisateurNonAuthentifie;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.registration.adapter.impl.WebRegistrationAdapter;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.exception.RegistrationException;
import com.kosmos.registration.exception.RegistrationViolationValidationException;
import com.kosmos.registration.service.RegistrationFlowService;
import com.kosmos.registration.service.impl.SimpleRegistrationFlowService;
import com.kosmos.registration.utils.RegistrationUtils;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.utils.json.NamingStrategyHelper;

/**
 * Created on 31/12/14.
 */
public abstract class AbstractRegistrationProcessus extends ProcessusBean {

    public static final String REGISTRATION_ID = "REGISTRATION_ID";

    public static final String REGISTRATION_MODEL_ID = "REGISTRATION_MODEL";

    public static final String REGISTRATION_VIEW_MODEL = "REGISTRATION_VIEW_MODEL";

    public static final String CONSTRAINT_VIOLATION_INFOBEAN_SUFFIX = ".constraint";

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRegistrationProcessus.class);

    private final RegistrationFlowService registrationFlowService;

    private final WebRegistrationAdapter webAdapter;

    public AbstractRegistrationProcessus(InfoBean infoBean) {
        super(infoBean);
        registrationFlowService = ApplicationContextManager.getCoreContextBean(SimpleRegistrationFlowService.ID_BEAN, SimpleRegistrationFlowService.class);
        webAdapter = ApplicationContextManager.getCoreContextBean(WebRegistrationAdapter.ID_BEAN, WebRegistrationAdapter.class);
    }

    private static String getErrorKey(ConstraintViolation<?> constraintViolation) {
        String errorKey = constraintViolation.getRootBeanClass().getSimpleName();
        if (StringUtils.isNotEmpty(constraintViolation.getPropertyPath().toString())) {
            errorKey += "." + constraintViolation.getPropertyPath();
        }
        errorKey += CONSTRAINT_VIOLATION_INFOBEAN_SUFFIX;
        return NamingStrategyHelper.translateUppercaseToUnderscore(errorKey);
    }

    protected abstract boolean handlePostRegistration(String action, Registration registration);

    protected abstract void handlePreRegistration(String action) throws ErreurApplicative;

    @Override
    protected boolean traiterAction() throws Exception {
        Long registrationId;
        Long modelId;
        Registration registration = null;
        try {
            handlePreRegistration(infoBean.get(InfoBean.ACTION, String.class));
            try {
                registrationId = StringUtils.isNumeric(infoBean.get(REGISTRATION_ID, String.class)) ? Long.valueOf(infoBean.getString(REGISTRATION_ID)) : null;
                modelId = StringUtils.isNumeric(infoBean.get(REGISTRATION_MODEL_ID, String.class)) ? Long.valueOf(infoBean.getString(REGISTRATION_MODEL_ID)) : null;
            } catch (IllegalArgumentException e) {
                LOG.debug("illegal argument", e);
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BEAN_VALIDATION.ERROR"));
            }
            String identity = getConnectedUserIdentity();
            if (registrationId != null || modelId != null) {
                if (registrationId == null || registrationId == 0L) {
                    registration = registrationFlowService.create(webAdapter.retrieveCreationData(infoBean.getValues(), modelId), modelId, identity);
                } else {
                    registration = registrationFlowService.update(webAdapter.retrieveUpdateData(infoBean.getValues(), registrationId), registrationId, identity);
                }
            } else {
                registration = RegistrationUtils.newRegistrationInProgress(modelId, null, identity);
            }
        } catch (RegistrationException e) {
            handleRegistrationException(e);
        } catch (ErreurApplicative e) {
            handleProcessusException(e);
        }
        return handlePostRegistration(infoBean.get(InfoBean.ACTION, String.class), registration);
    }

    protected String getConnectedUserIdentity() {
        final SessionUtilisateur userSession = getGp().getSessionUtilisateur();
        if (userSession == null) {
            return StringUtils.EMPTY;
        }
        final AutorisationBean authorization = (AutorisationBean) userSession.getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (authorization == null) {
            return StringUtils.EMPTY;
        }
        return StringUtils.defaultIfBlank(authorization.getCode(), StringUtils.EMPTY);
    }

    protected void handleRegistrationException(RegistrationException e) {
        if (e instanceof RegistrationViolationValidationException) {
            Map<String, Set<ConstraintViolation<?>>> constraintsVioldationByField = new HashMap<>();
            Set<ConstraintViolation<?>> violations = ((RegistrationViolationValidationException) e).getConstraintViolations();
            for (ConstraintViolation<?> constraintViolation : violations) {
                String errorKey = getErrorKey(constraintViolation);
                Set<ConstraintViolation<?>> constraintViolationsForCurrentField = constraintsVioldationByField.get(errorKey);
                if (constraintViolationsForCurrentField == null) {
                    constraintViolationsForCurrentField = new HashSet<>();
                }
                constraintViolationsForCurrentField.add(constraintViolation);
                constraintsVioldationByField.put(errorKey, constraintViolationsForCurrentField);
            }
            for (Map.Entry<String, Set<ConstraintViolation<?>>> constraintsByField : constraintsVioldationByField.entrySet()) {
                infoBean.set(constraintsByField.getKey(), constraintsByField.getValue());
            }
            infoBean.addMessageErreur(MessageHelper.getMessage(infoBean.getNomExtension(), e.getMessage()));
        } else {
            LOG.error("une exception s'est produite lors du traitement de la registration", e);
            infoBean.addMessageErreur(MessageHelper.getMessage(e.getExtensionId(), e.getMessage()));
        }
    }

    /**
     * Méthode permettant de gérer les exceptions lever par la registration
     * @param e l'erreur applicative à traiter.
     */
    protected void handleProcessusException(ErreurApplicative e) {
        if (e instanceof ErreurUtilisateurNonAuthentifie) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            infoBean.addMessageErreur(e.getMessage());
        }
    }
}
