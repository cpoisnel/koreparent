package com.kosmos.registration.utils;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.kosmos.registration.action.Action;
import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.bean.RegistrationState;

/**
 * Created by pierre.cosson on 19/01/15.
 */
public class RegistrationUtils {

    /**
     * Récupérer la dernière entrée de l'historique contenu dans la registration.
     *
     * @param registration la Registration dont on souhaite récupérer le dernier hsitorique
     * @return La dernière entrée d'historique de la registration OU null si l'historique est vide.
     */
    public static ActionHistory getLastActionHistory(final Registration registration) {
        List<Pair<String, ActionHistory>> actionsHistory = registration.getActionHistory();
        if (CollectionUtils.isNotEmpty(actionsHistory)) {
            return actionsHistory.get(actionsHistory.size() - 1).getValue();
        } else {
            return null;
        }
    }

    /**
     * Récupérer la dernière entrée de l'historique contenu dans la registration.
     *
     * @param registration la Registration dont on souhaite récupérer le dernier hsitorique
     * @return La dernière entrée d'historique de la registration OU null si l'historique est vide.
     */
    public static String getLastActionIdFromHistory(final Registration registration) {
        List<Pair<String, ActionHistory>> actionsHistory = registration.getActionHistory();
        if (CollectionUtils.isNotEmpty(actionsHistory)) {
            return actionsHistory.get(actionsHistory.size() - 1).getKey();
        } else {
            return null;
        }
    }

    /**
     * Récupère la dernière {@link ActionHistory} de l'action fourni en paramètre
     * @param actionsHistory les actionsHistory à filtrer
     * @param actionId l'action dont on souhaite récupérer la dernière history
     * @return l'{@link ActionHistory} de l'action fourni en paramètre
     */
    public static ActionHistory getLastActionHistoryFromActionID(List<Pair<String, ActionHistory>> actionsHistory, final String actionId) {
        CollectionUtils.filter(actionsHistory, new Predicate() {

            @Override
            public boolean evaluate(Object object) {
                return ((Pair<String, ActionHistory>) object).getKey().equals(actionId);
            }
        });
        return actionsHistory.get(actionsHistory.size() - 1).getValue();
    }

    /**
     * Initialisations d'une nouvelle Registration à
     * @param modelId l'id du model lié à la Registration
     * @param data les données par actions à setter
     * @param identity le code de l'utilisateur qui le créé
     * @return une instance de registration en état {@link RegistrationState#IN_PROGRESS}
     */
    public static Registration newRegistrationInProgress(Long modelId, Map<String, RegistrationData> data, String identity) {
        final Registration registration = new Registration();
        registration.setCreationDate(new Date());
        registration.setCreationIdentity(identity);
        registration.setModelId(modelId);
        registration.setState(RegistrationState.IN_PROGRESS);
        registration.setActionHistory(new ArrayList<Pair<String, ActionHistory>>() {});
        registration.setLastUpdateDate(new Date());
        registration.setLastUpdateIdentity(identity);
        registration.setDataByAction(data);
        return registration;
    }

    /**
     * Récupération du type utilisé par une action paramétrée au runtime.
     * @param action : l'action à partir de laquelle la recherche doit être effectuée.
     * @param clazz : la classe que l'on espère trouver.
     * @return : la classe effectivement découverte ou null.
     */
    public static <T> Class<? extends T> getActionDataType(Action action, Class<T> clazz) {
        Map<TypeVariable<?>, Type> parametrizedType = TypeUtils.getTypeArguments(action.getClass(), Action.class);
        if (MapUtils.isNotEmpty(parametrizedType)) {
            for (Type currentType : parametrizedType.values()) {
                if (TypeUtils.isAssignable(currentType, clazz)) {
                    return (Class<? extends T>) currentType;
                }
            }
        }
        return null;
    }
}
