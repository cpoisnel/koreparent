package com.kosmos.registration.utils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.registration.action.Action;
import com.kosmos.registration.bean.ModelDescriptor;
import com.kosmos.registration.exception.RegistrationModelNotFoundException;

/**
 * Classe utilitaire permettant de récupérer les {@link ModelDescriptor} déclaré dans les confs Spring de l'appli.
 */
public class ModelDescriptorUtils {

    /**
     * Permet de récupére un modelDescriptor à partir de son id de conf Spring
     *
     * @param idModelDescriptor l'id du bean Spring à récupérer
     * @return le {@link ModelDescriptor} correspondant ou null si non trouvé
     */
    public static ModelDescriptor getModelDescriptor(String idModelDescriptor) throws RegistrationModelNotFoundException {
        final ModelDescriptor modelDescriptor = ApplicationContextManager.getEveryContextBean(idModelDescriptor, ModelDescriptor.class);
        if (modelDescriptor == null) {
            throw new RegistrationModelNotFoundException("unable to find the model descriptor");
        }
        return modelDescriptor;
    }

    /**
     * Récupère l'ensemble des beans déclarés sur l'ensemble des contextes de l'application ayant pour super class
     * {@link ModelDescriptor}
     *
     * @return La liste de l'ensemble des beans ayant pour class {@link ModelDescriptor} ou une collection vide si non trouvé
     */
    public static Collection<ModelDescriptor> getAllModelDescriptor() {
        return ApplicationContextManager.getAllBeansOfType(ModelDescriptor.class).values();
    }

    /**
     * Récupérer l'ensemble des actions se trouvant aprés l'action référencée par l'ID et contenues dans l'enchainement défini par le {@link ModelDescriptor}.
     * <p/>
     * L'action référencée par l'ID ne fait par partie de la liste de retour.
     *
     * @param actionId        Identifiant de la l'action à partir de laquelle la liste est construite.
     * @param modelDescriptor Le {@link ModelDescriptor} contenant la liste des actions.
     * @return La liste des actions se situant aprés l'action référencée en paramétre.
     * Cette liste peut être vide si :
     * <ul>
     * <li>l'action référencée n'existe pas</li>
     * <li>l'action référencée est la dernière action du {@link ModelDescriptor} </li>
     * </ul>
     */
    public static Map<String, Action> getActionsAfterActionId(String actionId, ModelDescriptor modelDescriptor) {
        final Map<String, Action> allActions = modelDescriptor.getActions();
        final Map<String, Action> actionsToProcess = new LinkedHashMap<>();
        boolean actionFind = Boolean.FALSE;
        for (Map.Entry<String, Action> actionEntry : allActions.entrySet()) {
            if (actionFind) {
                actionsToProcess.put(actionEntry.getKey(), actionEntry.getValue());
            } else if (actionId.equals(actionEntry.getKey())) {
                actionFind = Boolean.TRUE;
            }
        }
        return actionsToProcess;
    }
}
