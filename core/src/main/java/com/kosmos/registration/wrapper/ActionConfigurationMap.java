package com.kosmos.registration.wrapper;

import java.util.HashMap;

import com.kosmos.registration.action.ActionConfiguration;

/**
 * Created by olivier.camon on 07/01/15.
 */
public class ActionConfigurationMap extends HashMap<String, ActionConfiguration> {

    private static final long serialVersionUID = -8754657290881169486L;
}
