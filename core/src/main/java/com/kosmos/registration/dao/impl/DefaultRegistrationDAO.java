package com.kosmos.registration.dao.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.ParametersDataSourceException;
import com.kosmos.datasource.sql.utils.SqlDateConverter;
import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.registration.dao.RegistrationDao;
import com.kosmos.registration.utils.RegistrationJacksonMapper;
import com.kosmos.registration.wrapper.RegistrationDataMap;

/**
 * DAO permettant de persister les données d'une inscription.
 */
public class DefaultRegistrationDAO extends RegistrationDao {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultRegistrationDAO.class);

    public DefaultRegistrationDAO() {
        this.tableName = "REGISTRATION";
        this.rowMapper = new RowMapper<Registration>() {

            @Override
            public Registration mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                Registration registration = new Registration();
                String registrationDataStored = StringUtils.EMPTY;
                try {
                    registration.setId(rs.getLong("ID_REGISTRATION"));
                    registration.setModelId(rs.getLong("MODEL_ID"));
                    registrationDataStored = rs.getString("REGISTRATIONS_DATA_BY_ACTION");
                    final ObjectMapper mapper = RegistrationJacksonMapper.getMapper();
                    registration.setDataByAction(mapper.<Map<String, RegistrationData>>readValue(registrationDataStored, mapper.getTypeFactory().constructMapLikeType(Map.class, String.class, RegistrationData.class)));
                    registration.setActionHistory(deserializeActionHistory(rs.getString("ACTION_HISTORY")));
                    registration.setState(RegistrationState.valueOf(rs.getString("STATE")));
                    registration.setCreationIdentity(rs.getString("CREATION_IDENTITY"));
                    registration.setCreationDate(SqlDateConverter.fromTimestamp(rs.getTimestamp("CREATION_DATE")));
                    registration.setLastUpdateIdentity(rs.getString("LAST_UPDATE_IDENTITY"));
                    registration.setLastUpdateDate(SqlDateConverter.fromTimestamp(rs.getTimestamp("LAST_UPDATE_DATE")));
                } catch (IOException e) {
                    LOG.error(String.format("Unable to map RegistrationData from the saved value [%s]", registrationDataStored), e);
                    registration = null;
                }
                return registration;
            }
        };
    }

    private List<Pair<String, ActionHistory>> deserializeActionHistory(String actionHistoryValue) throws IOException {
        final ObjectMapper mapper = RegistrationJacksonMapper.getMapper();
        final List<Pair<String, ActionHistory>> actionsHistories = new ArrayList<>();
        final Map<String, ActionHistory> tempValue = mapper.readValue(actionHistoryValue, mapper.getTypeFactory().constructMapType(LinkedHashMap.class, String.class, ActionHistory.class));
        for (Map.Entry<String, ActionHistory> currentEntry : tempValue.entrySet()) {
            actionsHistories.add(new ImmutablePair<>(currentEntry.getKey(), currentEntry.getValue()));
        }
        return actionsHistories;
    }

    protected SqlParameterSource getParameters(final Registration registration) {
        final MapSqlParameterSource params = new MapSqlParameterSource("modelId", registration.getModelId());
        try {
            params.addValue("registrationsDataByAction", serializeDataByAction(registration));
            params.addValue("actionHistory", serializeHistory(registration));
        } catch (IOException e) {
            throw new ParametersDataSourceException(String.format("Une erreur est survenue lors de la génération des paramètres pour l'objet Registration portant l'id %d", registration.getId()), e);
        }
        params.addValue("state", registration.getState().name());
        params.addValue("creationIdentity", registration.getCreationIdentity());
        params.addValue("creationDate", registration.getCreationDate());
        params.addValue("lastUpdateIdentity", registration.getLastUpdateIdentity());
        params.addValue("lastUpdateDate", registration.getLastUpdateDate());
        params.addValue("id", registration.getId());
        return params;
    }

    private String serializeDataByAction(Registration registration) throws IOException {
        final RegistrationDataMap registrationDataMap = new RegistrationDataMap();
        registrationDataMap.putAll(registration.getDataByAction());
        return RegistrationJacksonMapper.getMapper().writeValueAsString(registrationDataMap);
    }

    private String serializeHistory(Registration registration) throws IOException {
        final ActionHistories actionHistories = new ActionHistories();
        for (Pair<String, ActionHistory> currentPair : registration.getActionHistory()) {
            actionHistories.put(currentPair.getKey(), currentPair.getValue());
        }
        return RegistrationJacksonMapper.getMapper().writeValueAsString(actionHistories);
    }

    @Override
    public List<Registration> getByModel(Long modelId) {
        List<Registration> results;
        try {
            results = namedParameterJdbcTemplate.query("select * from `REGISTRATION` T1 WHERE T1.MODEL_ID = :modelId", new MapSqlParameterSource("modelId", modelId), rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException("Unable to select value from table \"REGISTRATION\"", dae);
        }
        return results;
    }

    @Override
    public List<Registration> getByModelAndState(Long modelId, RegistrationState state) {
        List<Registration> results;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("modelId", modelId);
            params.addValue("state", state.name());
            results = namedParameterJdbcTemplate.query("select * from `REGISTRATION` T1 WHERE T1.MODEL_ID = :modelId AND STATE = :state", params, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException("Unable to select value from table \"REGISTRATION\"", dae);
        }
        return results;
    }

    @Override
    public void deleteByModelId(Long modelId) {
        try {
            namedParameterJdbcTemplate.update("DELETE FROM `REGISTRATION` WHERE MODEL_ID = :modelId", new MapSqlParameterSource("modelId", modelId));
        } catch (DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with MODEL_ID %d from table \"REGISTRATION\"", modelId), dae);
        }
    }

    private class ActionHistories extends LinkedHashMap<String, ActionHistory> {

        private static final long serialVersionUID = 671353007607820018L;
    }
}
