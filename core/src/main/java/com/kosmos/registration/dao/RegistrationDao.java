package com.kosmos.registration.dao;

import java.util.List;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationState;

/**
 * DAO permettant de persister les données d'une inscription
 */
public abstract class RegistrationDao extends AbstractCommonDAO<Registration> {

    public abstract List<Registration> getByModel(Long modelId) throws DataSourceException;

    public abstract List<Registration> getByModelAndState(Long modelId, RegistrationState state) throws DataSourceException;

    public abstract void deleteByModelId(Long modelId);
}