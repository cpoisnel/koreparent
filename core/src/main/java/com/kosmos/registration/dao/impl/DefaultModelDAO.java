package com.kosmos.registration.dao.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.ParametersDataSourceException;
import com.kosmos.registration.bean.Model;
import com.kosmos.registration.dao.ModelDao;
import com.kosmos.registration.utils.RegistrationJacksonMapper;
import com.kosmos.registration.wrapper.ActionConfigurationMap;
import com.univ.utils.json.Views;

/**
 * Created on 06/01/15.
 */
public class DefaultModelDAO extends ModelDao {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultModelDAO.class);

    public DefaultModelDAO() {
        this.tableName = "MODEL";
        this.rowMapper = new RowMapper<Model>() {

            @Override
            public Model mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                Model model = null;
                try {
                    model = RegistrationJacksonMapper.getMapper().readValue(rs.getString("MODEL_DATAS"), Model.class);
                    model.setId(rs.getLong("ID_MODEL"));
                    model.setModelDescriptorId(rs.getString("MODEL_DESCRIPTOR_ID"));
                    model.setActionConfigurationById(RegistrationJacksonMapper.getMapper().readValue(rs.getString("ACTIONS_CONFIGURATIONS"), ActionConfigurationMap.class));
                } catch (IOException e) {
                    LOG.error("Unable to map the saved json values", e);
                }
                return model;
            }
        };
    }

    @Override
    public List<Model> getModelByDescriptorID(String modelDescriptorID) {
        List<Model> result;
        try {
            MapSqlParameterSource param = new MapSqlParameterSource("id", modelDescriptorID);
            result = namedParameterJdbcTemplate.query("select * from `MODEL` T1 WHERE T1.MODEL_DESCRIPTOR_ID = :id", param, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException("An error occured during selection of rows from table \"MODEL\"", dae);
        }
        return result;
    }

    protected SqlParameterSource getParameters(final Model model) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("modelDescriptorId", model.getModelDescriptorId());
        try {
            params.addValue("modelDatas", RegistrationJacksonMapper.getMapper().writerWithView(Views.DaoView.class).writeValueAsString(model));
            params.addValue("actionsConfigurations", serializeActionConfigurationMap(model));
        } catch (IOException e) {
            throw new ParametersDataSourceException(String.format("Une erreur est survenue lors de la génération des paramètres pour l'objet Model portant l'id %d", model.getId()), e);
        }
        params.addValue("id", model.getId());
        return params;
    }

    private String serializeActionConfigurationMap(Model model) throws IOException {
        ActionConfigurationMap actionMap = new ActionConfigurationMap();
        actionMap.putAll(model.getActionConfigurationById());
        return RegistrationJacksonMapper.getMapper().writeValueAsString(actionMap);
    }
}
