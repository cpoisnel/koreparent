package com.kosmos.registration.bean;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.kosmos.registration.action.history.ActionHistory;
import com.univ.objetspartages.bean.AbstractPersistenceBean;

/**
 * Bean contenant les données d'une inscription. Elle contient l'état de l'inscription (validé, modéré, terminé...)
 * ainsi que toute les données saisies.
 */
public class Registration extends AbstractPersistenceBean {

    private static final long serialVersionUID = -4052267846949744262L;

    private Long modelId;

    private Map<String, RegistrationData> dataByAction;

    private List<Pair<String, ActionHistory>> actionHistory;

    private RegistrationState state;

    private String creationIdentity;

    private Date creationDate;

    private String lastUpdateIdentity;

    private Date lastUpdateDate;

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public List<Pair<String, ActionHistory>> getActionHistory() {
        return actionHistory;
    }

    public void setActionHistory(List<Pair<String, ActionHistory>> actionHistory) {
        this.actionHistory = actionHistory;
    }

    public RegistrationState getState() {
        return state;
    }

    public void setState(RegistrationState state) {
        this.state = state;
    }

    public String getCreationIdentity() {
        return creationIdentity;
    }

    public void setCreationIdentity(String creationIdentity) {
        this.creationIdentity = creationIdentity;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getLastUpdateIdentity() {
        return lastUpdateIdentity;
    }

    public void setLastUpdateIdentity(String lastUpdateIdentity) {
        this.lastUpdateIdentity = lastUpdateIdentity;
    }

    public Map<String, RegistrationData> getDataByAction() {
        return dataByAction;
    }

    public void setDataByAction(Map<String, RegistrationData> dataByAction) {
        this.dataByAction = dataByAction;
    }
}
