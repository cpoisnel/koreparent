package com.kosmos.registration.service.impl;

import java.util.Collection;

import com.kosmos.registration.bean.Model;
import com.kosmos.registration.dao.ModelDao;
import com.kosmos.registration.exception.RegistrationModelNotFoundException;
import com.kosmos.service.impl.AbstractServiceBean;

/**
 * Created on 24/12/14.
 */
public class ModelService extends AbstractServiceBean<Model, ModelDao> {

    public Model getById(Long id) {
        throw new UnsupportedOperationException("Cette méthode n'est pas disponible pour ce service. Utilisez la méthode \"get\"");
    }

    public Model get(final Long id) throws RegistrationModelNotFoundException {
        final Model model = dao.getById(id);
        if (model == null) {
            throw new RegistrationModelNotFoundException(String.format("Le modèle portant l'id \"%d\" n'a pas été trouvé.", id));
        }
        return model;
    }

    public Model create(Model model) {
        return dao.add(model);
    }

    public Model update(Model model) {
        return dao.update(model);
    }

    public Collection<Model> getByModelDescriptorID(String modelDescriptorId) {
        return dao.getModelByDescriptorID(modelDescriptorId);
    }
}
