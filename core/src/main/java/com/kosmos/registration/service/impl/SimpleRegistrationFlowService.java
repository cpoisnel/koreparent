package com.kosmos.registration.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.registration.action.Action;
import com.kosmos.registration.action.ActionConfiguration;
import com.kosmos.registration.action.ActionWithStep;
import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Model;
import com.kosmos.registration.bean.ModelDescriptor;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.registration.exception.RegistrationException;
import com.kosmos.registration.exception.RegistrationExecutionException;
import com.kosmos.registration.exception.RegistrationModelNotFoundException;
import com.kosmos.registration.exception.RegistrationValidationException;
import com.kosmos.registration.exception.RegistrationViolationValidationException;
import com.kosmos.registration.service.RegistrationFlowService;
import com.kosmos.registration.utils.ModelDescriptorUtils;
import com.kosmos.registration.utils.RegistrationUtils;

/**
 * Created on 05/01/15.
 */
public class SimpleRegistrationFlowService implements RegistrationFlowService {

    public static final String ID_BEAN = "simpleRegistrationFlowService";

    private static final Logger LOG = LoggerFactory.getLogger(SimpleRegistrationFlowService.class);

    private RegistrationService registrationService;

    private ModelService modelService;

    public void setModelService(ModelService modelService) {
        this.modelService = modelService;
    }

    public void setRegistrationService(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @Override
    public Registration create(Map<String, RegistrationData> data, Long modelId, String identity) throws Exception {
        final Registration registration = RegistrationUtils.newRegistrationInProgress(modelId, data, identity);
        final Model model = modelService.get(modelId);
        final String modelDescriptorId = model.getModelDescriptorId();
        final ModelDescriptor modelDescriptor = getModelDescriptor(modelDescriptorId);
        processValidations(registration, model, modelDescriptor, identity);
        registrationService.save(registration);
        processActionsInCreation(registration, model, modelDescriptor, identity);
        updateRegistrationState(registration);
        registrationService.save(registration);
        return registration;
    }

    @Override
    public Registration update(final Map<String, RegistrationData> data, final Long registrationId, final String identity) throws RegistrationException {
        final Registration registration = loadRegistration(registrationId);
        final Long modelId = registration.getModelId();
        final Model model = modelService.get(modelId);
        final String modelDescriptorId = model.getModelDescriptorId();
        final ModelDescriptor modelDescriptor = getModelDescriptor(modelDescriptorId);
        updateRegistrationInformation(registration, data, identity);
        //processValidations(registration, model, modelDescriptor, identity);
        registrationService.save(registration);
        processActionsInUpdate(registration, model, modelDescriptor, identity);
        updateRegistrationState(registration);
        registrationService.save(registration);
        return registration;
    }

    private void processValidations(final Registration registration, final Model model, final ModelDescriptor modelDescriptor, final String identity) throws DataSourceException, RegistrationValidationException {
        final Map<String, Action> actionsById = modelDescriptor.getActions();
        Set<ConstraintViolation<?>> violations = new HashSet<>();
        for (Map.Entry<String, Action> actionsByIdEntry : actionsById.entrySet()) {
            final String actionId = actionsByIdEntry.getKey();
            final Action action = actionsByIdEntry.getValue();
            try {
                processActionValidation(actionId, action, registration, model, identity);
            } catch (RegistrationViolationValidationException e) {
                LOG.debug("validation error", e);
                violations.addAll(e.getConstraintViolations());
            }
        }
        if (CollectionUtils.isNotEmpty(violations)) {
            throw new RegistrationViolationValidationException("BEAN_VALIDATION.ERROR", violations);
        }
    }

    private void processActionValidation(String actionId, Action action, Registration registration, Model model, String identity) throws RegistrationValidationException {
        final Map<String, ? extends ActionConfiguration> configurationsByAction = model.getActionConfigurationById();
        final Map<String, ? extends RegistrationData> dataByAction = registration.getDataByAction();
        final RegistrationData data = dataByAction.get(actionId);
        final ActionConfiguration actionConfiguration = configurationsByAction.get(actionId);
        action.validatePrecondition(data, actionConfiguration, registration, identity, actionId);
    }

    private ModelDescriptor getModelDescriptor(final String modelDescriptorId) throws RegistrationModelNotFoundException {
        return ModelDescriptorUtils.getModelDescriptor(modelDescriptorId);
    }

    private void processActionsInCreation(final Registration registration, final Model model, final ModelDescriptor modelDescriptor, final String identity) throws RegistrationExecutionException, DataSourceException {
        final Map<String, Action> actions = modelDescriptor.getActions();
        processActions(actions, registration, model, identity);
    }

    private void processActions(Map<String, Action> actions, final Registration registration, Model model, String identity) throws RegistrationExecutionException {
        for (Map.Entry<String, Action> actionEntry : actions.entrySet()) {
            final String actionId = actionEntry.getKey();
            final Action action = actionEntry.getValue();
            final ActionHistory history = processAction(actionId, action, registration, model, identity);
            registration.getActionHistory().add(new ImmutablePair<>(actionId, history));
            if (!RegistrationState.FINISHED.equals(history.getState())) {
                break;
            }
        }
    }

    private ActionHistory processAction(String actionId, Action action, final Registration registration, Model model, String identity) throws RegistrationExecutionException {
        final Map<String, ? extends ActionConfiguration> configurationsByAction = model.getActionConfigurationById();
        final Map<String, ? extends RegistrationData> dataByAction = registration.getDataByAction();
        final RegistrationData data = dataByAction.get(actionId);
        final ActionConfiguration actionConfiguration = configurationsByAction.get(actionId);
        return action.execute(data, actionConfiguration, registration, identity, actionId);
    }

    private void updateRegistrationState(Registration registration) {
        if (CollectionUtils.isNotEmpty(registration.getActionHistory())) {
            final ActionHistory lastHistory = RegistrationUtils.getLastActionHistory(registration);
            registration.setState(lastHistory.getState());
        } else {
            registration.setState(RegistrationState.FINISHED);
        }
    }

    private Registration loadRegistration(final Long registrationId) throws RegistrationExecutionException {
        final Registration registration = registrationService.getById(registrationId);
        if (registration == null || !RegistrationState.WAITING.equals(registration.getState())) {
            // TODO
            throw new RegistrationExecutionException("TODO");
        }
        return registration;
    }

    private void updateRegistrationInformation(Registration registration, Map<String, RegistrationData> data, String identity) {
        registration.setLastUpdateDate(new Date());
        registration.setLastUpdateIdentity(identity);
        // TODO Fusion des data ???
        registration.setDataByAction(data);
    }

    private void processActionsInUpdate(final Registration registration, final Model model, final ModelDescriptor modelDescriptor, final String identity) throws RegistrationExecutionException, RegistrationValidationException {
        if (!RegistrationState.WAITING.equals(registration.getState())) {
            // TODO
            throw new RegistrationExecutionException("TODO");
        }
        final String restartActionId = processFirstActionInUpdate(registration, model, modelDescriptor, identity);
        updateRegistrationState(registration);
        if (RegistrationState.FINISHED.equals(registration.getState())) {
            final Map<String, Action> actionsToProcess = ModelDescriptorUtils.getActionsAfterActionId(restartActionId, modelDescriptor);
            processActions(actionsToProcess, registration, model, identity);
        }
    }

    private String processFirstActionInUpdate(final Registration registration, final Model model, final ModelDescriptor modelDescriptor, final String identity) throws RegistrationExecutionException, RegistrationValidationException {
        final String actionId = RegistrationUtils.getLastActionIdFromHistory(registration);
        final Action action = modelDescriptor.getActions().get(actionId);
        if (action instanceof ActionWithStep) {
            final ActionWithStep actionWithStep = (ActionWithStep) action;
            final ActionHistory history = processActionWithStep(actionId, actionWithStep, registration, model, identity);
            registration.getActionHistory().add(new ImmutablePair<>(actionId, history));
        } else {
            final ActionHistory history = processAction(actionId, action, registration, model, identity);
            registration.getActionHistory().add(new ImmutablePair<>(actionId, history));
        }
        return actionId;
    }

    private ActionHistory processActionWithStep(final String actionId, final ActionWithStep action, final Registration registration, final Model model, final String identity) throws RegistrationExecutionException, RegistrationValidationException {
        final Map<String, ? extends ActionConfiguration> configurationsByAction = model.getActionConfigurationById();
        final Map<String, ? extends RegistrationData> dataByAction = registration.getDataByAction();
        final RegistrationData data = dataByAction.get(actionId);
        final ActionConfiguration actionConfiguration = configurationsByAction.get(actionId);
        action.validateStepPrecondition(data, actionConfiguration, registration, identity, actionId);
        return action.executeStep(data, actionConfiguration, registration, identity, actionId);
    }
}