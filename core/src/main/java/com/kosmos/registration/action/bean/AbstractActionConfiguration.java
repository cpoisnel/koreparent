package com.kosmos.registration.action.bean;

import com.kosmos.registration.action.ActionConfiguration;

/**
 * Created on 09/01/15.
 */
public abstract class AbstractActionConfiguration implements ActionConfiguration {

    private String actionId;

    @Override
    public String getActionId() {
        return actionId;
    }

    public void setActionId(final String actionId) {
        this.actionId = actionId;
    }
}
