package com.kosmos.registration.action;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.apache.commons.collections.CollectionUtils;

import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.exception.RegistrationValidationException;
import com.kosmos.registration.exception.RegistrationViolationValidationException;
import com.kosmos.validation.ValidationUtils;

/**
 * Classe abstraite permettant de gérer la validation de bean de façon générique via la ValidationAPI de java
 */
public abstract class DefaultStepAction<R extends RegistrationData, A extends ActionConfiguration> extends DefaultAction<R, A> implements ActionWithStep<R, A> {

    @Override
    public void validateStepPrecondition(R data, A configuration, Registration registration, String identity, String actionID) throws RegistrationValidationException {
        Set<ConstraintViolation<R>> violations = ValidationUtils.validateBean(data);
        if (CollectionUtils.isNotEmpty(violations)) {
            throw new RegistrationViolationValidationException("BEAN_VALIDATION.ERROR", violations);
        }
    }
}
