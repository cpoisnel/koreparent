package com.kosmos.registration.action.history;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.kosmos.registration.bean.RegistrationState;

/**
 * Created on 24/12/14.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class ActionHistory {

    private Date creationDate;

    private String creationIdentity;

    private RegistrationState state;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationIdentity() {
        return creationIdentity;
    }

    public void setCreationId(final String creationIdentity) {
        this.creationIdentity = creationIdentity;
    }

    public RegistrationState getState() {
        return state;
    }

    public void setState(final RegistrationState state) {
        this.state = state;
    }
}
