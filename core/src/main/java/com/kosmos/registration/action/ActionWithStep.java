package com.kosmos.registration.action;

import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.exception.RegistrationExecutionException;
import com.kosmos.registration.exception.RegistrationValidationException;

/**
 * Created on 24/12/14.
 */
public interface ActionWithStep<R extends RegistrationData, A extends ActionConfiguration> extends Action<R, A> {

    void validateStepPrecondition(R data, A configuration, Registration registration, String identity, String actionID) throws RegistrationValidationException;

    ActionHistory executeStep(R data, A configuration, Registration registration, String identity, String actionID) throws RegistrationExecutionException;
}
