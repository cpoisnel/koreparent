package com.kosmos.registration.action;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created on 24/12/14.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public interface ActionConfiguration {

    String getActionId();
}
