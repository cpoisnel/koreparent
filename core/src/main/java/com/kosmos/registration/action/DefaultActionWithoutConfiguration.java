package com.kosmos.registration.action;

import com.kosmos.registration.action.bean.EmptyActionConfiguration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.exception.RegistrationViolationValidationException;

/**
 * Created by olivier.camon on 14/01/15.
 */
public abstract class DefaultActionWithoutConfiguration<R extends RegistrationData> extends DefaultAction<R, EmptyActionConfiguration> {

    @Override
    public void validateConfiguration(EmptyActionConfiguration configuration) throws RegistrationViolationValidationException {}
}
