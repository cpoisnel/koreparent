package com.kosmos.registration.action;

import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.exception.RegistrationExecutionException;
import com.kosmos.registration.exception.RegistrationValidationException;
import com.kosmos.registration.exception.RegistrationViolationValidationException;

/**
 * Created on 24/12/14.
 */
public interface Action<R extends RegistrationData, A extends ActionConfiguration> {

    void validateConfiguration(A configuration) throws RegistrationViolationValidationException;

    void validatePrecondition(R data, A configuration, Registration registration, String identity, String actionID) throws RegistrationValidationException;

    ActionHistory execute(R data, A configuration, Registration registration, String identity, String actionID) throws RegistrationExecutionException;
}
