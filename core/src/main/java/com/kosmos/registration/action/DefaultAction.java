package com.kosmos.registration.action;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.apache.commons.collections.CollectionUtils;

import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.exception.RegistrationValidationException;
import com.kosmos.registration.exception.RegistrationViolationValidationException;
import com.kosmos.validation.ValidationUtils;

/**
 * Classe abstraite permettant de gérer la validation de bean de façon générique via la ValidationAPI de java
 */
public abstract class DefaultAction<R extends RegistrationData, A extends ActionConfiguration> implements Action<R, A> {

    @Override
    public void validateConfiguration(A configuration) throws RegistrationViolationValidationException {
        Set<ConstraintViolation<A>> violations = ValidationUtils.validateBean(configuration);
        if (CollectionUtils.isNotEmpty(violations)) {
            throw new RegistrationViolationValidationException("CONFIGURATION_VALIDATION.ERROR", violations);
        }
    }

    @Override
    public void validatePrecondition(R data, A configuration, Registration registration, String identity, String actionId) throws RegistrationValidationException {
        Set<ConstraintViolation<R>> violations = ValidationUtils.validateBean(data);
        if (CollectionUtils.isNotEmpty(violations)) {
            throw new RegistrationViolationValidationException("BEAN_VALIDATION.ERROR", violations);
        }
    }
}
