package com.kosmos.datasource.sql.utils;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created on 08/04/15.
 */
public class SqlDateConverter {

    /**
     * Converts a {@link Date} to a {@link java.sql.Date}.
     * @param date : the {@link Date} to convert.
     * @return the converted {@link java.sql.Date} or null if the given date was null.
     */
    public static java.sql.Date toDate(Date date) {
        if (date != null) {
            return new java.sql.Date(date.getTime());
        }
        return null;
    }

    /**
     * Converts a {@link Date} to a {@link Time}.
     * @param date : the {@link Date} to convert.
     * @return the converted {@link Time} or null if the given date was null.
     */
    public static Time toTime(Date date) {
        if (date != null) {
            return new Time(date.getTime());
        }
        return null;
    }

    /**
     * Converts a {@link Date} to a {@link Timestamp}.
     * @param date : the {@link Date} to convert.
     * @return the converted {@link Timestamp} or null if the given date was null.
     */
    public static Timestamp toTimestamp(Date date) {
        if (date != null) {
            return new Timestamp(date.getTime());
        }
        return null;
    }

    /**
     * Converts a {@link java.sql.Date} to a {@link Date}.
     * @param date : the {@link java.sql.Date} to convert.
     * @return the converted {@link Date} or null if the given date was null.
     */
    public static Date fromDate(java.sql.Date date) {
        if (date != null) {
            return new Date(date.getTime());
        }
        return null;
    }

    /**
     * Converts a {@link Time} to a {@link Date}.
     * @param time : the {@link Time} to convert.
     * @return the converted {@link Date} or null if the given date was null.
     */
    public static Date fromTime(Time time) {
        if (time != null) {
            return new Date(time.getTime());
        }
        return null;
    }

    /**
     * Converts a {@link Timestamp} to a {@link Date}.
     * @param timestamp : the {@link Timestamp} to convert.
     * @return the converted {@link Date} or null if the given date was null.
     */
    public static Date fromTimestamp(Timestamp timestamp) {
        if (timestamp != null) {
            return new Date(timestamp.getTime());
        }
        return null;
    }
}
