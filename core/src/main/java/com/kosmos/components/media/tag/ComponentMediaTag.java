package com.kosmos.components.media.tag;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TryCatchFinally;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.kosmos.components.media.bean.ComponentMediaFile;
import com.kosmos.components.media.bean.ComponentMediaFileList;
import com.kosmos.components.media.bean.ComponentMediaType;
import com.kosmos.components.media.view.model.ComponentMediaViewModel;
import com.kosmos.components.tags.AbstractJspIncludeTag;
import com.kportal.core.config.MessageHelper;

/**
 * Created on 14/11/14.
 */
public class ComponentMediaTag extends AbstractJspIncludeTag implements TryCatchFinally {

    public static final String VIEW_MODEL = "viewModel";

    private static final long serialVersionUID = 3394667477792001928L;

    private String name;

    private ComponentMediaType type;

    private String label;

    private boolean multiple;

    private String extension;

    private ComponentMediaFileList medias;

    public ComponentMediaTag() {
        this.path = "/adminsite/components/media/media-component.jsp";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ComponentMediaType getType() {
        return type;
    }

    public void setType(ComponentMediaType type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public ComponentMediaFileList getMedias() {
        return medias;
    }

    public void setMedias(ComponentMediaFileList medias) {
        this.medias = medias;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Override
    public int doStartTag() throws JspException {
        pageContext.getRequest().setAttribute(VIEW_MODEL, buildModel());
        return super.doStartTag();
    }

    private ComponentMediaViewModel buildModel() {
        final ComponentMediaViewModel viewModel = new ComponentMediaViewModel();
        final List<Long> ids = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(medias)) {
            for (ComponentMediaFile currentComponentMediaFile : medias) {
                ids.add(currentComponentMediaFile.getId());
            }
            viewModel.setMedias(medias);
        }
        viewModel.setIds(String.format("[%s]", StringUtils.join(ids.iterator(), ",")));
        viewModel.setMultiple(multiple);
        viewModel.setName(name);
        viewModel.setType(type);
        final String defaultLabel = multiple ? MessageHelper.getCoreMessage("MEDIA_COMPONENT.LABEL.MULTIPLE") : MessageHelper.getCoreMessage("MEDIA_COMPONENT.LABEL.UNIQUE");
        String finalLabel = StringUtils.isNoneBlank(label) ? StringUtils.defaultIfBlank(MessageHelper.getExtensionMessage(extension, label), label) : defaultLabel;
        if (finalLabel.equals(label)) {
            finalLabel = defaultLabel;
        }
        viewModel.setLabel(finalLabel);
        return viewModel;
    }

    @Override
    public void doCatch(Throwable throwable) throws Throwable {
        throw throwable;
    }

    @Override
    public void doFinally() {
        name = null;
        type = null;
        medias = null;
        label = null;
        multiple = false;
    }
}
