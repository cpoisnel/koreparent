package com.kosmos.components.media.bean;

import java.util.ArrayList;

/**
 * Created by Fabien on 15/11/2014.
 */
public class ComponentMediaFileList extends ArrayList<ComponentMediaFile> {

    public static final ComponentMediaFileList EMPTY = new ComponentMediaFileList();

    private static final long serialVersionUID = 8215176845717913308L;
}
