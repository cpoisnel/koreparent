package com.kosmos.components.media.utils;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.components.media.bean.ComponentMediaFile;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.util.RessourceUtils;

/**
 * Created on 15/11/2014.
 */
public class ComponentMediaFileUtil {

    public static String getDisplayUrl(ComponentMediaFile componentMediaFile) {
        if(componentMediaFile != null) {
            final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
            final RessourceBean ressource = serviceRessource.getById(componentMediaFile.getId());
            if (ressource != null) {
                return RessourceUtils.getUrl(ressource);
            }
        }
        return StringUtils.EMPTY;
    }

    public static String getCodeParent(ComponentMediaFile componentMediaFile) {
        final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
        final RessourceBean ressource = serviceRessource.getById(componentMediaFile.getId());
        if (ressource != null) {
            return ressource.getCodeParent();
        }
        return null;
    }
}
