package com.kosmos.components.media.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.univ.utils.json.Views;

/**
 * Created by Fabien on 15/11/2014.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class ComponentMediaFile implements Serializable {

    private static final long serialVersionUID = -6649647100715273765L;

    @JsonView({Views.DaoView.class})
    private String format;

    @JsonView({Views.DaoView.class})
    private Long id;

    @JsonView({Views.DaoView.class})
    private String legende;

    @JsonView({Views.DaoView.class})
    private String titre;

    @JsonView({Views.DaoView.class})
    private int fileNumber = -1;

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLegende() {
        return legende;
    }

    public void setLegende(String legende) {
        this.legende = legende;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(int fileNumber) {
        this.fileNumber = fileNumber;
    }

    @Override
    public String toString() {
        return String.format("%d;%s;%s;%s;", id, titre, format, legende);
    }
}
