package com.kosmos.components.media.view.model;

import com.kosmos.components.media.bean.ComponentMediaFileList;
import com.kosmos.components.media.bean.ComponentMediaType;
import com.kosmos.components.view.model.ComponentViewModel;

/**
 * Created on 30/12/14.
 */
public class ComponentMediaViewModel extends ComponentViewModel {

    private static final long serialVersionUID = -8332154804839517308L;

    private String ids;

    private ComponentMediaType type;

    private boolean multiple;

    private ComponentMediaFileList medias = ComponentMediaFileList.EMPTY;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public ComponentMediaType getType() {
        return type;
    }

    public void setType(ComponentMediaType type) {
        this.type = type;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public ComponentMediaFileList getMedias() {
        return medias;
    }

    public void setMedias(ComponentMediaFileList medias) {
        this.medias = medias;
    }
}
