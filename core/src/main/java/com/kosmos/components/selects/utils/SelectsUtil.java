package com.kosmos.components.selects.utils;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.kosmos.components.selects.model.SelectItemModel;
import com.univ.utils.EscapeString;

/**
 * Created on 09/02/15.
 */
public class SelectsUtil {

    public static String generateDataAttributes(SelectItemModel model) {
        if (MapUtils.isNotEmpty(model.getData())) {
            final StringBuilder result = new StringBuilder();
            for (Map.Entry<String, String> currentData : model.getData().entrySet()) {
                result.append(String.format("data-%s=\"%s\" ", currentData.getKey(), EscapeString.escapeAttributHtml(currentData.getValue())));
            }
            return result.toString();
        }
        return StringUtils.EMPTY;
    }
}
