package com.kosmos.components.emaillink.tag;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TryCatchFinally;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.HTMLUtil;
import com.kosmos.components.tags.AbstractJspIncludeTag;

/**
 * Created on 12/08/15.
 */
public class EmailLinkTag extends AbstractJspIncludeTag implements TryCatchFinally {

    private static final long serialVersionUID = -5009503208575675898L;

    private String href;

    private String label;

    private String subject;

    private String body;

    public void setHref(final String href) {
        this.href = href;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    public void setBody(final String body) {
        this.body = body;
    }

    public EmailLinkTag() {
        this.path = "/adminsite/components/emaillink/email-link.jsp";
    }

    @Override
    public int doStartTag() throws JspException {
        final String emailHref = getEmailHref();
        pageContext.getRequest().setAttribute("href", HTMLUtil.obfuscateEmailLink(emailHref));
        if(StringUtils.isNotBlank(label)) {
            pageContext.getRequest().setAttribute("label", HTMLUtil.obfuscateEmailDisplay(label));
        } else {
            pageContext.getRequest().setAttribute("label", HTMLUtil.obfuscateEmailDisplay(href));
        }
        return super.doStartTag();
    }

    private String getEmailHref() {
        final String rawEmail = href.contains("?") ? StringUtils.substringBefore(href.replace("mailto:", StringUtils.EMPTY), "?") : href.replace("mailto:", StringUtils.EMPTY);
        final Map<String, String> params = getParams(href);
        String finalLink = "mailto:";
        if(StringUtils.isNotBlank(subject)) {
            params.put("SUBJECT", subject);
        }
        if(StringUtils.isNotBlank(body)) {
            params.put("BODY", body);
        }
        finalLink += rawEmail;
        boolean first = true;
        for(Map.Entry<String, String> currentEntry : params.entrySet()) {
            finalLink += (first ? "?" : "&") + currentEntry.getKey().toLowerCase() + "=" + currentEntry.getValue();
            first = false;
        }
        return finalLink;
    }

    private Map<String, String> getParams(String email) {
        final Map<String, String> realParams = new HashMap<>();
        final int startIndex = email.indexOf("?");
        if(startIndex != -1) {
            final String stringParams = email.substring(startIndex + 1);
            final String[] params = stringParams.split("&");
            for(String currentPair : params) {
                final String[] keyValue = currentPair.split("=");
                if(keyValue.length == 2) {
                    realParams.put(keyValue[0].toUpperCase(), keyValue[1]);
                }
            }
        }
        return realParams;
    }

    @Override
    public void doCatch(final Throwable throwable) throws Throwable {
        throw throwable;
    }

    @Override
    public void doFinally() {
        href = null;
        label = null;
    }
}
