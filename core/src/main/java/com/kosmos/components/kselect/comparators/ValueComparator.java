package com.kosmos.components.kselect.comparators;

import java.util.Comparator;

import com.kosmos.components.kselect.model.SelectItemModel;

public class ValueComparator implements Comparator<SelectItemModel> {

    @Override
    public int compare(final SelectItemModel model1, final SelectItemModel model2) {
        return model1.getValue().compareTo(model2.getValue());
    }
}
