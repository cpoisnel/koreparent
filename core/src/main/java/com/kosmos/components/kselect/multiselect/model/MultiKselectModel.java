package com.kosmos.components.kselect.multiselect.model;

import java.util.Collection;

import com.kosmos.components.kselect.model.AbstractKselectModel;
import com.kosmos.components.kselect.model.SelectItemModel;
import com.kosmos.components.kselect.multiselect.MultiSelectMode;

/**
 * Created on 06/02/15.
 */
public class MultiKselectModel extends AbstractKselectModel {

    private MultiSelectMode mode;

    private String value;

    private String attributes;

    private Collection<SelectItemModel> items;

    private Collection<SelectItemModel> selectedItems;

    private String classCss;

    private String concatenatedSelectedValues;


    private String concatenatedSelectedValuesLabels;

    private String formatValue;

    private String ariane;

    public void setValue(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(final String attributes) {
        this.attributes = attributes;
    }

    public MultiSelectMode getMode() {
        return mode;
    }

    public void setMode(MultiSelectMode mode) {
        this.mode = mode;
    }

    public Collection<SelectItemModel> getItems() {
        return items;
    }

    public void setItems(Collection<SelectItemModel> items) {
        this.items = items;
    }

    public Collection<SelectItemModel> getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(Collection<SelectItemModel> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public String getConcatenatedSelectedValues() {
        return concatenatedSelectedValues;
    }

    public void setConcatenatedSelectedValues(String concatenatedSelectedValues) {
        this.concatenatedSelectedValues = concatenatedSelectedValues;
    }

    public String getConcatenatedSelectedValuesLabels() {
        return concatenatedSelectedValuesLabels;
    }

    public void setConcatenatedSelectedValuesLabels(String concatenatedSelectedValuesLabels) {
        this.concatenatedSelectedValuesLabels = concatenatedSelectedValuesLabels;
    }

    public void setFormatValue(String formatValue) {
        this.formatValue = formatValue;
    }

    public String getFormatValue() {
        return formatValue;
    }

    public void setAriane(String ariane) {
        this.ariane = ariane;
    }

    public String getAriane() {
        return ariane;
    }

    public String getClassCss() {
        return classCss;
    }

    public void setClassCss(String classCss) {
        this.classCss = classCss;
    }
}
