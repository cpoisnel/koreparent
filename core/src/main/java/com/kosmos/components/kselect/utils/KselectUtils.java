package com.kosmos.components.kselect.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.kosmos.components.kselect.model.SelectItemModel;
import com.kosmos.components.kselect.multiselect.model.MultiKselectModel;
import com.univ.tree.processus.GroupsJsTree;
import com.univ.tree.processus.RubriquesJsTree;
import com.univ.tree.processus.StructuresJsTree;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.components.kselect.model.AbstractKselectModel;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.EscapeString;

public class KselectUtils {

    private static final Pattern selectPattern = Pattern.compile("([/]*[a-zA-Z0-9\\-]*)/([\\S]*)");

    private static final Map<String, String> URLS = new HashMap<>();

    static {
        URLS.put("utilisateur", WebAppUtil.SG_PATH + "?PROC=SAISIE_UTILISATEUR&ACTION=RECHERCHER&MODE=RECHERCHE&TOOLBOX=TRUE");
        URLS.put("pagelibre", WebAppUtil.SG_PATH + "?PROC=SAISIE_PAGELIBRE&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE_JOINTURE&LANGUE_FICHE=0");
        URLS.put("pagelibre_creation", WebAppUtil.SG_PATH + "?PROC=TRAITEMENT_PAGELIBRE&ACTION=AJOUTER&CODE_RUBRIQUE=LIBELLE_CODE_PAGE_TETE");
    }

    public static void fillModel(final KselectContext context, final AbstractKselectModel model, final InfoBean infoBean, final String nomDonnee, final String params, final boolean mono) {
        switch (context) {
            case CONTEXT_ZONE:
                handleZoneContext(model, infoBean, nomDonnee, params, mono);
                break;
            case CONTEXT_STRUCTURE:
                handleStructureContext(model, infoBean, nomDonnee, mono);
                break;
            case CONTEXT_GROUPEDSI_RESTRICTION:
                fillForGroup(model, mono);
                handleDsiRestrictionContext(model, infoBean, nomDonnee, params);
                break;
            case CONTEXT_GROUPEDSI_PUBLIC_VISE:
                fillForGroup(model, mono);
                handlePublicViseContext(model, infoBean, nomDonnee);
                break;
        }
    }

    public static String generateAriane(InfoBean infoBean, String nomDonnee, KselectContext context){
        String value = "";
        if (infoBean.get(nomDonnee) != null) {
            value = infoBean.getString(nomDonnee);
        }
        String ariane = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(value)) {
            final String[] values = value.split(";");
            final String[] tips = new String[values.length];
            for (int i = 0; i < values.length; i++) {
                switch (context) {
                    case CONTEXT_ZONE:
                        tips[i] = RubriquesJsTree.getPath("", values[i], " > ");
                        break;
                    case CONTEXT_STRUCTURE:
                        tips[i] = StructuresJsTree.getPath("", values[i], " > ");
                        break;
                    case CONTEXT_GROUPEDSI_RESTRICTION:
                    case CONTEXT_GROUPEDSI_PUBLIC_VISE:
                        tips[i] = GroupsJsTree.getPath("", values[i], " > ");
                        break;
                }
            }
            ariane = StringUtils.join(tips, ";");

        }
        return ariane;
    }

    public static void fillValues(final MultiKselectModel model){
        StringBuilder sbCodes = new StringBuilder();
        StringBuilder sbLabels = new StringBuilder();
        for(SelectItemModel item : model.getSelectedItems()){
            sbCodes.append(item.getValue()).append(";");
            sbLabels.append(item.getTitle()).append(";");
        }
        model.setConcatenatedSelectedValues(sbCodes.toString());
        model.setConcatenatedSelectedValuesLabels(sbLabels.toString());
    }

    private static String getLanguage(final InfoBean infoBean) {
        String lg = String.valueOf(LangueUtil.getIndiceLocale(LangueUtil.getDefaultLocale()));
        final String langueInfoBean = infoBean.get("LANGUE", String.class);
        final String langueFicheInfoBean = infoBean.get("LANGUE_FICHE", String.class);
        if (StringUtils.isNotEmpty(langueFicheInfoBean) && StringUtils.isNumeric(langueFicheInfoBean)) {
            lg = langueFicheInfoBean;
        } else if (StringUtils.isNotEmpty(langueInfoBean) && StringUtils.isNumeric(langueInfoBean)) {
            lg = langueInfoBean;
        }
        return lg;
    }

    private static String getSectionPermission(final InfoBean infoBean, final String nomDonnee) {
        if (infoBean.get("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE") != null && infoBean.getString("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE").equals(nomDonnee)) {
            final String type = infoBean.getString("GRS_PERMISSION_TYPE");
            final String objet = infoBean.getString("GRS_PERMISSION_OBJET");
            final String action = infoBean.getString("GRS_PERMISSION_ACTION");
            if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(objet) && (StringUtils.isNotBlank(action) || "acp".equals(objet))) {
                return "/" + type + "/" + objet + "/" + action + "/";
            }
        }
        return StringUtils.EMPTY;
    }

    private static String getPopinTitle(final String key) {
        return MessageHelper.getCoreMessage("MODALS.KSELECT.TITLE." + key);
    }

    private static void handleZoneContext(final AbstractKselectModel model, final InfoBean infoBean, final String nomDonnee, final String params, final boolean mono) {
        final Matcher matcher = selectPattern.matcher(params);
        final String realParams = matcher.matches() ? matcher.group(2) : params;
        model.setPopinWidth(650);
        model.setPopinTitle(getPopinTitle("OBJET"));
        model.setPopinValidate(false);
        // formulaire de recherche par fiche pour associer une fiche
        if (realParams.startsWith("saisie_")) {
            String param = "";
            if (realParams.contains("&")) {
                param = StringUtils.substring(realParams, realParams.indexOf("&"));
            }
            final String nomObjet = StringUtils.substring(realParams, 7, realParams.length() - param.length());
            final String codeObjet = ReferentielObjets.getCodeObjet(nomObjet);
            final String extension = ReferentielObjets.getExtension(codeObjet);
            final String proc = ReferentielObjets.getProcessus(codeObjet);
            model.setAction(WebAppUtil.SG_PATH + "?EXT=" + extension + "&PROC=" + EscapeString.escapeJavaScript(proc) + "&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE_JOINTURE&LANGUE_FICHE=-1" + param);
        } else if (realParams.startsWith("objet_")) {
            model.setAction("/adminsite/toolbox/choix_objet.jsp?TOOLBOX=" + realParams.substring(6).toUpperCase());
        } else {
            if (!StringUtils.isBlank(URLS.get(realParams))) {
                model.setPopinTitle(getPopinTitle(realParams.toUpperCase()));
                model.setAction(URLS.get(realParams));
            } else {
                model.setPopinTitle(getPopinTitle(mono ? "RUBRIQUE.MONO" : "RUBRIQUE.MULTI"));
                model.setPopinValidate(!mono);
                if ("rubrique".equals(realParams)) {
                    model.setAction("/adminsite/tree/tree.jsp?JSTREEBEAN=rubriquesJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&UNBINDED_SELECT=true" + getSectionPermission(infoBean, nomDonnee));
                } else {
                    model.setAction("/adminsite/tree/tree.jsp?JSTREEBEAN=rubriquesJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&UNBINDED_SELECT=true&PERMISSION=/" + realParams);
                }
            }
        }
    }

    private static String getGroupPermission(final InfoBean infoBean, final String nomDonnee) {
        if ("1".equals(infoBean.get("GRS_FILTRE_ARBRE_GROUPE")) || "1".equals(infoBean.get("GRS_FILTRE_ARBRE_GROUPE_" + nomDonnee.toUpperCase()))) {
            // Controle si affichage ecran PRINCIPAL ou RECHERCHE
            // Valorisé dans ControleurUniv
            final String type = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_TYPE");
            final String objet = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_OBJET");
            final String action = infoBean.getString("GRS_FILTRE_ARBRE_GROUPE_ACTION");
            if (!StringUtils.isBlank(type) && !StringUtils.isBlank(objet)) {
                return "&PERMISSION=/" + type + "/" + objet + "/" + action + "/";
            }
        }
        return StringUtils.EMPTY;
    }

    private static void handleStructureContext(final AbstractKselectModel model, final InfoBean infoBean, final String nomDonnee, final boolean mono) {
        final boolean frontOffice = infoBean.get("SAISIE_FRONT") != null;
        StringBuilder editAction = new StringBuilder("/adminsite/tree/tree.jsp?JSTREEBEAN=structuresJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&UNBINDED_SELECT=true");
        if (frontOffice) {
            editAction.append("&LANGUE=").append(getLanguage(infoBean));
            editAction.append("&RACINE=").append(nomDonnee);
            editAction.append("&FRONT=true");
            if (infoBean.get("FILTRE_" + nomDonnee) != null) {
                editAction.append("&FILTRE=").append(EscapeString.escapeJavaScript(infoBean.getString("FILTRE_" + nomDonnee)));
            }
        } else {
            editAction.append(getStructurePermission(infoBean, nomDonnee));
            if (infoBean.getEtatObjet() != null && !InfoBean.ETAT_OBJET_CREATION.equals(infoBean.getEtatObjet())) {
                editAction.append("&LANGUE=").append(getLanguage(infoBean));
            }
            if (infoBean.get("FILTRE_" + nomDonnee) != null) {
                editAction.append("&FILTRE=").append(EscapeString.escapeJavaScript(infoBean.getString("FILTRE_" + nomDonnee)));
            }
            if (infoBean.get("RACINE_" + nomDonnee) != null) {
                editAction.append("&RACINE=").append(EscapeString.escapeJavaScript(infoBean.getString("RACINE_" + nomDonnee)));
            }
        }
        model.setPopinWidth(650);
        model.setPopinTitle(getPopinTitle(mono ? "STRUCTURE.MONO" : "STRUCTURE.MULTI"));
        model.setPopinValidate(!mono);
        model.setAction(editAction.toString());
    }

    private static String getStructurePermission(final InfoBean infoBean, final String nomDonnee) {
        String nomDonneeFiltreArbre = nomDonnee;
        if (nomDonnee.startsWith("TMP")) {
            // On enlève le TMP_ (pour les listes)
            nomDonneeFiltreArbre = nomDonnee.substring(4);
        }
        // Controle si la donnée correspond bien à la structure de
        // l'objet métier
        if (infoBean.get("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT") != null && infoBean.getString("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT").equals(nomDonneeFiltreArbre) || infoBean.get("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT_AUTRES") != null && infoBean.getString("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT_AUTRES").equals(nomDonneeFiltreArbre)) {
            // Controle si affichage ecran PRINCIPAL ou RECHERCHE
            // Valorisé dans ControleurUniv
            final String type = infoBean.getString("GRS_PERMISSION_TYPE");
            final String objet = infoBean.getString("GRS_PERMISSION_OBJET");
            final String action = infoBean.getString("GRS_PERMISSION_ACTION");
            if (!StringUtils.isBlank(type) && !StringUtils.isBlank(objet) && !StringUtils.isBlank(action)) {
                return "&PERMISSION=/" + type + "/" + objet + "/" + action + "/";
            }
        }
        return StringUtils.EMPTY;
    }

    private static void handleDsiRestrictionContext(final AbstractKselectModel model, final InfoBean infoBean, final String nomDonnee, final String params) {
        final String editAction = "/adminsite/tree/tree.jsp?JSTREEBEAN=groupsJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&UNBINDED_SELECT=true" + ("lock_dyn".equals(params) ? "&LOCK_DYN=true" : StringUtils.EMPTY);
        model.setPopinWidth(650);
        model.setAction(editAction + getGroupPermission(infoBean, nomDonnee));
    }

    private static void handlePublicViseContext(final AbstractKselectModel model, final InfoBean infoBean, final String nomDonnee) {
        model.setPopinWidth(650);
        model.setAction("/adminsite/tree/tree.jsp?JSTREEBEAN=groupsJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&PUBLIC_VISE=1&UNBINDED_SELECT=true" + getGroupPermission(infoBean, nomDonnee));
    }

    private static void fillForGroup(final AbstractKselectModel model, final boolean mono) {
        model.setPopinWidth(650);
        model.setPopinTitle(getPopinTitle(mono ? "GROUPE.MONO" : "GROUPE.MULTI"));
        model.setPopinValidate(!mono);
    }
}
