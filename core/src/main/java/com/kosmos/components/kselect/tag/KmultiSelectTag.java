package com.kosmos.components.kselect.tag;

import com.kosmos.components.kselect.multiselect.MultiSelectMode;
import com.kportal.core.config.MessageHelper;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.kosmos.components.kselect.multiselect.model.MultiKselectModel;
import com.kosmos.components.kselect.utils.KmultiSelectUtils;
import com.kosmos.components.kselect.utils.KselectContext;
import com.kosmos.components.tags.AbstractComponentTag;

public class KmultiSelectTag extends AbstractComponentTag<MultiKselectModel> {

    private static final long serialVersionUID = -1842578617799617691L;

    public static final String MULTI_TEMPLATE = "/adminsite/components/kselect/multi/multi.jsp";

    /**
     * The context to compute the rendering {@link com.kosmos.components.kselect.utils.KselectContext}
     */
    private KselectContext context = KselectContext.CONTEXT_DEFAULT;

    private String params = StringUtils.EMPTY;

    private int editOption = FormateurJSP.SAISIE_FACULTATIF;

    private String dataSource;

    private boolean editable;

    private MultiSelectMode type;

    public void setContext(final KselectContext context) {
        this.context = context;
    }

    public void setEditOption(final int editOption) {
        this.editOption = editOption;
    }

    public void setParams(final String params) {
        this.params = params;
    }

    public void setDataSource(final String dataSource) {
        this.dataSource = dataSource;
    }

    public void setType(MultiSelectMode type) {
        this.type = type;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public KmultiSelectTag() {
        path = MULTI_TEMPLATE;
    }

    @Override
    protected MultiKselectModel buildModel() {
        final MultiKselectModel model = KmultiSelectUtils.buildMultiSelectModel(extension, context, retrieveInfoBean(), dataSource, fieldName, editOption, params, label, tooltip, type, editable);
        final FormateurJSP fmt = getFormateur();
        int currentIndex = fmt.getIndiceChamp();
        final String formatValue = String.format("%d;1;0;0;8pt;%d", editOption, currentIndex++);
        fmt.setIndiceChamp(currentIndex);
        model.setFormatValue(formatValue);
        return model;
    }


    private FormateurJSP getFormateur() {
        return (FormateurJSP) pageContext.getAttribute("fmt");
    }
}
