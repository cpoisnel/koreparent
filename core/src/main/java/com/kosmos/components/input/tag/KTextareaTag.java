package com.kosmos.components.input.tag;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.components.input.model.KTextareaViewModel;
import com.kosmos.components.tags.AbstractComponentTag;
import com.kportal.core.config.MessageHelper;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.jsp.tagext.JspTag;

/**
 * Created by alexandre.baillif on 29/12/16.
 */
public class KTextareaTag extends AbstractComponentTag<KTextareaViewModel> {


	private static final int DEFAULT_ROWS = 6;

	private static final int DEFAULT_COLS = 40;

	private int max;

	private int cols;

	private int rows;


	public KTextareaTag() {
		path = "/adminsite/components/inputs/ktextarea.jsp";
	}

	public void setMax(int max) {
		this.max = max;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	@Override
	protected KTextareaViewModel buildModel() {
		final FormateurJSP fmt = getFormateur();
		final KTextareaViewModel model = new KTextareaViewModel();
		final String name = StringUtils.defaultIfBlank(MessageHelper.getExtensionMessage(extension, label), label);
		final String helpMessage = StringUtils.defaultIfBlank(MessageHelper.getExtensionMessage(extension, tooltip), tooltip);
		int currentIndex = fmt.getIndiceChamp();
		final String formatValue = String.format("%d;%d;%d;%d;LIB=%s;%d", editOption, FormateurJSP.FORMAT_MULTI_LIGNE, 0, max, transformerNamePourFormat(name), currentIndex++);
		fmt.setIndiceChamp(currentIndex);
		model.setName(fieldName);
		model.setLabel(name);
		model.setTooltip(helpMessage);
		model.setValue(StringUtils.defaultString(retrieveValue(fieldName)));
		model.setFormatValue(formatValue);
		model.setMax(max);
		model.setRequired(editOption == FormateurJSP.SAISIE_OBLIGATOIRE);
		model.setEditOption(editOption);
		model.setRows(rows == 0 ? DEFAULT_ROWS : rows);
		model.setCols(cols == 0 ? DEFAULT_COLS : cols);
		return model;
	}


	/**
	 * Suppression des ' qui posent problème dans les balises INPUT. Suppression des " qui posent aussi problème
	 *
	 * @param name
	 *            the name
	 *
	 * @return the string
	 */
	private String transformerNamePourFormat(final String name) {
		final String result = StringUtils.replace(name, "\"", " ");
		return StringUtils.replace(result, "'", "\'");
	}




	private FormateurJSP getFormateur() {
		return (FormateurJSP) pageContext.getAttribute("fmt");
	}

	private String retrieveValue(String fieldKey) {
		final InfoBean infoBean = retrieveInfoBean();
		return infoBean.get(fieldKey, String.class);
	}
}