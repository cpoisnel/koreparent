package com.kosmos.components.input.model;

import com.kosmos.components.view.model.ComponentViewModel;

/**
 * Created by alexandre.baillif on 29/12/16.
 */
public class KTextViewModel extends ComponentViewModel {

	private String value;

	private String formatValue;

	private int min;

	private int max;

	private int size;

	private String autoComplete;

	private boolean required;

	private int editOption;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFormatValue() {
		return formatValue;
	}

	public void setFormatValue(String formatValue) {
		this.formatValue = formatValue;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getAutoComplete() {
		return autoComplete;
	}

	public void setAutoComplete(String autoComplete) {
		this.autoComplete = autoComplete;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public int getEditOption() {
		return editOption;
	}

	public void setEditOption(int editOption) {
		this.editOption = editOption;
	}
}
