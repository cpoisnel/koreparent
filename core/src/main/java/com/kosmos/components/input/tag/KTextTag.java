package com.kosmos.components.input.tag;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.components.input.model.KTextViewModel;
import com.kosmos.components.tags.AbstractComponentTag;
import com.kportal.core.config.MessageHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Created by alexandre.baillif on 29/12/16.
 */
public class KTextTag extends AbstractComponentTag<KTextViewModel> {


	private static final int DEFAULT_SIZE = 50;

	private static final String DEFAULT_AUTOCOMPLETE = "on";

	private static final String AUTOCOMPLETE_OFF = "nope";

	private int min;

	private int max;

	private int size;

	private String autoComplete;

	public KTextTag() {
		path = "/adminsite/components/inputs/ktext.jsp";
	}

	public void setMin(int min) {
		this.min = min;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setAutoComplete(String autoComplete) {
		this.autoComplete = autoComplete;
	}

	@Override
	protected KTextViewModel buildModel() {
		final FormateurJSP fmt = getFormateur();
		final KTextViewModel model = new KTextViewModel();
		final String name = StringUtils.defaultIfBlank(MessageHelper.getExtensionMessage(extension, label), label);
		final String helpMessage = StringUtils.defaultIfBlank(MessageHelper.getExtensionMessage(extension, tooltip), tooltip);
		int currentIndex = fmt.getIndiceChamp();
		final String formatValue = String.format("%d;%d;%d;%d;LIB=%s;%d", editOption, FormateurJSP.FORMAT_TEXTE, min, max, transformerNamePourFormat(name), currentIndex++);
		fmt.setIndiceChamp(currentIndex);
		model.setName(fieldName);
		model.setLabel(name);
		model.setTooltip(helpMessage);
		model.setValue(StringUtils.defaultString(retrieveValue(fieldName)));
		model.setFormatValue(formatValue);
		model.setMin(min);
		model.setMax(max);
		String autoC = StringUtils.equalsIgnoreCase(autoComplete,DEFAULT_AUTOCOMPLETE) ? DEFAULT_AUTOCOMPLETE : AUTOCOMPLETE_OFF;
		model.setAutoComplete(autoC);
		model.setSize(size > 0 ? size : DEFAULT_SIZE);
		model.setRequired(editOption == FormateurJSP.SAISIE_OBLIGATOIRE);
		model.setEditOption(editOption);
		return model;
	}


	/**
	 * Suppression des ' qui posent problème dans les balises INPUT. Suppression des " qui posent aussi problème
	 *
	 * @param name
	 *            the name
	 *
	 * @return the string
	 */
	private String transformerNamePourFormat(final String name) {
		final String result = StringUtils.replace(name, "\"", " ");
		return StringUtils.replace(result, "'", "\'");
	}




	private FormateurJSP getFormateur() {
		return (FormateurJSP) pageContext.getAttribute("fmt");
	}

	private String retrieveValue(String fieldKey) {
		final InfoBean infoBean = retrieveInfoBean();
		return infoBean.get(fieldKey, String.class);
	}
}
