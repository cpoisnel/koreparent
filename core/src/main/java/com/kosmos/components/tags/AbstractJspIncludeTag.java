package com.kosmos.components.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.kportal.frontoffice.util.JSPIncludeHelper;

/**
 * Created on 14/11/14.
 */
public abstract class AbstractJspIncludeTag extends BodyTagSupport {

    private static final long serialVersionUID = 2119692123398233748L;

    protected String path;

    @Override
    public int doStartTag() throws JspException {
        JSPIncludeHelper.includeJsp(pageContext.getOut(), pageContext.getServletContext(), (HttpServletRequest) pageContext.getRequest(), (HttpServletResponse) pageContext.getResponse(), path);
        return 0;
    }
}
