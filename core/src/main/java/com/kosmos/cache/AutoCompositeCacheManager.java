package com.kosmos.cache;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.cglib.core.Predicate;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.extension.module.AbstractBeanManager;

/**
 * Created on 23/11/15.
 */
public class AutoCompositeCacheManager extends AbstractBeanManager implements CacheManager, InitializingBean {

    private final NoOpCacheManager noOpCacheManager = new NoOpCacheManager();

    private final ConcurrentMap<String, Cache> caches = new ConcurrentHashMap<>(16);

    private boolean fallbackToNoOpCache = false;

    public AutoCompositeCacheManager() {
    }

    public AutoCompositeCacheManager(CacheManager... managers) {
        populateCaches(Arrays.asList(managers));
    }

    private void populateCaches(Collection<CacheManager> managers) {
        for (CacheManager cacheManager : managers) {
            for (String currentCache : cacheManager.getCacheNames()) {
                caches.put(currentCache, cacheManager.getCache(currentCache));
            }
        }
    }

    public void setFallbackToNoOpCache(final boolean fallbackToNoOpCache) {
        this.fallbackToNoOpCache = fallbackToNoOpCache;
    }

    public void setCacheManagers(Collection<CacheManager> cacheManagers) {
        populateCaches(cacheManagers);
    }

    @Override
    public Cache getCache(final String name) {
        if (caches.get(name) == null && fallbackToNoOpCache) {
            caches.put(name, noOpCacheManager.getCache(name));
        }
        return caches.get(name);
    }

    @Override
    public Collection<String> getCacheNames() {
        return caches.keySet();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
    }

    @Override
    public void refresh() {
        final Map<String, CacheManager> cacheManagerMap = ApplicationContextManager.getAllBeansOfType(CacheManager.class);
        final Collection<CacheManager> cacheManagers = cacheManagerMap.values();
        CollectionUtils.filter(cacheManagers, new Predicate() {

            @Override
            public boolean evaluate(final Object o) {
                return !(o instanceof AutoCompositeCacheManager);
            }
        });
        populateCaches(cacheManagers);
    }
}
