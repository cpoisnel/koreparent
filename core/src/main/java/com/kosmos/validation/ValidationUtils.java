package com.kosmos.validation;

import java.util.Arrays;
import java.util.Locale;
import java.util.Set;

import javax.validation.Configuration;
import javax.validation.Constraint;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.hibernate.validator.resourceloading.AggregateResourceBundleLocator;
import org.hibernate.validator.spi.resourceloading.ResourceBundleLocator;

import com.kosmos.validation.message.LocaleMessageInterpolator;

/**
 * Classe utilitaire permettant de valider un bean via la validation api. Si le bean fourni en paramètre contient des
 * {@link Constraint} sur ses champs, ce service s'assurera que celles-ci sont bien respectées.
 */
public final class ValidationUtils {

    private ValidationUtils(){
        // classe utilitaire à ne pas instancier
    }

    /**
     * Permet de valider le bean fourni en paramètre si celui ci contient des {@link Constraint} sur ses champs.
     *
     * @param objectToValidate
     *         le bean contenant des contraintes à valider
     * @param currentLocale
     *         la locale courante permettant d'avoir des messages d'erreurs dans la bonne locale
     * @param idExtension
     *         l'id de l'extension pour laquelle il faut rajouter un bundle de messages
     * @return l'ensemble des violations.
     */
    public static <T> Set<ConstraintViolation<T>> validateBean(T objectToValidate, Locale currentLocale, String idExtension) {
        final ResourceBundleLocator bundleLocator = new AggregateResourceBundleLocator(Arrays.asList(idExtension, "ValidationMessages"));
        final Configuration<?> config = Validation.byDefaultProvider().configure();
        final Validator validator = config.messageInterpolator(new LocaleMessageInterpolator(currentLocale, bundleLocator)).buildValidatorFactory().getValidator();
        return validator.validate(objectToValidate);
    }

    /**
     * Permet de valider le bean fourni en paramètre si celui ci contient des {@link Constraint} sur ses champs.
     *
     * @param objectToValidate
     *         le bean contenant des contraintes à valider
     * @param currentLocale
     *         la locale courante permettant d'avoir des messages d'erreurs dans la bonne locale
     * @return l'ensemble des violations.
     */
    public static <T> Set<ConstraintViolation<T>> validateBean(T objectToValidate, Locale currentLocale) {
        final Configuration<?> config = Validation.byDefaultProvider().configure();
        config.messageInterpolator(new LocaleMessageInterpolator(currentLocale));
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        final Validator validator = factory.getValidator();
        return validator.validate(objectToValidate);
    }

    /**
     * Permet de valider le bean fourni en paramètre si celui ci contient des {@link Constraint} sur ses champs.
     *
     * @param objectToValidate
     *         le bean contenant des contraintes à valider
     * @return l'ensemble des violations.
     */
    public static <T> Set<ConstraintViolation<T>> validateBean(T objectToValidate) {
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        final Validator validator = factory.getValidator();
        return validator.validate(objectToValidate);
    }
}
