package com.kosmos.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.validation.constraint.NotDefaultValue;

/**
 * Validateur servant à vérifier qu'un champ ne correspond pas à la valeur par défaut de kportal/ksup.
 * À savoir non vide et != 0000
 */
public class NotDefaultValueValidator implements ConstraintValidator<NotDefaultValue, String> {

    @Override
    public void initialize(NotDefaultValue constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return StringUtils.isNotBlank(value) && !"0000".equals(value);
    }
}
