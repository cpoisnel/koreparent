package com.kosmos.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.validation.constraint.PasswordSize;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.services.ServiceUserPass;

/**
 * Validateur servant à valider qu'un champ password correspond au valeur défini dans les properties de l'application.
 */
public class PasswordValidator implements ConstraintValidator<PasswordSize, String> {

    @Override
    public void initialize(PasswordSize constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean isValid = Boolean.FALSE;
        if (StringUtils.isNotEmpty(value)) {
            final int min = PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.min", 0);
            final int max = PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.max", ServiceUserPass.LONGUEUR_MOT_DE_PASSE_DEFAUT);
            isValid = value.length() >= min && value.length() <= max;
        }
        return isValid;
    }
}
