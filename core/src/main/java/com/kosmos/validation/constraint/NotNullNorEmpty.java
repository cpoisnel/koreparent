package com.kosmos.validation.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.kosmos.validation.validator.NotNullNorEmptyValidator;

/**
 * Annotation permettant de vérifier qu'une chaine de caractère n'est ni vide ni null.
 */
@Constraint(validatedBy = {NotNullNorEmptyValidator.class})
@Target(value = ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NotNullNorEmpty {

    String message() default "{com.kosmos.validation.constraint.NotNullNorEmpty.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
