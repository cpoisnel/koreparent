package com.kosmos.validation.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.kosmos.validation.validator.PasswordValidator;

/**
 * Interface permettant de valider un champ password pour kportal (avec les contraintes de longueur défini dans les properties
 */
@Constraint(validatedBy = {PasswordValidator.class})
@Target(value = ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordSize {

    String message() default "{com.kosmos.validation.constraint.PasswordSize.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
