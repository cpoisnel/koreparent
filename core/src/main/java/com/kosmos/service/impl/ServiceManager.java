package com.kosmos.service.impl;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.GenericTypeResolver;
import org.springframework.util.ClassUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.service.Service;
import com.kosmos.service.ServiceBean;
import com.kportal.extension.ExtensionManager;

/**
 * Created on 29/09/15.
 */
public class ServiceManager implements Observer {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceManager.class);

    public static final String ID_BEAN = "serviceManager";

    private final Map<Class<?>, ServiceBean<?>> beanServices = new HashMap<>();

    private final Map<Class<? extends Service>, Service> services = new HashMap<>();

    public ExtensionManager extensionManager;

    public void init() {
        refresh();
        extensionManager.addObserver(this);
    }

    public void setExtensionManager(ExtensionManager extensionManager) {
        this.extensionManager = extensionManager;
    }

    private static ServiceManager getServiceManager() {
        return ApplicationContextManager.getCoreContextBean(ID_BEAN, ServiceManager.class);
    }

    public static <S> S getServiceForBean(final Class<?> beanClass) {
        return getServiceManager().getInternalServiceForBean(beanClass);
    }

    public static <S> S getService(final Class<?> serviceClass) {
        return getServiceManager().getInternalService(serviceClass);
    }

    protected <S extends ServiceBean> S getInternalServiceForBean(final Class<?> beanClass) {
        return (S) beanServices.get(beanClass);
    }

    protected <S extends Service> S getInternalService(final Class<?> serviceClass) {
        return (S) services.get(serviceClass);
    }

    private static <T> Class<T> getGenericType(ServiceBean<T> service) {
        Class<?> serviceClass = ClassUtils.getUserClass(service.getClass());
        try {
            final Method method = serviceClass.getMethod("getById", Long.class);
            return (Class<T>) GenericTypeResolver.resolveReturnType(method, service.getClass());
        } catch (final NoSuchMethodException e) {
            LOG.error(String.format("Une erreur est survenue lors de la tentative de récupération de la classe gérée par le service \"%s\"", service.getClass().getName()), e);
        }
        return null;
    }

    public void refresh() {
        final Map<String, Service> foundServices = ApplicationContextManager.getAllBeansOfType(Service.class);
        for(Service currentService : foundServices.values()) {
            if(currentService instanceof ServiceBean) {
                final ServiceBean<?> currentServiceBean = (ServiceBean) currentService;
                beanServices.put(getGenericType(currentServiceBean), currentServiceBean);
            }
            services.put((Class<? extends Service>)ClassUtils.getUserClass(currentService.getClass()), currentService);
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        beanServices.clear();
        services.clear();
        refresh();
    }
}
