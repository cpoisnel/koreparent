package com.kosmos.service.impl;

import com.jsbsoft.jtf.datasource.dao.CommonDAO;
import com.kosmos.service.ServiceBean;
import com.kportal.extension.module.AbstractBeanManager;
import com.univ.objetspartages.bean.PersistenceBean;

/**
 * Created on 15/09/15.
 */
public abstract class AbstractBeanAwareServiceBean<T extends PersistenceBean, D extends CommonDAO<T>> extends AbstractBeanManager implements ServiceBean<T> {

    protected D dao;

    public void setDao(final D dao) {
        this.dao = dao;
    }

    @Override
    public void save(final T bean) {
        if(bean.getId() == null || bean.getId() == 0L) {
            dao.add(bean);
        } else {
            dao.update(bean);
        }
    }

    @Override
    public void delete(final Long id) {
        dao.delete(id);
    }

    @Override
    public T getById(final Long id) {
        return dao.getById(id);
    }
}
