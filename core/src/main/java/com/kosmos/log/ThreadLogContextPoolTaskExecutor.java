package com.kosmos.log;

import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Exécuteur de tâches Spring permettant de gérer la recopie du contexte MDC des logs au démarrage des tâches. L'objectif est d'écrire les messages de logs dans une ressource dédiée.
 *
 * @author cpoisnel
 */
public class ThreadLogContextPoolTaskExecutor extends ThreadPoolTaskExecutor {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1495261894152302266L;

    /**
     * Contexte de log défini pour les tâches exécutées.
     * Null par défaut.
     */
    private String logContext;

    /**
     * Constructeur par défaut.
     * <p>
     *     {@link #logContext} n'est pas initialisé par défaut.
     * </p>
     */
    public ThreadLogContextPoolTaskExecutor() {
        super();
    }

    /**
     * <p>
     * Exécute une tâche en enregistrant pour chaque tâche le contexte d'éxécution du job et le recopiant dans la tâche
     * executée.
     * Si {@link #logContext} est défini, alors le contexte de log est valorisé avec cette valeur.
     * </p>
     * {@inheritDoc}
     */
    @Override
    public void execute(final Runnable task) {
        final String currentLogContext;
        if (null != logContext) {
            currentLogContext =  logContext;
        }else {
            // Recopie des contextes de logs
            currentLogContext = LoggerContextUtils.getLogContext();
        }
        super.execute(new Runnable() {

            /**
             * <p>
             * Enregistre le contexte d'exécution de job, initialise le contexte des logs.
             * </p>
             * {@inheritDoc}
             */
            @Override
            public void run() {
                if (StringUtils.isNotEmpty(currentLogContext)) {
                    LoggerContextUtils.initContextLog(currentLogContext);
                }
                try {
                    task.run();
                } finally {
                    LoggerContextUtils.closeContextLog();
                }
            }
        });

    }

    /**
     * Accès au contexte de log paramétré pour cet exécuteurs de tâches
     * @return le contexte MDC paramétré, null si on reprend celui du parent.
     */
    protected String getLogContext() {
        return logContext;
    }

    public void setLogContext(final String logContext) {
        this.logContext = logContext;
    }
}
