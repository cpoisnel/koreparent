package com.kosmos.userfront.service;

import java.util.List;

import com.kosmos.service.impl.AbstractServiceBean;
import com.kosmos.userfront.bean.UserModerationData;
import com.kosmos.userfront.dao.UserModerationDAO;

/**
 * Created by olivier.camon on 16/01/15.
 */
public class UserModerationService extends AbstractServiceBean<UserModerationData, UserModerationDAO> {

    public List<UserModerationData> getAllUserModerationOrderByCreation() {
        return dao.selectAllOrderByCreattionDate();
    }

}
