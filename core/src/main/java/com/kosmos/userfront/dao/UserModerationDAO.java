package com.kosmos.userfront.dao;

import java.sql.Types;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.UpdateToDataSourceException;
import com.kosmos.userfront.bean.UserModerationData;

/**
 * Created by olivier.camon on 16/01/15.
 */
public class UserModerationDAO extends AbstractCommonDAO<UserModerationData> {

    public UserModerationDAO() {
        this.tableName = "USER_MODERATION";
    }

    public List<UserModerationData> selectAllOrderByCreattionDate() {
        final List<UserModerationData> result;
        try {
            result = namedParameterJdbcTemplate.query("select * from `USER_MODERATION` T1 ORDER BY CREATION_DATE DESC", rowMapper);
        } catch (final DataAccessException dae) {
            throw new UpdateToDataSourceException("An error occured retrieving objects from table USER_MODERATION", dae);
        }
        return result;
    }

    @Override
    protected SqlParameterSource getParameters(final UserModerationData bean) {
        final BeanPropertySqlParameterSource parameterSource = (BeanPropertySqlParameterSource) super.getParameters(bean);
        parameterSource.registerSqlType("moderationState", Types.VARCHAR);
        return parameterSource;
    }
}
