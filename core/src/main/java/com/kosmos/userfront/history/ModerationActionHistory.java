package com.kosmos.userfront.history;

import com.fasterxml.jackson.annotation.JsonView;
import com.kosmos.registration.action.history.ActionHistory;
import com.univ.utils.json.Views;

/**
 * Created by olivier.camon on 09/01/15.
 */
public class ModerationActionHistory extends ActionHistory {

    @JsonView({Views.DaoView.class})
    private boolean isModerated;

    public boolean isModerated() {
        return isModerated;
    }

    public void setModerated(final boolean isModerated) {
        this.isModerated = isModerated;
    }
}
