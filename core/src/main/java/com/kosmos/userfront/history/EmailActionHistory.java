package com.kosmos.userfront.history;

import com.fasterxml.jackson.annotation.JsonView;
import com.kosmos.registration.action.history.ActionHistory;
import com.univ.utils.json.Views;

/**
 * Created by olivier.camon on 09/01/15.
 */
public class EmailActionHistory extends ActionHistory {

    @JsonView({Views.DaoView.class})
    private String generatedKey;

    public String getGeneratedKey() {
        return generatedKey;
    }

    public void setGeneratedKey(final String generatedKey) {
        this.generatedKey = generatedKey;
    }
}
