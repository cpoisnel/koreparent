package com.kosmos.userfront.history;

/**
 * Created by olivier.camon on 12/01/15.
 */
public enum Moderation {
    REFUSED,
    VALIDATED,
    WAITING
}
