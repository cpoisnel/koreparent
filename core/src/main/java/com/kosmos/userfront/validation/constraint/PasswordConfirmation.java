package com.kosmos.userfront.validation.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.kosmos.userfront.validation.validator.PasswordConfirmationValidator;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {PasswordConfirmationValidator.class})
public @interface PasswordConfirmation {

    String message() default "{com.kosmos.userfront.validation.constraint.PasswordConfirmation.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
