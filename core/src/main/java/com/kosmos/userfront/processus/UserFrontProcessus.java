package com.kosmos.userfront.processus;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.TypeUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.exception.ErreurActionNonAutorisee;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurUtilisateurNonAuthentifie;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.registration.process.AbstractRegistrationProcessus;
import com.kosmos.registration.service.impl.RegistrationService;
import com.kosmos.registration.utils.RegistrationUtils;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.userfront.bean.UserModerationData;
import com.kosmos.userfront.history.EmailActionHistory;
import com.kosmos.userfront.module.UserFrontModule;
import com.kosmos.userfront.service.UserModerationService;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;

/**
 * Created by olivier.camon on 10/12/14.
 */
public class UserFrontProcessus extends AbstractRegistrationProcessus {

    public static final String ACTION_FRONT_CREATION = "CREATION";

    public static final String ACTION_FRONT_VALIDER_CREATION = "VALIDER_CREATION";

    public static final String ACTION_FRONT_CONFIRMATION = "CONFIRMATION";

    public static final String ACTION_ACCUEIL = "ACCUEIL";

    public static final String ACTION_DETAIL = "DETAIL";

    public static final String ACTION_VALIDER = "VALIDER";

    public static final String INFOBEAN_REGISTRATION = "REGISTRATIONS";

    public static final String INFOBEAN_ID_MODERATION_DATA = "ID_MODERATION_DATA";

    public UserFrontProcessus(final InfoBean infoBean) {
        super(infoBean);
    }

    @Override
    protected void handlePreRegistration(final String action) throws ErreurApplicative {
        etat = EN_COURS;
        final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (ACTION_ACCUEIL.equals(action) || ACTION_DETAIL.equals(action) || ACTION_VALIDER.equals(action)) {
            if (autorisations == null) {
                throw new ErreurUtilisateurNonAuthentifie();
            } else if (!autorisations.possedePermission(UserFrontModule.getPermissionGestion())) {
                throw new ErreurActionNonAutorisee(MessageHelper.getCoreMessage("BO_OPERATION_INTERDITE"));
            }
        } else if (ACTION_FRONT_CONFIRMATION.equals(action)) {
            final Long registrationId = StringUtils.isNumeric(infoBean.get(REGISTRATION_ID, String.class)) ? Long.valueOf(infoBean.getString(REGISTRATION_ID)) : null;
            if (registrationId != null) {
                final RegistrationService registrationService = ServiceManager.getServiceForBean(Registration.class);
                final Registration currentRegistration = registrationService.getById(registrationId);
                if (currentRegistration != null) {
                    final ActionHistory history = RegistrationUtils.getLastActionHistory(currentRegistration);
                    if (!TypeUtils.isAssignable(history.getClass(), EmailActionHistory.class)) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("FO.USERFRONT.EMAIL_VALIDATION_ALREADY_DONE"));
                    }
                }
            }
        }
    }

    @Override
    protected boolean handlePostRegistration(final String action, final Registration registration) {
        final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        final UserModerationService moderationService = ServiceManager.getServiceForBean(UserModerationData.class);
        if (ACTION_FRONT_CREATION.equals(action)) {
            infoBean.setEcranLogique(ACTION_FRONT_CREATION);
            infoBean.set(REGISTRATION_MODEL_ID, String.valueOf(UserFrontModule.getIdModelUserCreation()));
            infoBean.setTitreEcran(MessageHelper.getCoreMessage("FO.USERFRONT.CREATION_TITLE"));
        } else if (ACTION_FRONT_VALIDER_CREATION.equals(action)) {
            if (registration != null) {
                final String messageConfirm = MessageHelper.getCoreMessage("FO.USERFRONT.VALIDATE_BY_EMAIL");
                infoBean.set("MESSAGE_CONFIRMATION", messageConfirm);
                infoBean.setEcranLogique(ACTION_FRONT_CONFIRMATION);
            } else {
                infoBean.setTitreEcran(MessageHelper.getCoreMessage("FO.USERFRONT.PAGE_HEADER"));
                infoBean.setEcranLogique(ACTION_FRONT_CREATION);
                infoBean.set(REGISTRATION_MODEL_ID, String.valueOf(UserFrontModule.getIdModelUserCreation()));
            }
        } else if (ACTION_FRONT_CONFIRMATION.equals(action) && registration != null) {
            String messageConfirm = MessageHelper.getCoreMessage("FO.USERFRONT.REGISTRATION_CONFIRMATION");
            messageConfirm = String.format(messageConfirm, URLResolver.getAbsoluteUrl(WebAppUtil.CONNEXION_FO, ContexteUtil.getContexteUniv()));
            if (RegistrationState.WAITING.equals(registration.getState())) {
                messageConfirm = MessageHelper.getCoreMessage("FO.USERFRONT.REGISTRATION_CONFIRMATION_VALIDATION");
            }
            infoBean.set("MESSAGE_CONFIRMATION", messageConfirm);
            infoBean.setEcranLogique(ACTION_FRONT_CONFIRMATION);
        } else if (isAuthorized(autorisations) && ACTION_ACCUEIL.equals(action)) {
            infoBean.set(INFOBEAN_REGISTRATION, moderationService.getAllUserModerationOrderByCreation());
            infoBean.setTitreEcran(MessageHelper.getCoreMessage("BO.USERFRONT.USER_CREATION_LIST"));
            infoBean.setEcranLogique(ACTION_ACCUEIL);
        } else if (isAuthorized(autorisations) && ACTION_DETAIL.equals(action)) {
            final UserModerationData data = moderationService.getById(Long.valueOf(infoBean.getString("ID")));
            if (data == null) {
                infoBean.addMessageErreur(MessageHelper.getCoreMessage("ERROR.USERFRONT.DONNEES_NONTROUVE"));
            } else {
                infoBean.set(INFOBEAN_REGISTRATION, data);
                infoBean.set(REGISTRATION_ID, String.valueOf(data.getParentIdRegistration()));
                infoBean.setEcranLogique("DETAIL");
                infoBean.setTitreEcran(data.getEmail());
            }
        } else if (ACTION_VALIDER.equals(action)) {
            infoBean.setEcranLogique(ACTION_ACCUEIL);
            etat = FIN;
        }
        return etat == FIN;
    }

    private boolean isAuthorized(final AutorisationBean autorisations) {
        return autorisations != null && autorisations.possedePermission(UserFrontModule.getPermissionGestion());
    }
}
