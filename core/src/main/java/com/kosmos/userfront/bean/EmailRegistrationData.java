package com.kosmos.userfront.bean;

import org.hibernate.validator.constraints.Email;

import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.validation.constraint.NotNullNorEmpty;

/**
 * Bean contenant toute les infos permettant d'envoyer un email de confirmation
 */
public class EmailRegistrationData implements RegistrationData {

    private static final long serialVersionUID = 697769687841783201L;

    @Email
    @NotNullNorEmpty
    private String email;

    @NotNullNorEmpty
    private String idLocaleKportal;

    @NotNullNorEmpty
    private String processus;

    @NotNullNorEmpty
    private String extension;

    @NotNullNorEmpty
    private String aliasSite;

    private String generatedKey;

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getGeneratedKey() {
        return generatedKey;
    }

    public void setGeneratedKey(final String generatedKey) {
        this.generatedKey = generatedKey;
    }

    public String getIdLocaleKportal() {
        return idLocaleKportal;
    }

    public void setIdLocaleKportal(final String idLocaleKportal) {
        this.idLocaleKportal = idLocaleKportal;
    }

    public String getProcessus() {
        return processus;
    }

    public void setProcessus(final String processus) {
        this.processus = processus;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(final String extension) {
        this.extension = extension;
    }

    public String getAliasSite() {
        return aliasSite;
    }

    public void setAliasSite(final String aliasSite) {
        this.aliasSite = aliasSite;
    }
}
