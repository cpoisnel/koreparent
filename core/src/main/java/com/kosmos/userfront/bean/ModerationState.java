package com.kosmos.userfront.bean;

/**
 * L'état d'une création de compte lorsque la modération est activé.
 */
public enum ModerationState {
    VALIDATED,
    REFUSED,
    NOT_MODERATED
}
