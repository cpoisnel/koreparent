package com.kosmos.userfront.action;

import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.validation.ConstraintViolation;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.joda.time.DateTime;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.core.ProcessusHelper;
import com.jsbsoft.jtf.email.JSBMailbox;
import com.kosmos.registration.action.ActionWithStep;
import com.kosmos.registration.action.bean.EmptyActionConfiguration;
import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.registration.exception.RegistrationExecutionException;
import com.kosmos.registration.exception.RegistrationValidationException;
import com.kosmos.registration.exception.RegistrationViolationValidationException;
import com.kosmos.registration.utils.RegistrationUtils;
import com.kosmos.userfront.bean.EmailRegistrationData;
import com.kosmos.userfront.history.EmailActionHistory;
import com.kosmos.validation.ValidationUtils;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.utils.URLResolver;

/**
 * Classe permettant d'envoyer un email de confirmation et de gérer la validation par email.
 */
public class EmailConfirmation implements ActionWithStep<EmailRegistrationData, EmptyActionConfiguration> {

    private ServiceInfosSite serviceInfosSite;

    public void setServiceInfosSite(final ServiceInfosSite serviceInfosSite) {
        this.serviceInfosSite = serviceInfosSite;
    }

    @Override
    public void validateConfiguration(final EmptyActionConfiguration configuration) throws RegistrationViolationValidationException {
    }

    @Override
    public void validatePrecondition(final EmailRegistrationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionId) throws RegistrationValidationException {
        final Set<ConstraintViolation<EmailRegistrationData>> violations = ValidationUtils.validateBean(data);
        if (CollectionUtils.isNotEmpty(violations)) {
            throw new RegistrationViolationValidationException("BEAN_VALIDATION.ERROR", violations);
        }
    }

    @Override public ActionHistory execute(final EmailRegistrationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionId) throws RegistrationExecutionException {
        final EmailActionHistory history = new EmailActionHistory();
        history.setCreationDate(new Date());
        history.setGeneratedKey(UUID.randomUUID().toString());
        history.setState(RegistrationState.WAITING);
        final Locale locale = LangueUtil.getLocale(data.getIdLocaleKportal());
        final String confirmationEmail = MessageHelper.getCoreMessage(locale, "USERFRONT.EMAIL.CONFIRMATION_BODY");
        final String titleConformation = MessageHelper.getCoreMessage(locale, "USERFRONT.EMAIL.CONFIRMATION_TITLE");
        String urlEmailConfirm = ProcessusHelper.getUrlProcessAction(null, data.getExtension(), data.getProcessus(), "CONFIRMATION", new String[][] {{"REGISTRATION_ID", registration.getId().toString()}, {"email_registration_data.generated_key", history.getGeneratedKey()}, {"KUSER_CONNECTOR", "1"}});
        final InfosSite siteCourant = serviceInfosSite.getInfosSite(data.getAliasSite());
        urlEmailConfirm = URLResolver.getAbsoluteUrl(urlEmailConfirm, siteCourant, URLResolver.LOGIN_FRONT);
        final JSBMailbox mailbox = new JSBMailbox(false);
        try {
            mailbox.sendWmasterMsg(data.getEmail(), String.format(titleConformation, siteCourant.getIntitule()), String.format(confirmationEmail, siteCourant.getIntitule(), StringUtils.replace(urlEmailConfirm, "&amp;", "&")));
        } catch (MessagingException | EmailException e) {
            throw new RegistrationExecutionException("ERROR.USERFRONT.SENGING_MAIL", e);
        }
        return history;
    }

    @Override
    public void validateStepPrecondition(final EmailRegistrationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionID) throws RegistrationValidationException {
        if (data.getGeneratedKey() == null) {
            throw new RegistrationValidationException("ERROR.USERFRONT.EMPTY_GENERATED_KEY");
        }
        final EmailActionHistory lastActionHistory = (EmailActionHistory) RegistrationUtils.getLastActionHistoryFromActionID(registration.getActionHistory(), actionID);
        final DateTime date = DateTime.now();
        final DateTime oneDayBefore = date.minusDays(1);
        if (oneDayBefore.isAfter(lastActionHistory.getCreationDate().getTime())) {
            throw new RegistrationValidationException("ERROR.USERFRONT.EMAIL_EXPIRED");
        }
    }

    @Override
    public ActionHistory executeStep(final EmailRegistrationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionID) throws RegistrationExecutionException {
        final EmailActionHistory lastActionHistory = (EmailActionHistory) RegistrationUtils.getLastActionHistoryFromActionID(registration.getActionHistory(), actionID);
        final EmailActionHistory incomingHistory = new EmailActionHistory();
        incomingHistory.setCreationDate(new Date());
        final String generatedKey = lastActionHistory.getGeneratedKey();
        if (isValidKey(data, generatedKey)) {
            incomingHistory.setState(RegistrationState.FINISHED);
        } else {
            incomingHistory.setState(RegistrationState.ERROR);
        }
        return incomingHistory;
    }

    private boolean isValidKey(final EmailRegistrationData userDatas, final String generatedKey) {
        return StringUtils.isNotBlank(generatedKey) && generatedKey.equals(userDatas.getGeneratedKey());
    }
}
