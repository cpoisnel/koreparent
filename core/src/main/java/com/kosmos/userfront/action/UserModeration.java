package com.kosmos.userfront.action;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import javax.mail.MessagingException;
import javax.validation.ConstraintViolation;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.email.JSBMailbox;
import com.kosmos.registration.action.ActionWithStep;
import com.kosmos.registration.action.bean.EmptyActionConfiguration;
import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.registration.exception.RegistrationExecutionException;
import com.kosmos.registration.exception.RegistrationValidationException;
import com.kosmos.registration.exception.RegistrationViolationValidationException;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.userfront.bean.ModerationState;
import com.kosmos.userfront.bean.UserModerationData;
import com.kosmos.userfront.module.UserFrontModule;
import com.kosmos.userfront.service.UserModerationService;
import com.kosmos.validation.ValidationUtils;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.MailUtil;
import com.univ.utils.URLResolver;

/**
 * Registration permettant de gérer la modération d'une création de compte en BO.
 * Si elle est activé, l'action suivante est lancé uniquement si le compte est bien validé à cette étape.
 *
 */
public class UserModeration implements ActionWithStep<UserModerationData, EmptyActionConfiguration> {

    public static final String ID_BEAN = "moderationAction";

    public static final String PROPERTY_IS_ACTIVE = "moderation.backoffice";

    private static final Logger LOG = LoggerFactory.getLogger(UserModeration.class);

    private ServiceUser serviceUser;

    private ServiceInfosSite serviceInfosSite;

    public void setServiceUser(final ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    public void setServiceInfosSite(final ServiceInfosSite serviceInfosSite) {
        this.serviceInfosSite = serviceInfosSite;
    }

    @Override
    public void validateConfiguration(final EmptyActionConfiguration configuration) throws RegistrationViolationValidationException {
    }

    @Override
    public void validatePrecondition(final UserModerationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionId) throws RegistrationValidationException {
        if (isModerationActive()) {
            final Set<ConstraintViolation<UserModerationData>> violations = ValidationUtils.validateBean(data);
            if (CollectionUtils.isNotEmpty(violations)) {
                throw new RegistrationViolationValidationException("BEAN_VALIDATION.ERROR", violations);
            }
        }
    }

    @Override
    public ActionHistory execute(final UserModerationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionId) throws RegistrationExecutionException {
        final ActionHistory history = new ActionHistory();
        history.setCreationDate(new Date());
        if (isModerationActive()) {
            final Locale locale = LangueUtil.getDefaultLocale();
            final String confirmationEmail = MessageHelper.getCoreMessage(locale, "USERFRONT.EMAIL_GESTIONNAIRE.CONFIRMATION_BODY");
            final String titleConformation = MessageHelper.getCoreMessage(locale, "USERFRONT.EMAIL_GESTIONNAIRE.CONFIRMATION_TITLE");
            final InfosSite siteCourant = serviceInfosSite.getPrincipalSite();
            final String urlBackOffice = URLResolver.getAbsoluteUrl(WebAppUtil.CONNEXION_BO, siteCourant, URLResolver.STANDARD_ACTION);
            final Collection<String> administratorsEmails = getAdminsitratorsEmails();
            final JSBMailbox mailbox = new JSBMailbox(false);
            try {
                for (final String email : administratorsEmails) {
                    if (MailUtil.verifMail(email)) {
                        mailbox.sendWmasterMsg(email, String.format(titleConformation, siteCourant.getIntitule()), String.format(confirmationEmail, siteCourant.getIntitule(), urlBackOffice));
                    }
                }
            } catch (MessagingException | EmailException e) {
                LOG.error("Impossible d'envoyer un email aux adminsitrateurs de création de compte", e);
            }
            final UserModerationService service = ServiceManager.getServiceForBean(UserModerationData.class);
            data.setCreationDate(registration.getCreationDate());
            data.setModerationState(ModerationState.NOT_MODERATED);
            data.setParentIdRegistration(registration.getId());
            service.save(data);
            history.setState(RegistrationState.WAITING);
        } else {
            data.setCreationDate(registration.getCreationDate());
            data.setParentIdRegistration(registration.getId());
            history.setState(RegistrationState.FINISHED);
        }
        return history;
    }

    @SuppressWarnings("unchecked")
    private Collection<String> getAdminsitratorsEmails() {
        final PermissionBean permission = UserFrontModule.getPermissionGestion();
        Collection<String> emailUtilisateurs;
        try {
            final Collection<UtilisateurBean> utilisateurs = serviceUser.getUtilisateursPossedantPermission(permission);
            emailUtilisateurs = CollectionUtils.collect(utilisateurs, new Transformer() {

                @Override
                public Object transform(final Object input) {
                    final UtilisateurBean user = (UtilisateurBean) input;
                    return user.getAdresseMail();
                }
            });
        } catch (final Exception e) {
            LOG.error("impossible de récupérer la liste des utilisateurs ayant la permiission de modération", e);
            emailUtilisateurs = Collections.emptyList();
        }
        return emailUtilisateurs;
    }

    @Override
    public void validateStepPrecondition(final UserModerationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionID) throws RegistrationValidationException {
    }

    @Override
    public ActionHistory executeStep(final UserModerationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionID) throws RegistrationExecutionException {
        final ActionHistory history = new ActionHistory();
        history.setCreationDate(new Date());
        history.setCreationId(identity);
        if (isModerationActive()) {
            final UserModerationService service = ServiceManager.getServiceForBean(UserModerationData.class);
            if (ModerationState.REFUSED.equals(data.getModerationState())) {
                history.setState(RegistrationState.ERROR);
                final Locale locale = LangueUtil.getLocale(data.getIdLocaleKportal());
                String confirmationEmail = MessageHelper.getCoreMessage(locale, "USERFRONT.EMAIL.REFUSED_BODY");
                if (StringUtils.isEmpty(data.getModerationMessage())) {
                    confirmationEmail = MessageHelper.getCoreMessage(locale, "USERFRONT.EMAIL.REFUSED_BODY_EMPTY_MSG");
                }
                final String titleConformation = MessageHelper.getCoreMessage(locale, "USERFRONT.EMAIL.REFUSED_TITLE");
                final InfosSite siteCourant = serviceInfosSite.getInfosSite(data.getAliasSite());
                final JSBMailbox mailbox = new JSBMailbox(false);
                try {
                    mailbox.sendWmasterMsg(data.getEmail(), String.format(titleConformation, siteCourant.getIntitule()), String.format(confirmationEmail, data.getModerationMessage()));
                } catch (MessagingException | EmailException e) {
                    throw new RegistrationExecutionException("ERROR.USERFRONT.SENGING_MAIL", e);
                }
            } else if (ModerationState.VALIDATED.equals(data.getModerationState())) {
                history.setState(RegistrationState.FINISHED);
            }
            service.save(data);
        } else {
            history.setState(RegistrationState.FINISHED);
        }
        return history;
    }

    private boolean isModerationActive() {
        return PropertyHelper.getCorePropertyAsBoolean(PROPERTY_IS_ACTIVE);
    }
}
