package com.kosmos.userfront.action;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.mail.MessagingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.mail.EmailException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.email.JSBMailbox;
import com.kosmos.registration.action.DefaultActionWithoutConfiguration;
import com.kosmos.registration.action.bean.EmptyActionConfiguration;
import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.registration.exception.RegistrationExecutionException;
import com.kosmos.registration.exception.RegistrationValidationException;
import com.kosmos.registration.service.impl.RegistrationService;
import com.kosmos.userfront.bean.UserRegistrationData;
import com.kosmos.userfront.history.EmailActionHistory;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.security.MySQLHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.URLResolver;

/**
 * Classe permettant de transformer une Registration en Utilisateur K-Portal.
 * La validation gère l'unicité du login sur les Registration précédente ainsi que sur les utilisateurs précédents.
 *
 */
public class DefaultUserCreation extends DefaultActionWithoutConfiguration<UserRegistrationData> {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultUserCreation.class);

    private ServiceUser serviceUser;

    private RegistrationService registrationService;

    private ServiceInfosSite serviceInfosSite;

    public void setServiceUser(final ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    public void setRegistrationService(final RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    public void setServiceInfosSite(final ServiceInfosSite serviceInfosSite) {
        this.serviceInfosSite = serviceInfosSite;
    }

    @Override
    public void validatePrecondition(final UserRegistrationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionId) throws RegistrationValidationException {
        super.validatePrecondition(data, configuration, registration, identity, actionId);
        final String login = data.getLogin();
        UtilisateurBean utilisateurByLogin = null;
        utilisateurByLogin = serviceUser.getByCode(login);
        if (utilisateurByLogin != null && utilisateurByLogin.getCode().equals(login)) {
            throw new RegistrationValidationException("ERROR.USERFRONT.LOGIN_ALREADY_EXIST");
        }
        try {
            data.setPassword(MySQLHelper.encodePassword(data.getPassword()));
            data.setPasswordConfirmation(data.getPassword());
        } catch (final UnsupportedEncodingException e) {
            LOG.info("l'encodage du password n'a pas fonctionné", e);
        }
        if (registration != null) {
            final Long registrationId = registration.getId();
            final Collection<Registration> thisModelRegistrations = registrationService.getRegistrationsByModel(registration.getModelId());
            final boolean existValues = CollectionUtils.exists(thisModelRegistrations, new Predicate() {

                @Override
                public boolean evaluate(final Object object) {
                    final Registration registration = (Registration) object;
                    final UserRegistrationData dataValues = (UserRegistrationData) registration.getDataByAction().get(actionId);
                    final List<Pair<String, ActionHistory>> actionsHistory = registration.getActionHistory();
                    final boolean isValid = isWaitingForValidEmailValidation(registration, actionsHistory);
                    return isValid && !(RegistrationState.ERROR.equals(registration.getState()) || RegistrationState.FINISHED.equals(registration.getState())) && !registration.getId().equals(registrationId) && login.equals(dataValues.getLogin());
                }

                private boolean isWaitingForValidEmailValidation(final Registration registration, final List<Pair<String, ActionHistory>> actionsHistory) {
                    boolean isValid = Boolean.TRUE;
                    final DateTime now = DateTime.now();
                    final DateTime yesterday = now.minusDays(1);
                    if (actionsHistory.size() >= 1 && actionsHistory.get(actionsHistory.size() - 1).getValue() instanceof EmailActionHistory && RegistrationState.WAITING.equals(registration.getState())) {
                        isValid = yesterday.isBefore(registration.getCreationDate().getTime());
                    }
                    return isValid;
                }
            });
            if (existValues) {
                throw new RegistrationValidationException("ERROR.USERFRONT.LOGIN_ALREADY_EXIST");
            }
        }
    }

    @Override
    public ActionHistory execute(final UserRegistrationData data, final EmptyActionConfiguration configuration, final Registration registration, final String identity, final String actionId) throws RegistrationExecutionException {
        final UtilisateurBean userToCreate = new UtilisateurBean();
        userToCreate.setCode(data.getLogin());
        userToCreate.setNom(data.getLastName());
        userToCreate.setPrenom(data.getName());
        userToCreate.setMotDePasse(data.getPassword());
        userToCreate.setAdresseMail(data.getEmail());
        setNotByDefaultValue(data, userToCreate);
        serviceUser.save(userToCreate);
        final ActionHistory history = new ActionHistory();
        final Locale locale = LangueUtil.getLocale(data.getIdLocaleKportal());
        final String emailConfirmationTitle = MessageHelper.getCoreMessage(locale, "USERFRONT.EMAIL.ACCOUNT_CREAIED");
        final InfosSite infosSite = serviceInfosSite.getInfosSite(data.getAliasSite());
        final String identificationUrl = URLResolver.getAbsoluteUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION_FRONT&ACTION=CONNECTER&KUSER_CONNECTOR=1", infosSite, URLResolver.LOGIN_FRONT);
        final String emailConfirmationContent = MessageHelper.getCoreMessage(locale, "USERFRONT.EMAIL.ACCOUNT_CREATED_BODY");
        final JSBMailbox mailbox = new JSBMailbox(false);
        try {
            mailbox.sendWmasterMsg(data.getEmail(), emailConfirmationTitle, String.format(emailConfirmationContent, identificationUrl));
        } catch (MessagingException | EmailException e) {
            LOG.error("impossible d'envoyer un email à l'utilisateur ayant créé un compte", e);
        }
        history.setCreationDate(new Date());
        history.setState(RegistrationState.FINISHED);
        return history;
    }

    private void setNotByDefaultValue(final UserRegistrationData data, final UtilisateurBean userToCreate) {
        if (data.getBirthdate() != null) {
            userToCreate.setDateNaissance(data.getBirthdate());
        }
        if (data.getTitle() != null) {
            userToCreate.setCivilite(data.getTitle());
        }
        if (data.getInstitutionsCodes() != null) {
            userToCreate.setCodeRattachement(data.getInstitutionsCodes());
        }
        if (data.getGroups() != null) {
            userToCreate.setGroupes(data.getGroups());
        }
        if (data.getValidationRestriction() != null) {
            userToCreate.setRestrictionValidation(data.getValidationRestriction());
        }
        if (data.getEditionExtension() != null) {
            userToCreate.setExtensionModification(data.getEditionExtension());
        }
        if (data.getInterests() != null) {
            userToCreate.setCentresInteret(data.getInterests());
        }
        if (data.getProfiles() != null) {
            userToCreate.setProfilDsi(data.getProfiles());
        }
        if (data.getDsiGroups() != null) {
            userToCreate.setGroupesDsi(data.getDsiGroups());
        }
        if (data.getLdapCode() != null) {
            userToCreate.setCodeLdap(data.getLdapCode());
        }
        if (data.getDsiGroupsFromImport() != null) {
            userToCreate.setGroupesDsiImport(data.getDsiGroupsFromImport());
        }
        if (data.getRoles() != null) {
            userToCreate.setRoles(data.getRoles());
        }
        if (data.getDefaultProfile() != null) {
            userToCreate.setProfilDefaut(data.getDefaultProfile());
        }
        if (data.getMailFormat() != null) {
            userToCreate.setFormatEnvoi(data.getMailFormat());
        }
        if (data.getExpertMode() != null) {
            userToCreate.setModeSaisieExpert(data.getExpertMode());
        }
    }
}
