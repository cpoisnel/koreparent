package com.kosmos.userfront.module;

import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.ProcessusHelper;
import com.kosmos.registration.action.bean.EmptyActionConfiguration;
import com.kosmos.registration.bean.Model;
import com.kosmos.registration.bean.ModelDescriptor;
import com.kosmos.registration.service.impl.ModelService;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.extension.module.DefaultModuleImpl;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.ModuleHelper;
import com.univ.objetspartages.om.PermissionBean;

public class UserFrontModule extends DefaultModuleImpl {

    public static final String ID_MODEL_DESCRIPTOR_USER_CREATION = "userCreationDescription";

    public static final String MANAGEMENT_ACTION_CODE = "G";

    /**
     * La permission permettant de gérer les créations de compte en front
     */
    public static final String CODE_PERMISSION = "userFront";

    private static final String ID_BEAN = "userFrontModule";

    public static IModule getModule() {
        return ModuleHelper.getModule(ApplicationContextManager.DEFAULT_CORE_CONTEXT, ID_BEAN);
    }

    public static boolean isActiveModule() {
        return getModule() != null && IModule.ETAT_ACTIF == getModule().getEtat() && ApplicationContextManager.getCoreContextBean(ID_MODEL_DESCRIPTOR_USER_CREATION) != null;
    }

    /**
     * Retourne la permission de gestion des sites.
     */
    public static PermissionBean getPermissionGestion() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, MANAGEMENT_ACTION_CODE);
    }

    public static String getUrlUserRegistrationFront() {
        String result = StringUtils.EMPTY;
        if (isActiveModule()) {
            result = ProcessusHelper.getUrlProcessAction(null, ApplicationContextManager.DEFAULT_CORE_CONTEXT, "USERFRONT_FRONT", "CREATION", null);
        }
        return result;
    }

    public static long getIdModelUserCreation() {
        final ModelService modelService = ServiceManager.getServiceForBean(Model.class);
        final ModelDescriptor descriptorAccount = ApplicationContextManager.getCoreContextBean(ID_MODEL_DESCRIPTOR_USER_CREATION, ModelDescriptor.class);
        final Collection<Model> modelsByDescriptor = modelService.getByModelDescriptorID(descriptorAccount.getId());
        final Long idModel;
        if (CollectionUtils.isNotEmpty(modelsByDescriptor)) {
            idModel = modelsByDescriptor.iterator().next().getId();
        } else {
            final Model myModel = new Model();
            myModel.setModelDescriptorId(descriptorAccount.getId());
            myModel.setActionConfigurationById(new HashMap<String, EmptyActionConfiguration>());
            modelService.create(myModel);
            idModel = myModel.getId();
        }
        return idModel;
    }
}
