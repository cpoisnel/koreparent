package com.kosmos.userfront.extension.utils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.ModuleHelper;
import com.univ.objetspartages.om.PermissionBean;

/**
 * Created by olivier.camon on 19/01/15.
 */
public class UserFrontUtils {

    public static final String ID_BEAN = "userFront";

    /**
     * La permission permettant de modifier des sites
     */
    public static final String CODE_ACTION_MODIFICATION = "M";

    public static final String CODE_PERMISSION = "userCreation";

    /**
     * La permission permettant de gérer les sites (Create Update Delete)
     */
    public static final String CODE_ACTION_GESTION = "G";

    public static IModule getModule() {
        return ModuleHelper.getModule(ApplicationContextManager.DEFAULT_CORE_CONTEXT, ID_BEAN);
    }

    /**
     * Retourne la permission de gestion des sites.
     */
    public static PermissionBean getPermissionGestion() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, CODE_ACTION_GESTION);
    }

    /**
     * Retourne la permission de modification des sites.
     */
    public static PermissionBean getPermissionModification() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, CODE_ACTION_MODIFICATION);
    }
}
