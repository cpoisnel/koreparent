package com.jsbsoft.jtf.session;

/**
 * Partie de la session utilisateur qui n'est pas dédié à un contexte particulier.
 */
public class SessionParDefaut extends SousSession {

    /**
     * SessionHorsPartenaire constructor comment.
     *
     * @param s
     *            Session
     */
    public SessionParDefaut(SessionUtilisateur s) {
        super(s);
    }
}
