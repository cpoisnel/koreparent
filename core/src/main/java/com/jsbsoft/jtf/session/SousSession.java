package com.jsbsoft.jtf.session;

import java.util.Hashtable;

import com.jsbsoft.jtf.core.ProcedureBean;

/**
 * Partie d'une session (dédiée ou pas à un partenaire).
 */
public abstract class SousSession {

    /** Ensemble des beans de la sous-session, indexés par leur identifiant. */
    private Hashtable<String, ProcedureBean> beans = new Hashtable<>();

    /** La session utilisateur qui contient cette sous-session. */
    private SessionUtilisateur sessionUtilisateur;

    /**
     * Constructeur unique.
     *
     * @param s
     *            com.jsbsoft.jtf.session.Session
     */
    public SousSession(SessionUtilisateur s) {
        this.setSessionUtilisateur(s);
    }

    /**
     * ajouter un bean PU à la sous-session.
     *
     * @param unBean
     *            the un bean
     */
    public void ajouterProcedureBean(com.jsbsoft.jtf.core.ProcedureBean unBean) {
        beans.put(unBean.idBean(), unBean);
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (27/01/00 17:22:27)
     *
     * @return java.util.Hashtable
     */
    protected Hashtable<String, ProcedureBean> getBeans() {
        return beans;
    }

    /**
     * This method was created in VisualAge.
     *
     * @return com.jsbsoft.jtf.session.SessionUtilisateur
     */
    public SessionUtilisateur getSessionUtilisateur() {
        return sessionUtilisateur;
    }

    /**
     * Retirer un bean PU (sans le lire).
     *
     * @param idBean
     *            the id bean
     */
    public void retirerProcedureBean(String idBean) {
        beans.remove(idBean);
    }

    /**
     * Lire un bean PU (sans le retirer)<br>
     * protected cr elle n'est appelée que par la SessionUtilisateur.
     *
     * @param idBean
     *            the id bean
     *
     * @return the procedure bean
     */
    public com.jsbsoft.jtf.core.ProcedureBean retrouverProcedureBeanparID(String idBean) {
        return beans.get(idBean);
    }

    /**
     * Accesseur privé.
     *
     * @param newValue
     *            com.jsbsoft.jtf.session.Session
     */
    private void setSessionUtilisateur(SessionUtilisateur newValue) {
        this.sessionUtilisateur = newValue;
    }
}