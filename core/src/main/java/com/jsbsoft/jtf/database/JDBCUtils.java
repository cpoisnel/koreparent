package com.jsbsoft.jtf.database;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JDBCUtils {

    /** The Constant LOG. */
    private static final Logger LOGGER = LoggerFactory.getLogger(JDBCUtils.class);

    protected static Logger tempLogger = null;

    public static boolean existe(final Connection connection, final String nomTable) throws SQLException {
        boolean existe;
        final DatabaseMetaData dmd = connection.getMetaData();
        final ResultSet tables = dmd.getTables(connection.getCatalog(), null, nomTable, null);
        existe = tables.next();
        tables.close();
        return existe;
    }

    /**
     * Ajouter index.
     *
     * @param connection
     *            connection
     * @param table
     *            the table
     * @param champ
     *            the champ
     *
     * @throws SQLException
     *             the exception
     */
    public static void ajouterIndex(final Connection connection, final String table, final String champ) throws SQLException {
        final String items[] = champ.split("\\*", -2);
        final String nomChamp = items[0];
        String lg = "";
        if (items.length == 2) {
            lg = "(" + items[1] + ")";
        }
        final String commande = "ALTER TABLE " + table + " ADD INDEX IDX_" + nomChamp + "(" + nomChamp + lg + ")";
        try (PreparedStatement stmt = connection.prepareStatement(commande);){
            stmt.executeUpdate();
            getLogger().info(commande);
        }
    }

    /**
     * Lire indexs.
     *
     * @param connection
     *            connection
     * @param table
     *            the table
     *
     * @return the list
     *
     * @throws SQLException
     *             the exception
     */
    public static List<String> lireIndexs(final Connection connection, final String table) throws SQLException {
        final List<String> listIndexs = new ArrayList<>();
        final String commande = "SHOW INDEX FROM " + table;
        try (PreparedStatement stmt = connection.prepareStatement(commande);
             ResultSet rs = stmt.executeQuery(); ){
            while (rs.next()) {
                listIndexs.add(rs.getString("Column_name").toUpperCase());
            }
        }
        return listIndexs;
    }

    /**
     * Mettre a jour indexs table.
     *
     * @param connection
     *            connection
     * @param table
     *            the table
     * @param listeTotaleIndexs
     *            the liste totale indexs
     */
    public static void mettreAJourIndexsTable(final Connection connection, final String table, final List<String> listeTotaleIndexs) throws SQLException {
        // Lecture des indexs courants
        final List<String> listeIndexsLus = lireIndexs(connection, table);
        // Boucle sur tous les indexs à mettre en place
        for (String indexCourant : listeTotaleIndexs) {
            final String items[] = indexCourant.split("\\*", -2);
            final String nomChamp = items[0];
            if ("DIFFUSION_PUBLIC_VISE".equals(nomChamp) || "DIFFUSION_PUBLIC_VISE_RESTRICTION".equals(nomChamp) || "META_DIFFUSION_PUBLIC_VISE".equals(nomChamp) || "META_DIFFUSION_PUBLIC_VISE_RESTRICTION".equals(nomChamp)) {
                final boolean misAjour = mettreAJourChamp(connection, table, nomChamp);
                if (misAjour && listeIndexsLus.contains(nomChamp)) {
                    listeIndexsLus.remove(nomChamp);
                }
            }
            if (!listeIndexsLus.contains(nomChamp)) {
                ajouterIndex(connection, table, indexCourant);
            }
        }
    }

    /**
     * Mettre a jour champ.
     *
     * @param connexion
     *            the connexion
     * @param table
     *            the table
     * @param champ
     *            the champ
     *
     * @return true, if successful
     *
     * @throws SQLException
     *             the exception
     */
    public static boolean mettreAJourChamp(final Connection connexion, final String table, final String champ) throws SQLException {
        final String commande = "DESC " + table;
        boolean misAJour = false;
        try (PreparedStatement stmt = connexion.prepareStatement(commande);
             ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                final String nomColonne = rs.getString("Field");
                if (nomColonne.equalsIgnoreCase(champ)) {
                    if (!"text".equalsIgnoreCase(rs.getString("Type"))) {
                        if (!"".equalsIgnoreCase(rs.getString("Key"))) {
                            final String nomIndex = lireNomIndex(connexion, table, champ);
                            supprimerIndex(connexion, table, nomIndex);
                        }
                        changerTypeChamp(connexion, table, champ);
                        misAJour = true;
                    }
                }
            }
        }
        return misAJour;
    }

    /**
     * Changer type champ.
     *
     * @param connexion
     *            the connexion
     * @param table
     *            the table
     * @param nomChamp
     *            the nom champ
     *
     * @throws SQLException
     *             the exception
     */
    public static void changerTypeChamp(final Connection connexion, final String table, final String nomChamp) throws SQLException {
        final String commande = "ALTER TABLE " + table + " CHANGE " + nomChamp + " " + nomChamp + " TEXT";
        try (PreparedStatement stmt =  connexion.prepareStatement(commande);){
            stmt.executeUpdate();
            getLogger().info(commande);
        }
    }

    /**
     * Supprimer index.
     *
     * @param connexion
     *            the ctx
     * @param table
     *            the table
     * @param nomIndex
     *            the nom index
     *
     * @throws SQLException
     *             the exception
     */
    public static void supprimerIndex(final Connection connexion, final String table, final String nomIndex) throws SQLException {
        final String commande = "ALTER TABLE " + table + " DROP INDEX " + nomIndex;
        try (PreparedStatement stmt = connexion.prepareStatement(commande);){
            stmt.executeUpdate();
            getLogger().info(commande);
        }
    }

    /**
     * Lire nom index.
     *
     * @param connexion
     *            the ctx
     * @param table
     *            the table
     * @param champ
     *            the champ
     *
     * @return the string
     *
     * @throws SQLException
     *             the exception
     */
    public static String lireNomIndex(final Connection connexion, final String table, final String champ) throws SQLException {
        final String commande = "SHOW INDEX FROM " + table;
        String res = "";
        try (PreparedStatement stmt = connexion.prepareStatement(commande);
             ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                final String nomColonne = rs.getString("Column_name");
                if (nomColonne.equalsIgnoreCase(champ)) {
                    res = rs.getString("Key_name");
                }
            }
        }
        return res;
    }

    public static Logger getLogger() {
        return (tempLogger != null ? tempLogger : LOGGER);
    }

    public static void setTempLogger(final Logger _tempLogger) {
        tempLogger = _tempLogger;
    }

    /**
     * Met à jour l'auto-increment en ajoutant à la valeur courante le nombre de lignes à insérer.
     * @param table : Nom de la table pour laquelle on souhaite la valeur de l'auto-increment
     * @param nbRow : nombre de ligne à insérer
     * @return
     * @throws SQLException
     */
    public static Long updateTableAutoIncrement(final Connection connexion, final String table, final Long nbRow) throws SQLException {
        Long currentValue = getAutoIncrement(connexion, table);
        if (nbRow > 0) {
            String commande = String.format("ALTER TABLE `%s` AUTO_INCREMENT = %d", table, currentValue + nbRow);
            try (PreparedStatement stmt = connexion.prepareStatement(commande);
                 ResultSet rs = stmt.executeQuery();) {
            }
        }
        return currentValue + nbRow;
    }

    /**
     * Retourne le prochain increment pour une table données.
     * @param table : Nom de la table pour laquelle on souhaite connaitre la valeur de l'auto-increment
     * @return
     * @throws SQLException
     */
    public static Long getAutoIncrement(final Connection connexion, final String table) throws SQLException{
        String commande = String.format("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = '%s' AND TABLE_SCHEMA = '%s'", table, connexion.getCatalog());
        Long res = 0L;
        try (PreparedStatement stmt = connexion.prepareStatement(commande);
             ResultSet rs = stmt.executeQuery();){
            rs.next();
            res = rs.getLong(1);
        }
        return res;
    }
}
