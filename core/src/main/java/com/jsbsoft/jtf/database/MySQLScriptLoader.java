package com.jsbsoft.jtf.database;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MySQLScriptLoader {

    private static final Logger LOG = LoggerFactory.getLogger(MySQLScriptLoader.class);

    private static final String LINE_SEPARATOR = System.getProperty("line.separator", "\n");

    private static final String DEFAULT_DELIMITER = ";";

    private static final String DROP_TABLE_COMMAND = "DROP TABLE ?";

    private static final String TRUNCATE_TABLE_COMMAND = "DELETE FROM ?";

    private final Connection connection;

    private final List<String> createdTables = new ArrayList<>();

    private final List<String> existingTables = new ArrayList<>();

    private boolean stopOnError;

    private boolean onlyCreateTable;

    private boolean removeCRs;

    private boolean escapeProcessing = true;

    private String delimiter = DEFAULT_DELIMITER;

    private boolean fullLineDelimiter = false;

    public MySQLScriptLoader(final Connection connection) {
        this.connection = connection;
    }

    public void setStopOnError(final boolean stopOnError) {
        this.stopOnError = stopOnError;
    }

    public void setRemoveCRs(final boolean removeCRs) {
        this.removeCRs = removeCRs;
    }

    public void setEscapeProcessing(final boolean escapeProcessing) {
        this.escapeProcessing = escapeProcessing;
    }

    public void setDelimiter(final String delimiter) {
        this.delimiter = delimiter;
    }

    public void setFullLineDelimiter(final boolean fullLineDelimiter) {
        this.fullLineDelimiter = fullLineDelimiter;
    }

    public List<String> getCreatedTables() {
        return createdTables;
    }

    public void runScript(final Reader reader) throws Exception {
        try {
            executeLineByLine(reader);
        } finally {
            rollbackConnection();
        }
    }

    private void executeLineByLine(final Reader reader) throws Exception {
        StringBuilder command = new StringBuilder();
        try {
            final BufferedReader lineReader = new BufferedReader(reader);
            String line;
            while ((line = lineReader.readLine()) != null) {
                command = handleLine(command, line);
            }
            commitConnection();
            checkForMissingLineTerminator(command);
        } catch (final Exception e) {
            if (stopOnError) {
                for (final String table : createdTables) {
                    executeDropTable(table);
                }
            }
            throw new Exception("Error executing: " + command + ".  Cause: " + e, e);
        }
    }

    private void commitConnection() throws Exception {
        try {
            if (!connection.getAutoCommit()) {
                connection.commit();
            }
        } catch (final SQLException t) {
            throw new Exception("Could not commit transaction. Cause: " + t, t);
        }
    }

    private void rollbackConnection() {
        try {
            if (!connection.getAutoCommit()) {
                connection.rollback();
            }
        } catch (final SQLException e) {
            LOG.error("unable to rollback the commit", e);
        }
    }

    private void checkForMissingLineTerminator(final StringBuilder command) throws Exception {
        if (command != null && command.toString().trim().length() > 0) {
            throw new Exception("Line missing end-of-line terminator (" + delimiter + ") => " + command);
        }
    }

    private StringBuilder handleLine(final StringBuilder command, final String line) throws SQLException, UnsupportedEncodingException {
        final String trimmedLine = line.trim();
        if (!lineIsComment(trimmedLine) && commandReadyToExecute(trimmedLine)) {
            command.append(line.substring(0, line.lastIndexOf(delimiter)));
            command.append(LINE_SEPARATOR);
            // execution de la commande
            executeStatement(command.toString());
            // on vide la requete courante
            command.setLength(0);
        } else if (!lineIsComment(trimmedLine) && trimmedLine.length() > 0) {
            command.append(line);
            command.append(LINE_SEPARATOR);
        }
        return command;
    }

    private boolean lineIsComment(final String trimmedLine) {
        return trimmedLine.startsWith("//") || trimmedLine.startsWith("--");
    }

    private boolean commandReadyToExecute(final String trimmedLine) {
        return !fullLineDelimiter && trimmedLine.contains(delimiter) || fullLineDelimiter && trimmedLine.equals(delimiter);
    }

    private void executeStatement(final String command) throws SQLException, UnsupportedEncodingException {
        String sql = command;
        if (removeCRs) {
            sql = sql.replaceAll("\r\n", "\n");
        }
        String table = null;
        // commande de creation de table
        if (isCommandCreateTable(sql)) {
            table = getTableFromCommand(sql);
            if (JDBCUtils.existe(connection, table)) {
                existingTables.add(table);
                return;
            }
        } else if (onlyCreateTable) {
            return;
        }
        boolean hasResults = false;
        final Statement statement = connection.createStatement();
        statement.setEscapeProcessing(escapeProcessing);
        if (stopOnError) {
            hasResults = statement.execute(sql);
            // ajout de la table crée
            if (StringUtils.isNotEmpty(table)) {
                createdTables.add(table);
            }
        } else {
            try {
                hasResults = statement.execute(sql);
                if (StringUtils.isNotEmpty(table)) {
                    createdTables.add(table);
                }
            } catch (final SQLException e) {
                LOG.error("Error executing: " + command, e);
            }
        }
        printResults(statement, hasResults);
        try {
            statement.close();
        } catch (final SQLException e) {
            LOG.error("unable to close the statement", e);
        }
    }

    private String getTableFromCommand(String command) {
        command = StringUtils.remove(command, " ");
        command = StringUtils.remove(command, "`");
        final Pattern compile = Pattern.compile("CREATETABLE([\\S&&[^\\(]]+)[\\(]*");
        final Matcher matcher = compile.matcher(command.toUpperCase());
        if (matcher.find()) {
            return matcher.group(1);
        }
        return StringUtils.EMPTY;
    }

    private boolean isCommandCreateTable(final String command) {
        return StringUtils.startsWithIgnoreCase(StringUtils.remove(command, " "), "CREATETABLE");
    }

    public void executeDropTable(final String table) {
        PreparedStatement statement;
        try {
            //MySQL doesn't support prepared statements with variable table names
            statement = connection.prepareStatement(StringUtils.replace(DROP_TABLE_COMMAND, "?", table));
            statement.execute();
            statement.close();
        } catch (final Exception e) {
            LOG.error("erreur lors du drop table", e);
        }
    }

    public void executeTruncateTable(final String table) {
        PreparedStatement statement;
        try {
            //MySQL doesn't support prepared statements with variable table names
            statement = connection.prepareStatement(StringUtils.replace(TRUNCATE_TABLE_COMMAND, "?", table));
            statement.execute();
            statement.close();
        } catch (final Exception e) {
            LOG.error("erreur lors du truncate table", e);
        }
    }

    private void printResults(final Statement statement, final boolean hasResults) {
        try {
            if (hasResults) {
                final ResultSet rs = statement.getResultSet();
                if (rs != null) {
                    final ResultSetMetaData md = rs.getMetaData();
                    final int cols = md.getColumnCount();
                    for (int i = 0; i < cols; i++) {
                        final String name = md.getColumnLabel(i + 1);
                        LOG.info(name + "\t");
                    }
                    LOG.info("");
                    while (rs.next()) {
                        for (int i = 0; i < cols; i++) {
                            final String value = rs.getString(i + 1);
                            LOG.info(value + "\t");
                        }
                        LOG.info("");
                    }
                }
            }
        } catch (final SQLException e) {
            LOG.error("Error printing results", e);
        }
    }

    public void setDoOnlyCreateTable(final boolean b) {
        this.onlyCreateTable = b;
    }

    public List<String> getExistingTables() {
        return existingTables;
    }
}