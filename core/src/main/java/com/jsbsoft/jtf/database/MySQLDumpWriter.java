package com.jsbsoft.jtf.database;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.utils.ContexteDao;

public class MySQLDumpWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(MySQLDumpWriter.class);

    private static final String LINE_SEPARATOR = System.getProperty("line.separator", "\n");

    private static final String DEFAULT_COLUMN_QUOTE = "";

    private static final String DEFAULT_DELIMITER = ";";

    private static final String DEFAULT_ENGINE = "MyISAM";

    private static final String DEFAULT_CHARSET = "utf8";

    private static final Integer[] SET_SIZE_TYPE = new Integer[] {Types.VARCHAR};

    private boolean exportData = true;

    private boolean exportStructure = true;

    private Map<String, Map<String, String>> tablesInfos;

    /** Dump the whole database to an SQL string */
    public void dumpDB(final List<String> tables, final String filePath) throws IOException {
        final File dumpFile = new File(filePath);
        FileUtils.touch(dumpFile);
        try (ContexteDao contexteDao = new ContexteDao();
             Connection connection = contexteDao.getConnection()){
            final DatabaseMetaData dbMetaData = connection.getMetaData();
            try (final FileOutputStream outputStream = new FileOutputStream(dumpFile);
                 final OutputStreamWriter outW = new OutputStreamWriter(outputStream);
                 final Writer out = new BufferedWriter(outW)) {
                final ResultSet rs = dbMetaData.getTables(null, null, null, new String[] {"TABLE"});
                while (rs.next()) {
                    final String tableName = rs.getString("TABLE_NAME");
                    // filtre sur le nom de la table
                    if (tables.isEmpty() || tables.contains(tableName)) {
                        final String tableType = rs.getString("TABLE_TYPE");
                        if ("TABLE".equalsIgnoreCase(tableType)) {
                            out.write(LINE_SEPARATOR + "-- TABLE " + tableName);
                            if (exportStructure) {
                                out.write(LINE_SEPARATOR + "CREATE TABLE `" + tableName + "` (" + LINE_SEPARATOR);
                                final ResultSet tableMetaData = dbMetaData.getColumns(null, null, tableName, "%");
                                boolean firstLine = true;
                                while (tableMetaData.next()) {
                                    if (firstLine) {
                                        firstLine = false;
                                    } else {
                                        // ajout virgule de separation des champs
                                        out.write("," + LINE_SEPARATOR);
                                    }
                                    final String columnName = tableMetaData.getString("COLUMN_NAME");
                                    final String columnType = tableMetaData.getString("TYPE_NAME");
                                    final int columnDataType = tableMetaData.getShort("DATA_TYPE");
                                    final int columnSize = tableMetaData.getInt("COLUMN_SIZE");
                                    // Certains types ne doivent pas specifier la taille
                                    String columnSizeString = "";
                                    if (Arrays.asList(SET_SIZE_TYPE).contains(columnDataType)) {
                                        columnSizeString = " (" + columnSize + ")";
                                    }
                                    // ajout
                                    final String nullString = ("YES".equals(tableMetaData.getString("IS_NULLABLE"))) ? " NULL" : " NOT NULL";
                                    // ajout de l'auto-increment
                                    final String autoincrement = ("YES".equals(tableMetaData.getString("IS_AUTOINCREMENT"))) ? " auto_increment" : "";
                                    out.write("    " + DEFAULT_COLUMN_QUOTE + columnName + DEFAULT_COLUMN_QUOTE + " " + columnType + columnSizeString + nullString + autoincrement);
                                }
                                tableMetaData.close();
                                // Now we need to put the primary key constraint
                                try {
                                    final ResultSet primaryKeys = dbMetaData.getPrimaryKeys(null, null, tableName);
                                    String primaryKeyName = null;
                                    StringBuffer primaryKeyColumns = new StringBuffer();
                                    while (primaryKeys.next()) {
                                        final String thisKeyName = primaryKeys.getString("PK_NAME");
                                        if ((thisKeyName != null && primaryKeyName == null) || (thisKeyName == null && primaryKeyName != null) || (thisKeyName != null && !thisKeyName.equals(primaryKeyName)) || (primaryKeyName != null && !primaryKeyName.equals(thisKeyName))) {
                                            // the keynames aren't the same, so output all that we have so far (if anything)
                                            // and start a new primary key entry
                                            if (primaryKeyColumns.length() > 0) {
                                                // There's something to output
                                                out.write("," + LINE_SEPARATOR + "    PRIMARY KEY ");
                                                out.write("(" + primaryKeyColumns.toString() + ")");
                                            }
                                            // Start again with the new name
                                            primaryKeyColumns = new StringBuffer();
                                            primaryKeyName = thisKeyName;
                                        }
                                        // Now append the column
                                        if (primaryKeyColumns.length() > 0) {
                                            primaryKeyColumns.append(", ");
                                        }
                                        primaryKeyColumns.append(primaryKeys.getString("COLUMN_NAME"));
                                    }
                                    if (primaryKeyColumns.length() > 0) {
                                        // There's something to output
                                        out.write("," + LINE_SEPARATOR + "    PRIMARY KEY ");
                                        out.write("(" + primaryKeyColumns.toString() + ")");
                                    }
                                } catch (final SQLException e) {
                                    LOGGER.error("Unable to get primary keys for table " + tableName + " because " + e);
                                }
                                final String engine = getTableProperty(tableName, "Engine");
                                final String collation = getTableProperty(tableName, "Collation");
                                out.write(LINE_SEPARATOR + ")");
                                out.write(String.format(" ENGINE=%s", StringUtils.isNotBlank(engine) ? engine : DEFAULT_ENGINE));
                                if (StringUtils.isNotBlank(collation)) {
                                    out.write(String.format(" DEFAULT CHARACTER SET %s COLLATE %s", StringUtils.substringBefore(collation, "_"), collation));
                                } else {
                                    out.write(String.format(" DEFAULT CHARSET = %s", DEFAULT_CHARSET));
                                }
                                out.write(DEFAULT_DELIMITER + LINE_SEPARATOR);
                            }
                            // dump des donnees de la table
                            if (exportData) {
                                dumpTable(out, tableName);
                            }
                        }
                    }
                    out.flush();
                }
                rs.close();
            } catch (final SQLException e) {
                LOGGER.error("erreur lors de la requete SQL", e);
            }
        } catch (final SQLException e) {
            LOGGER.error("Unable to connect to database: " + e);
        }
    }

    /**
     * dump this particular table to the string buffer
     *
     * @throws IOException
     */
    private void dumpTable(final Writer out, final String tableName) throws IOException {
        try (ContexteDao contexteDao = new ContexteDao();
             Connection connection = contexteDao.getConnection();
             final PreparedStatement stmt = connection.prepareStatement(String.format("SELECT * FROM `%s`", tableName));
             final ResultSet rs = stmt.executeQuery()){
            // First we output the create table stuff
            final ResultSetMetaData metaData = rs.getMetaData();
            final int columnCount = metaData.getColumnCount();
            // Now we can output the actual data
            out.write(LINE_SEPARATOR + "-- INSERT FOR " + tableName + LINE_SEPARATOR);
            while (rs.next()) {
                out.write("INSERT INTO " + tableName + " VALUES (");
                for (int i = 0; i < columnCount; i++) {
                    if (i > 0) {
                        out.write(", ");
                    }
                    final Object value = rs.getObject(i + 1);
                    if (value == null) {
                        out.write("NULL");
                    } else {
                        out.write(String.format("'%s'", getMysqlRealScapeString(value.toString())));
                    }
                }
                out.write(")" + DEFAULT_DELIMITER + LINE_SEPARATOR);
            }
        } catch (final SQLException e) {
            LOGGER.error("Unable to dump table " + tableName + " because: " + e);
        }
    }

    public void setExportData(final boolean exportData) {
        this.exportData = exportData;
    }

    public void setExportStructure(final boolean exportStructure) {
        this.exportStructure = exportStructure;
    }

    private String getMysqlRealScapeString(String str) {
        String data = null;
        if (str != null && str.length() > 0) {
            str = str.replace("\\", "\\\\");
            str = str.replace("'", "\\'");
            str = str.replace("\0", "\\0");
            str = str.replace("\n", "\\n");
            str = str.replace("\r", "\\r");
            str = str.replace("\"", "\\\"");
            str = str.replace("\\x1a", "\\Z");
            data = str;
        }
        return data;
    }

    private String getTableProperty(final String tableName, final String property) {
        if (tablesInfos == null) {
            getTablesInfos();
        }
        final Map<String, String> infos = tablesInfos.get(tableName);
        if (infos != null) {
            return infos.get(property);
        }
        return StringUtils.EMPTY;
    }

    private void getTablesInfos() {
        tablesInfos = new HashMap<>();
        try (ContexteDao contexteDao = new ContexteDao();
             Connection connection = contexteDao.getConnection();
             final PreparedStatement stmt  = connection.prepareStatement("SHOW TABLE STATUS");
             final ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                final String name = rs.getString("Name");
                if (StringUtils.isNotBlank(name)) {
                    Map<String, String> currentMap = tablesInfos.get(name);
                    if (currentMap == null) {
                        currentMap = new HashMap<>();
                        tablesInfos.put(name, currentMap);
                    }
                    final ResultSetMetaData meta = rs.getMetaData();
                    for (int i = 1; i <= meta.getColumnCount(); i++) {
                        final String key = meta.getColumnLabel(i);
                        final String value = rs.getString(key);
                        currentMap.put(key, value);
                    }
                }
            }
        } catch (final SQLException e) {
            LOGGER.error("Une erreur est survenue lors de la récupération des informations concernants les tables", e);
        }
    }
}
