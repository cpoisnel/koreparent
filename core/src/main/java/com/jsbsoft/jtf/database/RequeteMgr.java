/*
 * Created on 22 févr. 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.jsbsoft.jtf.database;

import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Vector;

import javax.mail.MessagingException;

import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.datasource.pool.PoolInfos;
import com.jsbsoft.jtf.email.JSBMailbox;
import com.jsbsoft.jtf.webutils.FormateurHTML;
import com.kportal.core.config.PropertyHelper;

/**
 *
 * Gestion des dumps de requete
 *
 * - détection des boucles - affichage sur exception
 *
 * Les requêtes sont identifiées par un idRequete au niveau des contextes
 *
 * Attention, toutes les requetes ne sont pas actuellement mémorisées car cela demandait d'internir à trop d'endroits (en particulier dans les processus).
 *
 * La variable idRequete est alors vide au niveau du contexte
 *
 * @author KOSMOS
 */
public class RequeteMgr {

    private static final Logger LOG = LoggerFactory.getLogger(RequeteMgr.class);
    // Stockage des requetes

    /** The id requete. */
    private static String idRequete = String.valueOf(System.currentTimeMillis());

    /** The events. */
    private static TreeMap<String, EtatRequete> events = new TreeMap<>();

    /**
     * Creer requete.
     *
     * @return the string
     */
    public static synchronized String creerRequete() {
        // Increment de l'identifiant de requete
        idRequete = String.valueOf(Long.parseLong(idRequete) + 1);
        events.put(idRequete, new EtatRequete());
        /*
         *  On supprime les requetes terminees depuis 30 s.
         *  On logge les requêtes durant plus de 1 minute
         */
        final Vector<String> aSupprimer = new Vector<>();
        final Vector<String> aLogger = new Vector<>();
        final long maintenant = System.currentTimeMillis();
        for (final String id : events.keySet()) {
            final EtatRequete requete = events.get(id);
            if (requete.isTerminee()) {
                if (maintenant - requete.getTimestampDebut() > 30000) {
                    aSupprimer.add(id);
                }
            } else {
                if (maintenant - requete.getTimestampDebut() > 60000) {
                    aLogger.add(id);
                    aSupprimer.add(id);
                }
            }
        }
        /* Détection de boucles :
         * evènements à envoyer au webmaster
         */
        if ("1".equals(PropertyHelper.getCoreProperty("dump.detection_boucle")) && !aLogger.isEmpty()) {
            String dump = "";
            final Enumeration<String> e1 = aLogger.elements();
            while (e1.hasMoreElements()) {
                final String id = e1.nextElement();
                dump += "\n***** DUMP REQUETE ************";
                dump += "\"" + dumpRequete(events.get(id));
                dump += "\n***** FIN DUMP ********************";
                LOG.info(dump);
            }
            try {
                //Initialisation du mailbox
                final JSBMailbox mailbox = new JSBMailbox(false);
                final String sujet = "[Requete non terminée ?] > A ANALYSER";
                mailbox.sendSystemMsg("debug@kosmos.fr", sujet, dump);
            } catch (final MessagingException | EmailException e) {
                LOG.debug("unable to send the mail", e);
            }
        }
        /* Evènements à supprimer */
        final Enumeration<String> e2 = aSupprimer.elements();
        while (e2.hasMoreElements()) {
            final String id = e2.nextElement();
            events.remove(id);
        }
        return idRequete;
    }

    /**
     * Ajouter evenement.
     *
     * @param id
     *            the id
     * @param evenement
     *            the evenement
     */
    public static synchronized void ajouterEvenement(final String id, final String evenement) {
        final GregorianCalendar gCal = new GregorianCalendar();
        final SimpleDateFormat formateur = new SimpleDateFormat("hh:mm:ss:SSS");
        if ((id != null) && (id.length() > 0) && (events.get(id) != null)) {
            events.get(id).ajouterEvenement(formateur.format(gCal.getTime()) + "\n" + evenement);
        }
    }

    /**
     * Terminer requete.
     *
     * @param id
     *            the id
     */
    public static synchronized void terminerRequete(final String id) {
        if (id != null && id.length() > 0 && events.get(id) != null) {
            events.get(id).terminer();
        }
    }

    /**
     * Affichage du dump instantané (appelé dans /adminsite/dump.jsp)
     *
     * @return the string
     */
    public static synchronized String dump() {
        /* Durée moyenne des requêtes terminées */
        String res = "";
        long delai = 0;
        int nbRequetesTerminees = 0;
        Iterator<EtatRequete> iter = events.values().iterator();
        while (iter.hasNext()) {
            final EtatRequete requete = iter.next();
            if (requete.isTerminee()) {
                delai += requete.getTimestampFin() - requete.getTimestampDebut();
                nbRequetesTerminees++;
            }
        }
        if (nbRequetesTerminees > 0) {
            res += "<br /> Nb requetes OK (30 dernières sec)= " + nbRequetesTerminees;
            res += "<br /> Durée moyenne = " + (delai / nbRequetesTerminees) + " ms";
            res += "<br> Nombre de connexions actives sur la base de données =" + PoolInfos.getActiveConnections();
            res += "<br> Nombre de connexions disponibles sur la base de données =" + PoolInfos.getPoolSize();
        }
        /* Affichage des requetes suspendues */
        iter = events.values().iterator();
        res += "<font color=\"red\">";
        res += "<br /> Nb requêtes en cours (délai purge = 60 s.) = " + (events.size() - nbRequetesTerminees);
        while (iter.hasNext()) {
            final EtatRequete requete = iter.next();
            if (!requete.isTerminee()) {
                res += "<br />-----REQUETE -------------";
                res += "<br />" + FormateurHTML.formaterEnHTML(dumpRequete(requete));
            }
        }
        res += "</font>";
        /* Affichage des requetes terminées */
        String requetesTerminees = "";
        iter = events.values().iterator();
        while (iter.hasNext()) {
            final EtatRequete requete = iter.next();
            if (requete.isTerminee()) {
                requetesTerminees = "<br />-----REQUETE -------------<br />" + FormateurHTML.formaterEnHTML(dumpRequete(requete)) + requetesTerminees;
            }
        }
        res += requetesTerminees;
        return res;
    }

    /**
     * Affichage des des requêtes commencées depuis - de 3 s et non terminées Utilisé pour diagnostiquer les exceptions.
     *
     * @return the string
     */
    public static synchronized String dumpDernieresRequetesNonTerminees() {
        String res = "";
        String logRequete = "";
        final long maintenant = System.currentTimeMillis();
        for (final String id : events.keySet()) {
            final EtatRequete requete = events.get(id);
            if (!requete.isTerminee()) {
                if (maintenant - requete.getTimestampDebut() < 3000) {
                    logRequete = "\n-----REQUETE -------------\n" + dumpRequete(requete) + logRequete;
                }
            }
        }
        res = res + logRequete;
        return res;
    }

    /**
     * Suppression des requêtes commencées depuis - de 3 s et non terminées Permet de purger les erreurs applicatives et de ne pas les prendre en compte dans la détection de
     * boucles (appelé depuis jsb/exception.jsp)
     */
    public static synchronized void purgerRequetesNonTerminees() {
        final Vector<String> aSupprimer = new Vector<>();
        final long maintenant = System.currentTimeMillis();
        for (final String id : events.keySet()) {
            final EtatRequete requete = events.get(id);
            if (!requete.isTerminee()) {
                if (maintenant - requete.getTimestampDebut() < 3000) {
                    aSupprimer.add(id);
                }
            }
        }
        /* On supprime ces requêtes */
        final Enumeration<String> e2 = aSupprimer.elements();
        while (e2.hasMoreElements()) {
            final String id = e2.nextElement();
            events.remove(id);
        }
    }

    /**
     * Dump requete.
     *
     * @param requete
     *            the requete
     *
     * @return the string
     */
    public static synchronized String dumpRequete(final EtatRequete requete) {
        String res = "";
        final Enumeration<String> e = requete.getEvenements().elements();
        while (e.hasMoreElements()) {
            res += "\n" + e.nextElement() + "\n...............";
        }
        return res;
    }
}