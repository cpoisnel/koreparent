package com.jsbsoft.jtf.database;

import java.sql.Connection;
import java.util.Hashtable;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.AbstractProcessusBean;
import com.jsbsoft.jtf.core.InfoBean;
import com.univ.multisites.InfosSite;
import com.univ.utils.ContexteDao;
import com.univ.utils.ContexteUtil;

/**
 * Super-classe des processus.
 */
public abstract class ProcessusBean extends AbstractProcessusBean implements OMContext {

    private static final Logger LOG = LoggerFactory.getLogger(ProcessusBean.class);

    /** The action. */
    protected String action = null;

    /** The infos site. */
    private InfosSite infosSite = null;

    /** The secure. */
    private boolean secure = false;

    /** The datas. */
    private Hashtable<String, Object> datas = null;

    private ContexteDao contexteDao;

    /**
     * Interface processus d'aide à la codification.
     *
     * @param infoBean
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public ProcessusBean(final InfoBean infoBean) {
        super(infoBean);
        datas = new Hashtable<>();
    }

    /**
     * @see com.jsbsoft.jtf.database.OMContext#getDatas()
     */
    @Override
    public Hashtable<String, Object> getDatas() {
        return datas;
    }

    /**
     * Valorisation du DataStore associé au processus.
     *
     * @return the connection
     */
    @Override
    public Connection getConnection() {
        return contexteDao.getConnection();
    }

    /**
     * Valorisation du DataStore associBeané au processus.
     *
     * @return the locale
     */
    @Override
    public Locale getLocale() {
        return ContexteUtil.getContexteUniv().getLocale();
    }

    /**
     * Valorisation du DataStore associé au processus.
     */
    @Override
    protected void initConnection() {
        if (contexteDao == null) {
            contexteDao = new ContexteDao();
        }
    }

    /**
     * Valorisation du DataStore associé au processus.
     *
     */
    @Override
    protected void releaseConnection() {
        if (contexteDao != null) {
            contexteDao.close();
            contexteDao = null;
        }
    }

    /**
     * @see com.jsbsoft.jtf.core.AbstractProcessusBean#traiterAbandon()
     */
    @Override
    public void traiterAbandon() {
        releaseConnection();
    }

    /**
     * @see com.jsbsoft.jtf.database.OMContext#setInfosSite(com.univ.multisites.InfosSite)
     */
    @Override
    public void setInfosSite(final InfosSite infosSite) {
        this.infosSite = infosSite;
    }

    /**
     * Renvoie l'InfosSite correspondant au site courant.
     *
     * @return the infos site
     */
    @Override
    public InfosSite getInfosSite() {
        return infosSite;
    }

    /**
     * @see com.jsbsoft.jtf.database.OMContext#setSecure(boolean)
     */
    @Override
    public void setSecure(final boolean secure) {
        this.secure = secure;
    }

    /**
     * Renvoie true si on est en mode sécurisé (https).
     *
     * @return true, if checks if is secure
     */
    @Override
    public boolean isSecure() {
        return secure;
    }
}
