package com.jsbsoft.jtf.core;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;

import com.kportal.core.context.BeanUtil;
import com.kportal.core.context.MergedContextBean;
import com.kportal.core.context.OverridedContextBean;
import com.kportal.extension.ExtensionConfigurer;

import fr.kosmos.cluster.api.Cluster;
import fr.kosmos.cluster.api.MessageHandler;

/**
 * The Class ApplicationContextManager.
 */
public class ApplicationContextManager implements ApplicationContextAware, MessageHandler<Serializable> {

    /** The default ctx. */
    public static final String DEFAULT_CORE_CONTEXT = "core";

    private static final String ID_BEAN = "applicationContextManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationContextManager.class);

    /** The extensions ctx. */
    private static Map<String, ApplicationContext> allApplicationContext;

    /** The overrided beans. */
    private static Map<String, OverridedContextBean> overridedBeans;

    private Cluster cluster;

    /**
     * Point d'entrée pour le rechargement complet des contextes
     */
    public static synchronized void refresh() {
        final ConfigurableApplicationContext configurableCtx = (ConfigurableApplicationContext) allApplicationContext.get(DEFAULT_CORE_CONTEXT);
        configurableCtx.refresh();
        LOGGER.info("Chargement des contextes applicatifs OK");
    }

    public static void refresh(final ExtensionConfigurer extensionConfigurer) {
        // sauvegarde du contexte principal
        final ApplicationContext parentCtx = allApplicationContext.get(DEFAULT_CORE_CONTEXT);
        // reset de l'ensemble de contextes charges
        allApplicationContext.clear();
        overridedBeans.clear();
        // ajout des contextes
        allApplicationContext.put(DEFAULT_CORE_CONTEXT, parentCtx);
        allApplicationContext.putAll(extensionConfigurer.initAllApplicationContext(allApplicationContext.get(DEFAULT_CORE_CONTEXT)));
        // boucle sur tous les contextes
        final Iterator<Map.Entry<String, ApplicationContext>> it = allApplicationContext.entrySet().iterator();
        while (it.hasNext()) {
            final Map.Entry<String, ApplicationContext> currentEntry = it.next();
            final String idExtension = currentEntry.getKey();
            final ApplicationContext appCtx = currentEntry.getValue();
            processOverride(idExtension, appCtx);
            if (!processMerge(idExtension, appCtx)) {
                ((ConfigurableApplicationContext) appCtx).close();
                it.remove();
            }
        }
        ((ClassBeanManager) parentCtx.getBean(ClassBeanManager.ID_BEAN)).refresh();
    }

    private static boolean processMerge(final String idExtension, final ApplicationContext appCtx) {
        // boucle sur les beans à surcharger
        for (final String beanName : appCtx.getBeanNamesForType(MergedContextBean.class)) {
            final MergedContextBean bean = (MergedContextBean) appCtx.getBean(beanName);
            if (StringUtils.isEmpty(bean.getIdBeanToMerge())) {
                bean.setIdBeanToMerge(beanName);
            }
            if (StringUtils.isEmpty(bean.getIdExtensionToMerge())) {
                bean.setIdExtensionToMerge(DEFAULT_CORE_CONTEXT);
            }
            // test que l'extension cible existe bien dans le contexte
            if (allApplicationContext.get(bean.getIdExtensionToMerge()) != null) {
                // test que le bean à merger existe dans le contexte
                if (allApplicationContext.get(bean.getIdExtensionToMerge()).containsBean(bean.getIdBeanToMerge())) {
                    try {
                        bean.merge();
                    } catch (final Exception e) {
                        LOGGER.error("Erreur extension id=" + idExtension + " : le bean id=" + bean.getIdBeanToMerge() + " impossible à merger" + (bean.getIdExtensionToMerge().equals(DEFAULT_CORE_CONTEXT) ? " sur l'application" : " sur l'extension id=" + bean.getIdExtensionToMerge()), e);
                        LOGGER.debug("Erreur de merge de bean", e);
                    }
                } else {
                    LOGGER.error("Erreur extension id=" + idExtension + " : le bean id=" + bean.getIdBeanToMerge() + " à merger" + (bean.getIdExtensionToMerge().equals(DEFAULT_CORE_CONTEXT) ? " sur l'application" : " sur l'extension id=" + bean.getIdExtensionToMerge()) + " n'existe pas");
                }
            } else {
                LOGGER.error("Erreur extension id=" + idExtension + " : l'extension id=" + bean.getIdExtensionToMerge() + " à merger n'existe pas");
                if (bean.isMandatory()) {
                    LOGGER.error("Extension id=" + idExtension + "est ignorée");
                    return Boolean.FALSE;
                }
            }
        }
        return Boolean.TRUE;
    }

    private static void processOverride(final String idExtension, final ApplicationContext appCtx) {
        // boucle sur les beans à surcharger
        for (final String beanName : appCtx.getBeanNamesForType(OverridedContextBean.class)) {
            final OverridedContextBean bean = (OverridedContextBean) appCtx.getBean(beanName);
            if (StringUtils.isEmpty(bean.getIdBean())) {
                bean.setIdBean(beanName);
            }
            if (StringUtils.isEmpty(bean.getIdExtension())) {
                bean.setIdExtension(idExtension);
            }
            if (StringUtils.isEmpty(bean.getIdBeanToOverride())) {
                bean.setIdBeanToOverride(beanName);
            }
            if (StringUtils.isEmpty(bean.getIdExtensionToOverride())) {
                bean.setIdExtensionToOverride(DEFAULT_CORE_CONTEXT);
            }
            // test que l'extension cible existe bien dans le contexte
            if (allApplicationContext.get(bean.getIdExtensionToOverride()) != null) {
                // test que le bean à surcharger existe dans le contexte
                if (allApplicationContext.get(bean.getIdExtensionToOverride()).containsBean(bean.getIdBeanToOverride())) {
                    overridedBeans.put(BeanUtil.getBeanKey(bean.getIdBeanToOverride(), bean.getIdExtensionToOverride()), bean);
                } else {
                    LOGGER.error("Erreur extension id=" + idExtension + " : le bean id=" + bean.getIdBeanToOverride() + " à surcharger" + (bean.getIdExtensionToOverride().equals(DEFAULT_CORE_CONTEXT) ? " sur l'application" : " sur l'extension id=" + bean.getIdExtensionToOverride()) + " n'existe pas");
                }
            } else {
                LOGGER.error("Erreur extension id=" + idExtension + " : l'extension id=" + bean.getIdExtensionToOverride() + " à surcharger n'existe pas");
            }
        }
    }

    /**
     * Gets the bean.
     *
     * @param ctxName
     *            the ctx name
     * @param beanName
     *            the bean name
     * @return the bean
     */
    public static Object getBean(final String ctxName, final String beanName, final boolean includeOverrided) {
        // test si le bean ciblé dans le contexte ciblé est surchargé
        if (includeOverrided && overridedBeans.get(BeanUtil.getBeanKey(beanName, ctxName)) != null) {
            final OverridedContextBean over = overridedBeans.get(BeanUtil.getBeanKey(beanName, ctxName));
            // on rappelle le bean ciblé dans le bon contexte
            return getBean(over.getIdExtension(), over.getIdBean());
        }
        final ApplicationContext appCtx = allApplicationContext.get(ctxName);
        Object bean = null;
        if (appCtx == null) {
            LOGGER.warn("Contexte non trouvé : " + ctxName);
        } else {
            bean = appCtx.getBean(beanName);
        }
        return bean;
    }

    /**
     * Gets the bean.
     *
     * @param ctxName
     *            the ctx name
     * @param beanName
     *            the bean name
     * @return the bean
     */
    public static <T> T getBean(final String ctxName, final String beanName, final boolean includeOverrided, Class<T> clazz) {
        final Object bean = getBean(ctxName, beanName, includeOverrided);
        if (bean != null && clazz.isInstance(bean)) {
            return clazz.cast(bean);
        }
        return null;
    }

    /**
     * Gets the bean.
     *
     * @param ctxName
     *            the ctx name
     * @param beanName
     *            the bean name
     * @return the bean
     */
    public static Object getBean(final String ctxName, final String beanName) {
        return getBean(ctxName, beanName, true);
    }

    /**
     * Gets the bean.
     *
     * @param ctxName
     *            the ctx name
     * @param beanName
     *            the bean name
     * @return the bean
     */
    public static <T> T getBean(final String ctxName, final String beanName, final Class<T> clazz) {
        final Object bean = getBean(ctxName, beanName, true);
        if (bean != null && clazz.isInstance(bean)) {
            return clazz.cast(bean);
        }
        return null;
    }

    public static boolean containsBean(final String ctxName, final String beanName) {
        try {
            getBean(ctxName, beanName);
        } catch (final NoSuchBeanDefinitionException e) {
            LOGGER.debug(String.format("no bean fonund on context %s with name %s", ctxName, beanName), e);
            return false;
        }
        return true;
    }

    public static boolean containsBean(final String beanName) {
        return getEveryContextBean(beanName) != null;
    }

    public static Object getEveryContextBean(final String beanName) {
        for (final String idExtension : allApplicationContext.keySet()) {
            try {
                return getBean(idExtension, beanName);
            } catch (final BeansException e) {
                LOGGER.debug("unable to retrieve bean on context " + idExtension, e);
            }
        }
        LOGGER.warn("Le bean id=" + beanName + " n'existe dans aucun contexte applicatif");
        return null;
    }

    public static <T> T getEveryContextBean(final String beanName, final Class<T> clazz) {
        for (final String idExtension : allApplicationContext.keySet()) {
            try {
                final Object bean = getBean(idExtension, beanName);
                if (clazz.isInstance(bean)) {
                    return clazz.cast(bean);
                }
            } catch (final BeansException e) {
                LOGGER.debug("unable to retrieve bean on context" + idExtension, e);
            }
        }
        LOGGER.warn("Le bean id=" + beanName + " n'existe dans aucun contexte applicatif");
        return null;
    }

    public static String getExtensionIdForBeanName(final String beanName) {
        String extensionName = StringUtils.EMPTY;
        for (final Map.Entry<String, ApplicationContext> appContextByExtensionName : allApplicationContext.entrySet()) {
            if (appContextByExtensionName.getValue().containsBean(beanName)) {
                extensionName = appContextByExtensionName.getKey();
            }
        }
        return extensionName;
    }

    /**
     * Gets the bean.
     *
     * @param beanName
     *            the bean name
     * @return the bean
     */
    public static Object getCoreContextBean(final String beanName) {
        return getBean(DEFAULT_CORE_CONTEXT, beanName);
    }

    /**
     * Gets the bean.
     *
     * @param beanName
     *            the bean name
     * @return the bean
     */
    public static <T> T getCoreContextBean(final String beanName, Class<T> clazz) {
        return getBean(DEFAULT_CORE_CONTEXT, beanName, clazz);
    }

    /**
     * Gets All beans of type. ATTENTION ne pas utiliser cette méthode sans précaution Elle ne doit servir qu'a retrouver des beans techniques hors modules (qui possedent leur
     * manager dedié)
     *
     * @param type
     *            the type
     * @return the beans of type
     */
    public static <T> Map<String, T> getAllBeansOfType(final Class<T> type) {
        final Map<String, T> res = new LinkedHashMap<>();
        for (final String idExtension : allApplicationContext.keySet()) {
            res.putAll(getBeansOfType(idExtension, type));
        }
        return res;
    }

    /**
     * Retourne l'ensemble des beans déclaré dans spring pour la classe fourni en paramètre et les classes par extensions.
     * ATTENTION ne pas utiliser cette méthode sans précaution Elle ne doit servir qu'a retrouver des beans techniques hors modules (qui possedent leur
     * manager dedié)
     *
     * @param type la classe dont on souhaite récupérer tout les beans
     * @return Une map contenant l'ensemble des bean par extension
     */
    public static <T> Map<String, Collection<T>> getAllBeansOfTypeByExtension(final Class<T> type) {
        final Map<String, Collection<T>> res = new LinkedHashMap<>();
        for (final String idExtension : allApplicationContext.keySet()) {
            final Map<String, T> resultForExtension = getBeansOfType(idExtension, type);
            res.put(idExtension, resultForExtension.values());
        }
        return res;
    }

    /**
     * Gets the beans of type. ATTENTION ne pas utiliser cette méthode sans précaution Elle ne doit servir qu'a retrouver des beans techniques hors modules (qui possedent leur
     * manager dedié)
     *
     * @param type
     *            the type
     * @return the beans of type
     */
    public static <T> Map<String, T> getCoreContextBeansOfType(final Class<T> type) {
        return getBeansOfType(DEFAULT_CORE_CONTEXT, type);
    }

    /**
     * Gets the beans of type.
     *
     * @param ctxName
     *            the ctx name
     * @param type
     *            the type
     * @return the beans of type
     */
    @SuppressWarnings("unchecked")
    public static <T> Map<String, T> getBeansOfType(final String ctxName, final Class<T> type) {
        final Map<String, T> res = new LinkedHashMap<>();
        final ApplicationContext appCtx = allApplicationContext.get(ctxName);
        for (final String beanName : appCtx.getBeanNamesForType(type)) {
            res.put(beanName, (T) getBean(ctxName, beanName));
        }
        return res;
    }

    public static Map<String, ApplicationContext> getAllApplicationContext() {
        return allApplicationContext;
    }

    public static ApplicationContext getApplicationContext(final String name) {
        return allApplicationContext.get(name);
    }

    public static ApplicationContextManager getInstance() {
        return (ApplicationContextManager) allApplicationContext.get(DEFAULT_CORE_CONTEXT).getBean(ID_BEAN);
    }

    public void init() {
        if (cluster != null) {
            cluster.registerService(this);
        }
    }

    /* (non-Javadoc)
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    @Override
    public void setApplicationContext(final ApplicationContext arg0) throws BeansException {
        allApplicationContext = new LinkedHashMap<>();
        allApplicationContext.put(DEFAULT_CORE_CONTEXT, arg0);
        overridedBeans = new HashMap<>();
    }

    @Override
    public void handleMessage(final Serializable message) {
        refresh();
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(final Cluster cluster) {
        this.cluster = cluster;
    }
}
