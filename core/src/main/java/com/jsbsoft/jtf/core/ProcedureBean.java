package com.jsbsoft.jtf.core;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.DescriptifPageRetour.TypeFlux;
import com.jsbsoft.jtf.session.SousSession;
import com.kportal.core.config.PropertyHelper;
import com.univ.utils.ContexteUtil;

/**
 * Représente une Procédure <BR>
 * - renvoie une page en réponse à une requête HTTP<BR>
 * <BR>
 * .
 */
public class ProcedureBean {

    /** Nom de la page application par défaut. */
    public static final String CSTE_NOM_PAGE_DEFAUT = "adminsite/accueil";

    /** The MOD e_ sequentiel. */
    public static final int MODE_SEQUENTIEL = 0;

    /** The MOD e_ evenementiel. */
    public static final int MODE_EVENEMENTIEL = 1;

    private static final Logger LOG = LoggerFactory.getLogger(ProcedureBean.class);

    /** composant information. */
    private final InfoBean infoBean = new InfoBean();

    /** composant logique d'affichage (quelle que soit l'activité). */
    private ControleurNavigation cna;

    /** parser de requête. */
    private ParserRequete parser;

    /** session (partenaire ou hors partenaire). */
    private SousSession sessionPartenaire = null;

    /** The gp. */
    private ProcessusManager gp;

    /** The nom. */
    private String nom;

    /* Indicateur de Sous PU */

    /** The extension. */
    private String extension;

    /* Nom de la sousPU */

    /** The module exit. */
    private ModuleExit moduleExit;

    /** The est une sous pu. */
    private boolean estUneSousPU = false;

    /*Description des modes d'enchainement */

    /** The PU mere. */
    private ProcedureBean PUMere = null;

    /** The sous pu. */
    private ProcedureBean sousPU = null;

    /** The mode enchainement. */
    private int modeEnchainement = 0;

    /** The restaurable. */
    private boolean restaurable = Boolean.FALSE;

    /** The memorisation ci. */
    private boolean memorisationCI = Boolean.FALSE;

    /**
     * Constructeur de PU.
     *
     * @param ss
     *            SousSession : session à laquelle doît se rattacher la PU
     * @param _nom
     *            String : nom de la PU (PU_NAME de l'URL)
     */
    public ProcedureBean(final SousSession ss, final String _nom, final String extension) {
        //conserver la session
        setSessionPartenaire(ss);
        // Mémorisation du nom
        setNom(_nom);
        setExtension(StringUtils.defaultString(extension));
    }

    /**
     * Renvoie vrai si l'utilisateur a demandé l'abandon de la procédure. <br>
     * Consulte le composant d'information pour voir si la donnée "ABANDON" a été renseignée.
     *
     * @return true, if abandon
     */
    private boolean abandon() {
        final String action = getInfoBean().getActionUtilisateur();
        return InfoBean.ACTION_ABANDON.equals(action);
    }

    /**
     * Abandonne la PU en cours.<BR>
     * Signale l'abandon à toutes les activités en cours. <B>Attention: la session est soit spécifique à un partenaire, soit hors partenaire.</B><br>
     * Renvoie le descriptif de la page par défaut
     *
     * @return com.jsbsoft.jtf.session.DescriptifPageRetour
     */
    private DescriptifPageRetour traiterAbandonProcedure() {
        //signaler l'abandon à l'activité courante
        gp.traiterAbandonProcedure();
        //arrête la PU
        dereferencerPU();
        final InfoBean infoBean = getInfoBean();
        // Ajout de l'écran conteneur si il existe
        final String nomConteneur = ProcessusHelper.getEcranConteneur(infoBean.getNomExtension(), infoBean.getNomProcessus(), infoBean.getEcranLogique());
        if (StringUtils.isNotBlank(nomConteneur)) {
            infoBean.setEcranConteneur(nomConteneur);
        }
        String pageFin = PropertyHelper.getProperty(extension, "processus." + nom + ".page_fin");
        if (pageFin == null) {
            pageFin = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.page_fin");
            if (pageFin == null) {
                pageFin = ProcedureBean.CSTE_NOM_PAGE_DEFAUT;
            }
        }
        //renvoie la page par défaut (page hors procédure)
        return DescriptifPageRetour.descriptifPourJSP(pageFin);
    }

    /**
     * Arret brutal de la procédure (aucune notification )<br />
     * Stoppe la procédure par déréférencement Renvoie le descriptif de la page par défaut.
     *
     */
    public final void arreterProcedure() {
        //arrête la PU
        dereferencerPU();
    }

    /**
     * création d'une Sous-procédure.
     *
     * @param nom
     *            String : nom de la sous-procédure (PROC de l'URL)
     *
     * @return the procedure bean
     *
     * @throws Exception
     *             the exception
     */
    public ProcedureBean creerSousPU(final String nom, final String extension) {
        sousPU = new ProcedureBean(getSession(), nom, extension);
        sousPU.initialiser(this);
        return sousPU;
    }

    /**
     * Déréférence la PU en cours.<BR>
     * Signale l'abandon à toutes les activités en cours. <B>Attention: la session est soit spécifique à un partenaire, soit hors partenaire.</B><br>
     * Renvoie le descriptif de la page par défaut
     *
     */
    private void dereferencerPU() {
        //arrête la PU
        if (!estUneSousPU) {
            getSession().retirerProcedureBean(idBean());
        }
    }

    /**
     * Méthode qui renvoie le prochain flux (une JSP).<BR>
     *
     * @return the descriptif page retour
     */
    protected final DescriptifPageRetour descriptifFluxRetour() {
        DescriptifPageRetour desc;
        //si la procédure est abandonnée par l'utilisateur...
        if (abandon()) {
            LOG.debug("Abandon par l'utilisateur");
            //arrêter la procédure & renvoyer la page par défaut
            desc = this.traiterAbandonProcedure();
        } else {
            desc = descriptifFluxProcessus();
        }
        return desc;
    }

    /**
     * Méthode (appelée par la servlet PU) qui renvoie le prochain flux (en général, l'ihm).<BR>
     * Cette méthode masque la méthode descriptifFluxRetour sans argument.
     *
     * @param req
     *            javax.servlet.http.HttpServletRequest
     *
     * @return the descriptif page retour
     */
    public final DescriptifPageRetour descriptifFluxRetour(final HttpServletRequest req) {
        DescriptifPageRetour d = null;
        if (sousPU == null) {
            // appel du parser
            getParserRequete().recueillirDonneesSaisies(req);
            if (moduleExit != null) {
                d = moduleExit.apresCollecte(this);
            }
            // Cas particulier ou une sous-procédure a été créée
            // on appelle la sous-procédure directement
            if (sousPU != null) {
                d = sousPU.descriptifFluxRetour();
            } else if (d == null) {
                // Cas standard : appel controleur navigation et processus
                d = descriptifFluxRetour();
            }
        } else {
            // traitement de fin de la sous-procédure
            d = sousPU.descriptifFluxRetour(req);
            if (d == null) {
                // Appel du module d'EXIT
                if (moduleExit != null) {
                    d = moduleExit.apresSousPU(this, sousPU.getInfoBean());
                }
                // déréférencement de la sous-procédure
                sousPU = null;
                // rebranchement sur le dernier écran
                if (d == null) {
                    d = DescriptifPageRetour.descriptifPourJSP(getInfoBean().getEcranPhysique());
                }
            }
        }
        return d;
    }

    /**
     * Création d'un nouveau processus qui devient le processus actif.
     *
     * @param nomProcessus
     *            the nom processus
     */
    public void empilerProcessus(final String nomProcessus, final String nomExtension) {
        gp.creerProcessus(nomProcessus, nomExtension, getModuleExit(), ProcessusManager.TYPE_APPELANT_PU);
    }

    /**
     * Accesseur.
     *
     * @return the info bean
     */
    public final InfoBean getInfoBean() {
        /* On renvoie le CI du processus actif */
        InfoBean ret = gp.getInfoBean();
        /* ou le CI de la procédure si aucune activité */
        if (ret == null) {
            ret = infoBean;
        }
        return ret;
    }

    /**
     * retourne le composant d'information de la PU (et non celui du processus actif).
     *
     * @return the info procedure bean
     */
    protected final InfoBean getInfoProcedureBean() {
        return infoBean;
    }

    /**
     * Accesseur.
     *
     * @return the controleur navigation
     */
    protected final ControleurNavigation getControleurNavigation() {
        return cna;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (19/04/01 11:50:40)
     *
     * @return int
     */
    protected int getModeEnchainement() {
        return modeEnchainement;
    }

    /**
     * Accesseur.
     *
     * @return the module exit
     */
    public ModuleExit getModuleExit() {
        return moduleExit;
    }

    /**
     * Accesseur.
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Accesseur.
     *
     * @return the parser requete
     */
    public ParserRequete getParserRequete() {
        return parser;
    }

    /**
     * Accesseur.
     *
     * @return the session
     */
    protected final SousSession getSession() {
        return sessionPartenaire;
    }

    /**
     * Renvoie la Sous-procédure active pour la procédure en cours.
     *
     * @return the sous pu active
     */
    public ProcedureBean getSousPUActive() {
        if (sousPU != null) {
            return sousPU.getSousPUActive();
        }
        return this;
    }

    /**
     * Renvoie l'identifiant unique du bean.
     *
     * @return the string
     */
    public final String idBean() {
        String idBean;
        if (!estUneSousPU) {
            idBean = Integer.toHexString(hashCode());
        } else {
            idBean = PUMere.idBean();
        }
        return idBean;
    }

    /**
     * ProcedureBean constructor comment.
     *
     * @throws Exception
     *             the exception
     */
    protected void initialiser() {
        initialiser(null);
    }

    /**
     * ProcedureBean constructor comment.
     *
     * @param _PUMere
     *            the _ pu mere
     *
     * @throws Exception
     *             the exception
     */
    protected void initialiser(final ProcedureBean _PUMere) {
        // Mémorisation de la PU de lancement
        if (_PUMere != null) {
            PUMere = _PUMere;
            estUneSousPU = true;
        }
        referencerPU();
        //création du controleur de navigation
        setControleurNavigation(new ControleurNavigation(this));
        //création du parser
        setParserRequete(new ParserRequete(this));
        //placer l'identifiant du bean PU dans le composant d'information
        //  pour que la JSP puisse le récupérer et le renvoyer à chaque validation de page
        infoBean.setBEANPU(idBean());
        restaurable = "1".equals(PropertyHelper.getProperty(extension, "processus." + nom + ".restaurable"));
        memorisationCI = "1".equals(PropertyHelper.getProperty(extension, "processus." + nom + ".memorisation"));
        //Gestion du mode d'enchainement
        setModeEnchainement(MODE_SEQUENTIEL);
        final String mode = PropertyHelper.getProperty(extension, "processus." + nom + ".modeEnchainement");
        if ("EVENEMENTIEL".equals(mode)) {
            setModeEnchainement(MODE_EVENEMENTIEL);
        }
        //placer le mode d'enchiainement du bean PU dans le composant d'information
        //  pour que la JSP puisse le récupérer et le renvoyer à chaque validation de page
        infoBean.setModeEnchainement(getModeEnchainement());
        //Nom de la PU (doit être accessible partout)
        infoBean.setNomProcessus(getNom());
        infoBean.setNomExtension(getExtension());
        gp = new ProcessusManager(this);
        // Création du module d'Exit
        moduleExit = new DefaultExitModule();
        // Récupération du processus d'INIT
        empilerProcessus(nom, extension);
    }

    /**
     * Référence la procédure en cours.<BR>
     * Signale l'abandon à tous les processus en cours. <B>Attention: la session est soit spécifique à un partenaire, soit hors partenaire.</B><br>
     * Renvoie le descriptif de la page par défaut
     *
     */
    private void referencerPU() {
        //arrête la PU
        if (!estUneSousPU) {
            getSession().ajouterProcedureBean(this);
        }
    }

    /**
     * Accesseur.
     *
     * @param _cna
     *            the _cna
     */
    private void setControleurNavigation(final ControleurNavigation _cna) {
        cna = _cna;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (19/04/01 11:50:40)
     *
     * @param newModeEnchainement
     *            int
     */
    private void setModeEnchainement(final int newModeEnchainement) {
        modeEnchainement = newModeEnchainement;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (18/04/00 08:58:11)
     *
     * @param newNom
     *            java.lang.String
     */
    private void setNom(final java.lang.String newNom) {
        nom = newNom;
    }

    /**
     * Méthode non commentée.
     *
     * @param newValue
     *            com.jsbsoft.jtf.session.ComposantCollecte
     */
    private void setParserRequete(final ParserRequete newValue) {
        this.parser = newValue;
    }

    /**
     * Accesseur.
     *
     * @param ss
     *            the ss
     */
    private void setSessionPartenaire(final SousSession ss) {
        this.sessionPartenaire = ss;
    }

    /**
     * Calcul de la prochaine page demandée par le processus.
     *
     * @return the descriptif page retour
     *
     */
    private DescriptifPageRetour descriptifFluxProcessus() {
        boolean finProcessus = false;
        if (StringUtils.isEmpty(getInfoBean().getMessageErreur())) {
            finProcessus = gp.traiterAction();
        }
        DescriptifPageRetour desc = getControleurNavigation().getProchainFlux();
        // si le processus n'a pas fini
        if (!finProcessus) {
            // appel recursif
            // si le controleur de navigation ne renvoie pas de flux,
            // c'est qu'il a récupéré les données autrement et que le processus peut continuer
            if (desc == null) {
                desc = descriptifFluxProcessus();
            }
            // en cas de redirection on arrête la procedure
            else if (desc.getTypeDeFlux() == TypeFlux.URL) {
                LOG.debug(">>> fin procédure par redirection");
                dereferencerPU();
            }
        }
        // le processus est terminé
        // le controleur de navigation a peut-être une dernière page à afficher (ex: les résultats d'un calcul)
        else if (gp.existeUnProcessus()) {
            // appel recursif
            if (desc == null) {
                desc = descriptifFluxRetour();
            }
        } else {
            LOG.debug(">>> fin procédure");
            //arrêter la procédure
            dereferencerPU();
        }
        return desc;
    }

    /**
     * Accesseur.
     *
     * @return the locale
     */
    public Locale getLocale() {
        return ContexteUtil.getContexteUniv().getLocale();
    }

    /**
     * Accesseur.
     *
     * @return the processus
     */
    protected final AbstractProcessusBean getProcessus() {
        return gp.getProcessus();
    }

    /**
     * Accesseur.
     *
     * @return the processus manager
     */
    protected final ProcessusManager getProcessusManager() {
        return gp;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (25/06/2002 12:44:12)
     *
     * @return boolean
     */
    public boolean isRestaurable() {
        return restaurable;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (25/06/2002 12:44:12)
     *
     * @param newRestaurable
     *            boolean
     */
    public void setRestaurable(final boolean newRestaurable) {
        restaurable = newRestaurable;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (25/06/2002 12:44:12)
     *
     * @return boolean
     */
    public boolean getMemorisationCI() {
        return memorisationCI;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(final String extension) {
        this.extension = extension;
    }
}
