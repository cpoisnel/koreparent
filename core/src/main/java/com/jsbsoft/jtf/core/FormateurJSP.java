package com.jsbsoft.jtf.core;

import java.io.IOException;
import java.io.Writer;
import java.text.Collator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.upload.UploadedFile;
import com.jsbsoft.jtf.webutils.FormateurHTML;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.tree.processus.GroupsJsTree;
import com.univ.tree.processus.RubriquesJsTree;
import com.univ.tree.processus.StructuresJsTree;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.UnivFmt;

/**
 * Classe utilitaire destinée au formatage des JSP.
 */
public class FormateurJSP {

    /** The Constant FORMAT_TEXTE. */
    public final static int FORMAT_TEXTE = 0;

    /** The Constant FORMAT_COMBO. */
    public final static int FORMAT_COMBO = 1;

    /* Type d'affichage */

    /** The Constant FORMAT_DATE. */
    public final static int FORMAT_DATE = 2;

    /** The Constant FORMAT_ENTIER. */
    public final static int FORMAT_ENTIER = 3;

    /** The Constant FORMAT_DECIMAL. */
    public final static int FORMAT_DECIMAL = 4;

    /** The Constant FORMAT_MULTI_LIGNE. */
    public final static int FORMAT_MULTI_LIGNE = 5;

    /** The Constant FORMAT_LONG. */
    public final static int FORMAT_LONG = 6;

    /** The Constant FORMAT_CHECKBOX. */
    public final static int FORMAT_CHECKBOX = 7;

    /** The Constant FORMAT_RADIO. */
    public final static int FORMAT_RADIO = 8;

    /** The Constant FORMAT_FICHIER. */
    public final static int FORMAT_FICHIER = 9;

    /** The Constant FORMAT_TEXTE_CACHE. */
    public final static int FORMAT_TEXTE_CACHE = 10;

    /** Constante représentant le type d' un champ heure / minute */
    public final static int FORMAT_HEURE_MINUTE = 11;

    /** The Constant FORMAT_MULTI_LIGNE_HTML. */
    public final static int FORMAT_MULTI_LIGNE_HTML = 12;

    public final static int FORMAT_EMAIL = 13;

    public final static int FORMAT_URL = 14;

    /** The Constant FORMAT_LISTE_CHOIX_MULTIPLE. */
    public final static int FORMAT_LISTE_CHOIX_MULTIPLE = 9;

    /** The Constant BOUTON_VALIDER. */
    public final static int BOUTON_VALIDER = 0;

    /** The Constant BOUTON_REVENIR. */
    public final static int BOUTON_REVENIR = 1;

    /* Types de boutons  */

    /** The Constant BOUTON_ABANDONNER. */
    public final static int BOUTON_ABANDONNER = 2;

    /** The Constant BOUTON_ANNULER. */
    public final static int BOUTON_ANNULER = 3;

    /** The Constant BOUTON_SUPPRIMER. */
    public final static int BOUTON_SUPPRIMER = 4;

    /** The Constant BOUTON_RECHERCHER. */
    public final static int BOUTON_RECHERCHER = 5;

    /** The Constant BOUTON_CONFIRMER. */
    public final static int BOUTON_CONFIRMER = 6;

    /** The Constant BOUTON_DETAIL. */
    public final static int BOUTON_DETAIL = 7;

    /** The Constant BOUTON_AJOUTER. */
    public final static int BOUTON_AJOUTER = 8;

    /** The Constant BOUTON_CREER. */
    public final static int BOUTON_CREER = 9;

    /** The Constant BOUTON_SUITE. */
    public final static int BOUTON_SUITE = 10;

    /** The Constant BOUTON_MODIFIER. */
    public final static int BOUTON_MODIFIER = 11;

    /** The Constant BOUTON_TRADUIRE. */
    public final static int BOUTON_TRADUIRE = 12;

    /** The Constant BOUTON_ENREGISTRER. */
    public final static int BOUTON_ENREGISTRER = 13;

    /** The Constant SAISIE_AFFICHAGE. */
    public final static int SAISIE_AFFICHAGE = 0;

    /** The Constant SAISIE_FACULTATIF. */
    public final static int SAISIE_FACULTATIF = 1;
    // Associations boutons image

    /** The Constant SAISIE_OBLIGATOIRE. */
    public final static int SAISIE_OBLIGATOIRE = 2;

    /* Option de saisie (affichage,facultatif,obligatoire) */

    /** The Constant SAISIE_OBLIGATOIRE Avec première valeur selectionnée par défaut */
    public final static int SAISIE_OBLIGATOIRE_SELECTION_DEFAUT = 3;

    /** The Constant ORDRE_NORMAL. */
    public final static int ORDRE_NORMAL = 0;

    /** The Constant ORDRE_INVERSE. */
    public final static int ORDRE_INVERSE = 1;

    private static final Logger LOG = LoggerFactory.getLogger(FormateurJSP.class);

    /* indice du champ dans la page */

    /** The Constant LN. */
    private static final String LN = "\n";

    /* Gestion des radio-boutons */
    // Indice du radio-bouton dans le groupe

    /** The Constant images. */
    private final static String images[][] = {{InfoBean.ACTION_VALIDER, "Valider"}, {InfoBean.ACTION_REVENIR, "Revenir"}, {InfoBean.ACTION_ABANDON, "Abandonner"}, {InfoBean.ACTION_ANNULER, "Annuler"}, {InfoBean.ACTION_SUPPRIMER, "Supprimer"}, {InfoBean.ACTION_RECHERCHER, "Rechercher"}, {InfoBean.ACTION_CONFIRMER, "Confirmer"}, {InfoBean.ACTION_DETAIL, "D&eacute;tail"}, {InfoBean.ACTION_AJOUTER, "Ajouter"}, {InfoBean.ACTION_CREER, "Cr&eacute;er"}, {InfoBean.ACTION_SUITE, "Suite"}, {InfoBean.ACTION_MODIFIER, "Modifier"}, {InfoBean.ACTION_TRADUIRE, "Traduire"}, {InfoBean.ACTION_ENREGISTRER, "Enregistrer"}};
    // Nom du groupe

    /** The indice champ. */
    private int indiceChamp = -1;

    /* ordre des éléments pour les boutons radios */

    /** The indice radio. */
    private int indiceRadio = -1;

    /** The nom groupe radio. */
    private String nomGroupeRadio = "";

    /**
     * Commentaire relatif au constructeur FormateurJSP.
     */
    public FormateurJSP() {
        indiceChamp = 0;
    }

    /**
     * Décompose une Hashtable en 2 vecteurs (triés par cle ou libellé).
     *
     * @param h
     *            the h
     * @param _vCle
     *            the _v cle
     * @param _vLibelle
     *            the _v libelle
     * @param triCle
     *            the tri cle
     */
    public static void decomposerHashtable(final Hashtable<String, String> h, final Vector<String> _vCle, final Vector<String> _vLibelle, final boolean triCle) {
        /* Tri de la HashTable -> Vector */
        for (final Enumeration<String> e = h.keys(); e.hasMoreElements(); ) {
            String cleItem = e.nextElement();
            String libelleItem = h.get(cleItem);
            int i = 0;
            if (!triCle) {
                // tri par libelle
                for (final Enumeration<String> v = _vLibelle.elements(); v.hasMoreElements() && libelleItem.compareTo(v.nextElement()) > 0; ) {
                    i++;
                }
            } else {
                // tri par cle
                for (final Enumeration<String> v = _vCle.elements(); v.hasMoreElements() && cleItem.compareTo(v.nextElement()) > 0; ) {
                    i++;
                }
            }
            _vCle.insertElementAt(cleItem, i);
            _vLibelle.insertElementAt(libelleItem, i);
        }
    }

    /**
     * Renvoie la valeur d'un paramètre de formatage (ou null) en majuscule.
     *
     * @param _format
     *            the _format
     * @param _nomParametre
     *            the _nom parametre
     *
     * @return the string
     */
    protected static String renvoyerValeurFormatage(final String _format, final String _nomParametre) {
        String res = renvoyerValeurFormatageMinus(_format, _nomParametre);
        if (res != null) {
            res = res.toUpperCase();
        }
        return res;
    }

    /**
     * Renvoie la valeur d'un paramètre de formatage (ou null).
     *
     * @param _format
     *            the _format
     * @param _nomParametre
     *            the _nom parametre
     *
     * @return the string
     */
    protected static String renvoyerValeurFormatageMinus(final String _format, final String _nomParametre) {
        String res = null;
        final StringTokenizer st = new StringTokenizer(_format, ",");
        while (st.hasMoreTokens()) {
            final String val = st.nextToken();
            if (val.toLowerCase().indexOf(_nomParametre.toLowerCase()) == 0) {
                if (_nomParametre.length() + 1 < val.length()) {
                    res = val.substring(_nomParametre.length() + 1);
                } else {
                    res = StringUtils.EMPTY;
                }
            }
        }
        return res;
    }

    /**
     * trie une Hashtable sur la cle.
     *
     * @param h
     *            the h
     *
     * @return the vector
     */
    public static Vector<String> trierHashtableParCle(final Hashtable<String, String> h) {
        final Vector<String> vCle = new Vector<>();
        final Vector<String> vLibelle = new Vector<>();
        decomposerHashtable(h, vCle, vLibelle, true);
        return vCle;
    }

    /**
     * trie une Hashtable sur la cle.
     *
     * @param h
     *            the h
     *
     * @return the vector
     */
    public static Vector<String> trierHashtableParLibelle(final Hashtable<String, String> h) {
        final Vector<String> vCle = new Vector<>();
        final Vector<String> vLibelle = new Vector<>();
        decomposerHashtable(h, vCle, vLibelle, false);
        return vCle;
    }

    /**
     * Formatage d'un radio-bouton.
     *
     * @param nomDonnee
     *            the nom donnee
     * @param valeur
     *            the valeur
     *
     * @return the string
     */
    private String formaterCheckbox(final String nomDonnee, final String valeur, final int optionModification) {
        String selected = StringUtils.EMPTY;
        if ("1".equals(valeur)) {
            selected = "checked=\"checked\"";
        }
        return "<input " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " class=\"checkbox\" type=\"checkbox\" id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "\" value=\"1\" " + selected + " />";
    }

    /**
     * Formatage d'un radio-bouton.
     *
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the valeur
     *
     * @return the string
     */
    private String formaterRadioBouton(final InfoBean infoBean, final String nomDonnee, final int optionModification) {
        String valeur = StringUtils.defaultString(infoBean.getString(nomDonnee));
        if (StringUtils.isEmpty(valeur) && infoBean.get("DONOT_PRESELECT_" + nomDonnee) == null) {
            valeur = "0";
        }
        if (nomDonnee.equalsIgnoreCase(nomGroupeRadio)) {
            indiceRadio++;
        } else {
            /* Nouveau groupe */
            nomGroupeRadio = nomDonnee;
            indiceRadio = 0;
        }
        String checked = StringUtils.EMPTY;
        String value = String.valueOf(indiceRadio);
        // si la valeur du champ est renseignée
        if (StringUtils.isNotEmpty(infoBean.getString(nomDonnee + indiceRadio))) {
            value = infoBean.getString(nomDonnee + indiceRadio);
            if (value.equals(valeur)) {
                checked = "checked=\"checked\"";
            }
        } else if (StringUtils.isNotEmpty(valeur) && Integer.parseInt(valeur) == indiceRadio) {
            checked = "checked=\"checked\"";
        }
        return "<input " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " class=\"radio\" type=\"radio\" id=\"" + nomDonnee + "_" + indiceRadio + "\" name=\"" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(value) + "\" " + checked + " />";
    }

    /**
     * Insère le code HTML d'une série de boutons de type SUBMIT . <br/>
     *
     * Liste des boutons :
     * <UL>
     * <LI>BOUTON_VALIDER (*)
     * <LI>BOUTON_REVENIR
     * <LI>BOUTON_ABANDONNER
     * <LI>BOUTON_ANNULER
     * <LI>BOUTON_SUPPRIMER
     * <LI>BOUTON_RECHERCHER
     * <LI>BOUTON_CONFIRMER (*)
     * <LI>BOUTON_DETAIL
     * <LI>BOUTON_AJOUTER
     * <LI>BOUTON_CREER
     * <LI>BOUTON_SUITE (*)
     * <LI>BOUTON_MODIFIER (*)
     * <LI>BOUTON_TRADUIRE
     * <LI>BOUTON_ENREGISTRER (*)
     *
     * <br/>
     * (*) Pour ces boutons, un contrôle de format avant d'activer le processus .
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param boutons
     *            the boutons
     */
    public void insererBoutons(final Writer out, final InfoBean infoBean, final int boutons[]) {
        try {
            out.write("<p class=\"validation\">");
            for (final int bouton : boutons) {
                String libelle = MessageHelper.getCoreMessage("JTF_BOUTON_" + images[bouton][1].toUpperCase());
                if (libelle == null) {
                    libelle = images[bouton][1];
                }
                out.write("<input id=\"" + images[bouton][0].toLowerCase() + "\" type=\"submit\" name=\"" + images[bouton][0] + "\" value=\"" + libelle + "\" /> ");
            }
            out.write("</p>");
        } catch (final IOException e) {
            LOG.error("erreur sur l'insertion des boutons", e);
        }
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     */
    public void insererChampSaisie(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax) {
        insererChampSaisie(out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, nbCarMax, "-");
    }

    /**
     * AM 200501 : nouvelle méthode pour choix de la taille du champ texte (différente de maxlength).
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param format
     *            the format
     */
    public void insererChampSaisie(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final String format) {
        insererChampSaisie(out, infoBean, nomDonnee, optionModification, typeFormat, nbCarMin, nbCarMax, nbCarMax, format);
    }

    /**
     * Insère le code HTML d'un champ dans la page JSP . Les formats supportés sont :
     * <UL>
     * <LI>FORMAT_TEXTE
     * <LI>FORMAT_COMBO
     * <LI>FORMAT_DATE
     * <LI>FORMAT_ENTIER
     * <LI>FORMAT_DECIMAL
     * <LI>FORMAT_MULTI_LINE
     * <LI>FORMAT_LONG
     * <LI>FORMAT_CHECKBOX
     * <LI>FORMAT_RADIO
     * <LI>FORMAT_HEURE_MINUTE <br/>
     * format = {EMAIL}
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param typeFormat
     *            the type format
     * @param nbCarMin
     *            the nb car min
     * @param nbCarMax
     *            the nb car max
     * @param size
     *            the size
     * @param format
     *            the format
     */
    public void insererChampSaisie(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final int typeFormat, final int nbCarMin, final int nbCarMax, final int size, final String format) {
        /* Insertion du format */
        try {
            /* Le résultat doit être du type :
            <input type="hidden" name="#FORMAT_NAME" value="OPTIONMODIF,TYPEFORMAT,CARMIN,CARMAX;FORMAT;INDICE">
             */
            if (optionModification != SAISIE_AFFICHAGE) {
                out.write("<input type=\"hidden\" name=\"#FORMAT_" + nomDonnee + "\" value=\"" + optionModification + ";" + typeFormat + ";" + nbCarMin + ";" + nbCarMax + ";" + transformerFormat(format) + ";" + indiceChamp++ + "\" />");
            }
        } catch (final IOException e) {
            LOG.error("erreur sur l'écriture dans la sortie", e);
        }
        /* Insertion du champ HTML */
        /**************************/
        /* Variable non controlee */
        /**************************/
        //JSS 20020625-001 (pour la posterité)
        if (!infoBean.isControled(nomDonnee) && optionModification != SAISIE_AFFICHAGE) {
            String valeur = StringUtils.EMPTY;
            if (infoBean.get(nomDonnee) != null) {
                valeur = (String) infoBean.get(nomDonnee);
            }
            try {
                int nbCar = 32; // 32 caractères par défaut
                if (nbCarMax != 0) {
                    nbCar = nbCarMax;
                }
                /*
                 Le résultat doit être du type :
                <input type="TEXT"
                name="TNOUS" value='<%=information.get("TNOUS") %>' SIZE="50">
                 */
                if (typeFormat == FORMAT_MULTI_LIGNE || typeFormat == FORMAT_MULTI_LIGNE_HTML) {
                    out.write(formaterTextArea(nomDonnee, valeur, nbCarMax, format, optionModification));
                    out.write(LN);
                } else if (typeFormat == FORMAT_CHECKBOX) {
                    out.write(formaterCheckbox(nomDonnee, valeur, optionModification));
                    out.write(LN);
                } else if (typeFormat == FORMAT_RADIO) {
                    out.write(formaterRadioBouton(infoBean, nomDonnee, optionModification));
                    out.write(LN);
                } else if (typeFormat == FORMAT_FICHIER) {
                    out.write("<input " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " type=\"file\" id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "_FILE" + "\" />");
                    if (valeur.length() > 0) {
                        out.write("<br />" + EscapeString.escapeHtml(valeur));
                    } else {
                        // taille maximum du fichier autorisee
                        String alerte = StringUtils.EMPTY;
                        String array = StringUtils.EMPTY;
                        String maxSize = StringUtils.EMPTY;
                        if (StringUtils.isNotEmpty(infoBean.getString(UploadedFile.KEY_MAX_FILE_SIZE))) {
                            maxSize = infoBean.getString(UploadedFile.KEY_MAX_FILE_SIZE);
                        } else if (StringUtils.isNotEmpty(PropertyHelper.getCoreProperty("fichiergw.maxsize"))) {
                            maxSize = PropertyHelper.getCoreProperty("fichiergw.maxsize");
                        }
                        if (StringUtils.isNotBlank(maxSize)) {
                            try {
                                alerte += MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.SOURCE.TAILLE_MAX") + Integer.parseInt(maxSize) / 1024 + " Mo";
                            } catch (final NumberFormatException e) {
                                LOG.debug("unable tu parse value " + maxSize, e);
                            }
                        }
                        // filtre des extensions autorisees
                        String extensions = StringUtils.EMPTY;
                        if (infoBean.getString(UploadedFile.KEY_FILE_EXTENSIONS + "_" + nomDonnee) != null) {
                            extensions = infoBean.getString(UploadedFile.KEY_FILE_EXTENSIONS + "_" + nomDonnee);
                        } else if (StringUtils.isNotEmpty(PropertyHelper.getCoreProperty("fichiergw.extensions"))) {
                            extensions = PropertyHelper.getCoreProperty("fichiergw.extensions");
                        }
                        if (StringUtils.isNotBlank(extensions)) {
                            final String[] tExt = extensions.split(",");
                            String tmpExt = StringUtils.EMPTY;
                            for (final String string : tExt) {
                                if (tmpExt.length() > 0) {
                                    tmpExt += ", ";
                                    array += ",";
                                }
                                tmpExt += "." + string + "";
                                array += "\'." + string + "\'";
                            }
                            if (alerte.length() > 0) {
                                alerte += "<br />";
                            }
                            alerte += "Extension(s) : " + tmpExt;
                        }
                        if (StringUtils.isNotBlank(alerte)) {
                            out.write("<button type=\"button\" class=\"infobulle\" data-original-title=\"" + EscapeString.escapeAttributHtml(alerte) + "\"></button>");
                        }
                        if (StringUtils.isNotBlank(extensions)) {
                            out.write("<script type=\"text/javascript\">");
                            out.write("formFileExtensions['" + nomDonnee + "']= new Array(" + array + ");");
                            out.write("</script>");
                        }
                    }
                    out.write(LN);
                } else if (typeFormat == FORMAT_DATE) {
                    final StringBuilder champDate = new StringBuilder();
                    champDate.append("<input ").append(optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "").append(" class=\"type_date\" placeholder=\"JJ/MM/AAAA\" type=\"text\" id=\"").append(nomDonnee).append("\" name=\"").append(nomDonnee).append("\" value=\"").append(EscapeString.escapeAttributHtml(valeur));
                    if (nbCarMax != 0) {
                        champDate.append("\" maxlength=\"").append(nbCarMax);
                    }
                    champDate.append("\" size=\"").append(size).append("\" />");
                    out.write(champDate.toString());
                } else if (typeFormat == FORMAT_EMAIL) {
                    final StringBuilder champDate = new StringBuilder();
                    champDate.append("<input ").append(optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "").append(" class=\"type_email\" type=\"text\" id=\"").append(nomDonnee).append("\" name=\"").append(nomDonnee).append("\" value=\"").append(EscapeString.escapeAttributHtml(valeur));
                    if (nbCarMax != 0) {
                        champDate.append("\" maxlength=\"").append(nbCarMax);
                    }
                    champDate.append("\" size=\"").append(size).append("\" />");
                    out.write(champDate.toString());
                } else if (typeFormat == FORMAT_URL) {
                    final StringBuilder champDate = new StringBuilder();
                    champDate.append("<input ").append(optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "").append(" class=\"type_url\" type=\"text\" id=\"").append(nomDonnee).append("\" name=\"").append(nomDonnee).append("\" value=\"").append(EscapeString.escapeAttributHtml(valeur));
                    if (nbCarMax != 0) {
                        champDate.append("\" maxlength=\"").append(nbCarMax);
                    }
                    champDate.append("\" size=\"").append(size).append("\" />");
                    out.write(champDate.toString());
                } else if (typeFormat != FORMAT_TEXTE_CACHE) {
                    // Modif JSS 25/03 pour modification encadre (1ère toolbox en séquentiel)
                    // Modif AM 20030205-001 pour renvoyer bon formatage du champ. (attribut value avec " au lieu de ')
                    out.write(formaterChampTexte(nomDonnee, valeur, nbCar, nbCar, format, optionModification));
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
            return;
        }
        /**************************/
        /* Variable controlee     */
        /**************************/
        String value = StringUtils.EMPTY;
        if (typeFormat == FORMAT_TEXTE) {
            try {
                /* Le résultat doit être du type :
                <input type="TEXT"
                name="TNOUS" value='<%=information.get("TNOUS") %>' SIZE="50">
                 */
                value = (String) infoBean.get(nomDonnee);
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write(formaterChampTexte(nomDonnee, value, nbCarMax, size, format, optionModification));
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_CHECKBOX) {
            try {
                value = (String) infoBean.get(nomDonnee);
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write(formaterCheckbox(nomDonnee, value, optionModification));
                    out.write(LN);
                } else {
                    /* Pour le mode affichage */
                    value = StringUtils.defaultString(value);
                    if ("0".equals(value)) {
                        value = StringUtils.EMPTY;
                    } else if ("1".equals(value)) {
                        value = "X";
                    }
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_RADIO) {
            try {
                value = (String) infoBean.get(nomDonnee);
                out.write(formaterRadioBouton(infoBean, nomDonnee, optionModification));
                out.write(LN);
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_ENTIER) {
            try {
                int nbCar = 9; // 9 caractères par défaut
                if (nbCarMax != 0) {
                    nbCar = nbCarMax;
                }
                /* Le résultat doit être du type :
                <input type="TEXT"
                name="TNOUS" value='<%=information.get("TNOUS") %>' SIZE="50">
                 */
                value = Formateur.formater(infoBean.get(nomDonnee));
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write("<input class=\"numeric_input\" " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " type=\"text\" id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(value) + "\" size=\"" + size + "\" maxlength=\"" + nbCar + "\"/>");
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_LONG) {
            try {
                int nbCar = 9; // 9 caractères par défaut
                if (nbCarMax != 0) {
                    nbCar = nbCarMax;
                }
                /* Le résultat doit être du type :
                <input type="TEXT"
                name="TNOUS" value='<%=information.get("TNOUS") %>' SIZE="50">
                 */
                value = Formateur.formater(infoBean.get(nomDonnee));
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write("<input " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " type=\"text\" id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(value) + "\" size=\"" + size + "\" maxlength=\"" + nbCar + "\"/>");
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_MULTI_LIGNE || typeFormat == FORMAT_MULTI_LIGNE_HTML) {
            try {
                /* Le résultat doit être du type :
                <textarea name="item_description" cols=40 rows="4" wrap="SOFT">
                blabla
                </textarea>
                 */
                value = Formateur.formater(infoBean.get(nomDonnee));
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write(formaterTextArea(nomDonnee, value, nbCarMax, format, optionModification));
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_DATE) {
            try {
                value = Formateur.formater(infoBean.get(nomDonnee));
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write("<input type=\"text\" class=\"type_date\" placeholder=\"" + MessageHelper.getCoreMessage("ST_RECHERCHE_FORMAT_DATE") + "\" id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(value) + "\" size=\"11\" title=\"" + MessageHelper.getCoreMessage("ST_RECHERCHE_FORMAT_DATE") + " " + (optionModification == SAISIE_OBLIGATOIRE ? MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") : "") + "\"/>");
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_EMAIL) {
            try {
                value = Formateur.formater(infoBean.get(nomDonnee));
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write("<input type=\"text\" class=\"type_email\" id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(value) + "\" size=\"40\" " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " maxlength=\"" + size + "\"/>");
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_URL) {
            try {
                value = Formateur.formater(infoBean.get(nomDonnee));
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write("<input type=\"text\" class=\"type_url\" id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(value) + "\" size=\"40\" " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " maxlength=\"" + size + "\"/>");
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_DECIMAL) {
            try {
                /* Le résultat doit être du type :
                <input type="TEXT"
                name="TNOUS" value='<%=information.get("TNOUS") %>' SIZE="10">
                 */
                value = Formateur.formater(infoBean.get(nomDonnee));
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write("<input " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " type=\"text\" name=\"" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(value) + "\" size=\"10\" />");
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (typeFormat == FORMAT_FICHIER) {
            try {
                /* Le résultat doit être du type :
                <input type="file" name="FICHIER" METHOD="POST">
                 */
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write("<input " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " type=\"file\" name=\"" + nomDonnee + "_FILE\"" + " />");
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        //champ de type heure/minutes (HH:mm)
        if (typeFormat == FORMAT_HEURE_MINUTE) {
            try {
                value = Formateur.formater(infoBean.get(nomDonnee));
                if (optionModification != SAISIE_AFFICHAGE) {
                    out.write("<input type=\"text\" placeholder=\"HH:mm\" class=\"input-append type_time\" pattern=\"\\d\\d:\\d\\d\" id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "\" value=\"" + EscapeString.escapeAttributHtml(value) + "\" size=\"5\" title=\"" + MessageHelper.getCoreMessage("ST_RECHERCHE_FORMAT_TIME") + " " + (optionModification == SAISIE_OBLIGATOIRE ? MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") : "") + "\"/>");
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
        if (optionModification == SAISIE_AFFICHAGE) {
            try {
                if (StringUtils.isNotEmpty(value)) {
                    out.write("<span>" + FormateurHTML.formaterEnHTML(value) + "</span>");
                    out.write(LN);
                } else {
                    out.write("<span>&nbsp;</span>");
                    out.write(LN);
                }
            } catch (final IOException e) {
                LOG.error("erreur sur l'écriture sur la sortie", e);
            }
        }
    }

    /**
     * Insère le code HTML d'une COMBO (stocké en fichier texte) dans une JSP <br/>
     * Lève une exception en cas d'erreur.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomTable
     *            the nom table
     */
    public void insererCombo(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomTable) {
        insererCombo(out, infoBean, nomDonnee, optionModification, infoBean.getNomExtension(), nomTable, "");
    }

    /**
     * Insère le code HTML d'une COMBO (stocké en fichier .dat) dans une JSP <br/>
     * Lève une exception en cas d'erreur. <br/>
     * format peut contenir "PAS_DE_TRI"
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomTable
     *            the nom table
     * @param format
     *            the format
     */
    public void insererCombo(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomTable, final String format) {
        insererCombo(out, infoBean, nomDonnee, optionModification, infoBean.getNomExtension(), nomTable, format);
    }

    /**
     * Insère le code HTML d'une COMBO (stocké en fichier .dat) dans une JSP <br/>
     * Lève une exception en cas d'erreur. <br/>
     * format peut contenir "PAS_DE_TRI"
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param idExtention id de l'extension
     * @param nomTable
     *            the nom table
     * @param format
     *            the format
     */
    public void insererCombo(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String idExtention, final String nomTable, final String format) {
        final Map<String, String> table = CodeLibelle.lireTable(idExtention, nomTable, ContexteUtil.getContexteUniv().getLocale());
        insererCombo(out, infoBean, nomDonnee, optionModification, table, format);
    }

    /**
     * Inserer combo.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param table
     *            the table
     * @param format
     *            the format
     */
    public void insererCombo(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final Map<String, String> table, final String format) {
        insererComboMultiple(out, infoBean, nomDonnee, optionModification, table, format, 1);
    }

    /**
     * Insère le code HTML d'une COMBO (stocké en Hashtable [code, libelle] ) dans une JSP <br/>
     * Lève une exception en cas d'erreur. <br/>
     * format peut contenir "PAS_DE_TRI" ou "TRI_CLE" Si le paramètre size est > 1, le select sera multiple
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param table
     *            the table
     * @param format
     *            the format
     * @param size
     *            taille du select (nombre de lignes affichées), le select sera multiple si size >1
     */
    public void insererComboMultiple(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final Map<String, String> table, final String format, final int size) {
        //    AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        try {
            if (optionModification == SAISIE_AFFICHAGE) {
                //AM 200501 : evolution LMD
                String szTmp = nomDonnee;
                if (nomDonnee.startsWith("TMP_")) {
                    szTmp = szTmp.substring(4);
                }
                final String valeurDonnee = StringUtils.defaultString(infoBean.getString(szTmp));
                final StringTokenizer stValeurs = new StringTokenizer(valeurDonnee, ";");
                int i = 0;
                while (stValeurs.hasMoreTokens()) {
                    /* si le libellé n'est pas trouvé, on affiche vide au lieu de null */
                    String valeurTable = StringUtils.defaultString(table.get(stValeurs.nextToken()));
                    if (i > 0) {
                        out.write(",");
                    }
                    out.write(EscapeString.escapeHtml(valeurTable));
                    i++;
                }
                out.write(LN);
            } else {
                /* Le résultat doit être du type :
                <input type="hidden" name="#FORMAT_NAME" value="OPTIONMODIF,TYPEFORMAT,CARMIN,CARMAX">
                 */
                //AM 200501 : sur les sélections multiples, la zone qui doit être controlée n'est pas TMP_
                //le controle sur obligatoire ou non ne marchait pas.
                String szTmp = nomDonnee;
                if (szTmp.startsWith("TMP_")) {
                    szTmp = szTmp.substring(4);
                }
                out.write("<input type=\"hidden\" name=\"#FORMAT_" + szTmp + "\" value=\"" + optionModification + ";" + FORMAT_COMBO + ";0;0;" + transformerFormat(format) + ";" + (indiceChamp++) + "\" />");
                /* Insertion du champ HTML */
                final Object o = infoBean.get(nomDonnee);
                String valeurDonnee = StringUtils.EMPTY;
                if (o != null) {
                    valeurDonnee = StringUtils.defaultString(o.toString());
                }
                /* Le résultat doit être du type :
                <option value=null></option>
                <option value="030">Allemagne</option>
                <option value="031">Danemark</option>
                <option value="032" SELECTED >France</option>
                <option value="033">Hollande</option>
                <option value="034">Vatican</option>
                 */
                String multiple = StringUtils.EMPTY;
                if (size > 1) {
                    multiple = "multiple";
                }
                out.write("<select " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " " + multiple + " id=\"" + nomDonnee + "\" name=\"" + nomDonnee + "\" size=\"" + size + "\">");
                out.write(LN);
                if (table.get(" ") != null) {
                    out.write("<option value=\"\">" + StringUtils.defaultIfEmpty(table.get(" "), MessageHelper.getCoreMessage("JTF_SELECTIONNER_LISTE")) + "</option>");
                    out.write(LN);
                } else if (table.get("0000") != null) {
                    out.write("<option value=\"0000\">" + StringUtils.defaultIfEmpty(table.get("0000"), MessageHelper.getCoreMessage("JTF_SELECTIONNER_LISTE")) + "</option>");
                    out.write(LN);
                } else {
                    out.write("<option value=\"0000\">" + MessageHelper.getCoreMessage("JTF_SELECTIONNER_LISTE") + "</option>");
                    out.write(LN);
                }
                String cleItem = "", libelleItem = "";
                /* Tri de la HashTable -> Vector */
                final Vector<String> vector = new Vector<>(table.size());
                for (final Map.Entry<String, String> currentEntry: table.entrySet()) {
                    cleItem = currentEntry.getKey();
                    if (!" ".equals(cleItem)) {
                        libelleItem = currentEntry.getValue();
                        if (format.contains("PAS_DE_TRI")) {
                            vector.insertElementAt(libelleItem + "," + cleItem, 0);
                        } else {
                            if (format.contains("TRI_CLE_NUM")) {
                                int i = 0;
                                final Enumeration<String> v = vector.elements();
                                while (v.hasMoreElements()) {
                                    final String elemVect = v.nextElement();
                                    final float valCleItem = Float.parseFloat(cleItem);
                                    final String cleElemVect = elemVect.substring(elemVect.indexOf(",") + 1);
                                    final float valCleElemVect = Float.parseFloat(cleElemVect);
                                    if (valCleItem > valCleElemVect) {
                                        i++;
                                    }
                                }
                                vector.insertElementAt(libelleItem + "," + cleItem, i);
                            } else if (format.contains("TRI_CLE")) {
                                int i = 0;
                                final Enumeration<String> v = vector.elements();
                                while (v.hasMoreElements()) {
                                    final String elemVect = v.nextElement();
                                    final String cleElemVect = elemVect.substring(elemVect.lastIndexOf(",") + 1);
                                    if (cleItem.compareTo(cleElemVect) > 0) {
                                        i++;
                                    }
                                }
                                vector.insertElementAt(libelleItem + "," + cleItem, i);
                            } else {
                                // TRI ( par défaut)
                                int i = 0;
                                final Collator collator = Collator.getInstance(Locale.FRANCE);
                                for (final Enumeration<String> v = vector.elements(); v.hasMoreElements() && collator.compare(libelleItem, v.nextElement()) > 0; ) {
                                    i++;
                                }
                                vector.insertElementAt(libelleItem + "," + cleItem, i);
                            }
                        }
                    }
                }
                /* Tri de la HashTable -> Vector */
                for (final Object element : vector) {
                    final String nextElt = (String) element;
                    if (nextElt.contains(",")) {
                        libelleItem = nextElt.substring(0, nextElt.lastIndexOf(","));
                        cleItem = nextElt.substring(nextElt.lastIndexOf(",") + 1);
                    } else {
                        libelleItem = nextElt;
                    }
                    String selection = StringUtils.EMPTY;
                    if (valeurDonnee.equals(cleItem)) {
                        selection = "selected = \"selected\"";
                    }
                    /*
                     * FG 20060712
                     * Possibilité de faire des groupes de libellés (uniquement si tri sur la clé)
                     */
                    if ((format.contains("TRI_CLE")) && libelleItem.startsWith("#GROUPE#")) {
                        out.write("<optgroup id=\"opt" + EscapeString.escapeAttributHtml(cleItem) + "\" label=\"" + libelleItem.substring(8) + "\" >");
                    } else if ((format.contains("TRI_CLE")) && libelleItem.startsWith("#/GROUPE#")) {
                        out.write("</optgroup>");
                    } else if (!libelleItem.startsWith("#/GROUPE#")) {
                        out.write("<option value=\"" + EscapeString.escapeAttributHtml(cleItem) + "\" " + selection + " title=\"" + libelleItem + "\">" + libelleItem + "</option>");
                    }
                    out.write(LN);
                }
                out.write("</select>");
                out.write(LN);
            }
        } catch (final IOException e) {
            LOG.error("erreur lors de l'insertion d'une combo multiple", e);
        }
    }

    /**
     * Insère le code HTML d'une COMBO (stocké dans une hashtable) dans une JSP <br/>
     * Lève une exception en cas d'erreur.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomHashtable
     *            the nom hashtable
     */
    public void insererComboHashtable(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomHashtable) {
        insererComboHashtable(out, infoBean, nomDonnee, optionModification, nomHashtable, StringUtils.EMPTY);
    }

    /**
     * Insère le code HTML d'une COMBO (stocké dans une hashtable) dans une JSP <br/>
     * Lève une exception en cas d'erreur.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @param nomDonnee
     *            the nom donnee
     * @param optionModification
     *            the option modification
     * @param nomHashtable
     *            the nom hashtable
     * @param format
     *            the format
     */
    public void insererComboHashtable(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomHashtable, final String format) {
        Map<String, String> table = infoBean.getMap(nomHashtable);
        if (table == null) {
            table = new Hashtable<>();
        }
        insererCombo(out, infoBean, nomDonnee, optionModification, table, format);
    }

    /**
     * Insère une fonction javascript de traitement d'erreur (<i>afficherErreur()</i>) <br/>
     * Cette fonction est destniée à être appelée sur la méthode onLoad() de la page HTML.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     * @deprecated
     */
    @Deprecated
    public void insererMessageErreur(final Writer out, final InfoBean infoBean) {}

    /**
     * Insère le code HTML des variables cachées dans la page JSP A INSERER Dans chaque formulaire <br/>
     * Lève une exception en cas d'erreur.
     *
     * @param out
     *            the out
     * @param infoBean
     *            the info bean
     */
    public void insererVariablesCachees(final Writer out, final InfoBean infoBean) {
        try {
            final Locale localeCourante = ContexteUtil.getContexteUniv().getLocale();
            if (infoBean.getModeEnchainement() == ProcedureBean.MODE_SEQUENTIEL) {
                out.write("<input type=\"hidden\" name=\"" + InfoBean.BEANPU + "\" value=\"" + EscapeString.escapeAttributHtml(infoBean.getBEANPU()) + "\" />");
                out.write(LN);
                out.write("<input type=\"hidden\" name=\"" + InfoBean.ID_JSP + "\" value=\"" + EscapeString.escapeAttributHtml(infoBean.getIDJSP()) + "\" />");
                out.write(LN);
                out.write("<input type=\"hidden\" name=\"" + InfoBean.ECRAN_LOGIQUE + "\" value=\"" + EscapeString.escapeAttributHtml(infoBean.getEcranLogique()) + "\" />");
                out.write(LN);
            } else {
                out.write("<input type=\"hidden\" name=\"" + InfoBean.PROC + "\" value=\"" + EscapeString.escapeAttributHtml(infoBean.getNomProcessus()) + "\" />");
                out.write(LN);
                out.write("<input type=\"hidden\" name=\"" + InfoBean.EXT + "\" value=\"" + EscapeString.escapeAttributHtml(infoBean.getNomExtension()) + "\" />");
                out.write(LN);
                out.write("<input type=\"hidden\" name=\"" + InfoBean.ECRAN_LOGIQUE + "\" value=\"" + EscapeString.escapeAttributHtml(infoBean.getEcranLogique()) + "\" />");
                out.write(LN);
                out.write("<input type=\"hidden\" name=\"" + InfoBean.ETAT_OBJET + "\" value=\"" + EscapeString.escapeAttributHtml(infoBean.getEtatObjet()) + "\" />");
                out.write(LN);
                final String nomObjet = StringUtils.defaultIfBlank(infoBean.getString("OBJET"), ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")));
                out.write("<input type=\"hidden\" name=\"OBJET\" value=\"" + EscapeString.escapeAttributHtml(nomObjet) + "\" />");
                out.write(LN);
                if (infoBean.get("RH") != null) {
                    out.write("<input type=\"hidden\" name=\"RH\" value=\"" + EscapeString.escapeAttributHtml(infoBean.getString("RH")) + "\" />");
                    out.write(LN);
                }
            }
            out.write("<input type=\"hidden\" name=\"LOCALE\" value=\"" + LangueUtil.getLangueLocale(localeCourante) + "\" />");
            out.write(LN);
        } catch (final IOException e) {
            LOG.error("erreur sur l'écriture sur la sortie", e);
        }
    }

    /**
     * Formatage d'un Input.
     *
     * @param _nomDonnee
     *            the _nom donnee
     * @param _valeur
     *            the _valeur
     * @param nbCarMax
     *            the nb car max
     * @param _size
     *            the _size
     * @param _format
     *            the _format
     *
     * @return the string
     */
    private String formaterChampTexte(final String _nomDonnee, final String _valeur, final int nbCarMax, final int _size, final String _format, final int optionModification) {
        String type = "text";
        String autoComplete = StringUtils.EMPTY;
        LOG.debug("FORMAT " + _nomDonnee + " = " + _format);
        final String param = renvoyerValeurFormatage(_format, "password");
        if (param != null) {
            type = "password";
        }
        final String autoCompleteParam = renvoyerValeurFormatage(_format, "noautocomplete");
        if(autoCompleteParam != null) {
            autoComplete = "autocomplete=\"nope\"";
        }
        /* Le résultat doit être du type :
        <input type="text"
        name="TNOUS" value='<%=information.get("TNOUS") %>' SIZE="50">
         */
        int size = 0;
        if (_size > 0 && _size != nbCarMax) {
            size = _size;
        } else if (nbCarMax > 0) {
            size = Math.min(nbCarMax, 50);
        } else {
            size = 32; // 32 caractères par défaut
        }
        String s = "<input " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " type=\"" + type + "\" id=\"" + _nomDonnee + "\" name=\"" + _nomDonnee + "\" value=\"" + StringUtils.defaultIfBlank(EscapeString.escapeAttributHtml(_valeur), StringUtils.EMPTY);
        if (nbCarMax != 0) {
            s += "\" maxlength=\"" + nbCarMax;
        }
        s += "\" size=\"" + size + "\" " + autoComplete + " />";
        return s;
    }

    /**
     * Formatage d'un TEXTAREA. TODO Verifier si il faut echapper ou pas (pbm chez certains client qui rentre de l'html dans les textarea
     *
     * @param _nomDonnee
     *            the _nom donnee
     * @param _valeur
     *            the _valeur
     * @param _format
     *            the _format
     *
     * @return the string
     */
    private String formaterTextArea(final String _nomDonnee, final String _valeur, final int nbCarMax, final String _format, final int optionModification) {
        String rows = "6";
        String param = renvoyerValeurFormatage(_format, "ROWS");
        if (param != null) {
            rows = param;
        }
        String cols = "40";
        param = renvoyerValeurFormatage(_format, "COLS");
        if (param != null) {
            cols = param;
        }
        String s = "<textarea " + (optionModification == SAISIE_OBLIGATOIRE ? "title=\"" + MessageHelper.getCoreMessage("ST_CHAMP_OBLIGATOIRE") + "\" required=\"required\"" : "") + " id=\"" + _nomDonnee + "\" name=\"" + _nomDonnee + "\" cols=\"" + cols + "\" rows=\"" + rows + "\"";
        if (nbCarMax != 0) {
            s += " maxlength=\"" + nbCarMax + "\"";
        }
        s += ">" + EscapeString.escapeHtml(_valeur) + "</textarea>";
        return s;
    }

    /**
     * Suppression des ' qui posent problème dans les balises INPUT. Suppression des " qui posent aussi problème
     *
     * @param format
     *            the _format
     *
     * @return the string
     */
    private String transformerFormat(final String format) {
        final String result = StringUtils.replace(format, "\"", " ");
        return StringUtils.replace(result, "'", "\'");
    }

    /**
     * Gets the indice champ.
     *
     * @return the indiceChamp
     */
    public int getIndiceChamp() {
        return indiceChamp;
    }

    /**
     * Sets the indice champ.
     *
     * @param indiceChamp
     *            the indiceChamp to set
     */
    public void setIndiceChamp(final int indiceChamp) {
        this.indiceChamp = indiceChamp;
    }

    public int getIndiceRadio() {
        return indiceRadio;
    }

    public void setIndiceRadio(final int indiceRadio) {
        this.indiceRadio = indiceRadio;
    }

    public String genererHiddenInputs(final boolean tmpField, final String nomDonnee, final int optionModification, final String format, final InfoBean infoBean, final int context) {
        final StringBuilder result = new StringBuilder();
        String szTmp = nomDonnee;
        if (szTmp.startsWith("TMP_")) {
            szTmp = szTmp.substring(4);
        }
        String value = "";
        if (infoBean.get(nomDonnee) != null) {
            value = infoBean.getString(nomDonnee);
        }
        String ariane = "";
        if (StringUtils.isNotBlank(value)) {
            final String[] values = value.split(";");
            final String[] tips = new String[values.length];
            for (int i = 0; i < values.length; i++) {
                switch (context) {
                    case UnivFmt.CONTEXT_ZONE:
                        tips[i] = RubriquesJsTree.getPath("", values[i], " > ");
                        break;
                    case UnivFmt.CONTEXT_STRUCTURE:
                        tips[i] = StructuresJsTree.getPath("", values[i], " > ");
                        break;
                    case UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION:
                    case UnivFmt.CONTEXT_GROUPEDSI_PUBLIC_VISE:
                        tips[i] = GroupsJsTree.getPath("", values[i], " > ");
                        break;
                }
            }
            ariane = StringUtils.join(tips, ";");
        }
        String libelle = StringUtils.EMPTY;
        if (infoBean.get("LIBELLE_" + nomDonnee) != null) {
            libelle = infoBean.getString("LIBELLE_" + nomDonnee);
        }
        result.append("<!-- DEBUT GESTION LISTE ").append(szTmp).append(" -->\n");
        result.append("<input type=\"hidden\" name=\"#FORMAT_").append(szTmp).append("\" value=\"").append(optionModification).append(";").append(FORMAT_COMBO).append(";0;0;").append(transformerFormat(format)).append(";").append(indiceChamp++).append("\" />");
        result.append("<input type=\"hidden\" id =\"").append(szTmp).append("\" name=\"").append(szTmp).append("\" value=\"").append(EscapeString.escapeAttributHtml(value)).append("\" ").append(optionModification == SAISIE_OBLIGATOIRE ? "required=\"required\"" : "").append("/>\n");
        // Faire le ménage
        result.append("<input type=\"hidden\" name=\"LIBELLE_").append(nomDonnee).append("\" value=\"").append(EscapeString.escapeAttributHtml(libelle)).append("\"/>\n");
        if (tmpField) {
            result.append("<input type=\"hidden\" name=\"ARIANE_").append(nomDonnee).append("\" value=\"").append(EscapeString.escapeAttributHtml(ariane)).append("\"/>\n");
            result.append("<input type=\"hidden\" name=\"TMP_").append(nomDonnee).append("\" value=\"\"/>\n");
        }
        result.append(LN);
        return result.toString();
    }

    public String genererListe(final Writer out, final InfoBean infoBean, final String nomDonnee, final int optionModification, final Map<String, String> table, final String format, final int context) {
        if (optionModification == -1) {
            return "";
        }
        final StringBuilder htmlList = new StringBuilder();
        try {
            if (optionModification == SAISIE_AFFICHAGE) {
                //AM 200501 : evolution LMD
                String szTmp = nomDonnee;
                if (nomDonnee.startsWith("TMP_")) {
                    szTmp = szTmp.substring(4);
                }
                final String valeurDonnee = StringUtils.defaultString(infoBean.getString(szTmp));
                final StringTokenizer stValeurs = new StringTokenizer(valeurDonnee, ";");
                int i = 0;
                while (stValeurs.hasMoreTokens()) {
                    /* si le libellé n'est pas trouvé, on affiche vide au lieu de null */
                    String valeurTable = StringUtils.defaultString(table.get(stValeurs.nextToken()));
                    if (i > 0) {
                        out.write(",");
                    }
                    htmlList.append("<li class=\"ignore\" data-value=\"").append(EscapeString.escapeAttributHtml(valeurDonnee)).append("\" title=\"").append(valeurTable).append("\">").append(valeurTable).append("</li>");
                    i++;
                }
                htmlList.append(LN);
            } else {
                /* Insertion du champ HTML */
                final Object o = infoBean.get(nomDonnee);
                if (o != null) {
                    StringUtils.defaultString(o.toString());
                }
                String libelleItem = null;
                /* Tri de la HashTable -> Vector */
                final Vector<String> vector = new Vector<>(table.size());
                for (final String cleItem : table.keySet()) {
                    if (StringUtils.isNotBlank(cleItem)) {
                        libelleItem = table.get(cleItem);
                        if (format.contains("PAS_DE_TRI")) {
                            vector.insertElementAt(libelleItem + "," + cleItem, 0);
                        } else {
                            if (format.contains("TRI_CLE_NUM")) {
                                int i = 0;
                                final Enumeration<String> v = vector.elements();
                                while (v.hasMoreElements()) {
                                    final String elemVect = v.nextElement();
                                    final float valCleItem = Float.parseFloat(cleItem);
                                    final String cleElemVect = elemVect.substring(elemVect.indexOf(",") + 1);
                                    final float valCleElemVect = Float.parseFloat(cleElemVect);
                                    if (valCleItem > valCleElemVect) {
                                        i++;
                                    }
                                }
                                vector.insertElementAt(libelleItem + "," + cleItem, i);
                            } else if (format.contains("TRI_CLE")) {
                                int i = 0;
                                final Enumeration<String> v = vector.elements();
                                while (v.hasMoreElements()) {
                                    final String elemVect = v.nextElement();
                                    final String cleElemVect = elemVect.substring(elemVect.lastIndexOf(",") + 1);
                                    if (cleItem.compareTo(cleElemVect) > 0) {
                                        i++;
                                    }
                                }
                                vector.insertElementAt(libelleItem + "," + cleItem, i);
                            } else {
                                // TRI ( par défaut)
                                int i = 0;
                                final Collator collator = Collator.getInstance(Locale.FRANCE);
                                for (final Enumeration<String> v = vector.elements(); v.hasMoreElements() && collator.compare(libelleItem, v.nextElement()) > 0; ) {
                                    i++;
                                }
                                vector.insertElementAt(libelleItem + "," + cleItem, i);
                            }
                        }
                    }
                }
                /* Tri de la HashTable -> Vector */
                for (final Object element : vector) {
                    final String nextElt = (String) element;
                    String cleItem = StringUtils.EMPTY;
                    if (nextElt.contains(",")) {
                        libelleItem = nextElt.substring(0, nextElt.lastIndexOf(","));
                        cleItem = nextElt.substring(nextElt.lastIndexOf(",") + 1);
                    } else {
                        libelleItem = nextElt;
                    }
                    htmlList.append("<li data-value=\"").append(EscapeString.escapeAttributHtml(cleItem)).append("\" ").append(" title=\"").append(libelleItem).append("\">").append(libelleItem).append("</li>");
                    htmlList.append(LN);
                }
            }
        } catch (final IOException e) {
            LOG.error("erreur sur a génération d'une liste", e);
        }
        return htmlList.toString();
    }
}