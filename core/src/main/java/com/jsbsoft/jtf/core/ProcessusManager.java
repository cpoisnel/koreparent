package com.jsbsoft.jtf.core;

import java.lang.reflect.Constructor;
import java.util.Stack;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.database.RequeteMgr;
import com.jsbsoft.jtf.exception.ErreurAsyncException;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

/**
 * Gère l'appel et l'enchaînement des processus Une activité doit d'abord être empilée puis appelée via la méthode traiterAction.
 */
public class ProcessusManager {

    /** The Constant TYPE_APPELANT_PU. */
    public final static int TYPE_APPELANT_PU = 0;

    /* Type de l'appelant d'un processus */

    /** The Constant TYPE_APPELANT_PROCESSUS. */
    public final static int TYPE_APPELANT_PROCESSUS = 1;

    private static final Logger LOG = LoggerFactory.getLogger(ProcessusManager.class);

    /** la pile des processus. */
    private final Stack<InfosProcessus> pileProcessus;

    /** la PU. */
    private ProcedureBean procedure = null;

    /**
     * _procedure : la ProcedureBean active.
     *
     * @param _procedure
     *            the _procedure
     */
    public ProcessusManager(final ProcedureBean _procedure) {
        procedure = _procedure;
        //empilement de la première (OU UNIQUE) activité
        pileProcessus = new Stack<>();
    }

    /**
     * Abandon du processus actif (avec notification).
     */
    public void traiterAbandonActivite() {
        getProcessus().traiterAbandon();
        depilerActiviteCourante();
    }

    /**
     * Abandon des processus (avec notification).
     */
    protected void traiterAbandonProcedure() {
        while (existeUnProcessus()) {
            getProcessus().traiterAbandon();
            depilerActiviteCourante();
        }
    }

    /**
     * Arrêt brutal des processus (sans qu'ils soient notifié).
     */
    protected void arreterProcedure() {
        while (existeUnProcessus()) {
            depilerActiviteCourante();
        }
    }

    /**
     * Création d'un nouveau processus qui devient le processus actif.
     *
     * @param nomProcessus
     *            the nom processus
     * @param nomExtension
     *            the nom extension
     * @param module
     *            the module
     * @param typeAppelant
     *            the type appelant
     */
    public final void creerProcessus(String nomProcessus, final String nomExtension, final ModuleExitProcessus module, final int typeAppelant) {
        try {
            // Récupération de la classe associée au processus
            Class<? extends ProcessusBean> classeProcessus = ProcessusHelper.getClasseProcessus(nomProcessus, nomExtension);
            // Le processus n'a pas été trouvé on retourne un process générique qui renvoit vers une page d'erreur
            if (classeProcessus == null) {
                nomProcessus = ErrorProcessus.NOM_PROCESSUS;
                classeProcessus = ProcessusHelper.getClasseProcessus(nomProcessus, nomExtension);
            }
            final Class<?>[] classesParam = new Class<?>[1];
            classesParam[0] = InfoBean.class;
            // Récupération du constructeur de l'interface processus
            final Constructor<? extends ProcessusBean> constructeur = classeProcessus.getConstructor(classesParam);
            // Instanciation du Composant d'information
            // le CI du processus pointe sur le CI de la PU
            final InfoBean infoBeanFils = new InfoBean(getProcedureBean().getInfoProcedureBean());
            infoBeanFils.setNomProcessus(nomProcessus);
            infoBeanFils.setSessionHttp(getProcedureBean().getSession().getSessionUtilisateur().getHttpSession());
            // Instanciation du processus
            final Object[] params = new Object[1];
            params[0] = infoBeanFils;
            final ProcessusBean beanCA = constructeur.newInstance(params);
            // Association du gestionnaire de processus
            beanCA.setGp(this);
            // Empilement du processus
            empilerActivite(beanCA, module, typeAppelant);
        } catch (final ReflectiveOperationException | IllegalArgumentException e) {
            LOG.error("Erreur à la création du processus", e);
        }
    }

    /**
     * Fin de l'activité courante => on l'oublie.
     */
    private void depilerActiviteCourante() {
        pileProcessus.pop();
    }

    /**
     * Empile une activité: celle-ci devient donc à cet instant l'activité courante.
     *
     * @param activite
     *            com.jsbsoft.jtf.core.AbstractProcessusBean
     * @param module
     *            the module
     * @param typeAppelant
     *            the type appelant
     */
    private void empilerActivite(final AbstractProcessusBean activite, final ModuleExitProcessus module, final int typeAppelant) {
        final InfoBean infoBean = activite.getInfoBean();
        final InfosProcessus processus = new InfosProcessus(activite, infoBean, module, typeAppelant);
        pileProcessus.push(processus);
    }

    /**
     * Accesseur.
     *
     * @return the procedure ben
     */
    public final ProcedureBean getProcedureBean() {
        return procedure;
    }

    /**
     * Accesseur.
     *
     * @return the info bean
     */
    public final InfoBean getInfoBean() {
        InfoBean infoBean = null;
        if (existeUnProcessus()) {
            infoBean = pileProcessus.peek().getInfoBean();
        }
        return infoBean;
    }

    /**
     * Accesseur.
     *
     * @return the module exit processus
     */
    public final ModuleExitProcessus getModuleExitProcessus() {
        final ModuleExitProcessus processus = null;
        if (existeUnProcessus()) {
            return pileProcessus.peek().getModuleExitProcessus();
        }
        return processus;
    }

    /**
     * Accesseur.
     *
     * @return the session
     */
    public final HttpSession getSession() {
        return getProcedureBean().getSession().getSessionUtilisateur().getHttpSession();
    }

    /**
     * Traite les dernières données collectées et renvoie vrai si le processus est terminée.
     *
     * @return boolean
     * @throws ErreurAsyncException
     */
    public boolean traiterAction() throws ErreurAsyncException {
        //on renseigne le nom du dernier processus appellé dans la session
        getSessionUtilisateur().setNomDernierProcessusAppele(getInfoBean().getNomProcessus());
        boolean finProcessus = false;
        boolean bouclerSurAppelProcessus = false;
        do {
            try {
                bouclerSurAppelProcessus = false;
                /*
                Si le processus avant est différent du processus après,
                cela signifie qu'il y a eu appel de processus
                à processus, auquel cas il faut boucler
                 */
                AbstractProcessusBean ancienCA = getProcessus();
                // JSS 20050829 - pour éviter des problèmes de concurrence
                // sur un même processus, qui se traduisent notamment par
                // des 'null pointer' sur les connections jdbc, on met en place
                // une synchronisation sur chaque instance
                synchronized (ancienCA) {
                    try {
                        ancienCA.initConnection();
                        // JSS 20040222 - Gestion des identifiants de requete
                        // Attention, on ne passe pas systématiquement ici
                        // (notamment sur les controles de premier niveau qui
                        // sont effectués avant)
                        // la requete associée à un processus n'est donc
                        // pas systématiquement stockée
                        ancienCA.setIdRequete(RequeteMgr.creerRequete());
                        RequeteMgr.ajouterEvenement(ancienCA.getIdRequete(), "processus (debut) > " + ancienCA.getInfoBean());
                        finProcessus = getProcessus().traiterAction();
                        RequeteMgr.ajouterEvenement(ancienCA.getIdRequete(), "processus (fin) > " + ancienCA.getInfoBean());
                        RequeteMgr.terminerRequete(ancienCA.getIdRequete());
                    } finally {
                        ancienCA.releaseConnection();
                    }
                }
                AbstractProcessusBean nouveauCA = getProcessus();
                if (ancienCA != nouveauCA) {
                    bouclerSurAppelProcessus = true;
                }
                /* Recopie temporaire du composant information */
                /* pour éviter sa destruction avant l'appel de l'exit */
                final InfoBean cloneCI = getInfoBean();
                /* Mémorisation du moule d'EXIT associé au processus */
                final ModuleExitProcessus moduleExit = getModuleExitProcessus();
                if (finProcessus) {
                    /* Si l'appelant est un processus, il faut boucler sur
                    ce processus sans notifier la PU */
                    final int typeAppelant = pileProcessus.peek().getTypeAppelant();
                    if (typeAppelant == TYPE_APPELANT_PROCESSUS) {
                        bouclerSurAppelProcessus = true;
                    }
                    depilerActiviteCourante();
                    /* appel du module d'Exit associé au processus */
                    moduleExit.apresProcessus(this, cloneCI);
                    cloneCI.setMessagesDansInfoBeanPere();
                }
            } catch (final ErreurAsyncException e) {
                throw e;
            } catch (final Exception e) {
                final ContexteUniv ctx = ContexteUtil.getContexteUniv();
                String errorMessage = "erreur lors du traitement de l'action du processus";
                if (ctx != null && ctx.getRequeteHTTP() != null) {
                    final StringBuffer requestUrl = ctx.getRequeteHTTP().getRequestURL();
                    final String queryString = ctx.getRequeteHTTP().getQueryString();
                    if (StringUtils.isNotBlank(queryString)) {
                        requestUrl.append("?").append(queryString);
                    }
                    errorMessage += " sur la requête " + requestUrl.toString();
                }
                LOG.error(errorMessage, e);
                bouclerSurAppelProcessus = false;
                getInfoBean().addMessageErreur(e.toString());
            }
        } while (bouclerSurAppelProcessus);
        return finProcessus;
    }

    /**
     * Renvoie true si le processus courant est appelé par un autre processus.
     *
     * @return true, if est un processus fils
     */
    public final boolean estUnProcessusFils() {
        boolean fils = false;
        final int size = pileProcessus.size();
        if (size > 1) {
            fils = true;
        }
        return fils;
    }

    /**
     * Renvoie vrai s'il reste au moins un processus.
     *
     * @return boolean
     */
    protected final boolean existeUnProcessus() {
        return !pileProcessus.isEmpty();
    }

    /**
     * Accesseur.
     *
     * @return the processus
     */
    public final AbstractProcessusBean getProcessus() {
        AbstractProcessusBean processus = null;
        if (existeUnProcessus()) {
            processus = pileProcessus.peek().getComposantActivite();
        }
        return processus;
    }

    /**
     * Accesseur.
     *
     * @return the session utilisateur
     */
    public final SessionUtilisateur getSessionUtilisateur() {
        return getProcedureBean().getSession().getSessionUtilisateur();
    }
}
