package com.jsbsoft.jtf.core;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurAsyncException;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.jsbsoft.jtf.session.SousSession;
import com.jsbsoft.jtf.upload.ExtendedRequest;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.ExtensionHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;

/**
 * Traitement générique des servlets Une servlet reçoit des requêtes. Elle retrouve ou crée le bean approprié. Ensuite, elle extrait les paramètres, les place dans le composant
 * information du bean et demande au bean la prochaine IHM.<BR>
 * <B>Correspondance avec la maquette ADSU: </B><BR>
 * ServletPU implémente une partie de la classe COLLECTE:<BR>
 * <UL>
 * <LI>Ecriture dans le composant INFORMATION.
 * <LI>Récupération de la PU (procedure) courant à partir de la session
 * <LI>Demande à la PU de l'IHM suivante
 * </UL>
 * <BR>
 * <BR>
 */
public class TraitementRequeteHTTP {

    private static final Logger LOG = LoggerFactory.getLogger(TraitementRequeteHTTP.class);

    /** nom du paramètre de la requête http qui contient l'identifiant du bean. */
    private static final String CSTE_NOM_PARAMETRE_PROCEDURE = "BEANPU";

    /** nom du paramètre de la requête http qui contient le nom de la PU. */
    private static final String CSTE_NOM_PARAMETRE_NAMEPU = "PROC";

    /** nom du paramètre de la requête http qui contient le nom de la PU. */
    private static final String CSTE_NOM_PARAMETRE_EXTENSIONPU = "EXT";

    /** The servlet. */
    private HttpServlet servlet = null;

    /**
     * Renvoie une JSP, un objet sérialisé, un flux XML, etc.
     *
     * @param desc
     *            the desc
     * @param req
     *            the req
     * @param res
     *            the res
     * @param bean
     *            the bean
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public void renvoyerFlux(final DescriptifPageRetour desc, final HttpServletRequest req, final HttpServletResponse res, final ProcedureBean bean) throws ServletException, IOException {
        //agir en fonction du type
        switch (desc.getTypeDeFlux()) {
            case URL:
                renvoyerURL(req, res, desc.getUrlString(), bean);
                break;
            case JSP:
                renvoyerJSP(req, res, desc.getNom(), bean);
                break;
            case HTML:
                renvoyerHTML(req, res, desc.getNom());
                break;
            case XML:
                renvoyerXML(req, res, desc.getNom(), bean);
                break;
            case OBJET_SERIALISE:
                renvoyerObjetSerialisable(res, desc.getObjetSerialisable());
        }
    }

    /**
     * Renvoie la page dont le nom est préinfoBeansé dans le descriptif.
     *
     * @param res
     *            javax.servlet.http.HttpServletResponse
     * @param nom
     *            java.lang.String
     * @param req
     *            the req
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     *
     * @exception java.io.IOException
     */
    private void renvoyerHTML(final HttpServletRequest req, final HttpServletResponse res, final String nom) throws ServletException, IOException {
        LOG.debug("Envoi de la page HTML " + nom);
        final ServletContext context = servlet.getServletConfig().getServletContext();
        final RequestDispatcher rd = context.getRequestDispatcher("/" + nom + ".jsp");
        rd.forward(req, res);
    }

    /**
     * Renvoie la JSP dont le nom est précisé dans le descriptif.
     *
     * @param req
     *            javax.servlet.http.HttpServletRequest
     * @param res
     *            javax.servlet.http.HttpServletResponse
     * @param nomJSP
     *            java.lang.String
     * @param b
     *            com.jsbsoft.jtf.session.ProcedureBean
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     *
     * @exception javax.servlet.ServletException
     *                The exception description.
     * @exception java.io.IOException
     *                The exception description.
     */
    private void renvoyerJSP(final HttpServletRequest req, final HttpServletResponse res, String nomJSP, ProcedureBean b) throws ServletException, IOException {
        // généralisation des redirections après login dans le front office
        // On associe à l'écran logique LOGIN ce traitement de redirection
        // processus.GESTION_MEMBRES.ecran_physique.LOGIN=[login_front_redirect]
        if ("[login_front_redirect]".equals(nomJSP)) {
            try {
                UnivWebFmt.redirigerVersLogin(b.getInfoBean(), req, res);
            } catch (final IOException e) {
                LOG.error("erreur lors de la redirection vers le login", e);
            }
            return;
        }
        //si le bean n'est pas null
        if (b != null) {
            if (nomJSP != null) {
                // on récupère la Sous-PU
                b = b.getSousPUActive();
                final String ecranLogique = StringUtils.defaultString(b.getInfoBean().getEcranLogique());
                // si le processus est restaurable, on sauvegarde/restaure l'infoBean sauf les listes de fiches
                LOG.debug("*** SAUVEGARDE INFOBEAN POUR RESTAURATION : " + ecranLogique);
                TreeMap<String, InfoBean> listeCI;
                final Object liste = b.getProcessusManager().getSessionUtilisateur().getInfos().get(SessionUtilisateur.LISTE_COMPOSANTS_INFORMATIONS);
                if (liste != null) {
                    listeCI = (TreeMap<String, InfoBean>) liste;
                } else {
                    listeCI = new TreeMap<>();
                    b.getProcessusManager().getSessionUtilisateur().getInfos().put(SessionUtilisateur.LISTE_COMPOSANTS_INFORMATIONS, listeCI);
                }
                // si aucune erreur, sauvegarde du composant d'information
                if (b.getInfoBean().getMessageErreur().length() == 0) {
                    String idCI = "" + System.currentTimeMillis();
                    // ajout d'un patch via cette variable pour ne pas charger la liste de sauvegarde des infobeans
                    // ex ticket CORE-719 sur l'affichage des médias cf media_liste.jsp
                    // on reprend l'id initial pour ne conserver qu'un seul infoBean
                    if (BooleanUtils.toBoolean(b.getInfoBean().getString("SINGLE_CI_RESTAURATION"))) {
                        idCI = b.getInfoBean().getString("ID_CI_RESTAURATION");
                    }
                    listeCI.put(idCI, b.getInfoBean());
                    b.getInfoBean().set("ID_CI_RESTAURATION", idCI);
                    b.getInfoBean().set("NOM_JSP_RESTAURATION", nomJSP);
                    // conservation des 50 derniers items, suppression des 10 premiers
                    if (listeCI.size() > 50) {
                        final Iterator<InfoBean> iter = listeCI.values().iterator();
                        int i = 0;
                        while (iter.hasNext() && i < 10) {
                            i++;
                            iter.next();
                            iter.remove();
                        }
                    }
                }
                // si une erreur est survenue, restauration des données du dernier composant d'information
                if (b.getInfoBean().getMessageErreur().length() > 0) {
                    final Object oCI = b.getInfoBean().get("ID_CI_RESTAURATION");
                    if (oCI != null) {
                        final Object o = listeCI.get(oCI);
                        if (o != null) {
                            final InfoBean oldCI = (InfoBean) o;
                            // JSS 20050301 : pas besoin de restaurer les données si
                            // le composant d'information est mémorisé entre chaque appel
                            if (!b.getMemorisationCI()) {
                                for (final Data data : oldCI.getDatas().values()) {
                                    /* Ajout de chaque donnée sauvegardée */
                                    final String nomDonnee = data.getName();
                                    if (b.getInfoBean().get(nomDonnee) == null) {
                                        b.getInfoBean().setData(nomDonnee, data);
                                    }
                                }
                            }
                            nomJSP = oldCI.getString("NOM_JSP_RESTAURATION");
                        } else {
                            b.getInfoBean().setTitreEcran(MessageHelper.getCoreMessage("BO_ERREUR"));
                        }
                    } else {
                        b.getInfoBean().setTitreEcran(MessageHelper.getCoreMessage("BO_ERREUR"));
                    }
                }
                // on place un identifiant unique pour l'écran physique pour
                // le revérifier au niveau de la collecte
                final ServiceInfosSite serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
                final InfosSite infosSite = serviceInfosSite.getSiteByHost(req.getServerName());
                Boolean isFront = ProcessusHelper.isProcessusFrontOffice(b.getInfoBean().getNomProcessus(), b.getExtension());
                String path = ExtensionHelper.getTemplateExtension(b.getExtension(), nomJSP, isFront);
                if (StringUtils.isBlank(path)) {
                    res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    String forward = infosSite.getJspFo() + "/error/404.jsp?URL_DEMANDEE=" + URLEncoder.encode(req.getRequestURI(), CharEncoding.DEFAULT);
                    if (StringUtils.isNotEmpty(req.getHeader("referer"))) {
                        forward += "&REFERER=" + URLEncoder.encode(req.getHeader("referer"), CharEncoding.DEFAULT);
                    }
                    b.getInfoBean().setEcranConteneur(forward);
                } else {
                    b.getInfoBean().setEcranPhysique(path);
                }
                b.getInfoBean().setIDJSP(String.valueOf(System.currentTimeMillis()));
                LOG.debug("Stockage du Composant d'information");
                //placer le COMPOSANT D'INFORMATION dans la requête (servlet 2.1)
                req.setAttribute("infoBean", b.getInfoBean());
                LOG.debug("Le CUI avant l'envoi de la JSP " + nomJSP + " :\r\n" + b.getInfoBean().toString());
                final String ecranconteneur = b.getInfoBean().getEcranConteneur();
                nomJSP = calculerJSPAAfficher(nomJSP, infosSite, ecranconteneur);
                final ServletContext context = servlet.getServletConfig().getServletContext();
                final RequestDispatcher rd = context.getRequestDispatcher(nomJSP);
                if (b.getInfoBean().getMessageErreur().length() > 0) {
                    res.setHeader("ERREUR", b.getInfoBean().getMessageErreur());
                }
                rd.forward(req, res);
                //Réinitialisation du message d'erreur
                b.getInfoBean().removeMessage(TypeMessage.ERREUR.getCode());
            } else {
                // On se retouve dans ce cas quand la PU a été détruite
                res.setContentType("text/html");
                final PrintWriter out = res.getWriter();
                out.println("<HTML><HEAD><TITLE>Attention</TITLE></HEAD>");
                out.println("<BODY>");
                out.println("This page has expired");
                out.println("</BODY>");
                out.println("</HTML>");
            }
        } else {
            LOG.debug("Pas de ProcedureBean");
        }
    }

    private String calculerJSPAAfficher(String nomJSP, InfosSite infosSite, String ecranconteneur) {
        if (StringUtils.isNotBlank(ecranconteneur) && !"[ecran_logique]".equals(ecranconteneur)) {
            nomJSP = ecranconteneur;
        }
        // Remplacement du dossier template courant
        if (infosSite != null) {
            final String dossierJspFo = infosSite.getJspFo();
            if (nomJSP.startsWith("jsp") && !"/jsp".equals(dossierJspFo)) {
                nomJSP = StringUtils.replace(nomJSP, "jsp/", dossierJspFo.substring(1) + "/");
            }
        }
        if (!nomJSP.endsWith(ExtensionHelper.EXTENSION_TEMPLATE)) {
            nomJSP += ExtensionHelper.EXTENSION_TEMPLATE;
        }
        if (!nomJSP.startsWith("/")) {
            nomJSP = "/" + nomJSP;
        }
        return nomJSP;
    }

    /**
     * @param res
     *            javax.servlet.http.HttpServletResponse
     * @param obj
     *            java.io.Serializable
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     *
     * @exception java.io.IOException
     *                The exception description.
     */
    private void renvoyerObjetSerialisable(final HttpServletResponse res, final Serializable obj) throws IOException {
        //ouvrir le flux de sortie de la requête
        final ObjectOutputStream oos = new ObjectOutputStream(res.getOutputStream());
        //écrire l'objet sérialisé
        oos.writeObject(obj);
        //fermer le flux
        oos.close();
    }

    /**
     * Rediriger sur une URL précisée dans le descriptif.
     *
     * @param res
     *            javax.servlet.http.HttpServletResponse
     * @param urlString
     *            String
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     *
     * @exception java.io.IOException
     * @throws ServletException
     */
    private void renvoyerURL(final HttpServletRequest req, final HttpServletResponse res, final String urlString, final ProcedureBean b) throws IOException, ServletException {
        if (StringUtils.isNotEmpty(b.getInfoBean().getMessageErreur())) {
            final HttpSession session = req.getSession(false);
            session.setAttribute("infoBean", b.getInfoBean());
        }
        res.sendRedirect(urlString);
    }

    /**
     * Renvoie un doc XML dont le nom est précisé dans le descriptif.
     *
     * @param req
     *            javax.servlet.http.HttpServletRequest
     * @param res
     *            javax.servlet.http.HttpServletResponse
     * @param nomXml
     *            java.lang.String
     * @param b
     *            com.jsbsoft.jtf.session.ProcedureBean
     *
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     *
     * @exception javax.servlet.ServletException
     *                The exception description.
     * @exception java.io.IOException
     *                The exception description.
     */
    private void renvoyerXML(final HttpServletRequest req, final HttpServletResponse res, final String nomXml, ProcedureBean b) throws ServletException, IOException {
        //si le bean n'est pas null
        if (b != null) {
            // on récupère la Sous-PU
            b = b.getSousPUActive();
            // placer le COMPOSANT D'INFORMATION dans la requête
            req.setAttribute("infoBean", b.getInfoBean());
        }
        //envoyer le document
        final ServletContext context = servlet.getServletConfig().getServletContext();
        final RequestDispatcher rd = context.getRequestDispatcher("/" + nomXml + ".xml");
        rd.forward(req, res);
    }

    /**
     * Renvoie le bean PU en charge du traitement de la requête. Renvoie null si le bean n'existe plus (utilisation intempestive de la touche PRECEDENT du navigateur). Fait appel à
     * creerProcedureBean() si nécessaire. Utilise l'identifiant unique du bean qui doit se trouver dans les paramètres.
     *
     * @param sessionHttp
     *            the session http
     * @param req
     *            the req
     *
     * @return the procedure bean
     *
     * @throws Exception
     *             the exception
     */
    private ProcedureBean retrouverProcedureBean(final HttpSession sessionHttp, final HttpServletRequest req) throws Exception {
        LOG.debug("Recherche du bean PU...");
        ProcedureBean bean = null;
        try {
            //récupérer l'objet session dans la session HTTP
            SessionUtilisateur session = (SessionUtilisateur) sessionHttp.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
            String idBean = req.getParameter(CSTE_NOM_PARAMETRE_PROCEDURE);
            if (idBean != null) {
                bean = session.retrouverProcedureBeanparID(idBean);
            }
            if (bean != null) {
                LOG.debug("Le bean " + CSTE_NOM_PARAMETRE_PROCEDURE + " existe dejà");
            } else {
                LOG.debug("Aucun bean " + CSTE_NOM_PARAMETRE_PROCEDURE + "dans la session utilisateur, on le recrée");
                SousSession ss = session.getSessionParDefaut();
                LOG.debug("Création du bean");
                String nomPU = req.getParameter(CSTE_NOM_PARAMETRE_NAMEPU);
                String extensionPU = req.getParameter(CSTE_NOM_PARAMETRE_EXTENSIONPU);
                if (ss != null && nomPU != null) {
                    bean = new ProcedureBean(ss, nomPU, extensionPU);
                    bean.initialiser();
                }
            }
        } catch (final Exception e) {
            LOG.error("Impossible d'initialiser le bean " + CSTE_NOM_PARAMETRE_PROCEDURE, e);
            throw new Exception("Impossible de créer une procédure");
        }
        return bean;
    }

    /**
     * Méthode standard, sous-classée si besoin.
     *
     * @param _servlet
     *            the _servlet
     * @param req
     *            the req
     * @param res
     *            the res
     */
    public void traiterRequeteHTTP(final SG _servlet, final HttpServletRequest req, final HttpServletResponse res) {
        final MyRequestWrapper myrequest = new MyRequestWrapper(req);
        myrequest.setResponse(res);
        servlet = _servlet;
        LOG.debug("--------Requête reçue = " + myrequest.getServletPath() + "?" + myrequest.getQueryString() + "\r\n");
        //Parsing des fichiers uploadés
        ExtendedRequest ereq = null;
        try {
            ereq = new ExtendedRequest(myrequest, res, new File(WebAppUtil.getUploadDefaultPath()));
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
        }
        final HttpSession session = recupererSession(myrequest);
        //récupérer ou créer le bean PU
        ProcedureBean b = null;
        try {
            // La variable ereq permet de traiter les 'POST'
            b = retrouverProcedureBean(session, ereq);
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
        }
        //Appel de la PU
        try {
            //si le bean existe ou vient d'être créé
            if (b != null) {
                // ajout de la servlet config dans la session kportal
                if (b.getProcessus() instanceof ProcessusBean) {
                    ((ProcessusBean) b.getProcessus()).getDatas().put(OMContext.CLE_SERVLET_CONFIG, _servlet.getServletConfig());
                }
                // ajout de la requete pour pouvoir créer un ContexteUniv
                Hashtable<String, Object> datasOMContext = null;
                if (b.getProcessus() instanceof ProcessusBean) {
                    datasOMContext = ((ProcessusBean) b.getProcessus()).getDatas();
                    datasOMContext.put(OMContext.CLE_SERVLET_SG_REQUETE_HTTP, myrequest);
                }
                //demander au bean PU une description du flux à renvoyer
                // La variable ereq permet de traiter les 'POST'
                final DescriptifPageRetour desc = b.descriptifFluxRetour(ereq);
                //s'il existe un flux de retour
                if (desc != null) {
                    this.renvoyerFlux(desc, myrequest, res, b);
                    final String ecranLogique = b.getInfoBean().getEcranLogique();
                    if (ecranLogique != null) {
                        if (ecranLogique.equals(InfoBean.ECRAN_LOGIQUE_FIN)) {
                            b.getSession().retirerProcedureBean(b.idBean());
                        }
                    }
                    // Quand la PU est evenementiel, il faut l'afficher, puis la déréférencer
                    if (b.getModeEnchainement() == ProcedureBean.MODE_EVENEMENTIEL) {
                        b.getSession().retirerProcedureBean(b.idBean());
                    }
                } else {
                    this.renvoyerFlux(DescriptifPageRetour.descriptifPageDefaut(b.getNom(), b.getExtension()), myrequest, res, b);
                }
                // suppression de la référence sur la requete
                if (datasOMContext != null) {
                    datasOMContext.remove(OMContext.CLE_SERVLET_SG_REQUETE_HTTP);
                }
            } else {
                // le bean n'existe plus car la PU est terminée
                //renvoyer la page d'expiration
                // On se retrouve dans ce cas quand la PU a été détruite
                res.setContentType("text/html");
                final PrintWriter out = res.getWriter();
                out.println("<HTML><HEAD><TITLE>Expired page</TITLE></HEAD>");
                out.println("<BODY>");
                out.println("<B>404</B> This page has expired .");
                out.println("<script type=\"text/javascript\">");
                out.println("if(top.opener) {top.opener.parent.location.reload();window.close();}");
                final int i = req.getRequestURL().toString().indexOf(req.getServletPath());
                if (i != -1) {
                    out.println("else{window.location.href='" + req.getRequestURL().toString().substring(0, i) + "';}");
                }
                out.println("</script>");
                out.println("</BODY>");
                out.println("</HTML>");
            }
        } catch (final ErreurAsyncException e) {
            throw e;
        } catch (final IOException | ServletException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * récupère la session utilisateur à partir de la request. Si elle n'existe pas, on la créé
     *
     * @param request
     * @return la session utilisateur
     */
    public HttpSession recupererSession(final MyRequestWrapper request) {
        //récupérer la session
        HttpSession session = request.getSession(true);
        // sauvegarde du serveur
        session.setAttribute("basePath", URLResolver.getRequestScheme(request) + "://" + request.getServerName() + (("80".equals("" + request.getServerPort()) || "443".equals("" + request.getServerPort())) ? "" : ":" + request.getServerPort()));
        //si la session n'existe pas
        final SessionUtilisateur testUtilisateur = (SessionUtilisateur) session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        if (testUtilisateur == null) {
            LOG.debug("! La session utilisateur n'existe pas");
            session = request.getSession(true);
            //création de la session utilisateur
            final SessionUtilisateur su = new SessionUtilisateur(session);
            //placer la session utilisateur dans la session http
            session.setAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP, su);
            LOG.debug("La session utilisateur a été créée");
        }
        return session;
    }
}
