package com.jsbsoft.jtf.core;

/**
 * Permet de stocker les informations à l'intérieur du gestionnaire de processus les informations liées à chaque processus.
 */
public class InfosProcessus {

    /** composant information. */
    private InfoBean infoBean = new InfoBean();

    /** The composant activite. */
    private AbstractProcessusBean composantActivite;

    /** The module exit processus. */
    private ModuleExitProcessus moduleExitProcessus = null;

    /* Type du processus appelant */

    /** The type appelant. */
    private int typeAppelant;

    /**
     * Constructeur à ne pas appeler dans le cas général.
     */
    public InfosProcessus() {
        super();
    }

    /**
     * Constructeur à ne pas appeler dans le cas général.
     *
     * @param _activite
     *            the _activite
     * @param _infoBean
     *            the _info bean
     * @param module
     *            the module
     * @param _typeAppelant
     *            the _type appelant
     */
    public InfosProcessus(AbstractProcessusBean _activite, InfoBean _infoBean, ModuleExitProcessus module, int _typeAppelant) {
        super();
        setComposantActivite(_activite);
        setInfoBean(_infoBean);
        setModuleExitProcessus(module);
        setTypeAppelant(_typeAppelant);
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (17/04/00 14:10:39)
     *
     * @return com.jsbsoft.jtf.core.AbstractProcessusBean
     */
    public AbstractProcessusBean getComposantActivite() {
        return composantActivite;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (17/04/00 14:09:06)
     *
     * @return com.jsbsoft.jtf.core.InfoBean
     */
    public InfoBean getInfoBean() {
        return infoBean;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (22/05/00 15:41:44)
     *
     * @return com.jsbsoft.jtf.core.ModuleExitProcessus
     */
    public ModuleExitProcessus getModuleExitProcessus() {
        return moduleExitProcessus;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (25/05/00 11:22:14)
     *
     * @return int
     */
    public int getTypeAppelant() {
        return typeAppelant;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (17/04/00 14:10:39)
     *
     * @param newComposantActivite
     *            com.jsbsoft.jtf.core.AbstractProcessusBean
     */
    public void setComposantActivite(AbstractProcessusBean newComposantActivite) {
        composantActivite = newComposantActivite;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (17/04/00 14:09:06)
     *
     * @param newInfoBean
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public void setInfoBean(InfoBean newInfoBean) {
        infoBean = newInfoBean;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (22/05/00 15:41:44)
     *
     * @param newModuleExitProcessus
     *            com.jsbsoft.jtf.core.ModuleExitProcessus
     */
    public void setModuleExitProcessus(ModuleExitProcessus newModuleExitProcessus) {
        moduleExitProcessus = newModuleExitProcessus;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (25/05/00 11:22:14)
     *
     * @param newTypeAppelant
     *            int
     */
    public void setTypeAppelant(int newTypeAppelant) {
        typeAppelant = newTypeAppelant;
    }
}
