package com.jsbsoft.jtf.core;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet générique du framework.
 */
public class SG extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -8108206932575170795L;

    /**
     * Méthode standard, sous-classée si besoin.
     *
     * @param req
     *            la requete http en GET
     * @param res
     *            la réponse à renvoyer
     *
     */
    @Override
    public void doGet(final HttpServletRequest req, final HttpServletResponse res) {
        new TraitementRequeteHTTP().traiterRequeteHTTP(this, req, res);
    }

    /**
     * Méthode standard, sous-classée si besoin (forcement on fait comme ne get...)
     *
     * @param req
     *            la requete http en post
     * @param res
     *            la réponse à renvoyer
     */
    @Override
    public void doPost(final HttpServletRequest req, final HttpServletResponse res) {
        doGet(req, res);
    }
}
