package com.jsbsoft.jtf.core;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.utils.EscapeString;

/**
 * Insérez la description du type à cet endroit. Date de création : (10/06/2002 18:01:01)
 *
 * @author :
 */
public class HTMLUtil {

    private static final Logger LOG = LoggerFactory.getLogger(HTMLUtil.class);

    /**
     * Commentaire relatif au constructeur HTMLUtil.
     */
    public HTMLUtil() {
        super();
    }

    /**
     * Conversion des caractères HTML encodés - &#xxx; (encodage décimal) - <BR>
     * en cr
     *
     *
     * Cette conversion est utilisée à 2 endroits : - export XML - Saisie en mode HTML
     *
     * Pour l'instant, les caractères encodés supérizurs à 255 sont laissés tel quel
     *
     * En mode HTML ils sont affichés sous la forme &#8888; En export XML, ils sont convertis.
     *
     * @param _chaine
     *            the _chaine
     * @param _suppressionTagsHTML
     *            the _suppression tags html
     *
     * @return the string
     */
    @Deprecated
    public static String convertirEnASCII(String _chaine, boolean _suppressionTagsHTML) {
        StringBuilder sb = new StringBuilder();
        String curTag = "";
        String chaine = EscapeString.unescapeHtml(_chaine);
        int n = chaine.length();
        for (int i = 0; i < n; i++) {
            char c = chaine.charAt(i);
            //Détection tags HTML
            if (chaine.charAt(i) == '<') {
                curTag = extraireTagHTML(chaine, i);
                if (_suppressionTagsHTML) {
                    // Certains Tags doivent être remplacés
                    if ("LI".equalsIgnoreCase(curTag)) {
                        sb.append("\n  -");
                    }
                    if ("/UL".equalsIgnoreCase(curTag)) {
                        sb.append("\n");
                    }
                    if ("/OL".equalsIgnoreCase(curTag)) {
                        sb.append("\n");
                    }
                    if ("BR".equalsIgnoreCase(curTag)) {
                        sb.append("\n");
                    }
                    // JSS 20040226 : ajout paragraphes
                    if ("P".equalsIgnoreCase(curTag)) {
                        sb.append("\n\n");
                    }
                }
            }
            //Détection caractères spéciaux
            if (chaine.charAt(i) == '&') {
                // JSSS 20020923-002
                if ((i < chaine.length() - 1) && (chaine.charAt(i + 1)) == '#') {
                    String valeur = "";
                    int j = 0;
                    //JSS 20020710 : correction strictement sup à 0
                    while ((i + 2 + j < chaine.length()) && (chaine.charAt(i + 2 + j) >= '0') && (chaine.charAt(i + 2 + j) <= '9')) {
                        valeur += chaine.charAt(i + 2 + j);
                        j++;
                    }
                    try {
                        if (Integer.parseInt(valeur) < 256) {
                            c = (char) Integer.parseInt(valeur);
                            // Repositionnement
                            i = i + 2 + j;
                        } else
                            // JSSS 20020923-002
                            // Le caractère 8217 est issu de coller à partir de Word
                            if (Integer.parseInt(valeur) == 8217) {
                                c = '\'';
                                // Repositionnement
                                i = i + 2 + j;
                            } else {
                                // On laisse les encodages sup à 255
                                // Sinon, ils provoquent des erreurs
                                // en mode HTML
                                // Pour l'export XML, ces caractères devront
                                // être traités à part
                                c = '&';
                            }
                    } catch (NumberFormatException e) {
                        LOG.debug("unable to parse the given value", e);
                    }
                }
            }
            // Insertion du caractère
            switch (c) {
                default:
                    if ("".equals(curTag)) {
                        sb.append(c);
                    } else {
                        if (!_suppressionTagsHTML) {
                            sb.append(c);
                        }
                    }
                    break;
            }
            // Fin du tag
            if (chaine.charAt(i) == '>') {
                curTag = "";
            }
        }
        return sb.toString();
    }

    /**
     * Extraction du tag HTML situé à la position courante.
     *
     * @param chaine
     *            the chaine
     * @param i
     *            the i
     *
     * @return the string
     */
    @Deprecated
    public static String extraireTagHTML(String chaine, int i) {
        String tagsHTML[] = {"A", "H1", "H2", "H3", "H4", "H5", "IMG", "LI", "UL", "BR", "B", "I", "STRONG", "U", "P", "EM", "OL", "DIV", "BLOCKQUOTE", "TABLE", "PRE", "TR", "TD", "TBODY", "FONT", "SUP", "TBODY", "SPAN"};
        String curTag = "";
        int n = chaine.length();
        // Détection des tags
        // Un tag est sous la forme ( "<"  ou "</" ) puis XXXX puis ( " " ou ">")
        // Soit "<XXXX> ou </XXXX> ou "<XXXX " ou "</XXXX ">
        if (chaine.charAt(i) == '<' && i + 1 < n) {
            for (String element : tagsHTML) {
                String nomTag = "";
                if (chaine.charAt(i + 1) != '/') {
                    if (i + 1 + element.length() + 1 <= n) {
                        nomTag = chaine.substring(i + 1, i + 1 + element.length() + 1);
                    }
                } else {
                    if (i + 2 + element.length() + 1 <= n) {
                        nomTag = chaine.substring(i + 2, i + 2 + element.length() + 1);
                    }
                }
                if ((nomTag.equalsIgnoreCase(element + " ")) || (nomTag.equalsIgnoreCase(element + ">"))) {
                    curTag = nomTag.substring(0, nomTag.length() - 1).toUpperCase();
                    if (chaine.charAt(i + 1) == '/') {
                        curTag = "/" + curTag;
                    }
                }
            }
        }
        return curTag;
    }

    /* Conversion des carcatères encodés */

    /**
     * Unescape html.
     *
     * @param s
     *            the s
     *
     * @return the string
     * @deprecated utiliser {@link EscapeString#unescapeHtml(String)}
     */
    @Deprecated
    public static String unescapeHTML(String s) {
        return EscapeString.unescapeHtml(s);
    }

    public static String stripHtml(String value) {
        value = StringUtils.replacePattern(value, "<(li|LI)>", "\n - ");
        value = StringUtils.replacePattern(value, "</(ul|UL)>", "\n");
        value = StringUtils.replacePattern(value, "</(ol|OL)>", "\n");
        value = StringUtils.replacePattern(value, "<(br|BR)(|\\s)(|/)>", "\n");
        value = StringUtils.replacePattern(value, "</(p|P|div|DIV)>", "\n");
        value = StringUtils.replacePattern(value, "<[^>]*>", "");
        return EscapeString.unescapeHtml(value);
    }

    /**
     * Permet d'offusquer le lien mailto passé en paramètre. Cette méthode repose sur la conversion des caractères dans un format URL.
     * @param email : la chaîne brute sous la forme d'une {@link String}
     * @return : la chaîne convertie sous la forme d'une {@link String}, la chaîne brute si la transformation n'a pas pu être effectuée.
     */
    public static String obfuscateEmailLink(String email) {
        String params = email.contains("?") ? email.substring(email.indexOf("?")) : StringUtils.EMPTY;
        String rawEmail = email.contains("?") ? StringUtils.substringBefore(email.replace("mailto:", StringUtils.EMPTY), "?") : email.replace("mailto:", StringUtils.EMPTY);
        params = params.replace("\r\n", "%0D%0A");
        String obfuscatedEmail;
        try {
            obfuscatedEmail = URLEncoder.encode(rawEmail, "UTF-8");
            obfuscatedEmail = obfuscatedEmail.replace(".", "%2E");
        } catch (UnsupportedEncodingException e) {
            LOG.debug("Unable to encode the given value", e);
            obfuscatedEmail = rawEmail;
        }
        return "mailto:" + obfuscatedEmail + params;
    }

    /**
     * Permet l'email fournit en paramètre. On remplace simplement le "@" et les "." par les entités HTML équivalentes.
     * @param email : la chaîne brute sous la forme d'une {@link String}
     * @return : la chaîne convertie sous la forme d'une {@link String}, la chaîne brute si la transformation n'a pas pu être effectuée.
     */
    public static String obfuscateEmailDisplay(String email) {
        String rawEmail = email.contains("?") ? StringUtils.substringBefore(email.replace("mailto:", StringUtils.EMPTY), "?") : email.replace("mailto:", StringUtils.EMPTY);
        String obfuscatedEmail = rawEmail.replace(".", "&#46;");
        obfuscatedEmail = obfuscatedEmail.replace("@", "&#64;");
        return obfuscatedEmail;
    }
}