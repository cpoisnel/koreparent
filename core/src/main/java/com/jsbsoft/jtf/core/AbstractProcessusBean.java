package com.jsbsoft.jtf.core;

/**
 * Superclasse de tous les processus <BR>
 * Méthodes à écrire dans les sous-classes:
 * <UL>
 * <LI>Le constructeur avec en paramètre le composant d'information
 * <LI>traiterAction <BR>
 * (classe interne jtf).
 */
public abstract class AbstractProcessusBean implements ModuleExitProcessus {

    /** Etat initial du processus. */
    protected static final int DEBUT = 0;

    /** Etat en cours du processus. */
    protected static final int EN_COURS = 1;

    /** Etat final du processus (il peut rester un écran pour info). */
    protected static final int FIN = 2;

    /**
     * Le <B>composant information</B> auquel le composant activité s'adresse pour récupérer des données.<BR>
     * <B>Initialisé et partagé avec le Bean PU</B>
     */
    public InfoBean infoBean;

    /** The id requete. */
    public String idRequete = "";

    /** Ecran logique du processus. */
    protected String ecranLogique;

    /** code de l'état du processus le composant Information. */
    protected int etat = DEBUT;

    /** The gp. */
    private ProcessusManager gp = null;

    /**
     * Définition abstraite d'un processus.
     *
     * @param _infoBean
     *            the _info bean
     */
    public AbstractProcessusBean(final InfoBean _infoBean) {
        infoBean = _infoBean;
        etat = DEBUT;
        ecranLogique = "";
    }

    /**
     * Abandon du processus.
     */
    public abstract void traiterAbandon();

    /**
     * Permet de traiter le retour d'un sous-processus.
     *
     * @param _gp
     *            the _gp
     * @param cloneCI
     *            the clone ci
     *
     * @return the descriptif page retour
     *
     */
    @Override
    public DescriptifPageRetour apresProcessus(final ProcessusManager _gp, final InfoBean cloneCI) {
        return null;
    }

    /**
     * Renvoie la composant d'information du processus.
     *
     * @return the info bean
     */
    public InfoBean getInfoBean() {
        return infoBean;
    }

    /**
     * Renvoie le gestionnaire de processus associé à la procédure.
     *
     * @return the gp
     */
    public ProcessusManager getGp() {
        return gp;
    }

    /**
     * Association d'une connection JDBC au processus.
     *
     */
    protected abstract void initConnection();

    /**
     * Libération de la connexion JDBC.
     *
     */
    protected abstract void releaseConnection();

    /**
     * Valorisation du gestionnaire de processus.
     *
     * @param newGp
     *            the new gp
     */
    protected void setGp(final ProcessusManager newGp) {
        gp = newGp;
    }

    /**
     * Traite les dernières données collectées et renvoie vrai si le processus est terminé.
     *
     * @return boolean
     *
     * @throws Exception
     *             the exception
     */
    protected abstract boolean traiterAction() throws Exception;

    /**
     * Gets the id requete.
     *
     * @return the id requete
     */
    public String getIdRequete() {
        return idRequete;
    }

    /**
     * Sets the id requete.
     *
     * @param string
     *            the string
     */
    public void setIdRequete(final String string) {
        idRequete = string;
    }
}
