package com.jsbsoft.jtf.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurFormatage;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.jsbsoft.jtf.upload.ExtendedRequest;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.URLResolver;

/**
 * Composant chargé de la collecte des données qui sont renvoyées par les pages JSP. <br>
 *
 * Il va successivement : <br>
 * - vérifier que la page physique correspond bien à la dernière page renvoyée par la PU ( back browser, plusieurs instances du navigateur sur la même PU ...) <br>
 * - Déterminer l'action utilisateur (mot-clé VALIDER, REVENIR, RECHERCHE ... positionnés dans la requête HTML) et la stocker dans le Composant d'Information (une méthode
 * spéinfoBeanfique à l'action a été déclarée) de la manière suivante :<br>
 * information.setActionUtilisateur(InfoBean.ACTION_REVENIR) <br>
 * - Vérifier le format de saisie, la présence des zones obligatoires (en cas d'erreur, la page est réaffichée directement avec le message approprié ) <br>
 */
public class ParserRequete {

    /** The Constant REFRESH_PARAM. */
    public static final String REFRESH_PARAM = "PARAM";

    private static final Logger LOGGER = LoggerFactory.getLogger(ParserRequete.class);

    /** The procedure. */
    private ProcedureBean procedure = null;

    /**
     * Constructeur à utiliser.
     *
     * @param procedure
     *            the _procedure
     */
    public ParserRequete(final ProcedureBean procedure) {
        this.procedure = procedure;
    }

    /**
     * Extrait les données de la requête et les écrit dans le COMPOSANT D'INFORMATION UNIQUE.<BR>
     * (après avoir vérifié leur format et présence)
     *
     * @param valeur
     *            the valeur
     * @param cle
     *            the cle
     * @param formatSupplementaire
     *            the format supplementaire
     * @param nbCarMax
     *            the nb car max
     *
     * @return the string
     *
     * @throws ErreurFormatage
     *                Problème de conversion
     */
    public static String controlerTexte(String valeur, final String cle, final int typeFormat, final String formatSupplementaire, final int nbCarMax) throws ErreurFormatage {
        /************************************************/
        /*   Editeur HTML : Nettoyage du code           */
        /*                                              */
        /*   On enlève les tags autres que BR, UL, LI,  */
        /*   B, U                                       */
        /************************************************/
        final String chaineCodeHTML = FormateurJSP.renvoyerValeurFormatage(formatSupplementaire, "CODE_HTML");
        final boolean toolbox = "1".equals(chaineCodeHTML);
        if (toolbox) {
            // FBI 20051213: modif pour supprimer les hosts des urls dans la toolbox
            // AAR 200905 : utilisation d'une liste d'hôtes chargée au preload des sites pour optimiser le temps de traitement
            final ServiceInfosSite serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            for (final String host : serviceInfosSite.getFullAliasHostList()) {
                valeur = enleverHttp(valeur, host);
            }
            valeur = StringUtils.replace(valeur, "/servlet/com.univ.utils.LectureImageToolbox?TAG=", StringUtils.EMPTY);
            valeur = formaterLiensRequetes(valeur);
        } else if (typeFormat == FormateurJSP.FORMAT_MULTI_LIGNE) {
            valeur = EscapeString.escapeScriptAndEvent(valeur);
        }
        final Locale localeCourante = ContexteUtil.getContexteUniv().getLocale();
        /************************************************/
        /*   Traitements génériques                     */
        /************************************************/
        if (nbCarMax > 0 && valeur.length() > nbCarMax) {
            final String msg = MessageHelper.getCoreMessage(localeCourante, "JTF_ERR_FMT_NB_CAR_MAX_1") + " '" + getLibelleZone(cle, formatSupplementaire) + "' " + MessageHelper.getCoreMessage(localeCourante, "JTF_ERR_FMT_NB_CAR_MAX_2") + " " + nbCarMax + MessageHelper.getCoreMessage(localeCourante, "JTF_ERR_FMT_NB_CAR_MAX_3") + " " + valeur.length() + ".";
            throw new ErreurFormatage(msg);
        }
        if (FormateurJSP.renvoyerValeurFormatage(formatSupplementaire, "EMAIL") != null) {
            boolean erreurMail = false;
            final int indice = valeur.indexOf("@");
            if ((indice < 1) || (indice > valeur.length() - 2)) {
                erreurMail = true;
            }
            if (erreurMail) {
                final String msg = MessageHelper.getCoreMessage(localeCourante, "JTF_ERR_FMT_EMAIL");
                throw new ErreurFormatage(msg);
            }
            // Erreur si un deuxième aroba ( 2 adresses saisies)
            if (valeur.indexOf("@", indice + 1) != -1) {
                final String msg = MessageHelper.getCoreMessage(localeCourante, "JTF_ERR_FMT_2_EMAILS");
                throw new ErreurFormatage(msg);
            }
        }
        if (FormateurJSP.renvoyerValeurFormatage(formatSupplementaire, "URL") != null) {
            final int indice = valeur.indexOf("http://");
            if (indice != 0) {
                final String msg = MessageHelper.getCoreMessage(localeCourante, "JTF_ERR_FMT_URL");
                throw new ErreurFormatage(msg);
            }
        }
        // On garde les espaces
        boolean suppressionEspacesFinLigne = true;
        if (FormateurJSP.renvoyerValeurFormatage(formatSupplementaire, "NO_TRIM") != null) {
            suppressionEspacesFinLigne = false;
        }
        if (FormateurJSP.renvoyerValeurFormatage(formatSupplementaire, "CODE_GESTION") != null) {
            final int indice = valeur.indexOf("/");
            if (indice != -1) {
                final String msg = MessageHelper.getCoreMessage(localeCourante, "JTF_ERR_FMT_CODE");
                throw new ErreurFormatage(msg);
            }
        }
        if (suppressionEspacesFinLigne) {
            valeur = valeur.trim();
        }
        return valeur;
    }

    /**
     * Renvoie le libelle d'unz zone à afficher pour message d'erreur.
     *
     * @param cle
     *            the cle
     * @param format
     *            the format
     *
     * @return the libelle zone
     */
    private static String getLibelleZone(final String cle, final String format) {
        String libelleZone = FormateurJSP.renvoyerValeurFormatageMinus(format, "LIB");
        if ((libelleZone == null) || (libelleZone.length() == 0)) {
            libelleZone = cle;
        }
        return libelleZone;
    }

    /**
     * On enlève ce qui a été mis par mshtml dans les liens <a> et <img> (il rajoute le chemin absolu et /servlet).
     *
     * @param tout
     *            the tout
     * @param basePath
     *            the base path
     *
     * @return the string
     *
     */
    private static String enleverHttp(String tout, final String basePath) {
        //on supprime d'abord les http://basePath/servlet/com.univ.utils.LectureImageToolbox?TAG= sur les images
        tout = StringUtils.replace(tout, basePath + "/servlet/com.univ.utils.LectureImageToolbox?TAG=", "");
        final StringBuilder res = new StringBuilder(tout);
        int idDeb = 0;
        int idFin = 0;
        // on supprime les liens absolus sur le host courant
        while ((idDeb = tout.indexOf("=\"" + basePath, idFin)) != -1) {
            idDeb += 2; // "=\""
            idFin = idDeb + basePath.length();
            int idFinUrl = tout.indexOf("\"", idFin);
            // si le lien n'est pas le host tout court (lien vers accueil host)
            // ou il ne s'agit pas d'un lien externe (en fait interne identifié par #KLINK a la fin de l'url)
            //LUD20070103 : on vérifie que l'url commence par / ou [, car sinon ça
            if ((tout.charAt(idFin) == '/' || tout.charAt(idFin) == '[') && !(tout.indexOf("#KLINK", idFin) != -1 && tout.indexOf("#KLINK", idFin) < idFinUrl)) {
                //on supprime les /servlet/ rajoutés par le navigateur
                if (tout.indexOf("/servlet/", idFin) == idFin && tout.indexOf(WebAppUtil.SG_PATH, idFin) != idFin) {
                    idFin += 9;
                }
                // RP 20061205
                if (tout.charAt(idFin) == '/' && tout.charAt(idFin + 1) == '[') {
                    idFin++;
                }
                tout = tout.substring(0, idDeb) + tout.substring(idFin);
                res.delete(idDeb, idFin); //on supprime
                idFin = tout.indexOf("\"", idDeb);
            }
        }
        return res.toString();
    }

    /**
     * Dans les liens de requetes, les & sont encodés en &amp; et il ne faut pas car cela génère un ; de trop pour la décomposition des élements de la requete ensuite.
     *
     * @param tout
     *            the tout
     *
     * @return the string
     *
     */
    private static String formaterLiensRequetes(final String tout) {
        String res = "";
        if (tout != null && tout.length() > 0) {
            int idxDeb = 0;
            int idxDebID = tout.indexOf("&amp;");
            while (idxDebID != -1) {
                //tmp contient le debut de la chaine jusqu'à l'index du tag [id]
                String tmp = tout.substring(0, idxDebID);
                final int idxDebLien = tmp.lastIndexOf("href=\"");
                final int idxFinLien = tout.indexOf("\"", idxDebID);
                //le &amp; se situe bien dans un attribut href
                if (idxDebID > idxDebLien && idxDebLien != -1 && idxDebID < idxFinLien) {
                    //uniquement sur les liens de requete, pas sur tous les liens href
                    String part = tout.substring(idxDebLien, idxDebID);
                    if (part.contains("[")) {
                        res += tout.substring(idxDeb, idxDebID + 1);
                    } else {
                        res += tout.substring(idxDeb, idxDebID + 5);
                    }
                } else {
                    res += tout.substring(idxDeb, idxDebID + 5);
                }
                idxDeb = idxDebID + 5;
                //on recherche un nouveau chemin tout pourri.
                idxDebID = tout.indexOf("&amp;", idxDeb);
            }
            res = res + tout.substring(idxDeb, tout.length());
        } else {
            res = tout;
        }
        return res;
    }

    /**
     * Extrait les données de la requête et les écrit dans le COMPOSANT D'INFORMATION UNIQUE.<BR>
     * (après avoir vérifié leur format et présence)
     *
     * @param valeur
     *            the valeur
     *
     * @return the date
     *
     * @throws ErreurFormatage
     *             the erreur formatage
     *
     */
    private Date controlerDate(final String valeur, final String cle, final String formatSupplementaire) throws ErreurFormatage {
        Date res = null;
        final String msg = getLibelleZone(cle, formatSupplementaire) + ", " + MessageHelper.getCoreMessage(procedure.getLocale(), "JTF_ERR_FMT_DATE");
        try {
            res = Formateur.parserDate(valeur);
            //Validation que la date saisie existe (surcouche car le sdf valide une date de type 21/01/202g)
            final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            if (StringUtils.isNotEmpty(valeur) && !valeur.equals(sdf.format(res))) {
                throw new ErreurFormatage(msg);
            }
        } catch (final ParseException e) {
            LOGGER.debug("incorrect date format", e);
            throw new ErreurFormatage(msg);
        }
        return res;
    }

    /**
     * Renvoie l'indice d'une zone dans la page.
     *
     * @param valeurFormat
     *            the valeur format
     *
     * @return the indice
     * @throws ErreurApplicative
     */
    private int getIndice(final String valeurFormat) throws ErreurApplicative {
        int indice = -1;
        if (valeurFormat != null) {
            int i = 0;
            final StringTokenizer st = new StringTokenizer(valeurFormat, ";");
            while (st.hasMoreTokens()) {
                String format = st.nextToken();
                if (i == 5) {
                    try {
                        indice = Integer.parseInt(format);
                    } catch (final NumberFormatException e) {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("ERREUR_FORMAT_INDICE_FORMULAIRE"));
                    }
                }
                i++;
            }
        }
        return indice;
    }

    /**
     * Extrait les données de la requête et les écrit dans le COMPOSANT D'INFORMATION UNIQUE.<BR>
     * (après avoir vérifié leur format et présence)
     *
     * @param req
     *            HttpServletRequest
     *
     *
     */
    public void getUploadedFile(final HttpServletRequest req) {
        //Parsing des fichiers uploadés
        final ExtendedRequest ereq = (ExtendedRequest) req;
        final UploadedFile f[] = ereq.getFileParameterValues("FICHIER");
        if (f != null) {
            //Stockage dans le CI du nom du fichier
            procedure.getInfoBean().setUploadedFiles(f);
            for (final UploadedFile element : f) {
                LOGGER.debug("fichier uploadé : " + element.getTemporaryFile().getPath());
            }
        }
    }

    /**
     * Extrait les données de la requête et les écrit dans le COMPOSANT D'INFORMATION .<BR>
     * (après avoir vérifié leur format et présence)
     *
     * @param req
     *            HttpServletRequest
     *
     */
    public void recueillirDonneesSaisies(final HttpServletRequest req) {
        final InfoBean information = procedure.getInfoBean();
        try {
            Object valeurCI = null;
            LOGGER.debug("Traitement des données ...");
            // on utilise le user-agent pour identifier le navigateur (toolbox)
            information.set("#user-agent", req.getHeader("user-agent"));
            // JB 20050711 : passage de l'ip
            information.set("#remote-addr", req.getRemoteAddr());
            // On teste si la variable ecran_physique est renseignée, auquel cas on vérifie qu'il correspond bien au dernier écran envoyé
            if (procedure.getModeEnchainement() == ProcedureBean.MODE_SEQUENTIEL) {
                final String idJSP = req.getParameter(InfoBean.ID_JSP);
                if (idJSP != null && !idJSP.equals(information.getIDJSP())) {
                    information.setEcranLogique(req.getParameter(InfoBean.ECRAN_LOGIQUE));
                }
            }
            // gestion du multi-sites
            final ServiceInfosSite serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final InfosSite infosSite = serviceInfosSite.getSiteByHost(req.getServerName());
            final AbstractProcessusBean processus = procedure.getProcessus();
            if (processus instanceof ProcessusBean) {
                ((ProcessusBean) processus).setInfosSite(infosSite);
                ((ProcessusBean) processus).setSecure("https".equals(URLResolver.getRequestScheme(req)));
            }
            // on determine l'action utilisateur
            information.setActionUtilisateur("");
            boolean donneesAControlerSurActionCachee = false;
            Enumeration<String> en = req.getParameterNames();
            while (en.hasMoreElements()) {
                String cle = en.nextElement();
                if ((cle.equals(InfoBean.ACTION_VALIDER)) || (cle.equalsIgnoreCase(InfoBean.ACTION_VALIDER + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_VALIDER);
                } else if ((cle.equals(InfoBean.ACTION_ENREGISTRER)) || (cle.equalsIgnoreCase(InfoBean.ACTION_ENREGISTRER + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_ENREGISTRER);
                } else if ((cle.equals(InfoBean.ACTION_REVENIR)) || (cle.equals(InfoBean.ACTION_REVENIR + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_REVENIR);
                } else if ((cle.equals(InfoBean.ACTION_ABANDON)) || (cle.equals(InfoBean.ACTION_ABANDON + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_ABANDON);
                } else if ((cle.equals(InfoBean.ACTION_ANNULER)) || (cle.equals(InfoBean.ACTION_ANNULER + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_ANNULER);
                } else if ((cle.equals(InfoBean.ACTION_SUPPRIMER)) || (cle.equals(InfoBean.ACTION_SUPPRIMER + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_SUPPRIMER);
                } else if ((cle.equals(InfoBean.ACTION_CONFIRMER)) || (cle.equals(InfoBean.ACTION_CONFIRMER + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_CONFIRMER);
                } else if ((cle.equals(InfoBean.ACTION_AJOUTER)) || (cle.equals(InfoBean.ACTION_AJOUTER + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_AJOUTER);
                } else if ((cle.equals(InfoBean.ACTION_DETAIL)) || (cle.equals(InfoBean.ACTION_DETAIL + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_DETAIL);
                } else if ((cle.equals(InfoBean.ACTION_RECHERCHER)) || (cle.equals(InfoBean.ACTION_RECHERCHER + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_RECHERCHER);
                } else if ((cle.equals(InfoBean.ACTION_CREER)) || (cle.equals(InfoBean.ACTION_CREER + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_CREER);
                } else if ((cle.equals(InfoBean.ACTION_SUITE)) || (cle.equals(InfoBean.ACTION_SUITE + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_SUITE);
                } else if ((cle.equals(InfoBean.ACTION_MODIFIER)) || (cle.equals(InfoBean.ACTION_MODIFIER + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_MODIFIER);
                } else if ((cle.equals(InfoBean.ACTION_TRADUIRE)) || (cle.equals(InfoBean.ACTION_TRADUIRE + ".x"))) {
                    information.setActionUtilisateur(InfoBean.ACTION_TRADUIRE);
                } else if ((cle.length() > 6) && (cle.substring(0, 7).equals("ACTION_"))) {
                    information.setActionUtilisateur(cle.substring(7, cle.length()));
                }
            }
            /* Si aucun bouton de type submit, alors on regarde s'il existe une variable cachée ACTION */
            if ("".equals(information.getActionUtilisateur())) {
                final String paramValues[] = req.getParameterValues("ACTION");
                if (paramValues != null) {
                    final String valeurAction = paramValues[0];
                    if ((valeurAction != null) && (valeurAction.length() > 0)) {
                        information.setActionUtilisateur(valeurAction);
                        // Sur action variable cachée, controle systématique
                        // ceci est une bidouille pour les pages libres
                        if (!"ABANDON".equals(valeurAction) && !InfoBean.ACTION_ANNULER.equals(valeurAction) && !"SUPPRIMER".equals(valeurAction) && !valeurAction.startsWith("NOCTRL")) {
                            donneesAControlerSurActionCachee = true;
                        }
                    }
                }
            }
            // aucune action : VALIDER par défaut
            if (StringUtils.isEmpty(information.getActionUtilisateur())) {
                information.setActionUtilisateur(InfoBean.ACTION_VALIDER);
            }
            /************************************************************************
             * Tri des valeurs en fonction de l'indice dans la page CeinfoBean afin de controler les zones dans l'ordre d'affichage La vecteur sera de la forme suivante : { ZONE
             * CONTROLEE1.. ZONE CONTROLEEn } puis {ZONE NON CONTROLEE1 ..ZONE NON CONTROLEEn }
             **************************************************************************/
            final Vector<String> valeursTriees = new Vector<>();
            final HashSet<String> contentNewRessources = new HashSet<>();
            HashSet<String> contentOldRessources = new HashSet<>();
            final HashSet<String> contentNewRessourcesSaisies = new HashSet<>();
            final HashSet<String> contentRessourcesSupprimees = new HashSet<>();
            final ArrayList<String> lstToolbox = new ArrayList<>();
            /* Création du vecteur avec des valeurs null pour la partie correspondant
               aux zones controlees { ZONE CONTROLEE1.. ZONE CONTROLEEn }
               Parcours de toutes les chaines de formatage
             */
            int maxIndice = -1;
            en = req.getParameterNames();
            while (en.hasMoreElements()) {
                String cle = en.nextElement();
                if (cle.indexOf("#FORMAT_") == 0) {
                    final String valeurFormat = req.getParameter(cle);
                    final int indice = getIndice(valeurFormat);
                    if (indice != -1) {
                        maxIndice = Math.max(maxIndice, indice);
                    }
                }
            }
            for (int i = 0; i <= maxIndice; i++) {
                valeursTriees.add(null);
            }
            /* Remplissage du vecteur trié */
            en = req.getParameterNames();
            while (en.hasMoreElements()) {
                String cle = en.nextElement();
                /* On ne traite pas la variable action si elle est vide
                   (elle peut être utilisée en variable cachée
                 */
                boolean insererValeur = true;
                if ("ACTION".equals(cle)) {
                    insererValeur = false;
                }
                if (insererValeur) {
                    /* Nouveau comportement 5.1.6, on ajoute sytématiquement la valeur sans s'occuper de l'indice
                     * Permet de récupérer les valeurs de plusieurs formulaires (insérés en include)
                     */
                    valeursTriees.add(cle);
                }
            }
            /* ***************************************************************
            Parcours des valeurs
            A FAIRE : NETTOYAGE DES ACTIONS (VALIDER, PREC, ACTION_ ..)
             *******************************************************************/
            en = valeursTriees.elements();
            // si aucune erreur jusque là
            if (information.getMessageErreur().isEmpty()) {
                //TRACE
                LOGGER.debug("Collecte des données de la requête...");
                //lecture et écriture des paramètres de la requête
                boolean controleZoneOK = true;
                while (en.hasMoreElements()) {
                    String cle = en.nextElement();
                    if (cle != null) {
                        String valeur = req.getParameterValues(cle)[0];
                        //Log.debug("\t" + cle + " = " + valeur);
                        /************************************/
                        /*                                  */
                        /* Analyse    des chaines de format */
                        /*                                  */
                        /************************************/
                        boolean chaineFormatCourante = false;
                        int typeFormatCourant = FormateurJSP.FORMAT_TEXTE;
                        int optionModifCourante = FormateurJSP.SAISIE_FACULTATIF;
                        if ((cle.length() > 8) && ("#FORMAT_".equals(cle.substring(0, 8)))) {
                            chaineFormatCourante = true;
                            int i = 0;
                            final String[] options = valeur.split(";", -2);
                            for (final String option : options) {
                                switch (i) {
                                    case 0:
                                        try {
                                            optionModifCourante = Integer.parseInt(option);
                                        } catch (final NumberFormatException e) {
                                            LOGGER.debug("value" + option + " is not a valid integer", e);
                                        }
                                        break;
                                    case 1:
                                        try {
                                            typeFormatCourant = Integer.parseInt(option);
                                        } catch (final NumberFormatException e) {
                                            LOGGER.debug("value" + option + " is not a valid integer", e);
                                        }
                                        break;
                                }
                                i++;
                            }
                            /* Cas particulier des CHECKBOX */
                            /* Pourlesquelles Un format mais peut-être pas de données */
                            if (typeFormatCourant == FormateurJSP.FORMAT_CHECKBOX) {
                                /* Si pas de valeur, la checkbox est décochée */
                                final String nomCheckbox = cle.substring(8);
                                if (req.getParameter(nomCheckbox) == null) {
                                    information.set(nomCheckbox, "0");
                                }
                            }
                        }
                        if ((donneesAControlerSurActionCachee) || (information.getActionUtilisateur().equals(InfoBean.ACTION_VALIDER)) || (information.getActionUtilisateur().equals(InfoBean.ACTION_CONFIRMER)) || (information.getActionUtilisateur().equals(InfoBean.ACTION_ENREGISTRER)) || (information.getActionUtilisateur().equals(InfoBean.ACTION_MODIFIER)) || (information.getActionUtilisateur().equals(InfoBean.ACTION_SUITE)) || (information.getActionUtilisateur().equals(InfoBean.ACTION_ONGLET))) {
                            if (chaineFormatCourante) {
                                /************************************/
                                /*                                  */
                                /* Traitement des chaines de format */
                                /*     sur VALIDER                  */
                                /*                                  */
                                /************************************/
                                /* Cas particulier des Fichiers uploadés */
                                if (typeFormatCourant == FormateurJSP.FORMAT_FICHIER) {
                                    final String nomFichier = cle.substring(8);
                                    final ExtendedRequest ereq = (ExtendedRequest) req;
                                    final UploadedFile f = ereq.getFileParameter(nomFichier + "_FILE");
                                    procedure.getInfoBean().set(nomFichier + "_FILE", null);
                                    if ((f != null) && (f.getContentFilename().length() != 0)) {
                                        //Stockage dans le CI du fichier
                                        procedure.getInfoBean().set(nomFichier + "_FILE", f);
                                        //Stockage dans le CI du nom du fichier
                                        procedure.getInfoBean().set(nomFichier, f.getContentFullFilename());
                                        LOGGER.debug("fichier uploadé : " + f.getTemporaryFile().getPath() + "  name :" + f.getContentFilename());
                                    }
                                    /* Contrôle de présence */
                                    else {
                                        if (controleZoneOK) {
                                            final String[] exceptionFileUpload = ereq.getParameterValues(UploadedFile.PARAM_JTF_EXCEPTION_FILE_UPLOAD);
                                            if (exceptionFileUpload == null || exceptionFileUpload[0].equals(UploadedFile.EXCEPTION_FILE_UPLOAD_FICHIER_INEXISTANT)) {
                                                if (optionModifCourante == FormateurJSP.SAISIE_OBLIGATOIRE) {
                                                    if (procedure.getModeEnchainement() == ProcedureBean.MODE_SEQUENTIEL) {
                                                        final String msg = MessageHelper.getCoreMessage(procedure.getLocale(), "JTF_ERR_FIC_OBLIGATOIRE");
                                                        information.addMessageErreur(msg + "#" + nomFichier);
                                                    } else {
                                                        final String msg1 = MessageHelper.getCoreMessage(procedure.getLocale(), "JTF_ERR_FIC_OBLIGATOIRE_BIS_1");
                                                        final String msg2 = MessageHelper.getCoreMessage(procedure.getLocale(), "JTF_ERR_FIC_OBLIGATOIRE_BIS_2");
                                                        information.addMessageErreur(msg1 + " [" + nomFichier + "] " + msg2);
                                                    }
                                                    controleZoneOK = false;
                                                }
                                            } else if (exceptionFileUpload[0].equals(UploadedFile.EXCEPTION_FILE_UPLOAD_TAILLE_FICHIER)) {
                                                try {
                                                    int taille = Integer.parseInt(ereq.getParameterValues(UploadedFile.PARAM_JTF_EXCEPTION_FILE_UPLOAD)[1]);
                                                    taille = taille / 1024;
                                                    final String msg1 = MessageHelper.getCoreMessage(procedure.getLocale(), "JTF_ERR_FIC_TAILLE");
                                                    information.addMessageErreur(msg1 + " [" + taille + "Ko] ");
                                                    controleZoneOK = false;
                                                } catch (final NumberFormatException e) {
                                                    throw new ErreurApplicative(MessageHelper.getCoreMessage(procedure.getLocale(), "UPLOAD_FICHIER_SIMPLE_ERREUR_TAILLE"));
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                /************************************/
                                /*                                  */
                                /* Traitement des zones de saisie   */
                                /*                                  */
                                /************************************/
                                if (controleZoneOK) {
                                    // on récupére le champ de format
                                    final String cleFormat = "#FORMAT_".concat(cle);
                                    final String valeurFormat = req.getParameter(cleFormat);
                                    String formatSupplementaire = "";
                                    if (valeurFormat != null) {
                                        int optionModification = FormateurJSP.SAISIE_FACULTATIF;
                                        int typeFormat = FormateurJSP.FORMAT_TEXTE;
                                        int nbCarMax = 65000;
                                        int i = 0;
                                        final String[] options = valeurFormat.split(";", -2);
                                        for (final String option : options) {
                                            switch (i) {
                                                case 0:
                                                    try {
                                                        optionModification = Integer.parseInt(option);
                                                    } catch (final NumberFormatException e) {
                                                        LOGGER.debug("value" + option + " is not a valid integer", e);
                                                    }
                                                    break;
                                                case 1:
                                                    try {
                                                        typeFormat = Integer.parseInt(option);
                                                    } catch (final NumberFormatException e) {
                                                        LOGGER.debug("value" + option + " is not a valid integer", e);
                                                    }
                                                    break;
                                                case 2:
                                                    break;
                                                case 3:
                                                    try {
                                                        nbCarMax = Integer.parseInt(option);
                                                    } catch (final NumberFormatException e) {
                                                        LOGGER.debug("value" + option + " is not a valid integer", e);
                                                    }
                                                    break;
                                                case 4:
                                                    formatSupplementaire = option;
                                                    break;
                                            }
                                            i++;
                                        }
                                        /* Controle de présence */
                                        if (optionModification == FormateurJSP.SAISIE_OBLIGATOIRE) {
                                            if (((typeFormat == FormateurJSP.FORMAT_COMBO) && ("0000".equals(valeur))) || (valeur.length() == 0)) {
                                                final String msg1 = MessageHelper.getCoreMessage(procedure.getLocale(), "JTF_ERR_ZONE_OBLIGATOIRE_BIS_1");
                                                final String msg2 = MessageHelper.getCoreMessage(procedure.getLocale(), "JTF_ERR_ZONE_OBLIGATOIRE_BIS_2");
                                                information.addMessageErreur(msg1 + " " + EscapeString.escapeScriptAndEvent(getLibelleZone(cle, formatSupplementaire)) + " " + msg2);
                                                controleZoneOK = false;
                                            }
                                        } /* Contrôle de format */
                                        if (controleZoneOK) {
                                            try {
                                                switch (typeFormat) {
                                                    case FormateurJSP.FORMAT_DATE:
                                                        valeurCI = controlerDate(valeur, cle, formatSupplementaire);
                                                        break;
                                                    case FormateurJSP.FORMAT_ENTIER:
                                                        if (valeur.length() > 0) {
                                                            valeurCI = Integer.valueOf(valeur);
                                                        } else {
                                                            valeurCI = 0;
                                                        }
                                                        break;
                                                    case FormateurJSP.FORMAT_LONG:
                                                        if (valeur.length() > 0) {
                                                            valeurCI = Long.valueOf(valeur);
                                                        } else {
                                                            valeurCI = (long) 0;
                                                        }
                                                        break;
                                                    case FormateurJSP.FORMAT_DECIMAL:
                                                        if (valeur.length() > 0) {
                                                            valeurCI = Double.valueOf(valeur);
                                                        } else {
                                                            valeurCI = 0.;
                                                        }
                                                        break;
                                                    case FormateurJSP.FORMAT_CHECKBOX:
                                                        valeurCI = valeur;
                                                        break;
                                                    case FormateurJSP.FORMAT_TEXTE:
                                                        valeurCI = EscapeString.escapeScriptAndEvent(valeur);
                                                        break;
                                                    case FormateurJSP.FORMAT_TEXTE_CACHE:
                                                    case FormateurJSP.FORMAT_MULTI_LIGNE_HTML:
                                                    case FormateurJSP.FORMAT_MULTI_LIGNE:
                                                        if (valeur.trim().length() > 0) {
                                                            valeurCI = controlerTexte(valeur, cle, typeFormat, formatSupplementaire, nbCarMax);
                                                            final String chaineCodeHTML = FormateurJSP.renvoyerValeurFormatage(formatSupplementaire, "CODE_HTML");
                                                            if ("1".equals(chaineCodeHTML)) {
                                                                parseRessources((String) valeurCI, contentNewRessourcesSaisies);
                                                                lstToolbox.add(cle);
                                                            }
                                                        } else {
                                                            valeurCI = "";
                                                        }
                                                        break;
                                                    default:
                                                        valeurCI = valeur;
                                                        break;
                                                }
                                            } catch (final ErreurFormatage ef) {
                                                LOGGER.debug("format error", ef);
                                                information.addMessageErreur(ef.getMessage());
                                                controleZoneOK = false;
                                            } catch (final Exception e) {
                                                LOGGER.debug("an error occured", e);
                                                final String msg1 = MessageHelper.getCoreMessage(procedure.getLocale(), "JTF_ERR_FMT_INCORRECT_BIS_1");
                                                final String msg2 = MessageHelper.getCoreMessage(procedure.getLocale(), "JTF_ERR_FMT_INCORRECT_BIS_2");
                                                information.addMessageErreur(msg1 + " " + EscapeString.escapeScriptAndEvent(getLibelleZone(cle, formatSupplementaire)) + " " + msg2);
                                                controleZoneOK = false;
                                            }
                                        }
                                    } else {
                                        /* Pas de formatage : recopie en string */
                                        valeurCI = valeur;
                                    }
                                }
                                /* Stockage en tant que zone controlee */
                                // les fichiers uploades ont déjà été stockés (traitement du format)
                                if (!cle.endsWith("_FILE")) {
                                    if (controleZoneOK) {
                                        information.set(cle, valeurCI);
                                    } else {
                                        information.setDonneeSaisie(cle, valeur);
                                    }
                                }
                            }
                        } else {
                            /* Action autre que Valider
                            Les valeurs saisies sont stockées telle qu'elles
                            sans être contrôlées
                             */
                            information.setDonneeSaisie(cle, valeur);
                        }
                    }
                }
            }
            // rechargement des données sauvegardées avant de revenir dans le processus
            if (procedure.getMemorisationCI()) {
                final Object oCI = information.get("ID_CI_RESTAURATION");
                if (oCI != null) {
                    TreeMap<Object, Object> listeCI = new TreeMap<>();
                    final Object liste = procedure.getProcessusManager().getSessionUtilisateur().getInfos().get(SessionUtilisateur.LISTE_COMPOSANTS_INFORMATIONS);
                    if (liste != null) {
                        listeCI = (TreeMap<Object, Object>) liste;
                    }
                    final Object o = listeCI.get(oCI);
                    if (o != null) {
                        final Date nullableDate = new Date(0);
                        final InfoBean oldCI = (InfoBean) o;
                        for (final Data data : oldCI.getDatas().values()) {
                            final String nomDonnee = data.getName();
                            if (lstToolbox.contains(nomDonnee)) {
                                parseRessources((String) data.getValue(), contentRessourcesSupprimees);
                            }
                            if (information.get(nomDonnee) == null) {
                                information.setData(nomDonnee, data);
                                if (data.getValue() instanceof String) {
                                    parseRessources((String) data.getValue(), contentNewRessources);
                                }
                            }
                            // FIXME : patch foireux pour gérer les dates "vides". En l'occurence, on se prive du 01/01/1970. A revoir lorsque l'on empilera plus les infoBeans
                            if(nullableDate.equals(information.get(nomDonnee))) {
                                information.set(nomDonnee, null);
                            }
                        }
                    } else {
                        throw new ErreurApplicative(MessageHelper.getCoreMessage("AUTHENTIFICATION.ERREUR.SESSION_REINITIALISEE"));
                    }
                }
            }
            // on ajoute les listes d'id ressources au composant d'information
            if (information.get("contentOldRessources") != null) {
                contentOldRessources = ((HashSet<String>) information.get("contentOldRessources"));
            }
            contentNewRessources.addAll(contentNewRessourcesSaisies);
            information.set("contentNewRessources", contentNewRessources);
            contentOldRessources.addAll(contentRessourcesSupprimees);
            contentOldRessources.removeAll(contentNewRessources);
            information.set("contentOldRessources", contentOldRessources);
        } catch (final ErreurApplicative e) {
            LOGGER.error("erreur lors de la récupération des données de la requête", e);
            information.addMessageErreur(e.getMessage());
        }
    }

    /**
     * Parses the ressources.
     *
     * @param value
     *            the value
     * @param lstRessources
     *            the lst ressources
     */
    private void parseRessources(final String value, final Set<String> lstRessources) {
        if (StringUtils.contains(value, "[id-image]")) {
            parseRessourceTag("id-image", value, "IMG", lstRessources);
        }
        if (StringUtils.contains(value, "[id-fichier]")) {
            parseRessourceTag("id-fichier", value, "LIEN", lstRessources);
        }
    }

    /**
     * Parses the ressource tag.
     *
     * @param tag
     *            the tag
     * @param tout
     *            the tout
     * @param type
     *            the type
     * @param lstRessources
     *            the lst ressources
     *
     */
    private void parseRessourceTag(final String tag, final String tout, final String type, final Set<String> lstRessources) {
        if (tout != null && tout.length() > 0) {
            String tmpFin = tout;
            int idxDebID = tout.indexOf("[" + tag + "]");
            while (idxDebID != -1) {
                //contient la fin de la chaine
                tmpFin = tmpFin.substring(idxDebID + tag.length() + 2, tmpFin.length());
                int idxFinID = tmpFin.indexOf("[/" + tag + "]");
                //recup de l'id du fichier
                String id = tmpFin.substring(0, idxFinID);
                if (id.startsWith("F")) {
                    id = id.substring(1);
                }
                if (id.startsWith(";")) {
                    id = id.substring(1);
                }
                lstRessources.add(id + "#" + type);
                //on recherche un nouveau tag
                idxDebID = tmpFin.indexOf("[" + tag + "]");
            }
        }
    }
}
