package com.jsbsoft.jtf.core;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyConfigurer;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.logging.LogHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.service.ServiceInfosSite;

import ch.qos.logback.classic.AsyncAppender;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;

/**
 * Filtre permettant de logger chaque requête de l'utilisateur.
 * Il est désactiver par défaut. Chaque requête et logger au niveau trace,
 * les infos sont : l'url demander, le referer, la rubrique, la date, la session si connecté.
 */
public class LogAppFilter implements Filter {

    public static final String FILE_NAME_REQUETES = "requetes";

    private static final String LOGAPP_APPENDER_NAME = "LOGAPP";

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(LogAppFilter.class);

    private static final String PROP_LOGAPP_FILTER = "logapp.filter";

    private AsyncAppender asyncAppender;

    @Override
    public void destroy() {
        //nothing to release
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain filter) throws IOException, ServletException {
        if (isActivated()) {
            final long timerStart = System.currentTimeMillis();
            final String uuid = UUID.randomUUID().toString();
            final HttpServletRequest request = (HttpServletRequest) req;
            SessionUtilisateur sessionUtilisateur = null;
            // l'url
            final String url = request.getRequestURI();
            // la requete
            String requete = request.getQueryString();
            final ServiceInfosSite serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final InfosSite infosSite = serviceInfosSite.getSiteByHost(request.getServerName());
            final String codeSite = infosSite.getAlias();
            // le referer
            String hostReferer = request.getHeader("referer");
            if (StringUtils.isNotBlank(hostReferer)) {
                if (requete == null && hostReferer.contains("?")) {
                    requete = hostReferer.substring(hostReferer.indexOf("?") + 1) + "(referer)";
                }
                if (hostReferer.contains("//")) {
                    hostReferer = hostReferer.substring(hostReferer.indexOf("//") + 2);
                    if (hostReferer.contains("/")) {
                        hostReferer = hostReferer.substring(0, hostReferer.indexOf("/"));
                    }
                    if (hostReferer.contains(":")) {
                        hostReferer = hostReferer.substring(0, hostReferer.indexOf(":"));
                    }
                }
            }
            // le user-agent
            final String userAgent = StringUtils.defaultString(request.getHeader("user-agent"));
            // l'adresse ip
            String remoteAddr = request.getRemoteAddr();
            // /!\ avec un load balancer, la bonne IP client est dans le header
            if (request.getHeader("REMOTE_ADDR") != null && request.getHeader("REMOTE_ADDR").length() > 0) {
                remoteAddr = request.getHeader("REMOTE_ADDR");
            }
            final HttpSession _session = request.getSession(false);
            if (_session != null) {
                sessionUtilisateur = (SessionUtilisateur) _session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
            }
            logTraceActiviteWeb(sessionUtilisateur, uuid, timerStart, hostReferer, remoteAddr, userAgent, codeSite, url, requete);
        }
        filter.doFilter(req, res);
    }

    /**
     * Methode qui trace l'activite web a chaque appel
     *
     * @param uuid
     * @param hostReferer
     * @param ip
     * @param userAgent
     */
    private void logTraceActiviteWeb(final SessionUtilisateur sessionUtilisateur, final String uuid, final long timerStart, final String hostReferer, final String ip, final String userAgent, final String codeSite, final String url, final String requete) {
        final long timerNow = System.currentTimeMillis();
        Map<String, Object> infosSession = null;
        if (sessionUtilisateur != null) {
            infosSession = sessionUtilisateur.getInfos();
        }
        String logLine = uuid + ";" + timerNow + ";" + (timerStart > 0 ? timerNow - timerStart : "") + ";" + ip + ";";
        if (infosSession != null) {
            logLine += (String) infosSession.get(SessionUtilisateur.KSESSION) + ";" + (String) infosSession.get(SessionUtilisateur.CODE);
        }
        logLine +=  ";" + codeSite + ";" + ";" //service
            + ";" //rubrique
            + url + ";" + StringUtils.defaultString(requete) + ";";
        logLine += hostReferer + ";" + "\"" + userAgent + "\";"; // guillemets car on a des ";" dans les user-agents
        LOGGER.trace(logLine);
    }

    public boolean isActivated() {
        return "1".equals(PropertyHelper.getCoreProperty(PROP_LOGAPP_FILTER));
    }

    @Override
    public void init(final FilterConfig arg0) throws ServletException {
        if (!isActivated()) {
            // on supprime l'appender précédent
            if (asyncAppender != null) {
                LOGGER.detachAppender(asyncAppender);
                asyncAppender.stop();
                asyncAppender = null;
            }
        }
        // on initialise l'appender
        if (asyncAppender == null) {
            asyncAppender = new AsyncAppender();
            asyncAppender.setDiscardingThreshold(0);
            asyncAppender.setContext(LOGGER.getLoggerContext());
            asyncAppender.setName(LOGAPP_APPENDER_NAME);
            final RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<>();
            fileAppender.setContext(LOGGER.getLoggerContext());
            final File folderApp = new File(WebAppUtil.getLogsPath() + File.separator + "app");
            if (!folderApp.exists()) {
                folderApp.mkdir();
            }
            // Pattern de l'encoder
            final PatternLayoutEncoder encoder = new PatternLayoutEncoder();
            encoder.setPattern("%d{HH:mm:ss,SSS};%m %n");
            encoder.setContext(LOGGER.getLoggerContext());
            // Système de trigger
            final DefaultTimeBasedFileNamingAndTriggeringPolicy<ILoggingEvent> timeTrigger = new DefaultTimeBasedFileNamingAndTriggeringPolicy<>();
            timeTrigger.setContext(LOGGER.getLoggerContext());
            // Système de roulement des fichiers (en fonction du jour)
            final TimeBasedRollingPolicy<ILoggingEvent> policy = new TimeBasedRollingPolicy<>();
            policy.setContext(LOGGER.getLoggerContext());
            // nom du fichier archivé
            policy.setFileNamePattern(folderApp.getAbsolutePath() + File.separator + FILE_NAME_REQUETES + LogHelper.LOG_EXTENSION + "." + PropertyConfigurer.getProperty(LogHelper.PROP_LOG_FILENAME_PATTERN));
            policy.setTimeBasedFileNamingAndTriggeringPolicy(timeTrigger);
            // maximum d'archive
            if (StringUtils.isNotEmpty(PropertyConfigurer.getProperty(LogHelper.PROP_LOG_MAX_HISTORY))) {
                try {
                    policy.setMaxHistory(Integer.parseInt(PropertyConfigurer.getProperty(LogHelper.PROP_LOG_MAX_HISTORY)));
                } catch (final NumberFormatException e) {
                    LOGGER.debug("unable to parse the given valuen", e);
                    LOGGER.error("La valeur de la propriétée \"" + LogHelper.PROP_LOG_MAX_HISTORY + "\" n'est pas numérique");
                }
            }
            // Application des triggers
            timeTrigger.setTimeBasedRollingPolicy(policy);
            fileAppender.setRollingPolicy(policy);
            fileAppender.setEncoder(encoder);
            fileAppender.setFile(folderApp.getAbsolutePath() + File.separator + FILE_NAME_REQUETES + LogHelper.LOG_EXTENSION);
            fileAppender.setAppend(true);
            policy.setParent(fileAppender);
            policy.start();
            encoder.start();
            fileAppender.start();
            asyncAppender.addAppender(fileAppender);
            //On augmente la taille du buffer pour éviter les blocages
            asyncAppender.setQueueSize(1500);
            asyncAppender.start();
            LOGGER.addAppender(asyncAppender);
            LOGGER.setLevel(Level.TRACE);
            LOGGER.setAdditive(false);
        }
    }
}