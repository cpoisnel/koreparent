package com.jsbsoft.jtf.core;

import com.jsbsoft.jtf.database.ProcessusBean;

public class ErrorProcessus extends ProcessusBean {

    protected static final String NOM_PROCESSUS = "ERREUR_PROCESSUS";

    protected static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    public ErrorProcessus(final InfoBean infoBean) {
        super(infoBean);
    }

    @Override
    protected boolean traiterAction() {
        ecranLogique = infoBean.getEcranLogique();
        action = infoBean.getActionUtilisateur();
        etat = EN_COURS;
        // placer l'état dans le composant d'infoBean
        infoBean.setNomProcessus(NOM_PROCESSUS);
        infoBean.setEcranLogique(ECRAN_PRINCIPAL);
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }
}
