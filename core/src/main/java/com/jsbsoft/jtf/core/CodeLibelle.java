package com.jsbsoft.jtf.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;

/**
 * Gestionnaire de table code-libellé ( fichiers .dat) <BR>
 * Gère également les fichiers multi-langues
 */
public class CodeLibelle {

    public static final String ID_BEAN = "codeLibelle";

    /** The Constant TABLE_PATH. */
    public static final String TABLE_RELATIVE_PATH = "/WEB-INF/tables/";

    private static final String PROP_TABLE_REPERTOIRE_SPECIFIC = "table.repertoire.specific";

    private static final Logger LOGGER = LoggerFactory.getLogger(CodeLibelle.class);

    /**
     * renvoie le nom de la table suffixé par le code langue <BR>
     * (_fr_FR, _en ...)
     *
     * @param _locale
     *            the _locale
     *
     * @return nom de la table
     */
    public static String getSuffixe(final Locale _locale) {
        String suffixe = "_" + _locale.getLanguage();
        if (_locale.getCountry().length() > 0) {
            suffixe += "_" + _locale.getCountry();
        }
        return suffixe;
    }

    /**
     * Renvoie le libellé à partir du code.
     *
     * @param idCtx l'id de l'extension
     * @param nomTable
     *            le nom de la table à chercher
     * @param codes
     *            les codes à rechercher
     *
     * @return libelle
     */
    public static String lireLibelle(final String idCtx, final String nomTable, final String codes) {
        return lireLibelle(idCtx, nomTable, null, codes);
    }

    /**
     * Renvoie le libellé à partir du code.
     *
     * @param idCtx l'id de l'extension
     * @param nomTable
     *            the _nom table
     * @param locale
     *            the _locale
     * @param codes
     *            the code
     *
     * @return libelle
     */
    public static String lireLibelle(final String idCtx, final String nomTable, final Locale locale, final String codes) {
        String res = "";
        if (codes != null && !"".equals(codes) && !"0000".equals(codes)) {
            final Hashtable<String, String> h = lireTable(getPathTable(idCtx, nomTable, locale));
            final StringTokenizer st = new StringTokenizer(codes, ";");
            while (st.hasMoreTokens()) {
                final String code = st.nextToken();
                if (res.length() > 0) {
                    res += ";";
                }
                String libelle = h.get(code);
                if (libelle == null) {
                    libelle = "";
                }
                //AM 200501 : les libellés commencant par ... et finissant par ...
                //peuvent etre sélectionnés lors de la saisie
                // mais ces pointillés ne doivent pas apparaitre dans le front
                //donc on les enleve (contrairement aux libellés avec -- qui ne sont carrément pas saisissables)
                libelle = libelle.trim();
                if (libelle.startsWith("...") && libelle.endsWith("...")) {
                    while (libelle.startsWith(".")) {
                        libelle = libelle.substring(1);
                    }
                    while (libelle.endsWith(".")) {
                        libelle = libelle.substring(0, libelle.length() - 1);
                    }
                    libelle = libelle.trim();
                }
                res += libelle;
            }
        }
        return res;
    }

    /**
     * Renvoie le contenu d'une table sous forme de HashTable.
     *
     * @param pathTable
     *            the nom table
     *
     * @return contenu de la table
     */
    private static Hashtable<String, String> lireTable(final String pathTable) {
        return ((CodeLibelle) ApplicationContextManager.getCoreContextBean(ID_BEAN)).getTable(pathTable);
    }

    private static String getPathTable(String idCtx, final String nomTable, final Locale locale) {
        String nomTableLocale = nomTable;
        if (StringUtils.isEmpty(idCtx)) {
            idCtx = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
        }
        File fTable = null;
        if (locale != null) {
            nomTableLocale = nomTableLocale + getSuffixe(locale);
        }
        // on cherche un répertoire spécifique en absolu
        final String repSpecific = PropertyHelper.getProperty(idCtx, PROP_TABLE_REPERTOIRE_SPECIFIC);
        if (StringUtils.isNotEmpty(repSpecific)) {
            fTable = getFileTable(nomTable, nomTableLocale, repSpecific);
        }
        // sinon on recherche le répertoire en relatif
        if (fTable == null) {
            String rep = WebAppUtil.getAbsolutePath() + repSpecific;
            fTable = getFileTable(nomTable, nomTableLocale, rep);
        }
        // sinon on cherche dans le répertoire de l'extension ou du core en dernier recours
        final IExtension extension = ExtensionHelper.getExtension(idCtx) == null ? ExtensionHelper.getCoreExtension() : ExtensionHelper.getExtension(idCtx);
        if (fTable == null && extension != null) {
            String rep = WebAppUtil.getAbsolutePath() + extension.getRelativePath() + TABLE_RELATIVE_PATH;
            fTable = getFileTable(nomTable, nomTableLocale, rep);
        }
        // sinon on recherche dans le répertoire de l'application
        if (fTable == null && !idCtx.equals(ApplicationContextManager.DEFAULT_CORE_CONTEXT)) {
            String rep = WebAppUtil.getAbsolutePath() + ExtensionHelper.getExtension("").getRelativePath() + TABLE_RELATIVE_PATH;
            fTable = getFileTable(nomTable, nomTableLocale, rep);
        }
        if (fTable != null && fTable.exists()) {
            return fTable.getAbsolutePath();
        }
        LOGGER.debug("Le fichier " + nomTable + " n'a pas été trouvé");
        return StringUtils.EMPTY;
    }

    private static File getFileTable(final String nomTable, final String nomTableLocale, final String rep) {
        File fTable;
        fTable = new File(rep + nomTableLocale + ".dat");
        if (!fTable.exists() && !nomTable.equals(nomTableLocale)) {
            fTable = new File(rep + nomTable + ".dat");
        }
        if (!fTable.exists()) {
            fTable = null;
        }
        return fTable;
    }

    /**
     * Renvoie le contenu d'une table sous forme de HashTable (multi-langue).
     *
     * @param idCtx l'id de l'extension
     * @param nomTable
     *            the _nom table
     * @param locale
     *            the _locale
     *
     * @return contenu de la table
     */
    public static Hashtable<String, String> lireTable(final String idCtx, final String nomTable, final Locale locale) {
        return lireTable(getPathTable(idCtx, nomTable, locale));
    }

    /**
     * Renvoie le contenu d'une table sous forme de HashTable.
     *
     * @param pathTable
     *            the nom table
     * @return contenu de la table
     */
    @Cacheable(value = "CodeLibelle.getTable")
    public Hashtable<String, String> getTable(final String pathTable) {
        Hashtable<String, String> h = new Hashtable<>();
        if (StringUtils.isEmpty(pathTable)) {
            return h;
        }
        try {
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(pathTable)), CharEncoding.DEFAULT_CHARSET));
                String line = br.readLine();
                // boucle sur les caractères numériques
                if (line != null) {
                    // recherche de la longueur de la cle
                    // positionnement sur le dernier caractère de la cle
                    int iLastCarCle = line.length() - 1;
                    while ((!Character.isLetterOrDigit(line.charAt(iLastCarCle))) && (line.charAt(iLastCarCle) != '$')) {
                        iLastCarCle--;
                    }
                    // parcours de la cle
                    int lgCle = 0;
                    int i = iLastCarCle;
                    while ((Character.isLetterOrDigit(line.charAt(i))) || (line.charAt(i) == '$')) {
                        lgCle++;
                        i--;
                    }
                    // calcul longueur libellé
                    int lgLibelle = iLastCarCle - lgCle + 1;
                    // lecture de chaque ligne
                    do {
                        try {
                            if (line.length() > 10) {
                                String cle = line.substring(lgLibelle, lgLibelle + lgCle);
                                // Il faut supprimer les £ qui ont été rajoutés
                                // pour que les clefs aient la meme longueur
                                final int indiceDollar = cle.indexOf('$');
                                if (indiceDollar != -1) {
                                    cle = cle.substring(0, indiceDollar);
                                }
                                h.put(cle, line.substring(0, lgLibelle));
                            }
                        }catch (StringIndexOutOfBoundsException e) {
                            LOGGER.error("Une erreur est survenue lors de l'interprétation du fichier .dat \"" + pathTable + "\"", e);
                        }
                    } while ((line = br.readLine()) != null);
                }
            } finally {
                if (br != null) {
                    br.close();
                }
            }
        } catch (final FileNotFoundException e) {
            LOGGER.debug("file not found", e);
            LOGGER.error("Fichier table [" + pathTable + "] introuvable");
        } catch (final IOException e) {
            LOGGER.error("Erreur de lecture du fichier [" + pathTable + "]", e);
        }
        return h;
    }
}