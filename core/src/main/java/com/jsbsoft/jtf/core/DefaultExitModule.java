package com.jsbsoft.jtf.core;

/**
 * Module de traitement d'un procédure . A sous-classer (classe interne jtf)
 */
class DefaultExitModule implements ModuleExit {

    /**
     * Commentaire relatif au constructeur DefaltExitModule.
     */
    public DefaultExitModule() {
        super();
    }

    /**
     * Commentaire relatif à la méthode apresCollecte.
     *
     * @param procedure
     *            the procedure
     *
     * @return the descriptif page retour
     *
     */
    @Override
    public DescriptifPageRetour apresCollecte(final ProcedureBean procedure) {
        return null;
    }

    /**
     * Commentaire relatif à la méthode apresProcessus.
     *
     * @param gp
     *            the gp
     * @param cloneCI
     *            the clone ci
     *
     * @return the descriptif page retour
     *
     */
    @Override
    public DescriptifPageRetour apresProcessus(final ProcessusManager gp, final InfoBean cloneCI) {
        return null;
    }

    /**
     * Commentaire relatif à la méthode apresSousPU.
     *
     * @param procedure
     *            the procedure
     * @param infoBeanSousPU
     *            the info bean sous pu
     *
     * @return the descriptif page retour
     */
    @Override
    public DescriptifPageRetour apresSousPU(final ProcedureBean procedure, final InfoBean infoBeanSousPU) {
        return null;
    }

    /**
     * Commentaire relatif à la méthode calculEcranPhysique.
     *
     * @param procedure
     *            the procedure
     *
     * @return the descriptif page retour
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public DescriptifPageRetour calculEcranPhysique(final ProcedureBean procedure) {
        return null;
    }
}