package com.jsbsoft.jtf.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.jsbsoft.jtf.upload.UploadedFile;
import com.kportal.core.webapp.WebAppUtil;

/**
 * Composant d'information . <BR>
 * Contient les données en transit entre le processus et les JSP . <BR>
 */
public final class InfoBean implements Serializable {

    /** donnée fictive pour afficher le dernier écran d'une procédure (Cf. la méthode <code> demanderAffichageDernierEcran</code>) */
    public static final String ECRAN_LOGIQUE_FIN = "FIN";

    /** The Constant ECRAN_LOGIQUE. */
    public static final String ECRAN_LOGIQUE = "#ECRAN_LOGIQUE#";

    /** The Constant NOM_PROCESSUS. */
    public static final String PROC = "PROC";

    /** The Constant NOM_EXTENSION. */
    public static final String EXT = "EXT";

    /** The Constant ACTION. */
    public static final String ACTION = "ACTION";

    /** The Constant ETAT_OBJET. */
    public static final String ETAT_OBJET = "#ETAT#";

    public static final String ETAT_OBJET_MODIF = "MODIFICATION";

    /** The Constant ETAT_OBJET_CREATION. */
    public static final String ETAT_OBJET_CREATION = "CREATION";

    /** The Constant ETAT_OBJET_SUPPRESSION. */
    public static final String ETAT_OBJET_SUPPRESSION = "SUPPRESSION";

    /** The Constant ETAT_OBJET_RECHERCHE. */
    public static final String ETAT_OBJET_RECHERCHE = "RECHERCHE";

    /** liste des actions utilisateurs possibles. */
    public static final String ACTION_VALIDER = "VALIDER";

    /** The Constant ACTION_ABANDON. */
    public static final String ACTION_ABANDON = "ABANDON";

    /** The Constant ACTION_REVENIR. */
    public static final String ACTION_REVENIR = "REVENIR";

    /** The Constant ACTION_ANNULER. */
    public static final String ACTION_ANNULER = "ANNULER";

    /** The Constant ACTION_SUPPRIMER. */
    public static final String ACTION_SUPPRIMER = "SUPPRIMER";

    /** The Constant ACTION_RECHERCHER. */
    public static final String ACTION_RECHERCHER = "RECHERCHER";

    /** The Constant ACTION_CONFIRMER. */
    public static final String ACTION_CONFIRMER = "CONFIRMER";

    /** The Constant ACTION_DETAIL. */
    public static final String ACTION_DETAIL = "DETAIL";

    /** The Constant ACTION_AJOUTER. */
    public static final String ACTION_AJOUTER = "AJOUTER";

    /** The Constant ACTION_CREER. */
    public static final String ACTION_CREER = "CREER";

    /** The Constant ACTION_SUITE. */
    public static final String ACTION_SUITE = "SUITE";

    /** The Constant ACTION_MODIFIER. */
    public static final String ACTION_MODIFIER = "MODIFIER";

    /** The Constant ACTION_TRADUIRE. */
    public static final String ACTION_TRADUIRE = "TRADUIRE";

    /** The Constant ACTION_DUPLIQUER. */
    public static final String ACTION_DUPLIQUER = "DUPLIQUER";

    /** The Constant ACTION_ONGLET. */
    public static final String ACTION_ONGLET = "ONGLET";

    /** The Constant LIBELLE_DUPLICATION. */
    public static final String LIBELLE_DUPLICATION = "Copie";

    /** The Constant LIBELLE_TRADUCTION. */
    public static final String LIBELLE_TRADUCTION = "Traduction";

    /** The Constant ACTION_ENREGISTRER. */
    public static final String ACTION_ENREGISTRER = "ENREGISTRER";

    /** The Constant ECRAN_REDIRECTION. */
    protected static final String ECRAN_REDIRECTION = "#ECRAN_REDIRECTION#";

    /** The Constant MODE_ENCHAINEMENT. */
    protected static final String MODE_ENCHAINEMENT = "#MODE_ENCHAINEMENT#";

    /** The Constant ID_JSP. */
    protected static final String ID_JSP = "#ID_JSP#";

    /** The Constant BEAN_PU. */
    protected static final String BEANPU = "BEANPU";

    /** The Constant NOM_NOUVEL_ONGLET. */
    protected static final String NOM_NOUVEL_ONGLET = "NOUVEL_ONGLET";

    private static final Logger LOG = LoggerFactory.getLogger(InfoBean.class);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The Constant ECRAN_PHYSIQUE. */
    private static final String ECRAN_PHYSIQUE = "#ECRAN_PHYSIQUE#";

    /** The Constant ECRAN_PHYSIQUE. */
    private static final String TITRE_ECRAN = "#TITRE_ECRAN#";

    /** The Constant ECRAN_CONTENEUR. */
    private static final String ECRAN_CONTENEUR = "#ECRAN_CONTENEUR#";

    /** The Constant ID_COMPOSANT (contient le composant lié au processus courant . */
    private static final String ID_COMPOSANT = "#ID_COMPOSANT#";

    /** The Constant UPLOADED_FILE. */
    private static final String UPLOADED_FILE = "#FILE#";

    /** The Constant NOM_ONGLET. */
    private static final String NOM_ONGLET = "#ONGLET#";

    /** The DELA i_ purge. */
    private static final long DELAI_PURGE = 10 * 60 * 1000;

    /** The DELA i_ expiration. */
    private static final long DELAI_EXPIRATION = 60 * 60 * 1000;

    /** The ts last purge. */
    private static long tsLastPurge = 0;

    /** ensemble d'objet Data indexés par leur nom. */
    private Map<String, Data> datas = new HashMap<>(100);

    private Map<String, List<String>> messages = new HashMap<>();

    /** The info bean pere. */
    private InfoBean infoBeanPere = null;

    /** The session http. */
    private transient HttpSession sessionHttp = null;

    public InfoBean() {
    }

    /**
     * Constructeur spéinfoBeanfique au prototype pour éviter l'apparition du message "null" en rouge dans certaines pages. Date de création : (07/02/00 15:21:27)
     *
     * @param pere
     *            the pere
     */
    public InfoBean(final InfoBean pere) {
        infoBeanPere = pere;
    }

    /**
     * Restauration des datas à partir d'un fichier temporaire.
     *
     * @param request
     *            the request
     * @param idFichier
     *            the id fichier
     *
     * @return the info bean
     *
     * @throws IOException
     *             the exception
     */
    public static InfoBean restaurerDepuisFichier(final HttpServletRequest request, final String idFichier) throws IOException, ClassNotFoundException {
        // Récupéraation du code de l'utilisateur
        final HttpSession _session = request.getSession(false);
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) _session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
        final String ksession = (String) infosSession.get(SessionUtilisateur.KSESSION);
        // compteur du nombre de restauration du fichier
        final int count = ((int) (infosSession.get(idFichier) != null ? infosSession.get(idFichier) : 1)) + 1;
        infosSession.put(idFichier, count);
        // restauration datas
        final String pathFichier = WebAppUtil.getWorkDefaultPath() + File.separator + "datas_" + ksession + "_" + idFichier + ".tmp";
        final FileInputStream fis = new FileInputStream(pathFichier);
        final ObjectInputStream in = new ObjectInputStream(fis);
        final InfoBean infoBean = (InfoBean) in.readObject();
        in.close();
        fis.close();
        infoBean.setSessionHttp(_session);
        return infoBean;
    }

    /**
     * @param _ksession
     *            the _ksession
     */
    public static void supprimerDatas(final String _ksession) {
        //purge pour le type
        final File folder = new File(WebAppUtil.getWorkDefaultPath());
        final FilenameFilter filtre = new InfoBeanFilter(_ksession, "");
        final File[] listeFiles = folder.listFiles(filtre);
        for (final File listeFile : listeFiles) {
            listeFile.delete();
        }
    }

    /**
     * Renvoie la valeur d'une donnée (recherche récursive).
     *
     * @param name
     *            nom de la donnée
     *
     * @return valeur de l'objet
     */
    public String formater(final String name) {
        return Formateur.formater(get(name, true));
    }

    /**
     * Renvoie la valeur d'une donnée (recherche récursive).
     *
     * @param name
     *            nom de la donnée
     * @param table
     *            the table
     *
     * @return valeur de l'objet
     */
    public String formaterLibelle(final String name, final String table) {
        return Formateur.formaterLibelle(getNomExtension(), get(name, true), table);
    }

    /**
     * Renvoie la valeur d'une donnée (recherche récursive).
     *
     * @param name
     *            nom de la donnée
     *
     * @return valeur de l'objet
     */
    public Object get(final String name) {
        // Recherche récursive par défaut
        return get(name, true);
    }

    /**
     * Renvoie la valeur d'une donnée.
     *
     * @param name
     *            nom de la donnée
     * @param recursif
     *            (true si recherche récursive)
     *
     * @return valeur de l'objet
     */
    public Object get(final String name, final boolean recursif) {
        final Data data = datas.get(name);
        Object value = null;
        if (data == null) {
            if (recursif && infoBeanPere != null) {
                value = infoBeanPere.get(name, true);
            }
        } else {
            value = data.getValue();
        }
        return value;
    }

    public <T> T get(final String name, Class<T> clazz) {
        final Object data = get(name, true);
        if (clazz.isInstance(data)) {
            return clazz.cast(data);
        }
        return null;
    }

    /**
     * Renvoie la valeur d'une donnée sous forme de Set (recherche récursive)
     * @param name nom de la donnée
     * @return Set : valeur de l'objet
     */
    @SuppressWarnings("unchecked")
    public <T> Set<T> getHashSet(String name) {
        return (HashSet<T>) get(name);
    }

    /**
     * Renvoie la valeur d'une donnée sous forme de Collection (recherche récursive)
     * @param name nom de la donnée
     * @return Collection : valeur de l'objet
     */
    @SuppressWarnings("unchecked")
    public <T> Collection<T> getCollection(String name) {
        return (Collection<T>) get(name);
    }

    /**
     * Renvoie la valeur d'une donnée sous forme de List (recherche récursive)
     * @param name nom de la donnée
     * @return List : valeur de l'objet
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getList(String name) {
        return (List<T>) get(name);
    }

    /**
     * Renvoie la valeur d'une donnée sous forme de Map (recherche récursive)
     * @param name nom de la donnée
     * @return Map : valeur de l'objet
     */
    @SuppressWarnings("unchecked")
    public <T1, T2> Map<T1, T2> getMap(String name) {
        return (Map<T1, T2>) get(name);
    }

    /**
     * Retourne l'action utilisateur (ex : ACTION_VALIDER).
     *
     * @return String action (cf. variables statiques ACTION_xxx)
     */
    public String getActionUtilisateur() {
        return (String) get(ACTION);
    }

    /**
     * Retourne l'écran logique renvoyé par le processus).
     *
     * @return String ecran
     */
    public String getEcranLogique() {
        return (String) get(ECRAN_LOGIQUE);
    }

    /**
     * Retourne l'écran de redirection renvoyé par le processus).
     *
     * @return String ecran
     */
    public String getEcranRedirection() {
        return (String) get(ECRAN_REDIRECTION);
    }

    /**
     * Retourne le nom de l'écran physique ( ex : PT23E1 ).
     *
     * @return String ecranPhysique
     */
    public String getEcranPhysique() {
        return (String) get(ECRAN_PHYSIQUE);
    }

    /**
     * Retourne le nom de l'écran conteneur. Cet écran sert à inclure l'écran physique à appeler
     *
     * @return String l'écran conteneur
     */
    public String getEcranConteneur() {
        return (String) get(ECRAN_CONTENEUR);
    }

    /**
     * Retourne l'identifiant du composant lié au processus courant
     *
     * @return String l'id du composant courant
     */
    public String getIdComposant() {
        return (String) get(ID_COMPOSANT);
    }

    /**
     * Retourne l'état de l'objet prininfoBeanpal du processus.
     *
     * @return String ecranPhysique
     */
    public String getEtatObjet() {
        return (String) get(ETAT_OBJET);
    }

    /**
     * Renvoie l'identifiant unique d'une page.
     *
     * @return String
     */
    public String getIDJSP() {
        return (String) get(ID_JSP);
    }

    /**
     * Renvoie l'identifiant unique d'une procédure.
     *
     * @return String
     */
    public String getBEANPU() {
        return (String) get(BEANPU);
    }

    /**
     * Renvoie la valeur d'une donnée (recherche récursive).
     *
     * @param name
     *            nom de la donnée
     *
     * @return valeur de l'objet
     */
    public int getInt(final String name) {
        // Recherche récursive par défaut
        return (Integer) get(name, true);
    }

    /**
     * Renvoie la valeur d'une donnée (recherche récursive).
     *
     * @param name
     *            nom de la donnée
     *
     * @return valeur de l'objet
     */
    public Long getLong(final String name) {
        // Recherche récursive par défaut
        return (Long) get(name, true);
    }

    /**
     * Renvoie la valeur d'une donnée (recherche récursive).
     *
     * @param name
     *            nom de la donnée
     *
     * @return valeur de l'objet
     */
    public Integer getInteger(final String name) {
        // Recherche récursive par défaut
        return Integer.valueOf((String) get(name, true));
    }

    /**
     * Renvoie le message d'erreur.
     *
     * @return String
     */
    public String getMessageErreur() {
        return getMessage(TypeMessage.ERREUR.getCode());
    }

    /**
     * Renvoie le message de confirmation.
     *
     * @return String
     */
    public String getMessageConfirmation() {
        return getMessage(TypeMessage.CONFIRMATION.getCode());
    }

    /**
     * Renvoie le message d'alerte.
     *
     * @return String
     */
    public String getMessageAlerte() {
        return getMessage(TypeMessage.ALERTE.getCode());
    }

    private String getMessage(final String code) {
        String res = "";
        if (messages.get(code) != null) {
            for (final String message : messages.get(code)) {
                res += (res.length() > 0 ? ", " : "") + message;
            }
        } else if (get(code) != null) {
            res = (String) get(code);
        }
        return res;
    }

    public Map<String, List<String>> getMessages() {
        return messages;
    }

    public void setMessages(final Map<String, List<String>> messages) {
        this.messages = messages;
    }

    /**
     * Permet de faire remonter les messages de confirmation / alerte / erreur dans l'infoBean parent
     *
     */
    public void setMessagesDansInfoBeanPere() {
        if (infoBeanPere != null && MapUtils.isNotEmpty(messages)) {
            infoBeanPere.setMessages(messages);
        }
    }

    /**
     * Gets the mode enchainement.
     *
     * @return the mode enchainement
     */
    public int getModeEnchainement() {
        return getInt(MODE_ENCHAINEMENT);
    }

    /**
     * Renvoie le nom du processus actif.
     *
     * @return String
     */
    public String getNomProcessus() {
        return (String) get(PROC);
    }

    /**
     * Renvoie le nom du processus actif.
     *
     * @return String
     */
    public String getNomExtension() {
        return (String) get(EXT);
    }

    /**
     * Retourne l'écran logique renvoyé par le processus).
     *
     * @return String ecran
     */
    public UploadedFile getUploadedFile() {
        return ((UploadedFile[]) get(UPLOADED_FILE))[0];
    }

    /**
     * Retourne l'écran logique renvoyé par le processus).
     *
     * @return String ecran
     */
    public UploadedFile[] getUploadedFiles() {
        return (UploadedFile[]) get(UPLOADED_FILE);
    }

    /*
     * Stockage d'une donné saisie mais non contrôlée
     * (utile pour repositionner la zone quand on revient sur l(écran)
     */

    /**
     * Checks if is controled.
     *
     * @param name
     *            the name
     *
     * @return true, if is controled
     */
    public boolean isControled(final String name) {
        final Data data = datas.get(name);
        boolean controled = false;
        if (data == null) {
            if (infoBeanPere != null) {
                controled = infoBeanPere.isControled(name);
            }
        } else {
            controled = data.isControled();
        }
        return controled;
    }

    /**
     * Checks if is required.
     *
     * @param name
     *            the name
     *
     * @return true, if is required
     */
    public boolean isRequired(final String name) {
        final Data data = datas.get(name);
        boolean required = false;
        if (data != null) {
            required = data.isRequired();
        }
        return required;
    }

    /**
     * Removes the.
     *
     * @param name
     *            the name
     */
    public void remove(final String name) {
        if (name != null) {
            datas.remove(name);
        }
    }

    /**
     * Removes the.
     *
     * @param code
     *            le code du message à supprimer
     */
    public void removeMessage(final String code) {
        if (code != null) {
            messages.remove(code);
        }
    }

    /**
     * Require.
     *
     * @param name
     *            the name
     */
    public void require(final String name) {
        Data data = datas.get(name);
        if (data == null) {
            //donnée inexistante
            data = new Data(name, null, true);
            datas.put(name, data);
        } else {
            //donnée existante
            data.setRequired(true);
        }
    }

    /**
     * Sets the.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     */
    public void set(final String name, final Object value) {
        final Data data = datas.get(name);
        if (data == null) {
            datas.put(name, new Data(name, value, false));
        } else {
            data.setValue(value);
            data.setRequired(false);
            data.setControled(true);
        }
    }

    private void addMessage(final String name, final String value) {
        if (messages.get(name) == null) {
            messages.put(name, new ArrayList<String>());
            // on recopie le message existant de data vers message
            if (get(name) != null) {
                messages.get(name).add((String) get(name));
            }
        }
        if (!messages.get(name).contains(value)) {
            messages.get(name).add(value);
        }
    }

    /**
     * Positionnement action utilisateur.
     *
     * @param newAction
     *            String : cf . variable statiques ACTION_xxx pour valeurs prédéfinies
     */
    public void setActionUtilisateur(final String newAction) {
        set(ACTION, newAction);
    }

    /*
     * Fixe une valeur de donnée et demande sa nouvelle valeur (confirmation)
     *
     * @param name
     *            the name
     * @param value
     *            the value
     */
    public void setAndRequire(final String name, final Object value) {
        final Data data = datas.get(name);
        if (data == null) {
            datas.put(name, new Data(name, value, true));
        } else {
            data.setValue(value);
            data.setRequired(true);
            data.setControled(true);
        }
    }

    /**
     * Sets the donnee saisie.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     */
    public void setDonneeSaisie(final String name, final Object value) {
        final Data data = datas.get(name);
        if (data == null) {
            datas.put(name, new Data(name, value, false, false));
        } else {
            data.setValue(value);
            data.setRequired(false);
            data.setControled(false);
        }
    }

    /**
     * Positionnement de l'écran logique (appelé par le processus).
     *
     * @param newEcranLogique
     *            String
     */
    public void setEcranLogique(final String newEcranLogique) {
        set(ECRAN_LOGIQUE, newEcranLogique);
    }

    /**
     * Positionnement de l'écran de redirection (appelé par le processus).
     *
     * @param newRedirection
     *            l'url de redirection
     */
    public void setEcranRedirection(final String newRedirection) {
        set(ECRAN_REDIRECTION, newRedirection);
    }

    /**
     * Positionnement de l'écran logique de fin de procédure.
     */
    public void setEcranLogiqueFin() {
        setEcranLogique(ECRAN_LOGIQUE_FIN);
    }

    /**
     * Positionnement de l'écran physique Ne doit pas etre ecrasé par l'applicatif !!!.
     *
     * @param newEcranPhysique
     *            String
     */
    public void setEcranPhysique(final String newEcranPhysique) {
        set(ECRAN_PHYSIQUE, newEcranPhysique);
    }

    /**
     * Positionnement de l'écran conteneur qui inclura l'écran physique
     *
     * @param newEcranConteneur
     *            String
     */
    public void setEcranConteneur(final String newEcranConteneur) {
        set(ECRAN_CONTENEUR, newEcranConteneur);
    }

    /**
     * Positionnement de l'écran conteneur qui inclura l'écran physique
     *
     * @param idComposant
     *            String
     */
    public void setIdComposant(final String idComposant) {
        set(ID_COMPOSANT, idComposant);
    }

    /**
     * Positionnement de l'état de l'objet prininfoBeanpal Ne doit pas etre ecrasé par l'applicatif !!!.
     *
     * @param newEtat
     *            the new etat
     */
    public void setEtatObjet(final String newEtat) {
        set(ETAT_OBJET, newEtat);
    }

    /**
     * Positionnement de l'identifiant unique de JSP.
     *
     * @param newIDJSP
     *            String
     */
    public void setIDJSP(final String newIDJSP) {
        set(ID_JSP, newIDJSP);
    }

    /**
     * Positionnement de l'identifiant unique de la procédure.
     *
     * @param newIDBEAN
     *            String
     */
    public void setBEANPU(final String newIDBEAN) {
        set(BEANPU, newIDBEAN);
    }

    /**
     * Sets the int.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     */
    public void setInt(final String name, final int value) {
        set(name, value);
    }

    /**
     * @deprecated méthode à ne plus utiliser, ce n'est plus qu'un alias pour
     * @see #addMessageErreur
     *
     */
    @Deprecated
    public void setMessageErreur(final String newMsg) {
        addMessageErreur(newMsg);
    }

    /**
     * Positionnement du message d'erreur à afficher.
     *
     * @param newMsg
     *            String
     */
    public void addMessageErreur(final String newMsg) {
        addMessage(TypeMessage.ERREUR.getCode(), newMsg);
    }

    /**
     * Positionnement du message de confirmation à afficher.
     *
     * @param newMsg
     *            String
     */
    public void addMessageConfirmation(final String newMsg) {
        addMessage(TypeMessage.CONFIRMATION.getCode(), newMsg);
    }

    /**
     * Positionnement du message d'alerte à afficher.
     *
     * @param newMsg
     *            String
     */
    public void addMessageAlerte(final String newMsg) {
        addMessage(TypeMessage.ALERTE.getCode(), newMsg);
    }

    /**
     * Sets the mode enchainement.
     *
     * @param mode
     *            the new mode enchainement
     */
    public void setModeEnchainement(final int mode) {
        setInt(MODE_ENCHAINEMENT, mode);
    }

    /**
     * Positionnement du processus actif (appelé par le GP).
     *
     * @param processus
     *            String
     */
    public void setNomProcessus(final String processus) {
        set(PROC, processus);
    }

    /**
     * Positionnement de l'extension courante (appelé par le GP).
     *
     * @param processus
     *            String
     */
    public void setNomExtension(final String processus) {
        set(EXT, processus);
    }

    /**
     * Positionnement de l'écran logique (appelé par le processus).
     *
     * @param _file
     *            the _file
     */
    public void setUploadedFile(final UploadedFile _file) {
        final UploadedFile files[] = new UploadedFile[1];
        files[0] = _file;
        set(UPLOADED_FILE, files);
    }

    /**
     * Positionnement de l'écran logique (appelé par le processus).
     *
     * @param file
     *            the file
     */
    public void setUploadedFiles(final UploadedFile file[]) {
        set(UPLOADED_FILE, file);
    }
    // JSS 20051029 : gestion des listes paginées

    /**
     * Liste l'ensemble des informations du CUI.
     *
     * @return String
     */
    @Override
    public String toString() {
        String chaine = "";
        for (final Data data : datas.values()) {
            if (chaine.length() < 500) {
                chaine = chaine + "\t\t" + data.toString() + "\r\n";
            }
        }
        if (chaine.length() >= 500) {
            chaine = chaine + "\t\t(...)\r\n";
        }
        return chaine;
    }

    /**
     * Gets the datas.
     *
     * @return the datas
     */
    protected Map<String, Data> getDatas() {
        return datas;
    }

    /**
     * Gets the datas map <key,value>
     *
     * @return the datas value
     */
    public Map<String, Object> getValues() {
        final Map<String, Object> res = new HashMap<>();
        for (final String key : datas.keySet()) {
            res.put(key, datas.get(key).value);
        }
        return res;
    }

    /**
     * set datas map <key,value>
     */
    public void setValues(final Map<String, Object> map) {
        for (final String key : map.keySet()) {
            set(key, map.get(key));
        }
    }

    /**
     * Gets the data keys.
     *
     * @return the data keys
     */
    public Collection<String> getDataKeys() {
        return getDatas().keySet();
    }

    /**
     * Sets the datas.
     *
     * @param _datas
     *            the new datas
     */
    protected void setDatas(final Map<String, Data> _datas) {
        datas = _datas;
    }

    /**
     * Renvoie la valeur de l'onglet sélectionné (à "" si pas d'onglet).
     *
     * @return String
     */
    public String getNouvelOnglet() {
        String res = (String) get(NOM_NOUVEL_ONGLET);
        if (res == null) {
            res = "";
        }
        return res;
    }

    /**
     * Renvoie la valeur de l'onglet actif (à "" si pas d'onglet).
     *
     * @return String
     */
    public String getOnglet() {
        String res = (String) get(NOM_ONGLET);
        if (res == null) {
            res = "";
        }
        return res;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (08/01/2002 16:51:12)
     *
     * @return HttpSession
     */
    public HttpSession getSessionHttp() {
        return sessionHttp;
    }

    /**
     * Renvoie la valeur d'une donnée (recherche récursive).
     *
     * @param name
     *            nom de la donnée
     *
     * @return valeur de l'objet
     */
    public String getString(final String name) {
        return (String) get(name);
    }

    /**
     * Renvoie la valeur d'une donnée (recherche récursive).
     *
     * @param name
     *            nom de la donnée
     *
     * @return valeur de l'objet
     */
    public Date getDate(final String name) {
        return (Date) get(name);
    }

    /**
     * Stockage d'une donnée.
     *
     * @param name
     *            the name
     * @param data
     *            the data
     */
    protected void setData(final String name, final Data data) {
        datas.put(name, data);
    }

    /**
     * Positionnement d'un onglet.
     *
     * @param _nomOnglet
     *            the _nom onglet
     */
    public void setOnglet(final String _nomOnglet) {
        set(NOM_ONGLET, _nomOnglet);
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (08/01/2002 16:51:12)
     *
     * @param newSessionHttp
     *            HttpSession
     */
    protected void setSessionHttp(final HttpSession newSessionHttp) {
        sessionHttp = newSessionHttp;
    }

    public String getTitreEcran() {
        return (String) get(TITRE_ECRAN);
    }

    public void setTitreEcran(final String titre) {
        set(TITRE_ECRAN, titre);
    }

    /**
     * Purger fichiers.
     *
     * @param ksession
     *            the ksession
     * @param typeListe
     *            the type liste
     *
     * @throws Exception
     *             the exception
     */
    public synchronized void purgerFichiers(final String ksession, final String typeListe) {
        //purge des 5 derniers pour l'utilisateur et le type courant
        final File folder = new File(WebAppUtil.getWorkDefaultPath());
        FilenameFilter filtre = new InfoBeanFilter(ksession, typeListe);
        File[] listeFiles = folder.listFiles(filtre);
        if (listeFiles.length > 5) {
            // Tri alpha
            final TreeSet<File> listeTriee = new TreeSet<>(Arrays.asList(listeFiles));
            //suppression premier élément (+ancien)
            listeTriee.first().delete();
        }
        //Toutes les 10 mn, Purge globale des fichiers de plus d'une heure
        final long tsCourant = System.currentTimeMillis();
        if (tsCourant - tsLastPurge > DELAI_PURGE) {
            tsLastPurge = tsCourant;
            LOG.debug("Purge des fichiers de sessions");
            filtre = new InfoBeanFilter("", "");
            listeFiles = folder.listFiles(filtre);
            for (final File listeFile : listeFiles) {
                final long tsFile = listeFile.lastModified();
                if (tsFile != 0 && (tsCourant - tsFile > DELAI_EXPIRATION)) {
                    listeFile.delete();
                }
            }
        }
    }

    /**
     * Sauvegarde du contenu de l'infoBean dans un fichier.
     *
     * @param request
     *            the request
     * @param typeListe
     *            - permet de gérer l'archivage pour chaque type de fiche (suppression automatique des + anciennes sauvegardes)
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public String sauvegarderDansFichier(final HttpServletRequest request, final String typeListe) throws IOException, ClassNotFoundException {
        // Récupération du code de l'utilisateur
        final HttpSession _session = request.getSession(false);
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) _session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
        final String ksession = (String) infosSession.get(SessionUtilisateur.KSESSION);
        //sauvegarde datas
        final String idFichier = typeListe + "_" + System.currentTimeMillis();
        final String pathFichier = WebAppUtil.getWorkDefaultPath() + File.separator + "datas_" + ksession + "_" + idFichier + ".tmp";
        final FileOutputStream fos = new FileOutputStream(pathFichier);
        final ObjectOutputStream output = new ObjectOutputStream(fos);
        output.writeObject(this);
        output.close();
        fos.close();
        purgerFichiers(ksession, typeListe);
        return idFichier;
    }

    public List<String> getMessages(final String code) {
        if (messages.containsKey(code)) {
            return messages.get(code);
        }
        return new ArrayList<>();
    }
}
