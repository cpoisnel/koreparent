package com.jsbsoft.jtf.core;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.kportal.core.config.Langue;
import com.kportal.core.config.LangueConfig;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.MessageLoader;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

/**
 * The Class LangueUtil.
 */
public class LangueUtil {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(LangueUtil.class);

    /**
     * Instantiates a new langue util.
     */
    public LangueUtil() {
        super();
    }

    /**
     * Construit un libellé multi-langue par concaténation des libellés de chaque langue.
     *
     * @param libelleLangue
     *            the libelle langue
     * @return the string
     */
    public static String construireLibelleMultiLangue(final String[] libelleLangue) {
        String libelle = "";
        for (int i = 0; i < getNbLangues(); i++) {
            if (i < libelleLangue.length) {
                libelle += libelleLangue[i] + ";";
            } else {
                libelle += ";";
            }
        }
        return libelle;
    }

    /**
     * Décompose un libellé multi-langue en n libellés.
     *
     * @param libelle
     *            the libelle
     * @return the string[]
     */
    public static String[] decomposerLibelleMultiLangue(final String libelle) {
        final String libelleLangue[] = new String[getNbLangues()];
        int indice = 0;
        for (int i = 0; i < getNbLangues(); i++) {
            final int nextSeparator = libelle.indexOf(";", indice);
            if (nextSeparator != -1) {
                libelleLangue[i] = libelle.substring(indice, nextSeparator);
                // repositionnement sur le champ suivant
                indice = nextSeparator + 1;
            } else {
                libelleLangue[i] = "";
            }
        }
        return libelleLangue;
    }

    /**
     * Récupère le libellé dans la langue par défaut.
     *
     * @param libelle
     *            the libelle
     * @return the string
     */
    public static String extraireDefaultLibelleMultiLangue(final String libelle) {
        return extraireLibelleMultiLangue(libelle, Locale.FRANCE);
    }

    /**
     * Récupère le libellé dans une langue.
     *
     * @param libelle
     *            the libelle
     * @param locale
     *            the locale
     * @return the string
     */
    public static String extraireLibelleMultiLangue(final String libelle, final Locale locale) {
        final String libelles[] = decomposerLibelleMultiLangue(libelle);
        final int indice = getIndiceLocale(locale);
        return libelles[indice];
    }

    /**
     * Renvoie le label associé à une Langue (francais, anglais, allemand) ........
     *
     * @param indiceLocale
     *            l'indice de la langue
     * @return the display name
     */
    public static String getDisplayName(final String indiceLocale) {
        final int indice = getIndiceLocale(indiceLocale);
        if (indice != -1) {
            return getDisplayName(indice);
        }
        return "";
    }

    /**
     * Renvoie le label associé à une Langue (francais, anglais, allemand) ........
     *
     * @param indiceLocale
     *            l'indice de la langue
     * @return the display name
     */
    public static String getDisplayName(final int indiceLocale) {
        ContexteUniv ctx = ContexteUtil.getContexteUniv();
        String name;
        if (ctx != null) {
            name = getLocale(indiceLocale).getDisplayLanguage(ctx.getLocale());
        } else {
            name = getLocale(indiceLocale).getDisplayLanguage(getDefaultLocale());
        }
        return name;
    }

    /**
     * Renvoie les labels associés aux différentes langues (francais, anglais, allemenand) ........
     *
     * @return the display names
     */
    public static String[] getDisplayNames() {
        final String labels[] = new String[getNbLangues()];
        int i = 0;
        for (final Langue langue : getLangues().values()) {
            labels[i] = langue.getLocale().getDisplayLanguage(getDefaultLocale());
            i++;
        }
        return labels;
    }

    /**
     * Récupère le libellé dans une langue.
     *
     * @param locale
     *            the locale
     * @return the indice locale
     */
    public static int getIndiceLocale(final Locale locale) {
        int indice = -1;
        for (final Langue langue : getLangues().values()) {
            if (langue.getLocale().equals(locale)) {
                indice = langue.getIndice();
            }
        }
        return indice;
    }

    /**
     * Récupère le libellé dans la langue par défaut.
     *
     * @return the default locale
     */
    public static Locale getDefaultLocale() {
        return getLocale(0);
    }

    public static String getIndiceLocaleDefaut() {
        return String.valueOf(getIndiceLocale(getDefaultLocale()));
    }

    /**
     * Déduit la langue à partir du code Langue ISO.
     *
     * @param codeISO
     *            the code iso
     * @return the indice from code iso
     */
    public static int getIndiceFromCodeISO(final String codeISO) {
        int indiceLocale = -1;
        for (final Langue langue : getLangues().values()) {
            if (langue.getLocale().getLanguage().equalsIgnoreCase(codeISO)) {
                indiceLocale = langue.getIndice();
            }
        }
        return indiceLocale;
    }

    /**
     * Récupère l'indice sous forme de string.
     *
     * @param locale
     *            the locale
     * @return the langue locale
     */
    public static String getLangueLocale(final Locale locale) {
        return Integer.toString(getIndiceLocale(locale));
    }

    /**
     * Renvoie la liste des langues en francais (francais, anglais, allemand) ........
     *
     * @return the liste langues
     * @deprecated ça prend la locale FR en dur. Utiliser {@link LangueUtil#getListeLangues(Locale)}
     */
    @Deprecated
    public static Hashtable<String, String> getListeLangues() {
        return getListeLangues(Locale.FRANCE);
    }

    /**
     * Renvoie la liste des langues (francais, anglais, allemenand) ........
     *
     * @param locale
     *            the locale
     * @return the liste langues
     */
    public static Hashtable<String, String> getListeLangues(final Locale locale) {
        final Hashtable<String, String> h = new Hashtable<>();
        int i = 0;
        for (final Langue langue : getLangues().values()) {
            String libelle = langue.getLocale().getDisplayName(locale);
            final int indice = libelle.indexOf("(");
            if (indice != -1) {
                libelle = libelle.substring(0, indice);
            }
            h.put("" + i, libelle);
            i++;
        }
        return h;
    }

    /**
     * Récupère la valeur Locale associée à l'indice.
     *
     * @param indice
     *            the indice
     * @return the locale
     */
    public static Locale getLocale(final int indice) {
        if (getLangues().get(indice) != null) {
            return getLangues().get(indice).getLocale();
        }
        return null;
    }

    /**
     * Récupère le path vers l'image du drapeau de la langue.
     *
     * @param indiceLocale
     *            l'id kportal de la locale
     * @return l'url de l'image en chemin relatif
     */
    public static String getPathImageDrapeau(final String indiceLocale) {
        final int indice = getIndiceLocale(indiceLocale);
        if (indice != -1) {
            return getLangues().get(indice).getUrl() + "/" + LangueConfig.DEFAULT_IMAGE_DRAPEAU;
        }
        return "";
    }

    /**
     * Récupère le path vers l'image du drapeau de la langue.
     *
     * @param locale
     *            the locale
     * @return the url locale
     */
    public static String getPathImageDrapeau(final Locale locale) {
        final int indice = getIndiceLocale(locale);
        if (indice != -1) {
            return getLangues().get(indice).getUrl() + "/" + LangueConfig.DEFAULT_IMAGE_DRAPEAU;
        }
        return "";
    }

    /**
     * Gets the locale.
     *
     * @param indiceLocale
     *            l'id kportal de la locale
     * @return the locale
     */
    public static Locale getLocale(final String indiceLocale) {
        Locale locale = getLocale(getIndiceLocale(indiceLocale));
        if (locale == null) {
            locale = getDefaultLocale();
        }
        return locale;
    }

    /**
     * Gets the message.
     *
     * @param indiceLocale
     *            l'id kportal de la locale
     * @param key
     *            the key
     * @return the message
     * @deprecated utiliser @{@link MessageHelper#getCoreMessage(Locale, String)}
     */
    @Deprecated
    public static String getMessage(final String indiceLocale, final String key) {
        return getMessage(getLocale(indiceLocale), key);
    }

    /**
     * Gets the message.
     *
     * @param locale
     *            the locale
     * @param key
     *            the key
     * @return the message
     * @deprecated utiliser @{@link MessageHelper#getCoreMessage(Locale, String)} à la place
     */
    @Deprecated
    public static String getMessage(Locale locale, final String key) {
        if (locale == null || !isActiveLocale(locale)) {
            locale = getDefaultLocale();
        }
        String res = MessageLoader.getInstance().getProperties(getLanguageCountry(locale)).getProperty(key);
        // on ne met pas de warning pour les clés qui possède un point car il s'agit d'un libellé de de site : aliassite.ST_MESSAGE
        if (res == null && !key.contains(".")) {
            LOG.warn("Chaine " + key + " absente des messages " + locale.getCountry());
            if (!locale.getCountry().equals(getDefaultLocale().getCountry())) {
                res = getMessage(getDefaultLocale(), key);
            }
        }
        if (StringUtils.isEmpty(res)) {
            res = StringUtils.EMPTY;
        }
        return res;
    }

    /**
     * Checks if is active.
     *
     * @param locale
     *            l'id kportal de la locale
     * @return true, if is not active
     */
    public static boolean isActiveLocale(final Locale locale) {
        return getIndiceLocale(locale) != -1;
    }

    /**
     * Checks if indice.
     *
     * @param indiceLocale
     *            l'id kportal de la locale
     * @return true, if is not active
     */
    private static int getIndiceLocale(final String indiceLocale) {
        if (StringUtils.isNotBlank(indiceLocale) && StringUtils.isNumeric(indiceLocale)) {
            final int res = Integer.parseInt(indiceLocale);
            if (getLangues().get(res) != null) {
                return res;
            }
        }
        return -1;
    }

    /**
     * Gets the message.
     *
     * @param ctx
     *            the ctx
     * @param key
     *            the key
     * @return the message
     * @deprecated utiliser @{@link MessageHelper#getCoreMessage(String)} à la place
     */
    @Deprecated
    public static String getMessage(final OMContext ctx, final String key) {
        String res = getMessage(ctx.getLocale(), key);
        if (StringUtils.isEmpty(res)) {
            res = getMessage(getDefaultLocale(), key);
        }
        return res;
    }

    /**
     * Gets the language country.
     *
     * @param locale
     *            the locale
     * @return the language country
     */
    public static String getLanguageCountry(final Locale locale) {
        String res = locale.getLanguage();
        if (StringUtils.isNotEmpty(locale.getCountry())) {
            res += "_" + locale.getCountry();
        }
        return res;
    }

    /**
     * Gets the langue config.
     *
     * @return the langue config
     */
    public static LangueConfig getLangueConfig() {
        return (LangueConfig) ApplicationContextManager.getCoreContextBean(LangueConfig.ID_BEAN);
    }

    /**
     * Gets the nb langues.
     *
     * @return the nb langues
     */
    public static int getNbLangues() {
        return getLangueConfig().getNbLangues();
    }

    public static Map<Integer, Langue> getLangues() {
        return getLangueConfig().getLangues();
    }

    /**
     * @deprecated Cette méthode n'a plus de sens. la locale du backoffice dépend maintenant de la locale du navigateur de l'utilisateur.
     * Dans le cas ou elle n'est pas géré par l'application, on prend la locale par défaut.
     * @return la locale du backoffice configuré dans les properties
     */
    @Deprecated
    public static Locale getLocaleBO() {
        return getLocale(getLangueConfig().getIndiceLangueBo());
    }

    /**
     * calcule la liste complète des locales défini sur l'application.
     *
     * @return toutes les locales géré par l'application.
     */
    public static List<Locale> getLocales() {
        final List<Locale> list = new ArrayList<>();
        for (final Langue langue : getLangues().values()) {
            list.add(langue.getLocale());
        }
        return list;
    }

    /**
     * Permet de retourner une locale configurer dans l'application par rapport à la locale fourni en paramètre. Si la locale fourni est supporté par l'application, on retourne
     * cette dernière, sinon si uniquement la langue est supporté on retourne cette locale (ex : EN_US est supporté par l'appli, le paramètre est EN_GB, on retourne EN_GB).
     * Enfin si la langue n'est pas supporté, on retourne la locale par défaut de l'application.
     * @param locale la locale à tester
     * @return une locale supporté par l'application.
     */
    public static Locale getEffectiveLocale(final Locale locale) {
        Locale currentLocale = locale;
        final List<Locale> allLocales = getLocales();
        if (!allLocales.contains(locale)) {
            Boolean hasLocaleByLanguage = Boolean.FALSE;
            int i = 0;
            while (!hasLocaleByLanguage && i < allLocales.size()) {
                final Locale appDefineLocale = allLocales.get(i);
                if (appDefineLocale.getLanguage().equals(locale.getLanguage())) {
                    currentLocale = appDefineLocale;
                    hasLocaleByLanguage = Boolean.TRUE;
                }
                i++;
            }
            if (!hasLocaleByLanguage) {
                currentLocale = getDefaultLocale();
            }
        }
        return currentLocale;
    }
}
