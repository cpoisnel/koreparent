package com.jsbsoft.jtf.identification;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.security.MySQLHelper;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceUser;

/**
 * Prend en charge l'authentification sur la BD K-Portal. C'est le mode d'authentification par défaut.
 *
 * @author jbiard
 */
public class SourceAuthDefautKbd implements ISourceAuth {

    /** The Constant SOURCE_LIBELLE_KBD. */
    public final static String SOURCE_LIBELLE_KBD = "kbd";

    /** The Constant SOURCE_LIBELLE_AFFICHABLE. */
    public final static String SOURCE_LIBELLE_AFFICHABLE = "Base de données K-Portal";

    private final ServiceUser serviceUser;

    public SourceAuthDefautKbd() {
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.identification.ISourceAuth#connecte(java.lang.String, java.lang.String, com.jsbsoft.jtf.database.OMContext)
     */
    @Override
    public boolean connecte(String codeUtilisateur, String mdp, OMContext ctx) throws Exception {
        //le mot de passe ne peut pas être vide
        if (mdp.trim().length() == 0) {
            return false;
        }
        final String passwordEncode = MySQLHelper.encodePassword(mdp);
        return serviceUser.getByCodeAndPass(codeUtilisateur, passwordEncode) != null;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.identification.ISourceAuth#deconnecte(java.lang.String, com.jsbsoft.jtf.database.OMContext)
     */
    @Override
    public void deconnecte(String codeUtilisateur, OMContext ctx) {}

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.identification.ISourceAuth#getCodeSource()
     */
    @Override
    public String getCodeSource() {
        return SOURCE_LIBELLE_KBD;
    }

    /**
     * Gets the libelle source.
     *
     * @return the libelle source
     *
     * @see ISourceAuth
     */
    @Override
    public String getLibelleSource() {
        return SOURCE_LIBELLE_AFFICHABLE;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.identification.ISourceAuth#parametrageCharge()
     */
    @Override
    public String parametrageCharge() {
        return "";
    }
}
