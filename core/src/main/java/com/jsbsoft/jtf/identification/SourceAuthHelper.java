package com.jsbsoft.jtf.identification;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.core.config.PropertyHelper;

public class SourceAuthHelper {

    public static final String KUSER_CONNECTOR = "KUSER_CONNECTOR";

    private static final String PROP_AUTHENTIFICATION_KUSER_CONNECTOR = "authentification.kuser_connector";

    public static boolean isDoubleSourceAuthActif(HttpServletRequest request, InfoBean infoBean) {
        // activation de l'option
        if (!"1".equals(PropertyHelper.getCoreProperty(PROP_AUTHENTIFICATION_KUSER_CONNECTOR))) {
            return false;
        }
        // récupération du cookie KUSER_CONNECTOR
        boolean cookieKUserConnector = false;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                cookieKUserConnector |= KUSER_CONNECTOR.equals(cookie.getName()) && "ok".equals(cookie.getValue());
            }
        }
        // OU presence du parametre dans l'url du processus d'identification
        return "1".equals(infoBean.getString(KUSER_CONNECTOR)) || cookieKUserConnector;
    }
}
