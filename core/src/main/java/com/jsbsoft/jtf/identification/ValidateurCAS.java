package com.jsbsoft.jtf.identification;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;

import edu.yale.its.tp.cas.client.ProxyTicketValidator;
import edu.yale.its.tp.cas.client.ServiceTicketValidator;

/**
 * Prend en charge la validation de ticket aupres de CAS.
 *
 * @author jbiard
 */
public class ValidateurCAS implements ISourceAuth {

    /** The Constant SOURCE_LIBELLE_CAS. */
    public static final String SOURCE_LIBELLE_CAS = "cas";

    /** The Constant SOURCE_LIBELLE_AFFICHABLE. */
    public static final String SOURCE_LIBELLE_AFFICHABLE = "Central Authentication Server (CAS)";
    // JB 20050601 libelle de la source d'auth

    /** The Constant PARAM_JTF_CAS. */
    public static final String PARAM_JTF_CAS = "cas.mode";

    /** The Constant LIBELLE_JTF_CAS_MODE_BASIC. */
    public static final String LIBELLE_JTF_CAS_MODE_BASIC = "basic";
    // parametre pour le mode de cas

    /** The Constant LIBELLE_JTF_CAS_MODE_PROXY. */
    public static final String LIBELLE_JTF_CAS_MODE_PROXY = "proxy";
    // valeurs pour le parametre

    /** The Constant PARAM_JTF_CAS_HOTE. */
    public static final String PARAM_JTF_CAS_HOTE = "cas.hote";

    /** The Constant PARAM_JTF_CAS_URI_LOGIN. */
    public static final String PARAM_JTF_CAS_URI_LOGIN = "cas.uriLogin";

    /** The Constant PARAM_JTF_CAS_URI_LOGOUT. */
    public static final String PARAM_JTF_CAS_URI_LOGOUT = "cas.uriLogout";

    /** The Constant PARAM_JTF_CAS_URI_SERVICE. */
    public static final String PARAM_JTF_CAS_URI_SERVICE = "cas.uriCasServiceValidate";

    /** The Constant PARAM_JTF_CAS_URI_PROXY_VALIDATE. */
    public static final String PARAM_JTF_CAS_URI_PROXY_VALIDATE = "cas.uriCasProxyValidate";

    /** The Constant PARAM_JTF_CAS_URI_PROXY_RECEPTOR. */
    public static final String PARAM_JTF_CAS_URI_PROXY_RECEPTOR = "cas.uriCasProxyServlet";

    private static final Logger LOG = LoggerFactory.getLogger(ValidateurCAS.class);

    private static final String HTTPS = "https://";

    private static final String PARAM_JTF_CAS_REDIRECTION_FRONT = "cas.redirection_front";

    /** The url service cas login. */
    private String hoteCas, modeCas, urlCasServiceValidate, urlProxyValidate, urlServiceCasLogin;

    /** The port cas. */
    private int portCas;

    /** The map login pgt. */
    private Map<String, String> mapLoginPGT;

    /**
     * Constructeur non visible en dehors du package.
     *
     */
    ValidateurCAS() {
        initialiseParametrageCas();
    }

    /**
     * Gets the hote cas.
     *
     * @return the hote cas
     */
    public String getHoteCas() {
        return hoteCas;
    }

    /**
     * Gets the port serveur cas.
     *
     * @return the port serveur cas
     */
    public int getPortServeurCas() {
        return portCas;
    }

    /**
     * Gets the mode cas.
     *
     * @return the mode cas
     */
    public String getModeCas() {
        return modeCas;
    }

    /**
     * Gets the url cas service login.
     *
     * @param urlService
     *            the url service
     *
     * @return the url cas service login
     *
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     */
    public String getUrlCasServiceLogin(String urlService) throws UnsupportedEncodingException {
        return urlServiceCasLogin + URLEncoder.encode(urlService, CharEncoding.DEFAULT);
    }

    /**
     * Gets the url cas login.
     *
     * @param ctx
     *            the ctx
     *
     * @return the url cas login
     */
    public String getUrlCasLogin(OMContext ctx) {
        initialiserInfosSiteDansContexte(ctx);
        return getURLCasLogin(ctx.getInfosSite(), false);
    }

    /**
     * Gets the url cas login front.
     *
     * @param ctx
     *            the ctx
     *
     * @return the url cas login front
     */
    public String getUrlCasLoginFront(OMContext ctx) {
        initialiserInfosSiteDansContexte(ctx);
        return getURLCasLogin(ctx.getInfosSite(), true);
    }

    /**
     * Gets the url cas logout.
     *
     * @param ctx
     *            the ctx
     *
     * @return the url cas logout
     */
    public String getUrlCasLogout(OMContext ctx) {
        initialiserInfosSiteDansContexte(ctx);
        return getURLCasLogout(ctx.getInfosSite(), "1".equals(PropertyHelper.getCoreProperty(PARAM_JTF_CAS_REDIRECTION_FRONT)));
    }

    /**
     * Gets the url cas logout front.
     *
     * @param ctx
     *            the ctx
     *
     * @return the url cas logout front
     */
    public String getUrlCasLogoutFront(OMContext ctx) {
        initialiserInfosSiteDansContexte(ctx);
        return getURLCasLogout(ctx.getInfosSite(), true);
    }

    /**
     * Jamais utilisé : l'authentification se fait sur le serveur CAS.
     *
     * @param codeUtilisateur
     *            the code utilisateur
     * @param mdp
     *            the mdp
     * @param ctx
     *            the ctx
     *
     * @return true, if connecte
     *
     */
    @Override
    public boolean connecte(String codeUtilisateur, String mdp, OMContext ctx) {
        return false;
    }

    /**
     * On supprime le PGT de l'utilisateur.
     *
     * @param codeUtilisateur
     *            the code utilisateur
     * @param ctx
     *            the ctx
     */
    @Override
    public void deconnecte(String codeUtilisateur, OMContext ctx) {
        if (codeUtilisateur != null && LIBELLE_JTF_CAS_MODE_PROXY.equals(modeCas) && mapLoginPGT.get(codeUtilisateur) != null) {
            mapLoginPGT.remove(codeUtilisateur);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.jsbsoft.jtf.identification.ISourceAuth#getCodeSource()
     */
    @Override
    public String getCodeSource() {
        return SOURCE_LIBELLE_CAS;
    }

    /**
     * Gets the libelle source.
     *
     * @return the libelle source
     *
     * @see ISourceAuth
     */
    @Override
    public String getLibelleSource() {
        return SOURCE_LIBELLE_AFFICHABLE;
    }

    /**
     * Initialise le parametrage CAS & calcule les urls suivant les differents sites.
     *
     */
    private void initialiseParametrageCas() {
        initialisationModeCAS();
        initialisationMapLoginPGT();
        initialisationHoteEtPortCas();
        initialisationURLsCas();
    }

    private void initialisationURLsCas() {
        final String hoteCasPort = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_HOTE);
        // url de validation de service
        final String uriCasService = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_URI_SERVICE);
        urlCasServiceValidate = HTTPS + hoteCasPort + uriCasService;
        // url de validation proxy
        final String uriCasProxyValidate = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_URI_PROXY_VALIDATE);
        urlProxyValidate = HTTPS + hoteCasPort + uriCasProxyValidate;
        // url de login pour un service CASsifie appele depuis le portail
        final String uriCasLogin = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_URI_LOGIN);
        urlServiceCasLogin = HTTPS + hoteCasPort + uriCasLogin;
    }

    private void initialisationModeCAS() {
        // lecture du mode proxy | basic
        final String modeCasJTF = PropertyHelper.getCoreProperty(PARAM_JTF_CAS);
        if (StringUtils.isEmpty(modeCasJTF) || LIBELLE_JTF_CAS_MODE_BASIC.equals(modeCasJTF)) {
            modeCas = LIBELLE_JTF_CAS_MODE_BASIC;
        } else if (LIBELLE_JTF_CAS_MODE_PROXY.equals(modeCasJTF)) {
            modeCas = LIBELLE_JTF_CAS_MODE_PROXY;
        }
    }

    private void initialisationMapLoginPGT() {
        if (modeCas.equalsIgnoreCase(LIBELLE_JTF_CAS_MODE_PROXY)) {
            // sera sujet à la concurrence d'accès
            mapLoginPGT = new HashMap<>();
        }
    }

    private void initialisationHoteEtPortCas() {
        final String hoteCasPort = StringUtils.defaultString(PropertyHelper.getCoreProperty(PARAM_JTF_CAS_HOTE));
        final int indexDeuxpoints = hoteCasPort.indexOf(':');
        if (indexDeuxpoints != -1) {
            hoteCas = hoteCasPort.substring(0, indexDeuxpoints);
            // si l'hôte cas est de la forme : www.site.fr:8443/cas
            if (hoteCasPort.indexOf("/", indexDeuxpoints) != -1) {
                portCas = Integer.parseInt(hoteCasPort.substring(indexDeuxpoints + 1, hoteCasPort.indexOf("/", indexDeuxpoints)));
            } else {
                portCas = Integer.parseInt(hoteCasPort.substring(indexDeuxpoints + 1));
            }
        } else {
            hoteCas = hoteCasPort;
            portCas = 443;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.jsbsoft.jtf.identification.ISourceAuth#parametrageCharge()
     */
    @Override
    public String parametrageCharge() {
        return "| Mode CAS : " + modeCas + "\n";
    }

    /**
     * Lance la validation du ticket CAS suivant le mode choisi.
     *
     * @param ctx
     *            the ctx
     * @param infoBean
     *            the info bean
     *
     * @return vrai si la validation du ticket s'est faite correctement
     *
     * @throws Exception
     *             the exception
     */
    public UtilisateurBean validation(final OMContext ctx, final InfoBean infoBean) throws Exception {
        boolean bIdenticationBack = true;
        // on identifie de quel côté on souhaite se connecter
        if ("IDENTIFICATION_FRONT".equals(infoBean.getNomProcessus())) {
            bIdenticationBack = false;
        }
        if (LIBELLE_JTF_CAS_MODE_PROXY.equals(modeCas)) {
            return validationModeProxy(ctx, infoBean.getString("ticket"), bIdenticationBack, null);
        }
        return validationModeBasic(ctx, infoBean.getString("ticket"), bIdenticationBack);
    }

    /**
     * Prend en charge la validation du ticket CAS en mode simple.
     *
     * @param ctx
     *            contexte
     * @param ticket
     *            ticket CAS à valider (ST ou PT)
     * @param bIdenticationBack
     *            indique si l'identification est faite en back ou non
     *
     * @return utilisateur loggue
     *
     * @throws Exception
     *             levee si l'utilisateur est incorrect ou s'il y a eu echec lors de la connexion vers le serveur
     */
    protected UtilisateurBean validationModeBasic(OMContext ctx, String ticket, boolean bIdenticationBack) throws Exception {
        if (StringUtils.isEmpty(ticket)) {
            return null;
        }
        // prend en charge la validation du ticket aupres de CAS
        final ServiceTicketValidator sv = new ServiceTicketValidator();
        // url validant le ticket
        // l'url de validation diffère s'il s'agit d'un ST ou d'un PT
        if (ticket.contains("ST")) {
            sv.setCasValidateUrl(urlCasServiceValidate);
        } else {
            sv.setCasValidateUrl(urlProxyValidate);
        }
        // url de notre servlet (ici)
        initialiserInfosSiteDansContexte(ctx);
        sv.setService(getURLCasAuthentification(ctx.getInfosSite(), !bIdenticationBack));
        sv.setServiceTicket(ticket);
        sv.validate();
        return chargeUtilisateur(ctx, sv, bIdenticationBack);
    }

    /**
     * Valide pour un service particulier (url) le mode proxy, et recupere l'utilisateur correspondant.
     *
     * @param ctx
     *            contexte
     * @param ticket
     *            ticket proxy
     * @param urlService
     *            url du service pour lequel le ticket a ete attribue
     *
     * @return utilisateur correspondant
     *
     * @throws Exception
     *             the exception
     */
    public UtilisateurBean validationServiceModeProxy(OMContext ctx, String ticket, String urlService) throws Exception {
        return validationModeProxy(ctx, ticket, false, urlService);
    }

    /**
     * Prend en charge la validation du ticket CAS en mode proxy.
     *
     * @param ctx
     *            contexte
     * @param ticket
     *            ticket CAS à valider (ST ou PT)
     * @param bIdenticationBack
     *            indique si l'identification est faite en back ou non
     * @param urlService
     *            the url service
     *
     * @return utilisateur loggue
     *
     * @throws Exception
     *             levee si l'utilisateur est incorrect ou s'il y a eu echec lors de la connexion vers le serveur
     */
    protected UtilisateurBean validationModeProxy(OMContext ctx, String ticket, boolean bIdenticationBack, String urlService) throws Exception {
        if ((ticket == null) || "".equals(ticket)) {
            return null;
        }
        final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
        final ProxyTicketValidator pv = new ProxyTicketValidator();
        // url validant le ticket
        // l'url de validation diffère s'il s'agit d'un ST ou d'un PT
        if (ticket.contains("ST")) {
            pv.setCasValidateUrl(urlCasServiceValidate);
        } else {
            pv.setCasValidateUrl(urlProxyValidate);
        }
        pv.setProxyCallbackUrl(getURLCasProxyCallback(serviceInfosSite.getPrincipalSite()));
        // url du service demandeur
        if (urlService != null) {
            pv.setService(urlService);
        } else {
            pv.setService(getURLCasAuthentification(serviceInfosSite.getPrincipalSite(), !bIdenticationBack));
        }
        pv.setServiceTicket(ticket);
        // validation aupres de CAS
        pv.validate();
        final UtilisateurBean utilisateur = chargeUtilisateur(ctx, pv, bIdenticationBack);
        // on conserve l'association Login / PGT pour les demandes ultérieures
        // de PT
        if (utilisateur != null) {
            final String pgt = pv.getPgtIou();
            if (pgt != null) {
                String code;
                if (GestionnaireIdentification.getInstance().getMappingLogin()) {
                    // utilisation du code ldap pour correspondance
                    code = utilisateur.getCodeLdap();
                } else {
                    code = utilisateur.getCode();
                }
                if (mapLoginPGT.get(code) != null) {
                    mapLoginPGT.remove(code);
                }
                mapLoginPGT.put(code, pgt);
                LOG.debug("*** PGT acquis pour l'utilisateur " + code + ".");
            } else {
                LOG.debug("*** Impossible d'acquérir un PGT pour l'utilisateur.");
            }
        }
        return utilisateur;
    }

    /**
     * Retourne un proxy ticket pour l'utilisateur loggue.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur
     * @param codeLdap
     *            code ldap (utilise si le login sur CAS est different du code utilisateur K-Portal)
     * @param urlTarget
     *            url du service en charge de valider le PT
     *
     * @return PT
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public String getProxyTicket(String codeUtilisateur, String codeLdap, String urlTarget) throws IOException {
        if (LIBELLE_JTF_CAS_MODE_PROXY.equals(modeCas)) {
            String pgt;
            if (GestionnaireIdentification.getInstance().getMappingLogin()) {
                // utilisation du code ldap pour correspondance
                pgt = mapLoginPGT.get(codeLdap);
            } else {
                pgt = mapLoginPGT.get(codeUtilisateur);
            }
            return edu.yale.its.tp.cas.proxy.ProxyTicketReceptor.getProxyTicket(pgt, urlTarget);
        }
        return null;
    }

    /**
     * Charge l'utilisateur K-Portal.
     *
     * @param ctx
     *            contexte
     * @param stv
     *            validateur de ticket CAS
     * @param bIdenticationBack
     *            indique si on est en back
     *
     * @return l'utilisateur loggue
     *
     * @throws Exception
     *             levee si l'utilisateur est incorrect ou s'il y a eu echec lors de la connexion vers le serveur
     */
    private UtilisateurBean chargeUtilisateur(OMContext ctx, ServiceTicketValidator stv, boolean bIdenticationBack) throws Exception {
        UtilisateurBean utilisateur;
        // on recupere la reponse
        // String xmlResponse = sv.getResponse( );
        if (stv.isAuthenticationSuccesful()) {
            String codeUtilisateur = stv.getUser();
            utilisateur = GestionnaireIdentification.getInstance().estUtilisateurKportal(codeUtilisateur, "", ctx);
            // l'utilisateur n'a pas ete trouve dans notre BD
            if (utilisateur == null) {
                // on peut etre connecte sur CAS, sans que le user soit dans
                // la base KPortal
                String messageException = MessageHelper.getCoreMessage(ctx.getLocale(), "ST_CAS_ERREUR_MSG1") + " " + codeUtilisateur + " " + MessageHelper.getCoreMessage(ctx.getLocale(), "ST_CAS_ERREUR_MSG2") + "<br />" + MessageHelper.getCoreMessage(ctx.getLocale(), "ST_CAS_ERREUR_MSG3");
                initialiserInfosSiteDansContexte(ctx);
                final String urlCasLogout = getURLCasLogout(ctx.getInfosSite(), !bIdenticationBack);
                if (!bIdenticationBack) {
                    messageException += "<br /><a href=\"" + urlCasLogout + "\">Veuillez vous déconnecter de CAS.</a>";
                }
                throw new ErreurApplicative(messageException);
            }
        } else {
            // ne devrait etre jamais accede
            final String szCodeErreur = stv.getErrorCode();
            final String szMessageErreur = stv.getErrorMessage();
            throw new ErreurApplicative(String.format(MessageHelper.getCoreMessage(ctx.getLocale(), "AUTHENTIFICATION.ERREUR.AUTH_CAS"), szCodeErreur, szMessageErreur));
        }
        return utilisateur;
    }

    /**
     * Initialiser le {@link ContexteUniv} avec un {@link InfosSite}. Eviter les {@link NullPointerException}. Si le contexte ne contient aucun site c'est le site principal qui
     * sera forcé.
     *
     * @param ctx
     *            Le contexte à initialiser.
     */
    private void initialiserInfosSiteDansContexte(OMContext ctx) {
        if (ctx.getInfosSite() == null) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            ctx.setInfosSite(serviceInfosSite.getPrincipalSite());
        }
    }

	private String getURLCasAuthentification(InfosSite infosSite, boolean front) {
		if (front){
			return URLResolver.getAbsoluteUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION_FRONT", infosSite, URLResolver.LOGIN_FRONT);
		}else{
			return URLResolver.getAbsoluteBoUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION", infosSite);
		}
	}

    private String getURLCasLogin(InfosSite infosSite, boolean front) {
        final String uriCasLogin = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_URI_LOGIN);
        final String hoteCasPort = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_HOTE);
        final String urlCasAuth = getURLCasAuthentification(infosSite, front);
        try {
            return HTTPS + hoteCasPort + uriCasLogin + URLEncoder.encode(urlCasAuth, CharEncoding.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            LOG.error("Erreur durant l'encodage de l'URL CAS de login" + (front ? " front" : ""), e);
            return StringUtils.EMPTY;
        }
    }

    private String getURLCasLogout(InfosSite infosSite, boolean front) {
        final String uriCasLogoutCallback = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_URI_LOGOUT);
        if (StringUtils.isEmpty(uriCasLogoutCallback)) {
            return StringUtils.EMPTY;
        } else {
            final String hoteCasPort = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_HOTE);
            try {
                String url = URLEncoder.encode(URLResolver.getAbsoluteBoUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION&ACTION=DECONNECTER", infosSite), CharEncoding.DEFAULT);
                if (front) {
                    url = URLEncoder.encode(URLResolver.getAbsoluteUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION_FRONT&ACTION=DECONNECTER", infosSite, URLResolver.DECONNECTER_FRONT), CharEncoding.DEFAULT);
                }
                return HTTPS + hoteCasPort + uriCasLogoutCallback + url;
            } catch (UnsupportedEncodingException e) {
                LOG.error("Erreur durant l'encodage de l'URL CAS de logout" + (front ? " front" : ""), e);
                return StringUtils.EMPTY;
            }
        }
    }

    private String getURLCasProxyCallback(InfosSite infosSite) {
        if (modeCas.equals(LIBELLE_JTF_CAS_MODE_PROXY)) {
            return URLResolver.getAbsoluteUrl(PropertyHelper.getCoreProperty(PARAM_JTF_CAS_URI_PROXY_RECEPTOR), true, infosSite);
        } else {
            return StringUtils.EMPTY;
        }
    }
}
