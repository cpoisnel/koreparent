package com.jsbsoft.jtf.identification;

import com.jsbsoft.jtf.database.OMContext;

/**
 * Interface permettant l'implémentation d'une source d'authentification.
 *
 * @author jbiard
 */
public interface ISourceAuth {

    /**
     * Authentifie un utilisateur et renvoie un objet Utilisateur associé.
     *
     * @param codeUtilisateur
     *            login
     * @param mdp
     *            mode de passe
     * @param ctx
     *            contexte pour récupération de l'utilisateur K-Portal
     *
     * @return utilisateur K-Portal
     *
     * @throws Exception
     *             the exception
     */
    boolean connecte(String codeUtilisateur, String mdp, OMContext ctx) throws Exception;

    /**
     * Appelée avant destruction des infos de session.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur à déconnecter
     * @param ctx
     *            contexte
     *
     * @throws Exception
     */
    void deconnecte(String codeUtilisateur, OMContext ctx);

    /**
     * Renvoie le code associé à la source. Doit avoir pour valeur celle déclarée dans le jtf, soit authentification.code_source.classe.
     *
     * @return code
     */
    String getCodeSource();

    /**
     * Renvoie le libellé associée à la source.
     *
     * @return libellé
     */
    String getLibelleSource();

    /**
     * Renvoie sous forme d'une chaîne le paramétrage chargé par la source.
     *
     * @return paramétrage
     */
    String parametrageCharge();
}
