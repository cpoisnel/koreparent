package com.jsbsoft.jtf.identification;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.UUID;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.SynchroniseurUtilisateur;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.plugin.session.PluginSessionHelper;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.Utilisateur;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;
import com.univ.objetspartages.services.ServiceProfildsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.ContexteUniv;
import com.univ.utils.IAuthorizationExterne;
import com.univ.utils.ISynchroniseurUtilisateurMetier;
import com.univ.xml.NodeUtil;

/**
 * Singleton prenant en charge l'identification et le chargement de session d'un utilisateur.
 *
 * @author jbiard
 */
public class GestionnaireIdentification {

    private final static String ID_BEAN = "gestionnaireIdentification";

    /** The Constant PARAM_JTF_SOURCE_AUTH_SOURCE. */
    public final static String PARAM_JTF_SOURCE_AUTH_SOURCE = "authentification.source";

    /** The Constant PARAM_JTF_SOURCE_AUTH_MAPPING. */
    public final static String PARAM_JTF_SOURCE_AUTH_MAPPING = "authentification.mapping_login";

    /** The Constant JTF_LDAP_LOGIN_CREATION_UTILSATEUR. */
    public final static String JTF_LDAP_LOGIN_CREATION_UTILSATEUR = "ldap.utilisateur.login.creation";

    /** The Constant JTF_LDAP_LOGIN_SYNCHRO_UTILSATEUR. */
    public final static String JTF_LDAP_LOGIN_SYNCHRO_UTILSATEUR = "ldap.utilisateur.login.synchro";

    /** The _log. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GestionnaireIdentification.class);

    /* Time out du cache de session */

    /** The TIMEOU t_ cach e_ session. */
    private final int TIMEOUT_CACHE_SESSION = 600000;

    private final Map<String, Object> mapSynchroUtilisateurMetier;
    //FBO : 20060214 : Creation d'un utilisateur au login à partir du LDAP

    /** The source auth defaut kbd. */
    private final ISourceAuth sourceAuthDefautKbd = new SourceAuthDefautKbd();
    //FBO : 20060214 : Synchron d'un utilisateur au login avec le LDAP

    /** The cache infos utilisateur proxy client. */
    private final Map<String, Map<String, Object>> cacheInfosUtilisateurProxyClient = new HashMap<>();
    // JB 20050606 : map des objets permettant d'enrichir le contexte avec les infos de session pour l'applicatif metier
    // s'occupe également de la synchro utilisateur <-> utilisateurs métier
    // TODO : à passer en non modifiable ??

    private final ServiceUser serviceUser;

    /** The _validateur cas. */
    private ValidateurCAS _validateurCAS;
    // hashtable pour conserver compatibilité K-Portal (gestion des libellés)...

    /** The est creation compte auto. */
    private boolean estCreationCompteAuto = false;

    /** The est synchro compte auto. */
    private boolean estSynchroCompteAuto = false;

    /** The map synchro utilisateur metier. */
    private Map<String, ISourceAuth> mapCodeSourceSource;

    /** The map code source libelle sources. */
    private Hashtable<String, String> mapCodeSourceLibelleSources;

    /** The source auth. */
    private ISourceAuth sourceAuth;

    /** The b mapping login. */
    private boolean bMappingLogin = false;

    /** The authorization externe. */
    private IAuthorizationExterne authorizationExterne;

    /**
     * Constructeur : singleton.
     */
    protected GestionnaireIdentification() {
        sourceAuth = chargeSourceAuth("authentification." + PropertyHelper.getCoreProperty(PARAM_JTF_SOURCE_AUTH_SOURCE) + ".classe");
        // source par défaut
        if (sourceAuth == null) {
            sourceAuth = new SourceAuthDefautKbd();
            LOGGER.debug("Source d'authentification par défaut chargée : ");
        }
        // on charge les autres sources
        getLibellesSourcesDisponibles();
        // lecture du mapping
        bMappingLogin = "1".equals(PropertyHelper.getCoreProperty(PARAM_JTF_SOURCE_AUTH_MAPPING));
        mapSynchroUtilisateurMetier = SynchroniseurUtilisateur.getInstance().getMapSynchroUtilisateurMetier();
        //FBO : 20060214
        // Si une personne appartient au LDAP ie authentification valide et que cette personne n'existe pas dans Kportal
        // Alors on créer l'utilisateur : desactivé par défaut ; mettre à 1 pour le ativer
        estCreationCompteAuto = "1".equals(PropertyHelper.getAllProperties().getProperty(JTF_LDAP_LOGIN_CREATION_UTILSATEUR));
        // FBO : 20060214
        // Si une personne appartient au LDAP ie authentification valide et que cette personne existe dans Kportal
        // Alors on synchronise l'utilisateur avec l'annuaire : activé par défaut ; mettre à 0 pour le descativer
        estSynchroCompteAuto = "1".equals(PropertyHelper.getAllProperties().getProperty(JTF_LDAP_LOGIN_SYNCHRO_UTILSATEUR));
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Accesseur statique pour singleton.
     * @return instance unique du gestionnaire
     */
    public static GestionnaireIdentification getInstance() {
        return ApplicationContextManager.getCoreContextBean(ID_BEAN, GestionnaireIdentification.class);
    }

    public static boolean isAuthorizedAddr(final String remoteAddr) {
        boolean res = false;
        final String param = PropertyHelper.getCoreProperty("identification.ips");
        if (param != null) {
            for (final StringTokenizer st = new StringTokenizer(param, ";"); st.hasMoreTokens(); ) {
                if (st.nextToken().equals(remoteAddr)) {
                    res = true;
                    break;
                }
            }
        }
        return res;
    }

    /**
     * Gets the code source auth.
     *
     * @return the code source auth
     */
    public String getCodeSourceAuth() {
        return sourceAuth.getCodeSource();
    }

    /**
     * Est source auth.
     *
     * @param libelleSourceAuth
     *            the libelle source auth
     *
     * @return true, if successful
     */
    public boolean estSourceAuth(final String libelleSourceAuth) {
        return sourceAuth.getCodeSource().equals(libelleSourceAuth);
    }

    /**
     * Gets the mapping login.
     *
     * @return the mapping login
     */
    public boolean getMappingLogin() {
        return bMappingLogin;
    }

    /**
     * Gets the source auth.
     *
     * @return the source auth
     */
    public ISourceAuth getSourceAuth() {
        return sourceAuth;
    }

    /**
     * Genere un mot de passe.
     *
     * @return mot de passe genere
     *
     * @deprecated Utilisez plutôt {@link RandomStringUtils#randomAscii(int)}
     */
    @Deprecated
    public String genererMDP() {
        final String charMin = "abcdefghijklmnopqrstuvwxyz";
        final String charMaj = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String charNb = "0123456789";
        final String charTout = charMin + charMaj + charNb;
        String mdp = "";
        for (int i = 0; i < 7; i++) {
            mdp = mdp.concat("" + charTout.charAt((int) ((Math.random() * 1000) % charTout.length())));
        }
        return mdp;
    }

    /**
     * Renvoie la map des objets prennant en charge la synchro des utilisateurs métier.
     *
     * @return map
     */
    public Map<String, Object> getMapSynchroUtilisateurMetier() {
        return mapSynchroUtilisateurMetier;
    }

    /**
     * Gets the validateur cas.
     *
     * @return the validateur cas
     */
    public ValidateurCAS getValidateurCAS() {
        if (_validateurCAS == null) {
            final Object o = mapCodeSourceSource.get(ValidateurCAS.SOURCE_LIBELLE_CAS);
            if ((o != null) && (o instanceof ValidateurCAS)) {
                _validateurCAS = (ValidateurCAS) o;
            }
        }
        return _validateurCAS;
    }

    /**
     * Connecte un utilisateur et charge ses infos de session.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur à charger
     * @param mdp
     *            mot de passe
     * @param ctx
     *            contexte
     * @param mapInfosUser
     *            map des infos à alimenter
     * @param userAgent
     *            navigateur détecté
     *
     * @return objet utilisateur K-Portal
     *
     * @throws Exception
     *             the exception
     */
    public UtilisateurBean connecte(final String codeUtilisateur, final String mdp, final OMContext ctx, final Map<String, Object> mapInfosUser, final String userAgent) throws Exception {
        UtilisateurBean user = null;
        if (StringUtils.isNotEmpty(codeUtilisateur) && StringUtils.isNotBlank(mdp) && sourceAuth.connecte(codeUtilisateur, mdp, ctx)) {
            user = estUtilisateurKportal(codeUtilisateur, mdp, ctx);
            if (user != null) {
                /* Délégation de l'authentification */
                authorizationExterne = chargeAuthorization(IAuthorizationExterne.JTF_AUTHORIZATION_CLASSE);
                if (authorizationExterne != null && !authorizationExterne.authorized(codeUtilisateur, ctx)) {
                    return null;
                } else {
                    chargeInfoUser(user, ctx, mapInfosUser, userAgent, true);
                }
            }
        }
        return user;
    }

    /**
     * Connecte un utilisateur KPortal et charge ses infos de session.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur à charger
     * @param mdp
     *            mot de passe
     * @param ctx
     *            contexte
     * @param mapInfosUser
     *            map des infos à alimenter
     * @param userAgent
     *            navigateur détecté
     *
     * @return objet utilisateur K-Portal
     *
     * @throws Exception
     *             the exception
     */
    public UtilisateurBean connecteUserKbd(final String codeUtilisateur, final String mdp, final OMContext ctx, final Map<String, Object> mapInfosUser, final String userAgent) throws Exception {
        UtilisateurBean user = null;
        if (StringUtils.isNotEmpty(codeUtilisateur) && StringUtils.isNotBlank(codeUtilisateur) && sourceAuthDefautKbd.connecte(codeUtilisateur, mdp, ctx)) {
            user = serviceUser.getByCode(codeUtilisateur);
            if (user != null) {
                chargeInfoUser(user, ctx, mapInfosUser, userAgent, true);
            }
        }
        return user;
    }

    /**
     * Retourne le code de l'utilisateur de la session courante.
     *
     * @param request
     *            requete http
     *
     * @return code de l'utilisateur
     */
    public String getCodeUtilisateurSessionCourante(final HttpServletRequest request) {
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) request.getSession(Boolean.FALSE).getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        if (sessionUtilisateur == null) {
            return null;
        }
        return (String) sessionUtilisateur.getInfos().get(SessionUtilisateur.CODE);
    }

    /**
     * Lance le chargement des données de l'utilisateur métier. Ces données seront ensuite associées à la session de l'utilisateur.
     *
     * @param ctx
     *            contexte
     * @param bIdentificationFront
     *            indique s'il s'agit ou non d'une ident front
     * @param utilisateur
     *            the utilisateur
     * @param mapInfosUser
     *            the map infos user
     *
     * @return map pour enrichir les données de session de l'utilisateur, la clef est le nom de l'applicatif (par exemple kdecole).
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link GestionnaireIdentification#chargeInfosSessionUserMetier(OMContext, UtilisateurBean, Map, boolean)}
     */
    @Deprecated
    public Map<String, Map> chargeInfosSessionUserMetier(final OMContext ctx, final Utilisateur utilisateur, final Map<String, Object> mapInfosUser, final boolean bIdentificationFront) throws Exception {
        return chargeInfosSessionUserMetier(ctx, utilisateur.getPersistenceBean(), mapInfosUser, bIdentificationFront);
    }

    /**
     * Lance le chargement des données de l'utilisateur métier. Ces données seront ensuite associées à la session de l'utilisateur.
     *
     * @param ctx
     *            contexte
     * @param bIdentificationFront
     *            indique s'il s'agit ou non d'une ident front
     * @param utilisateur
     *            the utilisateur
     * @param mapInfosUser
     *            the map infos user
     *
     * @return map pour enrichir les données de session de l'utilisateur, la clef est le nom de l'applicatif (par exemple kdecole).
     *
     * @throws Exception
     *             the exception
     */
    public Map<String, Map> chargeInfosSessionUserMetier(final OMContext ctx, final UtilisateurBean utilisateur, final Map<String, Object> mapInfosUser, final boolean bIdentificationFront) throws Exception {
        if ((mapSynchroUtilisateurMetier != null) && (mapSynchroUtilisateurMetier.size() > 0)) {
            final Map<String, Map> mapInfosSessionApplicatifsMetiers = new HashMap<>();
            for (String nomApplicatif : mapSynchroUtilisateurMetier.keySet()) {
                ISynchroniseurUtilisateurMetier chgt = (ISynchroniseurUtilisateurMetier) mapSynchroUtilisateurMetier.get(nomApplicatif);
                // infos renvoyees par l'objet
                Map mapInfos = chgt.getInfosSessionUtilisateurMetier(utilisateur, mapInfosUser, ctx, bIdentificationFront);
                if (mapInfos != null) {
                    mapInfosSessionApplicatifsMetiers.put(nomApplicatif, mapInfos);
                }
            }
            return mapInfosSessionApplicatifsMetiers;
        }
        return null;
    }

    /**
     * Charge les informations de session pour un code utilisateur donné.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur
     * @param ctx
     *            contexte
     * @param session
     *            the session
     * @param userAgent
     *            the user agent
     *
     * @return vrai si les infos ont bien été chargées.
     *
     * @throws Exception
     *             the exception
     */
    public boolean chargeInfoUser(final String codeUtilisateur, final OMContext ctx, final HttpSession session, final String userAgent) throws Exception {
        final UtilisateurBean utilisateur = estUtilisateurKportal(codeUtilisateur, "", ctx);
        if (utilisateur != null) {
            final SessionUtilisateur sessionUtilisateur = new SessionUtilisateur(session);
            chargeInfoUser(utilisateur, ctx, sessionUtilisateur.getInfos(), userAgent, true);
            // on positionne la session créée
            session.setAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP, sessionUtilisateur);
            return true;
        }
        return false;
    }

    /**
     * Supprime le cache de l'utilisateur.
     *
     * @param cacheUser
     *            map des infos de l'utilisateur
     */
    protected void suppressionCacheUtilisateurProxyClient(final Map<String, Object> cacheUser) {
        LOGGER.debug("purge cache ws/user " + cacheUser.get(SessionUtilisateur.CODE));
        if (cacheUser.containsKey(SessionUtilisateur.KSESSION)) {
            // suppression fichier
            final String ksession = (String) cacheUser.get(SessionUtilisateur.KSESSION);
            final File fichierSortie = new File(WebAppUtil.getSessionsPath() + File.separator + "session_" + ksession + ".xml");
            if (fichierSortie.exists() && !fichierSortie.delete()) {
                LOGGER.debug("unable to remove the user data");
            }
            // suppression cache
            cacheInfosUtilisateurProxyClient.remove(cacheUser.get(SessionUtilisateur.CODE));
        }
    }

    /**
     * Lecture du cache (inclut un controle de validité).
     *
     * @param codeUser
     *            the code user
     *
     * @return the map
     */
    protected synchronized Map<String, Object> lireCacheUtilisateurProxyClient(final String codeUser) {
        Map<String, Object> cacheUser = cacheInfosUtilisateurProxyClient.get(codeUser);
        if (cacheUser != null) {
            /* Controle validité du cache */
            final long maintenant = System.currentTimeMillis();
            final long ts = (Long) cacheUser.get(SessionUtilisateur.TIMESTAMP);
            if (maintenant - ts > TIMEOUT_CACHE_SESSION) {
                suppressionCacheUtilisateurProxyClient(cacheUser);
                cacheUser = null;
            } else {
                cacheUser.put(SessionUtilisateur.TIMESTAMP, System.currentTimeMillis());
            }
        }
        return cacheUser;
    }

    /**
     * Ajout d'un élément dans le cache des informations d'authentification.
     *
     * @param codeUser
     *            the code user
     * @param hashInfosUser
     *            the hash infos user
     *
     * @return the map
     *
     */
    public synchronized Map<String, Object> ajouterCacheUtilisateurProxyClient(final String codeUser, final Map<String, Object> hashInfosUser) {
        /* Ajout du cache */
        LOGGER.debug("ajout cache ws/user " + codeUser);
        cacheInfosUtilisateurProxyClient.put(codeUser, hashInfosUser);
        /* Purges des caches > 10 minutes */
        final long maintenant = System.currentTimeMillis();
        for (Map<String, Object> chacheTmp : new ArrayList<>(cacheInfosUtilisateurProxyClient.values())) {
            final long ts = (Long) chacheTmp.get(SessionUtilisateur.TIMESTAMP);
            if (maintenant - ts > TIMEOUT_CACHE_SESSION) {
                suppressionCacheUtilisateurProxyClient(chacheTmp);
            }
        }
        return hashInfosUser;
    }

    /**
     * Initialise un contexte destiné en mode client d'un proxy (communication inter-serveur).
     *
     * @param req
     *            the req
     *
     * @return the contexte univ
     *
     * @throws Exception
     *             the exception
     */
    public ContexteUniv initialiserContexteProxyClient(final HttpServletRequest req) throws Exception {
        final ContexteUniv ctx = new ContexteUniv(req.getRequestURI());
        chargerContexteProxyClient(ctx, req.getParameter("USER"));
        return ctx;
    }

    /**
     * Initialise un contexte destiné en mode client d'un proxy (communication inter-serveur).
     *
     * @param ctx
     *            the ctx
     * @param codeUser
     *            the code user
     *
     * @throws Exception
     *             the exception
     */
    public void chargerContexteProxyClient(final ContexteUniv ctx, final String codeUser) throws Exception {
        /* Création du contexte utilisateur */
        if (codeUser != null && codeUser.length() > 0) {
            /* Lecture du cache */
            Map<String, Object> cacheUser = lireCacheUtilisateurProxyClient(codeUser);
            if (cacheUser == null) {
                cacheUser = creerCacheUtilisateurProxyClient(ctx, codeUser);
                if (cacheUser != null) {
                    ajouterCacheUtilisateurProxyClient(codeUser, cacheUser);
                }
            }
            if (cacheUser != null) {
                /* Initialisation du contexte avec les infos utilisateur */
                ctx.setCode((String) cacheUser.get(SessionUtilisateur.CODE));
                ctx.setCodeStructure((String) cacheUser.get(SessionUtilisateur.CODE_STRUCTURE));
                ctx.setCodeGestion((String) cacheUser.get(SessionUtilisateur.CODE_GESTION));
                ctx.setNom((String) cacheUser.get(SessionUtilisateur.NOM));
                ctx.setPrenom((String) cacheUser.get(SessionUtilisateur.PRENOM));
                ctx.setCentresInteret((Vector<String>) cacheUser.get(SessionUtilisateur.CENTRES_INTERET));
                ctx.setAdresseMail((String) cacheUser.get(SessionUtilisateur.ADRESSE_MAIL));
                final TreeSet<String> groupes = new TreeSet<>((Vector<String>) cacheUser.get(SessionUtilisateur.GROUPES_DSI));
                ctx.setGroupesDsi(groupes);
                final TreeSet<String> profils = new TreeSet<>((Vector<String>) cacheUser.get(SessionUtilisateur.PROFILS_DSI));
                ctx.setListeProfilsDsi(profils);
                ctx.calculerGroupesDsiAvecAscendants();
                ctx.initialiserListeServices();
                ctx.setAutorisation((AutorisationBean) cacheUser.get(SessionUtilisateur.AUTORISATIONS));
                /* Connecteurs Java/Php: ksession */
                ctx.setKsession((String) cacheUser.get(SessionUtilisateur.KSESSION));
            }
        }
    }

    /**
     * On associe un cache à l'utilisateur pour optimiser les accès suivants.
     *
     * @param ctx
     *            the ctx
     * @param codeUser
     *            the code user
     *
     * @return the map
     *
     * @throws Exception
     *             the exception
     */
    public Map<String, Object> creerCacheUtilisateurProxyClient(final ContexteUniv ctx, final String codeUser) throws Exception {
        HashMap<String, Object> hashInfosUser = null;
        final UtilisateurBean utilisateur = serviceUser.getByCode(codeUser);
        if (utilisateur != null) {
            hashInfosUser = new HashMap<>();
            final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
            final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
            hashInfosUser.put(SessionUtilisateur.TIMESTAMP, System.currentTimeMillis());
            final Vector<String> groupesUtilisateurs = new Vector<>(serviceGroupeUtilisateur.getAllGroupsCodesByUserCode(utilisateur.getCode()));
            final AutorisationBean autorisation = new AutorisationBean(utilisateur, groupesUtilisateurs, ctx);
            hashInfosUser.put(SessionUtilisateur.CODE, utilisateur.getCode());
            hashInfosUser.put(SessionUtilisateur.NOM, utilisateur.getNom());
            hashInfosUser.put(SessionUtilisateur.PRENOM, utilisateur.getPrenom());
            hashInfosUser.put(SessionUtilisateur.AUTORISATIONS, autorisation);
            hashInfosUser.put(SessionUtilisateur.CODE, utilisateur.getCode());
            hashInfosUser.put(SessionUtilisateur.CODE_STRUCTURE, utilisateur.getCodeRattachement());
            hashInfosUser.put(SessionUtilisateur.CODE_GESTION, utilisateur.getCodeLdap());
            hashInfosUser.put(SessionUtilisateur.ADRESSE_MAIL, utilisateur.getAdresseMail());
            hashInfosUser.put(SessionUtilisateur.GROUPES_DSI, groupesUtilisateurs);
            final Collection<String> vListeProfils = serviceProfildsi.renvoyerProfilsGroupes(groupesUtilisateurs);
            hashInfosUser.put(SessionUtilisateur.PROFILS_DSI, vListeProfils);
            hashInfosUser.put(SessionUtilisateur.CENTRES_INTERET, utilisateur);
            final String ksession = GestionnaireIdentification.getInstance().creationFichierSession(utilisateur, groupesUtilisateurs, new Vector<String>(), "");
            hashInfosUser.put(SessionUtilisateur.KSESSION, ksession);
        }
        return hashInfosUser;
    }

    /**
     * Charge les informations de session pour un code utilisateur donné.
     *
     * @param utilisateur
     *            utilisateur K-Portal existant
     * @param ctx
     *            contexte
     * @param mapInfoUser
     *            map à renseigner avec les infos de l'utilisateur
     * @param userAgent
     *            navigateur détecté (user-agent de la requête)
     * @param bIdentificationFront
     *            the b identification front
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link GestionnaireIdentification#chargeInfoUser(UtilisateurBean, OMContext, Map, String, boolean)}
     */
    @Deprecated
    public void chargeInfoUser(final Utilisateur utilisateur, final OMContext ctx, final Map<String, Object> mapInfoUser, final String userAgent, final boolean bIdentificationFront) throws Exception {
        chargeInfoUser(utilisateur.getPersistenceBean(), ctx, mapInfoUser, userAgent, bIdentificationFront);
    }

    /**
     * Charge les informations de session pour un code utilisateur donné.
     *
     * @param utilisateur
     *            utilisateur K-Portal existant
     * @param ctx
     *            contexte
     * @param mapInfoUser
     *            map à renseigner avec les infos de l'utilisateur
     * @param userAgent
     *            navigateur détecté (user-agent de la requête)
     * @param bIdentificationFront
     *            the b identification front
     *
     * @throws Exception
     *             the exception
     */
    public void chargeInfoUser(final UtilisateurBean utilisateur, final OMContext ctx, final Map<String, Object> mapInfoUser, final String userAgent, final boolean bIdentificationFront) throws Exception {
        mapInfoUser.put(SessionUtilisateur.DATE_DERNIERE_SESSION, utilisateur.getDateDerniereSession());
        final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        if (utilisateur.getDateDerniereSession() == null || !(sdf.format(utilisateur.getDateDerniereSession()).equals(sdf.format(new Date(System.currentTimeMillis()))))) {
            utilisateur.setDateDerniereSession(new Date(System.currentTimeMillis()));
            serviceUser.save(utilisateur);
        }
        // Mémorisation du user et de la langue
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        final Vector<String> groupesUtilisateurs = new Vector<>(serviceGroupeUtilisateur.getAllGroupsCodesByUserCode(utilisateur.getCode()));
        if (LOGGER.isDebugEnabled()) {
            for (String codeGroupe : groupesUtilisateurs) {
                LOGGER.debug("Groupe : " + codeGroupe);
            }
        }
        final AutorisationBean autorisation = new AutorisationBean(utilisateur, groupesUtilisateurs, ctx);
        mapInfoUser.put(SessionUtilisateur.AUTORISATIONS, autorisation);
        mapInfoUser.put(SessionUtilisateur.CODE, utilisateur.getCode());
        mapInfoUser.put(SessionUtilisateur.NOM, utilisateur.getNom());
        mapInfoUser.put(SessionUtilisateur.PRENOM, utilisateur.getPrenom());
        mapInfoUser.put(SessionUtilisateur.ADRESSE_MAIL, utilisateur.getAdresseMail());
        mapInfoUser.put(SessionUtilisateur.CODE_STRUCTURE, utilisateur.getCodeRattachement());
        mapInfoUser.put(SessionUtilisateur.CODE_GESTION, utilisateur.getCodeLdap());
        // Lecture du profil
        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
        String profilDefaut = utilisateur.getProfilDefaut();
        final List<String> vListeProfils = serviceProfildsi.renvoyerProfilsGroupes(groupesUtilisateurs);
        if (profilDefaut.length() == 0 || !vListeProfils.contains(profilDefaut)) {
            if (vListeProfils.size() > 0) {
                profilDefaut = vListeProfils.get(0);
            }
        }
        final ProfildsiBean defaultProfile = serviceProfildsi.getByCode(profilDefaut);
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        if (defaultProfile != null) {
            final RubriqueBean homeSection = serviceRubrique.getRubriqueByCode(defaultProfile.getCodeRubriqueAccueil());
            mapInfoUser.put(SessionUtilisateur.RUBRIQUE_ACCUEIL, homeSection);
        } else {
            mapInfoUser.put(SessionUtilisateur.RUBRIQUE_ACCUEIL, null);
        }
        mapInfoUser.put(SessionUtilisateur.PROFIL_DSI, profilDefaut);
        mapInfoUser.put(SessionUtilisateur.PROFILS_DSI, vListeProfils);
        mapInfoUser.put(SessionUtilisateur.GROUPES_DSI, groupesUtilisateurs);
        mapInfoUser.put(SessionUtilisateur.CENTRES_INTERET, serviceUser.getCentresInteret(utilisateur));
        //  Information Utilisateur Métier
        final Map<String, Map> infosSessionUserMetier = chargeInfosSessionUserMetier(ctx, utilisateur, mapInfoUser, bIdentificationFront);
        if (infosSessionUserMetier != null) {
            mapInfoUser.put(ContexteUniv.CLEF_INFOS_SESSION_APPLICATIFS_METIER, infosSessionUserMetier);
        }
        // Connecteurs KPortal (Java) vers autres applis (PHP ...)
        HttpSession session = null;
        if (ctx instanceof ProcessusBean) {
            session = ((ProcessusBean) ctx).getGp().getSessionUtilisateur().getHttpSession();
        } else if (ctx instanceof ContexteUniv) {
            session = ((ContexteUniv) ctx).getRequeteHTTP().getSession(Boolean.FALSE);
        }
        String sessionUser = StringUtils.EMPTY;
        if (session != null) {
            sessionUser = (String) session.getAttribute(SessionUtilisateur.KSESSION);
        }
        final String kSession = creationFichierSession(utilisateur, groupesUtilisateurs, vListeProfils, sessionUser);
        if (kSession == null) {
            LOGGER.error("Fichier de session impossible à créer pour l'utilisateur: " + utilisateur.getCode() + ".");
        } else {
            mapInfoUser.put(SessionUtilisateur.KSESSION, kSession);
        }
        String msgSession = "Session chargée pour l'utilisateur " + utilisateur.getCode();
        if (bMappingLogin) {
            msgSession = msgSession + " / " + utilisateur.getCodeLdap();
        }
        msgSession += " site " + ctx.getInfosSite().getAlias();
        LOGGER.info(msgSession);
        // Point d'extension de chargement de données en session
        PluginSessionHelper.setDonneesSession(session);
    }

    /**
     * Renvoie l'utilisateur K-Portal s'il existe correspondant au code utilisateur. Prend en charge le mapping de login.
     *
     * @param codeUtilisateur
     *            code de l'utilisateur
     * @param ctx
     *            contexte
     * @param password
     *            the password
     *
     * @return utilisateur s'il existe
     *
     * @throws Exception
     *             the exception
     */
    public UtilisateurBean estUtilisateurKportal(final String codeUtilisateur, final String password, final OMContext ctx) throws Exception {
        //récupération de l'utilisateur
        UtilisateurBean utilisateurBean;
        if (StringUtils.isEmpty(codeUtilisateur)) {
            return null;
        }
        // gestion du mapping
        if (bMappingLogin) {
            utilisateurBean = serviceUser.getByCodeLdap(codeUtilisateur);
        } else {
            utilisateurBean = serviceUser.getByCode(codeUtilisateur);
        }
        if (utilisateurBean == null) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage(ctx.getLocale(), "AUTHENTIFICATION.ERREUR.CONNEXION_REFUSEE"));
        }
        if (!(sourceAuth instanceof SourceAuthDefautKbd)) {
            //FBO : 20060214 : Synchronisation avec le LDAP au login (Creation ou Mise à jour)
            if (estCreationCompteAuto || estSynchroCompteAuto) {
                authorizationExterne = chargeAuthorization(IAuthorizationExterne.JTF_AUTHORIZATION_CLASSE);
                if (authorizationExterne != null && authorizationExterne.synchronise(codeUtilisateur)) {
                    return utilisateurBean;
                }
            }
        }
        //FBO : 20060214 : Gestion des Comptes locaux
        if (bMappingLogin) {
            if (sourceAuthDefautKbd.connecte(codeUtilisateur, password, ctx)) {
                utilisateurBean = serviceUser.getByCode(codeUtilisateur);
            }
        }
        return utilisateurBean;
    }

    /**
     * Deconnecte.
     *
     * @param idSession
     *            the id session
     * @param mapInfoUser
     *            the map info user
     * @param ctx
     *            the ctx
     */
    public void deconnecte(final String idSession, final Map<String, Object> mapInfoUser, final OMContext ctx) {
        // JB 20051125 : on ne supprime les infos de session que si l'id de session
        // existe, sinon on l'a deja fait auparavant
        if (idSession != null) {
            String code;
            if (bMappingLogin) {
                code = (String) mapInfoUser.get(SessionUtilisateur.CODE_GESTION);
            } else {
                code = (String) mapInfoUser.get(SessionUtilisateur.CODE);
            }
            sourceAuth.deconnecte(code, ctx);
            suppressionFichierSession(idSession);
            supprimeInfosSession(mapInfoUser);
            // TODO flush cacheService : gestion des services
            //CacheServiceMgr.suppressionCacheSession(idSession);
        }
    }

    /**
     * Supprime de la map des infos du user les propriétés chargées.
     *
     * @param mapInfoUser
     *            map du user
     */
    protected void supprimeInfosSession(final Map<String, Object> mapInfoUser) {
        mapInfoUser.remove(SessionUtilisateur.CODE);
        mapInfoUser.remove(SessionUtilisateur.CODE_GESTION);
        mapInfoUser.remove(SessionUtilisateur.NOM);
        mapInfoUser.remove(SessionUtilisateur.PRENOM);
        mapInfoUser.remove(SessionUtilisateur.AUTORISATIONS);
        // JSS 20030122
        mapInfoUser.remove(SessionUtilisateur.CODE_PAGE_ACCUEIL);
        mapInfoUser.remove(SessionUtilisateur.LANGUE_PAGE_ACCUEIL);
        // JSS 20050510 : profil dynamique
        mapInfoUser.remove(SessionUtilisateur.PROFIL_DSI);
        mapInfoUser.remove(SessionUtilisateur.PROFILS_DSI);
        mapInfoUser.remove(SessionUtilisateur.GROUPES_DSI);
        mapInfoUser.remove(SessionUtilisateur.KSESSION);
        // JSS 20040409
        mapInfoUser.remove(SessionUtilisateur.ESPACE);
    }
    //  FBO 20050216 Authorization externaliser / synchronisation au login

    /**
     * Prend en charge la creation du fichier de session.
     *
     * @param utilisateur
     *            utilisateur source pour la creation du fichier correspondant
     * @param groupesUtilisateur
     *            the groupes utilisateur
     * @param profilsUtilisateur
     *            the profils utilisateur
     * @param kSession
     *            identifiant de session si il existe
     * @return l'id de session (ksession)
     */
    private String creationFichierSession(final UtilisateurBean utilisateur, final List<String> groupesUtilisateur, final List<String> profilsUtilisateur, final String kSession) throws IOException {
        if (StringUtils.isNotEmpty(kSession)) {
            final File fichierSession = new File(WebAppUtil.getSessionsPath() + File.separator + "session_" + kSession + ".xml");
            if (fichierSession.exists()) {
                return kSession;
            }
        }
        final String idSession = UUID.randomUUID().toString();
        final File fichierSortie = new File(WebAppUtil.getSessionsPath() + File.separator + "session_" + idSession + ".xml");
        try (final FileWriter fileWriter = new FileWriter(fichierSortie);){
            final String codeUtilisateurSession = utilisateur.getCode();
            final String civiliteUtilisateurSession = utilisateur.getCivilite();
            final String nomUtilisateurSession = utilisateur.getNom();
            final String prenomUtilisateurSession = utilisateur.getPrenom();
            final String emailUtilisateurSession = utilisateur.getAdresseMail();
            final String structureUtilisateurSession = utilisateur.getCodeRattachement();
            final String codeLdapUtilisateurSession = utilisateur.getCodeLdap();
            String groupesUtilisateurSession = "";
            for (String codeGroupeUtilisateur : groupesUtilisateur) {
                if (groupesUtilisateurSession.length() > 0) {
                    groupesUtilisateurSession += ";";
                }
                groupesUtilisateurSession = groupesUtilisateurSession + codeGroupeUtilisateur;
            }
            String profilsUtilisateurSession = "";
            for (String codeProfilUtilisateur : profilsUtilisateur) {
                if (profilsUtilisateurSession.length() > 0) {
                    profilsUtilisateurSession += ";";
                }
                profilsUtilisateurSession = profilsUtilisateurSession + codeProfilUtilisateur;
            }
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.newDocument();
            final Node nodeFiche = document.createElement("SESSION");
            NodeUtil.addNode(nodeFiche, "ID", idSession);
            NodeUtil.addNode(nodeFiche, "CODE", codeUtilisateurSession);
            NodeUtil.addNode(nodeFiche, "CODE_LDAP", codeLdapUtilisateurSession);
            NodeUtil.addNode(nodeFiche, "CIVILITE", civiliteUtilisateurSession);
            NodeUtil.addNode(nodeFiche, "NOM", nomUtilisateurSession);
            NodeUtil.addNode(nodeFiche, "PRENOM", prenomUtilisateurSession);
            NodeUtil.addNode(nodeFiche, "EMAIL", emailUtilisateurSession);
            NodeUtil.addNode(nodeFiche, "STRUCTURE", structureUtilisateurSession);
            NodeUtil.addNode(nodeFiche, "PROFIL", profilsUtilisateurSession);
            NodeUtil.addNode(nodeFiche, "GROUPES", groupesUtilisateurSession);
            document.appendChild(nodeFiche);
            final TransformerFactory transFactory = TransformerFactory.newInstance();
            final Transformer transformer = transFactory.newTransformer();
            final DOMSource source = new DOMSource(document);
            fichierSortie.createNewFile();
            final StreamResult result = new StreamResult(fileWriter);
            transformer.setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, "utf-8");
            transformer.transform(source, result);
            return idSession;
        } catch (final IOException | TransformerException | ParserConfigurationException e) {
            LOGGER.error("Echec creation fichier session", e);
            return null;
        }
    }

    /**
     * Suppression fichier XML de session de l'utilisateur.
     *
     * @param idSession
     *            id de la session
     *
     * @return vrai si le fichier a été supprimé
     */
    protected boolean suppressionFichierSession(final String idSession) {
        boolean res = true;
        if (idSession != null) {
            // suppression fichiers de sauvegarde de liste back-office
            InfoBean.supprimerDatas(idSession);
            final File fichierSortie = new File(WebAppUtil.getSessionsPath() + File.separator + "session_" + idSession + ".xml");
            if (fichierSortie.exists()) {
                res = fichierSortie.delete();
            }
        }
        return res;
    }

    /**
     * Renvoie la liste des sources d'authentification spécifiée dans le jtf.
     *
     * @return liste non modifiable des libellés.
     */
    public Hashtable<String, String> getLibellesSourcesDisponibles() {
        if (mapCodeSourceSource == null) {
            // on enregistre la source active
            mapCodeSourceSource = new HashMap<>();
            mapCodeSourceSource.put(sourceAuth.getCodeSource(), sourceAuth);
            mapCodeSourceLibelleSources = new Hashtable<>();
            mapCodeSourceLibelleSources.put(sourceAuth.getCodeSource(), sourceAuth.getLibelleSource());
            final Enumeration<Object> enumProperties = PropertyHelper.getAllProperties().elements();
            while (enumProperties.hasMoreElements()) {
                String clef = (String) enumProperties.nextElement();
                // on repere les entrées
                if (clef.startsWith("authentification") && clef.endsWith(".classe")) {
                    ISourceAuth sourceAuthChargee = chargeSourceAuth(clef);
                    if (sourceAuthChargee != null) {
                        mapCodeSourceLibelleSources.put(sourceAuthChargee.getCodeSource(), sourceAuthChargee.getLibelleSource());
                        mapCodeSourceSource.put(sourceAuthChargee.getCodeSource(), sourceAuthChargee);
                    }
                }
            }
        }
        return mapCodeSourceLibelleSources;
    }

    /**
     * Charge l'objet en charge de l'authentification.
     *
     * @param prop
     *            propriété du jtf définissant la classe source auth à chargée
     *
     * @return source chargée, nulle le chargement n'a pu se faire
     */
    private ISourceAuth chargeSourceAuth(final String prop) {
        ISourceAuth sourceAuthChargee = null;
        try {
            if (prop != null) {
                final String nomClasse = PropertyHelper.getAllProperties().getProperty(prop);
                if ((sourceAuth != null) && prop.equals("authentification." + sourceAuth.getCodeSource() + ".classe")) {
                    return null;
                }
                if (nomClasse != null) {
                    final Object o = Class.forName(nomClasse).newInstance();
                    if (o instanceof ISourceAuth) {
                        sourceAuthChargee = (ISourceAuth) o;
                        LOGGER.debug("Source d'authentification " + sourceAuthChargee.getCodeSource() + " chargée : \n" + sourceAuthChargee.parametrageCharge());
                    }
                }
            }
        } catch (final ReflectiveOperationException e) {
            LOGGER.error("Echec chargement source auth. : " + prop, e);
        }
        return sourceAuthChargee;
    }

    /**
     * Charge l'objet en charge de la redirection.
     *
     * @param prop
     *            propriété du jtf définissant la classe source de redirection à chargée
     *
     * @return source chargée, nulle le chargement n'a pu se faire
     */
    private IAuthorizationExterne chargeAuthorization(final String prop) {
        IAuthorizationExterne sourceAuthorizationChargee = null;
        try {
            if (prop != null) {
                final String nomClasse = PropertyHelper.getAllProperties().getProperty(prop);
                if (nomClasse != null) {
                    final Object o = Class.forName(nomClasse).newInstance();
                    if (o instanceof IAuthorizationExterne) {
                        sourceAuthorizationChargee = (IAuthorizationExterne) o;
                        LOGGER.debug("Source de redirection chargée ");
                    }
                }
            }
        } catch (final ReflectiveOperationException e) {
            LOGGER.error("Echec chargement autorisations", e);
        }
        return sourceAuthorizationChargee;
    }
}
