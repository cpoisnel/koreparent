package com.jsbsoft.jtf.identification;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kportal.core.config.PropertyHelper;

/**
 * The Class SourceAuthClearTrust.
 */
public class SourceAuthClearTrust implements ISourceAuth {

    /** The Constant SOURCE_LIBELLE_CT. */
    public final static String SOURCE_LIBELLE_CT = "ClearTrust";

    /** The Constant SOURCE_LIBELLE_AFFICHABLE. */
    public final static String SOURCE_LIBELLE_AFFICHABLE = "RSA ClearTrust";

    /** The Constant URL_LOGIN_FRONT. */
    public final static String URL_LOGIN_FRONT = "/adminsite/dsi/login.jsp";

    /** The Constant PARAM_LOGIN_HTTP_HEADER. */
    public final static String PARAM_LOGIN_HTTP_HEADER = "ct-remote-user";

    /** The Constant PARAM_JTF_CTRUST_URL_DECONNEXION. */
    public final static String PARAM_JTF_CTRUST_URL_DECONNEXION = "ctrust.url_deconnexion";

    private final static Logger LOG = LoggerFactory.getLogger(SourceAuthClearTrust.class);

    /** The url deconnexion ct. */
    private String urlDeconnexionCT;

    /**
     * Instantiates a new source auth clear trust.
     */
    SourceAuthClearTrust() {
        initialiseParametrageCtrust();
    }

    /**
     * Gets the url ctrust login.
     *
     * @param ctx
     *            the ctx
     *
     * @return the url ctrust login
     */
    public String getUrlCtrustLogin(final OMContext ctx) {
        return URL_LOGIN_FRONT;
    }

    /**
     * Gets the url ctrust logout.
     *
     * @param ctx
     *            the ctx
     * @param urlCallback
     *            the url callback
     *
     * @return the url ctrust logout
     */
    public String getUrlCtrustLogout(final OMContext ctx, final String urlCallback) {
        String urlDeconnexion = "";
        if (!"".equals(urlDeconnexionCT)) {
            try {
                String prefix = StringUtils.EMPTY;
                if (!urlDeconnexionCT.endsWith("?") && !urlDeconnexionCT.endsWith("&") && !urlDeconnexionCT.endsWith("=")) {
                    if (!urlDeconnexionCT.endsWith("=") && urlDeconnexionCT.contains("?")) {
                        prefix = "&";
                    } else if (!urlDeconnexionCT.endsWith("=")) {
                        prefix = "?";
                    }
                }
                urlDeconnexion = urlDeconnexionCT + prefix + URLEncoder.encode(urlCallback, CharEncoding.DEFAULT);
            } catch (final UnsupportedEncodingException e) {
                LOG.error("erreur sur l'encodage de caractere", e);
            }
        } else {
            return urlCallback;
        }
        return urlDeconnexion;
    }

    /**
     * Non utilisée.
     *
     * @param codeUtilisateur
     *            the code utilisateur
     * @param mdp
     *            the mdp
     * @param ctx
     *            the ctx
     *
     * @return true, if connecte
     *
     */
    @Override
    public boolean connecte(final String codeUtilisateur, final String mdp, final OMContext ctx) {
        return false;
    }

    /**
     * Non utilisée.
     *
     * @param codeUtilisateur
     *            the code utilisateur
     * @param ctx
     *            the ctx
     */
    @Override
    public void deconnecte(final String codeUtilisateur, final OMContext ctx) {}

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.identification.ISourceAuth#getCodeSource()
     */
    @Override
    public String getCodeSource() {
        return SOURCE_LIBELLE_CT;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.identification.ISourceAuth#getLibelleSource()
     */
    @Override
    public String getLibelleSource() {
        return SOURCE_LIBELLE_AFFICHABLE;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.identification.ISourceAuth#parametrageCharge()
     */
    @Override
    public String parametrageCharge() {
        return " | URL deconnexion : " + urlDeconnexionCT;
    }

    /**
     * Initialise parametrage ctrust.
     */
    private void initialiseParametrageCtrust() {
        urlDeconnexionCT = PropertyHelper.getCoreProperty(PARAM_JTF_CTRUST_URL_DECONNEXION);
        if (urlDeconnexionCT == null) {
            urlDeconnexionCT = "";
        }
    }
}
