package com.jsbsoft.jtf.textsearch;

import java.io.Serializable;

/**
 * The Class ResultatRecherche.
 */
public class ResultatRecherche implements Serializable {

    /** The objet. */
    private static final long serialVersionUID = 1L;

    /** The objet. */
    private String objet = "";

    /** The id fiche. */
    private String idFiche = "";

    /** The code fiche. */
    private String codeFiche = "";

    /** The langue. */
    private String langue = "";

    /** The code rubrique. */
    private String codeRubrique = "";

    /** The code rattachement. */
    private String codeRattachement = "";

    /** The public vise. */
    private String publicVise = "";

    /** The public vide restruction */
    private String publicViseRestriction = "";

    /** The mode restriction. */
    private String modeRestriction = "";

    /** The url. */
    private String url = "";

    /** The titre. */
    private String titre = "";

    /** The date modification. */
    private String dateModification = "";

    /** The description. */
    private String description = "";

    /** The highlightedTextContent. */
    private String highlightedTextContent = "";

    /** The highlightedTextContentFile. */
    private String highlightedTextContentFile = "";

    /** The score. */
    private float score = 0;

    private int rank = 0;

    private int total = 0;

    private int totalHits = 0;

    /**
     * Gets the date modification.
     *
     * @return Returns the dateModification.
     */
    public String getDateModification() {
        return dateModification;
    }

    /**
     * Sets the date modification.
     *
     * @param dateModification
     *            The dateModification to set.
     */
    public void setDateModification(String dateModification) {
        this.dateModification = dateModification;
    }

    /**
     * Gets the description.
     *
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the url.
     *
     * @return Returns the url.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the url.
     *
     * @param url
     *            The url to set.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets the id fiche.
     *
     * @return Returns the idFiche.
     */
    public String getIdFiche() {
        return idFiche;
    }

    /**
     * Sets the id fiche.
     *
     * @param idFiche
     *            The idFiche to set.
     */
    public void setIdFiche(String idFiche) {
        this.idFiche = idFiche;
    }

    /**
     * Gets the langue.
     *
     * @return Returns the langue.
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Sets the langue.
     *
     * @param langue
     *            The langue to set.
     */
    public void setLangue(String langue) {
        this.langue = langue;
    }

    /**
     * Gets the objet.
     *
     * @return Returns the objet.
     */
    public String getObjet() {
        return objet;
    }

    /**
     * Sets the objet.
     *
     * @param objet
     *            The objet to set.
     */
    public void setObjet(String objet) {
        this.objet = objet;
    }

    /**
     * Gets the titre.
     *
     * @return Returns the titre.
     */
    public String getTitre() {
        return titre;
    }

    /**
     * Sets the titre.
     *
     * @param titre
     *            The titre to set.
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * Checks if is resultat fiche univ.
     *
     * @return true, if is resultat fiche univ
     */
    public boolean isResultatFicheUniv() {
        return idFiche != null && idFiche.length() > 0;
    }

    /**
     * Gets the highlighted text content.
     *
     * @return the highlighted text content
     */
    public String getHighlightedTextContent() {
        return highlightedTextContent;
    }

    /**
     * Sets the highlighted text content.
     *
     * @param highlightedTextContent
     *            the new highlighted text content
     */
    public void setHighlightedTextContent(String highlightedTextContent) {
        this.highlightedTextContent = highlightedTextContent;
    }

    /**
     * Gets the highlighted text content file.
     *
     * @return the highlighted text content file
     */
    public String getHighlightedTextContentFile() {
        return highlightedTextContentFile;
    }

    /**
     * Sets the highlighted text content file.
     *
     * @param highlightedTextContentFile
     *            the new highlighted text content file
     */
    public void setHighlightedTextContentFile(String highlightedTextContentFile) {
        this.highlightedTextContentFile = highlightedTextContentFile;
    }

    /**
     * Gets the score.
     *
     * @return the score
     */
    public float getScore() {
        return score;
    }

    /**
     * Sets the score.
     *
     * @param score
     *            the new score
     */
    public void setScore(float score) {
        this.score = score * 100;
    }

    public String getCodeRubrique() {
        return codeRubrique;
    }

    public void setCodeRubrique(String codeRubrique) {
        this.codeRubrique = codeRubrique;
    }

    public int getRank() {
        return rank;
    }

    public int getTotal() {
        return total;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int maxDoc) {
        this.totalHits = maxDoc;
    }

    public String getCodeFiche() {
        return codeFiche;
    }

    public void setCodeFiche(String codeFiche) {
        this.codeFiche = codeFiche;
    }

    public String getCodeRattachement() {
        return codeRattachement;
    }

    public void setCodeRattachement(String codeRattachement) {
        this.codeRattachement = codeRattachement;
    }

    public String getPublicVise() {
        return publicVise;
    }

    public void setPublicVise(String publicVise) {
        this.publicVise = publicVise;
    }

    public String getPublicViseRestriction() {
        return publicViseRestriction;
    }

    public void setPublicViseRestriction(String publicViseRestriction) {
        this.publicViseRestriction = publicViseRestriction;
    }

    public String getModeRestriction() {
        return modeRestriction;
    }

    public void setModeRestriction(String modeRestriction) {
        this.modeRestriction = modeRestriction;
    }
}
