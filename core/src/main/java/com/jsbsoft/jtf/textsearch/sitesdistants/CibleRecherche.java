/*
 * Created on 14 avr. 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.jsbsoft.jtf.textsearch.sitesdistants;

/**
 * The Class CibleRecherche.
 *
 * @author Administrateur
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 */
public class CibleRecherche {

    /** The code. */
    private String code;

    /** The libelle. */
    private String libelle;

    /**
     * Instantiates a new cible recherche.
     *
     * @param _code
     *            the _code
     * @param _libelle
     *            the _libelle
     */
    public CibleRecherche(String _code, String _libelle) {
        super();
        code = _code;
        libelle = _libelle;
    }

    /**
     * Gets the code.
     *
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            The code to set.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the libelle.
     *
     * @return Returns the libelle.
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets the libelle.
     *
     * @param libelle
     *            The libelle to set.
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
