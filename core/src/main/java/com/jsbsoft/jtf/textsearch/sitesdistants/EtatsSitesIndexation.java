package com.jsbsoft.jtf.textsearch.sitesdistants;

/**
 * Recense les etats d'indexation des sites.
 *
 * @author jbiard
 * @deprecated Utilisez l'enum {@link SiteIndexStates}
 */
@Deprecated
public interface EtatsSitesIndexation {

    /** The ETA t_ attente. */
    String ETAT_ATTENTE = "en attente";

    /** The ETA t_ indexatio n_ e n_ cours. */
    String ETAT_INDEXATION_EN_COURS = "indexation en cours";

    /** The ETA t_ termine. */
    String ETAT_TERMINE = "terminée";

    /** The ETA t_ no n_ lancee. */
    String ETAT_NON_LANCEE = "non exécutée";
}
