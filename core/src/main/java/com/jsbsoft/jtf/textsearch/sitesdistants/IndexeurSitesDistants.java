package com.jsbsoft.jtf.textsearch.sitesdistants;


import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.cluster.ClusterHelper;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.bean.SiteExterneBean;
import com.univ.objetspartages.services.ServiceSiteExterne;

/**
 * Singleton prennant en charge l'aspiration de sites distants, et l'indexation des pages. Code inspire de http://moguntia.ucd.ie/programming/webcrawler/ On a un index par site et
 * par langue. Pour eviter de referencer des pages qui n'existent plus, on repart de zéro.
 *
 * @author jbiard
 */
public class IndexeurSitesDistants {

    private static Logger logger = LoggerFactory.getLogger(IndexeurSitesDistants.class);

    /** The PARA m_ jt f_ nbthreads. */
    public static final String PARAM_JTF_NBTHREADS = "lucene.nb_threads_aspiration";

    /** The PARA m_ jt f_ timeout. */
    public static final String PARAM_JTF_TIMEOUT = "lucene.timeout_threads";

    /** */
    public static final String PARAM_JTF_MAXSIZE_HTMLQUEUE = "lucene.max_size_queue";

    /** */
    public static final String PARAM_JTF_TIME_SLEEP = "lucene.time_sleep";

    /** The N b_ ma x_ thread s_ defaut. */
    public static final int NB_MAX_THREADS_DEFAUT = 4;

    /** The TIMEOU t_ defaut. */
    public static final int TIMEOUT_DEFAUT = 10; // en s

    /** The _instance. */
    private static IndexeurSitesDistants _instance;

    /** The queue sites. */
    private final QueueSiteAIndexer queueSites;


    /** The n niveau courant profondeur. */
    private int nNiveauCourantProfondeur;

    /** The n niveau max profondeur. */
    private int nNiveauMaxProfondeur;

    /** The n nb threads courants. */
    private int nNbThreadsCourants;

    /** The n nb max threads. */
    private int nNbMaxThreads;

    /** The n timeout. */
    private int nTimeout;

    /** Le nombre max de page html contenu dans la queue de page html */
    private int maxSizeQueue;

    /** Le temps en millisecond d'attente du thread lorsque la queue de page est pleinne */
    private long timeToSleep;

    /** The b fin indexation. */
    private boolean bFinAspiration, bFinIndexation;

    /** The queue url. */
    private URLQueue queueURL;

    /** The queue html. */
    private QueueFluxHTML queueHTML;

    /** The pattern url refus. */
    private Pattern patternURLAcceptation, patternURLRefus;

    /** The sz url site. */
    private String szUrlSite;

    /** The asz url disallows. */
    private String[] aszUrlDisallows;

    /**
     * Constructeur prive. La classe est en fait un singleton.
     */
    private IndexeurSitesDistants() {
        bFinIndexation = true;
        queueSites = new QueueSiteAIndexer();
        // lecture du nombre de threads
        final String szNbThreadJTF = PropertyHelper.getCoreProperty(PARAM_JTF_NBTHREADS);
        if (szNbThreadJTF != null) {
            try {
                nNbMaxThreads = Integer.parseInt(szNbThreadJTF);
            } catch (final NumberFormatException e) {
                logger.debug("unable to parse the given value", e);
                nNbMaxThreads = NB_MAX_THREADS_DEFAUT;
            }
        } else {
            nNbMaxThreads = NB_MAX_THREADS_DEFAUT;
        }
        final String szTimeout = PropertyHelper.getCoreProperty(PARAM_JTF_TIMEOUT);
        if (szTimeout != null) {
            try {
                nTimeout = Integer.parseInt(szTimeout);
            } catch (final NumberFormatException e) {
                logger.debug("unable to parse the given value", e);
                nTimeout = TIMEOUT_DEFAUT;
            }
        } else {
            nTimeout = TIMEOUT_DEFAUT;
        }
        final String sMaxSizeQueue = PropertyHelper.getCoreProperty(PARAM_JTF_MAXSIZE_HTMLQUEUE);
        if (sMaxSizeQueue != null) {
            maxSizeQueue = Integer.parseInt(sMaxSizeQueue);
        } else {
            maxSizeQueue = 1000;
        }
        final String sTimeToSleep = PropertyHelper.getCoreProperty(PARAM_JTF_TIME_SLEEP);
        if (sTimeToSleep != null) {
            timeToSleep = Long.parseLong(sTimeToSleep);
        } else {
            timeToSleep = 30000L;
        }
        logger.debug("%%%% Nombre de threads affectes a l'aspiration: " + nNbMaxThreads + " - Timeout: " + nTimeout + "s");
    }

    /**
     * Renvoie l'unique instance (singleton).
     *
     * @return l'instance
     */
    public synchronized static IndexeurSitesDistants getInstance() {
        if (_instance == null) {
            _instance = new IndexeurSitesDistants();
        }
        return _instance;
    }

    public void setLogger(final Logger logger) {
        this.logger = logger;
    }

    /**
     * Lance l'indexation d'un site.
     *
     * @param site
     *            site a indexer
     *
     */
    public void indexeSite(final SiteExterneBean site, final boolean join) {
        try {
            queueSites.push(site);
            lanceIndexations(join);
        } catch (final InterruptedException e) {
            logger.error(e.getMessage(), e);
        } finally {
            // rechargement du singleton Searcher
            //TODO Elastic : Indexeur Site distant
            ClusterHelper.refresh(null, null);
        }
    }

    /**
     * Retourne pour chacun des sites son etat.
     *
     * @return map dont la clef est le libelle du site
     *
     * @see SiteIndexStates
     */
    public Map<Long, SiteIndexStates> getEtatsSites() {
        return queueSites.getEtats();
    }

    /**
     * Indexe l'ensemble des sites en base.
     *
     */
    public void indexeSites() {
        final ServiceSiteExterne serviceSiteExterne = ServiceManager.getServiceForBean(SiteExterneBean.class);
        List<SiteExterneBean> allSite = serviceSiteExterne.getAllExernalWebSite();
        for (SiteExterneBean site : allSite) {
            indexeSite(site, true);
        }
    }

    /**
     * Indexation en cours.
     *
     * @return true, if successful
     */
    public boolean indexationEnCours() {
        return !bFinIndexation;
    }

    /**
     * Gets the nb threads courants.
     *
     * @return the nb threads courants
     */
    public int getNbThreadsCourants() {
        return nNbThreadsCourants;
    }

    /**
     * Gets the nb max threads.
     *
     * @return the nb max threads
     */
    public int getNbMaxThreads() {
        return nNbMaxThreads;
    }

    /**
     * Gets the niveau courant profondeur.
     *
     * @return the niveau courant profondeur
     */
    public int getNiveauCourantProfondeur() {
        return nNiveauCourantProfondeur;
    }

    /**
     * Gets the niveau max profondeur.
     *
     * @return the niveau max profondeur
     */
    public int getNiveauMaxProfondeur() {
        return nNiveauMaxProfondeur;
    }

    /**
     * Indique si une url doit etre aspiree ou non (suivant le robots.txt)
     *
     * @param szUrl
     *            url a valider ou non
     *
     * @return vrai si l'url est acceptable
     */
    public boolean accepteUrlRobots(final String szUrl) {
        if (aszUrlDisallows == null) {
            return true;
        }
        for (final String aszUrlDisallow : aszUrlDisallows) {
            if (szUrl.contains(aszUrlDisallow)) {
                logger.debug("\t!!URL refusee (robots.txt) : " + szUrl);
                return false;
            }
        }
        return true;
    }

    /**
     * Renvoie le pattern correspondant a l'expression reguliere validant les url.
     *
     * @return pattern, null si non definie
     */
    public Pattern getPatternURLAcceptation() {
        return patternURLAcceptation;
    }

    /**
     * Renvoie le pattern correspondant a l'expression reguliere refusant les url.
     *
     * @return pattern, null si non definie
     */
    public Pattern getPatternURLRefus() {
        return patternURLRefus;
    }

    /**
     * Lance le thread dedie a l'indexation. Permet un traitement en tâche de fond.
     *
     * @throws InterruptedException
     */
    protected void lanceIndexations(final boolean join) throws InterruptedException {
        // TODO
    }



}
