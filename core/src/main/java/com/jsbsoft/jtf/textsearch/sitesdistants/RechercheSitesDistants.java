package com.jsbsoft.jtf.textsearch.sitesdistants;

import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Singleton prennant en charge la recherche dans les pages indexees des sites distants.
 *
 * @author jbiard
 */
public class RechercheSitesDistants {

    /** The lst repertoire recherche. */
    private static List<File> lstRepertoireRecherche = null;

    /** The lst cible. */
    private static Collection<CibleRecherche> lstCible = null;

    /**
     * Inits the.
     */
    public static void init() {

    }

    /**
     * Gets the cible.
     *
     * @param code
     *            the code
     *
     * @return the cible
     */
    public static CibleRecherche getCible(final String code) {
        if (lstCible == null) {
            init();
        }
        final Iterator<CibleRecherche> it = lstCible.iterator();
        CibleRecherche cible = null;
        while (it.hasNext()) {
            cible = it.next();
            if (cible.getCode().equals(code)) {
                return cible;
            }
        }
        return null;
    }

    /**
     * Gets the lst cible.
     *
     * @return the lst cible
     *
     */
    public Collection<CibleRecherche> getLstCible() {
        if (lstCible == null) {
            init();
        }
        return lstCible;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.textsearch.Recherche#getLstRepertoireRecherche()
     */
    public Collection<File> getLstRepertoireRecherche() {
        if (lstRepertoireRecherche == null) {
            init();
        }
        return lstRepertoireRecherche;
    }
}
