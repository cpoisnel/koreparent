package com.jsbsoft.jtf.textsearch.sitesdistants;

import com.kportal.core.config.MessageHelper;

/**
 * Created on 09/09/15.
 */
public enum SiteIndexStates {
    WAITING("BO.SITE_STATES.WAITING"),
    RUNNING("BO.SITE_STATES.RUNNING"),
    DONE("BO.SITE_STATES.DONE"),
    NOT_DONE("BO.SITE_STATES.NOT_DONE");

    private String state;

    SiteIndexStates(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return MessageHelper.getCoreMessage(state);
    }
}
