package com.jsbsoft.jtf.textsearch.sitesdistants;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Queue thread safe permettant l'ajout et la recuperation de flux HTML a indexer. Visibilité réduite au package.
 *
 * @author jbiard
 */
class QueueFluxHTML {

    /** The ll flux html. */
    private final LinkedList<FluxHTML> llFluxHTML;

    /** The gathered links. */
    Set<String> gatheredLinks;

    /**
     * Instantiates a new queue flux html.
     */
    public QueueFluxHTML() {
        gatheredLinks = new HashSet<>();
        llFluxHTML = new LinkedList<>();
    }

    /**
     * Gets the taille.
     *
     * @return the taille
     */
    public int getTaille() {
        return llFluxHTML.size();
    }

    /**
     * Ajoute une page a indexer.
     *
     * @param szPage
     *            page sous forme de chaine
     * @param szUrl
     *            url correspondant
     */
    public synchronized void push(String szPage, String szUrl) {
        if (gatheredLinks.add(szUrl)) {
            llFluxHTML.addLast(new FluxHTML(szPage, szUrl));
        }
    }

    /**
     * Depile un flux a indexer.
     *
     * @return flux
     */
    public synchronized FluxHTML pop() {
        if (llFluxHTML.size() > 0) {
            return llFluxHTML.removeFirst();
        }
        return null;
    }

    /**
     * Encapsule le flux et l'url d'une page.
     *
     * @author jbiard
     */
    public static class FluxHTML {

        /** The sz page. */
        private final String szPage;

        /** The sz url. */
        private final String szUrl;

        /**
         * Instantiates a new flux html.
         *
         * @param szPage
         *            the sz page
         * @param szUrl
         *            the sz url
         */
        FluxHTML(String szPage, String szUrl) {
            this.szPage = szPage;
            this.szUrl = szUrl;
        }

        /**
         * Gets the page.
         *
         * @return the page
         */
        public String getPage() {
            return szPage;
        }

        /**
         * Gets the url.
         *
         * @return the url
         */
        public String getUrl() {
            return szUrl;
        }
    }
}