package com.jsbsoft.jtf.textsearch.sitesdistants;

import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;

import com.univ.objetspartages.bean.SiteExterneBean;

/**
 * Queue permettant de stocker les sites a indexer.
 *
 * @author jbiard
 */
class QueueSiteAIndexer {

    /** The ll sites. */
    private final LinkedList<SiteExterneBean> llSites;

    /** The map sites. */
    private final Map<Long, SiteIndexStates> mapSites;

    /**
     * Instantiates a new queue site a indexer.
     */
    QueueSiteAIndexer() {
        llSites = new LinkedList<>();
        mapSites = new Hashtable<>();
    }

    /**
     * Ajoute un site et le passe en etat attente.
     *
     * @param site
     *            site a indexer
     */
    synchronized void push(final SiteExterneBean site) {
        final Long lgIdSite = site.getIdSite();
        final SiteIndexStates szEtat = mapSites.get(lgIdSite);
        // si on a deja le site on ne le reinjecte que si son indexation est terminee
        if (szEtat != null) {
            if (szEtat.equals(SiteIndexStates.DONE)) {
                mapSites.remove(lgIdSite);
            } else {
                return;
            }
        }
        llSites.addLast(site);
        mapSites.put(lgIdSite, SiteIndexStates.WAITING);
    }

    /**
     * Recupere le prochain site a indexer. Passage de son etat en "indexation en cours".
     *
     * @return site
     */
    synchronized SiteExterneBean pop() {
        if (llSites.size() > 0) {
            final SiteExterneBean site = llSites.removeFirst();
            final Long lgIdSite = site.getIdSite();
            mapSites.put(lgIdSite, SiteIndexStates.RUNNING);
            // en cours indexation
            return site;
        }
        return null;
    }

    /**
     * Indique pour un site que son indexation est terminee.
     *
     * @param site
     *            site pour lequel l'indexation est terminee
     */
    synchronized void finIndexation(final SiteExterneBean site) {
        final Long lgIdSite = site.getIdSite();
        if (mapSites.get(lgIdSite) != null) {
            mapSites.remove(lgIdSite);
            mapSites.put(lgIdSite, SiteIndexStates.DONE);
        }
    }

    /**
     * Renvoie les etats courant des sites.
     *
     * @return map dont la clef est le libelle du site
     */
    Map<Long, SiteIndexStates> getEtats() {
        return Collections.unmodifiableMap(mapSites);
    }
}
