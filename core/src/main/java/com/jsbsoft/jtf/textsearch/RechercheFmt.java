package com.jsbsoft.jtf.textsearch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.webutils.FormateurHTML;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.plugin.toolbox.PluginTagHelper;
import com.kportal.tag.util.ContexteTagUtil;
import com.univ.utils.Chaine;
import com.univ.utils.EscapeString;
import com.univ.utils.URLResolver;

/**
 * Fonction de formatage des JSP pour le front-office.
 */
public class RechercheFmt {

    private static final Logger LOG = LoggerFactory.getLogger(RechercheFmt.class);

    /**
     * Formatage d'une chaine en HTML - conversion des "\n" en <BR>
     * - interprétation des liens hypertexte.
     *
     * @param ctx
     *            the ctx
     * @param texteAFormater
     *            the texteAFormater
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public static String formaterEnHTML(final OMContext ctx, String texteAFormater) throws Exception {
        String texte = PluginTagHelper.interpreterTags(texteAFormater, ContexteTagUtil.INDEXATION);
        texte = StringUtils.defaultString(texte);
        int indexTexte = 0;
        int indexDebutMotCle = -1;
        StringBuilder newTexte = new StringBuilder(10000);
        //on n'indexe pas les images
        while ((indexDebutMotCle = texte.indexOf("[id-image]", indexTexte)) != -1) {
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            int indexFinMotCle = texte.indexOf("[/id-image]", indexDebutMotCle);
            if (indexFinMotCle == -1) {
                break;
            }
            final int indexDebutMotCleSuivant = texte.indexOf("[id-image]", indexDebutMotCle + 1);
            if ((indexDebutMotCleSuivant != -1) && (indexDebutMotCleSuivant < indexFinMotCle)) {
                break;
            }
            indexTexte = indexFinMotCle + 11;
        }
        newTexte.append(texte.substring(indexTexte));
        //on n'indexe pas les fiches
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuilder(10000);
        while ((indexDebutMotCle = texte.indexOf("[id-fiche]", indexTexte)) != -1) {
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            int indexFinMotCle = texte.indexOf("[/id-fiche]", indexDebutMotCle);
            if (indexFinMotCle == -1) {
                break;
            }
            final int indexDebutMotCleSuivant = texte.indexOf("[id-fiche]", indexDebutMotCle + 1);
            if ((indexDebutMotCleSuivant != -1) && (indexDebutMotCleSuivant < indexFinMotCle)) {
                break;
            }
            indexTexte = indexFinMotCle + 11;
        }
        newTexte.append(texte.substring(indexTexte));
        //on n'indexe pas les documents
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuilder(1000);
        while ((indexDebutMotCle = texte.indexOf("[id-document]", indexTexte)) != -1) {
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            int indexFinMotCle = texte.indexOf("[/id-document]", indexDebutMotCle);
            if (indexFinMotCle == -1) {
                break;
            }
            final int indexDebutMotCleSuivant = texte.indexOf("[id-document]", indexDebutMotCle + 1);
            if ((indexDebutMotCleSuivant != -1) && (indexDebutMotCleSuivant < indexFinMotCle)) {
                break;
            }
            indexTexte = indexFinMotCle + 15;
        }
        newTexte.append(texte.substring(indexTexte));
        //on n'indexe pas les liens externes
        //on indexe les insertions d'urls
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuilder();
        /* Boucle sur chaque mot clé */
        while ((indexDebutMotCle = texte.indexOf("[url", indexTexte)) != -1) {
            // Recopie portion avant le mot- clé
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            int indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            final String contenuTag = Chaine.remplacerPointsVirgules(texte.substring(indexDebutMotCle + 1, indexFinMotCle));
            String sUrl = "";
            final StringBuilder data = new StringBuilder();
            final StringTokenizer st = new StringTokenizer(contenuTag, "*");
            int indiceToken = 0;
            while (st.hasMoreTokens()) {
                final String itemTag = st.nextToken();
                if (indiceToken == 1) {
                    sUrl = itemTag;
                }
                indiceToken++;
            }
            if (sUrl.length() > 0) {
                // Si l'url commence par /, on rajoute le host
                if (sUrl.charAt(0) == '/') {
                    sUrl = URLResolver.getAbsoluteUrl(sUrl, ctx);
                }
                try {
                    final URL url = new URL(sUrl);
                    final URLConnection urlConnection = url.openConnection();
                    if (!(urlConnection instanceof HttpURLConnection)) {
                        urlConnection.setConnectTimeout(3000);
                        String ligne = "";
                        final BufferedReader flux = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        while ((ligne = flux.readLine()) != null) {
                            data.append(ligne);
                        }
                        flux.close();
                    }
                } catch (final IOException e) {
                    LOG.error("erreur lors du traitement de l'url", e);
                }
                newTexte.append(data.toString());
            }
            indexTexte = indexFinMotCle + 1;
        }
        newTexte.append(texte.substring(indexTexte));
        //on remplace tous les tags [titre;MON_TITRE] par MON_TITRE
        texte = newTexte.toString();
        newTexte = new StringBuilder(10000);
        newTexte.append(texte.replaceAll("\\[titre;(.+?)\\]", "$1"));
        newTexte.append(texte.replaceAll("\\[style;(.+?)\\]", "$1"));
        /*****************************/
        /* Transformation codes HTML */
        /*****************************/
        texte = FormateurHTML.formaterEnHTML(newTexte.toString());
        /* Indexation des pages statiques */
        indexTexte = 0;
        newTexte = new StringBuilder(10000);
        while ((indexDebutMotCle = texte.indexOf("[page;", indexTexte)) != -1) {
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            int indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            final String contenuTag = Chaine.remplacerPointsVirgules(texte.substring(indexDebutMotCle + 1, indexFinMotCle));
            String nomPage = "";
            final StringTokenizer st = new StringTokenizer(contenuTag, "*");
            int indiceToken = 0;
            while (st.hasMoreTokens()) {
                final String itemTag = st.nextToken();
                if (indiceToken == 1) {
                    nomPage = itemTag;
                }
                indiceToken++;
            }
            final String nomFichier = WebAppUtil.getAbsolutePath() + File.separator + StringUtils.replace(nomPage, "/", File.separator);
            /* lecture du fichier */
            final File f = new File(nomFichier);
            byte[] data = new byte[0];
            if (f.exists()) {
                final int l = (int) f.length();
                data = new byte[l];
                int bytesRead = 0;
                try (final FileInputStream input = new FileInputStream(f);) {
                    while (bytesRead < l) {
                        bytesRead += input.read(data);
                    }
                    input.close();
                }
            }
            newTexte.append(new String(data));
            indexTexte = indexFinMotCle + 1;
            /* AJOUT JSS 20020503-001 */
            // Suppression de la fin de la ligne (éviter de rajouter des <br>
            // notamment pour les tétières
            final int indexBR = texte.indexOf("<BR>", indexTexte);
            if (indexBR != -1) {
                indexTexte = indexBR + 4;
            }
        }
        if (indexTexte < texte.length()) {
            newTexte.append(texte.substring(indexTexte));
        }
        texte = new String(newTexte);
        indexTexte = 0;
        newTexte = new StringBuilder(10000);
        /* Boucle sur chaque mot clé */
        while ((indexDebutMotCle = texte.indexOf("[", indexTexte)) != -1) {
            // Recopie portion avant le mot- clé
            newTexte.append(texte.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            int indexFinMotCle = texte.indexOf("]", indexDebutMotCle);
            if (indexFinMotCle == -1) {
                return "";
            }
            newTexte.append(" ");
            indexTexte = indexFinMotCle + 1;
        }
        // Recopie dernière portion de texte
        newTexte.append(texte.substring(indexTexte));
        return newTexte.toString();
    }
    //RP2005 optimisation de la méthode (et appel en plus directement sur les fichier textes)

    /**
     * Formater byte recherche.
     *
     * @param buff
     *            the buff
     * @param count
     *            the count
     *
     * @return the string
     *
     */
    public static String formaterByteRecherche(final byte[] buff, final int count) {
        final CharBuffer ch = Charset.defaultCharset().decode(ByteBuffer.wrap(buff));
        final StringBuilder sb = new StringBuilder(count);
        // permet de savoir si le char precedent etait un bin
        boolean bin = false;
        // permet de savoir si le char precedent etait un blanc mais non ajoute
        boolean espace = false;
        for (int i = 0; i < count; i++) {
            final char c = ch.get(i);
            char c2 = '?';
            char c3 = '?';
            if (c >= 'a' && c <= 'z' || c == ' ') {
                c2 = c;
            } else if (c >= 'A' && c <= 'Z') {
                c2 = Character.toLowerCase(c);
            } else if (c >= '0' && c <= '9') {
                c2 = c;
            } else if (c == '-' || c == '_' || c == '@' || c == '.') {
                c2 = c;
            } else if (c == 'À' || c == 'Á' || c == 'Â' || c == 'à' || c == 'á' || c == 'â') {
                c2 = 'a';
            } else if (c == 'È' || c == 'É' || c == 'Ê' || c == 'Ë' || c == 'è' || c == 'é' || c == 'ê' || c == 'ë') {
                c2 = 'e';
            } else if (c == 'Ì' || c == 'Í' || c == 'Î' || c == 'Ï' || c == 'ì' || c == 'í' || c == 'î' || c == 'ï') {
                c2 = 'i';
            } else if (c == 'Ò' || c == 'Ó' || c == 'Ô' || c == 'ò' || c == 'ó' || c == 'ô') {
                c2 = 'o';
            } else if (c == 'Ù' || c == 'Ú' || c == 'Û' || c == 'Ü' || c == 'ù' || c == 'ú' || c == 'û' || c == 'ü') {
                c2 = 'u';
            } else if (c == 'Ç' || c == 'ç') {
                c2 = 'c';
            } else if (c == 'Ñ' || c == 'ñ') {
                c2 = 'n';
            } else if (c == 'Æ' || c == 'æ') {
                c2 = 'a';
                c3 = 'e';
            } else if (c == '' || c == '') {
                c2 = 'o';
                c3 = 'e';
            }
            if (c2 != '?') {
                sb.append(c2);
                if (c3 != '?') {
                    sb.append(c3);
                }
                bin = false;
                espace = false;
            } else {
                // si le char prec est un bin et on a retenu un blanc
                if (bin && espace) {
                    sb.append(' ');
                    espace = false;
                } else if (!bin) {
                    // le char d'avant n'est pas un bin on retient un blanc possible
                    espace = true;
                }
                // le char est un bin
                bin = true;
            }
        }
        return sb.toString();
    }

    /**
     * Formater texte recherche.
     *
     * @param contenu
     *            the contenu
     * @param recherche
     *            the recherche
     *
     * @return the string
     */
    public static String formaterTexteRecherche(final String contenu, final boolean recherche) {
        return formaterTexteRecherche(contenu, recherche, true);
    }

    /**
     * Formater texte recherche.
     *
     * @param contenu
     *            the contenu
     *
     * @return the string
     */
    public static String formaterTexteRecherche(final String contenu) {
        return formaterTexteRecherche(contenu, false, true);
    }

    /**
     * RP2005 optimisation de la méthode (et appel en plus directement sur les fichier textes) Formate le texte recherché pour lucene et pour la recherche avancée.
     *
     * @param contenu
     *            the contenu
     * @param recherche
     *            the recherche
     * @param toLowerCase
     *            the to lower case
     *
     * @return the string
     */
    public static String formaterTexteRecherche(final String contenu, final boolean recherche, final boolean toLowerCase) {
        if (contenu == null) {
            return "";
        }
        if (recherche) {
            return EscapeString.escapeURL(contenu);
        }
        final int lg = contenu.length();
        final StringBuilder sb = new StringBuilder(lg);
        boolean bin = false;
        for (int i = 0; i < lg; i++) {
            final char c = contenu.charAt(i);
            char c2 = '?';
            char c3 = '?';
            if (c >= 'a' && c <= 'z') {
                c2 = c;
            } else if (c >= 'A' && c <= 'Z') {
                if (toLowerCase) {
                    c2 = Character.toLowerCase(c);
                } else {
                    c2 = c;
                }
            } else if (c >= '0' && c <= '9') {
                c2 = c;
            } else if (c == '-' || c == '_' || c == '@' || c == '.' || c == '*') {
                c2 = c;
            } else if (c == 'À' || c == 'Á' || c == 'Â' || c == 'à' || c == 'á' || c == 'â') {
                c2 = c;
            } else if (c == 'È' || c == 'É' || c == 'Ê' || c == 'Ë' || c == 'è' || c == 'é' || c == 'ê' || c == 'ë') {
                c2 = c;
            } else if (c == 'Ì' || c == 'Í' || c == 'Î' || c == 'Ï' || c == 'ì' || c == 'í' || c == 'î' || c == 'ï') {
                c2 = c;
            } else if (c == 'Ò' || c == 'Ó' || c == 'Ô' || c == 'ò' || c == 'ó' || c == 'ô') {
                c2 = c;
            } else if (c == 'Ù' || c == 'Ú' || c == 'Û' || c == 'Ü' || c == 'ù' || c == 'ú' || c == 'û' || c == 'ü') {
                c2 = c;
            } else if (c == 'Ç' || c == 'ç') {
                c2 = c;
            } else if (c == 'Ñ' || c == 'ñ') {
                c2 = c;
            } else if (c == 'Æ' || c == 'æ') {
                c2 = 'a';
                c3 = 'e';
            } else if (c == '' || c == '') {
                c2 = 'o';
                c3 = 'e';
            } else if (c == '\'') {
                c2 = c;
            }
            if (c2 != '?') {
                sb.append(c2);
                if (c3 != '?') {
                    sb.append(c3);
                }
                bin = false;
            } else {
                if (!bin) {
                    sb.append(' ');
                }
                bin = true;
            }
        }
        return sb.toString();
    }
}
