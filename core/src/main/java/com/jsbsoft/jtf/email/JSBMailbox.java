package com.jsbsoft.jtf.email;

import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.kportal.core.config.PropertyHelper;

/**
 * The Class JSBMailbox.
 */
public class JSBMailbox {

    /** The Constant PARAM_JTF_MAIL_HOST. */
    public final static String PARAM_JTF_MAIL_HOST = "mail.host";

    /** The Constant PARAM_JTF_MAIL_PORT. */
    public final static String PARAM_JTF_MAIL_PORT = "mail.port";

    /** The Constant PARAM_JTF_MAIL_USER. */
    public final static String PARAM_JTF_MAIL_USER = "mail.user";

    /** The Constant PARAM_JTF_MAIL_PASSWORD. */
    public final static String PARAM_JTF_MAIL_PASSWORD = "mail.password";

    /** The Constant PARAM_JTF_MAIL_FROM. */
    public final static String PARAM_JTF_MAIL_FROM = "mail.from";

    /** The Constant PARAM_JTF_MAIL_WEBMASTER. */
    public final static String PARAM_JTF_MAIL_WEBMASTER = "mail.webmaster";

    /** The Constant PARAM_JTF_MAIL_TLS. */
    public final static String PARAM_JTF_MAIL_TLS = "mail.enabletls";

    /** The _log. */
    private static final Logger LOG = LoggerFactory.getLogger(JSBMailbox.class);

    /** Le host mail a utiliser pour les envois. Doit posseder un serveur smtp qui nous reconnait comme pouvant envoyer des messages avec l'adresse de retour (precisee dessous). */
    private String host;

    /** The port. */
    private int port;

    /** Le hote mail a utiliser pour les envois. Doit posseder un serveur smtp qui nous reconnait comme pouvant envoyer des messages avec l'adresse de retour (precisee dessous). */
    private String user;

    /** Le mot de passe pour la connexion au serveur SMTP */
    private String password;

    /** Est-ce que l'on est en mode deboggage ou pas?. */
    private boolean debug = true;

    /**
     * Constructeur qui permet de fixer la variable de deboggage. Classe instanciee sans fichier d'envois.
     *
     * @param _debug the _debug
     */
    public JSBMailbox(final boolean _debug) {
        String debug = PropertyHelper.getCoreProperty("mail.debug");
        if (debug == null) {
            debug = "false";
        }
        this.debug = _debug || "true".equalsIgnoreCase(debug);
        init();
    }

    /**
     * Initialisation des variables.
     */
    private void init() {
        LOG.debug("Initialisation session SMTP ");
        if (!debug) {
            LOG.debug("MAIL_INITIALISE L'envoi débute");
            // hote
            this.host = PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_HOST);
            // port, par défaut 25
            final String portJtf = PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_PORT);
            if (StringUtils.isNumeric(portJtf)) {
                port = Integer.parseInt(portJtf);
            } else {
                port = 25;
            }
            // émetteur
            this.user = PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_USER);
            //on verifie s'il y a un mot de passe sur le smtp
            this.password = "";
            if (PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_PASSWORD) != null) {
                this.password = PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_PASSWORD);
            }
        }
    }

    /**
     *
     * @param from Email expéditeur
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @throws MessagingException
     * @throws EmailException
     * @deprecated Cette méthode n'est plus utilisée, à remplacer par sendTxtMsg (format text/plain)
     */
    @Deprecated
    public void sendMsg(final String from, final String to, final String subject, final String message) throws MessagingException, EmailException {
        sendTxtMsg(from, to, subject, message);
    }

    /**
     *
     * @param from Email expéditeur
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @throws MessagingException
     * @throws EmailException
     * @deprecated Cette méthode n'est plus utilisée, à remplacer par sendHtmlMsg (format text/html)
     */
    @Deprecated
    public void sendMsg(final String from, final String to, final String subject, final String message, final String typeMime) throws MessagingException, EmailException {
        sendHtmlMsg(from, to, subject, message);
    }

    /**
     * Envoie un message au format text/plain, avec possibilité de spécifier l'expéditeur.
     *
     * @param from Email expéditeur
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @throws MessagingException the messaging exception
     * @throws EmailException the email exception
     */
    public void sendTxtMsg(final String from, final String to, final String subject, final String message) throws MessagingException, EmailException {
        final String[] tabTo = {to, to};
        sendMsgWithAttachedFiles(from, tabTo, subject, message, "text/plain", null, null);
    }

    /**
     * Envoie un message au format text/html, avec possibilité de spécifier l'expéditeur.
     *
     * @param from Email expéditeur
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @throws MessagingException the messaging exception
     * @throws EmailException the email exception
     */
    public void sendHtmlMsg(final String from, final String to, final String subject, final String message) throws MessagingException, EmailException {
        final String[] tabTo = {to, to};
        sendMsgWithAttachedFiles(from, tabTo, subject, message, "text/html", null, null);
    }

    /**
     * Envoie un message système au format text/plain.
     * L'expéditeur est l'adresse mail définie dans le paramètre mail.from de l'application.
     *
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @throws MessagingException
     * @throws EmailException
     */
    public void sendSystemMsg(final String to, final String subject, final String message) throws MessagingException, EmailException {
        sendTxtMsg(PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_FROM), to, subject, message);
    }

    /**
     * Envoie un message du webmaster au format text/plain.
     * L'expéditeur est l'adresse mail définie dans le paramètre mail.webmaster de l'application.
     *
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @throws MessagingException
     * @throws EmailException
     */
    public void sendWmasterMsg(final String to, final String subject, final String message) throws MessagingException, EmailException {
        sendTxtMsg(PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_WEBMASTER), to, subject, message);
    }

    /**
     * Send msg with attached files.
     *
     * @param from Email expéditeur
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @param filepaths Liste des pièces jointes
     * @throws MessagingException
     * @throws EmailException
     * @deprecated Cette méthode n'est plus utilisée, à remplacer par sendTxtMsgWithAttachedFiles (format text/html)
     */
    @Deprecated
    public void sendMsgWithAttachedFiles(final String from, final String to, final String subject, final String message, final String filepaths) throws MessagingException, EmailException {
        sendTxtMsgWithAttachedFiles(from, to, subject, message, new String[] {filepaths});
    }

    /**
     * Envoie un message au format text/plain, avec PJ et possibilité de spécifier l'expéditeur.
     *
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @param filepaths Liste des pièces jointes
     * @throws MessagingException
     * @throws EmailException
     */
    public void sendTxtMsgWithAttachedFiles(final String from, final String to, final String subject, final String message, final String[] filepaths) throws MessagingException, EmailException {
        sendMsgWithAttachedFiles(from, new String[] {to, to}, subject, message, "text/plain", filepaths, null);
    }

    /**
     * Envoie un message au format text/plain, avec PJ et possibilité de spécifier l'expéditeur.
     *
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @param filepaths Liste des pièces jointes
     * @throws MessagingException
     * @throws EmailException
     */
    public void sendHtmlMsgWithAttachedFiles(final String from, final String to, final String subject, final String message, final String[] filepaths) throws MessagingException, EmailException {
        sendMsgWithAttachedFiles(from, new String[] {to, to}, subject, message, "text/html", filepaths, null);
    }

    /**
     * Envoie un message système avec PJ au format text/plain.
     * L'expéditeur est l'adresse mail définie dans le paramètre mail.from de l'application.
     *
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @param filepaths Liste des pièces jointes
     * @throws MessagingException
     * @throws EmailException
     */
    public void sendSystemMsgWithAttachedFiles(final String to, final String subject, final String message, final String[] filepaths) throws MessagingException, EmailException {
        sendTxtMsgWithAttachedFiles(PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_FROM), to, subject, message, filepaths);
    }

    public void sendMsgWithAttachedFiles(final String _from, final String[] _to, final String _subject, final String _texte, final String _typeMime, final String[] _filepaths, final String[] _filenames, final boolean _newsletter) throws Exception {
        sendMsgWithAttachedFiles(_from, _to, _subject, _texte, _typeMime, _filepaths, _filenames);
    }

    public void sendMsgWithAttachedFiles(final String[] _from, final String[] _to, final String _subject, final String _texte, final String _typeMime, final String[] _filepaths, final String[] _filenames, final boolean _newsletter) throws MessagingException, EmailException {
        sendMsgWithAttachedFiles(_from[0], _to, _subject, _texte, _typeMime, _filepaths, _filenames);
    }

    /**
     * Fonction permettant l'envoi d'un courrier electronique RFC 822 avec fichier attachés.
     *
     * @param from Email expéditeur
     * @param to Email destinataire
     * @param subject Objet du message
     * @param message Contenu du message
     * @param typeMime Type mime du message
     * @param filepaths Liste des pièces jointes
     * @throws MessagingException
     * @throws EmailException
     */
    public void sendMsgWithAttachedFiles(final String from, final String[] to, final String subject, final String message, final String typeMime, final String[] filepaths, final String[] filenames) throws MessagingException, EmailException {
        sendMsgWithAttachedFiles(from, to, null, null, subject, message, typeMime, filepaths, filenames);
    }

    /**
     * Fonction permettant l'envoi d'un courrier electronique RFC 822 avec
     * fichier attachés.
     *
     * @param from Email expéditeur
     * @param to Email destinataire
     * @param cc Email des copies carbones, tableau comprenant le mail en première position et le nom du destinataire en seconde (non obligatoire)
     * @param cci Email des copies carbones cachées, tableau comprenant le mail en première position et le nom du destinataire en seconde (non obligatoire)
     * @param subject Objet du message
     * @param message Contenu du message
     * @param typeMime Type mime du message
     * @param filepaths Liste des pièces jointes
     * @throws MessagingException
     * @throws EmailException
     */
    public void sendMsgWithAttachedFiles(final String from, final String[] to, final List<String[]> cc, final List<String[]> cci, String subject, final String message, final String typeMime, final String[] filepaths, final String[] filenames) throws MessagingException, EmailException {
        LOG.debug("Envoi mail : [" + to[0] + "]");
        if (debug) {
            LOG.debug("from : " + from);
            LOG.debug("subject : " + subject);
            LOG.debug("text :\n" + message);
        }
        if (!debug) {
            final String mailDest = to[0];
            final String nomDest = to[1];
            /* ** Version Type MIME ** */
            //selon l'historique, le type mime vaut : text/plain, text/html, 0 (html) ou 1 (plain)
            boolean sendHtml = "text/html".equals(typeMime) || "0".equals(typeMime);
            /* ** Version Fichiers attachés ** */
            Email email = null;
            if (filepaths != null && filepaths.length > 0) {
                // Create the email message
                email = new MultiPartEmail();
                //on positionne le texte avant la pièce jointe dans le mail
                if (sendHtml) {
                    final MimeMultipart mmp = new MimeMultipart();
                    final MimeBodyPart bp = new MimeBodyPart();
                    bp.setContent(message, "text/html;charset=" + CharEncoding.DEFAULT.toLowerCase());
                    mmp.addBodyPart(bp);
                    ((MultiPartEmail) email).addPart(mmp);
                } else {
                    email.setMsg(message);
                }
                // Create the attachment
                EmailAttachment attachment;
                for (int i = 0; i < filepaths.length; i++) {
                    attachment = new EmailAttachment();
                    attachment.setPath(filepaths[i]);
                    attachment.setDisposition(EmailAttachment.ATTACHMENT);
                    if (filenames != null && filenames.length == filepaths.length) {
                        attachment.setName(filenames[i]);
                    }
                    // add the attachment
                    ((MultiPartEmail) email).attach(attachment);
                }
            } else if (sendHtml) {
                // Create the email message
                email = new HtmlEmail();
                // set the html message
                // email.setHtmlMsg(_texte);
                email.setContent(message, "text/html;charset=" + CharEncoding.DEFAULT.toLowerCase());
                // set the alternative message
                ((HtmlEmail) email).setTextMsg("Votre client ne supporte pas les messages HMTL. Pour consultez la newsletter, rendez-vous sur la page ");
            } else {
				/* ** Version Simple ** */
                email = new SimpleEmail();
                // test pour eviter org.apache.commons.mail.EmailException: Invalid message supplied
                email.setMsg(message.length() > 0 ? message : " ");
            }
            // Ajout des personnes en copie
            if (cc != null) {
                for (String[] mailCc : cc) {
                    if (mailCc.length > 1) {
                        email.addCc(mailCc[0], mailCc[1]);
                    } else if (mailCc.length == 1) {
                        email.addCc(mailCc[0]);
                    }
                }
            }
            // Ajout des personnes en copie caché
            if (cci != null) {
                for (String[] mailCci : cci) {
                    if (mailCci.length > 1) {
                        email.addBcc(mailCci[0], mailCci[1]);
                    } else if (mailCci.length == 1) {
                        email.addBcc(mailCci[0]);
                    }
                }
            }
            sendMessage(email, mailDest, nomDest, from, subject);
        }
    }

    /**
     * Send message.
     *
     * @param email
     *            the email
     * @param to
     *            the to
     * @param nomto
     *            the nomto
     * @param from
     *            the from
     * @param subject
     *            the subject
     *
     * @throws EmailException
     *             the email exception
     */
    private void sendMessage(final Email email, final String to, final String nomto, final String from, final String subject) throws EmailException {
        email.setHostName(host);
        email.setSmtpPort(port);
        if (!"".equals(this.user)) {
            email.setAuthentication(this.user, this.password);
        }
        if ("".equals(to)) {
            LOG.error("adresse destinataire non renseignee, envoi mail avorte.");
            return;
        }
        email.addTo(to, nomto);
        email.setFrom(from != null ? from : PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_FROM));
        email.setSubject(subject);
        email.setSentDate(new Date());
        email.setCharset(CharEncoding.DEFAULT);
        if ("true".equals(PropertyHelper.getCoreProperty(PARAM_JTF_MAIL_TLS))) {
            email.setStartTLSEnabled(true);
        }
        // send the email
        email.send();
    }
}
