package com.jsbsoft.jtf.upload;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.kportal.core.config.PropertyHelper;

/**
 * ExtendedRequest is a wrapper object around an HttpServletRequest object, providing multipart/form-data support and convenient getXXXParameter functions.
 *
 * <P>
 * When an ExtendedRequest object is created, and the request content-type is multipart/form-data, the data is parsed, and subsequent calls to ExtendedRequest.getParameter() work
 * as expected. Use getFileParameter() to retrieve file upload data.
 *
 * <P>
 * Additionally ExtendedRequest provides convenient retrieval functions like getBooleanParameter, getIntParameter and getDateParameter.
 *
 * <P>
 * All other methods just pass data on to the HttpServletRequest object. This only works with JSDK2.0. You may need to add or remove a number of dummy functions for other versions
 * of the JSDK.
 */
public class ExtendedRequest implements HttpServletRequest {

    private static final Logger LOG = LoggerFactory.getLogger(ExtendedRequest.class);

    /** The orig req. */
    private final HttpServletRequest request;

    /** The params. */
    private HashMap<String, Object> params;

    /**
     * Creates a ExtendedRequest object. If the input is of type multipart/form-data the input is parsed, and any uploaded file objects will be saved to a temporary directory.
     * Remember to call <CODE>deleteTemporaryFiles()</CODE> when you are done, to delete these temporary files.
     *
     * @param req
     *            is the request object passed to the <CODE>service()</CODE>, <CODE>doGet()</CODE> or <CODE>doPost()</CODE> method.
     * @param tmpDir
     *            is the directory that may be used to hold temporary files.
     * @param res
     *            the res
     *
     * @throws Exception
     *             the exception
     */
    public ExtendedRequest(final HttpServletRequest req, final HttpServletResponse res, final File tmpDir) throws Exception {
        request = req;
        if (ServletFileUpload.isMultipartContent(req)) {
            // controle de la taille du fichier uploadé
            long maxsize = 0;
            final DiskFileItemFactory fac = new DiskFileItemFactory();
            fac.setRepository(tmpDir);
            final ServletFileUpload upload = new ServletFileUpload(fac);
            upload.setHeaderEncoding(CharEncoding.DEFAULT);
            final List<FileItem> items = upload.parseRequest(req);
            // boucle pour recherche du paramètre MAX_SIZE_FILE présent dans la requete
            for (final FileItem fileItem : items) {
                if (fileItem.isFormField() && fileItem.getFieldName().equals(UploadedFile.KEY_MAX_FILE_SIZE)) {
                    try {
                        maxsize = Long.parseLong(fileItem.getString());
                        maxsize = maxsize * 1024;
                        break;
                    } catch (final NumberFormatException e) {
                        LOG.debug("unable to parse the given value", e);
                    }
                }
            }
            // sinon lecture de la propriété fichiergw.maxsize
            if (maxsize == 0) {
                try {
                    maxsize = Long.parseLong(PropertyHelper.getCoreProperty("fichiergw.maxsize"));
                    maxsize = maxsize * 1024;
                } catch (final NumberFormatException e) {
                    LOG.debug("unable to parse the given value", e);
                }
            }
            // valeur par defaut
            if (maxsize == 0) {
                maxsize = UploadedFile.MAX_SIZE;
            }
            // Process the uploaded items
            for (final FileItem item : items) {
                if (params == null) {
                    params = new HashMap<>();
                }
                final String fieldName = item.getFieldName();
                Object fieldValue;
                if (item.isFormField()) {
                    fieldValue = Streams.asString(item.getInputStream(), CharEncoding.DEFAULT);
                } else {
                    fieldValue = null;
                    // si fichier inexistant
                    if ("".equals(item.getName())) {
                        final String[] lstS = {UploadedFile.EXCEPTION_FILE_UPLOAD_FICHIER_INEXISTANT};
                        params.put(UploadedFile.PARAM_JTF_EXCEPTION_FILE_UPLOAD, lstS);
                    }
                    // si fichier non uploadé car trop gros
                    else if (item.getSize() > maxsize) {
                        final String[] lstS = {UploadedFile.EXCEPTION_FILE_UPLOAD_TAILLE_FICHIER, Long.toString(maxsize)};
                        params.put(UploadedFile.PARAM_JTF_EXCEPTION_FILE_UPLOAD, lstS);
                    } else {
                        final File fichier = new File(tmpDir, "upl_" + UUID.randomUUID());
                        item.write(fichier);
                        final UploadedFile upf = new UploadedFile(item.getContentType(), (int) fichier.length(), item.getName(), fichier);
                        fieldValue = upf;
                    }
                }
                // add value to hashmap
                if (fieldName != null && fieldValue != null) {
                    Object[] fieldValues = (Object[]) params.get(fieldName);
                    if (fieldValues == null) {
                        fieldValues = new Object[1];
                    } else {
                        final Object[] newVals = new Object[fieldValues.length + 1];
                        for (int i = 0; i < fieldValues.length; i++) {
                            newVals[i] = fieldValues[i];
                        }
                        fieldValues = newVals;
                    }
                    fieldValues[fieldValues.length - 1] = fieldValue;
                    params.put(fieldName, fieldValues);
                }
            }
        } else {
            params = null;
        }
    }

    /**
     * Returns the lone uploaded file value of the specified parameter, or null if the parameter does not exists or is not an uploaded file value.
     *
     * @param name
     *            the name of the parameter whose value is required.
     *
     * @return the file parameter
     *
     * @see #getFileParameterValues
     */
    public UploadedFile getFileParameter(final String name) {
        UploadedFile file = null;
        if (params != null) {
            final Object[] vals = (Object[]) params.get(name);
            if (vals != null && vals[0] instanceof UploadedFile) {
                file = (UploadedFile) vals[0];
            }
        }
        return file;
    }

    /**
     * Returns the uploaded file values of the specified parameter, or null if the specified parameter does not exist. The array contains null for a value that is not an uploaded
     * file.
     *
     * @param name
     *            the name of the parameter whose value is required.
     *
     * @return the file parameter values
     *
     * @see #getFileParameter
     */
    public UploadedFile[] getFileParameterValues(final String name) {
        UploadedFile[] files = null;
        if (params != null) {
            final Object[] vals = (Object[]) params.get(name);
            if (vals != null) {
                files = new UploadedFile[vals.length];
                for (int i = 0; i < vals.length; i++) {
                    if (vals[i] instanceof UploadedFile) {
                        files[i] = (UploadedFile) vals[i];
                    } else {
                        files[i] = null;
                    }
                }
            }
        }
        return files;
    }

    /**
     * Returns a string containing the lone value of the specified parameter, or null if the parameter does not exist. If the value is an uploaded file, the client-side filename is
     * returned.
     *
     * <P>
     * Servlet writers should use this method only when they are sure that there is only one value for the parameter. If the parameter has (or could have) multiple values, servlet
     * writers should use getParameterValues. If a multiple valued parameter name is passed as an argument, the return value is implementation dependent.
     * </P>
     *
     * @param name
     *            the name of the parameter whose value is required.
     *
     * @return the parameter
     *
     * @see #getParameterValues
     */
    @Override
    public String getParameter(final String name) {
        String parameter = null;
        if (params == null) {
            parameter = request.getParameter(name);
        } else {
            final Object[] vals = (Object[]) params.get(name);
            if (vals != null) {
                parameter = vals[0].toString();
            }
        }
        return parameter;
    }

    /**
     * Returns the parameter names for this request as an enumeration of strings, or an empty enumeration if there are no parameters or the input stream is empty.
     *
     * @return the parameter names
     */
    @Override
    public Enumeration<String> getParameterNames() {
        if (params == null) {
            return request.getParameterNames();
        } else {
            return Collections.enumeration(params.keySet());
        }
    }

    /**
     * Returns the values of the specified parameter for the request as an array of strings, or null if the named parameter does not exist. If any of the parameter's values are
     * uploaded files, the client-side filenames are returned.
     *
     * @param name
     *            the name of the parameter whose value is required.
     *
     * @return the parameter values
     *
     * @see #getParameter
     */
    @Override
    public String[] getParameterValues(final String name) {
        String[] parameterValues = null;
        if (params == null) {
            parameterValues = request.getParameterValues(name);
        } else {
            final Object[] vals = (Object[]) params.get(name);
            if (vals != null) {
                parameterValues = new String[vals.length];
                for (int i = 0; i < vals.length; i++) {
                    parameterValues[i] = vals[i].toString();
                }
            }
        }
        return parameterValues;
    }

    @Override
    public Object getAttribute(final String name) {
        return request.getAttribute(name);
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        return request.getAttributeNames();
    }

    @Override
    public String getAuthType() {
        return request.getAuthType();
    }

    @Override
    public String getCharacterEncoding() {
        return request.getCharacterEncoding();
    }

    @Override
    public int getContentLength() {
        return request.getContentLength();
    }

    @Override
    public String getContentType() {
        return request.getContentType();
    }

    @Override
    public java.lang.String getContextPath() {
        return request.getContextPath();
    }

    @Override
    public Cookie[] getCookies() {
        return request.getCookies();
    }

    @Override
    public long getDateHeader(final String name) {
        return request.getDateHeader(name);
    }

    @Override
    public String getHeader(final String name) {
        return request.getHeader(name);
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        return request.getHeaderNames();
    }

    @Override
    public Enumeration<String> getHeaders(final String arg0) {
        return request.getHeaders(arg0);
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        return request.getInputStream();
    }

    @Override
    public int getIntHeader(final String name) {
        return request.getIntHeader(name);
    }

    @Override
    public Locale getLocale() {
        return request.getLocale();
    }

    @Override
    public Enumeration<Locale> getLocales() {
        return request.getLocales();
    }

    @Override
    public String getMethod() {
        return request.getMethod();
    }

    @Override
    public String getPathInfo() {
        return request.getPathInfo();
    }

    @Override
    public String getPathTranslated() {
        return request.getPathTranslated();
    }

    @Override
    public String getProtocol() {
        return request.getProtocol();
    }

    @Override
    public String getQueryString() {
        return request.getQueryString();
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return request.getReader();
    }

    @Override
    public String getRealPath(final String virtualPath) {
        return request.getRealPath(virtualPath);
    }

    @Override
    public String getRemoteAddr() {
        return request.getRemoteAddr();
    }

    @Override
    public String getRemoteHost() {
        return request.getRemoteHost();
    }

    @Override
    public String getRemoteUser() {
        return request.getRemoteUser();
    }

    @Override
    public RequestDispatcher getRequestDispatcher(final String arg0) {
        return request.getRequestDispatcher(arg0);
    }

    @Override
    public String getRequestedSessionId() {
        return request.getRequestedSessionId();
    }

    @Override
    public String getRequestURI() {
        return request.getRequestURI();
    }

    @Override
    public String getScheme() {
        return request.getScheme();
    }

    @Override
    public String getServerName() {
        return request.getServerName();
    }

    @Override
    public int getServerPort() {
        return request.getServerPort();
    }

    @Override
    public String getServletPath() {
        return request.getServletPath();
    }

    @Override
    public HttpSession getSession() {
        return request.getSession();
    }

    @Override
    public HttpSession getSession(final boolean create) {
        return request.getSession(create);
    }

    @Override
    public Principal getUserPrincipal() {
        return request.getUserPrincipal();
    }

    @Override
    public boolean isRequestedSessionIdFromCookie() {
        return request.isRequestedSessionIdFromCookie();
    }

    @Override
    public boolean isRequestedSessionIdFromUrl() {
        return request.isRequestedSessionIdFromURL();
    }

    @Override
    public boolean isRequestedSessionIdFromURL() {
        return request.isRequestedSessionIdFromURL();
    }

    @Override
    public boolean isRequestedSessionIdValid() {
        return request.isRequestedSessionIdValid();
    }

    @Override
    public boolean isSecure() {
        return request.isSecure();
    }

    @Override
    public boolean isUserInRole(final String arg0) {
        return request.isUserInRole(arg0);
    }

    @Override
    public void removeAttribute(final String arg0) {
        request.removeAttribute(arg0);
    }

    @Override
    public void setAttribute(final String name, final Object o) {
        request.setAttribute(name, o);
    }

    @Override
    public StringBuffer getRequestURL() {
        return request.getRequestURL();
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return request.getParameterMap();
    }

    @Override
    public void setCharacterEncoding(final String arg0) throws UnsupportedEncodingException {
        request.setCharacterEncoding(arg0);
    }

    @Override
    public String getLocalAddr() {
        return request.getLocalAddr();
    }

    @Override
    public String getLocalName() {
        return request.getLocalName();
    }

    @Override
    public int getLocalPort() {
        return request.getLocalPort();
    }

    @Override
    public int getRemotePort() {
        return request.getRemotePort();
    }

    @Override
    public AsyncContext getAsyncContext() {
        return request.getAsyncContext();
    }

    @Override
    public DispatcherType getDispatcherType() {
        return request.getDispatcherType();
    }

    @Override
    public ServletContext getServletContext() {
        return request.getServletContext();
    }

    @Override
    public boolean isAsyncStarted() {
        return request.isAsyncStarted();
    }

    @Override
    public boolean isAsyncSupported() {
        return request.isAsyncSupported();
    }

    @Override
    public AsyncContext startAsync() {
        return request.startAsync();
    }

    @Override
    public AsyncContext startAsync(final ServletRequest arg0, final ServletResponse arg1) {
        return request.startAsync(arg0, arg1);
    }

    @Override
    public boolean authenticate(final HttpServletResponse arg0) throws IOException, ServletException {
        return request.authenticate(arg0);
    }

    @Override
    public Part getPart(final String arg0) throws IOException, IllegalStateException, ServletException {
        return request.getPart(arg0);
    }

    @Override
    public Collection<Part> getParts() throws IOException, IllegalStateException, ServletException {
        return request.getParts();
    }

    @Override
    public void login(final String arg0, final String arg1) throws ServletException {
        request.login(arg0, arg1);
    }

    @Override
    public void logout() throws ServletException {
        request.logout();
    }
}
