package com.jsbsoft.jtf.webutils;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.database.RequeteMgr;
import com.kportal.core.config.PropertyHelper;
import com.univ.multisites.InfosSite;
import com.univ.utils.ContexteDao;
import com.univ.utils.ContexteUniv;

/**
 * Insérez la description du type à cet endroit. Date de création : (20/02/01 15:23:51)
 *
 * @author :
 */
public class ContextePage implements OMContext {

    private static final Logger LOG = LoggerFactory.getLogger(ContextePage.class);

    /** The datas. */
    private final Map<String, Object> datas = new HashMap<>();

    private ContexteDao contexteDao;

    /** The connection longue. */
    private boolean connectionLongue = false;

    /** The locale. */
    private Locale locale;

    /** The id requete. */
    private String idRequete = "";

    /** The langue. */
    private String langue = "";

    /** The infos site. */
    private InfosSite infosSite;

    /** The secure. */
    private boolean secure = false;

    /**
     * Constructeur.
     *
     * @param _uri
     *            the _uri
     * @param _langue
     *            the _langue
     */
    public ContextePage(final String _uri, final String _langue) {
        // JSS 20040222 - Gestion des identifiants de requete
        setIdRequete(RequeteMgr.creerRequete());
        RequeteMgr.ajouterEvenement(getIdRequete(), "création contexte jsp uri " + _uri + ", langue=" + _langue);
        // JB 20060914 : ajout de la trace de creation de contexte
        if ("1".equals(PropertyHelper.getCoreProperty("dump.detection_boucle"))) {
            final Throwable tCreation = new Throwable();
            tCreation.fillInStackTrace();
            final StackTraceElement[] stackTraceElements = tCreation.getStackTrace();
            String trace = "";
            for (final StackTraceElement stackTraceElement : stackTraceElements) {
                trace += stackTraceElement + "\n";
            }
            RequeteMgr.ajouterEvenement(getIdRequete(), "*** trace creation contexte :\n\t" + trace + "\n***\n");
        }
        if (_uri.length() > 0) {
            LOG.debug(_uri);
        }
        // Initialisation langue
        try {
            locale = LangueUtil.getLocale(Integer.parseInt(_langue == null ? "0" : _langue));
        } catch (final NumberFormatException e) {
            LOG.debug("unable to parse the language attribute", e);
            locale = LangueUtil.getDefaultLocale();
        }
        setLangue(LangueUtil.getLangueLocale(getLocale()));
    }

    /**
     * Constructeur inutile utilisé pour récupérer une connection.
     *
     * @param uri
     *            the _uri
     */
    public ContextePage(final String uri) {
        this(uri, null);
    }

    /**
     * Constructeur inutile utilisé pour récupérer une connection.
     */
    public ContextePage() {
        this("", null);
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.OMContext#getDatas()
     */
    @Override
    public Map<String, Object> getDatas() {
        return datas;
    }

    /**
     * Renvoie la map des infos de session de l'utilisateur relatives à l'applicatif métier.
     *
     * @param nomApplicatif
     *            nom de l'applicatif metier
     *
     * @return map
     *
     * @see com.univ.utils.ISynchroniseurUtilisateurMetier
     */
    public Map getInfosSessionUserMetier(final String nomApplicatif) {
        final Map mapApplicatifs = (Map) getDatas().get(ContexteUniv.CLEF_INFOS_SESSION_APPLICATIFS_METIER);
        if (mapApplicatifs != null) {
            return (Map) mapApplicatifs.get(nomApplicatif);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.OMContext#getConnection()
     */
    @Override
    public Connection getConnection() {
        if (contexteDao == null) {
            contexteDao = new ContexteDao();
        }
        return contexteDao.getConnection();
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.OMContext#getLocale()
     */
    @Override
    public Locale getLocale() {
        return locale;
    }

    public void setLocale(final Locale locale) {
        this.locale = locale;
    }

    /**
     * Release.
     */
    public void release() {
        // JSS 20040222 : Gestion des identifiants de requete
        RequeteMgr.terminerRequete(getIdRequete());
        if (contexteDao != null) {
            contexteDao.close();
            contexteDao = null;
        }
    }

    /**
     * Gets the langue.
     *
     * @return java.lang.String
     */
    public String getLangue() {
        return langue;
    }

    public void setLangue(final String langue) {
        this.langue = langue;
    }

    /**
     * @deprecated Renvoie le libellé associé à la clé. Cette méthode ne doit plus être utilisé car elle renvoit uniquement les messages du core
     * il faut désormais utiliser MessageHelper.getMessage(
     * @param key the key
     *
     * @return the message
     *
     */
    @Deprecated
    public String getMessage(final String key) {
        String res = null;
        // on va chercher le libellé associé au site
        if (infosSite != null) {
            res = LangueUtil.getMessage(locale, infosSite.getAlias() + "." + key);
        }
        // sinon, le libellé par défaut
        if (res == null || "".equals(res)) {
            res = LangueUtil.getMessage(locale, key);
        }
        return res == null ? "" : res;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.OMContext#getIdRequete()
     */
    @Override
    public String getIdRequete() {
        return idRequete;
    }

    /**
     * Sets the id requete.
     *
     * @param string
     *            the string
     */
    public void setIdRequete(final String string) {
        idRequete = string;
    }

    /**
     * Sets the connection longue.
     *
     * @param connectionLongue
     *            The connectionLongue to set. permet de conserver la connexion + de 10 minutes (batch)
     */
    public void setConnectionLongue(final boolean connectionLongue) {
        this.connectionLongue = connectionLongue;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.OMContext#setInfosSite(com.univ.multisites.InfosSite)
     */
    @Override
    public void setInfosSite(final InfosSite infosSite) {
        this.infosSite = infosSite;
    }

    /**
     * Renvoie l'InfosSite correspondant au site courant.
     *
     * @return the infos site
     */
    @Override
    public InfosSite getInfosSite() {
        return infosSite;
    }

    /* (non-Javadoc)
     * @see com.jsbsoft.jtf.database.OMContext#setSecure(boolean)
     */
    @Override
    public void setSecure(final boolean secure) {
        this.secure = secure;
    }

    /**
     * Renvoie true si on est en mode sécurisé (https).
     *
     * @return true, if checks if is secure
     */
    @Override
    public boolean isSecure() {
        return secure;
    }

    public void putData(final String key, final Object value) {
        datas.put(key, value);
    }

    public Object getData(final String key) {
        return datas.get(key);
    }

    /*
     * renvoie un string si la valeur est assignable à un string sinon vide
     */
    public String getDataAsString(final String key) {
        if (getData(key) != null) {
            if (getData(key) instanceof String) {
                return (String) getData(key);
            }
        }
        return "";
    }
}
