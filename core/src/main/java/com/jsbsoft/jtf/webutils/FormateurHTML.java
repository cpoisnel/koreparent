package com.jsbsoft.jtf.webutils;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.CodeLibelle;

/**
 *
 * @author :
 */
public class FormateurHTML {

    /**
     * Formater date.
     *
     * @param _date
     *            the _date
     * @param locale
     *            the _locale
     *
     * @return the string
     */
    public static String FormaterDate(final Date _date, final Locale locale) {
        final DateFormat date = DateFormat.getDateInstance(DateFormat.LONG, locale);
        return date.format(_date);
    }

    /**
     *
     * @param table
     *            Hashtable
     * @param selected
     *            the _selected
     * @param triLibelle
     *            the tri libelle
     *
     * @return the string
     * @deprecated Utilisez {@link FormateurHTML#insererCombo(Map, String, boolean)}
     */
    @Deprecated
    public static String insererCombo(final Hashtable<String, String> table, final String selected, final boolean triLibelle) {
        return insererCombo(table, selected, triLibelle, 0);
    }

    /**
     *
     * @param table
     *            Hashtable
     * @param selected
     *            the _selected
     * @param triLibelle
     *            the tri libelle
     *
     * @return the string
     */
    public static String insererCombo(final Map<String, String> table, final String selected, final boolean triLibelle) {
        return insererCombo(table, selected, triLibelle, 0);
    }

    /**
     *
     * @param fichier
     *            the fichier
     * @param locale
     *            the _locale
     * @param selected
     *            the _selected
     * @param triLibelle
     *            the tri libelle
     *
     * @return the string
     */
    public static String insererComboFichier(final String idCtx, final String fichier, final Locale locale, final String selected, final boolean triLibelle) {
        final Map<String, String> table = CodeLibelle.lireTable(idCtx, fichier, locale);
        return insererCombo(table, selected, triLibelle);
    }

    /**
     *
     * @param fichier
     *            the fichier
     * @param locale
     *            the _locale
     * @param cle
     *            the _cle
     *
     * @return the string
     */
    public static String insererLibelleFichier(final String idCtx, final String fichier, final Locale locale, final String cle) {
        return CodeLibelle.lireLibelle(idCtx, fichier, locale, cle);
    }

    /**
     * Formatage d'une chaine en HTML - conversion des "\n" en <BR>
     * .
     *
     * @param texte
     *            the texte
     *
     * @return the string
     */
    public static String formaterEnHTML(final String texte) {
        String result = texte;
        if (StringUtils.isNotBlank(texte)) {
            result = texte.replaceAll("(\r\n|\n)", "<br />");
        }
        return result;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (26/02/2001 09:45:35)
     *
     * @param table
     *            Hashtable
     * @param selected
     *            the selected
     * @param triLibelle
     *            the tri libelle
     * @param lg
     *            the lg
     *
     * @return the string
     * @deprecated Utilisez {@link #insererCombo(Map, String, boolean, int)}
     */
    @Deprecated
    public static String insererCombo(final Hashtable<String, String> table, final String selected, final boolean triLibelle, final int lg) {
        return insererCombo((Map<String, String>) table, selected, triLibelle, lg);
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (26/02/2001 09:45:35)
     *
     * @param table
     *            Hashtable
     * @param selected
     *            the selected
     * @param triLibelle
     *            the tri libelle
     * @param lg
     *            the lg
     *
     * @return the string
     */
    public static String insererCombo(final Map<String, String> table, final String selected, final boolean triLibelle, final int lg) {
        String res = "";
        // Tri de la Hashtable
        final Map<String, String> sortedMap = new TreeMap<>();
        Collection<String> keys;
        if (triLibelle) {
            for (Entry<String, String> datas : table.entrySet()) {
                sortedMap.put(datas.getValue() + "," + datas.getKey(), datas.getKey());
            }
            keys = sortedMap.values();
        } else {
            sortedMap.putAll(table);
            keys = sortedMap.keySet();
        }
        // Parcours de la map triée
        for (String orderedKey : keys) {
            String libelle = table.get(orderedKey);
            if (lg > 0 && lg < libelle.length()) {
                libelle = libelle.substring(0, lg);
            }
            String sSelected = "";
            if (orderedKey.equals(selected)) {
                sSelected = "selected=\"true\"";
            }
            if (libelle.startsWith("#GROUPE#")) {
                res += "<optgroup id=\"opt" + orderedKey + "\" label=\"" + libelle.substring(8) + "\" >";
            } else if (libelle.startsWith("#/GROUPE#")) {
                res += "</optgroup>";
            } else {
                res += "<option value=\"" + orderedKey + "\" " + sSelected + ">" + libelle + "</option>";
            }
        }
        return res;
    }

    /**
     * Formatage HTML d'un ensemble de libellés séparés par un ; Les libellés sont séparés par un <BR>
     * .
     *
     * @param texte
     *            the texte
     *
     * @return the string
     */
    public static String formaterLibelleMultipleEnHTML(final String texte) {
        String res = "";
        final StringTokenizer st = new StringTokenizer(texte, ";");
        int nbItems = 0;
        while (st.hasMoreTokens()) {
            nbItems++;
            if (nbItems > 1) {
                res = res + "<br />";
            }
            res = res + st.nextToken();
        }
        return res;
    }
}
