package com.jsbsoft.jtf.exception;

/**
 *
 * Exception levée lorsque l'accés à une action n'est pas autorisée pour l'utilisateur.
 *
 * @author pierre.cosson
 *
 */
public class ErreurActionNonAutorisee extends ErreurApplicative {

    private static final String MESSAGE_DEFAUT = "Action non autorisée. Vous ne disposez pas des droits suffisants.";

    /**
     * ID
     */
    private static final long serialVersionUID = 2782914301062478226L;

    public ErreurActionNonAutorisee(int num, String mes) {
        super(num, mes);
    }

    public ErreurActionNonAutorisee(String mes) {
        super(mes);
    }

    /**
     * Exectption avec le message par défaut : {@value #MESSAGE_DEFAUT}.
     */
    public ErreurActionNonAutorisee() {
        super(MESSAGE_DEFAUT);
    }
}
