package com.jsbsoft.jtf.exception;

import org.apache.commons.lang3.StringUtils;

public class ErreurUtilisateurNonAuthentifie extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = -8733054795635513529L;

    public ErreurUtilisateurNonAuthentifie(final int num, final String mes) {
        super(num, mes);
    }

    public ErreurUtilisateurNonAuthentifie(final String mes) {
        super(mes);
    }

    public ErreurUtilisateurNonAuthentifie() {
        super(StringUtils.EMPTY);
    }
}
