package com.jsbsoft.jtf.exception;

/**
 *
 * Exception levée lorsqu'une données n'est pas unique. <br/>
 * <br/>
 * <strong>REMARQUE : </strong> permet d'éviter de revoyer des valeurs <code>null</code> et de créer de possible {@link NullPointerException} dans le reste de l'application. Avec
 * cette exception, l'anomalie remonte jusqu'à temps qu'elle soit gérée. De plus, héritant de {@link ErreurApplicative} elle est gérée par le produit.
 *
 * @author pierre.cosson
 *
 */
public class ErreurUniciteNonRespectee extends ErreurApplicative {

    /**
     * ID
     */
    private static final long serialVersionUID = -478250306252325465L;

    public ErreurUniciteNonRespectee(int num, String mes) {
        super(num, mes);
    }

    public ErreurUniciteNonRespectee(String mes) {
        super(mes);
    }
}
