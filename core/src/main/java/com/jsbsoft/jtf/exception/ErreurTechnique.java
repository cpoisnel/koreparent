package com.jsbsoft.jtf.exception;

/**
 * This type was created in VisualAge.
 */
public class ErreurTechnique extends Erreur {

    /** The Constant TYPERR_SGBD. */
    public static final int TYPERR_SGBD = 0;

    /**
     *
     */
    private static final long serialVersionUID = 1520332082253292668L;

    /**
     * Instantiates a new erreur technique.
     *
     * @param num
     *            the num
     * @param mes
     *            the mes
     */
    public ErreurTechnique(final int num, final String mes) {
        super(num, mes);
    }

    /**
     * Instantiates a new erreur technique.
     *
     * @param num
     *            the num
     * @param mes
     *            the mes
     * @param t
     *            the t
     */
    public ErreurTechnique(final int num, final String mes, final Throwable t) {
        super(num, mes, t);
    }
}