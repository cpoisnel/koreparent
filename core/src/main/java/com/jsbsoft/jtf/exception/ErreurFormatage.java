package com.jsbsoft.jtf.exception;

/**
 * Insérez la description du type à cet endroit. Date de création : (31/03/00 10:22:19)
 *
 * @author : Jean-Sébastien
 */
public class ErreurFormatage extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = -6248408082636629115L;

    /**
     * Commentaire relatif au constructeur ErreurFormatage.
     */
    public ErreurFormatage() {
        super(2000, "Format incorrect");
    }

    /**
     * Commentaire relatif au constructeur ErreurFormatage.
     *
     * @param libelle
     *            the libelle
     */
    public ErreurFormatage(final String libelle) {
        super(libelle);
    }
}
