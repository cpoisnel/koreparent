package com.jsbsoft.jtf.lang;

import java.nio.charset.Charset;

public class CharEncoding {

    public static final String DEFAULT = org.apache.commons.lang3.CharEncoding.UTF_8;

    public static final Charset DEFAULT_CHARSET = Charset.forName(DEFAULT);
}
