package com.jsbsoft.jtf.datasource.exceptions;

public class DataSourceException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -1950466212160047480L;

    public DataSourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataSourceException(String message) {
        super(message);
    }
}
