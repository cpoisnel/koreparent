package com.jsbsoft.jtf.datasource.dao.impl.mysql;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataAccessException;

import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.PersistenceBean;

/**
 * Classe abstraite rajoutant les méthodes pour les anciens objets du produit afin que ce soit  rétrocompatible.
 * Cette classe ne doit pas être utiliser pour des nouvelles implémentations.
 */
public abstract class AbstractLegacyDAO<T extends PersistenceBean> extends AbstractCommonDAO<T> {

    protected static final String SELECT_DISTINCT = "select DISTINCT %s from `%s` T1 ";

    protected static final String COLUMNS_PREFIX = "T1.";

    protected String columnsNameForRequest;

    protected void initColumns() throws DataSourceException {
        if (StringUtils.isBlank(columnsNameForRequest)) {
            final Set<String> columns = getColumns();
            if(CollectionUtils.isNotEmpty(columns)) {
                @SuppressWarnings("unchecked")
                final Collection<String> columnsName = CollectionUtils.collect(columns, new Transformer() {

                    @Override
                    public String transform(final Object input) {
                        return COLUMNS_PREFIX + input;
                    }
                });
                columnsNameForRequest = StringUtils.join(columnsName, " , ");
            }
        }
    }

    /**
     * Méthode permettant de gérer les anciennes méthodes select ou l'on ne passait que la partie WHERE de la requête SQL et sans paramètre.
     * Cette méthode ne DOIT PAS ÊTRE UTILISÉ sur les nouveaux objets
     * @param request la requête à exécuter
     * @return l'ensemble des résultats correspondant à la requête ou une liste vide si rien n'est retourné.
     * @throws DataSourceException
     */
    public List<T> select(final String request) throws DataSourceException {
        initColumns();
        String query = String.format(SELECT_DISTINCT, columnsNameForRequest, tableName);
        if (request != null) {
            query += request;
        }
        final List<T> results;
        try {
            results = namedParameterJdbcTemplate.query(query, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }
}
