package com.jsbsoft.jtf.datasource.dao.impl.mysql;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import javax.sql.DataSource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.jsbsoft.jtf.datasource.dao.CommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.UpdateToDataSourceException;
import com.jsbsoft.jtf.datasource.naming.ColumnNamingStrategy;
import com.jsbsoft.jtf.datasource.naming.impl.DefaultColumnNamingStategy;
import com.jsbsoft.jtf.datasource.utils.DaoUtils;
import com.univ.objetspartages.bean.PersistenceBean;
import com.univ.objetspartages.dao.mapper.CustomIdBeanPropertyRowMapper;

/**
 * Classe abstraite apportant les méthodes de base d'un DAO.
 * Attention, lors de l'implémentation de cet objet, il faut ABSOLUMENT initialiser les variables {@link #tableName} et {@link #rowMapper} pour que les
 * méthodes implémentés puissent fonctionner. Si ce n'est pas le cas, les requêtes ne pourront pas savoir sur quel tables aller faire la requête ni savoir
 * comment transformer le ResultSet en Bean.
 * @param <T> Le bean devant être mapper après la requête.
 */
public abstract class AbstractCommonDAO<T extends PersistenceBean> implements CommonDAO<T> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractCommonDAO.class);

    private final Set<String> columns = new ConcurrentSkipListSet<>();

    /**
     * Le nom de la table qui doit etre requetée. Il faut absolument que ce paramètre soit initialisé à la constraction du DAO.
     */
    protected String tableName;

    /**
     * La stratégie de nommage à utiliser pour ce DAO.
     */
    protected ColumnNamingStrategy columnNamingStrategy;

    /**
     * Le RowMapper sert à transformer le resultset en Bean du type {@link T}.
     */
    protected RowMapper<T> rowMapper;

    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public void setColumnNamingStrategy(final ColumnNamingStrategy columnNamingStrategy) {
        this.columnNamingStrategy = columnNamingStrategy;
    }

    public AbstractCommonDAO() {
        columnNamingStrategy = new DefaultColumnNamingStategy();
        rowMapper = new CustomIdBeanPropertyRowMapper<>(DaoUtils.getGenericType(this), columnNamingStrategy);
    }

    protected Set<String> getColumns() throws DataSourceException {
        if (CollectionUtils.isEmpty(columns)) {
            namedParameterJdbcTemplate.query(String.format("select * from `%s` LIMIT 1", tableName), new ResultSetExtractor<Integer>() {

                @Override
                public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
                    final ResultSetMetaData md = rs.getMetaData();
                    final int columnCount = md.getColumnCount();
                    for (int i = 1; i <= columnCount; i++) {
                        columns.add(md.getColumnName(i));
                    }
                    return columnCount;
                }
            });
        }
        return columns;
    }

    protected SqlParameterSource getParameters(T bean) {
        return new BeanPropertySqlParameterSource(bean);
    }

    @Override
    public T add(T bean) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            namedParameterJdbcTemplate.update(DaoUtils.getAddQuery(tableName, getColumns(), columnNamingStrategy), getParameters(bean), keyHolder);
            bean.setId(keyHolder.getKey().longValue());
        } catch (DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"%s\"", bean.toString(), tableName), dae);
        }
        return bean;
    }

    @Override
    public T update(T bean) throws UpdateToDataSourceException {
        try {
            namedParameterJdbcTemplate.update(DaoUtils.getUpdateQuery(tableName, getColumns(), columnNamingStrategy), getParameters(bean));
        } catch (DataAccessException dae) {
            throw new UpdateToDataSourceException(String.format("Unable to update %s with id %s", tableName, bean.getId()), dae);
        }
        return bean;
    }

    @Override
    public void delete(final Long id) throws DeleteFromDataSourceException {
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
            namedParameterJdbcTemplate.update(String.format("delete from `%1$s` WHERE ID_%1$s = :id", tableName), namedParameters);
        } catch (DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("An error occured during deletion of row with id %d from table \"%s\"", id, tableName), dae);
        }
    }

    @Override
    public T getById(final Long id) throws DataSourceException {
        T object = null;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
            object = namedParameterJdbcTemplate.queryForObject(String.format("select * from `%1$s` WHERE ID_%1$s = :id", tableName), namedParameters, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for id  %d on table %s", id, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size for id  %d on table %s", id, tableName));
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured retrieving object with id %d from table %s", id, tableName), dae);
        }
        return object;
    }

    @Override
    public List<T> select(final String request, SqlParameterSource parameters) {
        final List<T> results;
        try {
            results = namedParameterJdbcTemplate.query(request, parameters, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }

    /**
     * Méthode select prenant une requête avec des paramétres nommés.
     * @param request la requête en base
     * @param parameters l'ensemble des paramètres de la requête
     * @param callbackHandler le callback à appeler lorsque la requête est exécutée. Cela permet de ne pas garder en mémoire l'ensemble des objets retournés par la requête
     */
    public void select(final String request, SqlParameterSource parameters, RowCallbackHandler callbackHandler) {
        try {
            namedParameterJdbcTemplate.query(request, parameters, callbackHandler);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
    }

    /**
     * Permet de lancer une requete sans paramètre en BDD et de gérer le résultat par un callbackHandler.
     * @param request la requête en base
     * @param callbackHandler le callback à appeler lorsque la requête est exécutée. Cela permet de ne pas garder en mémoire l'ensemble des objets retournés par la requête
     */
    public void select(final String request, RowCallbackHandler callbackHandler) {
        try {
            namedParameterJdbcTemplate.query(request, callbackHandler);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
    }
}
