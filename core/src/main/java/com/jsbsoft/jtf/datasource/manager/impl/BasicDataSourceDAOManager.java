package com.jsbsoft.jtf.datasource.manager.impl;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.GenericTypeResolver;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.dao.CommonDAO;
import com.jsbsoft.jtf.datasource.manager.DataSourceDAOManager;
import com.kportal.extension.module.AbstractBeanManager;

public class BasicDataSourceDAOManager extends AbstractBeanManager implements DataSourceDAOManager {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(BasicDataSourceDAOManager.class);

    protected final Map<String, CommonDAO<?>> daosList = new HashMap<>();

    public Map<String, CommonDAO<?>> getDaosList() {
        return daosList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> CommonDAO<T> getDao(final Class<T> clazz) {
        CommonDAO<T> dao = null;
        if (daosList.containsKey(clazz.getName())) {
            dao = (CommonDAO<T>) daosList.get(clazz.getName());
        }
        return dao;
    }

    @Override
    public void refresh() {
        @SuppressWarnings("rawtypes")
        final Collection<CommonDAO> beans = ApplicationContextManager.getAllBeansOfType(CommonDAO.class).values();
        for (final CommonDAO<?> currentDao : beans) {
            final Class<?> clazz = getDaoGenericType(currentDao);
            if (clazz != null) {
                daosList.put(clazz.getName(), currentDao);
            } else {
                LOG.warn(String.format("Generic type for dao \"%s\" couldn't be retrieved : this dao will be ignored", currentDao.getClass().getName()));
            }
        }
    }

    private Class<?> getDaoGenericType(final CommonDAO<?> dao) {
        try {
            final Method method = dao.getClass().getMethod("getById", Long.class);
            return GenericTypeResolver.resolveReturnType(method, dao.getClass());
        } catch (final NoSuchMethodException e) {
            LOG.error(String.format("An error occured trying to determine generic type for dao \"%s\"", dao.getClass().getName()), e);
        }
        return null;
    }
}
