package com.jsbsoft.jtf.datasource.exceptions;

/**
 * Created on 25/09/15.
 */
public class ParametersDataSourceException extends DataSourceException {

    private static final long serialVersionUID = -8896562684366981009L;

    public ParametersDataSourceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ParametersDataSourceException(final String message) {
        super(message);
    }
}
