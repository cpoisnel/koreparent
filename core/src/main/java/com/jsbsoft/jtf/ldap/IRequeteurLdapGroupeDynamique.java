package com.jsbsoft.jtf.ldap;

import java.util.HashMap;
import java.util.Vector;

import javax.naming.NamingException;

/**
 * The Interface IRequeteurLdapGroupeDynamique.
 */
public interface IRequeteurLdapGroupeDynamique {

    /** The REQUETEU r_ lda p_ class e_ libell e_ jtf. */
    String REQUETEUR_LDAP_CLASSE_LIBELLE_JTF = "requeteurGroupeDynamiqueExterne.classe";

    /**
     * Checks if is valid requete.
     *
     * @param _s
     *            the _s
     *
     * @return true, if is valid requete
     *
     * @throws NamingException
     *             the naming exception
     * @throws Exception
     *             the exception
     */
    boolean isValidRequete(String _s) throws NamingException, Exception;

    /**
     * Gets the utilisteur.
     *
     * @param codeUtilisateur
     *            the code utilisateur
     *
     * @return the utilisteur
     *
     * @throws NamingException
     *             the naming exception
     * @throws Exception
     *             the exception
     */
    HashMap getUtilisteur(String codeUtilisateur) throws NamingException, Exception;

    /**
     * Gets the resultat requete.
     *
     * @param _requete
     *            the _requete
     * @param _codeUtilisateur
     *            the _code utilisateur
     *
     * @return the resultat requete
     *
     * @throws NamingException
     *             the naming exception
     * @throws Exception
     *             the exception
     */
    Vector getResultatRequete(String _requete, String _codeUtilisateur) throws NamingException, Exception;

    /**
     * Gets the list utilisateur.
     *
     * @param request
     *            the request
     *
     * @return the list utilisateur
     *
     * @throws NamingException
     *             the naming exception
     * @throws Exception
     *             the exception
     */
    Vector getListUtilisateur(String request) throws NamingException, Exception;
}
