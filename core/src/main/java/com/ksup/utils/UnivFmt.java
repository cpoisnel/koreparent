/*
 * Created on 6 juil. 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.ksup.utils;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Locale;
import java.util.TreeSet;

import javax.servlet.jsp.JspWriter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

public class UnivFmt {

    private static final Logger LOG = LoggerFactory.getLogger(UnivFmt.class);

    /*
     * FIXME : controle d'une faile Xss potentielle sur le select
     */
    public static void insererComboStructuresEnseignement(final ContexteUniv ctx, final Writer out, final String nomDonnee, final String idCss) {
        try {
            out.write("<select id=\"" + idCss + "\" name=\"" + nomDonnee + "\" size=\"1\">");
            insererContenuComboStructuresEnseignement(ctx, out);
            out.write("</select>");
        } catch (final IOException e) {
            LOG.error("erreur lors de l'ecriture sur le writer", e);
        }
    }

    public static void insererContenuComboStructuresEnseignement(final ContexteUniv ctx, final Writer out) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        try {
            final Collection<StructureModele> listeStructures = serviceStructure.getSubStructures(StringUtils.EMPTY, LangueUtil.getIndiceLocaleDefaut(), true);
            final TreeSet<String> tree = new TreeSet<>();
            for (final StructureModele infosSt : listeStructures) {
                if ("0".equals(infosSt.getAttributSpecifique1())) {
                    tree.add(infosSt.getLibelleLong() + "#" + infosSt.getCode());
                }
            }
            if (!(tree.isEmpty())) {
                for (final String aTree : tree) {
                    final String[] chaine = aTree.split("#", -2);
                    out.write("<option value=\"" + chaine[1] + "\">" + chaine[0] + "</option>");
                }
            }
        } catch (final IOException e) {
            LOG.error("erreur lors de l'insertion d'une combo structures", e);
        }
    }

    public void insererComboStructuresEnseignement(final com.jsbsoft.jtf.core.FormateurJSP fmt, final javax.servlet.jsp.JspWriter out, final com.jsbsoft.jtf.core.InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone) throws Exception {
        // AM 200412 : zones invisibles pour fiches LMD
        if (optionModification == -1) {
            return;
        }
        insererEnteteChamp(out, nomDonnee, nomZone, optionModification);
        insererContenuComboStructuresEnseignement(fmt, out, infoBean, nomDonnee, optionModification, nomZone);
        insererFinChamp(out);
    }

    public void insererContenuComboStructuresEnseignement(final com.jsbsoft.jtf.core.FormateurJSP fmt, final javax.servlet.jsp.JspWriter out, final com.jsbsoft.jtf.core.InfoBean infoBean, final String nomDonnee, final int optionModification, final String nomZone) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final Collection<StructureModele> listeStructures = serviceStructure.getSubStructures(StringUtils.EMPTY, LangueUtil.getIndiceLocaleDefaut(), true);
        final Hashtable<String, String> listeUfr = new Hashtable<>();
        String libelleFiche = "";
        final Locale localeContexte = ContexteUtil.getContexteUniv().getLocale();
        final String indiceLocale = String.valueOf(LangueUtil.getIndiceLocale(localeContexte));
        for (StructureModele currentStructure : listeStructures) {
            libelleFiche = currentStructure.getLibelleLong();
            if (!indiceLocale.equals(currentStructure.getLangue())) {
                final StructureModele localStructure = serviceStructure.getByCodeLanguage(currentStructure.getCode(), indiceLocale);
                if(localStructure != null) {
                    libelleFiche = localStructure.getLibelleLong();
                }
            }
            if ("0".equals(currentStructure.getAttributSpecifique1())) {
                listeUfr.put(currentStructure.getCode(), libelleFiche);
            }
        }
        infoBean.set(nomZone, listeUfr);
        fmt.insererComboHashtable(out, infoBean, nomDonnee, optionModification, nomZone);
    }

    /**
     * Insère le code HTML de l'entete d'un champ
     *
     * @throws IOException
     */
    private void insererEnteteChamp(final JspWriter out, final String nomDonnee, String nomZone, final int optionModification) throws IOException {
        if (optionModification == FormateurJSP.SAISIE_OBLIGATOIRE) {
            nomZone += " (*)";
        }
        out.println("<p>");
        if (nomZone.length() > 0) {
            out.println("<label for=\"" + nomDonnee + "\" class=\"colonne\"> " + nomZone + "</label>");
        }
    }

    /**
     * Insère le code HTML de la fin d'un champ
     */
    private void insererFinChamp(final JspWriter out) throws IOException {
        out.println("</p>");
    }
}
