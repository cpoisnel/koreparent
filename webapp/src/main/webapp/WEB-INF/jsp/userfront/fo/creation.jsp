<%@ page import="java.util.Set" %>
<%@ page import="javax.validation.ConstraintViolation" %>
<%@ page import="org.apache.commons.lang3.StringUtils, com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.LangueUtil" %>
<%@ page import="com.kosmos.userfront.processus.UserFrontProcessus" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.webapp.WebAppUtil" %>
<%@ page import="com.univ.objetspartages.services.ServiceUserPass" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.URLResolver" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request"/>
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page"/>
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page"/>
<%
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    if (StringUtils.isNotBlank(infoBean.getMessageErreur())) { %>
<p id="msg-erreur"><%=com.univ.utils.EscapeString.escapeScriptAndEvent(infoBean.getMessageErreur())%>
</p><%
    }
%>
<form class="edition_fiche"
      action="<%= URLResolver.getAbsoluteUrl(WebAppUtil.SG_PATH, ctx, URLResolver.DEMANDER_MDP_FRONT) %>" method="post">
    <div class="formulaire_hidden">
        <% fmt.insererVariablesCachees(out, infoBean);
        if (infoBean.get("ID_CI_RESTAURATION") != null) {
            %><input type="hidden" name="ID_CI_RESTAURATION" value="<%= infoBean.getString("ID_CI_RESTAURATION")%>"/>
            <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%= infoBean.getString("NOM_JSP_RESTAURATION")%>"/><%
        }
        if (infoBean.get(UserFrontProcessus.REGISTRATION_MODEL_ID) != null) {
            %><input type="hidden" name="<%= UserFrontProcessus.REGISTRATION_MODEL_ID%>" value="<%= infoBean.get(UserFrontProcessus.REGISTRATION_MODEL_ID,String.class) %>"/><%
        }
        %><input type="hidden" name="registration.alias_site" value="<%= ctx.getInfosSite().getAlias() %>"/>
        <input type="hidden" name="registration.id_locale_kportal" value="<%= LangueUtil.getLangueLocale(ctx.getLocale())%>"/>
        <input type="hidden" name="email_registration_data.processus" value="<%= infoBean.getNomProcessus()%>"/>
        <input type="hidden" name="email_registration_data.extension" value="<%= infoBean.getNomExtension()%>"/>
        <input type="hidden" name="ACTION" value="<%= UserFrontProcessus.ACTION_FRONT_VALIDER_CREATION %>"/>
    </div>
    <!-- .formulaire_hidden -->
    <p class="msg-aide"><%=MessageHelper.getCoreMessage("FO_ACCOUNT_EMAIL_HELP")%>
    </p>

    <p>
        <label for="registration.email"><%=MessageHelper.getCoreMessage("ST_DSI_EMAIL")%> (*)</label><!--
    --><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "registration.email", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_DSI_EMAIL"), "EMAIL=YES", false, "");%>
        <%
            if (infoBean.get("email_registration_data.email.constraint") != null) {
                Set<ConstraintViolation<?>> violations = infoBean.getHashSet("email_registration_data.email.constraint");
                %><span class="js-constraint-violation__message constraint-violation__message masquer"><%
                for (ConstraintViolation<?> currentViolation : violations) {
                    %><%= currentViolation.getMessage() %><%
                }
                %></span><%
            }
    %></p>
    <p>
        <label for="registration.login"><%=MessageHelper.getCoreMessage("ST_DSI_LOGIN")%> (*)</label><!--
    --><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "registration.login", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_DSI_LOGIN"));%>
        <%
            if (infoBean.get("user_registration_data.login.constraint") != null) {
                Set<ConstraintViolation<?>> violations = infoBean.getHashSet("user_registration_data.login.constraint");
                %><span class="js-constraint-violation__message constraint-violation__message masquer"><%
                for (ConstraintViolation<?> currentViolation : violations) {
                    %><%= currentViolation.getMessage() %><%
                }
                %></span><%
            }
    %></p>
    <p>
        <label for="registration.first_name"><%=MessageHelper.getCoreMessage("ST_DSI_PRENOM")%> (*)</label><!--
    --><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "registration.first_name", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_DSI_PRENOM"));%>
        <%
            if (infoBean.get("user_registration_data.firstName.constraint") != null) {
                Set<ConstraintViolation<?>> violations = infoBean.getHashSet("user_registration_data.firstName.constraint");
                %><span class="js-constraint-violation__message constraint-violation__message masquer"><%
                for (ConstraintViolation<?> currentViolation : violations) {
                    %><%= currentViolation.getMessage() %><%
                }
                %></span><%
            }
    %></p>

    <p>
        <label for="registration.last_name"><%=MessageHelper.getCoreMessage("ST_DSI_NOM")%> (*)</label><!--
    --><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "registration.last_name", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_DSI_NOM"));%>
        <%
            if (infoBean.get("user_registration_data.last_name.constraint") != null) {
                %><span class="js-constraint-violation__message constraint-violation__message masquer"><%
                Set<ConstraintViolation<?>> violations = infoBean.getHashSet("user_registration_data.last_name.constraint");
                for (ConstraintViolation<?> currentViolation : violations) {
                    %><%= currentViolation.getMessage() %><br/><%
                }
                %></span><%
            }
    %></p>

    <div>
        <label for="user_registration_data.password" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_MDP")%> (*)</label>

        <div class="pwdStrength">
            <% fmt.insererChampSaisie(out, infoBean, "user_registration_data.password", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, ServiceUserPass.getLongueurChampMotDePasse(), "password"); %>
        </div>
        <%
            if (infoBean.get("user_registration_data.password.constraint") != null) {
                Set<ConstraintViolation<?>> violations = infoBean.getHashSet("user_registration_data.password.constraint");
                %><span class="js-constraint-violation__message constraint-violation__message masquer"><%
                for (ConstraintViolation<?> currentViolation : violations) {
                    %><%= currentViolation.getMessage() %><%
                }
                %></span><%
            }
    %></div>
    <p>
        <label for="user_registration_data.password_confirmation" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_MDP_CONFIRMATION")%> (*)</label>
            <% fmt.insererChampSaisie(out, infoBean, "user_registration_data.password_confirmation", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, ServiceUserPass.getLongueurChampMotDePasse(), "password");
            if (infoBean.get("user_registration_data.constraint") != null) {
                Set<ConstraintViolation<?>> violations = infoBean.getHashSet("user_registration_data.constraint");
                %><span class="js-constraint-violation__message constraint-violation__message masquer"><%
                for (ConstraintViolation<?> currentViolation : violations) {
                    %><%= currentViolation.getMessage() %><%
                }
                %></span><%
            }
    %></p>

    <p id="valider-formulaire" class="validation">
        <input class="submit" type="submit" value="<%=MessageHelper.getCoreMessage("JTF_BOUTON_VALIDER")%>" name="SUBMIT"/>
    </p>

</form>

