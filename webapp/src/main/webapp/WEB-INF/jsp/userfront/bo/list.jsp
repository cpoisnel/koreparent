<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kosmos.userfront.bean.UserModerationData, com.kosmos.userfront.processus.UserFrontProcessus, com.kportal.core.config.MessageHelper, com.univ.utils.ContexteUtil, com.univ.utils.EscapeString"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<div id="content">
  <table class="datatable">
    <thead>
    <tr>
      <th ><%= MessageHelper.getCoreMessage("BO_LOGIN") %></th>
      <th ><%= MessageHelper.getCoreMessage("ST_DSI_EMAIL") %></th>
      <th class="js-datetime-order"><%= MessageHelper.getCoreMessage("BO.USERFRONT.DATE") %></th>
      <th ><%= MessageHelper.getCoreMessage("BO_ETAT") %></th>
      <th class="sanstri sansfiltre"><%= MessageHelper.getCoreMessage("BO_ACTIONS") %></th>
    </tr>
    </thead>
    <tbody><%
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",ContexteUtil.getContexteUniv().getLocale());
        List<UserModerationData> allRegistration = infoBean.getList(UserFrontProcessus.INFOBEAN_REGISTRATION);
        for (UserModerationData userModerationData : allRegistration) {
            %><tr>
                <td><%= EscapeString.escapeHtml(userModerationData.getLogin()) %></td>
                <td><%= EscapeString.escapeHtml(userModerationData.getEmail()) %></td>
                <td><%= format.format(userModerationData.getCreationDate())%></td>
                <td><%= MessageHelper.getCoreMessage("BO.USERFRONT." + userModerationData.getModerationState().name()) %></td>
                <td><a href="<%= ProcessusHelper.getUrlProcessAction(infoBean, null, null, UserFrontProcessus.ACTION_DETAIL, new String[][] {{ "ID", userModerationData.getId().toString()}})%>"><%= MessageHelper.getCoreMessage("JTF_BOUTON_DETAIL")%></a></td>
            </tr><%
        }
    %></tbody>
  </table>
</div>

