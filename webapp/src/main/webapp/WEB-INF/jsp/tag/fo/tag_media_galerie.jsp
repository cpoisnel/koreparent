<%@page import="java.io.StringWriter"%>
<%@page import="java.util.List"%>
<%@page import="com.univ.mediatheque.Mediatheque"%>
<%@page import="com.univ.mediatheque.galerie.Galerie"%>
<%@page import="com.univ.mediatheque.playlist.MediaPlaylist"%>
<%@page import="com.univ.mediatheque.playlist.visionneuse.VisionneusePlaylist"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@ page import="com.univ.objetspartages.bean.MediaBean" %>
<%
    StringWriter strWriter = new StringWriter();
    // ajout de la liste selon requete
    Mediatheque mediatheque = Mediatheque.getInstance();
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    String param = (String)request.getAttribute("PARAM");
    List<MediaBean> lstMedias = mediatheque.getMediasByParam(param,ctx);
    String modeAffichageGalerie = com.univ.utils.RequeteUtil.renvoyerParametre(param, "MODE");
    String ongletPresent = com.univ.utils.RequeteUtil.renvoyerParametre(param, "ONGLET");
    String titreGalerie = com.univ.utils.RequeteUtil.renvoyerParametre(param, "INTITULE_GALERIE");

    // ETAPE 1 => récupération de la Galerie
    Galerie galerie = null;
    if (ongletPresent == null || ongletPresent.length() == 0 || ongletPresent.equals("0")){
        galerie = (Galerie)mediatheque.getPlayList("galerieDefaut");
    } else {
        galerie = (Galerie)mediatheque.getPlayList("galerieOngletParTypeMedia");
    }

    // ETAPE 2 => récupération des visionneuse de playlist et de player
    VisionneusePlaylist visionneusePlaylist = null;
    MediaPlaylist mediaPlaylist = null;
    if (modeAffichageGalerie.equalsIgnoreCase("lightbox")){
        visionneusePlaylist = (VisionneusePlaylist)mediatheque.getPlayList("galeriePlaylistSimple");
        mediaPlaylist = (MediaPlaylist)mediatheque.getPlayList("playlistPopup");
    }
    //style par defaut si rien de selectionné
    if (visionneusePlaylist == null){
        visionneusePlaylist = (VisionneusePlaylist)mediatheque.getPlayList("galeriePlaylistSimple");
        mediaPlaylist = (MediaPlaylist)mediatheque.getPlayList("playlistPopup");
    }

    // ETAPE 3 => initialisation de la galerie
    galerie.setVisionneusePlaylist(visionneusePlaylist);
    galerie.setTitre(titreGalerie);

    // ETAPE 4 => initialisation de la playlist
    mediaPlaylist.setTitre("");
    mediaPlaylist.setMediaList(lstMedias);

    // ETAPE 5 => production de l'affichage
    galerie.afficherGalerieComplete(ctx, strWriter, mediaPlaylist);
%>

<%= strWriter.toString() %>
