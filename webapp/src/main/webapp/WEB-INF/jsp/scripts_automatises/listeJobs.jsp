<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="org.quartz.JobKey"%>
<%@page import="com.jsbsoft.jtf.core.ApplicationContextManager"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.scheduling.module.SchedulerManagerHelper"%>
<%@ page import="com.kportal.scheduling.monitoring.BatchMonitoringService" %>
<%@ page import="org.quartz.JobDetail" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /> 
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%
    BatchMonitoringService monitor = (BatchMonitoringService) ApplicationContextManager.getCoreContextBean(BatchMonitoringService.ID_BEAN);
    boolean peutParametrer = (Boolean) infoBean.get("PEUT_PARAMETRER");
    boolean peutLancer = (Boolean) infoBean.get("PEUT_LANCER");
    boolean onRun = monitor.areJobsRunning();
%>
<div id="scripts-automatises">
    <div id="content">
    <% if ( onRun || infoBean.getString("ACTION").equals("EXECUTER")) { %>
        <div class="message avertissement"><%= String.format(MessageHelper.getCoreMessage("BO_SCRIPTS_AUTOMATISES_TRAITEMENT"),"EXECUTER".equals(infoBean.getString("ACTION")) ? monitor.getJobDescription(infoBean.getString("JOB")) : monitor.getFirstRunningJobName()) %><a href="#" onclick="location.reload(true); return false;"><%=MessageHelper.getCoreMessage("BO_RAFRAICHIR")%></a></div>
    <% } %>
        <table class="datatable">
            <thead>
                <tr>
                    <th><%= MessageHelper.getCoreMessage("BO_INTITULE") %></th>
                    <th><%= MessageHelper.getCoreMessage("BO_DERNIERE_EXECUTION_MANUELLE") %></th>
                    <th><%= MessageHelper.getCoreMessage("BO_STATUT") %></th>
                    <th class="sanstri sansfiltre"><%= MessageHelper.getCoreMessage("BO_ACTIONS") %></th>
                </tr>
            </thead>


            <tbody><%
                for (Map.Entry<JobKey, JobDetail> currentEntry : SchedulerManagerHelper.getSchedulerManager().getJobDetails().entrySet()) {
                    final boolean hasParams = SchedulerManagerHelper.getParametersForJob(currentEntry.getKey().getName()).size() > 0;
                    if (peutLancer || ((List<String>)infoBean.get("LISTE_LOADABLE_JOBS")).contains(currentEntry.getKey().getName()) ) { %>
            <tr>
                <td title="<%= currentEntry.getKey().getName() %>"><%= StringUtils.defaultString(currentEntry.getValue().getDescription()) %></td>
                <td><%= monitor.getJobInfos(currentEntry.getKey().getName()).getLastFireDate() == null ? MessageHelper.getCoreMessage("BO_AUCUNE_EXECUTION") : new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(monitor.getJobInfos(currentEntry.getKey().getName()).getLastFireDate()) %></td>
                <td><%= monitor.getJobInfos(currentEntry.getKey().getName()).isRunning() ? MessageHelper.getCoreMessage("BO_EN_COURS_D_EXECUTION") : MessageHelper.getCoreMessage("BO_PRET") %></td>
                <td>
                    <% if (! onRun && ! infoBean.getString("ACTION").equals("EXECUTER")) { %>
                    <a <%= hasParams ? "data-manual-launch=\"" + currentEntry.getKey().getName() + "\"" : "" %> href="#" data-key="<%= currentEntry.getKey().getName() %>"><%= String.format("%s%s", MessageHelper.getCoreMessage("BO_EXECUTER"), (hasParams ? "..." : StringUtils.EMPTY)) %></a>
                    <% } else { %>
                    <span title="<%= MessageHelper.getCoreMessage("BO_JOB_EN_COURS_EXECUTION") %>" style="color: gray; text-decoration: underline;"><%= MessageHelper.getCoreMessage("BO_EXECUTER") %></span>
                    <% } %>
                </td>
            </tr><%
                    }
                }
            %></tbody>
        </table>

    </div>
</div>