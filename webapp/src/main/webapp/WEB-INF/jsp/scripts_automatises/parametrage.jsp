<%@page import="java.util.Arrays"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.fasterxml.jackson.core.type.TypeReference"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.scheduling.bean.JobParameter"%>
<%@page import="com.univ.utils.ParametrageHelper" %>
<%@page import="com.univ.utils.json.CodecJSon" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<%
    List<JobParameter> params = (List<JobParameter>) infoBean.get("JOB_PARAMS");
    String extensionId = infoBean.getString("JOB_EXTENSION_ID");
    String paramsField = infoBean.getString("PARAMS");
    Map<String, String> paramValue = (paramsField == null || "".equals(paramsField) ? new HashMap<String, String>() : CodecJSon.decodeStringJSonToClass(paramsField, new TypeReference<HashMap<String, String>>(){}));
%>
<!DOCTYPE html>
<html>
    <head>
        <title><%=MessageHelper.getCoreMessage("BO_PARAMETRAGE_DE_LA_TACHE") %></title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" media="screen, projection"  href="/adminsite/styles/screen.css">

    </head>
    <body>
        <div id="page">
            <div id="content">
                <form action="#">
                    <div class="fieldset">
                        <%
                            if (params.size() > 0) {
                                for (JobParameter param: params) {
                                    String name = param.getName();
                                    String id = "JOB_PARAM_" + name;
                        %>
                        <p>
                            <label for="<%= id %>" class="colonne"><%= ParametrageHelper.getMessage(extensionId, name) %> (*)</label>
                            <%
                                if ("".equals(param.getValue())) {
                            %>
                            <input data-field="<%= name %>" required="required" type="text" name="<%= id %>" id="<%= id %>" value="<%= paramValue.get(name) %>"/>
                            <%
                                } else {
                            %>

                            <select data-field="<%= name %>" required="required" name="<%= id %>" id="<%= id %>">
                                <option value=""><%= MessageHelper.getCoreMessage("JTF_SELECTIONNER_LISTE") %></option>
                                <%
                                    for (String v: Arrays.asList(param.getValue().toString().split(";"))) {
                                %>
                                <option <%= v.equals(paramValue.get(name)) ? "selected=\"selected\"" : "" %>value="<%= v %>"><%= ParametrageHelper.getMessage(extensionId, v) %></option>
                                <%
                                    }
                                %>
                            </select>
                            <%
                                }
                            %>
                        </p>
                        <%
                                }
                            } else {
                        %>
                            <h3><%=MessageHelper.getCoreMessage("BO_TRAITEMENT_SANS_PARAMETRES") %></h3>
                        <%
                            }
                        %>
                    </div>
                    (*) <%=MessageHelper.getCoreMessage("BO_CHAMPS_OBLIGATOIRES") %>
                </form>
            </div>
        </div>
    </body>
</html>