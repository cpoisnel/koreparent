<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Map"%>
<%@page import="com.jsbsoft.jtf.core.ApplicationContextManager"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.scheduling.module.SchedulerManagerHelper"%>
<%@page import="com.kportal.scheduling.monitoring.BatchMonitoringService"%>
<%@page import="com.kportal.scheduling.service.SequenceTrigger"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /> 
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%
    Map<String, SequenceTrigger> triggers = (Map<String, SequenceTrigger>) infoBean.get("LISTE_TRIGGERS");
    BatchMonitoringService monitor = (BatchMonitoringService) ApplicationContextManager.getCoreContextBean(BatchMonitoringService.ID_BEAN);
    boolean peutParametrer = (Boolean) infoBean.get("PEUT_PARAMETRER");
    boolean peutLancer = (Boolean) infoBean.get("PEUT_LANCER");
%>
<div id="content">

        <table class="datatable">
            <thead>
                <tr>
                    <th><%= MessageHelper.getCoreMessage("BO_INTITULE") %></th>
                    <th><%= MessageHelper.getCoreMessage("BO_EXPRESSION_CRON") %></th>
                    <th class="sanstri sansfiltre"><%= MessageHelper.getCoreMessage("BO_PROCHAINE_EXECUTION") %></th>
                </tr>
            </thead>

            <tbody>
                <%
                    for (String key: triggers.keySet()) {
                        SequenceTrigger value = triggers.get(key);
                        String keyJob = SchedulerManagerHelper.getJobNameFromTrigger(key);
                %>
                <tr>
                    <td title="<%= key %>">
                        <%= ("".equals(value.getDescription())) ? key : value.getDescription() %>
                    </td>
                    <td><%= value.getCronExpression() %></td>
                    <td><%= monitor.jobExists(keyJob) ? ( monitor.getJobInfos(keyJob).getNextFireDate() == null ? MessageHelper.getCoreMessage("BO_AUCUNE_PLANIFICATION") : new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(monitor.getJobInfos(keyJob).getNextFireDate()) ) : MessageHelper.getCoreMessage("BO_TRIGGER_INACTIF") %></td>
                </tr>

                <% } %>

            </tbody>
        </table>

</div>