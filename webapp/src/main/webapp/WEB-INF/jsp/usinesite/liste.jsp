<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.collections.CollectionUtils"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kosmos.usinesite.processus.SaisieInfosSite, com.kosmos.usinesite.processus.SaisieInfosSiteCommun, com.kosmos.usinesite.template.bean.TemplateSite, com.kportal.core.config.MessageHelper, com.univ.multisites.InfosSite, com.univ.utils.ContexteUtil, com.univ.utils.EscapeString"%>
<%@ page import="com.univ.utils.URLResolver" %>
<%@ page import="com.univ.objetspartages.services.ServiceRubrique" %>
<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<%@ page import="com.univ.objetspartages.bean.RubriqueBean" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>

<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /> 
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /> 
<div id="content">
    <table class="datatable">
        <thead>
            <tr>
                <th ><%= MessageHelper.getCoreMessage("USINESITE_LISTE_ENTETE_CODE") %></th>
                <th ><%= MessageHelper.getCoreMessage("USINESITE_LISTE_ENTETE_INTITULE") %></th>
                <th ><%= MessageHelper.getCoreMessage("USINESITE_LISTE_ENTETE_RUBRIQUE") %></th>
                <th ><%= MessageHelper.getCoreMessage("USINESITE_LISTE_TYPE_SITE") %></th>
                <th ><%= MessageHelper.getCoreMessage("USINESITE_LISTE_ENTETE_URL") %></th>
                <th ><%= MessageHelper.getCoreMessage("USINESITE_SAISIE_SITE_ACTIF") %></th>
                <th class="sanstri sansfiltre"><%= MessageHelper.getCoreMessage("BO_ACTIONS") %></th>
            </tr>
        </thead>
        <tbody>
        <%
        List<InfosSite> listeInfosSite = (List<InfosSite>) infoBean.get(SaisieInfosSiteCommun.INFOBEAN_LISTE_INFOSSITES);
        boolean isGestionnaire = (Boolean) infoBean.get(SaisieInfosSiteCommun.INFOBEAN_IS_GESTIONNAIRE_SITE);
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        // TRI de la liste
        Collections.sort(listeInfosSite, new Comparator<InfosSite>(){
            public int compare(InfosSite o1, InfosSite o2){
                return o1.getIntitule().compareToIgnoreCase(o2.getIntitule());
            }
        });
        if (CollectionUtils.isNotEmpty(listeInfosSite)){
            Map<String,TemplateSite> templates = (Map<String,TemplateSite>) infoBean.get(SaisieInfosSiteCommun.INFOBEAN_VALEURS_LISTE_TEMPLATES_INFOSSITE);
            for (InfosSite infosSite : listeInfosSite) {
                %>
                <tr>
                    <td><%= EscapeString.escapeHtml(infosSite.getAlias()) %></td>
                    <td><%= EscapeString.escapeHtml(infosSite.getIntitule()) %></td>
                    <td><%
                        String codeRubrique = infosSite.getCodeRubrique();
                        if (StringUtils.isNotEmpty(codeRubrique)) {
                            RubriqueBean rubriqueMereSite = serviceRubrique.getRubriqueByCode(codeRubrique);
                            if (rubriqueMereSite != null) {
                               %><%= EscapeString.escapeHtml(rubriqueMereSite.getIntitule()) %><%
                           }
                        }
                    %></td>
                    <td><%= templates.get(infosSite.getCodeTemplate())!=null?templates.get(infosSite.getCodeTemplate()).getIntitule():""%></td>
                    <td><a href="<%= URLResolver.getAbsoluteUrl(StringUtils.EMPTY, ContexteUtil.getContexteUniv(), infosSite) %>" title="<%= infosSite.getIntitule() %>"><%= URLResolver.getAbsoluteUrl(StringUtils.EMPTY, ContexteUtil.getContexteUniv(), infosSite) %></a></td>
                    <td><%= infosSite.isActif() ? MessageHelper.getCoreMessage("JTF_OUI") : MessageHelper.getCoreMessage("JTF_NON") %></td>
                    <td>
                        <a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=USINE_SITE&amp;ACTION=<%= SaisieInfosSite.ActionUtilisateur.MODIFIER %>&amp;CODE=<%= infosSite.getAlias() %>" title="<%= MessageHelper.getCoreMessage("USINESITE_LISTE_ACTION_MODIFIER_TITLE") %>"><%= MessageHelper.getCoreMessage("USINESITE_LISTE_ACTION_MODIFIER") %></a> <%
                        if (isGestionnaire) {
                            %><a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=USINE_SITE&amp;ACTION=<%= SaisieInfosSite.ActionUtilisateur.DUPLIQUER %>&amp;CODE=<%= infosSite.getAlias() %>" title="<%= MessageHelper.getCoreMessage("USINESITE_LISTE_ACTION_DUPLIQUER_TITLE") %>"><%= MessageHelper.getCoreMessage("USINESITE_LISTE_ACTION_DUPLIQUER") %></a>
                            <a data-confirm="<%= MessageHelper.getCoreMessage("USINESITE_MESSAGE_SUPPRESSION") %>" href="/servlet/com.jsbsoft.jtf.core.SG?PROC=USINE_SITE&amp;ACTION=<%=SaisieInfosSite.ActionUtilisateur.SUPPRIMER%>&amp;CODE=<%= infosSite.getAlias() %>" title="<%= MessageHelper.getCoreMessage("USINESITE_LISTE_ACTION_SUPPRIMER_TITLE") %>"><%= MessageHelper.getCoreMessage("USINESITE_LISTE_ACTION_SUPPRIMER") %></a><%
                        }
                    %></td>
                </tr><%
            }
        }
        %></tbody>
    </table>
</div>

