<%@page import="java.util.Collection"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve"%>
<%@page import="com.kosmos.service.impl.ServiceManager"%>
<%@page import="com.kosmos.usinesite.processus.SaisieInfosSite"%>
<%@page import="com.kosmos.usinesite.processus.SaisieInfosSiteCommun"%>
<%@page import="com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus"%>
<%@page import="com.kosmos.usinesite.template.bean.TemplateSite"%>
<%@page import="com.kosmos.usinesite.template.property.bean.TemplateSiteProperty"%>
<%@page import="com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur"%>
<%@page import="com.kosmos.usinesite.template.property.service.ServiceTemplateSiteProperty"%>
<%@page import="com.kosmos.usinesite.template.property.service.ServiceTemplateSitePropertyFactory"%>
<%@page import="com.kosmos.usinesite.utils.FrontUASHelper"%>
<%@page import="com.kosmos.usinesite.utils.InfosSiteHelper"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.univ.multisites.InfosSite"%>
<%@page import="com.univ.multisites.bean.impl.InfosSiteImpl" %>
<%@page import="com.univ.objetspartages.services.ServiceRubrique" %>
<%@page import="com.univ.utils.EscapeString" %>
<%@page import="com.univ.utils.UnivFmt" %>
<%@ page import="com.univ.objetspartages.bean.RubriqueBean" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
InfosSite infosSiteCourant = (InfosSite) infoBean.get(SaisieInfosSiteCommun.INFOBEAN_INFOSSITE);
// deux possibilités pour cette interface :
//  - AJOUTER
//  - MODIFIER
SaisieInfosSiteCommun.EtatInterfaceSaisie etatInterface = SaisieInfosSiteCommun.EtatInterfaceSaisie.CREATION;
Object etatInterfaceObject = infoBean.get(SaisieInfosSiteCommun.INFOBEAN_ETAT_INTERFACE_SAISIE);
if (etatInterfaceObject instanceof String) {
    etatInterface = SaisieInfosSiteCommun.EtatInterfaceSaisie.valueOf((String)etatInterfaceObject);
} else {
    etatInterface = (SaisieInfosSiteCommun.EtatInterfaceSaisie) etatInterfaceObject;
}
SaisieInfosSite.ActionUtilisateur actionValidation = SaisieInfosSite.ActionUtilisateur.AUCUNE;  
if (SaisieInfosSiteCommun.EtatInterfaceSaisie.CREATION == etatInterface) {
    actionValidation = SaisieInfosSite.ActionUtilisateur.VALIDER_AJOUTER;
} else if (SaisieInfosSiteCommun.EtatInterfaceSaisie.MODIFICATION == etatInterface) {
    String intituleSite = EscapeString.escapeHtml(infosSiteCourant.getIntitule());
    actionValidation = SaisieInfosSite.ActionUtilisateur.VALIDER_MODIFICATION;
}
List<TemplateSite> listeTemplateSite = infoBean.getList(SaisieInfosSiteCommun.INFOBEAN_VALEURS_LISTE_TEMPLATES_INFOSSITE);
%><form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post" data-ui="tabs">
    <div id="actions">
        <div>
            <ul><%
                if (SaisieInfosSiteCommun.EtatInterfaceSaisie.MODIFICATION == etatInterface) {
                    %><li><button data-confirm="<%=MessageHelper.getCoreMessage("USINESITE_MESSAGE_SUPPRESSION")%>" class="supprimer" type="submit" name="<%=SaisieInfosSite.ActionUtilisateur.SUPPRIMER%>" value="<%=SaisieInfosSite.ActionUtilisateur.SUPPRIMER%>"><%=MessageHelper.getCoreMessage("BO_SUPPRIMER")%></button></li><%
                }
                %><li><button class="enregistrer" type="submit" name="ACTION" value="<%=actionValidation%>"><%=MessageHelper.getCoreMessage("BO_ENREGISTRER")%></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->
    <div data-receive-class class="contenu1">
        <ul id="onglets" data-ui="tabs-header">
            <li class="onglet-contenu1" data-onglet="SITE"><a data-class="contenu1" href="#donnees_site" data-ui="tabs-tab"><%=MessageHelper.getCoreMessage("USINESITE_SAISIE_ONGLET_INFOS")%></a></li>
            <li class="onglet-contenu2" data-onglet="TEMPLATE"><a data-class="contenu2" href="#donnees_template" data-ui="tabs-tab"><%=MessageHelper.getCoreMessage("USINESITE_SAISIE_ONGLET_TEMPLATE")%></a></li>
            <li class="onglet-contenu3" data-onglet="PARAM"><a data-class="contenu3" href="#donnees_param" data-ui="tabs-tab"><%=MessageHelper.getCoreMessage("USINESITE_SAISIE_ONGLET_PARAMETRAGE")%></a></li>
        </ul>
    </div><!-- .contenu1 -->
    <div id="content">
        <div id="donnees_site" data-ui="tabs-container" class="content-deco">
            <div style="display:none"><%
                if (infoBean.getString(SaisieInfosSite.CODE_SITE_DUPLIQUE)!= null) {
                    %><input type="hidden" name="<%= SaisieInfosSite.CODE_SITE_DUPLIQUE %>" value="<%= infoBean.getString(SaisieInfosSite.CODE_SITE_DUPLIQUE)%>" /><%
                }
                %><input type="hidden" name="<%=SaisieInfosSiteCommun.INFOBEAN_ETAT_INTERFACE_SAISIE%>" value="<%=etatInterface%>"/><%
                fmt.insererVariablesCachees(out, infoBean);
            %></div>
            <p class="retrait">
                <input type="checkbox" id="actif" name="<%=SaisieInfosSiteCommun.INFOBEAN_ACTIF%>" value="1" <%=(infosSiteCourant.isActif()) ? "checked=\"checked\"" : StringUtils.EMPTY%>/>
                <label for="actif">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_SITE_ACTIF")%>
                </label>
            </p>
            <p>
                <label class="colonne" for="code">
                    <span class="obligatoire"><%=MessageHelper.getCoreMessage("USINESITE_SAISIE_CODE")%> *</span>
                </label><%
                if (SaisieInfosSiteCommun.EtatInterfaceSaisie.MODIFICATION == etatInterface) {
                    %><%=FrontUASHelper.genererInputHTML("text", SaisieInfosSiteCommun.INFOBEAN_CODE, "code", infosSiteCourant.getAlias(), 64, "typeTextCourt", Boolean.TRUE)%><%
                } else {
                    %><%=FrontUASHelper.genererInputHTML("text", SaisieInfosSiteCommun.INFOBEAN_CODE, "code", infosSiteCourant.getAlias(), 64, "typeTextCourt", Boolean.FALSE)%><%
                }
                %></p>
            <p>
                <label class="colonne" for="intitule">
                    <span class="obligatoire"><%=MessageHelper.getCoreMessage("USINESITE_SAISIE_INTITULE")%> *</span>
                </label><%
                %><%=FrontUASHelper.genererInputHTML("text", SaisieInfosSiteCommun.INFOBEAN_INTITULE, "intitule", infosSiteCourant.getIntitule(), 255, "typeText", Boolean.FALSE)%>
            </p>
            <p>
                <label class="colonne" for="httpHostName">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_HOST_HTTP")%>
                    <%=FrontUASHelper.genererMessageInformatif(MessageHelper.getCoreMessage("USINESITE_SAISIE_MSG_INFO_DOMAINE"))%>
                </label><%
                String httpHostNameInfosSite = StringUtils.EMPTY;
                if (!InfosSiteHelper.isHttpHostNameParDefaut(infosSiteCourant)) {
                    httpHostNameInfosSite = infosSiteCourant.getHttpHostname();
                }
                %><input type="text" id="httpHostName" name="<%=SaisieInfosSiteCommun.INFOBEAN_HTTP_HOSTNAME%>" value="<%=httpHostNameInfosSite%>" maxlength="255"/>
            </p>
            <div><%
                // FAKE pour produit
                String codeRubrique = infosSiteCourant.getCodeRubrique();
                infoBean.set(SaisieInfosSiteCommun.INFOBEAN_CODE_RUBRIQUE, codeRubrique);
                if (StringUtils.isNotEmpty(codeRubrique)) {
                    final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                    RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(codeRubrique);
                    if (rubrique != null) {
                        infoBean.set("LIBELLE_" + SaisieInfosSiteCommun.INFOBEAN_CODE_RUBRIQUE, rubrique.getIntitule());
                    }
                }
                infoBean.set(SaisieInfosSiteCommun.INFOBEAN_CODE_RUBRIQUE, codeRubrique);
                 univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("USINESITE_SAISIE_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE);
            %></div>
            <div>
                <span class="label colonne">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_MODE_REECRITURE")%>
                    <%=FrontUASHelper.genererMessageInformatif(MessageHelper.getCoreMessage("USINESITE_SAISIE_MSG_INFO_MODE_REECRITURE"))%>
                </span><%
                    Collection<Entry<Integer, String>> listeValeursModeReecriture = infoBean.getCollection(SaisieInfosSiteCommun.INFOBEAN_VALEURS_MODE_REECRITURE_RUBRIQUE);
                %><%=FrontUASHelper.genererListeRadio(SaisieInfosSiteCommun.INFOBEAN_MODE_REECRITURE, listeValeursModeReecriture, infosSiteCourant.getModeReecritureRubrique())%>
            </div>
            <p class="retrait">
                <label for="niveauReecritureMin">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_NIV_REECRITURE_MIN")%>
                </label>
                <input type="text" name="<%= SaisieInfosSiteCommun.INFOBEAN_NIVEAU_REECRITURE_MIN %>" id="niveauReecritureMin" value="<%= String.valueOf(infosSiteCourant.getNiveauMinReecritureRubrique()) %>" maxlength="3" size="3" class="typeInt">
                <label for="niveauReecritureMax">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_NIV_REECRITURE_MAX")%>
                </label>
                <input type="text" name="<%= SaisieInfosSiteCommun.INFOBEAN_NIVEAU_REECRITURE_MAX %>" id="niveauReecritureMax" value="<%= String.valueOf(infosSiteCourant.getNiveauMaxReecritureRubrique()) %>" maxlength="3" size="3" class="typeInt">
            </p>
            <div>
                <label class="colonne" for="<%=SaisieInfosSiteCommun.INFOBEAN_ALIAS%>">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_LISTE_ALIAS")%>
                </label>
                <input type="text" data-role="tagsinput" placeholder="<%=MessageHelper.getCoreMessage("USINESITE_AJOUTER_NOM_DOMAINE")%>" name="<%=SaisieInfosSiteCommun.INFOBEAN_ALIAS%>" id="<%=SaisieInfosSiteCommun.INFOBEAN_ALIAS%>" value="<%=StringUtils.join(infosSiteCourant.getListeHostAlias(), ",")%>" />
            </div>
            <div>
                <span class="label colonne"><%=MessageHelper.getCoreMessage("USINESITE_AUTRES_INFORMATIONS")%></span>
                <ul class="en_ligne"><%
                    final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
                    InfosSite sitePrincipal = serviceInfosSite.getPrincipalSite();
                    if (sitePrincipal == null || !infosSiteCourant.getAlias().equals(sitePrincipal.getAlias())){
                        %><li>
                            <input type="checkbox" id="principal" name="<%=SaisieInfosSiteCommun.INFOBEAN_PRINCIPAL%>" value="1" />
                            <label for="principal">
                                <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_SITE_PRINCIPAL")%>
                                <%=FrontUASHelper.genererMessageInformatif(MessageHelper.getCoreMessage("USINESITE_SAISIE_MSG_INFO_SITE_PRINCIPAL"))%>
                            </label>
                            <span><%= MessageHelper.getCoreMessage("USINESITE_SAISIE_IS_NOT_SITE_PRINCIPAL") %> <%= sitePrincipal != null ? sitePrincipal.getIntitule() : "" %></span>
                        </li><%
                    } else {
                        %><li>
                            <span><%= MessageHelper.getCoreMessage("USINESITE_SAISIE_IS_SITE_PRINCIPAL") %></span>
                        </li><%
                    }
                    %><li>
                        <input type="checkbox" id="sso" name="<%=SaisieInfosSiteCommun.INFOBEAN_SSO%>" value="1" <%=(infosSiteCourant.isSso()) ? "checked=\"checked\"" : StringUtils.EMPTY%>/>
                        <label for="sso">
                            <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_SITE_SSO")%>
                            <%=FrontUASHelper.genererMessageInformatif(MessageHelper.getCoreMessage("USINESITE_SAISIE_MSG_INFO_SITE_SSO"))%>
                        </label>
                    </li><!--
                     --><li>
                        <input type="checkbox" id="restreint" name="<%=SaisieInfosSiteCommun.INFOBEAN_RESTREINT%>" value="1" <%=(infosSiteCourant.getRestriction() == 1) ? "checked=\"checked\"" : StringUtils.EMPTY%>/>
                        <label for="restreint">
                            <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_SITE_RESTREINT")%>
                            <%=FrontUASHelper.genererMessageInformatif(MessageHelper.getCoreMessage("USINESITE_SAISIE_MSG_INFO_SITE_RESTREINT"))%>
                        </label>
                    </li><!--
                 --></ul>
            </div>
        </div>

        <div id="donnees_template" data-ui="tabs-container" class="content-deco">
            <p>
                <label class="colonne" for="selectTemplate"><%=MessageHelper.getCoreMessage("USINESITE_SAISIE_TEMPLATE_CHOIX")%></label>
                <select id="selectTemplate" name="<%=SaisieInfosSiteCommun.INFOBEAN_TEMPLATE%>" ><%
                    TemplateSite templateSiteCourant = (TemplateSite)infoBean.get(SaisieInfosSiteCommun.INFOBEAN_VALEURS_TEMPLATE_INFOSSITE);
                    for (TemplateSite templateSite : listeTemplateSite){
                        if (templateSiteCourant != null && templateSiteCourant.getCode().equalsIgnoreCase(templateSite.getCode())){
                            %><option value="<%=templateSite.getCode()%>" selected="selected"><%=templateSite.getIntitule()%></option><%
                        } else {
                            %><option value="<%=templateSite.getCode()%>"><%=templateSite.getIntitule()%></option><%
                        }
                    }
                %></select>
            </p>
            <div id="saisie_template"><%
                for (TemplateSite templateSite : listeTemplateSite){
                    %><div class="donnees_template content-deco" id="<%=templateSite.getCode()%>">
                        <h3><%=templateSite.getIntitule()%></h3><%
                        if (StringUtils.isNotEmpty(templateSite.getUrlVignette()) || StringUtils.isNotEmpty(templateSite.getDescription())){
                            if (StringUtils.isNotEmpty(templateSite.getUrlVignette())){
                                %><img class="vignette" src="<%=templateSite.getUrlVignette()%>" alt="<%= MessageHelper.getCoreMessage("USINESITE_SAISIE_VIGNETTE_TEMPLATE")%>"/><%
                            }
                            if (StringUtils.isNotEmpty(templateSite.getDescription())){
                                %><p>
                                    <span class="label colonne"><%=MessageHelper.getCoreMessage("USINESITE_SAISIE_TEMPLATE_DESCRIPTION")%></span> <span><%=templateSite.getDescription()%></span>
                                </p><%
                            }
                        }
                        if (StringUtils.isNotEmpty(templateSite.getDossierJSP())){
                            %><p>
                                <span class="label colonne"><%=MessageHelper.getCoreMessage("USINESITE_SAISIE_TEMPLATE_DOSSIER_JSP")%></span> <span><%=templateSite.getDossierJSP()%></span>
                            </p><%
                        }
                        if (!templateSite.getListeProprietesComplementaires().isEmpty()){
                            ServiceTemplateSiteProperty serviceTemplateSiteProperty = ServiceTemplateSitePropertyFactory.getServiceTemplateSite();
                            for (TemplateSiteProperty proprieteTemplate : templateSite.getListeProprietesComplementaires()) {
                                try {
                                    TemplateSitePropertyHTMLFormateur formateur = serviceTemplateSiteProperty.getTemplateSitePropertyFormateur(proprieteTemplate);
                                    %><%=formateur.formater(infosSiteCourant, templateSite, proprieteTemplate, fmt, infoBean)%><%
                                } catch (ErreurDonneeNonTrouve e){
                                }
                            }
                        }
                    %></div><%
                }
            %></div>
        </div>
        <div id="donnees_param" data-ui="tabs-container" class="content-deco">
            <p>
                <label class="colonne" for="httpPort">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_PORT_HTTP")%>
                </label><%
                if (InfosSiteHelper.isHttpPortParDefaut(infosSiteCourant)) {
                    %><%=FrontUASHelper.genererInputHTML("text", SaisieInfosSiteCommun.INFOBEAN_HTTP_PORT, "httpPort", StringUtils.EMPTY, 20, 20, "typeInt", "80", Boolean.FALSE)%><%
                } else {
                    int portHttpInfosSite = infosSiteCourant.getHttpPort();
                    %><%=FrontUASHelper.genererInputHTML("text", SaisieInfosSiteCommun.INFOBEAN_HTTP_PORT, "httpPort", String.valueOf(portHttpInfosSite), 20, 20, "typeInt", Boolean.FALSE)%><%
                }
            %></p>
            <p>
                <label class="colonne" for="hostHttps">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_HOST_HTTPS")%>
                </label><%
                String httpsHostNameInfosSite = StringUtils.EMPTY;
                if (!InfosSiteHelper.isHttpsHostNameParDefaut(infosSiteCourant)) {
                    httpsHostNameInfosSite = infosSiteCourant.getHttpsHostname();
                }
                %><%=FrontUASHelper.genererInputHTML("text", SaisieInfosSiteCommun.INFOBEAN_HTTPS_HOSTNAME, "hostHttps", httpsHostNameInfosSite, 255, "typeText", Boolean.FALSE)%>
            </p>
            <p>
                <label class="colonne" for="httpsPort">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_PORT_HTTPS")%>
                </label><%
                if (InfosSiteHelper.isHttpsPortParDefaut(infosSiteCourant)) {
                    %><%=FrontUASHelper.genererInputHTML("text", SaisieInfosSiteCommun.INFOBEAN_HTTPS_PORT, "httpsPort", StringUtils.EMPTY, 20, 20, "typeInt", "443", Boolean.FALSE)%><%
                } else {
                    int portHttpsInfosSite = infosSiteCourant.getHttpsPort();
                    %><%=FrontUASHelper.genererInputHTML("text", SaisieInfosSiteCommun.INFOBEAN_HTTPS_PORT, "httpsPort", String.valueOf(portHttpsInfosSite), 20, 20, "typeInt", Boolean.FALSE)%><%
                }
            %></p>
            <div>
                <span class="label colonne">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_SSL_FO")%>
                </span><%
                Collection<Entry<Integer, String>> listeValeursModeSSL = infoBean.getCollection(SaisieInfosSiteCommun.INFOBEAN_VALEURS_MODE_SSL);
                int sslModeInfosSite = infosSiteCourant.getSslMode();
                %><%=FrontUASHelper.genererListeRadio(SaisieInfosSiteCommun.INFOBEAN_MODE_SSL, listeValeursModeSSL, sslModeInfosSite)%>
            </div>
            <div>
                <span class="label colonne">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_SSL_BO")%>
                </span><%
                Collection<Entry<Integer, String>> listeValeursModeBoSSL = infoBean.getCollection(SaisieInfosSiteCommun.INFOBEAN_VALEURS_MODE_BO_SSL);
                int boSslModeInfosSite = infosSiteCourant.getBoSslMode();
                %><%=FrontUASHelper.genererListeRadio(SaisieInfosSiteCommun.INFOBEAN_BO_MODE_SSL, listeValeursModeBoSSL, boSslModeInfosSite)%>
            </div>
            <div>
                <span class="label colonne">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_ACTIONS_HTTP")%>
                    <%=FrontUASHelper.genererMessageInformatif(MessageHelper.getCoreMessage("USINESITE_SAISIE_MSG_INFO_ACTIONS_HTTP"))%>
                </span><%
                Collection<Entry<Integer, String>> listeActionsHTTP = infoBean.getCollection(SaisieInfosSiteCommun.INFOBEAN_VALEURS_ACTIONS_HTTP);
                Collection<Integer> listeActionsHTTPInfosSite = infosSiteCourant.getHttpActions();
                %><%=FrontUASHelper.genererListeCheckbox(SaisieInfosSiteCommun.INFOBEAN_ACTIONS_HTTP, listeActionsHTTP, listeActionsHTTPInfosSite)%>
            </div>
            <div>
                <span class="label colonne">
                    <%=MessageHelper.getCoreMessage("USINESITE_SAISIE_ACTIONS_HTTPS")%>
                    <%=FrontUASHelper.genererMessageInformatif(MessageHelper.getCoreMessage("USINESITE_SAISIE_MSG_INFO_ACTIONS_HTTPS"))%>
                </span><%
                Collection<Entry<Integer, String>> listeActionsHTTPS = infoBean.getCollection(SaisieInfosSiteCommun.INFOBEAN_VALEURS_ACTIONS_HTTPS);
                Collection<Integer> listeActionsHTTPSInfosSite = infosSiteCourant.getHttpsActions();
                %><%=FrontUASHelper.genererListeCheckbox(SaisieInfosSiteCommun.INFOBEAN_ACTIONS_HTTPS, listeActionsHTTPS, listeActionsHTTPSInfosSite)%>
            </div>
        </div>
    </div>
</form>
