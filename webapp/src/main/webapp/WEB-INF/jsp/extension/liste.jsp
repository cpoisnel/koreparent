<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map" %>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.extension.ExtensionHelper"%>
<%@page import="com.kportal.extension.IExtension" %>
<%@page import="com.kportal.extension.module.IModule" %>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@ page import="com.kportal.extension.bean.ModuleBean" %>
<%@ page import="com.kportal.extension.bean.ExtensionBean" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<div id="content">
    <div class="outils_contenu"><%
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    Locale locale = ctx.getLocale();
    List<String> expandedParams = StringUtils.isBlank(request.getParameter("EXPANDED")) ? new ArrayList<String>() : Arrays.asList(request.getParameter("EXPANDED").split(";"));
    Map<ExtensionBean,List<ModuleBean>> lstExtension = infoBean.getMap("LISTE_EXTENSIONS");
    if (!lstExtension.isEmpty()){
        %><ul><%
            for(ExtensionBean extension : lstExtension.keySet()){
                String extensionClass = "actif";
                switch(extension.getEtat()){
                    case IExtension.ETAT_NON_VALIDE:extensionClass="invalide";
                        break;
                    case IExtension.ETAT_NON_ACTIF:extensionClass="inactif";
                        break;

                }
                List<ModuleBean> lstModule = lstExtension.get(extension);
                String url = extension.getUrl();
                if (StringUtils.isEmpty(url)){
                    url ="http://www.ksup.org/fr/telechargement/extensions-editeur/";
                }
            %><li id="<%=extension.getIdExtension()%>" class="<%=extensionClass%>">
                <div class="extension">
                    <img class="icon" alt="extension icon" src="<%=StringUtils.isBlank(extension.getLogo()) ? "/adminsite/styles/img/64x64/extension-default.png" : extension.getLogo()%>"/> <!-- getLogo() -->
                    <div class="infos">
                        <p class="extension_libelle"><%=ExtensionHelper.getMessage(extension.getIdBean(), extension.getLibelle())%>
                        <span>(<%=StringUtils.isBlank(extension.getVersion()) ? MessageHelper.getCoreMessage("BO_EXTENSION_VERS_INDETERMINEE") : extension.getVersion()%>)</span>
                        <%if (extension.getEtat().equals(IExtension.ETAT_NON_VALIDE)){%>
                            <span class="erreur">
                                <%if (!ExtensionHelper.checkCoreVersion(extension.getCoreVersion())){ %>
                                    <%= MessageHelper.getCoreMessage("BO_EXTENSION_VERSION_PAS_COMPATIBLE") %>
                                <%} else{%>
                                    <%= MessageHelper.getCoreMessage("BO_EXTENSION_ERREUR_CHARGEMENT") %>
                                <%} %>
                            </span>
                        <%}%>
                        </p>
                        <p class="extension_description"><%=ExtensionHelper.getMessage(extension.getIdBean(), extension.getDescription())%></p>
                        <p class="extension_auteur"><%=extension.getAuteur()%> &ndash; <a href="<%=url%>"><img src="/adminsite/styles/img/16x16/infos.png" title="<%= MessageHelper.getCoreMessage("BO_EXTENSION_PLUS_INFOS") %>" alt="<%= MessageHelper.getCoreMessage("BO_EXTENSION_PLUS_INFOS") %>" /></a></p>
                    </div><!-- .infos -->
                    <div class="extension_actions">
                        <%
                        if (!lstModule.isEmpty()){
                            %><button type="button" class="details deplier"><%=MessageHelper.getCoreMessage("BO_DETAILS")%></button><%
                        }
                        if(extension.getType()==IExtension.TYPE_PARAMETRABLE && !extension.getEtat().equals(IExtension.ETAT_NON_VALIDE)) {
                            %><button type="button" class="interrupteur<%=extension.getEtat()==IExtension.ETAT_ACTIF ? " on" : " off"%>" data-activated="<%=extension.getEtat()==IExtension.ETAT_ACTIF%>" data-extensionid="<%=extension.getIdExtension()%>" ><%=MessageHelper.getCoreMessage(locale, (extension.getEtat()==IExtension.ETAT_ACTIF ? "BO_DESACTIVER" : "BO_ACTIVER"))%></button><%
                        }
                        if(extension.getType()==IExtension.TYPE_PARAMETRABLE){
                            %><button type="button" class="supprimer" data-message="<%=MessageHelper.getCoreMessage(locale, "BO_EXTENSION_SUPPRESSION_CONFIRMATION") %>" data-extensionid="<%=extension.getIdExtension()%>" ><%=MessageHelper.getCoreMessage(locale, "BO_SUPPRIMER") %></button><%
                        }
                        %>
                    </div><!-- .extension_actions -->
                </div><!-- .extension -->
                <%if (!lstModule.isEmpty()){%>
                <div class="extension-modules <%=expandedParams.contains(extension.getIdExtension().toString()) ? "expanded" : "collapsed" %>">
                    <ul><%
                        for(ModuleBean module : lstModule){
                            String moduleClass ="actif";
                            switch(module.getEtat()){
                                case IModule.ETAT_NON_VALIDE:moduleClass="invalide";
                                    break;
                                case IModule.ETAT_NON_ACTIF:moduleClass="inactif";
                                    break;
                            }
                            %><li>
                                <div class="infos <%=moduleClass%>">
                                    <p><%=ExtensionHelper.getMessage(module.getIdBeanExtension(), module.getLibelle())%></p>
                                    <% if (StringUtils.isNotEmpty(module.getDescription())) {%>
                                        <p class="extension_description" ><%=module.getDescription()%></p>
                                    <%}%>
                                </div><!-- .infos -->
                                <div class="extension_actions">
                                    <button type="button" class="interrupteur<%=module.getEtat()==IModule.ETAT_ACTIF ? " on" : " off" %><%=module.getType()==IModule.TYPE_PARAMETRABLE ? "" : " verrou" %>" data-enabled="<%=module.getType()==IModule.TYPE_PARAMETRABLE%>" data-activated="<%=module.getEtat()==IModule.ETAT_ACTIF%>" data-extensionid="<%=module.getId()%>"><%=MessageHelper.getCoreMessage(locale, (module.getEtat()==IModule.ETAT_ACTIF ? "BO_DESACTIVER" : "BO_ACTIVER"))%></button>
                                </div><!-- .extension_actions -->
                            </li><%
                        }
                        %>
                    </ul>
                </div><!-- .extension-modules -->
                <%
                } %>
            </li><%
        }
        %></ul><%
    }
    %>
    </div><!-- .outils_contenu -->
</div><!-- #content -->