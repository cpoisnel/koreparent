<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />

<div id="actions">
    <div>
        <ul>
            <li><button type="button" class="enregistrer" onclick="soumettreFormulaire('VALIDER');return false;" value="<%=MessageHelper.getCoreMessage("BO_ENREGISTRER")%>"><%=MessageHelper.getCoreMessage("BO_ENREGISTRER")%></button></li>
            <li><button type="button" class="supprimer" onclick="if (confirm('<%=MessageHelper.getCoreMessage("BO_EXTENSION_SUPPRESSION_CONFIRMATION")%>')){soumettreFormulaire('SUPPRIMER'); return false;}" value="<%=MessageHelper.getCoreMessage("BO_SUPPRIMER")%>"><%=MessageHelper.getCoreMessage("BO_ENREGISTRER")%></button></li>
        </ul>
        <div class="clearfix"></div>
        <span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
    </div>
</div><!-- #actions -->
<br/>
<div id="content">
<form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post">

<input type="hidden" name="ACTION" value="" />
<% fmt.insererVariablesCachees(out, infoBean); %>

<!-- Utile pour la restauration des données --><% 
if (infoBean.get("ID_CI_RESTAURATION") != null) { 
    %><input type="hidden" name="ID_CI_RESTAURATION" value="<%= infoBean.getString("ID_CI_RESTAURATION")%>" />
    <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%= infoBean.getString("NOM_JSP_RESTAURATION")%>" /><%
} %>

<fieldset><legend><%= MessageHelper.getCoreMessage("BO_EXTENSION_INFORMATIONS") %></legend>
<table class="saisie mise_en_page" role="presentation">
<%
    univFmt.insererChampSaisie(fmt, out, infoBean, "LIBELLE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255,  MessageHelper.getCoreMessage("BO_EXTENSION_LIBELLE"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "DESCRIPTION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 512,  MessageHelper.getCoreMessage("BO_EXTENSION_DESCRIPTION"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "AUTEUR", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255,  MessageHelper.getCoreMessage("BO_EXTENSION_AUTEUR"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "COPYRIGHT", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255,  MessageHelper.getCoreMessage("BO_EXTENSION_COPYRIGHT"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_CREATION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_EXTENSION_DATE_CREATION"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_MODIFICATION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_EXTENSION_DATE_MODIFICATION"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "TABLES", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 512,  MessageHelper.getCoreMessage("BO_EXTENSION_TABLES"));
%>

</table>
</fieldset>
<fieldset><legend><%= MessageHelper.getCoreMessage("BO_ETAT") %></legend>
<table class="saisie mise_en_page" role="presentation">
    <tr>
        <td><% fmt.insererChampSaisie(out, infoBean, "ETAT", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_RADIO, 0, 0, ""); %><%= MessageHelper.getCoreMessage("BO_DESACTIVER") %></td>
    </tr>
    <tr>
        <td><% fmt.insererChampSaisie(out, infoBean, "ETAT", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_RADIO, 0, 0, ""); %><%= MessageHelper.getCoreMessage("BO_ACTIVER") %></td>
    </tr>
</table>
</fieldset>

</form>
</div>

<script type="text/javascript">
    function soumettreFormulaire(action) {
        window.document.forms[0].ACTION.value=action;
        window.document.forms[0].submit();
    }
</script>
