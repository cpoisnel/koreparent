<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@taglib prefix="components" uri="http://kportal.kosmos.fr/tags/components" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
    // declaration des onglets
    String typeObjet = ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")).toUpperCase();
    Objetpartage module = ReferentielObjets.getObjetByNom(typeObjet);
    String[] nomOnglet = {"PRINCIPAL", "COMPLEMENTS"};
    String[] libelleOnglet = {module.getMessage("BO_ONGLET_CONTENU_PRINCIPAL"), module.getMessage("BO_ONGLET_INFOS_COMPLEMENTS")};
%><%@ include file="/adminsite/objetspartages/template/header_saisie.jsp" %><%

/**
***************************************************************************
************          ONGLET CONTENU PRINCIPAL              ***************
***************************************************************************
--*/
if (sousOnglet.equals("PRINCIPAL")) {
    %><div class="fieldset neutre"><%
    if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
        univFmt.insererComboHashtable(fmt, out, infoBean, "LANGUE", FormateurJSP.SAISIE_OBLIGATOIRE, "LISTE_LANGUES",module.getMessage("BO_LANGUE"));
    } else{
        univFmt.insererChampSaisie(fmt, out, infoBean, "CODE", UnivFmt.getModeSaisieCode(infoBean), FormateurJSP.FORMAT_TEXTE, 0, 20, module.getMessage("BO_LIBELLE_CODE"));
    }
    univFmt.insererChampSaisie(fmt, out, infoBean, "TITRE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255, module.getMessage("BO_FICHE_TITRE"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "SOUS_TITRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 512, module.getMessage("BO_ARTICLE_SOUS_TITRE"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_ARTICLE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, module.getMessage("BO_ARTICLE_DATE_ARTICLE"));
    %></div>
    <fieldset>
            <legend><%=module.getMessage("BO_FICHE_RATTACHEMENTS")%></legend><%
        univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, module.getMessage("BO_ARTICLE_CODE_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE);
        univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, module.getMessage("BO_ARTICLE_CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE);
    %></fieldset>
    <div class="fieldset neutre"><%
    univFmt.insererKmultiSelectLtl(fmt, out, infoBean, "THEMATIQUE", FormateurJSP.SAISIE_FACULTATIF, module.getMessage("BO_ARTICLE_THEMATIQUE"), "LISTE_THEMATIQUES", "8pt", UnivFmt.CONTEXT_DEFAULT);
    univFmt.insererChampSaisie(fmt, out, infoBean, "CHAPEAU", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 2048, module.getMessage("BO_ARTICLE_CHAPEAU"));
    %><components:toolbox fieldName="CORPS" label="BO_ARTICLE_CORPS" editOption="<%= FormateurJSP.SAISIE_FACULTATIF %>" min="0" max="65535" /><%
    univFmt.insererSaisieFichierGw(out, infoBean, module.getMessage("BO_ARTICLE_PHOTO"), "PHOTO", "0");
    %></div><%
}
/**
***************************************************************************
*********       ONGLET INFORMATIONS COMPLEMENTAIRES           *************
***************************************************************************
--*/
if (sousOnglet.equals("COMPLEMENTS")) {
    %><div class="fieldset neutre"><%
    univFmt.insererChampSaisie(fmt, out, infoBean, "ORDRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 3, module.getMessage("BO_ARTICLE_ORDRE"));
    %>
    <span class="label colonne"><%=module.getMessage("ST_FICHIERS_JOINTS") %></span>
    <%univFmt.insererContenuListeFichiersGw(out, infoBean, module.getMessage("ST_AJOUT_FICHIER"), "", "0");%>
    </div>
    <%
}
%><%@ include file="/adminsite/fiche_bas.jsp" %>
</form>