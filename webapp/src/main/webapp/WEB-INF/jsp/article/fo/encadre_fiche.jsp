<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.kportal.ihm.utils.EncadresFrontUtils"%>
<%@page import="com.kportal.ihm.utils.FrontUtil"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%
   ContexteUniv ctx = ContexteUtil.getContexteUniv();
   Objetpartage module = ReferentielObjets.getObjetByNom(ReferentielObjets.getNomObjet(ctx.getFicheCourante()));
   String fichiersJoints = FrontUtil.calculerListeFichiersJoints(ctx, ctx.getFicheCourante());
   if (StringUtils.isNotBlank(fichiersJoints)) {
       %><div class="<%= EncadresFrontUtils.AUTO_FICHE%>">
        <h2><%= module.getMessage("ST_DOCUMENTS_A_TELECHARGER") %></h2>
        <div class="encadre_contenu">
            <%= fichiersJoints %>
        </div><!-- .encadre_contenu -->
    </div><!-- .<%= EncadresFrontUtils.AUTO_FICHE%> --><%
   }
%>