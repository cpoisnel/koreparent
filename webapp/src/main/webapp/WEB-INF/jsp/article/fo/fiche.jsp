<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.Formateur" %>
<%@page import="com.kportal.cms.objetspartages.Objetpartage" %>
<%@page import="com.univ.objetspartages.bean.MetatagBean" %>
<%@page import="com.univ.objetspartages.om.Article" %>
<%@page import="com.univ.objetspartages.om.ReferentielObjets, com.univ.objetspartages.services.ServiceRessource, com.univ.objetspartages.util.MetatagUtils" errorPage="/jsp/jsb_exception.jsp" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.URLResolver" %>
<%@ page import="com.univ.objetspartages.bean.RessourceBean" %>
<%@ page import="com.univ.objetspartages.util.RessourceUtils" %>
<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<%

    /********************************************
    *         Initialisation de la fiche            *
    *********************************************/
    final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    Article article = (Article) ctx.getFicheCourante();
    Objetpartage module = ReferentielObjets.getObjetByNom(ReferentielObjets.getNomObjet(article));

    /* Lecture des champs */
    String corps = article.getFormatedCorps();
    String thematique = article.getLibelleThematique();
    DateFormat dateFormatLong = DateFormat.getDateInstance(DateFormat.LONG, ctx.getLocale());
    String urlPhoto = "";
    String altPhoto = "";
    /********************************************
    *         Calcul des champs         *
    *********************************************/
    /* calcul des propriétés de la photo jointe */
    RessourceBean photoJointe = serviceRessource.getFile(article);
    if (photoJointe != null) {
        urlPhoto = RessourceUtils.getUrlPhoto(photoJointe);
        altPhoto = RessourceUtils.getLegende(photoJointe);
    }
    /********************************************
    *         Affichage des champs         *
    *********************************************/
    /* affichage du sous-titre */
    if (StringUtils.isNotBlank(article.getSousTitre())) {
        %><p id="soustitre"><%=article.getSousTitre()%></p><%
    }
    final MetatagBean meta = MetatagUtils.lireMeta(article);
    if(Formateur.estSaisie(meta.getMetaDateMiseEnLigne())){
        %><div class="date-publication-maj">
            <span class="date-publication"><%=  module.getMessage("ST_ARTICLE_PUBLIE") + " " + dateFormatLong.format( meta.getMetaDateMiseEnLigne()) %></span><%
        if (Formateur.estSaisie(article.getDateModification()) && !article.getDateModification().equals(meta.getMetaDateMiseEnLigne())) {
            %> <span>&ndash;</span> <span class="date-maj"><%= module.getMessage("ST_ARTICLE_MAJ")  + " " +  dateFormatLong.format(article.getDateModification()) %></span><%
        }
        %></div><%
    }
    /* affichage du chapeau */
    if (StringUtils.isNotBlank(article.getChapeau())) {
        %><p id="resume"><%=article.getChapeau()%></p><%
    }
    /* affichage de la photo jointe et du corps */
    if (urlPhoto.length() > 0 || corps.length() > 0) {
        %><div id="description" class="toolbox"><%
        if (urlPhoto.length() > 0) {
            %><div class="photo">
                <img src="<%=URLResolver.getAbsoluteUrl(urlPhoto, ctx)%>" alt="<%=altPhoto%>" title="<%=altPhoto%>" /><%
                if (altPhoto.length() > 0) {
                    %><p class="legende"><%=altPhoto%></p><%
                }
            %></div> <!-- .photo --><%
        }
        %><%=corps%>
        <br class="separateur" />
        </div> <!-- #corps --><%
    }
%>