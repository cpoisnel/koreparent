<%@page import="com.kosmos.service.impl.ServiceManager"%>
<%@page import="com.univ.objetspartages.om.StructureModele"%>
<%@page import="com.univ.objetspartages.services.ServiceStructure"%>
<%@ page import="com.univ.objetspartages.util.LabelUtils" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.RequeteUtil" %>
<%
    final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    String parametreEnCours = "";
    String requete = (String)ctx.getData("requete");
    String modifRecherche = (String)ctx.getData("modifRecherche");
    String criteresRechAvancee = (String)ctx.getData("criteresRechAvancee");

    parametreEnCours = RequeteUtil.renvoyerParametre(requete, "TITRE");
    if (parametreEnCours.length() > 0) {
        modifRecherche += "&TITRE=" + com.univ.utils.EscapeString.escapeURL(parametreEnCours);
        criteresRechAvancee += "\"" + parametreEnCours + "\"";
    }

    parametreEnCours = RequeteUtil.renvoyerParametre(requete, "DATE_DEBUT");
    if (parametreEnCours.length() > 0) {
        modifRecherche += "&DATE_DEBUT=" + com.univ.utils.EscapeString.escapeURL(parametreEnCours);
        criteresRechAvancee += "\"" + parametreEnCours + "\"";
    }

    parametreEnCours = RequeteUtil.renvoyerParametre(requete, "DATE_FIN");
    if (parametreEnCours.length() > 0) {
        modifRecherche += "&DATE_FIN=" + com.univ.utils.EscapeString.escapeURL(parametreEnCours);
        criteresRechAvancee += "\"" + parametreEnCours + "\"";
    }

    parametreEnCours = RequeteUtil.renvoyerParametre(requete, "THEMATIQUE");
    if (parametreEnCours.length() > 0 && !parametreEnCours.equals("0000")) {
        modifRecherche += "&THEMATIQUE=" + com.univ.utils.EscapeString.escapeURL(parametreEnCours);
        criteresRechAvancee += "\"" + LabelUtils.getLibelle("04", parametreEnCours, ctx.getLocale()) + "\"";
    }

    parametreEnCours = RequeteUtil.renvoyerParametre(requete, "CODE_RATTACHEMENT");
    if (parametreEnCours.length() > 0) {
        modifRecherche += "&CODE_RATTACHEMENT=" + com.univ.utils.EscapeString.escapeURL(parametreEnCours);
        modifRecherche += "&LIBELLE_CODE_RATTACHEMENT=" + serviceStructure.getDisplayableLabel(parametreEnCours, ctx.getLangue());
        criteresRechAvancee += "\"" + serviceStructure.getDisplayableLabel(parametreEnCours, ctx.getLangue()) + "\"";
    }

    ctx.putData("modifRecherche",modifRecherche);
    ctx.putData("criteresRechAvancee",criteresRechAvancee);
%>