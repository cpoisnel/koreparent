<%@page import="java.util.List"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.textsearch.ResultatRecherche"%>
<%@page import="com.univ.objetspartages.om.Article"%>
<%@page import="com.univ.objetspartages.om.FicheUniv"%>
<%@page import="com.univ.objetspartages.services.ServiceRessource"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.RequeteUtil"%>
<%@ page import="com.univ.utils.URLResolver" %>
<%@ page import="com.univ.utils.UnivWebFmt" %>
<%@ page import="com.univ.objetspartages.bean.RessourceBean" %>
<%@ page import="com.univ.objetspartages.util.RessourceUtils" %>
<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<jsp:useBean id="frontOfficeBean" class="com.univ.url.FrontOfficeBean" scope="request" /><%
    final ServiceRessource serviceRessource = ServiceManager.getServiceForBean(RessourceBean.class);
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    String param = (String)request.getAttribute("param");
    String style = RequeteUtil.renvoyerParametre(param, "STYLE");
    List<ResultatRecherche> listeFiches = (List<ResultatRecherche>)request.getAttribute("listeFiches");
    int largeurPhotoMax = 80;
    %><ul class="objets articles"><%
    for (ResultatRecherche res : listeFiches){
        // Lecture de la fiche que l'on doit traiter
        FicheUniv fiche = RequeteUtil.lireFiche(res);
        /* il est possible que la fiche récupérée soit null, si son état a changé entre sa mise en cache et le rafraichissement de ce dernier */
        if (fiche == null) {
            continue;
        }
        // Url d'accès à cette fiche
        String urlFiche = URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(ctx, fiche), ctx);
        Article article = (Article) fiche;
        RessourceBean photo = serviceRessource.getFile(article);
        String urlPhoto = StringUtils.EMPTY;
        if (photo != null) {
            urlPhoto = RessourceUtils.getUrlVignette(photo);
        }
        // détaillé
        if ("0001".equals(style)) {
            %><li class="avec_vignette"><%
                if(StringUtils.isNotBlank(urlPhoto)) {
                    %><div class="vignette_deco" style="width:<%=largeurPhotoMax%>px;">
                        <img class="vignette" src="<%=URLResolver.getRessourceUrl(urlPhoto, ctx)%>" alt="" />
                    </div><%
                }
                %><div class="vignette_deco2">
                    <strong><a href="<%=urlFiche%>"><%=article.getTitre()%></a></strong><%
                    if (StringUtils.isNotBlank(article.getChapeau())) {
                        %><div class="resume"><%=article.getChapeau()%></div><!-- .resume --><%
                    }
                %></div> <!-- .vignette_deco2 -->
            </li><%
        } else {
            //simplifié
            %><li><a href="<%=urlFiche%>"><%=article.getTitre()%></a></li><%
        }
    }
    %>
    </ul>
