<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.cms.objetspartages.Objetpartage" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@ taglib prefix="components" uri="http://kportal.kosmos.fr/tags/components" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<% Objetpartage module = ReferentielObjets.getObjetByCode(infoBean.getString("CODE_OBJET")); %>
<jsp:include page="/adminsite/saisie_front/entete_saisie_front.jsp" /><%
if (infoBean.getString("CODE_RUBRIQUE").length() > 0) {
%><p>
	<label for="CODE_RUBRIQUE"><%=module.getMessage("ST_ARTICLEFRONT_RUBRIQUE")%></label><!-- 
	 --><input type="hidden" name="CODE_RUBRIQUE" value="<%=infoBean.getString("CODE_RUBRIQUE")%>" /><!-- 
	 --><input type="text" readonly="readonly" name="LIBELLE_CODE_RUBRIQUE" size="30" value="<%=infoBean.get("LIBELLE_CODE_RUBRIQUE")%>" />
</p><%
}
univFmt.insererKmultiSelectLtl(fmt, out, infoBean, "THEMATIQUE", FormateurJSP.SAISIE_FACULTATIF, module.getMessage("ST_ARTICLEFRONT_THEMATIQUE"), "LISTE_THEMATIQUES", "8pt", UnivFmt.CONTEXT_DEFAULT);
%><p>
	<label for="TITRE"><span class="obligatoire"><%=module.getMessage("ST_ARTICLEFRONT_TITRE")%>*</span></label>
	<%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "TITRE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255, module.getMessage("ST_ARTICLEFRONT_TITRE"));%>
</p>
<p>
	<label for="SOUS_TITRE"><span class="obligatoire"><%=module.getMessage("ST_ARTICLEFRONT_SOUSTITRE")%></span></label>
	<%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "SOUS_TITRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 512, module.getMessage("ST_ARTICLEFRONT_SOUSTITRE"));%>
</p>
<p>
	<label for="DATE_ARTICLE"><%=module.getMessage("ST_ARTICLEFRONT_DATE_PUBLICATION")%></label><!-- 
	 --><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_ARTICLE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, module.getMessage("ST_ARTICLEFRONT_DATE_PUBLICATION"), "", true, "");%>
</p>
<p>
	<label for="CHAPEAU"><%=module.getMessage("ST_ARTICLEFRONT_RESUME")%></label>
	<%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "CHAPEAU", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 2048, module.getMessage("ST_ARTICLEFRONT_RESUME"));%>
</p>
<components:toolbox fieldName="CORPS" label="ST_ARTICLEFRONT_CORPS" editOption="<%= FormateurJSP.SAISIE_FACULTATIF %>" min="0" max="65535"/>
<p class="edition_fiche__photo">
	<label for="PHOTO"><%=module.getMessage("ST_ARTICLEFRONT_PHOTO")%></label><!-- 
	 --><%univFmt.insererContenuSaisieFichierGw(out, infoBean, "PHOTO", "form_saisie_front");%>
</p>
<fieldset class="saisie__fichiersJoints">
	<legend><%=module.getMessage("ST_ARTICLEFRONT_FICHIER")%></legend> 
	<%univFmt.insererContenuListeFichiersGw(out, infoBean, "Ajouter un fichier", "fichier", "form_saisie_front");%>
</fieldset> <!-- .multi-col --><%
if (infoBean.getString("PUBLIC_VISE_DSI").length()>0) {
%><p>
	<label for="PUBLIC_VISE_DSI"><%=module.getMessage("ST_ARTICLEFRONT_PUBLIC_VISE")%></label><!-- 
	 --><input type="hidden" name="PUBLIC_VISE_DSI" value="<%=infoBean.getString("PUBLIC_VISE_DSI")%>" />
	<% if (infoBean.get("LIBELLE_PUBLIC_VISE_DSI") != null) {
			String chaineDsi = "";
			String[] temp = infoBean.getString("LIBELLE_PUBLIC_VISE_DSI").split(";", -2);
			for (int i = 0; i < temp.length; i++) {
				if (chaineDsi.length() > 0)
					chaineDsi += "\n";
				chaineDsi += temp[i];
			} %>
			<textarea rows="5" cols="30" readonly="readonly"><%=chaineDsi%></textarea>
	<% } %>
	
</p><%
}
%><jsp:include page="/adminsite/saisie_front/bas_saisie_front.jsp" />
