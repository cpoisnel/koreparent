<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.URLResolver" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<form class="en-colonne" action="<%= URLResolver.getAbsoluteUrl("/servlet/com.jsbsoft.jtf.core.SG", ContexteUtil.getContexteUniv(), URLResolver.DEMANDER_MDP_FRONT) %>" enctype="multipart/form-data" method="post">
<div class="formulaire_hidden">
    <% fmt.insererVariablesCachees( out, infoBean); %>
</div> <!-- .formulaire_hidden -->

<p class="msg-aide"><%=MessageHelper.getCoreMessage("ST_ENTREZ_EMAIL")%></p>

<p>
    <label for="mail"><%=MessageHelper.getCoreMessage("ST_DSI_EMAIL")%> (*)</label><!--
    --><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "EMAIL", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_EMAIL, 0, 64, MessageHelper.getCoreMessage("ST_DSI_EMAIL"),"EMAIL=YES",false,"");%>
</p>

<p class="msg-aide"><%=MessageHelper.getCoreMessage("ST_ENTREZ_LOGIN")%></p>

<p>
    <label for="login"><%=MessageHelper.getCoreMessage("ST_DSI_LOGIN")%></label><!--
    --><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LOGIN", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 64, MessageHelper.getCoreMessage("ST_DSI_LOGIN"));%>
</p>

  <p><%= MessageHelper.getCoreMessage("ST_LOGIN_OUBLIE") %></p>

<p id="valider-formulaire" class="validation">
    <input class="submit" type="submit" name="VALIDER" value="<%=MessageHelper.getCoreMessage("JTF_BOUTON_VALIDER")%>" />
</p>

</form>

