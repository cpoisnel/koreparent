<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.identification.GestionnaireIdentification"%>
<%@page import="com.jsbsoft.jtf.identification.SourceAuthDefautKbd"%>
<%@page import="com.jsbsoft.jtf.identification.SourceAuthHelper"%>
<%@ page import="com.kosmos.userfront.module.UserFrontModule, com.kportal.core.config.MessageHelper, com.kportal.core.config.PropertyHelper, com.univ.utils.ContexteUtil, com.univ.utils.URLResolver, com.univ.utils.UnivWebFmt" %>
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />

<%
if(infoBean.get("URL_DEMANDEE") != null) { 
    UnivWebFmt.initialiserUrlRedirection(ContexteUtil.getContexteUniv().getRequeteHTTP().getSession(),infoBean.getString("URL_DEMANDEE"));
}
boolean motDePasseChangeable =  GestionnaireIdentification.getInstance().estSourceAuth(SourceAuthDefautKbd.SOURCE_LIBELLE_KBD);;
boolean kUserConnector = SourceAuthHelper.isDoubleSourceAuthActif(request,infoBean);
%>

<form class="gestion en-colonne" action="<%= URLResolver.getAbsoluteUrl("/servlet/com.jsbsoft.jtf.core.SG", ContexteUtil.getContexteUniv(), URLResolver.LOGIN_FRONT) %>" enctype="multipart/form-data" method="post" <%=("0".equals(PropertyHelper.getCoreProperty("utilisateur.login.autocomplete"))?"autocomplete=\"off\"":"")%>>

<div class="formulaire_hidden">

<% if(infoBean.get("URL_REDIRECT") != null) { %>
    <input type="hidden" name="URL_REDIRECT" value="<%=infoBean.get("URL_REDIRECT")%>"/>
<% } %>

<%
// Ajout d'un paramétre à retourner au processus d'IDENTIFICATION.
if (kUserConnector) { %>
    <input type="hidden" name="KUSER_CONNECTOR" value="1"/><%
}%>

<input type="hidden" name="PROC" value="IDENTIFICATION<%=(infoBean.getNomProcessus().contains("_FRONT") ?"_FRONT":"")%>" />
<input type="hidden" name="#ECRAN_LOGIQUE#" value="PRINCIPAL" />
<input type="hidden" name="#ETAT#" value="null" />

</div> <!-- fin .formulaire_hidden -->

<p>
    <label for="LOGIN"><%=MessageHelper.getCoreMessage("ST_DSI_LOGIN")%></label>
    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LOGIN", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 64, 20, MessageHelper.getCoreMessage("ST_DSI_LOGIN"),"",false,"");%>
</p>

<p>
    <label for="PASSWORD"><%=MessageHelper.getCoreMessage("ST_DSI_MDP")%></label>
    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "PASSWORD", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 64, 20, MessageHelper.getCoreMessage("ST_DSI_MDP"),"PASSWORD=YES",false,"");%>
</p>

<% 
    //si CAS ou LDAP, pas possible de changer de mot de passe
    String accountCreation = UserFrontModule.getUrlUserRegistrationFront();
    if(motDePasseChangeable || StringUtils.isNotBlank(accountCreation)) {
        %><p><%
            if (motDePasseChangeable) {
                %><a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=IDENTIFICATION_FRONT&amp;ACTION=DEMANDER_MDP"><%=MessageHelper.getCoreMessage("ST_OUBLI_MDP") %></a> <%
            }
            if (StringUtils.isNotBlank(accountCreation)) {
                %>&nbsp;<a href="<%= URLResolver.getAbsoluteUrl(accountCreation,ContexteUtil.getContexteUniv())%>"><%= MessageHelper.getCoreMessage("FO.USERFRONT.CREATION")%></a><%
            }
        %></p><%
    }
    %>

<p id="valider-formulaire" class="validation">
      <input class="submit" type="submit" name="VALIDER" value="<%=MessageHelper.getCoreMessage("JTF_BOUTON_VALIDER")%>"/>
</p>

</form>