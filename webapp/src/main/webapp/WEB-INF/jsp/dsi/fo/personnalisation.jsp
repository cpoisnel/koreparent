<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@ page import="com.kportal.core.config.MessageHelper"%>
<%@ page import="com.univ.utils.ContexteUtil, com.univ.utils.URLResolver"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<p class="msg-aide"><%= MessageHelper.getCoreMessage("ST_FRONT_INTITULES")%> <span class="obligatoire">*</span> <%= MessageHelper.getCoreMessage("ST_FRONT_OBLIGATOIRES")%></p>

<form id="preferences_utilisateur" class="edition_fiche" action="<%= URLResolver.getAbsoluteUrl("/servlet/com.jsbsoft.jtf.core.SG", ContexteUtil.getContexteUniv(), URLResolver.PERSONNALISER_FRONT) %>" enctype="multipart/form-data" method="post" autocomplete="off">
  <% fmt.insererVariablesCachees( out, infoBean); %>
  
  <fieldset>
    <legend><%=MessageHelper.getCoreMessage("ST_DSI_PREFIDENTIFICATION")%></legend>
    
    <p>
        <label for="LOGIN"><%=MessageHelper.getCoreMessage("ST_DSI_LOGIN")%></label>
        <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LOGIN", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 64, MessageHelper.getCoreMessage("ST_DSI_LOGIN"));%>
    </p>
    
    <p>
        <label for="NOM"><%=MessageHelper.getCoreMessage("ST_DSI_NOM")%></label>
            <%fmt.insererChampSaisie( out, infoBean, "NOM" , FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255);%>
    </p>
    
    <p>
        <label for="PRENOM"><%=MessageHelper.getCoreMessage("ST_DSI_PRENOM")%></label>
            <%fmt.insererChampSaisie( out, infoBean, "PRENOM" , FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255);%>
    </p>
    
    <p>
        <label for="CODE_STRUCTURE"><%=MessageHelper.getCoreMessage("ST_DSI_RATTACHEMENT")%></label>
        <% if (StringUtils.isNotEmpty(infoBean.getString("LIBELLE_CODE_STRUCTURE"))){
            %><%= infoBean.getString("LIBELLE_CODE_STRUCTURE")%><%
        } else { %>
            <%=MessageHelper.getCoreMessage("ST_DSI_AUCUN_RATTACHEMENT")%>
        <% } %>
    </p>
    
    <% if (infoBean.getString("MODIFICATION_PROFIL_DEFAUT").equals("1"))    { %>
        <p>
            <label for="PROFIL_DEFAUT"><%=MessageHelper.getCoreMessage("ST_DSI_PROFIL_DEFAUT")%></label>
                <%fmt.insererComboHashtable( out, infoBean, "PROFIL_DEFAUT", FormateurJSP.SAISIE_OBLIGATOIRE, "LISTE_PROFILS" , "LIB="+ MessageHelper.getCoreMessage("ST_DSI_PROFIL_DEFAUT")); %>
        </p>
     <% } %>
  
  </fieldset>
  
  <% if (infoBean.getString("DROIT_MODIFICATION_PASSWORD").equals("1"))    { %>
  
  <fieldset>
      <legend><%=MessageHelper.getCoreMessage("ST_DSI_MDP")%></legend>
  
      <p>
          <label for="PASSWORD_1"><%=MessageHelper.getCoreMessage("ST_DSI_NMDP")%></label>
              <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "PASSWORD_1", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 64, MessageHelper.getCoreMessage("ST_DSI_MDP"),"PASSWORD=YES,NO_TRIM=YES",false,"");%>
      </p>
  
      <p>
          <label for="PASSWORD_2"><%=MessageHelper.getCoreMessage("ST_DSI_CMDP")%></label>
              <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "PASSWORD_2", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 64, MessageHelper.getCoreMessage("ST_DSI_MDP"),"PASSWORD=YES,NO_TRIM=YES",false,"");%>
      </p>
  
      <p class="msg-aide"><%=MessageHelper.getCoreMessage("ST_DSI_MESSMDP")%></p>
  
  </fieldset>
  <% } %>
  
  
  <% if (infoBean.getString("DROIT_MODIFICATION_EMAIL").equals("1"))    { %>
  
  <fieldset>
      <legend><%=MessageHelper.getCoreMessage("ST_DSI_CONTACT")%></legend>
  
      <p>
          <label for="ADRESSE_MAIL"><%=MessageHelper.getCoreMessage("ST_DSI_EMAIL")%></label>
          <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "ADRESSE_MAIL", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_DSI_EMAIL"),"EMAIL=YES",false,"");%>
      </p>
  
      <p class="msg-aide">
      <% if (infoBean.getString("SYNCHRONISATION").equals("1"))    { %>
          <%=MessageHelper.getCoreMessage("ST_DSI_REPORTANN")%>
      <% } else  { %>
          <%=MessageHelper.getCoreMessage("ST_DSI_PASREPORTANN")%>
      <% } %>
      </p>

  </fieldset>
  <% } %>

  <fieldset>
    <legend><%=MessageHelper.getCoreMessage("ST_DSI_CENTRESINTERET")%></legend>
    
    <!-- CENTRES D'INTERET -->
    <div class="multi-col">
    <input type="hidden" name="THEMES_NB_ITEMS" value="<%= infoBean.getInt("THEMES_NB_ITEMS")%>"/>
        <ul class="sans_puce deux_colonne"><!--
               <%    int nbLignes = infoBean.getInt("THEMES_NB_ITEMS");
                   for (int i=0; i< nbLignes; i++) { %>
   
            --><li>
                   <input type="hidden" name="CODE_THEME#<%= i %>" value="<%= infoBean.getString("CODE_THEME#" + i)%>"/>
                   <%fmt.insererChampSaisie( out, infoBean, "VALEUR_THEME#" + i, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
                   <label for="VALEUR_THEME#<%= i %>"><%= infoBean.getString( "LIBELLE_THEME#" + i) %></label>
               </li><!--
   
               <% } %>
        --></ul>
    </div> <!-- .multi-col -->
  
  </fieldset>
  <!-- CENTRES D'INTERET -->
  
  <p id="valider-formulaire" class="validation">
      <input class="submit" type="submit" name="VALIDER" value="<%=MessageHelper.getCoreMessage("ST_DSI_BOUTON_VALIDER")%>" />
  </p>

</form>