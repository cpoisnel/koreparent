<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<% if (StringUtils.isNotEmpty(ContexteUtil.getContexteUniv().getKsession()) && request.getCookies() != null) {
     Cookie[] tCookies = request.getCookies();
     for (Cookie cookie : tCookies) {
         if ("sso".equals(cookie.getName()) && "true".equals(cookie.getValue())){
             %><jsp:include page="/adminsite/sso/propagation.jsp" /><%
         }
    }
}
%>
