<%@page import="org.apache.commons.collections.CollectionUtils"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.extension.module.composant.Menu"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<div id="content">
    <nav role="navigation"><%
        Menu menuEcranCourant = (Menu)infoBean.get("MENU_ECRAN_COURANT");
        if (menuEcranCourant != null && CollectionUtils.isNotEmpty(menuEcranCourant.getSousMenu())) {
            %><ul class="menu_courant"><%
            for (Menu sousMenu : menuEcranCourant.getSousMenu()) {
                if (StringUtils.isNotBlank(sousMenu.getUrl()) || CollectionUtils.isNotEmpty(sousMenu.getSousMenu())) {
                    %><li>
                        <a href="<%= sousMenu.getUrl()%>"><%= sousMenu.getLibelle()%></a>
                    </li><%
                }
            }
        %></ul><%
        }
    %></nav>
</div><!-- #content -->