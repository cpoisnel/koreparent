<%@ page import="com.jsbsoft.jtf.core.LangueUtil" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.IRequeteurConstantes" %>
<%@ page import="com.univ.utils.RequeteurFiches" %>
<%@ page import="com.univ.utils.URLResolver" %>
<%@ page import="com.univ.utils.UnivWebFmt" %>
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<%
ContexteUniv ctx = ContexteUtil.getContexteUniv();
%><form id="form_saisie_front" class="saisie_fiche" action="<%=URLResolver.getAbsoluteUrl("/servlet/com.jsbsoft.jtf.core.SG", ctx)%>" method="post">
<div class="formulaire_hidden">
    <input type="hidden" name="PROC" value="GESTION_FICHES" />
    <input type="hidden" name="ACTION" value="LISTE" />
    <input type="hidden" name="#ECRAN_LOGIQUE#" value="LISTE" />
    <input type="hidden" name="LANGUE" value="<%=LangueUtil.getIndiceLocale(ctx.getLocale())%>" />
    <input type="hidden" name="ESPACE" value="<%=ctx.getEspaceCourant()%>" />
    <input type="hidden" name="RH" value="<%=ctx.getCodeRubriquePageCourante()%>" />
    <input type="hidden" name="TRI" value="<%=infoBean.getString("TRI")%>" />
    <input type="hidden" name="ORDRE_TRI" value="<%=infoBean.getString("ORDRE_TRI")%>" />
    <input type="hidden" name="FROM" value="<%=infoBean.getInt("FROM")%>" />
    <input type="hidden" name="INCREMENT" value="<%=infoBean.getInt("INCREMENT")%>" />
    <input type="hidden" name="ID_FICHE" value="" />
    <input type="hidden" name="TYPE_FICHE" value="" />
    <input type="hidden" name="<%=IRequeteurConstantes.MODE%>" value="<%=infoBean.getString(IRequeteurConstantes.MODE)%>" />
    <input type="hidden" name="<%=IRequeteurConstantes.AVEC_PAGINATION%>" value="<%=infoBean.getString(IRequeteurConstantes.AVEC_PAGINATION)%>" />
    <input type="hidden" name="<%=IRequeteurConstantes.ACTIONS%>" value="<%=infoBean.getString(IRequeteurConstantes.ACTIONS)%>" />
    <input type="hidden" name="<%=IRequeteurConstantes.AVEC_AJOUT%>" value="<%=infoBean.getString(IRequeteurConstantes.AVEC_AJOUT)%>" />
    <input type="hidden" name="<%=IRequeteurConstantes.REQUETE%>" value="<%=infoBean.getString(IRequeteurConstantes.REQUETE)%>" />
</div> <!-- .formulaire_hidden --><%
    String codeObjet = infoBean.getString(IRequeteurConstantes.LISTE_OBJETS);
    if (infoBean.getString(IRequeteurConstantes.AVEC_AJOUT).equals("1")) {
        String lienAjout = UnivWebFmt.formaterEnHTML(ctx, "[traitement;lien_fiches;ACTION=AJOUTER#LIBELLE=objet#SEPARATEUR=li#OBJET="+codeObjet+"#CODE_RUBRIQUE=DYNAMIK]", false);
        if (lienAjout.length() > 0) {
            %><div class="fiches__ajout plier-deplier">
                <button type="button" class="plier-deplier__bouton" aria-expanded="false"><span  aria-hidden="true" class="icon icon-plus2"></span><%=MessageHelper.getCoreMessage("ST_TABLEAU_AJOUT")%></button>
                <div class="plier-deplier__contenu plier-deplier__contenu--clos">
                    <%=lienAjout%>
                </div>
            </div><!-- .fiches__ajout --><%
        }
    } %>
    <%@ include file="/adminsite/saisie_front/gestion_fiches_tableau.jsp" %>
    <%@ include file="/adminsite/saisie_front/gestion_fiches_requeteur.jsp" %>
</form>

<script type="text/javascript">
<!--
function envoyerFormulaire(action,tri,from){

    oForm = window.document.forms['form_saisie_front'];
    oForm.ACTION.value = action;

    if(oForm.INPUT_INCREMENT){
        oForm.INCREMENT.value = oForm.INPUT_INCREMENT.value;
    }

    if (tri.length>0 && tri!='false'){
        ordre = oForm.ORDRE_TRI.value;
        if (ordre=='desc')
            {oForm.ORDRE_TRI.value ='';}
        if (ordre=='')
            {oForm.ORDRE_TRI.value ='desc';}

        {oForm.TRI.value = tri;}
    }

    if (from.length>0)
    {oForm.FROM.value = from;}

    oForm.submit();
}

function soumettreActionFiche(action,id,type){
    oForm = window.document.forms['form_saisie_front'];
    oForm.ACTION.value = action;
    oForm.ID_FICHE.value = id;
    oForm.TYPE_FICHE.value = type;
    oForm.submit();
}
//-->
</script>
