<%@page import="com.univ.objetspartages.om.PageLibre"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@ page import="com.univ.utils.URLResolver" %>
<%@ page import="com.univ.utils.UnivWebFmt" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%
ContexteUniv ctx = ContexteUtil.getContexteUniv();
PageLibre fiche = (PageLibre) ctx.getData("fiche");
String urlFiche = URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(ctx, fiche), ctx);
if (StringUtils.isNotBlank(urlFiche)) {
    %><a href="<%= urlFiche %>"><%=fiche.getLibelleAffichable()%></a><%
} else {
    %><br/><%=fiche.getLibelleAffichable()%><%
}
%>