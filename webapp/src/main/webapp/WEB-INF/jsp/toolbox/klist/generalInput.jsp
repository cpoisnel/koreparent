<%@ taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page import="org.apache.commons.collections.CollectionUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.kosmos.toolbox.model.TagViewDescriptor" %>
<%@ page import="com.kosmos.toolbox.service.impl.ServiceKList" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<%
    final Locale locale = ContexteUtil.getContexteUniv().getLocale();
    final List<TagViewDescriptor> viewDescriptors = infoBean.getList(ServiceKList.LIST_TYPES);
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="<%= locale.getLanguage() %>"><!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <title>Insertion de lien</title>
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" type="image/x-icon" href="/adminsite/images/favicon.ico" />
    <link rel="icon" type="image/png" href="/adminsite/images/favicon.png" />
    <!--[if lte IE 7]>
    <link rel="stylesheet" href="/adminsite/styles/fonts/icones/ie7/ie7.css">
    <script src="/adminsite/styles/fonts/icones/ie7/ie7.js"></script>
    <script src="/adminsite/scripts/libs/ckeditor/ckeditor.js"></script>
    <![endif]-->
    <resources:link group="stylesBo"/>
    <link rel="stylesheet" href="/adminsite/styles/dialog.css">
    <script type="text/javascript">
        var html = document.getElementsByTagName('html')[0];
        html.className = html.className.replace('no-js', 'js');
    </script>
</head>
<body>
<div id="content">
    <div class="js-tabs"><%
        if(CollectionUtils.isNotEmpty(viewDescriptors)) {
        %><ul class="tabs tabs--vertical"><%
            for (TagViewDescriptor currentModel : viewDescriptors) {
            %><li class="tabs__item<%= currentModel.isSelected() ? " tabs__item--active" : StringUtils.EMPTY %> js-tabs__item">
                <a class="tabs__link js-tabs__link" href="#panel-<%= currentModel.getId() %>" data-panel="<%= currentModel.getId() %>">
                    <div class="tabs__icon"><%
                        if(currentModel.getIcons() != null) {
                    %><img class="tabs__icon--normal" src="<%= currentModel.getIcons().getLeft() %>" alt="icon-<%= currentModel.getLabel() %>-normal">
                        <img class="tabs__icon--highlight" src="<%= currentModel.getIcons().getRight() %>" alt="icon-<%= currentModel.getLabel() %>-highlight"><%
                        } else {
                        %><img class="tabs__icon--normal" src="/adminsite/styles/img/24x24/ckeditor/ktag/tag.png" alt="icon-<%= currentModel.getLabel() %>-normal">
                        <img class="tabs__icon--highlight" src="/adminsite/styles/img/24x24/ckeditor/ktag/highlight/tag.png" alt="icon-<%= currentModel.getLabel() %>-highlight"><%
                            }
                        %>
                    </div>
                    <%= currentModel.getLabel() %>
                </a>
            </li><%
            }
        %></ul><!--<%
        }
    %>--><div class="list__edition-panel"><%
            for (TagViewDescriptor currentModel : viewDescriptors) {
                if(StringUtils.isNotBlank(currentModel.getInputFragment())) {
                    %><div class="tabs__panel<%= currentModel.isSelected() ? " tabs__panel--active" : StringUtils.EMPTY %> js-tabs__panel js-contentformatter__element"
                           data-panel="<%= currentModel.getId() %>"
                           data-contentformatter-pattern="<%= currentModel.getFormatterPattern() %>"
                            <%= StringUtils.isNotBlank(currentModel.getValidationPattern()) ? "data-contentformatter-validator=\"" + currentModel.getValidationPattern() + "\"" : StringUtils.EMPTY %>>
                        <form><%
                            request.setAttribute("viewDescriptor", currentModel);
                            %><jsp:include page="<%= currentModel.getInputFragment() %>"/>
                        </form>
                    </div><%
                }
            }%>
        </div><!-- .list__edition-panel -->
    </div><!-- .js-tabs -->
</div><!-- #content -->
<input id="klist" type="hidden" class="js-contentformatter" value="<%= StringUtils.defaultString(infoBean.get(ServiceKList.TAG_VALUE,String.class)) %>" />
<resources:script group="scriptsBo" locale="<%= locale.toString() %>"/>
<script type="text/javascript" src="/adminsite/scripts/contents/messages.js"></script>
<script type="text/javascript" src="/adminsite/scripts/contents/klist/common.js"></script><%
    for (TagViewDescriptor currentModel : viewDescriptors) {
        if (StringUtils.isNotBlank(currentModel.getJsScript())) {
            %><script type="text/javascript" src="<%= currentModel.getJsScript() %>"></script><%
        }
    }
%>
</body>
</html>