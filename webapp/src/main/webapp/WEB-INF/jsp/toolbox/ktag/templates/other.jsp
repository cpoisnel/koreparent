<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kosmos.toolbox.service.impl.ServiceKTag" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<jsp:useBean id="viewDescriptor" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
    String currentTagValue = StringUtils.defaultString(infoBean.get(ServiceKTag.TAG_VALUE, String.class));
    String pagePathValue = StringUtils.defaultString(viewDescriptor.getData().get("page"));
    String urlValue = StringUtils.defaultString(viewDescriptor.getData().get("url"));
    String nbNiveau = StringUtils.defaultString(viewDescriptor.getData().get("NB_NIVEAUX"));
%>
<p>
    <input type="radio" value="[page;]" id="page" name="otherTag" <%=currentTagValue.startsWith("[page;") ? "checked=\"checked\"" : "" %> />
    <label for="page"><%= MessageHelper.getCoreMessage("KTAG.KOTHER.PAGE") %></label>
</p>
<div class="<%= currentTagValue.startsWith("[page;") ? "" : "masquer" %> js-pagePath">
    <p><%= MessageHelper.getCoreMessage("KTAG.KOTHER.PAGE_HELP") %></p>
    <p>
        <label for="js-pagePath" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KOTHER.PAGE.FILE_PATH")%></label>
        <input type="text" name="pagePath" id="js-pagePath" value="<%= pagePathValue %>" required/>
    </p>
</div>
<p>
    <input type="radio" value="[url;]" id="url" name="otherTag" <%=currentTagValue.startsWith("[url;") ? "checked=\"checked\"" : "" %>/>
    <label for="url"><%= MessageHelper.getCoreMessage("KTAG.KOTHER.URL") %></label>
</p>
<p class="<%= currentTagValue.startsWith("[url;") ? "" : "masquer" %> js-url">
    <label for="js-url" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KOTHER.URL.LABEL_URL")%></label>
    <input class="type_url" type="text" name="url" id="js-url" value="<%= urlValue %>" required/>
</p>
<p>
    <input type="radio" value="[date]" id="date" name="otherTag" <%= "[date]".equals(currentTagValue) ? "checked=\"checked\"" : "" %> />
    <label for="date"><%= MessageHelper.getCoreMessage("KTAG.KOTHER.DATE") %></label>
</p>
<p>
    <input type="radio" value="[plan_site;]" id="planSite" name="otherTag" <%= currentTagValue.startsWith("[plan_site;") ? "checked=\"checked\"" : "" %> />
    <label for="planSite"><%= MessageHelper.getCoreMessage("KTAG.KOTHER.MAP") %></label>
</p>
<div class="<%= currentTagValue.startsWith("[plan_site;") ? "" : "masquer" %> js-planSite"><%
        if (viewDescriptor.getData().get("LIBELLE_CODE_RUBRIQUE") != null) {
            infoBean.set("planSite_codeRubrique", viewDescriptor.getData().get("CODE_RUBRIQUE"));
            infoBean.set("LIBELLE_planSite_codeRubrique", viewDescriptor.getData().get("LIBELLE_CODE_RUBRIQUE"));
        }
        univFmt.insererkMonoSelect(fmt, out, infoBean, "planSite_codeRubrique", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("KTAG.KOTHER.MAP.SECTION"), "rubrique", UnivFmt.CONTEXT_ZONE); %>
    <p>
        <label for="planSite_nbNiveaux" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KOTHER.MAP.LEVEL")%></label>
        <input type="text" name="plan_site_nb_niveaux" id="planSite_nbNiveaux" value="<%= nbNiveau %>" size="5"/> <%= MessageHelper.getCoreMessage("KTAG.KOTHER.MAP.LEVEL_HELP")%>
    </p>
</div>