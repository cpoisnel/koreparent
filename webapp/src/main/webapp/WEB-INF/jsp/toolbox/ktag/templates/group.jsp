<%@ page import="java.util.Map" %>
<%@ page import="com.jsbsoft.jtf.core.LangueUtil" %>
<%@ page import="com.kosmos.toolbox.service.impl.ServiceKTag" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.util.LabelUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />

<%
    final String currentTagValue = StringUtils.defaultString(infoBean.get(ServiceKTag.TAG_VALUE, String.class));
    final String typeLibelleTypeGroupe="11";
    final Map<String,String> codeLibelleTypesGroupes = LabelUtils.getLabelCombo(typeLibelleTypeGroupe, LangueUtil.getDefaultLocale());
%>
<p>
    <input type="radio" value="[traitement;debutgroupe]" id="debutgroupe" name="groupTag" <%= "[traitement;debutgroupe]".equals(currentTagValue) ? "checked=\"checked\"" : "" %> />
    <label for="debutgroupe"><%= MessageHelper.getCoreMessage("KTAG.KGROUP.DEBUT_GROUPE") %></label>
</p><%
    if(codeLibelleTypesGroupes.keySet().size() > 0) {
%><p>
    <input type="radio" value="[traitement;debutgroupe;<%= codeLibelleTypesGroupes.keySet().iterator().next() %>]" id="debutgroupeType" name="groupTag" <%= currentTagValue.startsWith("[traitement;debutgroupe;") ? "checked=\"checked\"" : "" %> />
    <label for="debutgroupeType"><%= MessageHelper.getCoreMessage("KTAG.KGROUP.DEBUT_GROUPE_TYPE") %></label>
</p><%
    }
%><p class="<%= currentTagValue.startsWith("[traitement;debutgroupe;") ? "" : "masquer" %> js-typeGroupe">
    <label for="js-debutgroupeType" class="colonne"><%= MessageHelper.getCoreMessage("BO_GROUPE_TYPE_GROUPE")%></label>
    <select name="comboTypeGroupe" id="js-debutgroupeType"><%
        for (Map.Entry<String,String> codeLibelleTypeGroupe : codeLibelleTypesGroupes.entrySet()) {
            %><option value="<%=codeLibelleTypeGroupe.getKey()%>" <%= currentTagValue.endsWith(codeLibelleTypeGroupe.getKey() + "]") ? "selected=\"selected\"" : "" %>><%=codeLibelleTypeGroupe.getValue()%></option><%
        } %>
    </select>
</p>
<p>
    <input type="radio" value="[traitement;fingroupe]" id="fingroupe" name="groupTag" <%= "[traitement;fingroupe]".equals(currentTagValue) ? "checked=\"checked\"" : "" %> />
    <label for="fingroupe"><%= MessageHelper.getCoreMessage("KTAG.KGROUP.FIN_GROUPE") %></label>
</p>
<p>
    <input type="radio" value="[groupe]" id="groupe" name="groupTag" <%= "[groupe]".equals(currentTagValue) ? "checked=\"checked\"" : "" %> />
    <label for="groupe"><%= MessageHelper.getCoreMessage("KTAG.KGROUP.GROUPE") %></label>
</p>
<p>
    <input type="radio" value="[intitulegroupe]" id="intitulegroupe" name="groupTag" <%= "[intitulegroupe]".equals(currentTagValue) ? "checked=\"checked\"" : "" %> />
    <label for="intitulegroupe"><%= MessageHelper.getCoreMessage("KTAG.KGROUP.INTITULE_GROUPE") %></label>
</p>
<p>
    <input type="radio" value="[pagegroupe]" id="pagegroupe" name="groupTag" <%= "[pagegroupe]".equals(currentTagValue) ? "checked=\"checked\"" : "" %> />
    <label for="pagegroupe"><%= MessageHelper.getCoreMessage("KTAG.KGROUP.PAGE_GROUPE") %></label>
</p>