<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.om.ServiceBean" %>
<%@ page import="com.univ.utils.ServicesUtil" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="viewDescriptor" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" />
<div class="js-tag_service"><%
    Map<String, String> listeServices = new HashMap<String, String>();
    for (ServiceBean service : ServicesUtil.getServices().values()){
        listeServices.put(service.getCode(), service.getIntitule());
    }
    infoBean.set("LISTE_SERVICES", listeServices);
    infoBean.set("code_service", viewDescriptor.getData().get("code_service"));
    %>
    <p>
        <label for="choix_tag_service" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KSERVICE.TAG")%> (*)</label>
        <select id="choix_tag_service" name="choix_tag_service">
            <option value="service_contenu" <%= "contenu".equals(viewDescriptor.getData().get("choix_tag_service")) ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KTAG.KSERVICE.SHRINKED_VIEW")%></option>
            <option value="service_link" <%= "link".equals(viewDescriptor.getData().get("choix_tag_service")) ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KTAG.KSERVICE.LINK")%></option>
        </select>
    </p>
    <p>
        <label for="code_service" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KSERVICE.CHOOSE")%> (*)</label><%
        fmt.insererComboHashtable( out, infoBean, "code_service", FormateurJSP.SAISIE_FACULTATIF, "LISTE_SERVICES" ); %>
    </p>
    <p id="js-service_link" class="<%= "link".equals(viewDescriptor.getData().get("choix_tag_service")) ? "" : "masquer" %>" >
        <label for="nom_link" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KSERVICE.LABEL_LINK")%></label>
        <input type="text" name="nom_link" id="nom_link" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("nom")) %>" /> <%= MessageHelper.getCoreMessage("KTAG.KSERVICE.LABEL_HINT")%>
    </p>
</div><!-- #tag_service -->