<%@ page import="com.kosmos.toolbox.service.impl.ServiceKTag" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<%
    String currentTagValue = infoBean.get(ServiceKTag.TAG_VALUE,String.class);
%>
<p>
    <input type="radio" value="[nom]" id="nom" name="userTag" <%= "[nom]".equals(currentTagValue) ? "checked=\"checked\"" : "" %> /><label for="nom"><%= MessageHelper.getCoreMessage("KTAG.KUSER.NOM") %></label>
</p>
<p>
    <input type="radio" value="[prenom]" id="prenom" name="userTag" <%= "[prenom]".equals(currentTagValue) ? "checked=\"checked\"" : "" %> /><label for="prenom"><%= MessageHelper.getCoreMessage("KTAG.KUSER.PRENOM") %></label>
</p>
