<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.apache.commons.collections.MapUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.LangueUtil" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.collaboratif.om.Espacecollaboratif" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.RequeteurFiches" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<jsp:useBean id="viewDescriptor" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
%><div id="js-liste_fiches">
    <div id="js-main-tagValues">
    <p class="message information"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.HINT")%></p>
    <p>
        <label for="liste_fiches_mode" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.CONTRIB_MODE") %> (*)</label>
        <select name="MODE" id="liste_fiches_mode" >
            <option value="<%= RequeteurFiches.MODE_CONSULTATION %>" <%= "CONSUL".equals(viewDescriptor.getData().get("MODE")) ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("BO_CONSULTER") %></option>
            <option value="<%= RequeteurFiches.MODE_MODIFICATION %>" <%= "MODIF".equals(viewDescriptor.getData().get("MODE")) ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("BO_MODIFICATION") %></option>
            <option value="<%= RequeteurFiches.MODE_VALIDATION %>" <%= "VALID".equals(viewDescriptor.getData().get("MODE")) ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("BO_VALIDATION") %></option>
        </select>
    </p>

    <h3><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.SELECTION")%></h3><%
    String objectCode = StringUtils.substringBefore(viewDescriptor.getData().get("OBJET"), "|");
    infoBean.set("OBJET", objectCode);
    if (StringUtils.isNotBlank(objectCode)) {
        infoBean.set("LIBELLE_OBJET", ReferentielObjets.getLibelleObjet(objectCode));
    }
    univFmt.insererKmultiSelectLtl(fmt, out, infoBean, "OBJET", FormateurJSP.SAISIE_FACULTATIF, "Objet", "LISTE_CODE_OBJET", "", UnivFmt.CONTEXT_DEFAULT);%>
    <%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.ALL")%>
    <%
        if (viewDescriptor.getData().get("LIBELLE_CODE_RUBRIQUE") != null) {
            infoBean.set("CODE_RUBRIQUE", viewDescriptor.getData().get("CODE_RUBRIQUE"));
            infoBean.set("LIBELLE_CODE_RUBRIQUE", viewDescriptor.getData().get("LIBELLE_CODE_RUBRIQUE"));
        }
        univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE);
    %>
  <p class="retrait">
    <input type="checkbox" name="CODE_RUBRIQUE_DYNAMIK" id="CODE_RUBRIQUE_DYNAMIK_DASHBOARD" value="DYNAMIK" <%="DYNAMIK".equals(
            viewDescriptor.getData().get("CODE_RUBRIQUE")) ? "checked" : StringUtils.EMPTY %>/>
    <label for="CODE_RUBRIQUE_DYNAMIK_DASHBOARD"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.SECTION_HELP")%></label>
  </p><%
    if (viewDescriptor.getData().get("LIBELLE_CODE_RATTACHEMENT") != null) {
        infoBean.set("CODE_RATTACHEMENT", viewDescriptor.getData().get("CODE_RATTACHEMENT"));
        infoBean.set("LIBELLE_CODE_RATTACHEMENT", viewDescriptor.getData().get("LIBELLE_CODE_RATTACHEMENT"));
    }
    univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT"), "",
            UnivFmt.CONTEXT_STRUCTURE);

    if (viewDescriptor.getData().get("LIBELLE_DIFFUSION_PUBLIC_VISE") != null) {
        infoBean.set("DIFFUSION_PUBLIC_VISE", viewDescriptor.getData().get("DIFFUSION_PUBLIC_VISE"));
        infoBean.set("LIBELLE_DIFFUSION_PUBLIC_VISE", viewDescriptor.getData().get("LIBELLE_DIFFUSION_PUBLIC_VISE"));
    }
    univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "DIFFUSION_PUBLIC_VISE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_TABLEAU_PUBLIC_VISE"), "TMP_DIFFUSION_PUBLIC_VISE", "Public visé", UnivFmt.CONTEXT_GROUPEDSI_PUBLIC_VISE, "");%>

  <p class="retrait">
    <input type="checkbox" name="DIFFUSION_PUBLIC_VISE_DYNAMIK" id="DIFFUSION_PUBLIC_VISE_DYNAMIK_DASHBOARD" value="DYNAMIK" <%="DYNAMIK".equals(viewDescriptor.getData().get("DIFFUSION_PUBLIC_VISE")) ? "checked" : StringUtils.EMPTY %>/>
    <label for="DIFFUSION_PUBLIC_VISE_DYNAMIK_DASHBOARD"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.GROUP_HELP")%></label>
  </p>
  <% if (Espacecollaboratif.isExtensionActivated()) { %>
  <p>
    <span class="label colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.COLLAB")%></span>
    <input type="radio" name="DIFFUSION_MODE_RESTRICTION" id="liste_fiches_espace1" value="1" <%= "1".equals(viewDescriptor.getData().get("DIFFUSION_MODE_RESTRICTION")) || StringUtils.isEmpty(viewDescriptor.getData().get("DIFFUSION_MODE_RESTRICTION")) ? "checked" : "" %> /><label for="liste_fiches_espace1"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.COLLAB.EXCLUDE")%></label>
    <input type="radio" name="DIFFUSION_MODE_RESTRICTION" id="liste_fiches_espace2" value="+4" <%= "+4".equals(viewDescriptor.getData().get("DIFFUSION_MODE_RESTRICTION")) ? "checked" : "" %>/><label for="liste_fiches_espace2"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.COLLAB.INCLUDE_ADD")%></label>
    <input type="radio" name="DIFFUSION_MODE_RESTRICTION" id="liste_fiches_espace3" value="4" <%= "4".equals(viewDescriptor.getData().get("DIFFUSION_MODE_RESTRICTION")) ? "checked" : "" %>/><label for="liste_fiches_espace3"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.COLLAB.INCLUDE")%></label>
  </p>
  <% } %>
  <p>
    <label for="SELECTION" class="colonne"><%= MessageHelper.getCoreMessage("BO_MODULE_DATE_MODIFICATION")%></label>
    <%
      Map<String, String> hash = new HashMap<String, String>();
      hash.put("0001",MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DATE.DAY"));
      hash.put("0002",MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DATE.WEEK"));
      hash.put("0003",MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DATE.MONTH"));
      infoBean.set("LISTE_SELECTION_DATE",hash);
      infoBean.set("SELECTION", viewDescriptor.getData().get("SELECTION"));
      fmt.insererComboHashtable(out, infoBean, "SELECTION", FormateurJSP.SAISIE_FACULTATIF, "LISTE_SELECTION_DATE", MessageHelper.getCoreMessage("BO_MODULE_DATE_MODIFICATION"));
    %>
  </p>
  <p>
    <label for="ETAT" class="colonne"><%= MessageHelper.getCoreMessage("BO_ETAT")%></label><%
    infoBean.set("ETAT", viewDescriptor.getData().get("ETAT"));
    univFmt.insererContenuComboHashtable(fmt, out, infoBean, "ETAT", FormateurJSP.SAISIE_FACULTATIF, ReferentielObjets.getEtatsObjetFront(), MessageHelper.getCoreMessage("BO_ETAT"));%>
  </p>
  <%	hash = LangueUtil.getListeLangues(ContexteUtil.getContexteUniv().getLocale());
    //on n'affiche pas l'option des langues si une seule langue est configurée pour le site
    if (hash.size() > 1) {
      infoBean.set("LISTE_LANGUES", hash); %>
  <p>
    <label for="LANGUE" class="colonne"><%= MessageHelper.getCoreMessage("BO_LANGUE")%></label><%
    infoBean.set("LANGUE", viewDescriptor.getData().get("LANGUE"));
    fmt.insererComboHashtable(out, infoBean, "LANGUE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_LANGUES", "Langue");%>
  </p>
  <% } %>
  <p>
      <input type="checkbox" name="CODE_REDACTEUR_DYNAMIK" id="CODE_REDACTEUR_DYNAMIK_DASHBOARD" value="DYNAMIK" <%="DYNAMIK".equals(viewDescriptor.getData().get("CODE_REDACTEUR")) ? "checked" : StringUtils.EMPTY %>/>
      <label for="CODE_REDACTEUR_DYNAMIK_DASHBOARD"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.CURRENT_USER")%></label>
  </p>

  <p><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DISPLAY.NOTE")%></p>
  <p>
    <label for="liste_fiches_affichage" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DISPLAY.FULL")%></label>
    <select name="liste_fiches_affichage" id="liste_fiches_affichage">
      <option value=""><%= MessageHelper.getCoreMessage("JTF_SELECTIONNER_LISTE")%></option>
      <option value="liste_fiches_liste" <%= "liste_fiches_liste".equals(viewDescriptor.getData().get("liste_fiches_affichage")) ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DISPLAY.CONTENT_LIST")%></option>
      <option value="liste_fiches_tableau" <%= "liste_fiches_tableau".equals(viewDescriptor.getData().get("liste_fiches_affichage")) ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DISPLAY.TABLE_LINK_ONLY")%></option>
      <option value="liste_fiches_listetableau" <%= "liste_fiches_listetableau".equals(viewDescriptor.getData().get("liste_fiches_affichage")) ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DISPLAY.TABLE_AND_LINK")%></option>
    </select>
  </p>
    </div><%
    final boolean displayListeFicheListe = "liste_fiches_liste".equals(viewDescriptor.getData().get("liste_fiches_affichage")) || "liste_fiches_listetableau".equals(viewDescriptor.getData().get("liste_fiches_affichage"));
%><div id="liste_fiches_liste" <%= !displayListeFicheListe ? "class=\"masquer\"" : StringUtils.EMPTY %>>
    <h3><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.LIST.PARAMETERS")%></h3>
    <p>
      <label for="TRI" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.LIST.ORDER")%></label>
      <%
        hash = new HashMap<String, String>();
        hash.put(RequeteurFiches.TRI_DATE_ASC,MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.DATE_ASC"));
        hash.put(RequeteurFiches.TRI_DATE_DESC,MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.DATE_DESC"));
        hash.put(RequeteurFiches.TRI_LIBELLE, MessageHelper.getCoreMessage("BO_EXTENSION_LIBELLE"));
        hash.put(RequeteurFiches.TRI_AUTEUR, MessageHelper.getCoreMessage("BO_EXTENSION_AUTEUR"));
        hash.put(RequeteurFiches.TRI_ETAT,MessageHelper.getCoreMessage("BO_ETAT"));
        hash.put(RequeteurFiches.TRI_OBJET,MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_CODE_OBJET"));
        hash.put(RequeteurFiches.TRI_RUBRIQUE, MessageHelper.getCoreMessage("BO_RUBRIQUE"));
        hash.put(RequeteurFiches.TRI_STRUCTURE, MessageHelper.getCoreMessage("CODE_RATTACHEMENT"));
        hash.put(RequeteurFiches.TRI_DSI, MessageHelper.getCoreMessage("BO_GROUPES"));
        infoBean.set("LISTE_TRI", hash);
        String order = viewDescriptor.getData().get("TRI");
        infoBean.set("TRI", order != null ? order : RequeteurFiches.TRI_DATE_ASC);
        fmt.insererComboHashtable(out, infoBean, "TRI", FormateurJSP.SAISIE_FACULTATIF, "LISTE_TRI","Langue");
      %>
    </p>
    <p>
      <label for="INCREMENT" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.LIST.RESULT_NUMBER")%></label>
      <input type="text" name="INCREMENT" id="INCREMENT" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("INCREMENT"), "5") %>" size="5" />
    </p>
    <p>
      <label for="liste_fiches_separateur" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.LIST.SEPARATOR")%></label>
      <%
        hash = new HashMap<String, String>();
        hash.put("li", MessageHelper.getCoreMessage("KTAG.KDASHBOARD.LIST.UL"));
        hash.put("br", MessageHelper.getCoreMessage("KTAG.KDASHBOARD.LIST.BR"));
        infoBean.set("LISTE_SEPARATEURS", hash);
        infoBean.set("SEPARATEUR", viewDescriptor.getData().get("SEPARATEUR"));
        fmt.insererComboHashtable( out, infoBean, "SEPARATEUR", FormateurJSP.SAISIE_FACULTATIF , "LISTE_SEPARATEURS" );
      %>
    </p>
    <p>
      <label for="CLASSE_CSS" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.LINK.CLASS")%></label>
      <input type="text" name="CLASSE_CSS" id="CLASSE_CSS" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("CLASSE_CSS")) %>" size="10" />
    </p>
  </div><%
    final boolean displayListeFicheTableau = "liste_fiches_tableau".equals(viewDescriptor.getData().get("liste_fiches_affichage")) || "liste_fiches_listetableau".equals(viewDescriptor.getData().get("liste_fiches_affichage"));
%><div id="liste_fiches_tableau" <%= !displayListeFicheTableau ? "class=\"masquer\"" : StringUtils.EMPTY %>>
    <h3><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.PARAMETERS")%></h3>
    <p>
      <label for="liste_fiches_intitule_lien" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.LINK_LABEL")%></label>
      <input type="text" name="LIEN" id="liste_fiches_intitule_lien" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("LIEN")) %>" size="20" />
    </p>
    <p>
      <label for="liste_fiches_titre" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.TITLE")%></label>
      <input type="text" name="TITRE" id="liste_fiches_titre" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("TITRE")) %>" size="20" />
    </p>
    <p>
        <input type="checkbox" name="AJOUT" id="liste_fiches_ajout" value="0" <%= StringUtils.isNotBlank(viewDescriptor.getData().get("AJOUT")) ? "" : "checked" %> />
      <label for="liste_fiches_ajout" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.WITH_ADD_LINK")%></label>
    </p>
    <p>
        <input type="checkbox" name="PAGE" id="PAGE" value="0" <%= StringUtils.isNotBlank(viewDescriptor.getData().get("PAGE")) ? "" : "checked" %> />
      <label for="PAGE"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.PAGINATE")%></label>
    </p>
    <span class="label colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.ACTIONS")%></span>
    <div class="retrait">
      <input type="checkbox" name="ACTION" id="liste_fiches_action_voir" value="V" <%= StringUtils.contains(viewDescriptor.getData().get("ACTION"), "V") ? "checked" : StringUtils.EMPTY %>/>
      <label for="liste_fiches_action_voir"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.SEE_CONTENT")%></label>
      <br />
      <input type="checkbox" name="ACTION" id="liste_fiches_action_modifier" value="M" <%= StringUtils.contains(viewDescriptor.getData().get("ACTION"), "M") ? "checked" : StringUtils.EMPTY %>/>
      <label for="liste_fiches_action_modifier"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.UPDATE_CONTENT")%></label>
      <br />
      <input type="checkbox" name="ACTION" id="liste_fiches_action_supprimer" value="S" <%= StringUtils.contains(viewDescriptor.getData().get("ACTION"), "S") ? "checked" : StringUtils.EMPTY %>/>
      <label for="liste_fiches_action_supprimer"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.DELETE_CONTENT")%></label>
    </div><!-- .retrait -->

    <span class="label colonne"><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.ORDERING")%></span>
    <table>
      <tr>
        <th><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.CRITERION")%></th>
        <th><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.SEARCH")%></th>
        <th><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.COLUMN_WITH_ORDER")%></th>
      </tr>
      <tr>
        <td><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.START_DATE")%></td>
        <td><input type="checkbox" data-associated-field="DATE_DEBUT" name="liste_fiches_req_DATE_DEBUT" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_req_DATE_DEBUT")) ? "" : "checked" %> /></td>
        <td><input type="checkbox" data-associated-field="DATE_DEBUT" name="liste_fiches_tri_DATE_DEBUT" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_tri_DATE_DEBUT")) ? "" : "checked" %> /></td>
      </tr>
      <tr>
          <td><%= MessageHelper.getCoreMessage("KTAG.KDASHBOARD.DASHBOARD.END_DATE")%></td>
          <td><input type="checkbox" data-associated-field="DATE_FIN" name="liste_fiches_req_DATE_FIN" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_req_DATE_FIN")) ? "" : "checked" %> /></td>
          <td><span style="display: none;"><input type="checkbox" data-associated-field="DATE_FIN" name="liste_fiches_tri_DATE_FIN" value="1" disabled="disabled"/></span></td>
      </tr>
      <tr>
        <td><%= MessageHelper.getCoreMessage("BO_RUBRIQUE")%></td>
        <td><input type="checkbox" data-associated-field="CODE_RUBRIQUE" name="liste_fiches_req_CODE_RUBRIQUE" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_req_CODE_RUBRIQUE")) ? "" : "checked" %> /></td>
        <td><input type="checkbox" data-associated-field="CODE_RUBRIQUE" name="liste_fiches_tri_CODE_RUBRIQUE" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_tri_CODE_RUBRIQUE")) ? "" : "checked" %> /></td>
      </tr>
      <tr>
        <td><%= MessageHelper.getCoreMessage("CODE_RATTACHEMENT")%></td>
        <td><input type="checkbox" data-associated-field="CODE_RATTACHEMENT" name="liste_fiches_req_CODE_RATTACHEMENT" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_req_CODE_RATTACHEMENT")) ? "" : "checked" %> /></td>
        <td><input type="checkbox" data-associated-field="CODE_RATTACHEMENT" name="liste_fiches_tri_CODE_RATTACHEMENT" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_tri_CODE_RATTACHEMENT")) ? "" : "checked" %> /></td>
      </tr>
      <tr>
        <td><%= MessageHelper.getCoreMessage("BO_GROUPE")%></td>
        <td><input type="checkbox" data-associated-field="DIFFUSION_PUBLIC_VISE" name="liste_fiches_req_DIFFUSION_PUBLIC_VISE" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_req_DIFFUSION_PUBLIC_VISE")) ? "" : "checked" %> /></td>
        <td><input type="checkbox" data-associated-field="DIFFUSION_PUBLIC_VISE" name="liste_fiches_tri_DIFFUSION_PUBLIC_VISE" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_tri_DIFFUSION_PUBLIC_VISE")) ? "" : "checked" %> /></td>
      </tr>
      <tr>
        <td><%= MessageHelper.getCoreMessage("CODE_REDACTEUR")%></td>
        <td><input type="checkbox" data-associated-field="CODE_REDACTEUR" name="liste_fiches_req_CODE_REDACTEUR" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_req_CODE_REDACTEUR")) ? "" : "checked" %> /></td>
        <td><input type="checkbox" data-associated-field="CODE_REDACTEUR" name="liste_fiches_tri_CODE_REDACTEUR" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_tri_CODE_REDACTEUR")) ? "" : "checked" %> /></td>
      </tr>
      <tr>
        <td><%= MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_CODE_OBJET")%></td>
        <td><input type="checkbox" data-associated-field="OBJET" name="liste_fiches_req_OBJEt" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_req_OBJET")) ? "" : "checked" %> /></td>
        <td><input type="checkbox" data-associated-field="OBJET" name="liste_fiches_tri_OBJET" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_tri_OBJET")) ? "" : "checked" %> /></td>
      </tr>
      <tr>
        <td><%= MessageHelper.getCoreMessage("BO_ETAT")%></td>
        <td><input type="checkbox" data-associated-field="ETAT" name="liste_fiches_req_ETAT" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_req_ETAT")) ? "" : "checked" %> /></td>
        <td><input type="checkbox" data-associated-field="ETAT" name="liste_fiches_tri_ETAT" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_tri_ETAT")) ? "" : "checked" %> /></td>
      </tr>
      <%  // on n'affiche pas l'option des langues si une seule langue est configurée pour le site
        if (infoBean.getMap("LISTE_LANGUES") != null && infoBean.getMap("LISTE_LANGUES").size() > 1) { %>
      <tr>
        <td><%= MessageHelper.getCoreMessage("BO_LANGUE") %></td>
        <td><input type="checkbox" data-associated-field="LANGUE" name="liste_fiches_req_LANGUE" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_req_LANGUE")) ? "" : "checked" %> /></td>
        <td><input type="checkbox" data-associated-field="LANGUE" name="liste_fiches_tri_LANGUE" value="1" <%= "0".equals(viewDescriptor.getData().get("liste_fiches_tri_LANGUE")) ? "" : "checked" %> /></td>
      </tr>
      <% } %>
    </table>
  </div><!-- #liste_fiches_tableau -->
</div><!-- #js-liste_fiches -->
