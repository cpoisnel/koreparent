<%@ page import="java.util.List" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.springframework.cglib.core.CollectionUtils" %>
<%@ page import="org.springframework.cglib.core.Predicate" %>
<%@ page import="com.kportal.cms.objetspartages.Objetpartage" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.config.PropertyHelper" %>
<%@ page import="com.kportal.extension.IExtension" %>
<%@ page import="com.kportal.extension.module.ModuleHelper" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<jsp:useBean id="viewModel" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" />
<%
    final String currentCode = viewModel.getData().get("CODE");
    final List<Objetpartage> objects = ReferentielObjets.getObjetsPartagesTries();
    CollectionUtils.filter(objects, new Predicate() {
        @Override
        public boolean evaluate(final Object o) {
            final Objetpartage objet = (Objetpartage) o;
            final IExtension extension = ModuleHelper.getExtensionModule(objet);
            final boolean hasAdvancedSearch = "1".equals(PropertyHelper.getExtensionProperty(extension.getId(), String.format("fiche.%s.recherche_avancee", objet.getNomObjet().toUpperCase())));
            return hasAdvancedSearch && !objet.isStrictlyCollaboratif();
        }
    });
    int split = (int) Math.ceil((double) objects.size() / 2);
    %>
    <p id="champs_obligatoires" class="message alert alert-info"><%= MessageHelper.getCoreMessage("LINK_TYPE.SEARCH_FORM.TIP") %></p>
    <p>
        <span><%= MessageHelper.getCoreMessage("LINK_TYPE.SEARCH_FORM.TYPE") %></span>
    </p>
    <ul class="boite"><%
    for (int i = 0 ; i < objects.size() ; i++) {
        Objetpartage objet = objects.get(i);
        if(i == split) {
            %></ul><ul class="boite"><%
        }
        final boolean isChecked = objet.getNomObjet().toUpperCase().equals(currentCode);
        %><li>
            <input type="radio" id="radio-<%= objet.getNomObjet() %>" name="CODE" value="<%= objet.getNomObjet().toUpperCase() %>" <%= isChecked ? "checked" : StringUtils.EMPTY %>/>
            <label for="radio-<%= objet.getNomObjet() %>"><%=objet.getLibelleAffichable()%></label>
        </li><%
    }
    %>
    </ul>