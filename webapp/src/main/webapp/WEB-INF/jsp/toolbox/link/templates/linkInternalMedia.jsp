<%@ page import="org.apache.commons.lang3.StringUtils" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="viewModel" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
    final String idMedia = viewModel.getData().get("ID_RESSOURCE");
    if(StringUtils.isNotBlank(idMedia)) {
    %><iframe class="media-iframe js-media-iframe" src="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER_RESSOURCE&FCK_PLUGIN=TRUE&MODE_FICHIER=LIEN&ID_MEDIA=<%= idMedia %>"></iframe><%
    } else {
    %><iframe class="media-iframe js-media-iframe"></iframe><%
    }
%>
