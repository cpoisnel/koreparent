<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="viewModel" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
    final String currentCode = viewModel.getData().get("CODE_RUBRIQUE");
%><div class="js-messages">
    <p id="champs_obligatoires" class="message alert alert-info"><%= MessageHelper.getCoreMessage("LINK_TYPE.SECTION.TIP") %></p>
</div>
<div id="treeView" class="view">
    <jsp:include page="/adminsite/tree/tree.jsp">
        <jsp:param name="JSTREEBEAN" value="rubriquesJsTree" />
        <jsp:param name="ACTIONS" value="false" />
        <jsp:param name="SELECTED" value="<%= currentCode %>" />
    </jsp:include>
</div>