<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="viewModel" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
    final String value = viewModel.getData().get("URL");
%>
<p class="message alert alert-info"><%= MessageHelper.getCoreMessage("LINK_TYPE.EXTERNAL.TIP") %></p>
<p>
    <label class="colonne" for="URL"><%= MessageHelper.getCoreMessage("LINK_TYPE.EXTERNAL.LABEL") %> (*)</label>
    <input class="type_url" id="URL" name="URL" value="<%= StringUtils.defaultIfEmpty(value, StringUtils.EMPTY) %>" size="50" required/>
</p>
