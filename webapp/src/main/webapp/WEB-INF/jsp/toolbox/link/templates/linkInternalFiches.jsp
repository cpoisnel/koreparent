<%@ page import="java.util.Map" %>
<%@ page import="java.util.Vector" %>
<%@ page import="com.jsbsoft.jtf.core.LangueUtil" %>
<%@ page import="com.kportal.cms.objetspartages.Objetpartage" %>
<%@ page import="com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.om.AutorisationBean" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    AutorisationBean autorisation = ctx.getAutorisation();
    Vector<String> objets = autorisation.getListeObjets();
%>
<p class="message alert alert-info"><%= MessageHelper.getCoreMessage("LINK_TYPE.FICHE.TIP") %></p>
<div class="boite colonne">
    <p>
        <label class="colonne" for="internal-fiches-TITRE"><%= MessageHelper.getCoreMessage("LINK_TYPE.FICHE.TITRE.LABEL") %></label>
        <input autocomplete="off" id="internal-fiches-TITRE" name="TITRE" type="text" maxlength="255" data-width="250" data-bean="multiFicheAutoComplete" data-autocompleteurl="/servlet/com.kportal.servlet.autoCompletionServlet" data-no-tooltip size="20"/>
    </p>
</div><!-- .boite .colonne
--><div class="boite colonne">
    <p>
        <label class="colonne" for="internal-fiches-AUTHOR"><%= MessageHelper.getCoreMessage("LINK_TYPE.FICHE.CODE_REDACTEUR.LABEL") %></label>
        <input type="hidden" data-inputfor="internal-fiches-AUTHOR" name="CODE_REDACTEUR">
        <input autocomplete="off" id="internal-fiches-AUTHOR" name="AUTHOR-AUTO" type="text" maxlength="255" data-groupby="category" data-position="right" data-width="250" data-bean="userAutoComplete" data-autocompleteurl="/servlet/com.kportal.servlet.autoCompletionServlet" data-no-tooltip size="20"/>
    </p>
</div><!-- .boite .colonne -->
<p>
    <label class="colonne" for="internal-fiches-URL_FICHE"><%= MessageHelper.getCoreMessage("LINK_TYPE.FICHE.URL_FICHE.LABEL") %></label>
    <input type="text" value="" id="internal-fiches-URL_FICHE" name="URL_FICHE" size="67">
</p>
<div class="boite colonne">
    <p>
        <label class="colonne" for="internal-fiches-CODE_OBJET"><%= MessageHelper.getCoreMessage("LINK_TYPE.FICHE.TYPE.LABEL") %></label>
        <select id="internal-fiches-CODE_OBJET" name="CODE_OBJET">
            <option value="0000"><%= MessageHelper.getCoreMessage("ST_TOUS") %></option><%
            for (Objetpartage objetCourant : ReferentielObjets.getObjetsPartagesTries()) {
                if (objets.contains(objetCourant.getCodeObjet())) {
                    if (FicheAnnotationHelper.isAccessibleBo(ReferentielObjets.instancierFiche(objetCourant.getCodeObjet()))) {
                    %><option value="<%=objetCourant.getCodeObjet()%>"><%=objetCourant.getLibelleObjet()%></option><%
                    }
                }
            }
        %></select>
    </p>
</div><!-- .boite .colonne
--><div class="boite colonne">
    <p>
        <label class="colonne" for="internal-fiches-LANGUE"><%= MessageHelper.getCoreMessage("LINK_TYPE.FICHE.LANGUE.LABEL") %></label>
        <select id="internal-fiches-LANGUE" name="LANGUE">
            <option value="0000"><%= MessageHelper.getCoreMessage("JTF_SELECTIONNER_LISTE") %></option>
            <%
            for(Map.Entry<String, String> currentEntry : LangueUtil.getListeLangues(ContexteUtil.getContexteUniv().getLocale()).entrySet()) {
                %><option value="<%= currentEntry.getKey() %>"><%= currentEntry.getValue() %></option><%
            }
        %>
        </select>
    </p>
</div><!-- .boite .colonne -->
<p>
    <label for="DATE_MISE_EN_LIGNE_DEBUT" class="colonne"><%= MessageHelper.getCoreMessage("LINK_TYPE.FICHE.DATE_DEBUT.LABEL") %></label>
    <input type="hidden" name="#FORMAT_DATE_MISE_EN_LIGNE_DEBUT" value="1;2;0;10;LIB=;10"><input class="type_date" placeholder="JJ/MM/AAAA" type="text" id="DATE_MISE_EN_LIGNE_DEBUT" name="DATE_MISE_EN_LIGNE_DEBUT" value="" maxlength="10" size="10">
    <label for="DATE_MISE_EN_LIGNE_FIN"><%= MessageHelper.getCoreMessage("LINK_TYPE.FICHE.DATE_FIN.LABEL") %></label>
    <input type="hidden" name="#FORMAT_DATE_MISE_EN_LIGNE_FIN" value="1;2;0;10;LIB=;11"><input class="type_date" placeholder="JJ/MM/AAAA" type="text" id="DATE_MISE_EN_LIGNE_FIN" name="DATE_MISE_EN_LIGNE_FIN" value="" maxlength="10" size="10">
</p>
<table class="dynamicDataTable">
    <thead>
        <tr>
            <th></th>
            <th><%= MessageHelper.getCoreMessage("BO_INTITULE") %></th>
            <th><%= MessageHelper.getCoreMessage("BO_MODIFICATION") %></th>
            <th><%= MessageHelper.getCoreMessage("LINK_TYPE.FICHE.RUBRIQUE.AFFICHAGE") %></th>
            <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_TYPE") %></th>
            <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_LANGUE") %></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>