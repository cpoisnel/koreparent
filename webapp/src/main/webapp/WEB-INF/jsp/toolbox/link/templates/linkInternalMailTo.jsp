<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.extension.ExtensionHelper" %>
<jsp:useBean id="viewModel" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
    String autoCompleteBean = StringUtils.EMPTY;
    final String email = viewModel.getData().get("EMAIL");
    final String subject = viewModel.getData().get("SUBJECT");
    final String body = viewModel.getData().get("BODY");
%>
<p id="champs_obligatoires" class="message alert alert-info"><%= MessageHelper.getCoreMessage("LINK_TYPE.MAIL_TO.TIP") %></p>
<%
    if(ExtensionHelper.getExtension("annuaire") != null) {
        autoCompleteBean = "annuaireAutocomplete";
    } else if(ExtensionHelper.getExtension("annuairesup") != null) {
        autoCompleteBean = "annuaireksupAutocomplete";
    }

    if(StringUtils.isNotBlank(autoCompleteBean)) {
        %><p>
            <label class="colonne" for="internal-mailto-EMAIL"><%= MessageHelper.getCoreMessage("LINK_TYPE.MAIL_TO.EMAIL.LABEL") %> (*)</label>
            <input type="hidden" data-inputfor="internal-mailto-EMAIL" name="EMAIL" value="<%= StringUtils.defaultString(email) %>">
            <input class="type_email" autocomplete="off" id="internal-mailto-EMAIL" name="EMAIL-AUTO" type="text" maxlength="255" size="50" data-bean="<%= autoCompleteBean %>" data-autocompleteurl="/servlet/com.kportal.servlet.autoCompletionServlet" data-no-tooltip value="<%= StringUtils.defaultIfEmpty(email, StringUtils.EMPTY) %>" data-ignorevalidation="ignore" required/>
        </p><%
    } else {
        %><p>
            <label class="colonne" for="EMAIL"><%= MessageHelper.getCoreMessage("LINK_TYPE.MAIL_TO.EMAIL.LABEL") %></label>
            <input type="text" id="EMAIL" name="EMAIL" value="<%= StringUtils.defaultIfEmpty(email, StringUtils.EMPTY) %>" size="50">
        </p><%
    }
    %>
<p>
    <label class="colonne" for="SUBJECT"><%= MessageHelper.getCoreMessage("LINK_TYPE.MAIL_TO.SUBJECT.LABEL") %></label>
    <input type="text" id="SUBJECT" name="SUBJECT" value="<%= StringUtils.defaultIfEmpty(subject, StringUtils.EMPTY) %>" size="50" maxlength="128">
</p>
<p>
    <label class="colonne" for="BODY"><%= MessageHelper.getCoreMessage("LINK_TYPE.MAIL_TO.BODY.LABEL") %></label>
    <textarea id="BODY" name="BODY" rows="10" cols="70" maxlength="1024"><%= StringUtils.defaultIfEmpty(body, StringUtils.EMPTY) %></textarea>
</p>
