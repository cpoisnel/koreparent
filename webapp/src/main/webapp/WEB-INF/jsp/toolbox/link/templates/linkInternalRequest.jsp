<%@ page import="java.util.List" %>
<%@ page import="org.apache.commons.collections.CollectionUtils" %>
<%@ page import="org.apache.commons.collections.Predicate" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.cms.objetspartages.Objetpartage" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.config.PropertyHelper" %>
<%@ page import="com.kportal.extension.IExtension" %>
<%@ page import="com.kportal.extension.module.ModuleHelper" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<jsp:useBean id="viewModel" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
    final String currentNom = viewModel.getData().get("OBJECT_NAME");
    Objetpartage currentObject = null;
    if(StringUtils.isNotBlank(currentNom)) {
        currentObject = ReferentielObjets.getObjetByNom(currentNom);
    }
    final List<Objetpartage> objects = ReferentielObjets.getObjetsPartagesTries();
    CollectionUtils.filter(objects, new Predicate() {
        @Override
        public boolean evaluate(final Object o) {
            final Objetpartage objet = (Objetpartage) o;
            final IExtension extension = ModuleHelper.getExtensionModule(objet);
            final boolean hasAdvancedSearch = "1".equals(PropertyHelper.getExtensionProperty(extension.getId(), String.format("fiche.%s.recherche_avancee", objet.getNomObjet().toUpperCase())));
            return hasAdvancedSearch && !objet.isStrictlyCollaboratif();
        }
    });
%>
<p id="champs_obligatoires" class="message alert alert-info"><%= MessageHelper.getCoreMessage("LINK_TYPE.REQUEST.TIP") %></p>
<p>
    <label for="OBJET" class="colonne"><%= MessageHelper.getCoreMessage("LINK_TYPE.REQUEST.OBJET.LABEL") %></label>
    <select id="OBJET" name="OBJET" class="request-select js-request-select"><%
        for (Objetpartage objet : objects) {
            if(ReferentielObjets.gereLienRequete(objet.getCodeObjet())) {
                final String value = String.format("/servlet/com.jsbsoft.jtf.core.SG?EXT=%s&PROC=%s&ACTION=RECHERCHER&TOOLBOX=LIEN_REQUETE&FCK_PLUGIN=TRUE", objet.getIdExtension(), objet.getParametreProcessus());
            %><option data-url="<%= value %>" value="<%= objet.getNomObjet() %>" <%= objet.getNomObjet().equals(currentNom) && currentObject != null ? "selected" : StringUtils.EMPTY %>><%= objet.getLibelleAffichable() %></option><%
            }
        }
    %></select>
</p>
<div class="edit-panel"><%
    if(currentObject != null) {
        final String requestData = StringUtils.defaultIfEmpty(viewModel.getData().get("REQUEST_DATA"), StringUtils.EMPTY);
        final String value = String.format("/servlet/com.jsbsoft.jtf.core.SG?EXT=%s&PROC=%s&ACTION=RECHERCHER&FCK_PLUGIN=TRUE&TOOLBOX=LIEN_REQUETE%s", currentObject.getIdExtension(), currentObject.getParametreProcessus(), requestData);
        %><iframe class="request-iframe js-request-iframe" src="<%= value %>"></iframe><%
    } else {
        %><iframe class="request-iframe js-request-iframe"></iframe><%
    }
    %>
</div>
