<%@page import="java.util.List"%>
<%@page import="com.jsbsoft.jtf.textsearch.ResultatRecherche"%>
<%@page import="com.univ.objetspartages.om.FicheUniv"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.RequeteUtil"%>
<%@page import="com.univ.utils.URLResolver"%>
<%@ page import="com.univ.utils.UnivWebFmt" %>
<%
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    List<ResultatRecherche> listeFiches = (List<ResultatRecherche>)request.getAttribute("listeFiches");
    String urlFiche = "";
    %>
    <ul class="objets pardefaut">

    <%
    for (ResultatRecherche res : listeFiches){
        // Lecture de la fiche que l'on doit traiter
        FicheUniv fiche = RequeteUtil.lireFiche(res);

        // il est possible que la fiche récupérée soit null, si son état a changé entre sa mise en cache et le rafraichissement de ce dernier
        if (fiche == null) {
            continue;
        }

        // Url d'accès à cette fiche
        urlFiche = URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(ctx, fiche), ctx);

        %><li><a href="<%=URLResolver.getAbsoluteUrl(urlFiche, ctx)%>"><%=fiche.getLibelleAffichable()%></a></li><%
    }%>

    </ul>
