<%@page import="java.util.Vector"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.core.webapp.WebAppUtil"%>
<%@ page import="com.univ.objetspartages.om.AutorisationBean" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.EscapeString" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<%
    boolean toolbox = Boolean.parseBoolean(StringUtils.defaultString(request.getParameter("TOOLBOX")));
%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%if(!toolbox){ %>
<div id="content" role="main">
    <form id="recherche_accueil_bo" method="post" action="<%= WebAppUtil.SG_PATH%>" data-no-tooltip>
    <input type="hidden" value="RECHERCHE" name="#ECRAN_LOGIQUE#"/>
    <input type="hidden" value="RECHERCHE_DIRECTE" name="PROC"/>
    <input type="hidden" value="VALIDER" name="ACTION"/>
<%} %>
        <div class="fieldset neutre">
<%
            ContexteUniv ctx = ContexteUtil.getContexteUniv();
            AutorisationBean autorisation = ctx.getAutorisation();
            Vector<String> objets = autorisation.getListeObjets();
            // Restriction du périmètre sur les arbres de structure et rubrique
            infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT","CODE_RATTACHEMENT");
            infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE","CODE_RUBRIQUE_RECHERCHE");
            infoBean.set("GRS_PERMISSION_TYPE", "FICHE");
            infoBean.set("GRS_PERMISSION_OBJET", "*");
            infoBean.set("GRS_PERMISSION_ACTION", "M");
            univFmt.insererChampSaisie(fmt, out, infoBean, "TITRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_TITRE"));
            univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE_RECHERCHE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE);
            univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_STRUCTURE"), "", UnivFmt.CONTEXT_STRUCTURE);
            univFmt.insererRechercheUtilisateur(fmt, out, infoBean, "CODE_REDACTEUR",MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_REDACTEUR"));
            String codeObjetSelectionne = infoBean.getString("CODE_OBJET");
            %><p>
              <label for="CODE_OBJET" class="colonne"><%= MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_CODE_OBJET")%></label>
                <select id="CODE_OBJET" name="CODE_OBJET">
                    <option value="0000"><%= MessageHelper.getCoreMessage("ST_TOUS") %></option>
                    <%
                    for (Objetpartage objetCourant : ReferentielObjets.getObjetsPartagesTries()) {
                        if(    objets.contains(objetCourant.getCodeObjet())){
                            if (FicheAnnotationHelper.isAccessibleBo(ReferentielObjets.instancierFiche(objetCourant.getCodeObjet()))) {
                                %><option value="<%=objetCourant.getCodeObjet()%>" <%= objetCourant.getCodeObjet().equals(codeObjetSelectionne) ? "selected=\"selected\"" : "" %>><%=objetCourant.getLibelleObjet()%></option><%
                            }
                        }
                    }
                    %>
                </select>
            </p>
            <p>
                <% univFmt.insererComboHashtable(fmt, out, infoBean, "LANGUE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_LANGUES", MessageHelper.getCoreMessage("BO_LANGUE")); %>
            </p>
            <p>
                <% univFmt.insererComboHashtable(fmt, out, infoBean, "ETAT_OBJET", FormateurJSP.SAISIE_FACULTATIF, ReferentielObjets.getEtatsObjet(), MessageHelper.getCoreMessage("BO_ETAT")); %>
            </p>
            <p>
                  <label for="DATE_CREATION_DEBUT" class="colonne"><%= MessageHelper.getCoreMessage("ST_RECHERCHE_DATE_CREATION_DEBUT")%></label>
                  <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_CREATION_DEBUT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, MessageHelper.getCoreMessage("ST_GESTION_NEWSLETTER_DATE_ENVOI"),"",true,"");%>
                  <label for="DATE_CREATION_FIN"><%= MessageHelper.getCoreMessage("ST_RECHERCHE_ET_AVANT_LE")%></label>
                  <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_CREATION_FIN", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, MessageHelper.getCoreMessage("ST_GESTION_NEWSLETTER_DATE_ENVOI"),"",true,"");%>
            </p>
            <p>
                  <label for="DATE_MODIFICATION_DEBUT" class="colonne"><%= MessageHelper.getCoreMessage("ST_RECHERCHE_DATE_MODIFICATION_DEBUT")%></label>
                  <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_MODIFICATION_DEBUT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, MessageHelper.getCoreMessage("ST_GESTION_NEWSLETTER_DATE_ENVOI"),"",true,"");%>
                  <label for="DATE_MODIFICATION_FIN"><%= MessageHelper.getCoreMessage("ST_RECHERCHE_ET_AVANT_LE")%></label>
                  <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_MODIFICATION_FIN", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, MessageHelper.getCoreMessage("ST_GESTION_NEWSLETTER_DATE_ENVOI"),"",true,"");%>
            </p>
            <p>
                  <label for="DATE_MISE_EN_LIGNE_DEBUT" class="colonne"><%= MessageHelper.getCoreMessage("ST_RECHERCHE_DATE_MISE_EN_LIGNE_DEBUT")%></label>
                  <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_MISE_EN_LIGNE_DEBUT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, MessageHelper.getCoreMessage("ST_GESTION_NEWSLETTER_DATE_ENVOI"),"",true,"");%>
                  <label for="DATE_MISE_EN_LIGNE_FIN"><%= MessageHelper.getCoreMessage("ST_RECHERCHE_ET_AVANT_LE")%></label>
                  <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_MISE_EN_LIGNE_FIN", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, MessageHelper.getCoreMessage("ST_GESTION_NEWSLETTER_DATE_ENVOI"),"",true,"");%>
            </p>
            <p>
                <label for="URL_FICHE" class="colonne"><%= MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_URL_FICHE")%></label>
                <input id="URL_FICHE" class="champ-saisie" size="50" type="text" value="<%= EscapeString.escapeAttributHtml(StringUtils.defaultString(infoBean.getString("URL_FICHE"))) %>" name="URL_FICHE" />
            </p>
<%if(!toolbox){ %>
            <p class="validation">
                <input class="submit" type="submit" value="<%= MessageHelper.getCoreMessage("ST_RECHERCHE_DIRECTE_VALIDER")%>" />
            </p>
        </div>
    </form>
</div>
<%}else{%>
        </div>
<%}%>