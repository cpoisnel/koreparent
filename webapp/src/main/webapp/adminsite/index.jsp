<%@ page import="com.jsbsoft.jtf.session.SessionUtilisateur, com.univ.objetspartages.om.AutorisationBean, com.univ.utils.SessionUtil" errorPage="/adminsite/jsbexception.jsp" %>
<%
    AutorisationBean autorisationParam = (AutorisationBean) SessionUtil.getInfosSession(request).get(SessionUtilisateur.AUTORISATIONS);
    //si l'utilisateur a perdu sa    session, ou    bien qu'il n'a pas les droits    sur    l'onglet administration, on    le redirige    vers le    login
    if (SessionUtil.getInfosSession(request).get(SessionUtilisateur.CODE)    == null    || autorisationParam ==    null) {
        request.getRequestDispatcher("/servlet/com.jsbsoft.jtf.core.SG?PROC=IDENTIFICATION&ACTION=CONNECTER").forward(request, response);
    } else {
       request.getRequestDispatcher("/servlet/com.jsbsoft.jtf.core.SG?PROC=IDENTIFICATION&ACTION=REVENIR_ACCUEIL").forward(request, response);
    }
%>

