<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.univ.utils.SessionUtil" %>
<%@ page import="com.univ.objetspartages.om.AutorisationBean" %>
<%@ page import="com.jsbsoft.jtf.session.SessionUtilisateur" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%
    Map<String, Object> infosSession = SessionUtil.getInfosSession(request);
    AutorisationBean autorisation = (AutorisationBean) infosSession.get(SessionUtilisateur.AUTORISATIONS);
    if (autorisation.getAutorisation("0016", AutorisationBean.INDICE_CREATION)) { %>
    <form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post">
        <script>
            function abort(){
                if(window.iFrameRegistration){
                    iFrameHelper.abort(window.iFrameRegistration);
                }
            }
        </script>
        <input type="hidden" name="PROC" value="TRAITEMENT_PAGELIBRE">
        <input type="hidden" name="#ECRAN_LOGIQUE#" value="PRINCIPAL">
        <input type="hidden" name="#ETAT#" value="CREATION">
        <input type="hidden" name="CODE_RUBRIQUE" value="<%=infoBean.get("CODE_RUBRIQUE")%>">
        <div id="content" role="main">
            <%univFmt.insererChampSaisie( fmt, out, infoBean, "TITRE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("BO_PAGELIBRE_TITRE"));%>
            <%univFmt.insererComboHashtable( fmt, out, infoBean, "LANGUE", FormateurJSP.SAISIE_OBLIGATOIRE, "LISTE_LANGUES", MessageHelper.getCoreMessage("BO_LANGUE") ); %>
            <div class="footer_popup">
                <p class="validation">
                    <input id="enregistrer" type="submit" name="ENREGISTRER" value="<%=MessageHelper.getCoreMessage("BO_ENREGISTRER") %>"/>
                    <input id="annuler" type="reset" name="ANNULER" value="<%=MessageHelper.getCoreMessage("JTF_BOUTON_ANNULER") %>" onclick="abort();"/>
                </p>
            </div><!-- .footer_popup -->
        </div><!-- #content -->
    </form>
<% } else { %>
    <div class="content_popup">
        <p>
            <span><%= MessageHelper.getCoreMessage("BO_PAGELIBRE_PAS_AUTORISATIONS") %></span>
            <span><a href="#" onclick="abort();"><%= MessageHelper.getCoreMessage("BO_PAGELIBRE_RETOUR_EDITION_RUBRIQUE") %></a></span>
        </p>
    </div><!-- .content_popup -->
<% } %>
