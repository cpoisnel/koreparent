<%@ page import="java.util.Collection" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="org.apache.commons.collections.CollectionUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.CodeLibelle" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.ProcessusHelper" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.config.PropertyHelper" %>
<%@ page import="com.kportal.core.webapp.WebAppUtil" %>
<%@ page import="com.kportal.extension.module.composant.IComposant" %>
<%@ page import="com.univ.objetspartages.services.ServiceUserPass" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@ page import="com.univ.objetspartages.bean.GroupeDsiBean" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />
<%
String ongletCourant = StringUtils.defaultString(infoBean.getString("ONGLET_COURANT"));
ongletCourant = StringUtils.isBlank(ongletCourant) ? "divGeneral" : ongletCourant;
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
boolean editAuthorized = "1".equals(StringUtils.defaultIfBlank(infoBean.get("MODIFICATION_AUTORISEE", String.class), "1"));
%>

<form action="<%= WebAppUtil.SG_PATH %>" enctype="multipart/form-data" method="post" autocomplete="off">
    <div id="actions">
        <div>
            <ul><%
                if ("MODIFIER".equals(infoBean.getActionUtilisateur()) || "MODIFIERPARID".equals(infoBean.getActionUtilisateur())) { %>
                    <li><button class="supprimer" data-confirm="<%= module.getMessage("BO_CONFIRM_SUPPR_UTILISATEUR") %>" type="submit" name="SUPPRIMER" value="<%= MessageHelper.getCoreMessage("JTF_BOUTON_SUPPRIMER") %>"><%= MessageHelper.getCoreMessage("JTF_BOUTON_SUPPRIMER") %></button></li><%
                }
                %><li><button class="enregistrer" type="submit" name="VALIDER" value="<%= MessageHelper.getCoreMessage("BO_ENREGISTRER")%>"><%= MessageHelper.getCoreMessage("BO_ENREGISTRER")%></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->

    <ul id="onglets">
        <li id="liGeneral" data-targetdivid="divGeneral" onclick="activeOnglet(this);" class="<%=ongletCourant.equals("divGeneral") ? "active" : ""%>"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_INFOS_GENERALES") %></li>
        <li id="liRattachements" data-targetdivid="divRattachements" onclick="activeOnglet(this);" class="<%=ongletCourant.equals("divRattachements") ? "active" : ""%>"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_RATTACHEMENTS") %></li>
        <li id="liAutorisations" data-targetdivid="divAutorisations" onclick="activeOnglet(this);" class="<%=ongletCourant.equals("divAutorisations") ? "active" : ""%>"><%= MessageHelper.getCoreMessage("BO_ROLES") %></li>
        <li id="liPreferences" data-targetdivid="divPreferences" onclick="activeOnglet(this);" class="<%=ongletCourant.equals("divPreferences") ? "active" : ""%>"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_PREFERENCES") %></li>
    </ul>

    <div id="content" role="main">
        <%-- JSS 20040409 : gestion des roles --%>
        <input type="hidden" name="ACTION" value="" />
        <input type="hidden" name="ONGLET_COURANT" value="<%= ongletCourant %>"/>
        <% fmt.insererVariablesCachees(out, infoBean); %>
        <div id="divGeneral" style="display: <%=ongletCourant.equals("divGeneral") ? "block" : "none"%>;">
            <div class="fieldset neutre">
                <p>
                    <label for="CIVILITE" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_CIVILITE") %> <% if (UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.CIVILITE") == FormateurJSP.SAISIE_OBLIGATOIRE) { %> (*)<% } %></label><%
                    if(editAuthorized) {
                        fmt.insererCombo(out, infoBean, "CIVILITE", UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.CIVILITE"), "civilite");
                    } else {
                        final String civilite = infoBean.get("CIVILITE", String.class);
                        if(StringUtils.isBlank(civilite) || "0000".equals(civilite)) {
                            %><span><%= MessageHelper.getCoreMessage("JTF_SELECTIONNER_LISTE") %></span><%
                        } else {
                            final Hashtable<String, String> civilites = CodeLibelle.lireTable(infoBean.getNomExtension(), "civilite", ContexteUtil.getContexteUniv().getLocale());
                            %><span><%= civilites.get(infoBean.get("CIVILITE", String.class)) %></span><%
                        }
                    }
                %></p>

                <p>
                    <label for="NOM" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_NOM") %> <% if (UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.NOM") == FormateurJSP.SAISIE_OBLIGATOIRE) { %> (*)<% } %></label><%
                    if(editAuthorized) {
                        fmt.insererChampSaisie(out, infoBean, "NOM", UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.NOM"), FormateurJSP.FORMAT_TEXTE, 0, 40);
                    } else {
                        %><span><%= infoBean.get("NOM", String.class) %></span><%
                    }
                %></p>

                <p>
                    <label for="PRENOM" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_PRENOM") %> <% if (UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.PRENOM") == FormateurJSP.SAISIE_OBLIGATOIRE) { %> (*)<% } %></label><%
                    if(editAuthorized) {
                        fmt.insererChampSaisie(out, infoBean, "PRENOM", UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.PRENOM"), FormateurJSP.FORMAT_TEXTE, 0, 40);
                    } else {
                        %><span><%= infoBean.get("PRENOM", String.class) %></span><%
                    }
                %></p>

                <p>
                    <label for="ADRESSE_MAIL" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_ADRESSE_MEL") %><% if (UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.ADRESSE_MAIL") == FormateurJSP.SAISIE_OBLIGATOIRE) { %> (*)<% } %></label><%
                    if(editAuthorized) {
                        fmt.insererChampSaisie(out, infoBean, "ADRESSE_MAIL", UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.ADRESSE_MAIL"), FormateurJSP.FORMAT_TEXTE, 0, 255,"EMAIL=YES");
                    } else {
                        %><span><%= infoBean.get("ADRESSE_MAIL", String.class) %></span><%
                    }
                %></p>

                <p>
                    <% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.CODE_RATTACHEMENT"), MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE);%>
                </p>
            </div><!-- .fieldset .neutre -->
            <fieldset>
            <legend><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_ACCES") %></legend>

            <p>
                <label for="CODE" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_IDENTIFIANT") %> <% if (UnivFmt.getModeSaisieCode(infoBean) == FormateurJSP.SAISIE_OBLIGATOIRE) { %> (*)<% } %></label>
                <% fmt.insererChampSaisie(out, infoBean, "CODE", UnivFmt.getModeSaisieCode(infoBean), FormateurJSP.FORMAT_TEXTE, 0, 64,64,"LIB=Identifiant,noautocomplete"); %>
            </p>
            <%
            // 20050908 JB : affichage du login si mapping active
            // ce login n'est pas modifiable mais sa saisie est obligatoire à l'ajout.
            if (UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.CODE_LDAP") != -1)  { %>
                <p>
                    <label for="CODE_LDAP" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_CODE_LDAP")%><% if (UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.CODE_LDAP") == FormateurJSP.SAISIE_OBLIGATOIRE) { %> (*)<% } %></label>
                    <% fmt.insererChampSaisie(out, infoBean, "CODE_LDAP", UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.CODE_LDAP"), FormateurJSP.FORMAT_TEXTE, 0, 64); %>
                </p>
            <% } %>

            <%if (UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.MOT_DE_PASSE") != -1 && "1".equals(infoBean.get("MODIFICATION_MOTDEPASSE_AUTORISEE")))  { %>
                <div>
                    <label for="NEW_MOT_DE_PASSE" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_MDP")%></label>
                    <div class="pwdStrength">
                        <% fmt.insererChampSaisie(out, infoBean, "NEW_MOT_DE_PASSE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, ServiceUserPass.getLongueurChampMotDePasse(),"password,noautocomplete"); %>
                    </div>
                </div>
                <p>
                    <label for="CONFIRM_MOT_DE_PASSE" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_MDP_CONFIRMATION")%></label>
                    <% fmt.insererChampSaisie(out, infoBean, "CONFIRM_MOT_DE_PASSE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, ServiceUserPass.getLongueurChampMotDePasse(),"password,noautocomplete"); %>
                </p>
            <% } %>

            </fieldset>
        </div><!-- #divGeneral -->

        <div id="divRattachements" class="fieldset neutre" style="display: <%=ongletCourant.equals("divRattachements") ? "block" : "none"%>;">
            <% if(UnivFmt.getModeSaisieZone(infoBean, "UTILISATEUR.GROUPES_DSI") != 0 ) {
                univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "GROUPE_DSI", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_GROUPES"), "TMP_GROUPE_DSI", MessageHelper.getCoreMessage("BO_GROUPE"), UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION, "lock_dyn");
                final Collection<GroupeDsiBean> dynGroups = infoBean.get("GROUPE_DSI_DYN", Collection.class);
                if (CollectionUtils.isNotEmpty(dynGroups)) {
            %>
            <div>
                <span class="label colonne"><%= MessageHelper.getCoreMessage("BO.SAISIE_UTILISATEUR.GROUPE_DYN") %></span>
                <ul><%
                    for (GroupeDsiBean currentGroup : dynGroups) {
                %>
                    <li><%= StringUtils.isNotBlank(currentGroup.getLibelle()) ? currentGroup.getLibelle() : MessageHelper.getCoreMessage("BO_GROUPE_INEXISTANT") %></li>
                    <%
                        }
                    %></ul>
            </div>
            <%
                }
            } else {%>
                   <p>
                    <%univFmt.insererChampSaisie(fmt, out, infoBean, "GROUPE_DSI", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 40, MessageHelper.getCoreMessage("BO_GROUPES"));%>
                   </p>
            <% } %>
            <p>
                <label for="PROFIL_DEFAUT" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_PROFIL_DEFAUT")%></label>
                <% fmt.insererComboHashtable(out, infoBean, "PROFIL_DEFAUT", FormateurJSP.SAISIE_FACULTATIF, "LISTE_PROFILS"); %>
                <input type="button" name="RAFRAICHIR_PROFIL" value="<%= MessageHelper.getCoreMessage("BO_ACTUALISER") %>" onclick="actualiserProfil(); return false" />
            </p>
        </div>

        <div id="divAutorisations" style="display: <%=ongletCourant.equals("divAutorisations") ? "block" : "none"%>;">
            <jsp:include page="/adminsite/objetspartages/affectation_role_saisie.jsp" />
        </div>

        <div id="divPreferences" class="fieldset neutre" style="display: <%=ongletCourant.equals("divPreferences") ? "block" : "none"%>;">
            <!-- CENTRES D'INTERET -->
            <div>
                <span class="label colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_CENTRES_INTERET") %></span>
                <div class="options">
                    <ul>
                    <%
                        int nbLignes = infoBean.getInt("THEMES_NB_ITEMS");
                        for (int i=0; i < nbLignes; i++) { %>
                            <li>
                                <%fmt.insererChampSaisie(out, infoBean, "VALEUR_THEME#" + i, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
                                <label for="VALEUR_THEME#<%=i%>"><%= infoBean.getString("LIBELLE_THEME#" + i) %></label>
                            </li>
                    <% } %>
                    </ul>
                </div>
            </div>
            <!-- CENTRES D'INTERET -->
            <div>
                <span class="label colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_FORMAT_EMAIL") %></span>
                <div class="options">
                    <ul>
                        <li>
                            <% fmt.insererChampSaisie(out, infoBean,"FORMAT_ENVOI", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_RADIO,0,    0,""); %>
                            <label for="FORMAT_ENVOI_0"><%= infoBean.getString("RADIO0") %></label>
                        </li>
                        <li>
                            <% fmt.insererChampSaisie(out, infoBean,"FORMAT_ENVOI", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_RADIO,0,    0,""); %>
                            <label for="FORMAT_ENVOI_1"><%= infoBean.getString("RADIO1") %></label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <% if (PropertyHelper.getCoreProperty("utilisateur.creation.annuaire") != null) { %>
            <jsp:include page="/adminsite/objetspartages/utilisateur_synchro_annuaire.jsp" />
        <% } %>
    </div><!-- #content -->
</form>

<script type="text/javascript">
    function actualiserProfil() {
        window.document.forms[0].ACTION.value="ACTUALISER_PROFIL";
        window.document.forms[0].submit();
    }

    function activeOnglet(element) {
        var $ = jQuery,
            $lis = $('#onglets').find('li[id]'),
            $inputOnglet = $('input[name="ONGLET_COURANT"]');

        $lis.each(function(index, li){
            $(li).removeClass('active');
            $('#' + $(li).data('targetdivid')).css({display: 'none'});
        });
        $(element).addClass('active');
        $('#' + $(element).data('targetdivid')).css({display: 'block'});
        $inputOnglet.val($(element).data('targetdivid'));
    }
</script>