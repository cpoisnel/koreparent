<%@page import="java.util.Collection"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@page import="com.kportal.ihm.utils.AdminsiteUtils"%>
<%@page import="com.univ.datagrid.utils.DatagridUtils"%>
<%@page import="com.univ.objetspartages.util.CritereRecherche"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
String mode = infoBean.getString("MODE");
boolean isPleinePage = "GESTION".equals(mode);
%><div id="content" role="main"><%
    Collection<CritereRecherche> criteresRecherche = AdminsiteUtils.getCriteresRechercheAAfficher(infoBean);
    if (!criteresRecherche.isEmpty() && isPleinePage) {
%><div id="recherche_criteres">
            <h3><%= module.getMessage("BO_CRITERES_RAPPEL") %></h3>
            <div>
                <ul><%
                for (CritereRecherche critere : criteresRecherche) {
                    String champ = StringUtils.defaultIfBlank(module.getMessage("UTILISATEUR." +critere.getNomChamp()),critere.getNomChamp());
                    %><li><strong><%= champ %></strong> <%=critere.getValeurAAfficher() %></li><%
                }
                %></ul>
                <p><a href="<%=AdminsiteUtils.getUrlRechercheAvanceeDatagrid(module,infoBean) %>"><%= module.getMessage("BO_AFFINER_RECHERCHE") %></a></p>
            </div>
        </div><!-- #recherche_criteres --><%
    }
    %><table class="datatableUtilisateur<%=mode %>" data-search="<%=DatagridUtils.getUrlTraitementDatagrid(infoBean)%>">
        <thead>
            <tr><%
                if (isPleinePage) { %>
                    <th><input type="checkbox" id="checkall" /></th><%
                }
                %><th><%= module.getMessage("BO_NOM_PRENOM") %></th>
                <th><%= module.getMessage("BO_IDENTIFIANT") %></th><%
                if (isPleinePage) {
                    %><th><%= module.getMessage("ST_DSI_EMAIL") %></th><%
                }
                %><th><%= module.getMessage("ST_TABLEAU_STRUCTURE") %></th><%
                if (isPleinePage) {
                    %><th class="sanstri sansfiltre"><%= module.getMessage("BO_ACTIONS") %></th><%
                }
            %></tr>
        </thead>
        <tbody></tbody>
    </table>
</div>