<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%
String nomRequete = (String) request.getAttribute("NOM_REQUETE");
%>
<!-- Groupes dynamiques de structure -->
<div id="groupedyn_<%= nomRequete %>" class="dyn_panel js-dyn_panel" data-panel="<%= nomRequete %>">
    <% univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "REQUETE_LDAP_"+ nomRequete, FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_STRUCTURES"), "REQUETE_LDAP_"+ nomRequete, MessageHelper.getCoreMessage("BO_STRUCTURES"), univFmt.CONTEXT_STRUCTURE, "");    %>
</div>
