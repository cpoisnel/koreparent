<%@page import="java.util.Collection"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@page import="com.kportal.ihm.utils.AdminsiteUtils"%>
<%@page import="com.univ.datagrid.utils.DatagridUtils"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<%@page import="com.univ.objetspartages.util.CritereRecherche"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.EscapeString"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
String toolbox = StringUtils.defaultString(infoBean.getString("TOOLBOX"));
String typeObjet = ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")).toUpperCase();
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
ContexteUniv ctx = ContexteUtil.getContexteUniv();
boolean isToolbox = StringUtils.isNotBlank(toolbox);
%><div id="content" role="main"><%
    Collection<CritereRecherche> criteresRecherche = AdminsiteUtils.getCriteresRechercheAAfficher(infoBean);
    if (!criteresRecherche.isEmpty() && !isToolbox) {
%><div id="recherche_criteres">
            <h3><%= module.getMessage("BO_CRITERES_RAPPEL") %></h3>
            <div>
                <ul><%
                for (CritereRecherche critere : criteresRecherche) {
                    String champ = StringUtils.defaultIfBlank(module.getMessage(typeObjet + "." +critere.getNomChamp()),critere.getNomChamp());
                    %><li><strong><%= champ %></strong> <%=EscapeString.escapeHtml(critere.getValeurAAfficher()) %></li><%
                }
                %></ul>
                <p><a href="<%=AdminsiteUtils.getUrlRechercheAvanceeDatagrid(module,infoBean) %>"><%= module.getMessage("BO_AFFINER_RECHERCHE") %></a></p>
            </div>
        </div><!-- #recherche_criteres --><%
    }
%><table class="datatableFiche<%= isToolbox ? "toolbox" : "" %>" 
        data-toolbox="<%= EscapeString.escapeAttributHtml(toolbox) %>"
        data-liste="<%=EscapeString.escapeAttributHtml(StringUtils.defaultString(infoBean.getString("LISTE"))) %>"
        data-search="<%=DatagridUtils.getUrlTraitementDatagrid(infoBean)%>">
        <thead>
            <tr><%
                if (!isToolbox) {
                    %><th><input type="checkbox" id="checkall" /></th><%
                }
                %><th><%= module.getMessage("BO_INTITULE") %></th><%
                %><th><%= module.getMessage("BO_RUBRIQUE") %></th><%
                %><th><%= module.getMessage("BO_LIBELLE_LANGUE") %></th><%
                if (!isToolbox) {
                    %><th><%= module.getMessage("BO_ETAT") %></th>
                    <th><%= module.getMessage("BO_MODIFICATION") %></th><%
                    %><th class="sanstri sansfiltre"><%= module.getMessage("BO_ACTIONS") %></th><%
                }
            %></tr>
        </thead>
        <tbody></tbody>
    </table>
</div><!--  #content -->