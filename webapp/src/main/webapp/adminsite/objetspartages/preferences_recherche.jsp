<%@page import="java.util.Collection"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.univ.objetspartages.om.ServiceBean"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
%><div id="content" role="main">
    <table class="datatable">
        <thead>
            <tr>
                <th><%= module.getMessage("BO_LIBELLE") %></th>
                <th><%= module.getMessage("BO_LIBELLE_CODE") %></th>
                <th class="sanstri sansfiltre"><%= module.getMessage("BO_ACTIONS") %></th>
            </tr>
        </thead>
        <tbody><%
        Collection<ServiceBean> lesServices = (Collection<ServiceBean>)infoBean.get("SERVICES");
        int i = 0;
        for (ServiceBean service : lesServices) {
            i++;
            String[][] params = new String[][]{{"CODE_SERVICE",String.valueOf(service.getCode())}};
            %><tr>
                <td><%= service.getIntitule() %></td>
                <td><%= service.getCode() %></td>
                <td>
                    <a href="<%=ProcessusHelper.getUrlProcessAction(infoBean, "", "", "MODIFIERPARCODE", params) %>" title="<%= module.getMessage("BO_MODIFIER") %>"><%= module.getMessage("BO_MODIFIER") %></a>
                    <a data-confirm="<%= module.getMessage("BO_CONFIRM_SUPPR_SERVICE") %>" href="<%=ProcessusHelper.getUrlProcessAction(infoBean, "", "", "SUPPRIMERPARCODE",params) %>" title="<%= module.getMessage("BO_SUPPRIMER") %>"><%= module.getMessage("BO_SUPPRIMER") %></a>
                </td>
            </tr><%
        }
        %></tbody>
    </table>
</div><!-- #content -->
