<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<div id="content" role="main">
<script type="text/javascript">
    function soumettreAjoutRequete(i) {
        window.document.forms[0].ACTION.value="AJOUTER_REQUETE#" + i;
        window.document.forms[0].submit();
    }
</script>
<form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post">

    <input type="hidden" name="ACTION" value="">

    <%
        fmt.insererVariablesCachees( out, infoBean);
        int nbLignes = infoBean.getInt("REQUETE_NB_ITEMS");
    %>
    <div class="content_popup">
        <div class="fieldset neutre">
            <p>
                <label for="REQUETE_OBJET_<%=nbLignes%>" class="colonne"><%= MessageHelper.getCoreMessage("CODE_OBJET") %></label>
                <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "REQUETE_OBJET_"+nbLignes, fmt.SAISIE_FACULTATIF , "LISTE_OBJETS_REQUETE", "","");%>
            </p>
            <div>
                <% univFmt.insererkMonoSelect(fmt, out, infoBean, "REQUETE_STRUCTURE_"+nbLignes, fmt.SAISIE_FACULTATIF,MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT"), "", univFmt.CONTEXT_STRUCTURE);%>
            </div>
            <p class="retrait">
                <%fmt.insererChampSaisie( out, infoBean, "REQUETE_STRUCTURE_ARBO_"+nbLignes ,  fmt.SAISIE_FACULTATIF , fmt.FORMAT_CHECKBOX, 0, 1);%>
                <label for="REQUETE_STRUCTURE_ARBO_<%=nbLignes%>"><%= MessageHelper.getCoreMessage("BO_ROLE_INCLURE_SOUS_STRUCTURES") %></label>
            </p>
            <div>
                <% univFmt.insererkMonoSelect(fmt, out, infoBean, "REQUETE_RUBRIQUE_"+nbLignes, fmt.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_RUBRIQUE"), "rubrique", univFmt.CONTEXT_ZONE);%>
            </div>
            <p class="retrait">
                <%fmt.insererChampSaisie( out, infoBean, "REQUETE_RUBRIQUE_ARBO_"+nbLignes ,  fmt.SAISIE_FACULTATIF , fmt.FORMAT_CHECKBOX, 0, 1);%>
                <label for="REQUETE_RUBRIQUE_ARBO_<%=nbLignes%>"><%= MessageHelper.getCoreMessage("BO_ROLE_INCLURE_SOUS_RUBRIQUES") %></label>
            </p>
            <p>
                <%univFmt.insererComboHashtable(fmt, out, infoBean, "REQUETE_LANGUE_"+nbLignes, fmt.SAISIE_FACULTATIF, "LISTE_LANGUES",MessageHelper.getCoreMessage("BO_LANGUE"));%>
            </p>
            <p>
                <input type="hidden" name="REQUETE_MODE_<%=nbLignes%>" value="1">
            </p>
            <p class="validation">
                <input type="button" name="AJOUTER_REQUETE" value="<%= MessageHelper.getCoreMessage("BO_VALIDER") %>" onclick="soumettreAjoutRequete (<%=nbLignes%>); return false">
                <input class="bouton" type="submit" name="REVENIR"  value="<%= MessageHelper.getCoreMessage("JTF_BOUTON_REVENIR") %>" />
            </p>
        </div>
    </div>
</form>
</div>


