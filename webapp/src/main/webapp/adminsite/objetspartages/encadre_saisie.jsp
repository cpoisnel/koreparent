<%@ taglib prefix="components" uri="http://kportal.kosmos.fr/tags/components" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.InfoBean" %>
<%@ page import="com.jsbsoft.jtf.core.ProcessusHelper" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.extension.module.composant.IComposant" %>
<%@ page import="com.univ.objetspartages.om.AutorisationBean" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
ContexteUniv ctx = ContexteUtil.getContexteUniv();
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
AutorisationBean autorisation = ctx.getAutorisation();
boolean actionPerimetre = infoBean.get("ACTION_PERIMETRE") != null ? ((Boolean)infoBean.get("ACTION_PERIMETRE")).booleanValue() : Boolean.FALSE ;
%><form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post" data-ui="tabs">
    <div id="actions">
        <div>
            <ul><%
                if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) {%>
                    <li><button class="supprimer" data-confirm="<%=module.getMessage("BO_CONFIRM_SUPPR_ENCADRE") %>" type="submit" name="SUPPRIMER" value="<%= MessageHelper.getCoreMessage("JTF_BOUTON_SUPPRIMER")%>"><%= MessageHelper.getCoreMessage("JTF_BOUTON_SUPPRIMER")%></button></li><%
                }
                %><li><button class="enregistrer" type="submit" name="VALIDER" value="<%= MessageHelper.getCoreMessage("BO_ENREGISTRER")%>"><%= MessageHelper.getCoreMessage("BO_ENREGISTRER")%></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->
    <div>
        <ul id="onglets" data-ui="tabs-header">
            <li class="onglet-contenu1 <%= actionPerimetre ? "" : "active" %>" data-action="ONGLET" data-onglet="PRINCIPAL"><a href="#contenu-principal" data-ui="tabs-tab"><%= module.getMessage("BO_ONGLET_CONTENU_PRINCIPAL") %></a></li>
            <li class="onglet-contenu2 <%= actionPerimetre ? "active" : "" %>" data-action="ONGLET" data-onglet="PERIMETRE"><a href="#perimetre-affichage" data-ui="tabs-tab"><%= module.getMessage("BO_ONGLET_PERIMETRE_AFFICHAGE") %></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div id="content" role="main">
        <div class="formulaire_hidden">
            <input type="hidden" name="ACTION" value="" />
            <!-- CFL 20080616 : HYPER IMPORTANT POUR CHARGEMENT MULTIPLES FCKEDITOR SUR UNE MEME PAGE
                 RESOUD LE PROBLEME DE CHARGEMENT INFINI (bug fck avec firefox...)-->
            <input type="hidden" name="FCK_EDITORS_NAMES" value="" />
            <% fmt.insererVariablesCachees( out, infoBean); %>
        </div> <!-- .formulaire_hidden -->
        <div id="contenu-principal" class="fieldset neutre">
            <input type="hidden" name="CODE" value="<%= infoBean.get("CODE") %>" />
            <p class="retrait">
                <%fmt.insererChampSaisie(out, infoBean, "ACTIF", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
                <label for="ACTIF" class="colonne"><%= module.getMessage("JTF_ACTIF") %></label>
            </p>
            <p>
                <label for="INTITULE" class="colonne"><%= module.getMessage("BO_INTITULE") %>(*)</label>
                <%fmt.insererChampSaisie(out, infoBean, "INTITULE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 32); %>
            </p><%
            if(!autorisation.isWebMaster())
                infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT", "CODE_RATTACHEMENT");
                univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, module.getMessage("CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE);
                if(!autorisation.isWebMaster())
                    infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE", "CODE_RUBRIQUE");
                univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, module.getMessage("BO_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE);
            %><p>
                <label for="LANGUE" class="colonne"><%= module.getMessage("BO_LANGUE") %>(*)</label>
                <%fmt.insererComboHashtable(out, infoBean, "LANGUE", FormateurJSP.SAISIE_OBLIGATOIRE , "LISTE_LANGUES" ); %>
            </p>
            <p>
                <label for="POIDS" class="colonne"><%= module.getMessage("BO_ENCADRE_ORDRE") %> (*)</label>
                <%fmt.insererChampSaisie( out, infoBean, "POIDS", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_ENTIER, 0, 2); %>
                <span><%= module.getMessage("BO_ENCADRE_ORDRE_EXPLICATION") %></span>
            </p>
            <components:toolbox fieldName="CONTENU" label="BO_ENCADRE_CONTENU" min="0" max="65535" editOption='<%= FormateurJSP.SAISIE_FACULTATIF %>'/>
        </div>
        <div id="perimetre-affichage">
        <%-- Liste des objets --%><%
        int nbLignes = infoBean.getInt("CODE_OBJETS_NB_ITEMS");
        if(nbLignes == 0){
            %><p class="message alert alert-info"><%= module.getMessage("BO_ENCADRE_AFFICHAGE_TOUS") %></p><%
        } else {
            %><p class="message alert alert-info"><%= module.getMessage("BO_ENCADRE_AFFICHAGE_SELECTIF") %><br/><span><%= module.getMessage("BO_ENCADRE_AFFICHAGE_SELECTIF_EXPLICATION") %></span></p>
            <script type="text/javascript">
                function soumettreSuppressionCode ( i) {
                    window.document.forms[0].ACTION.value="SUPPRIMER_CODE_OBJET#" + i;
                    window.document.forms[0].submit();
                }
            </script>
            <ul id="perimetre"><%
                for (int i=0; i< nbLignes; i++) { %>
                    <li><%
                        int indiceRub = infoBean.getString("CODE_OBJET#"+i).indexOf("RUB_");
                        if( indiceRub != 0) {
                            String codeComplet = infoBean.getString( "CODE_OBJET#"+i);
                            int indiceSeparateur = codeComplet.indexOf('/');
                            if(indiceSeparateur != -1) {
                                String codeObjet = codeComplet.substring(0, indiceSeparateur);
                                String codeStructure = codeComplet.substring( indiceSeparateur + 1, codeComplet.length());
                                String libelleObjet = StringUtils.defaultString((String)(((Hashtable) infoBean.get("LISTE_OBJETS")).get( codeObjet)));
                                libelleObjet = libelleObjet.toUpperCase();
                                String libelleStructure = StringUtils.defaultString((String)(((Hashtable) infoBean.get("LISTE_STRUCTURES")).get( codeStructure)));
                                if(libelleStructure.length()==0 && libelleObjet.length()>0) {
                                    %><span><%= libelleObjet %></span><%
                                } else if(libelleStructure.length() >0 && libelleObjet.length()==0) {
                                    %><span><%= libelleStructure %></span><%
                                } else {
                                    %><span><%= libelleObjet %> (<%= libelleStructure %>)</span><%
                                }
                            }
                        } else {
                            String codeRubrique = infoBean.getString("CODE_OBJET#"+i).substring( 4);
                            %><span><%= module.getMessage("BO_RUBRIQUE") %> '<%= infoBean.get("INTITULE_OBJET#" + i)%>'</span><%
                        }
                        %><input type="button" name="SUPPRIMER_CODE_OBJET#<%=i%>" value="<%= module.getMessage("BO_SUPPRIMER") %>" onclick="soumettreSuppressionCode( <%=i%> ); return false" /><%
                    %></li><%
                }
            %></ul><%
        }
        %><br/><%
        //si le perimetre de gestion des encadres est restreint à une rubrique, l'utilisateur ne pourra de toute façon pas appliquer son encadré à une structure entière
        if(autorisation.isWebMaster() || autorisation.possedePermissionPartielleSurPerimetre( new com.univ.objetspartages.om.PermissionBean ("TECH","enc","M"), new com.univ.objetspartages.om.Perimetre("","-","","","")) ) {
            if( !autorisation.isWebMaster()) {
                infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT", "CODE_STRUCTURE");
            } %>
            <fieldset>
                <legend><%= module.getMessage("BO_ENCADRE_PERIMETRE_FICHE") %></legend>
                <p>
                    <label class="colonne" for="CODE_OBJET"><%= module.getMessage("BO_TYPE_FICHE") %></label>
                    <%    fmt.insererComboHashtable( out, infoBean, "CODE_OBJET", FormateurJSP.SAISIE_FACULTATIF, "LISTE_OBJETS" ); %>
                </p>
                <div>
                    <span class="label colonne"><%= module.getMessage("BO_ENCADRE_PERIMETRE_FICHE_STRUCTURE") %></span>
                    <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_STRUCTURE", FormateurJSP.SAISIE_FACULTATIF, "", "", UnivFmt.CONTEXT_STRUCTURE);%>
                    <script type="text/javascript">
                        function soumettreAjoutStructure() {
                            window.document.forms[0].ACTION.value="AJOUTER_CODE_STRUCTURE";
                            window.document.forms[0].submit();
                        }
                    </script>
                    <input type="button" name="AJOUTER_CODE_STRUCTURE" value="<%= module.getMessage("BO_AJOUTER") %>" onclick="soumettreAjoutStructure( ); return false" />
                </div>
            </fieldset><%
        }
        //si le perimetre de gestion des encadres est restreint à une structure, l'utilisateur ne pourra de toute façon pas appliquer son encadré à une rubrique entière
        if(autorisation.isWebMaster() || autorisation.possedePermissionPartielleSurPerimetre( new com.univ.objetspartages.om.PermissionBean ("TECH","enc","M"), new com.univ.objetspartages.om.Perimetre("-","","","","")) ) {
            %><fieldset>
                <legend><%= module.getMessage("BO_ENCADRE_PERIMETRE_RUBRIQUE") %></legend>
                <script type="text/javascript">
                    function soumettreAjoutRubrique() {
                        window.document.forms[0].ACTION.value="AJOUTER_CODE_RUBRIQUE";
                        window.document.forms[0].submit();
                    }
                </script><%
                if(!autorisation.isWebMaster()) {
                    infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RUBRIQUE", "CODE_RUBRIQUE_APPLICATION");
                }
                %><div>
                    <span class="label colonne"><%= module.getMessage("BO_RUBRIQUE") %></span>
                    <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE_APPLICATION", FormateurJSP.SAISIE_FACULTATIF, "", "rubrique", UnivFmt.CONTEXT_ZONE);%>
                    <input type="button" name="AJOUTER_CODE_RUBRIQUE" value="<%= module.getMessage("BO_AJOUTER") %>" onclick="soumettreAjoutRubrique( ); return false" />
                </div>
            </fieldset><%
        }
        //si le perimetre de gestion des encadres est restreint à une rubrique, l'utilisateur ne pourra de toute façon pas appliquer son encadré à une structure entière
        if(autorisation.isWebMaster() || autorisation.possedePermissionPartielleSurPerimetre( new com.univ.objetspartages.om.PermissionBean ("TECH","enc","M"), new com.univ.objetspartages.om.Perimetre("","-","","","")) ) {
            %><fieldset>
                <legend><%= module.getMessage("BO_ENCADRE_PERIMETRE_STRUCTURE") %></legend>
                <script type="text/javascript">
                    function soumettreAjoutStructureSeule() {
                        window.document.forms[0].ACTION.value="AJOUTER_CODE_STRUCTURE_SEULE";
                        window.document.forms[0].submit();
                    }
                </script><%
                if(!autorisation.isWebMaster()) {
                    infoBean.set("GRS_FILTRE_ARBRE_NOM_CODE_RATTACHEMENT", "CODE_STRUCTURE_SEULE");
                }
                %><div>
                    <span class="label colonne"><%= module.getMessage("ST_CODE_RATTACHEMENT") %></span>
                    <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_STRUCTURE_SEULE", FormateurJSP.SAISIE_FACULTATIF, "", "", UnivFmt.CONTEXT_STRUCTURE);%>
                    <input type="button" name="AJOUTER_CODE_STRUCTURE_SEULE" value="<%= module.getMessage("BO_AJOUTER") %>" onclick="soumettreAjoutStructureSeule( ); return false" />
                </div>
            </fieldset><%
        }
        %></div><!-- #perimetre-affichage -->
    </div><!-- #content -->
</form>
