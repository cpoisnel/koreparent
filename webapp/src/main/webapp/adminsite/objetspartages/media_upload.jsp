<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Enumeration" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />
<%
    Integer nbUpload = infoBean.get("NB_UPLOAD") != null ? infoBean.getInt("NB_UPLOAD") : null;
    Integer nbErrors = infoBean.get("NB_ERROR") != null ? infoBean.getInt("NB_ERROR") : null;
%>
<form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post"><%
    if(StringUtils.isBlank(infoBean.getString("MSG_ERROR"))) {
    %><div id="actions">
        <div>
            <ul>
                <li><button class="enregistrer" type="submit" name="ENREGISTRER"  value="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.LANCER") %>" onclick="if(!checkFileExt('ZIP')){return false;}"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.LANCER") %></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions --><%
        }
    %><br />
    <div id="content" role="main">

        <% fmt.insererVariablesCachees( out, infoBean); %>
        <input type="hidden" name="GRS_SAUVEGARDE_CODE_RUBRIQUE" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RUBRIQUE") %>">
        <input type="hidden" name="GRS_SAUVEGARDE_CODE_RATTACHEMENT" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RATTACHEMENT") %>">
        <input type="hidden" name="ACTION" value="">

        <!-- Utile pour la restauration des données -->
        <% if (infoBean.get("ID_CI_RESTAURATION") != null) { %>
            <input type="hidden" name="ID_CI_RESTAURATION" value="<%= infoBean.getString("ID_CI_RESTAURATION")%>" />
            <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%= infoBean.getString("NOM_JSP_RESTAURATION")%>" />
        <% } %>

        <%if (infoBean.get("MSG_ERROR")!=null) { %>
            <div class="content_popup">
                <%
                String uploadLabel = nbUpload > 1 ? "MEDIATHEQUE.MEDIA.IMPORT.RESSOURCES_UPLOADES" : "MEDIATHEQUE.MEDIA.IMPORT.RESSOURCE_UPLOADE";
                String errorLabel = nbErrors > 1 ? "MEDIATHEQUE.MEDIA.IMPORT.ERRORS" : "MEDIATHEQUE.MEDIA.IMPORT.ERROR";
                %>
                <p>
                    <%= String.format(MessageHelper.getCoreMessage(uploadLabel), nbUpload) %>
                </p>
                <p>
                    <%= String.format(MessageHelper.getCoreMessage(errorLabel), nbErrors) %>
                </p>
                <% if (StringUtils.isNotBlank(infoBean.getString("MSG_ERROR"))) { %>
                    <br />
                    <%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.SUMSUP") %> :
                    <br/>
                    <%= infoBean.getString("MSG_ERROR") %>
                <% } %>
            </div>
        <% } else { %>
            <div class="content_popup">
                <div class="fieldset neutre">
                    <p>
                        <label for="ZIP" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.ARCHIVE") %></label>
                        <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "ZIP", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_FICHIER, 10, 100, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.ARCHIVE"));%>
                    </p>
                    <p>
                        <span class="retrait"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.FTP") %></span>
                    </p>
                    <p>
                        <label for="TYPE_RESSOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.TYPE_RESSOURCES") %></label>
                        <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_RESSOURCE", FormateurJSP.SAISIE_FACULTATIF,"LISTE_TYPE_RESSOURCES", MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.TYPE_RESSOURCES"), "");%>
                    </p>
                    <%
                    Hashtable hType = (Hashtable) infoBean.get("LISTE_TYPE_RESSOURCES");
                    Enumeration<String> en = hType.keys();
                    String sKeyType = "";
                    int iRessource = -1;
                    while (en.hasMoreElements()){
                        iRessource++;
                        sKeyType = en.nextElement();%>
                        <p data-type="<%=sKeyType.toLowerCase()%>" style="display:none;">
                            <label for="TYPE_MEDIA_<%=sKeyType.toUpperCase()%>" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.TYPE_MEDIA") %> <%=((String)hType.get(sKeyType)).toLowerCase()%></label>
                            <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_MEDIA_"+sKeyType.toUpperCase(), FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_MEDIA_"+sKeyType.toUpperCase(), "", "");%>
                        </p>
                    <%
                    }
                    %>
                    <p>
                        <label for="THEMATIQUE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.THEMATIQUE") %></label>
                        <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "THEMATIQUE", FormateurJSP.SAISIE_FACULTATIF,"LISTE_THEMATIQUES", MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.IMPORT.THEMATIQUE"), "");%>
                    </p>
                    <% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE); %>
                    <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE);%>
                    <p>
                        <label for="DESCRIPTION" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_DESCRIPTION_MEDIA")%></label>
                        <%fmt.insererChampSaisie( out, infoBean, "DESCRIPTION", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_DESCRIPTION_MEDIA") + ",COLS=37,ROWS=2");%>
                    </p>
                    <p>
                        <label for="AUTEUR" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_AUTEUR_MEDIA")%></label>
                        <%fmt.insererChampSaisie( out, infoBean, "AUTEUR", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_AUTEUR_MEDIA") + ",COLS=37,ROWS=2");%>
                    </p>
                    <p>
                        <label for="COPYRIGHT" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_COPYRIGHT_MEDIA")%></label>
                        <%fmt.insererChampSaisie( out, infoBean, "COPYRIGHT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0,    1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_COPYRIGHT_MEDIA") + ",COLS=37,ROWS=2");%>
                    </p>
                    <p>
                        <label for="META_KEYWORDS" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_META_KEYWORDS_MEDIA")%></label>
                        <%fmt.insererChampSaisie( out, infoBean, "META_KEYWORDS", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_META_KEYWORDS_MEDIA") + ",COLS=37,ROWS=2");%>
                    </p>
                </div><!-- .fieldset.neutre -->
            <% } %>
        </div><!-- .content_popup -->
    </div><!-- #content -->
</form>
<script type="text/javascript">
    document.getElementById('TYPE_RESSOURCE').onchange = function(){
        var $ = jQuery;
        $('p[data-type]').css({'display' : 'none'});
        $('p[data-type="' + this.value + '"]').css({'display' : 'block'});
    }
</script>