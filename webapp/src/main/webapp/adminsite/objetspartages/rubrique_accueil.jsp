<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.jsbsoft.jtf.session.SessionUtilisateur"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@page import="com.univ.datagrid.utils.DatagridUtils"%>
<%@page import="com.univ.objetspartages.om.AutorisationBean"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.EscapeString"%>
<%@page import=" com.univ.utils.SessionUtil"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
ContexteUniv ctx = ContexteUtil.getContexteUniv();
AutorisationBean autorisations = ctx.getAutorisation();
//si l'utilisateur a perdu sa session, ou bien qu'il n'a pas les droits sur l'onglet administration, on le redirige vers le login
//si on a pas de controle des permissions, on a pas besoin de rediriger (par exemple formulaire de recherche ou saisie front anonyme)
if ((SessionUtil.getInfosSession(request).get(SessionUtilisateur.CODE) == null || autorisations == null)) {
    request.getRequestDispatcher("/adminsite/").forward(request, response);
}
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
%>
<div id="content" role="main">
    <%
    String codeValue = "";
    if(infoBean.get("CODE") != null){
        codeValue = infoBean.get("CODE").toString();
    }
    %>
    <div id="treeView" class="view">
        <jsp:include page="/adminsite/tree/tree.jsp">
            <jsp:param name="SELECTED" value="<%=codeValue%>" />
            <jsp:param name="JSTREEBEAN" value="rubriqueJsTreeProcessus" />
            <jsp:param name="DND" value="true" />
            <jsp:param name="VIEW_SWITCHER" value="true" />
            <jsp:param name="ACTIONS" value="true" />
        </jsp:include>
    </div>
    <div id="listView" class="view" style="display: none;">
        <table class="datatableViews"
            data-search="<%=EscapeString.escapeAttributHtml(DatagridUtils.getUrlTraitementDatagrid(infoBean))%>">
            <thead>
                <tr>
                    <th> <%=module.getMessage("BO_INTITULE") %> </th>
                    <th> <%=module.getMessage("BO_LIBELLE_RUBRIQUE_MERE") %> </th>
                    <th> <%=module.getMessage("BO_LIBELLE_CODE") %> </th>
                    <th> <%=module.getMessage("BO_LIBELLE_LANGUE") %> </th>
                    <th class="sanstri sansfiltre"> <%=module.getMessage("BO_ACTIONS") %> </th>
                </tr>
            <thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script>
    function toggleView(element){
        var $ = jQuery,
            $li = $(element),
            $views = $('.view'),
            filterValue = $views.filter(':visible').find('input[type="text"]').val();

        $views.css({display: 'none'});
        $('#' + $li.data('targetid')).find('input[type="text"]').val(filterValue).keyup();
        $('#' + $li.data('targetid')).css({display: 'block'});
    }

    function specificTreeSelect(event, node){
        window.location.href='/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_RUBRIQUE&ACTION=MODIFIERRUBRIQUEPARCODE&CODE_RUBRIQUE=' + node.data('sCode');
    }

    // Animation de suppression de l'élément : transition({'min-height' : 0, 'max-height' : 0, scale : 0, opacity: 1});
    function specificTreeRemove(event, node, tree){
        var $ = jQuery,
            $messageApplicatif = $('#messageApplicatif'),
            $p = $('<p>').addClass('message alert fade in'),
            $closeButton = $('<button>').addClass('close').attr({ 'type' : 'button', 'data-dismiss' : 'alert', 'aria-hidden' : true}).html('&times;');
        $.ajax({
          type: "POST",
          url: '/servlet/com.kportal.servlet.JsTreeServlet',
          data: { 'JSTREEBEAN': 'rubriquesJsTree', 'ACTION': 'SUPPRIMER', 'CODES_RUBRIQUES': node.data('sCode') },
          success: function(data, status){
              $p.addClass('alert-success');
              $p.html(data);
              $p.appendTo($messageApplicatif);
              $closeButton.appendTo($p);
              $p.alert();
              node.transition({opacity: 0}, function(){
                  tree.delete_node(node);
              });
          },
          error: function(jqXHR, status, error){
              $p.addClass('alert-danger');
              $p.html(jqXHR.responseText);
              $p.appendTo($messageApplicatif);
              $closeButton.appendTo($p);
              $p.alert();
          }
        });
    }
</script>