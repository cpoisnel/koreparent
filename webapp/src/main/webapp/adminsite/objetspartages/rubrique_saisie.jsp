<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.collections.MapUtils"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.core.InfoBean"%>
<%@page import="com.jsbsoft.jtf.core.LangueUtil"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.core.config.PropertyHelper"%>
<%@page import="com.kportal.core.webapp.WebAppUtil"%>
<%@page import="com.kportal.extension.module.plugin.rubrique.IPageAccueilRubrique"%>
<%@page import="com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueHelper"%>
<%@page import="com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager"%>
<%@page import="com.univ.datagrid.utils.DatagridUtils"%>
<%@page import="com.univ.objetspartages.om.AutorisationBean"%>
<%@page import="com.univ.objetspartages.om.Perimetre"%>
<%@page import="com.univ.objetspartages.om.PermissionBean"%>
<%@page import="com.univ.url.UrlManager"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@ page import="com.univ.utils.EscapeString" %>
<%@ page import="com.univ.utils.FicheUnivHelper" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@ page  errorPage="/adminsite/jsbexception.jsp" %>
<%@taglib prefix="components" uri="http://kportal.kosmos.fr/tags/components" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
ContexteUniv ctx = ContexteUtil.getContexteUniv();
AutorisationBean autorisation = ctx.getAutorisation();
fmt.insererVariablesCachees( out, infoBean);
boolean activationDSI = "1".equals(PropertyHelper.getCoreProperty("dsi.activation"));
%><form action="<%= WebAppUtil.SG_PATH%>" enctype="multipart/form-data" method="post">
    <div id="actions">
        <div>
            <ul>
                <%
                    if ((!infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION))&&(autorisation.possedePermission(new PermissionBean ("TECH", "rub", "S"), new Perimetre("*", infoBean.getString("CODE"), "*", "*", "")))) {
                %>
                    <li><button class="supprimer" class="bouton" type="button" name="SUPPRESSION" value="<%= MessageHelper.getCoreMessage("BO_SUPPRIMER") %>" onclick="soumettreSuppression(); return false;"><%= MessageHelper.getCoreMessage("BO_SUPPRIMER") %></button></li>
                <%
                    }
                %>

                <li><button class="enregistrer" class="bouton" type="button" name="VALIDER" value="<%= MessageHelper.getCoreMessage("BO_ENREGISTRER") %>" onclick="soumettreFormulaire(); return false"><%= MessageHelper.getCoreMessage("BO_ENREGISTRER") %></button></li>
                <%
                    if (infoBean.get("URL_APERCU")!=null){
                %>
                    <li id="page-apercu" class="retrait">
                        <button class="apercu" type="button" onclick="openRub('<%=infoBean.getString("URL_APERCU")%>'); return false;" ><%= MessageHelper.getCoreMessage("BO_PREVISUALISER") %></button>
                    </li>
                <%
                    }
                %>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->

    <ul id="onglets">
        <li id="li-general" onclick="activeOnglet(0);"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_COMPLEMENTS")%></li><%
        if (!"AJOUTER".equals(infoBean.getActionUtilisateur())) {
            %><li id="li-sousrubrique" onclick="activeOnglet(1);"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_SOUS_RUBRIQUES")%></li><%
        }
         %><li id="li-personnalisation" onclick="activeOnglet(2);"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_PERSONNALISATION")%></li>
        <li id="li-encadre" onclick="activeOnglet(3);"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_ENCADRES")%></li><%
        if (!"AJOUTER".equals(infoBean.getActionUtilisateur())) {
            %><li id="li-contenu" onclick="activeOnglet(4);"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_CONTENU")%></li><%
        }
        if (activationDSI) {
            %><li id="li-diffusion" onclick="activeOnglet(5);"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_RESTRICTIONS_ACCES")%></li><%
        }
    %></ul>

    <div id="content" role="main">
        <input type="hidden" name="ACTION" value="" />
        <input type="hidden" name="ANCRE" value="" />
        <input type="hidden" name="FINAL" value="" />
        <input type="hidden" name="FCK_EDITORS_NAMES" value="" />
        <%
            if (infoBean.get("URL_APERCU")!=null){
        %>
            <input type="hidden" name="URL_APERCU" value="<%=infoBean.getString("URL_APERCU")%>" />
        <%
            }
        %>
        <%
            fmt.insererVariablesCachees( out, infoBean);
        %>

        <div id="div-general" style="display:none;">
            <fieldset>
                <legend><%= MessageHelper.getCoreMessage("BO_ONGLET_CONTENU_PRINCIPAL")%></legend>
                <p>
                    <label for="CODE" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_CODE") %> (*)</label>
                <% if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) { %>
                    <input type="hidden" name="CODE" value="<%=infoBean.get("CODE")%>" /><%=infoBean.get("CODE")%>
                <% } else { %>
                    <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, "CODE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 20, MessageHelper.getCoreMessage("BO_RUBRIQUE_CODE")); %>
                <% } %>
                </p>
                <p>
                    <label for="LANGUE" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_LANGUE") %> (*)</label>
                    <%fmt.insererComboHashtable(out, infoBean, "LANGUE", FormateurJSP.SAISIE_OBLIGATOIRE, "LISTE_LANGUES");%>
                </p>
                <p>
                    <label for="INTITULE" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_INTITULE") %> (*)</label>
                    <%fmt.insererChampSaisie(out, infoBean, "INTITULE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255,"LIB=" + MessageHelper.getCoreMessage("BO_RUBRIQUE_INTITULE"));%>
                </p>
                <%univFmt.insererComboHashtable(fmt, out, infoBean, "CATEGORIE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_CATEGORIES", MessageHelper.getCoreMessage("BO_RUBRIQUE_CATEGORIE"));%>
                <p>
                    <label for="NOM_ONGLET" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_INTITULE_FIL_ARIANE") %> (*)</label>
                    <%fmt.insererChampSaisie( out, infoBean, "NOM_ONGLET", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 128, "LIB=" + MessageHelper.getCoreMessage("BO_RUBRIQUE_INTITULE_FIL_ARIANE"));%>
                </p>
                <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE_MERE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_RUBRIQUE"), "rubrique", univFmt.CONTEXT_ZONE);%>
            </fieldset>
            <fieldset>
                <legend><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_ACCUEIL_RUBRIQUE") %></legend><%
                    univFmt.insererComboHashtable(fmt, out, infoBean, "TYPE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_RUBRIQUE", MessageHelper.getCoreMessage("BO_RUBRIQUE_ACCUEIL"));
                    Map<String, IPageAccueilRubrique> mapTypeRubrique = PageAccueilRubriqueManager.getInstance().getTypesRubrique();
                    for (IPageAccueilRubrique pageAccueil : mapTypeRubrique.values()) {
                        if (StringUtils.isNotEmpty(pageAccueil.getPathSaisieBO())){
                            %><div id="page-accueil-<%=pageAccueil.getTypeRubrique()%>" class="page-accueil" style="display:none;"><%
                                PageAccueilRubriqueHelper.includeJspBO(out, getServletConfig().getServletContext(), request, response, pageAccueil);
                            %></div><%
                        }
                    }
                %>
            </fieldset>
        </div><!-- #div-general --><%
        if (!"AJOUTER".equals(infoBean.getActionUtilisateur())) {
            %><div id="div-sousrubrique" style="display:none;">
                <a name="sousrubrique"></a>
                <fieldset>
                    <legend><%= MessageHelper.getCoreMessage("BO_LISTE") %></legend>
                    <!-- TODO : Tableau ? -->
                    <table class="resultat">
                    <%
                    int nbRub = infoBean.getInt("RUBRIQUES_FILLES_NB_ITEMS");
                    if(nbRub == 0) {
                    %>
                        <tr>
                            <td>
                                <b><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_AUCUNE_RUBRIQUE_FILLE") %></b>
                            </td>
                        </tr>
                    <% }else{
                            for (int i=0; i< nbRub; i++) { %>
                                <tr>
                                    <td>
                                        <% if( i > 0)    {    %>
                                            <img src="/adminsite/images/fhaut.gif" alt="<%= MessageHelper.getCoreMessage("BO_MONTER") %>" onclick="soumettreRemonterRubriqueFille ( <%=i%>)" />
                                        <%    }    %>
                                    </td>
                                    <td>
                                        <% if( i< nbRub -1)    {    %>
                                        <img src="/adminsite/images/fbas.gif" alt="<%= MessageHelper.getCoreMessage("BO_DESCENDRE") %>" onclick="soumettreBaisserRubriqueFille ( <%=i%>)" />
                                        <%    }    %>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td><%= infoBean.getString("RUBRIQUE_FILLE_INTITULE#"+i)%></td>
                                    <td><%= infoBean.getString("RUBRIQUE_FILLE_PAGE#"+i)%></td>
                                </tr>
                            <% }
                        }%>
                    </table>
                    <input type=button value="<%= MessageHelper.getCoreMessage("BO_RUBRIQUE_AJOUT_SOUS_RUBRIQUE") %>" onclick="addSubRubrique(); return false;"/>
                </fieldset>
            </div><!-- #div-sousrubrique --><%
        }
        %><div id="div-personnalisation" style="display:none;">
            <fieldset>
                <legend><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_AFFICHAGE") %></legend>
                <components:toolbox fieldName="ACCROCHE" label="BO_RUBRIQUE_ACCROCHE" editOption="<%= fmt.SAISIE_FACULTATIF %>" min="0" max="1024" />
                <p>
                    <label for="ID_PICTO" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_PICTO") %></label>
                    <%fmt.insererComboHashtable( out, infoBean, "ID_PICTO", FormateurJSP.SAISIE_FACULTATIF , "LISTE_PICTOS" ); %>
                </p>
                <p>
                    <label for="ID_BANDEAU" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_BANDEAU") %></label>
                    <%fmt.insererComboHashtable( out, infoBean, "ID_BANDEAU", FormateurJSP.SAISIE_FACULTATIF , "LISTE_BANDEAUX" ); %>
                </p>
                <p>
                    <label for="COULEUR_TITRE" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_COULEUR_1") %></label>
                    <%fmt.insererChampSaisie( out, infoBean, "COULEUR_TITRE", FormateurJSP.SAISIE_FACULTATIF , FormateurJSP.FORMAT_TEXTE, 0, 20); %>
                </p>
                <p>
                    <label for="COULEUR_FOND" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_COULEUR_2") %></label>
                    <%fmt.insererChampSaisie( out, infoBean, "COULEUR_FOND", FormateurJSP.SAISIE_FACULTATIF , FormateurJSP.FORMAT_TEXTE, 0, 20); %>
                </p>
            </fieldset>
            <fieldset>
                <legend>Contact</legend>
                <p>
                    <label for="CONTACT" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_CONTACT") %></label>
                    <%fmt.insererChampSaisie(out, infoBean, "CONTACT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255); %>
                </p>
            </fieldset>
        </div><!-- #div-personnalisation -->

        <div id="div-encadre" style="display:none;">
            <fieldset>
                <legend><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_CONTENU") %></legend>
                <components:toolbox fieldName="ENCADRE" label="BO_ONGLET_ENCADRE" editOption="<%= fmt.SAISIE_FACULTATIF %>" min="0" max="65535" />
                <div>
                    <span class="label colonne"><%=MessageHelper.getCoreMessage("BO_ENCADRE_AUTO")%></span>
                    <ul class="en_ligne"><!--
                        --><li>
                            <% fmt.insererChampSaisie( out, infoBean, "ENCADRE_SOUS_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0,1); %>
                            <label for="ENCADRE_SOUS_RUBRIQUE">
                                <%=MessageHelper.getCoreMessage("RUBRIQUE.APPLIQUER_SOUS_RUBRIQUE")%>
                            </label>
                        </li><!--
                        --><li>
                            <% fmt.insererChampSaisie( out, infoBean, "GESTION_ENCADRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0,1); %>
                            <label for="GESTION_ENCADRE">
                                <%=MessageHelper.getCoreMessage("RUBRIQUE.AFFICHER_ENCADRE_AUTO")%>
                            </label>
                        </li><!--
                     --></ul>
                </div>
            </fieldset>
        </div><!-- #div-encadre --><%
        if (!"AJOUTER".equals(infoBean.getActionUtilisateur())) {
            %><div id="div-contenu" style="display:none;">
                <fieldset>
                    <legend><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_FICHES_RATTACHEES_PRINCIPAL") %></legend>
                    <a name="contenu-fiches-rat"></a>
                        <table class="datatableMultiFicheRubrique"
                               data-search="<%=EscapeString.escapeAttributHtml(DatagridUtils.getUrlTraitementDatagrid("CODE_RUBRIQUE=" +  EscapeString.escapeURL(infoBean.getString("CODE")) + "&BEAN_RECHERCHE=rattachementPrincipalDatagrid"))%>">
                            <thead>
                                <tr>
                                    <th><%= MessageHelper.getCoreMessage("BO_INTITULE") %></th>
                                    <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_TYPE") %></th>
                                    <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_LANGUE")%></th>
                                    <th><%= MessageHelper.getCoreMessage("BO_ETAT") %></th>
                                    <th><%= MessageHelper.getCoreMessage("BO_MODIFICATION") %></th>
                                    <th class="sanstri sansfiltre"><%= MessageHelper.getCoreMessage("BO_ACTIONS") %></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                </fieldset>
                <fieldset>
                    <legend><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_FICHES_DIFFUSEES_SECONDAIRE") %></legend>
                    <a name="contenu-fiches-ref"></a>
                    <h3 class="titre_tableau"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_RATTACHEMENTS_MANUELS") %></h3>
                    <table class="datatableRubrique">
                        <thead>
                            <tr>
                                <th><%= MessageHelper.getCoreMessage("BO_INTITULE") %></th>
                                <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_TYPE") %></th>
                                <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_LANGUE")%></th>
                                <th><%= MessageHelper.getCoreMessage("BO_ETAT") %></th>
                                <th class="sanstri sansfiltre"><%= MessageHelper.getCoreMessage("BO_ACTIONS") %></th>
                            </tr>
                        </thead>
                        <tbody>
                        <%
                            int nbLignes = infoBean.getInt("FICHES_REFERENCEES_NB_ITEMS");
                            if (nbLignes != 0) {
                                for (int i=0; i < nbLignes; i++) { %>
                                        <tr>
                                            <td><%= infoBean.getString("FICHE_REFERENCEE_INTITULE#"+i) %></td>
                                            <td><%= infoBean.getString("FICHE_REFERENCEE_TYPE_LIBELLE#"+i)%></td>
                                            <td><img class="drapeau" alt="<%= LangueUtil.getDisplayName(infoBean.getString("FICHE_REFERENCEE_LANGUE#"+i)) %>" title="<%= LangueUtil.getDisplayName(infoBean.getString("FICHE_REFERENCEE_LANGUE#"+i)) %>" src="<%= LangueUtil.getPathImageDrapeau(infoBean.getString("FICHE_REFERENCEE_LANGUE#"+i)) %>" /></td>
                                            <td><span class="<%= FicheUnivHelper.getClassEtat(infoBean.getString("FICHE_REFERENCEE_ETAT_LIBELLE#"+i))%>"><%= infoBean.getString("FICHE_REFERENCEE_ETAT_LIBELLE#"+i) %></span></td>
                                            <td>
                                                <%= infoBean.getString("FICHE_REFERENCEE_URL_MODIF#"+i)%> | <a class="action" href="#" name="APERCU_ACCUEIL#<%=i%>"
                                                onclick="openRub('<%=UrlManager.calculerUrlApercu(infoBean.get("FICHE_REFERENCEE_ID_META#"+i)+"")%>','apercu');return false;"
                                                ><%= MessageHelper.getCoreMessage("BO_VOIR_EN_LIGNE") %></a> | <a class="action supprimer" onclick="soumettreSuppressionFicheReferencee(event,<%= i%>)" href="#" data-confirmation="<%= MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_RATTACHEMENT_SECONDAIRE")%>">Supprimer</a>
                                            </td>
                                        </tr>
                                <% } %>
                        <% } %>
                        </tbody>
                    </table>

                    <% if ("1".equals(infoBean.get("GRS_SAISIE_PUBLICATION"))) { %>
                        <p>
                            <input type="button" name="AJOUTER_FICHE_REFERENCEE" value="<%= MessageHelper.getCoreMessage("BO_RUBRIQUE_AJOUTER_FICHE") %>" onclick="soumettreAjoutFicheReferencee(); return false" />
                        </p>
                    <% } %>
                    <a name="contenu-requete"></a>
                    <h3 class="titre_tableau"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_FICHES_RATTACHEES_AUTOMATIQUE") %></h3>
                    <table class="datatableRubrique">
                        <thead>
                            <tr>
                                <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_TYPE") %></th>
                                <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_STRUCTURE") %></th>
                                <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_RUBRIQUE")%></th>
                                <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_LANGUE")%></th>
                                <th class="sanstri sansfiltre"><%= MessageHelper.getCoreMessage("BO_ACTIONS") %></th>
                            </tr>
                        </thead>
                        <tbody>
                        <%
                        nbLignes = infoBean.getInt("REQUETE_NB_ITEMS");
                        if(nbLignes != 0){
                            for (int i=0; i< nbLignes; i++) { %>
                                    <tr>
                                       <td><%= infoBean.getString( "REQUETE_OBJET#"+i) %></td>
                                        <td><%= infoBean.getString( "REQUETE_STRUCTURE#"+i) %></td>
                                        <td><%= infoBean.getString( "REQUETE_RUBRIQUE#"+i) %></td>
                                        <td><img class="drapeau" alt="<%= LangueUtil.getDisplayName(infoBean.getString("REQUETE_LANGUE#"+i)) %>" title="<%= LangueUtil.getDisplayName(infoBean.getString("REQUETE_LANGUE#"+i)) %>" src="<%= LangueUtil.getPathImageDrapeau(infoBean.getString("REQUETE_LANGUE#"+i)) %>" /></td>
                                        <td><a class="action supprimer" href="#" name="SUPPRIMER_REQUETE#<%=i%>" onclick="soumettreSuppressionRequete(event, <%=i%> ); return false;" data-confirmation="<%= MessageHelper.getCoreMessage("CONFIRMATION_SUPPRESSION_REQUETE_AUTO")%>"><%=MessageHelper.getCoreMessage("BO_SUPPRIMER") %></a></td>
                                    </tr>
                            <% } %>
                        <% } %>
                        </tbody>
                    </table>
                    <% if ("1".equals(infoBean.get("GRS_SAISIE_PUBLICATION"))) { %>
                        <p>
                            <input type="button" name="AJOUTER_FICHE_REFERENCEE" value="<%= MessageHelper.getCoreMessage("BO_RUBRIQUE_AJOUTER_REQUETE") %>" onclick="soumettreAjoutRequete(); return false" />
                        </p>
                    <% }%>
                </fieldset>
            </div><!-- #div-contenu --><%
        }
        %><div id="div-diffusion" style="display:none;">
                <div class="fieldset neutre"><%
                if ("1".equals(infoBean.getString("GRS_SAISIE_DIFFUSION"))) {
                    %><h3><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_RUB_COURANTE") %></h3>
                    <p><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_ACCES_RESTRIENT_GROUPE") %></p>
                    <p>
                        <%univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "GROUPES_DSI", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_GROUPES"), "TMP_GROUPES_DSI", MessageHelper.getCoreMessage("BO_RUBRIQUE_GROUPE"), UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION, ""); %>
                    </p><%
                } else {
                    %><p><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_PAS_DROIT_RESTRICTION") %></p><%
                }
                Map<String,Collection<String>> restrictionsParRubriques = infoBean.getMap("GROUPES_DSI_RESTRICTION");
                if (MapUtils.isNotEmpty(restrictionsParRubriques)) {
                    %>
                    <h3><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_RUB_PARENTE") %></h3>
                    <p><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_RUB_PARENTE_RESTRICTION") %></p>
                    <table class="publicsVises__restrictions">
                        <colgroup>
                            <col id="publicsVises__restrictions--rubrique"/>
                            <col id="publicsVises__restrictions--groupes" />
                        </colgroup>
                        <tr>
                            <th><%= MessageHelper.getCoreMessage("BO_RUBRIQUE") %></th>
                            <th><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_RESTRICTION_GROUPE") %></th>
                        </tr><%
                        int increment = 0;
                        List<String> keys = new ArrayList<String>(restrictionsParRubriques.keySet());
                        for (int i = keys.size() -1; i >= 0; i--) {
                            %><tr>
                                <td><span class="retrait_<%= increment %>">&bullet;</span> <%= keys.get(i) %></td>
                                <td><%= StringUtils.join(restrictionsParRubriques.get(keys.get(i)), ", ") %></td>
                            </tr><%
                            increment++;
                        }
                    %></table><%
                }
                %></div><!-- .fieldset -->
        </div> <!-- #div-diffusion -->

        <script type="text/javascript">

            function addSubRubrique(){
                var newWindow = window.open("/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_RUBRIQUE&ACTION=AJOUTER&CODE_RUBRIQUE_MERE=<%=infoBean.get("CODE")%>");
                newWindow.focus();
            }

            function openRub(url, name) {
                window.open(url, name, "");
            }

            function soumettreSuppressionRequete(event,i) {
                var that = jQuery(event.target);
                event.preventDefault();
                jQuery('<div></div>')
                .html( that.data('confirmation'))
                .dialog({
                    buttons: [{
                        text: LOCALE_BO.ok,
                        click: function() {
                            jQuery(this).dialog('close');
                            window.document.forms[0].ACTION.value="SUPPRIMER_REQUETE#" + i;
                            window.document.forms[0].submit();
                        }
                    },{
                        text: LOCALE_BO.fermer,
                        click: function() {
                            jQuery(this).dialog('close');
                        }
                    }],
                    title: LOCALE_BO.confirmer
                });
            }

            function soumettreAjoutRequete() {
                window.document.forms[0].ACTION.value="AJOUTER_REQUETE";
                window.document.forms[0].submit();
            }

            function soumettreRemonterRubriquePrincipale(i) {
                window.document.forms[0].ACTION.value="REMONTER_RUBRIQUE_PRINCIPALE#" + i;
                window.document.forms[0].submit();
            }

            function soumettreBaisserRubriquePrincipale(i) {
                window.document.forms[0].ACTION.value="BAISSER_RUBRIQUE_PRINCIPALE#" + i;
                window.document.forms[0].submit();
            }

            function soumettreRemonterRubriqueFille(i) {
                window.document.forms[0].ACTION.value="REMONTER_RUBRIQUE_FILLE#" + i;
                window.document.forms[0].submit();
            }

            function soumettreBaisserRubriqueFille(i) {
                window.document.forms[0].ACTION.value="BAISSER_RUBRIQUE_FILLE#" + i;
                window.document.forms[0].submit();
            }

            function soumettreSuppressionFicheReferencee(event, i) {
                var that = jQuery(event.target);
                event.preventDefault();
                jQuery('<div></div>')
                .html( that.data('confirmation'))
                .dialog({
                    buttons: [{
                        text: LOCALE_BO.ok,
                        click: function() {
                            jQuery(this).dialog('close');
                            window.document.forms[0].ACTION.value="SUPPRIMER_FICHE_REFERENCEE#" + i;
                            window.document.forms[0].submit();
                        }
                    },{
                        text: LOCALE_BO.fermer,
                        click: function() {
                            jQuery(this).dialog('close');
                        }
                    }],
                    title: LOCALE_BO.confirmer
                });
            }

            function soumettreAjoutFicheReferencee ( ) {
                window.document.forms[0].ACTION.value="AJOUTER_FICHE_REFERENCEE";
                window.document.forms[0].submit();
            }

            function soumettreSuppression() {
                window.document.forms[0].ACTION.value="SUPPRIMER";
                window.document.forms[0].submit();
            }

            function soumettreFormulaire() {
                var $= jQuery;
                if($(window.document.forms[0]).valid()){
                    window.document.forms[0].ACTION.value="VALIDER";
                    window.document.forms[0].submit();
                }
            }

            function soumettreFormulaireFinal() {
                window.document.forms[0].ACTION.value="VALIDER";
                window.document.forms[0].FINAL.value="true";
                window.document.forms[0].submit();
            }

            // JSS 20040419 : il vaut mieux gerer le rafraichissement
            // de facon standard
            function rafraichir() {
                window.document.forms[0].ACTION.value="RAFRAICHIR";
                window.document.forms[0].submit();
            }

            function soumettreAbandon() {
                window.document.forms[0].ACTION.value="ABANDON";
                window.document.forms[0].submit();
            }

            var erreurDSI = '';
            var arrayOnglets = [];
            arrayOnglets[0]='general';
            arrayOnglets[1]='sousrubrique';
            arrayOnglets[2]='personnalisation';
            arrayOnglets[3]='encadre';
            arrayOnglets[4]='contenu';
            arrayOnglets[5]='diffusion';
            arrayOnglets[6]='rubrique';

            function activeOnglet(indiceOnglet, ancre) {
                var nomOnglet = '';
                for (i=0; i<arrayOnglets.length;i++)
                {
                    nomOnglet = arrayOnglets[i];

                    if (i == indiceOnglet) // onglet selectionne
                    {
                        if (window.document.getElementById('li-'+nomOnglet)) {
                            window.document.getElementById('li-'+nomOnglet).className = 'active';
                        }

                        if (window.document.getElementById('div-'+nomOnglet)) {
                            window.document.getElementById('div-'+nomOnglet).style.display = 'block';
                        }
                    }
                    else
                    {
                        if (window.document.getElementById('li-'+nomOnglet)) {
                            window.document.getElementById('li-'+nomOnglet).className = '';
                        }

                        if (window.document.getElementById('div-'+nomOnglet)) {
                            window.document.getElementById('div-'+nomOnglet).style.display = 'none';
                        }
                    }
                }
                if (indiceOnglet == 0)
                     displayFieldPageTete();
                if (ancre)
                    window.location.href = '#' + ancre;
            }

            function displayFieldPageTete()    {
                //on recuperer la valeur selectionnee
                oTypeRubrique = document.forms[0].TYPE_RUBRIQUE;
                divs = window.document.getElementsByClassName("page-accueil");
                for (var i = 0; i < divs.length; i++) {
                    divs[i].style.display = 'none';
                    inputs = divs[i].getElementsByTagName('input');
                    for(var j=0; j<inputs.length;j++)
                    {
                        if (inputs[j].getAttribute('required')){
                            inputs[j].removeAttribute('required');
                        }
                    }
                    selects = divs[i].getElementsByTagName('select');
                    for(var j=0; j<selects.length;j++)
                    {
                        if (selects[j].getAttribute('required')){
                            selects[j].removeAttribute('required');
                        }
                    }
                }
                oUrl = document.forms[0].URL_APERCU;
                if (oUrl && oUrl.value.length>0){
                    oUrl.value='';
                }
                else if (window.document.getElementById('page-apercu')) {
                    window.document.getElementById('page-apercu').style.display = 'none';
                }
                
                div = window.document.getElementById('page-accueil-'+oTypeRubrique.value);
                if (div){
                    div.style.display = 'inline';
                    inputs = div.getElementsByTagName('input');
                    for(var i=0; i<inputs.length;i++)
                    {
                        if (inputs[i].getAttribute('data-required')){
                            inputs[i].setAttribute('required',true);
                        }
                    }
                    selects = div.getElementsByTagName('select');
                    for(var i=0; i<selects.length;i++)
                    {
                           if (selects[i].getAttribute('data-required')){
                               selects[i].setAttribute('required',true);
                        }
                    }
                }
            }

            function atteindreAncre() {
                ancre = '<%= infoBean.get("ANCRE")%>';
                if (ancre != 'null' && ancre != '')
                {
                    var breaked = false;
                    for (i=0;i<arrayOnglets.length;i++)
                    {
                        if (ancre.indexOf(arrayOnglets[i])==0)
                        {
                             activeOnglet(i,ancre);
                             breaked=true;
                             break;
                        }
                    }
                    if (!breaked)
                        activeOnglet(0);
                }
                else if (erreurDSI == 'true') {
                    activeOnglet(5);
                }
                else {
                    activeOnglet(0);
                }
            }
            atteindreAncre();
            document.forms[0].TYPE_RUBRIQUE.onchange = displayFieldPageTete;
        </script>
    </div><!-- #content -->
</form>