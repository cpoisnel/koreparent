<%-- DEBUT init_recherche.jsp --%>

<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>

<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<%
    String toolbox = StringUtils.defaultString(infoBean.get("TOOLBOX", String.class));
    boolean displayStructure = true;
    boolean displayRubrique = true;
    boolean listeIncluse = request.getParameter("LISTE_INCLUSE") != null && "1".equals(request.getParameter("LISTE_INCLUSE"));
    boolean rssFeed = request.getParameter("RSS_FEED") != null && "1".equals(request.getParameter("RSS_FEED"));
    if (rssFeed){
        listeIncluse = true;
    }
    IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
%>

