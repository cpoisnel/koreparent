<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.LangueUtil" %>
<%@ page import="com.kportal.cms.objetspartages.ObjetPartageHelper" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.config.PropertyHelper" %>
<%@ page import="com.univ.objetspartages.om.DiffusionSelective" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.univ.objetspartages.util.LabelUtils" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.objetspartages.services.ServiceRubrique" %>
<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<%@ page import="com.univ.objetspartages.bean.RubriqueBean" %>
<%@ page import="com.univ.objetspartages.services.ServiceGroupeDsi" %>
<%@ page import="com.univ.objetspartages.bean.GroupeDsiBean" %>
<%@ page import="com.univ.objetspartages.services.ServiceStructure" %>
<%@ page import="com.univ.objetspartages.om.StructureModele" %>
<%
    final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
String nomObjet = ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET"));
if (rssFeed || toolbox.length() == 0 || toolbox.startsWith("LIEN_INTERNE") || toolbox.equals("LIEN_REQUETE") ) {

    if(listeIncluse){%>
        <%if (!rssFeed){ %>
            <%if("1".equals( PropertyHelper.getProperty(module.getIdExtension(),"fiche."+nomObjet.toUpperCase()+".style_affichage") ) )  {%>
                <fieldset><legend><%= MessageHelper.getCoreMessage("BO_RECHERCHE_STYLE_AFFICHAGE") %></legend>
                    <%univFmt.insererComboHashtable(fmt, out, infoBean, "STYLE", FormateurJSP.SAISIE_FACULTATIF, ObjetPartageHelper.getStylesAffichage(nomObjet), MessageHelper.getCoreMessage("BO_RECHERCHE_FORMAT_AFFICHAGE"));%>
                </fieldset>
            <%}%>
        <%}%>
        <fieldset>
            <legend><%= MessageHelper.getCoreMessage("BO_RECHERCHE_CRITERES_GEN") %></legend>
    <%}

    if(displayRubrique){
        final String codeRubrique = infoBean.get("CODE_RUBRIQUE", String.class);
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(codeRubrique);
        final String label = rubriqueBean != null ? rubriqueBean.getIntitule() : StringUtils.EMPTY;
        %><div>
            <span class="label colonne "><%= MessageHelper.getCoreMessage("BO_RUBRIQUE") %></span>
            <div id="kMonoSelectCODE_RUBRIQUE" class="kmonoselect ui-buttonset" data-value="<%= StringUtils.defaultIfEmpty(label, StringUtils.EMPTY) %>" data-placeholder="" data-editaction="/adminsite/tree/tree.jsp?JSTREEBEAN=rubriquesJsTree&amp;DISPLAY=full&amp;SELECTED={0}&amp;CODE={1}" data-popintitle="LOCALE_BO.popin.title.rubrique.mono" data-popinwidth="530" data-popinvalidate="false" data-title="">
                <input type="hidden" name="#FORMAT_CODE_RUBRIQUE" value="1;1;0;0;LIB=Rubrique;0">
                <input type="hidden" id="CODE_RUBRIQUE" name="CODE_RUBRIQUE" value="<%= StringUtils.defaultIfEmpty(codeRubrique, StringUtils.EMPTY) %>">
                <input type="hidden" name="LIBELLE_CODE_RUBRIQUE" value="<%= StringUtils.defaultIfEmpty(label, StringUtils.EMPTY) %>">
            </div>
        </div><%
//        univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE);
        if (toolbox.equals("LIEN_REQUETE")) { %>
            <p class="retrait">
                <input id="NOARBO_RUBRIQUE" type="checkbox" name="NOARBO_RUBRIQUE" value="1" <%= StringUtils.isNotBlank(infoBean.get("NOARBO_RUBRIQUE", String.class)) ? "checked" : StringUtils.EMPTY %>/>
                <label for="NOARBO_RUBRIQUE"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_NO_ARBO_RUBRIQUE") %></label>
            </p>
    <% }
    }

    if(displayStructure){
        final String codeRattachement = infoBean.get("CODE_RATTACHEMENT", String.class);
        final String labelRattachement = StringUtils.isNotBlank(codeRattachement) ? serviceStructure.getDisplayableLabel(codeRattachement, ContexteUtil.getContexteUniv().getLangue()) : StringUtils.EMPTY;
        %><div>
            <span class="label colonne "><%= MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT") %></span>
            <div id="kMonoSelectCODE_RATTACHEMENT" class="kmonoselect ui-buttonset" data-value="<%= StringUtils.defaultIfEmpty(labelRattachement, StringUtils.EMPTY) %>" data-placeholder="" data-editaction="/adminsite/tree/tree.jsp?JSTREEBEAN=structuresJsTree&DISPLAY=full&SELECTED={0}&CODE={1}&LANGUE=0" data-popintitle="LOCALE_BO.popin.title.structure.mono" data-popinwidth="530" data-popinvalidate="false" data-title="">
                <input type="hidden" name="#FORMAT_CODE_RATTACHEMENT" value="1;1;0;0;LIB=Structure;0">
                <input type="hidden" id="CODE_RATTACHEMENT" name="CODE_RATTACHEMENT" value="<%= StringUtils.defaultIfEmpty(codeRattachement, StringUtils.EMPTY) %>">
                <input type="hidden" name="LIBELLE_CODE_RATTACHEMENT" value="<%= StringUtils.defaultIfEmpty(labelRattachement, StringUtils.EMPTY) %>">
            </div>
        </div><%
//        univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE);
        if (ReferentielObjets.getNombreObjetsTypeStructure()!=0 && toolbox.equals("LIEN_REQUETE")) { %>
            <p class="retrait">
                <input id="NOARBO_CODE_RATTACHEMENT" type="checkbox" name="NOARBO_CODE_RATTACHEMENT" value="1" <%= StringUtils.isNotBlank(infoBean.get("NOARBO_CODE_RATTACHEMENT", String.class)) ? "checked" : StringUtils.EMPTY %>/>
                <label for="NOARBO_CODE_RATTACHEMENT"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_NO_ARBO_STRUCTURE") %></label>
            </p>
        <%
        }
    }
    univFmt.insererComboHashtable(fmt, out, infoBean, "LANGUE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_LANGUES",  MessageHelper.getCoreMessage("ST_TABLEAU_LANGUE"));
    // critere etat uniquement sur une recherche back (non popup)
    if (toolbox.length() == 0) {
        univFmt.insererComboHashtable(fmt, out, infoBean, "ETAT_OBJET", FormateurJSP.SAISIE_FACULTATIF, ReferentielObjets.getEtatsObjet(), MessageHelper.getCoreMessage("ST_TABLEAU_ETAT"));
    }
    //criteres code et redacteur pour les recherches mais pas les requetes
    if (! toolbox.equals("LIEN_REQUETE")) {
        univFmt.insererRechercheUtilisateur(fmt, out, infoBean, "CODE_REDACTEUR", MessageHelper.getCoreMessage("ST_TABLEAU_REDACTEUR"));
    }
    if(listeIncluse){
        univFmt.insererChampSaisie(fmt, out, infoBean, "NOMBRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 5, MessageHelper.getCoreMessage("BO_RECHERCHE_NOMBRE_MAX"));
    %>
    </fieldset>
    <%}%>
<%
}
if (toolbox.equals("LIEN_REQUETE") && !rssFeed) {
    if (ReferentielObjets.instancierFiche(nomObjet.toLowerCase()) instanceof DiffusionSelective) {
        %><fieldset>
            <legend><%= MessageHelper.getCoreMessage("BO_RECHERCHE_DIFFUSION_SELECTIVE") %></legend><%
            final String codeGroup = infoBean.get("GROUPE_DSI", String.class);
            final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
            final String labelGroup = StringUtils.isNotBlank(codeGroup) ? serviceGroupeDsi.getIntitule(codeGroup) : StringUtils.EMPTY;
            %><div>
                <span class="label colonne ">Groupe</span>
                <div id="kMonoSelectGROUPE_DSI" class="kmonoselect ui-buttonset" data-value="<%= StringUtils.defaultIfEmpty(labelGroup, StringUtils.EMPTY) %>" data-placeholder="m" data-editaction="/adminsite/tree/tree.jsp?JSTREEBEAN=groupsJsTree&amp;DISPLAY=full&amp;SELECTED={0}&amp;CODE={1}&amp;PERMISSION=/TECH/dsi//" data-popintitle="LOCALE_BO.popin.title.groupe.mono" data-popinwidth="530" data-popinvalidate="false" data-title="00">
                    <input type="hidden" name="#FORMAT_GROUPE_DSI" value="1;1;0;0;LIB=Groupe;1">
                    <input type="hidden" id="GROUPE_DSI" name="GROUPE_DSI" value="<%= StringUtils.defaultIfEmpty(codeGroup, StringUtils.EMPTY) %>">
                    <input type="hidden" name="LIBELLE_GROUPE_DSI" value="<%= StringUtils.defaultIfEmpty(labelGroup, StringUtils.EMPTY) %>">
                </div>
            </div>
            <table class="saisie mise_en_page" role="presentation">
                <%--<%univFmt.insererkMonoSelect(fmt, out, infoBean, "GROUPE_DSI", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("GROUPEDSI.CODE"), "", UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION);%>--%>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <%= MessageHelper.getCoreMessage("BO_RECHERCHE_OU_GROUPE") %> :
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input id="GROUPE_DSI_COURANT_1" type="radio" name="GROUPE_DSI_COURANT" value="1" <%= StringUtils.isNotBlank(infoBean.get("GROUPE_DSI_COURANT_1", String.class)) ? "checked" : StringUtils.EMPTY %> /><label for="GROUPE_DSI_COURANT_1"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_GROUPE_TOUS") %></label><br/>
                        <input id="GROUPE_DSI_COURANT_2" type="radio" name="GROUPE_DSI_COURANT" value="2" <%= StringUtils.isNotBlank(infoBean.get("GROUPE_DSI_COURANT_2", String.class)) ? "checked" : StringUtils.EMPTY %> /><label for="GROUPE_DSI_COURANT_2"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_CLASSES_GROUPE") %></label><br/>
                        <input id="GROUPE_DSI_COURANT_3" type="radio" name="GROUPE_DSI_COURANT" value="3" <%= StringUtils.isNotBlank(infoBean.get("GROUPE_DSI_COURANT_3", String.class)) ? "checked" : StringUtils.EMPTY %> /><label for="GROUPE_DSI_COURANT_3"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_UNIQUEMENT_GROUPE") %></label>
                        <%
                        infoBean.set("LISTE_TYPES", LabelUtils.getLabelCombo("11", LangueUtil.getDefaultLocale()));
                        fmt.insererComboHashtable(out, infoBean, "TYPE_GROUPE_DSI", FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPES", MessageHelper.getCoreMessage("ST_TABLEAU_TYPE"));
                        %>
                    </td>
                </tr>
            </table>
        </fieldset>
        <script>
            function specificActionOnSelectInTree() {
                window.document.forms[0].GROUPE_DSI_COURANT[0].checked=false;
                window.document.forms[0].GROUPE_DSI_COURANT[1].checked=false;
                //FBO/PCO 2009-03-03 ajout d'un nouveau critére de restriction d'affichage
                window.document.forms[0].GROUPE_DSI_COURANT[2].checked=false;
            }
        </script><%
    }
}
if (toolbox.length() == 0) {
    %><div class="footer_popup"><%
    fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_VALIDER} );
    %></div><%
} else if (toolbox.startsWith("LIEN_INTERNE") || toolbox.equals("MAILTO")) {
    %><div class="footer_popup"><%
        fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_VALIDER} );
    %></div><!-- .footer_popup --><%
} else if (toolbox.equals("LIEN_REQUETE")) {
    if(infoBean.get("FCK_PLUGIN")==null){
        %><div class="footer_popup">
            <input class="bouton" type="button" name="ENREGISTRER" value="<%= MessageHelper.getCoreMessage("BO_RECHERCHE_ENREGISTRER_REQUETE") %>" onclick="sauvegarderRequete()" />
        </div><!-- .footer_popup --><%
    }
    %><script type="text/javascript" src="/adminsite/objetspartages/js/recherche.js"></script>
    <script type="text/javascript">
        // liste des champs pour lien de requete
        var aFieldNames = [];
        <%for (String critere : ObjetPartageHelper.getCriteresRequete(nomObjet,listeIncluse)){%>
        aFieldNames.push('<%=critere%>');
        <%}%>
        // renvoie la requete generee dans le champ toolbox
        function sauvegarderRequete() {
            var code = GetQuery(document.forms[0], aFieldNames);
            window.opener.saveField('<%=nomObjet%>', code, '<%= String.format(MessageHelper.getCoreMessage("BO_RECHERCHE_LISTE_DES"),nomObjet)%>');
        }
    </script>
<% }%>
</form>
</div><!-- #content -->
