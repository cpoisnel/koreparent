<%@page import="java.util.Hashtable"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<div id="content" role="main">
    <form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post" data-no-tooltip>
    <div><% fmt.insererVariablesCachees(out, infoBean); %></div><%
    if (toolbox.length() == 0) {
        if ( infoBean.getString("ETAT_FICHE_ENREGISTREE") != null) {
            %><p class="message confirmation">
                <%=MessageHelper.getCoreMessage("BO_ETAT_FICHE_ENREGISTREE_" + infoBean.getString("ETAT_FICHE_ENREGISTREE")) %>
            </p><%
        }
        %><div class="resume">
            <p id="etat"><%
            if ("OUI".equals(infoBean.get("RECHERCHE_ETENDUE"))) {
                %><input type="hidden" name="RECHERCHE_ETENDUE" value="OUI" /><%
                if (infoBean.get("PAGE") != null) { %>
                    <input type="hidden" name="PAGE" value="<%= infoBean.get("PAGE") %>" /><%
                }
                if (infoBean.get("ETAT_FICHE_ENREGISTREE") != null) {
                    %><input type="hidden" name="ETAT_FICHE_ENREGISTREE" value="<%= infoBean.get("ETAT_FICHE_ENREGISTREE") %>" /><%
                }
            }
            %></p>
        </div> <!-- .resume --><%
        if ( infoBean.getString("MESSAGE_WARNING") != null) { %>
            <p class="message alerte">
                <%=infoBean.getString("MESSAGE_WARNING")%>
            </p><%
        }
    } else {
        %><input type="hidden" name="PROC" value="<%= request.getParameter("PROC") %>" />
        <input type="hidden" name="TOOLBOX" value="<%= request.getParameter("TOOLBOX") %>" /><%
        if (request.getParameter("FCK_PLUGIN")!=null) {
            %><input type="hidden" name="FCK_PLUGIN" value="<%= request.getParameter("FCK_PLUGIN") %>" /><%
        }
        if (request.getParameter("LANGUE_FICHE")!=null) {
            %><input type="hidden" name="LANGUE_FICHE" value="<%= request.getParameter("LANGUE_FICHE") %>" /><%
        }
     }
    if (rssFeed) {
        //dans le cas d'un TAG de flux RSS
        %><fieldset><legend><%= MessageHelper.getCoreMessage("BO_RECHERCHE_TYPE_FLUX") %></legend>
            <table id="FLUX_RSS"><%
            univFmt.insererChampSaisie(fmt, out, infoBean, "FLUX_FEED_LIBELLE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 128, MessageHelper.getCoreMessage("BO_LIBELLE"));
            Hashtable<String, String> hTypeRss = new Hashtable<String, String>();
            hTypeRss.put("rss_1.0","RSS 1.0");
            hTypeRss.put("rss_2.0","RSS 2.0");
            hTypeRss.put("atom_1.0","Atom 1.0");
            univFmt.insererComboHashtable(fmt ,out, infoBean, "TYPE_FLUX_FEED", FormateurJSP.SAISIE_FACULTATIF, hTypeRss, MessageHelper.getCoreMessage("BO_RECHERCHE_TYPE_FLUX"));%>
            </table>
        </fieldset><%
    }
    %>