<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.webapp.WebAppUtil" %>
<%@ page import="com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper" %>

<form action="<%= WebAppUtil.SG_PATH %>" enctype="multipart/form-data" id="saisie_objet" method="post">
    <div>
        <input type="hidden" name="ID_<%= typeObjet %>" value="<%= infoBean.getString("ID_" + typeObjet)%>" />
        <% fmt.insererVariablesCachees(out, infoBean); %>
        <input type="hidden" name="ID_FICHE" value="<%= infoBean.getString("ID_" + typeObjet)%>" />
        <input type="hidden" name="CODE_OBJET" value="<%= infoBean.getString("CODE_OBJET")%>" />
        <input type="hidden" name="TS_CODE" value="<%= infoBean.getString("TS_CODE")%>" />
        <input type="hidden" name="LANGUE_FICHE" value="<%= infoBean.getString("LANGUE")%>" />
        <!-- Utile pour la restauration des données --><%
        if (infoBean.get("ID_CI_RESTAURATION") != null) {
            %><input type="hidden" name="ID_CI_RESTAURATION" value="<%= infoBean.getString("ID_CI_RESTAURATION")%>" />
            <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%= infoBean.getString("NOM_JSP_RESTAURATION")%>" /><%
        }
        %><input type="hidden" name="APERCU" value="0" />
        <input type="hidden" name="ACTION" value="ENREGISTRER" />
        <input type="hidden" name="GRS_SAUVEGARDE_CODE_RUBRIQUE" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RUBRIQUE") %>" />
        <input type="hidden" name="GRS_SAUVEGARDE_CODE_RATTACHEMENT" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RATTACHEMENT") %>" />
        <input type="hidden" name="GRS_SAUVEGARDE_PUBLIC_VISE_ESPACE" value="<%= infoBean.getString("GRS_SAUVEGARDE_PUBLIC_VISE_ESPACE") %>" />
        <input type="hidden" name="LISTE_VALIDATEURS" value="" />
        <input type="hidden" name="NIVEAU_APPROBATION" value="<%=infoBean.getString("NIVEAU_APPROBATION")%>" />
        <input type="hidden" name="NIVEAU_APPROBATION_DEMANDE" value="" />
        <input type="hidden" name="GRS_SAUVEGARDE_CODE_RATTACHEMENT_AUTRES" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RATTACHEMENT_AUTRES") %>" />

        <input type="hidden" name="SOUS_ONGLET_DEMANDE" value="" /><%
        if( infoBean.get("PREMIERE_ACTION") != null ) {
            %><input type="hidden" name="PREMIERE_ACTION" value="<%=infoBean.get("PREMIERE_ACTION")%>" /><%
        }%>
    </div>
<jsp:include page="/adminsite/actions_saisie_fiche.jsp" />
<div>
    <ul id="onglets"><%
        String sousOnglet = infoBean.getString("SOUS_ONGLET");
        for (int iOnglet = 0; iOnglet < nomOnglet.length; iOnglet++) {
            %><li class="<%=nomOnglet[iOnglet].equals(sousOnglet) ? "active" : "" %>" data-action="ONGLET" data-onglet="<%= nomOnglet[iOnglet] %>" ><%= libelleOnglet[iOnglet] %></li><%
        } %>
        <%if ("1".equals(infoBean.getString("GRS_SAISIE_ENCADRES"))) {
            %><li class="<%="encadres".equals(sousOnglet) ? "active" : "" %>" data-action="ONGLET" data-onglet="encadres"><%= MessageHelper.getCoreMessage("BO_ONGLET_ENCADRE") %></li><%
        } %>
        <% if (PluginFicheHelper.hasPlugin(ReferentielObjets.instancierFiche(infoBean.getString("CODE_OBJET")).getClass().getName(), true)) {%>
            <li class="<%="plugins".equals(sousOnglet) ? "active" : "" %>" data-action="ONGLET" data-onglet="plugins"><%= MessageHelper.getCoreMessage("BO_ONGLET_PLUGINS") %></li>
        <% } %>
        <li class="<%="suivi".equals(sousOnglet) ? "active" : "" %>" data-action="ONGLET" data-onglet="suivi"><%= MessageHelper.getCoreMessage("BO_ONGLET_SUIVI") %></li>
        <li class="<%="diffusion".equals(sousOnglet) ? "active" : "" %>" data-action="ONGLET" data-onglet="diffusion"><%= MessageHelper.getCoreMessage("BO_ONGLET_DIFFUSION") %></li>
    </ul>
</div>

<div id="content" role="main">
<% if (infoBean.get("FIL_ARIANE") != null) { %>
    <p id="fil_ariane"><%= infoBean.getString("FIL_ARIANE") %></p>
<% } %>
    <div class="content-deco">
        <p id="champs_obligatoires" class="message alert alert-info"><%= MessageHelper.getCoreMessage("BO_FICHE_CHAMPS_OBLIGATOIRES") %></p>
