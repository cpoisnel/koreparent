<%@page import="java.util.Collection"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@page import="com.kportal.ihm.utils.AdminsiteUtils"%>
<%@page import="com.univ.datagrid.utils.DatagridUtils"%>
<%@page import="com.univ.objetspartages.util.CritereRecherche"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /><%
String toolbox = StringUtils.defaultString(infoBean.getString("TOOLBOX"));
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
boolean isToolbox = StringUtils.isNotBlank(toolbox);
%><div id="content" role="main"><%
    Collection<CritereRecherche> criteresRecherche = AdminsiteUtils.getCriteresRechercheAAfficher(infoBean);
    if (!criteresRecherche.isEmpty() && !isToolbox) {
        %><div id="recherche_criteres">
            <h3><%= module.getMessage("BO_CRITERES_RAPPEL") %></h3>
            <div>
                <ul><%
                for (CritereRecherche critere : criteresRecherche) {
                    String champ = StringUtils.defaultIfBlank(module.getMessage("RUBRIQUE." +critere.getNomChamp()),critere.getNomChamp());
                    %><li><strong><%= champ %></strong> <%=critere.getValeurAAfficher() %></li><%
                }
                %></ul>
                <p><a href="<%=AdminsiteUtils.getUrlRechercheAvanceeDatagrid(module,infoBean) %>"><%= module.getMessage("BO_AFFINER_RECHERCHE") %></a></p>
            </div>
        </div><!-- #recherche_criteres --><%
    }%>
    <div id="treeView" class="view">
        <jsp:include page="/adminsite/tree/tree.jsp">
            <jsp:param name="JSTREEBEAN" value="rubriqueJsTreeProcessus" />
            <jsp:param name="VIEW_SWITCHER" value="true" />
            <jsp:param name="ACTIONS" value="true" />
            <jsp:param name="ACTION" value="RECHERCHER" />
            <jsp:param name="CODE_RECHERCHE" value="<%=StringUtils.defaultString(infoBean.getString(\"CODE\")) %>" />
            <jsp:param name="INTITULE" value="<%=StringUtils.defaultString(infoBean.getString(\"INTITULE\")) %>" />
            <jsp:param name="CODE_SAISI" value="<%=StringUtils.defaultString(infoBean.getString(\"CODE_SAISI\")) %>" />
            <jsp:param name="LANGUE" value="<%=StringUtils.defaultString(infoBean.getString(\"LANGUE\")) %>" />
        </jsp:include>
    </div>
    <div id="listView" class="view" style="display: none;">
        <table class="datatableViews"
            data-search="<%= DatagridUtils.getUrlTraitementDatagrid(infoBean)%>">
            <thead>
                <tr>
                    <th> <%=module.getMessage("BO_INTITULE") %> </th>
                    <th> <%=module.getMessage("BO_LIBELLE_RUBRIQUE_MERE") %> </th>
                    <th> <%=module.getMessage("BO_LIBELLE_CODE") %> </th>
                    <th> <%=module.getMessage("BO_LIBELLE_LANGUE") %> </th>
                    <th class="sanstri sansfiltre"> <%=module.getMessage("BO_ACTIONS") %> </th>
                </tr>
            <thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script>
    function toggleView(element){
        var $ = jQuery,
            $li = $(element),
            $views = $('.view'),
            filterValue = $views.filter(':visible').find('input[type="text"]').val();

        $views.css({display: 'none'});
        $('#' + $li.data('targetid')).find('input[type="text"]').val(filterValue).keyup();
        $('#' + $li.data('targetid')).css({display: 'block'});
    }

    function specificTreeSelect(event, node){
        window.location.href='/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_RUBRIQUE&ACTION=MODIFIERRUBRIQUEPARCODE&CODE_RUBRIQUE=' + node.data('sCode');
    }

    function specificTreeRemove(event, node, tree){
        var $ = jQuery;
        $.ajax({
          type: "POST",
          url: '/servlet/com.kportal.servlet.JsTreeServlet',
          data: { 'JSTREEBEAN': 'rubriquesJsTree', 'ACTION': 'SUPPRIMER', 'CODES_RUBRIQUES': [node.data('sCode')] },
          success: function(data, status){
              $('#messageApplicatif').transition({'scale' : '1', 'opacity' : '1'}, 0).html('<p class="message confirmation">' + data + '</p>').transition({'scale' : '0', 'opacity' : '0', delay: 2000}, function(){
                  $('#messageApplicatif').find('p').remove();
              });
              tree.delete_node(node);
          },
          error: function(jqXHR, status, error){
              $('#messageApplicatif').transition({'scale' : '1', 'opacity' : '1'}, 0).html('<p class="message erreur">' + jqXHR.responseText + '</p>').transition({'scale' : '0', 'opacity' : '0', delay: 2000}, function(){
                  $('#messageApplicatif').find('p').remove();
              });
          }
        });
    }
</script>