<%@page import="com.kportal.core.config.PropertyHelper"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />

<script type="text/javascript" src="/adminsite/utils/swfobject.js"></script>
<script type="text/javascript" src="/adminsite/utils/mediatheque/audio/player_audio.js"></script>
<script type="text/javascript" src="/adminsite/utils/mediatheque/commun/media.js"></script>

<%
String key = String.valueOf(System.currentTimeMillis());
%>
<div class="details-media-conteneur">
    <span id="container_audio_distant" class="apercu-audio-conteneur">
        <a class="apercu-audio" href="<%=infoBean.getString("URL_RESSOURCE")%>">Audio ici</a>
    </span>

    <script type="text/javascript">
    //<![CDATA[
        var playerAudio<%= key%> = new AudioPlayer('container_audio_distant', 240, 20, '<%= PropertyHelper.getCoreProperty("mediatheque.audio.player.url") %>');
        playerAudio<%= key%>.ajouterMedia("<%=infoBean.getString("URL_RESSOURCE")%>", "");
        playerAudio<%= key%>.setAutostart("0");
        playerAudio<%= key%>.genererPlayer();
    //]]>
    </script>