<%@page import="java.util.List"%>
<%@page import="com.jsbsoft.jtf.core.LangueUtil"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@page import="com.univ.objetspartages.bean.EncadreBean"%>
<%@page import="com.univ.objetspartages.om.AutorisationBean"%>
<%@page import="com.univ.objetspartages.om.Perimetre"%>
<%@page import="com.univ.objetspartages.om.PermissionBean"%>
<%@page import="com.univ.objetspartages.services.ServiceEncadre"%>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<%@ page import="com.univ.objetspartages.services.ServiceRubrique" %>
<%@ page import="com.univ.objetspartages.bean.RubriqueBean" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.services.ServiceStructure" %>
<%@ page import="com.univ.objetspartages.om.StructureModele" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
    final ServiceEncadre serviceEncadre = ServiceManager.getServiceForBean(EncadreBean.class);
    IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    AutorisationBean autorisation = ctx.getAutorisation();
    final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
%><div id="content" role="main">
    <table class="datatable">
        <thead>
            <tr>
                <th><%= module.getMessage("BO_INTITULE") %></th>
                <th><%= module.getMessage("BO_RUBRIQUE") %></th>
                <th><%= module.getMessage("ST_CODE_RATTACHEMENT") %></th>
                <th><%= module.getMessage("JTF_ACTIF") %></th>
                <th><%= module.getMessage("BO_LIBELLE_LANGUE") %></th>
                <th class="sanstri sansfiltre"><%= module.getMessage("BO_ACTIONS")%></th>
            </tr>
        </thead>
        <tbody><%
            final List<EncadreBean> encadres = serviceEncadre.getAllEncadres();
            /* Sélection des fils */
            for(EncadreBean currentEncadre : encadres) {
                if (autorisation.possedePermission( new PermissionBean ("TECH","enc","M"), new Perimetre(currentEncadre.getCodeRattachement(), currentEncadre.getCodeRubrique(),"*","*",""))) {
                    RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(currentEncadre.getCode());
                    %><tr>
                        <td>
                            <a class="ellipsis" href="<%=ProcessusHelper.getUrlProcessAction(infoBean, "", "", "MODIFIERPARID", new String[][]{{"ID_ENCADRE",String.valueOf(currentEncadre.getId())}}) %>" title="<%= module.getMessage("BO_MODIFIER") %>"><%= currentEncadre.getIntitule() %></a>
                        </td>
                        <td><%= rubrique != null ? rubrique.getIntitule() : MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE") %></td>
                        <td><%= serviceStructure.getDisplayableLabel(currentEncadre.getCodeRattachement(), LangueUtil.getLangueLocale(ctx.getLocale())) %></td>
                        <td><%= "1".equals(currentEncadre.getActif()) ? module.getMessage("JTF_OUI") : module.getMessage("JTF_NON")  %></td>
                        <td><img src="<%= LangueUtil.getPathImageDrapeau(currentEncadre.getLangue())%>" alt="<%= LangueUtil.getDisplayName(currentEncadre.getLangue())%>"/><p class="masquer"><%= LangueUtil.getDisplayName(currentEncadre.getLangue())%></p></td>
                        <td class="colonne-actions">
                            <a href="<%=ProcessusHelper.getUrlProcessAction(infoBean, "", "", "MODIFIERPARID", new String[][]{{"ID_ENCADRE",String.valueOf(currentEncadre.getId())}}) %>" title="<%= module.getMessage("BO_MODIFIER") %>"><%= module.getMessage("BO_MODIFIER") %></a>&nbsp;|
                            <a data-confirm="<%= module.getMessage("BO_CONFIRM_SUPPR_ENCADRE")%>" href="<%=ProcessusHelper.getUrlProcessAction(infoBean, "", "", "SUPPRIMERPARID", new String[][]{{"ID_ENCADRE",String.valueOf(currentEncadre.getId())}}) %>" title="<%= module.getMessage("BO_SUPPRIMER") %>"><%= module.getMessage("BO_SUPPRIMER") %></a>
                        </td>
                    </tr><%
                }
            }
        %></tbody>
    </table>
</div>