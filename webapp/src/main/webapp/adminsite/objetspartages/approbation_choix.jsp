<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="pragma" content="no-cache">
</head>

<body>

<h2><%= MessageHelper.getCoreMessage("BO.APPROBATION.SELECTION")%>/h2>

<form action="/servlet/com.jsbsoft.jtf.core.SG">

<% fmt.insererVariablesCachees( out, infoBean); %>

<table cellpadding="0" cellspacing="0" border="0">
    <tr >
        <td>&nbsp;</td>
    </tr>
<%if (infoBean.getString("CODE_NIVEAU_AUTOMATIQUE")!=null) { %>
    <tr >
        <td><b><%= MessageHelper.getCoreMessage("BO.VALIDATION.STATE")%></b></td>
    </tr>
        <tr >
        <td>&nbsp;</td>
    </tr>
    <tr >
    <td>
        <input type="radio" name="NIVEAU_APPROBATION" value="<%=infoBean.getString("CODE_NIVEAU_AUTOMATIQUE")%>" checked="true"><%=infoBean.getString("LIBELLE_NIVEAU_AUTOMATIQUE")%></input>
        </td>
    </tr>
<% } %>
    <tr >
        <td>&nbsp;</td>
    </tr>
    <% if (infoBean.getInt("NB_ITEMS_NIVEAU_APPROBATION") > 1) { %>
    <tr >
        <td><b><%= MessageHelper.getCoreMessage("BO.VALIDATION.DELEGATE.LEVELS")%></b></td>
    </tr>
    <% } else { %>
    <tr >
        <td><b><%= MessageHelper.getCoreMessage("BO.VALIDATION.DELEGATE.LEVEL")%></b></td>
    </tr>
    <% } %>
    <tr >
        <td>&nbsp;</td>
    </tr>
<% int i;
    for (i=0; i < infoBean.getInt("NB_ITEMS_NIVEAU_APPROBATION"); i++) { %>
    <tr >
        <td>
        <input type="radio" name="NIVEAU_APPROBATION" value="<%=infoBean.getString("CODE_NIVEAU_APPROBATION#"+i)%>"><%=infoBean.getString("LIBELLE_NIVEAU_APPROBATION#"+i)%></input>
        </td>
    </tr>

<% } %>

</table>

<br>

<%

       fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_VALIDER, FormateurJSP.BOUTON_ANNULER} );

%>


</form>
</body>
</html>