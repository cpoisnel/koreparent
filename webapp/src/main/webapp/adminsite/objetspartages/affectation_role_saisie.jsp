<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.ProcessusHelper" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.extension.module.composant.IComposant" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />
<% IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus()); %>
<table class="datatable">
        <thead>
            <tr>
                <th><%= module.getMessage("BO_ROLE") %></th>
                <th><%= module.getMessage("GROUPEDSI.PERIMETRE_STRUCTURE") %></th>
                <th><%= module.getMessage("GROUPEDSI.PERIMETRE_RUBRIQUE") %></th>
                <th><%= module.getMessage("GROUPEDSI.PERIMETRE_GROUPE") %></th>
                <th class="sanstri sansfiltre"><%= module.getMessage("BO_ACTIONS") %></th>
            </tr>
        </thead>
        <tbody>
        <%
        int nbLignesAffectations = infoBean.getInt("AFFECTATIONS_NB_ITEMS");
        if (nbLignesAffectations != 0) {
            for (int i = 0; i < Math.max(nbLignesAffectations, 1); i++) { %>
                    <tr>
                        <td>
                            <% if (i < nbLignesAffectations) { %>
                                <a href="<%=ProcessusHelper.getUrlProcessAction(infoBean, "core", "SAISIE_ROLE", "MODIFIER", new String[][]{{"ID_ROLE",String.valueOf(infoBean.get("AFFECTATION_ID_ROLE#" + i))}}) %>" title="<%= infoBean.getString("AFFECTATION_INTITULE#" + i ) %>"><%= infoBean.getString("AFFECTATION_INTITULE#" + i ) %></a>
                            <% } else { %>
                            &nbsp;
                            <% } %>
                        </td>
                        <% if (i < nbLignesAffectations) {%>
                            <td><%=infoBean.getString("AFFECTATION_PERIMETRE_STRUCTURE#" + i)%></td>
                            <td><%=infoBean.getString("AFFECTATION_PERIMETRE_RUBRIQUE#" + i)%></td>
                            <td><%=infoBean.getString("AFFECTATION_PERIMETRE_GROUPE#" + i)%></td>
                        <%} else { %>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        <% } %>
                        <td>
                            <% if (i < nbLignesAffectations) { %>
                            <a href="<%=ProcessusHelper.getUrlProcessAction(infoBean, "core", "SAISIE_ROLE", "MODIFIER", new String[][]{{"ID_ROLE",String.valueOf(infoBean.get("AFFECTATION_ID_ROLE#" + i))}}) %>" title="<%= MessageHelper.getCoreMessage("BO_MODIFIER") %>"><%= MessageHelper.getCoreMessage("BO_MODIFIER") %></a> |
                            <a href="#" class="suppression_utilisateur" data-index="<%=i%>"><%=MessageHelper.getCoreMessage("BO_SUPPRIMER") %></a>
                            <% } else { %>
                            &nbsp;
                            <% } %>
                        </td>
                    </tr>
            <% } %>
        <% } %>
        </tbody>
    </table>

    <br/>
    <fieldset id="perimetre">

        <legend><%= module.getMessage("GROUPEDSI.ASSOCIER_ROLE") %></legend>

        <p class="boite unique">
            <label for="ROLE_AFFECTATION" class="colonne"><%= module.getMessage("BO_ROLE") %></label>
            <% fmt.insererComboHashtable(out, infoBean, "ROLE_AFFECTATION", FormateurJSP.SAISIE_FACULTATIF, "LISTE_ROLES"); %>
        </p>

        <div id="perimetreStructure" style="display:none;" class="boite">
            <label for="STRUCTURE_TYPE_AFFECTATION" class="colonne"><%= module.getMessage("ST_CODE_RATTACHEMENT")%></label>
            <button class="infobulle" type="button" title="<%= module.getMessage("GROUPEDSI.TITRE_STRUCTURE") %>"></button>
            <% univFmt.insererkMonoSelect(fmt, out, infoBean, "STRUCTURE_AFFECTATION", FormateurJSP.SAISIE_FACULTATIF, "", "", UnivFmt.CONTEXT_STRUCTURE);%>
        </div><!--
            #perimetre-structure
        --><div id="perimetreRubrique" style="display:none;" class="boite">
                <label for="RUBRIQUE_TYPE_AFFECTATION" class="colonne"><%= module.getMessage("ST_CODE_RUBRIQUE") %></label>
                <button class="infobulle" type="button" title="<%= module.getMessage("GROUPEDSI.TITRE_RUBRIQUE") %>"></button>
                <% univFmt.insererkMonoSelect(fmt, out, infoBean, "RUBRIQUE_AFFECTATION", FormateurJSP.SAISIE_FACULTATIF, "", "rubrique", UnivFmt.CONTEXT_ZONE);%>
        </div><!-- #perimetre-rubrique -->
        <div id="perimetreGroupe" style="display:none;" class="boite">
                <label for="GROUPE_TYPE_AFFECTATION" class="colonne"><%= module.getMessage("GROUPEDSI.CODE") %></label>
                <button class="infobulle" type="button" title="<%= module.getMessage("GROUPEDSI.TITRE_GROUPE")%>"></button>
                <%univFmt.insererkMonoSelect(fmt, out, infoBean, "GROUPE_AFFECTATION", FormateurJSP.SAISIE_FACULTATIF, "", "", UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION);%>
        </div><!-- #perimetre-groupe -->
        <input id="AJOUTER_AFFECTATION" type="button" name="AJOUTER_AFFECTATION" value="<%= module.getMessage("BO_AJOUTER") %>" />
    </fieldset>
<%-- Fin liste des affectations --%>

<script type="text/javascript">

    var kMonoSelectPlaceHolder = { 'f' : 'Toutes', 'm' : 'Tous'};
    var arrayRoles = [];

    function Role(p1, p2, p3, p4)
    {
        this.code = p1;
        this.rubrique = p2;
        this.structure = p3;
        this.groupe = p4;
    }
    <% for (int i=0;i<infoBean.getInt("ROLES_NB_ITEMS"); i++) { %>
        arrayRoles[<%=i%>]= new Role('<%=infoBean.getString("ROLE_"+i)%>','<%=infoBean.getString("PERIMETRE_RUBRIQUE_"+i)%>','<%=infoBean.getString("PERIMETRE_STRUCTURE_"+i)%>','<%=infoBean.getString("PERIMETRE_GROUPE_"+i)%>');
    <% } %>

    function specificActionOnSelectInTree(sMode)
    {
        var oTypeItem = document.forms[0].elements[sMode + '_TYPE_AFFECTATION'];
        var oItem = document.forms[0].elements[sMode + '_AFFECTATION'];
        if (oItem && oItem.value != '')
        {
            oTypeItem[1].checked = true;
        }
    }
</script>
