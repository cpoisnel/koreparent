<%@ page import="java.util.Collection" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.ProcessusHelper" %>
<%@ page import="com.kportal.extension.module.composant.IComposant" %>
<%@ page import="com.univ.objetspartages.bean.RoleBean" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
%><div id="content" role="main">
    <table class="datatable">
        <thead>
        <tr>
            <th><%= module.getMessage("BO_INTITULE") %></th>
            <th><%= module.getMessage("BO_LIBELLE_CODE") %></th>
            <th class="sanstri sansfiltre"><%= module.getMessage("BO_ACTIONS") %></th>
        </tr>
        </thead>
        <tbody><%
            Collection<RoleBean> toutLesRoles = infoBean.getCollection("LISTE_ROLES");
            if (toutLesRoles != null && !toutLesRoles.isEmpty()) {
                for (RoleBean roleCourant : toutLesRoles) {
        %><tr>
            <td>
                <a href="<%=ProcessusHelper.getUrlProcessAction(infoBean, StringUtils.EMPTY, StringUtils.EMPTY, "MODIFIER", new String[][]{{"ID_ROLE",String.valueOf(roleCourant.getIdRole())}}) %>" title="<%= module.getMessage("BO_MODIFIER") %>"><%=roleCourant.getLibelle() %></a>
            </td>
            <td><%=roleCourant.getCode() %></td>
            <td>
                <a href="<%=ProcessusHelper.getUrlProcessAction(infoBean, StringUtils.EMPTY, StringUtils.EMPTY, "MODIFIER", new String[][]{{"ID_ROLE",String.valueOf(roleCourant.getIdRole())}}) %>" title="<%= module.getMessage("BO_MODIFIER") %>"><%= module.getMessage("BO_MODIFIER") %></a> |
                <a data-confirm="<%= module.getMessage("BO_CONFIRM_SUPPR_ROLE") %>" href="<%=ProcessusHelper.getUrlProcessAction(infoBean, StringUtils.EMPTY, StringUtils.EMPTY, "SUPPRIMER", new String[][]{{"ID_ROLE",String.valueOf(roleCourant.getIdRole())}}) %>" title="<%= module.getMessage("BO_SUPPRIMER") %>"><%= module.getMessage("BO_SUPPRIMER") %></a>
            </td>
        </tr><%
                }
            }
        %></tbody>
    </table>
</div>