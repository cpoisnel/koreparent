<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.core.InfoBean"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.core.config.PropertyHelper"%>
<%@ page import="com.kportal.extension.module.composant.IComposant" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %> 
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /> 
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /> 
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());%>

<form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post">
    <div id="actions">
        <div>
            <ul><%
                if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {%>
                    <li><button class="supprimer" data-confirm="<%= module.getMessage("BO_CONFIRM_SUPPR_SERVICE") %>" type="submit" name="SUPPRIMER" value="<%= module.getMessage("JTF_BOUTON_SUPPRIMER")%>" ><%= module.getMessage("JTF_BOUTON_SUPPRIMER")%></button></li><%
                }
                %><li><button class="enregistrer" type="submit" name="VALIDER" value="<%= module.getMessage("BO_ENREGISTRER")%>" ><%= module.getMessage("BO_ENREGISTRER")%></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= module.getMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->
    <br/>
    <div id="content" role="main">
        <input type="hidden" name="ACTION" value="">
        <% fmt.insererVariablesCachees( out, infoBean); %>

        <fieldset>
            <legend><%= MessageHelper.getCoreMessage("BO_PREFERENCES_INFOS_GENERALES") %></legend>

            <% if( infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) { %>
                <p>
                    <strong class="label colonne"><%= MessageHelper.getCoreMessage("BO_LIBELLE_CODE") %></strong>
                    <%fmt.insererChampSaisie( out, infoBean, "SERVICE_CODE", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 20); %>
                </p>
            <% } else { %>
                <p>
                    <label for="SERVICE_CODE" class="colonne"><%= MessageHelper.getCoreMessage("BO_LIBELLE_CODE") %>(*)</label>
                    <%fmt.insererChampSaisie( out, infoBean, "SERVICE_CODE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 20); %>
                </p>
            <% } %>
            <p>
                <label for="SERVICE_INTITULE" class="colonne"><%= MessageHelper.getCoreMessage("BO_INTITULE") %> (*)</label>
                <%fmt.insererChampSaisie( out, infoBean, "SERVICE_INTITULE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 30); %>
            </p>

            <p>
                <%fmt.insererChampSaisie( out, infoBean, "SERVICE_PROXY_CAS", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
                <label for="SERVICE_PROXY_CAS"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_SERVICE_CAS")%></label>
                <span><%= MessageHelper.getCoreMessage("BO_PREFERENCES_SERVICE_CAS_PROTEGE")%></span>
            </p>

            <p>
                <%fmt.insererChampSaisie( out, infoBean, "SERVICE_JETON_KPORTAL", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
                <label for="SERVICE_JETON_KPORTAL"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_TICKET_SSO")%></label> <span><%= MessageHelper.getCoreMessage("BO_PREFERENCES_TICKET_SSO_GENERATION")%></span>
            </p>

            <p>
                <label for="SERVICE_EXPIRATION_CACHE" class="colonne"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_EXPIRATION_CACHE") %></label>
                <%fmt.insererChampSaisie( out, infoBean, "SERVICE_EXPIRATION_CACHE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 4); %>
            </p>

            <p>
                <span class="label colonne">&nbsp;</span>
                <span><%= MessageHelper.getCoreMessage("BO_PREFERENCES_EXPIRATION_CACHE_DUREE") %></span>
            </p>
            <div id="appli">
                <p>
                    <label for="SERVICE_URL" class="colonne"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_URL_APP") %></label>
                    <% fmt.insererChampSaisie(out, infoBean, "SERVICE_URL", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, ""); %>
                </p>

                <p>
                    <span class="label colonne">&nbsp;</span>
                    <span><%= MessageHelper.getCoreMessage("BO_PREFERENCES_URL_APP_VIDE") %></span>
                </p>

                <p>
                    <%fmt.insererChampSaisie( out, infoBean, "SERVICE_URL_POPUP", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
                    <label for="SERVICE_URL_POPUP"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_ACTIVER_POPUP")%></label>
                </p>
            </div>
        </fieldset>

        <fieldset>
            <legend><%= MessageHelper.getCoreMessage("BO_PREFERENCES_VUE_REDUITE") %></legend>
            <p>
                <span class="label colonne">&nbsp;</span>
                <% fmt.insererChampSaisie(out, infoBean, "SERVICE_VUE_REDUITE_URL", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, ""); %>
            </p>
        </fieldset>

        <fieldset>
            <legend><%= MessageHelper.getCoreMessage("BO_PREFERENCES_DIFFUSION_PUB_VISE") %></legend>

            <% if ("1".equals(PropertyHelper.getCoreProperty("dsi.activation"))) { %>

            <p>
                <% fmt.insererChampSaisie(out, infoBean,"SERVICE_DIFFUSION_MODE",FormateurJSP.SAISIE_FACULTATIF,FormateurJSP.FORMAT_RADIO,0,    0,""); %>
                <label for="SERVICE_DIFFUSION_MODE_0"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_TOUS_UTILISATEURS") %></label>
            </p>

            <p>
                <% fmt.insererChampSaisie(out, infoBean,"SERVICE_DIFFUSION_MODE",FormateurJSP.SAISIE_FACULTATIF,FormateurJSP.FORMAT_RADIO,0,    0,""); %>
                <label for="SERVICE_DIFFUSION_MODE_1"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_GROUPES_SUIVANTS") %></label>
            </p>
            <%univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "SERVICE_DIFFUSION_PUBLIC_VISE", FormateurJSP.SAISIE_FACULTATIF, "", "TMP_SERVICE_DIFFUSION_PUBLIC_VISE", "Liste des groupes", UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION, "");%>
        </fieldset>

        <fieldset>
            <legend><%= MessageHelper.getCoreMessage("BO_PREFERENCES_RESTRICTIONS") %></legend>

            <p>
                <% fmt.insererChampSaisie(out, infoBean,"SERVICE_DIFFUSION_MODE_RESTRICTION",FormateurJSP.SAISIE_FACULTATIF,FormateurJSP.FORMAT_RADIO,0,    0,""); %>
                <label for="SERVICE_DIFFUSION_MODE_RESTRICTION_0"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_PAS_RESTRICTION") %></label>
            </p>

            <p>
                <% fmt.insererChampSaisie(out, infoBean,"SERVICE_DIFFUSION_MODE_RESTRICTION",FormateurJSP.SAISIE_FACULTATIF,FormateurJSP.FORMAT_RADIO,0,    0,""); %>
                <label for="SERVICE_DIFFUSION_MODE_RESTRICTION_1"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_RESTRICTION_PUBLIC") %></label>
            </p>

            <p>
                <% fmt.insererChampSaisie(out, infoBean,"SERVICE_DIFFUSION_MODE_RESTRICTION",FormateurJSP.SAISIE_FACULTATIF,FormateurJSP.FORMAT_RADIO,0,    0,""); %>
                <label for="SERVICE_DIFFUSION_MODE_RESTRICTION_2"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_RESTRICTION_GROUPE") %></label>
            </p>

             <%
                 univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "SERVICE_DIFFUSION_PUBLIC_VISE_RESTRICTION", FormateurJSP.SAISIE_FACULTATIF, "", "TMP_SERVICE_DIFFUSION_PUBLIC_VISE_RESTRICTION", MessageHelper.getCoreMessage("BO_PREFERENCES_LISTE_GROUPES"), UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION, "");
             } %>


            <p>
                <% fmt.insererChampSaisie(out, infoBean,"SERVICE_DIFFUSION_MODE_RESTRICTION",FormateurJSP.SAISIE_FACULTATIF,FormateurJSP.FORMAT_RADIO,0,    0,""); %>
                <label for="SERVICE_DIFFUSION_MODE_RESTRICTION_3"><%= MessageHelper.getCoreMessage("BO_PREFERENCES_ANONYME") %></label>
            </p>

        </fieldset>
    </div><!-- #content -->
</form>
