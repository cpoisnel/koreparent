<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<div id="content" role="main">
    <form action="/servlet/com.jsbsoft.jtf.core.SG" data-no-tooltip>
        <div class="fieldset neutre">
            <% fmt.insererVariablesCachees( out, infoBean); %>

                <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE", fmt.SAISIE_FACULTATIF,  MessageHelper.getCoreMessage("BO_RUBRIQUE"), "rubrique", univFmt.CONTEXT_ZONE);%>
                <p>
                    <label for="INTITULE" class="colonne"><%= MessageHelper.getCoreMessage("BO_INTITULE") %></label>
                    <%fmt.insererChampSaisie(out, infoBean, "INTITULE" , FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 32); %>
                </p>
                <p>
                    <label for="CATEGORIE" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_CATEGORIE") %></label>
                    <%fmt.insererComboHashtable(out, infoBean, "CATEGORIE", FormateurJSP.SAISIE_FACULTATIF, "CATEGORIES");%>
                </p>
                <p>
                    <label for="LANGUE" class="colonne"><%= MessageHelper.getCoreMessage("BO_LANGUE") %></label>
                    <%fmt.insererComboHashtable(out, infoBean, "LANGUE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_LANGUES"); %>
                </p>
                <p>
                    <label for="CODE_SAISI" class="colonne"><%= MessageHelper.getCoreMessage("RUBRIQUE.CODE_SAISI") %></label>
                    <%fmt.insererChampSaisie(out, infoBean, "CODE_SAISI" , FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 32);%>
                </p>

            <%
                   fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_VALIDER} );
            %>
        </div>
    </form>
</div><!--  #content -->
