<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.URLResolver" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" /><%
    final ContexteUniv ctx = ContexteUtil.getContexteUniv();
    int nbLignes = infoBean.get("NB_REFERENCES") == null ? 0 : infoBean.getInt("NB_REFERENCES");
%><div id="div-references" data-ui="tabs-container" style="display : none;">
    <table class="datatableLight">
        <thead>
        <tr>
            <th><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TITRE")%></th>
            <th><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TYPE_OBJET")%></th>
            <th><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.AUTEUR")%></th>
            <th><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.ACTION")%></th>
        </tr>
        </thead>
        <tbody><%
            for (int i=0; i < nbLignes; i++) {
            %><tr><%
                if(StringUtils.isNotBlank(infoBean.getString("URL#"+i))){ %>
                    <td><a href="<%=URLResolver.getAbsoluteUrl(infoBean.getString("URL#"+i),ctx)%>" target="_blank"><%=infoBean.getString("TITRE#"+i)%></a></td><%
                } else { %>
                    <td><%=infoBean.getString("TITRE#"+i)%></td><%
                }
                %><td><%= infoBean.getString("OBJET#"+i) %></td>
                <td><%=infoBean.getString("AUTEUR#"+i)%></td>
                <td><%
                if (StringUtils.isNotBlank(infoBean.getString("URL#" + i))) {
                    %><a href="<%=URLResolver.getAbsoluteUrl(infoBean.getString("URL#"+i),ctx)%>" target="_blank"><img src="/adminsite/images/modif.gif" alt="<%= MessageHelper.getCoreMessage("BO_MODIFIER_FICHE")%>" title="<%= MessageHelper.getCoreMessage("BO_MODIFIER_FICHE")%>" /><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.MODIFIER")%></a><%
                }
                %></td>
            </tr><%
            }
        %></tbody>
    </table>
</div><!-- .div-references -->