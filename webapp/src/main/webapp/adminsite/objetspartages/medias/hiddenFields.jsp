<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />

<% fmt.insererVariablesCachees( out, infoBean); %>

<input type="hidden" name="GRS_SAUVEGARDE_CODE_RUBRIQUE" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RUBRIQUE") %>">
<input type="hidden" name="GRS_SAUVEGARDE_CODE_RATTACHEMENT" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RATTACHEMENT") %>">

<!-- Utile pour la restauration des données -->
<% if (infoBean.get("ID_CI_RESTAURATION") != null) { %>
    <input type="hidden" name="ID_CI_RESTAURATION" value="<%= infoBean.getString("ID_CI_RESTAURATION")%>" />
    <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%= infoBean.getString("NOM_JSP_RESTAURATION")%>" />
<% } %>

<input type="hidden" name="NB_LANGUES" value="<%= infoBean.get("NB_LANGUES") %>">
<input type="hidden" name="URL_VIGNETTE" value="<%= infoBean.get("URL_VIGNETTE") %>">
<input type="hidden" name="ACTION" value="">

<%
    if (infoBean.get("ID_MEDIA") != null ) {
        %><input type="hidden" name="ID_MEDIA" value="<%= infoBean.getString("ID_MEDIA")%>"><%
    }
    if (infoBean.getString("MODE") != null) {
        %><input type="hidden" name="MODE" value="<%= infoBean.get("MODE") %>" /><%
    }
    if (infoBean.getString("CODE") != null) {
        %><input type="hidden" name="CODE" value="<%= infoBean.getString("CODE")%>" /><%
    }
    if (infoBean.getString("OBJET") != null) {
        %><input type="hidden" name="OBJET" value="<%= infoBean.getString("OBJET")%>" /><%
    }
    if (infoBean.getString("ID_FICHE") != null) {
        %><input type="hidden" name="ID_FICHE" value="<%= infoBean.getString("ID_FICHE")%>" /><%
    }
    if (infoBean.getString("MODE_FICHIER") != null) {
        %><input type="hidden" name="MODE_FICHIER" value="<%= infoBean.get("MODE_FICHIER") %>" /><%
    }
    if (infoBean.getString("CODE_PARENT") != null) {
        %><input type="hidden" name="CODE_PARENT" value="<%= infoBean.get("CODE_PARENT") %>" /><%
    }
    if (infoBean.get("NO_FICHIER") != null) {
        %><input type="hidden" name="NO_FICHIER" value="<%= infoBean.get("NO_FICHIER") %>" /><%
    }
    if (infoBean.get("IS_LOCAL") != null) {
        %><input type="hidden" name="IS_LOCAL" value="<%= infoBean.get("IS_LOCAL") %>" /><%
    }
%>