<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final String tag = StringUtils.defaultString(infoBean.get("TAG", String.class));
    final String urlRessource = infoBean.get("URL_RESSOURCE", String.class);
%><p>
    <label for="TAG" class="colonne"><%= MessageHelper.getCoreMessage("KVIDEO.TAG.LABEL") %></label>
    <input type="text" id="TAG" name="TAG" size="70" value="<%= tag %>" readonly>
    <a class="recharger" href="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=VIDEO&FCK_PLUGIN=TRUE&CODE=<%= infoBean.get("CODE") %>&OBJET=<%= infoBean.get("OBJET") %>&ID_FICHE=<%= infoBean.get("ID_FICHE") %>"><%= MessageHelper.getCoreMessage("LINK_TYPE.MEDIA.EDIT") %></a>
</p><%
    if(StringUtils.isNotBlank(urlRessource)) {
    %><div class="kvideo__preview js-kvideo__preview">
    <div class="video-container" flv_div="true" flv_local="1">
            <video controls="controls" width="480" height="270">
                <source src="<%= urlRessource %>" type="video/<%= infoBean.get("EXTENSION", String.class) %>" />
                <object width="480" height="270" type="application/x-shockwave-flash" data="/adminsite/scripts/libs/mediaElement/flashmediaelement.swf">
                    <param value="/adminsite/scripts/libs/mediaElement/flashmediaelement.swf" name="movie" />
                    <param value="controls=true&amp;file=<%= urlRessource %>" name="flashvars" />
                    <param value="true" name="allowFullScreen" />
                </object>
            </video>
        </div>In eros metus, gravida a, gravida sed, lobortis
        id, turpis. Ut ultrices, ipsum at venenatis fringilla, sem nulla lacinia tellus,
        eget aliquet turpis mauris non enim. Nam turpis. Suspendisse lacinia. Curabitur
        ac tortor ut ipsum egestas elementum. Nunc imperdiet gravida mauris.
    </div><%
    }
%>