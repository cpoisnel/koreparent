<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final String align = StringUtils.defaultIfBlank(infoBean.get("ALIGN", String.class), "none");
    final String url = StringUtils.defaultString(infoBean.get("URL", String.class));
%><p>
    <label for="URL" class="colonne"><%= MessageHelper.getCoreMessage("KFLASH.URL.LABEL") %></label>
    <input type="text" id="URL" name="URL" size="70" value="<%= url %>" readonly>
    <a class="recharger" href="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=FLASH&FCK_PLUGIN=TRUE&CODE=<%= infoBean.get("CODE") %>&OBJET=<%= infoBean.get("OBJET") %>&ID_FICHE=<%= infoBean.get("ID_FICHE") %>"><%= MessageHelper.getCoreMessage("LINK_TYPE.MEDIA.EDIT") %></a>
</p>
<div class="kflash__parameters">
    <p>
        <label for="WIDTH" class="colonne"><%= MessageHelper.getCoreMessage("KFLASH.WIDTH.LABEL") %></label>
        <input class="js-kflash__property-handler" type="text" id="WIDTH" name="WIDTH" size="4" value="<%= StringUtils.defaultString(infoBean.get("WIDTH", String.class)) %>" data-propertyhandler="width" data-propertyhandler-unit="px" data-propertyhandler-reset="<%= StringUtils.defaultString(infoBean.get("WIDTH", String.class)) %>">
    </p>

    <p>
        <label for="HEIGHT" class="colonne"><%= MessageHelper.getCoreMessage("KFLASH.HEIGHT.LABEL") %></label>
        <input class="js-kflash__property-handler" type="text" id="HEIGHT" name="HEIGHT" size="4" value="<%= StringUtils.defaultIfBlank(infoBean.get("HEIGHT", String.class), "250") %>" data-propertyhandler="height" data-propertyhandler-unit="px" data-propertyhandler-reset="<%= StringUtils.defaultIfBlank(infoBean.get("HEIGHT", String.class), "250") %>">
    </p>

    <p>
        <label for="HORIZONTAL_MARGIN" class="colonne"><%= MessageHelper.getCoreMessage("KFLASH.HORIZONTAL_MARGIN.LABEL") %></label>
        <input class="js-kflash__property-handler" type="text" id="HORIZONTAL_MARGIN" name="HORIZONTAL_MARGIN" size="4" value="<%= StringUtils.defaultIfBlank(infoBean.get("HORIZONTAL_MARGIN", String.class), "5") %>" data-propertyhandler="margin-left,margin-right"
               data-propertyhandler-unit="px" data-propertyhandler-reset="<%= StringUtils.defaultString(infoBean.get("HORIZONTAL_MARGIN", String.class)) %>">
    </p>

    <p>
        <label for="VERTICAL_MARGIN" class="colonne"><%= MessageHelper.getCoreMessage("KFLASH.VERTICAL_MARGIN.LABEL") %></label>
        <input class="js-kflash__property-handler" type="text" id="VERTICAL_MARGIN" name="VERTICAL_MARGIN" size="4" value="<%= StringUtils.defaultIfBlank(infoBean.get("VERTICAL_MARGIN", String.class), "4") %>" data-propertyhandler="margin-top,margin-bottom"
               data-propertyhandler-unit="px" data-propertyhandler-reset="<%= StringUtils.defaultString(infoBean.get("VERTICAL_MARGIN", String.class)) %>">
    </p>

    <p>
        <label for="ALIGN" class="colonne"><%= MessageHelper.getCoreMessage("KFLASH.ALIGN.LABEL") %></label>
        <select class="js-kflash__property-handler" id="ALIGN" name="ALIGN" data-propertyhandler="float" data-propertyhandler-reset="<%= StringUtils.defaultString(infoBean.get("ALIGN", String.class)) %>">
            <option value="none" <%= align.equals("none") ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KFLASH.ALIGN.UNDEFINED.LABEL") %></option>
            <option value="left" <%= align.equals("left") ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KFLASH.ALIGN.LEFT.LABEL") %></option>
            <option value="right" <%= align.equals("right") ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KFLASH.ALIGN.RIGHT.LABEL") %></option>
        </select>
    </p>
</div><%
    if(StringUtils.isNotBlank(url)) {
    %><div class="kflash__preview js-kflash__preview">
        <div class="kflash">
            <object type="application/x-shockwave-flash" data="<%= url %>" width="100%" height="100%">
                <param name="movie" value="<%= url %>" />
                <param name="allowfullscreen" value="false" />
                <param name="allowscriptaccess" value="always" />
                <param name="wmode" value="opaque" />
                <param name="quality" value="high" />
                <param name="menu" value="true" />
                <param name="play" value="false" />
            </object>
        </div>Lorem ipsum dolor sit amet, consectetuer adipiscing
        elit. Maecenas feugiat consequat diam. Maecenas metus. Vivamus diam purus, cursus
        a, commodo non, facilisis vitae, nulla. Aenean dictum lacinia tortor. Nunc iaculis,
        nibh non iaculis aliquam, orci felis euismod neque, sed ornare massa mauris sed
        velit. Nulla pretium mi et risus. Fusce mi pede, tempor id, cursus ac, ullamcorper
        nec, enim. Sed tortor. Curabitur molestie. Duis velit augue, condimentum at, ultrices
        a, luctus ut, orci. Donec pellentesque egestas eros. Integer cursus, augue in cursus
        faucibus, eros pede bibendum sem, in tempus tellus justo quis ligula. Etiam eget
        tortor. Vestibulum rutrum, est ut placerat elementum, lectus nisl aliquam velit,
        tempor aliquam eros nunc nonummy metus. In eros metus, gravida a, gravida sed, lobortis
        id, turpis. Ut ultrices, ipsum at venenatis fringilla, sem nulla lacinia tellus,
        eget aliquet turpis mauris non enim. Nam turpis. Suspendisse lacinia. Curabitur
        ac tortor ut ipsum egestas elementum. Nunc imperdiet gravida mauris.
    </div><%
    }
%>