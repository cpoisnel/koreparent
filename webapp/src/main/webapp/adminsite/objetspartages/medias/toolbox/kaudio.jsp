<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.URLResolver" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final String tag = StringUtils.defaultString(infoBean.get("TAG", String.class));
    final String urlRessource = infoBean.get("URL_RESSOURCE", String.class);
    final String extension = infoBean.get("EXTENSION", String.class);
%><p>
    <label for="TAG" class="colonne"><%= MessageHelper.getCoreMessage("KAUDIO.TAG.LABEL") %></label>
    <input type="text" id="TAG" name="TAG" size="70" value="<%= tag %>" readonly>
    <a class="recharger" href="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=AUDIO&FCK_PLUGIN=TRUE&CODE=<%= infoBean.get("CODE") %>&OBJET=<%= infoBean.get("OBJET") %>&ID_FICHE=<%= infoBean.get("ID_FICHE") %>"><%= MessageHelper.getCoreMessage("LINK_TYPE.MEDIA.EDIT") %></a>
</p><%
    if(StringUtils.isNotBlank(urlRessource)) {
    %><div class="kaudio__preview js-kaudio__preview">
        <div class="audio-container">
            <audio width="100%" height="30px" style="width:100%;height:30px" controls >
                <source src="<%= urlRessource %>" type="audio/<%= extension %>">
                <object width="100%" height="30px" type="application/x-shockwave-flash" data="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>">
                    <param value="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>" name="movie" />
                    <param name="flashvars" value="controls=true&amp;isvideo=false&amp;autoplay=false&amp;preload=none&amp;file=<%= urlRessource %>" />
                    <param value="false" name="allowFullScreen" />
                </object>
            </audio>
        </div>Lorem ipsum dolor sit amet, consectetuer adipiscing
        elit. Maecenas feugiat consequat diam. Maecenas metus. Vivamus diam purus, cursus
        a, commodo non, facilisis vitae, nulla. Aenean dictum lacinia tortor. Nunc iaculis,
        nibh non iaculis aliquam, orci felis euismod neque, sed ornare massa mauris sed
        velit. Nulla pretium mi et risus. Fusce mi pede, tempor id, cursus ac, ullamcorper
        nec, enim. Sed tortor. Curabitur molestie. Duis velit augue, condimentum at, ultrices
        a, luctus ut, orci. Donec pellentesque egestas eros. Integer cursus, augue in cursus
        faucibus, eros pede bibendum sem, in tempus tellus justo quis ligula. Etiam eget
        tortor. Vestibulum rutrum, est ut placerat elementum, lectus nisl aliquam velit,
        tempor aliquam eros nunc nonummy metus. In eros metus, gravida a, gravida sed, lobortis
        id, turpis. Ut ultrices, ipsum at venenatis fringilla, sem nulla lacinia tellus,
        eget aliquet turpis mauris non enim. Nam turpis. Suspendisse lacinia. Curabitur
        ac tortor ut ipsum egestas elementum. Nunc imperdiet gravida mauris.
    </div><%
    }
%>