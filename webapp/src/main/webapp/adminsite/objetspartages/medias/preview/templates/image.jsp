<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<div class="media-gallery js-media-gallery">
    <img src="<%= StringUtils.defaultString(infoBean.get("URL_RESSOURCE", String.class)) %>" alt="<%= MessageHelper.getCoreMessage("KIMAGE.PREVIEW.ALT") %>" title="<%= MessageHelper.getCoreMessage("KIMAGE.PREVIEW.ALT") %>"/>
</div>