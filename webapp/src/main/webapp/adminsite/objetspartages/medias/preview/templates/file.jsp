<%@ page import="com.univ.utils.EscapeString" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final String requestUrl = EscapeString.escapeURL(infoBean.get("URL_RESSOURCE", String.class));
%><iframe src="https://docs.google.com/viewer?url=<%= requestUrl %>&embedded=true" width="100%" height="100%"></iframe>