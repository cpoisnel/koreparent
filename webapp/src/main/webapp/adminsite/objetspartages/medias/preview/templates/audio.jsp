<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.URLResolver" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<audio width="100%" height="30px" style="width:100%;height:30px" controls >
    <source src="<%= infoBean.get("URL_RESSOURCE", String.class) %>" type="audio/<%= infoBean.get("EXTENSION", String.class) %>">
    <object width="100%" height="30px" type="application/x-shockwave-flash" data="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>">
        <param value="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>" name="movie" />
        <param name="flashvars" value="controls=true&amp;isvideo=false&amp;autoplay=false&amp;preload=none&amp;file=<%= infoBean.get("URL_RESSOURCE", String.class) %>" />
        <param value="false" name="allowFullScreen" />
    </object>
</audio>