<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    String extension = infoBean.get("EXTENSION", String.class);
    extension = "flv".equals(extension) ? "x-flv" : extension;
%>
<div class="video-container" flv_div="true" flv_local="1">
    <video controls="controls" width="640" height="480" style="width:100%; height:100%">
        <source src="<%= infoBean.get("URL_RESSOURCE", String.class) %>" type="video/<%= extension %>" />
        <object width="640" height="174" style="width:100%; height:100%" type="application/x-shockwave-flash" data="/adminsite/scripts/libs/mediaElement/flashmediaelement.swf">
            <param value="/adminsite/scripts/libs/mediaElement/flashmediaelement.swf" name="movie" />
            <param value="controls=true&amp;file=<%= infoBean.get("URL_RESSOURCE", String.class) %>" name="flashvars" />
            <param value="true" name="allowFullScreen" />
        </object>
    </video>
</div>