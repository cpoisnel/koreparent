<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />

<div class="apercu-photo-conteneur">
    <a href="<%=infoBean.getString("URL_RESSOURCE")%>" target="yoxview">
        <img src="<%= infoBean.getString("URL_RESSOURCE")%>" alt="" class="apercu-photo" />
    </a>
</div>

<div class="details-media-conteneur">

