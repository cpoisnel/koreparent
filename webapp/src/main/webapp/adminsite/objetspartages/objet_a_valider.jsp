<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<% if( infoBean.get("TOOLBOX") == null) { %>
    <div id="content" role="main">
        <table class="fiches_a_valider">
            <thead>
                    <tr>
                        <th><%=MessageHelper.getCoreMessage("BO_INTITULE_FICHE") %></th>
                        <th><%=MessageHelper.getCoreMessage("BO_TYPE_FICHE") %></th>
                        <th><%=MessageHelper.getCoreMessage("ST_TABLEAU_RUBRIQUE") %>
                        <th class="sanstri sansfiltre"><%=MessageHelper.getCoreMessage("ST_TABLEAU_LANGUE") %></th>
                        <th><%=MessageHelper.getCoreMessage("BO_FICHE_DATE_DEMANDE_VALIDATION") %></th>
                        <th><%=MessageHelper.getCoreMessage("ST_TABLEAU_REDACTEUR") %></th>
                        <th><%=MessageHelper.getCoreMessage("ST_TABLEAU_ACTIONS") %></th>
                    </tr>
            </thead>
            <tbody><%
                for (int i=0; i< infoBean.getInt("LISTE_NB_ITEMS"); i++) { %>
                    <tr>
                        <td>
                            <a class="ellipsis" href="/servlet/com.jsbsoft.jtf.core.SG?EXT=<%= infoBean.getString("EXTENSION#"+i)%>&ECRANVALIDER=TRUE&PROC=<%= infoBean.getString("NOM_PROCESSUS#"+i)%>&ACTION=MODIFIER&ID_FICHE=<%= infoBean.getString("ID_FICHE#"+i)%>"><%=infoBean.getString("LIBELLE#"+i) %></a>
                        </td>
                        <td>
                            <%fmt.insererChampSaisie( out, infoBean, "NOM_OBJET#"+i , FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 5); %>
                        </td>
                        <td>
                            <%fmt.insererChampSaisie( out, infoBean, "CODE_RUBRIQUE#"+i , FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 5); %>
                        </td>
                        <td>
                            <% univFmt.insererDrapeauLangue( out, infoBean.getString("LANGUE#"+i)); %>
                        </td>
                        <td>
                            <%fmt.insererChampSaisie( out, infoBean, "DATE_PROPOSITION#"+i , FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 5); %>
                        </td>
                        <td>
                            <%fmt.insererChampSaisie( out, infoBean, "LIBELLE_REDACTEUR#"+i , FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 5); %>
                        </td>
                        <td>
                            <a href="/servlet/com.jsbsoft.jtf.core.SG?EXT=<%= infoBean.getString("EXTENSION#"+i)%>&ECRANVALIDER=TRUE&PROC=<%= infoBean.getString("NOM_PROCESSUS#"+i)%>&ACTION=MODIFIER&ID_FICHE=<%= infoBean.getString("ID_FICHE#"+i)%>"><%= MessageHelper.getCoreMessage("BO_VERIFIER")%></a> |
                            <a href="/servlet/com.jsbsoft.jtf.core.SG?EXT=<%= infoBean.getString("EXTENSION#"+i)%>&PROC=<%= infoBean.getString("NOM_PROCESSUS#"+i)%>&ACTION=SUPPRIMER&ID_FICHE=<%= infoBean.getString("ID_FICHE#"+i)%>" data-confirm="<%= MessageHelper.getCoreMessage("BO_CONFIRM_SUPPR_FICHE")%>"><%= MessageHelper.getCoreMessage("BO_SUPPRIMER")%></a>
                        </td>
                    </tr><%
                } %>
            </tbody>
        </table>
    </div><%
} %>

