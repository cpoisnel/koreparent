// renvoie le formulaire demandé
function GetForm(sFormName) {
    var oForm = null;
    if (sFormName)
        oForm = document.forms[sFormName];
    else
        oForm = document.forms[0];
    return oForm;
}

// renvoie le champ demandé sous forme d'un objet Field
function GetField(oForm, sFieldName) {
    if (oForm.elements[sFieldName])
        return new Field(oForm.elements[sFieldName]);
    else
        return null;
}

// renvoie la valeur de la combo
function GetSelectValue(oSelect) {
    var sValue = '';
    var iIndex = oSelect.selectedIndex;
    if (iIndex > -1) {
        var oOption = oSelect.options[iIndex];
        if (oOption && oOption.value != ' ' && oOption.value != '0000') {
            sValue = oOption.value;
        }
    }
    return sValue;
}

// renvoie la valeur du champ input
function GetInputValue(oInput) {
    return oInput.value;
}

// renvoie la valeur du bouton radio
function GetRadioValue(aRadio) {
    var sValue = '';
    for (var i = 0; i < aRadio.length; i++) {
        if (aRadio[i].checked) {
            sValue = aRadio[i].value;
        }
    }
    return sValue;
}

// renvoie la valeur de la checkbox
function GetCheckboxValue(oCheckbox) {
    var sValue = '';
    if (oCheckbox.checked) {
        sValue = oCheckbox.value;
    }
    return sValue;
}

// wrapper pour un champ de formulaire
function Field(oHtmlField) {
    // champ HTML
    this.oHtmlField = oHtmlField;

    // renvoie le nom du champ
    this.GetName = function () {
        var sName = oHtmlField.name;
        if (!sName) {
            sName = oHtmlField[0].name; // radio bouton
        }
        return sName;
    }

    // renvoie le type de champ
    this.GetType = function () {
        var sType = oHtmlField.type;
        if (!sType) {
            sType = oHtmlField[0].type; // radio bouton
        }
        return sType;
    }

    // renvoie la valeur du champ
    this.GetValue = function () {
        var sValue = '';
        if (this.GetType() == 'select-one') {
            sValue = GetSelectValue(this.oHtmlField);
        }
        else if (this.GetType() == 'text' || this.GetType() == 'hidden') {
            sValue = GetInputValue(this.oHtmlField);
        }
        else if (this.GetType() == 'radio') {
            sValue = GetRadioValue(this.oHtmlField);
        }
        else if (this.GetType() == 'checkbox') {
            sValue = GetCheckboxValue(this.oHtmlField);
        }
        return sValue;
    }
}

// renvoie la requête générée pour le formulaire
function GetQuery(oForm, aFieldNames) {
    var sQuery = '';
    var oField = null;
    var oField2 = null;
    for (var i = 0; i < aFieldNames.length; i++) {
        oField = GetField(oForm, aFieldNames[i]);

        // verrue pour CENTRES_INTERETS
        if (oField && oField.GetName() == 'THEMATIQUE') {
            oField2 = GetField(oForm, 'CENTRE_INTERETS');
            if (oField2 && oField2.GetValue() == '1') {
                if (sQuery.length > 0)
                    sQuery += '&';
                sQuery += 'THEMATIQUE=[centresinteret]';
                continue;
            }
        }
        // fin de verrue

        if (oField && oField.GetValue() != '') {

            if (sQuery.length > 0)
                sQuery += '&';

            sQuery += oField.GetName() + '=' + oField.GetValue();

            // verrue pour NOARBO_RUBRIQUE
            if (oField.GetName() == 'CODE_RUBRIQUE') {
                oField = GetField(oForm, 'NOARBO_RUBRIQUE');
                if (oField && oField.GetValue() == '1') {
                    sQuery += '_NOARBO';
                }
            }
            // fin de verrue

            // verrue pour NOARBO_CODE_RATTACHEMENT
            if (oField && oField.GetName() == 'CODE_RATTACHEMENT') {
                oField = GetField(oForm, 'NOARBO_CODE_RATTACHEMENT');
                if (oField && oField.GetValue() == '1') {
                    sQuery += '_NOARBO';
                }
            }
            // fin de verrue

            // verrue pour NOARBO_CODE_STRUCTURE
            if (oField && oField.GetName() == 'CODE_STRUCTURE') {
                oField = GetField(oForm, 'NOARBO_CODE_STRUCTURE');
                if (oField && oField.GetValue() == '1') {
                    sQuery += '_NOARBO';
                }
            }
            // fin de verrue


        }
    }

    // ajout systématique de la DSI
    oField = null;
    if (oForm.GROUPE_DSI) {
        oField = GetField(oForm, 'GROUPE_DSI');
    }
    if (oField && oField.GetValue() != '') {
        if (sQuery.length > 0)
            sQuery += '&';
        sQuery += oField.GetName() + '=' + oField.GetValue().replace(/,/g, '@');
    }
    else {
        oField = oForm.GROUPE_DSI_COURANT;
        if (oField) {
            if (oField[0].checked || oField[1].checked || oField[2].checked) {
                if (sQuery.length > 0)
                    sQuery += '&';
                if (oField[0].checked)
                    sQuery += 'GROUPE_DSI=DYNAMIK';
                else if (oField[1].checked)
                    sQuery += 'GROUPE_DSI=[groupe]';
                else if (oField[2].checked) {
                    sQuery += 'GROUPE_DSI=' + oForm.TYPE_GROUPE_DSI.options[oForm.TYPE_GROUPE_DSI.selectedIndex].value + '_TYPEGROUPE';
                }
            }
        }
    }

    // AJOUT SYSTEMATIQUE LANGUE ET NOMBRE
    oField = null;
    if (oForm.LANGUE) {
        oField = GetField(oForm, 'LANGUE');
        if (oField && oField.GetValue() != '') {
            if (sQuery.length > 0)
                sQuery += '&';

            sQuery += oField.GetName() + '=' + oField.GetValue();
        }
    }

    oField = null;
    if (oForm.NOMBRE) {
        oField = GetField(oForm, 'NOMBRE');
        if (oField && oField.GetValue() != '') {
            if (sQuery.length > 0)
                sQuery += '&';

            sQuery += oField.GetName() + '=' + oField.GetValue();
        }
    }


    return sQuery;
}
