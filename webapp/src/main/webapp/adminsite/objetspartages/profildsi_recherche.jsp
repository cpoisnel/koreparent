<%@page import="java.util.Collection"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@ page import="com.univ.objetspartages.bean.ProfildsiBean" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
%>
<div id="content" role="main">
    <table class="datatable">
        <thead>
            <tr>
                <th> <%=module.getMessage("BO_INTITULE") %> </th>
                <th> <%=module.getMessage("BO_LIBELLE_CODE") %> </th>
                <th class="sanstri sansfiltre"> <%=module.getMessage("BO_ACTIONS") %> </th>
            </tr>
        </thead>
        <tbody><%
        Collection<ProfildsiBean> listeProfils = infoBean.getCollection("LISTE_PROFILDSI");
        for (ProfildsiBean profil : listeProfils) {
            %><tr>
                <td><%= profil.getLibelle() %></td>
                <td><%= profil.getCode() %></td>
                <td>
                    <a href="<%=ProcessusHelper.getUrlProcessAction(infoBean, StringUtils.EMPTY , StringUtils.EMPTY, "MODIFIERPARID", new String[][]{{"ID_PROFIL",String.valueOf(profil.getIdProfildsi())}})%>"><%=module.getMessage("BO_MODIFIER") %></a> |
                    <a data-confirm="<%=module.getMessage("BO_CONFIRM_SUPPR_PROFIL") %>" href="<%=ProcessusHelper.getUrlProcessAction(infoBean, StringUtils.EMPTY , StringUtils.EMPTY, "SUPPRIMERPARID", new String[][]{{"ID_PROFIL",String.valueOf(profil.getIdProfildsi())}})%>"><%=module.getMessage("BO_SUPPRIMER") %></a>
                </td>
            </tr><%
        }
        %></tbody>
    </table>
</div><!-- #content -->