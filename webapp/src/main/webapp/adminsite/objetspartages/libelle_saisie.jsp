<%@page import="java.util.Hashtable"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.core.InfoBean"%>
<%@page import="com.jsbsoft.jtf.core.LangueUtil"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
boolean isAjout = InfoBean.ACTION_AJOUTER.equals(infoBean.get("ACTION_DEMANDEE"));
int typeSaisie = FormateurJSP.SAISIE_AFFICHAGE;
if (isAjout) {
    typeSaisie = FormateurJSP.SAISIE_OBLIGATOIRE;
}
%><form action="/servlet/com.jsbsoft.jtf.core.SG">
    <div id="actions">
        <div>
            <ul><%
                if (!isAjout) {%>
                    <li><button class="supprimer" type="button" data-confirm="<%=module.getMessage("BO_CONFIRM_SUPPR_LIBELLE") %>" type="submit" name="SUPPRIMER" value="<%= module.getMessage("JTF_BOUTON_SUPPRIMER")%>"><%=module.getMessage("JTF_BOUTON_SUPPRIMER") %></button></li><%
                }
                %><li><button class="enregistrer" type="submit" name="VALIDER" value="<%= module.getMessage("BO_ENREGISTRER")%>"><%= module.getMessage("BO_ENREGISTRER")%></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= module.getMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->
    <br/>
    <div id="content" role="main">
        <% fmt.insererVariablesCachees(out, infoBean); %>
        <input type="hidden" name="ID_LIBELLE" value="<%= infoBean.getString("ID_LIBELLE") %>" />
        <input type="hidden" name="ACTION" value="<%= infoBean.get("ACTION") %>" />
        <div class="fieldset neutre">
            <p>
                <strong class="label colonne"><%= module.getMessage("BO_TYPE") %> (*)</strong>
                <%fmt.insererComboHashtable(out, infoBean, "TYPE", typeSaisie, "LISTE_TYPES_LIBELLES");%>
            </p>
            <p>
                <label for="CODE" class="colonne"><%= module.getMessage("BO_LIBELLE_CODE") %> (*)</label>
                <%fmt.insererChampSaisie(out, infoBean, "CODE", typeSaisie, FormateurJSP.FORMAT_TEXTE, 0, 8,50,"-"); %>
            </p><%
             Hashtable<String, String> langues = (Hashtable<String, String>) infoBean.get("LISTE_LANGUES");
            boolean isFirstLine = true;
            SortedSet<String> keys = new TreeSet<String>(langues.keySet());
            String labelKey = (langues.keySet().size() > 1 ? "BO_LIBELLES" : "BO_LIBELLE");
            for (String langue: keys) { %>
                <p><%
                    if (isFirstLine) {
                        %><label for="LIBELLE#<%= langue %>" class="colonne"><%= module.getMessage(labelKey) %> (*)</label><%
                        isFirstLine = false;
                    } else {
                        %><label for="LIBELLE#<%= langue %>" class="colonne"></label><%
                    }%>
                    <input type="text" maxlength="255" name="LIBELLE#<%= langue %>" id="LIBELLE#<%= langue %>" size="50" value="<%= infoBean.getString("LIBELLE#" + langue) %>" />
                    <img src="<%= LangueUtil.getPathImageDrapeau(langue) %>" title="<%= langues.get(langue) %>" />
                </p><%
            } %>
        </div><!-- .fieldset .neutre -->
    </div><!-- #content -->
</form>