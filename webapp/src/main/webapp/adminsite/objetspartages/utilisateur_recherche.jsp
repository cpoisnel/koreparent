<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.core.webapp.WebAppUtil"%>
<%@page import="com.univ.utils.UnivFmt"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%
String mode = StringUtils.defaultString(infoBean.getString("MODE"));
%><div id="content" role="main">
<form action="<%= WebAppUtil.SG_PATH %>" data-no-tooltip >
    <div class="fieldset neutre"><%
        fmt.insererVariablesCachees( out, infoBean);
        if (infoBean.get("LISTE") != null ) { %>
            <input type="hidden" name="LISTE" value="<%= infoBean.getString("LISTE")%>"><%
        }
        if (mode.equals("GESTION")){
            %><p>
                <label for="CODE" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_IDENTIFIANT") %></label><%
                fmt.insererChampSaisie( out, infoBean, "CODE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 64); %>
            </p><%
        }
    %><p>
        <label for="NOM" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_NOM") %></label><%
        fmt.insererChampSaisie( out, infoBean, "NOM", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 20); %>
    </p>
    <p>
        <label for="PRENOM" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_PRENOM") %></label><%
        fmt.insererChampSaisie( out, infoBean, "PRENOM", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 20); %>
    </p>
    <p>
        <label for="ADRESSE_MAIL" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_ADRESSE_MEL") %></label><%
        fmt.insererChampSaisie(out, infoBean, "ADRESSE_MAIL", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255); %>
    </p><%
    if (mode.equals("GESTION") && ( UnivFmt.getModeRechercheZone(infoBean, "UTILISATEUR.CODE_RATTACHEMENT" ) != -1 ) ){ %>
        <p>
            <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE);%>
        </p><%
    }
    if(UnivFmt.getModeRechercheZone(infoBean, "UTILISATEUR.PROFIL_DSI") != -1 ) {
        %><p><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_PROFIL_OU_GROUPE") %></p>
        <p>
            <label for="PROFIL_DSI" class="colonne"><%= MessageHelper.getCoreMessage("BO_PROFIL") %></label><%
            fmt.insererComboHashtable(out, infoBean, "PROFIL_DSI", FormateurJSP.SAISIE_FACULTATIF, "LISTE_PROFILS_DSI"); %>
        </p><%
    }
    %><p>
        <%univFmt.insererkMonoSelect(fmt, out, infoBean, "GROUPE_DSI", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_GROUPE"), "", UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION);%>
    </p>
    </div><%
    fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_VALIDER} );
%></form>
</div><!-- #content -->