<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.core.webapp.WebAppUtil"%>
<%@page import="com.univ.utils.UnivFmt"%>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Enumeration" %>
<%@page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<script type="text/javascript">
    var arrayTypeRessources = [];
    function displayFieldsTypeRessource(sTypeRessource) {
        for (i=0; i<arrayTypeRessources.length;i++) {
            var iResource = arrayTypeRessources[i];
            if (sTypeRessource == iResource) {
                window.document.getElementById("media1-"+iResource.toLowerCase()).style.display = 'block';
                window.document.getElementById("media2-"+iResource.toLowerCase()).style.display = 'block';
            }
            else {
                window.document.getElementById("media1-"+iResource.toLowerCase()).style.display = 'none';
                window.document.getElementById("media2-"+iResource.toLowerCase()).style.display = 'none';
            }
        }
    }
</script>
<div id="content" role="main">

    <form action="<%= WebAppUtil.SG_PATH %>" method="get" data-no-tooltip>
        <div class="fieldset neutre">
            <% fmt.insererVariablesCachees( out, infoBean); %>
            <input type="hidden" name="ACTION" value="" />
            <% if (infoBean.getString("MODE") != null) { %>
                <input type="hidden" name="MODE" value="<%= infoBean.get("MODE") %>" />
            <% } %>
            <% if (infoBean.getString("MODE_FICHIER") != null) { %>
                <input type="hidden" name="MODE_FICHIER" value="<%= infoBean.get("MODE_FICHIER") %>" />
            <% } %>
            <% if (infoBean.getString("CODE") != null) { %>
                <input type="hidden" name="CODE" value="<%= infoBean.getString("CODE")%>" />
            <% } %>
            <% if (infoBean.getString("OBJET") != null) { %>
                <input type="hidden" name="OBJET" value="<%= infoBean.getString("OBJET")%>" />
            <% } %>
            <% if (infoBean.getString("TYPE_RESSOURCE") != null) { %>
                <input type="hidden" name="TYPE_RESSOURCE" value="<%= infoBean.getString("TYPE_RESSOURCE")%>" />
            <% } %>
            <% if (infoBean.getString("ID_FICHE") != null) { %>
                <input type="hidden" name="ID_FICHE" value="<%= infoBean.getString("ID_FICHE")%>" />
            <% } %>
            <% if (infoBean.get("NO_FICHIER") != null) { %>
                <input type="hidden" name="NO_FICHIER" value="<%= infoBean.get("NO_FICHIER") %>" />
            <% } %>
            <% if (infoBean.getString("SAISIE_FRONT") != null) { %>
                <input type="hidden" name="SAISIE_FRONT" value="<%= infoBean.get("SAISIE_FRONT") %>" />
            <% } %>

            <%if (infoBean.getString("TYPE_RESSOURCE")==null){
                univFmt.insererComboHashtable(fmt, out, infoBean, "TYPE_RESSOURCE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_RESSOURCES", MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.TYPE_MEDIA"));
            %>
                <%
                Hashtable hType = (Hashtable) infoBean.get("LISTE_TYPE_RESSOURCES");
                Enumeration<String> en = hType.keys();
                String sKeyType = "";
                int iRessource = -1;
                while (en.hasMoreElements()){
                    iRessource++;
                    sKeyType = en.nextElement();%>
                    <p data-type="<%=sKeyType.toLowerCase()%>" style="display:none;">
                        <label for="TYPE_MEDIA_<%=sKeyType.toUpperCase()%>" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.TYPE") %> <%=((String)hType.get(sKeyType)).toLowerCase()%></label>
                        <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_MEDIA_"+sKeyType.toUpperCase(), FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_MEDIA_"+sKeyType.toUpperCase(), "", "");%>
                    </p>
                <%
                }
                %>
            <% } else { %>
                    <%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.TYPE_MEDIA") %>
                    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LIBELLE_TYPE_RESSOURCE", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.TYPE_RESSOURCE"));%>
                    <input type="hidden" name="TYPE_RESSOURCE" value="<%= infoBean.getString("TYPE_RESSOURCE")%>">
                    <%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.TYPE") %> <%=infoBean.getString("LIBELLE_TYPE_RESSOURCE").toLowerCase()%>
                    <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_MEDIA_"+infoBean.getString("TYPE_RESSOURCE"), FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_MEDIA_"+infoBean.getString("TYPE_RESSOURCE"), "", "");%>
            <% }
                univFmt.insererChampSaisie( fmt, out, infoBean, "TITRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.TITRE"));
                univFmt.insererChampSaisie( fmt, out, infoBean, "LEGENDE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.LEGENDE"));
                univFmt.insererChampSaisie( fmt, out, infoBean, "DESCRIPTION", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.DESCRIPTION"));
                univFmt.insererChampSaisie( fmt, out, infoBean, "AUTEUR", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.AUTEUR"));
                univFmt.insererChampSaisie( fmt, out, infoBean, "COPYRIGHT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.COPYRIGHT"));
                univFmt.insererChampSaisie( fmt, out, infoBean, "META_KEYWORDS", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.MOTS_CLES"));
                univFmt.insererComboHashtable(fmt, out, infoBean, "THEMATIQUE", FormateurJSP.SAISIE_FACULTATIF,"LISTE_THEMATIQUES", MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.THEMATIQUE"), "");
                univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.STRUCTURE"), "", UnivFmt.CONTEXT_STRUCTURE);
                univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE);
                univFmt.insererRechercheUtilisateur(fmt, out, infoBean, "CODE_REDACTEUR",MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.REDACTEUR"));
                univFmt.insererChampSaisie( fmt, out, infoBean, "DATE_CREATION", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.CREE_DEPUIS"));
                univFmt.insererChampSaisie( fmt, out, infoBean, "POIDS_MINIMUM", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 20, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.TAILLE_MIN"));
                univFmt.insererChampSaisie( fmt, out, infoBean, "POIDS_MAXIMUM", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 20, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.TAILLE_MAX"));
                %><label class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.NB_MEDIA_PAGE") %></label><%
                    String increment = "20";
                    if(infoBean.get("INCREMENT") != null) {
                        increment = infoBean.getString("INCREMENT");
                    }
                %><select name="INCREMENT">
                    <option value="10" <% if("10".equals(increment)) { %>selected="selected"<% } %>>10</option>
                    <option value="20" <% if("20".equals(increment)) { %>selected="selected"<% } %>>20</option>
                    <option value="30" <% if("30".equals(increment)) { %>selected="selected"<% } %>>30</option>
                    <option value="40" <% if("40".equals(increment)) { %>selected="selected"<% } %>>40</option>
                    <option value="50" <% if("50".equals(increment)) { %>selected="selected"<% } %>>50</option>
                </select>
            <div class="validation">
                <input class="bouton" type="submit" name="RECHERCHER"  value="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.RECHERCHE.RECHERCHER") %>" />
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    function updateUI(){
        var combos = document.querySelectorAll('p[data-type]'),
                select = document.getElementById('TYPE_RESSOURCE');
        if(select) {
            var selectedCombo = document.querySelector('p[data-type="' + select.value + '"]');
            if(selectedCombo) {
                for (var i = 0; i < combos.length; i++) {
                    combos[i].style.display = 'none';
                }
                selectedCombo.style.display = 'block';
            }
        }
    }

    function bindSelect() {
        var select = document.getElementById('TYPE_RESSOURCE');
        if(select) {
            select.onchange = updateUI;
        }
    }

    updateUI();
    bindSelect();
</script>