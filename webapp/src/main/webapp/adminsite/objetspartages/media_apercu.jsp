<%@ page import="org.slf4j.LoggerFactory,com.kportal.core.config.MessageHelper" errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />

<div id="content" role="main">
    <!-- APERCU -->
    <%  ServletContext context = getServletConfig().getServletContext();
        try{
            out.flush();
            context.getRequestDispatcher(infoBean.getString("TEMPLATE_JSP_APERCU")).include(request, response);
            out.flush();
        }
        catch (Exception e) {
            LoggerFactory.getLogger(this.getClass()).error("Erreur lors de l'aperçu", e);
        }
    %>
    <!-- CHEMIN -->
    <p>
        <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.APERCU.CHEMIN") %></span>
        <span><%= infoBean.getString("SOURCE") %></span>
    </p>

    <!-- TYPE DE RESSOURCE -->
    <p>
        <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.APERCU.TYPE_RESSOURCE") %></span>
        <span><%= infoBean.getString("TYPE_RESSOURCE") %></span>
    </p>

    <!-- TYPE DE MEDIA -->
    <% if (infoBean.getString("LIBELLE_TYPE_MEDIA").length()>0) { %>
    <p>
        <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.APERCU.TYPE_MEDIA") %></span>
        <span><%= infoBean.getString("LIBELLE_TYPE_MEDIA") %></span>
    </p>
    <% } %>
    <!-- POIDS -->
    <%
    final Integer poids = infoBean.getInt("POIDS");
    if (poids > 0) { %>
    <p>
        <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.APERCU.POIDS") %></span>
        <span><%= poids %> ko</span>
    </p>
    <% } %>

    <!-- FORMAT -->
    <% if (infoBean.getString("FORMAT").length()>0) { %>
    <p>
        <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.APERCU.FORMAT") %></span>
        <span><%= infoBean.getString("FORMAT") %></span>
    </p>
    <% } %>
    <input class="bouton" type="button" name="RETOUR" value="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.APERCU.RETOUR") %>" onclick="history.back();return false;" id="return"/>
</div>