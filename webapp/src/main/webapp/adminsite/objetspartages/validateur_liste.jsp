<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<!-- link rel="stylesheet" type="text/css" href="/adminsite/general.css" -->
</head>


<body>

<h2><%= MessageHelper.getCoreMessage("BO_VALIDATEUR_SELECTION_VALIDATEURS") %> <%=infoBean.getString("LIBELLE_APPROBATION").toLowerCase()%></h2>

<form action="/servlet/com.jsbsoft.jtf.core.SG">

<% fmt.insererVariablesCachees( out, infoBean); %>

<div class="content_popup">
<p style="text-align:center">

&nbsp;<%= MessageHelper.getCoreMessage("BO_VALIDATEUR_TOUS_VALIDATEUR") %>
</p>
<table cellpadding="0" cellspacing="0" border="0">

<% 
int total = infoBean.getInt("NB_ITEMS_VALIDATEUR");
if (total==0) { %>
<tr>                                            
    <td>&nbsp;</td>
    <td><%= MessageHelper.getCoreMessage("BO_VALIDATEUR_AUCUN_VALIDATEUR") %></td>
</tr>
<% } else { %>
<tr>                                            
    <td>&nbsp;</td>
    <td><a href="#" onclick="selectAll()"><%=MessageHelper.getCoreMessage("ST_TOUS") %></a></td>
</tr>
<%}
for (int i=0; i<total; i++) { %>
<tr>                                            
    <td>
        <%= infoBean.getString("NOM_VALIDATEUR_"+i) %>
    </td>
    <td>
        <input type="checkbox" name="VALIDATEUR_<%=i%>" value=""/>
    </td>
</tr>
<% } %>

</table>
</div>

<% fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_VALIDER, FormateurJSP.BOUTON_ANNULER} );%>

<script type="text/javascript">
var checked= false;
function selectAll(){
     var total = <%=((Integer) infoBean.get("NB_ITEMS_VALIDATEUR")).intValue()%>;
        for (i=0;i<total;i++){
            if (eval("window.document.forms['0'].VALIDATEUR_"+i+"")){
                if (checked){
                        eval("window.document.forms['0'].VALIDATEUR_"+i+".checked=false");
                }else{
                        eval("window.document.forms['0'].VALIDATEUR_"+i+".checked=true");
                }
            }
        }
        if (! checked)
            checked=true;
        else
            checked=false;
}
</script>

</form>
</body>
</html>