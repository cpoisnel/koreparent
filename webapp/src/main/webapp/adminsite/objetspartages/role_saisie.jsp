<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.ProcessusHelper "%>
<%@ page import="com.kportal.core.webapp.WebAppUtil" %>
<%@ page import="com.kportal.extension.module.composant.IComposant" %>
<%@ page import="com.univ.collaboratif.om.Espacecollaboratif" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /> 
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /> 
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
%>
<script type="text/javascript">
var kMonoSelectPlaceHolder = { 'f' : '<%= module.getMessage("ST_TOUTES")%>', 'm' : '<%= module.getMessage("ST_TOUS")%>'};
function switchOnglets(tab) {
    var $ = jQuery,
        $activeTab = $('#onglets').find('.active'),
        $tab = $(tab);
    $('#' + $activeTab.data('targetdivid')).addClass("masquer");
    $activeTab.removeClass('active');
    $tab.addClass('active');
    $('#' + $tab.data('targetdivid')).removeClass("masquer");
}
</script>
<form action="<%= WebAppUtil.SG_PATH %>" enctype="multipart/form-data" method="post">
    <div id="actions">
        <div>
            <ul><%
                if ("MODIFIER".equals(infoBean.getActionUtilisateur())) {%>
                    <li><button class="supprimer" data-confirm="<%= module.getMessage("BO_CONFIRM_SUPPR_ROLE") %>" type="submit" name="SUPPRIMER" value="<%= module.getMessage("JTF_BOUTON_SUPPRIMER")%>"><%= module.getMessage("JTF_BOUTON_SUPPRIMER")%></button></li><%
                }
                %><li><button class="enregistrer" type="submit" name="VALIDER" value="<%= module.getMessage("BO_ENREGISTRER")%>" ><%= module.getMessage("BO_ENREGISTRER")%></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= module.getMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->
    <ul id="onglets">
         <li id="li-contenu" class="active" data-targetdivid="permissions" onclick="switchOnglets(this)"><%= module.getMessage("BO_ROLE_PERMISSIONS")%></li>
         <li id="li-contribution-avancee" data-targetdivid="perimetres" onclick="switchOnglets(this)"><%= module.getMessage("BO_ROLE_PERIMETRE")%></li>
    </ul>

    <div id="content" role="main">
        <input type="hidden" name="ACTION" value="" />
        <% fmt.insererVariablesCachees( out, infoBean); %>
        <div id="permissions">
            <div class="fieldset neutre">
                <p>
                    <strong class="label colonne"><%= module.getMessage("BO_LIBELLE_CODE") %> (*)</strong>
                    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "CODE", UnivFmt.getModeSaisieCode(infoBean), FormateurJSP.FORMAT_TEXTE, 0, 20, module.getMessage("BO_LIBELLE_CODE") );%>
                </p>
                <p>
                    <label for="LIBELLE" class="colonne"><%= module.getMessage("BO_INTITULE") %> (*)</label>
                    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LIBELLE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255, module.getMessage("BO_INTITULE"));%>
                </p>
            </div><!-- .fieldset .neutre -->
            <ul>
<%-- Liste des permissions par fiches --%><%
    int indiceOnglet = -1;
    boolean collaboratif = false;
    for (int iFiche=0; iFiche <infoBean.getInt("PERMISSIONS_NB_ITEMS"); iFiche++) {
        boolean nouvelleSection = infoBean.getString("PERMISSION_TITRE_" + iFiche) != null;
        boolean nouvelOnglet = infoBean.getString("PERMISSION_ONGLET_" + iFiche) != null;
        if(nouvelOnglet){
            indiceOnglet++;
            collaboratif = "collaboratif".equalsIgnoreCase(infoBean.getString("PERMISSION_ONGLET_" + iFiche));
        } else {
            collaboratif = collaboratif && indiceOnglet > infoBean.getInt("FIN_ONGLET_"+indiceOnglet);
        }
        // test sur la section
        if (nouvelOnglet){
            if (iFiche!=0){
                %> </table><%
                if(nouvelleSection) {
                    %></div>
                    </li><%
                }
            }
            %><li id="li-<%=infoBean.getString("PERMISSION_ONGLET_" + iFiche)%>" class="rights_wrapper">
                <div class="rights_informations">
                    <img class="icon" alt="right-<%=infoBean.getString("PERMISSION_ONGLET_" + iFiche)%>" src="<%=module.getProperty(String.format("ROLES.ICON.%s", infoBean.getString("PERMISSION_ONGLET_" + iFiche).toUpperCase())) %>"/>
                    <div class="infos">
                        <p class="title"><%=module.getMessage(String.format("ROLES.TITRE.%s", infoBean.getString("PERMISSION_ONGLET_" + iFiche).toUpperCase())) %></p>
                        <p class="description"><%=module.getMessage(String.format("ROLES.DESCRIPTION.%s", infoBean.getString("PERMISSION_ONGLET_" + iFiche).toUpperCase())) %></p>
                    </div>
                    <div class="rights_action">
                        <button type="button" class="details deplier"><%= module.getMessage("BO_DETAILS") %></button>
                    </div>
                </div>
                <div class="rights collapsed">
            <% if (collaboratif) { %>
                <p class="message alert alert-warning"><%= module.getMessage("BO_ROLE_ATTENTION_DROIT_COLLAB") %></p>
            <% }
        }
        if (nouvelleSection) {
            %> </table><%
            if (infoBean.getString("PERMISSION_TITRE_" + iFiche).length()>0) { %>
                <h3><%= infoBean.getString("PERMISSION_TITRE_" + iFiche)%></h3><%
            }
            if ("Gestion éditoriale".equalsIgnoreCase(infoBean.getString("PERMISSION_TITRE_" + iFiche))) { %>
                <p class="message alert alert-info"><%= module.getMessage("BO_ROLE_APPLICATION_PERIMETRE") %></p>
            <% } %>
            <table class="roles_table k_fixed_thead"><%
        }
        // on boucle sur les libelles des actions pour voir tester affichage
        boolean afficherTitre = false;
        for (int j=0; j<infoBean.getInt("PERMISSIONS_NB_ACTIONS_"+iFiche); j++)    {
            if( infoBean.get("PERMISSION_ACTION_LIBELLE_" + iFiche + "_"+ j) != null) {
                afficherTitre = true;
            }
        }
        // ajout de la ligne de titre des actions
        if (afficherTitre) { %>
            <thead>
            <% if (iFiche == 0) { %>
                <tr>
                    <th></th>
                    <th colspan="7"><%= module.getMessage("BO_ROLE_DROITS_TOUT_CONTENU") %></th>
                    <th colspan="2"><%= module.getMessage("BO_ROLE_DROITS_FICHE_REDACTEUR") %></th>
                </tr>
            <% } %>
                <tr>
                    <th></th>
                    <% if (infoBean.getString("PERMISSION_TYPE_"+iFiche).equals("FICHE")) { %>
                        <th>Tous</th>
                    <% }
                       for (int j=0; j< infoBean.getInt("PERMISSIONS_NB_ACTIONS_"+iFiche); j++){
                            if( infoBean.get("PERMISSION_ACTION_LIBELLE_" + iFiche + "_"+ j) != null)    {    %>
                                <th><%= infoBean.get("PERMISSION_ACTION_LIBELLE_" + iFiche + "_"+ j)%></th>
                            <% } %>
                    <% } %>
                </tr>
            </thead><%
        }
        // affichage de la ligne
        %><tr>
            <td>
                <%= infoBean.getString("PERMISSION_INTITULE_"+iFiche) %>
                <% if( infoBean.getString("PERMISSION_HORS_PERIMETRE_" + iFiche ).equals("1")) { %>(*)<% } %>
            </td><%
            if (infoBean.getString("PERMISSION_TYPE_"+iFiche).equals("FICHE") ) { %>
                <td>
                    <input type="checkbox" name="CHECK_<%=iFiche%>" value="" data-check="all"/>
                </td><%
            }
            int k = 0;
            int j = 0;
            for (j=0; j< infoBean.getInt("PERMISSIONS_NB_ACTIONS_"+iFiche); j++) {
                %><td>
                    <% if( infoBean.get("PERMISSION_ACTION_" + iFiche + "_"+ j) != null) {
                        if ("1".equals(infoBean.getString("PERMISSION_ACTION_SELECT_" + iFiche + "_"+ j))) {
                            k++;
                        }
                        %>
                        <%fmt.insererChampSaisie( out, infoBean, "PERMISSION_ACTION_SELECT_" + iFiche + "_"+ j, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
                        <%=(infoBean.get("PERMISSION_ACTION_DISABLED_"+ iFiche +"_"+ j)!=null ? "<script type=\"text/javascript\">document.forms[0].PERMISSION_ACTION_SELECT_" + iFiche + "_"+ j+".disabled = true;arrayDisable[arrayDisable.length]='document.forms[0].PERMISSION_ACTION_SELECT_" + iFiche + "_"+ j+"';</script>" : "")%>

                    <% } %>
                </td><%
            }
            if(k==j) { %>
                <script type="text/javascript">
                    if (eval("document.forms[0].CHECK_<%=iFiche%>"))
                    {
                        eval("document.forms[0].CHECK_<%=iFiche%>.checked=true;");
                    }
                </script>
            <% } %>
        </tr><%-- fin affichage de la ligne --%>
<% } %><%-- Fin liste des permissions par fiche --%>
</table>
</div>
</li></ul>
</div><!-- #permissions -->

<div id="perimetres" class="masquer">
<% if (Espacecollaboratif.isExtensionActivated()) { %>
    <div class="fieldset neutre">
        <%fmt.insererChampSaisie( out, infoBean, "ESPACE_COLLABORATIF_COURANT_PERIMETRE", FormateurJSP.SAISIE_FACULTATIF , FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
        <label for="ESPACE_COLLABORATIF_COURANT_PERIMETRE"><%= module.getMessage("BO_ROLE_UNIQUEMENT_ESPACE") %></label>
    </div>
<% } %>
    <div id="perimetre-rubrique">
        <p class="message alert alert-info"><%= module.getMessage("BO_ROLE_DROITS_PERMISSIONS_PERIMETRE") %><br /></p>
        <div class="boite">
            <div>
                <label class="colonne"><%= module.getMessage("BO_RUBRIQUE") %></label>
                <% univFmt.insererkMonoSelect(fmt, out, infoBean, "RUBRIQUE_PERIMETRE", FormateurJSP.SAISIE_FACULTATIF, "", "rubrique", UnivFmt.CONTEXT_ZONE);%>
            </div>
        </div>
        <div class="boite">
            <div>
                <label class="colonne"><%= module.getMessage("ST_CODE_RATTACHEMENT") %></label>
                <% univFmt.insererkMonoSelect(fmt, out, infoBean, "STRUCTURE_PERIMETRE", FormateurJSP.SAISIE_FACULTATIF, "", "", UnivFmt.CONTEXT_STRUCTURE);%>
            </div>
        </div>
    </div>
    <div id="perimetre-groupe">
        <p class="message alert alert-info"><%= module.getMessage("BO_ROLE_DROITS_RESTREINT_GROUPE") %></p>
        <div class="boite">
            <label class="colonne"><%= module.getMessage("BO_GROUPE") %></label>
            <% univFmt.insererkMonoSelect(fmt, out, infoBean, "GROUPE_PERIMETRE", FormateurJSP.SAISIE_FACULTATIF, "", "", UnivFmt.CONTEXT_GROUPEDSI_PUBLIC_VISE);%>
        </div>
    </div>
</div><!-- #perimetres -->

</div><!-- #content -->
</form>