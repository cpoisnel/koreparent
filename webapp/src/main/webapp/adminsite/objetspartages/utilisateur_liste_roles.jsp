<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.core.webapp.WebAppUtil"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<div id="content" role="main">
<form action="<%= WebAppUtil.SG_PATH %>" method="post">
    <div class="content_popup"><%
        fmt.insererVariablesCachees( out, infoBean);
        %><input type="hidden" name="PROC" value="SAISIE_UTILISATEUR" />
        <input type="hidden" name="ACTION" value="LISTE_ROLES" /><%
        int nbUtilisateursAvecRoles = infoBean.getInt("NB_UTILISATEURS_ROLES");
        if (nbUtilisateursAvecRoles > 0) { %>
            <p><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_NBRE_UTILISATEUR_ROLE") %><strong><%=nbUtilisateursAvecRoles%></strong></p>
            <table>
            <tr>
                <th><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_CODE_UTILISATEUR") %></th>
                <th><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_ROLES_DIRECTS") %></th>
                <th><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_ROLE_HERITE") %></th>
            </tr><%
            for (int i = 0; i < nbUtilisateursAvecRoles; i++) { %>
                   <tr class="detail_ligne<%=i%2 +1 %>">
                       <td><%=infoBean.getString("UTILISATEUR#"+i)%></td>
                       <td><%=infoBean.getString("ROLES_DIRECTS#"+i) %></td>
                       <td><%=infoBean.getString("ROLES_HERITES#"+i) %></td>
                   </tr><%
               }
            %></table><%
        } else {
            %><p><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_AUCUN_UTILISATEUR") %></p><%
        }
    %></div> <!-- .content_popup -->
</form>
</div>