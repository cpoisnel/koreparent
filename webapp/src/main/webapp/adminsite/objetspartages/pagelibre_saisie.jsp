<%@ taglib prefix="components" uri="http://kportal.kosmos.fr/tags/components" %>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%
    // declaration des onglets
    String typeObjet = ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")).toUpperCase();
    Objetpartage module = ReferentielObjets.getObjetByNom(typeObjet);
    String[] nomOnglet = {"PRINCIPAL", "COMPLEMENTS"};
    String[] libelleOnglet = {module.getMessage("BO_ONGLET_CONTENU_PRINCIPAL"), module.getMessage("BO_ONGLET_INFOS_COMPLEMENTS")};
%>
<%@ include file="/adminsite/objetspartages/template/header_saisie.jsp" %><%
/**
***************************************************************************
************          ONGLET CONTENU PRINCIPAL              ***************
***************************************************************************
*/
if (sousOnglet.equals("PRINCIPAL")) {%>
    <div class="fieldset neutre">
        <%
    univFmt.insererChampSaisie(fmt, out, infoBean, "CODE", univFmt.getModeSaisieCode(infoBean), fmt.FORMAT_TEXTE, 0, 20, module.getMessage("BO_LIBELLE_CODE"));
    if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
        univFmt.insererComboHashtable(fmt, out, infoBean, "LANGUE", fmt.SAISIE_OBLIGATOIRE, "LISTE_LANGUES", module.getMessage("BO_LIBELLE_LANGUE"));
    }
    univFmt.insererChampSaisie(fmt, out, infoBean, "TITRE", fmt.SAISIE_OBLIGATOIRE, fmt.FORMAT_TEXTE, 0, 255, module.getMessage("BO_PAGELIBRE_TITRE"));
    %></div>
    <fieldset>
        <legend><%= module.getMessage("BO_FICHE_RATTACHEMENTS") %></legend><%
        univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", fmt.SAISIE_FACULTATIF, module.getMessage("BO_PAGELIBRE_CODE_RUBRIQUE"), "rubrique", univFmt.CONTEXT_ZONE);
        univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", fmt.SAISIE_FACULTATIF, module.getMessage("BO_PAGELIBRE_CODE_RATTACHEMENT"), "", univFmt.CONTEXT_STRUCTURE);
        boolean isParamSaisieBandeauStruct = "1".equals(module.getProperty("saisie.PAGELIBRE.RATTACHEMENT_BANDEAU"));
        if (isParamSaisieBandeauStruct && ReferentielObjets.getNombreObjetsTypeStructure()!=0) {
            univFmt.insererChampSaisie(fmt, out, infoBean, "RATTACHEMENT_BANDEAU", fmt.SAISIE_FACULTATIF, fmt.FORMAT_CHECKBOX, 0, 0, module.getMessage("BO_PAGELIBRE_BANDEAU"));
        }
    %></fieldset>
    <%--
       ********* PARAGRAPHES *************
    --%>
    <input type="hidden" name="PARAGRAPHES_NB_ITEMS" value="<%= infoBean.getString("PARAGRAPHES_NB_ITEMS") %>" /><%
    for (int i=0; i< Integer.parseInt(infoBean.getString("PARAGRAPHES_NB_ITEMS")); i++) {
        %><br />
        <fieldset>
            <legend><%= module.getMessage("BO_PAGELIBRE_ZONE") + (i+1) %></legend>
            <a name="PARAGRAPHE_<%=i%>"></a>
            <p>
                <label for="LIGNE_<%=i%>"><%= module.getMessage("BO_PAGELIBRE_LIGNE") %> </label>
                <%fmt.insererChampSaisie(out, infoBean, "LIGNE_"+i, fmt.SAISIE_OBLIGATOIRE, fmt.FORMAT_ENTIER, 0, 2, "LIB="+module.getMessage("BO_PAGELIBRE_LIGNE"));%>
                <label for="COLONNE_<%=i%>"><%= module.getMessage("BO_PAGELIBRE_COLONNE") %> </label>
                <%fmt.insererChampSaisie(out, infoBean, "COLONNE_"+i, fmt.SAISIE_OBLIGATOIRE, fmt.FORMAT_ENTIER, 0, 2, "LIB=" + module.getMessage("BO_PAGELIBRE_COLONNE"));%>
                <label for="LARGEUR_<%=i%>"><%= module.getMessage("BO_PAGELIBRE_LARGEUR") %> </label>
                <%fmt.insererChampSaisie(out, infoBean, "LARGEUR_"+i, fmt.SAISIE_OBLIGATOIRE, fmt.FORMAT_ENTIER, 0, 3, "LIB=" + module.getMessage("BO_PAGELIBRE_LARGEUR"));%>
                <input class="bouton" type="button" name="SUPPRIMER_PARAGRAPHE" value="<%= module.getMessage("BO_PAGELIBRE_SUPPRESSION_ZONE") %>" data-action="SUPPRIMER_PARAGRAPHE#<%= i %>" />
            </p>
            <components:toolbox fieldName='<%= String.format("CONTENU_%d", i) %>' label='<%= String.format("%s %d", module.getMessage("BO_PAGELIBRE_ZONE"), i+1) %>' editOption="<%= fmt.SAISIE_FACULTATIF %>" min="0" max="32000" />
            </fieldset><%
    }
    %>
    <p id="ajouter_zone"><input class="bouton" type="button" name="AJOUTER_PARAGRAPHE" value="<%= module.getMessage("BO_PAGELIBRE_AJOUT_ZONE") %>" data-action="AJOUTER_PARAGRAPHE" /></p><%
}
/*--
***************************************************************************
*********       ONGLET INFORMATIONS COMPLEMENTAIRES           *************
***************************************************************************
--*/
if (sousOnglet.equals("COMPLEMENTS")) {
    %><div class="fieldset neutre">
    <components:toolbox fieldName="COMPLEMENTS" label="BO_PAGELIBRE_COMPLEMENTS" min="0" max="16000" editOption='<%= FormateurJSP.SAISIE_FACULTATIF %>'/>
    </div><%
}
%><%@ include file="/adminsite/fiche_bas.jsp" %>
</form><!-- #saisie_objet -->
<script>
    var ligneId = '<%=infoBean.getString("FOCUS")%>';
    if(ligneId !== 'null'){
        document.getElementById('LIGNE_' + ligneId).focus();
    }
</script>