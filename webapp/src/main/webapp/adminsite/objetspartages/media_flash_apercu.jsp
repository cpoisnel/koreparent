<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />

<script type="text/javascript" src="/adminsite/utils/swfobject.js"></script>
<script type="text/javascript" src="/adminsite/utils/mediatheque/swf/player_swf.js"></script>
<script type="text/javascript" src="/adminsite/utils/mediatheque/commun/media.js"></script>

<div class="details-media-conteneur">
    <span id="container_swf_distant">
        <a style="max-width:300px; width: 100%; line-height: 200px; vertical-align: middle; display: inline-block;" href="<%=infoBean.getString("URL_RESSOURCE")%>">SWF ici</a>
    </span>

    <script type="text/javascript">
    //<![CDATA[
        var playerSWF = new SWFPlayer('container_swf_distant', 240, 200);
        playerSWF.ajouterMedia("<%=infoBean.getString("URL_RESSOURCE")%>", "");
        playerSWF.genererPlayer();
    //]]>
    </script>