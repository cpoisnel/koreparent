<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.core.InfoBean"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@ page import="com.kportal.core.webapp.WebAppUtil" %>
<%@ page import="com.kportal.extension.module.composant.IComposant" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /> 
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
%><form action="<%= WebAppUtil.SG_PATH%>" class="modification" method="post">
    <div id="actions">
        <div>
            <ul>
                <%if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) {%>
                    <li><button class="supprimer" data-confirm="<%= module.getMessage("BO_CONFIRM_SUPPR_SITE_EXTERNE") %>" type="submit" name="SUPPRIMER" value="<%= module.getMessage("JTF_BOUTON_SUPPRIMER")%>"><%= module.getMessage("JTF_BOUTON_SUPPRIMER")%></button></li>
                <%}%>
                <li><button class="enregistrer" type="submit" name="VALIDER" value="<%= module.getMessage("BO_ENREGISTRER") %>"><%= module.getMessage("BO_ENREGISTRER") %></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= module.getMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->
    <br/>
    <div id="content" role="main">
        <div class="fieldset neutre">
                <input type="hidden" name="ACTION" value="" />
            <% fmt.insererVariablesCachees(out, infoBean); %>
            <p>
                <label for="CODE" class="colonne"><%= module.getMessage("BO_LIBELLE_CODE") %> (*)</label>
                <%fmt.insererChampSaisie(out, infoBean, "CODE",  UnivFmt.getModeSaisieCode(infoBean), FormateurJSP.FORMAT_TEXTE, 0, 64); %>
            </p>
            <p>
                <label for="LIBELLE" class="colonne"><%= module.getMessage("BO_LIBELLE") %> (*)</label>
                <%fmt.insererChampSaisie(out, infoBean, "LIBELLE",  FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255); %>
            </p>
            <p>
                <label for="URL" class="colonne"><%= module.getMessage("BO_LIBELLE_URL") %> (*)</label>
                <%fmt.insererChampSaisie(out, infoBean, "URL",  FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255); %>
            </p>
            <p>
                <label for="NIVEAU_PROFONDEUR" class="colonne"><%= module.getMessage("BO_INDEXER_NIVEAU_PROFONDEUR") %> (*)</label>
                <%
                Integer niveauProfondeur = -1;
                if (infoBean.get("NIVEAU_PROFONDEUR") != null && infoBean.get("NIVEAU_PROFONDEUR") instanceof Integer) {
                    niveauProfondeur = (Integer)infoBean.get("NIVEAU_PROFONDEUR");
                }%>
                <select name="NIVEAU_PROFONDEUR" id="NIVEAU_PROFONDEUR">
                    <option value="-1" <%= (niveauProfondeur == -1) ? "selected=\"selected\"" : "" %>><%= module.getMessage("BO_INDEXER_TOUT_LE_SITE") %></option>
                    <option value="0" <%= (niveauProfondeur == 0) ? "selected=\"selected\"" : "" %>><%= module.getMessage("BO_INDEXER_PAGE_ACCUEIL") %></option>
                    <option value="1" <%= (niveauProfondeur == 1) ? "selected=\"selected\"" : "" %>><%= module.getMessage("BO_INDEXER_PAGES_REFERENCEES_ACCUEIL") %></option>
                    <option value="2" <%= (niveauProfondeur == 2) ? "selected=\"selected\"" : "" %>><%= module.getMessage("BO_INDEXER_PAGES_NIVEAU_UN_REFERENCES") %></option>
                </select>
            </p>
        </div>
    </div><!-- #content -->
</form>
