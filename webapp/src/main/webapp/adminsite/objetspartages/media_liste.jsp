<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.univ.objetspartages.processus.SaisieMedia"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%
    final boolean insertionMode = StringUtils.isNotBlank(infoBean.getString("MODE")) && infoBean.getString("MODE").equals("INSERTION");
    final boolean selectionMode = StringUtils.isNotBlank(infoBean.getString("MODE")) && infoBean.getString("MODE").equals("SELECTION");
    if (infoBean.get("REFERER") == null) {
        if (StringUtils.isNotEmpty(request.getHeader("referer"))) {
            infoBean.set("REFERER", request.getHeader("referer"));
        }
    }
%>
    <div id="content" role="main">

        <form action="/servlet/com.jsbsoft.jtf.core.SG">
        <% fmt.insererVariablesCachees( out, infoBean); %>
        <input type="hidden" name="ACTION" value="" />
        <input type="hidden" name="ID_MEDIA" value="" />
        <input type="hidden" name="HEIGHT" value="" />
        <input type="hidden" name="WIDTH" value="" />
        <% if (infoBean.get("FCK_PLUGIN") != null) { %>
            <input type="hidden" name="FCK_PLUGIN" value="<%= infoBean.get("FCK_PLUGIN") %>" />
        <% } %>
                <!-- Utile pour la restauration des données --><%
        if (infoBean.get("ID_CI_RESTAURATION") != null) {
            %><input type="hidden" name="ID_CI_RESTAURATION" value="<%=infoBean.getString("ID_CI_RESTAURATION")%>" />
            <input type="hidden" name="SINGLE_CI_RESTAURATION" value="true" />
            <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%=infoBean.getString("NOM_JSP_RESTAURATION")%>" /><%
        }
        %><input type="hidden" name="TITRE" value="<%= infoBean.getString("TITRE")%>">
        <input type="hidden" name="LEGENDE" value="<%= infoBean.getString("LEGENDE")%>">
        <input type="hidden" name="DESCRIPTION" value="<%= infoBean.getString("DESCRIPTION")%>">
        <input type="hidden" name="AUTEUR" value="<%= infoBean.getString("AUTEUR")%>">
        <input type="hidden" name="COPYRIGHT" value="<%= infoBean.getString("COPYRIGHT")%>">
        <input type="hidden" name="META_KEYWORDS" value="<%= infoBean.getString("META_KEYWORDS")%>">
        <input type="hidden" name="CODE_RATTACHEMENT" value="<%= infoBean.getString("CODE_RATTACHEMENT")%>">
        <input type="hidden" name="CODE_RUBRIQUE" value="<%= infoBean.getString("CODE_RUBRIQUE")%>">
        <% if (infoBean.getString("CODE_REDACTEUR") != null) { %>
            <input type="hidden" name="CODE_REDACTEUR" value="<%= infoBean.getString("CODE_REDACTEUR")%>">
        <% } %>
        <input type="hidden" name="DATE_CREATION" value="<%= infoBean.get("DATE_CREATION")%>">
        <input type="hidden" name="ID" value="<%= infoBean.getString("ID")%>">
        <input type="hidden" name="MODE_ONGLETS" value="insertion" />
        <input type="hidden" name="FROM" value="" />
        <% if (StringUtils.isNotBlank(infoBean.getString("MODE"))) { %>
            <input type="hidden" name="MODE" value="<%=  infoBean.getString("MODE")%>" />
        <% } %>
        <% if (infoBean.getString("SAISIE_FRONT") != null) { %>
            <input type="hidden" name="SAISIE_FRONT" value="<%= infoBean.get("SAISIE_FRONT") %>" />
        <% } %>
        <% if (infoBean.getString("REFERER") != null) { %>
            <input type="hidden" name="REFERER" value="<%= infoBean.get("REFERER") %>" />
        <% } %>
        <% if (infoBean.getString("TYPE_RESSOURCE") != null) { %>
            <input type="hidden" name="TYPE_RESSOURCE" value="<%= infoBean.getString("TYPE_RESSOURCE")%>" />
            <% if (infoBean.getString("TYPE_MEDIA_" + infoBean.getString("TYPE_RESSOURCE").toUpperCase()) != null) { %>
                <input type="hidden" name="TYPE_MEDIA_<%=infoBean.getString("TYPE_RESSOURCE").toUpperCase()%>" value="<%= infoBean.getString("TYPE_MEDIA_" + infoBean.getString("TYPE_RESSOURCE").toUpperCase())%>" />
            <% } %>
        <% } %>
        <% if (StringUtils.isNotBlank(infoBean.getString("MODE_FICHIER"))) { %>
            <input type="hidden" name="MODE_FICHIER" value="<%= infoBean.getString("MODE_FICHIER") %>" />
        <% } %>
        <% if (infoBean.getString("CODE") != null) { %>
            <input type="hidden" name="CODE" value="<%= infoBean.getString("CODE")%>" />
        <% } %>
        <% if (infoBean.getString("CODE_PARENT") != null) { %>
            <input type="hidden" name="CODE_PARENT" value="<%= infoBean.getString("CODE_PARENT")%>" />
        <% } %>
        <% if (infoBean.getString("OBJET") != null) { %>
            <input type="hidden" name="OBJET" value="<%= infoBean.getString("OBJET")%>" />
        <% } %>
        <% if (infoBean.getString("ID_FICHE") != null) { %>
            <input type="hidden" name="ID_FICHE" value="<%= infoBean.getString("ID_FICHE")%>" />
        <% } %>
        <% if (infoBean.get("NO_FICHIER") != null) { %>
            <input type="hidden" name="NO_FICHIER" value="<%= infoBean.get("NO_FICHIER") %>" />
        <% } %>
        <div class="content_popup media_yoxview media-list">
        <a href="javascript:history.back();"><span class="icon icon-arrow-left2"></span>&nbsp;<%= MessageHelper.getCoreMessage("MEDIATHEQUE.LISTE.RETURN") %></a><%
        final int mediaCount = Integer.parseInt(StringUtils.defaultString((String)infoBean.get("COUNT"), "0"));
        final String plural = mediaCount > 1 ? "s" : StringUtils.EMPTY;
        if (infoBean.getInt("LISTE_NB_ITEMS") == 0) {
            %><p class="message erreur"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.LISTE.AUCUNE_SELECTION") %></p><%
        } else {
            %><p class="message"><%= String.format(MessageHelper.getCoreMessage("MEDIATHEQUE.LISTE.SELECTION"), mediaCount, plural, plural)%></p><%
        }
        %><input type="hidden" name="INCREMENT" value="<%=infoBean.get("INCREMENT")%>" />
        <% if((infoBean.get("FROMPLUS") != null || infoBean.get("FROMMOINS") != null) && !insertionMode && !selectionMode) {
            %><%@ include file="/adminsite/objetspartages/media_pagination.jsp" %><%
        }
        String action = "MODIFIER";
        if (SaisieMedia.MODE_SELECTION.equals(infoBean.getString("MODE")))
            action = "SELECTIONNER_RESSOURCE";
        else if (SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE")))
            action = "INSERER_RESSOURCE";
        %>

        <% for (int i = 0; i < infoBean.getInt("LISTE_NB_ITEMS"); ++i) {
            final String dataHeight = infoBean.get("HEIGHT#"+i) != null ? String.format("data-height=\"%s\"", infoBean.get("HEIGHT#"+i)) : StringUtils.EMPTY;
            final String dataWidth = infoBean.get("WIDTH#"+i) != null ? String.format("data-width=\"%s\"", infoBean.get("WIDTH#"+i)) : StringUtils.EMPTY;
        %>
        <div class="media <%= StringUtils.isNotBlank(infoBean.getString("MODE")) ? infoBean.getString("MODE").toLowerCase() : ""%>">
            <div class="content-wrapper">
            <% if("1".equals(infoBean.getString("MODIFICATION_MEDIA")) || "1".equals(infoBean.getString("SUPPRESSION_MEDIA")) || "1".equals(infoBean.getString("SELECTION_MEDIA"))) {
            %>
                <div class="apercu-conteneur clickable js-media-list__container" data-idmedia="<%=infoBean.getString("ID_FICHE#"+i)%>" data-action="<%=action%>" <%= StringUtils.defaultString(dataHeight) %> <%= StringUtils.defaultString(dataWidth) %>>
            <% } else { %>
                <div class="apercu-conteneur">
            <% } %>
                    <img class="vignette_liste_media" src="<%=infoBean.getString("URL_VIGNETTE#"+i)%>" alt="<%=infoBean.getString("TITRE#"+i)%>" title="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.SELECTIONNER_RESSOURCE")%>" />
                </div>
            </div>

            <div class="description">
                <%= StringUtils.isEmpty(infoBean.getString("TITRE#"+i)) ? infoBean.getString("SOURCE#"+i) : infoBean.getString("TITRE#"+i) %>
            </div>

            <%
            if(!insertionMode && !selectionMode){  %>
                <div class="actions">
                <%if ("1".equals(infoBean.getString("MODIFICATION_MEDIA"))){
                %><button type="button" class="modifier js-media-list__edit-button" data-idmedia="<%=infoBean.getString("ID_FICHE#"+i)%>" data-action="<%=action%>" <%= StringUtils.defaultString(dataHeight) %> <%= StringUtils.defaultString(dataWidth) %>><%= MessageHelper.getCoreMessage("MEDIATHEQUE.LISTE.MODIFIER") %></button><%
                }
                if ("1".equals(infoBean.getString("SUPPRESSION_MEDIA"))) {
                %><button type="button" class="supprimer js-media-list__delete-button" data-idmedia="<%=infoBean.getString("ID_FICHE#"+i)%>"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.LISTE.SUPPRIMER") %></button><%
                }
                %><div class="clearfix"></div>
                </div><!-- #actions -->
            <%}%>
        </div>
        <% } %>

        <% if (infoBean.get("FROMPLUS") != null || infoBean.get("FROMMOINS") != null) { %>
            <%@ include file="/adminsite/objetspartages/media_pagination.jsp" %>
        <% } %>
            </div>
        </form>
    </div>