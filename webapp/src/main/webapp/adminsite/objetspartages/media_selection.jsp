<%@ page import="java.util.Locale" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="org.apache.commons.lang3.StringUtils" errorPage="/adminsite/jsbexception.jsp" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.webapp.WebAppUtil" %>
<%@ page import="com.univ.objetspartages.processus.SaisieMedia" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%Locale locale = ContexteUtil.getContexteUniv().getLocale();%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="<%= locale.getLanguage() %>"><!--<![endif]-->
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" media="screen, projection"  href="/adminsite/scripts/libs/css/jquery-ui-1.10.3.custom.css"> <!-- Sorti de WRO pour l'uri en base 64 ! -->
        <resources:link group="stylesBo"/>
        <link rel="stylesheet" media="screen, projection"  href="/adminsite/styles/screen.css">
        <link rel="stylesheet" media="screen, projection"  href="/adminsite/styles/dialog.css">
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
<script src="/adminsite/toolbox/toolbox_new.js"></script>
<script type="text/javascript">

    var arrayOnglets = [];
    arrayOnglets[0]='ajout';
<%if ("1".equals(infoBean.getString("RECHERCHE_MEDIA")) && !( SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE")) && infoBean.get("OBJET") == null ) ) {%>
    arrayOnglets[1]='recherche';
<%}%>
<%if (SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE")))  {%>
    arrayOnglets[2]='fiche';
    <%if (!"LIEN".equals(infoBean.getString("MODE_FICHIER"))) {%>
        arrayOnglets[3]='insertion';
    <%}%>
<%}%>
    var modeOngletsModif = 'selection';

    var arrayOngletsDispos = [];
    arrayOngletsDispos[0]='o_dispo';
    arrayOngletsDispos[1]='o_dispo';
    arrayOngletsDispos[2]='o_dispo';
    arrayOngletsDispos[3]='o_indispo';

    function basculeOngletsDispos(mode) {
        if (mode == '' || !mode) {
            if (window.document.getElementById('li-insertion').className == 'o_indispo') {
                mode = 'insertion';
            } else {
                mode = 'selection';
            }
        }
        if (mode == 'insertion') {
            arrayOngletsDispos[0]='o_indispo';
            arrayOngletsDispos[1]='o_indispo';
            arrayOngletsDispos[2]='o_indispo';
            arrayOngletsDispos[3]='o_dispo';

            for (i=0; i<arrayOngletsDispos.length;i++) {
                var nomOnglet = arrayOnglets[i];
                if (nomOnglet == 'insertion') {
                    if (window.document.getElementById('li-'+nomOnglet)) {window.document.getElementById('li-'+nomOnglet).className = 'active';}
                    if (window.document.getElementById('div-'+nomOnglet)) {window.document.getElementById('div-'+nomOnglet).style.display = 'block';}
                } else {
                    if (window.document.getElementById('li-'+nomOnglet)) {window.document.getElementById('li-'+nomOnglet).className = 'o_indispo';}
                    if (window.document.getElementById('div-'+nomOnglet)) {window.document.getElementById('div-'+nomOnglet).style.display = 'none';}
                }
            }

        } else if (mode == 'selection') {
            arrayOngletsDispos[0]='o_dispo';
            arrayOngletsDispos[1]='o_dispo';
            arrayOngletsDispos[2]='o_dispo';
            arrayOngletsDispos[3]='o_indispo';

            for (i=0; i<arrayOngletsDispos.length;i++) {
                var nomOnglet = arrayOnglets[i];
                if (nomOnglet == 'insertion') {
                    if (window.document.getElementById('li-'+nomOnglet)) {window.document.getElementById('li-'+nomOnglet).className = 'o_indispo';}
                    if (window.document.getElementById('div-'+nomOnglet)) {window.document.getElementById('div-'+nomOnglet).style.display = 'none';}
                } else if(nomOnglet == 'ajout') {
                    if (window.document.getElementById('li-'+nomOnglet)) {window.document.getElementById('li-'+nomOnglet).className = 'active';}
                    if (window.document.getElementById('div-'+nomOnglet)) {window.document.getElementById('div-'+nomOnglet).style.display = 'block';}
                } else {
                    if (window.document.getElementById('li-'+nomOnglet)) {window.document.getElementById('li-'+nomOnglet).className = 'o_dispo';}
                    if (window.document.getElementById('div-'+nomOnglet)) {window.document.getElementById('div-'+nomOnglet).style.display = 'none';}
                }
            }
        }
    }

    function init() {
        var media = window.document.getElementById('MEDIA');
        if(media){
            media.setAttribute('required', '');
        }
        ancre = '<%=infoBean.get("ANCRE")%>';
        sModeOnglets = '<%=infoBean.getString("MODE_ONGLETS")%>';
        sIdOnglet = '<%=infoBean.getString("ID_ACTIVE_ONGLET")%>';
        sIdTypeRessource = '<%=infoBean.getString("TYPE_RESSOURCE")%>';
        if (ancre == 'null')
            ancre = null;
        //si le parametre de l'infobean est null, on est sur une nouvelle insertion ou une modif.
        //par default, on atterit sur le mode selection, mais si on est dans le cas d'une modif de ressource, on force l'affichage de l'onglet "parametres d'insertion"
        if (sModeOnglets == 'null')
            sModeOnglets=modeOngletsModif;
        if (sIdOnglet == 'null')
            sIdOnglet='0';
        <%if (SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE")) && !"LIEN".equals(infoBean.getString("MODE_FICHIER")))  {%>
            basculeOngletsDispos(sModeOnglets);
        <%}%>
        activeOnglet(sIdOnglet, ancre);
    }
    function activeOnglet(indiceOnglet, ancre)
    {
        //on verifie tout d'abord si l'onglet cliquï¿½ est disponible
        var dispoOnglet = 'o_indispo';
            urlRessource = window.document.getElementById('URL_RESSOURCE'),
            media = window.document.getElementById('MEDIA');
        if (window.document.getElementById('li-'+arrayOnglets[indiceOnglet]))
            dispoOnglet = window.document.getElementById('li-'+arrayOnglets[indiceOnglet]).className;

        if(urlRessource && media){
            if(indiceOnglet == 0){
                var ficSrc = window.document.getElementById("ficsrc");
                if(ficSrc.style.display == 'none'){
                    urlRessource.setAttribute('required', '');
                    media.removeAttribute('required');
                }else{
                    media.setAttribute('required', '');
                    urlRessource.removeAttribute('required');
                }
            }else{
                media.removeAttribute('required');
                urlRessource.removeAttribute('required');
            }
        }

        //si on clique sur un onglet indispo, on ne continue pas
        if (dispoOnglet != 'o_indispo') {
            var nomOnglet = '';
            for (i=0; i<arrayOnglets.length;i++)
            {
                nomOnglet = arrayOnglets[i];
                var className = '';
                if (window.document.getElementById('li-'+nomOnglet))
                            className = window.document.getElementById('li-'+nomOnglet).className;

                //on verifie sil'onglet est dispo (selectionnable)
                if (arrayOngletsDispos[i] == 'o_dispo') {
                    if (i == indiceOnglet) // onglet sï¿½lectionnï¿½
                    {
                        if (window.document.getElementById('li-'+nomOnglet))
                            window.document.getElementById('li-'+nomOnglet).className = 'active';

                        if(i==arrayOnglets.length-1)
                            window.document.getElementById('li-'+nomOnglet).className = 'active last';

                        if (window.document.getElementById('div-'+nomOnglet))
                                window.document.getElementById('div-'+nomOnglet).style.display = 'block';
                    }
                    else
                    {
                        if (window.document.getElementById('li-'+nomOnglet))
                            window.document.getElementById('li-'+nomOnglet).className = 'o_dispo';

                        if(i==arrayOnglets.length-1)
                            window.document.getElementById('li-'+nomOnglet).className = 'o_dispo last';

                        if (window.document.getElementById('div-'+nomOnglet))
                            window.document.getElementById('div-'+nomOnglet).style.display = 'none';
                    }
                } else {
                    if (window.document.getElementById('li-'+nomOnglet))
                        window.document.getElementById('li-'+nomOnglet).className = 'o_indispo';

                    if(i==arrayOnglets.length-1)
                        window.document.getElementById('li-'+nomOnglet).className = 'o_indispo last';

                    if (window.document.getElementById('div-'+nomOnglet))
                            window.document.getElementById('div-'+nomOnglet).style.display = 'none';
                }
            }
            if (ancre)
                window.location.href = '#' + ancre;
        }
    }
    var arrayTypeRessources = [];
    function displayFieldsTypeRessource(sTypeRessource)
    {
        for (i=0; i<arrayTypeRessources.length;i++)
        {
            var iReesource = arrayTypeRessources[i];
            if (sTypeRessource == iReesource) // si accueil = page de tï¿½te
            {
                window.document.getElementById("media-"+iReesource.toLowerCase()).style.display = 'block';
            }
            else
            {
                window.document.getElementById("media-"+iReesource.toLowerCase()).style.display = 'none';
            }
        }
    }
    function renvoyerMedia(idMedia, hauteur, largeur){
        window.document.forms[0].ACTION.value='INSERER_RESSOURCE';
        window.document.forms[0].ID_MEDIA.value=idMedia;
        if( hauteur)
            window.document.forms[0].HEIGHT.value=hauteur;
        if( largeur)
            window.document.forms[0].WIDTH.value=largeur;
        window.document.forms[0].submit();
    }

    function changeSource(url){
        var ficSrc = window.document.getElementById("ficsrc"),
            urlSrc = window.document.getElementById("urlsrc"),
            urlRessource = window.document.getElementById('URL_RESSOURCE'),
            media = window.document.getElementById('MEDIA');
        if(url){
            ficSrc.style.display = 'none';
            urlSrc.style.display = '';
            urlRessource.setAttribute('required', '');
            document.forms[0].IS_LOCAL.value="false";
            if(media) {
                media.removeAttribute('required');
            }
        } else {
            urlSrc.style.display = 'none';
            ficSrc.style.display = '';
            urlRessource.removeAttribute('required');
            document.forms[0].IS_LOCAL.value="true";
            if(media) {
                media.setAttribute('required', '');
            }
        }
    }

    function checkSubmit(input){
        if(jQuery){
            var $ = jQuery;
            var $url = $('#URL_RESSOURCE[required]');
            if($url.length > 0){
                $url.rules('remove');
                $url.rules('add', {
                    url: true
                });
            }
            if($(window.document.forms[0]).valid()){
                window.document.forms[0].ACTION.value='CONTROLER_RESSOURCE';
                window.document.forms[0].submit();
            }
        }
    }

</script>

</head>

<body id="body_k_plugin" onload="init();" class="popup">

    <form action="<%= WebAppUtil.SG_PATH%>" enctype="multipart/form-data" method="post">
        <%
            fmt.insererVariablesCachees( out, infoBean);
        %>
        <!-- Utile pour la restauration des données -->
        <%
            if (infoBean.get("ID_CI_RESTAURATION") != null) {
        %>
        <input type="hidden" name="ID_CI_RESTAURATION"
            value="<%=infoBean.getString("ID_CI_RESTAURATION")%>" />
        <input type="hidden" name="NOM_JSP_RESTAURATION"
            value="<%=infoBean.getString("NOM_JSP_RESTAURATION")%>" />
        <%
            }
            boolean isLocal = Boolean.TRUE;
            if (infoBean.get("IS_LOCAL") != null) {
                if (infoBean.get("IS_LOCAL") instanceof Boolean) {
                    isLocal = infoBean.get("IS_LOCAL",Boolean.class);
                } else {
                    isLocal = Boolean.parseBoolean(infoBean.getString("IS_LOCAL"));
                }
            }
        %>
        <input type="hidden" name="ACTION" value="" />
        <input type="hidden" name="ID_MEDIA" value="" />
        <input type="hidden" name="HEIGHT" value="" />
        <input type="hidden" name="WIDTH" value="" />
        <input type="hidden" name="INCREMENT" value="12" />
        <input type="hidden" name="MODE_ONGLETS" value="insertion" />
        <input type="hidden" name="IS_LOCAL" value="<%= isLocal %>"><%
        if(infoBean.getString("EXTENSION") != null) {
            %><input type="hidden" name="EXTENSION" value="<%= infoBean.getString("EXTENSION") %>" /><%
        }
        if (infoBean.getString("MODE") != null) {
        %><input type="hidden" name="MODE" value="<%=infoBean.get("MODE")%>" /><%
        }
        if (infoBean.getString("MODE_FICHIER") != null) {
        %><input type="hidden" name="MODE_FICHIER" value="<%=infoBean.get("MODE_FICHIER")%>" /><%
        }
        if (infoBean.getString("CODE_PARENT") != null) {
        %><input type="hidden" name="CODE_PARENT" value="<%=infoBean.get("CODE_PARENT")%>" /><%
        }
        if (infoBean.getString("CODE") != null) {
        %><input type="hidden" name="CODE"            value="<%=infoBean.getString("CODE")%>" /> <%
        }
        if (infoBean.getString("OBJET") != null) {
        %><input type="hidden" name="OBJET" value="<%=infoBean.getString("OBJET")%>" /><%
        }
        if (infoBean.getString("TYPE_RESSOURCE") != null) {
        %><input type="hidden" name="TYPE_RESSOURCE" value="<%=infoBean.getString("TYPE_RESSOURCE")%>" /><%
        }
        if (infoBean.getString("ID_FICHE") != null) {
        %><input type="hidden" name="ID_FICHE" value="<%=infoBean.getString("ID_FICHE")%>" /><%
        }
        if (infoBean.get("NO_FICHIER") != null) {
        %><input type="hidden" name="NO_FICHIER" value="<%=infoBean.get("NO_FICHIER")%>" /><%
        }
        if (infoBean.get("CODE_RUBRIQUE2") != null) {
        %><input type="hidden" name="CODE_RUBRIQUE2" value="<%=infoBean.getString("CODE_RUBRIQUE2")%>" /><%
        }
        if (infoBean.get("CODE_RATTACHEMENT2") != null) {
        %><input type="hidden" name="CODE_RATTACHEMENT2" value="<%=infoBean.getString("CODE_RATTACHEMENT2")%>" /><%
        }
        if (infoBean.get("MAX_FILE_SIZE") != null) {
        %><input type="hidden" name="MAX_FILE_SIZE" value="<%=infoBean.get("MAX_FILE_SIZE")%>" /><%
        }
        if (infoBean.get("FCK_PLUGIN") != null) {
        %><input type="hidden" name="FCK_PLUGIN" value="<%=infoBean.get("FCK_PLUGIN")%>" /><%
        }
        %><ul id="onglets">
            <li id="li-ajout" onclick="activeOnglet(0);"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.AJOUT") %></li>
            <% if ("1".equals(infoBean.getString("RECHERCHE_MEDIA")) &&
                    !(SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE"))
                            && (infoBean.get("OBJET") == null || StringUtils.isBlank(infoBean.getString("OBJET"))))) { %>
            <li id="li-recherche" onclick="activeOnglet(1);"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.RECHERCHE") %></li>
            <% }
                if (SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE"))) {
                    if ( infoBean.get("OBJET") != null && StringUtils.isNotBlank(infoBean.getString("OBJET"))) { %>
            <li id="li-fiche" onclick="activeOnglet(2);"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.RESSOURCE_INSERES") %></li>
                 <% }
                    if ( !"LIEN".equals(infoBean.getString("MODE_FICHIER")) ) { %>
            <li id="li-insertion" onclick="activeOnglet(3);"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.PARAMETRE_INSERTION") %></li>
                 <% }
                }
            %>
        </ul><%
        if (infoBean.getMessageErreur() != null && infoBean.getMessageErreur().length() > 0) {
            infoBean.remove("MEDIA");
        %><p class="message erreur"><%=infoBean.getMessageErreur()%></p><%
        }
        %>
        <div id="content" role="main" class="js-inhibate"><%
                if (SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE"))) {
                    final String templateInsertion = infoBean.get("TEMPLATE_JSP_INSERTION", String.class);
                    if(StringUtils.isNotBlank(templateInsertion)) {
                        %><div id="div-insertion" style="display:none;">
                            <jsp:include page="<%= templateInsertion %>"/>
                        </div><!-- #div-insertion --><%
                    }
                }
                %><div id="div-recherche" style="display: none;">
                <%
                     if (infoBean.getString("TYPE_RESSOURCE") == null) {
                         univFmt.insererComboHashtable(fmt, out, infoBean, "TYPE_RESSOURCE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_RESSOURCES", MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TYPE_RESSOURCE"));
                         Enumeration<String> en = ((Hashtable) infoBean.get("LISTE_TYPE_RESSOURCES")).keys();
                         String sKeyType = "";
                         while (en.hasMoreElements()) {
                            sKeyType = en.nextElement();
                            %><p data-type="<%=sKeyType.toLowerCase()%>" style="display:none;">

                                <label for="TYPE_MEDIA_<%=sKeyType.toLowerCase()%>" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TYPE_MEDIA") %></label>
                                <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_MEDIA_" + sKeyType.toUpperCase(), FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_MEDIA_" + sKeyType.toUpperCase(), "", "");%>
                            </p><%
                         }
                     }
                     else { %>
                        <p>
                            <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TYPE_RESSOURCE") %></span>
                            <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LIBELLE_TYPE_RESSOURCE", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TYPE_RESSOURCE"));%>
                            <input type="hidden" name="TYPE_RESSOURCE" value="<%=infoBean.getString("TYPE_RESSOURCE")%>">
                        </p>
                        <p>
                            <label for="TYPE_MEDIA_<%=infoBean.getString("TYPE_RESSOURCE").toUpperCase()%>" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TYPE_MEDIA") %></label>
                            <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_MEDIA_" + infoBean.getString("TYPE_RESSOURCE").toUpperCase(), FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_MEDIA_" + infoBean.getString("TYPE_RESSOURCE").toUpperCase(), "", "");%>
                        </p>
                <% } %>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "TITRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TITRE"));%>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "LEGENDE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.LEGENDE"));%>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "DESCRIPTION", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.DESCRIPTION"));%>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "AUTEUR", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.AUTEUR"));%>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "COPYRIGHT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.COPYRIGHT"));%>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "META_KEYWORDS", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.MOTS_CLES"));%>
                        <% univFmt.insererComboHashtable(fmt, out, infoBean, "THEMATIQUE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_THEMATIQUES", MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.THEMATIQUE"), ""); %>
                        <% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE); %>
                        <% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE); %>
                        <% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_REDACTEUR", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.REDACTEUR"), "utilisateur", UnivFmt.CONTEXT_ZONE); %>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_CREATION", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.DATE_CREATION")); %>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "ID", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 20, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.IDENTIFIANT")); %>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "POIDS_MINIMUM", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 20, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TAILLE_MIN")); %>
                        <% univFmt.insererChampSaisie(fmt, out, infoBean, "POIDS_MAXIMUM", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 20, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TAILLE_MAX")); %>
                        <% if ("1".equals(infoBean.getString("RECHERCHE_MEDIA")) && !( SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE")) && infoBean.get("OBJET") == null )) { %>
                        <p class="validation">
                            <input class="bouton" type="submit" name="RECHERCHER" value="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.RECHERCHER") %>" />
                        </p>
                        <%
                        }
                    %></div><!-- #div-recherche --><%
                    if (SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE"))) {
                %>
                <div id="div-fiche" style="display: none;">
                    <%  final Integer nbFichier = (Integer)infoBean.get("FICHIER_NB_ITEMS");
                        for (int i = 0; i < nbFichier; ++i) { %>
                        <div class="media <%= StringUtils.isNotBlank(infoBean.getString("MODE")) ? infoBean.getString("MODE").toLowerCase() : ""%>">
                            <a class="link" href="#" onclick="renvoyerMedia('<%=infoBean.get("ID_FICHIER#" + i)%>'<%=(infoBean.get("HEIGHT#" + i) != null ? ",'" + infoBean.get("HEIGHT#" + i) + "','" + infoBean.get("WIDTH#" + i) + "'" : "")%>);">
                                <div class="apercu-conteneur">
                                    <img class="vignette_liste_media" src="<%=infoBean.getString("URL_VIGNETTE#"+i)%>" alt="<%=infoBean.getString("TITRE#"+i)%>" title="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.SELECTIONNER_RESSOURCE") %>" />
                                </div>
                            </a>

                            <div class="description">
                                <a class="link" href="#" onclick="renvoyerMedia('<%=infoBean.get("ID_FICHIER#" + i)%>'<%=(infoBean.get("HEIGHT#" + i) != null ? ",'" + infoBean.get("HEIGHT#" + i) + "','" + infoBean.get("WIDTH#" + i) + "'" : "")%>);">
                                    <%= infoBean.getString("LIBELLE_FICHIER#" + i) %>
                                </a>
                            </div>
                        </div>
                    <% } %>
                </div><!-- #div-fiche -->
                <%
                    }
                %>

                <div id="div-ajout" style="display: none;">
                        <% if (infoBean.getString("TYPE_RESSOURCE") != null && infoBean.getString("TYPE_RESSOURCE").length() > 0) { %>
                        <p>
                            <label for="LIBELLE_TYPE_RESSOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TYPE_RESSOURCE") %></label>
                            <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LIBELLE_TYPE_RESSOURCE", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255, "Path"); %>
                        </p>
                        <% } %>
                        <p id="ficsrc">
                            <label for="MEDIA" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.SOURCE") %> (*)</label>
                            <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, "MEDIA", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_FICHIER, 10, 100, "Fichier"); %>
                            <br />
                            <a href="#" onclick="changeSource(true);"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.URL_EXTERNE") %></a>
                        </p>

                        <p id="urlsrc" style="display: none;">
                            <label for="URL_RESSOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.URL") %> (*)</label>
                            <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, "URL_RESSOURCE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 10, 255, "Fichier"); %>
                            <br />
                            <a href="#" onclick="changeSource(false);"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.FICHIER_LOCAL") %></a>
                        </p>

                        <p>
                            <label for="TITRE2" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.TITRE") %></label>
                            <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, "TITRE2", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, "Titre"); %>
                        </p>
                        <p>
                            <label for="LEGENDE2" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.LEGENDE") %></label>
                            <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LEGENDE2", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, "Légende"); %>
                        </p>
                        <p>
                            <label for="DESCRIPTION2" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.DESCRIPTION") %></label>
                            <% fmt.insererChampSaisie(out, infoBean, "DESCRIPTION2", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, "LIB=Description,COLS=37,ROWS=2"); %>
                        </p>

                        <% if ("1".equals(infoBean.getString("AJOUT_MEDIA")) && StringUtils.isEmpty(infoBean.getString("ESPACE")) && !(SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE")) && infoBean.get("OBJET") == null)) { %>
                            <p class="retrait">
                                <% fmt.insererChampSaisie(out, infoBean, "MUTUALISE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
                                <label for="MUTUALISE"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.SAUVEGARDER") %></label>
                            </p>
                        <% } %>
                    <p class="validation">
                        <input type="button" name="VALIDER" value="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SELECTION.ENREGISTRER") %>" onclick="checkSubmit(this); return false;"/>
                    </p>
                </div><!-- #div-ajout -->
        </div><!-- #content -->
    </form>
    <script src="/adminsite/scripts/libs/jquery-1.11.0.js"></script>
    <script src="/adminsite/scripts/libs/jquery-ui-1.10.3.custom.js"></script>
    <resources:script group="scriptsBo" locale="<%= locale.toString() %>"/>
    <script src="/adminsite/scripts/backoffice.js"></script><%
    if(StringUtils.isNotBlank(infoBean.get("JS_INSERTION", String.class))) {
        %><script src="<%= infoBean.get("JS_INSERTION", String.class) %>"></script><%
    }
    %>
    <script type="text/javascript">
        function updateExternal() {
            var resourceInput = document.getElementById('URL_RESSOURCE');
            if(resourceInput && resourceInput.value) {
                changeSource(true);
            }
        }

        function updateUI(){
            var combos = document.querySelectorAll('p[data-type]'),
                    select = document.getElementById('TYPE_RESSOURCE');
            if(select) {
                var selectedCombo = document.querySelector('p[data-type="' + select.value + '"]');
                if(selectedCombo) {
                    for (var i = 0; i < combos.length; i++) {
                        combos[i].style.display = 'none';
                    }
                    selectedCombo.style.display = 'block';
                }
            }
        }

        function bindSelect() {
            var select = document.getElementById('TYPE_RESSOURCE');
            if(select) {
                select.onchange = updateUI;
            }
        }

        updateUI();
        updateExternal();
        bindSelect();
    </script>
</body>
</html>
