<%@ page errorPage="/adminsite/saisie_front/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />


<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<!-- link rel="stylesheet" type="text/css" href="/adminsite/general.css" -->
</head>

<body>

<script type="text/javascript">
<% if (!"M".equals(infoBean.getString("NIVEAU_APPROBATION"))) { %>
    window.opener.document.forms[0].NIVEAU_APPROBATION_DEMANDE.value= '<%=infoBean.getString("NIVEAU_APPROBATION")%>';
    window.opener.document.forms[0].LISTE_VALIDATEURS.value= '<%=infoBean.getString("LISTE_VALIDATEURS")%>';
    window.opener.document.forms[0].VALIDATEURS.value= '<%=infoBean.getString("CHAINE_VALIDATEURS")%>';
    window.opener.document.forms[0].NIVEAU_VALIDATION.value= '<%=infoBean.getString("LIBELLE_APPROBATION")%>';
<% } %>
    window.close();

</script>

</body>
</html>