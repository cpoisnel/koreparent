<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Map.Entry" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.InfoBean" %>
<%@ page import="com.jsbsoft.jtf.core.ProcessusHelper" %>
<%@ page import="com.kportal.core.config.PropertyHelper" %>
<%@ page import="com.kportal.core.webapp.WebAppUtil" %>
<%@ page import="com.kportal.extension.module.composant.IComposant" %>
<%@ page import="com.univ.datagrid.utils.DatagridUtils" %>
<%@ page import="com.univ.objetspartages.om.InfosRequeteGroupe" %>
<%@ page import="com.univ.utils.EscapeString" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
Map<String, InfosRequeteGroupe> listeTypeGroupedyn = infoBean.getMap("LISTE_REQUETES_GROUPES");
%>
<form action="<%= WebAppUtil.SG_PATH %>" enctype="multipart/form-data" method="post" class="modification">
    <div id="actions">
        <div>
            <ul><%
                if (!"AJOUTER".equals(infoBean.getActionUtilisateur())) {%>
                    <li><button class="supprimer" data-confirm="<%=module.getMessage("BO_CONFIRM_SUPPR_GROUPE") %>" type="submit" name="SUPPRIMER" value="<%= module.getMessage("JTF_BOUTON_SUPPRIMER")%>"><%= module.getMessage("JTF_BOUTON_SUPPRIMER")%></button></li><%
                }
                %><li><button class="enregistrer" type="submit" name="VALIDER" value="<%= module.getMessage("BO_ENREGISTRER")%>"><%= module.getMessage("BO_ENREGISTRER")%></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= module.getMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->

    <ul id="onglets">
        <li id="liGeneral" data-targetdivid="divGeneral" onclick="activeOnglet(this);" class="active"><%= module.getMessage("BO_ONGLET_INFOS_GENERALES") %></li>
        <li id="liAutorisations" data-targetdivid="divAutorisations" onclick="activeOnglet(this);"><%= module.getMessage("BO_ONGLET_AUTORISATIONS_ROLES") %></li>
        <%if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) {%>
            <li id="liUtilisateurs" data-targetdivid="divUtilisateurs" onclick="activeOnglet(this);"><%= module.getMessage("BO_ONGLET_UTILISATEURS") %></li>
        <%} %>
    </ul>

    <div id="content" role="main">
        <input type="hidden" name="ACTION" value="" /><%
        fmt.insererVariablesCachees( out, infoBean);
        if (infoBean.get("ID_GROUPE")!= null) {
            %><input type="hidden" name="ID_GROUPE" value="<%=infoBean.getString("ID_GROUPE")%>" /><%
        }
        %>
        <div id="divGeneral" class="fieldset neutre"><%
            int modeSaisieCode= FormateurJSP.SAISIE_OBLIGATOIRE;
            if ("0".equals(infoBean.getString("SAISIE_CODE"))) {
                modeSaisieCode = FormateurJSP.SAISIE_AFFICHAGE;
            }
            %><p>
                <span class="label colonne"><%= module.getMessage("BO_LIBELLE_CODE") %><% if (modeSaisieCode == FormateurJSP.SAISIE_OBLIGATOIRE) { %> (*)<% } %></span>
                <span><% fmt.insererChampSaisie(out, infoBean, "CODE", modeSaisieCode, FormateurJSP.FORMAT_TEXTE, 0, 20); %></span>
            </p>
            <p>
                <label for="LIBELLE" class="colonne"><%= module.getMessage("BO_LIBELLE") %>(*)</label>
                <% fmt.insererChampSaisie(out, infoBean, "LIBELLE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255); %>
            </p>
            <%
                boolean dsi = "1".equals(PropertyHelper.getCoreProperty("dsi.activation"));
                int modeSaisieType = FormateurJSP.SAISIE_OBLIGATOIRE;
                if (infoBean.getString("SAISIE_TYPE").equals("0")) {
                    modeSaisieType = FormateurJSP.SAISIE_AFFICHAGE;
                }
                else {
                    modeSaisieType = UnivFmt.getModeSaisieZone(infoBean, "GROUPE_DSI.TYPE");
                }
            %>
            <p>
                <%
                if (dsi) {
                %>
                    <label for="TYPE" class="colonne"><%= module.getMessage("BO_TYPE") %><% if (modeSaisieType == FormateurJSP.SAISIE_OBLIGATOIRE) { %> (*)<% } %></label>
                <%
                    fmt.insererComboHashtable(out, infoBean, "TYPE", modeSaisieType, "LISTE_TYPES");
                } else {
                %>
                    <span class="label colonne"><%= module.getMessage("BO_TYPE") %></span>
                    <span>
                        <input type="hidden" name="TYPE" value="NEWS" /><%= module.getMessage("BO_GROUPE_NEWSLETTER") %>
                    </span>
                <%
                }
                %>
            </p>
            <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_STRUCTURE", FormateurJSP.SAISIE_FACULTATIF, module.getMessage("ST_CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE);%>
            <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_GROUPE_PERE", UnivFmt.getModeSaisieZone(infoBean, "GROUPE_DSI.CODE_GROUPE_PERE"), module.getMessage("BO_GROUPE_GROUPE_PARENT"), "", UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION);%>
            <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_PAGE_TETE", FormateurJSP.SAISIE_FACULTATIF, module.getMessage("BO_GROUPE_HOME_PAGE"), "pagelibre", univFmt.CONTEXT_ZONE);%>
            <%
            /* Groupes dynamiques */
            if (listeTypeGroupedyn.size() > 0 && UnivFmt.getModeSaisieZone(infoBean, "GROUPE_DSI.GROUPE_DYNAMIQUE") != FormateurJSP.SAISIE_AFFICHAGE) {
            %>
                <fieldset>
                    <legend><%= module.getMessage("BO_GROUPE_GROUPES_DYNAMIQUES") %></legend>
                    <p>
                        <label for="REQUETE_GROUPE" class="colonne"><%= module.getMessage("BO_GROUPE_REQUETE_GROUPE_DYNAMIQUE") %> <% if (UnivFmt.getModeSaisieZone(infoBean, "GROUPE_DSI.REQUETE_GROUPE") == FormateurJSP.SAISIE_OBLIGATOIRE) { %> (*)<% } %></label><%
                        Map<String,InfosRequeteGroupe> requetesBrute = infoBean.getMap("LISTE_REQUETES_GROUPES");
                        Hashtable<String,String> requetesGroupe = new Hashtable<String,String>();
                        for (Entry<String,InfosRequeteGroupe> valeurRequete : requetesBrute.entrySet()) {
                            requetesGroupe.put(valeurRequete.getKey(), valeurRequete.getValue().getIntitule());
                        }
                        fmt.insererCombo(out, infoBean, "REQUETE_GROUPE", UnivFmt.getModeSaisieZone(infoBean, "GROUPE_DSI.REQUETE_GROUPE"), requetesGroupe, StringUtils.EMPTY); %>
                    </p>
                    <%
                    /* on parcourt les types de groupe dynamique (parametre a activer dans le jtf) */
                    for (InfosRequeteGroupe infosReqGroupeDynamque : listeTypeGroupedyn.values()) {
                        if (StringUtils.isNotBlank(infosReqGroupeDynamque.getTemplateJSP())) {
                            request.setAttribute("NOM_REQUETE", infosReqGroupeDynamque.getAlias());
                            %> <jsp:include page="<%= infosReqGroupeDynamque.getTemplateJSP() %>" /><%
                        }
                    }
                    %>
                </fieldset>
            <%
            }
            %>
        </div>

        <div id="divAutorisations" style="display: none;">
            <jsp:include page="/adminsite/objetspartages/affectation_role_saisie.jsp" />
        </div>
        <%
        if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) {
            /* Si c'est un groupe LDAP on passe en paramètre non pas le code du groupe mais la requete LDAP #req#*/
            if ( infoBean.getString("REQUETE_GROUPE") == null
                || !"1".equals(PropertyHelper.getCoreProperty("ldap.actif"))
                || !infoBean.getString("REQUETE_GROUPE").equals("req_ldap") ) {
                if (StringUtils.isNotBlank(infoBean.getString("REQUETE_GROUPE"))) {
                    %><div id="divUtilisateurs" style="display: none;" class="fieldset neutre">
                        <table class="datatableUtilisateurGROUPEDYN" data-search="<%=EscapeString.escapeAttributHtml(DatagridUtils.getUrlTraitementDatagrid("&GROUPE_DSI=" + infoBean.get("CODE") + "&BEAN_RECHERCHE=utilisateurDatagrid"))%>">
                            <thead>
                                <tr>
                                    <th><%= module.getMessage("BO_IDENTIFIANT") %></th>
                                    <th><%= module.getMessage("BO_NOM_PRENOM") %></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div><%
                } else {
                %><div id="divUtilisateurs" style="display: none;" class="fieldset neutre">
                    <table class="datatableUtilisateurGROUPE" data-search="<%=EscapeString.escapeAttributHtml(DatagridUtils.getUrlTraitementDatagrid("&GROUPE_DSI=" + infoBean.get("CODE") + "&BEAN_RECHERCHE=utilisateurDatagrid"))%>">
                        <thead>
                            <tr>
                                <th><%= module.getMessage("BO_IDENTIFIANT") %></th>
                                <th><%= module.getMessage("BO_NOM_PRENOM") %></th>
                                <th class="sanstri sansfiltre"><%= module.getMessage("BO_ACTIONS") %></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div><%
                }
            }
        }
        %>
    </div><!-- #content -->
</form>
<script type="text/javascript">
    function activeOnglet(element) {
        var $ = jQuery,
            $lis = $('#onglets').find('li[id]');

        $lis.each(function(index, li){
            $(li).removeClass('active');
            $('#' + $(li).data('targetdivid')).css({display: 'none'});
        });
        $(element).addClass('active');
        $('#' + $(element).data('targetdivid')).css({display: 'block'});
    }
</script>