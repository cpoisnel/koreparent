<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@ page import="com.univ.objetspartages.bean.SiteExterneBean" errorPage="/adminsite/jsbexception.jsp" %>
<%@ page import="com.jsbsoft.jtf.textsearch.sitesdistants.SiteIndexStates" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
Map<Long,SiteIndexStates> etatSite = infoBean.getMap("ETAT_SITES");
List<SiteExterneBean> allSites = infoBean.getList("LISTE_SITES");
%><div id="content" role="main">
    <%
        List<String> sitesEnCours = (List<String>) infoBean.get("SITES_EN_COURS_INDEXATION");
        if (sitesEnCours.size() > 0) {
            %><div class="message avertissement"><%= String.format(module.getMessage("BO_INDEXER_INDEXATION_EN_COURS"),StringUtils.join(sitesEnCours.iterator(), "&nbsp;», «&nbsp;")) %><a href="#" onclick="location.reload(true); return false;"><%= module.getMessage("BO_RAFRAICHIR") %></a></div><%
        }
    %>
    <table class="datatable">
        <thead>
            <tr>
                <th> <%=module.getMessage("BO_INTITULE") %> </th>
                <th> <%=module.getMessage("BO_LIBELLE_CODE") %> </th>
                <th> <%=module.getMessage("BO_LIBELLE_URL") %> </th>
                <th> <%=module.getMessage("BO_ETAT") %> </th>
                <th class="sanstri sansfiltre"> <%=module.getMessage("BO_ACTIONS") %> </th>
            </tr>
        </thead>
        <tbody><%
        for (SiteExterneBean site : allSites) {
            %><tr>
                <td>
                    <a class="ellipsis" href="<%=ProcessusHelper.getUrlProcessAction(infoBean, StringUtils.EMPTY , StringUtils.EMPTY, "MODIFIERPARID", new String[][]{{"ID_SITE",String.valueOf(site.getIdSite())}})%>"><%= site.getLibelle() %></a>
                </td>
                <td><%= site.getCode() %></td>
                <td><%= site.getUrl() %></td>
                <td><%= etatSite.get(site.getIdSite()) %></td>
                <td>
                    <a href="<%=ProcessusHelper.getUrlProcessAction(infoBean, StringUtils.EMPTY , StringUtils.EMPTY, "MODIFIERPARID", new String[][]{{"ID_SITE",String.valueOf(site.getIdSite())}})%>"><%=module.getMessage("BO_MODIFIER") %></a> |
                    <a data-confirm="<%= module.getMessage("BO_CONFIRM_SUPPR_SITE_EXTERNE") %>" href="<%=ProcessusHelper.getUrlProcessAction(infoBean, StringUtils.EMPTY , StringUtils.EMPTY, "SUPPRIMERPARID", new String[][]{{"ID_SITE",String.valueOf(site.getIdSite())}})%>"><%=module.getMessage("BO_SUPPRIMER") %></a> |
                    <a href="<%=ProcessusHelper.getUrlProcessAction(infoBean, StringUtils.EMPTY , StringUtils.EMPTY, "INDEXERPARID", new String[][]{{"ID_SITE",String.valueOf(site.getIdSite())}})%>"><%=module.getMessage("BO_INDEXER") %></a>
                </td>
            </tr><%
        }
        %></tbody>
    </table>
</div><!-- #content -->