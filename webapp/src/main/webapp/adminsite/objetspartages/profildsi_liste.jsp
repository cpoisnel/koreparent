<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />

<div id="content" role="main">
    <table>
        <thead>
            <tr>
                <th><%= MessageHelper.getCoreMessage("ST_TABLEAU_LIBELLE") %></th>
            </tr>
        </thead>
        <tbody><%
        for (int i=0; i< infoBean.getInt("LISTE_NB_ITEMS"); i++) { %>
            <tr >
                <td class=<%= ((i%2)==0)? "DetailLigne":"DetailLigne2" %>><input type="radio" name="LISTE_INDICE" value="<%=i%>" <%if (i == 0)  { %> checked <% } %>></td>
                <td class=<%= ((i%2)==0)? "DetailLigne":"DetailLigne2" %>> <%fmt.insererChampSaisie( out, infoBean, "LIBELLE#"+i, FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255); %> </td>
            </tr><%
        }
        %></tbody>
    </table>
</div>
