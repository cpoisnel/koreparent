<%@page import="java.util.List"%>
<%@page import="org.apache.commons.collections.CollectionUtils"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.core.InfoBean"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.om.FicheAnnuaire" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />

<% if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) { %>
<fieldset><legend><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_GENERATION_AUTO_ANNUAIRE") %></legend>
<p class="retrait">
    <%fmt.insererChampSaisie(out, infoBean, "CREER_FICHE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 1); %><label for="CREER_FICHE"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_CREER_ANNUAIRE") %></label>
</p><%
    List<Objetpartage> objetsAnnuaire = FicheAnnuaire.getObjetsAnnuaires();
    if (CollectionUtils.isNotEmpty(objetsAnnuaire)) {
        if (objetsAnnuaire.size() == 1) {
            %><input type="hidden" name="TYPE_FICHE" value="<%= objetsAnnuaire.get(0).getCodeObjet()%>" /><%
        } else {
            %><p>
                <label for="TYPE_FICHE" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_TYPE_ANNUAIRE") %></label>
                <select id="TYPE_FICHE" name="TYPE_FICHE"><%
                for (Objetpartage objetAnnuaire : objetsAnnuaire) {
                    %><option value="<%= objetAnnuaire.getCodeObjet() %>"><%= objetAnnuaire.getLibelleAffichable() %></option><%
                }
                %></select>
            </p><%
        }
    }
%><p>
    <label for="TYPE_POPULATION" class="colonne"><%= MessageHelper.getCoreMessage("BO_TYPE") %></label>
    <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_POPULATION", FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPES_POPULATION",MessageHelper.getCoreMessage("BO_TYPE"),""); %>
</p>
<p>
    <label for="DISCIPLINE" class="colonne"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_FONCTION") %></label>
    <td>
        <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "DISCIPLINE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_DISCIPLINES",MessageHelper.getCoreMessage("BO_UTILISATEUR_DISCIPLINE"),""); %>
    </td>
</tr>
<p class="retrait">
    <%fmt.insererChampSaisie(out, infoBean, "CREER_FICHE_EN_LIGNE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 1); %><label for="CREER_FICHE_EN_LIGNE"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_MISE_EN_LIGNE_AUTO") %></label>
</p>
</fieldset>
<% } %>
