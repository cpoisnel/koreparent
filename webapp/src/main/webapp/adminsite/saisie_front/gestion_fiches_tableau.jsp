<%@ page import="java.text.DateFormat" %>
<%@ page import="java.util.TreeMap" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.univ.objetspartages.om.FicheObjet" %>
<%@ page import="com.univ.objetspartages.om.FicheUniv" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<% if (infoBean.getInt("LISTE_NB_ITEMS") > 0) { %>
    <p id="nombre_resultats"><%=infoBean.getInt("NB_TOTAL")%>&nbsp;<%=MessageHelper.getCoreMessage("ST_TABLEAU_RESULTATS")%></p>
<%
    String codePagination = "";
    int nbTotalPages = infoBean.getInt("NB_PAGES");
    if (infoBean.getString(IRequeteurConstantes.AVEC_PAGINATION).equals("1") && nbTotalPages > 1) {
        int nbResultsParPage = infoBean.getInt("INCREMENT");
        int begin = 0;
        int nbLiens = 10;
        String pointsSuspension = "&hellip;";
        if (infoBean.getInt("FROM") > 0) {
            codePagination += "<span class=\"icon icon-first\" onclick=\"envoyerFormulaire('LISTE','','0');\">";
            codePagination += "<span>Début</span>";
            codePagination += "</span> ";
            codePagination += "<span class=\"icon icon-previous\" onclick=\"envoyerFormulaire('LISTE','','"+(infoBean.getInt("FROM") - infoBean.getInt("INCREMENT"))+"');\">";
            codePagination += "<span>Précédent</span>";
            codePagination += "</span>";
        }
        if (infoBean.get("FROM") != null) {
            begin = infoBean.getInt("FROM");
        }
        if (nbLiens > nbTotalPages) {
            nbLiens = nbTotalPages;
            pointsSuspension = "";
        }
        int nPageCourante = ( begin / nbResultsParPage ) + 1;
        int nPremierePage;
        int nDernierePage;
        if (nPageCourante - (nbLiens / 2) <= 0 ) {
            nPremierePage = 1;
            nDernierePage = nbLiens;
        }
        else if (nPageCourante > (nbTotalPages - (nbLiens / 2))){
            nPremierePage = nbTotalPages - (nbLiens - 1);
            nDernierePage = nbTotalPages;
        }
        else {
            nPremierePage = nPageCourante - (nbLiens / 2);
            nDernierePage = nPageCourante + (nbLiens / 2);
        }

        int nDebutFromPage = (nPremierePage - 1) * nbResultsParPage;
        if (nPremierePage != 1)
            codePagination += pointsSuspension;
        for (int nNumPage = nPremierePage ; nNumPage <= nDernierePage ; ++ nNumPage) {
            if (nNumPage == nPageCourante) {
                codePagination += " <strong>" + nNumPage + "</strong> ";
            } else {
                codePagination += "&nbsp;<a href=\"#\" onclick=\"envoyerFormulaire('LISTE','false','" + nDebutFromPage + "');\">" + nNumPage + "</a>&nbsp;";
            }
            nDebutFromPage += nbResultsParPage;
        }
        if (nPageCourante != nDernierePage) {
            codePagination += pointsSuspension;
        }
        if (infoBean.getInt("FROM") + infoBean.getInt("INCREMENT") < infoBean.getInt("NB_TOTAL")) {
            codePagination += "<span class=\"icon icon-next\" onclick=\"envoyerFormulaire('LISTE','','" + (infoBean.getInt("FROM") + infoBean.getInt("INCREMENT")) + "');\">";
            codePagination += "<span>Suivant</span>";
            codePagination += "</span> ";
            codePagination += "<span class=\"icon icon-last\" onclick=\"envoyerFormulaire('LISTE','','"+(infoBean.getInt("INCREMENT") * (infoBean.getInt("NB_PAGES")-1) ) + "');\">";
            codePagination += "<span>Fin</span>";
            codePagination += "</span>";
        } %>
    <p class="pagination"><%=codePagination%></p>
    <% } %>

    <table id="tableau_bord_resultats">
        <tr>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_LIBELLE_FICHE+"_TRI").equals("1")) { %>
            <th><a href="#" onclick="envoyerFormulaire('LISTE','1','0');"><%=MessageHelper.getCoreMessage("ST_TABLEAU_LIBELLE")%></a></th>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_TYPE_DATE+"_TRI").equals("1")) { %>
            <th><a href="#" onclick="envoyerFormulaire('LISTE','2','0');"><%=MessageHelper.getCoreMessage("ST_TABLEAU_DATE")%></a></th>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_CODE_RUBRIQUE+"_TRI").equals("1")) { %>
            <th><a href="#" onclick="envoyerFormulaire('LISTE','6','0');"><%=MessageHelper.getCoreMessage("ST_TABLEAU_RUBRIQUE")%></a></th>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_CODE_RATTACHEMENT+"_TRI").equals("1")) { %>
            <th><a href="#" onclick="envoyerFormulaire('LISTE','7','0');"><%=MessageHelper.getCoreMessage("ST_TABLEAU_STRUCTURE")%></a></th>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_CODE_REDACTEUR+"_TRI").equals("1")) { %>
            <th><a href="#" onclick="envoyerFormulaire('LISTE','3','0');"><%=MessageHelper.getCoreMessage("ST_TABLEAU_REDACTEUR")%></a></th>
        <% } %>
        <%if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_DIFFUSION_PUBLIC_VISE+"_TRI").equals("1")) { %>
            <th><a href="#" onclick="envoyerFormulaire('LISTE','8','0');"><%=MessageHelper.getCoreMessage("ST_TABLEAU_PUBLIC_VISE")%></a></th>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_CODE_OBJET+"_TRI").equals("1")) { %>
            <th><a href="#" onclick="envoyerFormulaire('LISTE','5','0');"><%=MessageHelper.getCoreMessage("ST_TABLEAU_TYPE")%></a></th>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_ETAT_OBJET+"_TRI").equals("1")) { %>
            <th><a href="#" onclick="envoyerFormulaire('LISTE','4','0');"><%=MessageHelper.getCoreMessage("ST_TABLEAU_ETAT")%></a></th>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_LANGUE+"_TRI").equals("1")) { %>
            <th><a href="#" onclick="envoyerFormulaire('LISTE','9','0');"><%=MessageHelper.getCoreMessage("ST_TABLEAU_LANGUE")%></a></th>
        <% } %>
        <% if (infoBean.getString("ACTION_FICHE").equals("1")) { %>
            <th><%=MessageHelper.getCoreMessage("ST_TABLEAU_ACTIONS")%> </th>
        <% } %>
        </tr>
        <%-- boucle sur chaque ligne --%>
        <%
            for (int i=0; i < infoBean.getInt("LISTE_NB_ITEMS"); i++) {
                String variationCouleur = (i%2==0) ? "odd" : "even"; %>
        <tr class="<%=variationCouleur%>">
        <%
            FicheUniv fiche = (FicheUniv) ((TreeMap)infoBean.get("TREE_FICHES")).get(new Integer (i));
            String lienFiche ="";
            String urlFiche = "";
            String urlConsultationFiche = "";
            String urlModificationFiche = infoBean.getString("URL_FICHE#"+i);
            if (fiche instanceof FicheObjet) {
                fiche = ((FicheObjet) fiche).renvoyerFicheParente();
                if (fiche != null) {
                    urlConsultationFiche = UnivWebFmt.determinerUrlFiche(ctx, fiche)+"#"+ReferentielObjets.getNomObjet(fiche)+fiche.getIdFiche();
                }
            }
            else {
                urlConsultationFiche = UnivWebFmt.determinerUrlFiche(ctx, fiche);
            }
            if (ctx.getEspaceCourant().length() > 0 ) {
                if (StringUtils.contains(urlConsultationFiche, "?")) {
                    urlConsultationFiche += "&ESPACE="+ctx.getEspaceCourant();
                } else {
                    urlConsultationFiche += "?ESPACE="+ctx.getEspaceCourant();
                }
            }
            if ("CONSUL".equals(infoBean.getString("MODE"))) {
                urlFiche = urlConsultationFiche;
            } else if (urlModificationFiche.length() > 0) {
                urlFiche = urlModificationFiche;
            }
            String intituleFiche = infoBean.getString("INTITULE_FICHE#"+i);
            if (urlFiche.length() > 0) {
                if (urlFiche.indexOf("href")!=-1){
                    lienFiche = "<a "+urlFiche+">"+intituleFiche+"</a>";
                } else {
                    lienFiche = "<a href=\""+urlFiche+"\">"+intituleFiche+"</a>";
                }
            }
            else {
                lienFiche = intituleFiche;
            }
            if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_LIBELLE_FICHE+"_TRI").equals("1")) { %>
            <td>
                <%=lienFiche%>
            </td>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_TYPE_DATE+"_TRI").equals("1")) { %>
            <td><%= DateFormat.getDateInstance(DateFormat.SHORT, ctx.getLocale()).format(infoBean.getDate("DATE#"+i)) %></td>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_CODE_RUBRIQUE+"_TRI").equals("1")) { %>
            <td><%= infoBean.getString("CODE_RUBRIQUE#"+i) %></td>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_CODE_RATTACHEMENT+"_TRI").equals("1")) { %>
            <td><%= infoBean.getString("CODE_RATTACHEMENT#"+i) %></td>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_CODE_REDACTEUR+"_TRI").equals("1")) { %>
            <td><%= infoBean.getString("REDACTEUR#"+i) %></td>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_DIFFUSION_PUBLIC_VISE+"_TRI").equals("1")) { %>
                <td><%= infoBean.getString("DIFFUSION_PUBLIC_VISE#"+i) %></td>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_CODE_OBJET+"_TRI").equals("1")) { %>
            <td><%=infoBean.getString("LIBELLE_FICHE#"+i)%></td>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_ETAT_OBJET+"_TRI").equals("1")) { %>
            <td>
                <% if (infoBean.getString("ETAT#"+i).equals("0001")) { %><acronym title="<%=MessageHelper.getCoreMessage("ST_GESTION_FICHES_0001")%>"> <%=MessageHelper.getCoreMessage("ST_GESTION_FICHES_0001")%> </acronym>
                <% } else if (infoBean.getString("ETAT#"+i).equals("0002")) { %><acronym title="<%=MessageHelper.getCoreMessage("ST_GESTION_FICHES_0002")%>"> <%=MessageHelper.getCoreMessage("ST_GESTION_FICHES_0002")%> </acronym>
                <% } else if (infoBean.getString("ETAT#"+i).equals("0003")) { %><acronym title="<%=MessageHelper.getCoreMessage("ST_GESTION_FICHES_0003")%>"> <%=MessageHelper.getCoreMessage("ST_GESTION_FICHES_0003")%> </acronym><% } %>
            </td>
        <% } %>
        <% if (infoBean.getString(IRequeteurConstantes.ATTRIBUT_LANGUE+"_TRI").equals("1")) { %>
            <td><% univFmt.insererDrapeauLangue(out, infoBean.getString("LANGUE#"+i));%></td>
        <% } %>
        <% if (infoBean.getString("ACTION_FICHE").equals("1")) { %>
            <td>
            <% if ("1".equals(infoBean.get("VOIR_FICHE#" + i))) { %>
                <a title="Voir la fiche" href="<%=urlConsultationFiche%>">
                    <img src="/adminsite/images/voir.gif" alt="Voir la fiche" title="Voir la fiche" />
                </a>
            <%    }    %>
            <% if ("1".equals(infoBean.get("MODIFIER_FICHE#" + i))) { %>
                <a title="Modifier la fiche" href="<%=infoBean.getString("URL_MODIF_FICHE#"+i)%>">
                    <img src="/adminsite/images/modif.gif" alt="Modifier la fiche" title="Modifier la fiche" />
                </a>
            <%    }    %>
            <% if ("1".equals(infoBean.get("SUPPRIMER_FICHE#" + i))) { %>
                <a title="Supprimer la fiche" href="<%=infoBean.getString("URL_SUPPR_FICHE#"+i)%>">
                    <img src="/adminsite/images/trash.gif" alt="Supprimer la fiche" title="Supprimer la fiche" />
                </a>
            <%    }    %>
            </td>
        <% } %>

        </tr>
        <% } %>
    </table>

    <% if (infoBean.getString(IRequeteurConstantes.AVEC_PAGINATION).equals("1")) { %>
        <div class="tableau_bord_resultats_pagination">
            <b><%=MessageHelper.getCoreMessage("ST_PAGE")%> <%= infoBean.get("NUM_PAGE") %>/<%= infoBean.get("NB_PAGES") %></b>
            <div>
                <label class="label-bouton" for="increment"><%=MessageHelper.getCoreMessage("ST_RESULTATS_PAR_PAGE")%></label>
                <input id="increment" class="champ-texte" type="text" name="INPUT_INCREMENT" value="<%=infoBean.getInt("INCREMENT")%>" size="2"/>
                <input class="bouton" type="submit" name="OK"  value="Ok" onclick="envoyerFormulaire('LISTE','','0');"/>
            </div>
        </div>
        <% if (nbTotalPages > 1) { %>
        <p class="pagination"><%=codePagination%></p>
        <% } %>
    <% } %>

<% } else { %>
    <p class="message alerte"><%=MessageHelper.getCoreMessage("ST_TABLEAU_PAS_CONTRIBUTION")%></p>
<% } %>
