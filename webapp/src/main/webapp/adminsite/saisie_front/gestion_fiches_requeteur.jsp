<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<fieldset id="requeteur">
    <legend><%=MessageHelper.getCoreMessage("ST_TABLEAU_RECHERCHE")%></legend>
    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_LIBELLE_FICHE+"_REQ").equals("1")) { %>
    <p>
        <label for="LIBELLE_FICHE"><%=MessageHelper.getCoreMessage("ST_REQUETEUR_TITRE")%></label>
        <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, RequeteurFiches.ATTRIBUT_LIBELLE_FICHE, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, "Libellé");%>
    </p>
    <% } %>

    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_DATE_DEBUT+"_REQ").equals("1")) { %>
    <p>
        <label for="DATE_DEBUT"><%=MessageHelper.getCoreMessage("ST_REQUETEUR_DATE_DEBUT")%></label>
        <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, RequeteurFiches.ATTRIBUT_DATE_DEBUT, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10,"depuis","",true,"");%>
    </p>
    <% } else { %>
    <span style="visibility: hidden">
        <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, RequeteurFiches.ATTRIBUT_DATE_DEBUT, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10,"jusqu'au","",true,"");%>
    </span>
    <% } %>

    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_DATE_FIN+"_REQ").equals("1")) { %>
    <p>
        <label for="DATE_FIN"><%=MessageHelper.getCoreMessage("ST_REQUETEUR_DATE_FIN")%></label>
        <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, RequeteurFiches.ATTRIBUT_DATE_FIN, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10,"depuis","",true,"");%>
    </p>
    <% } else { %>
    <span style="visibility: hidden">
        <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, RequeteurFiches.ATTRIBUT_DATE_FIN, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10,"jusqu'au","",true,"");%>
    </span>
    <% } %>

    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_CODE_RUBRIQUE+"_REQ").equals("1")) { %>
        <% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, "Rubrique", "rubrique", univFmt.CONTEXT_ZONE);%>
    <% } %>

    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_CODE_RATTACHEMENT+"_REQ").equals("1")) { %>
        <% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, "Structure de rattachement", "", univFmt.CONTEXT_STRUCTURE); %>
    <% } %>

    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_CODE_REDACTEUR+"_REQ").equals("1")) { %>
    <p>
        <label for="REDACTEUR"><%=MessageHelper.getCoreMessage("ST_REQUETEUR_REDACTEUR")%></label>
        <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, RequeteurFiches.ATTRIBUT_REDACTEUR, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 20, "Rédacteur");%>
    </p>
    <% } %>

    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_CODE_OBJET+"_REQ").equals("1")) { %>
    <p>
        <label for="CODE_OBJET"><%=MessageHelper.getCoreMessage("ST_REQUETEUR_OBJET")%></label>
        <% univFmt.insererContenuComboHashtable(fmt, out, infoBean, RequeteurFiches.ATTRIBUT_CODE_OBJET, FormateurJSP.SAISIE_FACULTATIF , "LISTE_"+RequeteurFiches.ATTRIBUT_CODE_OBJET, "","");%>
    </p>
    <% } %>

    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_DIFFUSION_PUBLIC_VISE+"_REQ").equals("1")) { %>
        <% univFmt.insererkMonoSelect(fmt, out, infoBean, RequeteurFiches.ATTRIBUT_DIFFUSION_PUBLIC_VISE, FormateurJSP.SAISIE_FACULTATIF, "Groupe", "", UnivFmt.CONTEXT_GROUPEDSI_PUBLIC_VISE); %>
    <% } %>

    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_ETAT_OBJET+"_REQ").equals("1")) { %>
    <p>
        <label for="ETAT_OBJET"><%=MessageHelper.getCoreMessage("ST_REQUETEUR_ETAT")%></label>
        <% univFmt.insererContenuComboHashtable(fmt, out, infoBean, RequeteurFiches.ATTRIBUT_ETAT_OBJET, FormateurJSP.SAISIE_FACULTATIF, ReferentielObjets.getEtatsObjetFront(),"Etat");%>
    </p>
    <% } %>

    <% if (infoBean.getString(RequeteurFiches.ATTRIBUT_LANGUE+"_REQ").equals("1")) { %>
    <p>
        <label for="LANGUE"><%=MessageHelper.getCoreMessage("ST_REQUETEUR_LANGUE")%></label>
        <% fmt.insererComboHashtable(out, infoBean, RequeteurFiches.ATTRIBUT_LANGUE, FormateurJSP.SAISIE_FACULTATIF, "LISTE_LANGUES","Langue");%>
    </p>
    <% } %>
</fieldset>

<p class="validation">
    <input class="submit" type="button" name="OK" value="<%=MessageHelper.getCoreMessage("ST_REQUETEUR_FILTRER")%>" onclick="envoyerFormulaire('LISTE','','0');" />
</p>
