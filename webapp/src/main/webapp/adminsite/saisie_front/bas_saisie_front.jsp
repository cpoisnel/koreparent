<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.frontoffice.util.JSPIncludeHelper" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%
String typeObjet = ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")).toUpperCase();
JSPIncludeHelper.includePluginFicheSaisiesFo(out, getServletConfig().getServletContext(), request, response, ReferentielObjets.instancierFiche(typeObjet).getClass().getName());

if (infoBean.get("NOTIFICATION_MAIL") != null) { %>
    <p>
        <label for="ENVOI_MAIL"><%=MessageHelper.getCoreMessage("ST_FRONT_ENVOI_MAIL")%></label>
        <%fmt.insererChampSaisie(out, infoBean, "ENVOI_MAIL", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %>
    </p>
<% } %>

<% if (infoBean.getEcranLogique().equals("PRINCIPAL") && StringUtils.isEmpty(ContexteUtil.getContexteUniv().getCode())) { %>
    <p>
        <label for="MAIL_ANONYME"><%=MessageHelper.getCoreMessage("ST_MAILANONYME")%> <%= UnivFmt.getModeSaisieZone(infoBean,"SAISIE_FRONT.MAIL_ANONYME") == FormateurJSP.SAISIE_OBLIGATOIRE ? "*" : "" %></label>
        <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "MAIL_ANONYME", UnivFmt.getModeSaisieZone(infoBean,"SAISIE_FRONT.MAIL_ANONYME"), FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_MAILANONYME"), "EMAIL=YES", true, "");%>
    </p>
    <p class="validation_saisie_honeypot">
        <label for="VALIDATION_ANONYME"><%=MessageHelper.getCoreMessage("ST_VALIDATION_HONEYPOT")%></label>
        <input type="text" id="VALIDATION_ANONYME" name="VALIDATION_ANONYME" value="" tabindex="-1">
    </p>
<% } %>

<jsp:include page="/adminsite/saisie_front/fiche_bas_type_enregistrement.jsp" />

<p id="valider-formulaire">
    <input class="submit" type="submit" name="ENREGISTRER" value="<%=MessageHelper.getCoreMessage("JTF_BOUTON_VALIDER")%>" />
    <input class="reset" type="button" name="ABANDONNER" value="<%=MessageHelper.getCoreMessage("JTF_BOUTON_ABANDONNER")%>" onclick="javascript:history.back();" />
</p>

</form>
