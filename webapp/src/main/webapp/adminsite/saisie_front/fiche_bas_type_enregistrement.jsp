<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<%-- *********         FICHE EN BROUILLON        ************* --%>
<% if ("0001".equals(infoBean.getString("ETAT_OBJET"))) { %>
    <input type="hidden" name="TYPE_ENREGISTREMENT" value="1"/>
<% } %>

<%-- *********         FICHE EN COURS DE VALIDATION        ************* --%>
<% if ("0002".equals(infoBean.get("ETAT_OBJET"))) {
     if ("1".equals(infoBean.getString("GRS_AUTORISATION_VALIDATION"))) { %>
        <div class="multi-col">
            <span>&nbsp;</span>
            <table summary="">
                <tr>
                    <td><input type="radio" name="TYPE_ENREGISTREMENT" id="TYPE_ENREGISTREMENT_3" value="3"/></td>
                    <td><label for="TYPE_ENREGISTREMENT_3"><%=MessageHelper.getCoreMessage("ST_FRONT_RETOUR_REDACTEUR")%></label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "MOTIF_RETOUR", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 512, MessageHelper.getCoreMessage("ST_FRONT_MOTIF_RETOUR"));%></td>
                </tr>
                <tr>
                    <td><input type="radio" name="TYPE_ENREGISTREMENT" id="TYPE_ENREGISTREMENT_2" value="2" checked="checked"/></td>
                    <td><label for="TYPE_ENREGISTREMENT_2"><%=MessageHelper.getCoreMessage("ST_FRONT_MISE_EN_LIGNE")%></label></td>
                </tr>
            </table>
         </div> <!-- .multi-col -->
    <% } else { %>
        <div class="multi-col">
            <span>&nbsp;</span>
            <table summary="">
                <tr>
                    <td><%fmt.insererChampSaisie(out, infoBean, "ANNULER_DEMANDE_VALIDATION", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %></td>
                    <td><%=MessageHelper.getCoreMessage("ST_FRONT_ANNULER_VALIDATION")%> :</td>
                </tr>
            </table>
         </div> <!-- .multi-col -->
    <% } %>
<% } %>

<%-- *********         FICHE EN LIGNE        ************* --%>
<% if ("0003".equals(infoBean.get("ETAT_OBJET"))) { %>
    <input type="hidden" name="TYPE_ENREGISTREMENT" value="1"/>
<% } %>
