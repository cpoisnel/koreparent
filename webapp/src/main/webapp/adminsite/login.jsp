<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.identification.SourceAuthHelper"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.core.config.PropertyHelper"%>
<%@page import="com.kportal.core.webapp.WebAppUtil"%>
<%@ page import="com.univ.utils.ContexteUniv, com.univ.utils.ContexteUtil" errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<%
boolean kUserConnector = SourceAuthHelper.isDoubleSourceAuthActif(request,infoBean);
ContexteUniv ctx = ContexteUtil.getContexteUniv();
String locale = ctx.getLocale().getLanguage();
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<%= locale %>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<%= locale %>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<%= locale %>"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="<%= locale %>"><!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <title><%= MessageHelper.getCoreMessage("BO_MODULE_ADMINISTRATION") %></title>

        <link rel="shortcut icon" type="image/x-icon" href="/adminsite/images/favicon.ico" />
        <link rel="icon" type="image/png" href="/adminsite/images/favicon.png" />

        <meta name="description" content="<%= MessageHelper.getCoreMessage("BO_MODULE_ADMINISTRATION") %>" />
        <meta name="viewport" content="width=device-width" />

        <link rel="stylesheet" media="screen, projection"  href="/adminsite/styles/screen.css" />

    </head>
    <body class="login">

        <div id="header">
            <div id="header_deco">
                <img id="logo" src="<%= PropertyHelper.getCoreProperty("application.logo") %>" alt="<%= PropertyHelper.getCoreProperty("application.nom") %>" />
            </div><!-- #header_deco -->
        </div><!-- #header -->

        <div id="page">
            <div id="entete" class="connexion">
                <h1><%= MessageHelper.getCoreMessage("BO_MODULE_ADMINISTRATION") %></h1>
            </div><!-- #entete -->

            <div id="content" class="connexion" role="main">
                <form action="<%= WebAppUtil.SG_PATH %>" method="post" <%=("0".equals(PropertyHelper.getCoreProperty("utilisateur.login.autocomplete"))?"autocomplete=\"off\"":"")%>><%
                    // Double identification (CAS + KDB), ajout d'un paramétre à retourner au processus d'IDENTIFICATION.
                    if (kUserConnector) { %>
                        <input type="hidden" name="KUSER_CONNECTOR" value="1"/><%
                    }%>
                    <fieldset>
                        <legend><%= MessageHelper.getCoreMessage("BO_MIRE_LOGIN") %></legend><%
                        if (StringUtils.isNotBlank(infoBean.getMessageErreur())) {
                            %><p class="message erreur"><%= infoBean.getMessageErreur() %></p><%
                        }
                        %><input type="hidden" name="PROC" value="IDENTIFICATION" />
                        <input type="hidden" name="#ECRAN_LOGIQUE#" value="PRINCIPAL" />
                        <p id="login_login">
                            <label for="identifiant"><%= MessageHelper.getCoreMessage("BO_LOGIN") %></label><!--
                            --><input type="text" name="LOGIN" id="identifiant" required="required" autofocus="autofocus" />
                        </p> <!-- #login_login -->
                        <p id="login_pass">
                            <label for="mot_passe"><%= MessageHelper.getCoreMessage("BO_PASSWORD") %></label><!--
                            --><input id="mot_passe" type="password" name="PASSWORD" required="required" />
                        </p> <!-- #login_pass -->
                        <p class="validation"><input id="valider" type="submit" name="VALIDER" value="<%= MessageHelper.getCoreMessage("JTF_BOUTON_VALIDER") %>" /></p>
                    </fieldset>
                </form>
            </div><!-- #content.connexion -->
        </div><!-- #page -->
        <jsp:include page="/adminsite/template/footer.jsp" />
        <jsp:include page="/adminsite/sso/propagation.jsp" />
    </body>
</html>
