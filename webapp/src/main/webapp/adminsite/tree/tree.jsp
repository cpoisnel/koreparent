<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Locale"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.session.SessionUtilisateur"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.univ.objetspartages.om.AutorisationBean"%>
<%@page import="com.univ.tree.utils.JsTreeUtils"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.EscapeString" %>
<%@page import="com.univ.utils.SessionUtil"%>
<%@taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
request.setCharacterEncoding("UTF-8");

    final ContexteUniv ctx = ContexteUtil.getContexteUniv();
    final AutorisationBean autorisations = ctx.getAutorisation();
    final Locale locale = ctx.getLocale();
    final String root = StringUtils.defaultString(request.getParameter("RACINE"));
    final boolean multiSelect = Boolean.parseBoolean(StringUtils.defaultString(request.getParameter("MULTISELECT")));
    final boolean twoState = Boolean.parseBoolean(StringUtils.defaultString(request.getParameter("UNBINDED_SELECT")));
    final boolean dnd = Boolean.parseBoolean(StringUtils.defaultString(request.getParameter("DND")));
    final boolean actions = Boolean.parseBoolean(StringUtils.defaultString(request.getParameter("ACTIONS")));

//si l'utilisateur a perdu sa session, ou bien qu'il n'a pas les droits sur l'onglet administration, on le redirige vers le login
//si on a pas de controle des permissions, on a pas besoin de rediriger (par exemple formulaire de recherche ou saisie front anonyme)
if ((SessionUtil.getInfosSession(request).get(SessionUtilisateur.CODE) == null || autorisations == null)) {
    request.getRequestDispatcher("/adminsite/").forward(request, response);
}
if(!StringUtils.isBlank(request.getParameter("DISPLAY")) && request.getParameter("DISPLAY").equals("full")){
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="<%= locale.getLanguage() %>"><!--<![endif]-->
    <head>
        <meta charset="UTF-8"/>
        <title><%= MessageHelper.getCoreMessage("DATAGRID.VIEWTOOL.VUE_ARBO") %></title>
        <meta name="viewport" content="width=device-width">
        <resources:link group="stylesBo"/>
        <link rel="stylesheet" media="screen, projection"  href="/adminsite/styles/screen.css">
    </head>
    <body>
        <div id="page">
            <div id="content">
<%} %>

                <%if(StringUtils.isBlank(request.getParameter("DISPLAY"))){ %>
                <div class="filtre">
                    <label for="kTreeSearch"><%= MessageHelper.getCoreMessage("DATAGRID.VIEWTOOL.FILTRER") %>
                        <input type="text" id="kTreeSearch" class="kTree-search">
                    </label>
                    <%if(!StringUtils.isBlank(request.getParameter("VIEW_SWITCHER")) && request.getParameter("VIEW_SWITCHER").equals("true")){%>
                        <div class="resume">
                            <div class="dropdown" style="display: inline-block; text-align: left;">
                                <strong data-toggle="dropdown" class="dropdown-toggle button"><img src="/adminsite/styles/img/16x16/arborescence.png" alt="Arborescence"><%= MessageHelper.getCoreMessage("DATAGRID.VIEWTOOL.VUE_ARBO") %></strong>
                                <div class="dropdown-menu">
                                    <ul>
                                        <li><strong><%= MessageHelper.getCoreMessage("DATAGRID.VIEWTOOL.CHANGER_VUE") %></strong>
                                            <ul>
                                                <li><img alt="<%= MessageHelper.getCoreMessage("DATAGRID.VIEWTOOL.VUE_ARBO") %>" src="/adminsite/styles/img/16x16/arborescence.png"><a href="#" data-targetid="treeView" onclick="toggleView(this);"><%= MessageHelper.getCoreMessage("DATAGRID.VIEWTOOL.VUE_ARBO") %></a></li>
                                                <li><img alt="<%= MessageHelper.getCoreMessage("DATAGRID.VIEWTOOL.VUE_TABLE") %>" src="/adminsite/styles/img/16x16/table.png"><a href="#" data-targetid="listView" onclick="toggleView(this);"><%= MessageHelper.getCoreMessage("DATAGRID.VIEWTOOL.VUE_TABLE") %></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <%}%>
                </div>
                <%} %>
                <div class="kTree" data-url="<%=EscapeString.escapeAttributHtml(JsTreeUtils.getAjaxUrl(request))%>"
                                   data-filterUrl="<%=EscapeString.escapeAttributHtml(JsTreeUtils.getFilterUrl(request)) %>"
                                   data-selected="<%= StringUtils.defaultString(JsTreeUtils.getSelected(request))%>"
                                   data-multiselect="<%= multiSelect%>"
                                   data-twostate="<%= twoState %>"
                                   data-dnd="<%=dnd%>"
                                   data-actions="<%=actions%>"
                                   data-root="<%= root %>">
                </div>
<%if(!StringUtils.isBlank(request.getParameter("DISPLAY")) && request.getParameter("DISPLAY").equals("full")){ %>
            </div>
        </div>
        <resources:script group="scriptsBo" locale="<%= locale.toString() %>"/>
    </body>
</html>
<%} %>