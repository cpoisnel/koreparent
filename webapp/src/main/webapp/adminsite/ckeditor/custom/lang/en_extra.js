CKEDITOR.plugins.languagemerge.mergeExtra('en', {
    'format': {
        'tag_h2': 'Header 1 (&lt;h2&gt;)',
        'tag_h3': 'Header 2 (&lt;h3&gt;)',
        'tag_h4': 'Header 3 (&lt;h4&gt;)',
        'tag_h5': 'Header 4 (&lt;h5&gt;)',
        'tag_h6': 'Header 5 (&lt;h6&gt;)',
        'tag_address': 'Address (&lt;address&gt;)',
        'tag_cite': 'Reference quotation (&lt;cite&gt;)',
        'tag_q': 'Short quotation (&lt;q&gt;)'
    }
});
