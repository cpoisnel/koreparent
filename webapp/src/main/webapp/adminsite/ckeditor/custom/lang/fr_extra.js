CKEDITOR.plugins.languagemerge.mergeExtra('fr', {
    'format': {
        'tag_h2': 'Titre 1 (&lt;h2&gt;)',
        'tag_h3': 'Titre 2 (&lt;h3&gt;)',
        'tag_h4': 'Titre 3 (&lt;h4&gt;)',
        'tag_h5': 'Titre 4 (&lt;h5&gt;)',
        'tag_h6': 'Titre 5 (&lt;h6&gt;)',
        'tag_address': 'Adresse (&lt;address&gt;)',
        'tag_cite': 'Citation de référence (&lt;cite&gt;)',
        'tag_q': 'Citation courte (&lt;q&gt;)'
    }
});
