(function () {
    'use strict';
    CKEDITOR.plugins.kflash = {
        getSelectedFlash: function (editor, element) {
            if (!element) {
                var sel = editor.getSelection();
                element = sel.getSelectedElement();
            }
            if (element && element.hasClass('cke_kflash') && !element.isReadOnly()) {
                return editor.restoreRealElement(element);
            }
        },
        getEditUrl: function (editor) {
            var url = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=FLASH&FCK_PLUGIN=TRUE',
                processConfiguration = CKEDITOR.plugins.kmedia.getProcessConfiguration(),
                selectedFlash = CKEDITOR.plugins.kflash.getSelectedFlash(editor),
                flashProperties = CKEDITOR.plugins.kflash.getFlashProperties(selectedFlash);
            if (processConfiguration.code && processConfiguration.objet && processConfiguration.idFiche) {
                url += '&OBJET=' + processConfiguration.objet;
                url += '&CODE=' + processConfiguration.code;
                url += '&ID_FICHE=' + processConfiguration.idFiche;
                url += '&MODE_ONGLETS=' + (selectedFlash ? 'insertion' : 'selection');
                if (selectedFlash && selectedFlash.$.innerHTML.indexOf('[id-image]') !== -1 && selectedFlash.$.innerHTML.indexOf('[/id-image]') !== -1) {
                    url += '&ID_MEDIA=' + selectedFlash.$.innerHTML.split('[id-image]')[1].split('[/id-image]')[0];
                }
                for (var property in flashProperties) {
                    if (flashProperties.hasOwnProperty(property)) {
                        url += '&' + property + '=' + flashProperties[property];
                    }
                }
            }
            return url;
        },
        createFakeFlash: function (editor, element) {
            return editor.createFakeElement(element, 'cke_kflash', 'object');
        },
        getFlashProperties: function (element) {
            var props = {};
            if (element) {
                var flashObject = element.getElementsByTag('cke:object').$[0];
                if (!flashObject) {
                    flashObject = element.getElementsByTag('object').$[0];
                }
                props.URL = new CKEDITOR.dom.element(flashObject).getAttribute('data');
                props.WIDTH = element.getStyle('width').replace(/[a-zA-Z]/g, '');
                props.HEIGHT = element.getStyle('height').replace(/[a-zA-Z]/g, '');
                props.HORIZONTAL_MARGIN = element.getStyle('margin-left').replace(/[a-zA-Z]/g, '');
                props.VERTICAL_MARGIN = element.getStyle('margin-top').replace(/[a-zA-Z]/g, '');
                props.BORDER = element.getStyle('border-width').replace(/[a-zA-Z]/g, '');
                props.ALIGN = element.getStyle('float');
            }
            return props;
        }
    };
    // kAudio plugin
    CKEDITOR.plugins.add(
        'kflash', {
            requires: ['kiframedialog', 'kmedia'],
            init: function (editor) {
                editor.addContentsCss(this.path + 'css/style.css');
                editor.addCommand('kflash', new CKEDITOR.dialogCommand('kflash'));
                editor.ui.addButton(
                    'kFlash', {
                        label: LOCALE_BO.ckeditor.plugins.kflash.title,
                        command: 'kflash',
                        icon: this.path + 'icons/kFlash.png'
                    });
                if (editor.contextMenu) {
                    editor.addMenuGroup( 'kflash' );
                    editor.addMenuItem(
                        'kflash', {
                            label: LOCALE_BO.ckeditor.plugins.kflash.menu,
                            command: 'kflash',
                            group: 'kflash',
                            icon: this.path + 'icons/kFlash.png'
                        });
                }
                editor.contextMenu.addListener(
                    function (element) {
                        if (CKEDITOR.plugins.kflash.getSelectedFlash(editor, element)) {
                            return { 'kflash': CKEDITOR.TRISTATE_OFF};
                        }
                    });
                editor.on(
                    'doubleclick', function (evt) {
                        var element = evt.data.element;
                        if (element.hasClass('cke_kflash') && !element.isReadOnly()) {
                            evt.data.dialog = 'kflash';
                        }
                    }, null, null, 100);
                CKEDITOR.dialog.addKIframe(
                    'kflash', LOCALE_BO.ckeditor.plugins.kflash.title,
                    CKEDITOR.plugins.kflash.getEditUrl,
                    940, 555,
                    function () {
                    }, {
                        onOk: function (e) {
                            var dialogEditor = this._.editor,
                                iframeDocument = e.sender.iframeDom.contentWindow.document,
                                element = new CKEDITOR.dom.element(iframeDocument.querySelector('.kflash')),
                                range = dialogEditor.getSelection().getRanges()[0],
                                flashObject = iframeDocument.querySelector('object');
                            dialogEditor.insertElement(CKEDITOR.plugins.kflash.createFakeFlash(dialogEditor, element));
                            range.select();
                            if (flashObject) {
                                flashObject.parentNode.removeChild(flashObject);
                            }
                        },
                        onHide: function (e) {
                            var iframeDocument = e.sender.iframeDom.contentWindow.document,
                                flashObject = iframeDocument.querySelector('object');
                            if (flashObject) {
                                flashObject.parentNode.removeChild(flashObject);
                            }
                        }
                    });
            },
            afterInit: function (editor) {
                // Empty anchors upcasting to fake objects.
                editor.dataProcessor.dataFilter.addRules(
                    {
                        elements: {
                            div: function (element) {
                                if (element.hasClass('kflash')) {
                                    return editor.createFakeParserElement(element, 'cke_kflash', 'kflash');
                                }
                                return null;
                            }
                        }
                    });
                var pathFilters = editor._.elementsPath && editor._.elementsPath.filters;
                if (pathFilters) {
                    pathFilters.push(
                        function (element, name) {
                            if (name === 'div') {
                                if (element.hasClass('kflash')) {
                                    return 'kflash';
                                }
                            }
                        });
                }
            }
        });
})();
