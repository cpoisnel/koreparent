﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.dialog.add(
    'kAnchor', function (editor) {
        'use strict';
        // Function called in onShow to load selected element.
        var loadElements = function (element) {
            this._.selectedElement = element;
            var attributeValue = element.data('cke-saved-name');
            this.setValueOf('info', 'txtName', attributeValue || '');
        };

        function onKeyUp(e) {
            validateField(e.sender);
        }

        function validateField(element) {
            var valid = element.getValue() && /^[a-zA-Z][\w:.-]*$/.test(element.getValue()),
                errorDiv = new CKEDITOR.dom.element(document.getElementById(element.getAttribute('id') + '-error') || 'div');
            if (!valid) {
                if (!element.hasClass('error')) {
                    errorDiv.setAttributes(
                        {
                            'id': element.getAttribute('id') + '-error'
                        });
                    errorDiv.addClass('error').addClass('bottom');
                    errorDiv.appendHtml('Le nom doit commencer par une lettre (majuscule ou minuscule) et ne doit contenir que les caractères suivant : A-Z, a-z, 0-9, :-_.');
                    errorDiv.insertAfter(element);
                    element.addClass('error');
                    element.on('keyup', onKeyUp);
                }
                return false;
            } else {
                errorDiv.remove();
                element.removeClass('error');
                element.removeListener('keyup', onKeyUp);
            }
            return true;
        }

        function createFakeAnchor(cuurentEditor, attributes) {
            return cuurentEditor.createFakeElement(
                cuurentEditor.document.createElement(
                    'a', {
                        attributes: attributes
                    }), 'cke_kanchor', 'anchor');
        }

        return {
            title: editor.lang.link.anchor.title,
            width: 300,
            height: 80,
            resizable: CKEDITOR.DIALOG_RESIZE_NONE,
            onOk: function () {
                var dialogEditor = this.getParentEditor();
                var name = CKEDITOR.tools.trim(this.getValueOf('info', 'txtName'));
                var attributes = {
                    id: name,
                    name: name,
                    'data-cke-saved-name': name
                };
                if (this._.selectedElement) {
                    if (this._.selectedElement.data('cke-realelement')) {
                        var newFake = createFakeAnchor(dialogEditor, attributes);
                        newFake.replace(this._.selectedElement);
                        // Selecting fake element for IE. (#11377)
                        if (CKEDITOR.env.ie) {
                            dialogEditor.getSelection().selectElement(newFake);
                        }
                    } else {
                        this._.selectedElement.setAttributes(attributes);
                    }
                } else {
                    var sel = dialogEditor.getSelection(),
                        range = sel && sel.getRanges()[0];
                    // Empty anchor
                    if (range.collapsed) {
                        var anchor = createFakeAnchor(dialogEditor, attributes);
                        range.insertNode(anchor);
                    } else {
                        if (CKEDITOR.env.ie && CKEDITOR.env.version < 9) {
                            attributes['class'] = 'cke_anchor';
                        }
                        // Apply style.
                        var style = new CKEDITOR.style(
                            {
                                element: 'a',
                                attributes: attributes
                            });
                        style.type = CKEDITOR.STYLE_INLINE;
                        dialogEditor.applyStyle(style);
                    }
                }
            },
            onHide: function () {
                var inputElement = this.getContentElement('info', 'txtName'),
                    input = new CKEDITOR.dom.element(document.getElementById(inputElement._.inputId)),
                    errorDiv = new CKEDITOR.dom.element(document.getElementById(input.getAttribute('id') + '-error') || 'div');
                errorDiv.remove();
                input.removeClass('error');
                delete this._.selectedElement;
            },
            onShow: function () {
                var dialogEditor = this.getParentEditor(),
                    sel = dialogEditor.getSelection(),
                    fullySelected = sel.getSelectedElement(),
                    fakeSelected = fullySelected && fullySelected.data('cke-realelement'),
                    linkElement = fakeSelected ?
                        CKEDITOR.plugins.link.tryRestoreFakeAnchor(dialogEditor, fullySelected) :
                        CKEDITOR.plugins.link.getSelectedLink(dialogEditor);
                if (linkElement) {
                    loadElements.call(this, linkElement);
                    !fakeSelected && sel.selectElement(linkElement);
                    if (fullySelected) {
                        this._.selectedElement = fullySelected;
                    }
                }
                this.getContentElement('info', 'txtName').focus();
            },
            contents: [{
                id: 'info',
                label: editor.lang.link.anchor.title,
                accessKey: 'I',
                elements: [{
                    type: 'text',
                    id: 'txtName',
                    label: editor.lang.link.anchor.name,
                    required: true,
                    validate: function () {
                        var input = new CKEDITOR.dom.element(document.getElementById(this._.inputId));
                        return validateField(input);
                    }
                }]
            }]
        };
    });