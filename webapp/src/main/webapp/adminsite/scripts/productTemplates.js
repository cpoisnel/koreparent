(function() {
    'use strict';
    // Register a template definition set named "default".
    CKEDITOR.addTemplates( 'default',
        {
            // The name of the subfolder that contains the preview images of the templates.
            imagesPath: CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),
            // Template definitions.
            templates:
                [
                    {
                        title: 'Style 1',
                        image: 'template1.gif',
                        html:
                            '[style1;Saisir le titre ici]'
                    },
                    {
                        title: 'Style 2',
                        image: 'template1.gif',
                        html:
                            '[style2;Saisir le titre ici]'
                    },
                    {
                        title: 'Style 3',
                        image: 'template1.gif',
                        html:
                            '[style3;Saisir le titre ici]'
                    }
                ]
        });
})();
