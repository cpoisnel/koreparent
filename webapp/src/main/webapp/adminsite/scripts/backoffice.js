(function ($) {

    'use strict';

    // Delegate .transition() calls to .animate()
    // if the browser can't do CSS transitions.
    if (!$.support.transition) {
        $.fn.transition = $.fn.animate;
    }

    /**
     * Locale de la page courante
     */
    var locale = $('html').attr('lang');

    /**
     * Initialisation des champs de type date
     */
    $('.type_date').datepicker($.datepicker.regional[locale]);

    /**
     * Propage la déconnexion sur les urls fourni au lien de déconnexion
     */
    $('#deconnexion').click(function () {
        var that = $(this);
        var urlsDePropagation = that.data('propagation');
        var nbUrls = urlsDePropagation.length;
        var body = $('body');
        for (var i = 0; i < nbUrls; i++) {
            body.append('<script src="' + urlsDePropagation[i] + '"></script>');
        }
        setTimeout(function () {
            location.href = that.attr('href');
        }, 500);
        return false;
    });

    var saisieObjet = $('#saisie_objet');

    /**
     * Validation du formulaire ne fonction de l'action
     */
    var validerFormulaireSaisie = function (action, onglet) {
        if (action === 'APERCU') {
            saisieObjet.find('input[name=APERCU]').val('1');
            saisieObjet.find('input[name=ACTION]').val('ENREGISTRER');
            saisieObjet.attr('target', 'newWindow');
        } else {
            saisieObjet.find('input[name=ACTION]').val(action);
        }
        saisieObjet.find('input[name=SOUS_ONGLET_DEMANDE]').val(onglet || '');
        saisieObjet.submit();
        saisieObjet.find('input[name=APERCU]').val('0');
        saisieObjet.attr('target', '');
        saisieObjet.find('input[name=ACTION]').val(action);
    };

    /**
     * Gestion des actions sur les formulaires de saisies (enregistrer/dupliquer/supprimer...)
     */
    saisieObjet.find('[data-action]').on('click', function () {
        var that = $(this);
        var actionAConfirmer = that.data('confirm');
        var formulaire = that.closest('form');
        if (that.attr('name') && that.attr('value')) {
            formulaire.append('<input type="hidden" name="' + that.attr('name') + '" value="' + that.attr('value') + '"/>');
        }
        if (actionAConfirmer) {
            $('<div>')
                .html(actionAConfirmer)
                .dialog({
                    buttons: [{
                        text: LOCALE_BO.ok,
                        click: function () {
                            $(this).dialog('close');
                            validerFormulaireSaisie(that.data('action'), that.data('onglet'));
                        }
                    }, {
                        text: LOCALE_BO.fermer,
                        click: function () {
                            $(this).dialog('close');
                        }
                    }],
                    title: LOCALE_BO.confirmer
                });
        } else {
            validerFormulaireSaisie(that.data('action'), that.data('onglet'), that.closest('form'));
        }
    });

    $('button[data-confirm]:not([data-action])').on('click', function () {
        var that = $(this);
        var formulaire = that.closest('form');
        var actionAConfirmer = that.data('confirm');
        $('<div>')
            .html(actionAConfirmer)
            .dialog({
                buttons: [{
                    text: LOCALE_BO.ok,
                    click: function () {
                        $(this).dialog('close');
                        if (formulaire.has('input[name="ACTION"]').length) {
                            $('input[name="ACTION"]').val(that.attr('name'));
                        } else {
                            formulaire.append('<input type="hidden" name="ACTION" value="' + that.attr('name') + '"/>');
                        }
                        formulaire.submit();
                    }
                }, {
                    text: LOCALE_BO.fermer,
                    click: function () {
                        $(this).dialog('close');
                    }
                }],
                title: LOCALE_BO.confirmer
            });
        return false;
    });

    /**
     * L'action refuser sur les fiches doit renvoyer une dialogue avec une textarea
     * On envoie ensuite le formulaire après avoir positionner un input "TYPE_ENREGISTREMENT" à 3
     * Pourquoi? euh bha c'est historique
     */
    $('.js-refuser').click(function () {
        var that = $(this);
        $('#dialogRefuser').dialog({
            buttons: [{
                text: LOCALE_BO.valider,
                click: function () {
                    var formulaire = that.closest('form');
                    formulaire.append('<input type="hidden" name="TYPE_ENREGISTREMENT" value="3" />');
                    $('[data-action="ENREGISTRER"]').click();
                    $(this).dialog('close');
                }
            }]
        });
    });

    $('.js-refusal-user').click(function () {
        var $this = $(this);
        $('.js-refusal-user__form').dialog({
            title: LOCALE_BO.userFront.title,
            buttons: [{
                text: LOCALE_BO.valider,
                click: function () {
                    var $form = $this.closest('form');
                    $form.append('<input type="hidden" name="' + $this.attr('name') + '" value="' + $this.val() + '" />');
                    $(this).dialog('close');
                    $form.submit();
                }
            }]
        });
    });

    /**
     * Tout les liens externes doivent s'ouvrir dans une nouvelle fenêtre
     * Les liens du BO sont tous en relatif (théoriquement) donc ceux qui commence par http(s)
     * souvre en window.open
     */
    $('body').on('click', 'a[href^="http"]', function () {
        window.open(this.href);
        return false;
    });

    /**
     * Binding des boutons "Paramètrer" et "Supprimer" du gestionnaire d'extensions
     */
    $('.extension button.supprimer').click(function () {
        var $this = $(this),
            name = $this.closest('.extension_actions').siblings('.infos').find('p:first-child').html(),
            expandedIds = [];

        $('<div>').html($this.data('message')).dialog({
            buttons: [{
                text: LOCALE_BO.ok,
                click: function () {
                    $(this).dialog('close');
                    var datas = {
                        PROC: 'GESTION_EXTENSION',
                        ACTION: 'SUPPRIMER_EXTENSION',
                        ID_EXTENSION: $this.data('extensionid')
                    };

                    $('.extension-modules.expanded').closest('li').each(function () {
                        expandedIds.push($(this).attr('id'));
                    });

                    $.simAsync({
                        loaderMessage: LOCALE_BO.lExtension + ' ' + name + ' ' + LOCALE_BO.enCoursSuppression + LOCALE_BO.patienter,

                        reloadParams: {EXPANDED: expandedIds.join(';')},

                        baseUrl: '/servlet/com.jsbsoft.jtf.core.SG',
                        baseParams: datas,
                        displayEndStatus: true,
                        successMessage: LOCALE_BO.enSuccesSuppression,
                        errorMessage: LOCALE_BO.enErreurSuppression
                    });
                }
            }, {
                text: LOCALE_BO.fermer,
                click: function () {
                    $(this).dialog('close');
                }
            }],
            title: LOCALE_BO.confirmer
        });
    });

    $('.extension_actions button.interrupteur:not(.verrou)').click(function () {
        var $this = $(this),
            name = $this.closest('.extension_actions').siblings('.infos').find('p:first-child').html(),
            extension = $this.closest('.extension').length > 0,
            target = $this.closest('li[id]').length > 0 ? $this.closest('li[id]').attr('id') : null,
            expandedIds = [];
        // Prepare ajax request
        var datas = {PROC: 'GESTION_EXTENSION', ACTION: extension ? 'ACTIVER_EXTENSION' : 'ACTIVER_MODULE'};
        if (extension) {
            datas.ID_EXTENSION = $this.data('extensionid');
        } else {
            datas.ID_MODULE = $this.data('extensionid');
        }

        // Retrieve currently expanded li
        $('.extension-modules.expanded').closest('li').each(function () {
            expandedIds.push($(this).attr('id'));
        });

        // Execute request and reload page
        $.simAsync({
            loaderMessage: (extension ? LOCALE_BO.lExtension : LOCALE_BO.leModule) + ' ' + name + ' ' + ($this.is('.on') ? LOCALE_BO.enCoursdeDesactivation : LOCALE_BO.enCoursdActivation) + LOCALE_BO.patienter,

            reloadParams: {EXPANDED: expandedIds.join(';')},
            reloadTarget: target,

            baseUrl: '/servlet/com.jsbsoft.jtf.core.SG',
            baseParams: datas,
            displayEndStatus: true,
            successMessage: ($this.is('.on') ? LOCALE_BO.enSuccesDesactivation : LOCALE_BO.enSuccesActivation),
            errorMessage: ($this.is('.on') ? LOCALE_BO.enErreurDesactivation : LOCALE_BO.enErreurActivation)
        });

        // Switch state
        if ($this.is('.on')) {
            $this.html(LOCALE_BO.activer);
            $this.removeClass('on').addClass('off');
        } else {
            $this.html(LOCALE_BO.desactiver);
            $this.removeClass('off').addClass('on');
        }
        if ($this.closest('li[id]').is('.actif')) {
            $this.removeClass('actif').addClass('inactif');
        } else {
            $this.removeClass('inactif').addClass('actif');
        }
    });

    $('#actions_composant .recharger a, #actions_composant .restaurer a').click(function () {
        var $this = $(this),
            url = $this.attr('href');
        $.simAsync({
            loaderMessage: LOCALE_BO.enCoursRechargement + LOCALE_BO.patienter,
            baseUrl: url,
            displayEndStatus: true,
            successMessage: LOCALE_BO.enSuccesRechargement,
            errorMessage: LOCALE_BO.enErreurRechargement
        });
        return false;
    });

    function updateExtensionsLayout($panel, animate) {
        $panel.each(function () {
            var $this = $(this),
                $li = $this.closest('li'),
                height = $this.outerHeight() + ($li.innerHeight() - $li.height());
            if ($this.is('.expanded')) {
                $li.transition({height: '+=' + height + 'px'}, animate ? 500 : 0);
            } else {
                $li.transition({height: '-=' + height + 'px'}, animate ? 500 : 0);
            }
        });
    }

    $('.extension button.details').click(function () {
        var $this = $(this),
            $modulesPanel = $this.closest('.extension').siblings('.extension-modules');

        if ($modulesPanel.is('.collapsed')) {
            $this.removeClass('deplier').addClass('plier');
            $modulesPanel.removeClass('collapsed').addClass('expanded');
        } else {
            $this.removeClass('plier').addClass('deplier');
            $modulesPanel.removeClass('expanded').addClass('collapsed');
        }

        updateExtensionsLayout($modulesPanel, true);
    });

    $('.rights_action button.details').click(function () {
        var $this = $(this),
            $rightsPanel = $this.closest('.rights_informations').siblings('.rights');

        if ($rightsPanel.is('.collapsed')) {
            $this.removeClass('deplier').addClass('plier');
            $rightsPanel.removeClass('collapsed').addClass('expanded');
        } else {
            $this.removeClass('plier').addClass('deplier');
            $rightsPanel.removeClass('expanded').addClass('collapsed');
        }

        updateExtensionsLayout($rightsPanel, true);
    });

    updateExtensionsLayout($('.extension-modules.expanded'));

    /**
     * dropdowns bootstrap
     */
    $('.dropdown-toggle').dropdown();

    $('.infobulle').tooltip({
        html: true,
        placement: 'right',
        trigger: 'hover focus'
    });

    /**
     * Session keep alive when user is filling a form
     */
    setInterval(function () {
        $.ajax(window.location.origin + '/servlet/com.kportal.servlet.KeepAliveServlet');
    }, 1740000);

    $('.js-media-component').mediaComponent({});

})(jQuery.noConflict());
