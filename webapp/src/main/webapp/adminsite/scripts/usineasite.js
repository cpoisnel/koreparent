(function ($) {
    'use strict';

    $('#selectTemplate').on('change', function () {
        $('.donnees_template').addClass('masquer');
        $('#' + this.value).removeClass('masquer');
    }).triggerHandler('change');

    var changementReecritureURL = function (valeurSelectionnee) {
        if (valeurSelectionnee === '1') {
            $('#niveauReecritureMin').removeAttr('disabled');
            $('#niveauReecritureMax').removeAttr('disabled');
        } else {
            $('#niveauReecritureMin').attr('disabled', 'disabled');
            $('#niveauReecritureMax').attr('disabled', 'disabled');
        }
    };

    changementReecritureURL($('input[name=MODE_REECRITURE]:checked').val());

    $('input[name=MODE_REECRITURE]').on('change', function () {
        changementReecritureURL(this.value);
    });

    $('.fileupload').each(function () {
        $(this).fileupload({'name': $(this).data('name')});
    });

    $('[data-dismiss="fileupload"]').on('click', function () {
        var nomElementLie = $(this).data('hiddeninputname');
        $('#' + nomElementLie).val('');
    });

})(jQuery.noConflict());
