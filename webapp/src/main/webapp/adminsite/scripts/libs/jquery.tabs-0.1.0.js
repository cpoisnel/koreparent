/*
 * $tabs jQuery plugin
 *
 * version 0.1.0
 *
 * Kosmos
 *
 * Files below need to be referenced in the calling page for the component to run correctly :
 *
 * 	JS :
 * 	- jQuery ( jquery-XX.XX.XX.js )
 *
 */

(function ($) {
    'use strict';

    $.tabs = function (root, options) {
        var $root = $(root),
            $panels = $('.js-tabs__panel', $root),
            settings;

        function fireEvent(type, data) {
            var event;
            if (document.createEvent) {
                event = document.createEvent('HTMLEvents');
                event.initEvent(type, true, true);
            } else {
                event = document.createEventObject();
                event.eventType = type;
            }
            event.eventName = type;
            event.data = data;
            if (document.createEvent) {
                $root[0].dispatchEvent(event);
            } else {
                $root[0].fireEvent('on' + event.eventType, event);
            }
        }

        //////////////////////////////////////////////////////
        //					PRIVATE METHODS					//
        //////////////////////////////////////////////////////

        // Initialize the component
        function init() {

            // Default settings
            var defaults = {};

            // Merge defaults and options settings
            settings = $.extend(true, {}, defaults, options);
            $.data(root, 'tabs', $root);
        }

        //////////////////////////////////////////////////////
        //					  UI UPDATES					//
        //////////////////////////////////////////////////////

        function showPanel($panel) {
            $panel.addClass('tabs__panel--active');
            fireEvent('TabChange', $panel);
        }

        function hidePanels(panels) {
            panels.removeClass('tabs__panel--active');
        }

        function updateItems($selected) {
            $('.js-tabs__item').removeClass('tabs__item--active');
            $selected.closest('.js-tabs__item').addClass('tabs__item--active');
        }

        //////////////////////UI UPDATES//////////////////////

        //////////////////////////////////////////////////////
        //					EVENTS HANDLING					//
        //////////////////////////////////////////////////////

        function onClick($link, $panel) {
            updateItems($link);
            hidePanels($panels);
            showPanel($panel);
        }

        function bindEvents() {
            $('.js-tabs__link', $root).on('click', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var $link = $(this),
                    $panel = $('.js-tabs__panel[data-panel="' + $link.attr('data-panel') + '"]', $root);
                onClick($link, $panel);
            });
            $('.js-tabs__item', $root).on('click', function() {
                var $this = $(this),
                    $link = $('.js-tabs__link', $this),
                    $panel = $('.js-tabs__panel[data-panel="' + $link.attr('data-panel') + '"]', $root);
                onClick($link, $panel);
            });
        }

        ////////////////////EVENTS HANDLING///////////////////

        ////////////////////PRIVATE METHODS///////////////////

        $root.getCurrentPanel = function() {
            return $('.js-tabs__panel.tabs__panel--active', $root);
        };

        $root.isPanelActive = function(panelName) {
            return $('.js-tabs__panel.tabs__panel--active', $root).is('[data-panel="' + panelName + '"]');
        };

        //////////////////////////////////////////////////////
        //					INITIALIZATION					//
        //////////////////////////////////////////////////////

        // Initialize the component
        init();
        bindEvents();

        ////////////////////INITIALIZATION////////////////////
    };


    $.fn.tabs = function (options) {
        if (options) {
            if (typeof options === 'object') {
                this.each(function () {
                    if ($(this).data('tabs')) {
                        if (options.remove) {
                            $(this).data('tabs').remove();
                            $(this).removeData('tabs');
                        }
                    } else if (!options.remove) {
                        if (options.enable === undefined && options.disable === undefined) {
                            options.enable = true;
                        }
                        new $.tabs(this, options);
                    }
                });

                if (options.instance) {
                    return $(this).data('tabs');
                }
                return this;
            }
        } else {
            return $(this).data('tabs');
        }
    };
})(jQuery);
