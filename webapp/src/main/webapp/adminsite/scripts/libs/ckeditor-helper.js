var CKEDITOR_HELPER = function helper() {
    'use strict';
    var toolboxesConfs = {};
    return {
        beforeDomMove: function beforeDomMove(editorName) {
            var currentInstance = CKEDITOR.instances[editorName];
            if (currentInstance) {
                toolboxesConfs[editorName] = currentInstance.config;
                currentInstance.destroy();
            }
        },
        afterDomMove: function afterDomMove(editorName) {
            if (toolboxesConfs[editorName]) {
                CKEDITOR.replace(editorName, toolboxesConfs[editorName]);
            }
        }
    };
}();