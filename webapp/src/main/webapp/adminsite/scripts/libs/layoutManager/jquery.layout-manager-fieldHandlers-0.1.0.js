/*
 * Layout-Manager fieldHandlers definitions
 *
 * version 0.1.0
 *
 * Kosmos
 *
 * Files below need to be referenced in the calling page for the component to run correctly :
 *
 * 	JS :
 * 	- jQuery ( jquery-XX.XX.XX.js )
 *  - layoutManager ( jquery.layout-manager-XX.XX.XX.js )
 *
 * Versions :
 *
 * 0.1.0 :
 *  - Field handlers creation
 *
 */
(function ($) {
    'use strict';

    $.layoutManagerHandlers.registerFieldHandler(
        [
            {
                name: 'ckToolbox',
                selector: '.js-ckeditor',
                edit: function (field, card) {
                    var $field = $(field);
                    $field.val(card.model[$field.attr('name')]);
                    return true;
                },
                save: function (field, card) {
                    var $field = $(field),
                        fieldName = $field.attr('name'),
                        editor = CKEDITOR.instances[fieldName];
                    if (editor && card.model.hasOwnProperty(fieldName)) {
                        editor.updateElement();
                        card.model[fieldName] = $field.val();
                        return true;
                    }
                    return false;
                }
            },
            {
                name: 'kMonoSelect',
                selector: '.kmonoselect',
                edit: function (field, card) {
                    if (field) {
                        var $component = $(field),
                            $libelle = $('input[type="hidden"][name^="LIBELLE_"]', $component),
                            name = $libelle.attr('name').replace('LIBELLE_', ''),
                            $code = $('#' + name, $component),
                            splitedValue = card.model[name] ? card.model[name].split('|') : ['', ''];
                        $component.kMonoSelect().clear();
                        $code.val(splitedValue[0]);
                        $libelle.val(splitedValue[1]);
                        $component.kMonoSelect().value(splitedValue[1], splitedValue[0]);
                        return true;
                    }
                    return false;
                },
                save: function (field, card) {
                    if (field) {
                        var $component = $(field),
                            $libelle = $('input[type="hidden"][name^="LIBELLE_"]', $component),
                            name = $libelle.attr('name').replace('LIBELLE_', ''),
                            $code = $('#' + name, $component);
                        card.model[name] = $code.val() && $libelle.val() ? $code.val() + '|' + $libelle.val() : null;
                        return true;
                    }
                    return false;
                }
            },
            {
                name: 'kMultiSelect',
                selector: '.kmultiselect-ttl, .kmultiselect-ltl',
                edit: function (field, card) {
                    if (field) {
                        var $component = $(field),
                            $compositionList = $('.kmultiselect-composition-list', $component),
                            $libelles = $('input[name^="LIBELLE_"]', $component),
                            name = $libelles.attr('name').replace('LIBELLE_', ''),
                            $input = $('#' + name, $component);
                        $input.val('');
                        $libelles.val('');
                        if (card.model[name]) {
                            $compositionList.empty();
                            $.each(card.model[name].split(';'), function (index, value) {
                                var pattern = /([0-9]+),LANGUE=([0-9])(,TYPE=[\w\-]+)?,(.+)/,
                                    match = pattern.exec(value);

                                var ignore = match[1].lastIndexOf('#AUTO#') === 0,
                                    langue = match[2] ? ',LANGUE=' + match[2] : '',
                                    type = match[3] ? ',TYPE=' + match[3] : '',
                                    realValue = match[1] + langue + type;
                                $input.val($input.val() + realValue + ';');
                                $libelles.val($libelles.val() + match[4] + ';');
                                $('<li>').addClass(ignore ? 'ignore' : '').html(match[4])
                                    .attr('data-value', realValue).attr('title', match[4]).appendTo($compositionList);
                            });
                            $component.kMultiSelect().update();
                            return true;
                        }
                    }
                    return false;
                },
                save: function (field, card) {
                    if (field) {
                        var $component = $(field),
                            $libelles = $('input[name^="LIBELLE_"]', $component),
                            name = $libelles.attr('name').replace('LIBELLE_', ''),
                            $input = $('#' + name, $component),
                            libelles = $.cleanArray($libelles.val().split(';')),
                            values = $.cleanArray($input.val().split(';')),
                            joinedValues = [];
                        if (libelles.length > 0) {
                            $.each(
                                values, function (index, value) {
                                    joinedValues.push(value + ',' + libelles[index]);
                                });
                            card.model[name] = joinedValues.join(';');
                            return true;
                        } else {

                            card.model[name] = null;
                        }
                    }
                    return false;
                }
            },
            {
                name: 'media-component',
                selector: 'input[name].js-media-component__id-ressource',
                edit: function (field, card) {
                    var $input = $(field);
                    $input.val(card.model[$input.attr('name')] ? JSON.stringify(card.model[$input.attr('name')]) : '');
                    $input.closest('.js-media-component').mediaComponent().refresh();
                    return true;
                },
                save: function (field, card) {
                    var $input = $(field);
                    if (card.model.hasOwnProperty($input.attr('name'))) {
                        card.model[$input.attr('name')] = $input.val() ? JSON.parse($input.val()) : null;
                        return true;
                    }
                    return false;
                }
            },
            {
                name: 'select',
                selector: 'select',
                edit: function (field, card) {
                    var $select = $(field);
                    $select.val(card.model[$select.attr('name')] ? card.model[$select.attr('name')] : '');
                    return true;
                },
                save: function (field, card) {
                    var $select = $(field);
                    if (card.model.hasOwnProperty($select.attr('name'))) {
                        card.model[$select.attr('name')] = $select.val() ? $select.val() : null;
                        return true;
                    }
                    return false;
                }
            },
            {
                name: 'textarea',
                selector: 'textarea',
                edit: function (field, card) {
                    var $input = $(field);
                    $input.val(card.model[$input.attr('name')] ? card.model[$input.attr('name')] : '');
                    return true;
                },
                save: function (field, card) {
                    var $input = $(field);
                    if (card.model.hasOwnProperty($input.attr('name'))) {
                        card.model[$input.attr('name')] = $input.val() ? $input.val() : null;
                        return true;
                    }
                    return false;
                }
            },
            {
                name: 'radio',
                selector: 'input[type="radio"]',
                edit: function (field, card) {
                    var $input = $(field);
                    $input.prop('checked', $input.val() === card.model[$input.attr('name')]);
                    return true;
                },
                save: function (field, card) {
                    var $input = $(field);
                    if (card.model.hasOwnProperty($input.attr('name'))) {
                        if ($input.is(':checked')) {
                            card.model[$input.attr('name')] = $input.val() ? $input.val() : null;
                        }
                        return true;
                    }
                    return false;
                }
            },
            {
                name: 'checkbox',
                selector: 'input[type="checkbox"]',
                edit: function (field, card) {
                    var $input = $(field);
                    var name = $input.attr('name');
                    var hasMultipleValues = $('input[type="checkbox"][name="' + name + '"]',
                            $input.parents('[data-card-class="' + card.model.class + '"]')).length > 1;
                    var isArray = $.isArray(card.model[name]) || hasMultipleValues;
                    var values,valuesArray;
                    if (isArray) {
                        valuesArray = card.model[name] || [];
                    } else {
                        values = card.model[name];
                    }
                    $input.prop('checked', isArray ? valuesArray.indexOf($input.val()) !== -1 : values);
                    return true;
                },
                save: function (field, card) {
                    var $input = $(field);
                    var name = $input.attr('name');
                    if (card.model.hasOwnProperty(name)) {
                        var hasMultipleValues = $('input[type="checkbox"][name="' + name + '"]',
                                $input.parents('[data-card-class="' + card.model.class + '"]')).length > 1;
                        var isArray = $.isArray(card.model[name]) || hasMultipleValues;
                        if (isArray) {
                            var valuesArray = card.model[name] || [];
                            if ($input.is(':checked')) {
                                if (valuesArray.indexOf($input.val()) === -1) {
                                    valuesArray.push($input.val());
                                }
                            } else {
                                var index = valuesArray.indexOf($input.val());
                                if (index !== -1) {
                                    valuesArray.splice(index);
                                }
                            }
                            card.model[name] = valuesArray;
                        } else {
                            card.model[name] = $input.is(':checked');
                        }
                        return true;
                    }
                    return false;
                }
            },
            {
                name: 'generic',
                selector: 'input[name]:not([type="hidden"])',
                edit: function (field, card) {
                    var $input = $(field);
                    $input.val(card.model[$input.attr('name')] ? card.model[$input.attr('name')] : '');
                    return true;
                },
                save: function (field, card) {
                    var $input = $(field);
                    if (card.model.hasOwnProperty($input.attr('name'))) {
                        card.model[$input.attr('name')] = $input.val() ? $input.val() : null;
                        return true;
                    }
                    return false;
                }
            }
        ]);
})(jQuery);