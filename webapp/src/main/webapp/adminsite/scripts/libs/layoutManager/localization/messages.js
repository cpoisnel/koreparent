(function ($) {
    $.extend($.layoutManager.messages, {
        states: {
            empty: "${LAYOUT_MANAGER.STATE.EMPTY}",
            checked: "${LAYOUT_MANAGER.STATE.CHECKED}",
            incomplete: "${LAYOUT_MANAGER.STATE.INCOMPLETE}",
            warning: "${LAYOUT_MANAGER.STATE.WARNING}",
            error: "${LAYOUT_MANAGER.STATE.ERROR}"
        },
        actions: {
            edit: "${LAYOUT_MANAGER.ACTIONS.EDIT}",
            empty: "${LAYOUT_MANAGER.ACTIONS.EMPTY}",
            confirm: {
                empty: "${LAYOUT_MANAGER.ACTIONS.CONFIRM.EMPTY}"
            }
        },
        popin: {
            title: "${LAYOUT_MANAGER.POPIN.TITLE}",
            save: "${LAYOUT_MANAGER.POPIN.SAVE}",
            cancel: "${LAYOUT_MANAGER.POPIN.CANCEL}"
        }
    });
})(jQuery);