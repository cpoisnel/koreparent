/**
 * Created by alexandre.baillif on 15/11/16.
 */
(function() {
    var SLOT_ROW_PREFIX = 'slot-row-';
    var SLOT_COLUMN_PREFIX = 'slot-column-';

    var container = document.querySelector('.js-layout');


    function getCurrentPos(item) {
        var i = 0;
        var currentItem = item;
        while ((currentItem = currentItem.previousSibling) !== null ) {
            if (currentItem.className && currentItem.className.indexOf(' js-layout__slot ') > 0) {
                i++;
            }
        }
        return i;
    }

    function updateLeft(item) {
        var currentItem = item;
        var key = item.getAttribute('data-slot-key');
        var rowValue = container.querySelector('.js-slot-row[data-slot-key="' + key + '"]').value;
        var columnValue = container.querySelector('.js-slot-column[data-slot-key="' + key + '"]').value;
        if (rowValue && columnValue) {
            var currentIdx = columnValue;
            while ((currentItem = currentItem.previousSibling) !== null) {
                if (currentItem.className && currentItem.className.indexOf(' js-layout__slot ') > 0) {
                    currentIdx--;
                    key = currentItem.getAttribute('data-slot-key');
                    container.querySelector('.js-slot-row[data-slot-key="' + key + '"]').value = rowValue;
                    container.querySelector('.js-slot-column[data-slot-key="' + key + '"]').value = currentIdx;
                }
            }
        }
    }

    function updateRight(item) {
        var currentItem = item;
        var key = item.getAttribute('data-slot-key');
        var rowValue = container.querySelector('.js-slot-row[data-slot-key="' + key + '"]').value;
        var columnValue = container.querySelector('.js-slot-column[data-slot-key="' + key + '"]').value;
        if (rowValue && columnValue) {
            var currentIdx = columnValue;
            while ((currentItem = currentItem.nextSibling) !== null) {
                if (currentItem.className && currentItem.className.indexOf(' js-layout__slot ') > 0) {
                    currentIdx++;
                    key = currentItem.getAttribute('data-slot-key');
                    container.querySelector('.js-slot-row[data-slot-key="' + key + '"]').value = rowValue;
                    container.querySelector('.js-slot-column[data-slot-key="' + key + '"]').value = currentIdx;
                }
            }
        }
    }

    function updatePos(item,to) {
        var currentItemIdx = getCurrentPos(item);
        var rowIdx = to ? parseInt(to.getAttribute('data-row-id'),10) : 0;
        var name = item.getAttribute('data-slot-key');
        var slotRowVal = container.querySelector('.js-slot-row[data-slot-key="' + name + '"]');
        if (slotRowVal) {
            slotRowVal.value = rowIdx;
        }
        var slotColumnVal = container.querySelector('.js-slot-column[data-slot-key="' + name + '"]');
        if (slotColumnVal) {
            slotColumnVal.value = currentItemIdx;
        }
        //Update right and left
        updateLeft(item);
        updateRight(item);
        //Reset layout id
        var layoutId = document.getElementById('CUSTOM_LAYOUT');
        if (layoutId) {
            layoutId.value = document.getElementById('ID_LAYOUT').value;
        }
    }





    if (container) {

        [].forEach.call( container.querySelectorAll('.js-row'), function (el) {
            Sortable.create(el, {
                group: {
                    name : 'slot',
                    pull : true,
                    put : true
                },
                animation: 150,
                onUpdate: function (evt) {
                    var item = evt.item;
                    if (item) {
                        updatePos(item,evt.to);
                    }
                },
                onAdd: function (evt) {
                    var item = evt.item;
                    if (item) {
                        updatePos(item,evt.to);
                    }
                },
                onStart: function (evt) {
                    [].forEach.call( container.querySelectorAll('.js-row'), function (elt) {
                        elt.className = elt.className + ' highlight ';
                    });
                },
                onEnd: function (evt) {
                    [].forEach.call( container.querySelectorAll('.js-row'), function (elt) {
                        elt.className = elt.className.replace(' highlight ', '');
                    });
                }

            });
        });
    }
})();