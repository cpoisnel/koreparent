/*
 * Layout-Manager jQuery plugin
 *
 * version 0.1.0
 *
 * Kosmos
 *
 * Files below need to be referenced in the calling page for the component to run correctly :
 *
 * 	JS :
 * 	- jQuery ( jquery-XX.XX.XX.js )
 *
 *  CSS :
 *  - component style ( layout-manager-XX.XX.XX.css )
 *
 * Versions :
 *
 * 0.1.0 :
 *  - Component creation
 *
 */
(function ($) {
    'use strict';
    // HANDLERS MANAGER ===============================
    // Object used to store handlers and layoutManagers instances
    var ManagerHandlers = function () {
        this.managers = [];
        this.cardHandlers = {};
        this.fieldHandlers = [];
    };
    ManagerHandlers.prototype = {
        // Function used to register an external card handler
        registerCardHandler: function (handler) {
            $.extend(true, this.cardHandlers, handler);
            this.notifyManagers();
        },
        // Function used to retrieve a card handler from a card class
        retrieveCardHandler: function (handlerKey) {
            if (handlerKey) {
                if (this.cardHandlers[handlerKey]) {
                    return this.cardHandlers[handlerKey];
                } else {
                    return this.cardHandlers.default;
                }
            }
            return null;
        },
        hasCardHandler: function (type) {
            return !!this.cardHandlers[type];
        },
        // Function used to register an external field handler
        // Note that every handler passed through this method will be added to the end of the beginning of field
        // handlers
        registerFieldHandler: function (handler) {
            if ($.isArray(handler)) {
                this.fieldHandlers = $.merge(handler, this.fieldHandlers);
            } else {
                this.fieldHandlers.unshift(handler);
            }
        },
        // Function used to retrieve all field handlers
        retrieveFieldHandlers: function () {
            return this.fieldHandlers;
        },
        // Function used to register a layout manager for futher notifications
        registerManager: function (manager) {
            this.managers.push(manager);
        },
        // Function used to notify all registered managers
        notifyManagers: function () {
            $.each(
                this.managers, function (index, manager) {
                    manager.notify();
                });
        }
    };
    $.layoutManagerHandlers = new ManagerHandlers();
    // LAYOUT MANAGER ==================================
    $.layoutManager = function (root, options) {
        var $layoutManager = $(root),
            $editDialog,
            cardsPool = {},
            slots = {},
            currentSlot,
            settings;
        // Initialize the component
        function init() {

            // Default settings
            var defaults = {
                itemPropertiesExclusion: ['class', 'id', 'key']
            };
            // Merge defaults and options settings
            settings = $.extend({}, defaults, options);
            // Store a reference to the layoutManager object
            $.data(root, 'layoutManager', $layoutManager);
        }

        // Initialize datas from discovered items
        function initializeDatas() {
            // Initialize slot datas
            var $slots = $('.js-layout__slot', $layoutManager);
            $slots.each(
                function () {
                    var $this = $(this),
                        $model = $('.js-layout__card-model', $this),
                        cardView = $('.js-layout__card', $this),
                        cardModel = $model.val() ? JSON.parse($model.val()) : null;
                    slots[$model.attr('data-key')] = {
                        key: $model.attr('data-key'),
                        view: $this,
                        allowedCardTypes: JSON.parse($this.attr('data-allowed-card-types')),
                        card: {
                            model: cardModel,
                            view: cardView
                        }
                    };
                });
            // Initialize card pool
            var $cardsPool = $('.js-layout-manager__cards-pool', $layoutManager);
            if ($cardsPool.val()) {
                var cardArray = JSON.parse($cardsPool.val());
                $.each(
                    cardArray, function (index, card) {
                        cardsPool[card.class] = card;
                    });
            }
        }

        // Initialize toolbar for each detected item
        function initializeStatus() {
            $.each(
                slots, function (key, slot) {
                    slot.view.attr('data-original-title', $.layoutManager.messages.states.empty);
                    $(slot.view).tooltip(
                        {
                            html: true,
                            placement: 'top',
                            trigger: 'hover focus',
                            container: '.js-layout-manager'
                        });
                });
        }

        // Retrieve status according to given slot
        function getSlotStatus(slot) {
            var status = $.layoutManager.states.empty;
            if (slot.allowedCardTypes && slot.allowedCardTypes.length === 1) {
                if (!$.layoutManagerHandlers.retrieveCardHandler(slot.allowedCardTypes[0])) {
                    status = $.layoutManager.states.warning;
                }
            }
            if (slot.card.model) {
                var currentHandler = $.layoutManagerHandlers.retrieveCardHandler(slot.card.model.class);
                if (currentHandler && currentHandler.check) {
                    status = currentHandler.check(slot.card);
                } else {
                    status = $.layoutManager.states.warning;
                }
            }
            return status;
        }

        // Fill the slot with a card model retrieved from the card pool
        function replaceModel(slot, className) {
            slot.card.model = {};
            $.extend(true, slot.card.model, cardsPool[className]);
            slot.card.model.key = slot.key;
        }

        // Create manager by initializing every js layers
        function createManager() {
            initializeDatas();
            initializeStatus();
            $.layoutManagerHandlers.registerManager($layoutManager);
            refresh();
        }

        // Edit Action ==============================================
        // Generic action to edit a card
        function genericEditAction(card) {
            var $activeEditPanel = $('.js-card-forms__edit-panel[data-card-class="' + card.model.class + '"]');
            $.each(
                $.layoutManagerHandlers.retrieveFieldHandlers(), function (index, fieldHandler) {
                    var $inputs = $(fieldHandler.selector, $activeEditPanel);
                    $inputs.removeData(['edit-processed', 'save-processed']);
                });
            $.each(
                $.layoutManagerHandlers.retrieveFieldHandlers(), function (index, fieldHandler) {
                    var $inputs = $(fieldHandler.selector, $activeEditPanel);
                    $inputs.each(
                        function () {
                            if (!$(this).data('edit-processed')) {
                                if (fieldHandler.edit($(this), card)) {
                                    $(this).data('edit-processed', true);
                                }
                            }
                        });
                });
        }

        // Common action to route edit action : custom edit action will always be prioritized if present and well
        // defined Priority is given to a possible definition of a "editAction" bring by the card handler used for the
        // card type.
        function editAction(slot) {
            var status = getSlotStatus(slot);
            if (status !== $.layoutManager.states.error) {
                currentSlot = $.extend(true, {}, slot);
                if (!currentSlot.card.model) {
                    var defaultModelClass;
                    if (currentSlot.allowedCardTypes && currentSlot.allowedCardTypes.length > 0) {
                        defaultModelClass = currentSlot.allowedCardTypes[0];
                    } else {
                        defaultModelClass = $('.js-cards-selector__item:first-child', $layoutManager).attr('data-class');
                    }
                    replaceModel(currentSlot, defaultModelClass);
                }
                showEditPanel(currentSlot);
                var cardHandler = $.layoutManagerHandlers.retrieveCardHandler(currentSlot.card.model.class);
                if (cardHandler && $.isFunction(cardHandler.edit)) {
                    cardHandler.edit(currentSlot.card);
                } else {
                    genericEditAction(currentSlot.card);
                }
                if (cardHandler && $.isFunction(cardHandler.onEditActive)) {
                    cardHandler.onEditActive(currentSlot.card);
                }
            }
        }

        // Empty Action ==============================================
        // Generic action to edit a card
        function genericEmptyAction(card) {
            card.model = null;
        }

        // Common action to route edit action : custom edit action will always be prioritized if present and well
        // defined Priority is given to a possible definition of a "emptyAction" bring by the card handler used for the
        // card type.
        function emptyAction(slot) {
            var cardHandler = $.layoutManagerHandlers.retrieveCardHandler(slot.card.model.class);
            if (cardHandler && $.isFunction(cardHandler.empty)) {
                cardHandler.empty(slot.card);
            } else {
                genericEmptyAction(slot.card);
            }
            $('.js-layout__card-model', slot.view).val('');
        }

        // Save Action ==============================================
        // Generic action to edit a card
        function genericSaveAction(card) {
            var $activeEditPanel = $('.js-card-forms__edit-panel[data-card-class="' + card.model.class + '"]');
            $.each(
                $.layoutManagerHandlers.retrieveFieldHandlers(), function (index, fieldHandler) {
                    var $inputs = $(fieldHandler.selector, $activeEditPanel);
                    $inputs.each(
                        function () {
                            if (!$(this).data('save-processed')) {
                                if (fieldHandler.save($(this), card)) {
                                    $(this).data('save-processed', true);
                                }
                            }
                        });
                });
        }

        // Function triggered to save the model of an item
        // Priority is given to a possible definition of a "saveAction" bring by the card handler used for the card
        // type.
        function saveAction() {
            var $model = $('.js-layout__card-model', currentSlot.view),
                cardHandler = $.layoutManagerHandlers.retrieveCardHandler(currentSlot.card.model.class);
            if (cardHandler) {
                if ($.isFunction(cardHandler.save)) {
                    cardHandler.save(currentSlot.card);
                } else {
                    genericSaveAction(currentSlot.card);
                }
            }
            $model.val(JSON.stringify(currentSlot.card.model));
            slots[currentSlot.key].card = $.extend(true, {}, currentSlot.card);
            refresh();
        }

        // Switch card model between two slots
        function switchAction(slotLeft, slotRight) {
            if (slotLeft && slotRight) {
                var tempCard = slotLeft.card;
                slotLeft.card = slotRight.card;
                slotRight.card = tempCard;
                updateForSwitch(slotLeft, slotRight);
                refresh();
            }
        }

        function updateForSwitch(slotLeft, slotRight) {
            if (slotLeft.card && slotLeft.card.model) {
                slotLeft.card.model.key = slotLeft.key;
                if (!slotRight.card || !slotRight.card.model) {
                    slotLeft.card.model.id = null;
                }
                $('.js-layout__card-model', slotLeft.view).val(JSON.stringify(slotLeft.card.model));
            } else {
                $('.js-layout__card-model', slotLeft.view).val('');
            }
            if (slotRight.card && slotRight.card.model) {
                slotRight.card.model.key = slotRight.key;
                if (!slotLeft.card || !slotLeft.card.model) {
                    slotRight.card.model.id = null;
                }
                $('.js-layout__card-model', slotRight.view).val(JSON.stringify(slotRight.card.model));
            } else {
                $('.js-layout__card-model', slotRight.view).val('');
            }
        }

        //////////////////////////////////////////////////
        //					UI Updates					//
        //////////////////////////////////////////////////
        // Refresh item statuses on UI
        function refresh() {
            $.each(
                slots, function (key, slot) {
                    var status = getSlotStatus(slot);
                    updateSlotState(slot);
                    updateStatus(slot, status);
                    updateToolBox(slot, status);
                    updateCardView(slot, status);
                });
        }

        // Remove any status UI for the given slot
        function cleanStatus(slot) {
            var $status = $('.js-layout__slot-status', slot.view);
            $status.removeClass('layout__slot-status--warning layout__slot-status--error layout__slot-status--checked layout__slot-status--incomplete layout__slot-status--empty');
            $('.icon', $status).removeClass('icon-warning icon-checkmark-circle icon-cancel-circle icon-slot-empty icon-incomplete');
        }

        // Update UI slot state
        function updateSlotState(slot) {
            if (slot && slot.card.model) {
                slot.view.removeClass('layout__slot--empty').addClass('layout__slot--filled');
            } else {
                slot.view.removeClass('layout__slot--filled').addClass('layout__slot--empty');
            }
        }

        // Update card view bo
        function updateCardView(slot, status) {
            var $status = $('.js-layout__slot-status', slot.view);
            $('.card', $status).remove();
            switch (status) {
                case 'checked' :
                case 'incomplete' :
                    $('.js-card-viewBo .js-card-viewBo__view[data-card-class="' + slot.card.model['class'] + '"] .card').clone().prependTo($status);
                    break;
            }
            if (slot.card && slot.card.model) {
                var cardHandler = $.layoutManagerHandlers.retrieveCardHandler(slot.card.model['class']);
                if (cardHandler && $.isFunction(cardHandler.updateView)) {
                    cardHandler.updateView(slot.card, slot.view);
                }
            }
        }

        // Updates visual status for the given item
        function updateStatus(slot, status) {
            if (slot && status) {
                var $status = $('.js-layout__slot-status', slot.view);
                cleanStatus(slot);
                updateCardView(slot, status);
                switch (status) {
                    case 'empty' :
                        $(slot.view).attr('data-original-title', $.layoutManager.messages.states.empty);
                        $status.addClass('layout__slot-status--empty');
                        $('.icon', $status).addClass('icon-slot-empty');
                        break;
                    case 'error' :
                        $(slot.view).attr('data-original-title', $.layoutManager.messages.states.error);
                        $status.addClass('layout__slot-status--error');
                        $('.icon', $status).addClass('icon-cancel-circle');
                        break;
                    case 'checked' :
                        $(slot.view).attr('data-original-title', $.layoutManager.messages.states.checked);
                        $status.addClass('layout__slot-status--checked');
                        $('.icon', $status).addClass('icon-checkmark-circle');
                        break;
                    case 'incomplete' :
                        $(slot.view).attr('data-original-title', $.layoutManager.messages.states.incomplete);
                        $status.addClass('layout__slot-status--incomplete');
                        $('.icon', $status).addClass('icon-incomplete');
                        break;
                    default:
                        $(slot.view).attr('data-original-title', $.layoutManager.messages.states.warning);
                        $status.addClass('layout__slot-status--warning');
                        $('.icon', $status).addClass('icon-warning');
                }
            }
        }

        // Initialize toolbar for each detected item
        function updateToolBox(slot, status) {
            var $toolbar = $('.js-layout__slot-toolbar', slot.view);
            if (status === $.layoutManager.states.checked || status === $.layoutManager.states.incomplete || status === $.layoutManager.states.warning) {
                if ($toolbar.size() === 0) {
                    var $itemToolbar = $('<div class="layout__slot-toolbar js-layout__slot-toolbar"></div>').prependTo(slot.view);
                    $('<button type="button" class="layout__slot-toolbar-button--edit js-layout__slot-button--edit" title="' + $.layoutManager.messages.actions.edit + '"><span class="icon icon-pencil"></span></button>').appendTo($itemToolbar);
                    $('<button type="button" class="layout__slot-toolbar-button--empty js-layout__slot-button--empty" title="' + $.layoutManager.messages.actions.empty + '"><span class="icon icon-remove"></span></button>').appendTo($itemToolbar);
                    initializeButtonHandlers();
                }
            } else {
                if ($toolbar.size() > 0) {
                    $toolbar.remove();
                }
            }
        }

        // Update card selector dropdown according to the selected element
        function updateCardSelector(className) {
            var $item = $('.js-cards-selector__item[data-class="' + className + '"] button', $layoutManager),
                $button = $('.js-cards-selector__button', $layoutManager);
            $button.children().remove();
            // Update dropdown UI with currently selected item
            var $selectedItem = $('<div class="cards-selector__item">').appendTo($button);
            $item.children().clone().appendTo($selectedItem);
            // Disable dropdown if only one element is present
            var $items = $('.js-cards-selector .cards-selector__item--active', $layoutManager);
            if ($items.length <= 1) {
                $button.attr('disabled', true);
            } else {
                $button.removeAttr('disabled');
            }
        }

        // Filter card selector items according to the given slot
        function filterCardSelector(slot) {
            var $items = $('.js-cards-selector__item', $layoutManager);
            $items.removeClass('cards-selector__item--active').addClass('cards-selector__item--inactive');
            $items.each(
                function () {
                    var $this = $(this),
                        classType = $this.attr('data-class');
                    if ($.inArray(classType, slot.allowedCardTypes) !== -1) {
                        // Disable item if no handler could be found for type
                        if (!$.layoutManagerHandlers.retrieveCardHandler(classType)) {
                            $('button', $this).attr('disabled', true);
                        } else {
                            $('button', $this).removeAttr('disabled');
                            $this.removeClass('cards-selector__item--inactive').addClass('cards-selector__item--active');
                        }
                    }
                });
        }

        // Update edit panel UI according to the given slot
        function updateEditPanel(slot) {
            var $wrapper = $('.js-layout-manager__edit-wrapper'),
                $panels = $('.card-forms__edit-panel', $wrapper),
                $currentPanel = $('.card-forms__edit-panel[data-card-class="' + slot.card.model.class + '"]', $wrapper);
            if ($currentPanel.is('.card-forms__edit-panel--inactive')) {
                $panels.removeClass('card-forms__edit-panel--active').addClass('card-forms__edit-panel--inactive');
                $currentPanel.removeClass('card-forms__edit-panel--inactive').addClass('card-forms__edit-panel--active');
            }
            filterCardSelector(slot);
            updateCardSelector(slot.card.model.class);
            return $currentPanel;
        }

        function beforeEditOpen() {
            $.each(
                $.layoutManagerHandlers.cardHandlers, function (property) {
                    var currentCardHandler = $.layoutManagerHandlers.cardHandlers[property];
                    if (currentCardHandler && $.isFunction(currentCardHandler.beforeEditOpen)) {
                        currentCardHandler.beforeEditOpen();
                    }
                });
        }

        function onEditClose() {
            $.each(
                $.layoutManagerHandlers.cardHandlers, function (property) {
                    var currentCardHandler = $.layoutManagerHandlers.cardHandlers[property];
                    if (currentCardHandler && $.isFunction(currentCardHandler.onEditClose)) {
                        currentCardHandler.onEditClose();
                    }
                });
        }

        // Function used to show the whole edit panel
        function showEditPanel(slot) {
            updateEditPanel(slot);
            beforeEditOpen();
            $(slot.view).tooltip('hide');
            if ($editDialog) {
                $editDialog.dialog('open');
            } else {
                $editDialog = $('.js-layout-manager__edit-wrapper').dialog(
                    {
                        appendTo: '.js-layout-manager',
                        buttons: [{
                            text: $.layoutManager.messages.popin.save,
                            click: function () {
                                saveAction();
                                $(this).dialog('close');
                                hideEditPanel();
                            }
                        }, {
                            text: $.layoutManager.messages.popin.cancel,
                            click: function () {
                                $(this).dialog('close');
                                hideEditPanel();
                            }
                        }],
                        open: function () {
                            $('.js-layout-manager__edit-wrapper').dialog(
                                'option', 'position', {
                                    my: 'center',
                                    at: 'center',
                                    of: window
                                });
                        },
                        close: function () {
                            onEditClose();
                            hideEditPanel();
                        },
                        closeOnEscape: true,
                        height: 'auto',
                        maxHeight: '100%',
                        minHeight: 500,
                        modal: true,
                        resizable: false,
                        title: $.layoutManager.messages.popin.title,
                        width: '100%'
                    });
            }
        }

        // Function used to hide the whole edit panel
        function hideEditPanel() {
            var $wrapper = $('.js-layout-manager__edit-wrapper'),
                $panels = $('.js-card-forms__edit-panel');
            $panels.removeClass('card-forms__edit-panel--active').addClass('card-forms__edit-panel--inactive');
            $wrapper.removeClass('layout-manager__edit-wrapper--active').addClass('layout-manager__edit-wrapper--inactive');
            $('.layout__slot--focus').removeClass('layout__slot--focus');
        }

        // Function used to confirm an action
        function confirmAction(action, message) {
            $('<div>')
                .html(message)
                .dialog(
                    {
                        buttons: [{
                            text: LOCALE_BO.ok,
                            click: function () {
                                $(this).dialog('close');
                                if ($.isFunction(action)) {
                                    action();
                                }
                            }
                        }, {
                            text: LOCALE_BO.fermer,
                            click: function () {
                                $(this).dialog('close');
                            }
                        }],
                        title: LOCALE_BO.confirmer
                    });
            return false;
        }

        //////////////////////////////////////////////////
        //				Events Handling					//
        //////////////////////////////////////////////////
        // Initialize button handlers
        function initializeButtonHandlers() {
            $.each(
                slots, function (key, slot) {
                    var $editButtons = $('.js-layout__slot-button--edit', slot.view),
                        $emptyButtons = $('.js-layout__slot-button--empty', slot.view);
                    // Prior unbind to prevent multi trigger
                    slot.view.unbind('click', slotEditClick);
                    $editButtons.unbind('click', slotEditClick);
                    $emptyButtons.unbind('click', slotEmptyClick);
                    // New binding
                    slot.view.bind('click', {slot: slot}, slotEditClick);
                    $editButtons.bind('click', {slot: slot}, slotEditClick);
                    $emptyButtons.bind('click', {slot: slot}, slotEmptyClick);
                });
        }

        // Edit click event handler
        function slotEditClick(event) {
            editAction(event.data.slot);
            refresh();
        }

        // Empty click event handler
        function slotEmptyClick(event) {
            event.stopImmediatePropagation();
            confirmAction(
                function () {
                    emptyAction(event.data.slot);
                    refresh();
                }, $.layoutManager.messages.actions.confirm.empty);
        }

        // Item selection event handler
        $('.js-cards-selector__item', $layoutManager).click(
            function () {
                replaceModel(currentSlot, $(this).attr('data-class'));
                editAction(currentSlot);
            });
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //						API						//
        //////////////////////////////////////////////////
        // Function used to retrieve an item according to it's location
        $layoutManager.getItem = function (key) {
            if (typeof key === 'string' || key instanceof String) {
                return slots[key];
            }
            return null;
        };
        $layoutManager.switchItems = function (keyLeft, keyRight) {
            var slotLeft = this.getItem(keyLeft),
                slotRight = this.getItem(keyRight);
            switchAction(slotLeft, slotRight);
        };
        // Function used to refresh an item's view
        $layoutManager.refreshItem = function (slot) {
            var status = getSlotStatus(slot);
            updateStatus(slot, status);
            updateToolBox(slot, status);
        };
        // Function used to update an item's view according to the status passed
        $layoutManager.updateItem = function (slot, status) {
            updateStatus(slot, status);
            updateToolBox(slot, status);
        };
        // Function used to notify the layout manager of a change
        $layoutManager.notify = function () {
            refresh();
        };
        // Initialize the component
        init();
        createManager();
        initializeButtonHandlers();
    };
    // Jquery wrapper
    $.fn.layoutManager = function (options, params) {
        if (options) {
            options = options || {};
            if (typeof options === 'object') {
                this.each(
                    function () {
                        if ($(this).data('layoutManager')) {
                            if (options.remove) {
                                $(this).data('layoutManager').remove();
                                $(this).removeData('layoutManager');
                            }
                        }
                        else if (!options.remove) {
                            if (options.enable === undefined && options.disable === undefined) {
                                options.enable = true;
                            }
                            new $.layoutManager(this, options);
                        }
                    });
                if (options.instance) {
                    return $(this).data('layoutManager');
                }
                return this;
            }
        } else {
            return $(this).data('layoutManager');
        }
    };
    // Blocs states
    $.layoutManager.states = {
        empty: 'empty',
        checked: 'checked',
        error: 'error',
        incomplete: 'incomplete',
        warning: 'warning'
    };
    // Messages
    $.layoutManager.messages = {};
})(jQuery);
