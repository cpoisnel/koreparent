(function ($) {
    var iframe = window.frameElement;
    if (iframe) {
        iframe.contentDocument = document;//normalization: some browsers don't set the contentDocument, only the contentWindow
        var parent = window.parent;
        $(parent.document).ready(
            function () {//wait for parent to make sure it has jQuery ready
                var parent$ = parent.jQuery;
                parent$(iframe).trigger("iframeloading");
                $(
                    function () {
                        parent$(iframe).trigger("iframeready");
                    });
                $(window).load(
                    function () {//kind of unnecessary, but here for completion
                        parent$(iframe).trigger("iframeloaded", [$(iframe.contentDocument)]);
                    });
                $(window).unload(
                    function (e) {//not possible to prevent default
                        parent$(iframe).trigger("iframeunloaded");
                    });
                $(window).on(
                    "beforeunload", function () {
                        parent$(iframe).trigger("iframebeforeunload");
                    });
            });
    }
})(jQuery.noConflict());
