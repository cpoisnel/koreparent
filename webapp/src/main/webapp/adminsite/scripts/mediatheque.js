/**
 * Mediathèque
 */

(function ($) {
    'use strict';
    var $content = $('#content:not(.js-inhibate)');
    $('[name="MEDIA_FILE"], [name="TYPE_RESSOURCE"]', $content).on(
        'change keyup', function () {
            if (checkFileExt('MEDIA')) {
                $('input[name="ACTION"]').val('CONTROLER_RESSOURCE');
                $('form').submit();
            }
        });
    $('.js-change-source').on(
        'click', function (e) {
            var $this = $(this);
            e.preventDefault();
            $('input[name="ACTION"]').val('AJOUTER');
            $('input[name="IS_LOCAL"]').val($this.attr('data-local') || false);
            $('form').submit();
        });
    $('.js-media-list__edit-button, .js-media-list__container', $content).on(
        'click', function () {
            var $this = $(this);
            $('input[name="ACTION"]').val($this.attr('data-action'));
            $('input[name="ID_MEDIA"]').val($this.attr('data-idmedia'));
            $('form').submit();
        });
    $('.js-media-list__delete-button', $content).on(
        'click', function () {
            var $button = $(this);
            $('<div>').html(LOCALE_BO.media.actions.delete.confirm).dialog(
                {
                    buttons: [{
                        text: LOCALE_BO.ok,
                        click: function () {
                            $(this).dialog('close');
                            $('input[name="ACTION"]').val('SUPPRIMER');
                            $('input[name="ID_MEDIA"]').val($button.attr('data-idmedia'));
                            $('form').submit();
                        }
                    },
                        {
                            text: LOCALE_BO.fermer,
                            click: function () {
                                $(this).dialog('close');
                            }
                        }],
                    title: LOCALE_BO.confirmer
                });
        });
    $('.js-thumbnail__delete-button', $content).on(
        'click', function () {
            var $imgVignette = $('#img_vignette'),
                $divVignette = $('#div_vignette');
            $('input[name="URL_VIGNETTE"]').val('');
            $('input[name="VIGNETTE"]').val('');
            if ($imgVignette.length > 0) {
                $imgVignette.attr('src', '');
                $divVignette.replaceWith($divVignette.clone(true)).html('');
            }
        });
    $('.js-tabs-medias').tabs({});
})(jQuery.noConflict());
