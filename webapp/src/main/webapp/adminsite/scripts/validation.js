(function ($) {
    'use strict';
    var $form = $('form');
    /**
     * jQuery validation
     */
        // Custom date format validation
    $.validator.addMethod(
        "ksupUrl",
        function ksupUrl(value, element) {
            if (!value) {
                return true;
            }
            var regExp = /^(?:(?:https?|ftp):\/\/)?(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
            return regExp.test(value);
        }, LOCALE_BO.validationMessages.ksupUrl);
    // Custom date format validation
    $.validator.addMethod(
        "ksupDate",
        function ksupDate(value, element) {
            if (!value) {
                return true;
            }
            var regExp = /([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/g,
                values = regExp.exec(value);
            if (values == null || regExp.test(value)) {
                return false;
            }
            var day = $.parseInteger(values[1]),
                month = $.parseInteger(values[2]);
            if (day > 31 || month > 12) {
                return false;
            }
            return !(month == 2 && day > 29);
        }, LOCALE_BO.validationMessages.ksupDate);
    // Custom phone format validation
    $.validator.addMethod(
        "ksupPhone",
        function ksupPhone(value, element) {
            if (!value) {
                return true;
            }
            var regExp = /^((\+)*[0-9]{1,3}|0)[0-9]{9}$/;
            return regExp.test(value);
        }, LOCALE_BO.validationMessages.ksupPhone);
    // CKEditor validation
    $.validator.addMethod(
        'ckeditor', function ckeditor(value, element) {
            var $element = $(element);
            if ($element.attr('maxlength')) {
                var editor = CKEDITOR.instances[$element.attr('name')];
                if (editor) {
                    return editor.getData().length <= $.parseInteger($element.attr('maxlength'));
                }
            }
            return true;
        }, $.validator.format(LOCALE_BO.validationMessages.ckeditor));
    $.validator.addClassRules(
        "js-ckeditor", {
            "required": function (element) {
                var $element = $(element),
                    editor = CKEDITOR.instances[$element.attr('name')];
                if (editor) {
                    editor.updateElement();
                    return $element.attr('maxlength') || $element.prop('required');
                }
                return false;
            },
            ckeditor: function (element) {
                var $element = $(element);
                return $.parseInteger($element.attr('maxlength'));
            }
        });
    $.validator.addClassRules(
        "type_phone", {
            "required": function (element) {
                return $(element).prop('required');
            },
            ksupPhone: true
        });
    $.validator.addClassRules(
        "type_email", {
            "required": function (element) {
                return $(element).prop('required');
            },
            "email": true
        });
    $.validator.addClassRules(
        "type_date", {
            required: function (element) {
                return $(element).prop('required');
            },
            ksupDate: true,
            maxlength: 10
        });
    $.validator.addClassRules(
        "type_url", {
            required: function (element) {
                return $(element).prop('required');
            },
            ksupUrl: true
        });
    $.validator.addClassRules(
        "numeric_input", {
            required: function (element) {
                return $(element).prop('required');
            },
            digits: true
        });
    $form.each(
        function () {
            $(this).validate(
                {
                    ignore: '.ignore, :hidden:not(.js-ckeditor)',
                    focusCleanup: true,
                    errorElement: 'div'
                });
        });
    /**
     * Input helper
     */
    /**
     * Tooltips : montre le nombre de caractères qui peuvent être saisis
     */

    var $maxedInputs = $('form:not([data-no-tooltip]) input[type="text"][maxlength], form:not([data-no-tooltip]) textarea[maxlength]');
    if ($maxedInputs.length > 0) {
        $maxedInputs.removeAttr('title');
        $maxedInputs.each(
            function (index, component) {
                var $component = $(component);
                $component.tooltip(
                    {
                        placement: 'right',
                        title: $component.val().length + " / " + $component.attr('maxlength') + " " + LOCALE_BO.validationMessages.caracteres_autorises,
                        trigger: 'focus'
                    });
                $component.bind('change keyup blur input focus', updateToolTip);
                if ($component.is(':focus')) {
                    $component.tooltip('show');
                }
            });
    }
    $('textarea[maxlength]').bind(
        'keyup blur', function () {
            var $this = $(this),
                len = $this[0].value.replace(/(\r\n|\n|\r)/g, '--').length,
                maxlength = $this.attr('maxlength');
            if (maxlength && len > maxlength) {
                var delta = len - maxlength;
                $this.val($this.val().slice(0, maxlength - delta));
            }
        });
    function updateToolTip() {
        var $this = $(this),
            popover = $(this).data('tooltip'),
            tip = popover.tip(),
            len = $this[0].value.replace(/(\r\n|\n|\r)/g, '--').length;
        popover.options.content = len + " / " + $(this).attr('maxlength') + " " + LOCALE_BO.validationMessages.caracteres_autorises;
        popover.options.title = popover.options.content;
        var visible = popover && tip && tip.is(':visible');
        if (visible) {
            tip.find('.tooltip-inner').html(popover.options.content);
        } else {
            popover.show();
        }
    }

    /**
     * Password strength meter
     */
    var options = {
        ui: {
            bootstrap2: true,
            showVerdicts: true,
            showErrors: true,
            verdicts: LOCALE_BO.validationMessages.pwdVerdicts,
            errorMessages: LOCALE_BO.validationMessages.pwdErrorMessages
        }
    };
    if (window.pwsOptions) {
        $.extend(true, options, window.pwsOptions);
    }
    $('.pwdStrength :password').pwstrength(options);
    $form.triggerHandler('validate.ready');
})(jQuery.noConflict());