(function($) {
    'use strict';
    // Media tags handling
    if (!$('html').is('.ie8')) { // Classic fallback
        $('video').mediaelementplayer({
            // remove or reorder to change plugin priority
            plugins: ['flash', 'silverlight'],
            pauseOtherPlayers: true,
            pluginPath: window.location.origin + '/adminsite/scripts/libs/mediaElement/'
        });
    }
    // For backward compatibility purpose : may be removed in future release
    $('audio').mediaelementplayer({
        // remove or reorder to change plugin priority
        plugins: ['flash', 'silverlight'],
        pauseOtherPlayers: true,
        features: ['playpause', 'progress', 'current', 'duration', 'tracks', 'volume'],
        pluginPath: window.location.origin + '/adminsite/scripts/libs/mediaElement/',
        alwaysShowControls: true,
        defaultAudioWidth: '100%',
        defaultAudioHeight: 30
    });

    function getImageItems($parent) {
        var $img = $parent.find('img'),
            items = [];
        $img.each(function() {
            var $this = $(this);
            items.push({
                src: $this.attr('src'),
                type: 'image'
            });
        });
        return items;
    }

    function getItems($parent) {
        var items = [];
        $.merge(items, getImageItems($parent));
        return items;
    }

    var $gallery = $('.js-media-gallery');
    $gallery.magnificPopup({
        items: getItems($gallery),
        gallery: {
            enabled: true
        }
    });

})(jQuery.noConflict());
