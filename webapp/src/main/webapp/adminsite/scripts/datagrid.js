(function ($) {
    'use strict';

    $.fn.dataTable.tools = {};

    $.fn.dataTableExt.oApi.fnResetAllFilters = function (oSettings, bDraw) {
        for (var iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
            oSettings.aoPreSearchCols[iCol].sSearch = '';
        }
        oSettings.oPreviousSearch.sSearch = '';
        if (typeof bDraw === 'undefined') {
            bDraw = true;
        }
        if (bDraw) {
            this.fnDraw();
        }
    };

    $.fn.dataTableExt.oApi.fnSetFilteringDelay = function (oSettings, iDelay) {
        var that = this;
        if (iDelay === undefined) {
            iDelay = 250;
        }
        this.each(function (i) {
            $.fn.dataTableExt.iApiIndex = i;
            var oTimerId = null,
                anControl = $('input', that.fnSettings().aanFeatures.f);
            anControl.unbind('keyup search input').bind('keyup search input', function () {
                window.clearTimeout(oTimerId);
                oTimerId = window.setTimeout(function () {
                    $.fn.dataTableExt.iApiIndex = i;
                    if (anControl.val()) {
                        that.fnFilter(anControl.val());
                    } else {
                        that.fnResetAllFilters();
                    }
                }, iDelay);
            });
            return this;
        });
        return this;
    };

    /**
     * Paramètre par défaut des datagrids en front
     */
    $.extend($.fn.dataTable.defaults, {
        'bJQueryUI': true,
        'bAutoWidth': false,
        'sPaginationType': 'full_numbers'
    });

    /**
     * Déclaration d'un tri spécifique pour les dates en dd/MM/YYYY
     */
    $.extend($.fn.dataTableExt.oSort, {
        'date-jjmmaaaa-pre': function (a) {
            var date = a.split('/');

            return date[2] + date[1] + date[0];
        },

        'date-jjmmaaaa-asc': function (x, y) {
            var result = 0;
            if (x < y) {
                result = -1;
            } else if (x > y) {
                result = 1;
            }
            return result;
        },

        'date-jjmmaaaa-desc': function (x, y) {
            var result = 0;
            if (x < y) {
                result = 1;
            } else if (x > y) {
                result = -1;
            }
            return result;
        }
    });

    /**
     * Déclaration d'un tri spécifique pour les dates en dd/MM/YYYY HH:MM
     */
    $.extend($.fn.dataTableExt.oSort, {
        'date-jjmmaaaa-hhmm-pre': function (a) {
            var datetime = a.split(' ');
            var date = datetime[0].split('/');
            var time = datetime[1].split(':');
            return date[2] + date[1] + date[0] + time[0] + time[1];
        },

        'date-jjmmaaaa-hhmm-asc': function (x, y) {
            var result = 0;
            if (x < y) {
                result = -1;
            } else if (x > y) {
                result = 1;
            }
            return result;
        },

        'date-jjmmaaaa-hhmm-desc': function (x, y) {
            var result = 0;
            if (x < y) {
                result = 1;
            } else if (x > y) {
                result = -1;
            }
            return result;
        }
    });

    /**
     * Gère l'affichage de la barre de navigation. Si il n'y a qu'une page, on
     * désactive la nav
     */
    $.fn.dataTable.tools.gestionNavigation = function (idDatagrid) {
        if ($('#' + idDatagrid + '_wrapper .dataTables_paginate > span > a').size() <= 1) {
            $('#' + idDatagrid + '_wrapper .dataTables_paginate').addClass('disabled');
        } else {
            $('#' + idDatagrid + '_wrapper .dataTables_paginate').removeClass('disabled');
        }
    };

    /**
     * Datatable des fiches à valider. Fait coté client via le dom
     */
    $('.fiches_a_valider').dataTable({
        'aaSorting': [[0, 'asc']],
        'aoColumns': [
            null,
            null,
            null,
            null,
            {
                'sType': 'date-jjmmaaaa'
            },
            null,
            null
        ],
        'fnDrawCallback': function () {
            $.fn.dataTable.tools.gestionNavigation(this.attr('id'));
        }
    }).fnSetFilteringDelay();

    /**
     * Datatable de l'historique d'une fiche trié sur la date de l'opération de
     * façon antéchronologique
     */
    $('.datatableHistorique').dataTable({
        'aaSorting': [[0, 'desc']],
        'aoColumns': [
            {
                'sType': 'date-jjmmaaaa-hhmm'
            },
            null,
            null,
            null
        ],
        'fnDrawCallback': function () {
            $.fn.dataTable.tools.gestionNavigation(this.attr('id'));
        }

    }).fnSetFilteringDelay();

    /**
     * Datagrid générique, utilisé lorsque l'on a une table pré-remplis dans la
     * JSP (sans TableTools) La table doit contenir un thead pour fonctionner
     * correctement
     */
    $('.datatableLight').dataTable({
        'sDom': '<"H"flr>t<"F"ip>',
        'aoColumnDefs': [
            {'bSortable': false, 'aTargets': ['sanstri']},
            {'sType': 'date-jjmmaaaa', 'aTargets': ['js-date-order']},
            {'sType': 'date-jjmmaaaa-hhmm', 'aTargets': ['js-datetime-order']},
            {'bSearchable': false, 'aTargets': ['sansfiltre']}
        ],
        'fnDrawCallback': function () {
            $.fn.dataTable.tools.gestionNavigation(this.attr('id'));
        }
    }).fnSetFilteringDelay();

    /**
     * Datagrid générique, utilisé lorsque l'on a une table pré-remplis dans la
     * JSP La table doit contenir un thead pour fonctionner correctement
     */
    $('.datatable').dataTable({
        'aoColumnDefs': [
            {'bSortable': false, 'aTargets': ['sanstri']},
            {'sType': 'date-jjmmaaaa', 'aTargets': ['js-date-order']},
            {'sType': 'date-jjmmaaaa-hhmm', 'aTargets': ['js-datetime-order']},
            {'bSearchable': false, 'aTargets': ['sansfiltre']}
        ],
        'fnDrawCallback': function () {
            $.fn.dataTable.tools.gestionNavigation(this.attr('id'));
        }
    }).fnSetFilteringDelay();

    /***************************************************************************
     * Gestion du serverSide
     **************************************************************************/

    /**
     * variable interne aux datagrids, elle permet de stocker l'url a appeler
     * pour la recherche
     */
    var ajaxURL = '';

    /**
     * Gère l'appel ajax et l'affichage du datagrid
     */
    $.fn.dataTable.tools.processRequest = function (sSource, aoData, fnCallback) {
        var idDatagrid = this.attr('id');
        $.getJSON(sSource, aoData, function (json) {
            if (json.sAlerte) {
                $('#messageApplicatif').html('<p class="message alerte">' + json.sAlerte + '</p>');
            }
            fnCallback(json);
            $.fn.dataTable.tools.gestionNavigation(idDatagrid);
        }).error(function () {
            $('#messageApplicatif').html('<p class="message erreur">' + LOCALE_BO.erreurRecherche + '</p>');
            $.fn.dataTable.tools.gestionNavigation(idDatagrid);
        });
    };

    /**
     * Définition des colonnes utilisées dans plusieurs datagrids
     */
    var colonneSelectionParId = {
            'mData': 'id',
            'sDefaultContent': '<input type="checkbox" disabled="disabled" />',
            'bSortable': false,
            'mRender': function (data, type, full) {
                if (data && (full.urlSuppression || full.urlModification)) {
                    return '<input id="idsMetas-' + data + '" type="checkbox" name="idsMetas" value="' + data + '" />';
                }
            }
        },
        colonneLibelle = {
            'mData': 'libelle',
            'sDefaultContent': '',
            'bSortable': true,
            'mRender': function (data, type, full) {
                if (full.urlModification) {
                    var lienModifier = $('<a href="' + full.urlModification + '" class="ellipsis">');
                    lienModifier.text(data);
                    lienModifier.attr('title', data);
                    return $('<div/>').html(lienModifier).html();
                }
                return $('<div/>').text(data).html();
            }
        },
        colonneDateModif = {
            'mData': 'dateModification',
            'sDefaultContent': '',
            'bSortable': true
        },
        colonneRubrique = {
            'mData': 'rubrique',
            'sDefaultContent': '',
            'bSortable': true,
            'mRender': function (data, type, full) {
                return '<span title="' + full.filAriane + '">' + data + '</span>';
            }
        },
        colonneTypeObjet = {
            'mData': 'typeObjet',
            'sDefaultContent': '',
            'bSortable': true
        },
        colonneLangue = {
            'mData': 'langue',
            'sDefaultContent': '',
            'bSortable': true,
            'mRender': function (data, type, full) {
                return '<img src="' + full.urlDrapeauLangue + '" alt="' + data + '"/>';
            }
        },
        colonneEtatFiche = {
            'mData': 'etatFiche',
            'sDefaultContent': '',
            'bSortable': true,
            'mRender': function (data, type, full) {
                return '<span class="' + full.classEtatFiche + '">' + data + '</span>';
            }
        },
        colonneAction = {
            'mData': 'url',
            'sDefaultContent': '',
            'bSortable': false,
            'bSearchable': false
        };

    /**
     *
     * Déclaration générique d'une action de masse sur les fiches
     * (suppression/archivage/...)
     */
    var boutonActionMasse = function (oConfig, action, selecteur, messageAvertissement, messageConfirm) {
        var objetsSelectionnees = $(selecteur);
        if (objetsSelectionnees.length === 0) {
            $('<div>' + LOCALE_BO.selectionnerUnELement + '</div>').dialog({
                title: LOCALE_BO.erreur,
                modal: true
            });
        } else {
            $('<div>' + messageAvertissement + '</div>').dialog({
                title: LOCALE_BO.confirmer,
                modal: true,
                buttons: [{
                    text: LOCALE_BO.ok,
                    click: function () {
                        $.ajax({
                            dataType: 'json',
                            url: oConfig.sAjaxUrl + '&ACTION=' + action + '&' + $.param(objetsSelectionnees),
                            success: function () {
                                $('#messageApplicatif').html('<p class="message confirmation">' + messageConfirm + '</p>');
                                oConfig.fnAjaxComplete();
                            },
                            error: function () {
                                $('#messageApplicatif').html('<p class="message erreur">' + LOCALE_BO.erreurActionMasse + '</p>');
                                oConfig.fnAjaxComplete();
                            }
                        });
                        $(this).dialog('close');
                    }
                }, {
                    text: LOCALE_BO.fermer,
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        }
    };
    var toolbox;
    var liste;
    /**
     * Datagrid utilisé pour les popups type recherche de page de tête & popin
     * de la toolbox.
     *
     */
    var datagridToolbox = $('.datatableFichetoolbox');
    if (datagridToolbox.length > 0) {
        toolbox = datagridToolbox.data('toolbox') || '';
        liste = datagridToolbox.data('liste') || '';
        ajaxURL = datagridToolbox.data('search') || '';
        datagridToolbox.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'sAjaxSource': ajaxURL,
            'aoColumns': [$.extend(colonneLibelle, {
                'mRender': function (data, type, full) {
                    return '<a href="' + full.url + '&amp;TOOLBOX=' + toolbox + '&amp;LISTE=' + liste + '">' + data + '</a>';
                }
            }),
                colonneRubrique,
                colonneLangue]
        }).fnSetFilteringDelay();
    }
    /**
     * Datagrid utilisé pour les popups type recherche de page de tête & popin
     * de la toolbox.
     *
     */
    var datagridMultiFicheToolbox = $('.datatableMultiFichetoolbox');
    if (datagridMultiFicheToolbox.length > 0) {
        toolbox = datagridMultiFicheToolbox.data('toolbox') || '';
        liste = datagridMultiFicheToolbox.data('liste') || '';
        ajaxURL = datagridMultiFicheToolbox.data('search') || '';
        datagridMultiFicheToolbox.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'sAjaxSource': ajaxURL,
            'aoColumns': [$.extend(colonneLibelle, {
                'mRender': function (data, type, full) {
                    return '<a href="' + full.url + '&amp;TOOLBOX=' + toolbox + '&amp;LISTE=' + liste + '">' + data + '</a>';
                }
            }),
                colonneRubrique,
                colonneTypeObjet,
                colonneLangue]
        }).fnSetFilteringDelay();
    }

    /**
     * Datagrid utilisé pour la recherche multifiche (recherche page d'accueil)
     */
    var datagridMultiFiche = $('.datatableMultiFiche');
    if (datagridMultiFiche.length > 0) {
        ajaxURL = datagridMultiFiche.data('search') || '';
        datagridMultiFiche.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'aaSorting': [[6, 'desc']],
            'sDom': '<"H"Tflr>t<"F"ip>',
            'oTableTools': {
                'aButtons': [{
                    sExtends: 'ajax',
                    sButtonText: LOCALE_BO.supprimer,
                    sAjaxUrl: ajaxURL,
                    fnClick: function (nButton, oConfig) {
                        boutonActionMasse(oConfig, 'SUPPRIMER', 'input[name="idsMetas"]:checked', LOCALE_BO.confirmSupprFiches, LOCALE_BO.confirmationActionMasseSuppr);
                    },
                    fnAjaxComplete: function () {
                        $('#checkall').prop('checked', false);
                        datagridMultiFiche.fnDraw();
                    }
                }, {
                    sExtends: 'ajax',
                    sButtonText: LOCALE_BO.archiver,
                    sAjaxUrl: ajaxURL,
                    fnClick: function (nButton, oConfig) {
                        boutonActionMasse(oConfig, 'ARCHIVER', 'input[name="idsMetas"]:checked', LOCALE_BO.confirmArchFiches, LOCALE_BO.confirmationActionMasseArchi);
                    },
                    fnAjaxComplete: function () {
                        $('#checkall').prop('checked', false);
                        datagridMultiFiche.fnDraw();
                    }
                }]
            },
            'sAjaxSource': ajaxURL,
            'aoColumns': [colonneSelectionParId,
                colonneLibelle,
                colonneRubrique,
                colonneTypeObjet,
                colonneLangue,
                colonneEtatFiche,
                colonneDateModif,
                $.extend(colonneAction, {
                    'mRender': function (data, type, full) {
                        var actions = '';
                        if (full.urlModification) {
                            actions += '<a class="action_modifier" href="' + full.urlModification + '" title="' + LOCALE_BO.modifier + '" >' + LOCALE_BO.modifier + '</a>';
                        }
                        if (full.urlSuppression) {
                            if (actions.length > 0) {
                                actions += ' | ';
                            }
                            actions += '<a class="action_supprimer" href="' + full.urlSuppression + '" title="' + LOCALE_BO.supprimer + '" data-confirm="' + LOCALE_BO.confirmSupprFiche + '" >' + LOCALE_BO.supprimer + '</a>';
                        }
                        if (full.url) {
                            if (actions.length > 0) {
                                actions += ' | ';
                            }
                            actions += '<a class="action_apercu" href="' + full.url + '" title="' + LOCALE_BO.nouvelleFenetre + '" >' + LOCALE_BO.voirEnLigne + '</a>';
                        }
                        return actions;
                    }
                })]
        }).fnSetFilteringDelay();

        datagridMultiFiche.on('click', '#checkall', function () {
            $('input[name="idsMetas"]').prop('checked', this.checked);
        });
    }

    /**
     * Datagrid utilisé pour les fiches en pleine page
     */
    var datagridFiche = $('.datatableFiche');
    if (datagridFiche.length > 0) {
        ajaxURL = datagridFiche.data('search') || '';
        datagridFiche.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'aaSorting': [[5, 'desc']],
            'sDom': '<"H"Tflr>t<"F"ip>',
            'oTableTools': {
                'aButtons': [{
                    sExtends: 'ajax',
                    sButtonText: LOCALE_BO.supprimer,
                    sAjaxUrl: ajaxURL,
                    fnClick: function (nButton, oConfig) {
                        boutonActionMasse(oConfig, 'SUPPRIMER', 'input[name="idsFiches"]:checked', LOCALE_BO.confirmSupprFiches, LOCALE_BO.confirmationActionMasseSuppr);
                    },
                    fnAjaxComplete: function () {
                        $('#checkall').prop('checked', false);
                        datagridFiche.fnDraw();
                    }
                }, {
                    sExtends: 'ajax',
                    sButtonText: LOCALE_BO.archiver,
                    sAjaxUrl: ajaxURL,
                    fnClick: function (nButton, oConfig) {
                        boutonActionMasse(oConfig, 'ARCHIVER', 'input[name="idsFiches"]:checked', LOCALE_BO.confirmArchFiches, LOCALE_BO.confirmationActionMasseArchi);
                    },
                    fnAjaxComplete: function () {
                        $('#checkall').prop('checked', false);
                        datagridFiche.fnDraw();
                    }
                }]
            },
            'sAjaxSource': ajaxURL,
            'aoColumns': [$.extend(colonneSelectionParId, {
                'mRender': function (data) {
                    if (data) {
                        return '<input id="idsFiches-' + data + '" type="checkbox" name="idsFiches" value="' + data + '" />';
                    }

                }
            }),
                colonneLibelle,
                colonneRubrique,
                colonneLangue,
                colonneEtatFiche,
                colonneDateModif,
                $.extend(colonneAction, {
                    'mRender': function (data, type, full) {
                        var actions = '';
                        if (full.urlModification) {
                            actions += '<a class="action_modifier" href="' + full.urlModification + '" title="' + LOCALE_BO.modifier + '" >' + LOCALE_BO.modifier + '</a>';
                        }
                        if (full.urlSuppression) {
                            if (actions.length > 0) {
                                actions += ' | ';
                            }
                            actions += '<a class="action_supprimer" href="' + full.urlSuppression + '" data-confirm="' + LOCALE_BO.confirmSupprFiche + '" title="' + LOCALE_BO.supprimer + '" >' + LOCALE_BO.supprimer + '</a>';
                        }
                        if (full.url) {
                            if (actions.length > 0) {
                                actions += ' | ';
                            }
                            actions += '<a class="action_apercu" href="' + full.url + '" title="' + LOCALE_BO.nouvelleFenetre + '" >' + LOCALE_BO.voirEnLigne + '</a>';
                        }
                        return actions;
                    }
                })]
        }).fnSetFilteringDelay();

        datagridFiche.on('click', '#checkall', function () {
            $('input[name="idsFiches"]').prop('checked', this.checked);
        });
    }

    /**
     * Datagrid utilisé pour la recherche d'utilisateur en popup...
     */
    var datagridUtilisateurPopup = $('.datatableUtilisateurRECHERCHE');
    if (datagridUtilisateurPopup.length > 0) {
        ajaxURL = datagridUtilisateurPopup.data('search') || '';
        datagridUtilisateurPopup.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'sAjaxSource': ajaxURL,
            'aoColumns': [{
                'mData': 'nom',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true,
                'mRender': function (data, type, full) {
                    return '<a href="#" class="userResult" data-userCode="' + full.code + '" title="' + full.nom + ' ' + full.prenom + '">' + full.nom + ' ' + full.prenom + '</a>';
                }
            }, {
                'mData': 'code',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true
            }, {
                'mData': 'structure',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true,
                'mRender': function (data, type, full) {
                    return '<span title="' + full.arboStructure + '">' + data + '</span>';
                }
            }]
        }).fnSetFilteringDelay();
        datagridUtilisateurPopup.on('click', '.userResult', function () {
            iFrameHelper.sendValues(window.iFrameRegistration, {
                sCode: $(this).attr('data-userCode'),
                libelle: $(this).attr('title')
            });
            return false;
        });
    }
    /**
     * Datagrid utilisé pour la recherche utilisateur
     */
    var datagridUtilisateur = $('.datatableUtilisateurGESTION');
    if (datagridUtilisateur.length > 0) {
        ajaxURL = datagridUtilisateur.data('search') || '';
        datagridUtilisateur.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'sDom': '<"H"Tflr>t<"F"ip>',
            'oTableTools': {
                'aButtons': [{
                    sExtends: 'ajax',
                    sButtonText: LOCALE_BO.supprimer,
                    sAjaxUrl: ajaxURL,
                    fnClick: function (nButton, oConfig) {
                        boutonActionMasse(oConfig, 'SUPPRIMER', 'input[name="idsUtilisateur"]:checked', LOCALE_BO.confirmSupprUtilisateurs, LOCALE_BO.confirmationActionMasseUtilisateur);
                    },
                    fnAjaxComplete: function () {
                        $('#checkall').prop('checked', false);
                        datagridUtilisateur.fnDraw();
                    }
                }]
            },
            'sAjaxSource': ajaxURL,
            'aaSorting': [[1, 'asc']],
            'aoColumns': [$.extend(colonneSelectionParId, {
                'mRender': function (data) {
                    return '<input id="idsUtilisateur-' + data + '" type="checkbox" name="idsUtilisateur" value="' + data + '" />';
                }
            }), {
                'mData': 'libelle',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true,
                'mRender': function (data, type, full) {
                    if (full.urlModification) {
                        return '<a title="' + data + '" href="' + full.urlModification + '" >' + data + '</a>';
                    } else {
                        return data;
                    }
                }
            }, {
                'mData': 'code',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true
            }, {
                'mData': 'mail',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true
            }, {
                'mData': 'structure',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true,
                'mRender': function (data, type, full) {
                    return '<span title="' + full.arboStructure + '">' + data + '</span>';
                }
            },
                $.extend(colonneAction, {
                    'mRender': function (data, type, full) {
                        if (full.urlModification && full.urlSuppression) {
                            return '<a class="action_modifier" href="' + full.urlModification + '" >' + LOCALE_BO.modifier + '</a> | <a class="action_supprimer" href="' + full.urlSuppression + '" data-confirm="' + LOCALE_BO.confirmSupprUtilisateur + '"  >' + LOCALE_BO.supprimer + '</a>';
                        } else {
                            return '';
                        }
                    }
                })]
        }).fnSetFilteringDelay();

        datagridUtilisateur.on('click', '#checkall', function () {
            $('input[name="idsUtilisateur"]').prop('checked', this.checked);
        });
    }


    /**
     * Datagrid utilisé pour l'affichage d'une liste d'utilisateurs au sein d'un
     * groupe
     */
    var datagridUtilisateurListe = $('.datatableUtilisateurGROUPE');
    if (datagridUtilisateurListe.length > 0) {
        ajaxURL = datagridUtilisateurListe.data('search') || '';
        datagridUtilisateurListe.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'sDom': '<"H"flr>t<"F"ip>',
            'sAjaxSource': ajaxURL,
            'aaSorting': [[1, 'asc']],
            'aoColumns': [{
                'mData': 'code',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true
            }, {
                'mData': 'libelle',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true
            },
                $.extend(colonneAction, {
                    'mRender': function (data, type, full) {
                        return '<a class="action_supprimer" href="' + full.urlSuppressionGroupe + '" data-confirm="' + LOCALE_BO.confirmSupprUtilisateur + '"  >' + LOCALE_BO.supprimer + '</a>';
                    }
                })]
        }).fnSetFilteringDelay();
    }


    /**
     * Datagrid utilisé pour l'affichage d'une liste d'utilisateurs au sein d'un
     * groupe dynamique
     */
    var datagridUtilisateurGroupeDynamque = $('.datatableUtilisateurGROUPEDYN');
    if (datagridUtilisateurGroupeDynamque.length > 0) {
        ajaxURL = datagridUtilisateurGroupeDynamque.data('search') || '';
        datagridUtilisateurGroupeDynamque.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'sDom': '<"H"flr>t<"F"ip>',
            'sAjaxSource': ajaxURL,
            'aaSorting': [[1, 'asc']],
            'aoColumns': [{
                'mData': 'code',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true
            }, {
                'mData': 'libelle',
                'sDefaultContent': '',
                'bSearchable': null,
                'bSortable': true
            }]
        }).fnSetFilteringDelay();
    }

    /**
     * Datagrid utilisé pour l'affichage de pages rattachées à une rubrique
     */
    var datagridMultiFicheRubrique = $('.datatableMultiFicheRubrique');
    if (datagridMultiFicheRubrique.length > 0) {
        ajaxURL = datagridMultiFicheRubrique.data('search') || '';
        datagridMultiFicheRubrique.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'aaSorting': [[6, 'desc']],
            'sDom': 'rt<"F"ip>',
            'sAjaxSource': ajaxURL,
            'aoColumns': [colonneLibelle,
                colonneTypeObjet,
                colonneLangue,
                colonneEtatFiche,
                colonneDateModif,
                $.extend(colonneAction, {
                    'mRender': function (data, type, full) {
                        var actions = '';
                        if (full.urlModification) {
                            actions += '<a class="action_modifier" href="' + full.urlModification + '" title="' + LOCALE_BO.modifier + '" >' + LOCALE_BO.modifier + '</a>';
                        }
                        if (full.url) {
                            if (actions.length > 0) {
                                actions += ' | ';
                            }
                            actions += '<a class="action_apercu" href="' + full.url + '" title="' + LOCALE_BO.nouvelleFenetre + '" >' + LOCALE_BO.voirEnLigne + '</a>';
                        }
                        return actions;
                    }
                })]
        }).fnSetFilteringDelay();
    }


    /**
     * Datagrid utilisé pour l'affichage d'une liste d'utilisateurs au sein d'un
     * groupe
     */
    var datagridLibelle = $('.datatableLibelle');
    if (datagridLibelle.length > 0) {
        ajaxURL = datagridLibelle.data('search') || '';
        datagridLibelle.dataTable({
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'sDom': '<"H"flr>t<"F"ip>',
            'sAjaxSource': ajaxURL,
            'aaSorting': [[0, 'asc']],
            'aoColumns': [{
                'mData': 'type',
                'sDefaultContent': '',
                'bSortable': true
            }, {
                'mData': 'code',
                'sDefaultContent': '',
                'bSortable': true
            }, {
                'mData': 'libelleParLangue',
                'sDefaultContent': '',
                'bSortable': true,
                'mRender': function (data) {
                    var valeur = '<ul>';
                    $.each(data, function (index, value) {
                        valeur += '<li><img src="' + value.urlLangue + '" alt="' + value.langage + '" title="' + value.langue + '" />' + value.libelle + '</li>';
                    });
                    valeur += '</ul>';
                    return valeur;
                }
            },
                $.extend(colonneAction, {
                    'mRender': function (data, type, full) {
                        return '<a class="action_modifier" href="' + full.urlModification + '">' + LOCALE_BO.modifier + '</a> | ' +
                            '<a class="action_supprimer" href="' + full.urlSuppression + '" data-confirm="' + LOCALE_BO.confirmSupprLibelle + '"  >' + LOCALE_BO.supprimer + '</a>';
                    }
                })]
        }).fnSetFilteringDelay();
    }

    /**
     * Datagrid des rubriques La table doit contenir un thead pour fonctionner
     * correctement
     */
    var datagridRubrique = $('.datatableViews');
    if (datagridRubrique.length > 0) {
        ajaxURL = datagridRubrique.data('search') || '';
        datagridRubrique.dataTable({
            'sDom': '<"H"<"resume"V>flr>t<"F"ip>',
            'aoColumnDefs': [
                {'bSortable': false, 'aTargets': ['sanstri']},
                {'bSearchable': false, 'aTargets': ['sansfiltre']}
            ],
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'sAjaxSource': ajaxURL,
            'aoColumns': [colonneLibelle,
                {
                    'mData': 'rubriqueMere',
                    'sDefaultContent': '',
                    'bSearchable': null,
                    'bSortable': true
                }, {
                    'mData': 'code',
                    'sDefaultContent': '',
                    'bSearchable': null,
                    'bSortable': true
                },
                colonneLangue,
                $.extend(colonneAction, {
                    'mRender': function (data, type, full) {
                        var actions = '';
                        if (full.urlModification) {
                            actions += '<a class="action_modifier" href="' + full.urlModification + '" title="' + LOCALE_BO.modifier + '" >' + LOCALE_BO.modifier + '</a>';
                        }
                        if (full.urlSuppression) {
                            actions += ' | <a class="action_supprimer" href="' + full.urlSuppression + '" data-confirm="' + LOCALE_BO.confirmSupprRubrique + '"  >' + LOCALE_BO.supprimer + '</a>';
                        }
                        return actions;
                    }
                })],
            'fnDrawCallback': function () {
                $.fn.dataTable.tools.gestionNavigation(this.attr('id'));
            },
            'oViewsTool': {
                bLabel: false,
                bReflectSelection: false,
                actions: {
                    'oDefault': {
                        label: LOCALE_BO.viewTools.tableView,
                        icon: '/adminsite/styles/img/16x16/table.png'
                    },
                    'oOptions': [
                        {
                            groupLabel: LOCALE_BO.viewTools.switchView,
                            group: [
                                {
                                    label: LOCALE_BO.viewTools.treeView,
                                    icon: '/adminsite/styles/img/16x16/arborescence.png',
                                    metadatas: {
                                        'targetid': 'treeView'
                                    },
                                    js: function (element) {
                                        if (window.toggleView) {
                                            window.toggleView(element);
                                        }
                                    }
                                },
                                {
                                    label: LOCALE_BO.viewTools.tableView,
                                    icon: '/adminsite/styles/img/16x16/table.png',
                                    metadatas: {
                                        'targetid': 'listView'
                                    },
                                    js: function (element) {
                                        if (window.toggleView) {
                                            window.toggleView(element);
                                        }
                                    }
                                }]
                        }]
                }
            }
        }).fnSetFilteringDelay();
    }

    /**
     * Datagrid des groupes passé en serverside pour la montée en charge.
     */
    var datagridGroupe = $('.datatableViewsGroupe');
    if (datagridGroupe.length > 0) {
        ajaxURL = datagridGroupe.data('search') || '';
        datagridGroupe.dataTable({
            'sDom': '<"H"<"resume"V>flr>t<"F"ip>',
            'aoColumnDefs': [
                {'bSortable': false, 'aTargets': ['sanstri']},
                {'bSearchable': false, 'aTargets': ['sansfiltre']}
            ],
            'fnServerData': $.fn.dataTable.tools.processRequest,
            'bServerSide': true,
            'bProcessing': true,
            'sAjaxSource': ajaxURL,
            'aoColumns': [colonneLibelle,
                {
                    'mData': 'typeGroupe',
                    'sDefaultContent': '',
                    'bSearchable': null,
                    'bSortable': true
                }, {
                    'mData': 'structure',
                    'sDefaultContent': '',
                    'bSearchable': null,
                    'bSortable': true
                },
                $.extend(colonneAction, {
                    'mRender': function (data, type, full) {
                        var actions = '';
                        if (full.urlModification) {
                            actions += '<a class="action_modifier" href="' + full.urlModification + '" title="' + LOCALE_BO.modifier + '" >' + LOCALE_BO.modifier + '</a>';
                        }
                        if (full.urlSuppression) {
                            actions += ' | <a class="action_supprimer" href="' + full.urlSuppression + '" data-confirm="' + LOCALE_BO.confirmSupprRubrique + '"  >' + LOCALE_BO.supprimer + '</a>';
                        }
                        return actions;
                    }
                })],
            'fnDrawCallback': function () {
                $.fn.dataTable.tools.gestionNavigation(this.attr('id'));
            },
            'oViewsTool': {
                bLabel: false,
                bReflectSelection: false,
                actions: {
                    'oDefault': {
                        label: LOCALE_BO.viewTools.tableView,
                        icon: '/adminsite/styles/img/16x16/table.png'
                    },
                    'oOptions': [
                        {
                            groupLabel: LOCALE_BO.viewTools.switchView,
                            group: [
                                {
                                    label: LOCALE_BO.viewTools.treeView,
                                    icon: '/adminsite/styles/img/16x16/arborescence.png',
                                    metadatas: {
                                        'targetid': 'treeView'
                                    },
                                    js: function (element) {
                                        if (window.toggleView) {
                                            window.toggleView(element);
                                        }
                                    }
                                },
                                {
                                    label: LOCALE_BO.viewTools.tableView,
                                    icon: '/adminsite/styles/img/16x16/table.png',
                                    metadatas: {
                                        'targetid': 'listView'
                                    },
                                    js: function (element) {
                                        if (window.toggleView) {
                                            window.toggleView(element);
                                        }
                                    }
                                }]
                        }]
                }
            }
        }).fnSetFilteringDelay();
    }
    /**
     * Datagrid générique, utilisé pour les tables pré-générées de rubrique La
     * table doit contenir un thead pour fonctionner correctement
     */
    $('.datatableRubrique').dataTable({
        'sDom': 't<"F"ip>',
        'fnDrawCallback': function () {
            $.fn.dataTable.tools.gestionNavigation(this.attr('id'));
        }
    }).fnSetFilteringDelay();

    /**
     *
     */
    $('.dataTable').on('click', 'a[data-confirm]', function (event) {
        var that = $(this);
        event.preventDefault();
        $('<div></div>')
            .html(that.data('confirm'))
            .dialog({
                buttons: [{
                    text: LOCALE_BO.ok,
                    click: function () {
                        $(this).dialog('close');
                        window.location = that.attr('href');
                    }
                }, {
                    text: LOCALE_BO.fermer,
                    click: function () {
                        $(this).dialog('close');
                    }
                }],
                title: LOCALE_BO.confirmer
            });
    });
})(jQuery.noConflict());
