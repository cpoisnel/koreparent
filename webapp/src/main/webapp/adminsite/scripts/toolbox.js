(function ($) {
    'use strict';
    // Default configuration : edition is disabled to prevent hazardous behaviours
    var defaultConfiguration;

    function customizeConf(configuration) {
        if (configuration.disableNativeSpellChecker === false) {
            configuration.globalNotifications = {
                globalMessages: [
                    {
                        'uuid': '6c0df62e-3c07-425e-9f96-a2e2bbcad016',
                        'message': LOCALE_BO.ckeditor.notifications.spellcheker,
                        'level': 'info'
                    }
                ]
            };
        }
        var htmlElement = document.getElementsByTagName('html')[0];
        configuration.allowedContent = true;
        configuration.language = htmlElement.lang;
    }

    function loadExternalPlugins(conf) {
        if (conf.externalPaths) {
            for (var property in conf.externalPaths) {
                if (conf.externalPaths.hasOwnProperty(property) && !CKEDITOR.plugins.externals.hasOwnProperty(property)) {
                    CKEDITOR.plugins.addExternal(property, conf.externalPaths[property]);
                }
            }
        }
    }

    // Retrieves editors configurations and load them
    function loadCkeditors(editors, confKeys) {
        $.ajax(
            {
                'url': '/servlet/ckeditor/configuration',
                'data': {'conf_keys[]': confKeys},
                'dataType': 'json',
                'success': function (configs) {
                    editors.each(
                        function () {
                            var $this = $(this),
                                currentConfKey = $this.attr('data-conf'),
                                currentConf = configs[currentConfKey];
                            // Only for editors exposing a configuration key
                            if (currentConfKey) {
                                customizeConf(currentConf);
                                if (currentConf) {
                                    var currentEditor = CKEDITOR.replace($this.attr('id'), currentConf);
                                    currentEditor.on(
                                        'configLoaded', function (event) {
                                            loadExternalPlugins(event.listenerData.conf);
                                        }, null, {
                                            'conf': currentConf
                                        });
                                    // Empêche l'apparition des poignées de rendimensionnement sur les objets
                                    currentEditor.on( 'instanceReady', function() {
                                        this.document.$.execCommand('enableObjectResizing', false, false);
                                    } );
                                } else {
                                    CKEDITOR.replace($this.attr('id'), defaultConfiguration);
                                }
                            }
                        });
                },
                'error': function () {
                    // Fallback : if something goes wrong, we load default configuration to prevent broken editors
                    editors.each(
                        function () {
                            var $this = $(this);
                            if ($this.attr('data-conf')) {
                                CKEDITOR.replace($this.attr('id'), defaultConfiguration);
                            }
                        });
                }
            });
    }

    // Retrieve the default configuration
    function loadDefaultConfiguration() {
        $.ajax(
            {
                url: '/adminsite/ckeditor/configurations/defaultConfig.json',
                async: false,
                dataType: 'json',
                success: function (response) {
                    defaultConfiguration = response;
                },
                error: function () {
                    defaultConfiguration = {};
                }
            });
    }

    // Retrieves all editor components and their conf key
    function init() {
        var editors = $('.js-ckeditor'),
            confKeys = [];
        loadDefaultConfiguration();
        if (editors.length > 0) {
            CKEDITOR.timestamp = +new Date();
            editors.each(
                function configureSpecificToolbox() {
                    var $this = $(this),
                        currentConfKey = $this.attr('data-conf');
                    // If the editor exposes a confkey, we try to retrieve it.
                    // Otherwise, the default configuration is loaded
                    if (currentConfKey) {
                        confKeys.push(currentConfKey);
                    } else {
                        CKEDITOR.replace($this.attr('id'), defaultConfiguration);
                    }
                });
            if (confKeys.length > 0) {
                loadCkeditors(editors, confKeys);
            }
        }
    }

    $(function onDomReadyToolbox() {
        init();
    });
})(jQuery.noConflict());
