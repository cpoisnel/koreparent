(function ($) {
    'use strict';
    var $tabs = $('.js-tabs'),
        $contentFormatter = $('.js-contentformatter'),
        $linkText = $('#linkText');
    // Check current panel to send message to parent accordingly
    function checkCurrentPanel() {
        var $currentPanel = $tabs.tabs().getCurrentPanel(),
            $currentForm = $('form', $currentPanel),
            message;

        if ($currentForm.valid() && $contentFormatter.contentFormatter().isValid()) {
            if ($linkText.length > 0 && $linkText.prop('required') && ! $linkText.val()) {
                message = 'erase';
            } else {
                message = {
                    'href': $contentFormatter.val(),
                    'text': $linkText.length > 0 ? $linkText.val() : '',
                    'attributes': $currentPanel.attr('data-attributes') || '{"class": "lien_interne"}'
                };
            }
        } else {
            message = 'erase';
        }
        COMMONS_MESSAGES.postMessageToParent(message);
    }

    // Triggered when the current active panel changes
    function onTabChange(event) {
        var $panel = event.originalEvent.data;
        $contentFormatter.contentFormatter().handle($panel.attr('data-contentformatter-pattern'));
        checkCurrentPanel();
    }

    // Add tabs component
    if ($tabs.length > 0) {
        $tabs.each(
            function () {
                var $this = $(this);
                $this.tabs({});
                $this.on('TabChange', onTabChange);
            });
    }
    // Add content formatter component to handle input
    if ($contentFormatter.length > 0) {
        $contentFormatter.each(
            function () {
                var $this = $(this);
                $this.contentFormatter(
                    {
                        handleAtInit: $tabs.tabs().getCurrentPanel().attr('data-contentformatter-pattern'),
                        handleFallback: true
                    });
                $this.contentFormatter().registerListener(
                    {
                        'afterValidate': checkCurrentPanel,
                        'validationError': function () {
                            COMMONS_MESSAGES.postMessageToParent('erase');
                        },
                        'onClear': function () {
                            COMMONS_MESSAGES.postMessageToParent('erase');
                        }
                    });
            });
    }
    // Bind link text input
    $linkText.on('change keyup', checkCurrentPanel);
})(jQuery.noConflict());
