(function ($) {
    'use strict';
    var $tabs = $('.js-tabs'),
        $contentFormatter = $('.js-contentformatter'),
        $iframe = $('.js-request-iframe'),
    // [UGLY]
        exclusions = ['NOARBO_RUBRIQUE', 'NOARBO_CODE_RATTACHEMENT', 'NOARBO_CODE_STRUCTURE', 'CENTRE_INTERETS', 'GROUPE_DSI_COURANT', 'TYPE_GROUPE_DSI'];
    // Check if the given input match the mandatory criteria
    function isValidInput($input) {
        var result = $input && $input.val() && $input.attr('name');
        if ($input.is(':checkbox')) {
            result = result && $input.is(':checked');
        }
        if ($input.is(':radio')) {
            result = result && $input.is(':checked');
        }
        if ($input.is('select')) {
            result = result && $input.val() !== '0000';
        }
        return result;
    }

    // [UGLY] : handle "NOARBO" checkboxes
    function handleNoArbo($content, params) {
        var $codeRubrique = $('input[name="CODE_RUBRIQUE"]', $content),
            $codeRattachement = $('input[name="CODE_RATTACHEMENT"]', $content),
            $codeStructure = $('input[name="CODE_STRUCTURE"]', $content);
        // Gestion du code rubrique
        if ($codeRubrique.length > 0 && $codeRubrique.val()) {
            var $noArboRubrique = $('input[type="checkbox"][name="NOARBO_RUBRIQUE"]', $content);
            params.CODE_RUBRIQUE = $codeRubrique.val() + ($noArboRubrique.is(':checked') ? '_NOARBO' : '');
        }
        // Gestion du code structure
        if ($codeRattachement.length > 0 && $codeRattachement.val()) {
            var $noArboRattachement = $('input[type="checkbox"][name="NOARBO_CODE_RATTACHEMENT"]', $content);
            params.CODE_RATTACHEMENT = $codeRattachement.val() + ($noArboRattachement.is(':checked') ? '_NOARBO' : '');
        }
        if ($codeStructure.length > 0 && $codeStructure.val()) {
            var $noArboStructure = $('input[type="checkbox"][name="NOARBO_CODE_STRUCTURE"]', $content);
            params.CODE_STRUCTURE = $codeStructure.val() + ($noArboStructure.is(':checked') ? '_NOARBO' : '');
        }
    }

    // [UGLY] : handle special case "CENTRE_INTERETS"
    function handleInterests($content, params) {
        var $checkbox = $('input[type="checkbox"][name="CENTRE_INTERETS"]', $content);
        if ($checkbox.is(':checked')) {
            params.THEMATIQUE = '[centresinteret]';
        }
    }

    // [UGLY] : handle dsi groups values
    function handleDsi($content, params) {
        var $dsi = $('input[name="GROUPE_DSI"]', $content);
        if ($dsi.length > 0 && $dsi.val()) {
            params.GROUPE_DSI = $dsi.val().replace(/,/g, '@');
        } else {
            var $radios = $('input[name="GROUPE_DSI_COURANT"]:checked', $content);
            if ($radios.length === 1) {
                switch ($radios.val()) {
                    case '1' :
                        params.GROUPE_DSI = 'DYNAMIK';
                        break;
                    case '2' :
                        params.GROUPE_DSI = '[groupe]';
                        break;
                    case '3' :
                        var $select = $('select[name="TYPE_GROUPE_DSI"]', $content);
                        if ($select.length > 0 && $select.val() && $select.val() !== '0000') {
                            params.GROUPE_DSI = $select.val() + '_TYPEGROUPE';
                        }
                        break;
                }
            }
        }
    }

    // [UGLY] : filter fields by checking value against an array of excluded values
    function specificFieldFilter() {
        var $this = $(this),
            name = $this.attr('name'),
            result = true;
        if (name) {
            $.each(
                exclusions, function (index, value) {
                    if (value === name) {
                        result = false;
                    }
                });
        }
        return result;
    }

    // Populate params according to inputs values ( <input's name> = <input's value>)
    function handleGenericInputs($content, params) {
        var $simpleInputs = $('input[type!="hidden"][type!="button"], select, textarea, .kmonoselect > input[id], .kmultiselect-ttl > input[id], .kmultiselect-ltl > input[id]', $content).filter(specificFieldFilter);
        $simpleInputs.each(
            function (i, input) {
                var $currentInput = $(input);
                if (isValidInput($currentInput)) {
                    params[$currentInput.attr('name')] = $currentInput.val();
                }
            });
    }

    // Generate a object containing every association <name> = <value> found on the current screen.
    function prepareParams($content) {
        var params = {};
        handleGenericInputs($content, params);
        handleNoArbo($content, params);
        handleDsi($content, params);
        handleInterests($content, params);
        return params;
    }

    // Generate an object request from the given parameters
    function buildRequest(params) {
        var request = '';
        $.each(
            params, function (name, value) {
                request += (request ? '&' : '') + name + '=' + value;
            });
        return request;
    }

    // Bind 'change' event on select input
    function bindSelect() {
        var $select = $('.js-request-select');
        $select.on(
            'change', function () {
                var $this = $(this);
                $iframe.attr('src', $('option:selected', $this).attr('data-url'));
            });
    }

    function updateFormatter() {
        var $iframeDocument = $($iframe[0].contentWindow.document);
        $contentFormatter.contentFormatter().update(
            {
                'REQUEST': $('.js-request-select').val() + ';' + buildRequest(prepareParams($iframeDocument))
            });
    }

    // Bind all possible input's 'change' in iframe container
    function bindIframeInputs() {
        $iframe.on(
            'load', function () {
                var $iframeDocument = $($iframe[0].contentWindow.document);
                $iframeDocument.ready(
                    function () {
                        var $inputs = $('input[type!="hidden"], select, textarea, .kmonoselect > input[id], .kmultiselect > input[id]', $iframeDocument);
                        $inputs.on(
                            'change keyup', {'$inputs': $inputs}, updateFormatter);
                        // [UGLY] Specific bindings for 'GROUPE_DSI' mutual exclusion
                        var $radios = $('input[type="radio"][name="GROUPE_DSI_COURANT"]', $iframeDocument),
                            $monoSelectDsi = $('#kMonoSelectGROUPE_DSI', $iframeDocument),
                            $dsiField = $('#GROUPE_DSI', $iframeDocument);
                        $radios.on(
                            'click', function () {
                                $monoSelectDsi.kMonoSelect().clear();
                            });
                        $dsiField.on(
                            'change', function () {
                                if ($(this).val()) {
                                    $radios.prop('checked', false);
                                }
                            });
                        updateFormatter();
                    });
            });
    }

    // Bind all necessary events
    function bindEvents() {
        bindSelect();
        bindIframeInputs();
    }

    // Screen initialization
    function onTabChange(e) {
        var $panel = e.originalEvent.data;
        if ($panel && $panel.is('[data-panel="internal-request"]')) {
            bindEvents();
            updateFormatter();
            $iframe.attr('src', $('option:selected', $('.js-request-select')).attr('data-url'));
        }
    }

    if ($tabs.tabs().isPanelActive('internal-request')) {
        bindEvents();
    }

    $tabs.on('TabChange', onTabChange);

})(jQuery.noConflict());
