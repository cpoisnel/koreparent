(function ($) {
    'use strict';
    var datagridToolbox = $('.dynamicDataTable'),
        requestStore = {},
        updateInterval;
    // Retrieves fields of interest
    function getRelevantFields() {
        var $fichePanel = $('[data-panel="internal-fiche"]');
        return $('input, select, textarea', $fichePanel).filter(
            function () {
                return $(this).closest('.dataTables_wrapper').length === 0;
            });
    }

    // Computes an returns a request according to the detected params in the inputs fields
    function buildRequest() {
        var finalUrl = '/servlet/com.univ.datagrid.servlet.DatagridServlet?BEAN_RECHERCHE=multiFicheToolboxDatagrid',
            $fichePanel = $('[data-panel="internal-fiche"]'),
            $fields = $('input, select, textarea', $fichePanel).filter(
                function () {
                    var $this = $(this);
                    return $this.attr('name') && $this.attr('name').indexOf('#', 0) !== 0 && $this.val() && $this.val() !== '0000';
                });
        $fields.each(
            function () {
                var $this = $(this);
                finalUrl += '&' + $this.attr('name') + '=' + $this.val();
            });
        return encodeURI(finalUrl);
    }

    // Get the request for the current inputs values.
    // This function handles a pool <key> = <request> to take advantage of the caching mechanism on the server side.
    function getRequest() {
        var request = buildRequest();
        if (!requestStore[request]) {
            requestStore[request] = $.generateGuid();
        }
        return request + '&ID_REQUETE=' + requestStore[request];
    }

    // Ask the datatable to refresh its data.
    function reloadData() {
        datagridToolbox.dataTable().fnReloadAjax(getRequest());
    }

    // Binds 'change' events on all the given fields (triggers 'reloadData()')
    function bindFields() {
        var $fields = getRelevantFields();
        $fields.on(
            'change keyup', function () {
                if (updateInterval) {
                    clearInterval(updateInterval);
                }
                updateInterval = setInterval(
                    function () {
                        reloadData();
                        clearInterval(updateInterval);
                        updateInterval = null;
                    }, 1000);
            });
    }

    // Bind 'click' events on datatable rows
    function bindDataTableRows() {
        $('tbody', datagridToolbox).on(
            'click', 'tr', function () {
                var $this = $(this),
                    $sectionChoice = $this.find('.js-section-choice'),
                    $valuesHolder = $this.find('.js-contentFormatter-valuesholder'),
                    $contentFormatter = $('.js-contentformatter');
                $('tr input[type="radio"]', $this.closest('tbody')).prop('checked', false);
                $valuesHolder.prop('checked', true);

                var content = {
                    'OBJET': $valuesHolder.attr('data-objet'),
                    'CODE': $valuesHolder.attr('data-code'),
                    'LANGUE': $valuesHolder.attr('data-langue')
                };
                if($sectionChoice.length > 0 && $sectionChoice.val()) {
                    $sectionChoice.attr('title', $sectionChoice.find(':selected').attr('data-path'));
                    content.PARAMS = '?RF=' + $sectionChoice.val();
                }
                $contentFormatter.contentFormatter().update(content);
            });
    }

    // Handle collapsible behaviour
    function bindCollapsible() {
        var $collapsable = $('.plier-deplier');
        $('.plier-deplier__bouton', $collapsable).on(
            'click', function () {
                var $this = $(this),
                    $label = $('.js-label', $this),
                    $icon = $('.icon', $this),
                    $content = $('.plier-deplier__contenu', $collapsable);
                if ($content.is('.plier-deplier__contenu--clos')) {
                    $content.removeClass('plier-deplier__contenu--clos');
                    $icon.removeClass('icon-plus').addClass('icon-minus');
                    $label.html('Moins de critères');
                } else {
                    $content.addClass('plier-deplier__contenu--clos');
                    $icon.removeClass('icon-minus').addClass('icon-plus');
                    $label.html('Plus de critères');
                }
            });
    }

    // Bind all necessary events
    function bindEvents() {
        bindFields();
        bindDataTableRows();
        bindCollapsible();
    }

    // Return params of the given request as an object
    function parseRequest(request) {
        // remove any preceding url and split
        request = request.substring(request.indexOf('?') + 1).split('&amp;');
        var params = {}, pair, d = decodeURIComponent;
        // march and parse
        for (var i = request.length - 1; i >= 0; i--) {
            pair = request[i].split('=');
            params[d(pair[0])] = d(pair[1]);
        }
        return params;
    }

    function buildOptionGroup(sectionGroup) {
        var group = '<optgroup label="' + sectionGroup.label + '">';
        for(var i = 0; i < sectionGroup.items.length; i++) {
            var currentItem = sectionGroup.items[i];
            group += '<option value="' + currentItem.code + '" data-path="' + currentItem.path + '">' + currentItem.label + '</option>';
        }
        group += '</optgroup>';
        return group;
    }

    function buildMultiSectionSelect(multiSections) {
        var select = '<select class="section-choice--width js-section-choice">';
        select += '<option value="" data-path="' + LOCALE_BO.ckeditor.link.section.contextual + '">' + LOCALE_BO.ckeditor.link.section.contextual + '</option>';
        for(var i = 0; i < multiSections.length; i++) {
            var section = multiSections[i];
            select += buildOptionGroup(section);
        }
        return select + '</select>';
    }

    // Initialize specific datatable
    function initDataTable() {
        if (datagridToolbox.length > 0) {
            datagridToolbox.dataTable(
                {
                    'fnServerData': $.fn.dataTable.tools.processRequest,
                    'bServerSide': true,
                    'bProcessing': true,
                    'aaSorting': [[1, 'asc']],
                    'sAjaxSource': getRequest(),
                    'iDisplayLength': 5,
                    'aoColumns': [
                        {
                            'sDefaultContent': '',
                            'bSortable': false,
                            'mRender': function (data, type, full) {
                                var params = parseRequest(full.url),
                                    checkId = full.typeObjet + '-' + full.id;
                                return '<input type="radio" id="' + checkId + '" class="js-contentFormatter-valuesholder" data-objet="' + params.OBJET + '" data-code="' + params.CODE + '" data-langue="' + params.LANGUE + '"><label>' + data + '</label>';
                            }
                        },
                        {
                            'mData': 'libelle',
                            'sDefaultContent': '',
                            'bSortable': true
                        },
                        {
                            'mData': 'dateModification',
                            'sDefaultContent': '',
                            'bSortable': true
                        },
                        {
                            'mData': 'rubrique',
                            'sDefaultContent': '',
                            'bSortable': true,
                            'mRender': function (data, type, full) {
                                if(full.multiSections && full.multiSections.length > 0) {
                                    return buildMultiSectionSelect(full.multiSections);
                                } else {
                                    return '<span title="' + full.filAriane + '">' + data + '</span>';
                                }
                            }
                        },
                        {
                            'mData': 'typeObjet',
                            'sDefaultContent': '',
                            'bSortable': true
                        },
                        {
                            'mData': 'langue',
                            'sDefaultContent': '',
                            'bSortable': true,
                            'mRender': function (data, type, full) {
                                return '<img src="' + full.urlDrapeauLangue + '" alt="' + data + '"/>';
                            }
                        }]
                }).fnSetFilteringDelay();
        }
    }

    // Initialization
    function init() {
        initDataTable();
        bindEvents();
    }

    init();
})(jQuery.noConflict());
