(function ($) {
    'use strict';
    var $tabs = $('.js-tabs'),
        $contentFormatter = $('.js-contentformatter'),
        $iframe = $('.js-media-iframe'),
        initDone;

    function getParentValueForInput(selector) {
        var currentInput =  window.parent.document.querySelector('input[type="hidden"][name=EXT]'),
            result = '';
        if (currentInput) {
            result = currentInput.value;
        }
        return result;
    }

    function getProcessConfiguration() {
        var processConfiguration = {};
        processConfiguration.ext = getParentValueForInput('input[type="hidden"][name=EXT]');
        processConfiguration.proc = getParentValueForInput('input[type="hidden"][name=PROC]');
        processConfiguration.code = getParentValueForInput('input[type="hidden"][name=TS_CODE]');
        processConfiguration.objet = getParentValueForInput('input[type="hidden"][name=OBJET]');
        processConfiguration.locale = getParentValueForInput('input[type="hidden"][name=LOCALE]');
        processConfiguration.idFiche = getParentValueForInput('input[type="hidden"][name=ID_FICHE]');
        return processConfiguration;
    }

    function updateFormatter() {
        var $iframeDocument = $($iframe[0].contentWindow.document),
            $input = $('[name="ID_RESSOURCE"]', $iframeDocument);
        if ($input.length > 0 && $input.val()) {
            $contentFormatter.contentFormatter().update(
                {
                    'ID_RESSOURCE': $input.val()
                });
        } else {
            $contentFormatter.contentFormatter().clear();
        }
    }

    function onIframeLoad() {
        var $iframeDocument = $($iframe[0].contentWindow.document);
        $iframeDocument.ready(updateFormatter);
    }

    function bindIframe() {
        $iframe.off('load', onIframeLoad);
        $iframe.on('load', onIframeLoad);
    }

    function loadiFrame() {
        if (!$iframe.attr('src')) {
            var processConf = getProcessConfiguration(),
                url = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&FCK_PLUGIN=TRUE&MODE_FICHIER=LIEN';
            url += '&CODE=' + processConf.code;
            url += '&OBJET=' + processConf.objet;
            url += '&ID_FICHE=' + processConf.idFiche;
            $iframe.attr('src', url);
            bindIframe();
        }
    }

    // Screen initialization
    function onTabChange(e) {
        var $panel = e.originalEvent.data;
        if (!initDone && $panel && $panel.is('[data-panel="internal-file"]')) {
            initDone = true;
            loadiFrame();
        }
    }

    if ($tabs.tabs().isPanelActive('internal-file')) {
        bindIframe();
    }

    $tabs.on('TabChange', onTabChange);
})(jQuery.noConflict());
