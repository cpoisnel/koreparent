(function ($) {
    'use strict';
    // Retrieves given editor anchors
    function getEditorAnchors(editor) {
        var editable = editor.editable(),
            // The scope of search for anchors is the entire document for inline editors
            // and editor's editable for classic editor/divarea (#11359).
            scope = ( editable.isInline() && !editor.plugins.divarea ) ? editor.document : editable,
            links = scope.getElementsByTag('a'),
            imgs = scope.getElementsByTag('img'),
            anchors = [],
            i = 0,
            item;
        // Retrieve all anchors within the scope.
        while (( item = links.getItem(i++) )) {
            if (item.data('cke-saved-name') || item.hasAttribute('name')) {
                anchors.push(
                    {
                        name: item.data('cke-saved-name') || item.getAttribute('name'),
                        id: item.getAttribute('id')
                    });
            }
        }
        // Retrieve all "fake anchors" within the scope.
        i = 0;
        while (( item = imgs.getItem(i++) )) {
            if (( item = window.parent.CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, item) )) {
                anchors.push(
                    {
                        name: item.getAttribute('name'),
                        id: item.getAttribute('id')
                    });
            }
        }
        return anchors;
    }

    // Update messages according to the select "NAME" values.
    function updateMessages($anchorPanel) {
        var $select = $('[name="NAME"]', $anchorPanel),
            $anchorsBlock = $('.js-anchor-block', $anchorPanel);
        if ($select.children('option').length === 1) {
            $anchorsBlock.hide();
            $('<p class="message alert alert-warning">').html(LOCALE_BO.ckeditor.plugins.link.anchor.tip.alert).appendTo($('.js-messages', $anchorPanel));
        } else {
            $anchorsBlock.show();
            $('p.alert-warning', $anchorPanel).remove();
        }
    }

    // Clean up and populate select "NAME" with the detected anchors on all editors present in the current page.
    function updateSelect($anchorPanel) {
        var $select = $('[name="NAME"]', $anchorPanel).empty();
        $('<option/>').val('#').text(LOCALE_BO.select.undefined).appendTo($select);
        $.each(
            window.parent.CKEDITOR.instances, function (index, editor) {
                var anchors = getEditorAnchors(editor);
                $.each(
                    anchors, function (anchorIndex, anchor) {
                        var $currentOption = $('<option/>').val(anchor.id).text(anchor.name).appendTo($select);
                        if ($select.attr('data-value') === anchor.id) {
                            $currentOption.prop('selected', true);
                        }
                    });
            });
    }

    // Initialization
    function init() {
        var $anchorPanel = $('.js-tabs__panel[data-panel="anchor"]');
        updateSelect($anchorPanel);
        updateMessages($anchorPanel);
        $('.js-contentformatter').contentFormatter().update();
    }

    function onTabChange(e) {
        var $panel = e.originalEvent.data;
        if ($panel && $panel.is('[data-panel="anchor"]')) {
            updateSelect($panel);
        }
    }

    init();

    $('.js-tabs').on('TabChange', onTabChange);
})(jQuery.noConflict());
