(function ($) {
    'use strict';
    function bindTreeSelect($jsTree) {
        $jsTree.on(
            'select_node.jstree', function (event, data) {
                var $contentFormatter = $('.js-contentformatter');
                if ($contentFormatter.contentFormatter()) {
                    $contentFormatter.contentFormatter().update(
                        {
                            'CODE_RUBRIQUE': data.rslt.obj.data('sCode')
                        });
                }
            });
    }

    // Initialization
    function init() {
        var $jsTree = $('.jstree');
        if ($jsTree.length > 0) {
            bindTreeSelect($jsTree);
        }
    }

    init();
})(jQuery.noConflict());
