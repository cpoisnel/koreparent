function onDialogEvent(e) {
    'use strict';
    if (e.name === 'load') {
        (function ($) {

            function checkOkButton() {
                var $previewImage = $('.js-kvideo__preview');
                if ($previewImage.length > 0) {
                    e.sender.getButton('ok').enable();
                } else {
                    e.sender.getButton('ok').disable();
                }
            }
            function initKVideoHandling() {
                checkOkButton();
            }

            initKVideoHandling();
        })(jQuery.noConflict());
    }
}