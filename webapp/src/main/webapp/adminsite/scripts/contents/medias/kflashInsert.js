function onDialogEvent(e) {
    'use strict';
    if (e.name === 'load') {
        (function ($) {
            function handleMediaList() {
                $('.js-media-list__container, .js-media-list__edit-button').on(
                    'click', function () {
                        var $this = $(this);
                        $('input[type="hidden"][name="ACTION"]').val($this.attr('data-action'));
                        $('input[type="hidden"][name="ID_MEDIA"]').val($this.attr('data-idmedia'));
                        $('input[type="hidden"][name="HEIGHT"]').val($this.attr('data-height') || 0);
                        $('input[type="hidden"][name="WIDTH"]').val($this.attr('data-width') || 0);
                        $this.closest('form').submit();
                    });
            }

            function generateStyleProperties() {
                var props = {},
                    $propertyHandlers = $('input[data-propertyhandler].js-kflash__property-handler, select[data-propertyhandler].js-kflash__property-handler');
                $propertyHandlers.each(
                    function () {
                        var $this = $(this),
                            properties = $this.attr('data-propertyhandler').split(',');
                        $.each(
                            properties, function (index, property) {
                                props[property] = $this.val() + ($this.attr('data-propertyhandler-unit') || '');
                            });
                    });
                return props;
            }

            function checkOkButton() {
                var $previewFlash = $('.js-kflash__preview > .kflash');
                if ($previewFlash.length > 0) {
                    e.sender.getButton('ok').enable();
                } else {
                    e.sender.getButton('ok').disable();
                }
            }

            function updateFlashPreview() {
                var $previewFlash = $('.js-kflash__preview > .kflash');
                $previewFlash.css(generateStyleProperties());
                checkOkButton();
            }

            function bindPropertyHandler() {
                var $propertyHandlers = $('input[data-propertyhandler].js-kflash__property-handler, select[data-propertyhandler].js-kflash__property-handler');
                $propertyHandlers.on(
                    'change keyup', function () {
                        updateFlashPreview();
                    });
            }

            function bindAlternativeInput() {
                $('input[name="ALT"]').on(
                    'change keyup', function () {
                        var $this = $(this),
                            $previewFlash = $('.js-kflash__preview > img');
                        $previewFlash.attr(
                            {
                                'alt': $this.val(),
                                'title': $this.val()
                            });
                    });
            }

            function initkFlashHandling() {
                handleMediaList();
                bindPropertyHandler();
                bindAlternativeInput();
                updateFlashPreview();
            }

            initkFlashHandling();
        })(jQuery.noConflict());
    }
}