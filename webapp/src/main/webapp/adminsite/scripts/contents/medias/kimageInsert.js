function onDialogEvent(e) {
    'use strict';
    if (e.name === 'load') {
        (function ($) {
            function handleMediaList() {
                $('.js-media-list__container, .js-media-list__edit-button').on(
                    'click', function () {
                        var $this = $(this);
                        $('input[type="hidden"][name="ACTION"]').val($this.attr('data-action'));
                        $('input[type="hidden"][name="ID_MEDIA"]').val($this.attr('data-idmedia'));
                        $('input[type="hidden"][name="HEIGHT"]').val($this.attr('data-height') || 0);
                        $('input[type="hidden"][name="WIDTH"]').val($this.attr('data-width') || 0);
                        $this.closest('form').submit();
                    });
            }

            function getSizeHandlers() {
                return $('input[data-propertyhandler="width"].js-kimage__property-handler, input[data-propertyhandler="height"].js-kimage__property-handler');
            }

            function handleScale($propertyHandler) {
                var $widthHandler = $('input[data-propertyhandler="width"].js-kimage__property-handler'),
                    $heightHandler = $('input[data-propertyhandler="height"].js-kimage__property-handler'),
                    naturalWidth = $.parseInteger($widthHandler.attr('data-propertyhandler-reset')),
                    naturalHeight = $.parseInteger($heightHandler.attr('data-propertyhandler-reset'));
                if ($propertyHandler.is('[data-propertyhandler="width"]')) {
                    $heightHandler.val(Math.round(naturalHeight * ($propertyHandler.val() / naturalWidth)));
                } else if ($propertyHandler.is('[data-propertyhandler="height"]')) {
                    $widthHandler.val(Math.round(naturalWidth * ($propertyHandler.val() / naturalHeight)));
                }
            }

            function generateStyleProperties() {
                var props = {},
                    $propertyHandlers = $('input[data-propertyhandler].js-kimage__property-handler, select[data-propertyhandler].js-kimage__property-handler');
                $propertyHandlers.each(
                    function () {
                        var $this = $(this),
                            properties = $this.attr('data-propertyhandler').split(',');
                        $.each(
                            properties, function (index, property) {
                                props[property] = $this.val() + ($this.attr('data-propertyhandler-unit') || '');
                            });
                    });
                return props;
            }

            function checkOkButton() {
                var $previewImage = $('.js-kimage__preview > img');
                if ($previewImage.length > 0 && $previewImage.attr('src') &&
                    $previewImage.attr('alt') && $previewImage.attr('title')) {
                    e.sender.getButton('ok').enable();
                } else {
                    e.sender.getButton('ok').disable();
                }
            }

            function updateImagePreview() {
                var $previewImage = $('.js-kimage__preview > img'),
                    $altInput = $('input[name="ALT"]');
                $previewImage.css(generateStyleProperties());
                $previewImage.attr(
                    {
                        'alt': $altInput.val(),
                        'title': $altInput.val()
                    });
                checkOkButton();
            }

            function updateLockButton() {
                var $lockButton = $('.js-kimage__button-lock'),
                    $widthHandler = $('input[data-propertyhandler="width"].js-kimage__property-handler'),
                    $heightHandler = $('input[data-propertyhandler="height"].js-kimage__property-handler'),
                    naturalWidth = $.parseInteger($widthHandler.attr('data-propertyhandler-reset')),
                    naturalHeight = $.parseInteger($heightHandler.attr('data-propertyhandler-reset')),
                    currentWidth = $.parseInteger($widthHandler.val()),
                    currentHeight = $.parseInteger($heightHandler.val());
                if((naturalWidth / naturalHeight) === (currentWidth/currentHeight)) {
                    $lockButton.addClass('js-kimage__button-lock--locked');
                    $lockButton.find('.icon').removeClass('icon-unlocked').addClass('icon-lock');
                } else {
                    $lockButton.removeClass('js-kimage__button-lock--locked');
                    $lockButton.find('.icon').removeClass('icon-lock').addClass('icon-unlocked');
                }
            }

            function resetSizeHandlers() {
                var $sizeHandlers = getSizeHandlers();
                $sizeHandlers.each(
                    function (index, component) {
                        var $currentSizeHandler = $(component);
                        $currentSizeHandler.val($currentSizeHandler.attr('data-propertyhandler-reset'));
                    });
                updateLockButton();
                updateImagePreview();
            }

            function bindPropertyHandler() {
                var $propertyHandlers = $('input[data-propertyhandler].js-kimage__property-handler, select[data-propertyhandler].js-kimage__property-handler');
                $propertyHandlers.on(
                    'change keyup', function () {
                        if ($('.js-kimage__button-lock').is('.js-kimage__button-lock--locked')) {
                            handleScale($(this));
                        }
                        updateImagePreview();
                    });
            }

            function bindAlternativeInput() {
                $('input[name="ALT"]').on(
                    'change keyup', function () {
                        updateImagePreview();
                    });
            }

            function bindSizeButtons() {
                $('.js-kimage__button-reset').on(
                    'click', function () {
                        resetSizeHandlers();
                    });
                $('.js-kimage__button-lock').on(
                    'click', function () {
                        var $this = $(this);
                        if (!updateLockButton()) {
                            $this.removeClass('js-kimage__button-lock--locked');
                            $('.icon', $this).removeClass('icon-lock').addClass('icon-unlocked');
                        } else {
                            $this.addClass('js-kimage__button-lock--locked');
                            $('.icon', $this).removeClass('icon-unlocked').addClass('icon-lock');
                            handleScale($('input[data-propertyhandler="width"].js-kimage__property-handler'));
                            updateImagePreview();
                        }
                    });
            }

            function initKImageHandling() {
                handleMediaList();
                bindPropertyHandler();
                bindAlternativeInput();
                bindSizeButtons();
                updateImagePreview();
                updateLockButton();
            }

            initKImageHandling();
        })(jQuery.noConflict());
    }
}