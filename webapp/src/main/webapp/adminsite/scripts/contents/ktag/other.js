(function ($) {
    'use strict';
    var pagePath = document.querySelector('.js-pagePath'),
        urlTag = document.querySelector('.js-url'),
        planSite = document.querySelector('.js-planSite');

    function buildPlanSiteMessage() {
        var inputUrl = document.getElementById('planSite');
        var codeRubrique = document.getElementById('planSite_codeRubrique').value;
        var nbNiveaux = document.getElementById('planSite_nbNiveaux').value;
        inputUrl.value = '[plan_site;';
        if (codeRubrique) {
            inputUrl.value += 'CODE_RUBRIQUE=' + codeRubrique;
        }
        if (nbNiveaux) {
            inputUrl.value += ';NB_NIVEAUX=' + nbNiveaux;
        }
        inputUrl.value += ']';
        return {
            'tag': 'kother',
            'value': inputUrl.value
        };
    }

    function buildUrlMessage() {
        var inputUrl = document.getElementById('url');
        inputUrl.value = '[url;' + $('#js-url').val() + ']';
        return {
            'tag': 'kother',
            'value': inputUrl.value
        };
    }

    function buildPageMessage() {
        var page = document.getElementById('page');
        page.value = '[page;' + $('#js-pagePath').val() + ']';
        return {
            'tag': 'kother',
            'value': page.value
        };
    }

    function buildDateMessage() {
        return {
            'tag': 'kother',
            'value': '[date]'
        };
    }

    function buildAndSendMessage() {
        var $currentRadio = $('input[name="otherTag"]:checked'),
            id = $currentRadio.attr('id'),
            message;
        if (id === 'page') {
            message = buildPageMessage();
        } else if (id === 'url') {
            message = buildUrlMessage();
        } else if (id === 'planSite') {
            message = buildPlanSiteMessage();
        } else if (id === 'date') {
            message = buildDateMessage();
        }
        if (message) {
            message.$form = $currentRadio.closest('form');
            COMMONS_MESSAGES.postMessageToParent(message);
        } else {
            COMMONS_MESSAGES.postMessageToParent('erase');
        }
    }

    $('input[name="otherTag"]').on(
        'change', function() {
            COMMONS_KTAG.toggleBlock(this.id, pagePath, 'page');
            COMMONS_KTAG.toggleBlock(this.id, urlTag, 'url');
            COMMONS_KTAG.toggleBlock(this.id, planSite, 'planSite');
            buildAndSendMessage();
        });
    $('#js-pagePath').on('change keyup', buildAndSendMessage);
    $('#js-url').on('change keyup', buildAndSendMessage);
    $('#planSite_nbNiveaux, #planSite_codeRubrique').on('change keyup', buildAndSendMessage);

    // Modification mode
    if ($('.js-tabs').tabs().isPanelActive('kother')) {
        buildAndSendMessage();
    }
})(jQuery.noConflict());
