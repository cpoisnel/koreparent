(function ($) {
    'use strict';
    var typeGroupe = document.querySelector('.js-typeGroupe');

    function buildDebutGroupeType() {
        var debutGroupeType = document.getElementById('js-debutgroupeType'),
            value = '[traitement;debutgroupe;' + debutGroupeType.value + ']';
        return {
            'tag': 'kgroup',
            'value': value
        };
    }

    function buildAndSendMessage() {
        var $currentRadio = $('input[name="groupTag"]:checked'),
            message;
        if ($currentRadio.attr('id') === 'debutgroupeType') {
            message = buildDebutGroupeType();
        } else {
            message = {
                'tag': 'kgroup',
                'value': $currentRadio.val()
            };
        }
        COMMONS_MESSAGES.postMessageToParent(message);
    }

    $('input[name="groupTag"]').on(
        'change', function() {
            COMMONS_KTAG.toggleBlock(this.id, typeGroupe, 'debutgroupeType');
            buildAndSendMessage();
        });
    $('#js-debutgroupeType').on('change', buildAndSendMessage);

    // Modification mode
    if ($('.js-tabs').tabs().isPanelActive('kgroup')) {
        buildAndSendMessage();
    }
})(jQuery.noConflict());
