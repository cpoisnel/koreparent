var COMMONS_KTAG = function() {
    'use strict';
    return {
        hideBlock: function(element) {
            if (element && element.className.indexOf('masquer') === -1) {
                element.className += element.className + ' masquer';
            }
        },
        toggleBlock: function(changedElementId, elementToToggle, elementId) {
            if (changedElementId === elementId) {
                elementToToggle.className = elementToToggle.className.replace(/masquer/g, '');
            } else {
                COMMONS_KTAG.hideBlock(elementToToggle);
            }
        },
        // Check if the given input match the mandatory criteria
        isValidInput: function (input) {
            var result = input && input.value && input.type !== 'checkbox' && input.getAttribute('name');
            if (input.tagName === 'SELECT') {
                result = result && input.value !== '0000';
            }
            return result;
        },
        buildRequest: function(params) {
            var request = '';
            for (var prop in params) {
                if (params.hasOwnProperty(prop)) {
                    request += (request ? '#' : '') + prop + '=' + params[prop];
                }
            }
            return request;
        },
        handleGenericInputs: function(content, params) {
            for (var i = 0; i < content.length; i++) {
                var currentInput = content[i];
                if (COMMONS_KTAG.isValidInput(currentInput)) {
                    params[currentInput.getAttribute('name')] = currentInput.value;
                }
            }
            return params;
        },
        handleCheckBoxes: function(content, params) {
            for (var i = 0; i < content.length; i++) {
                var currentInput = content[i],
                    inputName = currentInput.getAttribute('name');
                if (currentInput.checked && inputName) {
                    if (inputName.indexOf('_DYNAMIK') !== -1) {
                        params[inputName.substr(0, inputName.length - '_DYNAMIK'.length)] = currentInput.value;
                    } else {
                        params[inputName] = currentInput.value;
                    }
                }
            }
            return params;
        }
    };
}();
(function($) {
    'use strict';

    // Triggered when the current active panel changes
    function onTabChange() {
        COMMONS_MESSAGES.postMessageToParent('erase');
    }

    var $tabs = $('.js-tabs');
    if ($tabs.length > 0) {
        $tabs.each(
            function () {
                var $this = $(this);
                $this.tabs({});
                $this.on('TabChange', onTabChange);
            });
    }
})(jQuery.noConflict());
