(function ($) {
    'use strict';
    var addLink = document.querySelector('.js-lien_ajout'),
        updateLink = document.querySelector('.js-lien_modification'),
        deleteLink = document.querySelector('.js-lien_suppression'),
        validateLink = document.querySelector('.js-lien_validation'),
        $contribInputs = $('input[type!="hidden"], select, textarea, .kmonoselect > input[id], .kmultiselect-ttl > input[id]', $('#js-lien_fiches'));

    function contribChange() {
        var optionSelected = $('#lien_fiches_action').find('option:selected');
        COMMONS_KTAG.toggleBlock(optionSelected.attr('id'), addLink, 'lien_ajout');
        COMMONS_KTAG.toggleBlock(optionSelected.attr('id'), updateLink, 'lien_modification');
        COMMONS_KTAG.toggleBlock(optionSelected.attr('id'), deleteLink, 'lien_suppression');
        COMMONS_KTAG.toggleBlock(optionSelected.attr('id'), validateLink, 'lien_validation');
        COMMONS_MESSAGES.postMessageToParent('erase');
    }

    function handleLabel(params) {
        if (!params.LIBELLE && params.ACTION === 'AJOUTER') {
            params.LIBELLE = 'OBJET';
        }
    }

    // Generate a object containing every association <name> = <value> found on the current screen.
    function prepareParams($content) {
        var params = {};
        COMMONS_KTAG.handleGenericInputs($content, params);
        COMMONS_KTAG.handleCheckBoxes($content, params);
        handleLabel(params);
        return params;
    }

    function buildAndSendMessage() {
        var params = prepareParams($contribInputs),
            tagValue = COMMONS_KTAG.buildRequest(params),
            object = $('select[name="OBJET"]').val(),
            $form = $('#lien_fiches_action').closest('form');
        if (object && object !== '0000') {
            tagValue = '[traitement;lien_fiches;objet=' + object + '#' + tagValue + ']';
        } else if (params.ACTION !== 'AJOUTER') {
            tagValue = '[traitement;lien_fiches;' + tagValue + ']';
        } else {
            tagValue = '';
        }
        if (tagValue) {
            var message = { 'tag': 'kcontrib', 'value': tagValue, '$form': $form };
            COMMONS_MESSAGES.postMessageToParent(message);
        } else {
            COMMONS_MESSAGES.postMessageToParent('erase');
        }
    }

    $('#lien_fiches_action').on('change', contribChange);
    $contribInputs.on('change keyup', buildAndSendMessage);

    // Modification mode
    if ($('.js-tabs').tabs().isPanelActive('kcontrib')) {
        contribChange();
        buildAndSendMessage();
    }

})(jQuery.noConflict());
