(function($) {
    'use strict';
    var $tabs = $('.js-tabs'),
        listeFicheListe = document.getElementById('liste_fiches_liste'),
        listeFicheTableau = document.getElementById('liste_fiches_tableau'),
        $allInputs = $('input[type!="hidden"], select, textarea, .kmonoselect > input[id], .kmultiselect-ttl > input[id], .kmultiselect-ltl > input[id]', $('#js-liste_fiches')),
        $dashboardInputs = $('input[type!="hidden"], select, textarea, .kmonoselect > input[id], .kmultiselect-ttl > input[id], .kmultiselect-ltl > input[id]', $('#js-main-tagValues')),
        $listInputs = $('input[type!="hidden"], select', $('#liste_fiches_liste')),
        $displayType = $('#liste_fiches_affichage');

    $displayType.on('change', function() {
        if (this.value === 'liste_fiches_listetableau') {
            listeFicheListe.className = listeFicheListe.className.replace(/masquer/g, '');
            listeFicheTableau.className = listeFicheTableau.className.replace(/masquer/g, '');
        } else if (this.value === listeFicheListe.id) {
            listeFicheListe.className = listeFicheListe.className.replace(/masquer/g, '');
            listeFicheTableau.className += listeFicheTableau.className + ' masquer';
        } else {
            listeFicheListe.className += listeFicheListe.className + ' masquer';
            listeFicheTableau.className = listeFicheTableau.className.replace(/masquer/g, '');
        }
        COMMONS_MESSAGES.postMessageToParent('erase');
    });

    function setParamsForTableInputs(item, params) {
        var $current = $(item),
            associatedField = $current.data('associated-field');
        var valueToAdd = '|0';
        if (item.checked) {
            valueToAdd = '|1';
        }
        params[associatedField] = params[associatedField] ? params[associatedField] + valueToAdd : valueToAdd;
        if (params[associatedField] === '|1|1') {
            delete params[associatedField];
        }
    }

    function handleFicheParam(params) {
        var $tableReqInputs = $('input[data-associated-field][name^="liste_fiches_req_"]');
        $tableReqInputs.each(function(i, item) {
            setParamsForTableInputs(item, params);
        });
        var $tableOrderInputs = $('input[data-associated-field][name^="liste_fiches_tri_"]');
        $tableOrderInputs.each(function(i, item) {
            setParamsForTableInputs(item, params);
        });
    }

    function handleAction(params) {
        var action = $('input[name="ACTION"]:checked').map(function() {
            return this.value;
        }).get();
        if (action.length > 0) {
            params.ACTION = action.join('|');
        }
    }

    function handleTableInputs(params) {
        COMMONS_KTAG.handleGenericInputs($('input[type="text"]', $('#liste_fiches_tableau')), params);
        var page = $('input[name="PAGE"]:not(:checked)').val();
        if (page) {
            params.PAGE = page;
        }
        var ajout = $('input[name="AJOUT"]:not(:checked)').val();
        if (ajout) {
            params.AJOUT = ajout;
        }
        handleAction(params);
        handleFicheParam(params);
    }

    function handleSpecificInputs(params) {
        params.TAB = '1';
        params.LISTE = '1';
        if (params.OBJET) {
            params.OBJET = params.OBJET.replace(/;/g, ',');
        }
        if ($displayType.val() === listeFicheListe.id) {
            params.TAB = '0';
            COMMONS_KTAG.handleGenericInputs($listInputs, params);
        } else if ($displayType.val() === listeFicheTableau.id) {
            params.LISTE = '0';
            handleTableInputs(params);
        } else {
            COMMONS_KTAG.handleGenericInputs($listInputs, params);
            handleTableInputs(params);
        }
    }

    function buildAndSendMessage() {
        var params = {};
        COMMONS_KTAG.handleGenericInputs($dashboardInputs, params);
        COMMONS_KTAG.handleCheckBoxes($dashboardInputs, params);
        handleSpecificInputs(params);
        var tagValue = COMMONS_KTAG.buildRequest(params);
        if (tagValue) {
            var message = { 'tag': 'kdashboard', 'value': '[traitement;liste_fiches;' + tagValue + ']'};
            COMMONS_MESSAGES.postMessageToParent(message);
        }
    }

    function onTabChange() {
        if ($tabs.tabs().isPanelActive('kdashboard')) {
            buildAndSendMessage();
        }
    }

    $allInputs.on('change keyup', buildAndSendMessage);
    $tabs.on('TabChange', onTabChange);
    // Modification mode
    onTabChange();
})(jQuery.noConflict());
