(function ($) {
    'use strict';
    var $layouts = $('.js-layout-manager');
    if ($layouts.length > 0) {
        $layouts.each(function () {
            $(this).layoutManager({});
        });
    }

    $('[name="ID_LAYOUT"]').change(function (e) {
        var $selector = $(this);
        e.stopImmediatePropagation();
        $('<div>')
            .html($selector.attr('data-confirm'))
            .dialog({
                modal: true,
                close: function() {
                    $selector.val($('[selected]', $selector).val());
                },
                buttons: [{
                    text: LOCALE_BO.layout.confirm['continue'],
                    click: function () {
                        var $saisieObjet = $('#saisie_objet');
                        $saisieObjet.validate().cancelSubmit = true;
                        $saisieObjet.find('input[name=ACTION]').val('CHANGER_LAYOUT');
                        $saisieObjet.submit();
                    }
                }, {
                    text: LOCALE_BO.layout.confirm.cancel,
                    click: function () {
                        $(this).dialog('close');
                    }
                }],
                title: LOCALE_BO.layout.confirm.title
            });
    });

})(jQuery);
