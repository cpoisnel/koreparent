/*
 * Specific backoffice scripts wich handles "actions" and "haut_pages" behaviours
 */

(function ($) {
    'use strict';
    var $actions = $('#actions'),
        isHeadPageVisible = false,                                // Is the header frame visible
        isStickyActive = false,                                    // Is sticky enabled and expanded
        isStickyEnabled = false,                                // Is sticky enabled
        criticalPoint;

    // Handle pin states
    function handleStickyPin() {
        var $epingle = $('#epingle');
        if (isStickyActive) {
            $('#actions').transition({'top': '-4.91667em'});
            $epingle.transition({'padding-top': '2em', 'bottom': '-2em'});
            $epingle.attr('title', 'Ouvrir');
            $epingle.html('+');
            isStickyActive = false;
        } else {
            $('#actions').transition({'top': '0'});
            $epingle.transition({'padding-top': '0', 'bottom': '-1.5em'});
            $epingle.attr('title', 'Fermer');
            $epingle.html('&ndash;');
            isStickyActive = true;
        }
    }

    // Determine "actions" behaviour according to the "window.scrollTop" value
    function handleActions() {
        var $window = $(window);

        if ($window.scrollTop() >= criticalPoint && !isStickyEnabled) {
            var $dummy = $('<div>').attr('id', 'actions_dummy').css({
                'height': $actions.outerHeight(true),
                'width': $actions.outerWidth(true),
                'background': 'transparent'
            });

            // Creates a dummy element wich will act as a placeholder for #actions (avoid positionning bugs)
            $actions.before($dummy);
            $actions.addClass('sticky');
            $('#epingle').css({'display': 'inline-block', 'opacity': 1});
            isStickyEnabled = true;
            isStickyActive = true;
        } else {
            if ($window.scrollTop() >= 0 && $window.scrollTop() < criticalPoint && isStickyEnabled) {
                $('#actions_dummy').remove();
                $('#actions').removeClass('sticky');
                $('#epingle').css({'display': 'none', 'opacity': 0});
                isStickyEnabled = false;
                isStickyActive = false;
                handleStickyPin();
            }
        }
    }

    // Determine if "haut_page" should be visible according to the "window.scrollTop" value
    function handleButton() {
        var $window = $(window),
            $hautPage = $('#haut_page'),
            criticalPointHead = $('#header').outerHeight(true) + $('#entete').outerHeight(true);

        if ($window.scrollTop() > criticalPointHead) {
            if (!isHeadPageVisible) {
                $hautPage.transition({y: '-=100', opacity: '1'});
                isHeadPageVisible = true;
            }
        }

        if ($window.scrollTop() > 0 && $window.scrollTop() < criticalPointHead) {
            if (isHeadPageVisible) {
                $hautPage.transition({y: '+=100', opacity: '0'});
                isHeadPageVisible = false;
            }
        }
    }

    // Triggered when the window is scrolled
    function onWindowScroll() {
        handleButton();
        if ($actions.length > 0 && $actions.children().length > 0) {
            handleActions();
        }
    }

    $(window).load(function () {
        $(window).scroll(onWindowScroll);

        if ($actions.length > 0) {
            criticalPoint = $('#actions').position().top;    // Determine the point beyond wich the action bar should get 'sticky'
            handleActions();
        }
        // Specific values for initialization
        $('#haut_page').transition({y: '+=100', opacity: '0'}, function () {
            handleButton();
        });
    });

    // Triggered when 'haut_page' button is clicked
    $('#haut_page').click(function () {
        $(this).transition({y: '+=100', opacity: '0'});
        isHeadPageVisible = false;
        handleActions();
    });

    // Triggered when 'epingle' button is clicked
    $('#epingle').click(function () {
        handleStickyPin();
    });

})(jQuery.noConflict());
