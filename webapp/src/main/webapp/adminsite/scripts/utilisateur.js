(function ($) {
    // Ne sera déclenché que si l'écran d'affectation des rôles est utilisé
    if (window.arrayRoles) {

        function soumettreAjoutAffectation(i) {
            var $form = $(window.document.forms[0]);

            $form.find('input[name="ACTION"]').val('AJOUTER_AFFECTATION');
            $form.submit();
        }

        function soumettreSuppressionAffectation(i) {
            var $form = $(window.document.forms[0]);
            $form.find('input[name="ACTION"]').val('SUPPRIMER_AFFECTATION#' + i);
            $form.submit();
        }

        function affichePerimetre() {
            var $structure = $('#kMonoSelectSTRUCTURE_AFFECTATION'),
                $rubrique = $('#kMonoSelectRUBRIQUE_AFFECTATION'),
                $groupe = $('#kMonoSelectGROUPE_AFFECTATION'),
                codeRole = $('#ROLE_AFFECTATION').val();
            $rubrique.kMonoSelect().clear();
            $structure.kMonoSelect().clear();
            $groupe.kMonoSelect().clear();
            var $perimetreRubrique = $('#perimetreRubrique'),
                $perimetreStructure = $('#perimetreStructure'),
                $perimetreGroupe = $('#perimetreGroupe');
            if (codeRole == '0000') {
                $perimetreRubrique.css({display: 'none'});
                $perimetreStructure.css({display: 'none'});
                $perimetreGroupe.css({display: 'none'});
                return;
            }
            for (i = 0; i < arrayRoles.length; i++) {
                var role = arrayRoles[i];
                if (role.code == codeRole) {
                    if ($perimetreStructure.length > 0) {
                        if (role.structure == '1') {
                            $perimetreStructure.css({display: ''});
                            $('#messagePerimetreStructure').css({display: ''});
                        } else {
                            $perimetreStructure.css({display: 'none'});
                            $('#messagePerimetreStructure').css({display: 'none'});
                        }
                    }
                    if ($perimetreRubrique.length > 0) {
                        if (role.rubrique == '1') {
                            $perimetreRubrique.css({display: ''});
                            $('#messagePerimetreStructure').css({display: ''});
                        } else {
                            $perimetreRubrique.css({display: 'none'});
                            $('#messagePerimetreStructure').css({display: 'none'});
                        }
                    }
                    if ($perimetreGroupe.length > 0) {
                        if (role.groupe == '1') {
                            $perimetreGroupe.css({display: ''});
                            $('#messagePerimetreGroupe').css({display: ''});
                        } else {
                            $perimetreGroupe.css({display: 'none'});
                            $('#messagePerimetreGroupe').css({display: 'none'});
                        }
                    }
                }
            }
        }

        // Gestion des évènements
        $('#AJOUTER_AFFECTATION').click(function () {
            soumettreAjoutAffectation();
            return false;
        });
        $('#ROLE_AFFECTATION').change(function () {
            affichePerimetre();
        });
        $('#divAutorisations').on('click', '.suppression_utilisateur', function(){
            var $this = $(this),
                index = $.parseInteger($this.data('index'));
            soumettreSuppressionAffectation(index);
        });
        // Affichage au chargement de la page
        affichePerimetre();
    }
})(jQuery.noConflict());
