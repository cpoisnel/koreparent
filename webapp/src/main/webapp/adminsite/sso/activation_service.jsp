<%@page import="java.net.URLDecoder"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.univ.objetspartages.om.ServiceBean"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@ page import="com.univ.utils.ContexteUtil, com.univ.utils.ServicesUtil, com.univ.utils.URLResolver "%>

<%
    // utilisée pour rediriger vers un service k-portal en générant un ticket kportal
    ContexteUniv  ctx = ContexteUtil.getContexteUniv();
    ctx.setJsp(this);

    StringBuilder urlCible = new StringBuilder();
    String reqParam = "";
    String service = request.getParameter("service");

    if (service == null || service.length() == 0){
        if (request.getParameter("ksite")!=null){
            reqParam = URLDecoder.decode(request.getParameter("ksite"), "UTF-8");
            if (ServicesUtil.validerSite(reqParam)){
                urlCible = new StringBuilder(reqParam);
                if (ctx.getKsession().length()>0){
                    urlCible.append(( (urlCible.indexOf("?") == -1) ? "?" : "&" ));
                    urlCible.append("kticket=").append(ServicesUtil.genererTicketService(ctx));
                }
            }
        }
        else if (request.getParameter("kurl")!=null){
            reqParam = URLDecoder.decode(request.getParameter("kurl"), "UTF-8");
            if (ServicesUtil.validerUrl(reqParam)){
                urlCible = new StringBuilder(reqParam);
                if (ctx.getKsession().length()>0){
                    urlCible.append(( (urlCible.indexOf("?") == -1) ? "?" : "&" ));
                    urlCible.append("kticket=").append(ServicesUtil.genererTicketService(ctx)).append("&ksite=").append(URLResolver.getAbsoluteUrl("/",ctx.getInfosSite(),1));
                }
            }
        }

    }else{

        ServiceBean serviceBean = ServicesUtil.getService(service);
        if (!ctx.getListeServicesAutorises().contains(service) && StringUtils.isNotBlank(serviceBean.getCode())) {
            pageContext.getServletContext().getRequestDispatcher(ctx.getInfosSite().getJspFo() + "/error/403.jsp").forward(request, response);
            return;
        }

        urlCible = new StringBuilder(ServicesUtil.determinerUrlServiceTypeUrl(ctx, serviceBean));
        Map<String,String[]> parametreRequete = request.getParameterMap();
        for (Entry<String,String[]> param : parametreRequete.entrySet()) {
            if (!"service".equals(param.getKey())) {
                for (String valeurAttribut : param.getValue()) {
                    urlCible.append((urlCible.indexOf("?")!=-1)?"&":"?");
                    urlCible.append(param.getKey()).append("=").append(valeurAttribut);
                }
            }
        }
    }
    if (urlCible != null && urlCible.length()>0){
        response.sendRedirect(urlCible.toString());
    }else{
        pageContext.getServletContext().getRequestDispatcher(ctx.getInfosSite().getJspFo() + "/error/404.jsp").forward(request, response);
    }

%>
