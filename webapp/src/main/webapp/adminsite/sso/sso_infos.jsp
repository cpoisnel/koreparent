<%@ page import="com.univ.utils.ContexteUniv, com.univ.xml.TraitementExportTempsReel" %><%
    out.clear();
    ContexteUniv  ctx = new ContexteUniv ( request.getRequestURI());
    response.setContentType( "text/xml" );
    try {
        TraitementExportTempsReel.exportSSO(out, request.getQueryString());
    }
    finally    {
        ctx.release();
    }
%>