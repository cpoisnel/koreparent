<%@ page import="java.net.URLDecoder, com.jsbsoft.jtf.exception.ErreurApplicative, com.univ.utils.ContexteUniv, com.univ.utils.ServicesUtil"%>


<%
  // utilisée pour rediriger vers uue url
  // en générant un ticket SSO kportal


  ContexteUniv  ctx = new ContexteUniv (pageContext);
  String urlCible = "";


  try    {

  String url = request.getParameter( "url" ) ;
  if(( url == null )||(url.length() == 0)) 
    throw new ErreurApplicative("url incorrecte");

  urlCible = URLDecoder.decode( url, "UTF-8" ) ;

 
  ctx.setJsp(this);

  urlCible += (urlCible.indexOf("?") == -1 ? "?" : "&");
  urlCible += "kticket=" + ServicesUtil.genererTicketService( ctx);


  } finally    {
  ctx.release();
  }

  response.sendRedirect( urlCible);

%>
