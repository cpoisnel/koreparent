<%@page import="java.util.Collection"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.univ.multisites.InfosSite"%>
<%@page import="com.univ.url.UrlManager"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@ page import="com.univ.utils.URLResolver" %>
<%@ page import="com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus" %>
<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<%@ page import="com.univ.multisites.bean.impl.InfosSiteImpl" %>
<%@ page import="org.apache.commons.collections.CollectionUtils" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<%
    final ContexteUniv contexte = ContexteUtil.getContexteUniv();
    final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
    final Collection<InfosSite> sites = serviceInfosSite.getSitesList().values();
// propagation de la session sur chaque site après authentification
%>
<%if (StringUtils.isNotBlank(contexte.getKsession()) && "VALIDER".equals(infoBean.getActionUtilisateur()) || StringUtils.isNotEmpty(request.getParameter("ksession"))){ %>
<script type="text/javascript">
    //<![CDATA[
        <%
        if (CollectionUtils.isNotEmpty(sites)) {
            for (InfosSite infosSite : sites) {
                if (infosSite.isSso()) { %>
                    var script = document.createElement('script');
                    script.src = '<%=URLResolver.getAbsoluteBoUrl(UrlManager.calculerUrlForward(contexte,""),infosSite)%>';
                    script.id = 'connexion_<%=infosSite.getAlias()%>';
                    script.type = 'text/javascript';
                    document.body.appendChild(script);
                <%}
               }
           }%>
    //]]>
</script>
<%
}
%>