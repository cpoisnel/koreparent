<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.TreeMap"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.InfoBean"%>
<%@page import="com.jsbsoft.jtf.core.LangueUtil"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.ihm.utils.AdminsiteUtils"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<%@page import="com.univ.utils.FicheUnivHelper"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
String typeObjet = ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")).toUpperCase();
Objetpartage module = ReferentielObjets.getObjetByNom(typeObjet);
Map<String, String> classParEtat = FicheUnivHelper.getClassEtatsObjet();
String codeEtat = StringUtils.defaultString(infoBean.getString("ETAT_OBJET"));
%><div id="actions">
    <div>
        <div class="resume">
            <!-- Etat fiche et langue --><%
            Hashtable<String, String> etats = ((Hashtable<String,String>)infoBean.get("AUTRES_ETATS"));
            if(etats != null) {
                %><div class="dropdown">
                    <strong class="dropdown-toggle button" data-toggle="dropdown"><span class="etat"><span class="<%= StringUtils.defaultString(classParEtat.get(codeEtat))%>">&nbsp;</span><%= ReferentielObjets.getLibelleEtatObjet(codeEtat) %></span></strong>
                    <div class="dropdown-menu">
                        <ul>
                            <li><strong><%=MessageHelper.getCoreMessage("BO_AUTRES_ETATS") %></strong>
                                <ul><%
                                for(String key : etats.keySet()){
                                    String[][] parametresURL = new String[][] { { "ID_FICHE", etats.get(key) } };
                                    String urlFiche = ProcessusHelper.getUrlProcessAction(infoBean, module.getExtension().getId(), infoBean.getNomProcessus(), "MODIFIER", parametresURL);
                                    %><li><strong><span class="etat"><span class="<%= FicheUnivHelper.getClassEtat(key)%>">&nbsp;</span><a href="<%=urlFiche%>"><%= ReferentielObjets.getLibelleEtatObjet(key) %></a></span></strong></li><%
                                }
                                %></ul>
                            </li>
                        </ul>
                    </div><!-- .dropdown-menu -->
                </div><%
            } else {
                %><div class="etat"><span class="<%= StringUtils.defaultString(classParEtat.get(codeEtat))%>">&nbsp;</span> <strong><%= ReferentielObjets.getLibelleEtatObjet(codeEtat) %></strong></div><!-- #etat --><%
            }
            if (AdminsiteUtils.existeAutreLangue(infoBean)) {
                %><div class="dropdown">
                    <strong class="dropdown-toggle button" data-toggle="dropdown"><% univFmt.insererDrapeauLangue( out, infoBean.getString("LANGUE")); %><%= LangueUtil.getDisplayName(infoBean.getString("LANGUE")) %></strong>
                    <div class="dropdown-menu">
                        <ul><%
                            if (infoBean.get("FICHES_AUTRE_LANGUE")!=null) {
                                %><li><strong><%=MessageHelper.getCoreMessage("BO_AUTRES_LANGUES") %></strong>
                                    <ul><%
                                    TreeMap<String,String> versionTraduite = (TreeMap<String,String>)infoBean.get("FICHES_AUTRE_LANGUE");
                                    for (String langue : versionTraduite.keySet()) {
                                        %><li><% univFmt.insererDrapeauLangue( out, langue);%> <a href="<%=versionTraduite.get(langue)%>" title="<%= MessageHelper.getCoreMessage("BO_FICHE_VOIR_FICHE")%>"><%= LangueUtil.getDisplayName(langue) %></a></li><%
                                    }
                                    %></ul>
                                </li><%
                            }
                            final Hashtable<String, String> listeAutreLangue = (Hashtable<String, String>) infoBean.get("LISTE_AUTRES_LANGUES");
                             if (!infoBean.get("ETAT_OBJET").equals("0006") && "1".equals(infoBean.getString("GRS_AUTORISATION_TRADUCTION")) && !listeAutreLangue.isEmpty()) {
                                %><li><strong><%= MessageHelper.getCoreMessage("BO_TRADUIRE_EN") %></strong>
                                    <ul><%
                                    for (String cle : listeAutreLangue.keySet()) {
                                        %><li>
                                            <button type="button" data-action="TRADUIRE" data-confirm="<%=MessageHelper.getCoreMessage("BO_ENTETE_SAISIE_TRADUIRE_MSG_CONFIRMATION")%>" name="AUTRE_LANGUE" id="AUTRE_LANGUE" value="<%=cle %>" ><% univFmt.insererDrapeauLangue( out, cle); %> <%=listeAutreLangue.get(cle) %></button>
                                        </li><%
                                    }
                                    %></ul>
                                </li><%
                             }
                            %>
                        </ul>
                    </div><!-- .dropdown-menu -->
                </div><!-- .dropdown --><%
            } else if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {
                %><strong><% univFmt.insererDrapeauLangue( out, infoBean.getString("LANGUE")); %><%= LangueUtil.getDisplayName(infoBean.getString("LANGUE")) %></strong><%
            }%>
        </div><!-- .resume -->
    <ul><%
    if (AdminsiteUtils.isSuppressionPossible(infoBean)) { // tlm peut supprimer une sauvegarde auto
        %><li><button type="button" class="supprimer" data-confirm="<%=MessageHelper.getCoreMessage("BO_CONFIRM_SUPPR_FICHE") %>" data-action="SUPPRIMER"><%= module.getMessage("BO_FICHE_SUPPRIMER") %></button></li><%
    }
    if (AdminsiteUtils.isArchivagePossible(infoBean)) {
        %><li><button type="button" class="archiver" data-action="ARCHIVER"><%= MessageHelper.getCoreMessage("BO_FICHE_ARCHIVER") %></button></li><%
    }
    if (AdminsiteUtils.isAnnulerSuppressionPossible(infoBean)) {
        %><li>
            <div class="dropdown button restaurer">
                <strong class="dropdown-toggle button" data-toggle="dropdown"><%= MessageHelper.getCoreMessage("BO_FICHE_RESTAURER") %></strong>
                <div class="dropdown-menu">
                    <ul>
                        <li>
                            <button type="button" data-action="ANNULER_SUPPRESSION" name="RESTAURATION" value="REMPLACER_BROUILLON"><%=MessageHelper.getCoreMessage("BO_REMPLACER_BROUILLON") %></button>
                        </li>
                        <li>
                            <button type="button" data-action="ANNULER_SUPPRESSION" name="RESTAURATION" value="NOUVELLE_FICHE"><%=MessageHelper.getCoreMessage("BO_CREER_NOUVELLE_FICHE") %></button>
                        </li>
                    </ul>
                </div><!-- .dropdown-menu -->
            </div><!-- .dropdown button .restaurer -->
        </li><%
    } else if (AdminsiteUtils.isAnnulerSuppressionBrouillonPossible(infoBean)) {
        %><li><button type="button" class="restaurer" data-action="ANNULER_SUPPRESSION"><%= MessageHelper.getCoreMessage("BO_FICHE_RESTAURER") %></button>
            <input type="hidden" name="RESTAURATION" value="NOUVEAU_BROUILLON" />
        </li><%
    } else if (AdminsiteUtils.isRestaurerSauvegardePossible(infoBean)) {
                %><li>
                    <div class="dropdown button restaurer">
                        <strong class="dropdown-toggle button" data-toggle="dropdown"><%= MessageHelper.getCoreMessage("BO_FICHE_RESTAURER") %></strong>
                        <div class="dropdown-menu">
                            <ul>
                                <li>
                                    <button type="button" data-action="RESTAURER_SAUVEGARDE" name="RESTAURATION" value="REMPLACER_BROUILLON"><%=MessageHelper.getCoreMessage("BO_REMPLACER_BROUILLON") %></button>
                                </li>
                                <li>
                                    <button type="button" data-action="RESTAURER_SAUVEGARDE" name="RESTAURATION" value="NOUVELLE_FICHE"><%=MessageHelper.getCoreMessage("BO_CREER_NOUVELLE_FICHE") %></button>
                                </li>
                            </ul>
                        </div><!-- .dropdown-menu -->
                    </div><!-- .dropdown-toggle .button -->
                </li><%
    } else if (AdminsiteUtils.isRestaurerSauvegardeBrouillonPossible(infoBean)) {
        %><li><button type="button" class="restaurer" data-action="RESTAURER_SAUVEGARDE"><%= MessageHelper.getCoreMessage("BO_FICHE_RESTAURER") %></button>
            <input type="hidden" name="RESTAURATION" value="NOUVEAU_BROUILLON" />
        </li><%
    } else if (AdminsiteUtils.isRestaurerArchivePossible(infoBean)) {
        %><li>
            <div class="dropdown button restaurer">
                <strong class="dropdown-toggle button" data-toggle="dropdown"><%=module.getMessage("BO_FICHE_RESTAURER") %></strong>
                <div class="dropdown-menu">
                    <ul>
                        <li>
                            <button type="button" data-action="RESTAURER_ARCHIVE" name="RESTAURATION" value="REMPLACER_BROUILLON"><%=MessageHelper.getCoreMessage("BO_REMPLACER_BROUILLON") %></button>
                        </li>
                        <li>
                            <button type="button" data-action="RESTAURER_ARCHIVE" name="RESTAURATION" value="NOUVELLE_FICHE"><%=MessageHelper.getCoreMessage("BO_CREER_NOUVELLE_FICHE") %></button>
                        </li>
                    </ul>
                </div><!-- .dropdown-menu -->
            </div><!-- .dropdown-toggle .button -->
        </li><%
    } if (AdminsiteUtils.isRestaurerArchiveBrouillonPossible(infoBean)) {
        %><li><button type="button" class="restaurer" data-action="RESTAURER_ARCHIVE"><%= MessageHelper.getCoreMessage("BO_FICHE_RESTAURER_ARCHIVE") %></button></li>
        <li><input type="hidden" name="RESTAURATION" value="NOUVEAU_BROUILLON" /></li><%
    }
    if (AdminsiteUtils.isDuplicationPossible(infoBean)) {
        %><li><button type="button" class="dupliquer" data-action="DUPLIQUER"><%= MessageHelper.getCoreMessage("BO_FICHE_DUPLIQUER") %></button></li><%
    }
    if (AdminsiteUtils.isPublicationPossible(infoBean)) {
        %><li>
            <button class="brouillon" type="button" data-action="ENREGISTRER" name="MODE_BROUILLON" value="1"><%= MessageHelper.getCoreMessage("BO_FICHE_ENREGISTRER_BROUILLON") %></button>
        </li>
        <li>
            <button class="publier" type="button" data-action="ENREGISTRER" name="MODE_BROUILLON" value="0"><%= MessageHelper.getCoreMessage("BO_FICHE_PUBLIER")%></button>
        </li><%
    }
    if (AdminsiteUtils.isBrouillonPossible(infoBean)) {
        %><li><button class="brouillon" type="button" data-action="ENREGISTRER" name="MODE_BROUILLON" value="1"><%
            if("1".equals(infoBean.get("AFFICHER_REMPLACER_VERSION"))) {
                %><%= MessageHelper.getCoreMessage("BO_FICHE_REMPLACER_BROUILLON") %><%
            } else {
                %><%= MessageHelper.getCoreMessage("BO_FICHE_ENREGISTRER_BROUILLON") %><%
            }
            %></button>
        </li><%
    }
    if (AdminsiteUtils.isEnregistrerPossible(infoBean)) {
        %><li>
            <button class="enregistrer" type="button" data-action="ENREGISTRER" name="MODE_BROUILLON" value="0"><%= MessageHelper.getCoreMessage("BO_ENREGISTRER") %></button>
        </li><%
    }
    if (AdminsiteUtils.isValidationPossible(infoBean)) {
        %><li><button type="button" class="valider" name="TYPE_ENREGISTREMENT" value="2" data-action="ENREGISTRER"><%= MessageHelper.getCoreMessage("BO_FICHE_ACCEPTER_METTRE_EN_LIGNE") %></button></li>
        <li><button type="button" class="refuser js-refuser"><%= module.getMessage("BO_FICHE_REFUSER") %></button>
            <div id="dialogRefuser" title="<%= MessageHelper.getCoreMessage("BO_FICHE_REFUSER_PUBLICATION") %>">
                <label for="MOTIF_RETOUR" ><%= MessageHelper.getCoreMessage("BO_FICHE_MOTIF_REFUS") %></label>
                <textarea form="saisie_objet" name="MOTIF_RETOUR" id="MOTIF_RETOUR" cols="35" rows="5"></textarea>
            </div>
        </li><%
    }
    if (AdminsiteUtils.isAnnulerDemandeValidationPossible(infoBean)) {
        %><li><button type="button" class="refuser" data-action="ENREGISTRER" name="ANNULER_DEMANDE_VALIDATION" id="ANNULER_DEMANDE_VALIDATION" value="1"><%= MessageHelper.getCoreMessage("BO_FICHE_ANNULER_DEMANDE_VALIDATION") %></button></li><%
    }
    if (AdminsiteUtils.isApercuPossible(infoBean)) {
        %><li><button type="button" class="apercu" data-action="APERCU"><%= MessageHelper.getCoreMessage("BO_PREVISUALISER") %></button></li><%
    }
    %></ul>
    <div class="clearfix"></div>
    <span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
    </div><!-- . -->
</div><!-- #actions -->