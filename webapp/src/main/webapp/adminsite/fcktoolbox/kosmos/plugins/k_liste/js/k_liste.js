var dialog = window.parent;
var oEditor = dialog.InnerDialogLoaded();

var FCK = oEditor.FCK;
var FCKLang = oEditor.FCKLang;
var FCKConfig = oEditor.FCKConfig;
var FCKRegexLib = oEditor.FCKRegexLib;
var FCKTools = oEditor.FCKTools;

/** ICI LE CODE POUR GERER LES FAKE TAGS **/
var oFakeImage = dialog.Selection.GetSelectedElement();
var oSpanListe;

if (oFakeImage) {
    if (oFakeImage.tagName == 'IMG' && oFakeImage.getAttribute('_kliste'))
        oSpanListe = FCK.GetRealElement(oFakeImage);
    else
        oFakeImage = null;
}

/** FIN DU CODE POUR GERER LES FAKE TAGS**/

function Ok() {
    typeInsertion = 'liste';
    sauvegarderRequete();
    return true;
}

function insertKListe_lienRequete(objet, requete, libelle) {
    var txtHtml = "[traitement;requete;" + conversionRequete(objet, requete) + "]";

    oEditor.FCKUndo.SaveUndoStep();
    if (!oSpanListe) {
        oSpanListe = FCK.EditorDocument.createElement('SPAN');
        oFakeImage = null;
    }

    oSpanListe.innerHTML = txtHtml;
    oSpanListe.setAttribute('kliste_span', 'true');

    if (!oFakeImage) {
        oFakeImage = oEditor.FCKDocumentProcessor_CreateFakeImage('K_LISTE', oSpanListe);
        oFakeImage.setAttribute('_kliste', 'true', 0);
        oFakeImage.setAttribute('src', FCKConfig.PluginsPath + 'k_liste/images/list.png');
        oFakeImage.setAttribute('title', txtHtml);

        oFakeImage = FCK.InsertElement(oFakeImage);
    }

    oFakeImage.setAttribute('title', txtHtml);
}

/* Remplacement des & par des # (plantage NetWord) */
function conversionRequete(objet, code) {

    do {
        i = code.indexOf('&');
        if (i != -1) {
            code = code.substring(0, i) + "#" + code.substring(i + 1, code.length);
        }
    }
    while (i != -1);

    param = "objet=" + objet;
    if (code.length > 0)
        param = param + "#" + code;

    return param;
}