<%@ page import="java.util.Hashtable" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.univ.collaboratif.om.Espacecollaboratif" %>
<%@ page import="com.univ.collaboratif.om.InfosEspaceCollaboratif" %>
<%@ page import="com.univ.objetspartages.util.LabelUtils" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<div id="tag_collab" class="fieldset neutre" style="display:none">

	<p>
		<label for="choix_tag_collab" class="colonne">Type d'insertion</label>
		<select name="choix_tag_collab" id="choix_tag_collab" onchange="afficheComposants(this.name);saveChoixTag(this.value)">
			<option value="">Indéfini(e)</option>
			<option value="liste_espaces">Liste d'espaces (liée à l'utilisateur)</option>
			<option value="intitule_espace">Intitulé de l'espace courant</option>
		</select>
	</p>

	<div id="liste_espaces" style="display:none">
		<p><span class="label colonne">Mode de sélection (*)</span></p>
		<div>
			<strong>Sélectionner les espaces pour lesquels</strong>
			<ul>
				<li><input type="checkbox" name="liste_espaces_mode_selection_1" id="liste_espaces_mode_selection_1" value="1" /> <label for="liste_espaces_mode_selection_1">l'utilisateur est membre</label></li>
				<li><input type="checkbox" name="liste_espaces_mode_selection_2" id="liste_espaces_mode_selection_2" value="2" /> <label for="liste_espaces_mode_selection_2">l'utilisateur est simple visiteur</label></li>
				<li><input type="checkbox" name="liste_espaces_mode_selection_3" id="liste_espaces_mode_selection_3" value="2" /> <label for="liste_espaces_mode_selection_3">l'utilisateur peut s'inscrire</label></li>
			</ul>
		</div>
		<br/>

		<p class="message information">Sélectionner la rubrique de rattachement et/ou le thème visé(s)</p>
		<%
		infoBean.set("liste_espaces_code_rubrique","");
		univFmt.insererkMonoSelect(fmt, out, infoBean, "liste_espaces_code_rubrique", FormateurJSP.SAISIE_FACULTATIF, "Rubrique", "rubrique", UnivFmt.CONTEXT_ZONE);
		%>
		<p>
			<input type="checkbox" id="liste_espaces_code_rubrique_courante" name="liste_espaces_code_rubrique_courante" value="1" />
			<label for="liste_espaces_code_rubrique_courante">ou rubrique courante</label>
		</p>
		<p>
			<label for="liste_espaces_code_theme" class="colonne">Thème</label>
			<%
			infoBean.set("LISTE_THEMES", LabelUtils.getLabelCombo("0110", ContexteUtil.getContexteUniv().getLocale()));
			fmt.insererComboHashtable(out, infoBean, "liste_espaces_code_theme", FormateurJSP.SAISIE_FACULTATIF, "LISTE_THEMES" );
			%>
		</p>
		<p class="message information">OU sélectionner directement les espaces dans la liste ci-dessous</p>
		<p>
			<span class="label colonne">Espaces</span>
			<%
				Hashtable<String,String> codesEspaces = new Hashtable<String, String>();
				for (final InfosEspaceCollaboratif infosEspace : Espacecollaboratif.getListeEspaces()) {
					codesEspaces.put(infosEspace.getCode(), infosEspace.getIntitule());
				}
				infoBean.set("LISTE_CODE_ESPACE", codesEspaces);
				infoBean.set("LIBELLE_liste_espaces_code_espace", "");%>
				<%univFmt.insererKmultiSelectLtl(fmt, out, infoBean, "liste_espaces_code_espace", FormateurJSP.SAISIE_FACULTATIF, "Liste des espaces", "LISTE_CODE_ESPACE", "", UnivFmt.CONTEXT_DEFAULT);%>
		</p>
	</div>

</div>
