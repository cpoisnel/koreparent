<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.univ.utils.EscapeString"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/adminsite/styles/screen.css" media="screen">
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
<title></title>
</head>
<body class="toolbox">
<% 
final String code = request.getParameter("CODE");
if(StringUtils.isNotBlank(code)) {
%>
	<script type="text/javascript">
	(function(){
		window.parent.CreateLienIntranet('<%=EscapeString.escapeJavaScript(request.getParameter("CODE"))%>', '<%=EscapeString.unescapeURL(request.getParameter("LIBELLE"))%>');
	})();
	</script>
<%} else { %>
	<div id="content">
		<ul>
			<li>
				<a href="/servlet/com.kportal.servlet.LienPopupServlet?CODE=logindsi&LIBELLE=Ecran%20de%20connexion&TOOLBOX=LIEN_INTRANET">Authentification</a>
			</li>
			<li>
				<a href="/servlet/com.kportal.servlet.LienPopupServlet?CODE=prefdsi&LIBELLE=Ecran%20des%20préférences&TOOLBOX=LIEN_INTRANET">Préférences</a>
			</li>
			<li>
				<a href="/servlet/com.kportal.servlet.LienPopupServlet?CODE=logoutdsi&LIBELLE=Ecran%20de%20déconnexion&TOOLBOX=LIEN_INTRANET">Déconnexion</a>
			</li>
		</ul>
	</div>
<%} %>
</body>
</html>
