<%@page import="com.univ.utils.EscapeString"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
<title></title>
</head>
<body>
<script type="text/javascript">
(function(){
	window.parent.CreateLienInterne('<%=EscapeString.escapeJavaScript(request.getParameter("OBJET"))%>', '<%=EscapeString.escapeJavaScript(request.getParameter("CODE"))%>',
			'<%=EscapeString.escapeJavaScript(request.getParameter("LIBELLE"))%>', '','<%=EscapeString.escapeJavaScript(request.getParameter("LANGUE"))%>');
})();
</script>
</body>
</html>
