FCKCommands.RegisterCommand('KPortalRss', new FCKDialogCommand('KPortalRss', FCKLang['DlgKPortalRssTitle'], FCKConfig.PluginsPath + 'k_rss/k_rss.jsp', 600, 600));

var oKPortalRss = new FCKToolbarButton('KPortalRss', FCKLang['KPortalRss']);

oKPortalRss.IconPath = FCKConfig.PluginsPath + 'k_rss/images/rss_icone.png';

FCKToolbarItems.RegisterItem('KPortalRss', oKPortalRss);

FCKDocumentProcessor.AppendNew().ProcessDocument = function (document) {
    var aSpans = document.getElementsByTagName('SPAN');

    var oSpan;
    var i = aSpans.length - 1;
    while (i >= 0 && ( oSpan = aSpans[i--] )) {
        if (oSpan.getAttribute('krss_span')) {
            var oImg = FCKDocumentProcessor_CreateFakeImage('K_RSS', oSpan.cloneNode(true));
            oImg.setAttribute('_krss', 'true', 0);
            oImg.setAttribute('src', FCKConfig.PluginsPath + 'k_rss/images/rss.png');
            oImg.setAttribute('title', oSpan.innerHTML);

            oSpan.parentNode.insertBefore(oImg, oSpan);
            oSpan.parentNode.removeChild(oSpan);
        }
    }
}

/*FCK.ContextMenu.RegisterListener( {
 AddItems : function( menu, tag, tagName )
 {
 if ( tagName == 'IMG' && tag.getAttribute( '_krss' ) )
 {
 menu.AddSeparator() ;
 menu.AddItem( 'KPortalRss', FCKLang.KRssProperties, FCKConfig.PluginsPath + 'k_rss/images/rss_icone.png' ) ;
 }
 }}
 );*/