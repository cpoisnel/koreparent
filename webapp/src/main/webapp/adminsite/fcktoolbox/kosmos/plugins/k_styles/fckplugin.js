// Register the commands.
FCKCommands.RegisterCommand('k_styles', new FCKDialogCommand('k_styles', FCKLang['DlgKStylesTitle'], FCKConfig.PluginsPath + 'k_styles/k_styles.html', 380, 450));

// Create the toolbar buttons.
var oKStyles = new FCKToolbarButton('k_styles', FCKLang['DlgKStylesTitle'], FCKLang['DlgKStylesTooltip'], FCK_TOOLBARITEM_ONLYTEXT);

// Register the toolbar items (these names will be used in the config file).
FCKToolbarItems.RegisterItem('KStyles', oKStyles);