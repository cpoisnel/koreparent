// Register the related commands.
dialogPath = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=VIDEO&FCK_PLUGIN=TRUE';
if (FCKConfig.CodeObjet && FCKConfig.Code && FCKConfig.IdFiche) {
    dialogPath = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=VIDEO&OBJET=' + FCKConfig.CodeObjet + '&CODE=' + FCKConfig.Code + '&ID_FICHE=' + FCKConfig.IdFiche + '&FCK_PLUGIN=TRUE';
}

flvPlayerDialogCmd = new FCKDialogCommand(FCKLang["DlgFLVPlayerTitle"], FCKLang["DlgFLVPlayerTitle"], dialogPath, 800, 600);
FCKCommands.RegisterCommand('flvPlayer', flvPlayerDialogCmd);

// Create the Flash toolbar button.
var oFlvPlayerItem = new FCKToolbarButton('flvPlayer', FCKLang["DlgFLVPlayerVideo"]);
oFlvPlayerItem.IconPath = '/adminsite/images/medias/video_icone.png';

FCKToolbarItems.RegisterItem('flvPlayer', oFlvPlayerItem);
// 'Flash' is the name used in the Toolbar config.

FCKDocumentProcessor.AppendNew().ProcessDocument = function (document) {
    var aDivs = document.querySelectorAll('div.media.video, div[flv_div="true"]'),
        i = aDivs.length - 1,
        oDiv;
    while (i >= 0 && ( oDiv = aDivs[i--] )) {
        var oImg = FCKDocumentProcessor_CreateFakeImage('FLV_VIDEO', oDiv.cloneNode(true));
        oImg.setAttribute('_flv_video', 'true', 0);
        oImg.setAttribute('src', '/adminsite/images/medias/video.png');

        oDiv.parentNode.insertBefore(oImg, oDiv);
        oDiv.parentNode.removeChild(oDiv);
    }
}

FCK.ContextMenu.RegisterListener({
        AddItems: function (menu, tag, tagName) {
            if (tagName == 'IMG' && tag.getAttribute('_flv_video')) {
                menu.AddSeparator();
                menu.AddItem('flvPlayer', FCKLang.DlgFLVPlayerProperties, '/adminsite/images/medias/video_icone.png');
            }
        }
    }
);