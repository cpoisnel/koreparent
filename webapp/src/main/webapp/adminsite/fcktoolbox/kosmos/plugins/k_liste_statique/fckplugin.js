FCKCommands.RegisterCommand('KPortalListeStatique', new FCKDialogCommand('KPortalListeStatique', FCKLang['DlgKPortalListTitle'], '/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_REQUETE&LISTE_INCLUSE=1&LISTE_STATIQUE=1&FCK_PLUGIN=TRUE', 600, 600));

var oKPortalList = new FCKToolbarButton('KPortalListeStatique', FCKLang['KPortalList']);

oKPortalList.IconPath = FCKConfig.PluginsPath + 'k_liste_statique/images/list_icone.png';

FCKToolbarItems.RegisterItem('KPortalListeStatique', oKPortalList);

