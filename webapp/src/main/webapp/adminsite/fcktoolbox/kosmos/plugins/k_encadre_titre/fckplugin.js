var EncadreTitre = function () {

}

EncadreTitre.prototype.Execute = function () {
    FCK.InsertHtml("[titre;Votre titre ici]");
}

EncadreTitre.prototype.GetState = function () {
    return FCK_TRISTATE_OFF;
}


// Register the commands.
FCKCommands.RegisterCommand('k_encadre_titre', new EncadreTitre());

// Create the toolbar buttons.
var oKEncaTitre = new FCKToolbarButton('k_encadre_titre', FCKLang['DlgKEncaTitreTitle'], FCKLang['DlgKEncaTitreTooltip'], FCK_TOOLBARITEM_ONLYTEXT);

// Register the toolbar items (these names will be used in the config file).
FCKToolbarItems.RegisterItem('KEncaTitre', oKEncaTitre);