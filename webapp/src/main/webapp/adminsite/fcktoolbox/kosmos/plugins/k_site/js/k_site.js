var dialog = window.parent;
var oEditor = dialog.InnerDialogLoaded();

var FCK = oEditor.FCK;
var FCKLang = oEditor.FCKLang;
var FCKConfig = oEditor.FCKConfig;
var FCKRegexLib = oEditor.FCKRegexLib;
var FCKTools = oEditor.FCKTools;

/** ICI LE CODE POUR GERER LES FAKE TAGS **/
var oFakeImage = dialog.Selection.GetSelectedElement();
var oSpanSite;

if (oFakeImage) {
    if (oFakeImage.tagName == 'IMG' && oFakeImage.getAttribute('_ksite'))
        oSpanSite = FCK.GetRealElement(oFakeImage);
    else
        oFakeImage = null;
}

//méthode appelée lors de la validation
function Ok() {
    sTag = 'plan_site';
    var sRubrique = eval('document.forms.site.plan_site_code_rubrique').value;
    if (sRubrique != '')
        sTag += ';CODE_RUBRIQUE=' + sRubrique;
    var sNbNiveaux = eval('document.forms.site.plan_site_nb_niveaux').value;
    if (sNbNiveaux != '')
        sTag += ';NB_NIVEAUX=' + sNbNiveaux;
    insertTag('[' + sTag + ']');
    return true;
}


function insertTag(sTag) {
    oEditor.FCKUndo.SaveUndoStep();
    if (!oSpanSite) {
        oSpanSite = FCK.EditorDocument.createElement('SPAN');
        oFakeImage = null;
    }

    oSpanSite.innerHTML = sTag;
    oSpanSite.setAttribute('ksite_span', 'true');

    if (!oFakeImage) {
        oFakeImage = oEditor.FCKDocumentProcessor_CreateFakeImage('K_SITE', oSpanSite);
        oFakeImage.setAttribute('_ksite', 'true', 0);
        oFakeImage.setAttribute('src', '/adminsite/fcktoolbox/kosmos/plugins/k_site/images/site.png');
        oFakeImage.setAttribute('title', sTag);

        oFakeImage = FCK.InsertElement(oFakeImage);
    }

    oFakeImage.setAttribute('title', sTag);
}

