// Register the commands.
dialogPath = FCKConfig.PluginsPath + 'k_link/fck_link.jsp?FCK_PLUGIN=TRUE';
if (FCKConfig.CodeObjet && FCKConfig.Code && FCKConfig.IdFiche)
    dialogPath += '&OBJET=' + FCKConfig.CodeObjet + '&CODE=' + FCKConfig.Code + '&ID_FICHE=' + FCKConfig.IdFiche;
if (FCKConfig.LangueFiche)
    dialogPath += '&LANGUE_FICHE=' + FCKConfig.LangueFiche;

FCKCommands.RegisterCommand('KPortalLink', new FCKDialogCommand('KPortalLink', FCKLang['DlgKPortalLinkTitle'], dialogPath, 875, 650));

//le bouton
var oKPortalLink = new FCKToolbarButton('KPortalLink', FCKLang.InsertLinkLbl, FCKLang.InsertLink, null, false, true, 34);

//on enregistre 
FCKToolbarItems.RegisterItem('KPortalLink', oKPortalLink);

FCK.ContextMenu.RegisterListener({
        AddItems: function (menu, tag, tagName) {
            var bInsideLink = ( tagName == 'A' || FCKSelection.HasAncestorNode('A') ),
                saisieFrontElement = window.top.document.querySelector('input[name="SAISIE_FRONT"]'),
                isFront = saisieFrontElement ? saisieFrontElement.value === "true" : false;

            if (bInsideLink || FCK.GetNamedCommandState('Unlink') != FCK_TRISTATE_DISABLED) {
                // Go up to the anchor to test its properties
                var oLink = FCKSelection.MoveToAncestorNode('A');
                var bIsAnchor = ( oLink && oLink.name.length > 0 && oLink.href.length == 0 );
                // If it isn't a link then don't add the Link context menu
                if (bIsAnchor)
                    return;

                menu.AddSeparator();
                if (bInsideLink && !isFront)
                    menu.AddItem('KPortalLink', FCKLang.EditLink, 34);
                else
                    menu.AddItem('Link', FCKLang.EditLink, 34);
                menu.AddItem('Unlink', FCKLang.RemoveLink, 35);
            }
        }
    }
);