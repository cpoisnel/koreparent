/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2008 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * Scripts related to the Link dialog window (see fck_link.html).
 */

var dialog = window.parent;
var oEditor = dialog.InnerDialogLoaded();

var FCK = oEditor.FCK;
var FCKLang = oEditor.FCKLang;
var FCKConfig = oEditor.FCKConfig;
var FCKRegexLib = oEditor.FCKRegexLib;
var FCKTools = oEditor.FCKTools;

path = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&MODE_FICHIER=LIEN&FCK_PLUGIN=TRUE';
if (FCKConfig.CodeObjet && FCKConfig.Code && FCKConfig.IdFiche)
    path += '&OBJET=' + FCKConfig.CodeObjet + '&CODE=' + FCKConfig.Code + '&ID_FICHE=' + FCKConfig.IdFiche;
FCKConfig.PluginLoadedPath = path;

//#### Dialog Tabs

// Set the dialog tabs.
//dialog.AddTab( 'Info', FCKLang.DlgLnkInfoTab ) ;

if (!FCKConfig.K_LinkDlgHideTarget)
    dialog.AddTab('Target', FCKLang.DlgLnkTargetTab, true);

if (FCKConfig.K_LinkUpload)
    dialog.AddTab('Upload', FCKLang.DlgLnkUpload, true);

if (!FCKConfig.K_LinkDlgHideAdvanced)
    dialog.AddTab('Advanced', FCKLang.DlgAdvancedTag);

// Function called when a dialog tag is selected.
function OnDialogTabChange(tabCode) {
    ShowE('divInfo', ( tabCode == 'Info' ));
    ShowE('divTarget', ( tabCode == 'Target' ));
    ShowE('divUpload', ( tabCode == 'Upload' ));
    ShowE('divAttribs', ( tabCode == 'Advanced' ));

    dialog.SetAutoSize(true);
}

//#### Regular Expressions library.
var oRegex = new Object();

oRegex.UriProtocol = /^(((http|https|ftp|news):\/\/)|mailto:)/gi;

oRegex.UrlOnChangeProtocol = /^(http|https|ftp|news):\/\/(?=.)/gi;

oRegex.UrlOnChangeTestOther = /^((javascript:)|[#\/\.])/gi;

oRegex.ReserveTarget = /^_(blank|self|top|parent)$/i;

oRegex.PopupUri = /^javascript:void\(\s*window.open\(\s*'([^']+)'\s*,\s*(?:'([^']*)'|null)\s*,\s*'([^']*)'\s*\)\s*\)\s*$/;

// Accessible popups
oRegex.OnClickPopup = /^\s*on[cC]lick="\s*window.open\(\s*this\.href\s*,\s*(?:'([^']*)'|null)\s*,\s*'([^']*)'\s*\)\s*;\s*return\s*false;*\s*"$/;

oRegex.PopupFeatures = /(?:^|,)([^=]+)=(\d+|yes|no)/gi;

//#### Parser Functions

var oParser = new Object();

oParser.ParseEMailUrl = function (emailUrl) {
    // Initializes the EMailInfo object.
    var oEMailInfo = new Object();
    oEMailInfo.Address = '';
    oEMailInfo.Subject = '';
    oEMailInfo.Body = '';

    var oParts = emailUrl.match(/^([^\?]+)\??(.+)?/);
    if (oParts) {
        // Set the e-mail address.
        oEMailInfo.Address = oParts[1];

        // Look for the optional e-mail parameters.
        if (oParts[2]) {
            var oMatch = oParts[2].match(/(^|&)subject=([^&]+)/i);
            if (oMatch) oEMailInfo.Subject = decodeURIComponent(oMatch[2]);

            oMatch = oParts[2].match(/(^|&)body=([^&]+)/i);
            if (oMatch) oEMailInfo.Body = decodeURIComponent(oMatch[2]);
        }
    }

    return oEMailInfo;
}

oParser.ParseNewEMailUrl = function (emailUrl) {
    // Initializes the EMailInfo object.
    var oEMailInfo = new Object();
    oEMailInfo.Address = '';
    oEMailInfo.Subject = '';
    oEMailInfo.Body = '';

    var oParts = emailUrl.match(/^\[mailto\](.+)\[\/mailto\]/i);
    if (oParts) {
        var oMatch = oParts[1].match(/^email=([^&]+)/i);
        if (oMatch) oEMailInfo.Address = decodeURIComponent(oMatch[1]);

        oMatch = oParts[1].match(/(^|&)subject=([^&]+)/i);
        if (oMatch) oEMailInfo.Subject = decodeURIComponent(oMatch[2]);

        oMatch = oParts[1].match(/(^|&)body=([^&]+)/i);
        if (oMatch) oEMailInfo.Body = decodeURIComponent(oMatch[2]);
    }

    return oEMailInfo;
}

oParser.CreateEMailUri = function (address, subject, body) {
    var sBaseUri = 'email=' + address;
    if (subject.length > 0) {
        sBaseUri += '&subject=' + encodeURIComponent(subject);
    }
    if (body.length > 0) {
        sBaseUri += '&body=' + encodeURIComponent(body);
    }
    return "[mailto]" + sBaseUri + "[/mailto]";
}

//#### Initialization Code

// oLink: The actual selected link in the editor.
var oLink = dialog.Selection.GetSelection().MoveToAncestorNode('A');
if (oLink)
    FCK.Selection.SelectNode(oLink);

window.onload = function () {
    // Check the text selected in the editor
    //var text = GetSelection( oEditor );
    /*var text = (oEditor.FCKBrowserInfo.IsGecko) ? FCK.EditorWindow.getSelection() : FCK.EditorDocument.selection.createRange().text;
     if (text == "") // if nothing selected
     {
     alert(oEditor.FCKLang.DlgKPortalLinkErrMsg);
     dialog.Cancel();
     } else {*/
    FCKConfig

    dialog.SetAutoSize(true);

    dialog.SetOkButton(true);

    // Translate the dialog box texts.
    oEditor.FCKLanguageManager.TranslatePage(document);

    // Fill the Anchor Names and Ids combos.
    LoadAnchorNamesAndIds();

    // Load the selected link information (if any).
    LoadSelection();

    // Update the dialog box.
    SetLinkType(GetE('cmbLinkType').value);

    // Show/Hide the "Browse Server" button.
    GetE('divBrowseServer').style.display = FCKConfig.K_LinkBrowser ? '' : 'none';

    // Show the initial dialog content.
    GetE('divInfo').style.display = '';

    // Set the actual uploader URL.
    if (FCKConfig.K_LinkUpload)
        GetE('frmUpload').action = FCKConfig.K_LinkUploadURL;

    // Set the default target (from configuration).
    SetDefaultTarget();

    // Select the first field.
    switch (GetE('cmbLinkType').value) {
        case 'url' :
            SelectField('txtUrl');
            break;
        case 'email' :
            SelectField('txtEMailAddress');
            break;
        case 'anchor' :
            if (GetE('divSelAnchor').style.display != 'none')
                SelectField('cmbAnchorName');
            else
                SelectField('cmbLinkType');
    }
    //}
}

var bHasAnchors;

function LoadAnchorNamesAndIds() {
    // Since version 2.0, the anchors are replaced in the DOM by IMGs so the user see the icon
    // to edit them. So, we must look for that images now.
    var aAnchors = new Array();
    var i;
    var oImages = oEditor.FCK.EditorDocument.getElementsByTagName('IMG');
    for (i = 0; i < oImages.length; i++) {
        if (oImages[i].getAttribute('_fckanchor'))
            aAnchors[aAnchors.length] = oEditor.FCK.GetRealElement(oImages[i]);
    }

    // Add also real anchors
    var oLinks = oEditor.FCK.EditorDocument.getElementsByTagName('A');
    for (i = 0; i < oLinks.length; i++) {
        if (oLinks[i].name && ( oLinks[i].name.length > 0 ))
            aAnchors[aAnchors.length] = oLinks[i];
    }

    var aIds = FCKTools.GetAllChildrenIds(oEditor.FCK.EditorDocument.body);

    bHasAnchors = ( aAnchors.length > 0 || aIds.length > 0 );

    for (i = 0; i < aAnchors.length; i++) {
        var sName = aAnchors[i].name;
        if (sName && sName.length > 0)
            FCKTools.AddSelectOption(GetE('cmbAnchorName'), sName, sName);
    }

    ShowE('divSelAnchor', bHasAnchors);
    ShowE('divNoAnchor', !bHasAnchors);
}

function LoadSelection() {
    if (!oLink) return;

    var sType = 'url';

    // Get the actual Link href.
    var sHRef = oLink.getAttribute('_fcksavedurl');
    if (sHRef == null)
        sHRef = oLink.getAttribute('href', 2) || '';

    //CFL 20080618 : TODO : attribut ajoutÃ© rÃ©cemment. il faudrait refaire tout le chargement de lien Ã  partir de cet attribut
    // au lieu de dÃ©duire le type de lien a partir du tag...
    var linkType = oLink.getAttribute('_linktype');
    if (linkType == null)
        linkType = 'url';

    // Look for a popup javascript link.
    var oPopupMatch = oRegex.PopupUri.exec(sHRef);
    if (oPopupMatch) {
        GetE('cmbTarget').value = 'popup';
        sHRef = oPopupMatch[1];
        FillPopupFields(oPopupMatch[2], oPopupMatch[3]);
        SetTarget('popup');
    }

    // Accessible popups, the popup data is in the onclick attribute
    if (!oPopupMatch) {
        var onclick = oLink.getAttribute('onclick_fckprotectedatt');
        if (onclick) {
            // Decode the protected string
            onclick = decodeURIComponent(onclick);

            oPopupMatch = oRegex.OnClickPopup.exec(onclick);
            if (oPopupMatch) {
                GetE('cmbTarget').value = 'popup';
                FillPopupFields(oPopupMatch[1], oPopupMatch[2]);
                SetTarget('popup');
            }
        }
    }

    // Search for the protocol.
    var sProtocol = oRegex.UriProtocol.exec(sHRef);
    if (sProtocol) {
        sProtocol = sProtocol[0].toLowerCase();
        GetE('cmbLinkProtocol').value = sProtocol;


    } else {
        sProtocol = '';
    }
    // Remove the protocol and get the remaining URL.
    var sUrl = sHRef.replace(oRegex.UriProtocol, '');

    // It is an e-mail link.
    if (sHRef.indexOf('mailto:') == 0 || sHRef.indexOf('[mailto]') == 0) {
        sType = 'email';
        var oEMailInfo = new Object();
        if (sHRef.indexOf('mailto:') == 0) {
            oEMailInfo = oParser.ParseEMailUrl(sUrl);
        }
        else {
            oEMailInfo = oParser.ParseNewEMailUrl(sUrl);
        }

        if (oEMailInfo.Address.indexOf('@') == -1) {
            GetE('tag_kportal').value = oEMailInfo.Address;
            GetE('cmbLinkTypeEmail').value = 'KportalEmail';
            //GetE('txtEMailAddress').value	= FCKLang.DlgLnkContactAnnuaire ;
        } else {
            GetE('tag_kportal').value = '';
            GetE('cmbLinkTypeEmail').value = 'ClassiqueEmail';
        }
        SetEmailType(GetE('cmbLinkTypeEmail').value);
        GetE('txtEMailAddress').value = oEMailInfo.Address;
        GetE('txtEMailSubject').value = oEMailInfo.Subject;
        GetE('txtEMailBody').value = oEMailInfo.Body;

    }
    else if (linkType == 'interne')	// Lien vers une fiche (lien interne).
    {
        sType = 'interne';
        GetE('tag_kportal').value = sHRef;
    }
    else if (linkType == 'requete')	// Lien vers une fiche (lien interne).
    {
        sType = 'requete';
        GetE('tag_kportal').value = sHRef;
    }
    else if (sHRef.indexOf('[id-rubrique]') != -1)	// Lien vers une rubrique.
    {
        sType = 'rubrique';
        GetE('tag_kportal').value = sHRef;
    }
    else if (sHRef.indexOf('[id-fichier]') != -1)	// Lien vers un fichier.
    {
        sType = 'fichier';
        GetE('tag_kportal').value = sHRef;
    }
    else if (sHRef.indexOf('[form-recherche]') != -1)	// Lien vers un formulaire de recherche.
    {
        sType = 'recherche';
        var tag = sHRef;
        tag = tag.replace(/\[form\-recherche\]/gi, "");
        tag = tag.replace(/\[\/form-recherche\]/gi, "");
        GetE('cmbLinkFormRechercheObjet').value = tag.toUpperCase();
    }
    else if (sHRef.indexOf('[href') != -1)	// Lien intranet
    {
        sType = 'intranet';
        if (sHRef.indexOf('[href_logindsi]') != -1) {
            GetE('selectIntranet').value = "logindsi";
        } else if (sHRef.indexOf('[href_prefdsi]') != -1) {
            GetE('selectIntranet').value = "prefdsi";
        } else if (sHRef.indexOf('[href_logoutdsi]') != -1) {
            GetE('selectIntranet').value = "logoutdsi";
        } else {
            GetE('selectIntranet').value = "";
        }
    }
    else if (sHRef.substr(0, 1) == '#' && sHRef.length > 1)	// It is an anchor link.
    {
        sType = 'anchor';
        if (GetE('cmbAnchorId'))
            GetE('cmbAnchorName').value = GetE('cmbAnchorId').value = sHRef.substr(1);
        else
            GetE('txtAnchorName').value = sHRef.substr(1);
    }
    else					// It is another type of link.
    {
        sType = 'url';
        GetE('cmbLinkProtocol').value = sProtocol;
        GetE('txtUrl').value = sUrl;
    }

    if (!oPopupMatch) {
        // Get the target.
        var sTarget = oLink.target;

        if (sTarget && sTarget.length > 0) {
            if (oRegex.ReserveTarget.test(sTarget)) {
                sTarget = sTarget.toLowerCase();
                GetE('cmbTarget').value = sTarget;
            }
            else
                GetE('cmbTarget').value = 'frame';
            GetE('txtTargetFrame').value = sTarget;
        }
    }

    // Get Advances Attributes
    GetE('txtAttId').value = oLink.id;
    GetE('txtAttName').value = oLink.name;
    GetE('cmbAttLangDir').value = oLink.dir;
    GetE('txtAttLangCode').value = oLink.lang;
    GetE('txtAttAccessKey').value = oLink.accessKey;
    GetE('txtAttTabIndex').value = oLink.tabIndex <= 0 ? '' : oLink.tabIndex;
    GetE('txtAttTitle').value = oLink.title;
    GetE('txtAttContentType').value = oLink.type;
    GetE('txtAttCharSet').value = oLink.charset;

    var sClass;
    if (oEditor.FCKBrowserInfo.IsIE) {
        sClass = oLink.getAttribute('className', 2) || '';
        // Clean up temporary classes for internal use:
        sClass = sClass.replace(FCKRegexLib.FCK_Class, '');

        GetE('txtAttStyle').value = oLink.style.cssText;
    }
    else {
        sClass = oLink.getAttribute('class', 2) || '';
        GetE('txtAttStyle').value = oLink.getAttribute('style', 2) || '';
    }
    GetE('txtAttClasses').value = sClass;

    // Update the Link type combo.
    GetE('cmbLinkType').value = sType;
}

function SetEmailType(emailType) {
    ShowE('divLinkTypeEMailKPortal', ((emailType == 'KportalEmail')));
    ShowE('divLinkTypeEMailClassique', ((emailType == 'ClassiqueEmail')));
    if (emailType == 'KportalEmail')
        dialog.SetOkButton(false);
    else
        dialog.SetOkButton(true);
}

//#### Link type selection.
function SetLinkType(linkType) {
    dialog.SetOkButton(true);

    ShowE('divLinkTypeUrl', (linkType == 'url'));
    ShowE('divLinkTypeAnchor', (linkType == 'anchor'));
    ShowE('divLinkTypeEMail', (linkType == 'email'));
    if (linkType == 'email') {
        SetEmailType(GetE('cmbLinkTypeEmail').value)
    } else {
        SetEmailType('');
    }
    ShowE('divLinkTypeLienInterne', (linkType == 'interne'));
    ShowE('inputLienInterneRetourChoixTypeFiche', false);
    ShowE('divLinkTypeLienRubrique', (linkType == 'rubrique'));
    ShowE('divLinkTypeLienFichier', (linkType == 'fichier'));
    ShowE('divLinkTypeLienFormRecherche', (linkType == 'recherche'));
    ShowE('divLinkTypeLienRequete', (linkType == 'requete'));
    ShowE('inputLienRequeteRetourChoixTypeFiche', false);
    ShowE('divLinkTypeLienIntranet', (linkType == 'intranet'));

    if (!FCKConfig.K_LinkDlgHideTarget)
        dialog.SetTabVisibility('Target', (linkType == 'url'));

    if (FCKConfig.K_LinkUpload)
        dialog.SetTabVisibility('Upload', (linkType == 'url'));

    if (!FCKConfig.K_LinkDlgHideAdvanced)
        dialog.SetTabVisibility('Advanced', (linkType != 'anchor' || bHasAnchors));

    if (linkType == 'rubrique') {
        dialog.SetOkButton(true);
    }
    if (linkType == 'requete') {
        dialog.SetOkButton(false);
    }
    if (linkType == 'fichier') {
        dialog.SetOkButton(false);
    }
    if (linkType == 'interne') {
        dialog.SetOkButton(false);
    }
}

//#### Target type selection.
function SetTarget(targetType) {
    GetE('tdTargetFrame').style.display = ( targetType == 'popup' ? 'none' : '' );
    GetE('tdPopupName').style.display =
        GetE('tablePopupFeatures').style.display = ( targetType == 'popup' ? '' : 'none' );

    switch (targetType) {
        case "_blank" :
        case "_self" :
        case "_parent" :
        case "_top" :
            GetE('txtTargetFrame').value = targetType;
            break;
        case "" :
            GetE('txtTargetFrame').value = '';
            break;
    }

    if (targetType == 'popup')
        dialog.SetAutoSize(true);
}

//#### Called while the user types the URL.
function OnUrlChange() {
    var sUrl = GetE('txtUrl').value;
    var sProtocol = oRegex.UrlOnChangeProtocol.exec(sUrl);

    if (sProtocol) {
        sUrl = sUrl.substr(sProtocol[0].length);
        GetE('txtUrl').value = sUrl;
        GetE('cmbLinkProtocol').value = sProtocol[0].toLowerCase();
    }
    else if (oRegex.UrlOnChangeTestOther.test(sUrl)) {
        GetE('cmbLinkProtocol').value = '';
    }
}

//#### Called while the user types the target name.
function OnTargetNameChange() {
    var sFrame = GetE('txtTargetFrame').value;

    if (sFrame.length == 0)
        GetE('cmbTarget').value = '';
    else if (oRegex.ReserveTarget.test(sFrame))
        GetE('cmbTarget').value = sFrame.toLowerCase();
    else
        GetE('cmbTarget').value = 'frame';
}

// Accessible popups
function BuildOnClickPopup() {
    var sWindowName = "'" + GetE('txtPopupName').value.replace(/\W/gi, "") + "'";

    var sFeatures = '';
    var aChkFeatures = document.getElementsByName('chkFeature');
    for (var i = 0; i < aChkFeatures.length; i++) {
        if (i > 0) sFeatures += ',';
        sFeatures += aChkFeatures[i].value + '=' + ( aChkFeatures[i].checked ? 'yes' : 'no' );
    }

    if (GetE('txtPopupWidth').value.length > 0)    sFeatures += ',width=' + GetE('txtPopupWidth').value;
    if (GetE('txtPopupHeight').value.length > 0)    sFeatures += ',height=' + GetE('txtPopupHeight').value;
    if (GetE('txtPopupLeft').value.length > 0)    sFeatures += ',left=' + GetE('txtPopupLeft').value;
    if (GetE('txtPopupTop').value.length > 0)        sFeatures += ',top=' + GetE('txtPopupTop').value;

    if (sFeatures != '')
        sFeatures = sFeatures + ",status";

    return ( "window.open(this.href," + sWindowName + ",'" + sFeatures + "'); return false" );
}

//#### Fills all Popup related fields.
function FillPopupFields(windowName, features) {
    if (windowName)
        GetE('txtPopupName').value = windowName;

    var oFeatures = new Object();
    var oFeaturesMatch;
    while (( oFeaturesMatch = oRegex.PopupFeatures.exec(features) ) != null) {
        var sValue = oFeaturesMatch[2];
        if (sValue == ( 'yes' || '1' ))
            oFeatures[oFeaturesMatch[1]] = true;
        else if (!isNaN(sValue) && sValue != 0)
            oFeatures[oFeaturesMatch[1]] = sValue;
    }

    // Update all features check boxes.
    var aChkFeatures = document.getElementsByName('chkFeature');
    for (var i = 0; i < aChkFeatures.length; i++) {
        if (oFeatures[aChkFeatures[i].value])
            aChkFeatures[i].checked = true;
    }

    // Update position and size text boxes.
    if (oFeatures['width'])    GetE('txtPopupWidth').value = oFeatures['width'];
    if (oFeatures['height'])    GetE('txtPopupHeight').value = oFeatures['height'];
    if (oFeatures['left'])    GetE('txtPopupLeft').value = oFeatures['left'];
    if (oFeatures['top'])        GetE('txtPopupTop').value = oFeatures['top'];
}

function insertKListe_lienRequete(objet, requete, libelle) {
    var txtHtml = "[id-fiche]" + objet + ";" + requete + "[/id-fiche]";
    insertKLink(txtHtml);
}

function insertKLink(sUri, libelle) {
    // If no link is selected, create a new one (it may result in more than one link creation - #220).
    var aLinks = oLink ? [oLink] : oEditor.FCK.CreateLink(sUri, true);

    // If no selection, no links are created, so use the uri as the link text (by dom, 2006-05-26)
    var aHasSelection = ( aLinks.length > 0 );
    if (!aHasSelection) {
        sInnerHtml = sUri;

        // Built a better text for empty links.
        switch (GetE('cmbLinkType').value) {
            // anchor: use old behavior --> return true
            case 'anchor':
                sInnerHtml = sInnerHtml.replace(/^#/, '');
                break;

            // url: try to get path
            case 'url':
                var oLinkPathRegEx = new RegExp("//?([^?\"']+)([?].*)?$");
                var asLinkPath = oLinkPathRegEx.exec(sUri);
                if (asLinkPath != null)
                    sInnerHtml = asLinkPath[1];  // use matched path
                break;

            // mailto: try to get email address
            case 'email':
                sInnerHtml = GetE('txtEMailAddress').value;
                break;

            case 'interne' :
                sInnerHtml = 'lien interne';
                break;

            case 'rubrique' :
                sInnerHtml = 'la rubrique';
                break;

            case 'fichier' :
                sInnerHtml = 'le fichier';
                break;

            case 'recherche' :
                sInnerHtml = 'rechercher';
                break;

            case 'requete' :
                sInnerHtml = 'lien de requete';
                break;

            case 'intranet' :
                sInnerHtml = libelle ? libelle : 'lien intranet';
                break;
        }

        // Create a new (empty) anchor.
        aLinks = [oEditor.FCK.InsertElement('a')];
    }
    sUri = sUri + '#KLINK';

    for (var i = 0; i < aLinks.length; i++) {
        oLink = aLinks[i];

        if (aHasSelection)
            sInnerHtml = oLink.innerHTML;		// Save the innerHTML (IE changes it if it is like an URL).

        oLink.href = sUri;
        SetAttribute(oLink, '_fcksavedurl', sUri);
        SetAttribute(oLink, '_linktype', GetE('cmbLinkType').value);

        var onclick;
        // Accessible popups
        if (GetE('cmbTarget').value == 'popup') {
            onclick = BuildOnClickPopup();
            // Encode the attribute
            onclick = encodeURIComponent(" onclick=\"" + onclick + "\"");
            SetAttribute(oLink, 'onclick_fckprotectedatt', onclick);
        }
        else {
            // Check if the previous onclick was for a popup:
            // In that case remove the onclick handler.
            onclick = oLink.getAttribute('onclick_fckprotectedatt');
            if (onclick) {
                // Decode the protected string
                onclick = decodeURIComponent(onclick);

                if (oRegex.OnClickPopup.test(onclick))
                    SetAttribute(oLink, 'onclick_fckprotectedatt', '');
            }
        }

        oLink.innerHTML = sInnerHtml;		// Set (or restore) the innerHTML

        // Target
        if (GetE('cmbTarget').value != 'popup')
            SetAttribute(oLink, 'target', GetE('txtTargetFrame').value);
        else
            SetAttribute(oLink, 'target', null);

        // Let's set the "id" only for the first link to avoid duplication.
        if (i == 0)
            SetAttribute(oLink, 'id', GetE('txtAttId').value);

        // Advances Attributes
        SetAttribute(oLink, 'name', GetE('txtAttName').value);
        SetAttribute(oLink, 'dir', GetE('cmbAttLangDir').value);
        SetAttribute(oLink, 'lang', GetE('txtAttLangCode').value);
        SetAttribute(oLink, 'accesskey', GetE('txtAttAccessKey').value);
        SetAttribute(oLink, 'tabindex', ( GetE('txtAttTabIndex').value > 0 ? GetE('txtAttTabIndex').value : null ));
        SetAttribute(oLink, 'title', GetE('txtAttTitle').value);
        SetAttribute(oLink, 'type', GetE('txtAttContentType').value);
        SetAttribute(oLink, 'charset', GetE('txtAttCharSet').value);

        if (oEditor.FCKBrowserInfo.IsIE) {
            var sClass = GetE('txtAttClasses').value;
            // If it's also an anchor add an internal class
            if (GetE('txtAttName').value.length != 0)
                sClass += ' FCK__AnchorC';
            SetAttribute(oLink, 'className', sClass);

            oLink.style.cssText = GetE('txtAttStyle').value;
        }
        else {
            SetAttribute(oLink, 'class', GetE('txtAttClasses').value);
            SetAttribute(oLink, 'style', GetE('txtAttStyle').value);
        }
    }

    // Select the (first) link.
    oEditor.FCKSelection.SelectNode(aLinks[0]);
}

//#### The OK button was hit.
function Ok() {
    var sUri = "";
    var sInnerHtml = "";
    oEditor.FCKUndo.SaveUndoStep();

    switch (GetE('cmbLinkType').value) {
        case 'url' :
            sUri = GetE('txtUrl').value;

            if (sUri.length == 0) {
                alert(FCKLang.DlnLnkMsgNoUrl);
                return false;
            }

            sUri = GetE('cmbLinkProtocol').value + sUri;

            break;

        case 'email' :

            contactKportal = (GetE('cmbLinkTypeEmail').value == 'KportalEmail' ? true : false);
            //contact selectionné
            if (contactKportal) {
                sUri = oParser.CreateEMailUri(
                    GetE('tag_kportal').value,
                    GetE('txtEMailSubject').value,
                    GetE('txtEMailBody').value);
            }
            //autre adresse saisie
            else {
                sUri = GetE('txtEMailAddress').value;
                //aucune adresse saisie
                if (sUri.length == 0) {
                    alert(FCKLang.DlnLnkMsgNoEMail);
                    return false;
                }
                sUri = oParser.CreateEMailUri(
                    sUri,
                    GetE('txtEMailSubject').value,
                    GetE('txtEMailBody').value);
            }
            break;

        case 'anchor' :

            var sAnchor = GetE('cmbAnchorName').value;
            if (sAnchor.length == 0 && GetE('cmbAnchorId')) sAnchor = GetE('cmbAnchorId').value;

            var tAnchor = GetE('txtAnchorName').value;
            if (tAnchor.length > 0) sAnchor = tAnchor;

            if (sAnchor.length == 0) {
                alert(FCKLang.DlnLnkMsgNoAnchor);
                return false;
            }

            sUri = '#' + sAnchor;
            break;

        case 'interne' :
            sUri = GetE('tag_kportal').value;

            if (sUri.length == 0) {
                alert(FCKLang['DlnLnkMsgNoLienInterne']);
                return false;
            }

            break;

        case 'rubrique' :
            frames['iframe_rubrique'].renvoyerLienRubrique();
            sUri = GetE('tag_kportal').value;
            if (sUri.length == 0) {
                alert(FCKLang['FCKLang.DlnLnkMsgNoRubrique']);
                return false;
            }
            break;

        case 'fichier' :
            sUri = GetE('tag_kportal').value;
            if (sUri.length == 0) {
                alert(FCKLang['FCKLang.DlnLnkMsgNoFichier']);
                return false;
            }

            break;
        case 'recherche' :
            sUri = GetE('tag_kportal').value;
            break;
        case 'requete' :
            frames['iframe_requete'].sauvegarderRequete();

            return true;
        case 'intranet' :
            sUri = GetE('tag_kportal').value.split(';')[0];
            sInnerHtml = GetE('tag_kportal').value.split(';')[1];
            if (!sUri) {
                alert(FCKLang['FCKLang.DlnLnkMsgNoIntranet']);
                return false;
            }

            break;
    }
    insertKLink(sUri, sInnerHtml);

    return true;
}

function BrowseServer() {
    OpenFileBrowser(FCKConfig.K_LinkBrowserURL, FCKConfig.K_LinkBrowserWindowWidth, FCKConfig.K_LinkBrowserWindowHeight);
}

function SetUrl(url) {
    document.getElementById('txtUrl').value = url;
    OnUrlChange();
    dialog.SetSelectedTab('Info');
}

function OnUploadCompleted(errorNumber, fileUrl, fileName, customMsg) {
    switch (errorNumber) {
        case 0 :	// No errors
            alert('Your file has been successfully uploaded');
            break;
        case 1 :	// Custom error
            alert(customMsg);
            return;
        case 101 :	// Custom warning
            alert(customMsg);
            break;
        case 201 :
            alert('A file with the same name is already available. The uploaded file has been renamed to "' + fileName + '"');
            break;
        case 202 :
            alert('Invalid file type');
            return;
        case 203 :
            alert("Security error. You probably don't have enough permissions to upload. Please check your server.");
            return;
        case 500 :
            alert('The connector is disabled');
            break;
        default :
            alert('Error on file upload. Error number: ' + errorNumber);
            return;
    }

    SetUrl(fileUrl);
    GetE('frmUpload').reset();
}

var oUploadAllowedExtRegex = new RegExp(FCKConfig.K_LinkUploadAllowedExtensions, 'i');
var oUploadDeniedExtRegex = new RegExp(FCKConfig.K_LinkUploadDeniedExtensions, 'i');

function CheckUpload() {
    var sFile = GetE('txtUploadFile').value;

    if (sFile.length == 0) {
        alert('Please select a file to upload');
        return false;
    }

    if (( FCKConfig.K_LinkUploadAllowedExtensions.length > 0 && !oUploadAllowedExtRegex.test(sFile) ) ||
        ( FCKConfig.K_LinkUploadDeniedExtensions.length > 0 && oUploadDeniedExtRegex.test(sFile) )) {
        OnUploadCompleted(202);
        return false;
    }

    return true;
}

function SetDefaultTarget() {
    var target = FCKConfig.K_DefaultLinkTarget || '';

    if (oLink || target.length == 0)
        return;

    switch (target) {
        case '_blank' :
        case '_self' :
        case '_parent' :
        case '_top' :
            GetE('cmbTarget').value = target;
            break;
        default :
            GetE('cmbTarget').value = 'frame';
            break;
    }

    GetE('txtTargetFrame').value = target;
}

/** METHODES KPORTAL AJOUTEES **/

// Create a K-Portal specific mailto uri
function CreateKPortalEMailUri(codeAnnuaire, libelle) {
    GetE('txtEMailAddress').value = libelle;
    GetE("tag_kportal").value = "annuaire;" + codeAnnuaire;
    window.parent.Ok();
}

function CreateLienIntranet(selectIntranet, libelle) {
    GetE("tag_kportal").value = '[href_' + selectIntranet + '];' + libelle;
    window.parent.Ok();
}

function CreateLienInterne(objet, code, libelle, rubrique_forcage, langue) {
    var txtHtml = "[id-fiche]" + objet + ";" + code;
    if (langue && langue.length > 0) {
        txtHtml += ";" + langue;
    }
    txtHtml += "[/id-fiche]";
    if (rubrique_forcage.length > 0) {
        txtHtml += '?RF=' + rubrique_forcage;
    }

    GetE("tag_kportal").value = txtHtml;
    window.parent.Ok();
}

function CreateLienDocument(id) {
    GetE("tag_kportal").value = "[id-document];" + id + "[/id-document]";
    window.parent.Ok();
}

//crÃ©e un lien vers l'url d'un objet de type LIEN
function CreateLienLien(id) {
    GetE("tag_kportal").value = "[id-lien]" + id + "[/id-lien]";
    window.parent.Ok();
}

function CreateLienRubrique(codeRubrique) {
    if (codeRubrique.length > 0) {
        GetE("tag_kportal").value = "[id-rubrique]" + codeRubrique + "[/id-rubrique]";
    }
    //window.parent.Ok();
}

function CreateLienFichier(code, hauteur, largeur) {
    GetE("tag_kportal").value = "[id-fichier]" + code + "[/id-fichier]";
    //GetE('cmbTarget').value = 'popup';
    if (largeur != '')
        GetE('txtPopupWidth').value = parseInt(largeur) + 20;
    if (hauteur != '')
        GetE('txtPopupHeight').value = parseInt(hauteur) + 20;

    window.parent.Ok();
}

function CreateLienRecherche(code) {
    GetE("tag_kportal").value = "[form-recherche]" + code + "[/form-recherche]";
    window.parent.Ok();
}

//Fait revenir l'iframe de selection de lien interne à l'écran de séléction de type de fiche
function retourChoixTypeFicheLienInterne(langueFiche) {
    document.getElementById('iframeLinkTypeLienInterne').src = '/adminsite/template/recherche_back.jsp?TOOLBOX=LIEN_INTERNE&amp;FCK_PLUGIN=TRUE&LANGUE_FICHE=' + langueFiche;
}

//Fait revenir l'iframe de selection de lien requete à l'écran de séléction de type de fiche
function retourChoixTypeFicheLienRequete(langueFiche) {
    document.getElementById('iframe_requete').src = '/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_REQUETE&LANGUE_FICHE=' + langueFiche + '&FCK_PLUGIN=TRUE';
}

// methode permettant de savoir le type de lien en cours
function getLinkType() {
    return GetE('cmbLinkType').value;
}


