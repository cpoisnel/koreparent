FCKLang['DlnTagMsgNoTag'] = 'Veuillez sélectionner un type de tag';
FCKLang['DlnTagMsgNoTagUtil'] = 'Veuillez sélectionner un tag utilisateur';
FCKLang['DlnTagMsgNoTagService'] = 'Veuillez sélectionner un tag service';
FCKLang['DlnTagMsgNoTagGroupe'] = 'Veuillez sélectionner un tag groupe';
FCKLang['DlnTagMsgNoTagAutre'] = 'Veuillez sélectionner un tag';
FCKLang['KTagProperties'] = 'Modifier le tag';
FCKLang['KPortalTag'] = 'Tag';
FCKLang['DlgKPortalTagTitle'] = 'Insérer un tag';
