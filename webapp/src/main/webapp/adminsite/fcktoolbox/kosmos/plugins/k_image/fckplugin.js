// Register the commands.
dialogPath = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=PHOTO&FCK_PLUGIN=TRUE';
if (FCKConfig.CodeObjet && FCKConfig.Code && FCKConfig.IdFiche) {
    dialogPath = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=PHOTO&OBJET=' + FCKConfig.CodeObjet + '&CODE=' + FCKConfig.Code + '&ID_FICHE=' + FCKConfig.IdFiche + '&FCK_PLUGIN=TRUE';
}

FCKCommands.RegisterCommand('KPortalImage', new FCKDialogCommand('KPortalImage', FCKLang.DlgImgTitle, dialogPath, 960, 555));

// Create the toolbar buttons.
var oKPortalImage = new FCKToolbarButton('KPortalImage', FCKLang.InsertImageLbl, FCKLang.InsertImage, null, false, true, 37);

// Register the toolbar items (these names will be used in the config file).
FCKToolbarItems.RegisterItem('KPortalImage', oKPortalImage);

FCK.ContextMenu.RegisterListener({
        AddItems: function (menu, tag, tagName) {
            if (tagName == 'IMG' && !tag.getAttribute('_fckfakelement')) {
                menu.AddSeparator();
                menu.AddItem('KPortalImage', FCKLang.ImageProperties, 37);
            }
        }
    }
);