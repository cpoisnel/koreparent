function insertContenuStatique(actionUrl, objet, requete) {
    new Ajax.Request(actionUrl, {
        method: 'get', asynchronous: false,
        parameters: {OBJET: objet, REQUETE: requete},
        onSuccess: function (transport) {
            oEditor.FCKUndo.SaveUndoStep();
            var contenuStatique = FCK.EditorDocument.createElement('div');
            contenuStatique.innerHTML = transport.responseText;
            FCK.InsertElement(contenuStatique);
            if (oEditor.FCKBrowserInfo.IsIE) {
                var links = contenuStatique.getElementsByTagName("a");
                for (var i = 0; i < links.length; i++)
                    links[i].href = links[i].href.replace(FCKConfig.BasePath, '');
            }
        }
        , onComplete: function () {
            requeteFinished = true;
        }
    });

    document.forms[0].action = actionUrl;
    return false;
}

var dialog = window.parent;
var oEditor = dialog.InnerDialogLoaded();

var FCK = oEditor.FCK;
var FCKLang = oEditor.FCKLang;
var FCKConfig = oEditor.FCKConfig;
var FCKRegexLib = oEditor.FCKRegexLib;
var FCKTools = oEditor.FCKTools;

function Ok() {
    requeteFinished = false;
    sauvegarderRequete();
    typeInsertion = 'liste';
    return true;
}


function insertKListe_lienRequete(objet, requete, libelle) {
    insertContenuStatique(FCKConfig.KListeStatiqueTemplatePath, objet, requete);
}

