var dialog = window.parent;
var oEditor = dialog.InnerDialogLoaded();
var FCK = oEditor.FCK;
var FCKLang = oEditor.FCKLang;
var FCKConfig = oEditor.FCKConfig;
var FCKDebug = oEditor.FCKDebug;
var FCKTools = oEditor.FCKTools;
FCKConfig.PluginLoadedPath = window.location.href;

/** ICI LE CODE POUR GERER LES FAKE TAGS **/
var oFakeImage = dialog.Selection.GetSelectedElement();
var oSpanAudio;

if (oFakeImage) {
    if (oFakeImage.tagName == 'IMG' && oFakeImage.getAttribute('_kaudio'))
        oSpanAudio = FCK.GetRealElement(oFakeImage);
    else
        oFakeImage = null;
}

/** FIN DU CODE POUR GERER LES FAKE TAGS**/

function insertAudio(url, autoreplay) {

    oEditor.FCKUndo.SaveUndoStep();
    oDivAudio = FCK.EditorDocument.createElement('DIV');
    oDivAudio.className = 'media audio';

    var first = true,
        innerTag = '[media;audio;';
    if (url) {
        innerTag += (first ? 'URL=' : '#URL=') + url;
        first = false;
    }
    if (autoreplay) {
        innerTag += (first ? 'REPLAY=' : '#REPLAY=') + autoreplay;
        first = false;
    }
    innerTag += 'media;audio]';
    oDivAudio.innerHTML = innerTag;

    oFakeImage = null;

    if (!oFakeImage) {
        oFakeImage = oEditor.FCKDocumentProcessor_CreateFakeImage('K_AUDIO', oDivAudio);
        oFakeImage.setAttribute('_kaudio', 'true', 0);

        oFakeImage.setAttribute('src', '/adminsite/images/medias/audio.png');

        oFakeImage = FCK.InsertElement(oFakeImage);
    }
}


window_onload = function () {
    // Translate the dialog box texts.
    oEditor.FCKLanguageManager.TranslatePage(document);

    // Load the selected element information (if any).
    LoadSelection();

    // Show/Hide the "Browse Server" button.
    GetE('tdBrowse').style.display = FCKConfig.AudioBrowser ? '' : 'none';

    dialog.SetAutoSize(true);

    //SelectField( 'txtUrl' ) ;
}


function LoadSelection() {
    if (!oFakeImage) return;

    if (oSpanAudio.tagName.toLowerCase() == 'span') {
        GetE('txtUrl').value = GetAttribute(oSpanAudio, '_kaudio_url', '');
        GetE('dewreplay').checked = (GetAttribute(oSpanAudio, '_kaudio_autoreplay', '') == "1" ? true : false );
    } else {
        var audio = oSpanAudio.children[0];

        GetE('txtUrl').value = GetAttribute(audio, 'src', '');
        GetE('dewreplay').checked = GetAttribute(audio, 'loop', '') ? true : false;
    }

    //dans le cas d'une modif de ressource, on va directement sur l'onglet des parametres d'insertion
    if (GetE('txtUrl').value != '') {
        modeOngletsModif = 'insertion';
    }
}

//#### The OK button was hit.
function Ok() {
    var url = GetE('txtUrl').value;

    if (url == '') {
        alert(FCKLang.DlnAudioMsgNoAudio);
        return false;
    }

    insertAudio(url, GetE('dewreplay').checked);

    return true;
}

function BrowseServer() {
    OpenServerBrowser(
        'Image',
        FCKConfig.AudioBrowserURL,
        FCKConfig.AudioBrowserWindowWidth,
        FCKConfig.AudioBrowserWindowHeight);
}

function OpenServerBrowser(type, url, width, height) {
    sActualBrowser = type;
    OpenFileBrowser(url, width, height);
}

var sActualBrowser;

function SetUrl(url) {
    GetE('txtUrl').value = url;
}