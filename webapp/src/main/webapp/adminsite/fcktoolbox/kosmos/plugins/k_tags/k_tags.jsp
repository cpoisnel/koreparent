<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.LangueUtil" %>
<%@ page import="com.kportal.core.config.PropertyHelper" %>
<%@ page import="com.univ.collaboratif.om.Espacecollaboratif" %>
<%@ page import="com.univ.objetspartages.om.ServiceBean" %>
<%@ page import="com.univ.objetspartages.util.LabelUtils" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.ServicesUtil" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<%@taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Insertion d'un tag</title>

<link rel="stylesheet" media="screen, projection"  href="/adminsite/scripts/libs/css/jquery-ui-1.10.3.custom.css">
<link rel="stylesheet" media="screen, projection"  href="/adminsite/scripts/libs/css/jquery.kmultiselect-0.1.1.css">
<link rel="stylesheet" media="screen, projection"  href="/adminsite/scripts/libs/css/jquery.kmonoselect-0.1.0.css">
<link rel="stylesheet" media="screen, projection"  href="/adminsite/scripts/libs/css/JsTree.css">
<link rel="stylesheet" media="screen, projection"  href="/adminsite/styles/screen.css">

<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
<script type="text/javascript" src="js/k_tags.js"></script>
<script type="text/javascript" src="js/tags_espace.js"></script>
<script type="text/javascript" src="js/tags_contribution.js"></script>
<script type="text/javascript" src="/adminsite/fcktoolbox/fckeditor/editor/dialog/common/fck_dialog_common.js"></script>
<script type="text/javascript">
	window.parent.SetOkButton( true ) ;
	function toggleZoneTypeGroupeVisibility(){
		var eltChoixTagGroupe=document.getElementById('choix_tag_groupe');
		var eltZoneTypeGroupe=document.getElementById('zoneTypeGroupe');
		eltChoixTagGroupe.value=='debutgroupetype'?eltZoneTypeGroupe.style.display='block':eltZoneTypeGroupe.style.display='none';
	}
</script>
</head>
	<body class="popup">
		<div id="page">
			<form class="popup-tag" name="ktags" action="bidon" method="post">
				<div id="content">
					<p id="tags_type">
							<label for="type_tag" class="colonne">Type du tag </label>
							<select id="type_tag" name="tags_type" onchange="afficheComposants(this.name);saveChoixTag(this.value)">
								<option value="tag">Indéfini(e)</option>
								<option value="tag_util">Liés à l'utilisateur</option>
								<option value="tag_groupe">Liés aux groupes</option>
								<option value="tag_contrib">Contribution(s)</option>
								<% if (Espacecollaboratif.isExtensionActivated()) { %>
									<option value="tag_collab">Liés aux espaces collaboratifs</option>
								<% } %>
								<option value="tag_service">Liés aux services</option>
								<option value="tag_autre">Autres</option>
								<% String specificTypeTagOption = PropertyHelper.getCoreProperty("type_tag_option.specific");
								   if (specificTypeTagOption != null) { %>
									<jsp:include page="<%= specificTypeTagOption %>"/>
								<% } %>
							</select>
						</p>
				
					<div id="content_popup" class="popup_content" style="display:none;">
				
						<input type=hidden id="choix_tag" value="" />
					
						<div id="tag_util" style="display:none;">
							<p>
								<label for="choix_tag_util" class="colonne">Choix</label>
								<select name="choix_tag_util" id="choix_tag_util">
									<option value="nom">Nom de l'utilisateur - tag [nom]</option>
									<option value="prenom">Prénom de l'utilisateur - tag [prenom]</option>
									<option value="profil">Code du profil de l'utilisateur - tag [profil]</option>
									<option value="intituleprofil">Intitulé du profil de l'utilisateur - tag [intituleprofil]</option>
									<option value="ksession">Identifiant de session de l'utilisateur - tag [ksession]</option>
									<option value="centresinteret">Codes des centres d'intérêt de l'utilisateur - tag [centresinteret]</option>
									<option value="intitulestructure">Intitulé de la structure de l'utilisateur - tag [intitulestructure]</option>
								</select>
							</p>
						</div><!-- #tag_util -->
					
						<div id="tag_groupe" style="display:none">
							<p>
								<label for="choix_tag_groupe" class="colonne">Choix</label>
								<select name="choix_tag_groupe" id='choix_tag_groupe' onchange="toggleZoneTypeGroupeVisibility();">
									<option value="debutgroupe">Bloc qui sera répété pour chaque groupe de l'utilisateur - tag [traitement;debutgroupe]</option>
									<option value="debutgroupetype">Bloc qui sera répété pour chaque groupe de l'utilisateur d'un certain type - tag [traitement;debutgroupe;type]</option>
									<option value="fingroupe">Fin du bloc de répétition - tag [traitement;fingroupe]</option>
									<option value="groupe">Code du groupe (doit être inséré dans un bloc) - tag [groupe]</option>
									<option value="intitulegroupe">Nom du groupe (doit être inséré dans un bloc) - tag [intitulegroupe]</option>
									<option value="pagegroupe">Lien vers la page d'accueil du groupe (doit être inséré dans un bloc) - tag [pagegroupe]</option>
								</select>
							</p>
								
							<p id="zoneTypeGroupe"  style="display:none">
								<label for="comboTypeGroupe" class="colonne">Type de groupe</label>
								<select name="comboTypeGroupe" id="comboTypeGroupe">
									<%
									final String typeLibelleTypeGroupe="11"; 
									Map<String,String> codeLibelleTypesGroupes = LabelUtils.getLabelCombo(typeLibelleTypeGroupe, LangueUtil.getDefaultLocale());
									for (Map.Entry<String,String> codeLibelleTypeGroupe:codeLibelleTypesGroupes.entrySet()){
									%>	<option value="<%=codeLibelleTypeGroupe.getKey()%>"><%=codeLibelleTypeGroupe.getValue()%></option>
									<% } %>
								</select>
							</p>
						</div><!-- #tag_groupe -->
						
						<div id="tag_autre" style="display:none">
							
							<p>
								<label for="choix_tag_autre" class="colonne">Type d'insertion</label>
								
								<select name="choix_tag_autre" id="choix_tag_autre" onchange="afficheComposants(this.name)">
									<option value="">Indéfini(e)</option>
									<option value="tag_autre_page">Contenu d'une page html - tag [page;]</option>
									<option value="tag_autre_url">Contenu d'une page dynamique - tag [url;]</option>
									<option value="tag_autre_date">Date courante - tag [date]</option>
								</select>
							</p>
						
							<div id="tag_autre_page" style="display:none">
								<p>Saisir le chemin (relatif à la racine du site) d'accès au fichier à inclure</p>
								<label for="tag_autre_page_url" class="colonne">Fichier à inclure (*)</label>
								<input type="text" id="tag_autre_page_url" name="tag_autre_page_url" value="" />
							</div>
						
							<p id="tag_autre_url" style="display:none">
								<label for="tag_autre_url_url" class="colonne">Saisir l'url (*)</label>
								<input type="text" id="tag_autre_url_url" name="tag_autre_url_url" value="" />
							</p>
						
						</div><!-- #tag_autre -->
						
						<div id="tag_service" style="display:none">
						
						<%	Hashtable<String, String> listeServices = new Hashtable<String, String>();
							for (ServiceBean service : ServicesUtil.getServices().values()){
								listeServices.put(service.getCode(), service.getIntitule());
							}
							infoBean.set("LISTE_SERVICES", listeServices); %>
						
						<p>
							<label for="code_service" class="colonne">Choisir le service (*)</label>
							<%fmt.insererComboHashtable( out, infoBean, "code_service", FormateurJSP.SAISIE_FACULTATIF, "LISTE_SERVICES" ); %>
						</p>
						
						<p>
							<label for="choix_tag_service" class="colonne">Tag (*)</label>
							<select id="choix_tag_service" name="choix_tag_service" onchange="afficheComposants(this.name)">
								<option value="">Indéfini(e)</option>
								<option value="service">Affichage standard - tag [service;code]</option>
								<option value="service_contenu">Vue réduite - tag [service_contenu;[nomvue]]</option>
								<option value="service_link">Lien - tag [service_link]</option>
								<option value="service_url">Url - tag [service_url]</option>
								<option value="service_target">Fenêtre cible - tag [service_target]</option>
							</select>
						</p>
						
						<p id="service_contenu" style="display:none">
							<label for="nom_vue" class="colonne">Nom de la vue</label>
							<input type="text" name="nom_vue" id="nom_vue" value="" />
						</p>
						
						<p id="service_link" style="display:none">
							<label for="nom_link" class="colonne">Libellé du lien</label>
							<input type="text" name="nom_link" id="nom_link" value="" /> (nom du service si vide)
						</p>
						
						</div><!-- #tag_service -->

                        <jsp:include page="tags_contribution.jsp" />
                        <% if (Espacecollaboratif.isExtensionActivated()) { %>
						    <jsp:include page="tags_espace.jsp" />
                        <% } %>
                        <% String specificTypeTagDiv = PropertyHelper.getCoreProperty("type_tag_traitement.specific");
						   if (specificTypeTagDiv != null) { %>
							<jsp:include page="<%= specificTypeTagDiv %>"/>
						<% } %>
					
					</div>
				</div><!-- #content -->
			</form>
		</div><!-- #page -->
		<resources:script group="scriptsBo" locale="<%= ContexteUtil.getContexteUniv().getLocale().toString() %>"/>
		<script src="/adminsite/scripts/backoffice.js"></script>
	</body>
</html>
