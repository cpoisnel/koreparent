// correspond en fait au tag [lien_espaces]
function renvoyerTagListeEspaces() {

    codeEspace = document.forms.ktags.liste_espaces_code_espace.value.replace(/;/g, ',');
    if (!document.forms.ktags.liste_espaces_mode_selection_1.checked
        && !document.forms.ktags.liste_espaces_mode_selection_2.checked
        && !document.forms.ktags.liste_espaces_mode_selection_3.checked
        && codeEspace == '') {
        alert('Sélectionner au moins un mode de sélection OU une liste d\'espaces');
        document.forms.ktags.liste_espaces_mode_selection_1.focus();
        return false;
    }
    if ((document.forms.ktags.liste_espaces_mode_selection_1.checked
        || document.forms.ktags.liste_espaces_mode_selection_2.checked
        || document.forms.ktags.liste_espaces_mode_selection_3.checked)
        && codeEspace != '') {
        alert('Sélectionner un mode de sélection OU une liste d\'espaces');
        document.forms.ktags.liste_espaces_mode_selection_1.focus();
        return false;
    }

    mode = "";
    if (document.forms.ktags.liste_espaces_mode_selection_1.checked)
        mode += "1";
    if (document.forms.ktags.liste_espaces_mode_selection_2.checked)
        mode += "2";
    if (document.forms.ktags.liste_espaces_mode_selection_3.checked)
        mode += "3";

    codeRubrique = document.forms.ktags.liste_espaces_code_rubrique.value;
    if (document.forms.ktags.liste_espaces_code_rubrique_courante.checked)
        codeRubrique = 'DYNAMIK';
    codeTheme = document.forms.ktags.liste_espaces_code_theme.value;
    if (codeTheme == '0000')
        codeTheme = '';

    insertTag('[liste_espaces;' + setCritere('#CODE_RUBRIQUE', codeRubrique) + setCritere('#CODE_THEME', codeTheme) + setCritere('#CODE_ESPACE', codeEspace) + setCritere('#MODE', mode) + ']');
}

function renvoyerIntituleEspace() {
	tag = '[intitule_espace]'
	insertTag(tag);
}