<%@page import="java.util.Locale"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>
<%@ page import="com.univ.utils.UnivFmt" errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<%
// initialisation du contexte (infos utilisateur, connexion BdD,...)
ContexteUniv ctx = ContexteUtil.getContexteUniv();
Locale locale = ctx.getLocale();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>Insertion d'un tag</title>
	<link rel="stylesheet" type="text/css" href="/adminsite/general.css"/>

	<link rel="stylesheet" media="screen, projection" href="/adminsite/scripts/libs/css/jquery-ui-1.10.3.custom.css">
	<link rel="stylesheet" media="screen, projection" href="/adminsite/scripts/libs/css/jquery.kmultiselect-0.1.1.css">
	<link rel="stylesheet" media="screen, projection" href="/adminsite/scripts/libs/css/jquery.kmonoselect-0.1.0.css">
	<link rel="stylesheet" media="screen, projection" href="/adminsite/scripts/libs/css/JsTree.css">

	<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
	<script type="text/javascript" src="js/k_site.js"></script>
	<script type="text/javascript" src="/adminsite/fcktoolbox/fckeditor/editor/dialog/common/fck_dialog_common.js"></script>
	<script type="text/javascript">
		window.parent.SetOkButton(true);
	</script>
</head>
<body id="body_k_plugin">
<form class="popup-tag" name="site" action="bidon" method="post">
	<div class="content_popup">

		<div id="plan_site">
			<% univFmt.insererkMonoSelect(fmt, out, infoBean, "plan_site_code_rubrique", FormateurJSP.SAISIE_FACULTATIF, "Rubrique de départ", "rubrique", UnivFmt.CONTEXT_ZONE); %>
			<p>
				<label for="plan_site_nb_niveaux">Nombre de niveaux : </label>
				<input type="text" name="plan_site_nb_niveaux" id="plan_site_nb_niveaux" value="" size="5"/> (0 = illimité)
			</p>
		</div>
	</div>
</form>
		<resources:script group="scriptsBo" locale="<%= locale.toString() %>"/>
		<script src="/adminsite/scripts/backoffice.js"></script>
	</body>
</html>
