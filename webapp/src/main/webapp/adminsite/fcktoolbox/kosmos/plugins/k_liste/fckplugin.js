// Register the commands.
dialogPath = '/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_REQUETE&LISTE_INCLUSE=1&FCK_PLUGIN=TRUE';
if (FCKConfig.LangueFiche)
    dialogPath += '&LANGUE_FICHE=' + FCKConfig.LangueFiche;

FCKCommands.RegisterCommand('KPortalList', new FCKDialogCommand('KPortalList', FCKLang['DlgKPortalListTitle'], dialogPath, 960, 555));

var oKPortalList = new FCKToolbarButton('KPortalList', FCKLang['KPortalList']);

oKPortalList.IconPath = FCKConfig.PluginsPath + 'k_liste/images/list_icone.png';

FCKToolbarItems.RegisterItem('KPortalList', oKPortalList);

FCKDocumentProcessor.AppendNew().ProcessDocument = function (document) {
    var aSpans = document.getElementsByTagName('SPAN');

    var oSpan;
    var i = aSpans.length - 1;
    while (i >= 0 && ( oSpan = aSpans[i--] )) {
        if (oSpan.getAttribute('kliste_span')) {
            var oImg = FCKDocumentProcessor_CreateFakeImage('K_LISTE', oSpan.cloneNode(true));
            oImg.setAttribute('_kliste', 'true', 0);
            oImg.setAttribute('src', FCKConfig.PluginsPath + 'k_liste/images/list.png');
            oImg.setAttribute('title', oSpan.innerHTML);

            oSpan.parentNode.insertBefore(oImg, oSpan);
            oSpan.parentNode.removeChild(oSpan);
        }
    }
}

/*FCK.ContextMenu.RegisterListener( {
 AddItems : function( menu, tag, tagName )
 {
 if ( tagName == 'IMG' && tag.getAttribute( '_kliste' ) )
 {
 menu.AddSeparator() ;
 menu.AddItem( 'KPortalList', FCKLang.KListeProperties, FCKConfig.PluginsPath + 'k_liste/images/list_icone.png' ) ;
 }
 }}
 );*/