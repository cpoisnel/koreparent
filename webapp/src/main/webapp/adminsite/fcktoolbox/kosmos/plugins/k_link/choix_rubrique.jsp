<%@ page errorPage="/adminsite/jsbexception.jsp" %> 
<%@page import="java.util.Locale"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>

<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<%
ContexteUniv ctx = ContexteUtil.getContexteUniv();
Locale locale = ctx.getLocale();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<!-- link rel="stylesheet" type="text/css" href="/adminsite/general.css" media="screen" /-->
		<link rel="stylesheet" type="text/css" href="/adminsite/fcktoolbox/kosmos/skins/kportal_default/fck_dialog.css" media="screen"/>
		
		
		<link rel="stylesheet" media="screen, projection"  href="/adminsite/scripts/libs/css/jquery-ui-1.10.3.custom.css">
		<link rel="stylesheet" media="screen, projection"  href="/adminsite/scripts/libs/css/jquery.kmonoselect-0.1.0.css">
		<link rel="stylesheet" type="text/css" href="/adminsite/styles/screen.css" media="screen" />
		
		<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
		<script type="text/javascript">
			function renvoyerLienRubrique()
			{
				var oCodeRubrique = window.document.forms[0].CODE_RUBRIQUE;
				if (oCodeRubrique && oCodeRubrique.value != '')
				{
					var code =  oCodeRubrique.value;
					window.parent.CreateLienRubrique(code);
				} else {
					//on passe quand meme un code vide, le cas est géré plus loin avec un message d'erreur...
					window.parent.CreateLienRubrique('');
				}
			}
		</script>
		<title>Choix de la rubrique</title>
	</head>

	<body class="toolbox" id="body_k_plugin">
		<div id="content">
			<form action="bidon">
					<% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, "Rubrique", "rubrique", UnivFmt.CONTEXT_ZONE); %>
			</form>
		</div>
		<resources:script group="scriptsBo" locale="<%= locale.toString() %>"/>
		<script src="/adminsite/scripts/backoffice.js"></script>
	</body>
</html>
