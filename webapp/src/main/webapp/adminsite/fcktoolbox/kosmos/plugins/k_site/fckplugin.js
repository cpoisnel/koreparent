var pluginPathPlanSite = '/adminsite/fcktoolbox/kosmos/plugins/k_site/';

FCKCommands.RegisterCommand('KPortalPlanSite', new FCKDialogCommand('KPortalPlanSite', FCKLang['DlgKPortalPlanSiteTitle'], pluginPathPlanSite + 'k_site.jsp', 600, 600));

var oKPortalPlanSite = new FCKToolbarButton('KPortalPlanSite', FCKLang['KPortalPlanSite']);

oKPortalPlanSite.IconPath = pluginPathPlanSite + 'images/site_icone.png';

FCKToolbarItems.RegisterItem('KPortalPlanSite', oKPortalPlanSite);

FCKDocumentProcessor.AppendNew().ProcessDocument = function (document) {
    var aSpans = document.getElementsByTagName('SPAN');

    var oSpan;
    var i = aSpans.length - 1;
    while (i >= 0 && ( oSpan = aSpans[i--] )) {
        if (oSpan.getAttribute('ksite_span')) {
            var oImg = FCKDocumentProcessor_CreateFakeImage('K_SITE', oSpan.cloneNode(true));
            oImg.setAttribute('_ksite', 'true', 0);
            oImg.setAttribute('src', pluginPathPlanSite + 'images/site.png');
            oImg.setAttribute('title', oSpan.innerHTML);

            oSpan.parentNode.insertBefore(oImg, oSpan);
            oSpan.parentNode.removeChild(oSpan);
        }
    }
}