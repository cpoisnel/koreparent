// Register the commands.
FCKCommands.RegisterCommand('k_encadres_styles', new FCKDialogCommand('k_encadres_styles', FCKLang['DlgKEncadresStylesTitle'], FCKConfig.PluginsPath + 'k_encadres_styles/k_encadres_styles.html', 380, 450));

// Create the toolbar buttons.
var oKEncadresStyles = new FCKToolbarButton('k_encadres_styles', FCKLang['DlgKEncadresStylesTitle'], FCKLang['DlgKEncadresStylesTooltip'], FCK_TOOLBARITEM_ONLYTEXT);

// Register the toolbar items (these names will be used in the config file).
FCKToolbarItems.RegisterItem('KEncadresStyles', oKEncadresStyles);