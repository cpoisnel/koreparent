/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * 	Editor configuration settings.
 * 	
 * 	Follow this link for more information:
 * 	http://wiki.fckeditor.net/Developer%27s_Guide/Configuration/Configurations_Settings
 */

//=========================================================
//	K-PORTAL TOOLBOX PLUGINS (additional functionalities)
//=========================================================

FCKConfig.SkinPath = '/adminsite/fcktoolbox/kosmos/skins/kportal_default/';

//Set the base path for all KPortal specific files
//var sBasePath = /adminsite/fcktoolbox/kosmos/;
var sBasePath = FCKConfig.CustomConfigurationsPath.substring(0, FCKConfig.CustomConfigurationsPath.lastIndexOf('/') + 1);

//Set the path for the plugins files to use.
//FCKConfig.PluginsPath = FCKConfig.BasePath + 'plugins/' ;
FCKConfig.PluginsPath = sBasePath + 'plugins/';

if (window.parent.document.getElementsByName("CODE_OBJET")[0])
    FCKConfig.CodeObjet = window.parent.document.getElementsByName("CODE_OBJET")[0].value;
if (window.parent.document.getElementsByName("TS_CODE")[0])
    FCKConfig.Code = window.parent.document.getElementsByName("TS_CODE")[0].value;
if (window.parent.document.getElementsByName("ID_FICHE")[0])
    FCKConfig.IdFiche = window.parent.document.getElementsByName("ID_FICHE")[0].value;
if (window.parent.document.getElementsByName("LANGUE_FICHE")[0])
    FCKConfig.LangueFiche = window.parent.document.getElementsByName("LANGUE_FICHE")[0].value;

// add our plugin to the plugins list.
FCKConfig.Plugins.Add('k_link', 'fr,en');
FCKConfig.Plugins.Add('k_tags', 'fr,en');
FCKConfig.Plugins.Add('k_liste', 'fr,en');
FCKConfig.Plugins.Add('k_encadre_titre', 'fr,en');
FCKConfig.Plugins.Add('k_encadres_styles', 'fr,en');
FCKConfig.Plugins.Add('k_styles', 'fr,en');
FCKConfig.Plugins.Add('flvPlayer', 'fr,en');
FCKConfig.Plugins.Add('k_audio', 'fr,en');
FCKConfig.Plugins.Add('k_flash', '');
FCKConfig.Plugins.Add('k_image', '');
FCKConfig.Plugins.Add('k_rss', 'fr,en');
FCKConfig.Plugins.Add('k_galerie', 'fr,en');
FCKConfig.Plugins.Add('k_liste_statique', 'fr,en');

// ce plugin permet le chargement dynamique des items de la toolbar
FCKConfig.Plugins.Add('DynamicToolbar', null);

// ajout des plugins spécifiques
if (FCKConfig.CustomPlugins.length > 0) {
    tPlugins = FCKConfig.CustomPlugins.split("|");
    for (var i = 0; i < tPlugins.length; i++) {
        if (tPlugins[i].length > 0) {
            tPlugin = tPlugins[i].split(",");
            path = '';
            if (tPlugin[1]) {
                path = tPlugin[1];
            }
            FCKConfig.Plugins.Add(tPlugin[0], 'fr,en', path);
        }
    }
}

//suppression du parametre par defaut des styles
FCKConfig.CustomStyles = {};

//parametres du plugin k_encadres_styles
FCKConfig.KEncadresStylesXmlPath = sBasePath + '/encadres_styles.xml';
FCKConfig.KEncadresStyleReplaceAll = false;
FCKConfig.KEncadresStyleReplaceCheckbox = true;

//parametres du plugin k_styles
FCKConfig.KStylesXmlPath = sBasePath + '/kportalstyles.xml';
FCKConfig.KStyleReplaceAll = false;
FCKConfig.KStyleReplaceCheckbox = true;

FCKConfig.TemplateReplaceAll = false;
FCKConfig.TemplateReplaceCheckbox = true;

//Image Browser
FCKConfig.ImageBrowser = true;

//Flash manager
FCKConfig.FlashBrowser = true;
FCKConfig.FlashUpload = false;
FCKConfig.FlashDlgHideAdvanced = true;

//Flipbook manager
FCKConfig.FlipbookBrowser = true;

//parametres du plugin k_audio
FCKConfig.AudioBrowser = true;

//parametres du plugin k_video
FCKConfig.VideoBrowser = true;

FCKConfig.LinkBrowser = false;
//Le plugin k_link reprend les memes options de configuration que le plugin original fck_link
//a la différence que les noms de parametres sont prefixés par "K_"
FCKConfig.K_LinkDlgHideTarget = true;
FCKConfig.K_LinkUpload = false;
FCKConfig.K_LinkDlgHideAdvanced = true;
FCKConfig.k_LinkBrowser = false;

FCKConfig.EnterMode = 'br';
FCKConfig.IgnoreEmptyParagraphValue = false;
FCKConfig.AdditionalNumericEntities = "'|\"|<|>";
FCKConfig.ProcessNumericEntities = false;
FCKConfig.ProcessHTMLEntities = false;
FCKConfig.IncludeLatinEntities = true;
FCKConfig.IncludeGreekEntities = true;

/**OPTIONS REPRISES DE LA CONFIG FRONT **/
FCKConfig.DefaultLanguage = 'fr';
FCKConfig.UseBROnCarriageReturn = true;	// IE only.

FCKConfig.LinkDlgHideTarget = true;
FCKConfig.LinkDlgHideAdvanced = true;

FCKConfig.ImageDlgHideLink = true;
FCKConfig.ImageDlgHideAdvanced = true;

var _FileBrowserLanguage = 'php';	// asp | aspx | cfm | lasso | perl | php | py
var _QuickUploadLanguage = 'php';	// asp | aspx | cfm | lasso | php

FCKConfig.LinkUpload = false;
FCKConfig.ImageUpload = false;

//defaut context menu : 
//FCKConfig.ContextMenu = ['Generic','Link','Anchor','Image','Flash','Select','Textarea','Checkbox','Radio','TextField', 'HiddenField','ImageButton','Button','BulletedList','NumberedList','Form'] ;
FCKConfig.ContextMenu = ['Generic', 'Anchor', 'Select', 'Textarea', 'Checkbox', 'Radio', 'TextField', 'HiddenField', 'ImageButton', 'Button', 'BulletedList', 'NumberedList', 'Form', 'Table'];

//rendu personnalisé de l'editeur
FCKConfig.EditorAreaCSS = '/adminsite/fcktoolbox/kosmos/kportal_editorarea.css';

FCKConfig.SpellChecker = 'SCAYT';
FCKConfig.ScaytDefLang = 'fr_FR';
FCKConfig.ScaytAutoStartup = false;
//FCKConfig.FirefoxSpellChecker = true ;
FCKConfig.MediathequeMaxWidth = 420;
FCKConfig.FlipbookMaxWidth = 750;
FCKConfig.FlipbookMaxHeight = 450;

//cette option permet de garder la structure html des données issues d'un copier coller word
FCKConfig.CleanWordKeepsStructure = true;

// default language options:
// c++,csharp,css,delphi,java,jscript,php,python,ruby,sql,vb,xhtml
FCKConfig.SyntaxHighlight2LangDefault = 'java';
