<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.collections.CollectionUtils"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.extension.module.composant.Composant"%>
<%@page import="com.kportal.extension.module.composant.Menu"%>
<%@page import="com.kportal.ihm.utils.AdminsiteUtils"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.SessionUtil"%>
<%@page import="com.univ.utils.UnivWebFmt"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<div id="header">
    <nav id="header_deco" role="navigation"><%
        Map<String, Object> infosSession = SessionUtil.getInfosSession(request);
        Composant composantCourant = AdminsiteUtils.retrouverComposant(infoBean);
        Menu menuBO = AdminsiteUtils.getMenuPrincipalParUtilisateur(infosSession);
        if (menuBO != null && CollectionUtils.isNotEmpty(menuBO.getSousMenu())) {
            %><ul id="fonctionnalites"><%
            for (Menu menuPrincipal : menuBO.getSousMenu()) {
                if (StringUtils.isNotBlank(menuPrincipal.getUrl()) || CollectionUtils.isNotEmpty(menuPrincipal.getSousMenu())) {
                    %><li id="<%=menuPrincipal.getIdMenu()%>" class="<%=AdminsiteUtils.getClassMenuBackOffice(composantCourant,menuPrincipal)%>">
                    <a href="<%= menuPrincipal.getUrl()%>"><%= menuPrincipal.getLibelle()%></a>
                    <%
                       if (CollectionUtils.isNotEmpty(menuPrincipal.getSousMenu())) {
                           %><ul><%
                        for (Menu menuSecondaire : menuPrincipal.getSousMenu()) {
                            %><li class="<%= AdminsiteUtils.getClassMenuBackOffice(composantCourant,menuSecondaire) %>">
                                <a href="<%= menuSecondaire.getUrl()%>"><%= menuSecondaire.getLibelle()%></a>
                            </li><%
                        }
                           %></ul><%
                       }
                    %></li><%
                }
            }
        %></ul><%
        }
    if (!infosSession.isEmpty() && infosSession.get("NOM") != null) {
        String libelleUtilisateur = (String) infosSession.get( "PRENOM") + " " +  (String) infosSession.get("NOM");
        if (StringUtils.isBlank(libelleUtilisateur)) {
            libelleUtilisateur =  (String) infosSession.get("CODE");
        }
        %><div id="utilisateur" class="dropdown">
            <button class="dropdown-toggle" data-toggle="dropdown"><strong><%= libelleUtilisateur %></strong></button>
            <div class="dropdown-menu right" role="menu" aria-labelledby="dropdownMenu">
                <ul>
                    <li><a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=IDENTIFICATION&amp;ACTION=PERSONNALISER"><%= composantCourant.getMessage("BO_EDITER_PROFIL") %></a></li>
                    <li><a id="deconnexion" data-propagation="<%=AdminsiteUtils.getLiensPropagationDeconnexionFormatter() %>" href="<%=UnivWebFmt.getUrlDeconnexion(ContexteUtil.getContexteUniv(), true, false)%>"><%= composantCourant.getMessage("ST_DSI_DECONNEXION") %></a></li>
                </ul>

            </div><!-- .dropdown-menu right -->
        </div><!-- #utilisateur --><%
    }
    %></nav>
</div><!-- #header -->