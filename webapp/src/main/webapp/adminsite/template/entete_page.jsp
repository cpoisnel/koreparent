<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.extension.module.composant.Composant"%>
<%@page import="com.kportal.extension.module.composant.Menu"%>
<%@page import="com.kportal.ihm.utils.AdminsiteUtils"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.EscapeString"%>
<%@page import="com.univ.utils.SessionUtil"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
ContexteUniv ctx = ContexteUtil.getContexteUniv();
Map<String, Object> infosSession = SessionUtil.getInfosSession(request);
Menu menuAction = AdminsiteUtils.getMenuActionsCourante(infoBean,infosSession);
Composant composantCourant = AdminsiteUtils.retrouverComposant(infoBean);
String libelleComposant = composantCourant.getLibelleAffichable();
%><div id="entete">
    <div>
        <h1><a href="<%=EscapeString.escapeAttributHtml(composantCourant.getUrlAccueilBo()) %>" title="<%=libelleComposant %>"><%= libelleComposant %></a></h1><%
        if (menuAction != null && menuAction.getSousMenu() != null && menuAction.getSousMenu().size() > 0) {
            %><ul id="actions_composant"><%
                for (Menu menuCourant : menuAction.getSousMenu()) {
                    %><li class="<%= menuCourant.getVisuel() %>"><a href="<%= EscapeString.escapeAttributHtml(menuCourant.getUrl()) %>"><%= menuCourant.getLibelle() %></a></li><%
                }
            %></ul><%
        }
    %></div><!--  -->
</div><!-- #entete -->

<div id="titre_ecran">
    <h2><%= AdminsiteUtils.getTitrePageCourante(infoBean) %></h2>
    <div id="messageApplicatif">
    <%
    if (StringUtils.isNotBlank(infoBean.getMessageErreur())) {
        %><p class="message alert alert-danger fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <%=StringUtils.replace(EscapeString.escapeHtml(infoBean.getMessageErreur()),",","<br />")%>
          </p><%
    }
    if (StringUtils.isNotBlank(infoBean.getMessageConfirmation())) {
        %><p class="message alert alert-success fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <%=StringUtils.replace(EscapeString.escapeHtml(infoBean.getMessageConfirmation()),",","<br />")%>
          </p><%
    }
    if (StringUtils.isNotBlank(infoBean.getMessageAlerte())) {
        %><p class="message alert alert-warning fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <%=StringUtils.replace(EscapeString.escapeHtml(infoBean.getMessageAlerte()),",","<br />")%>
          </p><%
    }%>
    </div><!-- #messageApplicatif -->


</div><!-- #titre_ecran -->
