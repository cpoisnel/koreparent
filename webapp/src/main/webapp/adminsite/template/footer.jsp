<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.core.config.PropertyHelper"%>
<%
    String applicationVersion = StringUtils.defaultString(PropertyHelper.getCoreProperty("application.version"));
%>
<div id="footer" role="contentinfo">
    <a title="<%= String.format(MessageHelper.getCoreMessage("BO_EN_SAVOIR_PLUS_VERSION"),applicationVersion) %>" href="<%= PropertyHelper.getCoreProperty("application.url") %>"><%= PropertyHelper.getCoreProperty("application.nom") %> <%= applicationVersion %></a>
    <a href="#header" id="haut_page" title="<%= MessageHelper.getCoreMessage("ST_HAUT_PAGE") %>"><span><%= MessageHelper.getCoreMessage("ST_HAUT_PAGE") %></span></a>
</div><!-- #footer -->