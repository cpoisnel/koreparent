<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="viewModel" class="com.kosmos.components.media.view.model.ComponentMediaViewModel" scope="request" />
<div class="media-component js-media-component js-media-component--<%= viewModel.isMultiple() ? "multiple" : "unique" %>"
     data-type="<%= viewModel.getType() %>">
    <span class="label colonne"><%= viewModel.getLabel() %></span>
    <div><%
        if (!viewModel.isMultiple()) {
            %><input class="js-media-component__id-ressource" type="hidden" name="<%= viewModel.getName() %>" id="<%= viewModel.getName() %>"
                     value="<%= viewModel.getMedias().size() > 0 ? viewModel.getMedias().get(0).getId() : 0 %>"/>
                <button type="button"
                        class="rechercher media-component__select-button js-media-component__select-button"><%= MessageHelper.getCoreMessage("MEDIA_COMPONENT.BUTTON.SELECT_MEDIA") %>
                </button>
                <span class="media-component__name js-media-component__name"><%= viewModel.getMedias().size() > 0 ? viewModel.getMedias().get(0).getTitre() : StringUtils.EMPTY %></span><%
        } else {
            %><button type="button"
                      class="plus media-component__select-button js-media-component__select-button"><%= MessageHelper.getCoreMessage("MEDIA_COMPONENT.BUTTON.ADD_MEDIA") %>
            </button>
            <ul class="media-component__list js-media-component__list"><%
            if (viewModel.getMedias().size() > 0) {
                for (int i = 0; i < viewModel.getMedias().size(); i++) {
                %><li class="media-component__file js-media-component__file">
                    <span class="js-media-component__name"><%= viewModel.getMedias().get(i).getTitre() %></span>
                    <button type="button"
                            class="supprimer media-component__delete-button js-media-component__delete-button"><%= MessageHelper.getCoreMessage("MEDIA_COMPONENT.BUTTON.DELETE_MEDIA") %>
                    </button>
                </li><%
                }
            }
            %></ul>
            <input class="js-media-component__id-ressource" type="hidden" name="<%= viewModel.getName() %>" id="<%= viewModel.getName() %>"
                   value="<%= viewModel.getIds() %>"/><%
            }
        %></div>
</div><!-- .media-component -->