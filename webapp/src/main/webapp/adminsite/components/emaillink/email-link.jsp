<jsp:useBean id="href" class="java.lang.String" scope="request" />
<jsp:useBean id="label" class="java.lang.String" scope="request" />
<a href="<%= href %>" title="<%= label %>" rel="nofollow"><%= label %></a>