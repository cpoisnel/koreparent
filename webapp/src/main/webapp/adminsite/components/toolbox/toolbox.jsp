<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:useBean id="viewModel" class="com.kosmos.toolbox.component.view.model.ToolboxViewModel" scope="request" />
<div class="components__ckeditor wysiwyg">
    <input type="hidden" name="#FORMAT_${viewModel.name}" value="${viewModel.formatValue}">
    <label class="colonne label-ckeditor" for="${viewModel.name}">${viewModel.label}</label>
    <c:if test="${not empty fn:trim(viewModel.tooltip)}"><span class="ktooltip"><c:out value="${viewModel.tooltip}"/></span></c:if>
    <textarea class="js-ckeditor" id="${viewModel.name}" name="${viewModel.name}" rows="8" data-conf="${viewModel.confKey}" ${viewModel.disabled ? "disabled" : ""}>${viewModel.value}</textarea>
</div>