<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="utils" uri="http://kportal.kosmos.fr/tags/utils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kosmos.components.kselect.multiselect.MultiSelectMode" %>
<%@ taglib prefix="components" uri="http://kportal.kosmos.fr/tags/components" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<jsp:useBean id="viewModel" class="com.kosmos.components.kselect.multiselect.model.MultiKselectModel" scope="request" />
<div class="components__kmultiselect-${viewModel.mode.toString()}">
    <c:if test="${viewModel.editOption != -1}">
        <div  class="label colonne">
            <strong>${viewModel.label}</strong>
            <c:if test="${not empty fn:trim(viewModel.tooltip)}"><span class="ktooltip"><c:out value="${viewModel.tooltip}"/></span></c:if>
        </div><!--
        <c:choose>
            <c:when test="${viewModel.editOption == 0}">
                --><div id="kmultiselect${viewModel.name}" class="kmultiselect">
                    <ul class="kmultiselect__list-items">
                        <c:forEach items="${viewModel.items}" var="currentItem">
                            <li class="kmultiselect__item">${currentItem.title}</li>
                        </c:forEach>
                    </ul>
                </div>
            </c:when>
            <c:otherwise>
                --><div id="kmultiselect${viewModel.name}" class="kmultiselect-${viewModel.mode.toString()} ${viewModel.classCss}" ${viewModel.attributes} data-addaction="${viewModel.action}" data-popintitle="${viewModel.popinTitle}" data-popinwidth="${viewModel.popinWidth}" data-popinvalidate="${viewModel.popinValidate}">
                    <input name="#FORMAT_${viewModel.name}" value="<c:out value="${viewModel.formatValue}"/>" type="hidden">
                    <input id="${viewModel.name}" name="${viewModel.name}" value="<c:out value="${viewModel.concatenatedSelectedValues}"/>" type="hidden">
                    <input name="LIBELLE_${viewModel.name}" value="<c:out value="${viewModel.concatenatedSelectedValuesLabels}"/>" type="hidden"><!--
                    <c:if test="<%= viewModel.getMode() == MultiSelectMode.ttl %>">
                        --><input name="ARIANE_${viewModel.name}" value="<c:out value="${viewModel.ariane}"/>" type="hidden"><!--
                    </c:if>
                    <c:if test="<%= viewModel.getMode() == MultiSelectMode.ltl %>">
                        --><div class="kscrollable">
                            <ul class="ui-sortable kmultiselect-list kmultiselect-parent-list"><!--
                                <c:forEach items="${viewModel.items}" var="currentItem">
                                    --><li class="kmultiselect__item" data-value="${currentItem.value}" title="<c:out value="${currentItem.title}"/>"><c:out value="${currentItem.title}"/></li><!--
                                </c:forEach>
                            --></ul>
                        </div><!--
                    </c:if>
                    --><div class="kscrollable">
                        <ul class="ui-sortable kmultiselect-list kmultiselect-composition-list">
                        </ul>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </c:if>
</div>