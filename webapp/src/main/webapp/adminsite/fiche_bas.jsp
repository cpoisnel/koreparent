<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.core.InfoBean" %>
<%@page import="com.jsbsoft.jtf.core.LangueUtil"%>
<%@page import="com.kosmos.service.impl.ServiceManager"%>
<%@page import="com.kportal.extension.module.composant.ComposantUtilisateur"%>
<%@page import="com.univ.objetspartages.bean.HistoriqueBean"%>
<%@page import="com.univ.objetspartages.bean.ReferenceBean"%>
<%@page import="com.univ.objetspartages.bean.UtilisateurBean" %>
<%@page import="com.univ.objetspartages.om.AutorisationBean"%>
<%@page import="com.univ.objetspartages.om.EtatFiche" %>
<%@page import="com.univ.objetspartages.services.ServiceUser" %>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@ page import="com.univ.utils.FicheUnivHelper" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<%@ taglib prefix="components" uri="http://kportal.kosmos.fr/tags/components" %>
<%
    final ServiceUser serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
%>
<%--
***************************************************************************
*********************       ONGLET ENCADRES         ***********************
***************************************************************************
--%>
<% if (sousOnglet.equals("plugins")) { %>
    <%@ include file="/adminsite/objetspartages/fiche_plugins.jsp" %>
<% } %>

<% if ("1".equals(infoBean.getString("GRS_SAISIE_ENCADRES")) && sousOnglet.equals("encadres")) { %>
    <div class="fieldset neutre">
        <components:toolbox fieldName="CONTENU_ENCADRE" label="BO_FICHE_CONTENU_ENCADRE" min="0" max="8192" editOption='<%= FormateurJSP.SAISIE_FACULTATIF %>'/><%
        if (infoBean.get("CODE_OBJET").equals( "0016")) {
            // saisie des encadrés de recherche pour les pages libres
            univFmt.insererComboHashtable(fmt, out, infoBean, "ENCADRE_RECHERCHE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_ENCADRES_RECHERCHE", MessageHelper.getCoreMessage("BO_FICHE_ENCADRE_RECHERCHE"));
            univFmt.insererComboHashtable(fmt, out, infoBean, "ENCADRE_RECHERCHE_BIS", FormateurJSP.SAISIE_FACULTATIF, "LISTE_ENCADRES_RECHERCHE", MessageHelper.getCoreMessage("BO_FICHE_ENCADRE_RECHERCHE_2"));
        } %>
    </div>
<% }

/**
***************************************************************************
*********************         ONGLET SUIVI          ***********************
***************************************************************************
*/
if (sousOnglet.equals("suivi")) {
    %><fieldset>
        <legend><%=MessageHelper.getCoreMessage("BO_FICHE_CYCLE_VIE")%></legend><%
        univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_CREATION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_DATE_CREATION"));
        univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_MODIFICATION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_DERNIERE_MODIF"));
        if (EtatFiche.A_VALIDER.getEtat().equals(infoBean.get("ETAT_OBJET"))) {
            univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_PROPOSITION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_DATE_DEMANDE_VALIDATION"));
        }
        univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_VALIDATION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_DATE_VALIDATION"));
        univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_MISE_EN_LIGNE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_DATE_MISE_EN_LIGNE"));
        univFmt.insererChampSaisie(fmt, out, infoBean, "HEURE_MISE_EN_LIGNE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_HEURE_MINUTE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_HEURE_MISE_EN_LIGNE"));
        if (!(EtatFiche.A_SUPPRIMER.getEtat().equals(infoBean.get("ETAT_OBJET"))) && !(EtatFiche.ARCHIVE.getEtat().equals(infoBean.get("ETAT_OBJET")))){
            univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_ARCHIVAGE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_DATE_ARCHIVAGE"));
        }
        if (!(EtatFiche.A_SUPPRIMER.getEtat().equals(infoBean.get("ETAT_OBJET"))) && !(EtatFiche.SAUVEGARDE_AUTO.getEtat().equals(infoBean.get("ETAT_OBJET")))) {
            univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_SUPPRESSION", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_DATE_SUPPRESSION"));
        }
        if (InfoBean.ETAT_OBJET_MODIF.equals(infoBean.getEtatObjet())) {
            final AutorisationBean autorisations = ContexteUtil.getContexteUniv().getAutorisation();
            if (ComposantUtilisateur.isAutoriseParActionProcessusEtEcranLogique(autorisations, "RECHERCHER", StringUtils.EMPTY)){
                univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_REDACTEUR", FormateurJSP.SAISIE_OBLIGATOIRE, MessageHelper.getCoreMessage("BO_FICHE_LIBELLE_REDACTEUR"), "utilisateur", UnivFmt.CONTEXT_ZONE);
            } else {
                univFmt.insererChampSaisie(fmt, out, infoBean, "LIBELLE_CODE_REDACTEUR", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_LIBELLE_REDACTEUR"));
            }
        }
        // Case à cocher pour visibilite des structures dans l'arbre
        if( (infoBean.get("IN_TREE_ACTIF")) != null) {
            fmt.insererChampSaisie( out, infoBean, "IN_TREE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0);%>
            <label for="IN_TREE" class="colonne"><%=MessageHelper.getCoreMessage("BO_FICHE_VISIBLE_FO")%></label>
        <%
        }
        %></fieldset><fieldset>
            <legend><%=MessageHelper.getCoreMessage("BO_FICHE_HISTORIQUE")%></legend><%
        List<HistoriqueBean> historique = infoBean.getList("HISTORIQUE");
        %>
        <table class="datatableHistorique">
            <thead>
                <tr>
                    <th><%= MessageHelper.getCoreMessage("BO_DATE") %></th>
                    <th><%= MessageHelper.getCoreMessage("BO_LOGIN") %></th>
                    <th><%= MessageHelper.getCoreMessage("BO_ACTIONS") %></th>
                    <th><%= MessageHelper.getCoreMessage("BO_ETAT") %></th>
                </tr>
            </thead>
            <tbody><%
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Map<String, String> classParEtat = FicheUnivHelper.getClassEtatsObjet();
            for (HistoriqueBean hist : historique) {
                final UtilisateurBean utilisateurCourant = serviceUser.getByCode(hist.getUtilisateur());
                final String etat = classParEtat.get(hist.getEtat().toString());
                String libelleUtilisateur = StringUtils.EMPTY;
                if(utilisateurCourant != null){
                    libelleUtilisateur = utilisateurCourant.getPrenom() + " " + utilisateurCourant.getNom();
                }else{
                    if(hist.getUtilisateur().equals("nobody")){
                        libelleUtilisateur = MessageHelper.getCoreMessage("BO_FICHE_HISTORIQUE_SCRIPT");
                    }
                }
                %><tr>
                    <td><%= formatDate.format(hist.getDateAction()) %></td>
                    <td><%= libelleUtilisateur %></td>
                    <td><%= hist.getAction() %></td>
                    <td><span class="<%= etat %>"><%= MessageHelper.getCoreMessage("ETATFICHE_" + hist.getEtat().toString()) %></span></td>
                </tr><%
            }%>
            </tbody>
        </table>
        </fieldset>
        <fieldset>
            <legend><%=MessageHelper.getCoreMessage("BO_FICHE_LIENS_VERS_FICHE")%></legend><%
        List<ReferenceBean> references = infoBean.getList("REFERENCE");
        %>
        <table class="datatableLight">
            <thead>
                <tr>
                    <th><%= MessageHelper.getCoreMessage("BO_INTITULE") %></th>
                    <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_TYPE") %></th>
                    <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_LANGUE") %></th>
                    <th><%= MessageHelper.getCoreMessage("BO_ETAT") %></th>
                </tr>
            </thead>
            <tbody><%
            for (ReferenceBean reference : references) {
                final String etat = MessageHelper.getCoreMessage("ETATFICHE_" + reference.getEtat().toString());
                final Locale locale = LangueUtil.getLocale(reference.getLangue());
                %><tr>
                    <td><%= reference.getTitre() %></td><%
                    %><td><%= reference.getType() %></td><%
                    %><td><img src="/adminsite/images/langues/<%= locale.getLanguage() %>/drapeau.png" alt="<%= locale %>"/></td><%
                    %><td><span class="<%= StringUtils.deleteWhitespace(etat).toLowerCase()%>"><%= etat %></span></td>
                </tr><%
            }%>
            </tbody>
        </table>
        </fieldset><%
        if (!infoBean.get("CODE_OBJET").equals("0006")) {
            %><fieldset>
                <legend><%=MessageHelper.getCoreMessage("BO_FICHE_CHANGEMENT_RUBRIQUE_AUTO")%></legend><%
                univFmt.insererChampSaisie( fmt, out, infoBean, "DATE_RUBRIQUAGE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_DATE_RUBRIQUAGE"));
                univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUAGE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_FICHE_RUBRIQUE_DEST"), "rubbo/FICHE/"+ infoBean.getString("CODE_OBJET") + "/M/", UnivFmt.CONTEXT_ZONE);
            %></fieldset><%
        }
        %><fieldset>
            <legend><%=MessageHelper.getCoreMessage("BO_FICHE_ALERTE")%></legend><%
        univFmt.insererChampSaisie( fmt, out, infoBean, "DATE_ALERTE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_FICHE_DATE_ALERTE"));
        univFmt.insererChampSaisie( fmt, out, infoBean, "MESSAGE_ALERTE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 256, MessageHelper.getCoreMessage("BO_FICHE_MESSAGE_ALERTE"));
        %></fieldset><%
    }
/**
***************************************************************************
********************       ONGLET DIFFUSION         ***********************
***************************************************************************
*/
    if( sousOnglet.equals("diffusion"))    {
        if ("1".equals(infoBean.getString("GRS_SAISIE_DIFFUSION"))) {
            %><script type="text/javascript">
                function affichePublicVise(){
                    for (i=0;i<document.forms[0].DIFFUSION_MODE_RESTRICTION.length;i++){
                        if (document.forms[0].DIFFUSION_MODE_RESTRICTION[i].checked)
                            mode=document.forms[0].DIFFUSION_MODE_RESTRICTION[i].value;
                    }
                    if (mode=='3')
                        window.document.getElementById('restriction-groupe').style.display = 'block';
                    else
                        window.document.getElementById('restriction-groupe').style.display = 'none';
                }
            </script>
            <fieldset>
                <legend><%=MessageHelper.getCoreMessage("BO_FICHE_PUBLICS")%></legend><%
            if( !infoBean.getString("GRS_SAISIE_COLLABORATIF").equals("1"))    {
                %>
                <p class="fieldset_titre"><%= MessageHelper.getCoreMessage("BO_FICHE_PUBLICS_VISES") %></p>
                <p><%= MessageHelper.getCoreMessage("BO_FICHE_SELECTION_GROUPE_USER") %></p>
                <%
                    univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "PUBLIC_VISE_DSI", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_FICHE_LISTE_PUBLIC_VISES"), "TMP_PUBLIC_VISE_DSI", MessageHelper.getCoreMessage("BO_FICHE_PUBLIC_VISES"), UnivFmt.CONTEXT_GROUPEDSI_PUBLIC_VISE, "");
                String diffusionModerestriction = infoBean.getString("DIFFUSION_MODE_RESTRICTION");
                %>
                <br/>
                <div>
                    <p class="fieldset_titre"><%= MessageHelper.getCoreMessage("BO_FICHE_PUBLIC_AUTORISES") %></p>
                    <br/>
                    <p>
                        <input type="radio" <%= diffusionModerestriction.equals("0") ? " checked=\"checked\"" : "" %> value="0" name="DIFFUSION_MODE_RESTRICTION" id="DIFFUSION_MODE_RESTRICTION_0" class="radio">
                        <label for="DIFFUSION_MODE_RESTRICTION_0" ><%= MessageHelper.getCoreMessage("BO_FICHE_TOUT_PUBLIC") %></label></p>
                    <p>
                        <input type="radio" <%= diffusionModerestriction.equals("2") ? " checked=\"checked\"" : "" %> value="2" name="DIFFUSION_MODE_RESTRICTION" id="DIFFUSION_MODE_RESTRICTION_2" class="radio">
                        <label for="DIFFUSION_MODE_RESTRICTION_2" ><%= MessageHelper.getCoreMessage("BO_FICHE_RESTREINDRE_PUBLICS_VISES") %></label>
                    </p>
                    <p>
                        <input type="radio" <%= diffusionModerestriction.equals("3") ? " checked=\"checked\"" : "" %> value="3" name="DIFFUSION_MODE_RESTRICTION" id="DIFFUSION_MODE_RESTRICTION_3" class="radio">
                        <label for="DIFFUSION_MODE_RESTRICTION_3" ><%= MessageHelper.getCoreMessage("BO_FICHE_RESTREINDRE_GROUPES") %></label>
                    </p>
                    <div id="restriction-groupe" style="display:none;">
                    <%
                        univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "PUBLIC_VISE_DSI_RESTRICTION", FormateurJSP.SAISIE_FACULTATIF, "", "TMP_PUBLIC_VISE_DSI_RESTRICTION", MessageHelper.getCoreMessage("BO_FICHE_LISTE_PUBLIC_VISES"), UnivFmt.CONTEXT_GROUPEDSI_RESTRICTION, "");
                    %>
                    </div>
                </div>
                <script type="text/javascript">
                    affichePublicVise();
                    for (i=0;i<document.forms[0].DIFFUSION_MODE_RESTRICTION.length;i++)
                        window.document.forms[0].DIFFUSION_MODE_RESTRICTION[i].onclick=affichePublicVise;
                </script><%
            } else { // collab
                %>
                    <p><em><%= MessageHelper.getCoreMessage("BO_FICHE_RESTREINDRE_ESPACE_CO") %></em>
                    &nbsp;<%=infoBean.getString("PUBLIC_VISE_LIBELLE_ESPACE")%></p>
                <%
            }
            %>
            </fieldset><%
        }
        if ("1".equals(infoBean.getString("GRS_SAISIE_REFERENCEMENT"))) {
            %><fieldset>
                <legend><%=MessageHelper.getCoreMessage("BO_FICHE_REFERENCEMENT")%></legend><%
                univFmt.insererChampSaisie(fmt, out, infoBean, "META_KEYWORDS", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 1024, MessageHelper.getCoreMessage("BO_FICHE_MOTS_CLES"));
                univFmt.insererChampSaisie(fmt, out, infoBean, "META_DESCRIPTION", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, MessageHelper.getCoreMessage("BO_FICHE_BALISE_DESCRIPTION"));
            %></fieldset><%
        }
        if ("1".equals(infoBean.getString("GRS_SAISIE_PUBLICATION"))) {
            %><fieldset>
                <legend><%=MessageHelper.getCoreMessage("BO_FICHE_PUBLIER_AUTRE_RUB")%></legend><%
                univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "RUBRIQUE_PUBLICATION", FormateurJSP.SAISIE_FACULTATIF, "", "TMP_RUBRIQUE_PUBLICATION", MessageHelper.getCoreMessage("BO_FICHE_RUBRIQUE"), UnivFmt.CONTEXT_ZONE, "rubbo/TECH/pub//");
            %></fieldset><%
        }
    }
    %>
        <input type="hidden" name="GRS_SAISIE_PUBLICATION" value="<%= infoBean.getString("GRS_SAISIE_PUBLICATION") %>" />
        <br style="clear: both;" />
    </div> <!-- .form_temp --><%
    if (! infoBean.get("ETAT_OBJET").equals(EtatFiche.SAUVEGARDE_AUTO.getEtat())) {
        // Pas d'enregistrement pour les sauvegardes automatiques
        // --------------------- ZONE ENREGISTREMENT = WORKFLOW DE VALIDATION + ENREGISTREMENT ---------------------
        %><div id="console"><%
        // --------------------- Workflow de validation  ---------------------
         if ("1".equals(infoBean.getString("GRS_SAISIE_WORKFLOW"))) { %>
            <div id="conteneur_workflow">
                <fieldset>
                    <legend><%= MessageHelper.getCoreMessage("BO_FICHE_WORKFLOW_VALIDATION") %></legend>
                    <script type="text/javascript">
                    <!--
                        function grabObject (id) {
                            return document.getElementById(id);
                        }
                        function show_hide(id) {
                            var hdn_div = grabObject('hdn'+id);
                            if(hdn_div.style.display == 'block') {
                                hdn_div.style.display = 'none';
                            } else {
                                hdn_div.style.display = 'block';
                            }
                        }
                        function open_div(number, default_status) {
                            document.writeln("<div id='hdn" + number + "' name='hdn" + number + "' class='normaltext' style='display: " + default_status + ";'>");
                        }
                        function close_div(number, default_status) {
                            document.writeln("</div>");
                        }
                    //-->
                    </script>
                    <div><a name="WORKFLOW" href="#WORKFLOW" onclick="show_hide(1);"><%= MessageHelper.getCoreMessage("BO_FICHE_AFFICHER_WORKFLOW") %></a></div><br />
                    <script type="text/javascript">open_div(1,'none')</script>
                    <table>
                        <% if (infoBean.getString("NIVEAU_APPROBATION").length() > 0) { %>
                        <tr>
                                <td><%= MessageHelper.getCoreMessage("BO_FICHE_ETAT_COURANT") %></td>
                                <td><em><%= MessageHelper.getCoreMessage("BO_FICHE_A_VALIDER") %> : <%=infoBean.getString("LIBELLE_APPROBATION")%></em></td>
                        </tr>
                        <% } else {
                            univFmt.insererComboHashtable(fmt, out, infoBean, "ETAT_OBJET", FormateurJSP.SAISIE_AFFICHAGE, ReferentielObjets.getEtatsObjet(), MessageHelper.getCoreMessage("BO_FICHE_ETAT_COURANT"));
                           } %>
                        <tr>
                            <td><%= MessageHelper.getCoreMessage("BO_FICHE_NIVEAU_VALIDATION") %></td>
                            <td><input class="ChampSaisie" type="text" name="NIVEAU_VALIDATION" value="<%= MessageHelper.getCoreMessage("BO_FICHE_AUTOMATIQUE") %>"  size="50" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td><%= MessageHelper.getCoreMessage("BO_FICHE_VALIDATEURS_NOTIFIES") %></td>
                            <td><textarea class="ChampSaisie" name=VALIDATEURS cols="40" rows="6" wrap="soft" readonly><%= MessageHelper.getCoreMessage("BO_FICHE_LISTE_DEFAUT") %></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="3">&nbsp;</td>
                        </tr>
                            <% if (infoBean.getString("AFFICHER_CHOISIR_VALIDATEUR").equals("1")) { %>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input class="bouton" type="button" name="ENREGISTRER" value="<%= MessageHelper.getCoreMessage("BO_FICHE_MODIFIER") %>" onclick="parametrerValidation();"/>
                                <input class="bouton" type="button" name="ENREGISTRER" value="<%= MessageHelper.getCoreMessage("BO_FICHE_VALEUR_DEFAUT") %>" onclick="annulerParametrage();"/>
                            </td>
                        </tr>
                        <% } %>
                        <tr>
                            <td colspan="3">&nbsp;</td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        function annulerParametrage(){
                            window.document.forms[0].VALIDATEURS.value= '[liste par défaut]';
                            window.document.forms[0].NIVEAU_VALIDATION.value= '[automatique]';
                            window.document.forms[0].NIVEAU_APPROBATION_DEMANDE.value= '';
                            window.document.forms[0].LISTE_VALIDATEURS.value= '';
                        }
                        function parametrerValidation(){
                            if (window.document.forms[0].MODE_BROUILLON){
                                if (window.document.forms[0].MODE_BROUILLON.checked){
                                    alert("<%= MessageHelper.getCoreMessage("BO_FICHE_PAS_WORKFLOW") %>");
                                    return false;
                                }
                            }
                            codeObjet= '';
                            codeRubrique='';
                            codeRattachement='';
                            publicVise='<%=infoBean.getString("PUBLIC_VISE_DSI")%>';
                            codeApprobation='<%=infoBean.getString("NIVEAU_APPROBATION")%>';
                            Xmas95 =new Date();
                            secs = Xmas95.getSeconds();<%
                            if (infoBean.get("NOM_CODE_RUBRIQUE")!=null) { %>
                                nomCodeRubrique = '<%=infoBean.getString("NOM_CODE_RUBRIQUE")%>';
                                codeRubrique = '<%=infoBean.getString(infoBean.getString("NOM_CODE_RUBRIQUE"))%>'; <%
                            }
                            if (infoBean.get("NOM_CODE_RATTACHEMENT")!=null) { %>
                                nomCodeRattachement = '<%=infoBean.getString("NOM_CODE_RATTACHEMENT")%>';
                                codeRattachement = '<%=infoBean.getString(infoBean.getString("NOM_CODE_RATTACHEMENT"))%>'; <%
                            }
                            %>var name = "win" + secs;
                            if( window.document.forms[0].CODE_OBJET)
                                codeObjet=window.document.forms[0].CODE_OBJET.value;
                            if( eval("window.document.forms[0]."+nomCodeRubrique))
                                codeRubrique = eval("window.document.forms[0]."+nomCodeRubrique+".value");
                            if( eval("window.document.forms[0]."+nomCodeRattachement))
                                codeRattachement = eval("window.document.forms[0]."+nomCodeRattachement+".value");
                            if( window.document.forms[0].PUBLIC_VISE_DSI)
                                publicVise=window.document.forms[0].PUBLIC_VISE_DSI.value;
                            if( window.document.forms[0].NIVEAU_APPROBATION)
                                codeApprobation=window.document.forms[0].NIVEAU_APPROBATION.value;
                            window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=VALIDATION&ACTION=APPROBATION&NIVEAU_APPROBATION="+codeApprobation+"&CODE_OBJET="+codeObjet+"&CODE_RUBRIQUE="+codeRubrique+"&CODE_RATTACHEMENT="+codeRattachement+"&PUBLIC_VISE_DSI="+publicVise, name, 'status=yes,toolbar=no,scrollbars=yes,width=600,height=400');
                        }
                    </script>
                    <script type="text/javascript">close_div();</script>
                </fieldset>
            </div><!--  #conteneur_workflow --><%
        }
        %></div> <!-- #console --><%
    } // Pas d'enregistrement pour les sauvegardes automatiques
%>
</div> <!-- #content -->
