<%@page import="java.text.DateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="org.apache.commons.collections.CollectionUtils"%>
<%@page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="com.jsbsoft.jtf.core.LangueUtil"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.kportal.core.config.PropertyHelper"%>
<%@page import="com.univ.objetspartages.bean.MetatagBean"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<%@page import="com.univ.objetspartages.util.FichesAValiderUtil"%>
<%@page import="com.univ.objetspartages.util.ReferentielObjetsUtil"%>
<%@page import="com.univ.rss.RSSBean" %>
<%@page import="com.univ.rss.RequeteMultiRSS" %>
<%@page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.FicheUnivHelper" %>
<div id="content" class="accueil" role="main"><%
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    List<MetatagBean> mesFiches = ReferentielObjetsUtil.getListeMesFichesRecentes(ctx, 5);
    int blocsAffiches = 2;
%>
    <div class="">
        <h3><%= MessageHelper.getCoreMessage("ST_REQUETEUR_TITRE_MES_FICHES") %></h3>
        <div><%
            List<Objetpartage> objetsCreable = ReferentielObjetsUtil.getListeFichesCreableBO(ctx);
            Map<String, String> classParEtat = FicheUnivHelper.getClassEtatsObjet();
            if (CollectionUtils.isNotEmpty(objetsCreable)) {
                %><div class="dropdown_deco">
                    <div class="dropdown">
                        <button type="button"  class="dropdown-toggle" data-toggle="dropdown"><%= MessageHelper.getCoreMessage("DEFAUT.PRINCIPAL.AJOUTER") %></button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu"><%
                            for (Objetpartage objetCourant : objetsCreable) {
                                %><li><a href="<%= ProcessusHelper.getUrlProcessAction(null, objetCourant.getIdExtension(), objetCourant.getParametreProcessus(), "AJOUTER", new String[][]{}) %>" title="<%= MessageHelper.getCoreMessage("BO_AJOUTER") %>"><%= objetCourant.getLibelleAffichable()%></a></li><%
                            } %>
                        </ul>
                    </div><!-- .dropdown -->
                </div><!-- .dropdown_deco --><%
            } %>
            <div>
                <ul class="riche"><%
                    for (MetatagBean meta : mesFiches) {
                        Objetpartage objet = ReferentielObjets.getObjetByCode(meta.getMetaCodeObjet());
                        %><li>
                            <% if (LangueUtil.getLocales().size() > 1) { %>
                            <img class="drapeau" alt="<%= LangueUtil.getDisplayName(meta.getMetaLangue()) %>" title="<%= LangueUtil.getDisplayName(meta.getMetaLangue()) %>" src="<%= LangueUtil.getPathImageDrapeau(meta.getMetaLangue()) %>" />
                            <% } %>
                            <a href="/servlet/com.jsbsoft.jtf.core.SG?EXT=<%= objet.getIdExtension() %>&PROC=<%= ReferentielObjets.getProcessus(meta.getMetaCodeObjet()) %>&ACTION=MODIFIER&ID_FICHE=<%= meta.getMetaIdFiche() %>"
                               title="<%= meta.getMetaLibelleFiche() %>"><%=  meta.getMetaLibelleFiche() %></a>
                                <span class="type">
                                    <%= objet.getLibelleObjet() %>
                                    <span class="etat"><span title="<%= MessageHelper.getCoreMessage("ETATFICHE_" + meta.getMetaEtatObjet()) %>" class="<%= classParEtat.get(meta.getMetaEtatObjet()) %>"></span></span>
                                </span>
                        </li><%
                    }
                    %>
                </ul>
                <% if (mesFiches.size() > 0) { %>
                <p class="plus">
                    <strong><a href="/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=RECHERCHE_DIRECTE&%23ECRAN_LOGIQUE%23=RECHERCHE&DE_MOI=1&CODE_OBJET=0000"><%= String.format(MessageHelper.getCoreMessage("BO_LISTE_COMPLETE"),ReferentielObjetsUtil.compterMesFiches(ctx)) %></a></strong>
                </p>
                <% } %>
            </div>
        </div>
    </div><!-- . -->
    <jsp:include page="/adminsite/template/recherche_back.jsp"></jsp:include>

    <%
        List<MetatagBean> metasAValider = FichesAValiderUtil.getObjetsAValider(ctx.getAutorisation());
        if (CollectionUtils.isNotEmpty(metasAValider)) {
            List<MetatagBean> metasAAfficher = metasAValider.subList(0, Math.min(metasAValider.size(), 5));
            blocsAffiches++;
    %>
    <div id="fiches_a_valider">
        <h3><%= MessageHelper.getCoreMessage("BO_FICHES_A_VALIDER") %></h3>
        <div>
            <ul class="fiches-a-valider riche">
                <%
                    for (MetatagBean fiche: metasAAfficher) {
                        Objetpartage objet = ReferentielObjets.getObjetByCode(fiche.getMetaCodeObjet());
                %>
                <li>
                    <% if (LangueUtil.getLocales().size() > 1) { %>
                    <img class="drapeau" alt="<%= LangueUtil.getDisplayName(fiche.getMetaLangue()) %>" title="<%= LangueUtil.getDisplayName(fiche.getMetaLangue()) %>" src="<%= LangueUtil.getPathImageDrapeau(fiche.getMetaLangue()) %>" />
                    <% } %>
                    <a href="/servlet/com.jsbsoft.jtf.core.SG?EXT=<%= objet.getIdExtension() %>&PROC=<%= ReferentielObjets.getProcessus(fiche.getMetaCodeObjet()) %>&ACTION=MODIFIER&ID_FICHE=<%= fiche.getMetaIdFiche() %>" title="<%= fiche.getMetaLibelleFiche() %>"><%= fiche.getMetaLibelleFiche() %></a>
                        <span class="type">
                            <%= objet.getLibelleObjet() %>
                            <span class="etat"><span title="<%= MessageHelper.getCoreMessage("ETATFICHE_" + fiche.getMetaEtatObjet()) %>" class="<%= classParEtat.get(fiche.getMetaEtatObjet()) %>"></span></span>
                        </span>
                </li>
                <%
                    }
                %>
            </ul>

            <p class="plus">
                <strong><a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=VALIDATION&ACTION=LISTE"><%= String.format(MessageHelper.getCoreMessage("BO_LISTE_COMPLETE"), metasAValider.size()) %></a></strong>
            </p>
        </div>
    </div><!-- #fiches_a_valider -->
    <%
    } else {
        blocsAffiches++;
    %>
    <div id="dernieres_modifications">
        <h3><%= MessageHelper.getCoreMessage("BO_DERNIERES_MODIFICATIONS") %></h3>
        <div>
        <%
            List<MetatagBean> dernieresModifications = ReferentielObjetsUtil.getListeDernieresModifications(ctx, 5);
            if (dernieresModifications.size() > 0) {
        %>
            <ul class="dernieres-modifications riche">
                <%
                    for (MetatagBean metatag : dernieresModifications) {
                        Objetpartage objet = ReferentielObjets.getObjetByCode(metatag.getMetaCodeObjet());
                %>
                <li>
                    <% if (LangueUtil.getLocales().size() > 1) { %>
                    <img class="drapeau" alt="<%= LangueUtil.getDisplayName(metatag.getMetaLangue()) %>" title="<%= LangueUtil.getDisplayName(metatag.getMetaLangue()) %>" src="<%= LangueUtil.getPathImageDrapeau(metatag.getMetaLangue()) %>" />
                    <% } %>
                    <a href="/servlet/com.jsbsoft.jtf.core.SG?EXT=<%= objet.getIdExtension() %>&ECRANVALIDER=TRUE&PROC=<%= ReferentielObjets.getProcessus(metatag.getMetaCodeObjet()) %>&ACTION=MODIFIER&ID_FICHE=<%= metatag.getMetaIdFiche() %>" title="<%= metatag.getMetaLibelleFiche() %>"><%= metatag.getMetaLibelleFiche() %></a>
                        <span class="type">
                            <%= objet.getLibelleObjet() %>
                            <span class="etat"><span title="<%= MessageHelper.getCoreMessage("ETATFICHE_" + metatag.getMetaEtatObjet()) %>" class="<%= classParEtat.get(metatag.getMetaEtatObjet()) %>"></span></span>
                        </span>
                </li>
                <%
                    }
                %>
            </ul>


        <% } else {%>
            <span><%=MessageHelper.getCoreMessage("BO_NO_CONTENU_MODIFIABLE")%></span>
            <% } %>
        </div>
    </div><!-- #fiches_a_valider -->
    <% }

        RequeteMultiRSS requete = new RequeteMultiRSS();
        String urlProvider = PropertyHelper.getCoreProperty("accueil.rss.provider");
        String urlReading = PropertyHelper.getCoreProperty("accueil.rss.lienConsultation");
        List<RSSBean> vRss = requete.lancerRequetes(urlProvider);
        vRss = vRss.subList(0,Math.min(vRss.size(),3));
        DateFormat dateFormatLong = DateFormat.getDateInstance(DateFormat.SHORT, ctx.getLocale());
        if (1 == blocsAffiches % 2) {
    %><div class="" id="bulletin_info">
        <h3><%= MessageHelper.getCoreMessage("BO_BULLETIN_INFOS") %></h3>
        <div>
            <ul class="riche"><%
                for (RSSBean rss : vRss) { %>
                <li>
                    <span class="type"><%= dateFormatLong.format(rss.getDate()) %></span>
                    <p><a class="title" href="<%= rss.getLink() %>"><%= rss.getTitle() %></a><br/><%
                                String resume = rss.getDescription().replaceAll("\\<.*?>","");
                            %><%= StringUtils.abbreviate(resume, 140)%>
                </li>
                <% } %>
            </ul>
            <p class="plus">
                <strong><a href="<%= urlReading %>" class=""><%= MessageHelper.getCoreMessage("BO_TOUS_BULLETINS") %></a></strong>
            </p>

        </div>
    </div><!-- #bulletin_info -->
    <% } %>

</div><!-- #content -->