<%@ page import="com.univ.rss.ExportRssFactory, com.univ.utils.ContexteUniv"%>
<%@ page import="com.univ.utils.ContexteUtil" %><%
    out.clear();
    response.setContentType("text/xml;charset=UTF-8");
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    ExportRssFactory.getInstance().create(request.getQueryString()).export(ctx, out);
%>
