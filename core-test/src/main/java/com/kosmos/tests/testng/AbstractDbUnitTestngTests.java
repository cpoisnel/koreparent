package com.kosmos.tests.testng;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Classe abstraite permettant de faciliter les TU sur les DAO en créant/supprimant les tables nécessaires au test.
 */
@ContextConfiguration
public abstract class AbstractDbUnitTestngTests extends AbstractTestNGSpringContextTests {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractDbUnitTestngTests.class);

    @Autowired
    protected DataSource dataSource;

    /**
     * Ce constructeur sert juste à lancer le serveur nécessaire au TU.
     */
    public AbstractDbUnitTestngTests() {
        try {
            Server.createTcpServer().start();
        } catch (SQLException e) {
            LOG.error("Une erreur est survenue lors de la création des tables associées au test.", e);
        }
    }

    /**
     * Permet d'initialiser la/les table(s) nécessaire pour le TU.
     * On cherche d'abord un fichier du type NomClass.tables.sql puis si il n'est pas trouvé, tables.sql
     */
    @BeforeClass(dependsOnMethods = "springTestContextPrepareTestInstance", alwaysRun = true)
    public void initTables() {
        try (InputStream stream = this.getClass().getResourceAsStream(this.getClass().getSimpleName() + ".tables.sql")) {
            if (stream != null) {
                runScript(this.getClass().getSimpleName() + ".tables.sql");
            } else {
                runScript("tables.sql");
            }
        } catch (IOException e) {
            LOG.debug("unable to close the stream", e);
            Assert.fail("unable to close the stream");
        }
    }

    /**
     * Permet de supprimer les tables précedement créer pour le TU.
     * On cherche d'abord un fichier du type NomClass.reset.sql puis si il n'est pas trouvé, reset.sql
     */
    @AfterClass(dependsOnMethods = "springTestContextAfterTestClass", alwaysRun = true)
    public void resetTables() {
        try (InputStream stream = this.getClass().getResourceAsStream(this.getClass().getSimpleName() + ".reset.sql")) {
            if (stream != null) {
                runScript(this.getClass().getSimpleName() + ".reset.sql");
            } else {
                runScript("reset.sql");
            }
        } catch (IOException e) {
            LOG.debug("unable to close the stream", e);
            Assert.fail("unable to close the stream");
        }
    }

    private void runScript(String scriptPath) {
        try (Connection connection = dataSource.getConnection();
             InputStream stream = this.getClass().getResourceAsStream(scriptPath)) {
            if (stream == null) {
                Assert.fail(String.format("Impossible de lire le script SQL %s", scriptPath));
            }
            try (PreparedStatement statement = connection.prepareStatement(IOUtils.toString(stream));) {
                statement.executeUpdate();
            }
        } catch (SQLException | IOException e) {
            Assert.fail("Une erreur est survenue lors de la création des tables associées au test.", e);
        }
    }

    @Configuration
    static class ContextConfig {

        @Bean
        public HikariDataSource dataSource() {
            final HikariConfig conf = new HikariConfig();
            conf.setPoolName("springHikariCP");
            conf.setConnectionTestQuery("SELECT 1");
            conf.setDriverClassName("org.h2.Driver");
            conf.setMaximumPoolSize(5);
            conf.setIdleTimeout(30000);
            conf.setLeakDetectionThreshold(10000);
            conf.setJdbcUrl("jdbc:h2:mem:layout;MODE=MYSQL");
            conf.setUsername("sa");
            conf.setPassword(StringUtils.EMPTY);
            return new HikariDataSource(conf);
        }

    }

}
