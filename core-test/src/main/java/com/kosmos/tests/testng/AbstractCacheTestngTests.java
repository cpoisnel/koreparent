package com.kosmos.tests.testng;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.support.CompositeCacheManager;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

import com.kosmos.tests.testng.annotations.ClearCache;

/**
 * Created on 28/10/15.
 */
public abstract class AbstractCacheTestngTests extends AbstractTestNGSpringContextTests {

    @Autowired
    protected CompositeCacheManager compositeCacheManager;

    protected Collection<String> cachesToClear;

    public AbstractCacheTestngTests() {
        cachesToClear = new ArrayList<>();
    }

    @BeforeMethod
    public void clearCaches(Method method) {
        final ClearCache clearCache = method.getAnnotation(ClearCache.class);
        if(clearCache != null) {
            for (String currentCacheName : clearCache.names()) {
                final Cache cache = compositeCacheManager.getCache(currentCacheName);
                if (cache != null) {
                    cache.clear();
                }
            }
        }
    }
}
