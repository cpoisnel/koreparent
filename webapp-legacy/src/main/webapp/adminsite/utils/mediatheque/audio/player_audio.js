var objPlayerAudio = new Array();
var objPlayerAudioParam1 = new Array();
var objPlayerAudioParam2 = new Array();

function AudioPlayer(elementContainerId, width, height, playerFlashURL) {

    this.elementContainerId = elementContainerId;

    this.width = width;

    this.height = height;

    this.playerFlashURL = playerFlashURL;

    this.audios = new MediaContainer();

    /**
     * Le lanceur qui à lancer le dernier fichier AUDIO
     */
    this.oldLanceur = null;

    this.currentAudioIndex = 0;

    /**
     * Faire en sorte que le son joué démarre dés le chargement du player.
     */
    this.autostart = "0";

    /**
     * Le son joué tourne en boncle
     */
    this.autoreplay = "1";

    /**
     * Afficher le temps dans le player
     */
    this.showtime = "0";

    /**
     * La couleur de fond du player, si rien n'est défini alors c'est al couleur
     * transparente qui sera en fond.
     */
    this.bgcolor = "";

    try {
        if (typeof (_playerInstancier) != 'undefined') {
            _playerInstancier.push(this);
        }
    } catch (e) {
    }

    this.stop = function () {
        try {
            document.getElementById(this.playerName).dewstop();
        } catch (e) {
        }
        return false;
    }

    /**
     * Ajouter une nouvelle vidéo à la liste des vidéos du player.
     */
    this.ajouterMedia = function (audioUrl, audioTitle) {
        this.audios.ajouterMedia(new MediaObject(audioUrl, audioTitle));
    }

    /**
     * Fonction qui permet de générer le player audio dans l'élément qui doit le
     * contenir (elementContainerId)
     */
    this.genererPlayer = function () {

        var key = (new Date()).getMilliseconds();

        document.getElementById(elementContainerId).innerHTML = '<div id=\''
        + elementContainerId + key + '\'></div>';

        this.playerName = "player_audio" + key;

        var attributes = {
            id: this.playerName
        };
        var flashvars = {
            autostart: this.autostart,
            autoreplay: this.autoreplay,
            showtime: this.showtime
        };
        var params = {};

        if (this.bgcolor == "") {
            params["wmode"] = "opaque";
        } else {
            flashvars["bgcolor"] = this.bgcolor;
            params["bgcolor"] = this.bgcolor;
        }

        if (this.getAudioURLPlaylist() != null
            && this.getAudioURLPlaylist().length > 0) {
            flashvars["mp3"] = this.getAudioURLPlaylist();
        }

        new swfobject.embedSWF(this.playerFlashURL, elementContainerId + key,
            this.width, this.height, "8", '', flashvars, params, attributes);
    }

    this.lancerMedia = function (urlAudio, elementHTML) {

        try {
            var playerElement = document.getElementById(this.playerName);

            var nextIndex = this.audios.getMediaIndex(urlAudio);

            if (nextIndex > this.currentAudioIndex) {
                for (var i = 0; i < (nextIndex - this.currentAudioIndex); i++) {
                    playerElement.dewnext();
                }
            } else if (nextIndex < this.currentAudioIndex) {
                for (var i = 0; i < (this.currentAudioIndex - nextIndex); i++) {
                    playerElement.dewprev();
                }
            } else {
                playerElement.dewstop();
                playerElement.dewplay();
            }

            this.gererLanceur(elementHTML);
            this.currentAudioIndex = nextIndex;

        } catch (e) {
            // adaptation pour la gestion des flashs sous FIREFOX
            // alert(e);
            var position = objPlayerAudio.length;
            objPlayerVideo[position] = this;
            objPlayerAudioParam1[position] = urlAudio;
            objPlayerAudioParam2[position] = elementHTML;

            setTimeout(
                "objPlayerAudio[" + position + "].lancerMedia(objPlayerAudioParam1[" + position + "], objPlayerAudioParam2[" + position + "])",
                100);
        }
        return false;
    }

    this.getAudioURLPlaylist = function () {
        var audioUrlList = "";
        var urlArray = this.audios.getUrls();
        for (var i = 0; i < urlArray.length; i++) {
            audioUrlList += urlArray[i];
            // si on est pas à la derniére vidéo
            if (i < (urlArray.length - 1)) {
                audioUrlList += "|";
            }
        }
        return audioUrlList;
    }

    /**
     * Cette fonction permet de changer la classe d'affichage du lanceur en
     * focntion de l'état du player.
     */
    this.gererLanceur = function (elementHTML) {
        var classHTML;
        if (this.oldLanceur != null) {
            // on supprime les éléments d'état du précédent lanceur
            classHTML = this.oldLanceur.className;
            var indexPlay = classHTML.indexOf(" lanceur_media_play", 0);
            if (indexPlay > -1) {
                classHTML = classHTML.substring(0, indexPlay);
            }
            this.oldLanceur.className = classHTML;
        }
        if (typeof (elementHTML) != 'undefined' && elementHTML && elementHTML != null) {
            classHTML = elementHTML.className;
            elementHTML.className = elementHTML.className + " lanceur_media_play";
            this.oldLanceur = elementHTML;
        }
    }

    this.setAutostart = function (autostart) {
        this.autostart = autostart;
    }

    this.setShowtime = function (showtime) {
        this.showtime = showtime;
    }

    this.setAutoreplay = function (autoreplay) {
        this.autoreplay = autoreplay;
    }

    this.setBgcolor = function (bgcolor) {
        this.bgcolor = bgcolor;
    }
}