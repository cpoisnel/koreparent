<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.URLResolver" %>
<%
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    String urlMedia = request.getParameter("URL_MEDIA");
%>
<!DOCTYPE html>
<!--[if lte IE 7]> <html class="ie7 oldie" xmlns="http://www.w3.org/1999/xhtml" lang="<%=ctx.getLocale().getLanguage()%>" xml:lang="<%=ctx.getLocale().getLanguage()%>"> <![endif]-->
<!--[if IE 8]> <html class="ie8 oldie" xmlns="http://www.w3.org/1999/xhtml" lang="<%=ctx.getLocale().getLanguage()%>" xml:lang="<%=ctx.getLocale().getLanguage()%>"> <![endif]-->
<!--[if gt IE 8]><!--> <html xmlns="http://www.w3.org/1999/xhtml" lang="<%=ctx.getLocale().getLanguage()%>" xml:lang="<%=ctx.getLocale().getLanguage()%>"> <!--<![endif]-->
    <head>
        <link rel="stylesheet" type="text/css" media="screen" href="/adminsite/scripts/libs/mediaElement/mediaelementplayer.css">
    </head>
    <body style="margin : 0; overflow: hidden;">
        <div class="audio-container">
            <audio width="100%" height="30px" style="width:100%;height:30px" controls >
                <source src="<%= urlMedia %>" type="audio/<%= request.getParameter("EXTENSION") %>">
                <object width="100%" height="30px" type="application/x-shockwave-flash" data="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>">
                    <param value="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>" name="movie" />
                    <param name="flashvars" value="controls=true&amp;isvideo=false&amp;autoplay=false&amp;preload=none&amp;file=<%= urlMedia %>" />
                    <param value="false" name="allowFullScreen" />
                </object>
            </audio>
        </div>
        <script type="text/javascript" src="/adminsite/scripts/libs/jquery-1.11.0.js"></script>
        <script type="text/javascript" src="/adminsite/scripts/libs/mediaElement/mediaelement-and-player.js"></script>
        <script type="text/javascript" src="/jsp/scripts/medias.js"></script>
    </body>
</html>