// objet VUE : une vue c'est un bouton et une div qui contient l'information à
// afficher. Lors de l'activation de la vue, la div est affiché et le bouton
// passe en selectionné.
function View(name, divId, buttonId) {
    this.name = name; // nom de la vue (utiloisé comme ID)
    this.divId = divId; // nom de la DIV à afficher
    this.buttonId = buttonId; // nom du bouton à valider
}

function OngletManager() {

    /**
     * La vue qui est affichée. View currentView = null;
     */
    this.currentView = null;

    /**
     * La liste des vues que doit gérer le manager. Array<View> arrayViewButton =
     * new Array();
     */
    this.arrayViewButton = new Array();

    /**
     * PRIVATE---------------------------------------------------------------
     * Fonction qui permet d'ajouter des objet View dans la liste de vues que
     * doit gérer le manager.
     * ********************************************************************** - -
     * name : le nom de la vue. - divId : l'identifiant de la div qui contient
     * les éléments à afficher. - buttonId : l'identifiant de l'element qui
     * déclanche l'affichage de la DIV.
     */
    this.addNewView = function (name, divId, buttonId) {
        this.arrayViewButton[this.arrayViewButton.length] = new View(name,
            divId, buttonId);
    }

    /**
     * PRIVATE---------------------------------------------------------------
     * Fonction qui permet retrouver un objet View dans la liste des objets
     * gérés par cette classe. Pour cela il suffit de spécifier le nom de la
     * vue.
     *
     */
    this.retrieveView = function (name) {
        for (i = 0; i < this.arrayViewButton.length; i++) {
            if (this.arrayViewButton[i].name == name || (i == 0 && name == "")) {
                return this.arrayViewButton[i];
            }
        }
        return null;
    }

    /**
     * PUBLIC---------------------------------------------------------------
     * Changer de vue, passer de la vue courante à la nouvelle. La vue dite
     * courant se cache et la nouvelle vue devient visible.
     */
    this.changeView = function (newViewName) {
        if (newViewName != this.currentView.name) {
            var newView = this.retrieveView(newViewName);
            this.unselectView(document.getElementById(this.currentView.divId));
            this.selectView(document.getElementById(newView.divId));
            this.unselectButton(document
                .getElementById(this.currentView.buttonId));
            this.selectButton(document.getElementById(newView.buttonId))
            this.currentView = newView;

            if (_playerInstancier) {
                for (var i = 0; i < _playerInstancier.length; i++) {
                    try {
                        _playerInstancier[i].stop();
                    } catch (e) {
                    }
                }
            }
        }
    }

    /**
     * PUBLIC---------------------------------------------------------------
     * Initialiser la vue en fonction du nom de la vue passé en paramétre. Si le
     * nom est vide alors c'est la premiére vue enregistrée qui sera affichée.
     */
    this.initView = function (viewName) {
        this.currentView = this.retrieveView(viewName);
        for (i = 0; i < this.arrayViewButton.length; i++) {
            if (this.arrayViewButton[i].name == this.currentView.name) {
                this
                    .selectView(document
                        .getElementById(this.currentView.divId));
                this.selectButton(document
                    .getElementById(this.currentView.buttonId));
            } else {
                if (this.arrayViewButton[i].viewName != this.currentView.divId) {
                    this.unselectView(document
                        .getElementById(this.arrayViewButton[i].divId));
                }
                if (this.arrayViewButton[i].buttonName != this.currentView.buttonId) {
                    this.unselectButton(document
                        .getElementById(this.arrayViewButton[i].buttonId))
                }
            }
        }
    }

    // rendre visible la DIV.
    this.selectView = function (elementToSelect) {
        if (elementToSelect) {
            elementToSelect.style.display = 'block';
            elementToSelect.style.visibility = '';
        }
    }

    // rendre selectionné le bouton
    this.unselectView = function (elementToUnselect) {
        if (elementToUnselect) {
            elementToUnselect.style.display = 'none';
            elementToUnselect.style.visibility = 'hidden';
        }
    }

    // rendre invisible la DIV
    this.selectButton = function (buttonToSelect) {
        if (buttonToSelect) {
            var buttonClass = buttonToSelect.className;
            var indexOfUnselectClass = buttonClass
                .indexOf(' bouton_unfocus', 0);
            if (indexOfUnselectClass >= 0) {
                buttonClass = buttonClass.substring(0, indexOfUnselectClass);
            }
            buttonToSelect.className = buttonClass + ' bouton_focus';
        }
    }

    // rendre deselectionné le bouton.
    this.unselectButton = function (buttonToUnselect) {
        if (buttonToUnselect) {
            var buttonClass = buttonToUnselect.className;
            var indexOfSelectClass = buttonClass.indexOf(' bouton_focus', 0);
            if (indexOfSelectClass >= 0) {
                buttonClass = buttonClass.substring(0, indexOfSelectClass);
            }
            buttonToUnselect.className = buttonClass + ' bouton_unfocus';
        }
    }

}