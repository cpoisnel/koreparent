/**
 * Représentation d'une fonction et de sa priorité.
 *
 * @param action
 *            une chaine de caractéres qui correspond à la fonction à lancer.
 * @param priotity
 *            la priorité de lancement de la fonction ( un chiffre compris entre
 *            0 et 999).
 */
function functionLunchable(action, priotity) {
    this.action = action;
    this.priority = priotity;
}

/**
 * Fonction qui permet de prioriser des lancements de fonctions. Les priorités
 * sont entre 0 et 999.
 */
function FunctionLuncher() {

    /**
     * Tableau qui contient les fonctions et leur priorité respective.
     */
    this.functionArray = new Array();

    /**
     * Ajouter une fonction à lancer. Cette fonction est une chaine de caractére
     * qui représenet le code à de la fonction (attention donc aux simples et
     * doubles cotes). La priorité est un entrier compris entre 0 et 999 qui
     * détermine l'ordre de lancement de la fonction.
     */
    this.addFunctionLunchable = function (action, priority) {
        var finalPriority = 0;
        if (priority) {
            finalPriority = priority;
        }
        this.functionArray[this.functionArray.length] = new functionLunchable(
            action, finalPriority);
    }

    /**
     * Lancer toutes les fonctions stockée dans le lanceur par ordre de
     * priorité.
     */
    this.lunchAll = function () {
        var currentPriority = 0;
        var nextPriority = 0;
        while (currentPriority < 1000) {
            nextPriority = 1000;
            for (var i = 0; i < this.functionArray.length; i++) {
                if (this.functionArray[i].priority == currentPriority) {
                    // on lance la fonction
                    eval(this.functionArray[i].action);
                } else if (this.functionArray[i].priority > currentPriority
                    && this.functionArray[i].priority < nextPriority) {
                    // on calcul la prochaine priorité
                    nextPriority = this.functionArray[i].priority;
                }
            }
            currentPriority = nextPriority;
        }
    }
}
