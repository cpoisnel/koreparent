function changePageMediatheque(currentPage, nextPage) {
    var blockToHide = document.getElementById(currentPage);
    var blockToDisplay = document.getElementById(nextPage);
    blockToHide.style.visibility = 'hidden';
    blockToHide.style.display = 'none';
    blockToDisplay.style.display = '';
    blockToDisplay.style.visibility = 'visible';
}