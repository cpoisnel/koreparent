/**
 * Objet qui permet de modéliser une média.
 *
 * @param mediaUrl
 *            L'url du média servant aussi d'identifiant.
 * @param mediaTitle
 *            Le titre du média.
 */
function MediaObject(mediaUrl, mediaTitle) {
    this.mediaUrl = mediaUrl;
    this.mediaTitle = mediaTitle;
}


function MediaContainer() {

    this.arrayMedia = {};
    this.arrayIndex = {};
    this.arrayUrl = new Array();

    this.index = 0;

    this.ajouterMedia = function (media) {
        this.arrayMedia[media.mediaUrl] = media;
        this.arrayIndex[media.mediaUrl] = this.index;
        this.arrayUrl[this.arrayUrl.length] = media.mediaUrl;
        this.index++;
    }

    this.getMedia = function (urlMedia) {
        return this.arrayMedia[urlMedia];
    }

    this.getMediaIndex = function (urlMedia) {
        return this.arrayIndex[urlMedia];
    }

    this.getUrls = function () {
        return this.arrayUrl;
    }
}