function SWFPlayer(elementContainerId, width, height) {

    this.elementContainerId = elementContainerId;

    this.width = width;

    this.height = height;

    this.swfs = new MediaContainer();

    this.oldLanceur = null;

    try {
        if (typeof (_playerInstancier) != 'undefined') {
            _playerInstancier.push(this);
        }
    } catch (e) {
    }

    this.stop = function () {
        try {
            var element = document.getElementById(elementContainerId);
            if (element.hasChildNodes()) {
                while (element.childNodes.length >= 1) {
                    element.removeChild(element.firstChild);
                }
            }
        } catch (e) {
        }
        return false;
    }

    /**
     * Ajouter une nouvelle vidéo à la liste des vidéos du player.
     */
    this.ajouterMedia = function (swfUrl, swfTitle) {
        this.swfs.ajouterMedia(new MediaObject(swfUrl, swfTitle));
    }

    this.genererPlayer = function () {
        this.lancerMedia(this.swfs.getUrls()[0]);
    }

    this.lancerMedia = function (urlSWF, elementHTML) {
        this.stop();
        this.lancerSWF(urlSWF, elementHTML);
        return false;
    }

    this.lancerSWF = function (urlSWF, elementHTML) {

        document.getElementById(elementContainerId).innerHTML = '<div id=\'' + elementContainerId + 'SWF\'></div>';

        var key = (new Date()).getMilliseconds();
        this.playerName = "player_swf" + key;

        if (urlSWF.indexOf("?") != -1) {
            urlSWF += "&";
        } else {
            urlSWF += "?";
        }

        var attributes = {
            id: this.playerName
        };
        var flashvars = {};
        var params = {
            movie: urlSWF,
            wmode: "opaque",
            allowFullScreen: "true",
            allowScriptAccess: "always"
        };

        new swfobject.embedSWF(urlSWF, elementContainerId + 'SWF', this.width,
            this.height, "8", '', flashvars, params, attributes);

        this.gererLanceur(elementHTML);
    }

    /**
     * Cette fonction permet de changer la classe d'affichage du lanceur en
     * focntion de l'état du player.
     */
    this.gererLanceur = function (elementHTML) {
        var classHTML;
        if (this.oldLanceur != null) {
            // on supprime les éléments d'état du précédent lanceur
            classHTML = this.oldLanceur.className;
            var indexPlay = classHTML.indexOf(" lanceur_media_play", 0);
            if (indexPlay > -1) {
                classHTML = classHTML.substring(0, indexPlay);
            }
            this.oldLanceur.className = classHTML;
        }
        if (typeof (elementHTML) != 'undefined' && elementHTML && elementHTML != null) {
            classHTML = elementHTML.className;
            elementHTML.className = elementHTML.className
            + " lanceur_media_play";
            this.oldLanceur = elementHTML;
        }
    }

}
