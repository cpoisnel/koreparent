<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.univ.utils.EscapeString"%>
<script type="text/javascript" src="/adminsite/utils/swfobject.js"></script>
<script type="text/javascript" src="/adminsite/utils/mediatheque/swf/player_swf.js"></script>
<script type="text/javascript" src="/adminsite/utils/mediatheque/commun/media.js"></script>
<%
String urlMedia = request.getParameter("URL_MEDIA");
%>

<span id="container_swf_distant"> <a href="<%=EscapeString.escapeAttributHtml(urlMedia)%>">SWF ici</a> </span>

<script type="text/javascript">
//<![CDATA[
    <%
    String width = request.getParameter("width");
    if (StringUtils.isEmpty(width)){
        width = request.getParameter("WIDTH");
    }

    String height = request.getParameter("height");
    if (StringUtils.isEmpty(height)){
        height = request.getParameter("HEIGHT");
    }

    boolean pageSize = false;
    if (StringUtils.isEmpty(width) || !StringUtils.isNumeric(width) || StringUtils.equals("-1", width)) {
        %>var pagesize = getPageSize();
        var pageWidth = pagesize[2] - marginOnWidth;<%
        pageSize = true;
    } else {
        %>var pageWidth = <%= width %>;<%
    }
    if (StringUtils.isEmpty(height) || !StringUtils.isNumeric(height) || StringUtils.equals("-1", height)) {
        if (!pageSize){
            %>var pagesize = getPageSize();<%
        }
        %>var pageHeigth = pagesize[3] - marginTop - marginBottom - navHeight;<%
    } else {
        %>var pageHeigth = <%= height %>;<%
    }
    %>
    var playerSWF = new SWFPlayer('container_swf_distant', pageWidth, pageHeigth);
    playerSWF.ajouterMedia("<%=EscapeString.escapeJavaScript(urlMedia)%>", "");
    playerSWF.genererPlayer();
    playerSWF.lancerMedia("<%=EscapeString.escapeJavaScript(urlMedia)%>");
//]]>
</script>