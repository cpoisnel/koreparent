/**
 * Le player photo est un player qui permet d'afficher une photo. Pour
 * fonctionner ce player va utiliser une balise (dont l'identifiant est passé en
 * paramétre) et changer son dontenu en fonction des affichages de photos. De
 * plus ce player affiche une image de chargement pendant chaque chargement de
 * photos.
 *
 * @param elementContainerId
 *            Le nom de l'élément HTML qui va contenir le player de photo.
 * @param width
 *            La longueur du player
 * @param height
 *            La hauteur du player
 * @param imageLoading
 *            L'adresse de l'image qui sera afficher en transition de chaque
 *            changement de photo (durant le chargement).
 */
function PhotoPlayer(elementContainerId, width, height, imageLoading) {

    this.elementContainerId = elementContainerId;

    this.width = width;

    this.height = height;

    this.playerName = "";

    this.imageLoading = imageLoading;

    this.elementContainer = null;

    this.oldLanceur = null;

    /**
     * Générer le player photo.
     */
    this.genererPlayer = function () {
        var key = (new Date()).getMilliseconds();
        this.playerName = "player_photo" + key;
        this.elementContainer = document
            .getElementById(this.elementContainerId);
        // on applique les dimentions au player
        this.elementContainer.style.width = this.width + "px";
        this.elementContainer.style.height = this.height + "px";
        this.elementContainer.style.position = "relative";
        this.elementContainer.style.margin = "auto";
    }

    /**
     * Lancer l'affichage d'une image dans le player.
     */
    this.lancerMedia = function (urlPhoto, elementHTML) {

        var containerObj = this.elementContainer;
        var containerWidth = this.width;
        var containerHeight = this.height;

        // on affiche l'image d'attente (gif de loading)
        var imageLoading = new Image();
        imageLoading.onload = function () {
            // fonction lancée lorsque l'image est chargée
            imageLoading.onload = null;
            // taille de l'image poour calculer la position
            var width = imageLoading.width;
            var height = imageLoading.height;
            var tmpHeight = imageLoading.height;
            if (width > containerWidth) {
                width = containerWidth;
                height = height * (width / image.width);
            }
            if (height > containerHeight) {
                tmpHeight = height;
                height = containerHeight;
                width = width * (height / tmpHeight);
            }
            var loadingPosX = (containerWidth - imageLoading.width) / 2;
            var loadingPosY = (containerHeight - imageLoading.height) / 2;
            containerObj.innerHTML = "<img style=\"top: " + loadingPosY
            + "px; left: " + loadingPosX
            + "px; position: absolute;\" src=\"" + imageLoading.src
            + "\"></img>";
        }
        imageLoading.src = this.imageLoading;

        var image = new Image();
        image.onload = function () {
            // fonction lancée lorsque l'image est chargée
            image.onload = null;
            // calcul de la taille de l'image
            var width = image.width;
            var height = image.height;
            var tmpHeight = image.height;
            if (width > containerWidth) {
                width = containerWidth;
                height = height * (width / image.width);
            }
            if (height > containerHeight) {
                newHeight = height;
                height = containerHeight;
                width = width * (height / newHeight);
            }

            // Calcul de la position de l'image dans le player
            var posX = (containerWidth - width) / 2;
            var posY = (containerHeight - height) / 2;

            // on affiche la photo (une fois chargé)
            containerObj.innerHTML = "<img style=\"top: " + posY + "px; left: "
            + posX + "px; position: absolute;\" width=\"" + width
            + "px\" height=\"" + height + "px\" src=\"" + urlPhoto
            + "\"></img>";
        }
        image.src = urlPhoto;
        return false;
    }


}
