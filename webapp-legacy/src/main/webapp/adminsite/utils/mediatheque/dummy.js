/**
 * Dummy to insure backward compatibility
 */
function VideoPlayer(elementContainerId, width, height, playerFlashURL) {

    this.elementContainerId = elementContainerId;

    this.stop = function () {
    };

    /**
     * Ajouter une nouvelle vidéo à la liste des vidéos du player.
     */
    this.ajouterVideo = function (videoUrl, videoTitle) {
    };

    /**
     * Fonction qui permet de générer le player video dans l'élément qui doit le
     * contenir (elementContainerId)
     */
    this.genererPlayer = function () {
    };

    /**
     * lancer une video.
     */
    this.lancerMedia = function (urlVideo, elementHTML) {
    };

    this.getVideoURLPlaylist = function () {
    };

    /**
     * Cette fonction permet de changer la classe d'affichage du lanceur en
     * focntion de l'état du player.
     */
    this.gererLanceur = function (elementHTML) {
    };

    this.isDisplayableURL = function (sURL) {
        return true;
    };

    this.setLoop = function (loop) {
    };

    this.setAutoplay = function (autoplay) {
    };

    this.setAutoload = function (autoload) {
    };

    this.setShowfullscreen = function (showfullscreen) {
    };

    this.setBuffer = function (buffer) {
    };

    this.setMargin = function (margin) {
    };

    this.setBgcolor = function (bgcolor) {
    };

    this.setBgcolor1 = function (bgcolor1) {
    };

    this.setBgcolor2 = function (bgcolor2) {
    };

    this.setPlayercolor = function (playercolor) {
    };

    this.setLoadingcolor = function (loadingcolor) {
    };

    this.setButtoncolor = function (buttoncolor) {
    };

    this.setButtonovercolor = function (buttonovercolor) {
    };

    this.setSlidercolor1 = function (slidercolor1) {
    };

    this.setSlidercolor2 = function (slidercolor2) {
    };

    this.setSliderovercolor = function (sliderovercolor) {
    };

    this.setShowstop = function (showstop) {
    };

    this.setShowvolume = function (showvolume) {
    };

    this.setShowtime = function (showtime) {
    };

    this.setShowplayer = function (showplayer) {
    };

    this.setTitlesize = function (titlesize) {
    };

    this.setTitlecolor = function (titlecolor) {
    };

    this.setShowiconplay = function (showiconplay) {
    };

    this.setIconplaycolor = function (iconplaycolor) {
    };

    this.setIconplaybgcolor = function (iconplaybgcolor) {
    };

    this.setIconplaybgalpha = function (iconplaybgalpha) {
    };

    this.setStartimage = function (startimage) {
    };

    this.setShowopen = function (showopen) {
    };

    this.setAutonext = function (autonext) {
    };

}

function AudioPlayer(elementContainerId, width, height, playerFlashURL) {

    this.stop = function () {
    };

    /**
     * Ajouter une nouvelle vidéo à la liste des vidéos du player.
     */
    this.ajouterMedia = function (audioUrl, audioTitle) {
    };

    /**
     * Fonction qui permet de générer le player audio dans l'élément qui doit le
     * contenir (elementContainerId)
     */
    this.genererPlayer = function () {
    };

    this.lancerMedia = function (urlAudio, elementHTML) {
    };

    this.getAudioURLPlaylist = function () {
    };

    /**
     * Cette fonction permet de changer la classe d'affichage du lanceur en
     * focntion de l'état du player.
     */
    this.gererLanceur = function (elementHTML) {
    };

    this.setAutostart = function (autostart) {
    };

    this.setShowtime = function (showtime) {
    };

    this.setAutoreplay = function (autoreplay) {
    };

    this.setBgcolor = function (bgcolor) {
    };
}