<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    String url = request.getParameter("URL_MEDIA");
    String extension = request.getParameter("EXTENSION");
    if("flv".equals(extension)) {
        extension = "x-flv";
    }
%>
<!DOCTYPE html>
<!--[if lte IE 7]> <html class="ie7 oldie" xmlns="http://www.w3.org/1999/xhtml" lang="<%=ctx.getLocale().getLanguage()%>" xml:lang="<%=ctx.getLocale().getLanguage()%>"> <![endif]-->
<!--[if IE 8]> <html class="ie8 oldie" xmlns="http://www.w3.org/1999/xhtml" lang="<%=ctx.getLocale().getLanguage()%>" xml:lang="<%=ctx.getLocale().getLanguage()%>"> <![endif]-->
<!--[if gt IE 8]><!--> <html xmlns="http://www.w3.org/1999/xhtml" lang="<%=ctx.getLocale().getLanguage()%>" xml:lang="<%=ctx.getLocale().getLanguage()%>"> <!--<![endif]-->
    <head>
        <link rel="stylesheet" type="text/css" media="screen" href="/adminsite/scripts/libs/mediaElement/mediaelementplayer.css">
    </head>
    <body style="margin : 0; overflow: hidden;">
        <div class="video-container" flv_div="true" flv_local="1">
            <video controls="controls" width="480" height="270" style="width: 100%; height: 100%;">
                <source src="<%= url %>" type="video/<%= extension %>" />
                <object width="480" height="270" type="application/x-shockwave-flash" data="/adminsite/scripts/libs/mediaElement/flashmediaelement.swf" style="width: 100%; height: 100%;">
                    <param value="/adminsite/scripts/libs/mediaElement/flashmediaelement.swf" name="movie" />
                    <param value="controls=true&amp;file=<%= url %>" name="flashvars" />
                    <param value="true" name="allowFullScreen" />
                </object>
            </video>
        </div>
        <script type="text/javascript" src="/adminsite/scripts/libs/jquery-1.11.0.js"></script>
        <script type="text/javascript" src="/adminsite/scripts/libs/mediaElement/mediaelement-and-player.js"></script>
        <script type="text/javascript" src="/jsp/scripts/medias.js"></script>
    </body>
</html>