//pour eviter les mauvais rechargements du player sous IE
var noCacheKey = (new Date()).getTime();
var objPlayerVideo = new Array();
var objPlayerVideoParam1 = new Array();
var objPlayerVideoParam2 = new Array();

/**
 * Créer un player video. Cet objet JS va nous permette de céer et initialiser
 * le player video Flash, pour cela on utilise SWFObject.
 *
 * @param elementContainerId
 *            L'identifiant de la baslise HTML dans laquelle sera ajouté le
 *            player Flash.
 * @param width
 *            La largeur du player flash
 * @param heigth
 *            La hauteur du player flash
 * @param playerFlashURL
 *            L'url du player flash.
 */
function VideoPlayer(elementContainerId, width, height, playerFlashURL) {

    this.elementContainerId = elementContainerId;

    this.width = width;

    this.height = height;

    this.playerFlashURL = playerFlashURL;

    this.playerName = "";

    /**
     * Le lanceur qui à lancer la derniére video
     */
    this.oldLanceur = null;

    /**
     * Liste des vidéos que le player sera amené à jouer.
     */
    this.videos = new MediaContainer();

    /**
     * Image de départ (vue avant de lancer la video)
     */
    this.startimage = "";

    /**
     * Variable qui permet de préciser si on veut que le player joue les video
     * en boucle. La valeur de cette variable est 1 ou 0.
     */
    this.loop = "0";

    /**
     * 0 ou 1
     */
    this.autoplay = "0";

    /**
     * 0 ou 1
     */
    this.autoload = "0";

    /**
     * 0 ou 1
     */
    this.showfullscreen = "1";

    /**
     * Le nombre de secondes que le buffer doit contenir avant de lancer la
     * video.
     */
    this.buffer = "10";

    /**
     * Taille de la marge autour du player en pixel
     */
    this.margin = "2";

    /**
     * Couleur de fond du player
     */
    this.bgcolor = "ffffff";

    /**
     * Premiere couleur du dégradé (couleur du haut)
     */
    this.bgcolor1 = "7c7c7c";

    /**
     * Seconde couleur du dégradé (couleur du bas)
     */
    this.bgcolor2 = "333333";

    /**
     * La couleur de la barre d'outil du player.
     */
    this.playercolor = "000000";

    /**
     *
     */
    this.loadingcolor = "ffffff";

    /**
     * La couleur des boutons
     */
    this.buttoncolor = "ffffff"

    /**
     * La couleur des boutons au survol.
     */
    this.buttonovercolor = "cccccc";

    /**
     * Premiére couleur du dégradé de la barre d'avancement de la video.
     */
    this.slidercolor1 = "ffffff";

    /**
     * Seconde couleur du dégradé de la barre d'avancement de la video.
     */
    this.slidercolor2 = "ffffff";

    /**
     * Couleur au survol de la barre d'avancement de la video.
     */
    this.sliderovercolor = "cccccc";

    this.showstop = "1";

    this.showvolume = "1";

    this.showtime = "1";

    this.autonext = "0";

    /**
     * Indique si on veut que la barre de la video reste ou non. Les valeurs
     * possibles : always, never et autohide
     */
    this.showplayer = "always";

    /**
     * la couleur du titre de la video
     */
    this.titlecolor = "ffffff";

    /**
     * la tailel du titre de la video
     */
    this.titlesize = "20";

    /**
     * Mettre une icone play sur le player (0 ou 1)
     */
    this.showiconplay = "1";

    /**
     * couleur de l'icone play
     */
    this.iconplaycolor = "fffffa";

    /**
     * couleur de fond de l'icone play
     */
    this.iconplaybgcolor = "ffffff";

    /**
     * Transparence du fond de l'iconne play
     */
    this.iconplaybgalpha = "25";

    /**
     * Il s'agit d'afficher ou non le bouton permettant de voir la playlist dans
     * player flash.
     */
    this.showopen = "0";

    try {
        if (typeof (_playerInstancier) != 'undefined') {
            _playerInstancier.push(this);
        }
    } catch (e) {
    }

    this.stop = function () {
        try {
            $(this.playerName).SetVariable('player:jsStop', '');
        } catch (e) {
        }
        return false;
    }

    /**
     * Ajouter une nouvelle vidéo à la liste des vidéos du player.
     */
    this.ajouterVideo = function (videoUrl, videoTitle) {
        this.videos.ajouterMedia(new MediaObject(videoUrl, videoTitle));
    }

    /**
     * Fonction qui permet de générer le player video dans l'élément qui doit le
     * contenir (elementContainerId)
     */
    this.genererPlayer = function () {

        var playertimeout = 500;

        var key = (new Date()).getTime();

        document.getElementById(elementContainerId).innerHTML = '<div id=\'' + elementContainerId + key + '\'></div>';

        var playerFile = this.playerFlashURL + "?NO_CACHE=" + noCacheKey;

        // pour éviter les pb sous IE
        this.playerName = "player_video" + key;

        var attributes = {
            id: this.playerName
        };

        var flashvars = {
            showfullscreen: this.showfullscreen,
            iconplaybgalpha: playertimeout,
            loop: this.loop,
            autoplay: this.autoplay,
            autoload: this.autoload,
            buffer: this.buffer,
            margin: this.margin,
            bgcolor: this.bgcolor,
            bgcolor1: this.bgcolor1,
            bgcolor2: this.bgcolor2,
            playercolor: this.playercolor,
            loadingcolor: this.loadingcolor,
            buttoncolor: this.buttoncolor,
            buttonovercolor: this.buttonovercolor,
            slidercolor1: this.slidercolor1,
            slidercolor2: this.slidercolor2,
            sliderovercolor: this.sliderovercolor,
            showstop: this.showstop,
            showvolume: this.showvolume,
            showtime: this.showtime,
            showplayer: this.showplayer,
            titlecolor: this.titlecolor,
            titlesize: this.titlesize,
            showiconplay: this.showiconplay,
            iconplaycolor: this.iconplaycolor,
            iconplaybgcolor: this.iconplaybgcolor,
            iconplaybgalpha: this.iconplaybgalpha,
            showopen: this.showopen,
            autonext: this.autonext,
            flv: this.getVideoURLPlaylist()
        };

        var params = {
            movie: this.playerURL,
            wmode: "opaque",
            allowFullScreen: "true",
            allowScriptAccess: "always"
        };


        if (this.startimage != "") {
            flashvars["startimage"] = this.startimage;
        }

        new swfobject.embedSWF(playerFile, elementContainerId + key,
            this.width, this.height, "8", '', flashvars, params, attributes);

    }

    /**
     * lancer une video.
     */
    this.lancerMedia = function (urlVideo, elementHTML) {
        try {
            //pb avec FF qui regenére le FLASH lorsque ce dernier disparait (display:none)
            setTimeout('$(\'' + this.playerName + '\').SetVariable(\'player:jsPlay\', ' + this.videos.getMediaIndex(urlVideo) + ')', 750);
            if (elementHTML) {
                this.gererLanceur(elementHTML);
            }
        } catch (e) {
            // on attend 100ms pour relancer le média
            // alert(e);
            var position = objPlayerVideo.length;
            objPlayerVideo[position] = this;
            objPlayerVideoParam1[position] = urlVideo;
            objPlayerVideoParam2[position] = elementHTML;
            setTimeout('objPlayerVideo[' + position + '].lancerMedia(objPlayerVideoParam1[' + position + '], objPlayerVideoParam2[' + position + '])', 100);
        }
        return false;
    }

    this.getVideoURLPlaylist = function () {
        var videoUrlList = "";
        var urlArray = this.videos.getUrls();
        var urlVideo;
        for (var i = 0; i < urlArray.length; i++) {
            urlVideo = urlArray[i];
            if (this.isDisplayableURL(urlVideo)) {
                if (videoUrlList.length > 0) {
                    videoUrlList += "|";
                }
                videoUrlList += urlVideo;
            }
        }
        return videoUrlList;
    }

    /**
     * Cette fonction permet de changer la classe d'affichage du lanceur en
     * focntion de l'état du player.
     */
    this.gererLanceur = function (elementHTML) {
        var classHTML;
        if (this.oldLanceur != null) {
            // on supprime les éléments d'état du précédent lanceur
            classHTML = this.oldLanceur.className;
            var indexPlay = classHTML.indexOf(" lanceur_media_play", 0);
            if (indexPlay > -1) {
                classHTML = classHTML.substring(0, indexPlay);
            }
            this.oldLanceur.className = classHTML;
        }
        if (typeof (elementHTML) != 'undefined' && elementHTML && elementHTML != null) {
            classHTML = elementHTML.className;
            elementHTML.className = elementHTML.className + " lanceur_media_play";
            this.oldLanceur = elementHTML;
        }
    }

    this.isDisplayableURL = function (sURL) {
        return sURL.indexOf("dailymotion") == -1 && sURL.indexOf("youtube") == -1 && sURL.indexOf("google") == -1;
    }

    /***************************************************************************
     * Liste des fonctions capable de modifier les propriétés du player vidéo.
     **************************************************************************/

    this.setLoop = function (loop) {
        this.loop = loop;
    }

    this.setAutoplay = function (autoplay) {
        this.autoplay = autoplay;
    }

    this.setAutoload = function (autoload) {
        this.autoload = autoload;
    }

    this.setShowfullscreen = function (showfullscreen) {
        this.showfullscreen = showfullscreen;
    }

    this.setBuffer = function (buffer) {
        this.buffer = buffer;
    }

    this.setMargin = function (margin) {
        this.margin = margin;
    }

    this.setBgcolor = function (bgcolor) {
        this.bgcolor = bgcolor;
    }

    this.setBgcolor1 = function (bgcolor1) {
        this.bgcolor1 = bgcolor1;
    }

    this.setBgcolor2 = function (bgcolor2) {
        this.bgcolor2 = bgcolor2;
    }

    this.setPlayercolor = function (playercolor) {
        this.playercolor = playercolor;
    }

    this.setLoadingcolor = function (loadingcolor) {
        this.loadingcolor = loadingcolor;
    }

    this.setButtoncolor = function (buttoncolor) {
        this.buttoncolor = buttoncolor;
    }

    this.setButtonovercolor = function (buttonovercolor) {
        this.buttonovercolor = buttonovercolor;
    }

    this.setSlidercolor1 = function (slidercolor1) {
        this.slidercolor1 = slidercolor1;
    }

    this.setSlidercolor2 = function (slidercolor2) {
        this.slidercolo2 = slidercolor2;
    }

    this.setSliderovercolor = function (sliderovercolor) {
        this.sliderovercolor = sliderovercolor;
    }

    this.setShowstop = function (showstop) {
        this.showstop = showstop;
    }

    this.setShowvolume = function (showvolume) {
        this.showvolume = showvolume;
    }

    this.setShowtime = function (showtime) {
        this.showtime = showtime;
    }

    this.setShowplayer = function (showplayer) {
        this.showplayer = showplayer;
    }

    this.setTitlesize = function (titlesize) {
        this.titlesize = titlesize;
    }

    this.setTitlecolor = function (titlecolor) {
        this.titlecolor = titlecolor;
    }

    this.setShowiconplay = function (showiconplay) {
        this.showiconplay = showiconplay;
    }

    this.setIconplaycolor = function (iconplaycolor) {
        this.iconplaycolor = iconplaycolor;
    }

    this.setIconplaybgcolor = function (iconplaybgcolor) {
        this.iconplaybgcolor = iconplaybgcolor;
    }

    this.setIconplaybgalpha = function (iconplaybgalpha) {
        this.iconplaybgalpha = iconplaybgalpha;
    }

    this.setStartimage = function (startimage) {
        this.startimage = startimage;
    }

    this.setShowopen = function (showopen) {
        this.showopen = showopen;
    }

    this.setAutonext = function (autonext) {
        this.autonext = autonext;
    }

}