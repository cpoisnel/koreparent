<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<!-- link rel="stylesheet" type="text/css" href="/adminsite/general.css" media="screen" / -->
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
</head>


<body id="body_couleur_fond">

<form action="/servlet/com.jsbsoft.jtf.core.SG">

<% fmt.insererVariablesCachees( out, infoBean); %>

<script type="text/javascript">

    function sauvegarderFiche ( code, libelle ) {
            renvoyerValeurs('utilisateur',code,libelle);
    }
</script>  

<div class="content_popup">

<table cellpadding="0" cellspacing="0" border="0" id="tableau_liste_type_fiche_popup">

<tr>
    <th style="text-align:left"><%= MessageHelper.getCoreMessage("BO_INTITULE") %></th>
    <th style="text-align:left"><%= MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT") %></th>
</tr>


<%-- boucle sur chaque ligne --%>

<% int i;

for (i=0; i<((Integer) infoBean.get("LISTE_NB_ITEMS")).intValue(); i++) { %>

<tr>

    <tr>
        <td class="<%= ((i%2)==0)? "impair" : "pair" %>" style="text-align:left">

<a class="link" href="#" onclick="sauvegarderFiche('<%=infoBean.getString("CODE#"+i)%>','<%= StringUtils.replace(infoBean.getString("LIBELLE#"+i), "'", "\\'") %>')">
<%fmt.insererChampSaisie( out, infoBean, "LIBELLE#"+i, fmt.SAISIE_AFFICHAGE, fmt.FORMAT_TEXTE, 0, 5); %> 
</a>
</td>

<td class="<%= ((i%2)==0)? "impair" : "pair" %>" style="text-align:left"> 
    <%fmt.insererChampSaisie( out, infoBean, "LIBELLE_STRUCTURE#"+i ,  fmt.SAISIE_AFFICHAGE , fmt.FORMAT_TEXTE, 0, 5); %>

</td>

</tr>

<% } %>

</table>

</td>
</tr>
</table>
</div>
    </form>
  </body>
</html>