<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
</head>


<body id="body_couleur_fond">

<form action="/servlet/com.jsbsoft.jtf.core.SG">

<% fmt.insererVariablesCachees( out, infoBean); %>


<div class="content_popup">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">

    <tr>
    <td>


    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="360" class="tableaufichestitre">
           <%= MessageHelper.getCoreMessage("BO_GROUPE_TYPE_GROUPE") %>
        </td>
      </tr>

    <tr>
      <td colspan="3" height="5"><img src="/adminsite/images/filetbleu.gif" width="360" height="1"></td>
    </tr>

    <%-- boucle sur chaque ligne --%>

    <% int i;

    for (i=0; i<((Integer) infoBean.get("TYPES_NB_ITEMS")).intValue(); i++) { %>

    <tr>

    <td>
    <a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=RECHERCHE_GROUPE_DSI&ACTION=RECHERCHER&TYPE=<%= infoBean.getString("CODE_TYPE#"+i) %>"
    >
    <%fmt.insererChampSaisie( out, infoBean, "LIBELLE_TYPE#"+i ,  fmt.SAISIE_AFFICHAGE , fmt.FORMAT_TEXTE, 0, 5); %>
    </a>
    </td>


    </tr>

    <% } %>

    </table>

    </td>
    </tr>
    </table>
</div><!-- .content_popup -->

    </form>
  </body>
</html>