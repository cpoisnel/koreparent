<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %> 
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<!-- link rel="stylesheet" type="text/css" href="/adminsite/general.css" media="screen" / -->
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
</head>

<body id="body_couleur_fond">

<form action="/servlet/com.jsbsoft.jtf.core.SG" data-no-tooltip>

<% fmt.insererVariablesCachees( out, infoBean); %>



<%-- 
       ********************************************************************
       *********             CRITERES DE RECHERCHE            *************
       ********************************************************************
--%>
<div class="content_popup">
<table width="100%">

<% 
String permission = infoBean.getString("PERMISSION");
if( permission == null)
    permission = "";

// Le public est utilisé pour le formatage des résultats

String publicVise = request.getParameter("PUBLIC_VISE");
if( publicVise == null)
  publicVise = "";

%>

<input type="hidden" name="PERMISSION" value="<%=permission%>">
<input type="hidden" name="PUBLIC_VISE" value="<%=publicVise%>">


<%univFmt.insererChampSaisie( fmt, out, infoBean, "LIBELLE", fmt.SAISIE_FACULTATIF , fmt.FORMAT_TEXTE, 0,    20, MessageHelper.getCoreMessage("BO_INTITULE")); %>

<%univFmt.insererComboHashtable( fmt, out, infoBean, "TYPE", fmt.SAISIE_FACULTATIF   , "LISTE_TYPES",MessageHelper.getCoreMessage("BO_INTITULE")); %>

<%univFmt.insererSaisieStructure( fmt, out, infoBean, "CODE_STRUCTURE", fmt.SAISIE_FACULTATIF,MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT")); %>


</table>

</div>

<div class="footer_popup">
<%-- 
       ********************************************************************
       *********      BOUTONS DE BAS DE PAGE                  *************
       ********************************************************************
--%>


<%  fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_VALIDER} ); %>
</div>


    </form>
  </body>
</html>