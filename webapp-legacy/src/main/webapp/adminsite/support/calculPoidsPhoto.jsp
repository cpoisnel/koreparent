<%@page buffer="256kb" autoFlush="true" language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.File"%>
<%@page import="java.util.List"%>
<%@page import="com.jsbsoft.jtf.session.SessionUtilisateur"%>
<%@page import="com.jsbsoft.jtf.webutils.ContextePage"%>
<%@page import="com.kosmos.service.impl.ServiceManager"%>
<%@page import="com.univ.objetspartages.bean.MediaBean"%>
<%@page import="com.univ.objetspartages.om.AutorisationBean"%>
<%@ page import="com.univ.objetspartages.services.ServiceMedia" %>
<%@ page import="com.univ.objetspartages.util.MediaUtils" %>
<%@ page import="com.univ.utils.SessionUtil" %>
<%
    final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
    final AutorisationBean autorisationParam = (AutorisationBean) SessionUtil.getInfosSession(request).get(SessionUtilisateur.AUTORISATIONS);
    //si l'utilisateur a perdu sa session, ou bien qu'il n'a pas les droits sur l'onglet administration, on le redirige vers le login
    if (SessionUtil.getInfosSession(request).get(SessionUtilisateur.CODE)    == null    || autorisationParam ==    null ||    ! autorisationParam.isWebMaster()) {
        response.sendRedirect(response.encodeRedirectURL("/adminsite/"));
        return;
    }
%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>K-Portal 5.1 Maintenance : re calcul du poids des photos de la table MEDIA</title>
</head>
<body>
<%
ContextePage ctx = null;
String sMedia;
int count = 0;
int erreur = 0;
try{
    out.println("[INFO] début du traitement <br />");
    ctx = new ContextePage();
    List<MediaBean> medias = serviceMedia.getByResourceType("photo");
    for (MediaBean currentMedia : medias) {
        count ++;
        sMedia = "source : " + MediaUtils.getUrlAbsolue(currentMedia) + "<br />";
        try {
            File ressource = new File(MediaUtils.getPathAbsolu(currentMedia));
            if (ressource.exists()){
                currentMedia.setPoids(ressource.length());
                serviceMedia.update(currentMedia);
                out.println("[OK] Mise à jour du poids du fichier "+sMedia);
            }
            else {
                out.println("[ERREUR] le fichier n'existe pas "+sMedia);
            }
        } catch (Exception e) {
            out.println("[ERREUR] "+e.getMessage()+" "+sMedia);
        }
        out.flush();
    }

}catch (Exception e) {
    out.println("[ERREUR] " +e.getMessage());
}
finally{
    if (ctx!=null){
        ctx.release();
    }
}

out.println("[INFO] fin du traitement, "+count+" photo(s) traitee(s), "+erreur+" erreur(s)");
%>

</body>
</html>