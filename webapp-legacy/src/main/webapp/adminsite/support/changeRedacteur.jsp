<%@page import="com.jsbsoft.jtf.session.SessionUtilisateur"%>
<%@page import="com.kportal.rubrique.RubriqueManager"%>
<%@page import="com.univ.objetspartages.om.AutorisationBean"%>
<%@page import="com.univ.objetspartages.om.InfosRubriques"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.SessionUtil"%>
<%@ page import="com.univ.objetspartages.om.Rubrique" %>
<%
try{
AutorisationBean autorisationParam = (AutorisationBean) SessionUtil.getInfosSession(request).get(SessionUtilisateur.AUTORISATIONS);
//si l'utilisateur a perdu sa session, ou    bien qu'il n'a pas les droits sur l'onglet administration, on le redirige vers le login
if (SessionUtil.getInfosSession(request).get(SessionUtilisateur.CODE)    == null    || autorisationParam ==    null ||    ! autorisationParam.isWebMaster()) {
    response.sendRedirect(response.encodeRedirectURL("/adminsite/"));
    return;
}

out.write("*** Début Suppression ***<br /><br />");

String codeRubrique = "";
String oldUser = "";
String newUser = "";
boolean recursif = false;
if (request.getParameter("CODE")!=null)
    codeRubrique = request.getParameter("CODE");
if (request.getParameter("OLDUSER")!=null)
    oldUser = request.getParameter("OLDUSER");
if (request.getParameter("NEWUSER")!=null)
    newUser = request.getParameter("NEWUSER");
if (request.getParameter("RECURSIF")!=null)
    recursif = request.getParameter("RECURSIF").equalsIgnoreCase("true");

out.flush();

InfosRubriques infosRubriques = Rubrique.renvoyerItemRubrique(codeRubrique);
if (infosRubriques.getCode().length()==0)
    out.write("Rubrique inconnue");
else{
ContexteUniv ctx = new ContexteUniv(pageContext);

try{
    RubriqueManager.changeRedacteur(ctx,infosRubriques,oldUser,newUser,recursif);
}catch(Exception e){
    out.write("Une erreur est survenue, consulter les logs : "+ e.getMessage());
}finally{
    ctx.release();
}

}
}catch(Exception e){
    out.write("Une erreur est survenue, consulter les logs : "+e.getMessage());
}
out.write("<br />*** Fin ***");
out.flush();
%>
