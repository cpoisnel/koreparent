// Noeud de l'arbre
function Node(sCode, sLabel, bOpened, bSelectable) {
    // Données de l'élément
    this.sCode = sCode;             // code de l'élément
    this.sLabel = sLabel;           // libellé de l'élément
    this.bOpened = bOpened;         // élément ouvert ou fermé
    this.bSelectable = bSelectable; // élément sélectionnable ou non
    this.iLevel = 0;                // niveau de l'élément
    this.oParent;                   // parent de l'élément
    this.aChildNodes = new Array(); // fils de l'élément

    this.iNbPrintedChildren;        // nombre de fils déjà affichés

    // Ajoute un fils à l'élément
    this.AddChild = function (oNode) {
        this.aChildNodes[this.aChildNodes.length] = oNode;
        oNode.oParent = this;
        oNode.iLevel = this.iLevel + 1;
    }

    // Teste si l'élément est le dernier fils à afficher
    this.IsLastChildToPrint = function () {
        return (this.oParent == null || this.oParent.iNbPrintedChildren == this.oParent.aChildNodes.length);
    }

    // Renvoie une représentation de l'élément sous forme de chaine
    this.ToString = function () {
        var s = '[code=' + this.sCode + '; ';
        s += 'sLabel=' + this.sLabel + '; ';
        s += 'bOpened=' + this.bOpened + '; ';
        s += 'bSelectable=' + this.bSelectable + '; ';
        s += 'level=' + this.iLevel + '; ';
        if (this.oParent) {
            s += 'parent=' + this.oParent.sLabel + '; ';
        }
        s += 'nbchildren=' + this.aChildNodes.length + ']';
        return s;
    }

    // Affiche l'élément
    this.Draw = function () {
        var s = '';
        this.iNbPrintedChildren = 0; // réinitialise le nb de fils affichés
        if (this.oParent) // on n'est pas à la racine
        {
            // affiche l'image pour les niveaux supérieurs
            var oParentNode = this.oParent;
            while (oParentNode.iLevel > 0) {
                // précise comment s'affiche le niveau courant pour ses fils
                s = '<img src="/adminsite/menu/' + (oParentNode.IsLastChildToPrint() ? 'blank.gif' : 'line.gif') + '" align="top" width="19" height="16">' + s;
                oParentNode = oParentNode.oParent;
            }

            var sButton;
            if (this.aChildNodes.length > 0) // dossier
            {
                var sAction;
                if (this.bOpened) {
                    sAction = 'oTree.CollapseNode(\'' + this.sCode + '\');';
                    sButton = (this.IsLastChildToPrint() ? 'minusbottom.gif' : 'minus.gif');
                }
                else {
                    sAction = 'oTree.ExpandNode(\'' + this.sCode + '\');';
                    sButton = (this.IsLastChildToPrint() ? 'plusbottom.gif' : 'plus.gif');
                }
                s += '<a href="#" onclick="' + sAction + ' return false;" class="liens">';
                s += '<img src="/adminsite/menu/' + sButton + '" border="0" align="top">';
                s += '</a>';
            }
            else // élément sans fils
            {
                sButton = (this.IsLastChildToPrint() ? 'joinbottom.gif' : 'join.gif');
                s += '<img src="/adminsite/menu/' + sButton + '" border="0" align="top">';
            }
        }

        // affichage de l'icone de l'élément
        var sIcon;
        if (!this.oParent) // racine de l'arbre
        {
            sIcon = oTree.sRootIcon;
        }
        else if (this.aChildNodes.length > 0) // dossier
        {
            if (this.bOpened) {
                sIcon = 'folderopen.gif';
            }
            else {
                sIcon = 'folder.gif';
            }
        }
        else // élément sans fils
        {
            //sIcon = 'page.gif';
            sIcon = 'folder.gif';
        }
        s += '<img src="/adminsite/menu/' + sIcon + '" border="0" align="top">';

        // affichage du texte de l'élément
        if (this.bSelectable) {
            s += '<a href="#" onclick="oTree.SelectNode(\'' + this.sCode.replace(/'/g, "\\'") + '\')" class="liens">';
            s += this.sLabel;
            s += '</a>';
        }
        else {
            s += this.sLabel;
        }
        s += '<br />\r\n';

        // affichage des fils de l'élément
        if (this.bOpened) {
            for (var i = 0; i < this.aChildNodes.length; i++) {
                this.iNbPrintedChildren++;
                s += this.aChildNodes[i].Draw();
            }
        }
        return s;
    }
}

// Arbre des rubriques / structures / groupes
function Tree() {
    // Données de l'arbre
    this.oRoot = new Node('', '', true, true, false); // racine de l'arbre
    this.aNodeList = new Array(); // liste des noeuds de l'arbre
    this.oCachedNode; // cache pour optimiser le chargement de l'arbre
    this.tLoadStartTime = (new Date()).getTime();

    // Initialise l'arbre
    this.Init = function (sMode, sPermission, sLangue, sPublicVise, sFiltre, bFront, sRootIcon) {
        // informations sur l'arbre
        this.sMode = sMode; // RUBRIQUE / STRUCTURE / GROUPE
        this.sPermission = sPermission; // ex : FICHE/0016/M/
        this.sLangue = sLangue;
        this.sPublicVise = sPublicVise;
        this.sFiltre = sFiltre;
        this.bFront = bFront;
        this.sRootIcon = sRootIcon; // icone associée à la racine de l'arbre
        this.RefreshDisplay();
        //alert('Chargement de l\'arbre OK : ' + this.aNodeList.length + ' éléments chargés (tps de chargement : ' + ((new Date()).getTime() - this.tLoadStartTime) + ' ms)');
    }

    // Ajoute un élément à l'arbre
    this.AddNode = function (sParentCode, sCode, sLabel, bOpened, bSelectable) {
        // crée l'élément
        var oNode = new Node(sCode, sLabel, bOpened, bSelectable);
        // ajoute l'élément à la liste
        this.aNodeList[this.aNodeList.length] = oNode;
        // met à jour l'élément parent
        var oParent = this.GetNode(sParentCode);
        if (!oParent) {
            oParent = this.oRoot;
        }
        oParent.AddChild(oNode);
        // mise en cache du nouvel élément inséré
        this.oCachedNode = oNode;
        /*if (this.aNodeList.length % 2000 == 0)
         {
         alert((new Date()).getTime() - this.tLoadStartTime + ' ms'); // tps negligeable
         this.tLoadStartTime = (new Date()).getTime();
         }*/
    }

    // Renvoie l'élément demandé
    this.GetNode = function (sCode) {
        var oNode;
        // vérifie si l'élément recherché n'est pas la racine
        if (sCode == '') {
            oNode = this.oRoot;
        }
        // vérifie si l'élément recherché n'est pas en cache
        while (this.oCachedNode && !oNode) {
            if (this.oCachedNode.sCode == sCode) {
                oNode = this.oCachedNode;
            }
            else {
                this.oCachedNode = this.oCachedNode.oParent; // parent de l'élément en cache
            }
        }
        // sinon parcourt la liste des éléments
        if (!oNode) {
            for (var i = 0; i < this.aNodeList.length; i++) {
                if (this.aNodeList[i].sCode == sCode) {
                    oNode = this.aNodeList[i];
                }
            }
        }
        return oNode;
    }

    // Met à jour l'affichage de l'arbre
    this.RefreshDisplay = function () {
        document.getElementById('menu').innerHTML = this.oRoot.Draw();
    }

    // Ouvre un noeud de l'arbre
    this.ExpandNode = function (sCode) {
        this.GetNode(sCode).bOpened = true;
        this.RefreshDisplay();
    }

    // Ferme un noeud de l'arbre
    this.CollapseNode = function (sCode) {
        this.GetNode(sCode).bOpened = false;
        this.RefreshDisplay();
    }

    // Sélectionne un noeud de l'arbre
    this.SelectNode = function (sCode) {
        var oNode = this.GetNode(sCode);
        DoOnSelectNode(oNode);
    }
}

// Instance de l'arbre
var oTree = new Tree();
