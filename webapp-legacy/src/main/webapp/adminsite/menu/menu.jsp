<%@ page import="com.jsbsoft.jtf.session.SessionUtilisateur, com.univ.objetspartages.om.AutorisationBean, com.univ.objetspartages.om.Groupedsi, com.univ.objetspartages.om.Rubrique, com.univ.utils.SessionUtil" errorPage="/adminsite/jsbexception.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%
AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(request).get(SessionUtilisateur.AUTORISATIONS);
String permission = request.getParameter("PERMISSION");
if (permission == null)
    permission = "";

//si l'utilisateur a perdu sasession, ou bien qu'il n'a pas les droits sur l'onglet administration, on le redirige vers le login
//si on a pas de controle des permissions, on a pas besoin de rediriger (par exemple formulaire de recherche ou saisie front anonyme)
if ((SessionUtil.getInfosSession(request).get(SessionUtilisateur.CODE) == null || autorisations == null)) {
    request.getRequestDispatcher("/adminsite/").forward(request, response);
} else {

    String mode = request.getParameter("MODE"); // RUBRIQUE / STRUCTURE / GROUPE
    if (mode == null)
        mode = "";

    // JSS 20050510 : profils dynamiques
    // Pour assurer la compatibilité avec l'ancienne codification,
    // on met un séparateur devant le code

    String publicVise = request.getParameter("PUBLIC_VISE");
    if (publicVise == null)
        publicVise = "";

    String prefixeCode = "";
    if (publicVise.equals("1"))
        prefixeCode = "/";

    String code = request.getParameter("CODE");
    if (code == null)    {
        code = (mode.equals("STRUCTURE") ? "00" : "");
    }

    String langue = request.getParameter("LANGUE");
    if (langue == null)
        langue = "0";

    String filtre = request.getParameter("FILTRE");
    if (filtre == null)
        filtre = "";

    String front = request.getParameter("FRONT");

    String rootIcon = "folderopen.png";
    if (mode.equals("RUBRIQUE"))
        rootIcon = "rubriques.png";
    else if (mode.equals("STRUCTURE"))
        rootIcon = "structures.png";
    else if (mode.equals("GROUPE"))
        rootIcon = "groupes.png";
%>

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Arbre des <%= mode.toLowerCase() %>s</title>
<link rel="stylesheet" type="text/css" href="/adminsite/menu/tree.css" />
<script type="text/javascript" src="/adminsite/menu/tree.js"></script>
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
<script type="text/javascript">
    // renvoie le noeud sélectionné
    function DoOnSelectNode(oNode)
    {
        var sCode = oNode.sCode;
        var sLabel = oNode.sLabel;
        var sFilAriane = oNode.sLabel;
        var sMode = oTree.sMode;
        while (oNode.oParent)
        {
            oNode = oNode.oParent;
            if (oNode.sLabel.length > 0)
            {
                sFilAriane = oNode.sLabel + " > " + sFilAriane;
            }
        }
        window.opener.saveField(sMode, sCode, sLabel, sFilAriane);
        if (window.opener.specificActionOnSelectInTree)
        {
            window.opener.specificActionOnSelectInTree(sMode);
        }
        window.close();
    }

    // methode de chargement de l'arbre
<% 
    String racine = request.getParameter("RACINE");
    if( racine == null )
        racine = "";

    if (mode.equals("RUBRIQUE")) { %>
    <%= Rubrique.getArbreJavaScript(code, autorisations, permission, racine) %>
<% } else if (mode.equals("GROUPE")) { %>
    <%= Groupedsi.getArbreJavaScript(code, autorisations, permission) %>
<% } %>
</script>
</head>

<body onload="oTree.Init('<%= mode %>', '<%= permission %>', '<%= langue %>', '<%= publicVise %>', '<%= filtre %>', <%= (front == null ? "false" : front) %>, '<%= rootIcon %>');">

<% if (mode.equals("GROUPE")) { %>
<div style="text-align: right">
    [ <a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=RECHERCHE_GROUPE_DSI&amp;PUBLIC_VISE=<%=publicVise%>&amp;ACTION=RECHERCHER&amp;PERMISSION=<%=permission%>">Rechercher par critères</a> ]
</div>
<% } %>

<p id="legend-menu">Cliquer sur les icones <img src="plusbottom.gif" alt="Plus" /> pour déplier les branches de l'arbre et <img src="minusbottom.gif" alt="Moins" /> pour les refermer.<br/>Cliquer sur un intitulé pour sélectionner l'élément correspondant.</p>

<div id="menu"></div>

</body>
</html>

<% } %>
