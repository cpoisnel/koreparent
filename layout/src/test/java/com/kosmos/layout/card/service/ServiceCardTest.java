package com.kosmos.layout.card.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.bean.PictureCardBean;
import com.kosmos.layout.card.bean.SimpleCardBean;
import com.kosmos.layout.card.bean.ToolboxCardBean;
import com.kosmos.layout.card.dao.CardDAO;
import com.kosmos.layout.card.util.CardDescription;
import com.kosmos.layout.card.util.CardList;
import com.kosmos.layout.exception.CardNotFoundException;
import com.kosmos.layout.grid.impl.SimpleGrid;
import com.kosmos.layout.meta.bean.CardMetaBean;
import com.kosmos.layout.meta.dao.CardMetaDAO;
import com.kosmos.layout.slot.impl.GridSlot;
import com.kosmos.layout.slot.util.SlotList;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.util.MetatagUtils;

import mockit.Expectations;
import mockit.Invocation;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.Verifications;

@Test
@ContextConfiguration(locations = {"classpath:com/kosmos/layout/card/service/ServiceCardTest.test-context.xml"})
public class ServiceCardTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ServiceCard serviceCard;

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testSave() {
        serviceCard.save(null);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testDelete() {
        serviceCard.delete(null);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testGetById() {
        serviceCard.getById(null);
    }

    @Test
    public void testGetAllowedCardsPool() {
        new MockUp<ApplicationContextManager>() {
            @Mock
            public <T> Map<String, T> getAllBeansOfTypeByExtension(final Class<T> type) {
                return (Map<String, T>) MockedCardUtil.getCardsDescriptionFromContext();
            }
        };
        serviceCard.refresh();
        final CardList list1 = serviceCard.getAllowedCardsPool(MockedCardUtil.getPartialAllowedCardTypes());
        Assert.assertNotNull(list1, "La liste retournée ne doit pas être null.");
        Assert.assertEquals(list1.size(), 2, "La liste retournée doit contenir 2 éléments.");
        final CardList list2 = serviceCard.getAllowedCardsPool(MockedCardUtil.getAllowedCardTypes());
        Assert.assertNotNull(list2, "La liste retournée ne doit pas être null.");
        Assert.assertEquals(list2.size(), 3, "La liste retournée doit contenir 3 éléments.");
        final CardList list3 = serviceCard.getAllowedCardsPool(null);
        Assert.assertNotNull(list3, "La liste retournée ne doit pas être null.");
        Assert.assertTrue(list3.isEmpty(), "La liste retournée doit être vide.");
    }

    @Test
    public void testGetAllowedCardBoView() {
        new MockUp<ApplicationContextManager>() {
            @Mock
            public Map<String, Collection<CardDescription>> getAllBeansOfTypeByExtension(final Class<T> type) {
                return MockedCardUtil.getCardsDescriptionFromContext();
            }
        };
        serviceCard.refresh();
        final Map<String, String> map1 = serviceCard.getAllowedCardBoView(MockedCardUtil.getPartialAllowedCardTypes());
        Assert.assertNotNull(map1, "La map retournée ne doit pas être null.");
        Assert.assertEquals(map1.size(), 2, "La map retournée doit contenir 2 éléments.");
        Assert.assertTrue(map1.containsValue("/adminsite/layout/card/bo/view/simpleCardView.jsp"), "La map retournée doit contenir la valeur \"/adminsite/layout/card/bo/view/simpleCardView.jsp\"");
        Assert.assertTrue(map1.containsValue("/adminsite/layout/card/bo/view/toolboxCardView.jsp"), "La map retournée doit contenir la valeur \"/adminsite/layout/card/bo/view/toolboxCardView.jsp\"");
        final Map<String, String> map2 = serviceCard.getAllowedCardBoView(MockedCardUtil.getAllowedCardTypes());
        Assert.assertNotNull(map2, "La liste retournée ne doit pas être null.");
        Assert.assertEquals(map2.size(), 3, "La liste retournée doit contenir 3 éléments.");
        Assert.assertTrue(map2.containsValue("/adminsite/layout/card/bo/view/simpleCardView.jsp"), "La map retournée doit contenir la valeur \"/adminsite/layout/card/bo/view/simpleCardView.jsp\"");
        Assert.assertTrue(map2.containsValue("/adminsite/layout/card/bo/view/toolboxCardView.jsp"), "La map retournée doit contenir la valeur \"/adminsite/layout/card/bo/view/toolboxCardView.jsp\"");
        Assert.assertTrue(map2.containsValue("/adminsite/layout/card/bo/view/pictureCardView.jsp"), "La map retournée doit contenir la valeur \"/adminsite/layout/card/bo/view/pictureCardView.jsp\"");
        final Map<String, String> map3 = serviceCard.getAllowedCardBoView(null);
        Assert.assertNotNull(map3, "La liste retournée ne doit pas être null.");
        Assert.assertTrue(map3.isEmpty(), "La liste retournée doit être vide.");
    }

    @Test
    public void testAddCardForMeta(@Mocked({"add", "update"}) final CardDAO mockedCardDAO, @Mocked({"add", "getCardMeta"}) final CardMetaDAO mockedCardMetaDao) {
        final CardDAO cardDAO = new CardDAO();
        final CardMetaDAO cardMetaDAO = new CardMetaDAO();
        serviceCard.setDao(cardDAO);
        serviceCard.setCardMetaDao(cardMetaDAO);
        new Expectations(){{
            cardMetaDAO.getCardMeta(anyLong, anyLong);
            result = null;
        }};
        serviceCard.addCardForMeta(new SimpleCardBean(), new MetatagBean());
    }

    @Test
    public void testDeleteCardForMeta(@Mocked("delete") final CardDAO mockedCardDAO, @Mocked({"getCardMeta", "delete"}) final CardMetaDAO mockedCardMetaDao) {
        final CardDAO cardDAO = new CardDAO();
        final CardMetaDAO cardMetaDAO = new CardMetaDAO();
        serviceCard.setDao(cardDAO);
        serviceCard.setCardMetaDao(cardMetaDAO);
        new Expectations(){{
            cardMetaDAO.getCardMeta(anyLong, anyLong);
            result = new CardMetaBean();
        }};
        serviceCard.deleteCardForMeta(new SimpleCardBean(), new MetatagBean());
        serviceCard.deleteCardForMeta(null, new MetatagBean());
        serviceCard.deleteCardForMeta(new SimpleCardBean(), null);
        serviceCard.deleteCardForMeta(null, null);
    }

    @Test
    public void testUpdateCard(@Mocked("update") final CardDAO mockedCardDAO) {
        final CardBean test = new SimpleCardBean();
        test.setId(1L);
        final CardDAO cardDAO = new CardDAO();
        serviceCard.setDao(cardDAO);
        serviceCard.updateCard(test);
    }

    @Test
    public void testUpdateCardModel(@Mocked("update") final CardDAO mockedCardDAO) {
        final CardBean test1 = new SimpleCardBean();
        test1.setId(1L);
        final CardBean test2 = new SimpleCardBean();
        test2.setId(2L);
        final CardBean test3 = new SimpleCardBean();
        test3.setId(3L);
        final CardDAO cardDAO = new CardDAO();
        serviceCard.setDao(cardDAO);
        final Map<String, CardBean> model = new HashMap<>();
        model.put("plop1", test1);
        model.put("plop2", test2);
        model.put("plop3", test3);
        serviceCard.updateCardModel(model);
    }

    @Test
    public void testGetCardsForFicheUniv(@Mocked("getByMeta") final CardMetaDAO mockedCardMetaDao, @Mocked("getById") final CardDAO mockedCardDAO) {
        final CardDAO cardDAO = new CardDAO();
        final CardMetaDAO cardMetaDAO = new CardMetaDAO();
        serviceCard.setDao(cardDAO);
        serviceCard.setCardMetaDao(cardMetaDAO);
        final UUID uuid1 = UUID.randomUUID();
        final UUID uuid2 = UUID.randomUUID();
        final SimpleCardBean simpleCardBean = new SimpleCardBean();
        simpleCardBean.setKey(uuid1);
        final PictureCardBean pictureCardBean = new PictureCardBean();
        pictureCardBean.setKey(uuid2);
        final CardMetaBean cardMetaBean1 = new CardMetaBean();
        cardMetaBean1.setIdCard(1L);
        final CardMetaBean cardMetaBean2 = new CardMetaBean();
        cardMetaBean2.setIdCard(2L);
        final CardMetaBean cardMetaBean3 = new CardMetaBean();
        cardMetaBean3.setIdCard(3L);
        new MockUp<MetatagUtils>() {
            @Mock
            public MetatagBean lireMeta(Invocation context, final FicheUniv ficheUniv) {
                return context.getInvocationCount() > 1 ? null : new MetatagBean();
            }
        };
        new MockUp<MessageHelper>() {
            @Mock
            public String getCoreMessage(final String key) {
                return "Plop %d";
            }
        };
        new Expectations(){{
            cardMetaDAO.getByMeta(anyLong);
            result = Arrays.asList(cardMetaBean1, cardMetaBean2, cardMetaBean3);
            cardDAO.getById(1L);
            result = simpleCardBean;
            cardDAO.getById(2L);
            result = pictureCardBean;
            cardDAO.getById(3L);
            result = new DataSourceException("Plop");
        }};
        final Map<String, CardBean> map1 = serviceCard.getCards((FicheUniv) null);
        Assert.assertNotNull(map1, "La map renvoyée ne devrait pas être null");
        Assert.assertEquals(map1.size(), 2, "La map renvoyée devrait contenir 2 éléments");
        Assert.assertTrue(map1.containsKey(uuid1.toString()), "La map renvoyée devrait contenir la clé " + uuid1.toString());
        Assert.assertTrue(map1.containsKey(uuid2.toString()), "La map renvoyée devrait contenir la clé " + uuid2.toString());
        final Map<String, CardBean> map2 = serviceCard.getCards((FicheUniv) null);
        Assert.assertNotNull(map2, "La map renvoyée ne devrait pas être null");
        Assert.assertTrue(map2.isEmpty(), "La map renvoyée devrait être vide");
    }

    @Test
    public void testGetCardsForMetaId(@Mocked("getByMeta") final CardMetaDAO mockedCardMetaDao, @Mocked("getById") final CardDAO mockedCardDAO) {
        final CardDAO cardDAO = new CardDAO();
        final CardMetaDAO cardMetaDAO = new CardMetaDAO();
        final CardBean card = new SimpleCardBean();
        card.setKey(UUID.randomUUID());
        serviceCard.setDao(cardDAO);
        serviceCard.setCardMetaDao(cardMetaDAO);
        new MockUp<MessageHelper>() {
            @Mock
            public String getCoreMessage(final String key) {
                return "Plop %d";
            }
        };
        new Expectations(){{
            cardMetaDAO.getByMeta(anyLong);
            result = Collections.emptyList();
        }};
        final Map<String, CardBean> map1 = serviceCard.getCards(0L);
        Assert.assertNotNull(map1, "La map renvoyée ne devrait pas être null");
        Assert.assertTrue(map1.isEmpty(), "La map renvoyée devrait être vide");
        new Expectations(){{
            cardMetaDAO.getByMeta(anyLong);
            result = Collections.singletonList(new CardMetaBean());
            cardDAO.getById(anyLong);
            result = new DataSourceException("Plop");
        }};
        final Map<String, CardBean> map3 = serviceCard.getCards(0L);
        Assert.assertNotNull(map3, "La map renvoyée ne devrait pas être null");
        Assert.assertTrue(map3.isEmpty(), "La map renvoyée devrait être vide");
        new Expectations(){{
            cardMetaDAO.getByMeta(anyLong);
            result = Collections.singletonList(new CardMetaBean());
            cardDAO.getById(anyLong);
            result = card;
        }};
        final Map<String, CardBean> map2 = serviceCard.getCards(0L);
        Assert.assertNotNull(map2, "La map renvoyée ne devrait pas être null");
        Assert.assertEquals(map2.size(), 1, "La map renvoyée devrait contenir 1 élément");
    }

    @Test
    public void testGetCardById(@Mocked("getById") final CardDAO mockedCardDAO) throws CardNotFoundException {
        final CardDAO cardDAO = new CardDAO();
        final CardBean card = new SimpleCardBean();
        final UUID uuid = UUID.randomUUID();
        card.setKey(uuid);
        serviceCard.setDao(cardDAO);
        new Expectations(){{
            cardDAO.getById(anyLong);
            result = card;
        }};
        final CardBean result = serviceCard.getCardById(0L);
        Assert.assertNotNull(result, "La carte renvoyée ne devrait pas être null");
        Assert.assertEquals(result.getKey(), uuid, "La carte renvoyée devrait porter la clé " + uuid.toString());
    }

    @Test(expectedExceptions = CardNotFoundException.class)
    public void testGetCardByIdFailed(@Mocked("getById") final CardDAO mockedCardDAO) throws CardNotFoundException {
        final CardDAO cardDAO = new CardDAO();
        final CardBean card = new SimpleCardBean();
        serviceCard.setDao(cardDAO);
        new MockUp<MessageHelper>() {
            @Mock
            public String getCoreMessage(final String key) {
                return "Plop %d";
            }
        };
        new Expectations(){{
            cardDAO.getById(anyLong);
            result = new DataSourceException("Plop");
        }};
        serviceCard.getCardById(0L);
    }

    @Test
    public void testGetAllowedEditFragment() {
        final Map<String, String> allowedEditFragments = serviceCard.getAllowedEditFragments(MockedCardUtil.getPartialCardPool());
        Assert.assertNotNull(allowedEditFragments, "La map retournée ne devrait pas être null");
        Assert.assertEquals(allowedEditFragments.size(), 2, "La map retournée contenir 2 éléments");
        Assert.assertTrue(allowedEditFragments.containsKey("com.kosmos.layout.card.bean.SimpleCardBean"), "La map retournée devrait contenir \"com.kosmos.layout.card.bean.SimpleCardBean\"");
        Assert.assertEquals(allowedEditFragments.get("com.kosmos.layout.card.bean.SimpleCardBean"), "/adminsite/layout/card/bo/edit/simpleCardEdit.jsp", "La map retournée devrait contenir la valeur \"/adminsite/layout/card/bo/edit/simpleCardEdit.jsp\" pour la clé\"com.kosmos.layout.card.bean.SimpleCardBean\"");
        Assert.assertTrue(allowedEditFragments.containsKey("com.kosmos.layout.card.bean.ToolboxCardBean"), "La map retournée devrait contenir \"com.kosmos.layout.card.bean.ToolboxCardBean\"");
        Assert.assertEquals(allowedEditFragments.get("com.kosmos.layout.card.bean.ToolboxCardBean"), "/adminsite/layout/card/bo/edit/toolboxCardEdit.jsp", "La map retournée devrait contenir la valeur \"/adminsite/layout/card/bo/edit/toolboxCardEdit.jsp\" pour la clé\"com.kosmos.layout.card.bean.ToolboxCardBean\"");
        final Map<String, String> allowedEditFragments2 = serviceCard.getAllowedEditFragments(MockedCardUtil.getCardPool());
        Assert.assertNotNull(allowedEditFragments2, "La map retournée ne devrait pas être null");
        Assert.assertEquals(allowedEditFragments2.size(), 3, "La map retournée contenir 2 éléments");
        Assert.assertTrue(allowedEditFragments2.containsKey("com.kosmos.layout.card.bean.SimpleCardBean"), "La map retournée devrait contenir \"com.kosmos.layout.card.bean.SimpleCardBean\"");
        Assert.assertEquals(allowedEditFragments2.get("com.kosmos.layout.card.bean.SimpleCardBean"), "/adminsite/layout/card/bo/edit/simpleCardEdit.jsp", "La map retournée devrait contenir la valeur \"/adminsite/layout/card/bo/edit/simpleCardEdit.jsp\" pour la clé\"com.kosmos.layout.card.bean.SimpleCardBean\"");
        Assert.assertTrue(allowedEditFragments2.containsKey("com.kosmos.layout.card.bean.ToolboxCardBean"), "La map retournée devrait contenir \"com.kosmos.layout.card.bean.ToolboxCardBean\"");
        Assert.assertEquals(allowedEditFragments2.get("com.kosmos.layout.card.bean.ToolboxCardBean"), "/adminsite/layout/card/bo/edit/toolboxCardEdit.jsp", "La map retournée devrait contenir la valeur \"/adminsite/layout/card/bo/edit/toolboxCardEdit.jsp\" pour la clé\"com.kosmos.layout.card.bean.ToolboxCardBean\"");
        Assert.assertTrue(allowedEditFragments2.containsKey("com.kosmos.layout.card.bean.PictureCardBean"), "La map retournée devrait contenir \"com.kosmos.layout.card.bean.PictureCardBean\"");
        Assert.assertEquals(allowedEditFragments2.get("com.kosmos.layout.card.bean.PictureCardBean"), "/adminsite/layout/card/bo/edit/pictureCardEdit.jsp", "La map retournée devrait contenir la valeur \"/adminsite/layout/card/bo/edit/pictureCardEdit.jsp\" pour la clé\"com.kosmos.layout.card.bean.PictureCardBean\"");
    }

    @Test
    public void testCleanUpModel(@Mocked({"getCards", "deleteCardForMeta"}) final ServiceCard mockedServiceCard) {
        new MockUp<MetatagUtils>() {
            @Mock
            public MetatagBean lireMeta(final FicheUniv ficheUniv) {
                return new MetatagBean();
            }
        };
        final UUID uuid1 = UUID.randomUUID();
        final UUID uuid2 = UUID.randomUUID();
        final Map<String, CardBean> providedModel = new HashMap<>();
        providedModel.put(uuid1.toString(), new SimpleCardBean());
        providedModel.put(uuid2.toString(), new PictureCardBean());
        final Map<String, CardBean> testModel = new HashMap<>();
        testModel.put(uuid1.toString(), new SimpleCardBean());
        testModel.put(uuid2.toString(), new PictureCardBean());
        testModel.put(UUID.randomUUID().toString(), new PictureCardBean());
        testModel.put(UUID.randomUUID().toString(), new PictureCardBean());
        new Expectations(){{
            serviceCard.getCards((FicheUniv) null);
            result = testModel;
        }};
        serviceCard.cleanUpModel(null, providedModel);
        new Verifications(){{
            mockedServiceCard.deleteCardForMeta(withAny(new PictureCardBean()), withAny(new MetatagBean())); times = 2;
        }};
    }

    @Test
    public void testRefreshFailed() {
        new MockUp<ApplicationContextManager>() {
            @Mock
            public Map<String, Collection<CardDescription>> getAllBeansOfTypeByExtension(final Class<T> type) {
                return MockedCardUtil.getCardsDescriptionFromContext();
            }
        };
        new MockUp<Class<? extends CardBean>>() {
            @Mock
            public T newInstance() throws InstantiationException, IllegalAccessException {
                throw new InstantiationException();
            }
        };
        serviceCard.refresh();
    }

    @Test
    public void testGetAllowedCardTypes() {
        new MockUp<ApplicationContextManager>() {
            @Mock
            public Map<String, Collection<CardDescription>> getAllBeansOfTypeByExtension(final Class<T> type) {
                return MockedCardUtil.getCardsDescriptionFromContext();
            }
        };
        serviceCard.refresh();
        final UUID uuid1 = UUID.randomUUID();
        final SimpleGrid simpleGrid = new SimpleGrid();
        final SlotList slotList = new SlotList();
        final GridSlot gridSlot1 = new GridSlot();
        gridSlot1.setKey(uuid1);
        gridSlot1.setAllowedCardTypes(Arrays.asList(SimpleCardBean.class, ToolboxCardBean.class));
        slotList.add(gridSlot1);
        simpleGrid.setSlots(slotList);
        // Premier appel
        final Map<String, Collection<Class<? extends CardBean>>> allowedTypes = serviceCard.getAllowedCardTypes(simpleGrid);
        Assert.assertNotNull(allowedTypes, "La map retournée ne devrait pas être null");
        Assert.assertEquals(allowedTypes.size(), 1, "La map retournée devrait contenir 1 élément");
        Assert.assertNotNull(allowedTypes.containsKey(uuid1.toString()), "La map retournée devrait contenir la clé " + uuid1.toString());
        final Collection<Class<? extends CardBean>> classes = allowedTypes.get(uuid1.toString());
        Assert.assertNotNull(classes, "La liste retournée ne devrait pas être null");
        Assert.assertEquals(classes.size(), 2, "La liste retournée devrait contenir 2 éléments");
        Assert.assertTrue(classes.contains(SimpleCardBean.class), "La liste retournée devrait contenir la classe SimpleCardBean");
        Assert.assertTrue(classes.contains(ToolboxCardBean.class), "La liste retournée devrait contenir la classe ToolboxCardBean");
        //Préparation de l'appel suivant
        final UUID uuid2 = UUID.randomUUID();
        final SlotList slotList2 = new SlotList();
        final GridSlot gridSlot2 = new GridSlot();
        gridSlot2.setKey(uuid2);
        slotList2.add(gridSlot2);
        slotList2.add(gridSlot1);
        simpleGrid.setSlots(slotList2);
        //Deuxième appel
        final Map<String, Collection<Class<? extends CardBean>>> allowedTypes2 = serviceCard.getAllowedCardTypes(simpleGrid);
        Assert.assertNotNull(allowedTypes2, "La map retournée ne devrait pas être null");
        Assert.assertEquals(allowedTypes2.size(), 2, "La map retournée devrait contenir 2 éléments");
        Assert.assertNotNull(allowedTypes2.containsKey(uuid1.toString()), "La map retournée devrait contenir la clé " + uuid1.toString());
        Assert.assertNotNull(allowedTypes2.containsKey(uuid2.toString()), "La map retournée devrait contenir la clé " + uuid2.toString());
        final Collection<Class<? extends CardBean>> classes2 = allowedTypes2.get(uuid2.toString());
        Assert.assertNotNull(classes2, "La liste retournée ne devrait pas être null");
        Assert.assertEquals(classes2.size(), 3, "La liste retournée devrait contenir 3 éléments");
        Assert.assertTrue(classes2.contains(SimpleCardBean.class), "La liste retournée devrait contenir la classe SimpleCardBean");
        Assert.assertTrue(classes2.contains(ToolboxCardBean.class), "La liste retournée devrait contenir la classe ToolboxCardBean");
        Assert.assertTrue(classes2.contains(PictureCardBean.class), "La liste retournée devrait contenir la classe PictureCardBean");
        //Préparation de l'appel suivant
        final Collection<Class<? extends CardBean>> layoutAllowed = new ArrayList<>();
        layoutAllowed.add(PictureCardBean.class);
        simpleGrid.setAllowedCardTypes(layoutAllowed);
        //Troisième appel
        final Map<String, Collection<Class<? extends CardBean>>> allowedTypes3 = serviceCard.getAllowedCardTypes(simpleGrid);
        Assert.assertNotNull(allowedTypes3, "La map retournée ne devrait pas être null");
        Assert.assertEquals(allowedTypes3.size(), 2, "La map retournée devrait contenir 2 éléments");
        Assert.assertNotNull(allowedTypes3.containsKey(uuid1.toString()), "La map retournée devrait contenir la clé " + uuid1.toString());
        Assert.assertNotNull(allowedTypes3.containsKey(uuid2.toString()), "La map retournée devrait contenir la clé " + uuid2.toString());
        final Collection<Class<? extends CardBean>> classes3 = allowedTypes3.get(uuid2.toString());
        Assert.assertNotNull(classes3, "La liste retournée ne devrait pas être null");
        Assert.assertEquals(classes3.size(), 1, "La liste retournée devrait contenir 1 élément");
        Assert.assertTrue(classes3.contains(PictureCardBean.class), "La liste retournée devrait contenir la classe PictureCardBean");
        //Dernier appel
        final Map<String, Collection<Class<? extends CardBean>>> allowedTypes4 = serviceCard.getAllowedCardTypes(null);
        Assert.assertNotNull(allowedTypes4, "La map retournée ne devrait pas être null");
        Assert.assertTrue(allowedTypes4.isEmpty(), "La map retournée devrait être vide");
    }

    @Test
    public void testGetAllowedCardsDescriptions() {
        new MockUp<ApplicationContextManager>() {
            @Mock
            public Map<String, Collection<CardDescription>> getAllBeansOfTypeByExtension(final Class<T> type) {
                return MockedCardUtil.getCardsDescriptionFromContext();
            }
        };
        serviceCard.refresh();
        final UUID uuid1 = UUID.randomUUID();
        final UUID uuid2 = UUID.randomUUID();
        final SimpleGrid simpleGrid = new SimpleGrid();
        final SlotList slotList = new SlotList();
        final GridSlot gridSlot1 = new GridSlot();
        gridSlot1.setKey(uuid1);
        gridSlot1.setAllowedCardTypes(Arrays.asList(SimpleCardBean.class, ToolboxCardBean.class));
        slotList.add(gridSlot1);
        simpleGrid.setSlots(slotList);
        final GridSlot gridSlot2 = new GridSlot();
        gridSlot2.setKey(uuid2);
        slotList.add(gridSlot2);
        simpleGrid.setSlots(slotList);
        final Collection<CardDescription> descriptions = serviceCard.getAllowedCardsDescriptions(simpleGrid);
        Assert.assertNotNull(descriptions, "La collection retournée ne devrait pas être null");
        Assert.assertEquals(descriptions.size(), 3, "La collection retournée devrait contenir 3 éléments");
        final Collection<CardDescription> descriptionsNull = serviceCard.getAllowedCardsDescriptions(null);
        Assert.assertNotNull(descriptionsNull, "La collection retournée ne devrait pas être null");
        Assert.assertTrue(descriptionsNull.isEmpty(), "La collection retournée devrait être vide");
    }
}
