package com.kosmos.layout.grid.service;

import com.kosmos.layout.Layout;
import com.kosmos.layout.grid.impl.ContainerGrid;
import com.kosmos.layout.impl.AbstractLayout;
import com.kosmos.layout.slot.impl.ContainerSlot;
import com.kosmos.layout.slot.util.SlotList;
import com.kosmos.service.impl.ServiceManager;
import mockit.Expectations;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by alexandre.baillif on 02/12/16.
 */
@Test @ContextConfiguration(locations = { "classpath:com/kosmos/layout/service/ServiceGridLayoutTest.test-context.xml" })
public class ServiceGridLayoutTest extends AbstractTestNGSpringContextTests {

	@Autowired
	ServiceGridLayout serviceGridLayout;

	@Test
	public void testUpdateLayoutModel(){
		final Map<String,Object> datas = new HashMap<>();
		ContainerGrid newlayout = new ContainerGrid();
		ContainerGrid containerGrid = new ContainerGrid();
		SlotList slots = new SlotList();
		SlotList slots2 = new SlotList();
		for(int i=0; i<6 ; ++i) {
			for(int j=0; j<4; ++j) {
				ContainerSlot slot = new ContainerSlot();
				slot.setRow(i);
				slot.setColumn(j);
				UUID key = UUID.randomUUID();
				slot.setKey(key);
				datas.put("slot-row-"+ key,String.valueOf(j));
				datas.put("slot-column-"+ key,String.valueOf(i));
				slots.add(slot);
				slots2.add(slot);
			}
		}
		containerGrid.setSlots(slots);
		newlayout.setSlots(slots2);
		serviceGridLayout.updateLayoutModel(containerGrid,datas,newlayout);
	}


	@Test(expectedExceptions = NotImplementedException.class) public void testSave(){
		serviceGridLayout.save(new ContainerGrid());
	}


	@Test(expectedExceptions = NotImplementedException.class) public void testGetById(){
		serviceGridLayout.getById(0L);
	}

	@Test(expectedExceptions = NotImplementedException.class) public void testDelete(){
		serviceGridLayout.delete(0L);
	}
}
