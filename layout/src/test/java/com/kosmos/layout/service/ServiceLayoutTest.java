package com.kosmos.layout.service;

import java.util.*;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jsbsoft.jtf.datasource.dao.CommonDAO;
import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.kosmos.layout.grid.GridLayout;
import com.kosmos.layout.grid.impl.AbstractGrid;
import com.kosmos.layout.grid.impl.ContainerGrid;
import com.kosmos.layout.grid.service.ServiceGridLayout;
import com.kosmos.layout.impl.AbstractLayout;
import com.kosmos.layout.slot.impl.ContainerSlot;
import com.kosmos.layout.slot.util.SlotList;
import com.kosmos.service.impl.ServiceManager;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.layout.Layout;
import com.kosmos.layout.dao.LayoutDAO;
import com.kosmos.layout.exception.DefaultLayoutNotFoundException;
import com.kosmos.layout.exception.LayoutNotFoundException;
import com.kosmos.layout.grid.impl.SimpleGrid;
import com.kosmos.layout.grid.managers.impl.SimpleGridManager;
import com.kosmos.layout.meta.bean.LayoutMetaBean;
import com.kosmos.layout.meta.dao.LayoutMetaDAO;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.util.MetatagUtils;

@Test @ContextConfiguration(locations = { "classpath:com/kosmos/layout/service/ServiceLayoutTest.test-context.xml" })
public class ServiceLayoutTest	extends AbstractTestNGSpringContextTests {

	@Autowired private ServiceLayout serviceLayout;

	@Test(expectedExceptions = UnsupportedOperationException.class) public void testSave() {
		final Layout layout = new SimpleGrid();
		serviceLayout.save(layout);
	}

	@Test(expectedExceptions = UnsupportedOperationException.class) public void testGetById() {
		serviceLayout.getById(0L);
	}

	@Test(expectedExceptions = UnsupportedOperationException.class) public void testDelete() {
		serviceLayout.delete(0L);
	}

	@Test public void testGetAvailableLayouts(@Mocked("getAllLayouts") final LayoutDAO mockedDao) {
		final LayoutDAO dao = new LayoutDAO();
		serviceLayout.setDao(dao);
		final Collection<Layout> layouts = new ArrayList<>();
		layouts.add(new SimpleGrid());
		layouts.add(new SimpleGrid());
		new Expectations() {{
			dao.getAllLayouts();
			result = layouts;
		}};
		final Collection<Layout> results = serviceLayout.getAvailableLayouts();
		Assert.assertNotNull(results, "La collection retournée ne doit pas être null.");
		Assert.assertTrue(results.size() == 2, "La collection retournée doit contenir 2 éléments.");
		new Expectations() {{
			dao.getAllLayouts();
			result = new DataSourceException("Plop");
		}};
		final Collection<Layout> results2 = serviceLayout.getAvailableLayouts();
		Assert.assertNull(results2, "La collection retournée doit être null.");
	}

	@Test public void testGetLayoutById(@Mocked("getById") final LayoutDAO mockedDao) throws LayoutNotFoundException {
		final LayoutDAO dao = new LayoutDAO();
		serviceLayout.setDao(dao);
		final Layout layout = new SimpleGrid();
		layout.setId(0L);
		new Expectations() {{
			dao.getById(0L);
			result = layout;
		}};
		final Layout result = serviceLayout.getLayout(0L);
		Assert.assertNotNull(result, "Le layout retourné ne doit pas être null.");
		Assert.assertTrue(result.getId() == 0L, "Le layout retourné doit avoir l'id 0L");
	}

	@Test(expectedExceptions = LayoutNotFoundException.class) public void testGetLayoutByIdFailed1(@Mocked("getById") final LayoutDAO mockedDao) throws LayoutNotFoundException {
		new MockUp<MessageHelper>() {

			@Mock public String getCoreMessage(final String key) {
				return "PLOP %d";
			}
		};
		final LayoutDAO dao = new LayoutDAO();
		serviceLayout.setDao(dao);
		new Expectations() {{
			dao.getById(anyLong);
			result = null;
		}};
		serviceLayout.getLayout(0L);
	}

	@Test(expectedExceptions = LayoutNotFoundException.class) public void testGetLayoutByIdFailed2(@Mocked("getById") final LayoutDAO mockedDao) throws LayoutNotFoundException {
		new MockUp<MessageHelper>() {

			@Mock public String getCoreMessage(final String key) {
				return "PLOP %d";
			}
		};
		final LayoutDAO dao = new LayoutDAO();
		serviceLayout.setDao(dao);
		new Expectations() {{
			dao.getById(anyLong);
			result = new DataSourceException("Plop");
		}};
		serviceLayout.getLayout(0L);
	}

	@Test public void testGetLayoutByName(@Mocked("getByName") final LayoutDAO mockedDao) throws LayoutNotFoundException {
		final LayoutDAO dao = new LayoutDAO();
		serviceLayout.setDao(dao);
		final SimpleGrid layout = new SimpleGrid();
		layout.setId(0L);
		layout.setName("LayoutTest");
		new Expectations() {{
			dao.getByName("LayoutTest");
			result = layout;
		}};
		final Layout result = serviceLayout.getLayout("LayoutTest");
		Assert.assertNotNull(result, "Le layout retourné ne doit pas être null.");
		Assert.assertTrue(result.getId() == 0L, "Le layout retourné doit avoir l'id 0L");
		Assert.assertEquals(result.getName(), "LayoutTest", "Le layout retourné doit avoir le nom \"LayoutTest\"");
		final Layout result1 = serviceLayout.getLayout(StringUtils.EMPTY);
		Assert.assertNull(result1, "Le layout retourné doit être null.");
	}

	@Test(expectedExceptions = LayoutNotFoundException.class) public void testGetLayoutByNameFailed1(@Mocked("getByName") final LayoutDAO mockedDao)
		throws LayoutNotFoundException {
		new MockUp<MessageHelper>() {

			@Mock public String getCoreMessage(final String key) {
				return "PLOP %s";
			}
		};
		final LayoutDAO dao = new LayoutDAO();
		serviceLayout.setDao(dao);
		new Expectations() {{
			dao.getByName(anyString);
			result = new DataSourceException("Plop");
		}};
		serviceLayout.getLayout("Plop");
	}

	@Test public void testGetLayout1(@Mocked("getLayoutForMeta") final ServiceLayout mockedServiceLayout) throws LayoutNotFoundException, DefaultLayoutNotFoundException {
		final Layout layout = new SimpleGrid();
		new MockUp<MetatagUtils>() {

			@Mock public MetatagBean lireMeta(final FicheUniv ficheUniv) {
				return new MetatagBean();
			}
		};
		new Expectations() {{
			serviceLayout.getLayoutForMeta(anyLong);
			result = layout;
		}};
		final Layout result = serviceLayout.getLayout((FicheUniv) null);
		Assert.assertNotNull(result, "Le layout retourné ne doit pas être null");
	}

	@Test public void testGetLayout2(@Mocked("getLayoutForMeta") final ServiceLayout mockedServiceLayout, @Mocked("getByName") final LayoutDAO mockedDao)
		throws LayoutNotFoundException, DefaultLayoutNotFoundException {
		final Layout layout = new SimpleGrid();
		final Map<String, String> defaults = new HashMap<>();
		final MetatagBean metatagBean = new MetatagBean();
		final LayoutDAO layoutDAO = new LayoutDAO();
		metatagBean.setId(0L);
		defaults.put("com.univ.objetspartages.om.PageLibre", "plop");
		serviceLayout.setDefaultLayouts(defaults);
		serviceLayout.setDao(layoutDAO);
		new MockUp<MetatagUtils>() {

			@Mock public MetatagBean lireMeta(final FicheUniv ficheUniv) {
				return metatagBean;
			}
		};
		new MockUp<ReferentielObjets>() {

			@Mock public String getClasseObjet(final String codeOuNomObjet) {
				return "com.univ.objetspartages.om.PageLibre";
			}

			@Mock public String getCodeObjet(final FicheUniv _ficheUniv) {
				return StringUtils.EMPTY;
			}
		};
		new Expectations() {{
			serviceLayout.getLayoutForMeta(0L);
			result = new LayoutNotFoundException("Plop");
			layoutDAO.getByName("plop");
			result = layout;
		}};
		final Layout result = serviceLayout.getLayout((FicheUniv) null);
		Assert.assertNotNull(result, "Le layout retourné ne doit pas être null");
		final Map<String, String> defaultLayouts = serviceLayout.getDefaultLayouts();
		Assert.assertNotNull(defaultLayouts, "L'objet retourné ne doit pas être null");
		Assert.assertTrue(defaultLayouts.size() == 1, "L'objet retourné doit contenir 1 élément");
		Assert.assertEquals(defaultLayouts.get("com.univ.objetspartages.om.PageLibre"), "plop", "L'objet retourné doit contenir 1 élément \"plop\"");
	}

	@Test(expectedExceptions = LayoutNotFoundException.class) public void testGetLayoutFailed1() throws LayoutNotFoundException, DefaultLayoutNotFoundException {
		new MockUp<MetatagUtils>() {

			@Mock public MetatagBean lireMeta(final FicheUniv ficheUniv) {
				return null;
			}
		};
		new MockUp<MessageHelper>() {

			@Mock public String getCoreMessage(final String key) {
				return "PLOP %s";
			}
		};
		final FicheUniv ficheUniv = new MockUp<FicheUniv>() {

			public String getCode() {
				return "plop";
			}
		}.getMockInstance();
		serviceLayout.getLayout(ficheUniv);
	}

	@Test(expectedExceptions = DefaultLayoutNotFoundException.class)
	public void testGetLayoutFailed2(@Mocked("getLayoutForMeta") final ServiceLayout mockedServiceLayout, @Mocked("getByName") final LayoutDAO mockedDao)
		throws LayoutNotFoundException, DefaultLayoutNotFoundException {
		final Layout layout = new SimpleGrid();
		final Map<String, String> defaults = new HashMap<>();
		final MetatagBean metatagBean = new MetatagBean();
		final LayoutDAO layoutDAO = new LayoutDAO();
		metatagBean.setId(0L);
		defaults.put("com.univ.objetspartages.om.PageLibre", "plop");
		serviceLayout.setDefaultLayouts(defaults);
		serviceLayout.setDao(layoutDAO);
		new MockUp<MetatagUtils>() {

			@Mock public MetatagBean lireMeta(final FicheUniv ficheUniv) {
				return metatagBean;
			}
		};
		new MockUp<ReferentielObjets>() {

			@Mock public String getClasseObjet(final String codeOuNomObjet) {
				return "com.univ.objetspartages.om.PageLibre";
			}

			@Mock public String getCodeObjet(final FicheUniv _ficheUniv) {
				return StringUtils.EMPTY;
			}
		};
		new MockUp<MessageHelper>() {

			@Mock public String getCoreMessage(final String key) {
				return "PLOP %s";
			}
		};
		new Expectations() {{
			serviceLayout.getLayoutForMeta(0L);
			result = new LayoutNotFoundException("Plop");
			layoutDAO.getByName("plop");
			result = null;
		}};
		serviceLayout.getLayout((FicheUniv) null);
	}

	@Test public void testGetLayoutForMeta(@Mocked("getByMetaId") final LayoutMetaDAO mockedDao, @Mocked("getById") final LayoutDAO mockedLayoutDao)
		throws LayoutNotFoundException {
		final LayoutMetaBean layoutMetaBean = new LayoutMetaBean();
		final LayoutMetaDAO layoutMetaDAO = new LayoutMetaDAO();
		final LayoutDAO layoutDAO = new LayoutDAO();
		final Layout layout = new SimpleGrid();
		layout.setId(0L);
		serviceLayout.setLayoutMetaDao(layoutMetaDAO);
		serviceLayout.setDao(layoutDAO);
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = layoutMetaBean;
			layoutDAO.getById(anyLong);
			result = layout;
		}};
		final Layout result = serviceLayout.getLayoutForMeta(0L);
		Assert.assertNotNull(result, "Le layout renvoyé ne devrait pas être null.");
		Assert.assertEquals(result.getId(), new Long(0L), "Le layout renvoyé devrait porter l'id \"0\"");
	}

	@Test(expectedExceptions = LayoutNotFoundException.class)
	public void testGetLayoutForMetaFailed1(@Mocked("getByMetaId") final LayoutMetaDAO mockedDao, @Mocked("getById") final LayoutDAO mockedLayoutDao)
		throws LayoutNotFoundException {
		final LayoutMetaBean layoutMetaBean = new LayoutMetaBean();
		final LayoutMetaDAO layoutMetaDAO = new LayoutMetaDAO();
		final LayoutDAO layoutDAO = new LayoutDAO();
		final Layout layout = new SimpleGrid();
		layout.setId(0L);
		serviceLayout.setLayoutMetaDao(layoutMetaDAO);
		serviceLayout.setDao(layoutDAO);
		new MockUp<MessageHelper>() {

			@Mock public String getCoreMessage(final String key) {
				return "PLOP %d";
			}
		};
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = layoutMetaBean;
			layoutDAO.getById(anyLong);
			result = new DataSourceException("Plop");
		}};
		serviceLayout.getLayoutForMeta(0L);
	}

	@Test(expectedExceptions = LayoutNotFoundException.class) public void testGetLayoutForMetaFailed2(@Mocked("getByMetaId") final LayoutMetaDAO mockedDao)
		throws LayoutNotFoundException {
		final LayoutMetaDAO layoutMetaDAO = new LayoutMetaDAO();
		final Layout layout = new SimpleGrid();
		layout.setId(0L);
		serviceLayout.setLayoutMetaDao(layoutMetaDAO);
		new MockUp<MessageHelper>() {

			@Mock public String getCoreMessage(final String key) {
				return "PLOP %d";
			}
		};
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = null;
		}};
		serviceLayout.getLayoutForMeta(0L);
	}

	@Test public void testAddLayoutForMeta(@Mocked({ "getByMetaId", "update", "add" }) final LayoutMetaDAO mockedDao) {
		final LayoutMetaDAO layoutMetaDAO = new LayoutMetaDAO();
		serviceLayout.setLayoutMetaDao(layoutMetaDAO);
		final SimpleGrid simpleGrid = new SimpleGrid();
		simpleGrid.setId(0L);
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = null;
		}};
		serviceLayout.addLayoutForMeta(simpleGrid, 0L);
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = new LayoutMetaBean();
		}};
		serviceLayout.addLayoutForMeta(simpleGrid, 0L);
	}

	@Test(expectedExceptions = DataSourceException.class) public void testAddLayoutForMetaFailed1(@Mocked("getByMetaId") final LayoutMetaDAO mockedDao) {
		final LayoutMetaDAO layoutMetaDAO = new LayoutMetaDAO();
		serviceLayout.setLayoutMetaDao(layoutMetaDAO);
		final SimpleGrid simpleGrid = new SimpleGrid();
		simpleGrid.setId(0L);
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = new DataSourceException("Plop");
		}};
		serviceLayout.addLayoutForMeta(simpleGrid, 0L);
	}

	@Test(expectedExceptions = DataSourceException.class) public void testAddLayoutForMetaFailed2(@Mocked({ "getByMetaId", "add" }) final LayoutMetaDAO mockedDao) {
		final LayoutMetaDAO layoutMetaDAO = new LayoutMetaDAO();
		serviceLayout.setLayoutMetaDao(layoutMetaDAO);
		final SimpleGrid simpleGrid = new SimpleGrid();
		simpleGrid.setId(0L);
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = null;
			layoutMetaDAO.add(withAny(new LayoutMetaBean()));
			result = new DataSourceException("Plop");
		}};
		serviceLayout.addLayoutForMeta(simpleGrid, 0L);
	}

	@Test(expectedExceptions = DataSourceException.class) public void testAddLayoutForMetaFailed3(@Mocked({ "getByMetaId", "update" }) final LayoutMetaDAO mockedDao) {
		final LayoutMetaDAO layoutMetaDAO = new LayoutMetaDAO();
		serviceLayout.setLayoutMetaDao(layoutMetaDAO);
		final LayoutMetaBean layoutMetaBean = new LayoutMetaBean();
		final SimpleGrid simpleGrid = new SimpleGrid();
		simpleGrid.setId(0L);
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = layoutMetaBean;
			layoutMetaDAO.update(layoutMetaBean);
			result = new DataSourceException("Plop");
		}};
		serviceLayout.addLayoutForMeta(simpleGrid, 0L);
	}

	@Test public void testDeleteLayoutForMeta(@Mocked({ "getByMetaId", "delete" }) final LayoutMetaDAO mockedDao) {
		final LayoutMetaBean layoutMetaBean = new LayoutMetaBean();
		layoutMetaBean.setId(0L);
		final LayoutMetaDAO layoutMetaDAO = new LayoutMetaDAO();
		serviceLayout.setLayoutMetaDao(layoutMetaDAO);
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = layoutMetaBean;
		}};
		serviceLayout.deleteLayoutForMeta(0L);
	}

	@Test(expectedExceptions = DataSourceException.class) public void testDeleteLayoutForMetaFailed(@Mocked({ "getByMetaId" }) final LayoutMetaDAO mockedDao) {
		final LayoutMetaDAO layoutMetaDAO = new LayoutMetaDAO();
		serviceLayout.setLayoutMetaDao(layoutMetaDAO);
		new Expectations() {{
			layoutMetaDAO.getByMetaId(anyLong);
			result = new DataSourceException("Plop");
		}};
		serviceLayout.deleteLayoutForMeta(0L);
	}

	@Test public void testGetLayoutManagers() {
		new MockUp<ApplicationContextManager>() {

			@Mock public <T> Map<String, T> getAllBeansOfType(final Invocation context, final Class<T> type) {
				final Map<String, T> managers = new LinkedHashMap<>();
				managers.put("com.kosmos.layout.grid.managers.impl.SimpleGridManager", (T) new SimpleGridManager());
				return managers;
			}
		};
		serviceLayout.refresh();
		final Collection<SimpleGridManager> simpleGridManagers = serviceLayout.getLayoutManagers(SimpleGridManager.class);
		Assert.assertNotNull(simpleGridManagers, "La collection renvoyée ne devrait pas être null");
		Assert.assertTrue(simpleGridManagers.size() == 1, "La collection renvoyée devrait contenir 1 élément");
		final DummyLayoutManager dummyLayoutManager = new DummyLayoutManager();
		final Collection<DummyLayoutManager> dummyLayoutManagers = serviceLayout.getLayoutManagers(DummyLayoutManager.class);
		Assert.assertNotNull(dummyLayoutManagers, "La collection renvoyée ne devrait pas être null");
		Assert.assertTrue(dummyLayoutManagers.isEmpty(), "La collection renvoyée devrait être vide");
	}

	@Test public void testAddLayout(@Mocked("add") final LayoutDAO mockedDao) {
		final Layout layout = new AbstractLayout() {

		};
		final LayoutDAO dao = new LayoutDAO();
		serviceLayout.setDao(dao);
		new Expectations() {{
			dao.add(layout);
			result = layout;
		}};
		serviceLayout.addLayout(layout);
	}


	@Test public void testDeleteLayoutWithNoLayoutMeta(@Mocked("getLayout") final ServiceLayout mockedService, @Mocked("getLayoutMetasByLayoutId") final LayoutMetaDAO mockedLayoutMetaDao, @Mocked("delete") final LayoutDAO mockedLayoutDao ) throws LayoutNotFoundException {
		final Layout layout = new AbstractLayout() {

		};
		layout.setId(0L);
		((AbstractLayout)layout).setCustom(true);
		final List<LayoutMetaBean> metaBeans = new ArrayList<>();
		final LayoutMetaDAO metaDao = new LayoutMetaDAO();
		final LayoutDAO layoutDao = new LayoutDAO();
		serviceLayout.setLayoutMetaDao(metaDao);
		serviceLayout.setDao(layoutDao);
		new Expectations() {{
			serviceLayout.getLayout(anyLong);
			result = layout;
			metaDao.getLayoutMetasByLayoutId(anyLong);
			result = metaBeans;
		}};
		serviceLayout.deleteLayoutWithNoLayoutMeta(0L);
		new Expectations() {{
			serviceLayout.getLayout(anyLong);
			result = layout;
			metaDao.getLayoutMetasByLayoutId(anyLong);
			result = Collections.emptyList();
		}};
		serviceLayout.deleteLayoutWithNoLayoutMeta(0L);
	}

	@Test
	@DatabaseSetup("datas.xml")
	public void testUpdateLayoutModel(@Mocked(value = "getServiceForBean",stubOutClassInitialization = true) ServiceManager mockedServiceManager, @Mocked("updateLayoutModel") final ServiceModelLayout mockedService)
		throws LayoutNotFoundException {
		Layout layout = new AbstractLayout() {

		};
		final Map<String,Object> datas = new HashMap<>();
		ContainerGrid newlayout = new ContainerGrid();
		new Expectations(){{
				ServiceManager.getServiceForBean((Class)any);
				result = null;
		}};
		serviceLayout.updateLayoutModel(layout,datas,newlayout);
		final ServiceGridLayout serviceGridLayout = new ServiceGridLayout();
		new Expectations(){{
			ServiceManager.getServiceForBean((Class)any);
			result = serviceGridLayout;
		}};
		ContainerGrid containerGrid = new ContainerGrid();
		SlotList slots = new SlotList();
		SlotList slots2 = new SlotList();
		for(int i=0; i<6 ; ++i) {
			for(int j=0; j<4; ++j) {
				ContainerSlot slot = new ContainerSlot();
				slot.setRow(i);
				slot.setColumn(j);
				UUID key = UUID.randomUUID();
				slot.setKey(key);
				datas.put("slot-row-"+ key,String.valueOf(j));
				datas.put("slot-column-"+ key,String.valueOf(i));
				slots.add(slot);
				slots2.add(slot);
			}
		}
		containerGrid.setSlots(slots);
		newlayout.setSlots(slots2);
		serviceLayout.updateLayoutModel(containerGrid,datas,newlayout);

	}

}
