package com.kosmos.layout.service;

import java.util.Map;

import com.kosmos.layout.grid.impl.SimpleGrid;
import com.kosmos.layout.manager.LayoutManager;

public class DummyLayoutManager implements LayoutManager<SimpleGrid>{

    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public Map<String, SimpleGrid> getLayouts() {
        return null;
    }
}
