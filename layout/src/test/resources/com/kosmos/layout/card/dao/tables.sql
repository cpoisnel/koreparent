CREATE TABLE CARD (
  ID_CARD    BIGINT(20)  NOT NULL AUTO_INCREMENT,
  KEY_CARD   VARCHAR(40) NOT NULL,
  DATAS_CARD TEXT,
  PRIMARY KEY (ID_CARD)
);