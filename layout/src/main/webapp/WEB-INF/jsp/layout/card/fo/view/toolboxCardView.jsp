<%@ page import="org.apache.commons.lang3.StringUtils" %>
<jsp:useBean id="viewModel" class="com.kosmos.layout.card.view.model.ToolboxCardViewModel" scope="request" />
<div class="card toolboxCard <%= StringUtils.defaultString(viewModel.getStyle()) %>"><%
    if (StringUtils.isNotBlank(viewModel.getTitle())) {
        %><h2 class="toolboxCard__title"><%= viewModel.getTitle() %></h2><%
    }
    if (StringUtils.isNotBlank(viewModel.getContent())) {
        %><div class="toolboxCard__content"><%= viewModel.getContent() %></div><%
    }
%></div>