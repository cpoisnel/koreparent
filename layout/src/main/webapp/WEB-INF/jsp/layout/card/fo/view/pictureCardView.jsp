<%@ page import="org.apache.commons.lang3.StringUtils" %>
<jsp:useBean id="viewModel" class="com.kosmos.layout.card.view.model.PictureCardViewModel" scope="request" />
<<%= viewModel.isLink() ? "a" : "div" %>
    class="card pictureCard <%= StringUtils.defaultString(viewModel.getStyle()) %>"
    <%= viewModel.isLink() ? String.format("href=\"%s\"", viewModel.getUrl()) : StringUtils.EMPTY %>
    <%= viewModel.getDynamicStyle() %>><%
    if (StringUtils.isNotBlank(viewModel.getTitle())) {
        %><h2 class="pictureCard__title"><%= viewModel.getTitle() %></h2><%
    }
    if (StringUtils.isNotBlank(viewModel.getImgSrc())) {
        %><img src="<%= viewModel.getImgSrc() %>" alt="<%= viewModel.getTitle() %>"/><%
    }
%></<%= viewModel.isLink() ? "a" : "div" %>>