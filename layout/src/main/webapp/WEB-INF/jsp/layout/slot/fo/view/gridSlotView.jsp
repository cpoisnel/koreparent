<%@ taglib prefix="layout" uri="http://kportal.kosmos.fr/tags/layout" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="com.kosmos.layout.slot.impl.GridSlot" %>
<%@ page import="com.kosmos.layout.slot.util.GridSlotUtil" %>
<%@ page import="com.kosmos.layout.slot.util.SlotUtil" %>
<jsp:useBean id="idExtension" class="java.lang.String" scope="request" />
<jsp:useBean id="viewModel" class="com.kosmos.layout.slot.view.model.SlotViewModel" scope="request" />
<%
    final GridSlot slot = (GridSlot) viewModel.getSlot();
%>
<div class="<%= String.format("%s %s", SlotUtil.getClassName(slot), GridSlotUtil.getClassName(slot)) %> js-layout__slot">
    <layout:card viewModel="<%= viewModel.getCardViewModel() %>" front="true" extension="<%= idExtension %>"/>
</div>