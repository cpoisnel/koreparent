<%@ page import="com.kosmos.layout.slot.util.SlotUtil" %>
<%@ taglib prefix="layout" uri="http://kportal.kosmos.fr/tags/layout" %>
<jsp:useBean id="idExtension" class="java.lang.String" scope="request" />
<jsp:useBean id="viewModel" class="com.kosmos.layout.slot.view.model.SlotViewModel" scope="request" />
<div class="<%= SlotUtil.getClassName(viewModel.getSlot()) %> js-layout__slot">
    <layout:card viewModel="<%= viewModel.getCardViewModel() %>"/>
</div>