<%@ taglib prefix="components" uri="http://kportal.kosmos.fr/tags/components" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request"/>
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page"/>
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page"/>
<p>
    <label for="title-toolboxcardbean" class="colonne"><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.TITLE") %> (*)</label>
    <input type="text" name="title" id="title-toolboxcardbean">
</p>
<p>
    <label for="style-toolboxcardbean" class="colonne"><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.STYLE") %></label>
    <select id="style-toolboxcardbean" name="style">
        <option value=""><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.STYLE_NONE") %></option>
        <option value="style1"><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.STYLE1") %></option>
        <option value="style2"><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.STYLE2") %></option>
        <option value="style3"><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.STYLE3") %></option>
    </select>
</p>
<components:toolbox fieldName="dynamic" label="BO.CARD.EDIT_FRAGMENT.DYNAMIC" min="0" max="512" editOption='<%= FormateurJSP.SAISIE_FACULTATIF %>'/>
