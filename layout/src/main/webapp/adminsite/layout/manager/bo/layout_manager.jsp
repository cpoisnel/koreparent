<%@ page import="java.util.Map" %>
<%@ page import="org.apache.commons.collections.CollectionUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kosmos.layout.Layout" %>
<%@ page import="com.kosmos.layout.card.util.CardDescription" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.extension.ExtensionHelper" %>
<%@ taglib prefix="layout" uri="http://kportal.kosmos.fr/tags/layout" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="idExtension" class="java.lang.String" scope="request" />
<jsp:useBean id="viewModel" class="com.kosmos.layout.view.model.LayoutManagerViewModel" scope="request" />
<%
if (CollectionUtils.isNotEmpty(viewModel.getAvailableLayouts()) && viewModel.getAvailableLayouts().size() > 1) {
    %><label for="ID_LAYOUT" class="colonne"><%= MessageHelper.getCoreMessage("BO.LAYOUT") %></label>
        <select id="ID_LAYOUT" name="ID_LAYOUT" data-confirm="<%= MessageHelper.getCoreMessage("LAYOUT_MANAGER.CONFIRM.SWITCH") %>"><%
            for (Layout currentLayout : viewModel.getAvailableLayouts()) {
            %><option value="<%= currentLayout.getId() %>" <%= currentLayout.getId().equals(viewModel.getLayoutViewModel().getLayout().getId()) ? "selected" : "" %>><%= MessageHelper.getMessage(idExtension, currentLayout.getDescription()) %></option><%
            }
        %></select>
        <input type="hidden" id="CUSTOM_LAYOUT" name="CUSTOM_LAYOUT" value="">
<%
} else {
    %><input type="hidden" id="ID_LAYOUT" name="ID_LAYOUT" value="<%= viewModel.getLayoutViewModel().getLayout().getId() %>">
      <input type="hidden" id="CUSTOM_LAYOUT" name="CUSTOM_LAYOUT" value=""><%
}
%><div class="fieldset neutre">
    <div class="layout-manager js-layout-manager">
        <input type="hidden" name="FILES_START_INDEX" value="<c:out value="<%=request.getAttribute(\"filesStartIndex\")%>"/>">
        <input type="hidden" value="<%= viewModel.getCardsPool() %>" class="js-layout-manager__cards-pool"/>
        <layout:layout viewModel="<%= viewModel.getLayoutViewModel() %>" extension="<%= idExtension %>"/>
        <div class="clearfix"></div>
        <div class="layout-manager__edit-wrapper layout-manager__edit-wrapper--inactive js-layout-manager__edit-wrapper">
            <span class="label colonne"><%= MessageHelper.getCoreMessage("BO.CARD_SELECTOR.EDIT_FRAGMENT.LABEL") %></span>

            <div class="dropdown cards-selector js-cards-selector">
                <button type="button" class="dropdown-toggle button js-cards-selector__button"
                        data-toggle="dropdown"></button>
                <div class="dropdown-menu">
                    <ul class="cards-selector__list"><!--<%
                        for(CardDescription currentDescription : viewModel.getCardsDescriptions()) {
                        //Un peu cracra mais permet de savoir si une carte est dans le scope de l'extension et aussi n'affiche pas des <li> vides
                        if(StringUtils.isNoneBlank(MessageHelper.getMessage(idExtension, currentDescription.getName()),MessageHelper.getMessage(idExtension, currentDescription.getDescription())))  {
                            %>-->
                            <li class="cards-selector__item js-cards-selector__item"
                                data-class="<%= currentDescription.getType().getName() %>">
                                <button type="button" class="js-cards-selector__item-button"><%
                                    if(StringUtils.isNotBlank(currentDescription.getIcon())) {
                                    %><img src="<%= currentDescription.getIcon() %>" alt="<%= MessageHelper.getMessage(idExtension, currentDescription.getName()) %>"/><%
                                    }
                                    %><div class="cards-selector__item-infos">
                                        <span class="name"><%= MessageHelper.getMessage(idExtension, currentDescription.getName()) %></span>
                                        <p class="description"><%= MessageHelper.getMessage(idExtension, currentDescription.getDescription()) %></p>
                                    </div>
                                </button>
                            </li>
                            <!--<%
                            }
                        }
                    %>
                    --></ul>
                </div>
            </div>
            <div class="card-forms js-card-forms"><%
                for (Map.Entry<String, String> action : viewModel.getEditFragments().entrySet()) {
                    if(StringUtils.isNotBlank(ExtensionHelper.getTemplateExtension(idExtension, action.getValue(), false))){
                        %><div class="card-forms__edit-panel card-forms__edit-panel--inactive js-card-forms__edit-panel"
                        data-card-class="<%= action.getKey() %>">
                        <jsp:include page="<%= ExtensionHelper.getTemplateExtension(idExtension, action.getValue(), false)%>"/>
                        </div><%
                    }
                }
            %></div>
            <div class="card-viewBo js-card-viewBo"><%
                for (Map.Entry<String, String> view : viewModel.getCardBoViewPool().entrySet()) {
                    if(StringUtils.isNotBlank(ExtensionHelper.getTemplateExtension(idExtension, view.getValue(), false))){
                        %><div class="card-viewBo__view js-card-viewBo__view" data-card-class="<%= view.getKey() %>">
                            <jsp:include page="<%= ExtensionHelper.getTemplateExtension(idExtension, view.getValue(), false)%>"/>
                        </div><%
                    }
                }
            %></div>
        </div>
    </div>
    <!-- .layout-manager -->
</div>
</form>
