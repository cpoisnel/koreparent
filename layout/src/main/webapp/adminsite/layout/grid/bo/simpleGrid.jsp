<%@ page import="com.kosmos.layout.grid.GridLayout" %>
<%@ page import="com.kosmos.layout.slot.util.GridSlotUtil" %>
<%@ page import="com.kosmos.layout.slot.view.model.SlotViewModel" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="layout" uri="http://kportal.kosmos.fr/tags/layout" %>
<jsp:useBean id="idExtension" class="java.lang.String" scope="request" />
<jsp:useBean id="viewModel" class="com.kosmos.layout.view.model.LayoutViewModel" scope="request" />
<%
    final GridLayout grid = (GridLayout) viewModel.getLayout();
%>
<div class="layout layout__simple-grid layout__simple-grid--<%= grid.getName() %> js-layout"><%
    for (int row = 0; row < grid.getRows(); row++) {
        for (int column = 0; column < grid.getColumns(); column++) {
            final String key = GridSlotUtil.getSlotKey(grid, row, column);
            final SlotViewModel slotViewModel = viewModel.getSlotViewModels().get(key);
            if (slotViewModel != null) {
                %><layout:slot viewModel="<%= slotViewModel %>" extension="<%= idExtension %>"/><%
            }
        }
    }
%></div>
