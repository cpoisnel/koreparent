package com.kosmos.layout.impl;

import java.util.ArrayList;
import java.util.Collection;

import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.slot.util.SlotList;

public abstract class AbstractLayout implements Layout {

    private static final long serialVersionUID = -5678861752352058316L;

    protected boolean context = true;

    protected boolean custom = false;

    protected String viewBo;

    protected String view;

    private SlotList slots;

    private Collection<Class<? extends CardBean>> allowedCardTypes;

    private Long id;

    private String name;

    private String description;

    private Collection<Class<?>> allowedCarrierTypes = new ArrayList<>();

    public String getViewBo() {
        return this.viewBo;
    }

    public void setViewBo(String rendererBo) {
        this.viewBo = rendererBo;
    }

    public String getView() {
        return this.view;
    }

    public void setView(String rendererFo) {
        this.view = rendererFo;
    }

    public void setContext(boolean context) {
        this.context = context;
    }

    public boolean hasContext() {
        return this.context;
    }

    @Override
    public SlotList getSlots() {
        return this.slots;
    }

    public void setSlots(SlotList slots) {
        this.slots = slots;
    }

    @Override
    public Collection<Class<? extends CardBean>> getAllowedCardTypes() {
        return allowedCardTypes;
    }

    public void setAllowedCardTypes(Collection<Class<? extends CardBean>> allowedCardTypes) {
        this.allowedCardTypes = allowedCardTypes;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCustom() {
        return custom;
    }

    public void setCustom(boolean custom) {
        this.custom = custom;
    }

    @Override public Collection<Class<?>> getAllowedCarrierTypes() {
        return allowedCarrierTypes;
    }

    public void setAllowedCarrierTypes(Collection<Class<?>> allowedCarrierTypes) {
        this.allowedCarrierTypes = allowedCarrierTypes;
    }
}
