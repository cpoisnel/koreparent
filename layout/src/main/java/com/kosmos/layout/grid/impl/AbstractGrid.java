package com.kosmos.layout.grid.impl;

import com.fasterxml.jackson.annotation.JsonView;
import com.kosmos.layout.Layout;
import com.kosmos.layout.grid.GridLayout;
import com.kosmos.layout.impl.AbstractLayout;
import com.kosmos.layout.slot.Slot;
import com.kosmos.layout.slot.impl.GridSlot;
import com.univ.utils.json.Views;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.UUID;

public class AbstractGrid extends AbstractLayout implements GridLayout {

    private static final long serialVersionUID = -2461165284291786678L;

    @JsonView({Views.DaoView.class})
    private int rows;

    @JsonView({Views.DaoView.class})
    private int columns;

    public int getRows() {
        return this.rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return this.columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }


}
