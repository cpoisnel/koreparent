package com.kosmos.layout.grid;

import com.kosmos.layout.Layout;

public interface GridLayout extends Layout {

    int getRows();

    int getColumns();
}
