package com.kosmos.layout.grid.service;

import com.kosmos.layout.Layout;
import com.kosmos.layout.grid.GridLayout;
import com.kosmos.layout.grid.impl.AbstractGrid;
import com.kosmos.layout.grid.impl.ContainerGrid;
import com.kosmos.layout.service.ServiceLayout;
import com.kosmos.layout.service.ServiceModelLayout;
import com.kosmos.layout.slot.Slot;
import com.kosmos.layout.slot.impl.GridSlot;
import com.kosmos.service.ServiceBean;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.UUID;

/**
 * Created by alexandre.baillif on 30/11/16.
 */
public class ServiceGridLayout extends ServiceModelLayout implements ServiceBean<ContainerGrid> {

	public void updateLayoutModel(Layout layout, Map<String, Object> datas, Layout newLayout) {
		if(layout instanceof AbstractGrid && newLayout instanceof  AbstractGrid){
			if(newLayout.getSlots() != null) {
				newLayout.getSlots().clear();
				int maxNbOfRow = 0;
				int maxNbOfColumn = 0;
				for (Slot slot : layout.getSlots()) {
					GridSlot thisSlot = (GridSlot) slot;
					final String slotKey = slot.getKey().toString();
					final String newRow = (String) datas.get("slot-row-" + slotKey);
					final String newColumn = (String) datas.get("slot-column-" + slotKey);
					if (StringUtils.isNoneBlank(newRow, newColumn)) {
						Integer row = Integer.valueOf(newRow);
						Integer column = Integer.valueOf(newColumn);
						maxNbOfRow = Math.max(row + 1, maxNbOfRow); //Cause row index starts at 0
						maxNbOfColumn = Math.max(column + 1, maxNbOfColumn); //Cause column index starts at 0
						thisSlot.setRow(row);
						thisSlot.setColumn(column);
						newLayout.getSlots().add(thisSlot);
					}
				}
				((AbstractGrid) newLayout).setRows(maxNbOfRow);
				((AbstractGrid) newLayout).setColumns(maxNbOfColumn);
				String newName = "layout-" + UUID.randomUUID();
				((AbstractGrid) newLayout).setName(newName);
				((AbstractGrid) newLayout).setCustom(true);
			}
		}

	}

	@Override public void save(ContainerGrid bean) {
		throw new NotImplementedException("Cette méthode n'est pas implémentée pour ce service. Merci d'utiliser ServiceLayout");
	}

	@Override public void delete(Long id) {
		throw new NotImplementedException("Cette méthode n'est pas implémentée pour ce service. Merci d'utiliser ServiceLayout");

	}

	@Override public ContainerGrid getById(Long id)	{
		throw new NotImplementedException("Cette méthode n'est pas implémentée pour ce service. Merci d'utiliser ServiceLayout");
	}
}
