package com.kosmos.layout.grid.managers;

import java.util.Map;

import com.kosmos.layout.grid.GridLayout;
import com.kosmos.layout.manager.LayoutManager;

public interface GridManager<T extends GridLayout> extends LayoutManager<T> {

    Map<String, T> getAvailableGrids();

    T getGridByName(String name);
}
