package com.kosmos.layout.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jsbsoft.jtf.datasource.exceptions.*;
import com.jsbsoft.jtf.datasource.utils.DaoUtils;
import org.apache.commons.collections.SetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.kosmos.layout.Layout;
import com.kosmos.layout.impl.AbstractLayout;
import com.kosmos.layout.slot.util.SlotList;
import com.kosmos.layout.utils.LayoutJacksonMapper;
import com.univ.utils.json.Views;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

/**
 * Created on 10/11/2014.
 */
public class LayoutDAO extends AbstractCommonDAO<Layout> {

    private static final Logger LOG = LoggerFactory.getLogger(LayoutDAO.class);

    public LayoutDAO() {
        this.tableName = "LAYOUT";
        this.rowMapper = new RowMapper<Layout>() {

            @Override
            public Layout mapRow(final ResultSet resultSet, final int numRow) throws SQLException {
                AbstractLayout layout = null;
                try {
                    layout = LayoutJacksonMapper.getMapper().readValue(resultSet.getString("DATAS_LAYOUT"), AbstractLayout.class);
                    layout.setSlots(LayoutJacksonMapper.getMapper().readValue(resultSet.getString("SLOTS_LAYOUT"), SlotList.class));
                    layout.setId(resultSet.getLong("ID_LAYOUT"));
                    layout.setName(resultSet.getString("NAME_LAYOUT"));
                    layout.setDescription(resultSet.getString("DESCRIPTION_LAYOUT"));
                    layout.setContext(resultSet.getBoolean("HAS_CONTEXT_LAYOUT"));
                    layout.setCustom(resultSet.getBoolean("CUSTOM"));
                } catch (IOException e) {
                    LOG.error("An error occurred trying to map layout bean", e);
                }
                return layout;
            }
        };
    }

    @Override
    protected SqlParameterSource getParameters(final Layout layout) {
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", layout.getId());
        parameterSource.addValue("nameLayout", layout.getName());
        parameterSource.addValue("hasContextLayout", layout.hasContext());
        parameterSource.addValue("descriptionLayout", layout.getDescription());
        parameterSource.addValue("custom",layout.isCustom());
        try {
            parameterSource.addValue("slotsLayout", LayoutJacksonMapper.getMapper().writeValueAsString(layout.getSlots()));
            parameterSource.addValue("datasLayout", LayoutJacksonMapper.getMapper().writerWithView(Views.DaoView.class).writeValueAsString(layout));
        } catch (JsonProcessingException e) {
            throw new ParametersDataSourceException(String.format("Une erreur est survenue lors de la génération des paramètres pour l'objet Layout portant l'id %d", layout.getId()), e);
        }
        return parameterSource;
    }

    public Layout getByName(String name) throws DataSourceException {
        Layout result = null;
        try {
            SqlParameterSource params = new MapSqlParameterSource("name", name);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM LAYOUT T1 WHERE T1.NAME_LAYOUT= :name", params, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for name  %s on table %s", name, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info("incorrect resultset size for name " + name);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for LAYOUT row with name %s", name), dae);
        }
        return result;
    }

    /**
     * La condition "WHERE CUSTOM = '0'" permet actuellement de ne pas remonter les layouts customisés via l'IHM.
     * @return
     * @throws DataSourceException
     * @deprecated  Cette méthode ne filtre pas les layouts par fiches, il préférable d'utiliser {@link #getAllLayouts(Class)}
     *
     */
    @Deprecated
    public List<Layout> getAllLayouts() throws DataSourceException {
        try {
            return namedParameterJdbcTemplate.query("SELECT * FROM LAYOUT WHERE CUSTOM = '0' ", rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException("An error occured requesting for all LAYOUT row in the database", dae);
        }
    }


    /**
     * Cette méthode récupère uniquement les layouts autorisés pour un élément donné.
     * La condition "WHERE CUSTOM = '0'" permet actuellement de ne pas remonter les layouts customisés via l'IHM.
     * @return
     * @throws DataSourceException
     */
    public List<Layout> getAllLayouts(Class<?> clazz) throws DataSourceException {
        try {
            SqlParameterSource params = new MapSqlParameterSource("clazz", clazz.getCanonicalName());
            return namedParameterJdbcTemplate.query("SELECT L.* FROM LAYOUT L LEFT JOIN LAYOUT_CARRIER LC ON L.ID_LAYOUT = LC.ID_LAYOUT WHERE L.CUSTOM = '0' AND ( LC.CLASS = :clazz OR LC.ID_LAYOUT_CARRIER IS NULL)", params, rowMapper);

        } catch (DataAccessException dae) {
            throw new DataSourceException("An error occured requesting for all LAYOUT row in the database", dae);
        }
    }

    @Override public void delete(Long id) throws DeleteFromDataSourceException {
        super.delete(id);
        try {
            SqlParameterSource paramDelete = new MapSqlParameterSource("idLayout", id);
            namedParameterJdbcTemplate.update("DELETE FROM LAYOUT_CARRIER WHERE ID_LAYOUT = :idLayout",paramDelete);
        } catch (DataAccessException dae) {
            throw new DeleteFromDataSourceException(String.format("Unable to delete %s with id %s", tableName, id), dae);
        }

    }

    @Override
    public Layout add(Layout bean) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            namedParameterJdbcTemplate.update(DaoUtils.getAddQuery(tableName, getColumns(), columnNamingStrategy), getParameters(bean), keyHolder);
            bean.setId(keyHolder.getKey().longValue());
            for(Class<?> clazz : bean.getAllowedCarrierTypes()) {
                SqlParameterSource params = new MapSqlParameterSource("idLayout", bean.getId()).addValue("clazz",clazz.getCanonicalName());
                namedParameterJdbcTemplate.update("INSERT INTO LAYOUT_CARRIER(ID_LAYOUT,CLASS) VALUES (:idLayout,:clazz)",params);
            }
        } catch (DataAccessException dae) {
            throw new AddToDataSourceException(String.format("Unable to add [%s] to table \"%s\"", bean.toString(), tableName), dae);
        }
        return bean;
    }


    @Override
    public Layout update(Layout bean) throws UpdateToDataSourceException {
        try {
            namedParameterJdbcTemplate.update(DaoUtils.getUpdateQuery(tableName, getColumns(), columnNamingStrategy), getParameters(bean));
            SqlParameterSource paramDelete = new MapSqlParameterSource("idLayout", bean.getId());
            namedParameterJdbcTemplate.update("DELETE FROM LAYOUT_CARRIER WHERE ID_LAYOUT = :idLayout",paramDelete);
            for(Class<?> clazz : bean.getAllowedCarrierTypes()) {
                SqlParameterSource params = new MapSqlParameterSource("idLayout", bean.getId()).addValue("clazz",clazz.getCanonicalName());
                namedParameterJdbcTemplate.update("INSERT INTO LAYOUT_CARRIER(ID_LAYOUT,CLASS) VALUES (:idLayout,:clazz)",params);
            }
        } catch (DataAccessException dae) {
            throw new UpdateToDataSourceException(String.format("Unable to update %s with id %s", tableName, bean.getId()), dae);
        }
        return bean;
    }

}
