package com.kosmos.layout.tag;

import javax.servlet.jsp.JspException;

import com.kosmos.layout.plugin.LayoutFichePluginHelper;
import com.kosmos.layout.view.model.LayoutViewModel;

public class LayoutTag extends AbstractLayoutTag<LayoutViewModel> {

    public static final String LAYOUT_VIEW_MODEL = "layoutViewModel";

    private static final long serialVersionUID = 1533768700032345673L;

    public LayoutTag() {
        retrieveKey = LayoutFichePluginHelper.LAYOUT_VIEW_MODEL;
    }

    @Override
    public int doStartTag() throws JspException {
        super.doStartTag();
        includeJSP(this.front ? viewModel.getLayout().getView() : viewModel.getLayout().getViewBo());
        return 0;
    }
}
