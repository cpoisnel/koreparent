package com.kosmos.layout.view.model;

import java.io.Serializable;
import java.util.Map;

import com.kosmos.layout.Layout;
import com.kosmos.layout.slot.view.model.SlotViewModel;

/**
 * Created on 10/12/14.
 */
public class LayoutViewModel implements Serializable {

    private static final long serialVersionUID = 204779366469097674L;

    private Layout layout;

    private Map<String, SlotViewModel> slotViewModels;

    public Layout getLayout() {
        return layout;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    public Map<String, SlotViewModel> getSlotViewModels() {
        return slotViewModels;
    }

    public void setSlotViewModels(Map<String, SlotViewModel> slotViewModels) {
        this.slotViewModels = slotViewModels;
    }
}
