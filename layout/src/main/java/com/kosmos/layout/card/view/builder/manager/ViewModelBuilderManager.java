package com.kosmos.layout.card.view.builder.manager;

import java.util.HashMap;
import java.util.Map;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.view.builder.ViewModelBuilder;
import com.kosmos.layout.card.view.builder.impl.DefaultCardViewModelBuilder;
import com.kportal.extension.module.AbstractBeanManager;

/**
 * Created on 14/05/2015.
 */
public class ViewModelBuilderManager extends AbstractBeanManager{

    public static final String ID_BEAN = "viewModelBuilderManager";

    private final Map<Class<? extends CardBean>, ViewModelBuilder> builders;

    public ViewModelBuilderManager(){
        builders = new HashMap<>();
    }

    @Override
    public void refresh() {
        builders.clear();
        final Map<String, ViewModelBuilder> foundBuilders = ApplicationContextManager.getAllBeansOfType(ViewModelBuilder.class);
        for(ViewModelBuilder currentBuilder : foundBuilders.values()) {
            builders.put(currentBuilder.appliesTo(), currentBuilder);
        }
    }

    @SuppressWarnings("unchecked")
    public <C extends CardBean> ViewModelBuilder<?, ?> getBuilderForCard(C card) {
        if(card != null) {
            if(builders.containsKey(card.getClass())) {
                return (ViewModelBuilder<C, ?>) builders.get(card.getClass());
            }
        }
        return new DefaultCardViewModelBuilder();
    }
}
