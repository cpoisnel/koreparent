package com.kosmos.layout.card.tag;

import javax.servlet.jsp.JspException;

import com.kosmos.layout.card.view.model.CardViewModel;
import com.kosmos.layout.plugin.LayoutFichePluginHelper;
import com.kosmos.layout.tag.AbstractLayoutTag;

public class CardTag extends AbstractLayoutTag<CardViewModel> {

    private static final long serialVersionUID = 1L;

    public CardTag() {
        retrieveKey = LayoutFichePluginHelper.CARD_VIEW_MODEL;
    }

    @Override
    public int doStartTag() throws JspException {
        super.doStartTag();
        if (viewModel != null && viewModel.getCardBean() != null) {
            includeJSP(front ? viewModel.getCardBean().getView() : viewModel.getCardBean().getViewBo());
        }
        return 0;
    }
}
