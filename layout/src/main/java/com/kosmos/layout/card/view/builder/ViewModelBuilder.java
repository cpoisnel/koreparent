package com.kosmos.layout.card.view.builder;

import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.view.model.CardViewModel;
import com.kosmos.layout.exception.ViewModelBuilderException;
import com.kosmos.layout.slot.Slot;

/**
 * Created on 14/05/2015.
 */
public interface ViewModelBuilder<C extends CardBean, V extends CardViewModel> {

    Class<C> appliesTo();

    V getViewModel(C card, Slot slot, Layout layout) throws ViewModelBuilderException;

}
