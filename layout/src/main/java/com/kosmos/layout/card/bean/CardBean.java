package com.kosmos.layout.card.bean;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.univ.objetspartages.bean.AbstractPersistenceBean;
import com.univ.utils.json.Views;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class CardBean extends AbstractPersistenceBean {

    @JsonIgnore
    private static final long serialVersionUID = -7970587067964392860L;

    @JsonView({Views.BackOfficeView.class})
    protected UUID key;

    @JsonIgnore
    protected String view = "/WEB-INF/jsp/layout/card/fo/view/cardView.jsp";

    @JsonIgnore
    protected String viewBo;

    @JsonIgnore
    protected String editFragment;

    public UUID getKey() {
        return key;
    }

    public void setKey(UUID key) {
        this.key = key;
    }

    public String getView() {
        return this.view;
    }

    public void setView(String tileView) {
        this.view = tileView;
    }

    public String getEditFragment() {
        return this.editFragment;
    }

    public String getViewBo() {
        return this.viewBo;
    }
}
