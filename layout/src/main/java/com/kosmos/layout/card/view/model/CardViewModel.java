package com.kosmos.layout.card.view.model;

import java.io.Serializable;

import com.kosmos.layout.card.bean.CardBean;

/**
 * Created on 30/12/14.
 */
public class CardViewModel<C extends CardBean> implements Serializable {

    private static final long serialVersionUID = 4661438968091270233L;

    private C cardBean;

    private String jsonCard;

    public C getCardBean() {
        return cardBean;
    }

    public void setCardBean(final C cardBean) {
        this.cardBean = cardBean;
    }

    public String getJsonCard() {
        return jsonCard;
    }

    public void setJsonCard(final String jsonCard) {
        this.jsonCard = jsonCard;
    }
}
