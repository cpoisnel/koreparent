package com.kosmos.layout.card.util;

import java.util.ArrayList;

import com.kosmos.layout.card.bean.CardBean;

/**
 * Created by Fabien on 10/11/2014.
 */
public class CardList extends ArrayList<CardBean> {

    private static final long serialVersionUID = 4121175809013734842L;
}
