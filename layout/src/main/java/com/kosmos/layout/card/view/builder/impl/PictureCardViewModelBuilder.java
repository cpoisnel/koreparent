package com.kosmos.layout.card.view.builder.impl;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.components.media.utils.ComponentMediaFileUtil;
import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.PictureCardBean;
import com.kosmos.layout.card.util.CardUtil;
import com.kosmos.layout.card.util.SimpleCardUtil;
import com.kosmos.layout.card.view.model.PictureCardViewModel;
import com.kosmos.layout.slot.Slot;

/**
 * Created on 14/05/2015.
 */
public class PictureCardViewModelBuilder extends AbstractViewModelBuilder<PictureCardBean, PictureCardViewModel>{

    @Override
    protected PictureCardViewModel buildViewModel(PictureCardViewModel viewModel, PictureCardBean card, Slot slot, Layout layout) {
        viewModel.setUrl(CardUtil.getLink(card.getLink()));
        viewModel.setStyle(card.getStyle());
        viewModel.setDynamicStyle(SimpleCardUtil.computeDynamicStyle(card));
        viewModel.setLink(StringUtils.isNotBlank(viewModel.getUrl()));
        viewModel.setTitle(card.getTitle());
        viewModel.setImgSrc(ComponentMediaFileUtil.getDisplayUrl(card.getPicture()));
        return viewModel;
    }
}
