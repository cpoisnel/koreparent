package com.kosmos.layout.card.view.builder.impl;

import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.view.model.DefaultCardViewModel;
import com.kosmos.layout.slot.Slot;

/**
 * Created on 14/05/2015.
 */
public class DefaultCardViewModelBuilder extends AbstractViewModelBuilder<CardBean, DefaultCardViewModel>{

    @Override
    protected DefaultCardViewModel buildViewModel(DefaultCardViewModel viewModel, CardBean card, Slot slot, Layout layout) {
        return viewModel;
    }
}
