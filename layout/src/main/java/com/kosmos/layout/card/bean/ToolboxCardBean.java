package com.kosmos.layout.card.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.kosmos.layout.annotations.ContentTypes;
import com.kosmos.layout.annotations.MediaContent;
import com.univ.utils.json.Views;

public class ToolboxCardBean extends CardBean {

    @JsonIgnore
    private static final long serialVersionUID = -5919658238576844574L;

    @JsonView({Views.DaoView.class})
    protected String title;

    @JsonView({Views.DaoView.class})
    protected String style;

    @JsonView({Views.DaoView.class})
    @MediaContent(type = ContentTypes.TOOLBOX)
    private String dynamic;

    public ToolboxCardBean() {
        this.view = "/WEB-INF/jsp/layout/card/fo/view/toolboxCardView.jsp";
        this.viewBo = "/adminsite/layout/card/bo/view/toolboxCardView.jsp";
        this.editFragment = "/adminsite/layout/card/bo/edit/toolboxCardEdit.jsp";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getDynamic() {
        return this.dynamic;
    }

    public void setDynamic(String dynamic) {
        this.dynamic = dynamic;
    }
}
