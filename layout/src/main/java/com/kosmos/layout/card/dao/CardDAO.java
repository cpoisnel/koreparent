package com.kosmos.layout.card.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.ParametersDataSourceException;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.utils.LayoutJacksonMapper;
import com.univ.utils.json.Views;

public class CardDAO extends AbstractCommonDAO<CardBean> {

    private static final Logger LOG = LoggerFactory.getLogger(CardDAO.class);

    public CardDAO() {
        this.tableName = "CARD";
        rowMapper = new RowMapper<CardBean>() {

            @Override
            public CardBean mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                CardBean cardBean = null;
                try {
                    cardBean = LayoutJacksonMapper.getMapper().readValue(rs.getString("DATAS_CARD"), CardBean.class);
                    cardBean.setId(rs.getLong("ID_CARD"));
                    cardBean.setKey(UUID.fromString(rs.getString("KEY_CARD")));
                } catch (IOException | IllegalArgumentException e) {
                    LOG.error("An error occured trying to map card bean", e);
                }
                return cardBean;
            }
        };
    }

    @Override
    protected SqlParameterSource getParameters(final CardBean bean) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        try {
            final String datas = LayoutJacksonMapper.getMapper().writerWithView(Views.DaoView.class).writeValueAsString(bean);
            parameterSource.addValue("datasCard", datas);
        } catch (JsonProcessingException e) {
            throw new ParametersDataSourceException(String.format("Une erreur est survenue lors de la génération des paramètres pour l'objet CardBean portant l'id %d", bean.getId()), e);
        }
        parameterSource.addValue("keyCard", bean.getKey().toString());
        parameterSource.addValue("id", bean.getId());
        return parameterSource;
    }
}
