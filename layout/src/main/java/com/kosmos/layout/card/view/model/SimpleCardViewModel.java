package com.kosmos.layout.card.view.model;

import com.kosmos.layout.card.bean.SimpleCardBean;

/**
 * Created on 14/05/2015.
 */
public class SimpleCardViewModel extends CardViewModel<SimpleCardBean> {

    private static final long serialVersionUID = 2606751002249313470L;

    private String url;

    private String title;

    private String dynamicStyle;

    private boolean link;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDynamicStyle() {
        return dynamicStyle;
    }

    public void setDynamicStyle(final String dynamicStyle) {
        this.dynamicStyle = dynamicStyle;
    }

    public boolean isLink() {
        return link;
    }

    public void setLink(boolean link) {
        this.link = link;
    }
}
