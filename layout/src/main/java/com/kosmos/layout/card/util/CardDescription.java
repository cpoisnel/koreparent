package com.kosmos.layout.card.util;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.kosmos.layout.card.bean.CardBean;

/**
 * Created on 09/12/14.
 */
public class CardDescription implements Comparable<CardDescription>, Serializable {

    private static final long serialVersionUID = 3524745734405195338L;

    private Class<? extends CardBean> type;

    private String name;

    private String description;

    private String icon;

    private int order = 0;

    public Class<? extends CardBean> getType() {
        return type;
    }

    public void setType(Class<? extends CardBean> type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(name).append(description).append(icon).append(order).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj.getClass() == this.getClass())) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        CardDescription rhs = (CardDescription) obj;
        return new EqualsBuilder().append(name, rhs.name).append(description, rhs.description).append(icon, rhs.icon).append(order, rhs.order).isEquals();
    }

    /**
     * Respectant le contrat recommandé par la documentation Java, la comparaison avec o -> null lancera une "NullPointerException".
     *  http://docs.oracle.com/javase/7/docs/api/java/lang/Comparable.html
     * @param o : une {@link CardDescription} à comparer.
     * @return this.getOrder() - o.getOrder()
     */
    @Override
    public int compareTo(CardDescription o) {
        if (o == this) {
            return 0;
        }
        return this.getOrder() - o.getOrder();
    }
}
