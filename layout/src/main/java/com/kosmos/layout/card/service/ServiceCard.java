package com.kosmos.layout.card.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.UpdateToDataSourceException;
import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.dao.CardDAO;
import com.kosmos.layout.card.util.CardDescription;
import com.kosmos.layout.card.util.CardList;
import com.kosmos.layout.exception.CardNotFoundException;
import com.kosmos.layout.meta.bean.CardMetaBean;
import com.kosmos.layout.meta.dao.CardMetaDAO;
import com.kosmos.layout.slot.Slot;
import com.kosmos.service.impl.AbstractBeanAwareServiceBean;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.util.MetatagUtils;

public class ServiceCard extends AbstractBeanAwareServiceBean<CardBean, CardDAO> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceCard.class);

    private final CardList cardsPool;

    private Map<Class<? extends CardBean>, CardDescription> availableCards;

    private CardMetaDAO cardMetaDao;

    public ServiceCard() {
        this.availableCards = new HashMap<>();
        this.cardsPool = new CardList();
    }

    public void setCardMetaDao(CardMetaDAO cardMetaDao) {
        this.cardMetaDao = cardMetaDao;
    }

    @Override
    public void save(CardBean cardBean) {
        throw new UnsupportedOperationException("Cette opération n'est pas supportée sur ce service. Utilisez la méthode \"addCardForMeta\"");
    }

    @Override
    public void delete(Long id) {
        throw new UnsupportedOperationException("Cette opération n'est pas supportée sur ce service. Utilisez la méthode \"deleteCardForMeta\"");
    }

    @Override
    public CardBean getById(Long id) {
        throw new UnsupportedOperationException("Cette opération n'est pas supportée sur ce service. Utilisez la méthode \"getCardById\"");
    }

    public CardList getAllowedCardsPool(Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes) {
        final CardList allowedPool = new CardList();
        final Collection<Class<? extends CardBean>> allowedCardTypesSet = getAllowedCardTypesSet(allowedCardTypes);
        for (CardBean currentCardBean : cardsPool) {
            if (allowedCardTypesSet.contains(currentCardBean.getClass())) {
                allowedPool.add(currentCardBean);
            }
        }
        return allowedPool;
    }

    private Collection<Class<? extends CardBean>> getAllowedCardTypesSet(Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes) {
        final Collection<Class<? extends CardBean>> allowedTypesSet = new HashSet<>();
        if(MapUtils.isNotEmpty(allowedCardTypes)) {
            for (Map.Entry<String, Collection<Class<? extends CardBean>>> currentEntry : allowedCardTypes.entrySet()) {
                allowedTypesSet.addAll(currentEntry.getValue());
            }
        }
        return allowedTypesSet;
    }

    public Map<String, String> getAllowedCardBoView(Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes) {
        final Map<String, String> allowedPool = new HashMap<>();
        final Collection<Class<? extends CardBean>> allowedCardTypesSet = getAllowedCardTypesSet(allowedCardTypes);
        for (CardBean currentCardBean : cardsPool) {
            if (allowedCardTypesSet.contains(currentCardBean.getClass())) {
                allowedPool.put(currentCardBean.getClass().getName(), currentCardBean.getViewBo());
            }
        }
        return allowedPool;
    }

    /**
     * Useful method to save a card and bind it to a {@link MetatagBean}.
     *
     * @param card the {@link com.kosmos.layout.card.bean.CardBean} to save.
     * @param meta the {@link MetatagBean} to bind the {@link com.kosmos.layout.card.bean.CardBean} "card" to.
     * @throws DataSourceException thrown for one the following reason : <ul> <li>The "card" could not be persisted in the datasource</li> <li>The
     * "card" could not be updated ine the data source</li> <li>The binding could not be persisted in the data
     * source</li> </ul>
     */
    public void addCardForMeta(CardBean card, MetatagBean meta) {
        super.save(card);
        // Ajout du card meta s'il n'est pas déjà présent
        CardMetaBean cardMeta = this.cardMetaDao.getCardMeta(card.getId(), meta.getId());
        if (cardMeta == null) {
            cardMeta = new CardMetaBean();
            cardMeta.setIdCard(card.getId());
            cardMeta.setIdMeta(meta.getId());
            this.cardMetaDao.add(cardMeta);
        }
    }

    /**
     * Delete a {@link com.kosmos.layout.card.bean.CardBean} for the given {@link MetatagBean}.
     *
     * @param card the {@link com.kosmos.layout.card.bean.CardBean} to delete.
     * @param meta the {@link MetatagBean} to delete the card for.
     * @throws DeleteFromDataSourceException thrown for one of the following reasons : <ul> <li>The {@link com.kosmos.layout.card.bean.CardBean} card could
     * not be removed</li> <li>The {@link com.kosmos.layout.meta.bean.CardMetaBean} corresponding could not be
     * removed</li> </ul>
     * @see com.kosmos.layout.meta.bean.CardMetaBean
     */
    public void deleteCardForMeta(CardBean card, MetatagBean meta) {
        if(card != null) {
            super.delete(card.getId());
        }
        if(meta != null && card != null) {
            final CardMetaBean cardMetaBean = this.cardMetaDao.getCardMeta(card.getId(), meta.getId());
            if (cardMetaBean != null) {
                this.cardMetaDao.delete(cardMetaBean.getId());
            }
        }
    }

    public void updateCard(CardBean card) {
        super.save(card);
    }

    public void updateCardModel(Map<String, CardBean> model) {
        for (CardBean currentCardBean : model.values()) {
            updateCard(currentCardBean);
        }
    }

    /**
     * Retrieve a {@link java.util.Map} of {@link com.kosmos.layout.card.bean.CardBean} bound to a {@link
     * com.univ.objetspartages.om.FicheUniv} object.
     *
     * @param fiche the {@link com.univ.objetspartages.om.FicheUniv} to search from.
     * @return a {@link java.util.Map} of {@link com.kosmos.layout.card.bean.CardBean} full-filling the binding
     * conditions.
     */
    public Map<String, CardBean> getCards(FicheUniv fiche) {
        final Map<String, CardBean> cards = new HashMap<>();
        final MetatagBean meta = MetatagUtils.lireMeta(fiche);
        if(meta != null) {
            cards.putAll(getCards(meta.getId()));
        }
        return cards;
    }

    /**
     * Retrieve a {@link java.util.Map} of {@link com.kosmos.layout.card.bean.CardBean} bound to a {@link
     * MetatagBean}.
     *
     * @param metaId the id of the {@link MetatagBean} to search from.
     * @return a {@link java.util.Map} of {@link com.kosmos.layout.card.bean.CardBean} bean full-filling the binding
     * conditions.
     */
    public Map<String, CardBean> getCards(Long metaId) {
        Collection<CardMetaBean> cardMeta = this.cardMetaDao.getByMeta(metaId);
        Map<String, CardBean> cards = new HashMap<>();
        for (CardMetaBean currentCardMeta : cardMeta) {
            try {
                CardBean currentCardBean = this.dao.getById(currentCardMeta.getIdCard());
                cards.put(currentCardBean.getKey().toString(), currentCardBean);
            } catch (DataSourceException e) {
                LOG.warn(String.format(MessageHelper.getCoreMessage("BO.CARD.EXCEPTION.NOT_FOUND_META"), metaId), e);
            }
        }
        return cards;
    }

    /**
     * Retrieve a card by its id.
     *
     * @param cardId the id of the card to look for.
     * @return the card found as a {@link com.kosmos.layout.card.bean.CardBean}.
     * @throws com.kosmos.layout.exception.CardNotFoundException thrown if the card could not be found.
     */
    public CardBean getCardById(Long cardId) throws CardNotFoundException {
        try {
            return this.dao.getById(cardId);
        } catch (DataSourceException e) {
            throw new CardNotFoundException(String.format(MessageHelper.getCoreMessage("BO.CARD.EXCEPTION.NOT_FOUND_ID"), cardId), e);
        }
    }

    /**
     * Useful method to compute and retrieve a {@link java.util.Map} of edit fragment according to a {@link
     * com.kosmos.layout.card.bean.CardBean} Map.
     *
     * @param pool
     * 		the {@link java.util.Collection} of {@link com.kosmos.layout.card.bean.CardBean} from which the edit fragments should
     * 		be computed.
     * @return a {@link java.util.Map} of edit fragments.
     */
    public Map<String, String> getAllowedEditFragments(Collection<CardBean> pool) {
        final Map<String, String> editFragments = new HashMap<>();
        for (CardBean currentCard : pool) {
            editFragments.put(currentCard.getClass().getName(), currentCard.getEditFragment());
        }
        return editFragments;
    }

    /**
     * Useful method to assert consistency between a provided model and a data source model. The provided model stand as
     * the reference and shall remain intact. Only the data source model will be modified during the comparison.
     *
     * @param ficheUniv the {@link com.univ.objetspartages.om.FicheUniv} object carrying the model to clean up.
     * @param providedModel the model as a {@link java.util.Map} used to compare the persisted model to.
     */
    public void cleanUpModel(FicheUniv ficheUniv, Map<String, CardBean> providedModel) {
        final MetatagBean meta = MetatagUtils.lireMeta(ficheUniv);
        final Map<String, CardBean> recordedModel = getCards(ficheUniv);
        for (Map.Entry<String, CardBean> currentEntry : recordedModel.entrySet()) {
            if (!providedModel.containsKey(currentEntry.getKey())) {
                deleteCardForMeta(currentEntry.getValue(), meta);
            }
        }
    }

    @Override
    public void refresh() {
        availableCards.clear();
        cardsPool.clear();
        final Map<String, Collection<CardDescription>> retrievedCardDescription = ApplicationContextManager.getAllBeansOfTypeByExtension(CardDescription.class);
        if (MapUtils.isNotEmpty(retrievedCardDescription)) {
            for (Collection<CardDescription> currentsCardsDescriptions : retrievedCardDescription.values()) {
                for(CardDescription currentCardDescription : currentsCardsDescriptions) {
                    availableCards.put(currentCardDescription.getType(), currentCardDescription);
                    try {
                        cardsPool.add(currentCardDescription.getType().newInstance());
                    } catch (InstantiationException | IllegalAccessException e) {
                        LOG.error("Une erreur est survenue lors de la création du pool de carte", e);
                    }
                }
            }
        }
    }

    public Collection<CardDescription> getAllowedCardsDescriptions(Layout layout) {
        final List<CardDescription> descriptions = new ArrayList<>();
        final Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes = getAllowedCardTypes(layout);
        for (Collection<Class<? extends CardBean>> cards : allowedCardTypes.values()) {
            for (Class<? extends CardBean> currentClass : cards) {
                final CardDescription description = availableCards.get(currentClass);
                if (!descriptions.contains(description)) {
                    descriptions.add(description);
                }
            }
        }
        Collections.sort(descriptions);
        return descriptions;
    }

    public Map<String, Collection<Class<? extends CardBean>>> getAllowedCardTypes(Layout layout) {
        final Map<String, Collection<Class<? extends CardBean>>> cardsAllowed = new HashMap<>();
        if(layout != null) {
            for (Slot currentSlot : layout.getSlots()) {
                cardsAllowed.put(currentSlot.getKey().toString(), getSlotAllowedClasses(layout, currentSlot));
            }
        }
        return cardsAllowed;
    }

    private Collection<Class<? extends CardBean>> getSlotAllowedClasses(Layout layout, Slot slot) {
        final Collection<Class<? extends CardBean>> allowedClasses = new ArrayList<>();
        if (CollectionUtils.isEmpty(slot.getAllowedCardTypes())) {
            if (CollectionUtils.isEmpty(layout.getAllowedCardTypes())) {
                allowedClasses.addAll(getAllCardClasses());
            } else {
                allowedClasses.addAll(layout.getAllowedCardTypes());
            }
        } else {
            allowedClasses.addAll(slot.getAllowedCardTypes());
        }
        return allowedClasses;
    }

    private Collection<Class<? extends CardBean>> getAllCardClasses() {
        return availableCards.keySet();
    }
}
