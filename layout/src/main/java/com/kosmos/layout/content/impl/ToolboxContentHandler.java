package com.kosmos.layout.content.impl;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.layout.annotations.ContentTypes;
import com.kosmos.layout.annotations.MediaContent;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.dao.CardDAO;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.utils.ContexteUtil;

/**
 * Created on 11/11/2014.
 */
public class ToolboxContentHandler extends AbstractContentHandler<MediaContent> {

    private static final Logger LOG = LoggerFactory.getLogger(ToolboxContentHandler.class);

    private static final Pattern imagePattern = Pattern.compile("(\\[id-image\\]([0-9]+)\\[/id-image\\])");

    private static final Pattern linkPattern = Pattern.compile("\\[id-fichier\\]([0-9]+)\\[/id-fichier\\]");

    private CardDAO cardDao;

    private ServiceRessource serviceRessource;

    public void setCardDao(CardDAO cardDao) {
        this.cardDao = cardDao;
    }

    public void setServiceRessource(final ServiceRessource serviceRessource) {
        this.serviceRessource = serviceRessource;
    }

    @Override
    protected void prepareModelContent(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas) {
        for (CardBean currentCard : inputedModel.values()) {
            prepareCardContent(currentCard);
        }
    }

    private void prepareCardContent(CardBean card) {
        final HttpSession session = ContexteUtil.getContexteUniv().getRequeteHTTP().getSession();
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        if (sessionUtilisateur != null) {
            final Collection<Field> annotatedFields = getAnnotatedFields(card);
            for (Field currentField : annotatedFields) {
                try {
                    final String content = getContent(currentField, card);
                    if (StringUtils.isNotBlank(content)) {
                        final Map<String, Object> infosSession = sessionUtilisateur.getInfos();
                        final Matcher m = imagePattern.matcher(content);
                        while (m.find()) {
                            infosSession.put(m.group(1), Boolean.TRUE);
                        }
                        // on transforme les tags [id-image] en
                        // /servlet/com.univ.utils.LectureImageToolbox?TAG=[id-image]
                        final String replacePattern = "src=\"(http://)*([a-zA-Z0-9:]+)*(/servlet/com.univ.utils.LectureImageToolbox\\?TAG=)*\\[id-image\\]";
                        currentField.set(card, StringUtils.replacePattern(content, replacePattern, "src=\"/servlet/com.univ.utils.LectureImageToolbox?TAG=[id-image]"));
                    }
                } catch (IllegalAccessException e) {
                    LOG.error(String.format("An error occured trying to handle card %s", card.toString()), e);
                }
            }
        }
    }

    private String getContent(Field currentField, CardBean card) throws IllegalAccessException {
        final MediaContent contentType = currentField.getAnnotation(MediaContent.class);
        if (contentType.type() == ContentTypes.TOOLBOX) {
            currentField.setAccessible(true);
            return (String) currentField.get(card);
        }
        return StringUtils.EMPTY;
    }

    @Override
    protected int handleModel(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas, final int startIndex) {
        int index = startIndex;
        for (CardBean currentCard : inputedModel.values()) {
            final Collection<Field> annotatedFields = getAnnotatedFields(currentCard);
            for (Field currentField : annotatedFields) {
                final MediaContent contentType = currentField.getAnnotation(MediaContent.class);
                if (contentType.type() == ContentTypes.TOOLBOX) {
                    try {
                        currentField.setAccessible(true);
                        final CardBean recordedCard = getRecordedCard(currentCard);
                        final Set<String> newResources = findResources(currentField, currentCard);
                        final Set<String> oldResources = new HashSet<>();
                        if (recordedCard != null) {
                            oldResources.addAll(findResources(currentField, recordedCard));
                            oldResources.removeAll(newResources);
                        }
                        datas.put("contentNewRessources", newResources);
                        datas.put("contentOldResources", oldResources);
                        serviceRessource.saveContentResource(datas, ficheUniv);
                        index += newResources.size();
                    } catch (IllegalAccessException e) {
                        LOG.error(String.format("An error occurred trying to handle card %s", currentCard.toString()), e);
                    }
                }
            }
        }
        return index;
    }

    private CardBean getRecordedCard(CardBean card) {
        CardBean recordedCard;
        try {
            recordedCard = cardDao.getById(card.getId());
        } catch (DataSourceException e) {
            LOG.debug("unable to query the database", e);
            return null;
        }
        return recordedCard;
    }

    private Set<String> findResources(Field field, CardBean card) throws IllegalAccessException {
        final String content = (String) field.get(card);
        final Set<String> newResources = new HashSet<>();
        if (StringUtils.isNotBlank(content)) {
            final Matcher linkMatcher = linkPattern.matcher(content);
            final Matcher imageMatcher = imagePattern.matcher(content);
            while (imageMatcher.find()) {
                newResources.add(String.format("%s#IMG", imageMatcher.group(2)));
            }
            while (linkMatcher.find()) {
                newResources.add(String.format("%s#LIEN", linkMatcher.group(1)));
            }
        }
        return newResources;
    }
}
