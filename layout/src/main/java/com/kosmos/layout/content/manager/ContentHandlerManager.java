package com.kosmos.layout.content.manager;

import java.util.Collection;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.layout.content.ContentHandler;
import com.kportal.extension.module.AbstractBeanManager;

/**
 * Created by Fabien on 11/11/2014.
 */
public class ContentHandlerManager extends AbstractBeanManager {

    public static final String ID_BEAN = "contentHandlerManager";

    private Collection<ContentHandler> contentHandlers;

    public Collection<ContentHandler> getContentHandlers() {
        return contentHandlers;
    }

    @Override
    public void refresh() {
        contentHandlers = ApplicationContextManager.getAllBeansOfType(ContentHandler.class).values();
    }
}
