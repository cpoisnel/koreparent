package com.kosmos.layout.content;

import java.util.Map;

import com.kosmos.layout.card.bean.CardBean;
import com.univ.objetspartages.om.FicheUniv;

/**
 * Created by Fabien on 11/11/2014.
 */
public interface ContentHandler {

    void prepareContent(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas);

    int handleContent(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas, final int startIndex);
}
