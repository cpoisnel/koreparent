package com.kosmos.layout.exception;

import com.jsbsoft.jtf.exception.ErreurApplicative;

/**
 * Created on 03/09/15.
 */
public class ViewModelBuilderException extends ErreurApplicative {

    private static final long serialVersionUID = -2773526251216698341L;

    public ViewModelBuilderException(String mes) {
        super(mes);
    }

    public ViewModelBuilderException(String mes, Throwable t) {
        super(mes, t);
    }
}