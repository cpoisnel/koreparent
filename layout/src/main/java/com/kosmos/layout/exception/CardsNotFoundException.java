package com.kosmos.layout.exception;

import com.jsbsoft.jtf.exception.ErreurApplicative;

public class CardsNotFoundException extends ErreurApplicative {

    private static final long serialVersionUID = -2773526251216698341L;

    public CardsNotFoundException(String mes) {
        super(mes);
    }

    public CardsNotFoundException(String mes, Throwable t) {
        super(mes, t);
    }
}
