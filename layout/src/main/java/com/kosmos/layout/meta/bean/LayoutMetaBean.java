package com.kosmos.layout.meta.bean;

import com.univ.objetspartages.bean.AbstractPersistenceBean;

public class LayoutMetaBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = -7508545377327770309L;

    private Long idLayout;

    private Long idMeta;

    public Long getIdLayout() {
        return this.idLayout;
    }

    public void setIdLayout(Long idLayout) {
        this.idLayout = idLayout;
    }

    public Long getIdMeta() {
        return this.idMeta;
    }

    public void setIdMeta(Long idMeta) {
        this.idMeta = idMeta;
    }
}
