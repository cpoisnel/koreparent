package com.kosmos.layout.meta.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.layout.meta.bean.LayoutMetaBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LayoutMetaDAO extends AbstractCommonDAO<LayoutMetaBean> {

    private static final Logger LOG = LoggerFactory.getLogger(LayoutMetaDAO.class);

    public LayoutMetaDAO() {
        this.tableName = "LAYOUT_META";
    }

    public LayoutMetaBean getByMetaId(Long metaId) throws DataSourceException {
        LayoutMetaBean result = null;
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("id", metaId);
        try {
            result= namedParameterJdbcTemplate.queryForObject("SELECT * FROM LAYOUT_META T1 WHERE T1.ID_META = :id", parameterSource, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for meta id  %d on table %s", metaId, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size on LAYOUT_META with metaId %d", metaId));
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on LAYOUT_META with meta id %d", metaId), dae);
        }
        return result;
    }

    public LayoutMetaBean getLayoutMeta(Long layoutId, Long metaId) throws DataSourceException {
        LayoutMetaBean result = null;
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("id", metaId);
        parameterSource.addValue("layoutId", layoutId);
        try {
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM LAYOUT_META T1 WHERE T1.ID_LAYOUT = :layoutId AND T1.ID_META = :id", parameterSource, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for layout id %d and meta id %d on table %s", layoutId, metaId, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size on layout id %d and meta id %d", layoutId, metaId));
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on LAYOUT_META with layout id %d and meta id %d", layoutId, metaId), dae);
        }
        return result;
    }

    public List<LayoutMetaBean> getLayoutMetasByLayoutId(Long layoutId){
        List<LayoutMetaBean> result = Collections.emptyList();
        final MapSqlParameterSource parameterSource = new MapSqlParameterSource("layoutId", layoutId);
        try {
            result = namedParameterJdbcTemplate.query("SELECT * FROM LAYOUT_META T1 WHERE T1.ID_LAYOUT = :layoutId", parameterSource, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("Unable to query on LAYOUT_META with layout id %d", layoutId), dae);
        }
        return result;
    }
}
