package com.kosmos.layout.meta.bean;

import com.univ.objetspartages.bean.AbstractPersistenceBean;

public class CardMetaBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = -2628220734768596349L;

    private Long idCard;

    private Long idMeta;

    public Long getIdCard() {
        return this.idCard;
    }

    public void setIdCard(Long idCard) {
        this.idCard = idCard;
    }

    public Long getIdMeta() {
        return this.idMeta;
    }

    public void setIdMeta(Long idMeta) {
        this.idMeta = idMeta;
    }
}
