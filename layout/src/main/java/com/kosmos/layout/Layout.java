package com.kosmos.layout;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.slot.util.SlotList;
import com.univ.objetspartages.bean.PersistenceBean;
import com.univ.objetspartages.om.FicheUniv;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public interface Layout extends PersistenceBean, Serializable {

    SlotList getSlots();

    Collection<Class<? extends CardBean>> getAllowedCardTypes();

    String getName();

    String getDescription();

    String getView();

    String getViewBo();

    boolean hasContext();

    boolean isCustom();

    Collection<Class<?>> getAllowedCarrierTypes();

}
