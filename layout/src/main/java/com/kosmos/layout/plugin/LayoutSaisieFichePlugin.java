package com.kosmos.layout.plugin;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.plugin.objetspartages.DefaultPluginSaisieFiche;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;

/**
 * Created by fabien.leconte on 13/11/14.
 */
public class LayoutSaisieFichePlugin extends DefaultPluginSaisieFiche {

    private List<String> classes;

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(final List<String> classes) {
        this.classes = classes;
    }

    @Override
    public void traiterAction(final Map<String, Object> datas, final FicheUniv ficheUniv, MetatagBean meta) throws Exception {
        if (isActive(ficheUniv.getClass().getName())) {
            final String action = (String) datas.get("ACTION");
            if ("CHANGER_LAYOUT".equals(action)) {
                changeLayout(datas, ficheUniv);
            } else if ("ONGLET".equals(action)) {
                final Long layoutId = StringUtils.isNumeric((String) datas.get("ID_LAYOUT")) ? Long.parseLong((String) datas.get("ID_LAYOUT")) : null;
                if (layoutId != null) {
                    LayoutFichePluginHelper.propagateInputedModel(datas, layoutId, ficheUniv);
                }
            }
        }
    }

    private void changeLayout(Map<String, Object> datas, FicheUniv ficheUniv) throws Exception {
        final Long idFiche = Long.valueOf((String) datas.get("ID_FICHE"));
        if (idFiche != 0) {
            try {
                ficheUniv.setIdFiche(idFiche);
                ficheUniv.retrieve();
            } catch (final Exception e) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_FICHE_INEXISTANTE"), e);
            }
        }
        final Long layoutId = Long.valueOf(StringUtils.defaultIfBlank((String) datas.get("ID_LAYOUT"), "0"));
        LayoutFichePluginHelper.prepareBoDatas(datas, ficheUniv, layoutId);
    }

    public boolean isActive(final String classe) {
        if (classes == null) {
            return Boolean.TRUE;
        } else {
            if (classes.contains(classe)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}
