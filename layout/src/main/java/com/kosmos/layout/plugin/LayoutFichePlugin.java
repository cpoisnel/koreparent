package com.kosmos.layout.plugin;

import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.service.ServiceCard;
import com.kosmos.layout.content.ContentHandler;
import com.kosmos.layout.content.manager.ContentHandlerManager;
import com.kosmos.layout.service.ServiceLayout;
import com.kportal.core.context.BeanUtil;
import com.kportal.extension.module.plugin.objetspartages.DefaultPluginFiche;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.utils.ContexteUtil;

public class LayoutFichePlugin extends DefaultPluginFiche {

    private ContentHandlerManager contentHandlerManager;

    private ServiceCard serviceCard;

    private ServiceLayout serviceLayout;

    public void setContentHandlerManager(ContentHandlerManager contentHandlerManager) {
        this.contentHandlerManager = contentHandlerManager;
    }

    public void setServiceCard(ServiceCard serviceCard) {
        this.serviceCard = serviceCard;
    }

    public void setServiceLayout(ServiceLayout serviceLayout) {
        this.serviceLayout = serviceLayout;
    }

    @Override
    public void supprimerObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjetCible) throws Exception {
        final Map<String, CardBean> cardModel = serviceCard.getCards(meta.getId());
        for (CardBean currentCard : cardModel.values()) {
            serviceCard.deleteCardForMeta(currentCard, meta);
        }
        serviceLayout.deleteLayoutForMeta(meta.getId());
    }

    @Override
    public void preparerPrincipal(final Map<String, Object> datas, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        LayoutFichePluginHelper.prepareBoDatas(datas, ficheUniv);
    }

    @Override
    public void traiterPrincipal(final Map<String, Object> datas, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        final Long idFiche = Long.valueOf(StringUtils.defaultIfBlank((String) datas.get("ID_FICHE"), "0"));
        final Long layoutId = Long.valueOf(StringUtils.defaultIfBlank((String) datas.get("ID_LAYOUT"), "0"));
        final Long customLayoutId = Long.valueOf(StringUtils.defaultIfBlank((String) datas.get("CUSTOM_LAYOUT"), "0"));
        final int filesStartIndex = Integer.valueOf(StringUtils.defaultIfBlank((String) datas.get("FILES_START_INDEX"),"0"));

        if (!layoutId.equals(0L) && customLayoutId.equals(0L)) {
            // Association du layout à la fiche
            final Layout layout = serviceLayout.getLayout(layoutId);
            persistModel(layout,meta,ficheUniv,idFiche,datas,filesStartIndex);
        } else {
            //Case custom layout
            if (customLayoutId != 0L) {
                final Layout oldLayout = serviceLayout.getLayout(customLayoutId);
                //Recuperation d'une instance similaire
                final Layout newLayout = serviceLayout.getLayout(customLayoutId);
                //Initialisation du nouveau model
                newLayout.setId(0L);
                //Mise à jour du model avec les données de formulaires
                serviceLayout.updateLayoutModel(oldLayout,datas,newLayout);
                serviceLayout.addLayout(newLayout);
                serviceLayout.deleteLayoutWithNoLayoutMeta(layoutId);
                persistModel(newLayout,meta,ficheUniv,idFiche,datas,filesStartIndex);
            }
        }
    }

    private void persistModel(Layout layout, MetatagBean meta, FicheUniv ficheUniv, Long idFiche, Map<String, Object> datas, int filesStartIndex){
        serviceLayout.addLayoutForMeta(layout, meta.getId());
        // Récupération du modèle saisi
        final boolean resetModelIds = !Objects.equals(ficheUniv.getIdFiche(), idFiche);
        final Map<String, CardBean> inputedModel = LayoutFichePluginHelper.retrieveModelFromDatas(datas, layout, resetModelIds);
        // Nettoyage du modèle eventuellement présent par rapport au modèle saisi
        serviceCard.cleanUpModel(ficheUniv, inputedModel);
        // Sauvegarde du modèle et des références vers la fiche
        handleContent(inputedModel, ficheUniv, datas, filesStartIndex);
        for (Map.Entry<String, CardBean> currentEntry : inputedModel.entrySet()) {
            // Prise en charge du contenu à partir des "ContentHandler" définis
            serviceCard.addCardForMeta(currentEntry.getValue(), meta);
        }
    }

    private void handleContent(Map<String, CardBean> inputedModel, FicheUniv ficheUniv, Map<String, Object> datas, int filesStartIndex) {
        int startIndex = filesStartIndex;
        for (ContentHandler currentHandler : contentHandlerManager.getContentHandlers()) {
            startIndex += currentHandler.handleContent(inputedModel, ficheUniv, datas, startIndex);
        }
    }

    @Override
    public void setDataContexteUniv(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        LayoutFichePluginHelper.prepareFoDatas(ContexteUtil.getContexteUniv().getDatas(), ficheUniv);
    }

    @Override
    public Object getDataContexteUniv() {
        return ContexteUtil.getContexteUniv().getData(BeanUtil.getBeanKey(getId(), getIdExtension()));
    }
}
