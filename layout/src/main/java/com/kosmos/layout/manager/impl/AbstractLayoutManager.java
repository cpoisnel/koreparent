package com.kosmos.layout.manager.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.layout.Layout;
import com.kosmos.layout.dao.LayoutDAO;
import com.kosmos.layout.manager.LayoutManager;
import com.kportal.extension.module.AbstractBeanManager;

public abstract class AbstractLayoutManager<T extends Layout> extends AbstractBeanManager implements LayoutManager<T> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractLayoutManager.class);

    private Class<T> actualClass;

    private LayoutDAO layoutDao;

    private String id;

    private String name;

    private Map<String, T> layouts;

    public AbstractLayoutManager() {
        actualClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        layouts = new HashMap<>();
    }

    public void setLayoutDao(LayoutDAO layoutDao) {
        this.layoutDao = layoutDao;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, T> getLayouts() {
        return layouts;
    }

    public void setLayouts(Map<String, T> layouts) {
        this.layouts = layouts;
    }

    @Override
    public void refresh() {
        final Collection<T> beans = ApplicationContextManager.getAllBeansOfType(actualClass).values();
        for (T currentLayout : beans) {
            try {
                syncLayout(currentLayout);
            } catch (DataSourceException e) {
                LOG.error(String.format("An error occurred trying to persist layout with name %s", currentLayout.getName()), e);
            } catch (InvocationTargetException | IllegalAccessException e) {
                LOG.error(String.format("An error occurred trying to update layout with name %s", currentLayout.getName()), e);
            }
        }
    }

    private void syncLayout(final T currentLayout) throws IllegalAccessException, InvocationTargetException {
        final Layout layout = layoutDao.getByName(currentLayout.getName());
        if (layout != null) {
            if (actualClass.isInstance(layout)) {
                currentLayout.setId(layout.getId());
                layouts.put(layout.getName(), (T) layoutDao.update(currentLayout));
            } else {
                layoutDao.delete(layout.getId());
                layouts.put(currentLayout.getName(), (T) layoutDao.add(currentLayout));
            }
        } else {
            layouts.put(currentLayout.getName(), (T) layoutDao.add(currentLayout));
        }
    }
}
