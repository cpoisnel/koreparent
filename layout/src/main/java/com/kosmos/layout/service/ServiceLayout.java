package com.kosmos.layout.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kosmos.service.impl.ServiceManager;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.layout.Layout;
import com.kosmos.layout.dao.LayoutDAO;
import com.kosmos.layout.exception.DefaultLayoutNotFoundException;
import com.kosmos.layout.exception.LayoutNotFoundException;
import com.kosmos.layout.manager.LayoutManager;
import com.kosmos.layout.meta.bean.LayoutMetaBean;
import com.kosmos.layout.meta.dao.LayoutMetaDAO;
import com.kosmos.service.impl.AbstractBeanAwareServiceBean;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.util.MetatagUtils;

public class ServiceLayout extends AbstractBeanAwareServiceBean<Layout, LayoutDAO> {

	private static final Logger L0G = LoggerFactory.getLogger(ServiceLayout.class);

	private final Map<Class<?>, LayoutManager> layoutManagers = new HashMap<>();

	private LayoutMetaDAO layoutMetaDao;

	private Map<String, String> defaultLayouts = new HashMap<>();

	public void setLayoutMetaDao(LayoutMetaDAO layoutMetaDao) {
		this.layoutMetaDao = layoutMetaDao;
	}

	public Map<String, String> getDefaultLayouts() {
		return defaultLayouts;
	}

	public void setDefaultLayouts(Map<String, String> defaultLayouts) {
		this.defaultLayouts = defaultLayouts;
	}

	public void refresh() {
		Collection<LayoutManager> managers = ApplicationContextManager.getAllBeansOfType(LayoutManager.class).values();
		for (LayoutManager currentManager : managers) {
			this.layoutManagers.put(currentManager.getClass(), currentManager);
		}
	}

	@Override public void save(Layout layout) {
		throw new UnsupportedOperationException("Cette opération n'est pas supportée pour ce service. Utilisez la méthode \"addLayoutForMeta\"");
	}

	@Override public Layout getById(Long id) {
		throw new UnsupportedOperationException("Cette opération n'est pas supportée pour ce service. Utilisez la méthode \"getLayout\"");
	}

	@Override public void delete(Long id) {
		throw new UnsupportedOperationException("Cette opération n'est pas supportée pour ce service. Utilisez la méthode \"deleteLayoutForMeta\"");
	}

	/**
	 * Method used to retrieve a Collection of layout.
	 *
	 * @return a layout Collection
	 *
	 * @deprecated  This method does not filter by the class carrier, use {@link #getAvailableLayouts(Class)} instead.
	 */
	@Deprecated
	public Collection<Layout> getAvailableLayouts() {
		try {
			return dao.getAllLayouts();
		} catch (DataSourceException e) {
			L0G.debug("unable to query the database", e);
			return null;
		}
	}


	/**
	 * Method used to retrieve a Collection of layout.
	 *
	 * @return a layout Collection
	 */
	public Collection<Layout> getAvailableLayouts(Class<?> clazz) {
		try {
			return dao.getAllLayouts(clazz);
		} catch (DataSourceException e) {
			L0G.debug("unable to query the database", e);
			return null;
		}
	}

	/**
	 * Allow to retrieve a given layout.
	 *
	 * @param layoutId : the id of the layout to retrieve.
	 * @return found layout.
	 * @throws LayoutNotFoundException : thrown when no eligible layout was found.
	 */
	public Layout getLayout(Long layoutId) throws LayoutNotFoundException {
		try {
			final Layout layout = dao.getById(layoutId);
			if (layout == null) {
				throw new LayoutNotFoundException(String.format(MessageHelper.getCoreMessage("BO.LAYOUT.EXCEPTION.NOT_FOUND_ID"), layoutId));
			}
			return layout;
		} catch (DataSourceException e) {
			throw new LayoutNotFoundException(String.format(MessageHelper.getCoreMessage("BO.LAYOUT.EXCEPTION.NOT_FOUND_ID"), layoutId), e);
		}
	}

	/**
	 * Allow to retrieve a layout according to its name.
	 *
	 * @param layoutName : the name of the layout as it appears in the data source.
	 * @return found layout.
	 * @throws LayoutNotFoundException : thrown when no eligible layout was found.
	 */
	public Layout getLayout(String layoutName) throws LayoutNotFoundException {
		if (StringUtils.isNotBlank(layoutName)) {
			try {
				return dao.getByName(layoutName);
			} catch (DataSourceException e) {
				throw new LayoutNotFoundException(String.format(MessageHelper.getCoreMessage("BO.LAYOUT.EXCEPTION.NOT_FOUND_NAME"), layoutName), e);
			}
		}
		return null;
	}

	/**
	 * Allow to retrieve a layout given a "FicheUniv" object.
	 *
	 * @param fiche : the "FicheUniv" object to search from.
	 * @return layout attached to the "FicheUniv" object.
	 * @throws LayoutNotFoundException        : thrown if no attached layout was found.
	 * @throws DefaultLayoutNotFoundException : thrown if no default layout was found for the fiche type.
	 */
	public Layout getLayout(FicheUniv fiche) throws DefaultLayoutNotFoundException, LayoutNotFoundException {
		MetatagBean meta = MetatagUtils.lireMeta(fiche);
		if (meta == null) {
			throw new LayoutNotFoundException(String.format(MessageHelper.getCoreMessage("BO.LAYOUT.EXCEPTION.NOT_FOUND_FICHE"), fiche.getCode()));
		}
		// Tentative de récupération du layout par défaut pour la fiche.
		try {
			return getLayoutForMeta(meta.getId());
		} catch (LayoutNotFoundException e) {
			L0G.debug("unable to find any layout", e);
			final String classObject = ReferentielObjets.getClasseObjet(ReferentielObjets.getCodeObjet(fiche));
			final String defaultLayout = defaultLayouts.get(classObject);
			Layout layout = getLayout(defaultLayout);
			if (layout == null) {
				throw new DefaultLayoutNotFoundException(String.format(MessageHelper.getCoreMessage("BO.LAYOUT.EXCEPTION.DEFAULT_NOT_FOUND"), classObject));
			}
			return layout;
		}
	}

	/**
	 * Allow to retrieve a layout according to a Metatag id.
	 *
	 * @param metaId : the id of the meta to search from.
	 * @return layout attached to the meta id.
	 * @throws DataSourceException : thrown if no eligible layout was found.
	 */
	public Layout getLayoutForMeta(Long metaId) throws LayoutNotFoundException {
		final LayoutMetaBean layoutMeta;
		try {
			layoutMeta = layoutMetaDao.getByMetaId(metaId);
			if (layoutMeta != null) {
				return dao.getById(layoutMeta.getIdLayout());
			}
		} catch (DataSourceException e) {
			throw new LayoutNotFoundException(String.format(MessageHelper.getCoreMessage("BO.LAYOUT.EXCEPTION.NOT_FOUND_META_ID"), metaId), e);
		}
		throw new LayoutNotFoundException(String.format(MessageHelper.getCoreMessage("BO.LAYOUT.EXCEPTION.NOT_FOUND_META_ID"), metaId));
	}

	/**
	 * Useful method to save bind a Layout and a Metatag
	 *
	 * @param layout : the layout to bind.
	 * @param metaId : id of the Metatag to bind.
	 * @throws DataSourceException : thrown if one of the following occurs : <ul> <li>The creation and persistence of the binding
	 *                             failed</li> <li>The existing bind could not be updated</li> </ul>
	 */
	public void addLayoutForMeta(Layout layout, Long metaId) throws DataSourceException {
		LayoutMetaBean layoutMeta = layoutMetaDao.getByMetaId(metaId);
		if (layoutMeta != null) {
			layoutMeta.setIdLayout(layout.getId());
			layoutMetaDao.update(layoutMeta);
		} else {
			layoutMeta = new LayoutMetaBean();
			layoutMeta.setIdLayout(layout.getId());
			layoutMeta.setIdMeta(metaId);
			layoutMetaDao.add(layoutMeta);
		}
	}

	/**
	 * Add a layout in DB
	 *
	 * @param layout
	 */
	public void addLayout(Layout layout) {
		dao.add(layout);
	}

	/**
	 * Remove old layout if custom
	 * @param idLayout
	 * Id of the layout used as layoutIdMeta
	 *
	 */
	public void deleteLayoutWithNoLayoutMeta(Long idLayout) throws LayoutNotFoundException {
		Layout layout = getLayout(idLayout);
		if (!hasLayoutMetaForLayout(layout) && layout.isCustom()) {
			dao.delete(layout.getId());
		}
	}

	/**
	 * Test is a layout has a LayoutMeta
	 * @param layout
	 * @return
	 */
	protected boolean hasLayoutMetaForLayout(Layout layout) {
		List<LayoutMetaBean> layoutMeta = layoutMetaDao.getLayoutMetasByLayoutId(layout.getId());
		return !layoutMeta.isEmpty();
	}

	/**
	 * Useful method to delete the reference between a layout a the given meta id.
	 *
	 * @param metaId : the id of the meta to delete the reference to.
	 */
	public void deleteLayoutForMeta(Long metaId) throws DataSourceException {
		LayoutMetaBean layoutMeta = layoutMetaDao.getByMetaId(metaId);
		layoutMetaDao.delete(layoutMeta.getId());
	}

	/**
	 * Useful method to retrieve a Collection of LayoutManager of the given or inheriting class.
	 *
	 * @param managerClass : the LayoutManager's class to retrieve.
	 * @param <T>          : should extends LayoutManager class.
	 * @return a Collection of LayoutManager full-filling the class condition.
	 */
	public <T extends LayoutManager> Collection<T> getLayoutManagers(Class<T> managerClass) {
		final List<T> managers = new ArrayList<>();
		for (Map.Entry<Class<?>, LayoutManager> currentEntry : this.layoutManagers.entrySet()) {
			if (ClassUtils.isAssignable(currentEntry.getKey(), managerClass)) {
				managers.add(managerClass.cast(currentEntry.getValue()));
			}
		}
		return managers;
	}

	public void updateLayoutModel(Layout layout, Map<String, Object> datas, Layout newLayout){
		ServiceModelLayout delegateService = ServiceManager.getServiceForBean(layout.getClass());
		if(delegateService != null) {
			delegateService.updateLayoutModel(layout, datas, newLayout);
		}
	}
}
