package com.kosmos.layout.service;

import com.kosmos.layout.Layout;

import java.util.Map;

/**
 * Created by alexandre.baillif on 01/12/16.
 */
public abstract class ServiceModelLayout {

	/**
	 * Provide the possibilities of updating a layout according to datas from form.
	 * @param layout
	 * @param datas
	 */
	public abstract void updateLayoutModel(Layout layout, Map<String, Object> datas, Layout newLayout);

}
