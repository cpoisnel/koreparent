package com.kosmos.layout.slot.view.model;

import java.io.Serializable;

import com.kosmos.layout.card.view.model.CardViewModel;
import com.kosmos.layout.slot.Slot;

/**
 * Created on 30/12/14.
 */
public class SlotViewModel implements Serializable {

    private static final long serialVersionUID = 1781451193534968607L;

    private Slot slot;

    private String allowedCardTypes;

    private CardViewModel cardViewModel;

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public String getAllowedCardTypes() {
        return allowedCardTypes;
    }

    public void setAllowedCardTypes(String allowedCardTypes) {
        this.allowedCardTypes = allowedCardTypes;
    }

    public CardViewModel getCardViewModel() {
        return cardViewModel;
    }

    public void setCardViewModel(CardViewModel cardViewModel) {
        this.cardViewModel = cardViewModel;
    }
}
