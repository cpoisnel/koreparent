package com.kosmos.layout.slot.util;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.layout.slot.Slot;

/**
 * Created by fabien.leconte on 08/12/14.
 */
public class SlotUtil {

    public static String getClassName(Slot slot) {
        return String.format("layout__slot layout__slot--%s layout__%s", slot.getState() == SlotState.EMPTY ? "empty" : "filled", StringUtils.uncapitalize(slot.getClass().getSimpleName()));
    }
}
