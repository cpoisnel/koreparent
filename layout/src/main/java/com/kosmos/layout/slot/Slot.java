package com.kosmos.layout.slot;

import java.io.Serializable;
import java.util.Collection;
import java.util.UUID;

import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.slot.util.SlotState;

/**
 * Created by fabien.leconte on 08/12/14.
 */
public interface Slot extends Serializable {

    Collection<Class<? extends CardBean>> getAllowedCardTypes();

    UUID getKey();

    SlotState getState();

    void setState(SlotState state);

    String getView();

    String getViewBo();

    String getDescription();
}
