package com.kosmos.search.index.service.fiche.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.Closure;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.objetspartages.util.MediaUtils;
import com.kosmos.search.index.bean.FicheIndexDocument;
import com.kosmos.search.index.bean.IndexingCommand;
import com.kosmos.search.index.bean.IndexingCommand.IndexingAction;
import com.kosmos.search.index.bean.MediaAttachmentBean;
import com.kosmos.search.index.bean.PluginIndexableBean;
import com.kosmos.search.index.bean.ResourceAttachmentBean;
import com.kosmos.search.index.service.ServiceIndexer;
import com.kosmos.search.query.service.ServiceSearcher;
import com.kosmos.search.index.service.fiche.ServiceIndexerFiche;
import com.kosmos.search.util.IndexerUtil;

/**
 * Service d'indexation des fiches et des pièces jointes associées. Implémentation Elasticsearch.
 * <p>La fiche est indexée sous l'index /fiche_nomfiche_langue/ et sous le type fiche. La ressource
 *  est également indexée sous ce même index sous le type "ressource". Ce choix est effectué pour éviter
 *   la collision de type potentiels dans les fiches (deux attributs avec le même nom mais un type différent)
 *   qui serait en erreur si toutes les fiches étaient dans le même index.
 * </p>
 * @author cpoisnel
 *
 */
public class ServiceIndexerFicheBeanImpl<T extends AbstractFicheBean> implements ServiceIndexerFiche<T> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerFicheBeanImpl.class);

    private Class<T> type;

    @Autowired
    private ServiceIndexer serviceIndexer;

    @Autowired
    private ServiceSearcher serviceSearcher;

    /**
     * Service Ressource, fait partie du Core (Injection possible)
     */
    @Autowired
    private ServiceRessource serviceRessource;

    /**
     * Service Media, fait partie du Core (Injection possible)
     */
    @Autowired
    private ServiceMedia serviceMedia;

    @Autowired
    private ServiceRubrique serviceRubrique;

    @Autowired
    private ServiceRubriquePublication serviceRubriquePublication;

    @Autowired
    private ServiceAccessControlFiche serviceAccessControlFiche;

    /**
     * Type paramétré.
     */
    public ServiceIndexerFicheBeanImpl() {
        this.type = (Class<T>) AbstractFicheBean.class;
    }

    public ServiceIndexerFicheBeanImpl(Class<T> type) {
        this.type = type;
    }

    public void setType(final Class<T> type) {
        this.type = type;
    }

    /**
     * Accesseur du type paramétré.
     * @return
     */
    @Override
    public Class<T> getType() {
        return type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveIndex(final MetatagBean metatag, final String type, final T bean) {
        final IndexingCommand indexingCommand = this.createSaveIndexCommand(metatag, type, bean);
        this.serviceIndexer.processIndexCommand(indexingCommand);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexingCommand createSaveIndexCommand(final MetatagBean metatag, final String type, final T bean) {
        final IndexingCommand indexingCommand = this.generateIndexingCommand(metatag, type, bean);
        LOG.debug("Commande d'indexation générée : {}", indexingCommand);
        return indexingCommand;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexingCommand createSaveIndexCommand(final RessourceBean ressource, final String type, final T bean, final Long parent) {
        final IndexingCommand indexingCommand = this.generateIndexingCommand(ressource, type, bean, parent);
        LOG.debug("Commande d'indexation de ressource générée : {}", indexingCommand);
        return indexingCommand;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveIndexRessource(final RessourceBean ressource, final String type, final T bean, final Long parent) {
        final IndexingCommand indexingCommand = this.createSaveIndexCommand(ressource, type, bean, parent);
        this.serviceIndexer.processIndexCommand(indexingCommand);
    }

    protected IndexingCommand generateIndexingCommand(final RessourceBean ressource, final String type, final T bean, final Long parent) {
        final IndexingCommand indexingCommand = new IndexingCommand();
        indexingCommand.setType(IndexerUtil.TYPE_RESSOURCE);
        final Locale locale = LangueUtil.getLocale(bean.getLangue());
        indexingCommand.setIndex(IndexerUtil.INDEX_FICHE + IndexerUtil.INDEX_SEPARATEUR + type + IndexerUtil.INDEX_SEPARATEUR + locale.getLanguage());
        final ResourceAttachmentBean resourceAttachmentBean = this.generateAttachment(bean, ressource);
        indexingCommand.setAction(IndexingAction.CREATE_UPDATE);
        indexingCommand.setDocument(resourceAttachmentBean);
        indexingCommand.setId(String.valueOf(ressource.getId()));
        indexingCommand.setParent(String.valueOf(parent));
        return indexingCommand;
    }

    /**
     *
     * @param ressourceBean
     * @return
     */
    protected String getCodeObjet(final RessourceBean ressourceBean) {
        final String codeParent = ressourceBean.getCodeParent();
        final String[] codes = codeParent.split(",");
        if (codes.length == 3) {
            final String[] type = codes[1].split("=");
            if (type.length == 2) {
                return type[1];
            }
        }
        LOG.warn("Impossible de déterminer le type de la fiche {}", ressourceBean);
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteIndex(final Long idMetatag) {
        // On fait le traitement pour les PJ
        final IndexingCommand indexingCommandRessource = this.createDeleteIndexRessourceByIdMeta(idMetatag);
        final IndexingCommand indexingCommand = this.createDeleteIndexCommand(idMetatag);
        this.serviceIndexer.processIndexCommand(indexingCommandRessource);
        this.serviceIndexer.processIndexCommand(indexingCommand);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexingCommand createDeleteIndexCommand(final Long idMetatag) {
        final IndexingCommand indexingCommand = new IndexingCommand();
        indexingCommand.setIndex(IndexerUtil.INDEX_FICHE + IndexerUtil.INDEX_WILDCARD);
        indexingCommand.setType(IndexerUtil.TYPE_FICHE);
        indexingCommand.setAction(IndexingAction.DELETE);
        indexingCommand.setId(String.valueOf(idMetatag));
        return indexingCommand;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexingCommand createDeleteIndexRessource(final Long idResource) {
        final IndexingCommand indexingCommand = new IndexingCommand();
        indexingCommand.setIndex(IndexerUtil.INDEX_FICHE + IndexerUtil.INDEX_WILDCARD);
        indexingCommand.setType(IndexerUtil.TYPE_RESSOURCE);
        final String byQuery = String.format("{\"query\": { \"term\": {\"_id\":\"%1$s\"}}}", idResource);
        indexingCommand.setByQuery(byQuery);
        indexingCommand.setAction(IndexingAction.DELETE);
        return indexingCommand;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexingCommand createDeleteIndexRessourceByIdMeta(final Long idMeta) {
        // Spécificité ES, on supprime en faisant une recherche par idRessource
        final IndexingCommand indexingCommand = new IndexingCommand();
        indexingCommand.setIndex(IndexerUtil.INDEX_FICHE + IndexerUtil.INDEX_WILDCARD);
        indexingCommand.setType(IndexerUtil.TYPE_RESSOURCE);
        final String byQuery = String.format("{\"query\": { \"term\": {\"_parent\":\"%1$s#%2$s\"}}}", IndexerUtil.TYPE_FICHE, idMeta);
        indexingCommand.setByQuery(byQuery);
        indexingCommand.setAction(IndexingAction.DELETE);
        return indexingCommand;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteIndexRessource(final Long idResource) {
        final IndexingCommand indexingCommand = createDeleteIndexRessource(idResource);
        this.serviceIndexer.processIndexCommand(indexingCommand);
    }

    /**
     * Générer une commande d'indexation pour le metatag, le type.
     * @param metatag
     * @param type
     * @param bean
     * @return une commande d'indexation à traiter.
     */
    protected IndexingCommand generateIndexingCommand(final MetatagBean metatag, final String type, final T bean) {
        final IndexingCommand indexingCommand = new IndexingCommand();
        indexingCommand.setType(IndexerUtil.TYPE_FICHE);
        final Locale locale = LangueUtil.getLocale(metatag.getMetaLangue());
        indexingCommand.setIndex(IndexerUtil.INDEX_FICHE + IndexerUtil.INDEX_SEPARATEUR + type + IndexerUtil.INDEX_SEPARATEUR + locale.getLanguage());
        indexingCommand.setId(String.valueOf(metatag.getId()));
        indexingCommand.setAction(IndexingAction.CREATE_UPDATE);
        final FicheIndexDocument ficheDocument = new FicheIndexDocument();
        ficheDocument.setTypeFiche(type);
        ficheDocument.setCodeObjet(metatag.getMetaCodeObjet());
        ficheDocument.setFiche(bean);
        // Ajout des plugins (peut être spécifique à chaque projet)
        ficheDocument.setPlugins(this.generatePlugins(metatag, bean));
        // Droits de la fiche
        ficheDocument.setAccessControl(this.serviceAccessControlFiche.genererAccessControl(bean, metatag.getMetaCodeObjet()));
        // Ajout des rubriques + rubriques de publication
        final List<String> codesRubriques = generateRubriques(bean, metatag.getMetaCodeObjet());
        ficheDocument.setRubriques(StringUtils.join(codesRubriques, ";"));
        indexingCommand.setDocument(ficheDocument);
        return indexingCommand;
    }

    protected List<RubriqueBean> getAllRubriques(final String codeRubrique) {
        final List<RubriqueBean> rubriques;
        if (StringUtils.isNotBlank(codeRubrique)) {
            rubriques = serviceRubrique.getAllAscendant(codeRubrique);
            Collections.reverse(rubriques);
            final RubriqueBean rubriqueFicheBean = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (null != rubriqueFicheBean) {
                rubriques.add(rubriqueFicheBean);
            } else {
                LOG.warn("Rubrique de code {} non trouvée", codeRubrique);
            }
        } else {
            rubriques = Collections.emptyList();
        }
        return rubriques;
    }

    protected List<String> generateRubriques(final T bean, final String typeObjet) {
        final Set<RubriqueBean> rubriques = new HashSet<>();
        rubriques.addAll(getAllRubriques(bean.getCodeRubrique()));
        final List<RubriquepublicationBean> rubriquesPublication = serviceRubriquePublication.getByTypeCodeLanguage(typeObjet, bean.getCode(), bean.getLangue());
        for (final RubriquepublicationBean rubriquepublicationBean : rubriquesPublication) {
            rubriques.addAll(getAllRubriques(rubriquepublicationBean.getRubriqueDest()));
        }
        final List<String> codesRubriques = new ArrayList<>(rubriques.size());
        IterableUtils.forEach(rubriques, new Closure<RubriqueBean>() {

            @Override
            public void execute(final RubriqueBean input) {
                codesRubriques.add(input.getCode());
            }
        });
        return codesRubriques;
    }

    /**
     * Générer la liste (ordonnée) des pièces jointes d'une fiche.
     * @param metatagBean
     * @param bean
     * @return la liste de ressources liées
     */
    public List<ResourceAttachmentBean> generateAttachments(final MetatagBean metatagBean, final T bean) {
        final List<ResourceAttachmentBean> attachments = new ArrayList<>();
        final String codeParent = bean.getId() + ",TYPE=" + metatagBean.getMetaCodeObjet() + "%";
        final List<RessourceBean> ressources = this.serviceRessource.getFilesOrderBy(codeParent, "ORDRE");
        for (final RessourceBean ressourceBean : ressources) {
            final ResourceAttachmentBean resourceAttachmentBean = this.generateAttachment(bean, ressourceBean);
            if (null != resourceAttachmentBean) {
                attachments.add(resourceAttachmentBean);
            }
        }
        return attachments;
    }

    /**
     * Générer un bean "attachment", indexé par le moteur ES. Ce bean contient un sous-objet de type media : attachment dont le contenu est indexé par Tika.
     *
     * @param bean
     * @param ressourceBean
     * @return
     */
    protected ResourceAttachmentBean generateAttachment(final T bean, final RessourceBean ressourceBean) {
        ResourceAttachmentBean resourceAttachmentBean = null;
        final MediaBean mediaBean = this.serviceMedia.getById(ressourceBean.getIdMedia());
        if (mediaBean != null && this.isAttachmentIndexable(bean, ressourceBean, mediaBean)) {
            final String pathValue = MediaUtils.getPathAbsolu(mediaBean);
            final Path path = Paths.get(pathValue);
            resourceAttachmentBean = new ResourceAttachmentBean();
            resourceAttachmentBean.setMedia(mediaBean);
            resourceAttachmentBean.setId(ressourceBean.getId());
            if (Files.exists(path)) {
                LOG.info("Fichier à indexer : {}", mediaBean.getSource());
                if (this.isAttachmentContentIndexable(bean, ressourceBean, mediaBean)) {
                    try {
                        final MediaAttachmentBean attachment = new MediaAttachmentBean();
                        attachment.setName(mediaBean.getSource());
                        attachment.setContentType(mediaBean.getFormat());
                        attachment.setContentLength(mediaBean.getPoids());
                        attachment.setContent(IndexerUtil.copyToBytes(path));
                        resourceAttachmentBean.setFile(attachment);
                    } catch (final IOException e) {
                        LOG.error("Impossible d'indexer le fichier {}. Chemin : {}", mediaBean.getSource(), pathValue, e);
                    }
                }
            }
            else {
                LOG.debug("Le fichier {} n'existe pas", mediaBean.getSource());
            }
        }
        return resourceAttachmentBean;
    }

    /**
     * Est-ce que la pièce jointe est indexable ?
     * <p>
     * Par défaut, la pièce jointe est indexable.
     * </p>
     * Cette méthode a vocation à être surchargée par les sous-classes pour gérer un comportement particulier sur chaque fiche.
     * @param ressourceBean
     * @param mediaBean
     * @return
     */
    protected boolean isAttachmentIndexable(final T bean, final RessourceBean ressourceBean, final MediaBean mediaBean) {
        return true;
    }

    /**
     * Est-ce que le contenu de la pièce jointe est indexable ?
     * <p>
     * Par défaut, les contenus sont indexables si le type de fichier (content-type) fait partie d'une liste autorisée.
     * </p>
     * Cette méthode a vocation à être surchargée par les sous-classes pour gérer un comportement particulier sur chaque fiche.
     * @param ressourceBean
     * @param mediaBean
     * @return vrai si le contenu de la ressource est indexable (cas si les content-types ne sont pas déclarés).
     */
    protected boolean isAttachmentContentIndexable(final T bean, final RessourceBean ressourceBean, final MediaBean mediaBean) {
        final String contentTypeRessource = mediaBean.getFormat();
        final String contentTypesProperty = PropertyHelper.getCoreProperty("query.indexation.contenttypes");
        if (StringUtils.isNotEmpty(contentTypesProperty)) {
            final String[] contentTypes = contentTypesProperty.split(",");
            return ArrayUtils.contains(contentTypes, contentTypeRessource);
        }
        // Si la propriété n'est pas déclarée (ou vide), il n'y a pas de limite sur les content types autorisés.
        return true;
    }

    /**
     * Retourne une liste vide.
     * TODO Produit : Implémentation d'une généricité sur les plugins par le produit.
     * @param metatag
     * @param bean
     * @return
     */
    protected Map<String, PluginIndexableBean> generatePlugins(final MetatagBean metatag, final T bean) {
        return new HashMap<>();
    }

    protected final ServiceRessource getServiceRessource() {
        return this.serviceRessource;
    }

    protected final ServiceMedia getServiceMedia() {
        return this.serviceMedia;
    }

    public final void setServiceIndexer(final ServiceIndexer serviceIndexer) {
        this.serviceIndexer = serviceIndexer;
    }

    public final void setServiceAccessControlFiche(final ServiceAccessControlFiche serviceAccessControlFiche) {
        this.serviceAccessControlFiche = serviceAccessControlFiche;
    }

    /**
     * @return the serviceSearcher
     */
    public ServiceSearcher getServiceSearcher() {
        return serviceSearcher;
    }

    /**
     * @param serviceSearcher the serviceSearcher to set
     */
    public void setServiceSearcher(final ServiceSearcher serviceSearcher) {
        this.serviceSearcher = serviceSearcher;
    }

    /**
     * @return the serviceRubriquePublication
     */
    protected ServiceRubriquePublication getServiceRubriquePublication() {
        return serviceRubriquePublication;
    }

    /**
     * @param serviceRubriquePublication the serviceRubriquePublication to set
     */
    public void setServiceRubriquePublication(final ServiceRubriquePublication serviceRubriquePublication) {
        this.serviceRubriquePublication = serviceRubriquePublication;
    }

    /**
     * @return the serviceAccessControlFiche
     */
    protected ServiceAccessControlFiche getServiceAccessControlFiche() {
        return serviceAccessControlFiche;
    }

    public void setServiceRessource(final ServiceRessource serviceRessource) {
        this.serviceRessource = serviceRessource;
    }

    public void setServiceMedia(final ServiceMedia serviceMedia) {
        this.serviceMedia = serviceMedia;
    }

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }
}