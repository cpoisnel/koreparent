/**
 * Fournit l'ensemble des composants pour <b>indexer</b> des fiches et d'autres entités du produit.
 *
 * @author cpoisnel
 */
package com.kosmos.search.index;