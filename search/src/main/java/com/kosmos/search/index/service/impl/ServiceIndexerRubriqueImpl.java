package com.kosmos.search.index.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.kosmos.search.index.service.ServiceIndexation;
import com.univ.objetspartages.bean.RubriqueBean;

/**
 * Service permettant de gérer les actions associées à des modifications de rubriques.
 * @author cpoisnel
 *
 */
public class ServiceIndexerRubriqueImpl implements ServiceIndexation<RubriqueBean> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerRubriqueImpl.class);

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("searchIndexerRubriqueJob")
    private Job job;

    /**
     * Mise à jour de l'ensemble des index d'une rubriques.
     * @param rubriqueBean
     */
    public void saveIndex(final RubriqueBean rubriqueBean) {
        LOG.info("Mise à jour de l'ensemble des données indexées attachées à la rubrique de code '{}'", rubriqueBean);
        final Map<String, JobParameter> jobParametersMap = new HashMap<>();
        jobParametersMap.put("job.id_rubrique", new JobParameter(rubriqueBean.getId()));
        jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
        final JobParameters jobParameters = new JobParameters(jobParametersMap);
        try {
            final JobExecution execution = jobLauncher.run(job, jobParameters);
            final BatchStatus status = execution.getStatus();
            final ExitStatus exitStatus = execution.getExitStatus();
            LOG.info("Fin du job  de réindexation liée à la modification de la rubrique de code '{}' au statut '{}' (exit status : '{}')", rubriqueBean.getCode(), status, exitStatus);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            LOG.error("Erreur à l'exécution du job de réindexation des données liées à la rubrique de code '{}'", rubriqueBean.getCode(), e);
        }
    }


    /**
     * Mise à jour de l'ensemble des index d'une rubriques suite à une suppression.
     * @param idRubrique
     */
    public void deleteIndex(final Long idRubrique) {
        LOG.info("Mise à jour de l'ensemble des données indexées attachées à la rubrique de code '{}'", idRubrique);
        final Map<String, JobParameter> jobParametersMap = new HashMap<>();
        jobParametersMap.put("job.id_rubrique", new JobParameter(idRubrique));
        jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
        final JobParameters jobParameters = new JobParameters(jobParametersMap);
        try {
            final JobExecution execution = jobLauncher.run(job, jobParameters);
            final BatchStatus status = execution.getStatus();
            final ExitStatus exitStatus = execution.getExitStatus();
            LOG.info("Fin du job  de réindexation liée à la suppression de la rubrique d'id '{}' au statut '{}' (exit status : '{}')", idRubrique, status, exitStatus);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            LOG.error("Erreur à l'exécution du job de réindexation des données liées à la rubrique de code '{}'", idRubrique, e);
        }
    }
}
