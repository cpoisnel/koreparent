package com.kosmos.search.index.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.kosmos.search.index.service.ServiceIndexation;
import com.univ.objetspartages.bean.LabelBean;

/**
 * Service permettant de gérer les actions associées à des modifications de labels (ServiceLabel).
 * @author cpoisnel
 *
 */
public class ServiceIndexerLabelImpl implements ServiceIndexation<LabelBean> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerLabelImpl.class);

    @Autowired
    private JobLauncher jobLauncher;

    /**
     * Job de réindexation des fiches liées à des labels.
     */
    @Autowired
    @Qualifier("searchIndexerLabelJob")
    private Job job;

    /**
     * Mise à jour de l'ensemble des index référençant ce label.
     * @param labelBean
     *          Label créé ou mis à jour
     */
    public void saveIndex(final LabelBean labelBean) {
        LOG.info("Mise à jour de l'ensemble des données indexées liées au label {}", labelBean);
        final Map<String, JobParameter> jobParametersMap = new HashMap<>();
        jobParametersMap.put("job.id_label", new JobParameter(labelBean.getId()));
        jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
        final JobParameters jobParameters = new JobParameters(jobParametersMap);
        try {
            final JobExecution execution = jobLauncher.run(job, jobParameters);
            final BatchStatus status = execution.getStatus();
            final ExitStatus exitStatus = execution.getExitStatus();
            LOG.info("Fin du job  de réindexation liée à la modification du label de code '{}' au statut '{}' (exit status : '{}')", labelBean.getCode(), status, exitStatus);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            LOG.error("Erreur à l'exécution du job de réindexation des données liées au label de code '{}'", labelBean.getCode(), e);
        }
    }

    /**
     * Mise à jour de l'ensemble des index référençant ce label supprimé.
     * @param idLabel
     * ID du label supprimé
     */
    public void deleteIndex(final Long idLabel) {
        LOG.info("Mise à jour de l'ensemble des données indexées liées au label d'id '{}'", idLabel);
        final Map<String, JobParameter> jobParametersMap = new HashMap<>();
        jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
        jobParametersMap.put("job.id_label", new JobParameter(idLabel));
        final JobParameters jobParameters = new JobParameters(jobParametersMap);
        try {
            final JobExecution execution = jobLauncher.run(job, jobParameters);
            final BatchStatus status = execution.getStatus();
            final ExitStatus exitStatus = execution.getExitStatus();
            LOG.info("Fin du job  de réindexation liée à la suppression du label d'id '{}' au statut '{}' (exit status : '{}')", idLabel, status, exitStatus);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            LOG.error("Erreur à l'exécution du job de réindexation des données liées au label d'id '{}'", idLabel, e);
        }
    }
}
