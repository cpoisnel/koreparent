package com.kosmos.search.index.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.univ.objetspartages.bean.MediaBean;

/**
 *
 * Attachment Bean.
 *
 * @author cpoisnel
 *
 */
public class ResourceAttachmentBean implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 5897110637944958525L;

    @JsonIgnore
    private Long id;

    private MediaAttachmentBean file;

    private MediaBean media;

    public final Long getId() {
        return id;
    }

    public final void setId(final Long id) {
        this.id = id;
    }

    public final MediaAttachmentBean getFile() {
        return file;
    }

    public final void setFile(final MediaAttachmentBean file) {
        this.file = file;
    }

    public MediaBean getMedia() {
        return media;
    }

    public void setMedia(final MediaBean media) {
        this.media = media;
    }
}
