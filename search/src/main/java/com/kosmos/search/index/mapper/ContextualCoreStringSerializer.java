package com.kosmos.search.index.mapper;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

/**
 * Serialisation des chaines spécifiques (labels / toolbox / rubriques / ...).
 * <p>
 *     Mise en place du pattern delegate avec l'utilisation de l'attribut {@link #contextSerializers} pour l'initialisation des contextes.
 * </p>
 * @author cpoisnel
 *
 */
public class ContextualCoreStringSerializer extends StdSerializer<String> implements ContextualSerializer {

    private static final long serialVersionUID = -6561754957235576202L;

    private static final Logger LOG = LoggerFactory.getLogger(ContextualCoreStringSerializer.class);

    /**
     * Liste des contextes de sérialisation. Si cette liste est vide, la sérialisation par défaut ({@link ToStringSerializer}) est utilisée.
     */
    private List<ContextualSerializer> contextSerializers;

    protected ContextualCoreStringSerializer(final Class<String> t) {
        super(t);
    }

    protected ContextualCoreStringSerializer(final JavaType type) {
        super(type);
    }

    protected ContextualCoreStringSerializer(final Class<?> t, final boolean dummy) {
        super(t, dummy);
    }

    protected ContextualCoreStringSerializer(final StdSerializer<?> src) {
        super(src);
    }

    /**
     * <p>
     *     N'est pas sensé être appelé directement, car on a renvoyé une instance spécifique lors de l'initalisation ({@link #createContextual(SerializerProvider, BeanProperty)}).
     * </p>
     * {@inheritDoc}
     */
    @Override
    public void serialize(final String code, final JsonGenerator jgen, final SerializerProvider provider) throws IOException {
        ToStringSerializer.instance.serialize(code, jgen, provider);
    }

    /**
     * <p>
     *     Parcours de l'ensemble des {@link #contextSerializers} définis.
     *     Si cette liste est vide, on renvoie la sérialisation par défaut des chaînes ({@link ToStringSerializer}).
     *
     *
     * </p>
     *
     * {@inheritDoc}
     *
     */
    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider paramSerializerProvider, final BeanProperty paramBeanProperty) throws JsonMappingException {
        JsonSerializer<?> serializer = null;
        if (CollectionUtils.isNotEmpty(contextSerializers)) {
            final Iterator<ContextualSerializer> iterator = contextSerializers.iterator();
            while (null == serializer && iterator.hasNext()) {
                if (null != paramBeanProperty) {
                    serializer = iterator.next().createContextual(paramSerializerProvider, paramBeanProperty);
                    LOG.debug("Création du contexte de sérialisation pour le paramètre {}", paramBeanProperty.getName());
                    // Ordre par défaut (défini dans la configuration XML)
                    // Annotation @Label
                    // Annotation @GetterAnnotation
                    // Annotation @Rubrique
                    // Annotation @User
                    // Annotation @Fiche
                }
            }
        }
        if (serializer == null) {
            // Sérializer par défaut des chaines
            serializer = ToStringSerializer.instance;
        }
        return serializer;
    }

    public List<ContextualSerializer> getContextSerializers() {
        return contextSerializers;
    }

    public void setContextSerializers(final List<ContextualSerializer> contextSerializers) {
        this.contextSerializers = contextSerializers;
    }
}