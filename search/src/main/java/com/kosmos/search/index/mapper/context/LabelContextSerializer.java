package com.kosmos.search.index.mapper.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.kosmos.search.index.mapper.serializer.DatLabelSerializer;
import com.kosmos.search.index.mapper.serializer.ServiceLabelSerializer;
import com.kportal.cms.objetspartages.annotation.Label;
import com.kportal.cms.objetspartages.annotation.strategy.LabelStrategy;
import com.kportal.cms.objetspartages.annotation.strategy.ServiceLabelStrategy;

/**
 * Contexte de sérialisation des labels, nécessaire pour faire les traitements spécifiques en fonction des annotations.
 * @author cpoisnel
 */
public class LabelContextSerializer implements ContextualSerializer {

    private static final Logger LOG = LoggerFactory.getLogger(LabelContextSerializer.class);

    /**
     * <p>
     *     Renvoie une instance de {@link DatLabelSerializer} ou {@link ServiceLabelSerializer} si l'annotation @{@link Label} est positionnée sur la propriété, sinon null.
     * </p>
     * {@inheritDoc}
     */
    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider paramSerializerProvider, final BeanProperty paramBeanProperty) throws JsonMappingException {
        // On cherche les annotations (une seule autorisée à chaque fois)
        final String nomParam = paramBeanProperty.getName();
        // Annotation @Label
        Label ann = paramBeanProperty.getAnnotation(Label.class);
        if (ann == null) {
            ann = paramBeanProperty.getContextAnnotation(Label.class);
        }
        if (ann != null) {
            final String type = ann.type();
            final Class<? extends LabelStrategy> labelStrategy = ann.strategy();
            StdSerializer<String> labelSerializer = null;
            if (ServiceLabelStrategy.class.isAssignableFrom(labelStrategy)) {
                LOG.debug("Utilisation de la sérialisation ServiceLabel pour l'attribut '{}' (type '{}')", nomParam, type);
                labelSerializer = new ServiceLabelSerializer(type);
            } else if (DatLabelSerializer.class.isAssignableFrom(labelStrategy)) {
                LOG.debug("Utilisation de la sérialisation .dat pour l'attribut '{}' (type '{}')", nomParam, type);
                labelSerializer = new DatLabelSerializer(type, ann.contexte());
            }
            return labelSerializer;
        }
        return null;
    }
}
