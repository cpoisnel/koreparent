package com.kosmos.search.index.bean;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Plugin indexé (wrapper).
 * @author cpoisnel
 *
 */
public class PluginIndexableBean implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -3509563649295065194L;

    /**
     * Type de plugin.
     */
    @JsonIgnore
    private String type;

    /**
     * Items de chaque type de plugins.
     */
    @JsonUnwrapped
    private List<? extends Serializable> items;

    public final String getType() {
        return type;
    }

    public final void setType(final String type) {
        this.type = type;
    }

    public final List<? extends Serializable> getItems() {
        return items;
    }

    public final void setItems(final List<? extends Serializable> items) {
        this.items = items;
    }

}
