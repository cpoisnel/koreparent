package com.kosmos.search.index.mapper.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.kosmos.search.index.mapper.serializer.RubriqueSerializer;
import com.kportal.cms.objetspartages.annotation.Rubrique;

/**
 * Contexte de sérialisation pour les rubriques, nécessaire pour faire les traitements spécifiques en fonction des annotations.
 * @author cpoisnel
 */
public class RubriqueContextSerializer implements ContextualSerializer {

    private static final Logger LOG = LoggerFactory.getLogger(RubriqueContextSerializer.class);

    /**
     * <p>
     *     Renvoie l'instance de {@link RubriqueSerializer} si l'annotation @{@link Rubrique} est positionnée sur la propriété, sinon null.
     * </p>
     * {@inheritDoc}
     */
    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider prov, final BeanProperty paramBeanProperty) throws JsonMappingException {
        Rubrique rubriqueAnnotation = paramBeanProperty.getAnnotation(Rubrique.class);
        if (rubriqueAnnotation == null) {
            rubriqueAnnotation = paramBeanProperty.getContextAnnotation(Rubrique.class);
        }
        if (rubriqueAnnotation != null) {
            LOG.debug("Utilisation de la sérialisation Rubrique pour l'attribut '{}'", paramBeanProperty.getName());
            return RubriqueSerializer.instance;
        }
        return null;
    }
}
