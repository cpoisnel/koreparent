/**
 * Le package "mapper" est dédié à l'enrichissement des beans Java lors de la sérialisation en JSON.
 * Cela repose sur les fonctionnalités Jackson de <a href="http://wiki.fasterxml.com/JacksonHowToCustomSerializers">Custom Serializer</a>
 *
 *
 * <h2>Fonctionnement</h2>
 *
 * Le mapper {@link com.kosmos.search.index.mapper.CoreSearchMapper} est utilisé pour sérialiser les données lors de l'indexation (soit une indexation unitaire, soit une indexation en masse par batch).
 *
 *
 * <h3>Mapper</h3>
 * Le mapper a pour objectif d'orchestrer la sérialisation (et désérialisation, même si non traitée), gérer les types de beans,...
 * Le mapper {@link com.kosmos.search.index.mapper.CoreSearchMapper} a un attribut {@link com.kosmos.search.index.mapper.CoreSearchMapper#modules} qui sont initialisés après
 * l'initialisation de la factory Spring (en {@link javax.annotation.PostConstruct})
 * <ol>
 *     <li>Un module simple "coreModule" est initialisé, mais il est possible d'en ajouter de nouveaux</li>
 *     <li>Dans ce module, on défini un {@link com.fasterxml.jackson.databind.ser.BeanSerializerModifier} qui a pour objectif d'ajouter dans le contexte de sérialisation Jackson, la langue de la fiche, par interception (modification du Serializer)</li>
 *     <li>Dans ce module, on défini un {@link com.fasterxml.jackson.databind.ser.std.StdSerializer qui a pour objectif de traiter tous les attributs de type {@link java.lang.String}/li>
 * </ol>
 *
 * Il est possible de définir d'autres modules (liés à d'autres sérializers) en modifiant la liste.
 *
 * <pre>
 * &lt;bean id=&quot;coreSearchMapper&quot; class=&quot;com.kosmos.search.index.mapper.CoreSearchMapper&quot;&gt;
 *  &lt;property name=&quot;modules&quot;&gt;
 *      &lt;list&gt;
 *          &lt;ref bean=&quot;coreModule&quot;/&gt;
 *      &lt;/list&gt;
 *  &lt;/property&gt;
 * &lt;/bean&gt;
 * </pre>
 *
 * <h3>Modules</h3>
 * <p>
 * Les modules sont configurés avec un modificateur de la sérialisation (intercepte l'action de sérialisation) et une gestion de la sérialisation des types simples (type eraser, ne traite pas les collections).
 * Il est également possible de définir des désérialiseurs dans ce module (non traité pour la recherche).
 * </p>
 *
 * <pre>
 * &lt;bean id=&quot;coreModule&quot; class=&quot;com.fasterxml.jackson.databind.module.SimpleModule&quot;&gt;
 *  &lt;constructor-arg type=&quot;java.lang.String&quot; value=&quot;CoreModule&quot;/&gt;
 *  &lt;property name=&quot;serializerModifier&quot;&gt;
 *      &lt;ref bean=&quot;ficheBeanSerializerModifier&quot;/&gt;
 *  &lt;/property&gt;
 *  &lt;property name=&quot;serializers&quot;&gt;
 *      &lt;ref bean=&quot;simpleContextualCoreStringSerializer&quot;/&gt;
 *  &lt;/property&gt;
 * &lt;/bean&gt;
 * </pre>
 *
 * <h3>Serializers</h3>
 * <p>
 *     La sérialisation est proposée est celui des types String (il serait possible de définir pour d'autres types d'attributs des comportements de sérialisations).
 *     Le bean "contextualCoreStringSerializer" (fonctionne pour des {@link java.lang.String}) est paramétré avec des configurations de Serialiseurs. La détection du serialiseur
 *     pour un attribut (appartenant à une classe précise) n'est fait qu'une et une seule fois pendant la durée de vie de la JVM (cache des Serialiseurs pour chaque bean).
 * </p>
 * <p>
 * <pre>
 * &lt;bean id=&quot;simpleContextualCoreStringSerializer&quot; class=&quot;com.fasterxml.jackson.databind.module.SimpleSerializers&quot;&gt;
 *  &lt;constructor-arg&gt;
 *      &lt;list&gt;
 *          &lt;ref bean=&quot;contextualCoreStringSerializer&quot;/&gt;
 *      &lt;/list&gt;
 *  &lt;/constructor-arg&gt;
 * &lt;/bean&gt;
 *  &lt;bean id=&quot;contextualCoreStringSerializer&quot; class=&quot;com.kosmos.search.index.mapper.ContextualCoreStringSerializer&quot;&gt;
 *       &lt;constructor-arg type=&quot;java.lang.Class&quot;&gt;
 *           &lt;value&gt;java.lang.String&lt;/value&gt;
 *       &lt;/constructor-arg&gt;
 *       &lt;property name=&quot;contextSerializers&quot;&gt;
 *           &lt;list&gt;
 *               &lt;ref bean=&quot;ficheContextSerializer&quot;/&gt;
 *               &lt;ref bean=&quot;labelContextSerializer&quot;/&gt;
 *               &lt;ref bean=&quot;rubriqueContextSerializer&quot;/&gt;
 *               &lt;ref bean=&quot;toolboxContextSerializer&quot;/&gt;
 *               &lt;ref bean=&quot;userContextSerializer&quot;/&gt;
 *           &lt;/list&gt;
 *       &lt;/property&gt;
 *   &lt;/bean&gt;
 *  </pre>
 *
 *  <p>
 *   <b>Note : </b>L'ajout d'un contexte de sérialisation et d'une sérialisation peut facilement être implémentée en référençant une classe implémentant {@link com.fasterxml.jackson.databind.ser.ContextualSerializer} et une classe gérant la sérialisation, héritant de {@link com.fasterxml.jackson.databind.ser.std.StdSerializer}.
 *  </p>
 *
 * <h2>Exemple</h2>
 * <p>
 * Cela permet par exemple, à partir d'un attribut code Libellé de type {@link java.lang.String}
 * de pouvoir avoir en JSON la valeur de ce libellé : *
 *
 *      Avec un mapper par défaut sans modules :
 *  <pre>
 *  &#123;
 *      "thematique" : "1234"
 *      &#125;
 *  </pre>
 *
 *  Avec un Mapper (héritant de) {@link com.kosmos.search.index.mapper.CoreSearchMapper} : paramétré avec la sérialisation de labels ({@link com.kosmos.search.index.mapper.serializer.ServiceLabelSerializer}), l'attribut :
 *
 *  <pre>
 *  &#x40;Label(type="04")
 *  private String thematique;
 *  </pre>
 *
 *  sera sérialisé en JSON :
 *
 *  <pre>
 *      &#123;
 *      "thematique" : &#123;
 *              "label_code":"1234",
 *              "label_id":50,
 *              "label_value":"Thématique A"
 *          &#125;
 *      &#125;
 *
 *  </pre>
 * </p>
 *
 *
 * @author cpoisnel
 */
package com.kosmos.search.index.mapper;