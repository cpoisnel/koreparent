package com.kosmos.search.index.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.kosmos.search.index.service.ServiceIndexation;
import com.univ.objetspartages.bean.UtilisateurBean;

/**
 * Services d'indexation liées à une modification d'utilisateur.
 */
public class ServiceIndexerUserImpl implements ServiceIndexation<UtilisateurBean> {

	/**
	 * The Constant LOG.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerUserImpl.class);

	/**
	 * The jobLauncher.
	 */
	@Autowired
	private JobLauncher jobLauncher;

	/**
	 * The job.
	 */
	@Autowired
	@Qualifier("searchIndexerUserJob")
	private Job job;

	/**
	 * Mise à jour de l'ensemble des index d'un utilisateur.
	 *
	 * @param user
	 *            UtilisateurBean - L'utilisateur.
	 */
	public void saveIndex(final UtilisateurBean user) {
		LOG.info("Mise à jour de l'ensemble des données indexées rattachées à l'utilisateur (code '{}')", user.getCode());
		final Map<String, JobParameter> jobParametersMap = new HashMap<>();
		jobParametersMap.put("job.id_user", new JobParameter(user.getId()));
		jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
		final JobParameters jobParameters = new JobParameters(jobParametersMap);
		try {
			final JobExecution execution = this.jobLauncher.run(this.job, jobParameters);
			final ExitStatus exitStatus = execution.getExitStatus();
			final BatchStatus status = execution.getStatus();
			LOG.info("Fin du job  de réindexation liée à la modification de l'utilisateur (code '{}') au statut '{}' (exit status : '{}')", user.getCode(), status, exitStatus);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
			LOG.error("Erreur à l'exécution du job de réindexation des données liées à l'utilisateur (code '{}')", user.getCode(), e);
		}
	}

	/**
	 * Mise à jour de l'ensemble des index d'un utilisateur suite à une suppression.
	 *
	 * @param userId
	 *            Long - L'identifiant de l'utilisateur.
	 */
	public void deleteIndex(final Long userId) {
		LOG.info("Mise à jour de l'ensemble des données indexées rattachées à l'utilisateur (id '{}')", userId);
		final Map<String, JobParameter> jobParametersMap = new HashMap<>();
		jobParametersMap.put("job.id_user", new JobParameter(userId));
		jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
		final JobParameters jobParameters = new JobParameters(jobParametersMap);
		try {
			final JobExecution execution = this.jobLauncher.run(this.job, jobParameters);
			final ExitStatus exitStatus = execution.getExitStatus();
			final BatchStatus status = execution.getStatus();
			LOG.info("Fin du job  de réindexation liée à la suppression de l'utilisateur (id '{}') au statut '{}' (exit status : '{}')", userId, status, exitStatus);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
			LOG.error("Erreur à l'exécution du job de réindexation des données liées à l'utilisateur (id '{}')", userId, e);
		}
	}
}
