package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.jsbsoft.jtf.core.CodeLibelle;
import com.jsbsoft.jtf.core.LangueUtil;

/**
 * Sérialiseur de labels (.dat). Enrichit la donnée métier en Json.
 * @author cpoisnel
 *
 */
public class DatLabelSerializer extends AbstractLabelSerializer<String> {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -7397292142661428648L;

    private static final Logger LOG = LoggerFactory.getLogger(DatLabelSerializer.class);

    private final String type;

    private final String idContexte;

    /**
     * Constrcteur, définition du type et du contexte Spring utilisé.
     * @param type
     * Type de label
     * @param idContexte
     * Contexte Spring où les .dat sont référencés
     */
    public DatLabelSerializer(final String type, final String idContexte) {
        super(String.class);
        this.type = type;
        this.idContexte = idContexte;
    }

    /**
     * Sérialise les codes de label en enrichissant les données avec leurs traductions.
     * @param codesSerialise
     * Codes de label
     * @param jgen
     * Générateur de flux Json
     * @param provider
     * Provider de la sérialisation Jackson
     * @throws IOException si erreur lors de l'écriture sur le générateur json
     */
    @Override
    public void serialize(final String codesSerialise, final JsonGenerator jgen, final SerializerProvider provider) throws IOException {

        if (StringUtils.isNotEmpty(codesSerialise)) {
            jgen.writeStartArray();
            final String[] codes = codesSerialise.split(";");
            for (final String code : codes) {
                final String codeLabel;
                if ("".equals(code) || "0000".equals(code)) {
                    LOG.debug("Code label par défaut '{}', le code 'null' est conservé", code);
                    codeLabel = null;
                } else {
                    codeLabel = code;
                }
                jgen.writeStartObject();
                jgen.writeStringField(LABEL_CODE, codeLabel);

				final String langue = processLanguage(jgen, provider);
				final Locale locale = LangueUtil.getLocale(langue);
                String libelleLabel = null;
                if (codeLabel != null) {
                    final String labelDat = CodeLibelle.lireLibelle(idContexte, type, locale, code);
                    if (StringUtils.isNotBlank(labelDat)) {
                        libelleLabel = labelDat;
                    } else {
                        LOG.info("Label non trouvé pour le type {}, code {}, langue {}. Le code est conservé.", type, codeLabel, "0");
                    }
                }
                jgen.writeStringField(LABEL_VALUE, libelleLabel);
                jgen.writeEndObject();
            }
            jgen.writeEndArray();
        } else {
            jgen.writeNull();
        }
    }

}
