package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.kosmos.search.util.IndexerUtil;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.PersistenceBean;
import com.univ.objetspartages.om.AbstractFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.FicheUnivHelper;

/**
 * The Class FicheSerializer.
 */
public class FicheSerializer extends AbstractLanguageSerializer<String> {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -6590254568954269002L;

	/**
	 * The Constant FICHE_ID_METATAG.
	 */
	private static final String FICHE_ID_METATAG = "fiche_id_metatag";

	/**
	 * The Constant FICHE_VALUE.
	 */
	private static final String FICHE_VALUE = "fiche_value";

	/**
	 * The Constant LOG.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(FicheSerializer.class);

	/**
	 * The type.
	 */
	private final String type;

	/**
	 * Constructeur.
	 * @param type
	 * Type de fiche paramétré
	 */
	public FicheSerializer(final String type) {
		super(String.class);
		this.type = type;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void serialize(final String codes, final JsonGenerator jsonGen, final SerializerProvider provider) throws IOException {
		if (StringUtils.isBlank(codes)) {
			jsonGen.writeNull();
			return;
		}
		jsonGen.writeStartArray();
		for (final String frg : StringUtils.split(codes, ';')) {
			// extraction des données contenues dans le fragment de codes courant
			final Triple<String, String, String> datas = IndexerUtil.getRessourceCodeLangueType(frg);
			final String type = this.getType(datas);
			// récupération de la fiche associée
			final FicheUniv fiche = this.getFiche(datas, type, jsonGen, provider);
			if (null == fiche) {
				LOG.warn("La fiche recherchée est introuvable avec les données suivantes (code='{}',type='{}')", frg, type);
				continue;
			}
			jsonGen.writeStartObject();
			// extraction des données metatag
			final MetatagBean metatag = MetatagUtils.lireMeta(fiche);
			if (null == metatag) {
				LOG.warn("Le metatag recherché est introuvable avec les données suivantes (code='{}',type='{}')", fiche.getCode(), type);
				continue;
			}
			jsonGen.writeObjectField(FICHE_ID_METATAG, metatag.getId());
			// extraction des données de fiche
			final PersistenceBean bean = ((AbstractFiche<?>) fiche).getPersistenceBean();
			jsonGen.writeObjectField(FICHE_VALUE, bean);
			jsonGen.writeEndObject();
		}
		jsonGen.writeEndArray();
	}

	/**
	 * {@link FicheSerializer#serialize(String, JsonGenerator, SerializerProvider)}.
	 */
	private FicheUniv getFiche(final Triple<String, String, String> datas, final String type, final JsonGenerator jsonGen, final SerializerProvider provider) {
		if (null == datas || StringUtils.isBlank(type)) {
			return null;
		}
		try {
			// récupération de la langue contextuelle si vide
			String langue = datas.getMiddle();
			if (StringUtils.isBlank(langue)) {
				langue = super.processLanguage(jsonGen, provider);
			}
			// récupération du code (non nul)
			final String code = datas.getLeft();
			return FicheUnivHelper.getFiche(type, code, langue);
		} catch (final Exception e) {
			LOG.error("Exception lors de la lecture de la fiche '{}'", datas, e);
		}
		return null;
	}

	/**
	 * {@link FicheSerializer#serialize(String, JsonGenerator, SerializerProvider)}.
	 */
	private String getType(final Triple<String, String, String> datas) {
		if (null == datas) {
			return StringUtils.EMPTY;
		}
		String type = datas.getRight();
		if (StringUtils.isBlank(type)) {
			type = this.type;
		}
		return type;
	}
}
