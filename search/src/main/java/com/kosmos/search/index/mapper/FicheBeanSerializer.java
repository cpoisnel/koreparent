package com.kosmos.search.index.mapper;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.univ.objetspartages.bean.AbstractFicheBean;

/**
 * Sérialisation des AbstractFicheBean. Permet de définir la langue de la fiche et la sérialisation éventuelle de libellés (selon la bonne langue).
 * <p>
 * Toutes les sérialisations (pour l'indexation) spécifiques des objets metiers *Bean <b>doivent</b> hériter de cette classe, pour pouvoir gérer le contexte de langue.
 * </p>
 *
 * @author cpoisnel
 *
 */
public class FicheBeanSerializer extends JsonSerializer<AbstractFicheBean> implements ContextualSerializer {

    private static final Logger LOG = LoggerFactory.getLogger(FicheBeanSerializer.class);

    private final JsonSerializer<AbstractFicheBean> defaultSerializer;

    /**
     * Construit le serializer spécifique avec un sérialiseur délégué.
     * @param defaultSerializer
     * Serializer délégué
     *
     */
    public FicheBeanSerializer(final JsonSerializer<AbstractFicheBean> defaultSerializer) {
        this.defaultSerializer = defaultSerializer;
    }

    /**
     * <p>Pas d'action par défaut sur la création du contexte, on renvoie uniquement l'instance (sans action particulière)</p>
     * {@inheritDoc}
     */
    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider prov, final BeanProperty property) throws JsonMappingException {
        LOG.debug("Initialisation du contexte de sérialisation '{}'", FicheBeanSerializer.class);
        return this;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void serialize(final AbstractFicheBean value, final JsonGenerator gen, final SerializerProvider serializers) throws IOException, JsonProcessingException {
        // Au moment de la sérialisation, on ajoute la langue dans le contexte (utile pour les libellés par exemple).
        initContext(value, gen, serializers);
        defaultSerializer.serialize(value, gen, serializers);
    }

    /**
     * Initialisation du contexte de sérialisation du bean.
     * <p>Au moment de la sérialisation, s'il y a une langue définie sur la fiche, on va l'ajouter dans le contexte du {@link SerializerProvider}</p>
     * @param value
     *          Bean métier.
     * @param gen
     *          Générateur de flux Json
     * @param serializers
     *          Serializer à utiliser
     */
    protected void initContext(final AbstractFicheBean value, final JsonGenerator gen, final SerializerProvider serializers) {
        serializers.setAttribute(ContextAttributeSerializerUtils.CONTEXT_LANGUAGE, value.getLangue());
    }

}
