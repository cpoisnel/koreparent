package com.kosmos.search.index.mapper;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;
import com.univ.objetspartages.bean.AbstractFicheBean;

/**
 * Modification du Sérialiseur des fiches {@link AbstractFicheBean}.
 * @author cpoisnel
 *
 */
public class FicheBeanSerializerModifier extends BeanSerializerModifier {

    /**
     * <p>
     * Modification de la sérialisation s'il s'agit d'un bean {@link AbstractFicheBean}.
     * </p>
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public JsonSerializer<?> modifySerializer(final SerializationConfig config, final BeanDescription beanDesc, final JsonSerializer<?> serializer) {
        if (AbstractFicheBean.class.isAssignableFrom(beanDesc.getBeanClass())) {
            // On va utiliser le contexte de sérialisation pour tous les objets. On prend en compte les autres sérializers déjà définis (via annotations...)
            return new FicheBeanSerializer((JsonSerializer<AbstractFicheBean>) serializer);
        }
        return serializer;
    }
}
