package com.kosmos.search.index.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.kosmos.search.index.bean.IndexableFiche;
import com.kosmos.search.index.service.ServiceCommandIndexation;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.kosmos.search.index.bean.IndexingCommand;
import com.kosmos.search.index.service.ServiceIndexation;
import com.kosmos.search.index.service.fiche.ServiceIndexerFicheFactory;
import com.univ.objetspartages.services.ServiceMetatag;

/**
 * Service d'indexation des ressources.
 * @author cpoisnel
 *
 */
public class ServiceIndexerIndexerRessource extends AbstractServiceIndexerCore<RessourceBean> implements ServiceIndexation<RessourceBean>, ServiceCommandIndexation<RessourceBean> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerIndexerRessource.class);

    @Autowired
    private ServiceIndexerFicheFactory serviceIndexerFicheFactory;

    @Autowired
    private ServiceMetatag serviceMetatag;

    /**
     *  {@inheritDoc}
     */
    @Override
    public void saveIndex(final RessourceBean bean) {
        final IndexableFiche indexableFiche = this.fetchIndexableFiche(bean);

        if (indexableFiche != null) {
            MetatagBean metatag = serviceMetatag.getByCodeAndIdFiche(indexableFiche.getCodeObjet(),indexableFiche.getFiche().getId());
            this.serviceIndexerFicheFactory.getServiceIndexerFiche(indexableFiche.getFiche().getClass()).saveIndexRessource(bean, indexableFiche.getType(), indexableFiche.getFiche(),metatag.getId());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteIndex(final Long id) {
        LOG.info("Suppression des données liées à la ressource : {}. Le service d'indexation par défaut est utilisé.", id);
        this.serviceIndexerFicheFactory.getServiceIndexerFiche(null).deleteIndexRessource(id);
    }

    /**
     *  {@inheritDoc}
     */
    @Override
    protected String getCodeObjet(final RessourceBean bean) {
        final String codeParent = bean.getCodeParent();
        final String[] codes = codeParent.split(",");
        if (codes.length == 3) {
            final String[] type = codes[1].split("=");
            if (type.length == 2) {
                return type[1];
            }
        }
        LOG.warn("Impossible de déterminer le type de la fiche {}", bean);
        return null;
    }

    /**
     *  {@inheritDoc}
     */
    @Override
    protected Long getIdFiche(final RessourceBean bean) {
        final String codeParent = bean.getCodeParent();
        final String[] codes = codeParent.split(",");
        if (codes.length == 3) {
            try {
                return Long.valueOf(codes[0]);
            } catch (final NumberFormatException e) {
                LOG.warn("Erreur lors de la lecture de l'id de la fiche : {}", codes[0]);
            }
        }
        LOG.warn("Impossible de déterminer l'id de la fiche {}", bean);
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexingCommand createIndexingCommand(final RessourceBean bean) {
        LOG.debug("Ressource à indexer : {}", bean);
        IndexingCommand indexingCommand = null;
        final IndexableFiche indexableFiche = this.fetchIndexableFiche(bean);

        if (indexableFiche != null) {
            MetatagBean metatag = serviceMetatag.getByCodeAndIdFiche(indexableFiche.getCodeObjet(),indexableFiche.getFiche().getId());
            indexingCommand = this.serviceIndexerFicheFactory.getServiceIndexerFiche(indexableFiche.getFiche().getClass()).createSaveIndexCommand(bean, indexableFiche.getType(), indexableFiche.getFiche(),metatag.getId());
        } else {
            LOG.debug("Pas de fiche indexable pour la ressource {}", bean);
        }
        return indexingCommand;
    }

    @Override
    public IndexingCommand createDeletingCommand(final Long idMetatag) {
        return this.serviceIndexerFicheFactory.getServiceIndexerFiche(null).createDeleteIndexRessourceByIdMeta(idMetatag);
    }
}
