package com.kosmos.search.index.service;

import com.kosmos.search.index.bean.IndexingCommand;

/**
 * Interface des services d'indexation métiers pour ElasticSearch. Les services implémentant cette interface peuvent préparer des
 * commandes d'indexation qui peuvent être utilisées plus tard.
 * @param <T>
 *    Bean manipulé par les services d'indexation
 */
public interface ServiceCommandIndexation<T> {


    /**
     * Créer une commande d'indexation pour un bean.
     * @param bean
     * Bean manipulé
     * @return une commande d'indexation (Serializable) qui peut être traitée plus tard
     */
    IndexingCommand createIndexingCommand(T bean);

    /**
     * Créer une commande de suppression d'index Elasticsearch
     * @param id : Id d'un bean
     * @return une commande d'indexation (Serializable) qui peut être traitée plus tard
     */
    IndexingCommand createDeletingCommand(Long id);
}
