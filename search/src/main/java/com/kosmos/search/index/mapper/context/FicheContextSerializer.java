package com.kosmos.search.index.mapper.context;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.kosmos.search.index.mapper.serializer.FicheSerializer;
import com.kportal.cms.objetspartages.annotation.Fiche;
import com.kportal.core.config.PropertyHelper;

/**
 * Contexte de sérialisation des fiches.
 *
 * @author cpoisnel
 */
public class FicheContextSerializer implements ContextualSerializer {

    private static final Logger LOG = LoggerFactory.getLogger(FicheContextSerializer.class);

    /**
     * <p>
     *     Renvoie une instance de {@link FicheSerializer} si l'annotation @{@link Fiche} est positionnée sur la propriété, sinon null.
     * </p>
     * {@inheritDoc}
     */
    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider paramSerializerProvider, final BeanProperty paramBeanProperty) throws JsonMappingException {
        final String nomParam = paramBeanProperty.getName();
        Fiche ficheAnnotation = paramBeanProperty.getAnnotation(Fiche.class);
        if (null == ficheAnnotation) {
            ficheAnnotation = paramBeanProperty.getContextAnnotation(Fiche.class);
        }
        if (null != ficheAnnotation) {
            LOG.debug("Utilisation de la sérialisation Fiche pour l'attribut '{}'", nomParam);
            final String contexte = ficheAnnotation.contexte();
            // Gestion de la surcharge si définie dans la configuration
            final String simpleClass = paramBeanProperty.getMember().getDeclaringClass().getSimpleName();
            final String key = StringUtils.join(contexte, ".", simpleClass, ".", nomParam, ".", "nomObjet");
            String type = PropertyHelper.getProperty(contexte, key);
            if (StringUtils.isBlank(type)) {
                type = ficheAnnotation.nomObjet();
            } else {
                LOG.info("Surcharge du paramètre 'nomObjet' pour l'attribut '{}' ({} --> {})", nomParam, ficheAnnotation.nomObjet(), type);
            }
            return new FicheSerializer(type);
        }
        return null;
    }
}
