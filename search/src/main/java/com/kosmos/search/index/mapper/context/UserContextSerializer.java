package com.kosmos.search.index.mapper.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.kosmos.search.index.mapper.serializer.UserSerializer;
import com.kportal.cms.objetspartages.annotation.User;

/**
 * Contexte de sérialisation pour les utilisateurs, nécessaire pour faire les traitements spécifiques en fonction des annotations.
 * @author cpoisnel
 */
public class UserContextSerializer implements ContextualSerializer {

    private static final Logger LOG = LoggerFactory.getLogger(UserContextSerializer.class);

    /**
     * <p>
     *     Renvoie l'instance de {@link UserSerializer} si l'annotation @{@link User} est positionnée sur la propriété, sinon null.
     * </p>
     * {@inheritDoc}
     */
    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider paramSerializerProvider, final BeanProperty paramBeanProperty) throws JsonMappingException {
        // Annotation @User
        User userAnnotation = paramBeanProperty.getAnnotation(User.class);
        if (null == userAnnotation) {
            userAnnotation = paramBeanProperty.getContextAnnotation(User.class);
        }
        if (null != userAnnotation) {
            LOG.debug("Utilisation de la sérialisation User pour l'attribut '{}'", paramBeanProperty.getName());
            return UserSerializer.instance;
        }
        return null;
    }
}
