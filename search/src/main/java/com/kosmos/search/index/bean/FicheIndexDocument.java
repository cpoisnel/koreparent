package com.kosmos.search.index.bean;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kosmos.search.util.IndexerUtil;
import com.kportal.cms.objetspartages.annotation.Rubrique;

/**
 * Wrapper d'un document indexé avec
 * <ul>
 * <li>La fiche</li>
 * <li>Les plugins</li>
 * <li>Les pièces jointes</li>
 * <li>Les restrictions d'accès</li>
 * </ul>
 * @author cpoisnel
 *
 */
public class FicheIndexDocument implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -3185464839940684819L;

    @JsonProperty("code_objet")
    private String codeObjet;

    /**
     * Type de la fiche (est présent également dans l'index)
     */
    @JsonProperty("type_fiche")
    private String typeFiche;

    /**
     * Fiche. L'ensemble des attributs est remis au premier niveau. On considère qu'il n'y a pas de collisions possibles (si collision, elles doivent porter le même sens fonctionnel et donc la même valeur !)
     * <p>
     *
     * </p>
     */
    @JsonProperty("fiche")
    private Serializable fiche;

    /**
     * Tous les codes rubriques (courante et parente) de la fiche.
     */
    @JsonProperty("rubriques")
    @Rubrique
    private String rubriques;

    /**
     * Plugins de fiche associés à la fiche.
     */
    @JsonProperty("plugins")
    private Map<String, PluginIndexableBean> plugins;

    /**
     * Elément portant les restrictions d'accès au document Elasticsearch.
     */
    @JsonProperty(IndexerUtil.FIELD_ACCESSCONTROL)
    private AccessControl accessControl;

    public final Serializable getFiche() {
        return fiche;
    }

    public final void setFiche(final Serializable fiche) {
        this.fiche = fiche;
    }

    public final Map<String, PluginIndexableBean> getPlugins() {
        return plugins;
    }

    public final void setPlugins(final Map<String, PluginIndexableBean> plugins) {
        this.plugins = plugins;
    }

    public final String getTypeFiche() {
        return typeFiche;
    }

    public final void setTypeFiche(final String typeFiche) {
        this.typeFiche = typeFiche;
    }

    public final AccessControl getAccessControl() {
        return accessControl;
    }

    public final void setAccessControl(final AccessControl accessControl) {
        this.accessControl = accessControl;
    }

    public String getCodeObjet() {
        return codeObjet;
    }

    public void setCodeObjet(final String codeObjet) {
        this.codeObjet = codeObjet;
    }

    /**
     * @return the rubriques
     */
    public String getRubriques() {
        return rubriques;
    }

    /**
     * @param rubriques the rubriques to set
     */
    public void setRubriques(final String rubriques) {
        this.rubriques = rubriques;
    }

}
