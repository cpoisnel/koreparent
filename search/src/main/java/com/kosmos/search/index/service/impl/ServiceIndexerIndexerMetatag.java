package com.kosmos.search.index.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.kosmos.search.index.bean.IndexableFiche;
import com.kosmos.search.index.bean.IndexingCommand;
import com.kosmos.search.index.service.ServiceCommandIndexation;
import com.kosmos.search.index.service.ServiceIndexation;
import com.kosmos.search.index.service.fiche.ServiceIndexerFicheFactory;
import com.kosmos.log.LoggerContextUtils;
import com.univ.objetspartages.bean.MetatagBean;

/**
 * Implémentation du service d'indexation des objets liés à des metatags.
 * @author cpoisnel
 *
 */
public class ServiceIndexerIndexerMetatag extends AbstractServiceIndexerCore<MetatagBean> implements ServiceIndexation<MetatagBean>,ServiceCommandIndexation<MetatagBean> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerIndexerMetatag.class);

    @Autowired
    private ServiceIndexerFicheFactory serviceIndexerFicheFactory;

	/**
	 * The jobLauncher.
	 */
	@Autowired
	private JobLauncher jobLauncher;

	/**
	 * The job.
	 */
	@Autowired
	@Qualifier("searchIndexerFicheMetatagJob")
	private Job job;

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveIndex(final MetatagBean metatag) {
		LOG.debug("Metatag à indexer : {}", metatag);
		String oldContexte = LoggerContextUtils.getLogContext();
    	LoggerContextUtils.initContextLogIfNotDefault("indexation");
		try {
			LOG.debug("Metatag à indexer : {}", metatag);
			final IndexableFiche<?> indexableFiche = this.fetchIndexableFiche(metatag);
			if (indexableFiche != null) {
				this.serviceIndexerFicheFactory.getServiceIndexerFiche(indexableFiche.getFiche().getClass()).saveIndex(metatag, indexableFiche.getType(), indexableFiche.getFiche());
			}
		}
		finally {
			LoggerContextUtils.initContextLog(oldContexte);
		}
	}

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexingCommand createIndexingCommand(final MetatagBean metatag) {
        LOG.debug("Metatag à indexer : {}", metatag);
        IndexingCommand indexingCommand = null;
        final IndexableFiche<?> indexableFiche = this.fetchIndexableFiche(metatag);
        if (indexableFiche != null) {
            indexingCommand = this.serviceIndexerFicheFactory.getServiceIndexerFiche(indexableFiche.getFiche().getClass()).createSaveIndexCommand(metatag, indexableFiche.getType(), indexableFiche.getFiche());
        } else {
            LOG.debug("Pas de fiche indexable pour le metatag {}", metatag);
        }
        return indexingCommand;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteIndex(final Long id) {
        LOG.info("Suppression des données liées au métatag : {}. Le service d'indexation par défaut est utilisé.", id);
        this.serviceIndexerFicheFactory.getServiceIndexerFiche(null).deleteIndex(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getCodeObjet(final MetatagBean bean) {
        return bean.getMetaCodeObjet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Long getIdFiche(final MetatagBean bean) {
        return bean.getMetaIdFiche();
    }

    @Override
    public IndexingCommand createDeletingCommand(final Long id) {
        return this.serviceIndexerFicheFactory.getServiceIndexerFiche(null).createDeleteIndexCommand(id);
    }

	/**
	 * Mise à jour de l'ensemble des index référençant la fiche associée au metatag spécifié en paramètres.
	 *
	 * @param metatag
	 *            MetatagBean - Le metatag.
	 */
	public void saveAllIndex(final MetatagBean metatag) {
		LOG.info("Mise à jour de l'ensemble des données indexées référençant la fiche (code={}, type={})", metatag.getMetaCode(), metatag.getMetaLibelleObjet());
		final Map<String, JobParameter> jobParametersMap = new HashMap<>();
		jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
		jobParametersMap.put("job.id_fiche_metatag", new JobParameter(metatag.getId()));
		final JobParameters jobParameters = new JobParameters(jobParametersMap);
		try {
			final JobExecution execution = this.jobLauncher.run(this.job, jobParameters);
			final ExitStatus exitStatus = execution.getExitStatus();
			final BatchStatus status = execution.getStatus();
			LOG.info("Fin du job  de réindexation liée à la modification de la fiche (code={}, type={})", metatag.getMetaCode(), metatag.getMetaLibelleObjet(), status, exitStatus);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
			LOG.error("Erreur à l'exécution du job de réindexation des données liées à la fiche (code={}, type={})", metatag.getMetaCode(), metatag.getMetaLibelleObjet(), e);
		}
	}

	/**
	 * Mise à jour de l'ensemble des index (rattachés) à une fiche suite à une suppression.
	 *
	 * @param metatagId
	 *            Long - L'identifiant du metatag associé à la fiche.
	 */
	public void deleteAllIndex(final Long metatagId) {
		LOG.info("Mise à jour de l'ensemble des données indexées rattachées à la fiche (id_metatag={})", metatagId);
		final Map<String, JobParameter> jobParametersMap = new HashMap<>();
		jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
		jobParametersMap.put("job.id_fiche_metatag", new JobParameter(metatagId));
		final JobParameters jobParameters = new JobParameters(jobParametersMap);
		try {
			final JobExecution execution = this.jobLauncher.run(this.job, jobParameters);
			final ExitStatus exitStatus = execution.getExitStatus();
			final BatchStatus status = execution.getStatus();
			LOG.info("Fin du job  de réindexation liée à la suppression de la fiche (id_metatag={}) au statut '{}' (exit status : '{}')", metatagId, status, exitStatus);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
			LOG.error("Erreur à l'exécution du job de réindexation des données liées à la fiche (id_metatag={})", metatagId, e);
		}
	}
}