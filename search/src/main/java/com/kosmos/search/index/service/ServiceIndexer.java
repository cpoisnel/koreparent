package com.kosmos.search.index.service;

import com.kosmos.search.index.bean.IndexingCommand;

/**
 * Service d'indexation Elasticsearch (manipule des commandes d'indexation).
 * @author cpoisnel
 *
 */
public interface ServiceIndexer {

    /**
     * Effectue la commande d'indexation sur Elas
     * @param command
     * Commande d'indexation
     */
    void processIndexCommand(IndexingCommand command);

    /**
     * Rafraîchit le ou les indices Elasticsearch.
     * Voir <a href="https://www.elastic.co/guide/en/elasticsearch/reference/2.4/indices-refresh.html">la documentation Elasticsearch</a>
     * @param indices
     * Index Elasticsearch
     * @return vrai en cas de succès
     */
    boolean refreshIndex(String... indices);

    /**
     * Supprime le ou les indices Elasticsearch. Si pas d'argument, supprime l'ensemble des indices.
     * Voir <a href="https://www.elastic.co/guide/en/elasticsearch/reference/2.4/indices-delete-index.html">la documentation Elasticsearch</a>
     * @param indices
     * Index Elasticsearch
     * @return vrai en cas de succès (soulève une exception sinon)
     */
    boolean deleteIndex(String... indices);

}
