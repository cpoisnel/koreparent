package com.kosmos.search.index.mapper.serializer;


/**
 * Classe abstraite de sérialisation des labels (sous la forme label_code / label_value).
 *
 * @author cpoisnel
 *
 * @param <T>
 *            Type d'attribut manipulé (String,...)
 */
public abstract class AbstractLabelSerializer<T> extends AbstractLanguageSerializer<T> {

	public static final String LABEL_CODE = "label_code";

	public static final String LABEL_VALUE = "label_value";

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = -224084146135447233L;

	/**
	 * Constructeur.
	 * @param classe
	 * Type d'attribut manipulé
	 */
	protected AbstractLabelSerializer(final Class<T> classe) {
		super(classe);
	}

}
