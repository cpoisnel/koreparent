package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceUser;

/**
 * Sérialisation d'un code utilisateur en données non
 */
public class UserSerializer extends StdSerializer<String> {

	/**
	 * Instance courante de sérialisation.
	 */
	public static final UserSerializer instance = new UserSerializer();


	private static final long serialVersionUID = 2213847317087036603L;


	private static final Logger LOG = LoggerFactory.getLogger(UserSerializer.class);

	/**
	 * DATE_ZERO est la référence statique utilisée pour l'initialisation de certaine dates (non nulles)
	 */
	private static final Date DATE_ZERO = new Date(0);


	private final transient ServiceUser serviceUser;

	/**
	 * Constructeur.
	 * Initialisation de la référence du ServiceUser.
	 */
	public UserSerializer() {
		super(String.class);
		this.serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void serialize(final String userCode, final JsonGenerator jsonGen, final SerializerProvider provider) throws IOException {
		if (StringUtils.isBlank(userCode)) {
			jsonGen.writeNull();
			return;
		}
		jsonGen.writeStartObject();
		jsonGen.writeStringField("user_code", userCode);
		final UtilisateurBean user = this.serviceUser.getByCode(userCode);
		if (null == user) {
			LOG.debug("Code utilisateur par défaut '{}', le code 'null' est conservé", userCode);
		} else {
			// spécificités de l'utilisateur
			dealWithGender(jsonGen, user);
			dealWithBirthday(jsonGen, user);
			jsonGen.writeNumberField("user_id", user.getId());
			jsonGen.writeStringField("user_name", user.getNom());
			jsonGen.writeStringField("user_mail", user.getAdresseMail());
			jsonGen.writeStringField("user_first_name", user.getPrenom());
			jsonGen.writeStringField("user_ldap_code", user.getCodeLdap());
		}
		jsonGen.writeEndObject();
	}

	/**
	 * {@link UserSerializer#serialize(String, JsonGenerator, SerializerProvider)}.
	 */
	private static void dealWithGender(final JsonGenerator jsonGen, final UtilisateurBean user) throws IOException {
		final String gender = user.getCivilite();
		if (StringUtils.equals("0000", gender)) {
			// spécificité des champs "produit" positionnés à l'état "0000" --> indéfini
			LOG.debug("Civilité par défaut '{}', la civilité 'null' est conservée", gender);
			jsonGen.writeStringField("user_gender", null);
		} else {
			jsonGen.writeStringField("user_gender", gender);
		}
	}

	/**
	 * {@link UserSerializer#serialize(String, JsonGenerator, SerializerProvider)}.
	 */
	private static void dealWithBirthday(final JsonGenerator jsonGen, final UtilisateurBean user) throws IOException {
		final Date dateNaissance = user.getDateNaissance();
		if (DateUtils.isSameDay(DATE_ZERO, dateNaissance)) {
			// spécificité des dates "produit" positionnées au 01-01-1970 (attention au décalage où la date zéro peut être positionnée au 31-12-1969)
			LOG.debug("Date de naissance par défaut '{}', la date de naissance 'null' est conservée", dateNaissance);
			jsonGen.writeObjectField("user_birthday", null);
		} else {
			jsonGen.writeObjectField("user_birthday", dateNaissance);
		}
	}
}
