package com.kosmos.search.index.service.impl;

import java.io.Serializable;

import org.apache.commons.lang3.ArrayUtils;
import org.elasticsearch.action.ShardOperationFailedException;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.refresh.RefreshResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.deletebyquery.DeleteByQueryAction;
import org.elasticsearch.action.deletebyquery.DeleteByQueryRequestBuilder;
import org.elasticsearch.action.deletebyquery.DeleteByQueryResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.kosmos.search.index.bean.IndexingCommand;
import com.kosmos.search.index.bean.IndexingCommand.IndexingAction;
import com.kosmos.search.exception.IndexationException;
import com.kosmos.search.index.service.ServiceIndexer;

/**
 * Service d'indexation, implémentation Elastic Search.
 *
 *
 * @author cpoisnel
 *
 */
public class ServiceIndexerImpl implements ServiceIndexer {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerImpl.class);

    /**
     * Client Elasticsearch.
     */
    private Client client;

    /**
     * Object Mapper JSON.
     */
    private ObjectMapper objectMapper;

    /**
     * Constructeur du service d'indexation.
     */
    public ServiceIndexerImpl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processIndexCommand(final IndexingCommand command) {
        if (null != command) {
            LOG.debug("Commande d'indexation : {}", command);
            final IndexingAction action = command.getAction();
            if (action == IndexingAction.CREATE_UPDATE) {
                final byte[] jsonsByte;
                final Serializable document = command.getDocument();
                try {
                    // C'est la sérialisation qui porte la "transformation" d'attributs du bean en attributs elastic-query
                    jsonsByte = objectMapper.writeValueAsBytes(document);
                } catch (final JsonProcessingException e) {
                    LOG.error("Impossible de sérialiser le document {}", document, e);
                    throw new IndexationException("Impossible de sérialiser en JSON le document", e);
                }
                final IndexResponse response = client.prepareIndex(command.getIndex(), command.getType(), command.getId()).setParent(command.getParent()).setSource(jsonsByte).get();
                LOG.debug("Retour de l'indexation Elasticsearch : {}", response);
                if (ArrayUtils.isEmpty(response.getShardInfo().getFailures())) {
                    LOG.info("Indexation effectuée [index : {}, type : {}, id : {}]", command.getIndex(), command.getType(), command.getId());
                } else {
                    LOG.warn("Echec de l'indexation [index : {}, type : {}, id : {}].  Il faut nécessairement relancer une indexation globale pour resynchroniser l'index. Retour Elasticsearch : {}", command.getIndex(), command.getType(), command.getId(), response);
                }
            } else if (action == IndexingAction.DELETE) {
                final String idDocument = command.getId();
                final String byQuery = command.getByQuery();
                if (idDocument != null) {
                    final DeleteResponse response = client.prepareDelete(command.getIndex(), command.getType(), idDocument).get();
                    LOG.debug("Suppression par id : {}", response);
                    if (ArrayUtils.isEmpty(response.getShardInfo().getFailures())) {
                        LOG.info("Suppression d'index effectuée [index : {}, type : {}, id : {}]", command.getIndex(), command.getType(), command.getId());
                    } else {
                        LOG.warn("Echec de la suppression de indexation [index : {}, type : {}, id : {}].  Il faut nécessairement relancer une indexation globale pour resynchroniser l'index. Retour Elasticsearch : {}.", command.getIndex(), command.getType(), command.getId(), response);
                    }
                } else if (byQuery != null) {
                    final DeleteByQueryResponse response = new DeleteByQueryRequestBuilder(client, DeleteByQueryAction.INSTANCE).setIndices(command.getIndex()).setTypes(command.getType()).setSource(byQuery).execute().actionGet();
                    LOG.debug("Suppression par query : {}", response);
                    if (ArrayUtils.isEmpty(response.getShardFailures())) {
                        LOG.info("Suppression d'index effectuée [index : {}, type : {}, query : {}]", command.getIndex(), command.getType(), byQuery);
                    } else {
                        LOG.warn("Echec de la suppression par requête [index : {}, type : {}, query : {}]. Il faut nécessairement relancer une indexation globale pour resynchroniser l'index. Retour Elasticsearch : {}", command.getIndex(), command.getType(), byQuery, response);
                    }
                }
            } else {
                LOG.warn("Action non gérée : {}", command);
            }
        } else {
            LOG.warn("La commande est envoyée est nulle");
        }
    }

    public final void setClient(final Client client) {
        this.client = client;
    }

    public final void setObjectMapper(final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean refreshIndex(final String... indices) {
        final RefreshResponse response = client.admin().indices().prepareRefresh(indices).get();
        final ShardOperationFailedException[] shardFailures = response.getShardFailures();
        final boolean succes = ArrayUtils.isEmpty(shardFailures);
        if (!succes) {
            LOG.warn("Exceptions rencontrées lors du rafraîchissement des indices '{}' : {}", new Object[] {indices, shardFailures});
        }
        return succes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean deleteIndex(final String... indices) {
        LOG.info("Suppression des indices : '{}'", new Object[] {indices});
        final DeleteIndexResponse response = client.admin().indices().prepareDelete(indices).get();
        final boolean succes = response.isAcknowledged();
        LOG.info("Suppression prise en compte des indices : '{}'", new Object[] {indices});
        return succes;
    }
}
