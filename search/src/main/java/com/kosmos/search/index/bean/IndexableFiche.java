package com.kosmos.search.index.bean;

import com.univ.objetspartages.bean.PersistenceBean;

/**
 * Fiche indexable. Wrapper du beau de fiche, du type, et du code objet.
 *
 * @author cpoisnel
 *
 */
public class IndexableFiche<T extends PersistenceBean> {

    private T fiche;

    private String type;

    private String codeObjet;

    public final  T getFiche() {
        return fiche;
    }

    public final void setFiche(final T fiche) {
        this.fiche = fiche;
    }

    public final String getType() {
        return type;
    }

    public final void setType(final String type) {
        this.type = type;
    }

    public String getCodeObjet() {
        return codeObjet;
    }

    public void setCodeObjet(final String codeObjet) {
        this.codeObjet = codeObjet;
    }

    /**
     * Constructeur d'une fiche indexable.
     * @param fiche
     * Bean de la fiche (persistenceBean)
     * @param type
     * Type de la fiche
     * @param codeObjet
     * Code objet de la fiche
     */
    public IndexableFiche(final T fiche, final String type, final String codeObjet) {
        super();
        this.fiche = fiche;
        this.type = type;
        this.codeObjet = codeObjet;
    }

    /**
     *{@inheritDoc}
     */
    @Override
    public String toString() {
        return "IndexableFiche [fiche=" + fiche + ", type=" + type + "codeObjet=" + codeObjet + "]";
    }

}
