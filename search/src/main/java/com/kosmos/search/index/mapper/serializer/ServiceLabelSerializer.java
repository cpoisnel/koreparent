package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.services.ServiceLabel;

/**
 * Sérialisation des labels.
 *
 * @author cpoisnel
 *
 */
public class ServiceLabelSerializer extends AbstractLabelSerializer<String> {

    public static final String LABEL_ID = "label_id";

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 5387783319155027057L;

    private static final Logger LOG = LoggerFactory.getLogger(ServiceLabelSerializer.class);

    private final transient ServiceLabel serviceLabel;

    protected final String type;

    /**
     * Constructeur (appelle le constructeur de la classe parente), Le type de label est défini, et le service label est référencé.
     * @param type
     * Code type du label
     */
    public ServiceLabelSerializer(final String type) {
        super(String.class);
        this.type = type;
        this.serviceLabel = ServiceManager.getService(ServiceLabel.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(final String codesSerialise, final JsonGenerator jgen, final SerializerProvider provider) throws IOException {
        if (StringUtils.isNotEmpty(codesSerialise)) {
            jgen.writeStartArray();
            final String[] codes = codesSerialise.split(";");
            for (final String code : codes) {
                final String codeLabel;
                if ("".equals(code) || "0000".equals(code)) {
                    LOG.debug("Code label par défaut '{}', le code 'null' est conservé", code);
                    codeLabel = null;
                } else {
                    codeLabel = code;
                }
				final String langue = processLanguage(jgen, provider);
                if (codeLabel != null) {
                    final LabelBean labelBean = serviceLabel.getByTypeCodeLanguage(type, codeLabel, langue);
                    jgen.writeStartObject();
                    if (labelBean != null) {
                        jgen.writeStringField(LABEL_VALUE, labelBean.getLibelle());
                        jgen.writeNumberField(LABEL_ID, labelBean.getId());
                    } else {
                        LOG.info("Label non trouvé pour le type '{}', code '{}', langue '{}'. Seul le code '{}' est conservé", type, codeLabel, langue, codeLabel);
                    }
                    jgen.writeStringField(LABEL_CODE, codeLabel);
                    jgen.writeEndObject();
                }
            }
            jgen.writeEndArray();
        } else {
            jgen.writeNull();
        }
    }
}
