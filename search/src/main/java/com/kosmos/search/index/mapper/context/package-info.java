/**
 *
 * Initialisation des contextes de sérialisation spécifique. Repose sur les fonctionnalités de Jackson de <a href="http://wiki.fasterxml.com/ContextualSerializer">contexte de sérialisation</a>.
 *
 * <h2>Fonctionnement des classes implémentées dans ce package</h2>
 * <p>
 *     Chaque classe implémentant {@link com.fasterxml.jackson.databind.ser.ContextualSerializer} possède la méthode
 *     <pre>
 *      public JsonSerializer<?> createContextual(final SerializerProvider paramSerializerProvider, final BeanProperty paramBeanProperty) throws JsonMappingException
 *     </pre>
 * </p>
 * <p>
 *         Cette méthode devrait respecter le contrat suivant :
 *         <ul>
 *             <li>Vérification des annotations sur la propriété (ou le getter associé). Le contexte de sérialisation peut aussi être initialisé à partir d'un nom de champ
 *             ou encore le type de la propriété</li>
 *             <li>Récupération des valeurs utiles (attributs de l'annotation, nom de la propriété,...)</li>
 *             <li>Si applicable, on renvoie une instance (nouvelle) si c'est spécifique à des valeurs de la propriété, sinon, on renvoie une instance de contexte</li>
 *             <li>Si le contexte de sérialisation ne s'applique pas sur ce champ, alors le contexte renvoyé doit être nul.</li>
 *         </ul>
 *         Le dernier point est important car dans l'utilisation qui en est faite ({@link com.kosmos.search.index.mapper.ContextualCoreStringSerializer#createContextual(com.fasterxml.jackson.databind.SerializerProvider, com.fasterxml.jackson.databind.BeanProperty)}) on itère sur l'ensemble des contextes initialisés,
 *         jusqu'à ce qu'on rencontre un contexte non null.
 *
 *
 *
 * </p>
 *
 * @author cpoisnel
 */
package com.kosmos.search.index.mapper.context;