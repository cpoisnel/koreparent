package com.kosmos.search.index.mapper.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.kosmos.search.index.mapper.FicheBeanSerializer;
import com.kosmos.search.index.mapper.ContextAttributeSerializerUtils;

/**
 * Serializer abstrait d'attributs Jackson, qui ont connaissance du contexte de sérialisation et peuvent utiliser la langue courante.
 *
 * @param <T>
 *           Le type d'attribut manipulé
 */
public abstract class AbstractLanguageSerializer<T> extends StdSerializer<T> {

	/**
	 * The serialVersionUID.
	 */
	private static final long serialVersionUID = -3868731331458427301L;

	/**
	 * Constructeur (appelle le constructeur de la classe parente).
	 * @param clazz
	 * Type d'attribut manipulé
	 */
	protected AbstractLanguageSerializer(final Class<T> clazz) {
		super(clazz);
	}

	/**
	 * Retourne la langue pour la sérialisation du Label, à partir du contexte de sérialisation (initialisé dans {@link FicheBeanSerializer}).
	 *
	 * @param jsonGen
	 *            JsonGenerator - Le générateur JSON.
	 * @param provider
	 *            SerializerProvider - L'utilitaire de sérialisation.
	 * @return String langue - La langue attendue sinon <code>0</code>.
	 */
	protected String processLanguage(final JsonGenerator jsonGen, final SerializerProvider provider) {
		final Object langue = provider.getAttribute(ContextAttributeSerializerUtils.CONTEXT_LANGUAGE);
		if (null != langue) {
			return String.valueOf(langue);
		}
		return "0";
	}
}
