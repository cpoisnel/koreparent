package com.kosmos.search.index.mapper.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.kosmos.search.index.mapper.serializer.ToolboxSerializer;
import com.kportal.cms.objetspartages.annotation.GetterAnnotation;

/**
 * Contexte de sérialisation pour les Toolbox, nécessaire pour faire les traitements spécifiques en fonction des annotations.
 * @author cpoisnel
 */
public class ToolboxContextSerializer implements ContextualSerializer {

    private static final Logger LOG = LoggerFactory.getLogger(ToolboxContextSerializer.class);

    /**
     * <p>
     *     Renvoie l'instance de {@link ToolboxSerializer} si l'annotation @{@link GetterAnnotation} est positionnée sur la propriété, sinon null.
     * </p>
     * {@inheritDoc}
     */
    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider paramSerializerProvider, final BeanProperty paramBeanProperty) throws JsonMappingException {
        GetterAnnotation getterAnnotation = paramBeanProperty.getAnnotation(GetterAnnotation.class);
        if (getterAnnotation == null) {
            getterAnnotation = paramBeanProperty.getContextAnnotation(GetterAnnotation.class);
        }
        if (getterAnnotation != null && getterAnnotation.isToolbox()) {
            LOG.debug("Utilisation de la sérialisation Toolbox pour l'attribut '{}'", paramBeanProperty.getName());
            return ToolboxSerializer.instance;
        }
        return null;
    }
}
