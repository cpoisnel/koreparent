package com.kosmos.search.index.service.fiche;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.search.exception.IndexationException;
import com.univ.objetspartages.bean.AbstractFicheBean;

/**
 * Factory de services d'indexation de fiche.
 *
 * @author cpoisnel
 *
 */
public class ServiceIndexerFicheFactory {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerFicheFactory.class);

    /**
     * Service indexer fiche.
     * <p>
     *     Renvoie le service d'indexation correspondant à la classe.
     *
     * </p>
     * @param typeFiche
     * @return le service d'indexation associé à la classe de la fiche.
     */
    @SuppressWarnings("unchecked")
    public <T, F> ServiceIndexerFiche<T> getServiceIndexerFiche(final Class<F> typeFiche) {
        LOG.debug("Recherche du service d'indexation pour le type fiche {}", typeFiche);
        final Class<?> typeFicheTeste;
        if (null == typeFiche) {
            // Si c'est null, il faut utiliser le cas par défaut
            typeFicheTeste = AbstractFicheBean.class;
            LOG.debug("Type AbstractFicheBean par défaut, car le type demandé est null");
        } else {
            typeFicheTeste = typeFiche;
        }
        Map<String, ServiceIndexerFiche> retrievedServiceIndexer = ApplicationContextManager.getAllBeansOfType(ServiceIndexerFiche.class);
        Class<?> electedClass = AbstractFicheBean.class;
        ServiceIndexerFiche<T> electedService = null;
        // Election du service d'indexation
        for (ServiceIndexerFiche serviceIndexer : retrievedServiceIndexer.values()) {
            if (null != electedClass && serviceIndexer.getType().isAssignableFrom(typeFicheTeste) && electedClass.isAssignableFrom(typeFicheTeste)) {
                electedClass = typeFicheTeste;
                electedService = serviceIndexer;
                // On peut sortir de la boucle si les classes sont égales (on ne trouvera pas de service plus proche)
                if (null != electedClass && electedClass.equals(serviceIndexer.getType())) {
                    break;
                }
            }
        }
        if (null == electedService) {
            LOG.error("Aucun service d'indexation trouvé pour le type {}", typeFiche);
            throw new IndexationException("Aucun service d'indexation par défaut n'a été défini");
        }
        LOG.debug("Service d'indexation renvoyé : '{}'. Type fiche : '{}'", electedService, typeFiche);
        return electedService;
    }
}
