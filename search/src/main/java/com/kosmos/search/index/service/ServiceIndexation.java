package com.kosmos.search.index.service;

/**
 * Interface des services d'indexation métiers pour ElasticSearch.
 * @author cpoisnel
 */
public interface ServiceIndexation<T> {

    /**
     * Indexation lors d'une sauvegarde d'un bean.
     * @param bean
     * Bean manipulé
     */
    void saveIndex(final T bean);

    /**
     * Effectue des actions lors de la suppression d'une donnée indexée.
     * @param id
     * ID d'un metatag
     */
    void deleteIndex(Long id);
}
