package com.kosmos.search.index.bean;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * Commande d'indexation.
 *
 * <p>Une commande d'indexation est générique, elle peut porter tout type de {@link #document}, pourvu qu'il soit {@link Serializable}. Cette commande d'indexation peut être sérialisée et/ou elle-même mise dans une file d'attente.</p>
 * <p>La commande d'indexation reprend les <a href="https://www.elastic.co/guide/en/elasticsearch/reference/2.4/_basic_concepts.html">concepts de base d'Elasticsearch</a></p>
 * @author cpoisnel
 *
 */

public class IndexingCommand implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -8341591462983674453L;

    /**
     * Action attendue ({@link IndexingAction#CREATE_UPDATE} ou {@link IndexingAction#DELETE}).
     * <p>
     *     Le type d'action est liée directement à l'API Elasticsearch utilisée.
     * </p>
     */
    @NotNull
    private IndexingAction action;

    /**
     * Nom de l'index Elasticsearch.
     */
    @NotNull
    private String index;

    /**
     * Type de document dans l'index.
     */
    @NotNull
    private String type;

    /**
     * Dans le cas d'une <a href="https://www.elastic.co/guide/en/elasticsearch/guide/2.x/parent-child.html">relation parent/child</a>, définit l'ID du document parent.
     * Attention, le document parent doit exister pour que la commande puisse être exécutée.
     */
    private String parent;

    /**
     * Instruction spécifique au plugin "delete-by-query", et uniquement dans le cas d'une action {@link IndexingAction#DELETE}.
     *
     */
    private String byQuery;

    /**
     * ID de la donnée. L'ID est facultatif et sera généré par le moteur d'indexation si non présent.
     */
    private String id;

    /**
     * Contenu à indexer.
      */
    private Serializable document;

    public final IndexingAction getAction() {
        return action;
    }

    public final void setAction(final IndexingAction action) {
        this.action = action;
    }

    public final Serializable getDocument() {
        return document;
    }

    public final void setDocument(final Serializable document) {
        this.document = document;
    }

    public final String getIndex() {
        return index;
    }

    public final void setIndex(final String index) {
        this.index = index;
    }

    public final String getType() {
        return type;
    }

    public final void setType(final String type) {
        this.type = type;
    }

    public final String getId() {
        return id;
    }

    public final void setId(final String id) {
        this.id = id;
    }

    public final String getByQuery() {
        return byQuery;
    }

    public final void setByQuery(final String byQuery) {
        this.byQuery = byQuery;
    }


    public final String getParent() {
        return parent;
    }

    public final void setParent(final String parent) {
        this.parent = parent;
    }


    /**
     * Type de demande d'indexation.
     */
    public enum IndexingAction {
        /**
         * Création ou mise à jour (Elasticsearch ne fait pas de différence entre une création d'un index ou sa mise à jour).
         */
        CREATE_UPDATE,

        /**
         * Suppression d'un document (ou plusieurs si combiné avec l'utilisation du plugin delete-by-query).
         */
        DELETE;
    }

    @Override
    public String toString() {
        return "IndexingCommand [action=" + action + ", index=" + index + ", type=" + type + ", id=" + id + ", byQuery=" + byQuery +", document=" + document + ", parent=" + parent + "]";
    }

}
