package com.kosmos.search.index.service.impl;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import com.kosmos.search.index.bean.IndexableFiche;
import com.kosmos.service.ServiceBean;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.annotation.FicheAnnotation;
import com.univ.objetspartages.bean.AbstractPersistenceBean;
import com.univ.objetspartages.bean.PersistenceBean;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;

import com.kosmos.search.exception.IndexationException;
import com.kosmos.search.util.IndexerUtil;

/**
 * Classe abstraite d'indexation, à utiliser par les services d'indexation manipulant des fiches.
 * @param <T>
 *     Bean, avec des attributs de fiche
 */
public abstract class AbstractServiceIndexerCore<T extends PersistenceBean> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractServiceIndexerCore.class);

    /**
     * Recherche du type correspondant à la classe du bean. Est utilisé pour déterminer le nom de l'index Elasticsearch.
     * <p>Renvoie le nom de la classe de bean en minuscule</p>
     * @param beanClass
     * Classe du bean métier
     * @return le type de fiche
     */
    public <F> String getTypeFiche(final Class<F> beanClass) {
        return beanClass.getSimpleName().toLowerCase();
    }

    /**
     * Récupérer le code de l'objet cible.
     * @param bean
     * PersistenceBean
     * @return le code objet du bean manipulé
     */
    protected abstract String getCodeObjet(T bean);

    /**
     * Récupérer l'ID de la fiche.
     * @param bean
     * PersistenceBean
     * @return l'ID de la fiche
     */
    protected abstract Long getIdFiche(T bean);

    /**
     * Récupérer la fiche indexable à partir du bean.
     * @param bean
     *
     * @return la fiche à indexer
     */
    @SuppressWarnings("rawtypes")
    protected IndexableFiche fetchIndexableFiche(final T bean) {
        IndexableFiche indexableFiche = null;
        PersistenceBean ficheBean;
        String typeFiche;
        final String codeObjet = getCodeObjet(bean);
        final Long idFiche = getIdFiche(bean);
        if (codeObjet != null && idFiche != null) {

            // On récupère la classe de la fiche univ
            final Class<?> classeFiche = IndexerUtil.getClasseFiche(codeObjet);
            typeFiche = getTypeFiche(classeFiche);

            if (typeFiche != null && AbstractOm.class.isAssignableFrom(classeFiche)) {
                final FicheAnnotation ficheAnnotation = AnnotationUtils.findAnnotation(classeFiche, FicheAnnotation.class);
                if (ficheAnnotation != null && ficheAnnotation.isIndexable()) {
                    @SuppressWarnings({"unchecked"})
                    final Class<?> classBean = IndexerUtil.resolveClassPersistenceBean((Class<AbstractOm>) classeFiche);
                    // Avec persistence bean !
                    // Charger le service correspondant au document indexé
                    final ServiceBean<?> serviceBean = ServiceManager.getServiceForBean(classBean);
                    if (serviceBean != null) {
                        Method methodGet;
                        try {
                            methodGet = serviceBean.getClass().getMethod("getById", Long.class);
                        } catch (NoSuchMethodException | SecurityException e) {
                            LOG.warn("Impossible de déterminer la méthode getById pour le service '{}' et l'ID '{}'", serviceBean, idFiche);
                            throw new IndexationException("Impossible de déterminer la methode getById", e);
                        }
                        ficheBean = (AbstractPersistenceBean) ReflectionUtils.invokeMethod(methodGet, serviceBean, idFiche);
                    } else {
                        // Il se peut qu'il n'y ait pas de service pour l'objet, ex : Actualite -> Avoir un comportement dégradé avec les fiches non migrées sur les services.
                        LOG.debug("Service de la classe '{}' non trouvé, utilisation de l'objet FicheUniv", classeFiche);
                        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
                        if (ficheUniv ==null) {
                            throw new IndexationException("Impossible d'instancier la fiche de code objet : "+codeObjet);
                        }
                        ficheUniv.setIdFiche(idFiche);
                        try {
                            ficheUniv.retrieve();
                        } catch (final Exception e) {
                            LOG.error("Impossible de récupérer la fiche", e);
                        }
                        ficheBean = ((AbstractOm) ficheUniv).getPersistenceBean();

                    }
                    indexableFiche = new IndexableFiche(ficheBean, typeFiche, codeObjet);
                } else {
                    LOG.debug("Indexation désactivé pour le type {} (FicheAnnotation)", typeFiche);
                }
            }
        }

        return indexableFiche;
    }
}
