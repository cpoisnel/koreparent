package com.kosmos.search.index.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.print.attribute.standard.Media;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.kosmos.search.index.service.ServiceIndexation;
import com.univ.objetspartages.bean.MediaBean;

/**
 * Service permettant de gérer les actions associées à des modifications de medias (ServiceMedia).
 * @author cpoisnel
 *
 */
public class ServiceIndexerMediaImpl implements ServiceIndexation<MediaBean> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerMediaImpl.class);

    @Autowired
    private JobLauncher jobLauncher;

    /**
     * Job de réindexation des fiches liées à des medias.
     */
    @Autowired
    @Qualifier("searchIndexerMediaJob")
    private Job job;

    /**
     * Mise à jour de l'ensemble des index référençant ce media.
     * @param mediaBean
     *         Bean de media créé ou mis à jour
     */
    public void saveIndex(final MediaBean mediaBean) {
        LOG.info("Mise à jour de l'ensemble des données indexées liées au media {}", mediaBean);
        final Map<String, JobParameter> jobParametersMap = new HashMap<>();
        jobParametersMap.put("job.id_media", new JobParameter(mediaBean.getId()));
        jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
        final JobParameters jobParameters = new JobParameters(jobParametersMap);
        try {
            final JobExecution execution = jobLauncher.run(job, jobParameters);
            final BatchStatus status = execution.getStatus();
            final ExitStatus exitStatus = execution.getExitStatus();
            LOG.info("Fin du job  de réindexation liée à la modification du media d'ID '{}' au statut '{}' (exit status : '{}')", mediaBean.getId(), status, exitStatus);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            LOG.error("Erreur à l'exécution du job de réindexation des données liées au media de code '{}'", mediaBean.getId(), e);
        }
    }

    /**
     * Mise à jour de l'ensemble des index référençant ce media supprimé.
     * @param idMedia
     *  ID du media supprimé
     */
    public void deleteIndex(final Long idMedia) {
        LOG.info("Mise à jour de l'ensemble des données indexées liées au media d'id '{}'", idMedia);
        final Map<String, JobParameter> jobParametersMap = new HashMap<>();
        jobParametersMap.put("job.timestamp", new JobParameter(new Date()));
        jobParametersMap.put("job.id_media", new JobParameter(idMedia));
        final JobParameters jobParameters = new JobParameters(jobParametersMap);
        try {
            final JobExecution execution = jobLauncher.run(job, jobParameters);
            final BatchStatus status = execution.getStatus();
            final ExitStatus exitStatus = execution.getExitStatus();
            LOG.info("Fin du job  de réindexation liée à la suppression du media d'id '{}' au statut '{}' (exit status : '{}')", idMedia, status, exitStatus);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            LOG.error("Erreur à l'exécution du job de réindexation des données liées au media d'id '{}'", idMedia, e);
        }
    }
}
