package com.kosmos.search.index.mapper;

/**
 * Clés du contexte de sérialisation (Jackson).
 * @author cpoisnel
 *
 */
public class ContextAttributeSerializerUtils {

    /**
     * Clé de la donnée de langue stockée dans le
     */
    public static final String CONTEXT_LANGUAGE = "CONTEXT_LANGUAGE";

    /**
     * Constructeur privé, classe non instanciable.
     */
    private ContextAttributeSerializerUtils() {}

}
