/**
 * Instances des Serializers.
 *
 * <h2>Fonctionnement</h2>
 * Chaque classe de ce package implémente {@link com.fasterxml.jackson.databind.ser.std.StdSerializer} et possède la méthode
 * <pre>
 *     public void serialize(final String codesSerialise, final JsonGenerator jgen, final SerializerProvider provider) throws IOException
 * </pre>
 *  L'implémentation de cette méthode doit utiliser le générateur jgen pour ajouter des balises JSON (array / value), basées sur les attributs de la classe initialisée depuis une classe étendant
 *  {@link com.fasterxml.jackson.databind.ser.ContextualSerializer}. Les classes sont orchestrées par Spring et peuvent donc l'injection @{@link org.springframework.beans.factory.annotation.Autowired}
 *
 *
 * Voir le package "com.kosmos.search.index.mapper.context" pour plus d'explications sur le fonctionnement du contexte à initialiser
 *
 * ![Serializer](serializer.png)
 *
 * @uml serializer.png
 * ContextSerializer ->  StdSerializer : Initialisation de la classe de sérialisation
 * StdSerializer -> GénérateurJSON : Ecriture du flux JSON
 *
 * @author cpoisnel
 */
package com.kosmos.search.index.mapper.serializer;