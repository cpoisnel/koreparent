package com.kosmos.search.index.service.fiche;

import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.kosmos.search.index.bean.IndexingCommand;

/**
 * Interface commune à l'ensemble des indexations de fiches.
 * @author cpoisnel
 *
 * @param <T>
 * Type de bean manipulé par les fiches.
 */
public interface ServiceIndexerFiche<T> {

    /**
     * Renvoie la classe applicable.
     * @return
     */
    Class<T> getType();

    /**
     * Sauvegarde de l'index (création ou mise à jour et suppression de l'index).
     * @param metatagBean
     * @param type
     * @param bean
     */
    void saveIndex(MetatagBean metatagBean, String type, T bean);

    /**
     * Sauvegarde de l'index (création ou mise à jour et suppression de l'index).
     * @param ressource
     * @param type
     * @param bean
     * @param parent
     * Identifiant parent
     */
    void saveIndexRessource(RessourceBean ressource, String type, T bean, final Long parent);

    /**
     * Suppression de l'index à partir du metatag.
     * @param idMetatag
     */
    void deleteIndex(Long idMetatag);

    /**
     * Suppression de l'index de la ressoure.
     * @param idResource
     */
    void deleteIndexRessource(Long idResource);

    /**
     * Créer une commande d'indexation de suppression de metatag (à utiliser par les traitements batchs par exemple).
     * @param idMetatag
     * @return
     */
    IndexingCommand createDeleteIndexCommand(Long idMetatag);

    /**
     * Créer une commande de sauvegarde d'index.
     * @param metatag
     * @param type
     * @param bean
     * @return
     */
    IndexingCommand createSaveIndexCommand(MetatagBean metatag, String type, T bean);

    /**
     * Générer une commande d'indexation pour une ressource (à utiliser par les traitements batchs par exemple).
     *
     * @param ressourceBean
     * @param type
     * @param bean
     * @return
     */
    IndexingCommand createSaveIndexCommand(RessourceBean ressourceBean, String type, T bean, final Long parent);

    /**
     * Générer une commande d'indexation pour une suppression d'index d'une ressource (à utiliser par les traitements batchs par exemple).
     * @param idResource
     * @return
     */
    IndexingCommand createDeleteIndexRessource(Long idResource);

    /**
     * Création d'une demande de suppression de l'index.
     * @param idMeta
     * @return
     */
    IndexingCommand createDeleteIndexRessourceByIdMeta(Long idMeta);
}
