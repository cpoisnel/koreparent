package com.kosmos.search.query.configuration;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.drew.lang.annotations.NotNull;

/**
 * Created by cpoisnel on 03/01/17.
 */
public class SearchIndexConfiguration {

    /**
     * Nom d'objet, utilisé pour l'index.
     */
    @NotNull
    private String objectName;

    /**
     * Pondération / Boost sur l'index (utilisé exclusivement pour la recherche).
     * Boost (entre 0 et 20)
     */
    private float boost = 1;

    /**
     * Constructeur par défaut.
     */
    public SearchIndexConfiguration(){
        super();
    }

    /**
     * Constructeur avec nom de l'objet.
     * @param objectName
     * Nom de l'objet
     */
    public SearchIndexConfiguration(final String objectName){
        super();
        this.objectName=objectName;
    }

    /**
     * Constructeur avec nom d'objet et boost paramtré.
     * @param objectName
     * Nom de l'objet
     * @param boost
     * Boost (entre 0 et 20)
     */
    public SearchIndexConfiguration(final String objectName, final float boost){
        super();
        this.objectName=objectName;
        this.boost=boost;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(final String objectName) {
        this.objectName = objectName;
    }

    public float getBoost() {
        return boost;
    }

    public void setBoost(final float boost) {
        this.boost = boost;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SearchIndexConfiguration that = (SearchIndexConfiguration) o;
        return new EqualsBuilder().append(objectName, that.objectName).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(objectName).toHashCode();
    }
}
