package com.kosmos.search.query.configuration;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.elasticsearch.index.query.QueryBuilder;

import com.kosmos.search.query.request.SearchQueryBuilder;
import com.kosmos.search.query.view.bean.SearchFilterModel;
import com.kosmos.search.query.view.builder.SearchViewModelBuilder;

/**
 * Configuration d'un filtre de recherche.
 *
 * Created by cpoisnel on 19/12/16.
 */
public class SearchFilterConfiguration<V extends SearchFilterModel> implements Serializable{

    private static final long serialVersionUID = 7319905491164262421L;

    /**
     * Nom du paramètre dans la requête HTTP et de l'identifiant du filtre.
     * Un filtre est unique que par le nom !
     */
    private String name;

    /**
     * Vue JSP d'affichage du filtre.
     * TODO : A supprimer si Tiles
     */
    private String view;

    /**
     * Requête de filtre Elasticsearch.
     */
    private SearchQueryBuilder queryBuilder;

    /**
     * Builder de la vue.
     * TODO : A supprimer si Tiles
     */
    private SearchViewModelBuilder<V> builder;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getView() {
        return view;
    }

    public void setView(final String view) {
        this.view = view;
    }

    public SearchQueryBuilder getQueryBuilder() {
        return queryBuilder;
    }

    public void setQueryBuilder(final SearchQueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    public SearchViewModelBuilder<V> getBuilder() {
        return builder;
    }

    public void setBuilder(final SearchViewModelBuilder<V> builder) {
        this.builder = builder;
    }

    /**
     * Vérifie l'égalité de deux filtres. Deux filtres sont égaux s'ils ont le même nom
     * @param o
     * Filtre
     * @return
     * Vrai si le filtre est égal, faux sinon
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SearchFilterConfiguration<?> that = (SearchFilterConfiguration<?>) o;
        return new EqualsBuilder().append(name, that.name).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(name).toHashCode();
    }
}
