package com.kosmos.search.query.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.HasAggregations;
import org.elasticsearch.search.aggregations.bucket.missing.Missing;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kosmos.search.query.bean.aggregation.AggregationMissingModel;
import com.kosmos.search.query.bean.aggregation.AggregationModel;
import com.kosmos.search.query.bean.aggregation.AggregationSumModel;
import com.kosmos.search.query.bean.aggregation.AggregationTermModel;
import com.kosmos.search.query.bean.aggregation.BucketModel;
import com.kosmos.search.query.bean.aggregation.HasAggregationsModel;

/**
 *
 */
public class SearchAggregationBean implements Iterable<AggregationModel> {

    public static final String TOTAL = "total";

    private final Collection<AggregationModel> aggregations;

    @JsonIgnore
    private final Map<String, Aggregation> internalAggregations;

    public SearchAggregationBean(final SearchResponse response) {
        this.aggregations = new ArrayList<>();
        final Aggregations responseAggregations = response.getAggregations();
        if (responseAggregations != null) {
            for (final Aggregation aggregation : responseAggregations) {
                if (HasAggregations.class.isAssignableFrom(aggregation.getClass())) {
                    for (Aggregation itemAggregation : ((HasAggregations) aggregation).getAggregations()) {
                        addRootAggregation(itemAggregation);
                    }
                } else {
                    addRootAggregation(aggregation);
                }
            }
            this.internalAggregations = responseAggregations.asMap();
        } else {
            this.internalAggregations = null;
        }
    }

    protected void addRootAggregation(Aggregation aggregation) {
        AggregationModel aggregationModel = processAggregation(aggregation);
        if (aggregationModel != null) {
            this.aggregations.add(aggregationModel);
        }
    }

    @Override
    public Iterator<AggregationModel> iterator() {
        return this.aggregations.iterator();
    }

    protected AggregationModel processAggregation(final Aggregation aggregation) {
        final AggregationModel aggregationModel;
        if (Missing.class.isAssignableFrom(aggregation.getClass())) {
            aggregationModel = processMissingAggregation((Missing) aggregation);
        } else if (Terms.class.isAssignableFrom(aggregation.getClass())) {
            aggregationModel = processTermsAggregation((Terms) aggregation);
        } else if (Sum.class.isAssignableFrom(aggregation.getClass())) {
            aggregationModel = processSumAggregation((Sum) aggregation);
        } else {
            throw new IllegalArgumentException("Aggregation type not supported yet: " + aggregation.getClass());
        }
        return aggregationModel;
    }

    protected void processSubAggregation(HasAggregationsModel hasAggregationsModel, Aggregations aggregations) {
        if (null != aggregations && CollectionUtils.isNotEmpty(aggregations.asList())) {
            Collection<AggregationModel> subAggregationsModels = new ArrayList<>(aggregations.asList().size());
            for (Aggregation subAggregation : aggregations) {
                AggregationModel subAggregationModel = processAggregation(subAggregation);
                if (subAggregationModel != null) {
                    subAggregationsModels.add(subAggregationModel);
                }
            }
            hasAggregationsModel.setAggregations(subAggregationsModels);
        }
    }

    private AggregationMissingModel processMissingAggregation(final Missing aggregation) {
        final long docCount = aggregation.getDocCount();
        AggregationMissingModel aggregationModel = null;
        if (docCount > 0L) {
            aggregationModel = new AggregationMissingModel(aggregation.getName());
            aggregationModel.setDocCount(docCount);
            processSubAggregation(aggregationModel, aggregation.getAggregations());
        }
        return aggregationModel;
    }

    private AggregationTermModel processTermsAggregation(final Terms aggregation) {
        String facetName = aggregation.getName();
        AggregationTermModel aggregationTermModel = new AggregationTermModel(facetName);
        aggregationTermModel.setOtherDocCount(aggregation.getSumOfOtherDocCounts());
        Collection<BucketModel> bucketModels = new ArrayList<>(aggregation.getBuckets().size());
        for (final Terms.Bucket bucket : aggregation.getBuckets()) {
            BucketModel bucketModel = new BucketModel();
            bucketModel.setKey(bucket.getKeyAsString());
            bucketModel.setDocCount(bucket.getDocCount());
            processSubAggregation(bucketModel, bucket.getAggregations());
            bucketModels.add(bucketModel);
        }
        aggregationTermModel.setBuckets(bucketModels);
        return aggregationTermModel;
    }

    private AggregationSumModel processSumAggregation(final Sum aggregation) {
        AggregationSumModel aggregationSumModel = new AggregationSumModel(aggregation.getName());
        aggregationSumModel.setValue(aggregation.getValue());
        return aggregationSumModel;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SIMPLE_STYLE);
    }
}