package com.kosmos.search.query.view.bean;

import java.io.Serializable;

/**
 * Created by cpoisnel on 19/12/16.
 */
public class SearchFilterModel implements Serializable {

    private static final long serialVersionUID = -3930200090406896153L;

    private String key;


    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }
}
