package com.kosmos.search.query.configuration;

/**
 * Configuration d'aggregation liée à une valeur d'un champ.
 * Created by cpoisnel on 19/12/16.
 */
public class SearchAggregationValueConfiguration extends SearchAggregationConfiguration implements SearchFilterValueConfiguration{

    private static final long serialVersionUID = -8102598385125970120L;

    /**
     * Adresse du champ à rechercher.
     */
    private String field;

    /**
     * Bucket dans lequel les documents qui ne correspondent à aucune valeur pour ce champ seront rengés (non défini par défaut).
     */
    private String missing;

    @Override
    public String getField() {
        return field;
    }

    @Override
    public void setField(final String field) {
        this.field = field;
    }

    public String getMissing() {
        return missing;
    }

    public void setMissing(final String missing) {
        this.missing = missing;
    }
}
