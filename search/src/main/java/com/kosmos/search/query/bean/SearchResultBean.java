package com.kosmos.search.query.bean;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.elasticsearch.action.search.SearchResponse;

import com.kosmos.search.query.util.QueryUtil;

/**
 * Classe de résultats de recherche.
 * @author cpoisnel
 */
public class SearchResultBean {

    /**
     * Les documents Elasticsearch retournés.
     */
    private final Collection<SearchResultDoc> docs;

    /**
     * Résultats des aggrégations (facettes).
     */
    private final SearchAggregationBean aggregations;

    /**
     * Options de recherche utilisées (si défini).
     */
    private final SearchOptions searchOptions;

    /**
     * Nombre total de résultats (attention, ne pas confondre avec le nombre de résultats demandés (pagination)).
     */
    private final long total;

    /**
     * Temps pris en ms pour effectuer la requête Elasticsearch.
     */
    private final long tookInMillis;

    /**
     * Code HTTP du résultat de l'appel à Elasticsearch.
     */
    private final int status;

    /**
     * Constructeur d'un bean modèle de résultat Elasticsearch.
     * @param response
     * Response Elasticsearch
     * @param searchOptions
     * Options de recherche
     */
    public SearchResultBean(final SearchResponse response,final SearchOptions searchOptions) {
        this.aggregations = new SearchAggregationBean(response);
        this.total = response.getHits().totalHits();
        this.docs = QueryUtil.convertToDocs(response.getHits());
        this.tookInMillis = response.getTookInMillis();
        this.status = response.status().getStatus();
        this.searchOptions = searchOptions;
    }

    public Collection<SearchResultDoc> getDocs() {
        return docs;
    }

    public long getTotal() {
        return total;
    }

    public final long getTookInMillis() {
        return tookInMillis;
    }

    public final int getStatus() {
        return status;
    }

    public SearchAggregationBean getAggregations() {
        return this.aggregations;
    }

    /**
     * Affichage
     * @return
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}
