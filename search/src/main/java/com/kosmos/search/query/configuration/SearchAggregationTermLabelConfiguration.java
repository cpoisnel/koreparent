package com.kosmos.search.query.configuration;

/**
 * Configuration d'une aggregation par Terme, avec les labels du CMS.
 * Created by cpoisnel on 19/12/16.
 */
public class SearchAggregationTermLabelConfiguration extends SearchAggregationTermConfiguration {

    private static final long serialVersionUID = 5148844867614908320L;

    private String title;

    private String labelType;

    /**
     * Constructeur.
     */
    public SearchAggregationTermLabelConfiguration() {
        // TODO : La vue sera portée par Tiles.
        this.setView("/WEB-INF/jsp/query/fo/aggregation/aggregationTermLabelView.jsp");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getLabelType() {
        return labelType;
    }

    public void setLabelType(final String labelType) {
        this.labelType = labelType;
    }
}
