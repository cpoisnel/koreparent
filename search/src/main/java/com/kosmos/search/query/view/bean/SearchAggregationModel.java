package com.kosmos.search.query.view.bean;

/**
 * Created by cpoisnel on 19/12/16.
 */
public class SearchAggregationModel extends SearchFilterModel {

    private static final long serialVersionUID = -2140357879668717855L;



    private String name;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
