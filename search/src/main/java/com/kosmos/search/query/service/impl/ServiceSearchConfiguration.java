package com.kosmos.search.query.service.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.search.exception.SearchException;
import com.kosmos.search.query.configuration.SearchFieldConfiguration;
import com.kosmos.search.query.configuration.SearchCommonConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kosmos.search.query.configuration.SearchFilterConfiguration;
import com.kosmos.search.query.configuration.SearchIndexConfiguration;
import com.kosmos.search.query.configuration.SearchPluginConfiguration;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.cms.objetspartages.annotation.Label;
import com.kportal.cms.objetspartages.annotation.Rubrique;
import com.kportal.cms.objetspartages.annotation.User;
import com.kportal.extension.module.plugin.objetspartages.IPluginFiche;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper;
import com.univ.objetspartages.bean.PersistenceBean;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.FicheUnivHelper;

/**
 * Service permettant de constituer et d'utiliser les configurations de recherche (paramètres de fiche recherchés, boost, aggrégations,...).
 *
 * Created by cpoisnel on 28/12/16.
 */
public class ServiceSearchConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceSearchConfiguration.class);

    protected static final Collection<String> IGNORED_FIELDS = Arrays.asList("serialVersionUID");

    public static final String PREFIX_FICHE = "fiche";

    public static final String PREFIX_PLUGIN = "plugins";

    public static final String PREFIX_PLUGIN_ITEMS = "items";

    public static final String FIELD_SEPARATOR = ".";

    /**
     * Récupération de la configuration mergée de fiches filtrées par nom d'objet.
     * @param objectNames
     * Noms des objets (FicheUniv)
     * @return une configuration de fiche, fusion de toutes les configurations de fiche applicables
     */
    public SearchFicheConfiguration fetchMergedFilteredConfiguration(final String... objectNames) {
        // Récupérer l'ensemble des configurations de fiche
        Collection<SearchFicheConfiguration> searchFicheConfigurationList = this.fetchFilteredConfigurations(objectNames);
        // Si pas de résultat, on soulève une exception
        if (CollectionUtils.isEmpty(searchFicheConfigurationList)) {
            LOG.error("Aucune configuration de recherche avec les noms d'objets {}", Arrays.asList(objectNames));
            throw new SearchException("Impossible d'effectuer la recherche, aucune configuration de recherche");
        }
        SearchFicheConfiguration searchFicheConfiguration = new SearchFicheConfiguration();
        Map<String, SearchFieldConfiguration> ficheFields = new HashMap<>();
        for (SearchFicheConfiguration searchFicheItemConfiguration : searchFicheConfigurationList) {
            searchFicheConfiguration.getIndex().addAll(searchFicheItemConfiguration.getIndex());
            searchFicheConfiguration.getFilters().addAll(searchFicheItemConfiguration.getFilters());
            // On parcourt les configurations de champs pour ne les ajouter qu'une et une seule fois (il n'est pas possible de faire une différenciation).
            for (SearchFieldConfiguration itemField : searchFicheItemConfiguration.getFields().values()) {
                if (ficheFields.containsKey(itemField.getFieldName())) {
                    SearchFieldConfiguration oldField = ficheFields.get(itemField.getFieldName());
                    // On prend celui qui a le boost le plus élevé.
                    if (oldField.compareTo(itemField) < 0) {
                        ficheFields.put(itemField.getFieldName(), itemField);
                    }
                } else {
                    ficheFields.put(itemField.getFieldName(), itemField);
                }
            }
        }
        searchFicheConfiguration.setFields(ficheFields);
        return searchFicheConfiguration;
    }

    /**
     * Récupération des configurations de fiches filtrées par nom d'objet.
     * @param objectNames
     * Noms des objets (FicheUniv)
     * @return les configurations de recherche de fiche
     */
    public Collection<SearchFicheConfiguration> fetchFilteredConfigurations(final String... objectNames) {
        Collection<SearchFicheConfiguration> searchFicheConfigurationList = fetchAllConfigurationsBean();
        if (objectNames.length == 0) {
            return searchFicheConfigurationList;
        }
        Collection<String> objetNamesList = Arrays.asList(objectNames);
        Collection<SearchFicheConfiguration> filteredConfigurations = new ArrayList<>(objectNames.length);
        for (SearchFicheConfiguration searchFicheConfiguration : searchFicheConfigurationList) {
            for (SearchIndexConfiguration searchIndexConfiguration : searchFicheConfiguration.getIndex()) {
                if (objetNamesList.contains(searchIndexConfiguration.getObjectName())) {
                    filteredConfigurations.add(searchFicheConfiguration);
                }
            }
        }
        return filteredConfigurations;
    }

    /**
     * Methode pour charger toutes les configurations pour chaque index.
     * On itère sur toutes les fiches indexables.
     * Cette méthode inititalise les beans avec des valeurs par défaut puis merge les surcharges si il y en a.
     * @return configurationEsBeanList : liste de configuration
     */
    @Cacheable(value = "ServiceSearcher.fetchAllConfigurationsBean")
    public List<SearchFicheConfiguration> fetchAllConfigurationsBean() {
        LOG.debug("Calcul des configurations de recherche");
        List<SearchFicheConfiguration> searchFicheConfigurationList = new ArrayList<>();
        for (Objetpartage objetpartage : ReferentielObjets.getObjetsPartagesTries()) {
            // On va d'abord chercher la configuration de recherche, si elle existe (valeur de l'autoscan)
            SearchFicheConfiguration searchFicheConfiguration = fetchSearchConfiguration(objetpartage.getNomObjet());
            SearchFicheConfiguration defaultSearchFicheConfiguration = null;
            if (null == searchFicheConfiguration) {
                searchFicheConfiguration = fetchDefaultSearchConfiguration();
                searchFicheConfiguration.setIndex(new SearchIndexConfiguration(objetpartage.getNomObjet(), 1.0F));
            }
            // La configuration de recherche ne peut pas être nulle
            // On vérifie que la configuration est paramétrée pour chercher les attributs par introspection
            if (searchFicheConfiguration.isAutoscan()) {
                defaultSearchFicheConfiguration = new SearchFicheConfiguration(objetpartage.getNomObjet());
                final FicheUniv fiche = FicheUnivHelper.instancierFiche(objetpartage.getNomObjet());
                PersistenceBean ficheBean = ((AbstractOm) fiche).getPersistenceBean();
                scanDefaultConfiguration(defaultSearchFicheConfiguration, ficheBean.getClass());
            } else {
                LOG.debug("Autoscan désactivé pour la configuration de recherche de l'objet {}", objetpartage.getNomObjet());
            }
            // Ajout de la configuration des plugins
            mergePluginsConfiguration(searchFicheConfiguration);
            // Merge de la configuration du bean avec celui déclaré en Spring
            mergeOverridedConfiguration(searchFicheConfiguration, defaultSearchFicheConfiguration);
            LOG.debug("Configuration de recherche initialisée {}", searchFicheConfiguration);
            searchFicheConfigurationList.add(searchFicheConfiguration);
        }
        return searchFicheConfigurationList;
    }

    protected SearchFicheConfiguration initDefaultConfiguration(Objetpartage objetpartage) {
        SearchFicheConfiguration searchFicheConfiguration = null;
        // On manipule les ficheuniv pour l'instanciation, mais on ne veut plus les utiliser à terme (on utilise bien le bean)
        final FicheUniv fiche = FicheUnivHelper.instancierFiche(objetpartage.getNomObjet());
        if (FicheAnnotationHelper.isIndexable(fiche)) {
            searchFicheConfiguration = new SearchFicheConfiguration(objetpartage.getNomObjet());
            PersistenceBean ficheBean = ((AbstractOm) fiche).getPersistenceBean();
            scanDefaultConfiguration(searchFicheConfiguration, ficheBean.getClass());
        }
        return searchFicheConfiguration;
    }

    /**
     * Merge la configuration de plugin avec celle de la recherche.
     * @param searchFicheConfiguration
     * Configuration de recherche à fusionner
     */
    protected void mergePluginsConfiguration(final SearchFicheConfiguration searchFicheConfiguration) {
        // Plugins activés par objet
        for (SearchIndexConfiguration indexConfiguration : searchFicheConfiguration.getIndex()) {
            String objet = indexConfiguration.getObjectName();
            Collection<IPluginFiche> plugins = PluginFicheHelper.getPluginsByObjet(objet);
            for (IPluginFiche plugin : plugins) {
                // Vérification qu'il n'existe pas une configuration de plugin associée à ce plugin ?
                // Configuration du plugin
                // On prend l'ID comme nom de d'objet
                SearchPluginConfiguration searchObjectConfiguration = this.fetchPluginSearchConfiguration(plugin.getId());
                if (null != searchObjectConfiguration) {
                    SearchPluginConfiguration defaultPluginConfiguration = null;
                    if (searchObjectConfiguration.isAutoscan()) {
                        defaultPluginConfiguration = scanDefaultPluginConfiguration(searchObjectConfiguration.getType(), plugin.getId());
                    }
                    this.mergeOverridedConfiguration(searchFicheConfiguration, defaultPluginConfiguration);
                }
            }
        }
    }

    /**
     * Constitution de la configuration par défaut des fiches.
     * On fonctionne par introspection.
     * Par défaut la configuration suivante est valable pour tous les champs :
     * - indexable
     * - boost = 1
     * - highlight = true
     *<p>
     *     Si un projet désire gérer de manière générique les recherches par introspection, il peut surcharger cette méthode.
     *</p>
     *
     * @param type
     * Bean serializable
     */
    protected void scanDefaultConfiguration(SearchFicheConfiguration searchFicheConfiguration, Class<?> type) {
        Map<String, SearchFieldConfiguration> fieldEsBeanMap = new HashMap<>();
        // On ne gère pas l'ensemble des éléments d'une fiche
        for (Field field : FieldUtils.getAllFieldsList(type)) {
            // Tous les champs ne peuvent pas être automatiquement gérés (on ne traite que les String)
            if (!String.class.isAssignableFrom(field.getType()) || IGNORED_FIELDS.contains(field.getName())) {
                continue;
            }
            if (field.isAnnotationPresent(Label.class)) {
                String fieldName = PREFIX_FICHE + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "label_value";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            } else if (field.isAnnotationPresent(Rubrique.class)) {
                String fieldName = PREFIX_FICHE + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "rubrique_value";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            } else if (field.isAnnotationPresent(User.class)) {
                String fieldName = PREFIX_FICHE + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "user_name";
                String fieldName2 = PREFIX_FICHE + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "user_first_name";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
                fieldEsBeanMap.put(fieldName2, new SearchFieldConfiguration(fieldName2));
            } else {
                String fieldName = PREFIX_FICHE + FIELD_SEPARATOR + field.getName();
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            }
            // L'annotation @Fiche n'est pas gérée automatiquement
        }
        searchFicheConfiguration.setFields(fieldEsBeanMap);
    }

    /**
     * Constitution de la configuration par défaut d'un plugin.
     * On fonctionne par introspection.
     * Par défaut la configuration suivante est valable pour tous les champs :
     * - indexable
     * - highlight = false
     * <p>
     *     Si un projet désire gérer de manière générique les recherches par introspection, il peut surcharger cette méthode.
     *</p>
     * @param type Classe à scanner
     * @param pluginName Nom du plugin (habituellement en minuscule)
     * @return la configuration de recherche de plugin de fiche
     */
    protected SearchPluginConfiguration scanDefaultPluginConfiguration(Class<?> type, String pluginName) {
        SearchPluginConfiguration searchConfiguration = new SearchPluginConfiguration();
        Map<String, SearchFieldConfiguration> fieldEsBeanMap = new HashMap<>();
        // On ne gère pas l'ensemble des éléments d'une fiche
        for (Field field : FieldUtils.getAllFieldsList(type)) {
            // Tous les champs ne peuvent pas être automatiquement gérés (on ne traite que les String)
            if (!String.class.isAssignableFrom(field.getType()) || IGNORED_FIELDS.contains(field.getName())) {
                continue;
            }
            if (field.isAnnotationPresent(Label.class)) {
                String fieldName = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "label_value";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            } else if (field.isAnnotationPresent(Rubrique.class)) {
                String fieldName = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "rubrique_value";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            } else if (field.isAnnotationPresent(User.class)) {
                String fieldName = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "user_name";
                String fieldName2 = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "user_first_name";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
                fieldEsBeanMap.put(fieldName2, new SearchFieldConfiguration(fieldName2));
            } else {
                String fieldName = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName();
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            }
            // L'annotation @Fiche n'est pas gérée automatiquement
        }
        searchConfiguration.setFields(fieldEsBeanMap);
        return searchConfiguration;
    }

    /**
     * Récupérer la configuration de recherche d'une fiche à partir un nom d'objet.
     * @param objectName
     * Nom d'objet
     * @return la configuration de recherche correspondant au nom d'objet
     */
    protected SearchFicheConfiguration fetchSearchConfiguration(String objectName) {
        Map<String, SearchFicheConfiguration> contextSearchConfigurationMap = ApplicationContextManager.getAllBeansOfType(SearchFicheConfiguration.class);
        for (SearchFicheConfiguration searchFicheConfiguration : contextSearchConfigurationMap.values()) {
            for (SearchIndexConfiguration searchIndexConfiguration : searchFicheConfiguration.getIndex()) {
                if (objectName.equals(searchIndexConfiguration.getObjectName())) {
                    return searchFicheConfiguration;
                }
            }
        }
        return null;
    }

    protected SearchFicheConfiguration fetchDefaultSearchConfiguration() {
        SearchFicheConfiguration contextSearchConfigurationMap = ApplicationContextManager.getBean("core", "searchDefaultFicheConfiguration", SearchFicheConfiguration.class);
        if (null == contextSearchConfigurationMap) {
            throw new SearchException("La configuration de recherche par défaut n'est pas définie");
        }
        return contextSearchConfigurationMap;
    }

    /**
     * Récupérer la configuration de recherche de plugin à partir d'un nom de plugin.
     * @param pluginId
     *  ID du plugin
     * @return la configuration de plugin
     */
    protected SearchPluginConfiguration fetchPluginSearchConfiguration(String pluginId) {
        Map<String, SearchPluginConfiguration> contextSearchConfigurationMap = ApplicationContextManager.getAllBeansOfType(SearchPluginConfiguration.class);
        for (SearchPluginConfiguration searchConfiguration : contextSearchConfigurationMap.values()) {
            if (pluginId.equals(searchConfiguration.getId())) {
                return searchConfiguration;
            }
        }
        return null;
    }

    /**
     * Merge des configurations cible et défaut. La configuration cible est étendue de celle par défaut s'il n'y a pas de valeur définie.
     * @param searchConfiguration
     * Configuration de recherche cible (qui aura le merge de la configuration par défaut)
     * @param defaultSearchConfiguration
     * Configuration par défaut utilisée
     */
    protected void mergeOverridedConfiguration(SearchCommonConfiguration searchConfiguration, SearchCommonConfiguration defaultSearchConfiguration) {
        // S'il n'y a pas de configuration par défaut, ce n'est pas nécessaire de merger
        if (null != defaultSearchConfiguration) {
            // Pas de recopie sur le boost, par défaut, il est à 1
            // Suppression des champs que l'on ne souhaite pas rechercher
            for (String excludedField : defaultSearchConfiguration.getExcludedFields()) {
                if (!searchConfiguration.getExcludedFields().contains(excludedField)) {
                    searchConfiguration.getExcludedFields().add(excludedField);
                }
            }
            //Merge des données propres à chaque field
            for (Map.Entry<String, SearchFieldConfiguration> entryField : defaultSearchConfiguration.getFields().entrySet()) {
                // Si la clé n'existe pas dans la configuration cible, alors on prend celle par défaut
                if (!searchConfiguration.getFields().containsKey(entryField.getKey())) {
                    searchConfiguration.getFields().put(entryField.getKey(), entryField.getValue());
                }
            }
        }
        // Suppression des fields exclus
        for (String excludedField : searchConfiguration.getExcludedFields()) {
            searchConfiguration.getFields().remove(excludedField);
        }
    }

    /**
     * Merge des configurations cible et défaut. La configuration cible est étendue de celle par défaut s'il n'y a pas de valeur définie.
     * @param searchFicheConfiguration
     * Configuration de recherche cible (qui aura le merge de la configuration par défaut)
     * @param defaultSearchFicheConfiguration
     * Configuration par défaut utilisée
     */
    protected void mergeOverridedConfiguration(SearchFicheConfiguration searchFicheConfiguration, SearchFicheConfiguration defaultSearchFicheConfiguration) {
        this.mergeOverridedConfiguration((SearchCommonConfiguration) searchFicheConfiguration, (SearchCommonConfiguration) defaultSearchFicheConfiguration);
        // S'il n'y a pas de configuration par défaut, ce n'est pas nécessaire de merger
        if (null != defaultSearchFicheConfiguration) {
            for (SearchFilterConfiguration entryFilter : defaultSearchFicheConfiguration.getFilters()) {
                // Si la clé n'existe pas dans la configuration cible, alors on prend celle par défaut
                if (!searchFicheConfiguration.getFilters().contains(entryFilter)) {
                    searchFicheConfiguration.getFilters().add(entryFilter);
                }
            }
        }
    }
}
