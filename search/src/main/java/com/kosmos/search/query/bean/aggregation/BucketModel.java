package com.kosmos.search.query.bean.aggregation;

import java.util.Collection;

/**
 * Created by cpoisnel on 30/12/16.
 */
public class BucketModel implements HasAggregationsModel {

    private String key;

    private Long docCount;

    private Collection<AggregationModel> aggregations;

    public BucketModel() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public Long getDocCount() {
        return docCount;
    }

    public void setDocCount(final Long docCount) {
        this.docCount = docCount;
    }

    @Override
    public Collection<AggregationModel> getAggregations() {
        return aggregations;
    }


    public void setAggregations(final Collection<AggregationModel> aggregations) {
        this.aggregations = aggregations;
    }
}
