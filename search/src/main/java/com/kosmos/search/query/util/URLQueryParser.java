package com.kosmos.search.query.util;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


import org.apache.commons.lang3.StringUtils;
import org.springframework.util.LinkedMultiValueMap;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

/**
 * URL Parser.
 *
 * Created by cpoisnel on 15/12/16.
 */
public class URLQueryParser {

    /**
     * Private constructor
     */
    private URLQueryParser() {
        // No Public constructor
    }

    public static Map<String,List<String>> toMultimap(Map<String, String[]> map) {
        Map<String,List<String>> multimap = new LinkedMultiValueMap(map.size());
        for(Map.Entry<String,String[]> entry : map.entrySet()) {
            multimap.put(entry.getKey(),Arrays.asList(entry.getValue()));
        }
        return  multimap;
    }


    /**
     * Split query.
     *
     * <p>
     *     From : http://stackoverflow.com/questions/13592236/parse-a-uri-string-into-name-value-collection
     * </p>
     * @param url
     * @return Map of params
     * @throws UnsupportedEncodingException
     */
    public static Multimap<String, String> splitQuery(URL url) throws UnsupportedEncodingException {
        final Multimap<String, String> queryPairs = LinkedListMultimap.create();
        String query = url.getQuery();
        if (StringUtils.isNotEmpty(query)) {
            final String[] pairs = query.split("&");
            for (String pair : pairs) {
                final int idx = pair.indexOf('=');
                final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
                final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
                queryPairs.put(key, value);
            }
        }
        return queryPairs;
    }
}
