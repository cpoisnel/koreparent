package com.kosmos.search.query.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.highlight.HighlightField;

import com.kosmos.search.query.bean.SearchResultDoc;
import com.kosmos.search.query.bean.SearchResultDoc.HighlightedResult;

/**
 * Utilitaire pour la recherche.
 * @author cpoisnel
 *
 */
public class QueryUtil {

    /**
     * Conversion de résultats de recherche Elasticsearch.
     * @param hits
     * @return
     */
    public static List<SearchResultDoc> convertToDocs(final SearchHits hits) {
        final List<SearchResultDoc> docs = new ArrayList<>();
        for (final SearchHit hit : hits.getHits()) {
            final SearchResultDoc resultat = new SearchResultDoc();
            resultat.setScore(hit.getScore());
            resultat.setId(hit.getId());
            final Map<String, Object> source = hit.getSource();
            if (source != null) {
                resultat.setSource(source);
                // Spécificque Fiche Core
                resultat.setTypeFiche(String.valueOf(source.get("type_fiche")));
                resultat.setCodeObjet(String.valueOf(source.get("code_objet")));
            }

            resultat.setHighlightedFields(convertToHighlightedResults(hit.getHighlightFields()));

            final Map<String, SearchHits> innerHits = hit.getInnerHits();
            if (MapUtils.isNotEmpty(innerHits)) {
                final Map<String, List<SearchResultDoc>> innerDocs = new HashMap<>();
                for (final Map.Entry<String, SearchHits> innerHit : innerHits.entrySet()) {
                    innerDocs.put(innerHit.getKey(), convertToDocs(innerHit.getValue()));
                }
                resultat.setInnerDocs(innerDocs);
            }

            docs.add(resultat);
        }
        return docs;
    }

    /**
     * Convertisseurs de highlights de résultats
     * @param highlights
     * Highlights Elasticsearch
     * @return
     * Modèle d'highlights
     */
    public static Map<String, HighlightedResult> convertToHighlightedResults(final Map<String, HighlightField> highlights) {
        final Map<String, HighlightedResult> highlightsModels = new HashMap<>();
        for (final Map.Entry<String, HighlightField> entry : highlights.entrySet()) {
            final HighlightField hField = entry.getValue();
            final Collection<String> fragments = new ArrayList<String>();
            if (ArrayUtils.isNotEmpty(hField.getFragments())) {
                for (final Text fragment : hField.getFragments()) {
                    fragments.add(fragment.string());
                }
            }
            highlightsModels.put(entry.getKey(), new HighlightedResult(hField.getName(), fragments));
        }

        return highlightsModels;
    }
}
