package com.kosmos.search.query.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchAction;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.HasChildQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RegexpQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.query.support.QueryInnerHitBuilder;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.kosmos.search.exception.SearchException;
import com.kosmos.search.index.service.fiche.impl.ServiceAccessControlFiche;
import com.kosmos.search.query.configuration.SearchFieldConfiguration;
import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.SearchResultBean;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kosmos.search.query.configuration.SearchFilterConfiguration;
import com.kosmos.search.query.configuration.SearchIndexConfiguration;
import com.kosmos.search.query.configuration.SearchObjectConfiguration;
import com.kosmos.search.query.service.ServiceSearcher;
import com.kosmos.search.util.IndexerUtil;
import com.univ.objetspartages.om.EtatFiche;

/**
 * Service de recherche (Elasticsearch).
 * @author cpoisnel
 *
 */
public class ServiceSearcherImpl implements ServiceSearcher {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceSearcherImpl.class);

    @Autowired
    private ServiceAccessControlFiche serviceAccessControlFiche;

    @Autowired
    private ServiceSearchConfiguration serviceSearchConfiguration;

    private Client client;

    private TimeValue timeout;

    /**
     * Configuration de recherche des ressources (global pour l'ensemble des fiches).
     */
    private SearchObjectConfiguration ressourceConfiguration;

    /**
     * Constructeur par défaut.
     */
    public ServiceSearcherImpl(){
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResultBean search(final SearchOptions searchOptions) {
        if (null == client) {
            throw new SearchException("Client Elasticsearch indisponible");
        }

        // Merge l'ensemble des configurations de fiches en une seule !
        SearchFicheConfiguration searchFicheConfiguration = this.serviceSearchConfiguration.fetchMergedFilteredConfiguration(searchOptions.getFilters().get("types"));

        // Builder de requête Elasticsearch
        SearchRequestBuilder searchRequestBuilder = this.buildSearchRequestBuilder();
        searchRequestBuilder.setSize(searchOptions.getLimit()).setFrom(searchOptions.getOffset());
        addSort(searchRequestBuilder, searchOptions);
        addBooleanQueryBuilder(searchRequestBuilder, searchFicheConfiguration, searchOptions);
        // Recherche sur les indices
        if (LOG.isInfoEnabled()) {
            LOG.info("Requête ElasticSearch :\n{}", searchRequestBuilder);
        }
        return executeQuery(searchRequestBuilder,searchOptions);
    }

    /**
     * Initialisation de la requête générale (query)
     * <p>
     *     Parcourt la liste des configurations de fiche et effectue :
     *     <ul>
     *         <li>Définition des index</li>
     *         <li>Ajout des champs recherchés (avec pondération</li>
     *         <li>Ajout des filtres de recherche (obligatoire - DSI / Etat fiche - ou non)</li>
     *     </ul>
     * </p>
     * @param searchRequestBuilder
     * Requête Elasticsearch
     * @param searchFicheConfiguration
     * Configuration de fiche
     * @param searchOptions
     * Options de recherche
     */
    protected void addBooleanQueryBuilder(SearchRequestBuilder searchRequestBuilder, SearchFicheConfiguration searchFicheConfiguration, SearchOptions searchOptions) {
        Set<String> indices = new HashSet<>();
        Set<String> queryTerms = new HashSet<>();
        Set<SearchFieldConfiguration> searchFieldConfigurations = new HashSet<>();
        // Parcours des configurations de fiche

            // Gestion de l'index
        for (SearchIndexConfiguration indexConfiguration : searchFicheConfiguration.getIndex()) {
            String indexName = this.buildIndexName(indexConfiguration, searchOptions);
            indices.add(indexName);
            if (indexConfiguration.getBoost() != 1.0F) {
                searchRequestBuilder.addIndexBoost(indexName, indexConfiguration.getBoost());
            }

        }
        // Gestion de la recherche
        for (Map.Entry<String, SearchFieldConfiguration> field : searchFicheConfiguration.getFields().entrySet()) {
            // Gérer le cas même chemin, boost différent
            searchFieldConfigurations.add(field.getValue());
        }
        addAggregation(searchRequestBuilder, searchFicheConfiguration);
        for (SearchFieldConfiguration searchFieldConfiguration : searchFieldConfigurations) {
            String fieldName = searchFieldConfiguration.getFieldName();
            final double boost = searchFieldConfiguration.getBoost();
            if (boost != 1.0F) {
                fieldName += "^" + boost;
            }
            queryTerms.add(fieldName);
            if (searchFieldConfiguration.isHighLight()) {
                searchRequestBuilder.addHighlightedField(fieldName);
            }
        }
        // Traitement requête
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (StringUtils.isNotEmpty(searchOptions.getQuery())) {
            boolQueryBuilder.should(QueryBuilders.multiMatchQuery(searchOptions.getQuery(), queryTerms.toArray(new String[queryTerms.size()])));
        } else {
            boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        }
        addChildQuery(boolQueryBuilder, searchFicheConfiguration, searchOptions);
        // Ajout des filtres
        boolQueryBuilder.filter(buildFilterQuery(searchOptions, searchFicheConfiguration));
        boolQueryBuilder.minimumShouldMatch("1");



        searchRequestBuilder.setIndices(indices.toArray(new String[indices.size()]));
        if (LOG.isInfoEnabled()) {
            LOG.info("Execution de la requête sur l'index et type : /{}/{}/", StringUtils.join(indices.toArray(new String[indices.size()]), ","), IndexerUtil.TYPE_FICHE);
        }
        // Ajout de la query
        searchRequestBuilder.setQuery(boolQueryBuilder);
    }

    /**
     * Calcul du nom d'indice à partir de la configuration et des paramètres d'appels.
     * @param searchIndexConfiguration
     * Configuration de fiche
     * @param searchOptions
     * Options de recherche
     * @return le nom de l'index utilisé.
     */
    protected String buildIndexName(SearchIndexConfiguration searchIndexConfiguration, SearchOptions searchOptions) {
         return IndexerUtil.INDEX_FICHE + IndexerUtil.INDEX_SEPARATEUR + searchIndexConfiguration.getObjectName().toLowerCase() + IndexerUtil.INDEX_SEPARATEUR + searchOptions.getLanguage();
    }

    /**
     * Ajout de la requête interne pour les resssources.
     * @param boolQueryBuilder
     * Builder de requête (global)
     * @param searchFicheConfiguration
     * Configuration de recherche de fiche
     * @param searchOptions
     * Options de recherche
     */
    protected void addChildQuery(final BoolQueryBuilder boolQueryBuilder, SearchFicheConfiguration searchFicheConfiguration, SearchOptions searchOptions) {
        // Traitement des ressources
        if (null != ressourceConfiguration && StringUtils.isNotEmpty(searchOptions.getQuery())) {
            final Set<String> childFieldNames = new HashSet<>();
            for (Map.Entry<String, SearchFieldConfiguration> searchFieldBeanEntry : ressourceConfiguration.getFields().entrySet()) {
                SearchFieldConfiguration searchFieldConfiguration = searchFieldBeanEntry.getValue();
                String fieldName = searchFieldBeanEntry.getKey();
                final float boost = searchFieldConfiguration.getBoost();
                if (boost > 1.0F) {
                    fieldName += "^" + boost;
                }
                childFieldNames.add(fieldName);
            }
            final MultiMatchQueryBuilder childQueryBuilder = QueryBuilders.multiMatchQuery(searchOptions.getQuery(), childFieldNames.toArray(new String[childFieldNames.size()]));
            final HasChildQueryBuilder childQuery = new HasChildQueryBuilder(IndexerUtil.TYPE_RESSOURCE, childQueryBuilder);
            childQuery.scoreMode("max");
            final QueryInnerHitBuilder innerHit = new QueryInnerHitBuilder();
            innerHit.setNoFields();
            final HighlightBuilder chb = innerHit.highlightBuilder();
            for (String field : ressourceConfiguration.getFields().keySet()) {
                chb.field(field, 100, 1);
                chb.order("score");
                chb.preTags("<mark>");
                chb.postTags("</mark>");
            }
            childQuery.innerHit(innerHit);
            boolQueryBuilder.should(childQuery);
        }
    }

    /**
     * Ajout des aggrégations à la requête
     * @param searchRequestBuilder
     * Builder de requête Elasticsearch
     * @param searchFicheConfiguration
     * Configuration
     */
    protected void addAggregation(final SearchRequestBuilder searchRequestBuilder, final SearchFicheConfiguration searchFicheConfiguration) {
        for (SearchFilterConfiguration filterConfiguration : searchFicheConfiguration.getFilters()) {
            if (SearchAggregationConfiguration.class.isAssignableFrom(filterConfiguration.getClass())) {
                SearchAggregationConfiguration aggregationConfiguration = (SearchAggregationConfiguration) filterConfiguration;
                searchRequestBuilder.addAggregation(aggregationConfiguration.getAggregationBuilder().build(aggregationConfiguration));
            }
        }
    }

    /**
     * Exécution de la requête Elasticsearch sur le client.
     * @param searchRequestBuilder
     *   Builder de requête Elasticsearch
     * @return Un bean de resultat constitué.
     */
    protected SearchResultBean executeQuery(SearchRequestBuilder searchRequestBuilder,SearchOptions searchOptions) {
        try {
            return new SearchResultBean(searchRequestBuilder.get(),searchOptions);
        } catch (final RuntimeException e) {
            throw new SearchException("Exception lors de la requête Elasticsearch avec les options", e);
        }
    }

    /**
     * Initialise la requête Elasticsearch.
     * @return
     */
    protected SearchRequestBuilder buildSearchRequestBuilder() {
        SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(client, SearchAction.INSTANCE);
        searchRequestBuilder.setHighlighterPreTags("<mark>");
        searchRequestBuilder.setHighlighterPostTags("</mark>");
        searchRequestBuilder.setHighlighterOrder("score");
        searchRequestBuilder.setTimeout(this.timeout);
        searchRequestBuilder.setTypes(IndexerUtil.TYPE_FICHE);
        searchRequestBuilder.setIndicesOptions(IndicesOptions.fromOptions(true, true, true, false));
        return searchRequestBuilder;
    }

    /**
     * Initialisation de la requête de filtres sur la requête.
     * @param searchOptions
     * Options de recherche
     * @param searchFicheConfiguration
     * Configuration de fiche
     * @return la requête de filtres (les résultats filtrés seront non affichées, et ne rentreront pas en compte dans la définition des scores)
     */
    protected BoolQueryBuilder buildFilterQuery(SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration) {
        final BoolQueryBuilder filterQueryBuilder = new BoolQueryBuilder();
        addFilterAccessControl(filterQueryBuilder, searchOptions);
        addFilterConfiguration(filterQueryBuilder, searchOptions, searchFicheConfiguration);
        addFilterState(filterQueryBuilder, searchOptions);
        return filterQueryBuilder;
    }

    /**
     * Filtrer sur les états des objets en ligne.
     * @param filterQueryBuilder
     *  Builder de requête de filtre
     * @param searchOptions
     * Options de recherche
     */
    protected void addFilterState(final BoolQueryBuilder filterQueryBuilder, final SearchOptions searchOptions) {
        final QueryBuilder filterEtatObjet = new TermQueryBuilder("fiche.etatObjet", EtatFiche.EN_LIGNE.getEtat());
        filterQueryBuilder.must(filterEtatObjet);
    }

    /**
     * Ajout des filtres pour chaque configuration (voir {@link SearchFicheConfiguration#getFilters()}).
     * @param filterQueryBuilder
     * Builder de requête de filtre
     * @param searchOptions
     * Options de recherche
     * @param searchFicheConfiguration
     * Configuration des fiches
     */
    protected void addFilterConfiguration(BoolQueryBuilder filterQueryBuilder, final SearchOptions searchOptions, final SearchFicheConfiguration searchFicheConfiguration) {
        // Gestion des filtres
        for (SearchFilterConfiguration filter : searchFicheConfiguration.getFilters()) {
            if (searchOptions.getFilters().containsKey(filter.getName())) {
                if (null !=  filter.getQueryBuilder()) {
                    QueryBuilder queryBuilder = filter.getQueryBuilder().build(filter, searchOptions.getFilters().get(filter.getName()));
                    filterQueryBuilder.must(queryBuilder);
                }
            }
        }
    }

    /**
     * Méthode d'ajout des contrôles d'accès à la requête de recherche.
     * <p>
     *     Attention, il s'agit d'une méthode permettant de gérer la DSI. La surcharge de cette méthode peut empêcher d'avoir accès aux résultats ou au contraire rendre visible des résultats à l'ensemble des utilisateurs.
     * </p>
     * @param filterQueryBuilder
     * Builder de requête de filtre
     * @param searchOptions
     * Options de recherche
     */
    protected void addFilterAccessControl(final BoolQueryBuilder filterQueryBuilder, final SearchOptions searchOptions) {
        // Ajout du filtre sur les droits
        final Set<String> groupesAutorises = searchOptions.getAuthorizedGroups();
        final Collection<String> idGroupesAutorises = new ArrayList<>(groupesAutorises.size());
        for (final String groupe : groupesAutorises) {
            idGroupesAutorises.add(this.serviceAccessControlFiche.genererIdGroupeDsiES(groupe));
        }
        final String[] idGroupesAutorisesArray = idGroupesAutorises.toArray(new String[idGroupesAutorises.size()]);

        // Filtrage sur les rubriques
        final RegexpQueryBuilder accessControlRegexQueryBuilder = new RegexpQueryBuilder(IndexerUtil.FIELD_ACCESSCONTROL + ".groupesRubrique", processAccessControlRegexGroups(idGroupesAutorisesArray));
        // TODO Foireux sur la regex
        //filterQueryBuilder.must(accessControlRegexQueryBuilder);

        // Filtrage sur les fiches
        final BoolQueryBuilder accessControlQuery = new BoolQueryBuilder();
        accessControlQuery.should(QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery(IndexerUtil.FIELD_ACCESSCONTROL + ".groupesFicheRestriction", "2")));
        accessControlQuery.should(QueryBuilders.boolQuery().must(QueryBuilders.termQuery(IndexerUtil.FIELD_ACCESSCONTROL + ".groupesFicheRestriction", "2")).must(QueryBuilders.termsQuery(IndexerUtil.FIELD_ACCESSCONTROL + ".groupesFiche", idGroupesAutorisesArray)));
        accessControlQuery.minimumShouldMatch("1");
        filterQueryBuilder.must(accessControlQuery);
    }

    /**
     *
     * Récupération de l'expression régulière correspondant aux groupes autorisés à afficher les documents de la requête.
     *
     * <pre>
     *
     * La grammaire :
     *     - les codes de groupes ne peuvent avoir que des caractères hexadécimaux compris entre a et f 0 et 9 (donc à adapter au vrai cas). La grammaires est réduite à a-f0-9[](), ceci permettant d'accélérer les recherches.
     *     - les codes de groupes sont délimités par un crochet ouvrant et un crochet fermant
     *     - les parenthèses sont utilisés pour délimiter les unions de groupes (dans notre cas les groupes contenus dans la diffusion d'une rubrique)
     *     - les expressions parenthésées qui se suivent forment des intersections d'expression soit dans notre cas des intersections d'unions de groupes
     *     - exemple : ([A])([A][AB][C][D])([AB][D])([ABC])
     *     -- traduction : A et (A ou AB ou C ou D) et (AB ou D) et ABC
     *     -- traduction fonctionnelle : (Codes groupes de diffusion rubrique 1)(Codes groupes de diffusion rubrique 2 mère de rubrique 1)(Codes groupes de diffusion rubrique 3 mère de rubrique 2)(Codes groupes de diffusion rubrique 4 mère de rubrique 3)
     *     La régex de validation :
     *     ^(\((\[[A-Z]+\])*(\[A\]|\[AB\])+(\[[A-Z]+\])*\))+$
     * </pre>
     *
     * @param authorizedGroups
     * IDs des Groupes autorisés (ID techique ou md5 si le groupe n'est pas présent)
     * @return expression régulière à valider
     */
    protected String processAccessControlRegexGroups(final String[] authorizedGroups) {
        final StringBuilder regexBuilder = new StringBuilder();
        //^ Ex : (\((\[[a-f0-9]+\])*(\[1\]|\[2\])+(\[[a-f0-9]+\])*\))+$
        regexBuilder.append("(\\((\\[[a-f0-9]+\\])*");
        int length = authorizedGroups.length;
        if (length > 0) {
            regexBuilder.append("(");
            for (int i = 0; i < length; i++) {
                regexBuilder.append(String.format("\\[%s\\]", authorizedGroups[i]));
                if (i != (length - 1)) {
                    regexBuilder.append("|");
                }
            }
            regexBuilder.append(")+");
        }
        regexBuilder.append("(\\[[a-f0-9]+\\])*\\))+");
        return regexBuilder.toString();
    }

    /**
     * Ajout du tri sur la requête Elasticsearch.
     * @param searchRequestBuilder
     * Requête Elasticsearch
     * @param searchOptions
     * Options de recherche
     */
    protected void addSort(final SearchRequestBuilder searchRequestBuilder, final SearchOptions searchOptions) {
        // Ajout de l'ordre
        Set<SearchOptions.SortOption> sortOptions = searchOptions.getSort();
        if (CollectionUtils.isNotEmpty(sortOptions)) {
            for (final SearchOptions.SortOption sortOption : sortOptions) {
                final SortBuilder sortBuilder = new FieldSortBuilder(sortOption.getField());
                SortOrder sortOrder = SortOrder.ASC;
                if (sortOption.getOrder() == SearchOptions.SortOrderEnum.DESC) {
                    sortOrder = SortOrder.DESC;
                }
                sortBuilder.order(sortOrder);
                searchRequestBuilder.addSort(sortBuilder);
            }
        }
    }

    public final Client getClient() {
        return client;
    }

    public final void setClient(final Client client) {
        this.client = client;
    }

    public TimeValue getTimeout() {
        return timeout;
    }

    public void setTimeout(final TimeValue timeout) {
        this.timeout = timeout;
    }

    public SearchObjectConfiguration getRessourceConfiguration() {
        return ressourceConfiguration;
    }

    public void setRessourceConfiguration(final SearchObjectConfiguration ressourceConfiguration) {
        this.ressourceConfiguration = ressourceConfiguration;
    }
}
