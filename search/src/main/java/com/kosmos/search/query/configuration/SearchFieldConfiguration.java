package com.kosmos.search.query.configuration;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.beans.factory.annotation.Required;

/**
 * Définition d'un champ recherché.
 */
public class SearchFieldConfiguration implements Serializable,Comparable<SearchFieldConfiguration> {

    private static final long serialVersionUID = -6240572345905898336L;

    @NotNull
    private String fieldName;

    private boolean highLight = true;

    private float boost = 1.0F;

    /**
     * Constructeur par défaut.
     */
    public SearchFieldConfiguration() {
        super();
    }

    /**
     * Constructeur avec le nom du champ manipulé. Le nom du champ est l'adresse exacte de l'attribut dans l'index Elasticsearch.
     * @param fieldName
     */
    public SearchFieldConfiguration(String fieldName) {
        this.fieldName = fieldName;
    }

    @Required
    public void setFieldName(final String fieldName) {
        this.fieldName = fieldName;
    }

    public boolean isHighLight() {
        return highLight;
    }

    public void setHighLight(final boolean highLight) {
        this.highLight = highLight;
    }

    public float getBoost() {
        return boost;
    }

    public void setBoost(final float boost) {
        this.boost = boost;
    }

    public String getFieldName() {
        return fieldName;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SearchFieldConfiguration)) {
            return false;
        }
        final SearchFieldConfiguration that = (SearchFieldConfiguration) o;
        return new EqualsBuilder().append(isHighLight(), that.isHighLight()).append(getBoost(), that.getBoost()).append(getFieldName(), that.getFieldName()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getFieldName()).append(isHighLight()).append(getBoost()).toHashCode();
    }

    /**
     * Mettre une notion d'ordre pour une prévalence. Par défaut celui qui a le plus de boost est prioritaire.
     * @param o
     * Bean
     * @return 0 si égal
     * 1 si supérieur
     */
    @Override
    public int compareTo(final SearchFieldConfiguration o) {
        int compareResult = this.getFieldName().compareTo(o.getFieldName());
        if (compareResult ==0) {
         // On compare sur le boost
            compareResult = Float.compare(this.getBoost(),o.getBoost());
        }
        return  compareResult;
    }


}
