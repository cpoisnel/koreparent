package com.kosmos.search.query.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.search.query.bean.SearchResultBean;
import com.kosmos.search.query.service.ServiceSearcher;
import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.util.URLQueryParser;
import com.kportal.servlet.ExtensionServlet;
import com.univ.utils.ContexteUtil;

/**
 * Search servlet.
 * <p>
 *     La servlet de recherche effectue la recherche Elasticsearch et prépare la vue d'affichage.
 * </p>
 */
@WebServlet(name = "search", urlPatterns = "/servlet/search", loadOnStartup = 0)
public class SearchServlet extends ExtensionServlet {

    private static final Logger LOG = LoggerFactory.getLogger(SearchServlet.class);

    @Autowired
    private ServiceSearcher serviceSearcher;

    /**
     *
     * {@inheritDoc}
     * <p>
     *     Bean's autowiring.
     * </p>
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    protected SearchOptions processRequest(final HttpServletRequest request) {
        SearchOptions searchOptions = new SearchOptions();
        // Parcours des propriétés
        Map<String, String[]> parameterMap = request.getParameterMap();
        for (Map.Entry<String, String[]> parameterEntry : request.getParameterMap().entrySet()) {
            String parameter = parameterEntry.getKey();
            String[] values = parameterEntry.getValue();
            if ("q".equals(parameter)) {
                searchOptions.setQuery(getFirst(values));
            } else if ("agg".equals(parameter)) {
                // Aggregations forcées (demandée par la requête)
                searchOptions.addFacets(values);
            } else if ("s".equals(parameter)) {
                // Sort
            } else if ("facets".equals(parameter)) {
                searchOptions.addFacets(values);
            } else if ("l".equals(parameter)) {
                searchOptions.setLanguage(LangueUtil.getLocale(getFirst(values)).getLanguage());
            }
            else if ("types".equals(parameter)) {
                // Traité à part
            } else {
                // Ajout d'un filtre
                searchOptions.addFilter(parameter, values);
            }
        }
        // Aggregations obligatoires
        String[] typesValues = new String[0];
        if (parameterMap.containsKey("types")) {
            typesValues = parameterMap.get("types");
        }
        searchOptions.addFilter("types", typesValues);
        searchOptions.addFacets("types");
        // Langue (fr par défaut)
        if (!parameterMap.containsKey("l")) {
            searchOptions.setLanguage(LangueUtil.getLocale(ContexteUtil.getContexteUniv().getLangue()).getLanguage());
        }
        // Ajout des groupes autorisés pour effectuer la recherche
        searchOptions.setAuthorizedGroups(ContexteUtil.getContexteUniv().getGroupesDsiAvecAscendants());
        return searchOptions;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        SearchOptions searchOptions = this.processRequest(request);
        LOG.debug("Search options : {} ", searchOptions);
        // Appel du traitement de la recherche
        SearchResultBean result =null;
        try {
            result = serviceSearcher.search(searchOptions);
        }
        catch (Exception e) {
            LOG.error("Exception lors de la recherche",e);
            throw e;
        }
        // Ecriture de la réponse (TODO : MVC @Camille)
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader("Content-type", "application/json");
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(response.getWriter(), result);
        }
        catch (IOException  e) {
            LOG.error("Erreur à la sérialisation", e);
        }
    }

    protected static String getFirst(String[] stringValues) {
        String result = null;
        if (ArrayUtils.isNotEmpty(stringValues)) {
            result = stringValues[0];
        }
        return result;
    }
}

