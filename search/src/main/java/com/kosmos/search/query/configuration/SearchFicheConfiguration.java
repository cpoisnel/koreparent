package com.kosmos.search.query.configuration;

import java.util.HashSet;
import java.util.Set;

/**
 * Configuration de recherche par fiche (ou de plusieurs fiches).
 */
public class SearchFicheConfiguration extends SearchCommonConfiguration {

    private static final long serialVersionUID = -7273693594032435986L;

    private Set<SearchIndexConfiguration> index = new HashSet<>();

    /**
     * Filtres positionnés sur l'objet.
     */
    private Set<SearchFilterConfiguration> filters = new HashSet<>();


    /**
     * Constructeur par défaut, sans définition d'un nom d'objet.
     */
    public SearchFicheConfiguration() {
        super();
    }

    /**
     * Constructeur, avec un seul index défini par défaut.
     * @param objectName
     * Nom de l'objet, correspondant à un seul index
     */
    public SearchFicheConfiguration(String objectName) {
        super();
        this.setIndex(new SearchIndexConfiguration(objectName));
    }

    public Set<SearchIndexConfiguration> getIndex() {
        return index;
    }

    public void setIndex(final Set<SearchIndexConfiguration> index) {
        this.index = index;
    }

    public void setIndex(SearchIndexConfiguration index) {
        this.index.add(index);
    }

    public Set<SearchFilterConfiguration> getFilters() {
        return filters;
    }

    public void setFilters(final Set<SearchFilterConfiguration> filters) {
        this.filters = filters;
    }
}
