package com.kosmos.search.query.configuration;

import javax.validation.constraints.NotNull;

/**
 * Configuration de recherche par objet.
 */
public class SearchObjectConfiguration extends SearchCommonConfiguration {

    private static final long serialVersionUID = -7273693594032435987L;

    @NotNull
    public Class<?> type;

    /**
     * Constructeur par défaut.
     */
    public SearchObjectConfiguration() {
        super();
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(final Class<?> type) {
        this.type = type;
    }
}
