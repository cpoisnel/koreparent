package com.kosmos.search.query.configuration;

import java.util.Set;

import com.kosmos.search.query.request.SearchAggregationBuilder;
import com.kosmos.search.query.view.bean.SearchAggregationModel;

/**
 * Search aggregation configuration.
 * Created by cpoisnel on 19/12/16.
 */
public class SearchAggregationConfiguration<V extends SearchAggregationModel> extends SearchFilterConfiguration<V> {

    private static final long serialVersionUID = 7814363052361430110L;

    private SearchAggregationBuilder aggregationBuilder;

    private Set<SearchAggregationConfiguration> subAggregations;

    public SearchAggregationBuilder getAggregationBuilder() {
        return aggregationBuilder;
    }

    public void setAggregationBuilder(final SearchAggregationBuilder aggregationBuilder) {
        this.aggregationBuilder = aggregationBuilder;
    }

    public Set<SearchAggregationConfiguration> getSubAggregations() {
        return subAggregations;
    }

    public void setSubAggregations(final Set<SearchAggregationConfiguration> subAggregations) {
        this.subAggregations = subAggregations;
    }
}
