package com.kosmos.search.query.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.google.common.base.Preconditions;

/**
 * Options de recherche, simplification et valeurs par défaut ElasticSearch.
 * Ces Options de recherche peuvent être utilisés dans les recherches simples (issues de la servlet proposée). Si la recherche est spécifique à un projet, l'API Java Elasticsearch pourrait directement être utilisée.
 * @author cpoisnel
 *
 */
public class SearchOptions implements Serializable {

    public static final int DEFAULT_OFFSET = 0;

    public static final int DEFAULT_LIMIT = 10;

    public static final String DEFAULT_LANGUAGE = "fr";

    public static final int MAX_LIMIT = 500;

    private static final long serialVersionUID = 6714525796288072130L;

    private int offset = DEFAULT_OFFSET;

    private int limit = DEFAULT_LIMIT;

    private String query;

    private String type;

    private String language = DEFAULT_LANGUAGE;

    private final Set<String> facets = new LinkedHashSet<>();

    private final Map<String,String[]> filters = new HashMap<>();

    private final Set<QueryOption> querys = new HashSet<>();

    private final Set<SortOption> sort = new HashSet<>();

    private Set<String> authorizedGroups = new HashSet<>();

    /**
     * Attribut permettant de définir une recherche via une flux JSON (écrase les autres paramètres utiles, hors index / type.
     */
    private String jsonSource;

    public final String getQuery() {
        return query;
    }

    public final SearchOptions setQuery(final String query) {
        this.query = query;
        return this;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    public String getLanguage() {
        return language;
    }

    public SearchOptions setLanguage(final String language) {
        this.language = language;
        return this;
    }

    /**
     * @param type the type to set
     */
    public SearchOptions setType(final String type) {
        this.type = type;
        return this;
    }

    /**
     * Offset of the first result to return. Defaults to {@link #DEFAULT_OFFSET}
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Sets the offset of the first result to return (zero-based).
     */
    public SearchOptions setOffset(final int offset) {
        Preconditions.checkArgument(offset >= 0, "Offset must be positive");
        this.offset = offset;
        return this;
    }



    /**
     * Set offset and limit according to page approach. If pageSize is negative, then
     * {@link #MAX_LIMIT} is used.
     */
    public SearchOptions setPage(final int page, final int pageSize) {
        Preconditions.checkArgument(page >= 1, "Page must be greater or equal to 1 (got " + page + ")");
        setLimit(pageSize);
        setOffset((page * this.limit) - this.limit);
        return this;
    }

    public int getPage() {
        return limit > 0 ? (int) Math.ceil((double) (offset + 1) / (double) limit) : 0;
    }

    /**
     * @param sortOption the sort to add
     */
    public void addSort(final SortOption sortOption) {
        this.sort.add(sortOption);
    }

    public void addFilter(final String parameter, final String[] values) {
        this.filters.put(parameter,values);
    }

    /**
     * @return the sort
     */
    public Set<SortOption> getSort() {
        return sort;
    }

    public Map<String, String[]> getFilters() {
        return filters;
    }

    /**
     * @param authorizedGroup the sort to add
     */
    public void addAuthorizedGroup(final String authorizedGroup) {
        this.authorizedGroups.add(authorizedGroup);
    }

    /**
     * @param authorizedGroups the authorizedGroups to set
     */
    public void setAuthorizedGroups(final Set<String> authorizedGroups) {
        this.authorizedGroups = authorizedGroups;
    }

    /**
     * @return the sort
     */
    public Set<String> getAuthorizedGroups() {
        return authorizedGroups;
    }

    /**
     * Limit on the number of results to return. Defaults to {@link #DEFAULT_LIMIT}.
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Sets the limit on the number of results to return.
     */
    public SearchOptions setLimit(final int limit) {
        if (limit <= 0) {
            this.limit = MAX_LIMIT;
        } else {
            this.limit = Math.min(limit, MAX_LIMIT);
        }
        return this;
    }

    /**
     * Lists selected facets.
     */
    public Collection<String> getFacets() {
        return facets;
    }

    /**
     * Selects facets to return for the domain.
     */
    public SearchOptions addFacets(final Collection<String> f) {
        if (f != null) {
            this.facets.addAll(f);
        }
        return this;
    }

    public SearchOptions addFacets(final String... array) {
        Collections.addAll(facets, array);
        return this;
    }

    /**
     * Source JSON de la recherche. Si elle est non vide, elle prévaut sur toutes les autres options de recherche.
     * @return the jsonSource
     */
    public String getJsonSource() {
        return jsonSource;
    }

    /**
     * @param jsonSource the rawSearch to set
     */
    public void setJsonSource(final String jsonSource) {
        this.jsonSource = jsonSource;
    }

    /**
     * Lists selected querys.
     */
    public Collection<QueryOption> getQuerys() {
        return querys;
    }

    /**
     * Selects querys to return for the domain.
     */
    public SearchOptions addQuerys(final Collection<QueryOption> f) {
        if (f != null) {
            this.querys.addAll(f);
        }
        return this;
    }

    public SearchOptions addQuerys(final QueryOption... array) {
        Collections.addAll(querys, array);
        return this;
    }

    /**
     * Option de recherche (recherche mots clés).
     * @author cpoisnel
     *
     */
    public static class QueryOption implements Serializable{

        private static final long serialVersionUID = 8090398313849621686L;

        private final QueryType type = QueryType.TERM;

        private final String field;

        private final String[] values;

        public final QueryType getType() {
            return type;
        }

        public final String getField() {
            return field;
        }

        public final String[] getValues() {
            return values;
        }

        /**
         * Constructeur d'une recherche sur un champ.
         * @param field
         * @param values
         */
        public QueryOption(final String field, final String... values) {
            super();
            this.field = field;
            this.values = values;
        }

        /**
         * Constructeur d'une recherche sur un champ.
         * @param field
         * @param values
         */
        public QueryOption(final String field, @NotNull final Collection<String> values) {
            super();
            this.field = field;
            this.values = values.toArray(new String[values.size()]);
        }
    }

    /**
     * Option d'ordonnancement des résultats de résultats.
     * @author cpoisnel
     *
     */
    public static class SortOption implements Serializable {

        private static final long serialVersionUID = 6488572212206347588L;

        private String field;

        private SortOrderEnum order = SortOrderEnum.ASC;

        /**
         * Option d'ordonnancement des résultats de résultats.
         * @param field
         * @param order
         */
        public SortOption(final String field, final SortOrderEnum order) {
            super();
            this.field = field;
            this.order = order;
        }

        /**
         * Option d'ordonnancement des résultats de résultats.
         * @param field
         */
        public SortOption(final String field) {
            super();
            this.field = field;
        }

        /**

         * @return the field
         */
        public String getField() {
            return field;
        }

        /**
         * @param field the field to set
         */
        public void setField(final String field) {
            this.field = field;
        }

        /**
         * @return the order
         */
        public SortOrderEnum getOrder() {
            return order;
        }

        /**
         * @param order the order to set
         */
        public void setOrder(final SortOrderEnum order) {
            this.order = order;
        }
    }

    /**
     * Déclaration d'une facette pour une recherche.
     * @author cpoisnel
     *
     */
    public static class FilterOption {

        private final String name;

        private final String[] values;

        /**
         * Constructeur d'une option de facette.
         * @param name
         */
        public FilterOption(final String name, final String... values) {
            super();
            this.name = name;
            this.values = values;
        }

        public final String getName() {
            return name;
        }

        public String[] getValues() {
            return values;
        }
    }

    /**
     * Type de recherche.
     * @author cpoisnel
     *
     */
    public enum QueryType {
        TERM;
    }

    /**
     * Ordre de tri des résultats de recherche.
     * @author cpoisnel
     *
     */
    public enum SortOrderEnum {
        ASC, DESC;
    }

    /**
     * Types de facets supportés.
     * @author cpoisnel
     *
     */
    public enum FacetType {
        TERM, NESTED
    }
}