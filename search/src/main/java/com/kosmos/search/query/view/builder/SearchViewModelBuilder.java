package com.kosmos.search.query.view.builder;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.configuration.SearchFilterConfiguration;
import com.kosmos.search.query.view.bean.SearchFilterModel;

/**
 * Created by cpoisnel on 19/12/16.
 * @param <T>
 *     Type de modèle de filtre
 */
public interface SearchViewModelBuilder<T extends SearchFilterModel> {

    T build(SearchOptions searchOptions,SearchFilterConfiguration configuration);

}
