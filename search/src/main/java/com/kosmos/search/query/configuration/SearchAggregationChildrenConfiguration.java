package com.kosmos.search.query.configuration;

import com.kosmos.search.query.request.SearchAggregationBuilder;
import com.kosmos.search.query.view.bean.SearchAggregationModel;

/**
 * Configuration d'aggrégation de recherche pour les enfants.
 * Created by cpoisnel on 19/12/16.
 */
public class SearchAggregationChildrenConfiguration extends SearchAggregationConfiguration {

    private static final long serialVersionUID = 7814363052361430111L;

    /**
     * Type du document fils.
     */
    private String childType;

    public String getChildType() {
        return childType;
    }

    public void setChildType(final String childType) {
        this.childType = childType;
    }
}
