package com.kosmos.search.query.bean.aggregation;

import java.util.Collection;

/**
 * Modèle d'aggrégation qui permet de présenter les documents qui ont un champ (field) manquant.
 *
 * Created by cpoisnel on 30/12/16.
 */
public class AggregationMissingModel extends AggregationModel implements HasAggregationsModel {

    private static final long serialVersionUID = -5987666537717613035L;

    private long docCount;

    private Collection<AggregationModel> aggregations;

    public AggregationMissingModel(final String name) {
        super(name);
    }

    @Override
    public String getType() {
        return "missing";
    }

    public long getDocCount() {
        return docCount;
    }

    public void setAggregations(final Collection<AggregationModel> aggregations) {
        this.aggregations = aggregations;
    }

    public void setDocCount(final Long docCount) {
        this.docCount = docCount;
    }

    @Override
    public Collection<AggregationModel> getAggregations() {
        return aggregations;
    }
}
