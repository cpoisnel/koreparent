package com.kosmos.search.query.request;

import java.util.Set;

import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;

import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchAggregationTermConfiguration;

/**
 * Builder d'aggregation de type Term (rangement des documents selon le chemin d'un champ donné).
 *
 */
public class SearchAggregationTermBuilder implements SearchAggregationBuilder<SearchAggregationTermConfiguration> {

    /**
     * Constructeur d'une requête d'aggregation pour les termes simples (chaines de caractères, en général des codes).
     * @param configuration
     *  Configuration de recherche.
     * @return le builder de requête Elasticsearch pour l'aggregation de type Term.
     * @see TermsBuilder
     */
    @Override
    public AbstractAggregationBuilder build(final SearchAggregationTermConfiguration configuration) {
        TermsBuilder termsBuilder = new TermsBuilder(configuration.getName());
        termsBuilder.field(configuration.getField());
        termsBuilder.exclude(configuration.getExclude());
        termsBuilder.include(configuration.getInclude());
        termsBuilder.size(configuration.getSize());
        termsBuilder.showTermDocCountError(true);
        if (configuration.getSubAggregations() != null) {
            Set<SearchAggregationConfiguration> sub = configuration.getSubAggregations();
            for (SearchAggregationConfiguration subAggregationConfiguration : sub) {
                termsBuilder.subAggregation(subAggregationConfiguration.getAggregationBuilder().build(subAggregationConfiguration));
            }
        }
        return termsBuilder;
    }
}
