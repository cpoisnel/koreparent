package com.kosmos.search.query.request;

import java.util.Set;

import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.children.ChildrenBuilder;

import com.kosmos.search.query.configuration.SearchAggregationChildrenConfiguration;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;

/**
 * Builder de requête d'aggrégation spécifique pour les relations parents / enfants.
 * <p>Voir <a href="https://www.elastic.co/guide/en/elasticsearch/reference/2.4/search-aggregations-bucket-children-aggregation.html">la documentation</a></p>
 */
public class SearchAggregationChildrenBuilder implements SearchAggregationBuilder<SearchAggregationChildrenConfiguration> {

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractAggregationBuilder build(final SearchAggregationChildrenConfiguration configuration) {
        ChildrenBuilder childrenBuilder = new ChildrenBuilder(configuration.getName());

        childrenBuilder.childType(configuration.getChildType());
        // Enfants gérés
        if (configuration.getSubAggregations() != null) {
            Set<SearchAggregationConfiguration> sub = configuration.getSubAggregations();
            for (SearchAggregationConfiguration subAggregationConfiguration : sub) {
                childrenBuilder.subAggregation(subAggregationConfiguration.getAggregationBuilder().build(subAggregationConfiguration));
            }
        }
        return childrenBuilder;
    }
}
