package com.kosmos.search.query.bean.aggregation;

import java.util.Collection;

/**
 * Modèle avec des aggrégations. Permet de lister les aggrégations et d'effectuer des opérations imbriquées.
 * Created by cpoisnel on 30/12/16.
 */
public interface HasAggregationsModel {

    /**
     * Accesseur des aggrégations du modèle.
     * @return les aggrégations du modèle
     */
    Collection<AggregationModel> getAggregations();

    /**
     * Définit les aggrégations.
     * @param aggregationModels
     * Les aggrégations internes
     *
     */
    void setAggregations(Collection<AggregationModel> aggregationModels);
}
