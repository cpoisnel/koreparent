package com.kosmos.search.query.view.builder;

import org.elasticsearch.search.aggregations.Aggregation;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.view.bean.SearchAggregationModel;

/**
 * Created by cpoisnel on 19/12/16.
 */
public class SearchAggregationTermLabelViewModelBuilder implements SearchAggregationViewModelBuilder<SearchAggregationModel>{

    /**
     * {@inheritDoc}
     */
    @Override
    public <A extends Aggregation> SearchAggregationModel build(final SearchOptions searchOptions, final A aggregation, final SearchAggregationConfiguration configuration) {
        SearchAggregationModel searchAggregationModel = new SearchAggregationModel();
        searchAggregationModel.setName(aggregation.getName());
        return searchAggregationModel;
    }
}
