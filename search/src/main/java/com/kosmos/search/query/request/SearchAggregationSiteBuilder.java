package com.kosmos.search.query.request;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import com.kosmos.search.exception.SearchException;
import com.kosmos.search.query.configuration.SearchAggregationTermConfiguration;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;

/**
 * Builder d'aggregation pour les sites configurés dans l'usine à site.
 * Cela permet de ranger l'ensemble des documents selon les sites référencés.
 * Created by cpoisnel on 29/12/16.
 */
public class SearchAggregationSiteBuilder extends SearchAggregationTermBuilder {

    @Autowired
    private ServiceInfosSite serviceInfosSite;

    /**
     * Constructeur par défaut.
     */
    public SearchAggregationSiteBuilder(){
        super();
    }

    /**
     * Builder d'aggrégation de type term, spécifique aux sites configurés.
     * @param configuration
     *  Configuration de recherche.
     * @return Un builder d'aggrégation où
     */
    @Override
    public AbstractAggregationBuilder build(final SearchAggregationTermConfiguration configuration) {
        TermsBuilder termsBuilder  = (TermsBuilder) super.build(configuration);
        Set<String> codesRubriques = new HashSet<>();
        try {
            final Collection<InfosSite> listeSites = serviceInfosSite.getListeTousInfosSites();
            for (InfosSite site : listeSites) {
                codesRubriques.add(site.getCodeRubrique());
            }
        } catch (Exception e) {
            throw new SearchException("Erreur à la recherche de sites", e);
        }
        termsBuilder.include(codesRubriques.toArray(new String[codesRubriques.size()]));
        return  termsBuilder;
    }
}
