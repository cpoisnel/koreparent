package com.kosmos.search.query.request;

import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;

import com.kosmos.search.query.configuration.SearchAggregationConfiguration;

/**
 * Builder de la requête d'aggregation spécifique.
 * @param <S>
 *      Type de configuration de l'aggregation.
 */
public interface SearchAggregationBuilder<S extends SearchAggregationConfiguration> {

    /**
     * Construit une requête d'aggregation.
     * @param configuration
     * Configuration de l'aggregation.
     * @return le builder de la requête d'aggrégation Elasticsearch
     */
    AbstractAggregationBuilder build(S configuration);
}
