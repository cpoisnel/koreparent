package com.kosmos.search.query.bean.aggregation;

/**
 * Modèle d'aggrégation.
 * Created by cpoisnel on 30/12/16.
 */
public class AggregationSumModel extends AggregationModel {

    private static final long serialVersionUID = -1077195375921564515L;

    private double value;

    /**
     * Constructeur par défaut pour une aggrégation de type Somme.
     * @param name
     */
    public AggregationSumModel(final String name) {
        super(name);
    }

    public double getValue() {
        return value;
    }

    public void setValue(final double value) {
        this.value = value;
    }

    @Override
    public String getType() {
        return "sum";
    }
}
