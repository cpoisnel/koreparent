package com.kosmos.search.query.bean.aggregation;

import java.util.Collection;

/**
 * Created by cpoisnel on 30/12/16.
 */
public class AggregationTermModel extends AggregationModel {

    private long otherDocCount;

    private Collection<BucketModel> buckets ;

    public AggregationTermModel() {
    }

    public AggregationTermModel(final String name) {
        super(name);
    }

    @Override
    public String getType() {
        return "term";
    }

    public long getOtherDocCount() {
        return otherDocCount;
    }

    public void setOtherDocCount(final long otherDocCount) {
        this.otherDocCount = otherDocCount;
    }

    public Collection<BucketModel> getBuckets() {
        return buckets;
    }

    public void setBuckets(final Collection<BucketModel> buckets) {
        this.buckets = buckets;
    }
}
