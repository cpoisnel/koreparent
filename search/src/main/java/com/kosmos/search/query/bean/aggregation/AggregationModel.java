package com.kosmos.search.query.bean.aggregation;

import java.io.Serializable;

import com.drew.lang.annotations.NotNull;

/**
 * Modèle d'aggregation générique pour le module de recherche.
 * Created by cpoisnel on 30/12/16.
 */
public abstract class AggregationModel implements Serializable {

    private static final long serialVersionUID = 8193386274728899951L;

    /**
     * Nom de l'aggrégation.
     */
    @NotNull
    private String name;

    /**
     * Constructeur par défaut (utilisée pour une écriture simplifiée dans Spring).
     */
    public AggregationModel() {
        super();
    }

    /**
     * Modèle d'aggrégation avec le nom de l'aggrégation en paramètre.
     * @param name
     * Nom de l'aggrégation
     */
    public AggregationModel(final String name) {
        this.name = name;
    }

    /**
     * Accesseur du type de l'aggrégation (unique par classe).
     * @return le type de l'aggrégation
     */
    public abstract String getType();

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
