package com.kosmos.search.query.view.builder;

import org.elasticsearch.search.aggregations.Aggregation;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.view.bean.SearchAggregationModel;

/**
 * Created by cpoisnel on 19/12/16.
 */
public interface SearchAggregationViewModelBuilder<T extends SearchAggregationModel> {

    /**
     * Build a model for Aggregation
     * @param searchOptions
     * @param aggregation
     * @param configuration
     * @param <A>
     * @return
     */
    <A extends Aggregation> T build(SearchOptions searchOptions, A aggregation, SearchAggregationConfiguration configuration);

}
