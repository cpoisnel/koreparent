package com.kosmos.search.query.request;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;

import com.kosmos.search.query.configuration.SearchFilterValueConfiguration;

/**
 * Builder d'un fragment de requête d'un filtre Elasticsearch.
 * Created by cpoisnel on 28/12/16.
 */
public class SearchQueryTermBuilder implements SearchQueryBuilder<SearchFilterValueConfiguration>{

    /**
     * Builder d'une requête à partir d'une configuration
     * @param configuration
     *      Configuration d'un filtre
     * @param values
     *      Valeurs correspondant au filtre
     * @return un fragment de requête Elasticsearch (ajout en "must" sur la requête)
     */
    @Override
    public QueryBuilder build(SearchFilterValueConfiguration configuration, final String[] values) {
        final TermsQueryBuilder queryBuilder = new TermsQueryBuilder(configuration.getField(),values);
        return queryBuilder;
    }
}
