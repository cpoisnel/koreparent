package com.kosmos.search.query.configuration;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.util.Assert;

/**
 * Configuration de recherche par défaut pour tous les beans.
 */
public class SearchCommonConfiguration implements Serializable {

    private static final long serialVersionUID = -7273693594032435988L;

    /**
     * Scan automatiquement le bean par introspection (vrai par défaut).
     */
    private boolean autoscan = true;

    /**
     * Champs exclus de la recherche.
     */
    private Set<String> excludedFields = new HashSet<>();

    /**
     * Champs de recherche.
     */
    private Map<String, SearchFieldConfiguration> fields = new HashMap<>();

    /**
     * Constructeur par défaut.
     */
    public SearchCommonConfiguration() {
        super();
    }

    public Map<String, SearchFieldConfiguration> getFields() {
        return fields;
    }

    public void setFields(final Map<String, SearchFieldConfiguration> fields) {
        this.fields = fields;
    }

    /**
     * Ajout des fields par un set de données, plutôt qu'une map (à utiliser pour une configuration Spring par exemple).
     * @param fieldsSet
     * Set de configuration de champs
     */
    public void setFieldsSet(final Set<SearchFieldConfiguration> fieldsSet) {
        Assert.notNull(fieldsSet);
        this.fields = new HashMap<>(fieldsSet.size());
        for (SearchFieldConfiguration searchFieldConfiguration : fieldsSet){
            this.fields.put(searchFieldConfiguration.getFieldName(),searchFieldConfiguration);
        }
    }

    public Set<String> getExcludedFields() {
        return excludedFields;
    }

    public void setExcludedFields(final Set<String> excludedFields) {
        this.excludedFields = excludedFields;
    }

    public boolean isAutoscan() {
        return autoscan;
    }

    public void setAutoscan(final boolean autoscan) {
        this.autoscan = autoscan;
    }

    /**
     * Construction d'une chaîne avec les attributs de l'objet.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("autoscan", autoscan).append("excludedFields", excludedFields).append("fields", fields).toString();
    }
}
