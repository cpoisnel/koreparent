package com.kosmos.search.query.configuration;

/**
 * Created by cpoisnel on 19/12/16.
 */
public class SearchAggregationTermConfiguration extends SearchAggregationValueConfiguration {

    private static final long serialVersionUID = 7997167230133452498L;

    public static final int DEFAULT_SIZE = 20;

    public static final int DEFAULT_MIN_DOC_COUNT = 1;

    /**
     * Taille de l'aggregation (initialisé à {@link #DEFAULT_SIZE} )
     */
    private int size = DEFAULT_SIZE;

    private String include;

    private String exclude;

    private int minDocCount= DEFAULT_MIN_DOC_COUNT;

    public int getSize() {
        return size;
    }

    public void setSize(final int size) {
        this.size = size;
    }

    public String getInclude() {
        return include;
    }

    public void setInclude(final String include) {
        this.include = include;
    }

    public String getExclude() {
        return exclude;
    }

    public void setExclude(final String exclude) {
        this.exclude = exclude;
    }

    public int getMinDocCount() {
        return minDocCount;
    }

    public void setMinDocCount(final int minDocCount) {
        this.minDocCount = minDocCount;
    }
}
