package com.kosmos.search.query.request;

import org.elasticsearch.index.query.QueryBuilder;

/**
 * Builder d'un fragment de requête Elasticsearch de filtre.
 * <p>
 *     Pas de contrainte sur la configuration du filtre.
 * </p>
 * @param <S>
 *     Configuration de filtre (pas de contrainte sur le type)
 */
public interface SearchQueryBuilder<S> {

    /**
     * Construit une requête de filtre.
     * @param configuration
     * Configuration de filtre
     * @param values valeurs à filtrer
     * @return Un fragment de requête Elasticsearch
     */
    QueryBuilder build(S configuration, String[] values);
}
