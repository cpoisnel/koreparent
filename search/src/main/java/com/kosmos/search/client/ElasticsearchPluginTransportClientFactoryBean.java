package com.kosmos.search.client;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.plugin.deletebyquery.DeleteByQueryPlugin;

import fr.pilato.spring.elasticsearch.ElasticsearchTransportClientFactoryBean;

/**
 * Builder d'un client Elasticsearch qui a conscience de certains plugins (par exemple : delete-by-query).
 * <p>En attendant la release de spring-elasticsearch 2.2, ajout du plugin sur le TransportClient.
 * https://github.com/dadoonet/spring-elasticsearch/issues/90</p>
 * @author cpoisnel
 *
 */
public class ElasticsearchPluginTransportClientFactoryBean extends ElasticsearchTransportClientFactoryBean {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Client buildClient() throws Exception {
        final Settings.Builder builder = Settings.builder();

        if (null != this.settings && null == properties) {
            builder.put(this.settings);
        }

        if (null != this.settingsFile && null == properties) {
            logger.warn("settings has been deprecated in favor of properties. See issue #15: https://github.com/dadoonet/spring-elasticsearch/issues/15.");
            builder.loadFromStream(settingsFile, ElasticsearchTransportClientFactoryBean.class.getResourceAsStream("/" + settingsFile));
        }

        if (null != this.properties) {
            builder.put(this.properties);
        }

        final TransportClient.Builder clientBuilder = TransportClient.builder().settings(builder.build());

        // Ajout du plugin delete-by-query
        clientBuilder.addPlugin(DeleteByQueryPlugin.class);

        final TransportClient client = clientBuilder.build();

        for (int i = 0; i < getEsNodes().length; i++) {
            client.addTransportAddress(toAddress(getEsNodes()[i]));
        }

        return client;
    }

    /**
     * Helper to define an hostname and port with a String like hostname:port
     * @param address Node address hostname:port (or hostname)
     * @return
     */
    private InetSocketTransportAddress toAddress(final String address) throws UnknownHostException {
        if (address == null) {
            return null;
        }

        final String[] splitted = address.split(":");
        int port = 9300;
        if (splitted.length > 1) {
            port = Integer.parseInt(splitted[1]);
        }

        return new InetSocketTransportAddress(InetAddress.getByName(splitted[0]), port);
    }

}
