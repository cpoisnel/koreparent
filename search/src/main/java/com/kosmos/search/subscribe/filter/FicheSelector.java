package com.kosmos.search.subscribe.filter;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.integration.core.MessageSelector;


/**
 * Filtrage des fiches ou données associées.
 * @author cpoisnel
 *
 */
public abstract class FicheSelector implements MessageSelector {

    private Collection<String> objetsExclus;

    private Collection<String> objetsInclus;

    /**
     * Constructeur public.
     */
    public FicheSelector() {
        super();
    }

    /**
     * Est-ce que le code objet est autorisé (via la vérification dans les properties).
     * @param codeObjet
     *      Code de fiche ex : 0010,9999,...
     * @return vrai si le code objet est autorisé, faux sinon (élément rejeté)
     */
    protected boolean isCodeObjetAutorise(final String codeObjet) {
        boolean isAccepted = true;

        if (CollectionUtils.isNotEmpty(objetsInclus)) {
            isAccepted = objetsInclus.contains(codeObjet);
        }
        if (isAccepted && CollectionUtils.isNotEmpty(objetsExclus)) {
            isAccepted = !objetsExclus.contains(codeObjet);
        }
        return isAccepted;
    }

    public Collection<String> getObjetsExclus() {
        return objetsExclus;
    }

    public void setObjetsExclus(final Collection<String> objetsExclus) {
        this.objetsExclus = objetsExclus;
    }


    public Collection<String> getObjetsInclus() {
        return objetsInclus;
    }

    public void setObjetsInclus(final Collection<String> objetsInclus) {
        this.objetsInclus = objetsInclus;
    }
}
