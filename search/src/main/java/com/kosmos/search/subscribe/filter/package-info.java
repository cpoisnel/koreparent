/**
 * Classes spécifiques pour <a href="http://docs.spring.io/spring-integration/docs/2.0.0.RC1/reference/html/filter.html">filtrer les messages des channels
 * de Spring-Intégration</a> (filtre les types de fiches, les états,...)
 *
 *
 * @author cpoisnel
 */
package com.kosmos.search.subscribe.filter;