package com.kosmos.search.subscribe.filter;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.EtatFiche;

/**
 * Filtre sur les metatags. N'accepte que les metatags avec un état validé et dont l'objet peut être indexé.
 * @author cpoisnel
 *
 */
public class MetatagSelector extends FicheSelector {

    private static final Logger LOG = LoggerFactory.getLogger(MetatagSelector.class);

    /**
     * Constructeur par défaut.
     */
    public MetatagSelector() {
        super();
    }

    /**
     * Etat(s) de fiche autorisés à être selectionné. Par défaut, pas de sélection, ce qui signifie que tous les états sont autorisés.
     */
    private Collection<EtatFiche> etatsAutorises;

    /**
     * {@inheritDoc}
     * Filtrage sur l'état et le code objet du metatag bean.
     */
    @Override
    public boolean accept(final Message<?> message) {
        boolean isAccepted = false;
        final Object payload = message.getPayload();
        if (MetatagBean.class.isAssignableFrom(payload.getClass())) {
            final MetatagBean metatagBean = (MetatagBean) payload;
            isAccepted = isEtatAutorise(metatagBean) && isCodeObjetAutorise(metatagBean.getMetaCodeObjet());
        }
        LOG.debug("Message accepté ? => '{}'. (Message : '{}').", isAccepted, message);
        // Donnée filtrée !
        return isAccepted;
    }

    /**
     * Est-ce que l'état est filtré?
     * @param metatagBean
     *      Bean de Metatag
     * @return vrai si l'état du Metatag est autorisé, faux sinon (élément rejeté)
     * @see #etatsAutorises
     */
    protected boolean isEtatAutorise(final MetatagBean metatagBean) {
        if (CollectionUtils.isNotEmpty(etatsAutorises)) {
            final String etatFicheCode = metatagBean.getMetaEtatObjet();
            return etatsAutorises.contains(EtatFiche.getEtatParCode(etatFicheCode));
        }
        return true;
    }

    public final void setEtatsAutorises(final Collection<EtatFiche> etatsAutorises) {
        this.etatsAutorises = etatsAutorises;
    }

    public Collection<EtatFiche> getEtatsAutorises() {
        return etatsAutorises;
    }

}
