package com.kosmos.search.subscribe.filter;

import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

import com.univ.objetspartages.bean.RessourceBean;

import com.kosmos.search.util.IndexerUtil;

/**
 * Filtre sur les ressources. On ne conserve que les ressources dont le code parent correspond à un type de fiche indexable.
 * <p>La fiche correspondant à cette ressource sera également un critère déterminant (si elle a l'annotation FicheIndexable).</p>
 * @author cpoisnel
 *
 */
public class RessourceSelector extends FicheSelector {

    private static final Logger LOG = LoggerFactory.getLogger(RessourceSelector.class);

    /**
     * Constructeur par défaut.
     */
    public RessourceSelector() {
        super();
    }

    /**
     * {@inheritDoc}.
     * Ne traite pas les codes objets non autorisés et l'état objet à 0.
     */
    @Override
    public boolean accept(final Message<?> message) {
        boolean isAccepted = false;
        final Object payload = message.getPayload();
        if (RessourceBean.class.isAssignableFrom(payload.getClass())) {
            final RessourceBean metatagBean = (RessourceBean) payload;
            final Triple<Long, String, Long> idTypeNumero = IndexerUtil.getRessourceIdTypeNumero(metatagBean.getCodeParent());
            isAccepted = !"0".equals(metatagBean.getEtat()) && idTypeNumero != null && isCodeObjetAutorise(idTypeNumero.getMiddle());
        }
        LOG.debug("Message accepté ? => '{}'. (Message : '{}').", isAccepted, message);
        // Donnée filtrée !
        return isAccepted;
    }

}
