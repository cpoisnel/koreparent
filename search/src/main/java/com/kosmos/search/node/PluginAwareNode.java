package com.kosmos.search.node;

import java.util.Collection;

import org.elasticsearch.Version;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.internal.InternalSettingsPreparer;
import org.elasticsearch.plugins.Plugin;

/**
 * Noeud Elasticsearch (embarqué) avec plugins.
 *
 * @author cpoisnel
 *
 */
public class PluginAwareNode extends Node {

    /**
     * Constructeur d'un noeud, avec les plugins chargés au démarrage.
     *
     * @param settings
     * @param version
     * @param classpathPlugins
     */
    public PluginAwareNode(final Settings settings, final Version version, final Collection<Class<? extends Plugin>> classpathPlugins) {
        super(InternalSettingsPreparer.prepareEnvironment(settings, null), version, classpathPlugins);
    }

}
