package com.kosmos.search.node.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.elasticsearch.node.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import com.jsbsoft.jtf.core.ApplicationContextManager;


/**
 * Shutdown Listener de l'extension Elasticsearch.
 * <ul>
 * <li>Arrêt du pool de threads (client).</li>
 * <li>Arrêt du noeud ElasticSearch démarré (si es.mode=embedded).</li>
 * </ul>
 * @author cpoisnel
 *
 * PATCH CORE-1724 : web.xml, En attente
 *
 */
@WebListener
public class ShutdownNodeListener implements ServletContextListener {

    private static final Logger LOG = LoggerFactory.getLogger(ShutdownNodeListener.class);

    /**
     * Notification that the web application is ready to process requests.
     *
     * @param servletContextEvent
     */
    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {
        //nothing required
    }

    /**
     * Notification that the servlet context is about to be shut down.
     * <p/>
     * Shuts down all cache managers known to {@link Node}
     *
     * @param servletContextEvent
     */
    @Override
    public void contextDestroyed(final ServletContextEvent servletContextEvent) {
        stopElasticSearchTaskExecutor();
        stopElasticSearchNode();
    }

    protected void stopElasticSearchTaskExecutor() {
        try {
            final DisposableBean elasticSearchTaskExecutor = ApplicationContextManager.getCoreContextBean("elasticSearchTaskExecutor", DisposableBean.class);
            if (null != elasticSearchTaskExecutor) {
                LOG.info("Arrêt du pool de threads client Elasticsearch : '{}'", elasticSearchTaskExecutor);
                elasticSearchTaskExecutor.destroy();
            }
        } catch (final NoSuchBeanDefinitionException exception) {
            LOG.debug("Aucun task executor démarré", exception);
        } catch (final Exception e) {
            LOG.warn("Exception à l'arrêt des threads", e);
        }
    }

    protected void stopElasticSearchNode() {
        try {
            final Object objectNode = ApplicationContextManager.getCoreContextBean("elasticsearchCoreNode");
            if (objectNode !=null) {
                final Node node = (Node) objectNode;
                LOG.debug("Noeud Elasticsearch démarré. L'arrêt est demandé");
                if (!node.isClosed()) {
                    LOG.info("Arrêt du noeud Elasticsearch : '{}'", node);
                    node.close();
                }
            }
        } catch (final NoSuchBeanDefinitionException exception) {
            LOG.debug("Aucun noeud elasticsearch démarré en local", exception);
        }
    }
}
