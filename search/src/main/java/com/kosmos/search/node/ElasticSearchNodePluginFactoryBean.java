package com.kosmos.search.node;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.elasticsearch.Version;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.plugins.Plugin;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.kosmos.log.LoggerContextUtils;

import fr.pilato.spring.elasticsearch.ElasticsearchNodeFactoryBean;
import fr.pilato.spring.elasticsearch.proxy.GenericInvocationHandler;

/**
 * Plugin aware node factory.
 * <p>Depuis la version 2.1 d'Elastic Search, les plugins ont été supprimés dans la configuration du noeud. Gestion d'une factory de noeuds (internes) permettant de gérer le fonctionnement des plugins. Plus d'informations sur le forum d'Elastic query : <a href="https://discuss.elastic.co/t/add-plugins-from-classpath-in-embedded-elasticsearch-client-node/36269/7">Add plugins from classpath in embedded Elasticsearch client node</a>.</p>
 * @author cpoisnel
 *
 */
public class ElasticSearchNodePluginFactoryBean extends ElasticsearchNodeFactoryBean implements FactoryBean<Node>, InitializingBean, DisposableBean {


    private Node node;

    private Node proxyfiedNode;

    private List<Class<? extends Plugin>> plugins = new ArrayList<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() throws Exception {

        // On initialise le contexte de logs pour l'initialisation du noeud dans un fichier dédié (*-elasticsearch.log)
        String oldContexte = LoggerContextUtils.getLogContext();
        LoggerContextUtils.initContextLog("elasticsearch");

        try {
            if (async) {
                Assert.notNull(taskExecutor);
                final Future<Node> nodeFuture = taskExecutor.submit(new Callable<Node>() {

                    @Override
                    public Node call() throws Exception {
                        return initialize();
                    }
                });
                final ProxyFactory proxyFactory = new ProxyFactory();
                proxyFactory.setProxyTargetClass(true);
                proxyFactory.setTargetClass(Node.class);
                proxyFactory.addAdvice(new GenericInvocationHandler<Node>(nodeFuture));
                proxyfiedNode = (Node) proxyFactory.getProxy();
            } else {
                node = initialize();
            }
        }
        finally {
            LoggerContextUtils.initContextLog(oldContexte);
        }
    }

    /**
     * Initialisation d'un noeud ElasticSearch avec configuration des plugins.
     * <p>
     * La méthode close n'est pas appelée dans cette méthode, car le client est fermé lors de l'arrêt de la webapp.
     * </p>
     * @return
     */
    @SuppressWarnings("resource")
    private Node initialize() {

        final NodeBuilder nodeBuilder = NodeBuilder.nodeBuilder();

        if (null != settings && null == properties) {
            logger.warn("settings has been deprecated in favor of properties. See issue #15: https://github.com/dadoonet/spring-elasticsearch/issues/15.");
            nodeBuilder.getSettings().put(settings);
        }

        if (null != settingsFile && null == properties) {
            final Settings settings = Settings.builder().loadFromStream(settingsFile, ElasticsearchNodeFactoryBean.class.getResourceAsStream("/" + settingsFile)).build();
            nodeBuilder.getSettings().put(settings);
        }

        if (null != properties) {
            nodeBuilder.getSettings().put(properties);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Starting ElasticSearch node...");
        }
        // Il manque la méthode build avec plugins sur le NodeBuilder. On initialise directement le noeud, qu'on démarre après l'avoir construit.
        node = new PluginAwareNode(nodeBuilder.getSettings().build(), Version.CURRENT, plugins).start();
        logger.info("Node [" + node.settings().get("name") + "] for [" + node.settings().get("cluster.name") + "] cluster started...");
        if (logger.isDebugEnabled()) {
            logger.debug("  - home : " + node.settings().get("path.home"));
        }
        if (logger.isDebugEnabled()) {
            logger.debug("  - data : " + node.settings().get("path.data"));
        }
        if (logger.isDebugEnabled()) {
            logger.debug("  - logs : " + node.settings().get("path.logs"));
        }
        return node;
    }

    @Override
    public Node getObject() throws Exception {
        return async ? proxyfiedNode : node;
    }

    public final List<Class<? extends Plugin>> getPlugins() {
        return plugins;
    }

    private void setPlugins(final List<Class<? extends Plugin>> plugins) {
        this.plugins = plugins;
    }

    /**
     * Activation des plugins.
     * @param pluginsNames
     */
    @SuppressWarnings("unchecked")
    public void setPlugins(final String[] pluginsNames) {
        if (pluginsNames == null) {
            return;
        }

        final List<Class<? extends Plugin>> classes = new ArrayList<>(pluginsNames.length);
        for (final String className : pluginsNames) {
            try {
                final Class<?> pluginClass = Class.forName(className.trim());
                if (Plugin.class.isAssignableFrom(pluginClass)) {
                    classes.add((Class<? extends Plugin>) pluginClass);
                } else {
                    logger.warn("Plugin incompatible {}", className);
                }
            } catch (final ClassNotFoundException ex) {
                logger.warn("Plugin incompatible {}", className, ex);
            }
        }
        this.setPlugins(classes);
    }
}
