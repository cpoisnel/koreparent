package com.kosmos.search.exception;

/**
 * Exception soulevée pour une recherche Elasticsearch.
 * @author cpoisnel
 *
 */
public class SearchException extends RuntimeException {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 8355993107393938236L;

    /**
     * Exception avec message.
     * @param message
     * Message d'erreur
     */
    public SearchException(final String message) {
        super(message);
    }

    /**
     * Exception avec cause parente.
     * @param message
     * Message d'erreur
     * @param cause
     * Exception parente (nécessaire pour avoir toutes les informations lors du log de cette exception)
     */
    public SearchException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
