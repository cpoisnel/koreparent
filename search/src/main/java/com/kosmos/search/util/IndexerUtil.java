package com.kosmos.search.util;

import static org.elasticsearch.common.io.Streams.copy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.elasticsearch.common.io.stream.BytesStreamOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.GenericTypeResolver;

import com.kosmos.search.exception.IndexationException;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.objetspartages.om.ReferentielObjets;

/**
 * Itilitaire pour l'indexation.
 * @author cpoisnel
 *
 */
public final class IndexerUtil {

    private static final Logger LOG = LoggerFactory.getLogger(IndexerUtil.class);

    /**
     * Préfixe des index de fiches.
     */
    public static final String INDEX_FICHE = "fiche";

    public static final String INDEX_SEPARATEUR = "_";

    public static final String INDEX_WILDCARD = "*";

    /**
     * Type fiche (dans un index de fiche).
     */
    public static final String TYPE_FICHE = "fiche";


    public static final String TYPE_ALL = "_all";

    /**
     * Type ressource (media et ressource).
     */
    public static final String TYPE_RESSOURCE = "ressource";

    /**
     * Champ de contrôle d'accès, utilisé pour la DSI.
     */
    public static final String FIELD_ACCESSCONTROL="access_control";

    private IndexerUtil() {
        // Constructeur privé, pas d'instanciation de l'utilitaire.
    }

    /**
     * Copie d'un fichier dans un tableau de byte[].
     * <p>
     *     Attention, il n'est pas encore possible de streamer le flux Json à Elasticsearch pour des fichiers binaires.
     *     Le fichier est donc monté en mémoire.
     * </p>
     * @param path
     * Chemin de la ressource
     * @return Tableau d'octets de la ressource
     * @throws IOException Exception liée à la lecture du fichier
     */
    public static byte[] copyToBytes(final Path path) throws IOException {
        try (InputStream is = Files.newInputStream(path)) {
            if (is == null) {
                throw new FileNotFoundException("Ressource [" + path + "] non trouvée");
            }
            try (BytesStreamOutput out = new BytesStreamOutput()) {
                copy(is, out);
                return out.bytes().toBytes();
            }
        }
    }

    /**
     * A partir d'une fiche, déterminer le bean de persistence associé.
     * @param classeFiche
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static Class<?> resolveClassPersistenceBean(final Class<? extends AbstractOm> classeFiche) {
        final Class<?>[] classParameters = GenericTypeResolver.resolveTypeArguments(classeFiche, AbstractOm.class);
        final Class<?> classBean = classParameters[0];
        return classBean;
    }

    /**
     * Récupérer la classe d'une fiche à partir de son code Objet. Utilise le {@link ReferentielObjets} pour connaître la classe.
     * @param codeObjet
     * Code objet de la fiche (paramétré le plus souvent en XML)
     * @return la classe correspondant
     * @throws IndexationException si la classe n'a pas été trouvée
     */
    public static Class<?> getClasseFiche(final String codeObjet) {
        final String classeFicheChaine = ReferentielObjets.getClasseObjet(codeObjet);

        final Class<?> classeFiche;
        try {
            classeFiche = ClassUtils.getClass(classeFicheChaine);
        } catch (final ClassNotFoundException e) {
            throw new IndexationException(String.format("Classe '%s' non trouvée pour le code objet '%s'",classeFicheChaine,codeObjet), e);
        }
        return classeFiche;
    }

    /**
     * Renvoie un triplet issu du code parent d'une ressource.
     * @param codeParent
     * Code parent d'un objet ressource
     * @return
     * Un triplet contenant
     * <ul><li>Id de la fiche</li>
     * <li>Type de la fiche (code objet)</li>
     * <li>Numéro de la ressource</li>
     * </ul>
     */
    public static Triple<Long, String, Long> getRessourceIdTypeNumero(final String codeParent) {
        Triple<Long, String, Long> resultat = null;
        if (StringUtils.isNotEmpty(codeParent)) {
            final String[] codes = codeParent.split(",");
            if (codes.length == 3) {
                final Long id = Long.valueOf(codes[0]);

                String type = null;
                final String[] typeTab = codes[1].split("=");
                if (typeTab.length == 2) {
                    type = typeTab[1];
                }
                Long numero = null;
                final String[] numeroTab = codes[2].split("=");
                if (numeroTab.length == 2) {
                    numero = Long.valueOf(numeroTab[1]);
                }
                if (id != null && type != null && numero != null) {
                    resultat = Triple.of(id, type, numero);
                }
                LOG.debug("Triple déterminé à partir du code parent '{}' -> '{}'", codeParent, resultat);
            } else {
                LOG.warn("Impossible de déterminer les informations de la fiche pour le code parent '{}'", codeParent);
            }
        }
        return resultat;
    }

	/**
	 * Extrait les données contenues dans le fragment de codes spécifié en paramètres.
	 *
	 * <p>
	 * Formats attendus:
	 * </p>
	 * <ul>
	 * <li>1481812692311</li>
	 * <li>1481812692311,LANGUE=0</li>
	 * <li>1481812692311,LANGUE=0,TYPE=annuaireksup</li>
	 * </ul>
	 *
	 * @param frg
	 *            String - Le fragment de codes.
	 * @return ressource - Le code, la langue et le type de la ressource sinon <code>null</code>.
	 */
	public static Triple<String, String, String> getRessourceCodeLangueType(final String frg) {
		final Triple<String, String, String> ressource = null;
		if (StringUtils.isBlank(frg)) {
			return ressource;
		}
		final String[] datas = StringUtils.split(frg, ',');
		final int length = ArrayUtils.getLength(datas);
		if (ArrayUtils.isEmpty(datas)) {
			return ressource;
		}
		final String code = datas[0];
		String langue = null;
		String type = null;
		if (length > 1) {
			langue = StringUtils.substringAfter(datas[1], "=");
		}
		if (length > 2) {
			type = StringUtils.substringAfter(datas[2], "=");
		}
		return Triple.of(code, langue, type);
	}
}
