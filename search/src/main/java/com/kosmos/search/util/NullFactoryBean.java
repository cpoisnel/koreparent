package com.kosmos.search.util;

import org.springframework.beans.factory.FactoryBean;

/**
 * Factory d'objets null (utilitaire pour configuration Spring).
 * @author cpoisnel
 *
 * @param <T>
 *  Type de l'objet renvoyé.
 */
public class NullFactoryBean<T> implements FactoryBean<T> {

    private Class<T> type;

    public void setType(final Class<T> type) {
        this.type = type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<?> getObjectType() {
        return type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T getObject() throws Exception {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSingleton() {
        return true;
    }
}
