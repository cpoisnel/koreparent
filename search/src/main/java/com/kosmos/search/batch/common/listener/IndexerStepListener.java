package com.kosmos.search.batch.common.listener;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

/**
 * Listener avant / après étape d'un job.
 * @author cpoisnel
 *
 */
public class IndexerStepListener implements StepExecutionListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexerStepListener.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeStep(final StepExecution stepExecution) {
        LOGGER.info("[Step] Démarrage de l'étape '{}'", stepExecution.getStepName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExitStatus afterStep(final StepExecution stepExecution) {
        ExitStatus exitStatus;
        if (!ExitStatus.FAILED.equals(stepExecution.getExitStatus()) && stepExecution.getSkipCount() > 0) {
            LOGGER.warn("[Step] Etape '{}' avec {} élément(s) en erreur", stepExecution.getStepName(), stepExecution.getSkipCount());
            exitStatus = new ExitStatus("COMPLETED WITH SKIPS");
        } else {
            exitStatus = stepExecution.getExitStatus();
        }
        LOGGER.info("[Step] Fin de l'étape '{}' au statut '{}'. Temps passé : {} ms", new Object[] {stepExecution.getStepName(), exitStatus.getExitCode(), new Date().getTime() - stepExecution.getStartTime().getTime()});
        return exitStatus;
    }

}