package com.kosmos.search.batch.common.listener;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemWriteListener;

/**
 * Listener des écritures du job d'indexation.
 *
 * @author cpoisnel
 *
 * @param <S>
 *            Type de données persistées par le writer
 */
public class IndexerItemWriteListener<S> implements ItemWriteListener<S> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexerItemReadListener.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeWrite(final List<? extends S> items) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("[Write] Avant écriture des entités. Items=%s", items.toString()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterWrite(final List<? extends S> items) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("[Write] Après écriture des entités. Items=%s", items.toString()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onWriteError(final Exception exception, final List<? extends S> items) {
        LOGGER.error("[Write] Exception à l'écriture des entités. Items='{}'", items, exception);
    }
}
