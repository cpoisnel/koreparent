package com.kosmos.search.batch.common.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemProcessListener;

/**
 * Process listener, utilisé pour les traces du traitement.
 *
 * @param <T>
 *            Type d'entrée du processor.
 * @param <S>
 *            Type de sortie du processor.
 */
public class IndexerItemProcessListener<T, S> implements ItemProcessListener<T, S> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexerItemProcessListener.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeProcess(final T item) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("[Process] Traitement de l'entité '%s' [%s]", null == item ? "N/A" : item.getClass().getCanonicalName(), item));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterProcess(final T item, final S result) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("[Process] Fin de traitement de l'entité '%s' [%s]. Retour '%s' [%s] ", null == item ? "N/A" : item.getClass().getCanonicalName(), item, null == result ? "N/A" : result.getClass().getCanonicalName(), result));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onProcessError(final T item, final Exception exception) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("[Process] Erreur de traitement de l'entité '%s' [%s].", null == item ? "N/A" : item.getClass().getCanonicalName(), item), exception);
        }
    }
}
