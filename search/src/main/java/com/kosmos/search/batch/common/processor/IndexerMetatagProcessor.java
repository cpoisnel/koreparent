package com.kosmos.search.batch.common.processor;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.kosmos.search.index.service.ServiceCommandIndexation;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.services.ServiceRessource;

import com.kosmos.search.batch.common.bean.BulkIndexingCommand;
import com.kosmos.search.index.bean.IndexingCommand;
import com.kosmos.search.index.bean.IndexingCommand.IndexingAction;

/**
 * Traiter les metatags et créer des commandes d'indexation.
 * @author cpoisnel
 *
 */
public class IndexerMetatagProcessor implements ItemProcessor<MetatagBean, BulkIndexingCommand> {

    private static final Logger LOG = LoggerFactory.getLogger(IndexerMetatagProcessor.class);

    @Autowired
    @Qualifier("serviceIndexerMetatag")
    private ServiceCommandIndexation<MetatagBean> serviceIndexationMetatag;

    @Autowired
    @Qualifier("serviceIndexerRessource")
    private ServiceCommandIndexation<RessourceBean> serviceIndexationRessource;

    @Autowired
    private ServiceRessource serviceRessource;

    /**
     * Est-ce que l'indexation des ressources doit être faîte ?
     * <p>
     * Par défaut, l'indexation des ressources est réalisée (cas réindexation globale,...)
     * </p>
     */
    private boolean indexationRessources = true;

    /**
     * {@inheritDoc}
     */
    @Override
    public BulkIndexingCommand process(final MetatagBean metatagBean) throws Exception {
        final IndexingCommand indexingCommand = this.serviceIndexationMetatag.createIndexingCommand(metatagBean);
        if (indexingCommand == null) {
            LOG.debug("Aucune indexation pour ce metatag. Metatag : {}", metatagBean);
            return null;
        } else {
            final BulkIndexingCommand bulkIndexingCommand = new BulkIndexingCommand();
            bulkIndexingCommand.addIndexingCommand(indexingCommand);

            if (indexationRessources && indexingCommand.getAction() == IndexingAction.CREATE_UPDATE) {
                // Parcours de toutes les ressources
                final List<IndexingCommand> attachmentsIndexingCommands = this.createAttachmentIndexingCommands(metatagBean);
                for (final IndexingCommand attachmentIndexingCommand : attachmentsIndexingCommands) {
                    bulkIndexingCommand.addIndexingCommand(attachmentIndexingCommand);
                }
            }
            return bulkIndexingCommand;
        }

    }

    /**
     * Créer des commandes d'indexations pour l'ensemble des ressources associées à une fiche (donc un MetatagBean).
     * <p>
     *     Note : Attention, cette liste peut contenir des fichiers en mémoire, et donc chaque commande d'indexation peut
     * être volumineuse en mémoire.
     * </p>
     * @param metatagBean
     * MetatagBean (référençant la fiche)
     * @return la liste des commandes d'indexations.
     */
    protected List<IndexingCommand> createAttachmentIndexingCommands(final MetatagBean metatagBean) {
        final List<IndexingCommand> indexingCommandList = new ArrayList<>();
        final String codeParent = metatagBean.getMetaIdFiche() + ",TYPE=" + metatagBean.getMetaCodeObjet() + "%";
        final List<RessourceBean> ressources = this.serviceRessource.getFilesOrderBy(codeParent, "ORDRE");
        for (final RessourceBean ressourceBean : ressources) {
            indexingCommandList.add(serviceIndexationRessource.createIndexingCommand(ressourceBean));
        }
        return indexingCommandList;
    }

    public final void setServiceIndexationMetatag(final ServiceCommandIndexation<MetatagBean> serviceIndexationMetatag) {
        this.serviceIndexationMetatag = serviceIndexationMetatag;
    }

    public final void setServiceIndexationRessource(final ServiceCommandIndexation<RessourceBean> serviceIndexationRessource) {
        this.serviceIndexationRessource = serviceIndexationRessource;
    }

    public final void setServiceRessource(final ServiceRessource serviceRessource) {
        this.serviceRessource = serviceRessource;
    }


    public boolean isIndexationRessources() {
        return indexationRessources;
    }


    public void setIndexationRessources(final boolean indexationRessources) {
        this.indexationRessources = indexationRessources;
    }

}
