package com.kosmos.search.batch.common.writer;

import java.io.Serializable;
import java.util.List;

import org.elasticsearch.action.ActionRequest;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.deletebyquery.DeleteByQueryRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.kosmos.search.batch.common.bean.BulkIndexingCommand;
import com.kosmos.search.index.bean.IndexingCommand;
import com.kosmos.search.index.bean.IndexingCommand.IndexingAction;

/**
 * Ecriture des indexations dans ES.
 * <p>NOTE : Spring-Data (ES) n'était pas sorti en version finale pour la version 2.2 d'ES au moment. Une modification intégrant spring-data pourrait simplifier les développements
 * mais nécessite d'avoir Spring Data.</p>
 * @author cpoisnel
 *
 */
public class IndexerWriter implements ItemWriter<BulkIndexingCommand> {

    private static final Logger LOG = LoggerFactory.getLogger(IndexerWriter.class);

    /**
     * Object Mapper JSON. Par défaut : instance avec constructeur par défaut.
     */
    private ObjectMapper objectMapper;

    /**
     * Traitement par lots d'indexations Elasticsearch. Paramétré en bean spring.
     */
    private BulkProcessor bulkProcessor;

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(final List<? extends BulkIndexingCommand> bulkIndexingCommands) throws Exception {
        for (final BulkIndexingCommand bulkIndexingCommand : bulkIndexingCommands) {
            this.writeIndexingCommands(bulkIndexingCommand.getIndexingCommandList());
        }
        bulkProcessor.flush();
    }

    /**
     * Persister les commandes d'indexation.
     * @param indexingCommands
     */
    protected void writeIndexingCommands(final List<? extends IndexingCommand> indexingCommands) {
        for (final IndexingCommand indexingCommand : indexingCommands) {
            ActionRequest<?> request = null;
            final IndexingAction action = indexingCommand.getAction();
            if (IndexingAction.CREATE_UPDATE == action) {
                final IndexRequest indexRequest = new IndexRequest(indexingCommand.getIndex(), indexingCommand.getType(), indexingCommand.getId());
                indexRequest.parent(indexingCommand.getParent());
                final Serializable document = indexingCommand.getDocument();
                try {
                    // C'est la sérialisation qui porte la "transformation" d'attributs du bean en attributs elastic-query
                    final byte[] jsonsByte = this.objectMapper.writeValueAsBytes(document);
                    indexRequest.source(jsonsByte);
                    request = indexRequest;
                } catch (final JsonProcessingException e) {
                    LOG.error("Impossible de sérialiser le document {}. L'indexation est impossible pour le document [{}/{}/{}]", new Object[] {document, indexingCommand.getIndex(), indexingCommand.getType(), indexingCommand.getId(), e});
                }
            } else if (IndexingAction.DELETE == action) {
                final String idDocument = indexingCommand.getId();
                final String byQuery = indexingCommand.getByQuery();
                if (idDocument != null) {
                    request = new DeleteRequest(indexingCommand.getIndex(), indexingCommand.getType(), idDocument);
                } else if (byQuery != null) {
                    final DeleteByQueryRequest dbqRequest = new DeleteByQueryRequest();
                    dbqRequest.indices(indexingCommand.getIndex());
                    dbqRequest.types(indexingCommand.getType());
                    dbqRequest.source(indexingCommand.getByQuery());
                    request = dbqRequest;
                }
            }
            if (request != null) {
                bulkProcessor.add(request);
            } else {
                LOG.warn("Requête null ! Commande d'indexation : {}", indexingCommand);
            }
        }

    }

    public final void setObjectMapper(final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public final void setBulkProcessor(final BulkProcessor bulkProcessor) {
        this.bulkProcessor = bulkProcessor;
    }

    protected final ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    protected final BulkProcessor getBulkProcessor() {
        return bulkProcessor;
    }

}