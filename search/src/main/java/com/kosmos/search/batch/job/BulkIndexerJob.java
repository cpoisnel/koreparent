package com.kosmos.search.batch.job;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.scheduling.spring.quartz.LogReportJob;
import com.univ.utils.Chaine;

/**
 * Job massif d'indexation des metatag.
 * <p>
 *     Ce job n'est à lancer qu'au démarrage d'un projet ou en cas d'anomalie sur les indexations au fil de l'eau (Metatag, Ressources,...).
 * </p>
 * @author cpoisnel
 *
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class BulkIndexerJob extends LogReportJob {

    /**
     * Timestamp de job Spring Batch. Avec ce paramètre, on s'assure que le job n'est jamais lancé deux fois avec les mêmes paramètres, ce qui n'est pas autorisé par le launcher de
     * Job (restartable = false)
     */
    public static final String PARAMETRE_JOB_TIMESTAMP = "job.timestamp";
    /**
     * Codes Objet exclus de l'indexation
     */
    public static final String PARAMETRE_JOB_EXCLUSION_OBJET = "job.exclusionobjet";

    public static final String PARAMETRE_JOB_INCLUSION_OBJET = "job.inclusionobjet";

    private static final Logger LOGGER = LoggerFactory.getLogger(BulkIndexerJob.class);

    private final JobLauncher jobLauncher;

    private final Job job;


    /**
     * Constructeur par défaut.
     */
    public BulkIndexerJob() {
        // Le bean n'est pas orchestré par Spring, on doit faire manuellement les affectations.
        this.jobLauncher = ApplicationContextManager.getCoreContextBean("jobLauncher", JobLauncher.class);
        this.job = ApplicationContextManager.getCoreContextBean("searchIndexerJob", Job.class);
    }

    /**
     * {@inheritDoc}
     * <p>Lancement du job Spring Batch.</p>
     */
    @Override
    public void perform() {
        LOGGER.info("Lancement du traitement d'indexation");
        // On ajoute en propriétés du job les informations nécessaires
        final Map<String, JobParameter> jobParametersMap = new HashMap<>();
        final Date dateLancement = new Date();
        jobParametersMap.put(PARAMETRE_JOB_TIMESTAMP, new JobParameter(dateLancement));
        // Calcul des codes objets exclus (liste noire)
        final Collection<String> objetsExclus = Chaine.getVecteurPointsVirgules(PropertyHelper.getCoreProperty("search.restriction_objet"));
        if (CollectionUtils.isNotEmpty(objetsExclus)) {
            jobParametersMap.put(PARAMETRE_JOB_EXCLUSION_OBJET, new JobParameter(StringUtils.join(objetsExclus, ",")));
        }

        // Calcul des codes objets inclus (liste blanche)
        final Collection<String> objetsInclus = Chaine.getVecteurPointsVirgules(PropertyHelper.getCoreProperty("search.inclusion_objet"));
        if (CollectionUtils.isNotEmpty(objetsInclus)) {
            jobParametersMap.put(PARAMETRE_JOB_INCLUSION_OBJET, new JobParameter(StringUtils.join(objetsInclus, ",")));
        }

        final JobParameters jobParameters = new JobParameters(jobParametersMap);
        LOGGER.info("Paramètres du job d'indexation : {}", jobParametersMap);
        try {
            final JobExecution execution = this.jobLauncher.run(this.job, jobParameters);
            final BatchStatus status = execution.getStatus();
            final ExitStatus exitStatus = execution.getExitStatus();
            LOGGER.info("Fin du job  d'indexation au statut '{}' (exit status : '{}')", status, exitStatus);
        } catch (final JobExecutionException e) {
            LOGGER.error("Une erreur a été rencontrée lors de l'exécution du traitement", e);
        }
    }

    public final JobLauncher getJobLauncher() {
        return jobLauncher;
    }

    public final Job getJob() {
        return job;
    }

}