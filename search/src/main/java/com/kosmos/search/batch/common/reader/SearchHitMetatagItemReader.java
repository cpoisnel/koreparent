package com.kosmos.search.batch.common.reader;

import java.util.Map;

import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.ReaderNotOpenException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.services.ServiceMetatag;

/**
 *
 * Reader de metatag, qui prend en attribut délégué un reader Elasticsearch.
 * Cela permet de chercher un metatag à partir d'un id issu d'un résultat de recherche d'Elasticsearch.
 * @author cpoisnel
 *
 */
public class SearchHitMetatagItemReader implements ItemReader<MetatagBean> {

    private static final Logger LOG = LoggerFactory.getLogger(SearchHitMetatagItemReader.class);

    @Autowired
    private ServiceMetatag serviceMetatag;

    private ItemReader<SearchHit> delegate;

    /**
     * Lecture du bean metatag, on utilise un reader délégué pour effectuer une recherche de Metatag.
     */
    @Override
    public MetatagBean read() throws Exception {
        final SearchHit searchHit = delegate.read();
        MetatagBean metatagBean = null;
        if (searchHit != null) {
            metatagBean = serviceMetatag.getById(Long.valueOf(searchHit.getId()));
        }
        else {
            LOG.trace("Aucune donnée renvoyée par le reader {}",delegate);
        }
        return metatagBean;
    }

    /**
     * @return the serviceMetatag
     */
    public ServiceMetatag getServiceMetatag() {
        return serviceMetatag;
    }

    /**
     * @param serviceMetatag the serviceMetatag to set
     */
    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    /**
     * @return the delegate
     */
    public ItemReader<SearchHit> getDelegate() {
        return delegate;
    }

    /**
     * @param delegate the delegate to set
     */
    @Required
    public void setDelegate(final ItemReader<SearchHit> delegate) {
        this.delegate = delegate;
    }

}
