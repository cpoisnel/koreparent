package com.kosmos.search.batch.common.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.StepExecution;

/**
 * Listener des jobs Spring Batchs.
 *
 * <p>
 * Traitement pre / post Job d'indexation
 * </p>
 *
 */
public class IndexerJobListener implements JobExecutionListener {

    private static final Logger LOG = LoggerFactory.getLogger(IndexerJobListener.class);

    /**
     * <p>
     * Création du job.
     * </p>
     * {@inheritDoc}
     */
    @Override
    public void beforeJob(final JobExecution jobExecution) {
        LOG.debug(String.format("Le traitement d'indexation commence le %1$td/%1$tm/%1$tY à %1$tT", new Date()));
    }

    /**
     * <p>
     * Traitement après arrêt du job. Affiche un compte-rendu du traitement Elasticsearch d'indexation et affiche les exceptions remontées pendant le traitement.
     * <pre>
     *
     *
     * </pre>
     * </p>
     * {@inheritDoc}
     */
    @Override
    public void afterJob(final JobExecution jobExecution) {
        final long durationMs = getTimeInMillis(jobExecution.getStartTime(), jobExecution.getEndTime());
        final String durationHMS = getTimeInHoursMinSec(durationMs);

        final ExitStatus exitStatus = ExitStatus.COMPLETED;
        final Collection<StepExecution> collectionStepExecution = jobExecution.getStepExecutions();
        final List<StepExecution> listeStepExecution = new ArrayList<StepExecution>(collectionStepExecution);
        // On parcourt les étapes pour savoir s'il y a eu des exécutions terminées avec des erreurs (statut!=COMPLETED)
        LOG.info(String.format("%-25s|%-20s|%-20s|%-20s|%-20s", "Etape", "Eléments lus", "Eléments filtrés", "Eléments indexés", "Erreurs"));
        Collections.sort(listeStepExecution, new Comparator<StepExecution>() {

            @Override
            public int compare(final StepExecution o1, final StepExecution o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        for (final StepExecution stepExecution : listeStepExecution) {
            LOG.info(String.format("%-25s|%-20s|%-20s|%-20s|%-20s", stepExecution.getStepName(), stepExecution.getReadCount(), stepExecution.getFilterCount(), stepExecution.getWriteCount(), stepExecution.getSkipCount()));
            if (!stepExecution.getExitStatus().getExitCode().equals(exitStatus.getExitCode()) && !jobExecution.getExitStatus().getExitCode().equals("COMPLETED WITH SKIPS")) {
                jobExecution.setExitStatus(new ExitStatus("COMPLETED WITH SKIPS"));
            }
        }
        // On remonte les exceptions du traitement
        final List<Throwable> listeExceptions = jobExecution.getAllFailureExceptions();
        if (CollectionUtils.isNotEmpty(listeExceptions)) {
            for (int index = 0; index < listeExceptions.size(); index++) {
                final Throwable exception = listeExceptions.get(index);
                LOG.error(String.format("Exception fatale n° %d : %s", index + 1, exception.getMessage()), exception);
            }
        }
        LOG.info(String.format("Le traitement d'indexation s'est terminé le %1$td/%1$tm/%1$tY à %1$tT. Durée : %2$s", jobExecution.getEndTime(), durationHMS));
    }

    /**
     * Renvoie le temps passé entre deux dates.
     *
     * @param start
     *            Date de début
     * @param stop
     *            Date de fin
     * @return la différence de temps en ms
     */
    private static long getTimeInMillis(final Date start, final Date stop) {
        return stop.getTime() - start.getTime();
    }

    private static String getTimeInHoursMinSec(final long durationMs) {
        return DurationFormatUtils.formatDurationHMS(durationMs);
    }
}