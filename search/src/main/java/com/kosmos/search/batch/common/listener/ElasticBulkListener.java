package com.kosmos.search.batch.common.listener;

import org.apache.commons.lang3.ArrayUtils;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkItemResponse.Failure;
import org.elasticsearch.action.bulk.BulkProcessor.Listener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listener sur le traitement par lot Elasticsearch.
 * @see org.elasticsearch.action.bulk.BulkProcessor
 *
 */
public class ElasticBulkListener implements Listener {

    private static final Logger LOG = LoggerFactory.getLogger(ElasticBulkListener.class);

    /**
     * {@inheritDoc}
     * <p>
     *     Ajoute des logs sur les traitements en masse des indexation.
     * </p>
     */
    @Override
    public void afterBulk(final long executionId, final BulkRequest request, final BulkResponse response) {
        LOG.trace("Execution '{}'. Requete : {}. Durée : {}", executionId, request, response.getTook());
        final BulkItemResponse[] responseItems = response.getItems();
        if (ArrayUtils.isNotEmpty(responseItems)) {
            for (final BulkItemResponse item : responseItems) {
                final Failure failure = item.getFailure();
                if (failure != null) {
                    LOG.error("Erreur à l'indexation [{}/{}/{}].", new Object[] {item.getIndex(), item.getType(), item.getId(), failure.getCause()});
                } else {
                    LOG.info("Indexation effectuée avec succès [{}/{}/{}].", new Object[] {item.getIndex(), item.getType(), item.getId()});
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     *  <p>
     *     Ajoute des logs sur les traitements en masse des indexation.
     * </p>
     */
    @Override
    public void afterBulk(final long executionId, final BulkRequest request, final Throwable failure) {
        LOG.trace("Execution '{}'. Requete : {}.", executionId, request, failure);
    }

    /**
     * {@inheritDoc}
     *  <p>
     *     Ajoute des logs sur les traitements en masse des indexation.
     * </p>
     */
    @Override
    public void beforeBulk(final long executionId, final BulkRequest request) {
        LOG.trace("Execution '{}'. Taille du paquet : {} octets", executionId, request.estimatedSizeInBytes());
    }

}
