/**
 * Batchs de traitement massifs d'<b>indexation</b>, basés sur les implémentations Spring-Batch.
 * <p>
 *     <ul>
 *          <li>Le script d'indexation du produit, accessible depuis l'écran est défini dans {@link com.kosmos.search.batch.job.BulkIndexerJob}. Ce job a pour objectif de lancer un job  Spring-Batch dont l'ensemble des traitements sont définis dans le package de ressources com.kosmos.search.batch</li>
 *          <li>Le package common contient l'arborescence attendue pour un traitement spring batch.</li>
 *      </ul>
 *      Ce package de batch est lié au package index (utilisation des services,...)
 * </p>
 *
 *
 * @author cpoisnel
 */
package com.kosmos.search.batch;