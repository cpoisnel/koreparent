package com.kosmos.search.batch.util;

import com.kosmos.log.LoggerContextUtils;
import com.kosmos.log.ThreadLogContextPoolTaskExecutor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.scope.context.JobSynchronizationManager;

/**
 * Task Executor permettant de gérer le scope "job" des beans, en multi-threadé (exemple : les beans utilisés pour les données traitées).
 * <p>
 * Un ticket référence l'absence native de la gestion des scope "job" en multi-threadé : https://jira.spring.io/browse/BATCH-2269.
 * D'autre part, permet de recopier le contexte de log dans les threads lancés (comme {@link ThreadLogContextPoolTaskExecutor})
 * </p>
 * @author cpoisnel
 */
public class ThreadPoolJobTaskExecutor extends ThreadLogContextPoolTaskExecutor {


    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -6624962078033060636L;

    /**
     * <p>
     * Exécute une tâche en enregistrant pour chaque tâche le contexte d'éxécution du job et le recopiant dans la tâche
     * executée le contexte MDC.
     * </p>
     * {@inheritDoc}
     */
    @Override
    public void execute(final Runnable task) {
        // Récupère le jobExecution du thread local.
        final JobExecution jobExecution = JobSynchronizationManager.getContext().getJobExecution();
        // Recopie des contextes de logs
        final String contexteLog = LoggerContextUtils.getLogContext();
        super.execute(new Runnable() {

            /**
             * <p>
             * Enregistre le contexte d'exécution de job, initialise le contexte des logs.
             * </p>
             * {@inheritDoc}
             */
            @Override
            public void run() {
                JobSynchronizationManager.register(jobExecution);
                if (StringUtils.isNotEmpty(contexteLog)) {
                    LoggerContextUtils.initContextLog(contexteLog);
                }
                try {
                    task.run();
                } finally {
                    JobSynchronizationManager.close();
                    LoggerContextUtils.closeContextLog();
                }
            }
        });
    }
}
