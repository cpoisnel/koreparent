package com.kosmos.search.batch.common.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.kosmos.search.index.service.ServiceIndexer;
import com.kosmos.search.util.IndexerUtil;

/**
 * Initialisation de l'indexation.
 * @author cpoisnel
 *
 */
public class IndexationFichesInitTasklet implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger(IndexationFichesInitTasklet.class);

    @Autowired
    private ServiceIndexer serviceIndexer;

    /**
     * {@inheritDoc}
     */
    @Override
    public RepeatStatus execute(final StepContribution arg0, final ChunkContext arg1) throws Exception {
        LOG.info("Suppression des indices existants");
        this.serviceIndexer.deleteIndex(IndexerUtil.INDEX_FICHE + IndexerUtil.INDEX_WILDCARD);
        return RepeatStatus.FINISHED;
    }

}
