package com.kosmos.search.batch.common.reader;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.TemplateQueryBuilder;
import org.elasticsearch.script.Template;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.database.AbstractPagingItemReader;
import org.springframework.util.Assert;

import com.kosmos.search.exception.SearchException;

/**
 * Item Reader Elasticsearch, prenant en paramètre un Template (avec paramètres), permettant de faire des recherches en batch.
 *
 *
 * @author cpoisnel
 *
 */
public class ElasticsearchTemplateScrollItemReader extends AbstractPagingItemReader<SearchHit> {

    private static final Logger LOG = LoggerFactory.getLogger(ElasticsearchTemplateScrollItemReader.class);

    protected static int DEFAULT_TIMEOUT_DURATION = 60;

    protected static TimeUnit DEFAULT_TIMEOUT_UNIT = TimeUnit.SECONDS;

    /**
     * Client ElasticSearch. Doit nécessairement être renseigné.
     */
    private Client client;

    /**
     * Template Elasticsearch, permettant de définir une recherche sous forme Texte (json), avec template Mustache.
     */
    private Template template;

    /**
     * Indices recherchés. Par défaut, recherche dans tous les indices.
     */
    private String[] indices = new String[] {
        "*"
    };

    /**
     * Types de fiches recherchés. Par défaut, tous les types sont recherchés.
     */
    private String[] types;

    /**
     * Durée du Scroll Elasticsearch. Par défaut, {@link #DEFAULT_TIMEOUT_DURATION} et {@link #DEFAULT_TIMEOUT_UNIT}.
     */
    private TimeValue timeout = new TimeValue(DEFAULT_TIMEOUT_DURATION, DEFAULT_TIMEOUT_UNIT);

    /**
     * Scroll ID, valeur spécifique
     */
    protected volatile String scrollId;

    /**
     * Lecture d'une page de données Elasticsearch.
     * <p>
     * On utilise le fonctionnement Scroll d'Elasticsearch, qui permet de faire une première requête, d'obtenir un ID de scroll, et de refaire la même requête avec la référence de l'ID de scroll.
     * <a href="https://www.elastic.co/guide/en/elasticsearch/client/java-api/current/java-search-scrolling.html">Scroll Java API</a>
     * </p>
     */
    @Override
    protected void doReadPage() {
        if (results == null) {
            results = new CopyOnWriteArrayList<>();
        } else {
            results.clear();
        }
        final SearchResponse scrollResp;
        // Première page = initialisation de la requête
        if (getPage() == 0) {
            LOG.debug("Première page, calcul du template : {}", template);
            final TemplateQueryBuilder templateQueryBuilder = new TemplateQueryBuilder(template);
            LOG.debug("TemplateQueryBuilder : {}, Indices : {}, Types : {}, Timeout : {}, Size : {}.", templateQueryBuilder, indices, types, timeout, this.getPageSize());
            // Les options d'indices sont paramétrées pour ignorer les erreurs d'index non présents et d'ouvrir les index wildcard fiche*
            scrollResp = client.prepareSearch(indices).setTypes(types).setIndicesOptions(IndicesOptions.fromOptions(true,true,true,false))
                .setScroll(timeout).setQuery(templateQueryBuilder).setSize(this.getPageSize()).execute().actionGet();
            if (scrollResp.getScrollId() != null) {
                scrollId = scrollResp.getScrollId();
            } else {
                LOG.error("Le ScrollID n'est pas renvoyé, impossible de faire les requêtes paginées. Requête :\n{}\nRetour :\n{}", template,scrollResp);
                throw new SearchException("ScrollId inexistant");
            }
        } else {
            scrollResp = client.prepareSearchScroll(scrollId).setScroll(timeout).execute().actionGet();
        }
        // Si la liste de résultats est vide, ça n'ira pas plus loin !
        results = new CopyOnWriteArrayList<>(scrollResp.getHits().hits());
    }

    /**
     * N'est pas implémenté (non supporté, mais ne soulève pas d'erreur, comme pour la méthode du reader Hibernate).
     * @param itemIndex
     * Index de l'élément
     */
    @Override
    protected void doJumpToPage(final int itemIndex) {
        // On ne peut pas faire ça...  (pour l'instant)
        // N'est pas implémenté
    }

    /**
     * @return the client
     */
    protected Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(final Client client) {
        this.client = client;
    }

    /**
     * @return the template
     */
    public Template getTemplate() {
        return template;
    }

    /**
     * @param template the template to set
     */
    public void setTemplate(final Template template) {
        this.template = template;
    }

    /**
     * @return the types
     */
    public String[] getTypes() {
        return types;
    }

    /**
     * @param types the types to set
     */
    public void setTypes(final String[] types) {
        this.types = types;
    }

    /**
     * @return the indices
     */
    public String[] getIndices() {
        return indices;
    }

    /**
     * @return the timeout
     */
    public TimeValue getTimeout() {
        return timeout;
    }

    /**
     * @param indices the indices to set
     */
    public void setIndices(final String[] indices) {
        this.indices = indices;
    }

    /**
     * @param timeout the timeout to set
     */
    public void setTimeout(final TimeValue timeout) {
        this.timeout = timeout;
    }

    /**
     * Check mandatory properties.
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
        Assert.notNull(client);
        Assert.notNull(template);
        Assert.notNull(indices);
    }

}
