package com.kosmos.search.subscribe.filter;

import java.util.Arrays;
import java.util.Map;

import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableMap;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.EtatFiche;

import mockit.Expectations;
import mockit.Mocked;

/**
 * Created by cpoisnel on 15/12/16.
 */
@Test
public class MetatagSelectorTest {


    @Test
    public void testAccept() {
        MetatagSelector metatagSelector = new MetatagSelector();
        MetatagBean metatagBean = new MetatagBean();
        metatagBean.setId(1L);
        metatagBean.setMetaCodeObjet("9998");

        Map<String, Object> mapHeaders = ImmutableMap.of("target", (Object) "metatag", "action", (Object) "save", "targetId", (Object) metatagBean.getId());
        Assert.assertTrue(metatagSelector.accept(MessageBuilder.createMessage(metatagBean, new MessageHeaders(mapHeaders))));

        metatagSelector = new MetatagSelector();
        metatagSelector.setObjetsInclus(Arrays.asList("9997"));

        Assert.assertFalse(metatagSelector.accept(MessageBuilder.createMessage(metatagBean, new MessageHeaders(mapHeaders))));


        metatagBean.setMetaCodeObjet("9999");
        metatagSelector = new MetatagSelector();
        metatagSelector.setObjetsExclus(Arrays.asList("9998"));
        Assert.assertTrue(metatagSelector.accept(MessageBuilder.createMessage(metatagBean, new MessageHeaders(mapHeaders))));

        metatagBean.setMetaCodeObjet("9999");
        metatagSelector = new MetatagSelector();
        Assert.assertTrue(metatagSelector.accept(MessageBuilder.createMessage(metatagBean, new MessageHeaders(mapHeaders))));


        metatagBean.setMetaCodeObjet("9996");
        metatagBean.setMetaEtatObjet(EtatFiche.EN_LIGNE.getEtat());


        metatagSelector = new MetatagSelector();

        metatagSelector.setEtatsAutorises(Arrays.asList(EtatFiche.EN_LIGNE));
        metatagSelector.setObjetsExclus(Arrays.asList("9997"));
        metatagSelector.setObjetsInclus(Arrays.asList("9996"));

        Assert.assertTrue(metatagSelector.accept(MessageBuilder.createMessage(metatagBean, new MessageHeaders(mapHeaders))));
    }
}