package com.kosmos.search.query.util;

import java.net.URL;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.common.collect.Multimap;
import com.kosmos.search.query.util.URLQueryParser;

/**
 *
 * Created by cpoisnel on 15/12/16.
 */
public class URLQueryParserTest {

    @Test
    public void testSplitQuery() throws Exception {
        String urlEmpty = "http://www.kosmos.fr/";
        Multimap<String, String> mapParamsEmpty = URLQueryParser.splitQuery(new URL(urlEmpty));
        Assert.assertEquals(mapParamsEmpty.size(), 0);
        String urlEmpty2 = "http://www.kosmos.fr/query?";
        Multimap<String, String> mapParamsEmpty2 = URLQueryParser.splitQuery(new URL(urlEmpty2));
        Assert.assertEquals(mapParamsEmpty2.size(), 0);
        String urlNull = "http://www.kosmos.fr/query?q";
        Multimap<String, String> mapParamsNull = URLQueryParser.splitQuery(new URL(urlNull));
        Assert.assertEquals(mapParamsNull.size(), 1);
        Assert.assertTrue(mapParamsNull.containsEntry("q", null));
        String url = "http://www.kosmos.fr/query?q=test&p=2&facet_a=1&facet_b=2&facet_a=%20";
        Multimap<String, String> mapParams = URLQueryParser.splitQuery(new URL(url));
        Assert.assertEquals(mapParams.keySet().size(), 4);
        Assert.assertEquals(mapParams.size(), 5);
        Assert.assertEquals(mapParams.get("facet_a").size(), 2);
        Assert.assertTrue(mapParams.containsEntry("q", "test"));
        Assert.assertTrue(mapParams.containsEntry("p", "2"));
        Assert.assertTrue(mapParams.containsEntry("facet_a", "1"));
        Assert.assertTrue(mapParams.containsEntry("facet_a", " "), "Le paramètre facet_a n'est pas décodé");
        Assert.assertFalse(mapParams.containsEntry("facet_a", "%20"), "Le paramètre facet_a doit être décodé");
        Assert.assertTrue(mapParams.containsEntry("facet_b", "2"));
    }
}