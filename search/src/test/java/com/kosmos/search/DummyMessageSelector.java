package com.kosmos.search;

import org.springframework.integration.core.MessageSelector;
import org.springframework.messaging.Message;

/**
 * Created by cpoisnel on 15/12/16.
 */
public class DummyMessageSelector implements MessageSelector {

    @Override
    public boolean accept(final Message<?> message) {
        return true;
    }
}
