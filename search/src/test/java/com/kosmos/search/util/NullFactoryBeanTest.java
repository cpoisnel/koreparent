package com.kosmos.search.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by cpoisnel on 15/12/16.
 */
@Test
@ContextConfiguration
public class NullFactoryBeanTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @Qualifier("testBean")
    String testBean;

    @Test
    public void testBean() {
        Assert.assertEquals(testBean, null);
    }
}