package com.kosmos.search;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.MessageDispatchingException;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageDeliveryException;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableMap;
import com.univ.objetspartages.bean.MetatagBean;


/**
 * Created by cpoisnel on 14/12/16.
 */
@Test
@ContextConfiguration
public class IntegrationSearchTest extends AbstractTestNGSpringContextTests {


    @Autowired
    @Qualifier("channel.in.core")
    private MessageChannel inputChannel;


    @Test(expectedExceptions = MessageDeliveryException.class)
    public void testSubscribeMetatag() {
        // On envoie un message sur le channel principal
        MetatagBean metatagBean = new MetatagBean();
        metatagBean.setId(1L);
        Map<String, Object> mapHeaders = ImmutableMap.of("target", (Object) "metatag", "action", (Object) "save", "targetId", (Object) metatagBean.getId());
        inputChannel.send(MessageBuilder.createMessage(metatagBean, new MessageHeaders(mapHeaders)));
    }
}
