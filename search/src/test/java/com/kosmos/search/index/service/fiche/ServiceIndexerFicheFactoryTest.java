package com.kosmos.search.index.service.fiche;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.search.exception.IndexationException;
import com.kosmos.search.index.service.fiche.impl.ServiceIndexerFicheBeanImpl;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.bean.AbstractRestrictionFicheBean;
import com.univ.objetspartages.bean.PagelibreBean;

import mockit.Expectations;
import mockit.Mocked;

/**
 * Created by cpoisnel on 13/12/16.
 */
@Test
@ContextConfiguration
public class ServiceIndexerFicheFactoryTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ServiceIndexerFicheFactory serviceIndexerFicheFactory;

    @Mocked(stubOutClassInitialization = true)
    ApplicationContextManager mockedAppCtx;

    @Test(expectedExceptions = IndexationException.class)
    public void testGetServiceIndexerFicheAbsent() throws Exception {
        Class<?> classe = PagelibreBean.class;
        final Map<String, ServiceIndexerFiche> mapServices = new HashMap<>();
        new Expectations() {{
            ApplicationContextManager.getAllBeansOfType(ServiceIndexerFiche.class);
            result = mapServices;
        }};
        serviceIndexerFicheFactory.getServiceIndexerFiche(classe);
    }

    public void testGetServiceIndexerFiche() throws Exception {
        Class<?> classe = PagelibreBean.class;
        final Map<String, ServiceIndexerFiche> mapServices = new HashMap<>();
        ServiceIndexerFiche defaultServiceIndexer = new ServiceIndexerFicheBeanImpl();
        mapServices.put("1", defaultServiceIndexer);
        new Expectations() {{
            ApplicationContextManager.getAllBeansOfType(ServiceIndexerFiche.class);
            result = mapServices;
        }};
        ServiceIndexerFiche serviceIndexerFiche = serviceIndexerFicheFactory.getServiceIndexerFiche(classe);
        Assert.assertNotNull(serviceIndexerFiche, "Le service d'indexation ne peut pas être nul");
        mapServices.put("2", new ServiceIndexerFicheBeanImpl(PagelibreBean.class));
        mapServices.put("3", new ServiceIndexerFicheBeanImpl(AbstractRestrictionFicheBean.class));
        ServiceIndexerFiche serviceIndexerFiche2 = serviceIndexerFicheFactory.getServiceIndexerFiche(classe);
        Assert.assertEquals(serviceIndexerFiche2.getType(), PagelibreBean.class, "Le service d'indexation doit être celui de la page libre");

        ServiceIndexerFiche serviceIndexerFiche3 = serviceIndexerFicheFactory.getServiceIndexerFiche(null);
        Assert.assertEquals(serviceIndexerFiche3.getType(), AbstractFicheBean.class, "Le service d'indexation doit être celui par défaut");
    }
}